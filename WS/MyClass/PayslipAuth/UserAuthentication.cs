﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using illimitadoWepAPI.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace illimitadoWepAPI.Methods.PayslipAuth
{
    public class UserAuthentication : IDisposable
    {
        public string ValidateUser(PAUserLogin model)
        {
            #region Appsettings
            string path = HttpContext.Current.Server.MapPath("~/appsettings.json");

            string jsonString = File.ReadAllText(path);
            var jsonObject = JObject.Parse(jsonString);
            var jsonPACredentials = jsonObject["LoginPA"].ToString();
            LoginPA loginPACredentials = JsonConvert.DeserializeObject<LoginPA>(jsonPACredentials);
            #endregion

            bool isUsernameValid = model.username == loginPACredentials.username ? true : false;
            bool isPasswordValid = model.password == loginPACredentials.password ? true : false;

            if (isUsernameValid && isPasswordValid)
                return "true";
            else
                return "false";
        }

        public void Dispose()
        {
            // Dispose here
        }
    }
}