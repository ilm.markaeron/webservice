﻿using illimitadoWepAPI.Methods.PayslipAuth;
using illimitadoWepAPI.Models;
using Microsoft.Owin.Security.OAuth;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;

namespace illimitadoWepAPI.PayslipAuth
{
    public class AuthorizationServerProvider : OAuthAuthorizationServerProvider
    {
        public override async Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            context.Validated();
        }

        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            using (UserAuthentication OBJ = new UserAuthentication())
            {
                var user  = OBJ.ValidateUser(new PAUserLogin { username = context.UserName, password = context.Password } );
                if (user == "false")
                {
                    context.SetError("invalid_grant", "Username or password is incorrect");
                    return;
                }

                #region Appsettings
                string path = HttpContext.Current.Server.MapPath("~/appsettings.json");

                string jsonString = File.ReadAllText(path);
                var jsonObject = JObject.Parse(jsonString);
                var jsonPAUser = jsonObject["UserPA"].ToString();
                UserPA userPA = JsonConvert.DeserializeObject<UserPA>(jsonPAUser);
                #endregion

                var identity = new ClaimsIdentity(context.Options.AuthenticationType);
                identity.AddClaim(new Claim(ClaimTypes.Role, userPA.role));
                identity.AddClaim(new Claim(ClaimTypes.Name, userPA.name));

                context.Validated(identity);
            }
        }
    }
}