﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Reflection;
using System.Web;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using illimitadoWepAPI.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace illimitadoWepAPI.Methods.PayslipAuth
{
    public class PayslipAuthMethods
    {

        const string LIVE_WEB_ROOT_PATH = "https://ws.durusthr.com/ILM_WS_Live";

        #region Checkers

        public List<string> ValidateCrawfordModel(PayslipModelPA model)
        {
            string code = model.Code.ToLower();
            List<string> errorList = new List<string>();

            if (!(code == "bsi" || code == "ccli" || code == "ccbpi"))
            {
                errorList.Add("Code does not exist");
            }

            return errorList;
        }

        #endregion

        #region Database

        public ConnectionStringModel GetConnectionString(string code)
        {
            ConnectionStringModel _csm = new ConnectionStringModel(); 
            code = code.ToLower();

            #region appsettings
            //series_code = Crypto.url_decrypt(series_code);
            string path = HttpContext.Current.Server.MapPath("~/appsettings.json");

            //StreamReader r = new StreamReader(path);
            string jsonString = File.ReadAllText(path);
            var jsonObject = JObject.Parse(jsonString);
            var jsonCrawfordConnectionString = jsonObject["CrawfordConnectionString"].ToString();
            ConnectionStringModel connectionObject = JsonConvert.DeserializeObject<ConnectionStringModel>(jsonCrawfordConnectionString);
            #endregion

            string connectionString = "0";

            if (code == "bsi" || code == "ccli" || code == "ccbpi")
            {
                connectionString = $"Data Source={connectionObject.instance_name}; Database = {connectionObject.catalog};User Id={connectionObject.user_name};Password={connectionObject.user_hash};MultipleActiveResultSets=True;";

                _csm.DB_Master = connectionObject.DB_Master;
                _csm.instance_name = connectionObject.instance_name;
                _csm.catalog = connectionObject.catalog;
                _csm.user_name = connectionObject.user_name;
                _csm.user_hash = connectionObject.user_hash;

                return _csm;
            }
            else
            {
                return null;
            }
        }

        public DataTable ToDataTable<T>(List<T> items)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);
            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);



            foreach (PropertyInfo prop in Props)
            {
                dataTable.Columns.Add(prop.Name);
            }



            foreach (T item in items)
            {
                var values = new object[Props.Length];
                for (int i = 0; i < Props.Length; i++)
                {
                    values[i] = Props[i].GetValue(item, null);
                }
                dataTable.Rows.Add(values);
            }
            return dataTable;
        }

        #endregion

        #region Payslip Generation

        private class payslip_object
        {
            public string employee_number { get; set; }
            public string payslip_name { get; set; }
            public string payslip_link { get; set; }
        }

        private class payslip_json
        {
            public string EmployeeNumber { get; set; }
            public string PeriodStartDate { get; set; }
            public string PeriodEndDate { get; set; }
            public string ActualFileName { get; set; }
            public string NetPay { get; set; }
        }

        public string GenerateAndCompressPayslips(DataTable _dt_users, string _tenant_id, PayslipModelPA model)
        {
            string response = "";
            ConnectionStringModel csm = GetConnectionString(model.Code);
            SqlConnection oConn = new SqlConnection(csm.DB_Master);
            SqlTransaction oTrans;
            oConn.Open();
            oTrans = oConn.BeginTransaction();
            SqlCommand oCmd = new SqlCommand();
            oCmd.Connection = oConn;
            oCmd.Transaction = oTrans;

            try
            {
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = oCmd;

                // Generate Payslip
                DataTable dt_payslip = new DataTable();

                oCmd.CommandText = "sp_GetClientUserIds_Payslip";
                da.SelectCommand.CommandType = CommandType.StoredProcedure;

                oCmd.Parameters.Clear();
                oCmd.Parameters.AddWithValue("@tenantID", _tenant_id);
                oCmd.Parameters.AddWithValue("@empID", _dt_users);
                da.Fill(dt_payslip);

                oConn.Close();

                // Declare directories
                var webRootPath = HttpContext.Current.Server.MapPath("~");
                string customDir = $"GeneratedPayslip/Compressed/{model.Code.ToUpper()}/";
                string reportDir = Path.Combine(webRootPath, customDir);
                string liveDir = Path.Combine(LIVE_WEB_ROOT_PATH, customDir);
                string zipFileName = $"{model.Code.ToUpper()} Payslips - {model.PayoutDate}.zip";

                if (!Directory.Exists(reportDir))
                    Directory.CreateDirectory(reportDir);

                if (File.Exists(reportDir + zipFileName))
                    File.Delete(reportDir + zipFileName);

                string payout_datetime = Convert.ToDateTime(model.PayoutDate).ToString("yyyy-MM-dd") + " 00:00:00.0000000";
                List<payslip_object> payslipObjectList = new List<payslip_object>();

                using (var archive = ZipFile.Open(reportDir + zipFileName, ZipArchiveMode.Create))
                {
                    foreach (DataRow item in dt_payslip.Rows)
                    {
                        var payslipOBJ = GeneratePayslip(
                            item["empID"].ToString(),
                            "{" + item["Id"].ToString() + "}",
                            payout_datetime,
                            model.Code.ToUpper(),
                            item["LastName"].ToString(),
                            csm
                        );

                        if (payslipOBJ != null)
                        {
                            archive.CreateEntryFromFile(payslipOBJ.payslip_link, Path.GetFileName(payslipOBJ.payslip_link));
                            payslipObjectList.Add(payslipOBJ);
                        }
                    }
                }

                // Insert payslip object json values
                DataTable payslip_object = new DataTable();
                payslip_object = ToDataTable(payslipObjectList.ToList());

                oConn.Open();
                oCmd.CommandText = "sp_CrawfordInsert_PayslipObject";
                da.SelectCommand.CommandType = CommandType.StoredProcedure;

                oCmd.Parameters.Clear();
                oCmd.Parameters.AddWithValue("@tenant_code", model.Code);
                oCmd.Parameters.AddWithValue("@payout_date", model.PayoutDate);
                oCmd.Parameters.AddWithValue("@payslip_obj", payslip_object);
                int resp = (Int32)oCmd.ExecuteScalar();

                // Return file path
                response = liveDir + zipFileName;
                //response = reportDir + zipFileName;
                oConn.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine("Error: " + e.Message);
                response = "Error in generating payslips...";
            }
            finally
            {
                oConn.Close();
            }

            return response;
        }

        public string GenerateJSONPayslips(PayslipModelPA model)
        {
            string response = "";
            SqlConnection oConn = new SqlConnection(GetConnectionString(model.Code).DB_Master);
            SqlTransaction oTrans;
            oConn.Open();
            oTrans = oConn.BeginTransaction();
            SqlCommand oCmd = new SqlCommand();
            oCmd.Connection = oConn;
            oCmd.Transaction = oTrans;

            try
            {
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = oCmd;

                // Fetch Inserted Values
                DataTable dt_payslip_json = new DataTable();

                oCmd.CommandText = "sp_CrawfordFetch_PayslipObject";
                da.SelectCommand.CommandType = CommandType.StoredProcedure;

                oCmd.Parameters.Clear();
                oCmd.Parameters.AddWithValue("@tenant_code", model.Code);
                oCmd.Parameters.AddWithValue("@payout_date", model.PayoutDate);
                da.Fill(dt_payslip_json);

                List<payslip_json> payslipJsonList = new List<payslip_json>();
                foreach (DataRow row in dt_payslip_json.Rows)
                {
                    var values = row.ItemArray;
                    payslip_json payslipJsonValue = new payslip_json()
                    {
                        EmployeeNumber  = values[0].ToString(),
                        PeriodStartDate = values[1].ToString(),
                        PeriodEndDate   = values[2].ToString(),
                        ActualFileName  = values[3].ToString(),
                        NetPay          = values[4].ToString()
                    };

                    payslipJsonList.Add(payslipJsonValue);
                }

                string json = JsonConvert.SerializeObject(payslipJsonList, Formatting.Indented);

                // Declare directories
                var webRootPath = HttpContext.Current.Server.MapPath("~");
                string customDir = $"GeneratedPayslip/JSON/{model.Code.ToUpper()}/";
                string reportDir = Path.Combine(webRootPath, customDir);
                string liveDir = Path.Combine(LIVE_WEB_ROOT_PATH, customDir);
                string jsonFileName = $"{model.Code.ToUpper()} Payslips - {model.PayoutDate}.json";

                if (!Directory.Exists(reportDir))
                    Directory.CreateDirectory(reportDir);

                if (File.Exists(reportDir + jsonFileName))
                    File.Delete(reportDir + jsonFileName);

                File.WriteAllText(reportDir + jsonFileName, json);

                // Return file path
                response = liveDir + jsonFileName;
                //response = reportDir + jsonFileName;
                oConn.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine("Error: " + e.Message);
                response = "Error in generating json file...";
            }
            finally
            {
                oConn.Close();
            }

            return response;
        }

        private payslip_object GeneratePayslip(string empID, string userID, string payoutDate, string _tenant_name, string lastname, ConnectionStringModel _connectionStringModel)
        {
            try
            {
                payslip_object _gpo = new payslip_object();

                #region Crystal Reports Generation
                ReportDocument reportDocument = new ReportDocument();
                ConnectionInfo conRpt = new ConnectionInfo();
                string path = "";
                
                // Get rpt template
                path = HttpContext.Current.Server.MapPath("~/Payslip.rpt");

                // Set Database
                conRpt.DatabaseName = _connectionStringModel.catalog;
                conRpt.ServerName = _connectionStringModel.instance_name;
                conRpt.UserID = _connectionStringModel.user_name;
                conRpt.Password = _connectionStringModel.user_hash;

                reportDocument.Load(path);
                reportDocument.SetParameterValue("@empID", userID);
                reportDocument.SetParameterValue("@payoutDate", payoutDate);
                reportDocument.SetDatabaseLogon(_connectionStringModel.user_name, _connectionStringModel.user_hash);

                Tables rptTables = reportDocument.Database.Tables;
                for (int i = 0; i < rptTables.Count; i++)
                {
                    Table rptTable = rptTables[i];
                    TableLogOnInfo tblInfo = rptTable.LogOnInfo;
                    tblInfo.ConnectionInfo = conRpt;
                    // next table
                }
                // if the report contains sub reports, you will need to set the connection info there too
                if (reportDocument.Subreports.Count > 0)
                {
                    for (int i = 0; i < reportDocument.Subreports.Count; i++)
                    {
                        using (ReportDocument rptSub = reportDocument.OpenSubreport(reportDocument.Subreports[i].Name))
                        {
                            Tables rptTables2 = rptSub.Database.Tables;
                            for (int ix = 0; ix < rptTables2.Count; ix++)
                            {
                                Table rptTable = rptTables2[ix];
                                TableLogOnInfo tblInfo = rptTable.LogOnInfo;
                                tblInfo.ConnectionInfo = conRpt;
                                // next table
                            }
                            rptSub.Close();
                        }
                    }
                }

                Directory.CreateDirectory(HttpContext.Current.Server.MapPath("~/GeneratedPayslip/" + _tenant_name));
                string fileNameIden = userID.Trim(new char[] { '{', '}' });
                string[] filename2 = payoutDate.Split(' ');
                string fileName = empID + "_" + lastname + "_" + filename2[0].Trim('-') + ".pdf";
                string filePathLink = HttpContext.Current.Server.MapPath("~/GeneratedPayslip/" + _tenant_name + "/" + empID + "_" + lastname + "_" + filename2[0].Trim('-') + ".pdf");
                reportDocument.ExportToDisk(ExportFormatType.PortableDocFormat, filePathLink);
                reportDocument.Dispose();
                #endregion

                _gpo.employee_number = empID;
                _gpo.payslip_link = filePathLink;
                _gpo.payslip_name = fileName;

                return _gpo;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        #endregion
    }
}