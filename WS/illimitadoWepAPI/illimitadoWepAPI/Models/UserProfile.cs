﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace illimitadoWepAPI.Models
{
    public class UserProfile
    {
        public string FIRSTNAME { get; set; }
        public string LASTNAME { get; set; }
        public string MIDDLENAME { get; set; }
        public string SUFFIX { get; set; }
        public string CARDNAME { get; set; }
        public string BIRTHDAY { get; set; }
        public string PLACEOFBIRTH { get; set; }
        public string NATIONALITY { get; set; }
        public string NATCOUNTRY { get; set; }
        public string GENDER { get; set; }
        public string MOBILE { get; set; }
        public string TELPHONE { get; set; }
        public string PRESHOUSENUMBER { get; set; }
        public string PRESHOUSEADDRESS { get; set; }
        public string PRESBRGY { get; set; }
        public string PRESMUNI { get; set; }
        public string PRESCOUNTRY { get; set; }
        public string PRESREGION { get; set; }
        public string PRESPROVINCE { get; set; }
        public string PRESZIPCODE { get; set; }
        public string PRESCITY { get; set; }
        public string PERMHOUSENUMBER { get; set; }
        public string PERMHOUSEADDRESS { get; set; }
        public string PERMBRGY { get; set; }
        public string PERMUNI { get; set; }
        public string PERMCOUNTRY { get; set; }
        public string PERMREGION { get; set; }
        public string PERMPROVINCE { get; set; }
        public string PERMZIPCODE { get; set; }
        public string PERMCITY { get; set; }
        public string EMAILADD { get; set; }
        public string EMPLOYERNAME { get; set; }
        public string CIVILSTATUS { get; set; }
        public string MOTHFIRSTNAME { get; set; }
        public string MOTHLASTNAME { get; set; }
        public string MOTHMIDDLENAME { get; set; }
        public string MOTHSUFFIX { get; set; }
        public string NUMBEROFPADS { get; set; }
        public string OTHERDETAILS { get; set; }
        public string FILENAME { get; set; }
        public string NTID { get; set; }
        public string TIN { get; set; }
        public string SCORE { get; set; }
        public string ProfileID { get; set; }
        public string Status { get; set; }
        public String LoginID { get; set; }
        public String Password { get; set; }
        public String FaceID { get; set; }
    }


    public class UserProfile_Images
    {
        public string Signature1 { get; set; }
        public string Signature2 { get; set; }
        public string Signature3 { get; set; }
        public string Card1 { get; set; }
        public string Card2 { get; set; }
        public string EmpID { get; set; }
        public string ImageType { get; set; }

    }

    public class OR_Reimbursement
    {
        public string EmpID { get; set; }
        public string BenefitType { get; set; }
        public string ORDateTIme { get; set; }
        public string ORAmount { get; set; }
        public string VendorName { get; set; }
        public string VendorTin { get; set; }
        public string Base { get; set; }

    }


    public class UpdateEmployeeConfiInfo
    {
        public string EmpID { get; set; }
        public string TaxStatus { get; set; }
        public string SSS { get; set; }
        public string TIN { get; set; }
        public string PhilHealth { get; set; }
        public string Pagibig { get; set; }
        public string DriversLicense { get; set; }
        public string Passport { get; set; }
        public string Unified { get; set; }
        public string Postal { get; set; }
        public string BirthCertificate { get; set; }
        public string SssImage { get; set; }
        public string TinImage { get; set; }
        public string PhilImage { get; set; }
        public string PagImage { get; set; }
        public string DriverImage { get; set; }
        public string PassportImage { get; set; }
        public string UmidImage { get; set; }
        public string PostalImage { get; set; }
        public string BirthImage { get; set; }

    }



    public class UpdateEmployeeInfo
    {
        public string EmpID { get; set; }
        public string First_Name { get; set; }
        public string Middle_Name { get; set; }
        public string Last_Name { get; set; }
        public string Ntlogin { get; set; }
        public string Alias { get; set; }
        public string Deptcode { get; set; }
        public string Deptname { get; set; }
        public string BUnit { get; set; }
        public string BUnithead { get; set; }
        public string Workstation { get; set; }
        public string Supervisor { get; set; }
        public string DTransFrom { get; set; }
        public string DTransTo { get; set; }
        public string LocFrom { get; set; }
        public string LocTo { get; set; }
        public string CCChangeFrom { get; set; }
        public string CCChangeTo { get; set; }
        public string Email { get; set; }
        public string WorkEmail { get; set; }
        public string Extension { get; set; }
        public string JoinDate { get; set; }
        public string ProdStart { get; set; }
        public string Tenure { get; set; }
        public string TenureMnth { get; set; }
        public string Batch { get; set; }
        public string Class { get; set; }
        public string Skill { get; set; }
        public string JobDesc { get; set; }
        public string Promotion { get; set; }
        public string Status { get; set; }
        public string Emp_Name { get; set; }
        public string approved { get; set; }
        public string Employee_Level { get; set; }
        public string Employee_Category { get; set; }
        public string BusinessSegment { get; set; }
        public string IsApprove { get; set; }
        public string ResignedDate { get; set; }

    }


    public class UpdateEmployeePersonalInfo
    {
        public string EmpID { get; set; }
        public string HouseNumber { get; set; }
        public string StreetName { get; set; }
        public string Barangay { get; set; }
        public string Town { get; set; }
        public string City { get; set; }
        public string Region { get; set; }
        public string ZipCode { get; set; }
        public string BloodType { get; set; }
        public string Gender { get; set; }
        public string DOB { get; set; }
        public string Citizenship { get; set; }
        public string NameofOrganization { get; set; }
        public string MobileAreaCode { get; set; }
        public string MobileNumber { get; set; }
        public string HomeAreaCode { get; set; }
        public string HomeNumber { get; set; }
        public string PersonalEmail { get; set; }
        public string EmrgName { get; set; }
        public string EmrgNumber { get; set; }
        public string EmrgRelationship { get; set; }
        public string OptionalEmail { get; set; }
        public string ContactNo { get; set; }
        public string Province { get; set; }
        public string EmrgFname { get; set; }
        public string EmrgMname { get; set; }
        public string EmrgLname { get; set; }


    }
    public class Master_User_Info_Personal_Confi_Info
    {
        public string SeriesID { get; set; }
        public string EmpID { get; set; }
        public string First_Name { get; set; }
        public string Middle_Name { get; set; }
        public string Last_Name { get; set; }
        public string NTID { get; set; }
        public string EmpName { get; set; }
        public string EmpType { get; set; }
        public string EmpLevel { get; set; }
        public string EmpStatus { get; set; }
        public string UserType { get; set; }
        public string Gender { get; set; }
        public string DateJoined { get; set; }
        public string ContractEndDate { get; set; }
        public string LeaveBal { get; set; }
        public string LeaveBalCTO { get; set; }
        public string Country { get; set; }
        public string JobDesc { get; set; }
        public string DeptCode { get; set; }
        public string DeptName { get; set; }
        public string MngrID { get; set; }
        public string MngrName { get; set; }
        public string MngrNTID { get; set; }
        public string BusinessUnit { get; set; }
        public string BusinessUnitHead { get; set; }
        public string BusinessSegment { get; set; }
        public string AccessLevel { get; set; }
        public string DOB { get; set; }
        public string Alias { get; set; }
        public string ExtensionNumber { get; set; }
        public string Skill { get; set; }
        public string Batch { get; set; }
        public string Class { get; set; }
        public string Workstation { get; set; }
        public string ProductionStartDate { get; set; }
        public string ModifiedBy { get; set; }
        public string DateModified { get; set; }
        public string EmpPayHrsCat { get; set; }
        public string Salary { get; set; }
        public string DailyRate { get; set; }
        public string EcolaWageOrder { get; set; }
        public string EcolaRate { get; set; }
        public string Transportation { get; set; }
        public string Rice { get; set; }
        public string Communication { get; set; }
        public string EcolaRegion { get; set; }
        public string Branch { get; set; }
        public string Wallet { get; set; }
        public string FaceID { get; set; }
        public string DTransFrom { get; set; }
        public string DTransTo { get; set; }
        public string LocFrom { get; set; }
        public string LocTo { get; set; }
        public string CCChangeFrom { get; set; }
        public string CCChangeTo { get; set; }
        public string Email { get; set; }
        public string WorkEmail { get; set; }
        public string Extension { get; set; }
        public string JoinDate { get; set; }
        public string ProdStart { get; set; }
        public string Tenure { get; set; }
        public string TenureMnth { get; set; }
        public string Batch1 { get; set; }
        public string Class1 { get; set; }
        public string Skill1 { get; set; }
        public string JobDesc1 { get; set; }
        public string Promotion { get; set; }
        public string Status { get; set; }
        public string Emp_Name { get; set; }
        public string approved { get; set; }
        public string Employee_Level { get; set; }
        public string Employee_Category { get; set; }
        public string BusinessSegment1 { get; set; }
        public string IsApprove { get; set; }
        public string ResignedDate { get; set; }
        public string HouseNumber { get; set; }
        public string StreetName { get; set; }
        public string Barangay { get; set; }
        public string Town { get; set; }
        public string City { get; set; }
        public string Region { get; set; }
        public string ZipCode { get; set; }
        public string BloodType { get; set; }
        public string Gender1 { get; set; }
        public string DOB1 { get; set; }
        public string Citizenship { get; set; }
        public string NameofOrganization { get; set; }
        public string MobileAreaCode { get; set; }
        public string MobileNumber { get; set; }
        public string HomeAreaCode { get; set; }
        public string HomeNumber { get; set; }
        public string PersonalEmail { get; set; }
        public string EmrgName { get; set; }
        public string EmrgNumber { get; set; }
        public string EmrgRelationship { get; set; }
        public string OptionalEmail { get; set; }
        public string ContactNo { get; set; }
        public string Province { get; set; }
        public string TaxStatus { get; set; }
        public string SSS { get; set; }
        public string TIN { get; set; }
        public string PhilHealth { get; set; }
        public string Pagibig { get; set; }
        public string DriversLicense { get; set; }
        public string Passport { get; set; }
        public string Unified { get; set; }
        public string Postal { get; set; }
        public string BirthCertificate { get; set; }

    }





    public class EmployerHistory
    {
        public string Company_Name { get; set; }
        public string Work_From { get; set; }
        public string Work_To { get; set; }
    }

    public class EmployerList
    {
        public List<EmployerHistory> EmployerListing;
        public string myreturn;
    }


    public class DependentList
    {
        public List<DependentHistory> DependentListing;
        public string myreturn;
    }

    public class DependentHistory
    {
        public string FullName { get; set; }
        public string DOB { get; set; }
        public string Relationship { get; set; }
        public string EmpID { get; set; }
        public string First { get; set; }
        public string Mid { get; set; }
        public string Lastname { get; set; }
        public string Extension { get; set; }
        public string Birth { get; set; }
        public string Comments { get; set; }
        public string DocFilename { get; set; }


    }
}