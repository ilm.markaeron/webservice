﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace illimitadoWepAPI.Models
{
    public class Login
    {
        public String LoginID { get; set; }
        public String Password { get; set; }
        public String FaceID { get; set; }
    }


    public class TempTbl
    {
        public String EmpID { get; set; }
        public String Fname { get; set; }
        public String Lname { get; set; }
        public String MI { get; set; }
        public String BDay { get; set; }
        public String Email { get; set; }
        public String Type { get; set; }
        public String Action { get; set; }

    }



}