﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace illimitadoWepAPI.Models
{
    public class LeaveMngt
    {
        public string LeaveDate { get; set; }
        public string LeaveStatus { get; set; }
        public string LeaveType { get; set; }
        public string isHalfDay { get; set; }
        public string isPaid { get; set; }
        public string NTID { get; set; }
        public string Coordinates { get; set; }
        public string LeaveID { get; set; }
        public string EmployeeName { get; set; }

    }



    public class GetLeaveResult
    {
        public List<LeaveMngt> Leave;
        public string myreturn;
    }

    public class AvailableLeave
    {
        public string Ltype { get; set; }
        public string SerialID { get; set; }
        //public string LastLeave { get; set; }


    }
    public class GetAvailableLeaves
    {
        public List<AvailableLeave> AvailableLeaves;
        public string myreturn;
    }

    public class LeaveParameters
    {
        public string NTID { get; set; }
        public string Start_Date { get; set; }
        public string End_Date { get; set; }

    }

}