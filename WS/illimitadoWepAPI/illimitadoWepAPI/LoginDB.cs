﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using illimitadoWepAPI.MyClass;
using illimitadoWepAPI.Models;

namespace illimitadoWepAPI
{
    public class LoginDB
    {

        public string  saveLogin(Login userToSave)
        {
            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@UserID", mytype = SqlDbType.NVarChar, Value = userToSave.LoginID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@LoginFrom", mytype = SqlDbType.NVarChar, Value = userToSave.Password });

           // DataTable EmployeeDt = new DataTable();
            String ID = Connection.ExecuteScalar("spINSERT_LoginLogs");

            return ID;

        }


        public Login searchLogin(string UserID)
        {

            Login L = new Login();
            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@empId", mytype = SqlDbType.NVarChar, Value = UserID.ToString() });
            DataRow dr;

            dr = Connection.GetSingleRow("sp_LoginMobile");


            L.LoginID = dr["empId"].ToString();
            L.Password = dr["Upassword"].ToString();
            
            return L;

        }


        public UserProfile validateLogin(Login userToSave)
        {
            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@UserID", mytype = SqlDbType.NVarChar, Value = userToSave.LoginID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@LoginFrom", mytype = SqlDbType.NVarChar, Value = userToSave.Password });
            DataRow dr;

            dr = Connection.GetSingleRow("spLogin_Mobile");

            UserProfile userprofile = new UserProfile
            {
                LoginID = dr["username"].ToString(),
                Password = dr["userpassword"].ToString(),
                FIRSTNAME = dr["First_Name"].ToString(),
                LASTNAME = dr["Last_Name"].ToString(),
                MIDDLENAME = dr["Middle_Name"].ToString()
            };
            return userprofile;

        }


    }


}