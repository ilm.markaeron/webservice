﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using illimitadoWepAPI.Models;
using System.Xml.Serialization;
using System.IO;
using System.Data;
using System.Data.SqlClient;
using illimitadoWepAPI.MyClass;


namespace illimitadoWepAPI.Controllers
{
    public class LoginController : ApiController
    {
        /// <summary>
        /// Get All sample info
        /// </summary>
        /// <returns></returns>
        // GET: api/Login
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }
        /// <summary>
        /// Sample Again.....
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        // GET: api/Login/5


        [HttpPost]
        [Route("Login")]
        public Login Post(string id)
        {

   
            LoginDB logDB = new LoginDB();
            Login User = logDB.searchLogin(id);

            //XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
            //ns.Add("", "");
            //XmlSerializer s = new XmlSerializer(typeof(Login));
            //StringWriter myWriter = new StringWriter();
            //s.Serialize(myWriter, User, ns);

            return User;
        }









        // POST: api/Login
        [HttpPost]
        [Route("LoginNow")]
        public void Post([FromBody]Login value)
        {

            LoginDB logDB = new LoginDB();
            string id;
            id = logDB.saveLogin(value);
 


        }

        // PUT: api/Login/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Login/5
        public void Delete(int id)
        {

        }


        //public HttpPostAttribute ValidateUser([FromBody]Login value)
        //{



        //    LoginDB logDB = new LoginDB();
        //    UserProfile User = logDB.validateLogin(value);

        //    return User;




        //}


        /// <summary>
        ///    {
        ///    "LoginID": "i0078",
        ///    "Password": "password"
        ///        }
        /// </summary>
        /// <param name="userToSave"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("validateLogin")]
        public UserProfile validatelogin(Login userToSave)
        {
            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@empId", mytype = SqlDbType.NVarChar, Value = userToSave.LoginID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@password", mytype = SqlDbType.NVarChar, Value = userToSave.Password });
            DataRow dr;

            dr = Connection.GetSingleRow("spLogin_Mobile");

            UserProfile userprofile = new UserProfile
            {
                LoginID = dr["username"].ToString(),
                Password = dr["userpassword"].ToString(),
                FIRSTNAME = dr["First_Name"].ToString(),
                LASTNAME = dr["Last_Name"].ToString(),
                MIDDLENAME = dr["Middle_Name"].ToString(),
                NTID = dr["username"].ToString(),
                FaceID = dr["faceID"].ToString()

            };
            return userprofile;

        }




        [HttpPost]
        [Route("validateLoginURL")]
        public UserProfile validateloginURL([FromUri]Login userToSave)
        {
            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@empId", mytype = SqlDbType.NVarChar, Value = userToSave.LoginID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@password", mytype = SqlDbType.NVarChar, Value = userToSave.Password });
            DataRow dr;

            dr = Connection.GetSingleRow("spLogin_Mobile");

            UserProfile userprofile = new UserProfile
            {
                LoginID = dr["username"].ToString(),
                Password = dr["userpassword"].ToString(),
                FIRSTNAME = dr["First_Name"].ToString(),
                LASTNAME = dr["Last_Name"].ToString(),
                MIDDLENAME = dr["Middle_Name"].ToString(),
                NTID = dr["username"].ToString(),
                FaceID = dr["faceID"].ToString()
            };
            return userprofile;

        }

        [HttpPost]
        [Route("validateFaceLogin")]
        public UserProfile validateFacelogin(Login userToSave)
        {
            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@FaceId", mytype = SqlDbType.NVarChar, Value = userToSave.FaceID });
          
            DataRow dr;

            dr = Connection.GetSingleRow("spFacialLogin_Mobile");

            UserProfile userprofile = new UserProfile
            {
                LoginID = dr["username"].ToString(),
                Password = dr["userpassword"].ToString(),
                FIRSTNAME = dr["First_Name"].ToString(),
                LASTNAME = dr["Last_Name"].ToString(),
                MIDDLENAME = dr["Middle_Name"].ToString(),
                NTID = dr["username"].ToString(),
                FaceID = dr["FaceID"].ToString()
            };
            return userprofile;

        }




        [HttpPost]
        [Route("InsertOnTempTbl")]
        public string UserBreak(TempTbl TempTBLDetals)
        {
            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = TempTBLDetals.EmpID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@FN", mytype = SqlDbType.NVarChar, Value = TempTBLDetals.Lname });
            Connection.myparameters.Add(new myParameters { ParameterName = "@MN", mytype = SqlDbType.NVarChar, Value = TempTBLDetals.Fname });
            Connection.myparameters.Add(new myParameters { ParameterName = "@LN", mytype = SqlDbType.NVarChar, Value = TempTBLDetals.BDay });
            Connection.myparameters.Add(new myParameters { ParameterName = "@BDDAY", mytype = SqlDbType.NVarChar, Value = TempTBLDetals.Email });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Email", mytype = SqlDbType.NVarChar, Value = TempTBLDetals.Type });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Action", mytype = SqlDbType.NVarChar, Value = TempTBLDetals.Action });

            // DataTable EmployeeDt = new DataTable();
            String ID = Connection.ExecuteScalar("sp_EmployeeCRUD");

            return ID;

        }






    }
}
