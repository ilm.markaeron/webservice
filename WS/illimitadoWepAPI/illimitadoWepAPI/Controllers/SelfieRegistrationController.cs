﻿using illimitadoWepAPI.Models;
using illimitadoWepAPI.MyClass;
using Microsoft.ProjectOxford.Face;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Hosting;
using System.Web.Http;

namespace illimitadoWepAPI.Controllers
{
    public class SelfieRegistrationController : ApiController
    {
        [HttpPost]
        [Route("UploadProfile")]
        public async Task<HttpResponseMessage> UploadProfile()
        {
            HttpRequest httpRequest = HttpContext.Current.Request;

            BinaryReader binaryReader = new BinaryReader(httpRequest.Files[0].InputStream);

            string NTID = httpRequest.Form["NTID"];
            //string Name = httpRequest.Form["Name"];
            byte[] Photo = binaryReader.ReadBytes(httpRequest.Files[0].ContentLength);
            string Set = httpRequest.Form["Set"];
            
            HttpResponseMessage msg;

            var path = HttpContext.Current.Server.MapPath("~/ProfilePhotos/");
            //var path = "http://uat-vm.southeastasia.cloudapp.azure.com:81/hrms_test/ProfilePhotos";
            var folder = Path.Combine(path, NTID);
            Directory.CreateDirectory(folder);

            MemoryStream stream = new MemoryStream();

            try
            {
                using (stream)
                {
                    File.WriteAllBytes(folder + "/" + NTID + "_pic" + Set + ".png", Photo);
                }

                msg = Request.CreateResponse(HttpStatusCode.Accepted);
            }
            catch
            {
                msg = Request.CreateResponse(HttpStatusCode.NotAcceptable);
            }

            return msg;
        }

        [HttpPost]
        [Route("RegisterFace")]
        public async Task<string> RegisterFace(SelfieRegistration selfieReg)
        {
            string name = selfieReg.Name;
            string ntid = selfieReg.NTID;

            try
            {
                FaceServiceClient faceServiceClient = new FaceServiceClient("2d623d35f32646168461b7345db08ad8", "https://westcentralus.api.cognitive.microsoft.com/face/v1.0");
                var person = await faceServiceClient.CreatePersonAsync("blulemonsdevs", name);

                Guid persondID = person.PersonId;

                var path = HttpContext.Current.Server.MapPath("~/ProfilePhotos/");
                //var path = "http://uat-vm.southeastasia.cloudapp.azure.com:81/hrms_test/ProfilePhotos";
                var folder = Path.Combine(path, ntid);

                Stream stream1 = File.OpenRead(folder + "/" + ntid + "_pic1.png");
                Stream stream2 = File.OpenRead(folder + "/" + ntid + "_pic2.png");

                await faceServiceClient.AddPersonFaceAsync("blulemonsdevs", persondID, stream1);
                await faceServiceClient.AddPersonFaceAsync("blulemonsdevs", persondID, stream2);

                await faceServiceClient.TrainPersonGroupAsync("blulemonsdevs");

                string guidStr = persondID.ToString("D");

                return guidStr;
            }
            catch
            {
                return "null";
            }
        }
    }
}