﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using illimitadoWepAPI.Models;
using System.Xml.Serialization;
using System.IO;
using System.Data;
using System.Data.SqlClient;
using illimitadoWepAPI.MyClass;
using static illimitadoWepAPI.Models.TimeKeeping;

namespace illimitadoWepAPI.Controllers
{
    public class TimeKeepingController : ApiController
    {
        // GET: api/TimeKeeping
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/TimeKeeping/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/TimeKeeping
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/TimeKeeping/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/TimeKeeping/5
        public void Delete(int id)
        {
        }


        [HttpPost]
        [Route("timein")]
        public string TimeIN(Login userToTimeIN)
        {
            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.NVarChar, Value = userToTimeIN.LoginID });


            // DataTable EmployeeDt = new DataTable();
            String ID = Connection.ExecuteScalar("sp_TimeIn_Mobile");

            return ID;

        }
        /// <summary>
        /// {
        /// "NTID": "i00143"
        /// }
        /// </summary>
        /// <param name="DailyParameters"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("TimeINDisplay")]
        public GetDailyResult ShowDailyRecord(DailyParameters DailyParameters)
        {
            GetDailyResult GetDailyResult = new GetDailyResult();
            //try
            //{
            Connection Connection = new Connection();
            List<Daily> DailyList = new List<Daily>();
            DataTable dtDAILY = new DataTable();
            //BasicFunction BasicFunction = new BasicFunction();
            Connection.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.NVarChar, Value = DailyParameters.NTID });
            //    Connection.myparameters.Add(new myParameters { ParameterName = "@STARTDATE", mytype = SqlDbType.DateTime, Value = Convert.ToDateTime(DailyParameters.Start_Date) });
            //    Connection.myparameters.Add(new myParameters { ParameterName = "@ENDDATE", mytype = SqlDbType.DateTime, Value = BasicFunction.getenddate(DailyParameters.End_Date) });
            dtDAILY = Connection.GetDataTable("sp_TimeIn_Mobile");
            //CultureInfo provider = CultureInfo.InvariantCulture;
            if (dtDAILY.Rows.Count > 0)
            {
                foreach (DataRow row in dtDAILY.Rows)
                {
                    Daily Daily = new Daily
                    {
                        SchedDate = Convert.ToDateTime(row["SchedDate"]).ToString("MMM-dd-yyyy"),
                        //SchedDay = Convert.ToDateTime(row["SchedDate"]).DayOfWeek.ToString().Substring(0, 3),
                        SchedIN = row["Start_Time"].ToString(),
                        SchedOUT = row["End_Time"].ToString(),
                        //TotalTimeHour = BasicFunction.Decimal102(row["TotalTimeHour"].ToString()),
                        SchedType = row["ReasonDesc"].ToString()
                        //DateFormat = Convert.ToDateTime(row["SchedDate"]).ToString()
                    };
                    DailyList.Add(Daily);
                }
                GetDailyResult.Daily = DailyList;
                GetDailyResult.myreturn = "Success";
            }
            else
            {
                GetDailyResult.myreturn = "No Data Available";
            }
            //}
            //catch
            //{
            //    GetDailyResult.myreturn = "Error Fetching Data";

            //}
            return GetDailyResult;
        }


        /// <summary>
        /// {
        ///"NTID": "EMP0003",
        ///"Start_Date": "May-05-2017"
        ///}
        /// </summary>
        /// <param name="DailyParameters"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("TimeOutDisplay")]
        public GetDailyResult TimeOutAndDisplay(DailyParameters DailyParameters)
        {
            GetDailyResult GetDailyResult = new GetDailyResult();
            //try
            //{
            Connection Connection = new Connection();
            List<Daily> DailyList = new List<Daily>();
            DataTable dtDAILY = new DataTable();
            //BasicFunction BasicFunction = new BasicFunction();
            Connection.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.NVarChar, Value = DailyParameters.NTID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@LastSched", mytype = SqlDbType.DateTime, Value = Convert.ToDateTime(DailyParameters.Start_Date) });
            //    Connection.myparameters.Add(new myParameters { ParameterName = "@ENDDATE", mytype = SqlDbType.DateTime, Value = BasicFunction.getenddate(DailyParameters.End_Date) });
            dtDAILY = Connection.GetDataTable("sp_TimeOut_Mobile");
            //CultureInfo provider = CultureInfo.InvariantCulture;
            if (dtDAILY.Rows.Count > 0)
            {
                foreach (DataRow row in dtDAILY.Rows)
                {
                    Daily Daily = new Daily
                    {
                        SchedDate = Convert.ToDateTime(row["SchedDate"]).ToString("MMM-dd-yyyy"),
                        //SchedDay = Convert.ToDateTime(row["SchedDate"]).DayOfWeek.ToString().Substring(0, 3),
                        SchedIN = row["Start_Time"].ToString(),
                        SchedOUT = row["End_Time"].ToString(),
                        //TotalTimeHour = BasicFunction.Decimal102(row["TotalTimeHour"].ToString()),
                        SchedType = row["ReasonDesc"].ToString()
                        //DateFormat = Convert.ToDateTime(row["SchedDate"]).ToString()
                    };
                    DailyList.Add(Daily);
                }
                GetDailyResult.Daily = DailyList;
                GetDailyResult.myreturn = "Success";
            }
            else
            {
                GetDailyResult.myreturn = "No Data Available";
            }
            //}
            //catch
            //{
            //    GetDailyResult.myreturn = "Error Fetching Data";

            //}
            return GetDailyResult;
        }

        /// <summary>
        /// {
        ///"NTID": "EMP0003"
        ///}
        /// </summary>
        /// <param name="DailyParameters"></param>
        /// <returns></returns>

        [HttpPost]
        [Route("DisplayCurrentDayActivity")]
        public GetDailyResult DisplayCurrentDayActivity(DailyParameters DailyParameters)
        {
            GetDailyResult GetDailyResult = new GetDailyResult();
            //try
            //{
            Connection Connection = new Connection();
            List<Daily> DailyList = new List<Daily>();
            DataTable dtDAILY = new DataTable();
            //BasicFunction BasicFunction = new BasicFunction();
            Connection.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.NVarChar, Value = DailyParameters.NTID });
            dtDAILY = Connection.GetDataTable("sp_DisplayCurrentDayActivity_Mobile");
            //CultureInfo provider = CultureInfo.InvariantCulture;
            if (dtDAILY.Rows.Count > 0)
            {
                foreach (DataRow row in dtDAILY.Rows)
                {
                    Daily Daily = new Daily
                    {
                        SchedDate = Convert.ToDateTime(row["SchedDate"]).ToString("MMM-dd-yyyy"),
                        //SchedDay = Convert.ToDateTime(row["SchedDate"]).DayOfWeek.ToString().Substring(0, 3),
                        SchedIN = row["Start_Time"].ToString(),
                        SchedOUT = row["End_Time"].ToString(),
                        //TotalTimeHour = BasicFunction.Decimal102(row["TotalTimeHour"].ToString()),
                        SchedType = row["ReasonDesc"].ToString()
                        //DateFormat = Convert.ToDateTime(row["SchedDate"]).ToString()
                    };
                    DailyList.Add(Daily);
                }
                GetDailyResult.Daily = DailyList;
                GetDailyResult.myreturn = "Success";
            }
            else
            {
                GetDailyResult.myreturn = "No Data Available";
            }
            //}
            //catch
            //{
            //    GetDailyResult.myreturn = "Error Fetching Data";

            //}
            return GetDailyResult;
        }

        /// <summary>
        /// {
        ///"NTID": "EMPLOYEETEST",
        ///"ReasonID": "3",
        ///"StartOrEnd": "2"

        /// </summary>
        /// <param name="uBreak"></param>
        /// <returns></returns>

        [HttpPost]
        [Route("UserBreaks")]
        public string UserBreak(ForBreaks uBreak)
        {
            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = uBreak.NTID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@REASONID", mytype = SqlDbType.NVarChar, Value = uBreak.ReasonID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@STARTOREND", mytype = SqlDbType.NVarChar, Value = uBreak.StartOrEnd });

            // DataTable EmployeeDt = new DataTable();
            String ID = Connection.ExecuteScalar("sp_InsertToActivityTrackerBreaks_Mobile");

            return ID;

        }



        //{
        //    "NTID": "EMP127",
        //    "FaceID": "10002"
        //}

        [HttpPost]
        [Route("UpdateFaceID")]
        public string UpdateFaceID(UpadateFaceID FaceIDUpdate)
        {
            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@NTID", mytype = SqlDbType.NVarChar, Value = FaceIDUpdate.NTID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@FaceID", mytype = SqlDbType.NVarChar, Value = FaceIDUpdate.FaceID });
   

            // DataTable EmployeeDt = new DataTable();
            String ID = Connection.ExecuteScalar("sp_UpdateEmployeeMasterFaceID_Mobile");

            return ID;

        }


        [HttpPost]
        [Route("ShowHolidays")]
        public GetHolidayResult GetAllHolidays()
        {
            GetHolidayResult GetHolidayResult = new GetHolidayResult();
            List<Holiday> Holidays = new List<Holiday>();
            try
            {
                Connection Connection = new Connection();
                DataTable dtHolidays = new DataTable();
                dtHolidays = Connection.GetDataTable("sp_ShowHolidays");
                // CultureInfo provider = CultureInfo.InvariantCulture;
                foreach (DataRow row in dtHolidays.Rows)
                {
                    Holiday Holiday = new Holiday
                    {
                        HolidayName = row["HolDesc"].ToString(),
                        Type = row["HolStatus"].ToString(),
                        HolidayDate = Convert.ToDateTime(row["HolDate"]).ToString("MMM-dd-yyyy")
                    };

                    Holidays.Add(Holiday);
                    GetHolidayResult.Holidays = Holidays;
                    GetHolidayResult.myreturn = "Success";
                }
                return GetHolidayResult;
            }
            catch
            {
                GetHolidayResult.myreturn = "Error";
                return GetHolidayResult;
            }


        }


        [HttpPost]
        [Route("DisplayActivityByRange")]
        //{
        //"NTID": "i101",
        //"Start_Date": "2017-04-01",
        //"End_Date": "2017-04-30"
        //}
        public ListAllActivityResult DisplayActivityByRange(DailyParameters DailyParameters)
        {
            ListAllActivityResult GetActivityResult = new ListAllActivityResult();
            //try
            //{
            Connection Connection = new Connection();
            List<ListAllActivityByRange> AllActivity = new List<ListAllActivityByRange>();
            DataTable dtDAILY = new DataTable();
            //BasicFunction BasicFunction = new BasicFunction();
            Connection.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.NVarChar, Value = DailyParameters.NTID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@SchedDate1", mytype = SqlDbType.NVarChar, Value = DailyParameters.Start_Date });
            Connection.myparameters.Add(new myParameters { ParameterName = "@SchedDate2", mytype = SqlDbType.NVarChar, Value = DailyParameters.End_Date});
            dtDAILY = Connection.GetDataTable("sp_GetEmployeeActivity_Mobile");
            //CultureInfo provider = CultureInfo.InvariantCulture;
            if (dtDAILY.Rows.Count > 0)
            {
                foreach (DataRow row in dtDAILY.Rows)
                {
                    ListAllActivityByRange ListAllActivity = new ListAllActivityByRange
                    {
                        EmpID = row["EMPID"].ToString(),
                        StartTIme = row["Start_Time"].ToString(),
                        StartEnd = row["End_Time"].ToString(),
                        ReasonID = row["ReasonID"].ToString(),
                        ReasonDesc = row["ReasonDesc"].ToString(),
                        IsPaid = row["IsPaid"].ToString(),
                        SchedDate = row["SchedDate"].ToString(),
                        ActivityStatus = row["Activity_Status"].ToString()
                    };
                    AllActivity.Add(ListAllActivity);
                }
                GetActivityResult.ListAllActivity = AllActivity;
                GetActivityResult.myreturn = "Success";
            }
            else
            {
                GetActivityResult.myreturn = "No Data Available";
            }

            return GetActivityResult;
        }




        [HttpPost]
        [Route("DisplayLogsForApproval")]
        public GetDailyResult DisplayLogsForApproval(DailyParameters DailyParameters)
        {
            GetDailyResult GetDailyResult = new GetDailyResult();
            //try
            //{
            Connection Connection = new Connection();
            List<Daily> DailyList = new List<Daily>();
            DataTable dtDAILY = new DataTable();
            //BasicFunction BasicFunction = new BasicFunction();
            Connection.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.NVarChar, Value = DailyParameters.NTID });
            dtDAILY = Connection.GetDataTable("sp_DisplayLogsForApproval_Mobile");
            //CultureInfo provider = CultureInfo.InvariantCulture;
            if (dtDAILY.Rows.Count > 0)
            {
                foreach (DataRow row in dtDAILY.Rows)
                {
                    Daily Daily = new Daily
                    {
                        SchedDate = Convert.ToDateTime(row["SchedDate"]).ToString("MMM-dd-yyyy"),
                        EmpID = row["EmpID"].ToString(),
                        Name = row["Name"].ToString(),
                        ForApproval = row["ReasonForApproval"].ToString(),
                        SchedIN = row["Start_Time"].ToString(),
                        SchedOUT = row["End_Time"].ToString(),
                        SeriesID = row["SeriesID"].ToString()
                        

                    };
                    DailyList.Add(Daily);
                }
                GetDailyResult.Daily = DailyList;
                GetDailyResult.myreturn = "Success";
            }
            else
            {
                GetDailyResult.myreturn = "No Data Available";
            }
            //}
            //catch
            //{
            //    GetDailyResult.myreturn = "Error Fetching Data";

            //}
            return GetDailyResult;
        }



        [HttpPost]
        [Route("GetTotalHoursWrk")]
        //{
        //"NTID": "i101",
        //"Start_Date": "2017-04-01",
        //"End_Date": "2017-04-30"
        //}
        public ListAllActivityResult GetTotalHoursWrk(DailyParameters DailyParameters)
        {
            ListAllActivityResult GetActivityResult = new ListAllActivityResult();
            //try
            //{
            Connection Connection = new Connection();
            List<ListAllActivityByRange> AllActivity = new List<ListAllActivityByRange>();
            DataTable dtDAILY = new DataTable();
            //BasicFunction BasicFunction = new BasicFunction();
            Connection.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.NVarChar, Value = DailyParameters.NTID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@StartDate", mytype = SqlDbType.NVarChar, Value = DailyParameters.Start_Date });
            Connection.myparameters.Add(new myParameters { ParameterName = "@EndDate", mytype = SqlDbType.NVarChar, Value = DailyParameters.End_Date });
            dtDAILY = Connection.GetDataTable("sp_GetEmpTotalWorkHrs_Mobile");
            //CultureInfo provider = CultureInfo.InvariantCulture;
            if (dtDAILY.Rows.Count > 0)
            {
                foreach (DataRow row in dtDAILY.Rows)
                {
                    ListAllActivityByRange ListAllActivity = new ListAllActivityByRange
                    {
                        EmpID = row["EMPID"].ToString(),
                        SchedDate = row["SchedDate"].ToString(),
                        HourWork = row["TotalHoursWrk"].ToString()
                    };
                    AllActivity.Add(ListAllActivity);
                }
                GetActivityResult.ListAllActivity = AllActivity;
                GetActivityResult.myreturn = "Success";
            }
            else
            {
                GetActivityResult.myreturn = "No Data Available";
            }

            return GetActivityResult;
        }



        [HttpPost]
        [Route("ApprovedLogs")]
        public string ApprovedLogs(Daily SeriesIDForApproval)
        {
            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@SeriesID", mytype = SqlDbType.NVarChar, Value = SeriesIDForApproval.SeriesID });
         
            // DataTable EmployeeDt = new DataTable();
            String ID = Connection.ExecuteScalar("sp_ApprovedLogs_Mobile");

            return ID;

        }




        [HttpPost]
        [Route("DisplayBreakTypes")]
        public ListAllReasonBreakTypes DisplayDependents(Login userToSave)
        {
            ListAllReasonBreakTypes GetAllBreakTypes = new ListAllReasonBreakTypes();
            try
            {
                Connection Connection = new Connection();
                List<ListAllBreakReasonTypes> ListDependents = new List<ListAllBreakReasonTypes>();
                DataSet ds = new DataSet();
                DataTable dtLEAVES = new DataTable();

                //Connection.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = userToSave.LoginID });

                ds = Connection.GetDataset("sp_ShowAllbreaktyp");
                dtLEAVES = ds.Tables[0];
                if (dtLEAVES.Rows.Count > 0)
                {
                    foreach (DataRow row in dtLEAVES.Rows)
                    {
                        ListAllBreakReasonTypes AvailableLeaves = new ListAllBreakReasonTypes
                        {


                            ReasonID = row["ReasonID"].ToString(),
                            ReasonDesc = row["ReasonDesc"].ToString()


                        };
                        ListDependents.Add(AvailableLeaves);
                    }
                    GetAllBreakTypes.ListAllResonType = ListDependents;
                    GetAllBreakTypes.myreturn = "Success";
                }
                else
                {
                    GetAllBreakTypes.myreturn = "No Data Available";
                }
            }
            catch (Exception ex)
            {
                GetAllBreakTypes.myreturn = ex.Message;
            }
            return GetAllBreakTypes;
        }




















    }
}

