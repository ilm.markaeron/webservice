﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using illimitadoWepAPI.Models;
using System.Xml.Serialization;
using System.IO;
using System.Data;
using System.Data.SqlClient;
using illimitadoWepAPI.MyClass;
using static illimitadoWepAPI.Models.LeaveMngt;
namespace illimitadoWepAPI.Controllers
{
    public class LeaveManagementController : ApiController
    {
        // GET: api/LeaveManagement
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/LeaveManagement/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/LeaveManagement
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/LeaveManagement/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/LeaveManagement/5
        public void Delete(int id)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="FileNewLeave"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("FileLeave")]
        public string FileANewLeave(LeaveMngt FileNewLeave)
        {
            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = FileNewLeave.NTID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@LEAVEDATE", mytype = SqlDbType.NVarChar, Value = FileNewLeave.LeaveDate});
            Connection.myparameters.Add(new myParameters { ParameterName = "@LEAVETYPE", mytype = SqlDbType.NVarChar, Value = FileNewLeave.LeaveType });
            Connection.myparameters.Add(new myParameters { ParameterName = "@LEAVEID", mytype = SqlDbType.NVarChar, Value = FileNewLeave.LeaveID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@ISHALFDAY", mytype = SqlDbType.NVarChar, Value = FileNewLeave.isHalfDay });

            // DataTable EmployeeDt = new DataTable();
            String ID = Connection.ExecuteScalar("sp_InsertNewLeave_Mobile");

            return ID;

        }

        /// <summary>
        /// {
        ///"NTID": "i101"
        ///}
    /// </summary>
    /// <param name="LeaveParameters"></param>
    /// <returns></returns>
        [HttpPost]
        [Route("ShowLeaves")]
        public GetLeaveResult ShowLeaves(LeaveParameters LeaveParameters)
        {
            GetLeaveResult GetLeaveResult = new GetLeaveResult();
            try
            {
                Connection Connection = new Connection();
                List<LeaveMngt> ListLeave = new List<LeaveMngt>();
                DataSet ds = new DataSet();
                DataTable dtLEAVES = new DataTable();
                //BasicFunction BasicFunction = new BasicFunction();
                Connection.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = LeaveParameters.NTID });
                //Connection.myparameters.Add(new myParameters { ParameterName = "@STARTDATE", mytype = SqlDbType.NVarChar, Value = LeaveParameters.Start_Date });
                //Connection.myparameters.Add(new myParameters { ParameterName = "@ENDDATE", mytype = SqlDbType.NVarChar, Value = LeaveParameters.End_Date });
                ds = Connection.GetDataset("sp_ShowLeaves_Mobile");
                dtLEAVES = ds.Tables[0];
                if (dtLEAVES.Rows.Count > 0)
                {
                    foreach (DataRow row in dtLEAVES.Rows)
                    {
                        LeaveMngt Leave = new LeaveMngt
                        {
                            LeaveDate = Convert.ToDateTime(row["LeaveDate"]).ToString("MMM-dd-yyyy"),
                            isHalfDay = row["isHalfDay"].ToString(),
                            isPaid = row["isPaid"].ToString(),
                            LeaveStatus = row["LeaveStatus"].ToString(),
                            LeaveType = row["LeaveType"].ToString()
                        };
                        ListLeave.Add(Leave);
                    }
                    GetLeaveResult.Leave = ListLeave;
                    GetLeaveResult.myreturn = "Success";
                }
                else
                {
                    GetLeaveResult.myreturn = "No Data Available";
                }
            }
            catch (Exception ex)
            {
                GetLeaveResult.myreturn = ex.Message;
            }
            return GetLeaveResult;
        }


        [HttpPost]
        [Route("ShowLeavesForApproval")]
        public GetLeaveResult ShowLeavesForApproval(LeaveParameters LeaveParameters)
        {
            GetLeaveResult GetLeaveResult = new GetLeaveResult();
            try
            {
                Connection Connection = new Connection();
                List<LeaveMngt> ListLeave = new List<LeaveMngt>();
                DataSet ds = new DataSet();
                DataTable dtLEAVES = new DataTable();
                //BasicFunction BasicFunction = new BasicFunction();
                Connection.myparameters.Add(new myParameters { ParameterName = "@MNGRID", mytype = SqlDbType.NVarChar, Value = LeaveParameters.NTID });
                //Connection.myparameters.Add(new myParameters { ParameterName = "@STARTDATE", mytype = SqlDbType.NVarChar, Value = LeaveParameters.Start_Date });
                //Connection.myparameters.Add(new myParameters { ParameterName = "@ENDDATE", mytype = SqlDbType.NVarChar, Value = LeaveParameters.End_Date });
                ds = Connection.GetDataset("sp_SearchLeaveForApproval_Mobile");
                dtLEAVES = ds.Tables[0];
                if (dtLEAVES.Rows.Count > 0)
                {
                    foreach (DataRow row in dtLEAVES.Rows)
                    {
                        LeaveMngt Leave = new LeaveMngt
                        {
                            LeaveID = row["ID"].ToString(),
                            EmployeeName = row["EmpName"].ToString(),
                            LeaveType = row["LeaveType"].ToString(),
                            LeaveDate = Convert.ToDateTime(row["LeaveDate"]).ToString("MMM-dd-yyyy"),
                            LeaveStatus = row["LeaveStatus"].ToString()
                            
                        };
                        ListLeave.Add(Leave);
                    }
                    GetLeaveResult.Leave = ListLeave;
                    GetLeaveResult.myreturn = "Success";
                }
                else
                {
                    GetLeaveResult.myreturn = "No Data Available";
                }
            }
            catch (Exception ex)
            {
                GetLeaveResult.myreturn = ex.Message;
            }
            return GetLeaveResult;
        }

        [HttpPost]
        [Route("ApprovedLeaves")]
        public string ApprovedLeaves(LeaveMngt LeaveForApproval)
        {
            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@LEAVEID", mytype = SqlDbType.NVarChar, Value = LeaveForApproval.LeaveID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@LEAVESTATUS", mytype = SqlDbType.NVarChar, Value = LeaveForApproval.LeaveStatus });

            // DataTable EmployeeDt = new DataTable();
            String ID = Connection.ExecuteScalar("sp_ApprovedDenyLeaves_Mobile");

            return ID;

        }

        [HttpPost]
        [Route("ShowAvailableLeaves")]
        public GetAvailableLeaves ShowAvailableLeaves(LeaveParameters LeaveParameters)
        {
            GetAvailableLeaves GetLeaveResult = new GetAvailableLeaves();
            try
            {
                Connection Connection = new Connection();
                List<AvailableLeave> ListLeave = new List<AvailableLeave>();
                DataSet ds = new DataSet();
                DataTable dtLEAVES = new DataTable();

                Connection.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = LeaveParameters.NTID });

                ds = Connection.GetDataset("sp_AvailableLeaves_Mobile");
                dtLEAVES = ds.Tables[0];
                if (dtLEAVES.Rows.Count > 0)
                {
                    foreach (DataRow row in dtLEAVES.Rows)
                    {
                        AvailableLeave AvailableLeaves = new AvailableLeave
                        {
                            Ltype = row["Ltype"].ToString(),
                            SerialID = row["SerialID"].ToString(),
                            // LastLeave = row["LastLeaveDate"].ToString()
                        };
                        ListLeave.Add(AvailableLeaves);
                    }
                    GetLeaveResult.AvailableLeaves = ListLeave;
                    GetLeaveResult.myreturn = "Success";
                }
                else
                {
                    GetLeaveResult.myreturn = "No Data Available";
                }
            }
            catch (Exception ex)
            {
                GetLeaveResult.myreturn = ex.Message;
            }
            return GetLeaveResult;
        }




    }
}
