﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using illimitadoWepAPI.Methods.PayslipAuth;
using illimitadoWepAPI.Models;

namespace illimitadoWepAPI.Services.PayslipAuth
{
    public interface IPayslipAuth
    {
        List<PayslipURL> GetCrawfordPayslip(PayslipModelPA model);
    }

    public class SPayslipAuth : IPayslipAuth
    {
        private const string ERR_CHECK = "Parameter Error... please check your parameters";
        private const string ERR_DATABASE = "Database error...";
        private const string ERR_NO_DATA = "Succesfully executed the procedure but it returned no data...";

        public List<PayslipURL> GetCrawfordPayslip(PayslipModelPA model)
        {
            PayslipAuthMethods methods = new PayslipAuthMethods();

            // Check if there's an error in the parameters
            var errorList = methods.ValidateCrawfordModel(model);
            if (errorList.Count > 0) 
                return new List<PayslipURL> { new PayslipURL { url = ERR_CHECK } };

            var connectionStringModel = methods.GetConnectionString(model.Code);
            if (connectionStringModel == null) 
                return new List<PayslipURL> { new PayslipURL { url = ERR_CHECK } };

            List<PayslipURL> response = new List<PayslipURL>();
            SqlConnection oConn = new SqlConnection(connectionStringModel.DB_Master);
            SqlTransaction oTrans;
            oConn.Open();
            oTrans = oConn.BeginTransaction();
            SqlCommand oCmd = new SqlCommand();
            oCmd.Connection = oConn;
            oCmd.Transaction = oTrans;

            try
            {
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = oCmd;

                // Check if payoutdate is existing
                oCmd.CommandText = "sp_CheckPayoutDate";
                da.SelectCommand.CommandType = CommandType.StoredProcedure;

                oCmd.Parameters.Clear();
                oCmd.Parameters.AddWithValue("@tenant_code", model.Code);
                oCmd.Parameters.AddWithValue("@payout_date", model.PayoutDate);
                int hasPayout = (int)oCmd.ExecuteScalar();

                if (hasPayout < 1)
                    return new List<PayslipURL> { new PayslipURL { url = "Payout date doesn't exist" } };

                // Get Tenant ID and Name
                DataTable dt_tenant_info = new DataTable();
                
                oCmd.CommandText = "sp_GetTenantInfo_TenantCode";
                da.SelectCommand.CommandType = CommandType.StoredProcedure;

                oCmd.Parameters.Clear();
                oCmd.Parameters.AddWithValue("@tenant_code", model.Code);
                da.Fill(dt_tenant_info);

                string tenant_id = dt_tenant_info.Rows[0][0].ToString();
                string tenant_name = dt_tenant_info.Rows[0][1].ToString();

                // Get all users from based on tenant code
                DataTable dt_users = new DataTable();

                oCmd.CommandText = "sp_CrawfordGetEmployee_PayslipObject";
                da.SelectCommand.CommandType = CommandType.StoredProcedure;

                oCmd.Parameters.Clear();
                oCmd.Parameters.AddWithValue("@tenant_code", model.Code);
                oCmd.Parameters.AddWithValue("@payout_date", model.PayoutDate);
                da.Fill(dt_users);

                oConn.Close();

                if (dt_users.Rows.Count < 1)
                    return new List<PayslipURL> { new PayslipURL { url = ERR_NO_DATA } };

                // Generate and Pack Payslips
                response.Add(new PayslipURL(){
                    url = methods.GenerateAndCompressPayslips(dt_users, tenant_id, model)
                });

                // Generate JSON File
                response.Add(new PayslipURL(){
                    url = methods.GenerateJSONPayslips(model)
                });
            }
            catch (Exception e)
            {
                Console.WriteLine("Error: " + e.Message);
                return new List<PayslipURL> { new PayslipURL { url = ERR_DATABASE } };
            }
            finally
            {
                oConn.Close();
            }

            return response;
        }
    }
}
