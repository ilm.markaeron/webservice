﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace illimitadoWepAPI.Models.LoanMakerModels
{
    public class LoanMutliselectItems
    {
        public string Value { get; set;}
        public string Text { get; set; }
    }
    public class Master
    {
        public string EmpLevel { get; set; }
        public string Gender { get; set; }
        public string MonthlySalary { get; set; }
        public string Tenure { get; set; }
        public string Industry { get; set; }
    }
    public class LoanMainList
    {
        public string ListItem { get; set; }
    }
    public class LoanMain
    {
        public string _ID { get; set; }
        public string _LoanType { get; set; }
        public string _CountryID { get; set; }
        public string _RegionStateID { get; set; }
        public string _CityMunicipalityID { get; set; }
        public string _ResidentialStatus { get; set; }
        public string _NoCarsOwnedDD { get; set; }
        public string _NoCarsOwned { get; set; }
        public string _TypeOfEmployement { get; set; }
        public string _EmployeeStatus { get; set; }
        public string _IndustryID { get; set; }
        public string _CompanyID { get; set; }
        public string _RankDD { get; set; }
        public string _Rank { get; set; }
        public string _TenureDD { get; set; }
        public string _TenureNo { get; set; }
        public string _TenureType { get; set; }
        public string _SecurityBankHolder { get; set; }
        public string _CreditCardHolder { get; set; }
        //public string _ExitPayRangeFromDD { get; set; }
        public string _ExitPayRangeFromNo { get; set; }
        //public string _ExitPayRangeToDD { get; set; }
        public string _ExitPayRangeToNo { get; set; }
        //public string _SalaryRangeFromDD { get; set; }
        public string _SalaryRangeFromNo { get; set; }
        //public string _SalaryRangeToDD { get; set; }
        public string _SalaryRangeToNo { get; set; }
        //public string _CreditScoreFromDD { get; set; }
        public string _CreditScoreFromNo { get; set; }
        //public string _CreditScoreToDD { get; set; }
        public string _CreditScoreToNo { get; set; }
        public string _CriminalOffenseDD { get; set; }
        public string _CriminalOffenseNo { get; set; }
        public string _DisciplinaryDD1 { get; set; }
        public string _DisciplinaryDD2 { get; set; }
        public string _DisciplinaryNo { get; set; }
        //public string _AttritionFromDD { get; set; }
        public string _AttritionFromNo { get; set; }
        //public string _AttritionToDD { get; set; }
        public string _AttritionToNo { get; set; }
        //public string _EmpReliabilityFromDD { get; set; }
        public string _EmpReliabilityFromNo { get; set; }
        //public string _EmpReliabilityToDD { get; set; }
        public string _EmpReliabilityToNo { get; set; }
        public string _Gender { get; set; }
        //public string _AgeFromDD { get; set; }
        public string _AgeFromNo { get; set; }
        //public string _AgeToDD { get; set; }
        public string _AgeToNo { get; set; }
        public string _TaxStatus { get; set; }
        public string _DependentDetails { get; set; }
        public string _DepDetAgeDD { get; set; }
        public string _DepDetAgeNo { get; set; }
        public string _EducationalAttainment { get; set; }
        public string b64 { get; set; }
        public string _CreatedBy { get; set; }
        public string _ModifiedBy { get; set; }
        public string _RequestDateTime { get; set; }
        public string message { get; set; }
        public List<LoanMainList> GenderList { get; set; }
        public List<LoanMainList> ResidentialList { get; set; }
        public List<LoanMainList> EmploymentList { get; set; }
        public List<LoanMainList> EmploymentStatusList { get; set; }
        public List<LoanMainList> CompanyList { get; set; }
        public List<LoanMainList> DisciplinaryList { get; set; }
        public List<LoanMainList> TaxList { get; set; }
        public List<LoanMainList> DependentList { get; set; }
        public List<LoanMainList> EducationalList { get; set; }
        public List<LoanMainList> CountryList { get; set; }
        public List<LoanMainList> RegionList { get; set; }
        public List<LoanMainList> CityList { get; set; }
        public List<LoanMainList> IndustryList { get; set; }
        public List<LoanMainList> RankList { get; set; }
        //Type of Employment
        //Company
        //Disciplinary Action Count of
        //Gender
        //Tax Status
        //Dependent Details
        //Highest Educational Attainment

    }

    public class DropList
    {
        public string _Id { get; set; }
        public string _Subject { get; set; }
        public bool _Status { get; set; }
        public string _DefaultName { get; set; }
        public string _ModifiedBy { get; set; }
        public string _ModifiedDate { get; set; }
        public bool _Edit { get; set; }
    }

    public class DropDownList
    {
        public List<DropList> SampleList;
        public string _myreturn { get; set; }
    }
    public class DropDownSubjectList
    {
        public string _Id { get; set; }
        public string _Sid { get; set; }
        public string _SubSubject { get; set; }
        public bool _Status { get; set; }
        public string _ModifiedDate { get; set; }
        public string _ModifiedBy { get; set; }
        public bool _Edit { get; set; }
    }


    public class FunctionUserAccessType
    {
        public string _Functions { get; set; }
    }
    public class SubjectList
    {
        public List<DropDownSubjectList> SubjectListData;
        public string _myreturn { get; set; }
    }


    public class AutoLoanDetails
    {
        public string _LoanID { get; set; }
        public string _LoanName { get; set; }
        public string _DebtIncomeDD { get; set; }
        public string _DebtIncomeNo { get; set; }
        public string _AmortizationType { get; set; }
        public string _CarType { get; set; }
        public string _CashPriceOpt { get; set; }
        //public string _CashPriceFromDD { get; set; }
        public string _CashPriceFromNo { get; set; }
        //public string _CashPriceToDD { get; set; }
        public string _CashPriceToNo { get; set; }
        public string _CashPrice { get; set; }
        public string _CashPriceDD { get; set; }
        public string _PaymentOption { get; set; }
        public string _LoanTerm { get; set; }
        public string _InterestRate { get; set; }
        public string _InterestType { get; set; }
        public string _InterestOptionNo { get; set; }
        public string _InterestOptionDateType { get; set; }
        public string _PaymentDate { get; set; }
        public string _LimitedSlotOpt { get; set; }
        public string _LimitedSlotNo { get; set; }
        public string message { get; set; }
    }

    public class HomeLoanDetails
    {
        public string _LoanID { get; set; }
        public string _LoanName { get; set; }
        public string _DebtIncomeDD { get; set; }
        public string _DebtIncomeNo { get; set; }
        public string _PropertyType { get; set; }
        public string _PropertyStatus { get; set; }
        public string _CashPriceOpt { get; set; }
        //public string _CPO1FromDD { get; set; }
        public string _CPO1FromNo { get; set; }
        //public string _CPO1ToDD { get; set; }
        public string _CPO1ToNo { get; set; }
        public string _CPO2No { get; set; }
        public string _CPO2DD { get; set; }
        public string _PaymentOption { get; set; }
        public string _LoanTerm { get; set; }
        public string _InterestRate { get; set; }
        public string _InterestType { get; set; }
        public string _InterestOptionNo { get; set; }
        public string _InterestOptionDD { get; set; }
        public string _PaymentDate { get; set; }
        public string _LimitedSlotsOpt { get; set; }
        public string _LimitedSlotsNo { get; set; }
        public string _BrokerName { get; set; }
        public string _BrokerContactNo { get; set; }
        public string _ProjectName { get; set; }
        public string _Developer { get; set; }
        public string _PropertyContactPerson { get; set; }
        public string _PropertyContactNo { get; set; }
        public string _AppraisedProperty { get; set; }
        public string message { get; set; }
    }

    public class UserAccessTypeList
    {
        public string _Id { get; set; }
        public string _AccessTypes { get; set; }
    }
    public class UpdatingUserAccessTypeList
    {
        public List<UserAccessTypeList> UpdateUserAccessListData;
        public string _myreturn { get; set; }
    }

    public class PersonalLoanDetails
    {
        public string _LoanID { get; set; }
        public string _LoanName { get; set; }
        public string _LoanAmountOpt { get; set; }
        public string _LoanAmount { get; set; }
        public string _LoanAmountPercent { get; set; }
        public string _LoanAmountDD { get; set; }
        public string _DebtIncomeRatioDD { get; set; }
        public string _DebtIncomeRatio { get; set; }
        public string _InterestRate { get; set; }
        public string _InterestTypeDD { get; set; }
        public string _LoanTerm { get; set; }
        public string _LoanTermDD { get; set; }
        public string _LoanTermPayback { get; set; }
        public string _LimtedSlotsOpt { get; set; }
        public string _LimitedSlotsApp { get; set; }
        public string _PaymentDateDD { get; set; }
        public string message { get; set; }
    }
    public class LoanList
    {
        public string _LoanID { get; set; }
        public string _LoanName { get; set; }
        public string _LoanType { get; set; }
        public string _CreatedBy { get; set; }
        public string _CreatedDate { get; set; }
        public string _ModifiedBy { get; set; }
        public string _ModifiedDate { get; set; }
        public string Message { get; set; }
    }

    public class LoanStatusList
    {
        public string _LoanID { get; set; }
        public string _LoanName { get; set; }
        public string _LoanType { get; set; }
        public string _StartDateTime { get; set;}
        public string _AccessType { get; set; }
        public string _CreatedBy { get; set; }
        public string _CreatedDate { get; set; }
        public string _ModifiedBy { get; set; }
        public string _ModifiedDate { get; set; }
        public string _Status { get; set; }
        public string Message { get; set; }
    }

    public class LoanDeniedStatusList
    {
        public string _LoanID { get; set; }
        public string _LoanName { get; set; }
        public string _LoanType { get; set; }
        public string _AccessType { get; set; }
        public string _CreatedBy { get; set; }
        public string _CreatedDate { get; set; }
        public string _ModifiedBy { get; set; }
        public string _ModifiedDate { get; set; }
        public string _Status { get; set; }
        public string Message { get; set; }
    }

    public class ZipMunRegCountryParam
    {
        public string ID { get; set; }
        public string Zipcode { get; set; }
        public string Country { get; set; }
        public string Region { get; set; }
        public string Province { get; set; }
        public string Municipal { get; set; }

    }
    public class ZipMunRegCountry
    {
        public List<ZipMunRegCountryParam> ZipMunRegCountryParam;
        public string myreturn;
    }

    public class LoanStatus
    {
        public string StatusList { get; set; }
    }

    public class updatestat
    {
        public string _ID { get; set; }
        public string _Status { get; set; }
        public List<LoanStatus> ID { get; set; }
    }

    public class InsertUserAccessType
    {
        public string _Functions { get; set; }
        public string _AccessTypes { get; set; }
    }
    public class InsertUserAccessTypeList
    {
        public List<InsertUserAccessType> InsertUserAccessListData;
        public string _myreturn { get; set; }
    }

    public class DeleteUserAccessType
    {
        public string _Id { get; set; }
    }
    public class DeleteUserAccessTypeList
    {
        public List<DeleteUserAccessType> DeleteUserAccessListData;
        public string _myreturn { get; set; }
    }

    public class deleteLoan
    {
        public string ID { get; set; }
    }

    public class insertemplist
    {
        public string _LoanID { get; set; }
    }
    public class AcuiroInsurance
    {
        public string _LoanID { get; set; }
        public string _SmokingHistory { get; set; }
        public string _AlcoholHistory { get; set; }
        public string ExcludedIllnesses { get; set; }
        public string _PremiumIncomeDD { get; set; }
        public string _PremiumIncomeNo { get; set; }
        public string _InterestRate { get; set; }
        public string _InterestType { get; set; }
        public string _PaymentTermNo { get; set; }
        public string _PaymentTermDD { get; set; }
        public string _LimitedSlots { get; set; }
        public string _LimitedSlotsNo { get; set; }
        public string _PaymentDate { get; set; }
    }

    public class loanMakerGetCountParam
    {
        public string personalLoan { get; set; }
        public string autoLoan { get; set; }
        public string homeLoan { get; set; }
    }

    public class loanMakerGetCount
    {
        public List<loanMakerGetCountParam> loanMakerGetCountParam;
        public string myreturn;
    }

    public class loangetwebnotif
    {
        public string getNotif { get; set; }
    }

    public class loanMakerGetCountWebNotif
    {
        public List<loangetwebnotif> loanMakerGetCountParam;
        public string myreturn;
    }
    public class UserAccessList
    {
        public string _Id { get; set; }
        public string _UserType { get; set; }
    }
    public class AcuiroUserAccess
    {
        public List<UserAccessList> UserAccessListData;
        public string _myreturn { get; set; }
    }
    public class AllFunctionAccessList
    {
        public string _AccessTypeID { get; set; }
        public string _Parameter2 { get; set; }
    }

    public class AllFunctionAccessType
    {
        public List<AllFunctionAccessList> AllFunctionListAccessData;
        public string _myreturn { get; set; }
    }
    public class AllFunctionID
    {
        public string AccessType { get; set; }
    }
    public class SelectedFunctionAccessList
    {
        public string _AccessTypeID { get; set; }
        public string _Parameter1 { get; set; }
    }
    public class SelectedFunctionAccessType
    {
        public List<SelectedFunctionAccessList> SelectedFunctionListAccessData;
        public string _myreturn { get; set; }
    }
    public class SelectFunctionID
    {
        public string AccessType { get; set; }
    }
    public class UpdateDropDownTypeList
    {
        public string _Id { get; set; }
        public string _Subject { get; set; }
        public string _ModifiedBy { get; set; }
    }

    public class UpdatingDropDownTypeList
    {
        public List<UpdateDropDownTypeList> UpdateDropdownListData;
        public string _myreturn { get; set; }
    }
    public class SUbjectListDetails
    {
        public string _userID { get; set; }
        public string _Id { get; set; }
        public string _sId { get; set; }
        public string _SubSubject { get; set; }
        public string _Status { get; set; }
        public string _ModifiedDate { get; set; }
        public string _ModifiedBy { get; set; }
    }
    /// SubjectListInactiveActive
    public class SubjectListInactiveActive
    {
        public string _UserID { get; set; }
        public string _SubjectID { get; set; }
    }
    /// SubjectInactiveActive
    public class SubjectInactiveActive
    {
        public string _UserID { get; set; }
        public string _SubjectID { get; set; }
    }
    ///  InsertSelectedFunction
    public class DataTableSelInsert
    {
        public string _UserType { get; set; }
        public string _Function { get; set; }
        public string _AccessTypeID { get; set; }
        public String[] list { get; set; }
    }
    // Create user
    public class userType
    {
        public string _userID { get; set; }
        public string _userType { get; set; }
    }

    public class employeeInfoList
    {
        public string _empid { get; set; }
        public string _firstName { get; set; }
        public string _middleName { get; set; }
        public string _lastName { get; set; }
        public string _title { get; set; }
        public string _role { get; set; }
        public string _area { get; set; }
        public string _branch { get; set; }
        public string _department { get; set; }
        public string _subdepartment { get; set; }
        public string _accessType { get; set; }
        public string _email { get; set; }
        public string _phoneNumber { get; set; }
        public string _status { get; set; }
        public string _userid { get; set; }
        public string _name { get; set; }
        public string _loginid { get; set; }
        public bool _autoapprove { get; set; }

    }
    public class employeeInfo
    {
        public List<employeeInfoList> employeeInfoList;
        public string _myreturn { get; set; }
        public string _UID { get; set; }
    }

    public class createuser
    {
        public string _userid { get; set; }
        public string _loginid { get; set; }
        public string _user { get; set; }
        public string _pass { get; set; }
        public string _fname { get; set; }
        public string _mname { get; set; }
        public string _lname { get; set; }
        public string _title { get; set; }
        public string _role { get; set; }
        public string _area { get; set; }
        public string _branch { get; set; }
        public string _department { get; set; }
        public string _subdepartment { get; set; }
        public string _company { get; set; }
        public string _usertype { get; set; }
        public string _email { get; set; }
        public string _phone { get; set; }
        public string _updatetype { get; set; }
        public bool _autoapprove { get; set; }
    }

    public class changePassword
    {
        public string _UID { get; set; }
        public string _oldPassword { get; set; }
        public string _newPassword { get; set; }
    }

    public class subjectlist
    {
        public string _SID { get; set; }
        public string _Subject { get; set; }
        public bool _Status { get; set; }
        public string _DefaultName { get; set; }
        public string _ModifiedBy { get; set; }
        public string _ModifiedDate { get; set; }
        public bool _Edit { get; set; }
    }
    public class subjects
    {
        public List<subjectlist> subjectlist;
        public string _myreturn { get; set; }
    }
    public class subsubjectlist
    {
        public string _RowID { get; set; }
        public string _SID { get; set; }
        public string _Subject { get; set; }
        public bool _Status { get; set; }
        public string _ModifiedBy { get; set; }
        public string _ModifiedDate { get; set; }
        public bool _Edit { get; set; }
    }
    public class subsubjects
    {
        public List<subsubjectlist> subsubjectlist;
        public string _myreturn { get; set; }
    }
    public class gridsearch
    {
        public string _status { get; set; }
        public string _branch { get; set; }
        public string _department { get; set; }
        public string _subdepartment { get; set; }
        public string _requestor { get; set; }
        public string _datefrom { get; set; }
        public string _dateto { get; set; }
    }

    public class requestor
    {
        public string _userid { get; set; }
        public string _requestorname { get; set; }
        public string _usertype { get; set; }
    }
    public class requestorlist
    {
        public List<requestor> requestor;
        public string _myreturn { get; set; }
    }

    public class subjectlabel
    {
        public string _title { get; set; }
        public string _role { get; set; }
        public string _area { get; set; }
        public string _branch { get; set; }
        public string _department { get; set; }
        public string _subdepartment { get; set; }
    }
    public class subjectlabellist
    {
        public List<subjectlabel> subjectlabel;
        public string _myreturn { get; set; }
    }

    public class DropdownHierarchy
    {
        public string _source { get; set; }
        public string _target { get; set; }
        public string _paramater { get; set; }
        public String[] _items { get; set; }
    }
    public class DropdownHierarchyItems
    {
        public string _itemid { get; set; }
        public string _itemname { get; set; }
    }
    public class DropdownHierarchyList
    {
        public List<DropdownHierarchyItems> DDITEMS;
        public string _myreturn { get; set; }
    }

    // Access Type

    public class accesslist
    {
        public string _SID { get; set; }
        public string _Subject { get; set; }
    }
    public class access
    {
        public List<accesslist> accesslist;
        public string _myreturn { get; set; }
    }
    public class accesstypelist
    {
        public string _RowID { get; set; }
        public string _SID { get; set; }
        public string _Subject { get; set; }
    }
    public class accesstype
    {
        public List<accesstypelist> accesstypelist;
        public string _myreturn { get; set; }
    }

    public class UserDataMappingItems
    {
        public String[] _userid { get; set; }
        public String[] _fname { get; set; }
        public String[] _mname { get; set; }
        public String[] _lname { get; set; }
        public String[] _title { get; set; }
        public String[] _role { get; set; }
        public String[] _area { get; set; }
        public String[] _branch { get; set; }
        public String[] _department { get; set; }
        public String[] _subdepartment { get; set; }
        public String[] _usertype { get; set; }
        public String[] _email { get; set; }
        public String[] _phonenumber { get; set; }
    }
    public class UserMap
    {
        public string _rowid { get; set; }
        public string _fname { get; set; }
        public string _mname { get; set; }
        public string _lname { get; set; }
        public string _title { get; set; }
        public string _role { get; set; }
        public string _area { get; set; }
        public string _branch { get; set; }
        public string _department { get; set; }
        public string _subdepartment { get; set; }
        public string _usertype { get; set; }
        public string _email { get; set; }
        public string _phonenumber { get; set; }
    }
    public class UserList
    {
        public string[] _ret { get; set; }
        public List<UserMap> UserMapList;
        public string _myreturn { get; set; }
    }

    // End Create user

    public class DataTableAllInsert
    {
        public string _UserType { get; set; }
        public string _Function { get; set; }
        public string _AccessTypeID { get; set; }
        public String[] list { get; set; }
    }
    /// GetEmpSelectedFunction
    public class GetSelectedFunctionList
    {
        public string _EMPID { get; set; }
        public string _FUNCTION { get; set; }

    }
    public class SelectedFunctionList
    {
        public string _Parameter1 { get; set; }
        public List<SelectedFunctionList> Selectedfunction;
        public string _myreturn { get; set; }
    }

    public class SaveReport
    {
        public string _SaveAs { get; set; }
        public string _ReportName { get; set; }
        public string _Description { get; set; }
        public string _ValidUntil { get; set; }
        public string _Type { get; set; }
    }

    public class Report_FieldIds
    {
        public string _ID { get; set; }
    }

    public class RunReportIDs
    {
        public List<Report_FieldIds> FId;
        public string _myreturn { get; set; }
    }

    public class SelectedFieldID
    {
        public string FieldID { get; set; }
    }

    public class view_report
    {
        public string _Id { get; set; }
        public string _ReportName { get; set; }
        public string _CreatedDate { get; set; }
        public string _CreatedBy { get; set; }
    }

    public class reportList
    {
        public List<view_report> RList;
        public string _myreturn { get; set; }
    }
    public class ReportCred
    {
        public string ID { get; set; }
    }
}