﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace illimitadoWepAPI.Models
{
    public class Access
    {
        public string Parameter { get; set; }
        public string EditName { get; set; }
        public string Link { get; set; }
        public string Icon { get; set; }
        public string id { get; set; }
    }

    public class AccessParam
    {
        public string CN { get; set; }
        public string EmpID { get; set; }
        public string Function { get; set; }
    }


    public class ListAccess
    {
        public List<Access> Access;
        public string myreturn;
    }
    public class MR_Export
    {
        public string CN { get; set; }
        public string qry { get; set; }
    }
    public class CR_Report
    {
        public string CN { get; set; }
        public string datefrom { get; set; }
        public string dateto { get; set; }
    }
    public class SchedParam
    {
        public string CN { get; set; }
        public string[] EmpID { get; set; }
        public string[] ScheduleType { get; set; }
        public string startdate { get; set; }
        public string enddate { get; set; }
    }
    public class SchedClass
    {
        public List<SchedList> ReturnList;
        public string myreturn { get; set; }
    }
    public class SchedList
    {
        public string SeriesID { get; set; }
        public string BusinessSegment { get; set; }
        public string BusinessUnit { get; set; }
        public string ImmediateSupervisor { get; set; }
        public string EmpLevel { get; set; }
        public string EmpCategory { get; set; }
        public string EmpName { get; set; }
        public string EmpID { get; set; }
        public string CutOff_From { get; set; }
        public string CutOff_To { get; set; }
        public string Date { get; set; }
        public string Day { get; set; }
        public string Schedule { get; set; }
        public string SchedIN { get; set; }
        public string Break1dsply { get; set; }
        public string Lunchdsply { get; set; }
        public string Break2dsply { get; set; }
        public string Break3dsply { get; set; }
        public string SchedOUT { get; set; }
        public string SchedDate { get; set; }
        public string LunchEnd { get; set; }
        public string Break1Minutes { get; set; }
        public string Break2Minutes { get; set; }
        public string Break3Minutes { get; set; }
        public string ClientName { get; set; }
        public string ClientID { get; set; }
        public string BreakType { get; set; }
        public string Break1End { get; set; }
        public string Break2End { get; set; }
        public string Break3End { get; set; }
        public string ShiftType { get; set; }
        public string Break1dsplyEnd { get; set; }
        public string Break2dsplyEnd { get; set; }
        public string Berak3dsplyEnd { get; set; }
        public string ShiftNum { get; set; }
        public string ScheduleType { get; set; }
    }
}