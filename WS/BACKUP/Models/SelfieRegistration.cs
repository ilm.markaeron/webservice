﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace illimitadoWepAPI.Models
{
    public class SelfieRegistration
    {
        public string NTID { get; set; }
        public string Name { get; set; }
        //public string FaceID { get; set; }
        public string PersonGroupID { get; set; }
        public string CN { get; set; }
    }

    public class CreatePersonGroup
    {
        public string PersonGroupID { get; set; }
        public string Name { get; set; }
    }
    public class CreateLargePersonGroup
    {
        public string LargePersonGroupID { get; set; }
    }
    public class DetectFace
    {
        public string PersonGroup { get; set; }
        public string NTID { get; set; }
        public string imgB64 { get; set; }
    }
    public class DeletePerson
    {
        public Guid PersonID { get; set; }
        public string PersonGroupID { get; set; }
    }
    public class DeletePersonCredentials
    {
        public string CN { get; set; }
        public string EmpID { get; set; }
    }

}