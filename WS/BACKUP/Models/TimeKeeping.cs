﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace illimitadoWepAPI.Models
{
    public class TimeKeeping
    {
        public class Daily
        {
            public string SchedDate { get; set; }
            public string SchedDay { get; set; }
            public string SchedIN { get; set; }
            public string SchedOUT { get; set; }
            public string TotalTimeMin { get; set; }
            public string SchedType { get; set; }
            public string DateFormat { get; set; }
            public string EmpID { get; set; }
            public string ForApproval { get; set; }
            public string SeriesID { get; set; }
            public string Name { get; set; }
            public string Reason { get; set; }
            public string ReasonID { get; set; }

        }
        public class GetDailyResult
        {
            public List<Daily> Daily;
            public string myreturn;
        }

        public class DailyParameters
        {
            public string NTID { get; set; }
            public string Start_Date { get; set; }
            public string End_Date { get; set; }
            public string Coordinates { get; set; }
            public string NearestEstablishment { get; set; }
            public string CN { get; set; }
            public string SeriesID { get; set; }
            public string TempID { get; set; }
        }

        // Nhoraisa Insert Survey
        public class CarInformation
        {
            public string CN { get; set; }
            public string _EmpID { get; set; }
            public string _Own { get; set; }
            public string _isRemindedDue { get; set; }
            public string _Brand { get; set; }
            public string _Model { get; set; }
            public string _Variant { get; set; }
            public string _DatePurchased { get; set; }
            public string _YearMake { get; set; }
            public string _PlateNum { get; set; }
            public string _isInsured { get; set; }
            public string _PolicyExpDate { get; set; }
            public string _PremiumAmount { get; set; }
            public string _isRemindedExp { get; set; }
            public string _isCarLoan { get; set; }
            public string _DueDate { get; set; }
            public string _AmortizationAmount { get; set; }
            public string _LoanEndMonth { get; set; }
            public string _LoanEndYear { get; set; }
            public string _Url { get; set; }
        }

        public class HomeInformation
        {
            public string CN { get; set; }
            public string _EmpID { get; set; }
            public string _HomeStatus { get; set; }
            public string _HouseType { get; set; }
            public string _PayOften { get; set; }
            public string _NextPayDue { get; set; }
            public string _PayDueDate { get; set; }
            public string _RentAmount { get; set; }
            public string _isRemindedHoa { get; set; }
            public string _PayOftenHoa { get; set; }
            public string _NextPayDueHoa { get; set; }
            public string _PayDueDateHoa { get; set; }
            public string _HoaAmount { get; set; }
            public string _LoanBank { get; set; }
            public string _LoanTerm { get; set; }
            public string _LoanTermMonth { get; set; }
            public string _LoanStartMonth { get; set; }
            public string _LoanStartYear { get; set; }
            public string _PayOftenLoan { get; set; }
            public string _NextPayDueLoan { get; set; }
            public string _PayDueDateLoan { get; set; }
            public string _PayMonthlyAmount { get; set; }
            public string _Url { get; set; }
        }

        public class SchoolFeeInformation
        {
            public string CN { get; set; }
            public string _EmpID { get; set; }
            public string _isPayingTuition { get; set; }
            public string _SchoolName { get; set; }
            public string _DownPaymentAmount { get; set; }
            public string _1stQAmount { get; set; }
            public string _1stQDate { get; set; }
            public string _2ndQAmount { get; set; }
            public string _2ndQDate { get; set; }
            public string _3rdQAmount { get; set; }
            public string _3rdQDate { get; set; }
            public string _4thQAmount { get; set; }
            public string _4thQDate { get; set; }
            public string Url { get; set; }
        }

        public class LoanInformation
        {
            public string CN { get; set; }
            public string _EmpID { get; set; }
            public string _isOtherLoan { get; set; }
            public string _BankLoan { get; set; }
            public string _LoanTermNum { get; set; }
            public string _LoanTerm { get; set; }
            public string _LoanStartMonth { get; set; }
            public string _LoanStartYears { get; set; }
            public string _PayOften { get; set; }
            public string _NextPayDue { get; set; }
            public string _PayDueDate { get; set; }
            public string _PayMonthlyAmount { get; set; }
            public string _Url { get; set; }
        }

        public class UtlitiesInformation
        {
            public string CN { get; set; }
            public string _EmpID { get; set; }
            public string _ServiceType { get; set; }
            public string _ProviderName { get; set; }
            public string _AccountNumber { get; set; }
            public string _MonthlyFee { get; set; }
            public string _DueDate { get; set; }
        }

        public class ForBreaks
        {
            public string CN { get; set; }
            public string NTID { get; set; }
            public string ReasonID { get; set; }
            public string Coordinates { get; set; }
            public string NearestEstablishment { get; set; }
            // public string StartOrEnd { get; set; }
        }

        public class UpadateFaceID
        {
            public string NTID { get; set; }
            public string FaceID { get; set; }

        }


        public class ListAllActivityByRange
        {
            public string EmpID { get; set; }
            public string SeriesID { get; set; }
            public string StartTIme { get; set; }
            public string StartEnd { get; set; }
            public string ReasonID { get; set; }
            public string ReasonDesc { get; set; }
            public string IsPaid { get; set; }
            public string SchedDate { get; set; }
            public string HourWork { get; set; }
            public string ActivityStatus { get; set; }
            public string Day { get; set; }
            public string SchedDateStatus { get; set; }
        }
        public class ListAllActivityResult
        {
            public List<ListAllActivityByRange> ListAllActivity;
            public string myreturn;
        }

        public class Hoursworked
        {
            public string EmpID { get; set; }
            //public string SeriesID { get; set; }
            //public string StartTIme { get; set; }
            //public string StartEnd { get; set; }
            //public string ReasonID { get; set; }
            //public string ReasonDesc { get; set; }
            //public string isPaid { get; set; }
            public string SchedDate { get; set; }
            public string HourWork { get; set; }
            public string Schedule { get; set; }
            public string Day { get; set; }
            public string TotalTime { get; set; }
        }
        public class ListAllHoursworked
        {
            public List<Hoursworked> ListAllActivity;
            public string myreturn;
        }

        //public class GetLogs
        //{
        //    public string SeriesID { get; set; }
        //    public string EmpID { get; set; }
        //    public string EmpName { get; set; }
        //    public string StartTime { get; set; }
        //    public string EndTime { get; set; }
        //    public string Reason { get; set; }
        //    public string SchedDate { get; set; }
        //    public string TotalTime { get; set; }
        //    public string Status { get; set; }
        //    public string Approver { get; set; }

        //}

        //public class GetLogsID
        //{
        //    public string EmpID { get; set; }
        //}

        //public class Logs
        //{
        //    public List<GetLogs> LogsListData;
        //    public string _myreturn { get; set; }
        //}



        //public class GetLeaves
        //{
        //    public string ID { get; set; }
        //    public string EmpID { get; set; }
        //    public string EmpName { get; set; }
        //    public string ManagerID { get; set; }
        //    public string ManagerName { get; set; }
        //    public string LeaveDate { get; set; }
        //    public string LeaveType { get; set; }
        //    public string DateApplied { get; set; }
        //    public string Halfday { get; set; }
        //    public string LeaveID { get; set; }
        //    public string Gender { get; set; }
        //    public string Expired { get; set; }
        //    public string Approver { get; set; }

        //}

        //public class GetLeavesID
        //{
        //    public string EmpID { get; set; }
        //}

        //public class Leaves
        //{
        //    public List<GetLeaves> LeavesListData;
        //    public string _myreturn { get; set; }
        //}

        //public class UpdateisSeen
        //{
        //    public string Type { get; set; }
        //    public string SeriesID { get; set; }
        //}

        //public class ApproveDenyLeaves
        //{
        //    public string SeriesID { get; set; }
        //    public string EmpID { get; set; }
        //    public string Status { get; set; }

        //}

        //public class ApproveDenyLogs
        //{
        //    public string SeriesID { get; set; }
        //    public string EmpID { get; set; }
        //    public string Status { get; set; }

        //}


        public class ListAllReasonBreakTypes
        {
            public List<ListAllBreakReasonTypes> ListAllResonType;
            public string myreturn;
        }

        public class AvailableLastSeries
        {
            public string LastSeries { get; set; }

        }


        public class ListAllBreakReasonTypes
        {
            public string ReasonID { get; set; }
            public string ReasonDesc { get; set; }

        }


        public class ListAllReasonType
        {
            public string ReasonID { get; set; }
            public string ReasonDesc { get; set; }
        }
        public class ListAllReasonTypeResult
        {
            public List<ListAllReasonType> ListAllReasonTypes;
            public string myreturn;
        }

        public class ListAllSchedulebyRange
        {
            public string EmpID { get; set; }
            public string Start_Time { get; set; }
            public string End_Time { get; set; }
            public string SchedDate { get; set; }

        }
        public class ListAllSchedulebyRangeResult
        {
            public List<ListAllSchedulebyRange> ListAllSchedule;
            public string myreturn;
        }

        public class ListAllSwapRequest
        {
            public string EMPID { get; set; }
            public string SwapType { get; set; }
            public string Schedule { get; set; }
            public string SwapID { get; set; }
        }

        public class ListAllSwapRequesteResult
        {
            public List<ListAllSwapRequest> ListAllSwapRequests;
            public string myreturn;
        }

        public class LastActivity
        {
            public string LastReasonID { get; set; }
            public string LastType { get; set; }
        }
        public class ListLastActivityResult
        {
            public List<LastActivity> ListLastActivity;
            public string myreturn;
        }

        //---------------------------------------------------------------Revised Change DB Start------------------------------------------------------------------------

        public class DailyParametersv2
        {
            public string NTID { get; set; }
            public string Start_Date { get; set; }
            public string End_Date { get; set; }
            public string Coordinates { get; set; }
            public string NearestEstablishment { get; set; }
            public string CN { get; set; }
        }

        public class ForBreaksv2
        {
            public string NTID { get; set; }
            public string ReasonID { get; set; }
            public string CN { get; set; }
        }

        public class ForIDandLicense
        {
            public string EmpID { get; set; }
            public string IDTYpe { get; set; }
            public string FilePAth { get; set; }
            public string CN { get; set; }
        }
        public class ListAllActivityByRangev2
        {
            public string EmpID { get; set; }
            public string SeriesID { get; set; }
            public string StartTIme { get; set; }
            public string StartEnd { get; set; }
            public string ReasonID { get; set; }
            public string ReasonDesc { get; set; }
            public string IsPaid { get; set; }
            public string SchedDate { get; set; }
            public string HourWork { get; set; }
            public string ActivityStatus { get; set; }
            public string Day { get; set; }
            public string SchedDateStatus { get; set; }
            public string CN { get; set; }
            public string EMPCOMMENT { get; set; }
            public string APPROVERCOMMENT { get; set; }
            public string FILEPATH { get; set; }
            public string Coordinates { get; set; }
            public string NearestEstablishment { get; set; }
            public string ShiftNum { get; set; }
            public string SeriesIDd2 { get; set; }
        }

        public class Dailyv2
        {
            public string SchedDate { get; set; }
            public string SchedDay { get; set; }
            public string SchedIN { get; set; }
            public string SchedOUT { get; set; }
            public string TotalTimeMin { get; set; }
            public string SchedType { get; set; }
            public string DateFormat { get; set; }
            public string EmpID { get; set; }
            public string ForApproval { get; set; }
            public string SeriesID { get; set; }
            public string Name { get; set; }
            public string Reason { get; set; }
            public string CN { get; set; }
        }

        public class UpadateFaceIDv2
        {
            public string NTID { get; set; }
            public string FaceID { get; set; }
            public string CN { get; set; }
        }
        public class AllowTimeINParameter
        {
            public String EMPID { get; set; }
            public String CN { get; set; }
        }
        public class TypeParameter
        {
            public String EMPID { get; set; }
            public String CN { get; set; }
        }
        public class TopDataParameters
        {
            public string NTID { get; set; }
            public string Start_Date { get; set; }
            public string End_Date { get; set; }
            public string Coordinates { get; set; }
            public string NearestEstablishment { get; set; }
            public string IpLocation { get; set; }
            public string Status { get; set; }
            public string RequestEarly { get; set; }
            public string RequestLate { get; set; }
            public string CN { get; set; }
            public string RestDay { get; set; }
        }
        public class DailyTOPDATA
        {
            public string SchedDate { get; set; }
            public string SchedDay { get; set; }
            public string SchedIN { get; set; }
            public string SchedOUT { get; set; }
            public string TotalTimeMin { get; set; }
            public string SchedType { get; set; }
            public string DateFormat { get; set; }
            public string EmpID { get; set; }
            public string ForApproval { get; set; }
            public string SeriesID { get; set; }
            public string Name { get; set; }
            public string Reason { get; set; }
            public string ReasonID { get; set; }

        }
        public class GetDailyResultTOPDATA
        {
            public List<DailyTOPDATA> DailyTOPDATA;
            public string myreturn;
        }
        public class BreaksTOPDATA
        {
            public string CN { get; set; }
            public string NTID { get; set; }
            public string ReasonID { get; set; }
            public string RestDay { get; set; }
            public string Coordinates { get; set; }
            public string NearestEstablishment { get; set; }
        }
        public class DailyParametersTOPDATA
        {
            public string NTID { get; set; }
            public string ResyDay { get; set; }
            public string RequestLate { get; set; }
            public string CN { get; set; }
            public string Coordinates { get; set; }
            public string NearestEstablishment { get; set; }
        }
        public class DailyParametersv3
        {
            public string NTID { get; set; }
            public string Start_Date { get; set; }
            public string End_Date { get; set; }
            public string Coordinates { get; set; }
            public string NearestEstablishment { get; set; }
            public string IpLocation { get; set; }
            public string Status { get; set; }
            public string RequestEarly { get; set; }
            public string RequestLate { get; set; }
            public string CN { get; set; }
        }
        public class DailyParametersv4
        {
            public string NTID { get; set; }
            public string Start_Date { get; set; }
            public string End_Date { get; set; }
            public string Coordinates { get; set; }
            public string NearestEstablishment { get; set; }
            public string IpLocation { get; set; }
            public string Status { get; set; }
            public string RequestEarly { get; set; }
            public string RequestLate { get; set; }
            public string Device { get; set; }
            public string CN { get; set; }
        }
        #region 2010-10-10
        public class GetSegmentSetv1
        {
            public List<DHRSegmentSetv1> SegmentSettingsList;
            public string myreturn;
        }


        public class DHRSegmentSetv1
        {
            public string CN { get; set; }
            public string ROWID { get; set; }

            public string SEGMENTID { get; set; }
            public string SEGMENTPLATFORM { get; set; }
            public string ISFACIALRECOG { get; set; }
            public string ISGEOFENCING { get; set; }
            public string ISGEOLOCATION { get; set; }

            public string REASONDESC { get; set; }
            public string CATEGORY { get; set; }
            public string REASONDESCRIPTION { get; set; }
            public string ALLOWDELETE { get; set; }


        }
        #endregion
        #region 2018-10-15
        public class DHRSEGMENTV1
        {
            public string CN { get; set; }
            public string SEGMENTID { get; set; }
            public string SEGMENT { get; set; }
            public string SEGMENTTYPE { get; set; }
            public string SEGMENTDESC { get; set; }
            public string ISACTIVE { get; set; }
            public string ISMULTIPLE { get; set; }
            public string LOGINID { get; set; }

        }
        public class DHRSEGMENTARRAYV1
        {
            public string CN { get; set; }
            public bool FACIALISACTIVE { get; set; }
            public bool GEOISACTIVE { get; set; }
            public bool GEOLOCATIONISACTIVE { get; set; }
            public string ROWID { get; set; }

        }
        #endregion

        public class SwapModel
        {
            public string CN { get; set; }
            public string EmpID { get; set; }
            public string RequestDate { get; set; }
            public string OldDate { get; set; }
            public string Type { get; set; }
            public string SelectedType { get; set; }

            public string EmpLevel { get; set; }
            public string EmpCat { get; set; }

            public string SwapID { get; set; }
            public string SystemLevel { get; set; }
            public string SystemCategory { get; set; }
            public string CanSwapWithLevel { get; set; }
            public string CanSwapWithCategory { get; set; }
            public string CanSwapFromBranch { get; set; }
            public string CanSwapFromSegment { get; set; }
            public string CanSwapFromBusinessUnit { get; set; }
            public string ID { get; set; }
        }
        public class SwapSched
        {
            public string EmpID { get; set; }
            public string Sched { get; set; }
        }
        public class SwapSchedList
        {
            public List<SwapSched> swapDateList { get; set; }
        }

        public class WeekSched
        {
            public string SchedDate { get; set; }
            public string Schedule { get; set; }
            public string SeriesID { get; set; }
            public string SchedTitle { get; set; }
        }
        public class ListWeekSchedResult
        {
            public List<WeekSched> ListWeekSched;
            public string myreturn;
        }
        public class SwapSelectedSched
        {
            public string SchedDate { get; set; }
            public string EmpID { get; set; }
            public string Shift { get; set; }
            public string Lunch { get; set; }
            public string Break1 { get; set; }
            public string Break2 { get; set; }
            public string LeaveDate { get; set; }
        }
        public class ListSwapSelectedSched
        {
            public List<SwapSelectedSched> ListSelectedSched;
            public string myReturn;
        }
        public class SwapModelv2
        {
            public string CN { get; set; }
            public string EmpID { get; set; }
            public string SelectedDate { get; set; }
            public string SelectedSched { get; set; }
            public string SwapID { get; set; }
            public string SwapStatus { get; set; }
            public string DDLSWAPTYPE { get; set; }
        }
        public class SwapSelectedSchedv2
        {
            public string EmpID { get; set; }
            public string Schedule { get; set; }
            public string NewSchedule { get; set; }
        }
        public class ListSwapSelectedSchedv2
        {
            public List<SwapSelectedSchedv2> listSwapSelectedSchedv2 { get; set; }
            public string myReturn { get; set; }
        }

        public class NotifParam
        {
            public string EMPID { get; set; }
            public string CN { get; set; }
        }

        public class ListAllLeaveNotification
        {
            public string EmpID { get; set; }
            public string SeriesID { get; set; }
            public string EmpName { get; set; }
            public string LeaveDate { get; set; }
            public string LeaveStatus { get; set; }
            public string DateApproved { get; set; }
            public string LeaveType { get; set; }
            public string NotifLike { get; set; }
            public string NotifType { get; set; }
            public string isShow { get; set; }
            public string isSeen { get; set; }
        }
        public class ListAllLeaveNotificationResult
        {
            public List<ListAllLeaveNotification> ListAllLeave;
            public string myreturn;
        }

        public class ListAllTimeNotification
        {
            public string EmpID { get; set; }
            public string SeriesID { get; set; }
            public string EmpName { get; set; }
            public string SchedDate { get; set; }
            public string StartTIme { get; set; }
            public string StartEnd { get; set; }
            public string ReasonName { get; set; }
            public string ActivityStatus { get; set; }
            public string NotifLike { get; set; }
            public string NotifType { get; set; }
            public string isShow { get; set; }
            public string isSeen { get; set; }
            public string DateApproved { get; set; }
        }
        public class ListAllTimeNotificationResult
        {
            public List<ListAllTimeNotification> ListAllTime;
            public string myreturn;
        }

        public class ListAllCountNotification
        {
            public string Time { get; set; }
            public string Leave { get; set; }
            public string News { get; set; }
            public string Greet { get; set; }
            public string All { get; set; }

        }
        public class ListAllCountNotificationResult
        {
            public List<ListAllCountNotification> ListAllCount;
            public string myreturn;
        }

        public class checkMC
        {
            public string EMPID { get; set; }
            public string CN { get; set; }
            public string PARAM { get; set; }
        }
        //---------------------------------------------------------------Revised Change DB End------------------------------------------------------------------------
        public class subjectlabellist
        {
            public string _myreturn { get; set; }
        }
        public class Image
        {
            public string image { get; set; }
        }
        public class TKDDLv2
        {
            public string CN { get; set; }
            public string EMPID { get; set; }
            public string MONTH { get; set; }
            public string YEAR { get; set; }
        }
        public class GetTKDayv2
        {
            public List<TKDayvarv2> TKDAYv2;
            public string myreturn;
        }
        public class TKDayvarv2
        {
            public string TKDays { get; set; }
        }
        public class TKDDL
        {
            public string CN { get; set; }
            public string EMPID { get; set; }
        }
        public class GetTKDay
        {
            public List<TKDayvar> TKDAY;
            public string myreturn;
        }
        public class TKDayvar
        {
            public string TKDays { get; set; }
        }
        public class ListPreviousNext
        {
            public List<PreviousNext> PreviousNext;
            public string myreturn;
        }
        public class PreviousNext
        {
            public string CN { get; set; }
            public string EMPID { get; set; }
            public string SELECTEDDATE { get; set; }
            public string PreviousEndTime { get; set; }
            public string NextStartTime { get; set; }
        }
        public class PreviousNextv2
        {
            public string PreviousEndTime { get; set; }
            public string NextStartTime { get; set; }

        }
        public class TWHrs
        {
            public string CN { get; set; }
            public string EMPID { get; set; }
            public string STARTDATE { get; set; }
            public string ENDDATE { get; set; }
        }

        public class GetTotalWorkedHours
        {
            public string TWH { get; set; }
        }
        public class ListTotalWorkedHours_Result
        {
            public List<GetTotalWorkedHours> GetTotalWorkedHours;
            public string myreturn;
        }
        public class BioRaw
        {
            public string CN { get; set; }
            public string DT1 { get; set; }
            public string DT2 { get; set; }
        }
        public class BioRawData
        {
            public string RowID { get; set; }
            public string SerialNumber { get; set; }
            public string RecordDT { get; set; }
            public string LogType { get; set; }
            public string VerifyMode { get; set; }
            public string AccessNumber { get; set; }
        }


        #region new timekeeping
        public class ListScheduleParam
        {
            public string CN { get; set; }
            public string EMPID { get; set; }
            public string Start_Date { get; set; }
            public string End_Date { get; set; }
            public string SchedDate { get; set; }
            public string ShiftNum { get; set; }
            public string SeriesID { get; set; }
        }
        public class ListScheduleView
        {
            public string EmpID { get; set; }
            public string SeriesID { get; set; }
            public string SchedDate { get; set; }
            public string HoursWorked { get; set; }
            public string SchedIN { get; set; }
            public string SchedOUT { get; set; }
            public string ApprovedWorked { get; set; }
            public string dow { get; set; }
            public string Absent { get; set; }
            public string ScheduleType { get; set; }
            public string ShiftNum { get; set; }
            public string Schedule { get; set; }
            public string TotalTime { get; set; }
            public string SchedStatus { get; set; }
            public string TempID { get; set; }
        }
        public class TKSchedRequest
        {
            public string EMPID { get; set; }
            public string REQUESTDATE { get; set; }
            public string SCHEDULETYPE { get; set; }
            public string SHIFTTYPE { get; set; }
            public string SHIFTVAL { get; set; }
            public string BREAKTYPE { get; set; }
            public string LUNCH { get; set; }
            public string FIRSTBREAK { get; set; }
            public string SECONDBREAK { get; set; }
            public string ENDBREAK { get; set; }
            public string MCSCHEDID { get; set; }
            public string ISDELETED { get; set; }
            public string SCHEDSTATUS { get; set; }
        }
        public class TodoApproveDenySchedRequest
        {
            public string UID { get; set; }
            public string STATUSTYPE { get; set; }
        }

        public class TodoApproveDenySchedRequestUpdate
        {
            public List<TodoApproveDenySchedRequest> TODOSCHEDREQUESTLIST;
            public string CN { get; set; }
        }
        public class ListTodoScheduleView
        {
            public string SERIESID { get; set; }
            public string EMPID { get; set; }
            public string EMPNAME { get; set; }
            public string ISMANAGERNAME { get; set; }
            public string REQUESTDATE { get; set; }
            public string DAY { get; set; }
            public string CURRENTSCHEDULETYPE { get; set; }
            public string CURRENTSHIFT { get; set; }
            public string CURRENTSTARTTIME { get; set; }
            public string CURRENTENDTIME { get; set; }
            public string NEWSCHEDULETYPE { get; set; }
            public string NEWSHIFT { get; set; }
            public string NEWSTARTTIME { get; set; }
            public string NEWENDTIME { get; set; }
            public string BREAKTIME { get; set; }
            public string LUNCH { get; set; }
            public string FIRSTBREAK { get; set; }
            public string SECONDBREAK { get; set; }
            public string ENDBREAK { get; set; }

        }
        public class ListTodoScheduleView_Result
        {
            public List<ListTodoScheduleView> ListTodoScheduleView;
            public string myreturn;
        }
        public class TKSchedRequestInsert
        {
            public List<TKSchedRequest> SCHEDREQUESTLIST;
            public string CN { get; set; }
        }
        public class ListTKGetSettings
        {
            public string IsAutoApproved { get; set; }
            public string Approvers { get; set; }
            public string IsEscalateApprover { get; set; }
            public string TotalHours { get; set; }
            public string WithinDays { get; set; }
            public string WithinNum { get; set; }
            public string ID { get; set; }

        }
        public class ListTKGetSettings_Result
        {
            public List<ListTKGetSettings> ListTKGetSettings;
            public string myreturn;
        }
        public class ListScheduleView_Result
        {
            public List<ListScheduleView> ListScheduleView;
            public string myreturn;
        }
        public class ListTimesheetView
        {
            public string SeriesID { get; set; }
            public string EmpID { get; set; }
            public string Start_Time { get; set; }
            public string End_Time { get; set; }
            public string ReasonDesc { get; set; }
            public string SchedDate { get; set; }
            public string TotalTimeMin { get; set; }
            public string Activity_Status { get; set; }
            public string AddedBy { get; set; }
            public string ApproverComment { get; set; }
            public string FilePath { get; set; }
            public string ReasonID { get; set; }
            public string EmpComment { get; set; }

        }
        public class ListTimesheetView_Result
        {
            public List<ListTimesheetView> ListTimesheetView;
            public string myreturn;
        }

        #endregion

        public class emprequest
        {
            public string empid { get; set; }
            public string timefiled { get; set; }
            public int ReasonID { get; set; }
            public string scheddate { get; set; }
            public string starttime { get; set; }
            public string endtime { get; set; }
            public string cn { get; set; }
        }
        public class GetLeaveType
        {
            public string MOBILE { get; set; }
            public string WEB { get; set; }
            public string ALLOWUSERTRACK { get; set; }

            public string SERIESID { get; set; }
        }
        public class LeaveTypeList
        {
            public List<GetLeaveType> LeaveType;
            public string myreturn;
        }
        public class TimeInOut
        {
            public string CN { get; set; }
            public string EmpID { get; set; }
        }
        public class Dailyv3
        {
            public string SchedDate { get; set; }
            public string EmpID { get; set; }
            public string CN { get; set; }
            public string SeriesID { get; set; }
        }

        public class OTWvalidation
        {
            public string empid { get; set; }
            public string selecteddate { get; set; }
            public string start { get; set; }
            public string end { get; set; }
        }
        public class SegmentRequestor
        {
            public string CN { get; set; }
            public String[] SegmentID { get; set; }
        }
        public class countingparameters
        {
            public string CN { get; set; }
            public string empid { get; set; }
        }
    }
}