﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace illimitadoWepAPI.Models.WorkforceModel
{
    public class WorkForceModel
    {
      
        public class GetDatabase
        {
            public string ClientName { get; set; }
            public string ClientLink { get; set; }
        }
        public class autoLogin
        {
            public string companyname { get; set; }
            public string id { get; set; }
            public string UID { get; set; }
            public string[] EmpID { get; set; }
        }
        public class SearchFilterParams
        {
            public string company { get; set; }
        }
        public class viewLogs
        {
            public string company { get; set; }
            public string curDate { get; set; }
            public string maxDate { get; set; }
            public string empID { get; set; }
        }
        public class GetLiveMonitorList
        {
            public string ReasonDesc { get; set; }
            public string ReasonID { get; set; }
            public string Count { get; set; }

        }
        public class GetViewLogsList
        {
            public string Date { get; set; }
            public string Day { get; set; }
            public string ScheduleType { get; set; }
            public string Shift { get; set; }
            public string Schedule { get; set; }
            public string StartTime { get; set; }
            public string EndTime { get; set; }
            public string ReasonID { get; set; }
            public string Activity { get; set; }
            public string TotalWorkedHours { get; set; }
            public string ApproverStatus { get; set; }
            public string UserStatus { get; set; }
        }
        public class AutoLoginList
        {
            public string company { get; set; }
            public string id { get; set; }
            public string status { get; set; }

        }
        public class manualLogin
        {
            public string companyname { get; set; }
            public string username { get; set; }
            public string password { get; set; }
        }
        public class FilterName
        {
            public string EmpID { get; set; }
            public string EmployeeName { get; set; }
            public string MngrID { get; set; }
            public string EmpLevel { get; set; }
            public string BusinessSegment { get; set; }
            public string BusinessUnit { get; set; }

        }
        public class FilterLevel
        {
            public string EmpLevel { get; set; }
            public string BusinessUnit { get; set; }
        }
        public class ISLevel
        {
            public string ImmediateSupervisor { get; set; }
            public string EmpName { get; set; }
            public string EmpLevel { get; set; }
            public string BusinessUnit { get; set; }
            public string BusinessSegment { get; set; }
        }

        public class FilterBusinessUnit
        {
            public string BusinessUnit { get; set; }
            public string BusinessSegment { get; set; }
          
        }
        public class FilterBusinessSegment
        {
            public string BusinessSegment { get; set; }
            
        }
        public class SearchPanelParams
        {
            public string company { get; set; }
            public String[] EmpID { get; set; }
            public String[] EmpName { get; set; }
            public String[] EmpLevel { get; set; }
            public String[] ISLIst { get; set; }
            public String[] BusinessUnit { get; set; }
            public String[] BusinessSegment { get; set; }
        }
        public class ClassSearchPanel
        {
            //FOR THE RESULT
            public List<SearchPanelReturns> returnEmployeeID;
            public string myreturn;
        }

        public class SearchPanelReturns
        {
            public string SeriesID { get; set; }
            public string EmpID { get; set; }
            public string Scheddate { get; set; }
            public string Schedule { get; set; }
            public string SeriesIDb { get; set; }
            public string EmpIDb { get; set; }
            public string Start_Time { get; set; }
            public string End_Time { get; set; }
            public string Activity_Status { get; set; }
            public string ReasonID { get; set; }

        }

        public class SearchPanelParamsV2
        {
            public string company { get; set; }
            public String[] EmpID { get; set; }
            public String[] EmpName { get; set; }
        }

        public class ClassSearchPanelV2
        {
            //FOR THE RESULT
            public List<SearchPanelReturnsV2> returnEmployeeID;
            public string myreturn;
        }

        public class SearchPanelReturnsV2
        {
            public string EmpID { get; set; }
            public string EmpName { get; set; }
            public string ImmediateSupervisor { get; set; }
            public string ScheduleType { get; set; }
            public string SchedIN { get; set; }
            public string SchedOUT { get; set; }
            public string Status { get; set; }
            public string Status2 { get; set; }
            public string Status1 { get; set; }
            public string BusinessSegment { get; set; }
            public string BusinessUnit { get; set; }
            public string EmpLevel { get; set; }
            public string Activity_Status { get; set; }
            public string ReasonID { get; set; }
            public string EmpStatus { get; set; }
            public string StartTime { get; set; }
        }
        public class EmployeeStatus
        {
            public string CN { get; set; }
            public String[] EmpID { get; set; }
            public string EmpStatus { get; set; }
        }

        public class RetrieveLabel
        {
            public string CN { get; set; }
        }
    }
}