﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace illimitadoWepAPI.Models
{
    public class Holiday
    {
        public string HolidayName { get; set; }
        public string Type { get; set; }
        public string HolidayDate { get; set; }

    }


    public class GetHolidayResult
    {
        public List<Holiday> Holidays;
        public string myreturn;
    }

}