﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using illimitadoWepAPI.SchedulerPortalClass;
using illimitadoWepAPI.Models.ScheduleDemoModels;
using illimitadoWepAPI.Controllers.SchedulerDemo;

namespace illimitadoWepAPI.Models.ScheduleDemoModels
{
    public class SchedUploadModel
    {
    }
    public class businessSegments
    {
        public string businessSegment { get; set; }
    }

    public class businessUnits
    {
        public string businessUnit { get; set; }
    }

    public class employees
    {
        public string empName { get; set; }
        public string empUID { get; set; }
    }
    public class emp
    {
        public string empUID { get; set; }
    }
    public class srch
    {
        public String[] UID { get; set; }
        public String[] Department { get; set; }
        public String[] SubDepartment { get; set; }
        public String SchedFrom { get; set; }
        public String SchedTo { get; set; }
        public string ClientID { get; set; }
    }

    public class schedule
    {
        public string SeriesID { get; set; }
        public string Department { get; set; }
        public string SubDepartment { get; set; }
        public string EmpName { get; set; }
        public string UserID { get; set; }
        public string CutOff_From { get; set; }
        public string CutOff_To { get; set; }
        public string Date { get; set; }
        public string Day { get; set; }
        public string Schedule { get; set; }
        public string SchedIN { get; set; }
        public string Break1dsply { get; set; }
        public string Lunch { get; set; }
        public string Break2dsply { get; set; }
        public string Break3dsply { get; set; }
        public string SchedOUT { get; set; }
        public string SchedDate { get; set; }
        public string LunchEnd { get; set; }
        public string Break1Minutes { get; set; }
        public string Break2Minutes { get; set; }
        public string Break3Minutes { get; set; }
        public string MonthName { get; set; }
        public string BreakType { get; set; }
    }

    public class emparray
    {
        public string empid { get; set; }
    }
    //public class schedule
    //{
    //    public string SeriesID { get; set; }
    //    public string Department { get; set; }
    //    public string SubDepartment { get; set; }
    //    public string EmpName { get; set; }
    //    public string UserID { get; set; }
    //    public string CutOff_From { get; set; }
    //    public string CutOff_To { get; set; }
    //    public string Date { get; set; }
    //    public string Day { get; set; }
    //    public string Schedule { get; set; }
    //    public string SchedIN { get; set; }
    //    public string Break1dsply { get; set; }
    //    public string Lunch { get; set; }
    //    public string Break2dsply { get; set; }
    //    public string Break3dsply { get; set; }
    //    public string SchedOUT { get; set; }
    //    public string SchedDate { get; set; }
    //    public string LunchEnd { get; set; }
    //    public string Break1Minutes { get; set; }
    //    public string Break2Minutes { get; set; }
    //    public string Break3Minutes { get; set; }
    //}

    public class subjectlist
    {
        public string _SID { get; set; }
        public string _Subject { get; set; }
        public bool _Status { get; set; }
        public string _DefaultName { get; set; }
        public string _ModifiedBy { get; set; }
        public string _ModifiedDate { get; set; }
        public bool _Edit { get; set; }
        public string ClientID { get; set; }
    }
    public class subsubjectlist
    {
        public string _RowID { get; set; }
        public string _SID { get; set; }
        public string _Subject { get; set; }
        public bool _Status { get; set; }
        public string _ModifiedBy { get; set; }
        public string _ModifiedDate { get; set; }
        public bool _Edit { get; set; }
        public string ClientID { get; set; }
    }
    public class subsubjects
    {
        public List<subsubjectlist> subsubjectlist;
        public string _myreturn { get; set; }
    }
    public class subjects
    {
        public List<subjectlist> subjectlist;
        public string _myreturn { get; set; }
    }
    public class SDGetNationality
    {
        public string Nationality { get; set; }
    }
    public class SDGetNationalityList
    {
        public List<SDGetNationality> Displaynationality;
        public string myreturn;
    }
}