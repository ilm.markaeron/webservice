﻿using illimitadoWepAPI.Models;
using illimitadoWepAPI.MyClass;
using Microsoft.ProjectOxford.Face;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Hosting;
using System.Web.Http;
using System.Data;
using System.Configuration;
using System.Reflection;
using Microsoft.ProjectOxford.Face.Contract;

namespace illimitadoWepAPI.Controllers
{
    public class SelfieRegistration2Controller : ApiController
    {

        [HttpPost]
        [Route("FaceAPIDetectv3")]
        public async Task<HttpResponseMessage> DetekMukaaa(profilepicmo SavePic)
        {
            var client = new HttpClient();
            var queryString = HttpUtility.ParseQueryString(string.Empty);

            client.DefaultRequestHeaders.Add("Ocp-Apim-Subscription-Key", "bc6cf65c8dab414f92383ed4830341dd");

            queryString["returnFaceId"] = "true";
            queryString["returnFaceLandmarks"] = "false";
            //queryString["returnFaceAttributes"] = "&returnFaceAttributes=age,gender,headPose,smile,facialHair,glasses," +
            //    "emotion,hair,makeup,occlusion,accessories,blur,exposure,noise";
            var uri = "https://southeastasia.api.cognitive.microsoft.com/face/v1.0/detect?" + queryString;

            HttpResponseMessage response;

            byte[] Photo = Convert.FromBase64String(SavePic.b64img);
            var path = HttpContext.Current.Server.MapPath("~/ProfilePhotos/" + SavePic.CN.ToLower() + "/" + "");
            var folder = Path.Combine(path, SavePic.NTID);
            Directory.CreateDirectory(folder);
            MemoryStream stream = new MemoryStream();

            using (stream)
            {

                File.WriteAllBytes(folder + "/" + SavePic.NTID + "_" + SavePic.num + ".png", Photo);
                using (FileStream fileStream = new FileStream(folder + "/" + SavePic.NTID + "_" + SavePic.num + ".png", FileMode.Open, FileAccess.Read))
                {
                    BinaryReader binaryReader = new BinaryReader(fileStream);
                    // return binaryReader.ReadBytes((int)fileStream.Length);
                    using (var content = new ByteArrayContent(binaryReader.ReadBytes((int)fileStream.Length)))
                    {
                        content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
                        response = await client.PostAsync(uri, content);
                        return response;
                    }
                }
            }
        }


        [HttpPost]
        [Route("FaceAPIDetectv4")]
        public async Task<HttpResponseMessage> DetekMukaaaa(profilepicmo SavePic)
        {
            var client = new HttpClient();
            var queryString = HttpUtility.ParseQueryString(string.Empty);

            client.DefaultRequestHeaders.Add("Ocp-Apim-Subscription-Key", "bc6cf65c8dab414f92383ed4830341dd");

            queryString["returnFaceId"] = "true";
            queryString["returnFaceLandmarks"] = "false";
            //queryString["returnFaceAttributes"] = "&returnFaceAttributes=age,gender,headPose,smile,facialHair,glasses," +
            //    "emotion,hair,makeup,occlusion,accessories,blur,exposure,noise";
            var uri = "https://southeastasia.api.cognitive.microsoft.com/face/v1.0/detect?" + queryString;

            HttpResponseMessage response;

            byte[] Photo = Convert.FromBase64String(SavePic.b64img);
            var path = HttpContext.Current.Server.MapPath("~/ProfilePhotos/" + SavePic.CN.ToLower() + "/" + "");
            var folder = Path.Combine(path, SavePic.NTID);
            Directory.CreateDirectory(folder);
            MemoryStream stream = new MemoryStream();
            Stream inputStream = new MemoryStream(Photo);
            string savedConfidThres = "";
            string empID = SavePic.NTID;
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = SavePic.personID;
            GetDB2(GDB);
            try
            {
                FaceServiceClient faceServiceClient = new FaceServiceClient("bc6cf65c8dab414f92383ed4830341dd", "https://southeastasia.api.cognitive.microsoft.com/face/v1.0");
                //string personGroupId = "ilmdevs";
                string personGroupId = "";
                //string personGroupId = "ilmdevs";
                if (SavePic.personID.ToLower() == "lomedagroupofcompanies")
                {
                    personGroupId = "hondacarsbatangas";
                }
                else
                {
                    personGroupId = SavePic.personID;
                }
                //var thresholdHolder = SavePic.thres;
                // empID = httpRequest.Form["EmpID"].ToString();
                // float thresh = Convert.ToInt32(thresholdHolder);
                float thresh = float.Parse(SavePic.thres);
                var faces = await faceServiceClient.DetectAsync(inputStream);
                var faceIds = faces.Select(face => face.FaceId).ToArray();

                try
                {
                    var results = await faceServiceClient.IdentifyAsync(personGroupId, faceIds, thresh, 1);
                    savedConfidThres = results.FirstOrDefault().Candidates.FirstOrDefault().Confidence.ToString();
                    if (results.Count() > 1)
                    {
                        return Request.CreateResponse(HttpStatusCode.BadRequest, "Multiple faces detected!");
                        //return "Multiple faces detected!";
                        //return Request.CreateResponse(HttpStatusCode.NotAcceptable);
                    }
                    else
                    {
                        var candidateId = results.FirstOrDefault().Candidates.FirstOrDefault().PersonId;
                        var person = await faceServiceClient.GetPersonAsync(personGroupId, candidateId);
                        var confidthres = results.FirstOrDefault().Candidates.FirstOrDefault().Confidence;
                        Connection con = new Connection();
                        con.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.NVarChar, Value = (person.PersonId.ToString() == null || person.PersonId.ToString() == "") ? "null" : person.PersonId.ToString() });
                        con.myparameters.Add(new myParameters { ParameterName = "@ConfidLevel", mytype = SqlDbType.NVarChar, Value = (confidthres.ToString() == null || confidthres.ToString() == "") ? "0%" : confidthres.ToString() });
                        con.ExecuteNonQuery("sp_InsertConfidenceLevelThreshold");
                        return Request.CreateResponse(HttpStatusCode.Accepted, person.PersonId);
                        //return person.Name;
                        //return Request.CreateResponse(HttpStatusCode.Accepted);
                    }
                }
                catch
                {
                    Connection con = new Connection();
                    con.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.NVarChar, Value = (empID == null || empID == "") ? "null" : empID });
                    con.myparameters.Add(new myParameters { ParameterName = "@ConfidLevel", mytype = SqlDbType.NVarChar, Value = (savedConfidThres == null || savedConfidThres == "") ? "0%" : savedConfidThres });
                    con.ExecuteNonQuery("sp_InsertConfidenceLevelThreshold");
                    return Request.CreateResponse(HttpStatusCode.NotAcceptable, "Error");
                    //return "Error";
                    //return Request.CreateResponse(HttpStatusCode.NotAcceptable);
                }
            }
            catch
            {
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.NVarChar, Value = (empID == null || empID == "") ? "null" : empID });
                con.myparameters.Add(new myParameters { ParameterName = "@ConfidLevel", mytype = SqlDbType.NVarChar, Value = (savedConfidThres == null || savedConfidThres == "") ? "0%" : savedConfidThres });
                con.ExecuteNonQuery("sp_InsertConfidenceLevelThreshold");
                return Request.CreateResponse(HttpStatusCode.NotAcceptable, "Error");
                //return "Error";
                //return Request.CreateResponse(HttpStatusCode.NotAcceptable);
            }
        }

        [HttpPost]
        [Route("FaceAPIDetectv2")]
        public async Task<HttpResponseMessage> DetekMukaa(profilepicmo SavePic)
        {
            var client = new HttpClient();
            var queryString = HttpUtility.ParseQueryString(string.Empty);

            client.DefaultRequestHeaders.Add("Ocp-Apim-Subscription-Key", "bc6cf65c8dab414f92383ed4830341dd");

            queryString["returnFaceId"] = "true";
            queryString["returnFaceLandmarks"] = "false";
            //queryString["returnFaceAttributes"] = "&returnFaceAttributes=age,gender,headPose,smile,facialHair,glasses," +
            //    "emotion,hair,makeup,occlusion,accessories,blur,exposure,noise";
            var uri = "https://southeastasia.api.cognitive.microsoft.com/face/v1.0/detect?" + queryString;

            HttpResponseMessage response;

            byte[] Photo = Convert.FromBase64String(SavePic.b64img);
            var path = HttpContext.Current.Server.MapPath("~/ProfilePhotos/");
            var folder = Path.Combine(path, SavePic.NTID);
            Directory.CreateDirectory(folder);
            MemoryStream stream = new MemoryStream();
            Stream inputStream = new MemoryStream(Photo);
            string savedConfidThres = "";
            string empID = SavePic.NTID;
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = SavePic.personID;
            GetDB2(GDB);
            try
            {
                FaceServiceClient faceServiceClient = new FaceServiceClient("bc6cf65c8dab414f92383ed4830341dd", "https://southeastasia.api.cognitive.microsoft.com/face/v1.0");
                string personGroupId = "";
                //string personGroupId = "ilmdevs";
                if (SavePic.personID.ToLower() == "lomedagroupofcompanies")
                {
                    personGroupId = "hondacarsbatangas";
                }
                else
                {
                    personGroupId = SavePic.personID;
                }
                //var thresholdHolder = SavePic.thres;
                // empID = httpRequest.Form["EmpID"].ToString();
                // float thresh = Convert.ToInt32(thresholdHolder);
                float thresh = float.Parse(SavePic.thres);
                var faces = await faceServiceClient.DetectAsync(inputStream);
                var faceIds = faces.Select(face => face.FaceId).ToArray();

                try
                {
                    var results = await faceServiceClient.IdentifyAsync(personGroupId, faceIds, thresh, 1);
                    savedConfidThres = results.FirstOrDefault().Candidates.FirstOrDefault().Confidence.ToString();
                    if (results.Count() > 1)
                    {
                        return Request.CreateResponse(HttpStatusCode.BadRequest, "Multiple faces detected!");
                        //return "Multiple faces detected!";
                        //return Request.CreateResponse(HttpStatusCode.NotAcceptable);
                    }
                    else
                    {
                        var candidateId = results.FirstOrDefault().Candidates.FirstOrDefault().PersonId;
                        var person = await faceServiceClient.GetPersonAsync(personGroupId, candidateId);
                        var confidthres = results.FirstOrDefault().Candidates.FirstOrDefault().Confidence;
                        Connection con = new Connection();
                        con.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.NVarChar, Value = (person.PersonId.ToString() == null || person.PersonId.ToString() == "") ? "null" : person.PersonId.ToString() });
                        con.myparameters.Add(new myParameters { ParameterName = "@ConfidLevel", mytype = SqlDbType.NVarChar, Value = (confidthres.ToString() == null || confidthres.ToString() == "") ? "0%" : confidthres.ToString() });
                        con.ExecuteNonQuery("sp_InsertConfidenceLevelThreshold");
                        return Request.CreateResponse(HttpStatusCode.Accepted, person.PersonId);
                        //return person.Name;
                        //return Request.CreateResponse(HttpStatusCode.Accepted);
                    }
                }
                catch (Exception ex)
                {
                    var ar = ex.ToString();
                    Connection con = new Connection();
                    con.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.NVarChar, Value = (empID == null || empID == "") ? "null" : empID });
                    con.myparameters.Add(new myParameters { ParameterName = "@ConfidLevel", mytype = SqlDbType.NVarChar, Value = (savedConfidThres == null || savedConfidThres == "") ? "0%" : savedConfidThres });
                    con.ExecuteNonQuery("sp_InsertConfidenceLevelThreshold");
                    return Request.CreateResponse(HttpStatusCode.NotAcceptable, "Error");
                    //return "Error";
                    //return Request.CreateResponse(HttpStatusCode.NotAcceptable);
                }
            }
            catch
            {
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.NVarChar, Value = (empID == null || empID == "") ? "null" : empID });
                con.myparameters.Add(new myParameters { ParameterName = "@ConfidLevel", mytype = SqlDbType.NVarChar, Value = (savedConfidThres == null || savedConfidThres == "") ? "0%" : savedConfidThres });
                con.ExecuteNonQuery("sp_InsertConfidenceLevelThreshold");
                return Request.CreateResponse(HttpStatusCode.NotAcceptable, "Error");
                //return "Error";
                //return Request.CreateResponse(HttpStatusCode.NotAcceptable);
            }
        }
        [HttpPost]
        [Route("DetectFaceAlternative")]
        public async Task<HttpResponseMessage> DetectFace()
        {
            
            HttpRequest httpRequest = HttpContext.Current.Request;
            BinaryReader binaryReader = new BinaryReader(httpRequest.Files[0].InputStream);
            byte[] Photo = binaryReader.ReadBytes(httpRequest.Files[0].ContentLength);
            //MemoryStream inputStream = new MemoryStream(Photo);
            Stream inputStream = new MemoryStream(Photo);

            try
            {
                FaceServiceClient faceServiceClient = new FaceServiceClient("bc6cf65c8dab414f92383ed4830341dd", "https://southeastasia.api.cognitive.microsoft.com/face/v1.0");
                //string personGroupId = "ilmdevs";
                string personGroupId = "";
                //string personGroupId = "ilmdevs";
                if (httpRequest.Form["PersonGroupID"].ToLower() == "lomedagroupofcompanies")
                {
                    personGroupId = "hondacarsbatangas";
                }
                else
                {
                    personGroupId = httpRequest.Form["PersonGroupID"].ToLower();
                }
                var faces = await faceServiceClient.DetectAsync(inputStream);
                var faceIds = faces.Select(face => face.FaceId).ToArray();

                try
                {
                    var results = await faceServiceClient.IdentifyAsync(personGroupId, faceIds);

                    if (results.Count() > 1)
                    {
                        return Request.CreateResponse(HttpStatusCode.BadRequest, "Multiple faces detected!");
                        //return "Multiple faces detected!";
                        //return Request.CreateResponse(HttpStatusCode.NotAcceptable);
                    }
                    else
                    {
                        var candidateId = results.FirstOrDefault().Candidates.FirstOrDefault().PersonId;
                        var person = await faceServiceClient.GetPersonAsync(personGroupId, candidateId);
                        var confidthres = results.FirstOrDefault().Candidates.FirstOrDefault().Confidence;
                        Connection con = new Connection();
                        con.myparameters.Add(new myParameters { ParameterName = "@FaceID", mytype = SqlDbType.NVarChar, Value = person.PersonId.ToString() });
                        con.myparameters.Add(new myParameters { ParameterName = "@Confid", mytype = SqlDbType.NVarChar, Value = confidthres.ToString() });
                        con.ExecuteNonQuery("sp_InsertConfidThresh");
                        return Request.CreateResponse(HttpStatusCode.Accepted, person.PersonId);
                        //return person.Name;
                        //return Request.CreateResponse(HttpStatusCode.Accepted);
                    }
                }
                catch
                {
                    return Request.CreateResponse(HttpStatusCode.NotAcceptable, "Error");
                    //return "Error";
                    //return Request.CreateResponse(HttpStatusCode.NotAcceptable);
                }
            }
            catch
            {
                return Request.CreateResponse(HttpStatusCode.NotAcceptable, "Error");
                //return "Error";
                //return Request.CreateResponse(HttpStatusCode.NotAcceptable);
            }
        }



        [HttpPost]
        [Route("RegisterFaceAlternative")]
        public async Task<string> RegisterFace(SelfieRegistration selfieReg)
        {
            string name = selfieReg.Name;
            string ntid = selfieReg.NTID;
            
            try
            {
                FaceServiceClient faceServiceClient = new FaceServiceClient("bc6cf65c8dab414f92383ed4830341dd", "https://southeastasia.api.cognitive.microsoft.com/face/v1.0");
                //var person = await faceServiceClient.CreatePersonAsync("ilmdevs", name);
                string personGroupId = "";
                //string personGroupId = "ilmdevs";
                if (selfieReg.PersonGroupID.ToLower() == "lomedagroupofcompanies")
                {
                    personGroupId = "hondacarsbatangas";
                }
                else
                {
                    personGroupId = selfieReg.PersonGroupID.ToLower();
                }
                var person = await faceServiceClient.CreatePersonAsync(personGroupId, name);
                Guid persondID = person.PersonId;

                //var path = HttpContext.Current.Server.MapPath("~/Profile/");
                var path = HttpContext.Current.Server.MapPath("~/ProfilePhotos/");
                //var path = "http://uat-vm.southeastasia.cloudapp.azure.com:81/hrms_test/ProfilePhotos";
                var folder = Path.Combine(path, ntid);

                Stream stream1 = File.OpenRead(folder + "/" + ntid + "_pic1.png");
                Stream stream2 = File.OpenRead(folder + "/" + ntid + "_pic2.png");

                //await faceServiceClient.AddPersonFaceAsync("ilmdevs", persondID, stream1);
                await faceServiceClient.AddPersonFaceAsync(selfieReg.PersonGroupID.ToLower(), persondID, stream1);
                //await faceServiceClient.AddPersonFaceAsync("ilmdevs", persondID, stream2);
                await faceServiceClient.AddPersonFaceAsync(selfieReg.PersonGroupID.ToLower(), persondID, stream2);

                //await faceServiceClient.TrainPersonGroupAsync("ilmdevs");
                await faceServiceClient.TrainPersonGroupAsync(selfieReg.PersonGroupID.ToLower());

                string guidStr = persondID.ToString("D");

                return guidStr;
            }
            catch
            {
                return "null";
            }
        }
        [HttpPost]
        [Route("RegisterFaceAlternativev2")]
        public async Task<string> RegisterFacev2(SelfieRegistration selfieReg)
        {
            string name = selfieReg.Name;
            string ntid = selfieReg.NTID;

            try
            {
                FaceServiceClient faceServiceClient = new FaceServiceClient("bc6cf65c8dab414f92383ed4830341dd", "https://southeastasia.api.cognitive.microsoft.com/face/v1.0");
                //var person = await faceServiceClient.CreatePersonAsync("ilmdevs", name);
                string personGroupId = "";
                //string personGroupId = "ilmdevs";
                if (selfieReg.PersonGroupID.ToLower() == "lomedagroupofcompanies")
                {
                    personGroupId = "hondacarsbatangas";
                }
                else
                {
                    personGroupId = selfieReg.PersonGroupID.ToLower();
                }
                var person = await faceServiceClient.CreatePersonAsync(personGroupId, name);
                Guid persondID = person.PersonId;

                //var path = HttpContext.Current.Server.MapPath("~/Profile/");
                var path = HttpContext.Current.Server.MapPath("~/ProfilePhotos/" + selfieReg.PersonGroupID.ToLower() + "/");
                //var path = "http://uat-vm.southeastasia.cloudapp.azure.com:81/hrms_test/ProfilePhotos";
                var folder = Path.Combine(path, ntid);

                Stream stream1 = File.OpenRead(folder + "/" + ntid + "_pic1.png");
                Stream stream2 = File.OpenRead(folder + "/" + ntid + "_pic2.png");

                //await faceServiceClient.AddPersonFaceAsync("ilmdevs", persondID, stream1);
                await faceServiceClient.AddPersonFaceAsync(selfieReg.PersonGroupID.ToLower(), persondID, stream1);
                //await faceServiceClient.AddPersonFaceAsync("ilmdevs", persondID, stream2);
                await faceServiceClient.AddPersonFaceAsync(selfieReg.PersonGroupID.ToLower(), persondID, stream2);

                //await faceServiceClient.TrainPersonGroupAsync("ilmdevs");
                await faceServiceClient.TrainPersonGroupAsync(selfieReg.PersonGroupID.ToLower());

                string guidStr = persondID.ToString("D");

                return guidStr;
            }
            catch
            {
                return "null";
            }
        }


        [HttpPost]
        [Route("DeletePersonAlternative")]
        public async Task<string> DeletePerson(DeletePerson _deletePerson)
        {
            try
            {
                FaceServiceClient faceServiceClient = new FaceServiceClient("bc6cf65c8dab414f92383ed4830341dd", "https://southeastasia.api.cognitive.microsoft.com/face/v1.0");
                //await faceServiceClient.DeletePersonAsync("ilmdevs", _deletePerson.PersonID);
                await faceServiceClient.DeletePersonAsync(_deletePerson.PersonGroupID.ToLower(), _deletePerson.PersonID);
                return "";
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        [HttpPost]
        [Route("DisplayActivityByRangeAlternative2")]
        public ListAllActivityResult2 DisplayActivityByRangeAlternative(DailyParameters2 DailyParameters)
        {
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = DailyParameters.companyname;
            GetDB2(GDB);
            ListAllActivityResult2 GetActivityResult = new ListAllActivityResult2();
            //try
            //{
            Connection Connection = new Connection();
            List<ListAllActivityByRange2> AllActivity = new List<ListAllActivityByRange2>();
            DataTable dtDAILY = new DataTable();
            //BasicFunction BasicFunction = new BasicFunction();
            Connection.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.NVarChar, Value = DailyParameters.NTID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@SchedDate1", mytype = SqlDbType.NVarChar, Value = DailyParameters.Start_Date });
            Connection.myparameters.Add(new myParameters { ParameterName = "@SchedDate2", mytype = SqlDbType.NVarChar, Value = DailyParameters.End_Date });
            dtDAILY = Connection.GetDataTable("sp_GetEmployeeActivity_Mobile_Alternative");
            //CultureInfo provider = CultureInfo.InvariantCulture;
            if (dtDAILY.Rows.Count > 0)
            {
                foreach (DataRow row in dtDAILY.Rows)
                {
                    ListAllActivityByRange2 ListAllActivity = new ListAllActivityByRange2
                    {
                        EmpID = row["EMPID"].ToString(),
                        StartTIme = row["Schedule"].ToString() == "LEAVE" || row["Schedule"].ToString() == "RD" ? row["Schedule"].ToString() : row["Start_Time"].ToString(),
                        StartEnd = row["Schedule"].ToString() == "LEAVE" || row["Schedule"].ToString() == "RD" ? row["Schedule"].ToString() : row["End_Time"].ToString(),
                        //StartTIme = row["Start_Time"].ToString(),
                        //StartEnd = row["End_Time"].ToString(),
                        ReasonID = row["ReasonID"].ToString(),
                        ReasonDesc = row["ReasonDesc"].ToString(),
                        IsPaid = row["IsPaid"].ToString(),
                        SchedDate = row["SchedDate"].ToString(),
                        ActivityStatus = row["Activity_Status"].ToString(),
                        HourWork = row["TotalTimeMin"].ToString(),
                        SeriesID = row["SeriesID"].ToString(),
                        //SchedDateStatus = row["Activity_Tag"].ToString() + "/" + row["ApprovedWork"].ToString()
                        SchedDateStatus = row["ApprovedWork"].ToString()
                    };
                    AllActivity.Add(ListAllActivity);
                }
                GetActivityResult.ListAllActivity = AllActivity;
                GetActivityResult.myreturn = "Success";
            }
            else
            {
                GetActivityResult.myreturn = "No Data Available";
            }

            return GetActivityResult;
        }
        public static string GetDB2(GetDatabase GetDatabase)
        {
            // com.connectionstring = ConfigurationManager.ConnectionStrings["connectionstring"].ConnectionString.ToString();
            var settings = ConfigurationManager.ConnectionStrings["connectionstring"];
            var fi = typeof(ConfigurationElement).GetField(
                          "_bReadOnly",
                          BindingFlags.Instance | BindingFlags.NonPublic);
            fi.SetValue(settings, false);
            // settings.ConnectionString = "Data Source=tcp:ilmuat.c3srcolvclfy.ap-southeast-1.rds.amazonaws.com,1400;Database=ILM_Main;User Id=devuatuser;Password=ILMV3ry53cur3dMGTpa55w0rd2020;";
            settings.ConnectionString = "Data Source=livedb-ilm.c3srcolvclfy.ap-southeast-1.rds.amazonaws.com,1412;Database=ILM_Main;User Id=ILM_LiveDBUser;Password=ILMV3ry53cur3dMGTpa55w0rd2020;";
            //settings.ConnectionString = "Data Source=192.168.2.4,1444;Database=ILM_Local;User Id=DevUser;Password=securedmgt@1230;";

            if (GetDatabase.ClientName == "ilmvivere" 
                || GetDatabase.ClientName == "ilmvisiontv"
                || GetDatabase.ClientName == "ilmayhpi"
                || GetDatabase.ClientName == "ilmbataan2020"
                || GetDatabase.ClientName == "ilmbataan2020-samal")
            {

                Connection MyConnection = new Connection();
                MyConnection.myparameters.Add(new myParameters { ParameterName = "@ClientName", mytype = SqlDbType.NVarChar, Value = GetDatabase.ClientName.Substring(3, GetDatabase.ClientName.Length - 3) });
                DataRow Mydr;
                Mydr = MyConnection.GetSingleRow("sp_CheckClientDB");
                GetDatabase Mydab = new GetDatabase
                {
                    ClientName = Mydr["DB_Settings"].ToString()
                };
                settings.ConnectionString = "Data Source=ilmlive002.c3srcolvclfy.ap-southeast-1.rds.amazonaws.com,1433;Database=" + Mydab.ClientName + ";User Id=ILM_LiveDBUser;Password=ILMpa55w0rd2021;";
            }
            else if (GetDatabase.ClientName == "vivere")
            {
                //Connection VivereConnection = new Connection();
                //VivereConnection.myparameters.Add(new myParameters { ParameterName = "@ClientName", mytype = SqlDbType.NVarChar, Value = GetDatabase.ClientName.Substring(3, GetDatabase.ClientName.Length - 3) });
                //DataRow Viveredr;
                //Viveredr = VivereConnection.GetSingleRow("sp_CheckClientDB");
                //GetDatabase Viveredab = new GetDatabase
                //{
                //    ClientName = Viveredr["DB_Settings"].ToString()
                //};
                settings.ConnectionString = "Data Source=ilmlive002.c3srcolvclfy.ap-southeast-1.rds.amazonaws.com,1433;Database=VivereDB;User Id=ILM_LiveDBUser;Password=ILMpa55w0rd2021;";
            }
            else 
            {
                #region Original
                Connection Connection = new Connection();

                Connection.myparameters.Add(new myParameters { ParameterName = "@ClientName", mytype = SqlDbType.NVarChar, Value = GetDatabase.ClientName });
                // DataTable EmployeeDt = new DataTable();
                DataRow dr;
                dr = Connection.GetSingleRow("sp_CheckClientDB");

                GetDatabase dab = new GetDatabase
                {
                    ClientName = dr["DB_Settings"].ToString()
                };

                if (dab.ClientName == "ILM_Live")
                {
                    settings.ConnectionString = "Data Source=livedb-ilm.c3srcolvclfy.ap-southeast-1.rds.amazonaws.com,1412;Database=ILM_Live;User Id=ILM_LiveDBUser;Password=ILMV3ry53cur3dMGTpa55w0rd2020;";
                }
                else
                {
                    settings.ConnectionString = "Data Source=livedb-ilm.c3srcolvclfy.ap-southeast-1.rds.amazonaws.com,1412;Database=" + dab.ClientName + ";User Id=ILM_LiveDBUser;Password=ILMV3ry53cur3dMGTpa55w0rd2020;";
                }
                #endregion
            }
            return settings.ToString();
        }
        public static string GetDB3(GetDatabase GetDatabase)
        {
            // com.connectionstring = ConfigurationManager.ConnectionStrings["connectionstring"].ConnectionString.ToString();
            var settings = ConfigurationManager.ConnectionStrings["connectionstring"];
            var fi = typeof(ConfigurationElement).GetField(
                          "_bReadOnly",
                          BindingFlags.Instance | BindingFlags.NonPublic);
            fi.SetValue(settings, false);
            // settings.ConnectionString = "Data Source=tcp:ilmuat.c3srcolvclfy.ap-southeast-1.rds.amazonaws.com,1400;Database=ILM_Main;User Id=devuatuser;Password=ILMV3ry53cur3dMGTpa55w0rd2020;";
            settings.ConnectionString = "Data Source=livedb-ilm.c3srcolvclfy.ap-southeast-1.rds.amazonaws.com,1412;Database=ILM_Main;User Id=ILM_LiveDBUser;Password=ILMV3ry53cur3dMGTpa55w0rd2020;";
            //settings.ConnectionString = "Data Source=192.168.2.4,1444;Database=ILM_Local;User Id=DevUser;Password=securedmgt@1230;";



            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@ClientName", mytype = SqlDbType.NVarChar, Value = GetDatabase.ClientName });
            // DataTable EmployeeDt = new DataTable();
            DataRow dr;
            dr = Connection.GetSingleRow("sp_CheckClientDB");

            GetDatabase dab = new GetDatabase
            {
                ClientName = dr["DB_Settings"].ToString(),
                ClientLink = dr["WebLink"].ToString()
            };

            if (dab.ClientName == "ILM_Live")
            // if (dab.ClientName == "ILM_DevSvr")
            {
                // settings.ConnectionString = "Data Source=tcp:ilm-dev-uat.c3srcolvclfy.ap-southeast-1.rds.amazonaws.com,1400;Database=ILM_DevSvr;User Id=devuatuser;Password=ILMV3ry53cur3dMGTpa55w0rd2020;";
                settings.ConnectionString = "Data Source=livedb-ilm.c3srcolvclfy.ap-southeast-1.rds.amazonaws.com,1412;Database=ILM_Live;User Id=ILM_LiveDBUser;Password=ILMV3ry53cur3dMGTpa55w0rd2020;";

            }
            //else if (dab.ClientName == "ILM_DevSvr")
            //{
            //    settings.ConnectionString = "Data Source=tcp:ilm-dev-uat.c3srcolvclfy.ap-southeast-1.rds.amazonaws.com,1400;Database=ILM_DevSvr;User Id=devuatuser;Password=ILMV3ry53cur3dMGTpa55w0rd2020;";
            //    // settings.ConnectionString = "Data Source=livedb-ilm.c3srcolvclfy.ap-southeast-1.rds.amazonaws.com,1412;Database=ILM_DemoA;User Id=ILM_LiveDBUser;Password=ILMV3ry53cur3dMGTpa55w0rd2020;";

            //}
            else if (dab.ClientName == "StarDB")
            {
                settings.ConnectionString = "Data Source=livedb-ilm.c3srcolvclfy.ap-southeast-1.rds.amazonaws.com,1412;Database=StarDB;User Id=ILM_LiveDBUser;Password=ILMV3ry53cur3dMGTpa55w0rd2020;";
                //settings.ConnectionString = "Data Source=tcp:ilmuat.c3srcolvclfy.ap-southeast-1.rds.amazonaws.com,1433;Database=StarDB;User Id=ILMUATSA;Password=ILMV3ry53cur3dMGTpa55w0rd2020;";

            }
            else if (dab.ClientName == "PetBoweDB")
            {
                settings.ConnectionString = "Data Source=tcp:ilmuat.c3srcolvclfy.ap-southeast-1.rds.amazonaws.com,1433;Database=PetBoweDB;User Id=ILMUATSA;Password=ILMV3ry53cur3dMGTpa55w0rd2020;";

            }
            else if (dab.ClientName == "UnionBankDB")
            {
                settings.ConnectionString = "Data Source=tcp:ilmuat.c3srcolvclfy.ap-southeast-1.rds.amazonaws.com,1433;Database=UnionBankDB;User Id=ILMUATSA;Password=ILMV3ry53cur3dMGTpa55w0rd2020;";

            }
            else if (dab.ClientName == "ILM_UAT")
            {
                settings.ConnectionString = "Data Source=tcp:ilmuat.c3srcolvclfy.ap-southeast-1.rds.amazonaws.com,1433;Database=ILM_UAT;User Id=ILMUATSA;Password=ILMV3ry53cur3dMGTpa55w0rd2020;";

            }
            else if (dab.ClientName == "UAT_HitachiDB")
            {
                settings.ConnectionString = "Data Source=ilm-prodv2.c3srcolvclfy.ap-southeast-1.rds.amazonaws.com,1411;Initial Catalog=HitachiUAT;User ID=Admin;Password=ILMV3ry53cur3dMGTpa55w0rd2020;";
            }
            //else if (dab.ClientName == "ILM_Local")
            //{
            //    settings.ConnectionString = "Data Source=192.168.2.4,1444;Database=ILM_Local;User Id=DevUser;Password=securedmgt@1230;";
            //}
            else
            {
                settings.ConnectionString = "Data Source=livedb-ilm.c3srcolvclfy.ap-southeast-1.rds.amazonaws.com,1412;Database=" + dab.ClientName + ";User Id=ILM_LiveDBUser;Password=ILMV3ry53cur3dMGTpa55w0rd2020;";

            }
            return dab.ClientLink.ToString();

        }
        [HttpPost]
        [Route("CheckCompany")]
        public string CheckCompany(GetDatabase GDB)
        {
            try
            {
                var settings = ConfigurationManager.ConnectionStrings["connectionstring"];
                var fi = typeof(ConfigurationElement).GetField(
                              "_bReadOnly",
                              BindingFlags.Instance | BindingFlags.NonPublic);
                fi.SetValue(settings, false);
                //if (GDB.ClientName == "HitachiUAT")
                //{
                //    settings.ConnectionString = "Data Source=ilm-prodv2.c3srcolvclfy.ap-southeast-1.rds.amazonaws.com,1411;Initial Catalog=HitachiUAT;User ID=Admin;Password=ILMV3ry53cur3dMGTpa55w0rd2020;";
                //}
                //else
                //{
                settings.ConnectionString = "Data Source=livedb-ilm.c3srcolvclfy.ap-southeast-1.rds.amazonaws.com,1412;Database=ILM_Main;User Id=ILM_LiveDBUser;Password=ILMV3ry53cur3dMGTpa55w0rd2020;";
                //}

                Connection Connection = new Connection();
                Connection.myparameters.Add(new myParameters { ParameterName = "@DBNAME", mytype = SqlDbType.NVarChar, Value = GDB.ClientName });
                return Connection.ExecuteScalar("sp_IsDBExist").ToString();
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }

        [HttpPost]
        [Route("UploadProPicture")]
        public string UploadProPicture(profilepicmo SavePic)
        {

            byte[] Photo = Convert.FromBase64String(SavePic.b64img);
            var path = HttpContext.Current.Server.MapPath("~/ProfilePhotos/");
            var folder = Path.Combine(path, SavePic.NTID);
            Directory.CreateDirectory(folder);
            MemoryStream stream = new MemoryStream();

            try
            {
                using (stream)
                {
                    //File.WriteAllBytes(folder + "/" + NTID + "_pic" + Set + ".png", Photo);
                    File.WriteAllBytes(folder + "/" + SavePic.NTID + "_" + SavePic.num + ".png", Photo);
                }

                return "success";
            }
            catch
            {
                return "error";
            }


        }


        [HttpPost]
        [Route("Upsigna")]
        public string UploadProfile(signamo samp)
        {
            byte[] sign1 = Convert.FromBase64String(samp.sign1);
            byte[] sign2 = Convert.FromBase64String(samp.sign2);
            byte[] sign3 = Convert.FromBase64String(samp.sign3);

            var path = HttpContext.Current.Server.MapPath("~/ProfilePhotos/");
            var folder = Path.Combine(path, samp.EmpID);
            Directory.CreateDirectory(folder);

            MemoryStream stream = new MemoryStream();

            try
            {
                using (stream)
                {
                    //File.WriteAllBytes(folder + "/" + NTID + "_pic" + Set + ".png", Photo);
                    File.WriteAllBytes(folder + "/" + samp.EmpID + "_sign_0.png", sign1);
                    File.WriteAllBytes(folder + "/" + samp.EmpID + "_sign_1.png", sign2);
                    File.WriteAllBytes(folder + "/" + samp.EmpID + "_sign_2.png", sign3);
                }

                return "success";
            }
            catch
            {
                return "error";
            }

        }
        [HttpPost]
        [Route("MonitoredExportList")]
        public ExportMonitoredClass MonitoredExportList(EmpCountParams mp)
        {
            try
            {
                ExportMonitoredClass MC = new ExportMonitoredClass();
                List<ExportedMonitoredList> ML = new List<ExportedMonitoredList>();

                var settings = ConfigurationManager.ConnectionStrings["connectionstring"];
                var fi = typeof(ConfigurationElement).GetField(
                              "_bReadOnly",
                              BindingFlags.Instance | BindingFlags.NonPublic);
                fi.SetValue(settings, false);
                settings.ConnectionString = "Data Source=livedb-ilm.c3srcolvclfy.ap-southeast-1.rds.amazonaws.com,1412;Database=ILM_Main;User Id=ILM_LiveDBUser;Password=ILMV3ry53cur3dMGTpa55w0rd2020;";

                Connection Connection = new Connection();
                Connection.myparameters.Add(new myParameters { ParameterName = "@clientName", mytype = SqlDbType.NVarChar, Value = mp._clientname });
                DataTable DT = Connection.GetDataTable("sp_GetMonitoredInfo");
                foreach (DataRow row in DT.Rows)
                {
                    ExportedMonitoredList monList = new ExportedMonitoredList()
                    {
                        Emp_ID = row["empId"].ToString(),
                        Employee_Name = row["employee_name"].ToString(),
                        Date_Added = row["date_added"].ToString(),
                        Active_Date = row["active_date"].ToString(),
                        Inactive_Date = row["inactive_date"].ToString()
                    };
                    ML.Add(monList);
                }
                MC.ExportList = ML;
                MC.MyReturn = "Success";
                return MC;
            }
            catch (Exception e)
            {
                ExportMonitoredClass MC = new ExportMonitoredClass();
                List<ExportedMonitoredList> ML = new List<ExportedMonitoredList>();
                MC.ExportList = ML;
                MC.MyReturn = e.ToString();
                return MC;
            }
        }
        [HttpPost]
        [Route("GetMonitoringClients")]
        public MonitoringClientsList GetMonitoringClients()
        {
            try
            {
                MonitoringClientsList clientList = new MonitoringClientsList();
                List<MonitoringClients> monitoringClient = new List<MonitoringClients>();

                var settings = ConfigurationManager.ConnectionStrings["connectionstring"];
                var fi = typeof(ConfigurationElement).GetField(
                              "_bReadOnly",
                              BindingFlags.Instance | BindingFlags.NonPublic);
                fi.SetValue(settings, false);
                settings.ConnectionString = "Data Source=livedb-ilm.c3srcolvclfy.ap-southeast-1.rds.amazonaws.com,1412;Database=ILM_Main;User Id=ILM_LiveDBUser;Password=ILMV3ry53cur3dMGTpa55w0rd2020;";

                Connection con = new Connection();
                DataTable dt = con.GetDataTable("sp_GetClientList");
                foreach (DataRow row in dt.Rows)
                {
                    MonitoringClients CL = new MonitoringClients()
                    {
                        ClientID = row["ClientID"].ToString(),
                        ClientName = row["ClientName"].ToString()
                    };
                    monitoringClient.Add(CL);
                }
                clientList.ClientsList = monitoringClient;
                clientList.MyReturn = "Success";
                return clientList;
            }
            catch (Exception e)
            {
                MonitoringClientsList clientList = new MonitoringClientsList();
                List<MonitoringClients> monitoringClient = new List<MonitoringClients>();
                clientList.ClientsList = monitoringClient;
                clientList.MyReturn = e.ToString();
                return clientList;
            }
        }
        [HttpPost]
        [Route("MonitoringList")]
        public MonitoringClass MonClass(MonitoringParams MP)
        {
            try
            {
                MonitoringClass MC = new MonitoringClass();
                List<MonitoringList> ML = new List<MonitoringList>();

                var settings = ConfigurationManager.ConnectionStrings["connectionstring"];
                var fi = typeof(ConfigurationElement).GetField(
                              "_bReadOnly",
                              BindingFlags.Instance | BindingFlags.NonPublic);
                fi.SetValue(settings, false);
                settings.ConnectionString = "Data Source=livedb-ilm.c3srcolvclfy.ap-southeast-1.rds.amazonaws.com,1412;Database=ILM_Main;User Id=ILM_LiveDBUser;Password=ILMV3ry53cur3dMGTpa55w0rd2020;";

                Connection Connection = new Connection();
                Connection.myparameters.Add(new myParameters { ParameterName = "@startdate", mytype = SqlDbType.NVarChar, Value = MP._datefrom });
                Connection.myparameters.Add(new myParameters { ParameterName = "@enddate", mytype = SqlDbType.NVarChar, Value = MP._dateto });
                DataTable DT = Connection.GetDataTable("sp_GetMonitoringList");
                foreach (DataRow row in DT.Rows)
                {
                    MonitoringList MonList = new MonitoringList()
                    {
                        _date = row["Date"].ToString(),
                        _client = row["Client"].ToString(),
                        _count = row["Num"].ToString(),
                        _bundyclockcount = row["Bundy"].ToString(),
                        _mobilecount = row["Mobile_Android"].ToString(),
                        _mobileioscount = row["Mobile_iOS"].ToString(),
                        _webcount = row["Web"].ToString(),
                        _otherscount = row["Others"].ToString()

                    };
                    ML.Add(MonList);
                }
                MC.MonitoringList = ML;
                MC._myreturn = "Success";
                return MC;
            }
            catch (Exception e)
            {
                MonitoringClass MC = new MonitoringClass();
                List<MonitoringList> ML = new List<MonitoringList>();
                MC.MonitoringList = ML;
                MC._myreturn = e.ToString();
                return MC;
            }
        }
        [HttpPost]
        [Route("MonitoringList_V2")]
        public MonitoringClass MonClass_V2(MonitoringParams MP)
        {
            try
            {
                MonitoringClass MC = new MonitoringClass();
                List<MonitoringList> ML = new List<MonitoringList>();

                var settings = ConfigurationManager.ConnectionStrings["connectionstring"];
                var fi = typeof(ConfigurationElement).GetField(
                              "_bReadOnly",
                              BindingFlags.Instance | BindingFlags.NonPublic);
                fi.SetValue(settings, false);
                settings.ConnectionString = "Data Source=livedb-ilm.c3srcolvclfy.ap-southeast-1.rds.amazonaws.com,1412;Database=ILM_Main;User Id=ILM_LiveDBUser;Password=ILMV3ry53cur3dMGTpa55w0rd2020;";

                Connection Connection = new Connection();
                Connection.myparameters.Add(new myParameters { ParameterName = "@startdate", mytype = SqlDbType.NVarChar, Value = MP._datefrom });
                Connection.myparameters.Add(new myParameters { ParameterName = "@enddate", mytype = SqlDbType.NVarChar, Value = MP._dateto });
                DataTable DT = Connection.GetDataTable("sp_GetMonitoringList_v2");
                foreach (DataRow row in DT.Rows)
                {
                    MonitoringList MonList = new MonitoringList()
                    {
                        _date = row["Date"].ToString(),
                        _client = row["Client"].ToString(),
                        _count = row["Num"].ToString(),
                        _bundyclockcount = row["Bundy"].ToString(),
                        _mobilecount = row["Mobile_Android"].ToString(),
                        _mobileioscount = row["Mobile_iOS"].ToString(),
                        _webcount = row["Web"].ToString(),
                        _otherscount = row["Others"].ToString()

                    };
                    ML.Add(MonList);
                }
                MC.MonitoringList = ML;
                MC._myreturn = "Success";
                return MC;
            }
            catch (Exception e)
            {
                MonitoringClass MC = new MonitoringClass();
                List<MonitoringList> ML = new List<MonitoringList>();
                MC.MonitoringList = ML;
                MC._myreturn = e.ToString();
                return MC;
            }
        }
        [HttpPost]
        [Route("MonitoringEmpCount")]
        public EmpCountClass EmpClass(EmpCountParams EP)
        {
            try
            {
                EmpCountClass EC = new EmpCountClass();
                List<EmpCountList> EL = new List<EmpCountList>();

                var settings = ConfigurationManager.ConnectionStrings["connectionstring"];
                var fi = typeof(ConfigurationElement).GetField(
                              "_bReadOnly",
                              BindingFlags.Instance | BindingFlags.NonPublic);
                fi.SetValue(settings, false);
                settings.ConnectionString = "Data Source=livedb-ilm.c3srcolvclfy.ap-southeast-1.rds.amazonaws.com,1412;Database=ILM_Main;User Id=ILM_LiveDBUser;Password=ILMV3ry53cur3dMGTpa55w0rd2020;";

                Connection Connection = new Connection();
                Connection.myparameters.Add(new myParameters { ParameterName = "@startdate", mytype = SqlDbType.NVarChar, Value = EP._datefrom });
                Connection.myparameters.Add(new myParameters { ParameterName = "@enddate", mytype = SqlDbType.NVarChar, Value = EP._dateto });
                Connection.myparameters.Add(new myParameters { ParameterName = "@clientname", mytype = SqlDbType.NVarChar, Value = EP._clientname });
                DataTable DT = Connection.GetDataTable("sp_Monitoring_EmpCount");
                foreach (DataRow row in DT.Rows)
                {
                    EmpCountList EmpCountList = new EmpCountList()
                    {
                        _date = row["Date"].ToString(),
                        _activeEmpCount = row["ActiveEmployeeCount"].ToString(),
                        _addition = row["Addition"].ToString(),
                        _deletion = row["Deletion"].ToString(),
                        _net = row["Net"].ToString()
                    };
                    EL.Add(EmpCountList);
                }
                EC.EmpCountList = EL;
                EC._myreturn = "Success";
                return EC;
            }
            catch (Exception e)
            {
                EmpCountClass EC = new EmpCountClass();
                List<EmpCountList> EL = new List<EmpCountList>();
                EC.EmpCountList = EL;
                EC._myreturn = e.ToString();
                return EC;
            }
        }
        [HttpPost]
        [Route("FaceAPIDetect")]
        public async Task<HttpResponseMessage> DetekMuka(profilepicmo SavePic)
        {
            var client = new HttpClient();
            var queryString = HttpUtility.ParseQueryString(string.Empty);

            client.DefaultRequestHeaders.Add("Ocp-Apim-Subscription-Key", "bc6cf65c8dab414f92383ed4830341dd");

            queryString["returnFaceId"] = "true";
            queryString["returnFaceLandmarks"] = "false";
            //queryString["returnFaceAttributes"] = "&returnFaceAttributes=age,gender,headPose,smile,facialHair,glasses," +
            //    "emotion,hair,makeup,occlusion,accessories,blur,exposure,noise";
            var uri = "https://southeastasia.api.cognitive.microsoft.com/face/v1.0/detect?" + queryString;

            HttpResponseMessage response;

            byte[] Photo = Convert.FromBase64String(SavePic.b64img);
            var path = HttpContext.Current.Server.MapPath("~/ProfilePhotos/");
            var folder = Path.Combine(path, SavePic.NTID);
            Directory.CreateDirectory(folder);
            MemoryStream stream = new MemoryStream();

            using (stream)
            {

                File.WriteAllBytes(folder + "/" + SavePic.NTID + "_" + SavePic.num + ".png", Photo);
                using (FileStream fileStream = new FileStream(folder + "/" + SavePic.NTID + "_" + SavePic.num + ".png", FileMode.Open, FileAccess.Read))
                {
                    BinaryReader binaryReader = new BinaryReader(fileStream);
                    // return binaryReader.ReadBytes((int)fileStream.Length);
                    using (var content = new ByteArrayContent(binaryReader.ReadBytes((int)fileStream.Length)))
                    {
                        content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
                        response = await client.PostAsync(uri, content);
                        return response;
                    }
                }
            }
        }

        [HttpPost]
        [Route("DetectFaceAlternativev2")]
        public async Task<HttpResponseMessage> DetectFacev2()
        {

            HttpRequest httpRequest = HttpContext.Current.Request;
            BinaryReader binaryReader = new BinaryReader(httpRequest.Files[0].InputStream);
            byte[] Photo = binaryReader.ReadBytes(httpRequest.Files[0].ContentLength);
            //MemoryStream inputStream = new MemoryStream(Photo);
            Stream inputStream = new MemoryStream(Photo);

            try
            {
                FaceServiceClient faceServiceClient = new FaceServiceClient("bc6cf65c8dab414f92383ed4830341dd", "https://southeastasia.api.cognitive.microsoft.com/face/v1.0");
                //string personGroupId = "ilmdevs";
                string personGroupId = "";
                //string personGroupId = "ilmdevs";
                if (httpRequest.Form["PersonGroupID"].ToLower() == "lomedagroupofcompanies")
                {
                    personGroupId = "hondacarsbatangas";
                }
                else
                {
                    personGroupId = httpRequest.Form["PersonGroupID"].ToLower();
                }
                var faces = await faceServiceClient.DetectAsync(inputStream);
                var faceIds = faces.Select(face => face.FaceId).ToArray();

                try
                {
                    var results = await faceServiceClient.IdentifyAsync(personGroupId, faceIds);

                    if (results.Count() > 1)
                    {
                        return Request.CreateResponse(HttpStatusCode.BadRequest, "Multiple faces detected!");
                        //return "Multiple faces detected!";
                        //return Request.CreateResponse(HttpStatusCode.NotAcceptable);
                    }
                    else
                    {
                        var candidateId = results.FirstOrDefault().Candidates.FirstOrDefault().PersonId;
                        var person = await faceServiceClient.GetPersonAsync(personGroupId, candidateId);
                        var confidthres = results.FirstOrDefault().Candidates.FirstOrDefault().Confidence;
                        //Connection con = new Connection();
                        //con.myparameters.Add(new myParameters { ParameterName = "@FaceID", mytype = SqlDbType.NVarChar, Value = person.PersonId.ToString() });
                        //con.myparameters.Add(new myParameters { ParameterName = "@Confid", mytype = SqlDbType.NVarChar, Value = confidthres.ToString() });
                        //con.ExecuteNonQuery("sp_InsertConfidThresh");
                        List<string> listReturn = new List<string>
                        {
                            candidateId.ToString(),
                            person.PersonId.ToString(),
                            confidthres.ToString()
                        };
                        return Request.CreateResponse(HttpStatusCode.Accepted, listReturn);
                        //return person.Name;
                        //return Request.CreateResponse(HttpStatusCode.Accepted);
                    }
                }
                catch
                {
                    return Request.CreateResponse(HttpStatusCode.NotAcceptable, "Error");
                    //return "Error";
                    //return Request.CreateResponse(HttpStatusCode.NotAcceptable);
                }
            }
            catch
            {
                return Request.CreateResponse(HttpStatusCode.NotAcceptable, "Error");
                //return "Error";
                //return Request.CreateResponse(HttpStatusCode.NotAcceptable);
            }
        }



        [HttpPost]
        [Route("DetectFaceAlternativev3")]
        public async Task<HttpResponseMessage> DetectFacev3()
        {

            HttpRequest httpRequest = HttpContext.Current.Request;
            BinaryReader binaryReader = new BinaryReader(httpRequest.Files[0].InputStream);
            byte[] Photo = binaryReader.ReadBytes(httpRequest.Files[0].ContentLength);
            //MemoryStream inputStream = new MemoryStream(Photo);
            Stream inputStream = new MemoryStream(Photo);

            try
            {
                FaceServiceClient faceServiceClient = new FaceServiceClient("bc6cf65c8dab414f92383ed4830341dd", "https://southeastasia.api.cognitive.microsoft.com/face/v1.0");
                //string personGroupId = "ilmdevs";
                string personGroupId = "";
                //string personGroupId = "ilmdevs";
                if (httpRequest.Form["PersonGroupID"].ToLower() == "lomedagroupofcompanies")
                {
                    personGroupId = "hondacarsbatangas";
                }
                else
                {
                    personGroupId = httpRequest.Form["PersonGroupID"].ToLower();
                }
                var faces = await faceServiceClient.DetectAsync(inputStream);
                var faceIds = faces.Select(face => face.FaceId).ToArray();

                try
                {
                    var results = await faceServiceClient.IdentifyAsync(personGroupId, faceIds, 0.7f, 1);

                    if (results.Count() > 1)
                    {
                        return Request.CreateResponse(HttpStatusCode.BadRequest, "Multiple faces detected!");
                        //return "Multiple faces detected!";
                        //return Request.CreateResponse(HttpStatusCode.NotAcceptable);
                    }
                    else
                    {
                        var candidateId = results.FirstOrDefault().Candidates.FirstOrDefault().PersonId;
                        var person = await faceServiceClient.GetPersonAsync(personGroupId, candidateId);
                        var confidthres = results.FirstOrDefault().Candidates.FirstOrDefault().Confidence;
                        Connection con = new Connection();
                        con.myparameters.Add(new myParameters { ParameterName = "@FaceID", mytype = SqlDbType.NVarChar, Value = person.PersonId.ToString() });
                        con.myparameters.Add(new myParameters { ParameterName = "@Confid", mytype = SqlDbType.NVarChar, Value = confidthres.ToString() });
                        con.ExecuteNonQuery("sp_InsertConfidThresh");
                        return Request.CreateResponse(HttpStatusCode.Accepted, person.PersonId);
                        //return person.Name;
                        //return Request.CreateResponse(HttpStatusCode.Accepted);
                    }
                }
                catch
                {
                    return Request.CreateResponse(HttpStatusCode.NotAcceptable, "Error");
                    //return "Error";
                    //return Request.CreateResponse(HttpStatusCode.NotAcceptable);
                }
            }
            catch
            {
                return Request.CreateResponse(HttpStatusCode.NotAcceptable, "Error");
                //return "Error";
                //return Request.CreateResponse(HttpStatusCode.NotAcceptable);
            }
        }



        [HttpPost]
        [Route("DetectFaceAlternativev4")]
        public async Task<HttpResponseMessage> DetectFacev4()
        {

            HttpRequest httpRequest = HttpContext.Current.Request;
            BinaryReader binaryReader = new BinaryReader(httpRequest.Files[0].InputStream);
            byte[] Photo = binaryReader.ReadBytes(httpRequest.Files[0].ContentLength);
            //MemoryStream inputStream = new MemoryStream(Photo);
            Stream inputStream = new MemoryStream(Photo);

            try
            {
                FaceServiceClient faceServiceClient = new FaceServiceClient("bc6cf65c8dab414f92383ed4830341dd", "https://southeastasia.api.cognitive.microsoft.com/face/v1.0");
                //string personGroupId = "ilmdevs";
                string personGroupId = "";
                //string personGroupId = "ilmdevs";
                if (httpRequest.Form["PersonGroupID"].ToLower() == "lomedagroupofcompanies")
                {
                    personGroupId = "hondacarsbatangas";
                }
                else
                {
                    personGroupId = httpRequest.Form["PersonGroupID"].ToLower();
                }
                var thresholdHolder = httpRequest.Form["thresholdHolder"].ToString();
                // float thresh = Convert.ToInt32(thresholdHolder);
                float thresh = float.Parse(thresholdHolder);
                var faces = await faceServiceClient.DetectAsync(inputStream);
                var faceIds = faces.Select(face => face.FaceId).ToArray();

                try
                {
                    var results = await faceServiceClient.IdentifyAsync(personGroupId, faceIds, thresh, 1);

                    if (results.Count() > 1)
                    {
                        return Request.CreateResponse(HttpStatusCode.BadRequest, "Multiple faces detected!");
                        //return "Multiple faces detected!";
                        //return Request.CreateResponse(HttpStatusCode.NotAcceptable);
                    }
                    else
                    {
                        var candidateId = results.FirstOrDefault().Candidates.FirstOrDefault().PersonId;
                        var person = await faceServiceClient.GetPersonAsync(personGroupId, candidateId);
                        var confidthres = results.FirstOrDefault().Candidates.FirstOrDefault().Confidence;
                        Connection con = new Connection();
                        con.myparameters.Add(new myParameters { ParameterName = "@FaceID", mytype = SqlDbType.NVarChar, Value = person.PersonId.ToString() });
                        con.myparameters.Add(new myParameters { ParameterName = "@Confid", mytype = SqlDbType.NVarChar, Value = confidthres.ToString() });
                        con.ExecuteNonQuery("sp_InsertConfidThresh");
                        return Request.CreateResponse(HttpStatusCode.Accepted, person.PersonId);
                        //return person.Name;
                        //return Request.CreateResponse(HttpStatusCode.Accepted);
                    }
                }
                catch
                {
                    return Request.CreateResponse(HttpStatusCode.NotAcceptable, "Error");
                    //return "Error";
                    //return Request.CreateResponse(HttpStatusCode.NotAcceptable);
                }
            }
            catch
            {
                return Request.CreateResponse(HttpStatusCode.NotAcceptable, "Error");
                //return "Error";
                //return Request.CreateResponse(HttpStatusCode.NotAcceptable);
            }
        }


        [HttpPost]
        [Route("DetectFaceAlternativev5")]
        public async Task<HttpResponseMessage> DetectFacev5()
        {

            HttpRequest httpRequest = HttpContext.Current.Request;
            BinaryReader binaryReader = new BinaryReader(httpRequest.Files[0].InputStream);
            byte[] Photo = binaryReader.ReadBytes(httpRequest.Files[0].ContentLength);
            //MemoryStream inputStream = new MemoryStream(Photo);
            Stream inputStream = new MemoryStream(Photo);

            try
            {
                FaceServiceClient faceServiceClient = new FaceServiceClient("bc6cf65c8dab414f92383ed4830341dd", "https://southeastasia.api.cognitive.microsoft.com/face/v1.0");
                //string personGroupId = "ilmdevs";
                string personGroupId = "";
                //string personGroupId = "ilmdevs";
                if (httpRequest.Form["PersonGroupID"].ToLower() == "lomedagroupofcompanies")
                {
                    personGroupId = "hondacarsbatangas";
                }
                else
                {
                    personGroupId = httpRequest.Form["PersonGroupID"].ToLower();
                }
                var faces = await faceServiceClient.DetectAsync(inputStream);
                var faceIds = faces.Select(face => face.FaceId).ToArray();
                int returnvalue = 0;
                float threshValue = 0f;
                for (int THRESH = 100; THRESH > 0; THRESH--)
                {
                    var results = await faceServiceClient.IdentifyAsync(personGroupId, faceIds, THRESH, 1);

                    if (results.Count() > 1)
                    {
                        threshValue = THRESH;
                        returnvalue = 0;
                        //return "Multiple faces detected!";
                        //return Request.CreateResponse(HttpStatusCode.NotAcceptable);
                    }
                    else if (results.Count() == 1)
                    {
                        threshValue = THRESH;
                        returnvalue = 2;
                        break;
                    }
                    else
                    {
                        var candidateId = results.FirstOrDefault().Candidates.FirstOrDefault().PersonId;
                        var person = await faceServiceClient.GetPersonAsync(personGroupId, candidateId);
                        var confidthres = results.FirstOrDefault().Candidates.FirstOrDefault().Confidence;
                        Connection con = new Connection();
                        con.myparameters.Add(new myParameters { ParameterName = "@FaceID", mytype = SqlDbType.NVarChar, Value = person.PersonId.ToString() });
                        con.myparameters.Add(new myParameters { ParameterName = "@Confid", mytype = SqlDbType.NVarChar, Value = confidthres.ToString() });
                        con.ExecuteNonQuery("sp_InsertConfidThresh");
                        threshValue = THRESH;
                        returnvalue = 1;
                        //return person.Name;
                        //return Request.CreateResponse(HttpStatusCode.Accepted);
                    }
                }

                if (returnvalue == 0)
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, "Multiple faces detected!");
                }
                else if (returnvalue == 1)
                {
                    return Request.CreateResponse(HttpStatusCode.Accepted, threshValue);
                }
                else if (returnvalue == 2)
                {
                    return Request.CreateResponse(HttpStatusCode.NotAcceptable, threshValue);
                }
                return Request.CreateResponse(HttpStatusCode.Accepted, threshValue);
            }
            catch
            {
                return Request.CreateResponse(HttpStatusCode.NotAcceptable, "Error");
                //return "Error";
                //return Request.CreateResponse(HttpStatusCode.NotAcceptable);
            }
        }

        [HttpPost]
        [Route("DetectFaceAlternativev6")]
        public async Task<HttpResponseMessage> DetectFacev6()
        {

            HttpRequest httpRequest = HttpContext.Current.Request;
            BinaryReader binaryReader = new BinaryReader(httpRequest.Files[0].InputStream);
            byte[] Photo = binaryReader.ReadBytes(httpRequest.Files[0].ContentLength);
            //MemoryStream inputStream = new MemoryStream(Photo);
            Stream inputStream = new MemoryStream(Photo);
            string savedConfidThres = "";
            string empID = "";
            try
            {
                FaceServiceClient faceServiceClient = new FaceServiceClient("bc6cf65c8dab414f92383ed4830341dd", "https://southeastasia.api.cognitive.microsoft.com/face/v1.0");
                //string personGroupId = "ilmdevs";
                string personGroupId = "";
                //string personGroupId = "ilmdevs";
                if (httpRequest.Form["PersonGroupID"].ToLower() == "lomedagroupofcompanies")
                {
                    personGroupId = "hondacarsbatangas";
                }
                else
                {
                    personGroupId = httpRequest.Form["PersonGroupID"].ToLower();
                }
                var thresholdHolder = httpRequest.Form["thresholdHolder"].ToString();
                empID = httpRequest.Form["EmpID"].ToString();
                // float thresh = Convert.ToInt32(thresholdHolder);
                float thresh = float.Parse(thresholdHolder);
                var faces = await faceServiceClient.DetectAsync(inputStream);
                var faceIds = faces.Select(face => face.FaceId).ToArray();

                try
                {
                    var results = await faceServiceClient.IdentifyAsync(personGroupId, faceIds, thresh, 1);
                    savedConfidThres = results.FirstOrDefault().Candidates.FirstOrDefault().Confidence.ToString();
                    if (results.Count() > 1)
                    {
                        return Request.CreateResponse(HttpStatusCode.BadRequest, "Multiple faces detected!");
                        //return "Multiple faces detected!";
                        //return Request.CreateResponse(HttpStatusCode.NotAcceptable);
                    }
                    else
                    {
                        var candidateId = results.FirstOrDefault().Candidates.FirstOrDefault().PersonId;
                        var person = await faceServiceClient.GetPersonAsync(personGroupId, candidateId);
                        var confidthres = results.FirstOrDefault().Candidates.FirstOrDefault().Confidence;
                        Connection con = new Connection();
                        con.myparameters.Add(new myParameters { ParameterName = "@FaceID", mytype = SqlDbType.NVarChar, Value = (person.PersonId.ToString() == null || person.PersonId.ToString() == "") ? "null" : person.PersonId.ToString() });
                        con.myparameters.Add(new myParameters { ParameterName = "@ConfidLevel", mytype = SqlDbType.NVarChar, Value = (confidthres.ToString() == null || confidthres.ToString() == "") ? "0%" : confidthres.ToString() });
                        con.ExecuteNonQuery("sp_InsertConfidenceLevelThreshold");
                        return Request.CreateResponse(HttpStatusCode.Accepted, person.PersonId);
                        //return person.Name;
                        //return Request.CreateResponse(HttpStatusCode.Accepted);
                    }
                }
                catch
                {
                    Connection con = new Connection();
                    con.myparameters.Add(new myParameters { ParameterName = "@FaceID", mytype = SqlDbType.NVarChar, Value = (empID == null || empID == "") ? "null" : empID });
                    con.myparameters.Add(new myParameters { ParameterName = "@ConfidLevel", mytype = SqlDbType.NVarChar, Value = (savedConfidThres == null || savedConfidThres == "") ? "0%" : savedConfidThres });
                    con.ExecuteNonQuery("sp_InsertConfidenceLevelThreshold");
                    return Request.CreateResponse(HttpStatusCode.NotAcceptable, "Error");
                    //return "Error";
                    //return Request.CreateResponse(HttpStatusCode.NotAcceptable);
                }
            }
            catch
            {
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@FaceID", mytype = SqlDbType.NVarChar, Value = (empID == null || empID == "") ? "null" : empID });
                con.myparameters.Add(new myParameters { ParameterName = "@ConfidLevel", mytype = SqlDbType.NVarChar, Value = (savedConfidThres == null || savedConfidThres == "") ? "0%" : savedConfidThres });
                con.ExecuteNonQuery("sp_InsertConfidenceLevelThreshold");
                return Request.CreateResponse(HttpStatusCode.NotAcceptable, "Error");
                //return "Error";
                //return Request.CreateResponse(HttpStatusCode.NotAcceptable);
            }
        }



        #region Marvic
        [HttpPost]
        [Route("DisplayActivityByRangeAlternative4")]
        public ListAllActivityResult2 DisplayActivityByRangeAlternative4(DailyParameters3 DailyParameters)
        {
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = DailyParameters.companyname;
            GetDB2(GDB);
            ListAllActivityResult2 GetActivityResult = new ListAllActivityResult2();
            //try
            //{
            Connection Connection = new Connection();
            List<ListAllActivityByRange2> AllActivity = new List<ListAllActivityByRange2>();
            DataTable dtDAILY = new DataTable();
            //BasicFunction BasicFunction = new BasicFunction();
            Connection.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.NVarChar, Value = DailyParameters.NTID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@SchedDate1", mytype = SqlDbType.NVarChar, Value = DailyParameters.Start_Date });
            Connection.myparameters.Add(new myParameters { ParameterName = "@SchedDate2", mytype = SqlDbType.NVarChar, Value = DailyParameters.End_Date });
            dtDAILY = Connection.GetDataTable("sp_GetEmployeeActivity_Mobile_AlternativeV3");
            //CultureInfo provider = CultureInfo.InvariantCulture;
            if (dtDAILY.Rows.Count > 0)
            {

                for (int I = 0; I < dtDAILY.Rows.Count; I++)
                {
                    ListAllActivityByRange2 ListAllActivity = new ListAllActivityByRange2();

                    ListAllActivity.EmpID = dtDAILY.Rows[I]["EMPID"].ToString();
                    if (dtDAILY.Rows[I]["Start_Time"].ToString() == "" || dtDAILY.Rows[I]["End_Time"].ToString() == "")
                    {

                        ListAllActivity.StartTIme = dtDAILY.Rows[I]["Schedule"].ToString() == "ABSENT" || dtDAILY.Rows[I]["Schedule"].ToString() == "HOLIDAY" || dtDAILY.Rows[I]["Schedule"].ToString() == "LEAVE" || dtDAILY.Rows[I]["Schedule"].ToString() == "RD" ? dtDAILY.Rows[I]["Schedule"].ToString() : dtDAILY.Rows[I]["Start_Time"].ToString();
                        ListAllActivity.StartEnd = dtDAILY.Rows[I]["Schedule"].ToString() == "ABSENT" || dtDAILY.Rows[I]["Schedule"].ToString() == "HOLIDAY" || dtDAILY.Rows[I]["Schedule"].ToString() == "LEAVE" || dtDAILY.Rows[I]["Schedule"].ToString() == "RD" ? dtDAILY.Rows[I]["Schedule"].ToString() : dtDAILY.Rows[I]["End_Time"].ToString();

                    }
                    else
                    {
                        ListAllActivity.StartTIme = dtDAILY.Rows[I]["Start_Time"].ToString();
                        ListAllActivity.StartEnd = dtDAILY.Rows[I]["End_Time"].ToString();

                    }
                    ListAllActivity.ReasonID = dtDAILY.Rows[I]["ReasonID"].ToString();
                    ListAllActivity.ReasonDesc = dtDAILY.Rows[I]["ReasonDesc"].ToString();
                    ListAllActivity.IsPaid = dtDAILY.Rows[I]["IsPaid"].ToString();
                    ListAllActivity.SchedDate = dtDAILY.Rows[I]["SchedDate"].ToString();
                    ListAllActivity.ActivityStatus = dtDAILY.Rows[I]["Activity_Status"].ToString();
                    ListAllActivity.HourWork = dtDAILY.Rows[I]["TotalTimeMin"].ToString();
                    ListAllActivity.SeriesID = dtDAILY.Rows[I]["SeriesID"].ToString();
                    ListAllActivity.SchedDateStatus = dtDAILY.Rows[I]["ApprovedWork"].ToString();
                    ListAllActivity.AddedBy = dtDAILY.Rows[I]["AddedBy"].ToString();
                    ListAllActivity.SchedIn = dtDAILY.Rows[I]["ScheduleIn"].ToString();
                    ListAllActivity.SchedOut = dtDAILY.Rows[I]["ScheduleOut"].ToString();
                    ListAllActivity.EmpComment = dtDAILY.Rows[I]["EmpComment"].ToString();
                    ListAllActivity.ApproverComment = dtDAILY.Rows[I]["ApproverComment"].ToString();
                    ListAllActivity.FilePath = dtDAILY.Rows[I]["FilePath"].ToString();

                    AllActivity.Add(ListAllActivity);
                }
                GetActivityResult.ListAllActivity = AllActivity;
                GetActivityResult.myreturn = "Success";
            }
            else
            {
                GetActivityResult.myreturn = "No Data Available";
            }

            return GetActivityResult;
        }
        #endregion

        [HttpPost]
        [Route("DeleteBase64")]
        public string DeleteBase64(Base64cmp cmp)
        {
            try
            {
                GetDatabase GDB = new GetDatabase();
                GDB.ClientName = cmp.CN;
                GetDB2(GDB);
                Connection Connection = new Connection();
                Connection.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = cmp.EmpID });
                Connection.ExecuteNonQuery("sp_Delete_Base64");
                return "Success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }

        [HttpPost]
        [Route("InsertConfidenceLevel")]
        public string InsertConfidenceLevel(ConfidenceLevel CL)
        {
            try
            {
                GetDatabase GDB = new GetDatabase();
                GDB.ClientName = CL.CN;
                GetDB2(GDB);
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.NVarChar, Value = CL.EmpID });
                con.myparameters.Add(new myParameters { ParameterName = "@ConfidLevel", mytype = SqlDbType.NVarChar, Value = CL.ConfidLevel });
                con.ExecuteNonQuery("sp_InsertConfidenceLevelThreshold");
                return "Success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }
        [HttpPost]
        [Route("DeleteFaceIDv3")]
        public string DeleteFaceIDv3(Loginv2 fID)
        {
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = fID.CN;
            SelfieRegistration2Controller.GetDB2(GDB);

            Connection con = new Connection();
            DataTable dt = new DataTable();
            con.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.NVarChar, Value = fID.LoginID });
            con.ExecuteScalar("sp_DeleteFaceID");
            return "Success";
        }
        #region 2010-10-10
        [HttpPost]
        [Route("UploadProPicturev2")]
        public string UploadProPicturev2(profilepicmo SavePic)
        {


            byte[] Photo = Convert.FromBase64String(SavePic.b64img);
            var path = HttpContext.Current.Server.MapPath("~/ProfilePhotos/" + SavePic.CN + "/" + "");
            var folder = Path.Combine(path, SavePic.NTID);
            Directory.CreateDirectory(folder);
            MemoryStream stream = new MemoryStream();

            try
            {
                using (stream)
                {
                    //File.WriteAllBytes(folder + "/" + NTID + "_pic" + Set + ".png", Photo);
                    File.WriteAllBytes(folder + "/" + SavePic.NTID + "_" + SavePic.num + ".png", Photo);
                }

                return "success";
            }
            catch
            {
                return "error";
            }


        }

        [HttpPost]
        [Route("RegisterFaceAlternativev3")]
        public async Task<string> RegisterFaceAlternativev3(SelfieRegistration selfieReg)
        {
            string name = selfieReg.Name;
            string ntid = selfieReg.NTID;

            try
            {
                FaceServiceClient faceServiceClient = new FaceServiceClient("bc6cf65c8dab414f92383ed4830341dd", "https://southeastasia.api.cognitive.microsoft.com/face/v1.0");
                //var person = await faceServiceClient.CreatePersonAsync("ilmdevs", name);
                string personGroupId = "";
                //string personGroupId = "ilmdevs";
                if (selfieReg.PersonGroupID.ToLower() == "lomedagroupofcompanies")
                {
                    personGroupId = "hondacarsbatangas";
                }
                else
                {
                    personGroupId = selfieReg.PersonGroupID.ToLower();
                }
                var person = await faceServiceClient.CreatePersonAsync(personGroupId, name);
                Guid persondID = person.PersonId;


                //var path = HttpContext.Current.Server.MapPath("~/Profile/");
                var path = HttpContext.Current.Server.MapPath("~/ProfilePhotos/");
                //var path = "http://uat-vm.southeastasia.cloudapp.azure.com:81/hrms_test/ProfilePhotos";
                var folder = Path.Combine(path, selfieReg.CN.ToLower() + "/", ntid);

                Stream stream1 = File.OpenRead(folder + "/" + ntid + "_pic1.png");
                Stream stream2 = File.OpenRead(folder + "/" + ntid + "_pic2.png");


                //await faceServiceClient.AddPersonFaceAsync("ilmdevs", persondID, stream1);
                await faceServiceClient.AddPersonFaceAsync(selfieReg.PersonGroupID.ToLower(), persondID, stream1);
                //await faceServiceClient.AddPersonFaceAsync("ilmdevs", persondID, stream2);
                await faceServiceClient.AddPersonFaceAsync(selfieReg.PersonGroupID.ToLower(), persondID, stream2);

                //await faceServiceClient.TrainPersonGroupAsync("ilmdevs");
                await faceServiceClient.TrainPersonGroupAsync(selfieReg.PersonGroupID.ToLower());

                string guidStr = persondID.ToString("D");

                return guidStr;
            }
            catch
            {
                return "null";
            }
        }



        [HttpPost]
        [Route("Upsignav2")]
        public string UploadProfilev2(signamo samp)
        {
            //string def = "/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxITEhUTExMWFhUXGBoaFxcYFx0fGhcgGh0eGB0YHiAfHSkgHR8lHx0YIjEhJSktLjAuGB8zODMtNygtLisBCgoKDg0OGxAQGy8mICUtLS0tLS0tLS0tLy0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLf/AABEIAL0BCwMBEQACEQEDEQH/xAAcAAACAgMBAQAAAAAAAAAAAAAFBgQHAAMIAgH/xABOEAACAQIDBAYGBwUFBAkFAAABAgMEEQAFIQYSMUEHEyJRYXEUMkKBkaEjM1JicoKxFUOSosEkU2Oy0TREwvAWNVRzg5PS4fEIFyWjs//EABsBAAIDAQEBAAAAAAAAAAAAAAAEAgMFBgEH/8QANxEAAQMCBAIIBwABBAMBAAAAAQACAwQRBRIhMUFREyIyYXGBkdEUQqGxweHw8RUjM1JDcoI0/9oADAMBAAIRAxEAPwCXX5qkdgTdibBRqSe4Aak+GBCj1tHVsm/MVpIe+X6w/kuN3n6xB8Djwmyk1pcbAJZkzWkgN4YzPL/ey8Ae8C36AeZxWZRwWhDhz3avNvuguaZ1NNrJISPsjRR7hx8zc4qLi5aLIY4RdoTf0f5JFTw/tetXsqf7HCeMr8pLfHdv3FuSnFmjG3KQIfWTCOPZBs3zOWpmeaVru59yjko7gOA/1wm5xcbldZBAyBgjZsP66hk48VytTY7Y/wBHpzUzL9NIvZU/ulP6M3PuGnfhyGPL1iuSxbEOmd0TD1R9T7KBmdGs0bxNwYEX7jyPuNj7sXEXFllRSGN4cEhbFbLisrjBOSkUCtJUWNjZCBug8rk8e4G3LFMbea06yckAM4pu/wClmWg9UMpg9GGgIVRLbk/q6Nz9a/jiHxGu2ibGAuyXL+t4aeqn5Zkw3mqMnqusHF6SZrSLy7JOvK3b0PHfxe14dssaopJac2kHnw9VGzvb19YJkaKUesjix89eI7iNDiaWQWPOGOoOBCm5evp1VBS+yzb03/dpq1/xaJ5uMCEN21zj0qslkHqA7kfduJoCPM3b82M+R2ZxK7jD6foKdrTudT5oCZbag2I1BHEeI7jiFk05zbWKO7XZxHmFJHNwroLI5A+vTlw531HjvD2gcOt67dd1yVQDRzEx9k/1kcyKgGSUvpE4D5jUL9HGdRTqeZ8e88z2RoCSPcIxpuvaWnfXy3d2RufwkyonZ2Z3Ys7ElmPEk8ScJXvuuuYxrGhrRYBa8Cknfo2qlk6/L5GstQpaM/ZkQXuPGwVv/DwxTusbLAxymzMEw4aHw/z90/8AR9nbzRNFMN2aFjHIO5kO6fMXHHDa5hNmBCp3pc2g62YUqHsQ6yeLkcPyg/Fj3YTnfc5QupwSkyMMztzt4ftV9ihbqdNhYUpop80nF0gBWFftyN2dPiFv95vs4vgb85WDjE5dlpmbnf8AHupWQQxVV5aWqaGvclpIpydyZjqSp1t+W9gPUGGGStcsWpw+aAZiLjmPzyTNl+2EtPIIK6Non5E+q/irDRh5HS+tsWJFO1HXJILqwOBClYELMCFmBCzAhUrQ5qKHLhXIqtVVMskauwv1SIzrYee5e3MtrcKBiuR5aNE7QUzZ5CH7AXVd5tm09S+/PI0jct46DyA0HuGF7k7rcbGxmjBYKCTgUrpn6P8AZVayRqioO5Q0+szHQSEa9UD8C1tbEDiwIuYABcrKqpnSOEUepKnbX7RNWzb1tyFOzDHyVe+w03jYX7tBywrI8vK6Ggom0sdvmO5/HgECxBPKxOi7ZDrmFZOv0Sn6JT7bA+ufuqeHefLViGO/WKwcXxDIDBGdeJ5dytLNlvGfLDa5hVxKNT54EKDslABmVUoH11FJfzVkW/vDD4YiRurmSatvwKr2PgPLGaF9AXwVTRkOjFXXVWUkMPIjUYk3fRLzluUh2oRFtqhUAJXxiZeTgWdfHS2vitj54bbKeK5qfD2uN49O5apsmYKZaOYTRjijECRfDkD5HdPni4OB2WVJE+M2cEX6Pcrq5Y6iq6xKSJgYjUTaboGjBF0ubmxJIF1HEgjHjrW1RCHF4yi55L7X7ByiF5aKrhrhGLukY3ZAPBd5t7S5tcE20udML9E09kre/wBTnjIEzCLpEkqri99MRDUw6ou3NdWLsNkSUEIzWuW7kf2OnPEk6iRh38x9kdrja1xIjbcrIa19bNkZtz7uaX81zKWplaaZt52OvcO5QOQHIYTcS43K62CBkLAxg0H9dRMeK5ZgQt9DVvDIkqGzxsGXzBvbyPD349BsbhVyxiVhY7Y6K36SVFroqyL6muiVj4SKADfxK7unej40Qbi64GWMxvLHbg2TFtdnq0dK8xtverGD7Tn1R7tSfBTiMj8rbq6jpjUTBg24+C55kcsSzElmJLE8SSbknzOM9d01oaABsFuy2heeWOGMXeRgo8L8SfAC5PgDj0C5sFCaVsUZkdsEw9I1cgeHLYD9BRgb5+3KRqT4qCfzO3dhiU5RlCwcNidPK6ok47f30SrhZdCm7KNuJAnUVsYq6c+y9jIvcQx4kcr6/eGLmTkb6rIq8Hil60fVP0/SYMsozbrsoqOuQavSytaVB3KW1PcN/jqd84ba8O2XNVFLLAbSC329UxZDtujsYpgYpV0ZHBVgfEHXEkunCGZWFwb4ELZgQswIXN4zFFRqOpDejmQvHIvGFmuTy4XLG+vrNfQ6Rc0OFlbDM6J2ZqEZtkUkIDgiSE6rKmq2PC9uHnw8cLuYWrcp6tkotseXsvGy+z82YVK08Wg4yyco05sfHkBzPhciTGX1VVZU5BlG6bNttoIVRMvouzSQaXH75wdWJ9oXub8ySe7EJX5jYbJvDaUU46WTtHbuHuUn+ljFORafxIRzZLLhUzfSXEKayEcW7kB7zzPIe7FjIsx7knW4mIWdXtHb3V1wbQxIoVVCqoAVQLAAaADww6uSc4uNzuvFdtIhQ4F4q5zHaBFY+eBCH7ObThMxWQjQxyIffY/8OBANknemqNO7TGf0ZXbtrWlt1Dnq74sDLJOWpDitRqBiWVUmdoR3YPIpcwqxHGzRxIN6eUabifZvw3m4AHuJ4KcWMZxWdV1IcMgCaNts/Sdkp6cBaSn7MSjgxAt1h7+4X5XPFjheWTMbDZbuF0Hw7M7h1j9By90L2azs0dTHUC9lNnA9pDow8e8eIGIxuyuuma6Fs0DmHy8UwZbsjSPVVGZzEDLkfrIo7aTOdSoHtIH4KNCSF4KRh12UdYrkI+mlIgbugm1G0EtbOZZNBwjS+ka93iTxJ5nwAARe8vNyuwo6RlNHkbvxPM/2yEYim089G+xvpT+kTr/Z0PZU/vWHL8APHvOnfi6KLMbnZYuK4j0I6KM9Y79w91o6VMk9HrOsUWjnG+O4MNHH6N+Y4JmZXX5qeDVPSwZDu37cEm4pWurB2DqjUUc1GNZYT6RTd+h7SDzJIv8A4xw1Tu0yrmMdprPEw46HxH6+yV9tttDWvGo+riXQd7H1m92i+49+PJusbKzCQ2GMu4n7IFFLfC5FlvMkDlY3R9QGCB64j6WQ9TSgjmfWfxAsfdG3fhmnZ8y57G6u5EDeGp8eARbNei6OSPrIXKTnV98kpIx1JbmpJ4kfDEpIQ7UJahxV1OMjxdv1Huq1znJqilfcnjZDyPFW8VYaH9e+2FHNLdCungqYp23jN/uoGPFettNUPG4eN2R14MpII94wAkahQexr25Xi4705U22EFUqxZpFvldEqohuyp4nd1tzO7oeanDLKjg5YFXgl+tAfI/g+6P0wrKRBNTSenUnJo9ZVA5Mo9a33dfujDIIIuFz8kb43ZXix7007PbXwVIFmF+6+PVBMYcd+BC5yliDCxGBCG5c9SlQKek7fWG3UnVWLG2g9k8SW4WBJ0GBCdc8ljyumOX0pBqJe1WTLpa4+rXuFjYDkvixOFpZLdVq3sLojKenl2G3f3+AVZ1B1xU3ZacxJctuW0LTSBF05kngoHFj/AM9wxMAk2CUmlbEwuKfsvrI4l6mnjkkCcerRnNzzbdB1OGgABZc7LI6R2ZylDNJP+zVH/kSf+nHqrWuozRt03gmHnE//AKcCEjZlUMXP0cg80b/TAhQIqgrPG5VgBvX7J5qw7u84ELbkOy1bmDv6JCWRWIMhIVBztvHibW0FzqMV5E6aqwAC27Q7FV9AFkqoPobgGRGDILm1iRqvvtj3IvPirhBqbLmqJo4aa7ySMFVOdzzv3AXJPIAnHuVQ+IJGqtbPWiyykGVUzBpGG9WTDi5Yep5EaW5KANSScLzyfKFsYPQ5z8RJ5e/lwSWThZdITYIvsZs16fI7yt1dHBrPITYNYX6sHvtqTyB7yMMxR/MVz2JVznO6KPcqZtjtH6U6xxL1dLCN2GMCwsBbfI5G2gHIeZxVLJnPctDDqAUzLu7R37u5LuK1pJl2G2Vaum1usCEdY/fz6tT9o/Ia8xeyOPOe5ZuI1wpmWHaO3ur5pqdI0VEUKigBVHAAaADDwFtAuNc4uJc43JS30j5J6VROFF5IvpE7yV9Zfetx52xXMzM1PYZU9BUAnY6H+7lQowiu2RPZrNzSVUU44I3bA5qdGHw18wMSY7K66VrKcTwuj48PHgtPSrlApMwZ1t1FSOujI4Xb1wPzdrycYbkbfULlqOfJdjuCDZBBJPIqxRyuCQGMcbPujmeyDyvivoiVojEo4gSNVclLnAWpiSenkp4YkEcAkRlUm1yQSLE2AFuPZvzOGQLCy517y9xc7cqx4ZQwBU3Bx6orVX0MUyGOVFdDxVhcf/PjjwgHQqccj43ZmGx7lWO0/RaReSia4/uXOv5HPHyb44WfB/1XQUmN/LOPMfkeyreqpnjcpIjI68VYEEe44WIsbFb7JGyNzMNwtWBTU/J85npX34JGQ8wPVbwZTof+bWx61xbqFRPTRTtyyC/3ThDndDXG9R/YqvlUx/Vufvg6fxcuDjDTJwe0ubq8Gkj60XWH1/aY46POEAVRDKo9WQSWDjkbEaeWvmeOGFikWVXVtSEUk4EJoyqlGT0xqZgDmNULRof93TvPjzPebLwBJqlkyDvWjh1CaqTXsjf2SRK5YlmJZmJLEm5JOpJ8ScI7rsw0NGUbBQpKVnYKgLMxsAOJJ5YsYb6JCqaGgvOybGyk01OI1I62QqGe2gLGwH4Vv79Tzw4xuULk6qoMz78OCZNrdopMvdaChtDHCq7zbqlnZhvEksCDe4JNrkk4omlcDYLYwvDYZYulk1udkCXpBzMf7zfzjj/9GKumfzWkcJpP+v1K9P0j5kB9cv8A5af6Y9Ez1B2EUgHZPql6q24rCxJdSfwDFwkcsx9DTg7H1QzMNsKuQbgYEtoAF1JOgHHFjXOO6RnhiYOqrE6QqlqUQZdAxSGKFS4UkGRmJuWI1PDe8S5J5Yoneb5QtbBKVhjMrhc3sLoNs3tdPS3Q/TQNo8MhupB42vfdPyPMYrZK5q0KzDYagbWdzH55plyvK4k62vyVQ8pjKtTSHtwX1PVg8jod29ju2U27OGw/MOquWlpHU8obMNOY4+Crc1JZm397f3jv79w29ftb19b3ve+EnNIOq7CnnjkYMmynbN5FLmFSKeLsoO1NLyjXv/EdQBzPgCRbFHfUrOxKvEYyN3TFthnsPVrl9EN2kh0JH75gblifaG9rf2jr3Y8lkvoNl7hdAYv96XtHbu/f2SlilbSLbM5BLWziGPQcXe2ka958eQHM+FyJMYXmwSlZVspo87vIc10Bk+VxU0KwxLZFHvJ5sTzJOpOH2tDRYLiZpnzPL3nUqbiSqWYELnvbnJfRKySMCyMesj/C5On5TvL7hjPkbldZdvh1T09OHHcaHyQDEE+n6jaKsyoGaITSZc29uE2LREHgRqLLfz6kYchfdtuS5HFqXo6nNsHa+fH381uybMqGrtBStUUM7D6J+sLoWtcKQzEe7Q9xvj0TBxtsoS4ZLAzpdHAbrzsLm1VNUy5ZXkzKRIjh9SjJ7Sta9jyP4SLYixzg/KVbVwwPpRPGLHT+8U79H0rGn3Wbe3SVDfasbX9/HDCxU04ELMCELz7Z6mq03Z4w1vVYaOv4WGo8uB54i5gdumKeqlp3XjNvt6Kp9qOjepp7vBeeLuA+kUeK+15rr4DCj4S3bVdJSYxFL1ZOqfp+vP1SRilbKzAhSocwnRQqTSqo4KsjADyANhguearMMbjctB8gmfZHLo44/wBrVo+iQ/2SI8Zn5SW7gQd3yL8ACdF7w0XK4ampn1EgjZ/gc0t5zmktTM88pu7H3KOSjuA/9+JOM9zi43K7engZBGGM2ChE48V6tvo+2J6uH0mdfppF7Ckaxqf0Zhx7hpzOHIY7alcni1f0ruiZ2R9T7BDNrcoJV01HcRofdi9YqHbSr6fRrXAf2intDWKBxt6soHdre45MRfsHC08fzBb2C1mR3QO2O3j+0ivKBhay6N0gaoNVVaaYsa1Iz1GmiESyYuAWNI8onsPRdfmVHHxBnQnyQ77fJTi1qQndonbpBq+szGpbkHCD8ihD8wcJym7yuswyPJSsHPX1/SXsVp5SKCulgkEsLsjrwZTr5HkR4HTHoJBuFCWJkrcjxcJjzKtizgpEINzMuAlj0R1GhaT7qjjfUabpN90tscJB1guUqoXUL7xO0PBTdoa6HL6f9mUTXb/e5/adiNVuPge4WXvxCWS3VanMMoTI74mbyH59kkYWXRqXleXS1EqwxLvO50HId7E8gOJOPQCTYKmedkLC950Cv/ZTZ6OigESasdZHtq7d/gOQHIYfYwMFguJq6p9TIXu8hyCM4mlVmBCzAhIPS9kvW0wqFHbgOvijWDfA7reQbFE7btvyWzgtT0c3RnZ334eyprCa61MnR9nApq1N76uX6KQHhZzoTy0a3uLYsidlcs7FKbpqc23Go/P0W/K9lHizoUa3CRSrMrd0SkSKfHlHf7WLXRnpBZZkVe34F2be2XzKPUNcokzbNYxfeYU9MQCd5rLHvgDiCwRtOQOLmi7iVlTOLIGRH/29k3bDVkKwrCLq6gXVgQw05g64sSSa8CFmBCzAheXcAEkgAC5J4DxwIAvsqW6Sc7oaiS1PEGlB7dQvZDW5AD6z8R918JzOaTp6rrMKpqmJt5DZvBv9t4eqSMULZWYF6ju2G0TVkwKjcgjG7DGNAqjS9uFzYeQsOWs5H5ys+gom0sdvmO5/HgEBxBPqwei/ZDrmFXMv0SH6JT+8YH1j91T8T5asQx36xWFi+IdGDDHud+4cvNW/JIANThtcukrbCthAJuLjAhVdBtOKWpMg+plXq51IuCp4MR90k+4sOeBegkG4QDaHLjBMU4oe1Ge9Tw15kcPnzwo5uU2XSU9T07Mx34oNUtpj1qJnWC9RTxOoHVgvwsBqT3gDv/1xeAFgyucHbp36HcqP7VSR03BDDLJ5aCPW2nBzp4eGJqq5duhVXUGSR5Dxd2c/mJb+uMwm5uvoEbMjGt5ABeIomZgqglmICgcSSbADxJwKTnBoJOwWl0dnEUaM0rNuKgHaLcN23Lxvw54mxmYpOrq2RMzXVgSbuS03URsr5hOoM8o/cqeCr5cu83Y20GL5H9GMrd1i0VK6ul6aXsjhz7vdIhOFV069wQs7KiKWZiAqjiSdABg32Xj3BjS52gCvXYLZJaGK7WaeQfSN9n/DXwHfzOvcA9FHkHeuMxGvNU/Tsjb3KasWrOWYELMCFmBC11MCujIwurAqw7wRYjARdetcWkEbhc3Z5ljU1RLA3GNiAftLxVvepBxmublNl31NOJ4myDj9+KgkY8V6sLP85lkypa+H69ENLUsPWCsQvWXGt9bju62/s4da+7L8VxlTSiKr6M9km48D/WQva6qaipcuoIW3XVfSJiLGzNcAaixG8Zf4BiLjkYArqeJtXVOcR1f6yPZNnklVRPNJb0ijeO0gAXfRyAVNtOG8bCwuFxKF5cNVVitEymkGTY8OSs6gl3o1bvGLllqRgQhO0W0VPRx78z2J9VBq7nuUf1Og5nEHvDRcpimpZah2Vg8+AVMbW7aVFaSpPVwX0iU8fFz7R8OHhzwnJKX+C6yiw2Km13dz9v66WsVrSWyngeRlRFLOxsqqLknuAwDXZQe9rGlzjYBPFL0WVTIrPLFGxFyhuSvgSNL+WLxTu5rGfjsIcQ1pI56JCxQttHNmMi9IMksl1poEaSZ720UFtwHvIB15C57r2RR5ys/Ea4U0enaO3umDLenekVFQ0UsYVQFWNkYKALWF93QYfAtouMc4uJJ3Kg5z0wQTi0fWRj766/ys2BRS+ufR1BIEu8bEkG40HE6jAhBcwkjYHddD5MDgQpGX5h6TTCl3WeoiP0G6CzOv2LDU2At5BTyOIPbmCZpagwuvwO61Vmy8kQDVjrCD7AIL++1wD4DePljwNDd1bLUPmNmhRv2lHECtPGF73bVj/wA+PwwF/JDKXi8p06KpHWlzasY3IhWJGPewckfEx/HHjjZhK9hjD6pjBzCXQMIrt0U2WqVjq4pG4Rln/hUkfzbuLIRd4WbisoZTOHPRPtdULQBq+dFOYVClYYyNYl+0/jwuePBR7RwzI8M8VztBSSVbg0k5Bv7DvVaVNQ8jtI7FnYksx4knnhI73XYsY1jQ1osAtWBSVy9Guxno6ipnX6dh2FP7pT/xkce4ad93IYsup3XJ4piPTnoo+yPr+uSfsXrGWYELMCFmBCzAhZgQqt6Zsl+qrFH+FJ8yjfHeHvXCtQ35l0WBVNi6E+I/Kq7Cy6ROPRrWoZZaKb6qrjKEdzAGxHcSCwv3hcXQOs63NYuNU+eESjdv2PskvaKWojrZFqwd9T1asQQrCPsgr4H1vz4tmaSs7CahkZsd1YWzsAGVxINWrKoE9+7FqPdvRj+LEoG2aqsZm6SosNgAPyrdo491FHcBi5ZKRNsOkmOG8VJaWUaF/wB2nl9s+WnjywvJOBoFtUODvls+XRv1Psqlrq2SZzJK7O7cWY6+XgPAaYVJJNyuoiiZE3KwWC0Y8ViN7M7LVFYSUASJfXmfRFtxt9o+A95GJsjL9kjWV8VMOtqeQ/tFZGz2Wxwgx5et2OklY4uW8IwRa3j6vDRr3w4yMM2XJ1dbLUuu86cBwRobF0zayKJHPrO/aZj3knU4sSao3KcteolWKPidSx4Io4sfAfMkDnjOa0uNgu9qqllPGXu/yrSp6iOngFNAv0YBBJ1Lk+szd5Pw5cMPtaGiwXEVE755DI/cpO2woKcUs8hghDCNrMI0BBOgIIGhuRiSpVXbK5ak0j763VUvxI1uAOHvwIRyHKkV2SlSSSZ0ZVjW7HXnYC/drwGBCP5T0TJAgmzepWnQ6iFGDSt4aAi/gob3Yi5wbqVdDTyTHLGLohXbZ09LGYMqpVp0OhlYXlfxNyf5ifIYoMxPZWvHhTY9ZTc8h7qusxqnkcvIxdjxLG5/58MAXr2tbo0WUORrDEgFS9waLq0ckgMGzjbws1VV3sePYI0//Tw8Tgm0ZZeYQ3pKvNyBP4SrNIFFzhMC66uSQMFynTYzKEpIRm1aOI/skHNydRIR7rjuHa47tmxaNtyuXmfJXziKPbn+Ut5vmktTM00zXdj7lHJVHID/AN+JOFXOLjcrpoIGQMDGDT+1UPHiuVm9F+xm9u1tQunGBDz7pSP8vx7sMwxfMVzmL4jvBGf/AGP491a2GlzizAhZgQswIWYELMCFmBCgZ5li1NPJA/CRSL9x4hvMGx92IubmFlbBMYZGyN4Fc4VNO0btG4s6MVYdxU2OM4i2i75j2vaHN2Oq8wysjK6mzKQynuKm4PuIGBevaHtLXbFWXtRnUYjp656dJ6SrUJUxEDsSKLAi+l7Bl1t9WtiL4eMnVDlxbaI9O6AmxGy05dlMUpilyuo31hDH0OY2dA1r7pJv7IHauPv4k17XbKmppZoHf7g8+B80F2023rJnalkjelUCzxHR38WPNT3Loe84omc7bgtrCKentn3d9ko2wsuiX1FJIABJJsABckngAOZ8MCiSALnZOdJstBRxipzV9wHWOlU3lltyIB8tARa4uRwxeyGwu9YVTirnu6Kl1P8A29vcoomaSV9MZYvooaVxvUSAWMfJzb1iAN6w7PZYWJAJZY9rtlh1dNLC4GTjrdWVkVRG8KtHaxA4YmlERwIVf5ZssKKktoZnsZWHfyQfdX5m5xXFHkHen8QrTUyX+UbD8+JQg4sSCWtti8sBpYUaSaZlVUUXJswY+6wNzyGBC17OdHQoUMuaVawBwPoIyGla2tri/kQob8QxFzw3dXwU0s5tG2/2U+fbWOnQw5ZTLTIeMrANK/K5vfXxYsfLCz6g/KugpcDY3WY3PIbeqTMxrXdi8js7nizEkn3n9MUC5NytY5IWZWCw7kIka+LQFnvdfVQmBJsASe4YtCzpXAG5RrI8pCxvWT6Rx36tftuNL+Njp5+RxaBbUrOkkMrsrU97ZSdVluVwnQmIzOO5nCtb4u/wxTUHYLVwJob0kh7h/eigbD7NxzK2YVvZooT2VI+vYG27b2lB0t7R7PfgjYGjM5RrqqSokEESjbVbQyVs5lfRRpGl9EXu8zxJ/oBhd7y83K3KKjbTR5BvxPM+yD4inE7dHGx3pb9fMv8AZ0OgP71h7P4Rz7+Hfa6KLMbnZY2K4h0A6OM9Y/Qe6uxRbDq5JfcCFmBCzAhZgQswIWYELMCFmBCpnpeyXqqlahR2Jx2vB1Fvmtj+VsJzts6/NdVglTniMR3bt4H2KQsULcTtsOBV0tVljmxkXrYCfZdbH/MEa3dv9+GIDcFhXP4zEWOZUt4aH8JHpZXRr9qORCQbEhkYaEXGoINxilwLStaB7Z4hfUFOe0lQ1ZlEVXNrPT1HU9YRYyIwGnjxX3oe84vvmjuViOiFLXhkexF7cv63ogezuz1RWPuQpcD1nOiJ5nv8Bc4oYwuNgtqprIqZt3nXlxKeYaRMvhkegiFXVIp36hh2EA9bqwPWt3KeRuxI3cNiMRi41K5mWtfWyhjzlbfb35qsquolmkM08jSytxdv0FtAPAADCrnly6Smoo4G2aEV2Wz16KoWZbleEiD20PEefMeIHjjxjy03UqylbUxFh34HkVaeVVK0k6CM3pKkb8DDgpOpj8LXuB3aeycaANxcLhnscxxa7cJ5Vr649UVVY6V0cWlpGH4JQ3yKr+uFhU9y6F2AO+V48x/lCqra+ja5CzJ+JFI/lY4kJ2lKyYNUN2sV62GzEMM0qYDvTxQL1V11UHrCxAI11VSR9wd+Jud1CWpOKny1DY5dASLpHnneRi7szseLMSSfMnCBN9V3DGNYMrRYLTI1sAQ51ghs8lzi0CyzJZMxTHl+yBKCSZwFIuFQgk/m4fC+L2x31Kx6jEA27WDXvWuky1ZZTDEAka6yuOS91z7RsQL8NTyti4ABZT3ukOpUPaOtFTJFTQ9mEMsUQHA7xCb1v08PM4qzZnLS6DoICTuVYmeZOMzzJgW6uho4kWaS9h2bv1anvIIufZGvMX9LbuuVVFUlkHRR9px/SA7ZbSCqZIoV6ukhG7DGBYaC2+Ry00A5DxJwtLJnPcujw2gFMy7u0d+7u90t4qWmmPYjZV66axusKWMr/wDAv3j8hr3XsjjznuWdiFc2lj07R2H5UvPNqM/o6qaOmpJPRI3KQJ6IWjCL2QQyKCd4C+rHjh4ADZcY5xcS5xuSotL081iErUUcTMpIYKzxkEHUENvWPLHqimDLunylawlpJ0Y8oysn6lCfhgQnPNekfLKaoemnqOrlS28DG5AuoYahSOBGBCIUO2OXTECOtp2J4L1qhvgTfAhGo5AwuCCO8G+BC9YELMCFmBCzAhANuMk9Lo5IgLuBvx/jXUD3i6+THFcjczbJygqfh52v4bHwK57wgu6UzJsxanninTjGwa3eODL71JHvx605SCqaiETROjPEf4TjtLstDJX+k9ekNHUxioMhtx0392+lzdWuebnQ8MNvjDzfguYpcQdSsMZF3A2AXvNjTVNLAYbrllNNZwAeskc9lXbmAS9rWuetB09mQa0tsNkq+eeKbpZO0RxTll2XPPGqKgpqQDswpoXH+IRxvzUaam5bjiwAAWCTkkdI7M43KaKSjSNQqKABj1QVI9I2zHodRvxj6CUkpbgh4tH/AFHhp7OEZmZT3FdjhVb08WV3ab9e9KWKlqJ42CzJZo2y2dt1XO9TPzikHasPM6259oe1bDEEluqVz+NUWYdOzh2vdMse23o46iosk0fZcE8xzHeCLEHmCMNrmlUWMxfRlhGBeELdkuaT0NQKmnI3rbro3qyKeKt8Ab8iMXRy5d1j4hhwm6zd02RwZfmlzSkUdZxamkP0ch4nqz8fVHLVRxxN0TX6tSUGJT0pyTi458f2kfOqWWGQxSoUYcjwPiCNCPEYrDC3daLqpswuw3CG4kqVNyquqd9YICWMhsEPq95b7thckjkNcWR3vos6vEeW5GqM59UrTRehxNdjrPJzYn2f005AAd+JSP4BVUFN/wCV3kh+xeRT1tdFHCLCNhJJIR2YgpuGPebjReZ7gCQRt4or5geoE6babQx7goKPSmjPbe+s7g3JJ5je1vzOvADFM0l+qNlq4Vh3RATSDrHYcv39knYoW4imzmRS1k4hi05u5Gka82Pf4DmfeRJjC42CVq6tlNHnd5DmV0DkuVRUsKwxLZVHvY82J5k4fa0NFguInnfM8vedSppOJKpcj7OJ6ZnMRsCJqwSEEaFTJ1jaH7t8CF01V7E5bIwdqKDfDBgyxhWuDcElbE6jngQuaekFzU5zVKDq1T1QJ+6RF8NMCFZuYdAEDa09bIv/AHkavf3qU+NsCEZ2P2cOz9FXVE7rLwcblxvBAQqm47JLMRz4jAhVzSbd7QZnUMlG7AgFuqiCKqLcDVn8SOLc8CFOqNrNqMv7VSjtGOJkhV0Hm8Y097YEJz2q6WZMvNNHPSB5ZadJZlWTcETOSNwXVr2sefdrgQvND08Ze2kkNRH47qsPk1/lgQmGh6VsnlNhVqp/xEdPmV3fngQqy25o4kqTLA6SU9ReWJ0IZTqQ6gjTRw2nK4GEZm5XLs8JqempwDu3Q/j+7kvYqWmnWgh/aGUTUtt6ejImg7yupKj3dYv5kw1EczC1czicQgqmz20O/jx91P2foeqoEpiQZquWOUoP3UaFWBbxJS35jb1TiyFhaNUhiVU2eQZdgLK2aSPdRR3DFqzltwIQzaPJY6uneCTgwurc0Yeqw8j8Rcc8Re0OFlfTVDoJBI3h9lzvmFFJBK8Mgs6NusP6jvBFiD3EYzyCDYruopWysD2bFaUYgggkEEEEcQRqCPEY8UyARYq1ss6RaJokNVFee1pCIgQSNN69uYANuV7csNtnFtVy82By9Iejtl4apWpKnI51lYU1dEsWjsCGC3vr67X4HlgMLN1BuK1gcG6EnuH4WpaXIn0TMpoyeHWwNb3nq1HzxHoWcCmhi1U3tRj6rYmyVJJpDm9G55KxCn4b5Pyx58PyKsGOgdqM+v6QDabY2aKxEkEmtw0bk+R9UW+OJMie0paqxGnnbaxUalzicL1VfCZ4vt3BkTxve7eejcdTwxda41WQH9G7NG5R83ylFj6+nlEsPwdPBhp+gPhzxUYyNlpxYg1ws/Q/RM0GXfsii66UD0+qFkQjWCPjY+PAnx3V9knE3HILcUvFG6rkLj2R/WSXluXz1c608ALyyHiToObOx5AcSf1Jtitrbp6pqGxNsFZGeVcOW037Mo2u5/2ucaM7EaoO7ut7I04kkeTSW6rV7hdAZD8RLtwHPv8AZJGFl0qlZZl8lRKsMS7zubAfqSeQA1Jx60EmwVU0zIWF7zoFfuyWzkdDAI01c6yPbV2/oBwA5DxucPsYGCy4isq31Mmd23AcgjmJpVB9sK7qKGqmHFIJGHmFO787YELnToLo+sziFuUaSP8AyFB82B92BC6hkcKCx4AEn3YELj3JMubMsxWLe3GqZXJa192+85Nri9teeBCsiXoazWD/AGSvW3hJJEfgLj54EK5M6yJamhejkY2eLcL8TcAWfXiQwB144ELmWsy/M8hqw43omFwsqi8Uy8StyLMDYEqdRobA2OBCuDo86YIa1lp6pRBUMQqMD9FKTyF9UYnQKSQdBe5AwIVjV+VU84tNBFLpb6SNW07u0DgQuY+mqngizSSKnijiSNIwVjUKu8V3ybKLXsw+GBCsrLOhWgmpKd2aaOZoY2co4sWKgnRlPM8BbAhGNsNjEiymOKG7GjXeUkC7Lxkvbv8AX81GKZ23bfktXCKnoqgNOztPZVFhJdijexmc+iVkUpNkvuSfgbQn3GzflxON2VwKSxCn+Igcwb7jxHurP2ayeOmrqhDrch4r8o29VR3KpDIB3JjQXDJ4wIWYELMCFXvSxsx1sfpcS/SRD6QD2043811PkT3DC88dxmC2sHreif0L9jt3H9qnsKLq1mBepi6Iisr11C3+805ZSeTJdfj27/kw63rNIXG1J6Kdsg4JaaAcCtiNCO48xhK5XW9GxwvZRqmFLcBibSUtNFGBshUtIvEC3li8PIWRLSsdwWtnkUaSOPzn/XEw9JPpGhP3RfkKoj5vWk+jxaQoeM8inQ25hW0F/a103TeZcGi5ScUDppRHHxQTa/O5KmZ55T2m4DkgHBB4D9bnnhVpLnXK6SWNtNCGM4f109UKLk2XxlBeurUDvIbfQoQDur5XA8WuToAMXSPyNsN1mUFL8ZKXv7I+vd7pJJvqdSeJPE+OE11gAGgXzAvVMyzNJqdi8EjRsRYlbajjbUcMehxGypmgjmFpBcJhpekjMU4ypJ+ONf8Ah3cWCd4SD8GpXbAjwPvdGKTpbqB9ZTRP+BmT9d7ExUHiEq/AIz2XkeIv7LdnXSBR11NJS1MNRGkoAZomQkWIbQt4juxMVDeSUfgMw7LgfUIb0e02TZfVNURVk12jMe5NEQBdla+8q29kD34kJ2HilX4RVt+W/gQrFzfOoKmlnjpaqBpXidY7yqLMykC/Mce7Fge07FKPpZmdphHkqFoujvPaKZaingu8dykkbxONQVJCkknQkariSo2RtukTaSn+upmNuclKwHxTdGBCaqrpeemoaGeogR56nfZ442KhI1cqri+8bsN0gE62bhgQpH/3gyWqiaOpWQKws0csO8D/AA7wwIVDZ2kDVrjLxJ1RkAgBvv6kWA5+twvra19cCF2NShgih9W3RvedtfngQuTdvJTV5xUheL1JiU/hIiB8tBgQutY0CgAcAAB7sCFjqCCDqDoR34EA2XOm1WTmkqpYPZU3TxRtV+A081OM57crrLu6KoE8DX8ePj/aoTiKbVrbP5qZqKCpveWkbqZu8xmwDHv9g3Pc+HoXZmri8VpuhqDbY6hWRTShlDDmMWrNWzAhZgQvhGBCofpC2Z9CqLoPoJbtH3Kfaj93LwI7jhCVmQ9y7LDK34iKzu0N+/v90q4qWovGx2Z+jZjSz3solCvr7Mn0bE+QYn3YcjOq5TEGXZfkmDpAofR6+oW1lLdYvlIN/wDzFh7sUSNs8hbWHz56Vrjw09P0k+aS5x6BZVSPzFGaLZaSWFZY5EIYG4a4IsSCOBB1Hhi0Rki4WbJXMjeWOBW3ZfYmWrq+pkskMY36iQMOyn2QeTNYgX4AMeVsTYwjdK1NY14yx8UY212hWodYoBuUkA3IUAsDbTft4jQdw8ScLSyZz3Lew2h+Gju7tHfu7vdI+YrvXGJM0UKwZ9FZeX1UGb0dPHJMtPW069UOt0ScCwBU8ybA6ag72lrHF8jOkFwsajrDQyFrhcFaajo2zBRdUjlHfHKNf4wuFzA9bTMZpXbkjxHtdCKrZWuj9ekm9yb3+W+IGNw3CaZX0z9nj7fdCZkKGzgqe5gQfgcR23TTXNd2TfwXkHApLMCFmBCzAhfCMCFupqh49Y3ZD3oxX9CMAJGyg+Nj+0AfEXRel2wzCP1auX8xD/5wcTEjxxSr8OpX7sHlp9lJrNtqidQlVDS1Kj++gBI8ipFj4jExO9KPwSmO1x5/pCimVObyZZu95hqZB/K1wPccTFTzCVfgA+V/qE07I5lkNJIJEo5klHCR/pN3xXtndPiFBxMVDeKVfgVQOyQfO33Vh0u32XScKlVP31Zf8wA+eJCZh4pR+F1TPkPlr9lUGz3RjUenwVHplHUIs6Su0ct3IDhzdd21zbhfniwOB2KUfDIw2c0jyXQ2PVWswIVb9MmS78UdWo1iO4/irHQ+5v8AOcL1DbjMt3A6jLIYj823iP0qkwoupTZ0bZosVV1MmsNSvVODwub7nzJX8+LYXWdbmsnGKbpYMw3br5cfdWrspMyh6Zzd4WKkniw4q3vUqffh5cemHAhZgQtU9SiC7uqjvZgP1x5dSDXO2CU9sMzyypp3glq4QTqpDhijD1Wstz594JHPFUjmOFiVoUUVVFIJGMPpuFRxFtND4jgfEeGEl2Y1F0BqkupGGGmxWBO3MwhPvSBmHpUFDWDjJCEkI+0vEe5utHuxOUagpPDJSGui8/dI+K1opq2RrpHQ0kI3pnk+jX8Q1ueQG6WJ5C+L4jpZYuJNGcOTPtbXJRwfsyna7HtVko4yObXTy0FxyUKuuuKp5PlC0cHoP/O//wCff2SK3DCy6I7Lxk+VtUzbguFGrt9kf6ngP/Y4YY26wqyoEQLuPBOW0NBClOewAqrYDuAGGgLLmXOLjcqHtfkq0K0ccDPFL6OrStGxQsxCi53SOauffiiaQtIstvCqFk8bnPHGwQ6l2ozOL1K6b85D/wCcNisTlOvwWM7KxNk80zippRKzwSgswAkh4hbC/YZRe+9hiN2dt1hVkHw0uRpW+rp2P12UUsneyHcP+Qn549MbTuFBlZUM7Lz6oVUZXlhNnoKyA/ajcMo+Lk/y4gYGck2zGKtvzX8QEGbLckdikeamJxoVnTge4ndQfPEDTjgU0zH5PmYD4XHutkewfW/7LmFHP5SWP8u/riBp3cCm2Y9Ee00j0PstFX0eZkn7gOO9JFP6kH5YiYXjgmmYvSO+a3iPa6DVWQVcfr00y+PVsR8QLYgWOG4TTKynf2Xj190NLAGx492Ipka7L7gQswIWYELMCF8Kg8hjyyLqXSZlPFbq5pUA4BJGUfAG2JAkbFUvgif2mg+QRml26zFOFUx8HVG/Vb/PExK8cUq/C6R3yehKnzdJFXJE8M8cMiOpVuyytZhbiGsPhiXTOIsVQMGhY4PjcQQb8D+EmjFK11gJ4gkHkRxHiMC8IB0KsvP9pZ1pabNKbc3mAhqQVJCsL2awIA7W8NeIdMO5yWZguPFHGyrMMl7cPwlio2/zJ/8AeSo7kRB/w3+eFjM88VvswqkHy38SUKqc8q5PXqZm85Wt8L2xEvcdymWUkDOyweiHFRe9te/EUwNNl9wIX22BFkGkHHF4WM4cE25RRtLlM0fFqdhMo7ke7X+Uvxxe7VixIXZKuw4myUJTYXxQNVsvOUXVmbP065RQiqYA19Yn0QI+ojNmub89VJH2t0eyTi17ujbYbrMpKc1093dkb+3mk5mJJJJJJuSTcknUknmThNdaAALBa5jpj0bqMhsFHo80mhuIpCoJuQLanhzGLwSNliSwxym7xdTaTNqmpngpnk3lmmjjI3V4O4U6he4nFjHEnVZ1ZTxRsu0Jp6UKrfzGUDhGqRj3LvH5scUTG7ytzB48tKDzJP4/CVMVLTV77J1UFFldM08qQoYw5Z2CgmTt8+J7XAYfiFmBcPiL89U899vTRJ20/TnSx3Sjiaob7b9iPzA9dvKy+eLEkguzsm0eYVUFSymKnWRX3WAiiZbi67vryBlvYne48eGBCSOlfZz0DMpFVQIpCJohbSzHVLcLBgwt3W78CEzZ1t/lQgRKagDSFF3lICxRsRqoA7T2OnK/fgQnDo6yCapoxLKk9HKHYBVMkQZdGVlW993W2vHdwITO+SZhGPo66QAfbCP8S6k/PAhIu1nSY1KerdqOta9igjvu9+84YoD90Anvtjy11Jr3N2NlKyWshrYBPJkSoj33Wjk3WYfaACLoTfW/LEDEw8E0zEKpuzz9/uh1VLkBYq/pdMw0NiJAP4TJ/rioxR81ox4jXgXLbjvH+F4GR5TJrDm8a+E67v8AmKfpjwwA7FXjGpW/8kfpcfe63Do5qHG9T1FLOvIpKbn+Uj54iad3BMMx2A9oEehQ+q2EzFONMzeKMrfo1/liBieOCaZilI757eIIQeqyyeP6yCVPF42UfMYgWkbhNMqIn9l4PmFDDDvxFXL7j1CzAhZgQnXo6kSdanLZT2KmMlD9l1HEeNgG/wDDGL4DqW81hY1CQ1s7d2m3skdY2Qsj6OjMjeBUlT8wcVPFjZadHL0sQcvcY3jZdT3DU/LERqmXEN3NkSptnqyT1KWc+PVMB8SAPniQY48Eu+sp2dp49UWpej3Mn/3fc/HIg/Qk4mIXnglX4vSN+a/gCpw6Ma3nJTA9xla//wDPHvw7+5U/65Tcneg91WtQh3rDieHvx41TmGUkq19k4khr0gb6uogaAjkSq7y/JXH58O20suSLznzd91W+dZYY2lhPrRsyH8pK3+WEAcrrLtJGNngDhxF1YVRTftilp5qZ09JgiEU9OzAMLcGW/IneIJ0IPG6kYYlZnsWrEw+rbROdFKNDxQObYfMV40j+5kP6McL9E/ktoYnSH5/ofZCMyyKrjB36WdfExPb42tgDHA6hEtZA5vVePUIBJA6+sjDzUj9RiyySD2nYpj6KqTrc3puYj35D+VCB/MVxbEs3EXaALM+qutqp5PtyyEeW8bfK2FXm7iV09KzJCxvID7KCsZYhRxYgDzOgxFXOcGguPDVWZtb0QmtrlmNR1dOIo1Ki7PdBuWUHsopUKb6672nfpjRfPXOzEkpC6Zej6HLhTy0obqnBjfea53xdgxPey30AA7GBRVp9CW0PpeWojG8lOeqbvKjWM/w2XzQ4EId/9QGz3X0C1Kjt0zXNuaPZWHuO43gA3fgQkToR2ny+jFR6YIkdbPFMU3pCPVaNSAW+yQB3t3YEI3tJ04ySN1OW05udFkkG85P3Y1v7rk/hwIXnpB2brcw9Gqpp46SJqaLrkqJGTdlBYuFjsSTwIFgceFwG6sjifKcrASe5LdBleWUhDBGr5R7Uo6unB7xHq8nkxAPdih1QPlWzT4HI7WU27hqfZTM62nq6rsyync4CNOzGB3bo4j8V8Lukc7crcp6Cng7LdeZ1P94IKUHdiCbLGleGp1PEY9DiqzAw8EQyXYqoqiDBAxH956qDx3z/AEucWsLzss2rbRRf8hF+W5TK8EeXD6fOZ2mH+7U0rvY8d1rmw/NuYYF29orAkyTOtBH5rMg23zSoqVhhn3Va4QTojliNQCVVSCQDpvHuwCUE2RJh00cZkPBEqvaWuCqKnLqOokMrxMgXcIZb83LAg2JB5ixxMgEahLxyPa7qvI9fwpD0tMy71Tkjwn/AmB/RkHxxDomHgmhiVVHtJfx1+6F1FFkZ0M9TTt9lgHt57oc/PETTt4JhmOVA7QB8vZav+ilC5AhzemufVSWyMfDV7/y4gaY8Cm2Y+PmZ6FTsuho8oZqmeqinqFUiGnhNzdha55gEXFyAACeJtiTIshuSl6zEHVreiiabcUEy7bWOMEjLIHlYlmllYFmZjcm3V3FyTpvY8dKy97KUWHVZYG57Dlqnemz6uZKN09HijqQ19yI3Qru2W5Yjm3L2cXsNxdZFTG6KVzHG9kzrktSw+krJT+HdT5oqnElQvv8A0UiP1jySfjkZv1JwIXtdkKMC3Up/CMCFz7lVLv1UI++GPknb/phGLUgLr8SOSJzu5OeeyGLq6hb3hkSTTidxgxX3gEeRw8uQULpVphHWtKvqTokqnkbjdNv4QfzYSmb1/FdbhVQDS2Pym35Vfux3t5SVYcGUkEe8a49aSFCdjJdwnvZhqiWAOKypDAlWtPJxGo9ruK4ZY64XPVcIiksFuzisr1Xd9NlI+9ut/mUnE0sghz6sjHaqFb8SJ/wgYEJh6M8xkaWvrJQt6akO6VUi+/duZP8AdW9+IuNgSrYWmSRreZASao0xmr6AjWxlJ1tfTJ/iq38H0h+S4nGLvASlfJkpnnu++i6KxorhUudIeQenZfPABdyu9H+NO0vxI3fJjgQqE6Ddo/RcxWJzaKpAibuD3vG3xuv5zgQulsxo0mikhkF0kRkYd4YFT8jgQudYujyhpGP7QrhIyk/2ekG8xsdN5zolxxUgceOK3StanqfDqifVrbDmdAicW1CUylMupYqRToZLdZOw8XYfLW19Dhd1Q47aLcp8EiZrIcx9AgNXVSSsXkdnc+07En4n9MUEk6lbMcbIxlYLDuWnApr3FGWYKoLMeCqCSfIDU4FFzg0XJ0TRRbB1G51tVJHRwji8zAH+G4+DEHFzYHHfRZVRjMEejOse7b1X1s9yik0pad6+YfvZuzED3gEa25dn82LA2Nnes901dVaDqju0/aD53tbmFZ2ZZzHH/dQ9hLdxsd5h4EkeGIunPBMU+DNGsmqCw0qrwGKC4lbEdOyMWAUmGRkZWQ7rKQykcQQbg+4gY8VrmhzS07HRWPta4nio6sXTrzG8gViLOn0VwRqbiS3ki40WOzNBXBVMPQyuj5GydaTZOkZVZow5te7an54kqFOXZulH7lfhgQtcuytI37pfhgQqm6VMlip6uPq1Cq0I4DmHa/yK4UqLhy6jAw10TuYP4SgMLrdCsrJJb5bQOP3VW6nybrT+pXDsHYC43GG2q3d9vsFa0Juo8sXLMXvAhZgQqD6PsuMk8r20jiP8TkAfIPhOnHWuuqxx4EIbzP2U3aDM6dEZJJBvW9Uat8Bw99sNFwC5yOmlk7IQ/aKX0rJqGpF7wM1O1+IAO6pPjZIz+fFUouA5aGGuLHuiPH8JCxUtROHRwtRM8tPAm8TZyx0SPiCWPK+mguTbhocXRLIxKxLeaJbRU1NE1qiuEjDikA4eBPa+e7iZkaEoyjmfs310QFtoaeL/AGelXeHCSU3b+p/mGIGXknGYbxe70TJs7Vv+xK6ocjfqJ1iFtOyu4LfOTHkh/wBte0EI+Oa0bDX0F0n4TXXp26IqTfr9+31cTt5FrIPkzYugHXWRjcmWmy8yPortw6uRUetrooVLyyLGo9pmAHzx4SBupsjc85Wi57lR1Z+xaepkqIKd6uVpGkUysVgjJO92VABYA8Lg+eKHVAGy2KfA5n6ynKPUqJnm2NbVXEkxVD+7j7K+Rsbt+YnC7pHO3W5T4bTwdltzzOqAAYgn1mBCJZPkNTVG0ELOObWsg82Nl9174k1jnbBLT1cMH/I63dx9Efm2doKL/rGtBk/7NT9p/Jja4B7yFHji4QAdorHlxmSQ5adnmfZQqjpCeJTHldHHSqdOtkAeZvE8gfMvifSMbslDRVdSc0p/vshUO005ffqqeGqbvmLk+QuxCjwAtg6cHgpf6PK3Vrkx5FtBBUyiFcjpSxBPZk3NBx4QnHrTG82sq5o6ymZnzm3ipNBU5VUcMtcNrdI6jt6Gxspdbi/PhiRhYeCobitWPn+gXysospQXkpcxg8bBwPEkM/zxEwsCZjxarOxB8h+kPjGz9970yrYD2OqN28LiIAfEeeIdFHzTP+p1pFhGL/3ejwrxX08sywmGmg6qGlQ8SesQsTbS+iaC9gOPHF7HA7bLGqonxuvIbuOpVo5WCIkvxsMTSqlYELMCFVfTdDrSv4Sr/kI/rhWpGxXR4A7/AJG+B+6rHCy6NP2y8t8om74quJh4BjGP6thunPVXKY6204dzH5VuZc940PgMMLEUnAhZgQuYY62VEdI5HRXtvhWI3rXsDbUjU6cNcZoJC7+WFj9XC9tkDkWxIxaFmvFjZPHRovpFHmVAbFigniHey6H5rD8Ti/tMIWM89DVNedkkTR21wuDdbksZaU+7KEpkNY8N99qkLMRowTdTTv3df52xc+4j0WTShrq8Z/LxtokSoOuKW7LXmN3LRIdDiYS7zZqsfMU6nIsthvrKzznxBLMPlKnwxKc9UBLYI3NO+Tu+5/SUsKrpU89GmfUtEtRLO9mbcVEUEu1t5joOA1XUkDTF0L2suSsbFqaapcxkY0FyTwUnPOlWd7rTRiFfttZn8wPVX+bHrqgnZV0+Bxt1lN+4aD3+yRa+ulmbfmkeRu92Jt4C/AeA0xSSTutmKFkQysAA7lHx4rV9VSSAASToANSfADngXhNhc7Joy3YWqdOtm3KWEamSc7th37t7j827i1sLjvosuoxeni0b1j3e/tde3zPJqPSKN8xmHtN2YAfeLEe5/PFoZGzfVZrqquqtGdUd3vuhOe7aZlVKU64U8XAR043BbuLX3jpyuB4Y8M/JWR4LcXedUVgNNTwQQ5vTheuUtHUQraWIaG7qAb8Rew4mxU2JxaQHN6yy43SQTHodbehWjPdj5YE6+FlqaUi6zRa2H3gCbeYuNNbcMLPic3XddHR4pFP1T1XcvZLYxUtROHRR/wBZJ/3cn6Yug7ayMa//AC+Y/KWc+CPIq2H0caL7z2yfO7fLHszzn0VWFUrHU3XG5upWWbUV1N9XUMyj2JfpF8hvdpR4KRjwTuCnNg0L+zorAqEjkmSGqyyCWcxJIzxEp61xutxPEHixw1ka7cLnG1MsRIjebJloclkkMfWIkMMWscEfqqftE2G82p1sOJ04kyAtoEu57nnM43KaVFtMeqK+4ELMCFXvTTDelhb7M4HuZH/qBheoHVC28CdadzebfyFT4wouqTrsNJehzRPswrKB4oHY/wCVcM0x3C53H2/8bvEK29nJd6njP3Rhpc4ieBCzAhcuBbkAC5OgA4nwA54zF9FJAFyph2Pqj25E6hOO9Kd3T8PEe+2GGRuIXP1ddAx3VN/BFNiZIKLMqZo5jJ1jdTIwWyWlG6oB/H1etzw5YYa3KsOoqDMb2sh21mV9TV1EPJZG3fwt2l/lIwk7quIXX0zhPTsdzH20UXZTaaTLZmO71lPKN2eLTtDhvLfTeFzodCCQeRDEbxsViV9GQc7d192oyhEInpzv0smqMPYv7J525C+vI6jXx7Mu2ylS1fTaP7Q+qWqu+6QOJ0GPG7q2pdljJVp9KAEctLSrwp6ZF8r6fogxGoPWAV2BMtC5/M/b/KSsULbX22BC+YEKdlWUVFS27BE8h57o7I82NlHvOPWtLtlTNUxQi8jrf3JMkuytJSANmdakZ49RD25T8iR57tvHF4g/7FY02NFxy07L959lGbb6ODs5ZRJAvBp5hvzEc7C5seerMPDEs7GbBLGkrKrWV3l/aIxmeRVMM8VZVyvVxD6xWO8NxuLIo7KkaNZQL2tzwysOxBS1tjs+tJUWjsYJR1kDDUFDra/PdvbyKnnhCVuVy7PDKhs8IPzDQ+/mtuwuRiqq1V/qYx1kxPDdX2Ty7RsPLe7sEbMzrKWJVXw8BI3Og9/JEdoqP9rLLmVM/WhCYzDbtRxoTusFtfUXcg/aNuFsXzNJF2rDwqeONxjlFr8fdKuQ51VUDl6SSwOrRNrFJ+Jb8fEEHxxUyYhaVXhTJNW6FNSSZbmeqsuX1ntRuR1Ep5lToL+Vjx7J44sMbX6t0SUVdU0ZyTDM36jz90b2ZyWLLpGnkqIpqjcKxQxNfjxdjxAsONgAL8TYYlHFkOYqnEMTFU0RsbYX4qsEJN2JuWJPxwq43N10lLF0UQbyRXZqg6+rghtcPIu8Pug7zfyg4GC7gF5WS9FA9/IfpOce3kMWa1jSxuyCTq1ZCCV6sBCN0203gx0PM6YbdKGmxXLU2GPqI87CL96sXJtqKOqt1M6M32D2X/haxxNsjXbFKz0c8H/I0jv4eqMYmllmBCzAhJ/SvDvZbIfsvG384U/InFM46hWng7rVbe+/2VGjCS7JOvRV2p6iHlLTOPgVH6McX056xWJjrbwNdyP3H6VldH9Rv0UR+6P0w4uVTJgQswIVd5TBMBu0FHFSqdDLIN+Yj9AfMsMRaxrdlfPVTTG8jiVG2g2QZgJKmV5nGvbNwPJfVX3AYkqEt53l4EJMY3WXVT3Eag+42wIX3pMAlalrVHZqqdW8ioBsfGzqLfdOE6gWdddTgUt4nR8jf1SLUQhhikGy1pYg8WRDZecwxurqXpy27Klr7oYX3wOfO4HELpqBh2M5mrkK6F0E1x4qcdj2izChAIenmniaOQG4Khg5W457oNjz+NvAzK5WS1IlgPPipnSLUl8xqT9llUeAVFH63PvwtKbvK6LCmZaRluNz9Ut4rT6Zsp2HrJl33UU8QFzJOd0Ad9vW+Nh44tbC4rNqMWp4dAcx7vfZFY6LLKXUJJmMo7huU4PnwYfx4vbA0b6rDqMZnk0Z1R3b+vtZD9odrcxHV3K01IrLvxU67p3LjeG9617X9XdHhi9ZJcXG5Q/bnZtKSsdUHYkVZFPfvaNrz7QY+8YSmuHLrMHbG+nBtqDYoIBilbAFlemwlQtXlkavqUBib8mg+K7p9+HoXXYFxWKQ9FUuA2Oo80j5vtDSU7Nl9TC1XTxMSrRm0kDX1jU7w3gPBhb1deCxe9h6pV1LTVcTRNFpf6hRaTbDLn//AB9PFLRwVN1mqJSN83Fgl95rA3I3ibC9rC5YSYGAWaqqp1S54fNrb0QqajrMlrAyGxHA/u6hL8CP6cVPuJoDnRusVrGCCuhzR6EeoPsj2Y5DFmUfpmWgByf7RSkgMjcyL2GvwPEa3GPXxZusxQo8RdTnoanhsf7gvORbKCj3q3M1WKOMHcjZlLyORYAAEjhew79dAL49ijLTdyhiVdFM3oodSeKMvlyUeV1FQVUSzA2K985tdfAbxI8Bi6V1mErMw+HpKpjTzufLVVbhBdwnDozCxzT1b+pS08kh8yNP5RJi+nHWusXG5LQBg+Y/b+CRqEsRvObsxLMe8nUn44rkN3Jygi6OEBSSMQTyYcn21rqawScuo9iXtr8T2h7iMWNlc3is+fDKabdtjzGn6+iecn6WYmsKmFoz9uPtL5kaMPdvYubUDiFjz4FI3WJ1+46H2TzlWeU1SLwTJJ3gHtDzU6j3jF7Xh2xWPNTywm0jSFC26h38vqh/hMf4Rvf0xGXsFW0DstTGe8LnkYQXdJs6Lp93MoR9tZF/kLf8OLYTZ6zMYbmpHdxB/H5VmdHp3YpI/wC7kdP4WK/0w8uNTZgQswIXxVA4YEIbn0QMZ8sCFXFYgKsPPAhDqxOsyMb3GnqmVD3hidP5/wCUYoqB1LrYwR5FTl5g+6RsJrrUz9H0AkmmiYdl4Sx80ZQPk7Yvpz1rLCxyMdE13EH7/wCFIikeKX0VW7AJmhPOF4z1l171JFyvC+vM3cXLp1j2Wpc2ijrZA8Msg7fVMLNu9m53lPd/82xU+FrjdaNLik9OzI2xHfwSvtdn8WTzCmoqSLrSt/SJSXYe7Q3/ADW8MGVrNgvRNNWOtI/Tlw9FA2IzKWvrkjrnao3wxXe0WMqCbqoG6LjThfhr3xZIS6yYq8Ojig6Rp1BVp5zlUSRWVQLeGL1iqvs6pleJge44EKJtT9LlWW1DfWKHgJ+0EuoJ8fo7/mOFqkaAroMBec72cLX+v7SZhVdKj2Q7UT0lPURRG3Wslm5x6MGI8SAovytfFjXlrSAkamhjnmY9/C+nPa33QC2K09ZaJKISMqcN5lW9uG8QL+PHE2E3SNZE0sJPJWfsgorBNlNTeRII1eGcn6SPXcC8NbfppwtZ1zA4WK5CCqfBJnZ6cLclXckTRStuOyOjMu+jFW0NuINxe3C+EQ4t2XZPp452BzxuL+q11BeRg0skkrDgZHZyPIsScemRx3XkNFDGbtCJvnkzUq0bNeJXDqDxWwI3Qfs3N7ciMeZzlyqbaSNs3TAa2t+/FDcRTSd9nIB+yKrvnqY4XI+x2CR7wzD34bpx1SuWxyQ9O0chf6leNrNhI6OBZ45mKmw3HUE6/eFv0xXLFl1BTmG4k6Zwic0eISXihbi+YELMCF6RiCGBII4EGxHkeIwLwgEWKYqTbitSN4nl66NkKFZRvGzAg2bRr2PMnFglcBZIPwunc4PaMpBvp7JbAxWtBHNiZCuYUpH96o/i7J/XE4+2EniIvSvHcrc2SXdqaxBw69z/ABnfPzJxoLhk2YELMCF//9k=";

            byte[] sign1 = Convert.FromBase64String(samp.sign1);
            byte[] sign2 = Convert.FromBase64String(samp.sign2);
            byte[] sign3 = Convert.FromBase64String(samp.sign3);
             
         
            var path = HttpContext.Current.Server.MapPath("~/ProfilePhotos/" + samp.CN.ToLower() + "/" + "");
            var folder = Path.Combine(path, samp.EmpID);
            Directory.CreateDirectory(folder); 
            MemoryStream stream = new MemoryStream();

            try
            {
                using (stream)
                {
                    
                    File.WriteAllBytes(folder + "/" + samp.EmpID + "_sign_0.png", sign1);
                    File.WriteAllBytes(folder + "/" + samp.EmpID + "_sign_1.png", sign2);
                    File.WriteAllBytes(folder + "/" + samp.EmpID + "_sign_2.png", sign3);
                }

                return "success";
            }
            catch
            {
                return "error";
            }

        }
        #endregion
    }
}

