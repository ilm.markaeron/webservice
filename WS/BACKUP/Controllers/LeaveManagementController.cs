﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using illimitadoWepAPI.Models;
using System.Xml.Serialization;
using System.IO;
using System.Data;
using System.Data.SqlClient;
using illimitadoWepAPI.MyClass;
using static illimitadoWepAPI.Models.LeaveMngt;
using System.Web;
using System.Net.Mail;
using System.Net.Mime;


namespace illimitadoWepAPI.Controllers
{
    public class LeaveManagementController : ApiController
    {
        // GET: api/LeaveManagement
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/LeaveManagement/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/LeaveManagement
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/LeaveManagement/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/LeaveManagement/5
        public void Delete(int id)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="FileNewLeave"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("FileLeave")]
        public string FileANewLeave(LeaveMngt FileNewLeave)
        {
            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = FileNewLeave.NTID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@LEAVEDATE", mytype = SqlDbType.NVarChar, Value = FileNewLeave.LeaveDate });
            Connection.myparameters.Add(new myParameters { ParameterName = "@LEAVETYPE", mytype = SqlDbType.NVarChar, Value = FileNewLeave.LeaveType });
            Connection.myparameters.Add(new myParameters { ParameterName = "@LEAVEID", mytype = SqlDbType.NVarChar, Value = FileNewLeave.LeaveID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@ISHALFDAY", mytype = SqlDbType.NVarChar, Value = FileNewLeave.isHalfDay });
            Connection.myparameters.Add(new myParameters { ParameterName = "@LOCATION", mytype = SqlDbType.NVarChar, Value = FileNewLeave.Coordinates });
            // DataTable EmployeeDt = new DataTable();
            String ID = Connection.ExecuteScalar("sp_InsertNewLeave_Mobile");

            return ID;

        }


        /// <summary>
        /// {
        ///"NTID": "i101"
        ///}
        /// </summary>
        /// <param name="LeaveParameters"></param>
        /// <returns></returns>
        /// 

        //leaveslot local upload '01-24-2020 at 11:50 rimar pogi'

        [HttpPost]
        [Route("HoopLeaveSlotDeduction")]
        public HoopLeaveSlotDeduction HoopLeaveSlotDeduction(SlotDeduction LP)
        {

            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = LP.CN;
            SelfieRegistration2Controller.GetDB2(GDB);

            HoopLeaveSlotDeduction GetLeaveResult = new HoopLeaveSlotDeduction();
            try
            {
                Connection Connection = new Connection();
                List<HoopLeaveSlotDeductionlist> ListLeave = new List<HoopLeaveSlotDeductionlist>();
                DataSet ds = new DataSet();
                DataTable dtLEAVES = new DataTable();
                //BasicFunction BasicFunction = new BasicFunction();
                Connection.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = LP.EMPID });
                Connection.myparameters.Add(new myParameters { ParameterName = "@leaveid", mytype = SqlDbType.NVarChar, Value = LP.leaveid });
                Connection.myparameters.Add(new myParameters { ParameterName = "@leavereason", mytype = SqlDbType.NVarChar, Value = LP.leavereason });
                Connection.myparameters.Add(new myParameters { ParameterName = "@LeaveDate", mytype = SqlDbType.NVarChar, Value = LP.LeaveDate });
                Connection.myparameters.Add(new myParameters { ParameterName = "@type", mytype = SqlDbType.NVarChar, Value = LP.type });
                ds = Connection.GetDataset("sp_HoopLeaveSlotDeduction");
                dtLEAVES = ds.Tables[0];
                if (dtLEAVES.Rows.Count > 0)
                {
                    foreach (DataRow row in dtLEAVES.Rows)
                    {
                        HoopLeaveSlotDeductionlist Leave = new HoopLeaveSlotDeductionlist
                        {
                            /*returnslot = row["returnslot"].ToString(),*/
                        };
                        ListLeave.Add(Leave);
                    }
                    GetLeaveResult.Leave = ListLeave;
                    GetLeaveResult.myreturn = "Success";
                }
                else
                {
                    GetLeaveResult.myreturn = "No Data Available";
                }
            }
            catch (Exception ex)
            {
                GetLeaveResult.myreturn = ex.Message;
            }
            return GetLeaveResult;
        }

        //ViewLeaveSlotV2
        [HttpPost]
        [Route("ViewLeaveSlotV2")]
        public ViewLeaveSlotV2 ViewLeaveSlotV2(SlotAvailable LP)
        {

            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = LP.CN;
            SelfieRegistration2Controller.GetDB2(GDB);

            ViewLeaveSlotV2 GetLeaveResult = new ViewLeaveSlotV2();
            try
            {
                Connection Connection = new Connection();
                List<ViewLeaveSlotV2list> ListLeave = new List<ViewLeaveSlotV2list>();
                DataSet ds = new DataSet();
                DataTable dtLEAVES = new DataTable();
                //BasicFunction BasicFunction = new BasicFunction();
                Connection.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = LP.EMPID });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Leave", mytype = SqlDbType.NVarChar, Value = LP.Leave });
                Connection.myparameters.Add(new myParameters { ParameterName = "@startdate", mytype = SqlDbType.NVarChar, Value = LP.startdate });
                Connection.myparameters.Add(new myParameters { ParameterName = "@enddate", mytype = SqlDbType.NVarChar, Value = LP.enddate });
                ds = Connection.GetDataset("sp_ViewLeaveSlotV2");
                dtLEAVES = ds.Tables[0];
                if (dtLEAVES.Rows.Count > 0)
                {
                    foreach (DataRow row in dtLEAVES.Rows)
                    {
                        ViewLeaveSlotV2list Leave = new ViewLeaveSlotV2list
                        {
                            dayleavetype = row["dayleavetype"].ToString(),
                            leavereason = row["leavereason"].ToString(),
                            date = row["date"].ToString(),
                            Slot = row["Slot"].ToString(),
                            leaveids = row["leaveids"].ToString(),
                            leavestatus = row["leavestatus"].ToString()
                        };
                        ListLeave.Add(Leave);
                    }
                    GetLeaveResult.Leave = ListLeave;
                    GetLeaveResult.myreturn = "Success";
                }
                else
                {
                    GetLeaveResult.myreturn = "No Data Available";
                }
            }
            catch (Exception ex)
            {
                GetLeaveResult.myreturn = ex.Message;
            }
            return GetLeaveResult;
        }



        //email notif for requestor
        [HttpPost]
        [Route("requestorLeaveEmailNotif")]
        public string requestorLeaveEmailNotif(leaveRequestor requestorLeave)
        {
            try
            {
                DataTable EmpDT = new DataTable();

                EmpDT.Columns.AddRange(new DataColumn[1] { new DataColumn("LeaveID") });

                for (int i = 0; i < requestorLeave.LeaveID.Length; i++)
                {
                    EmpDT.Rows.Add(requestorLeave.LeaveID[i].ToString());
                }

                GetDatabase GDB = new GetDatabase();
                GDB.ClientName = requestorLeave.CN;
                SelfieRegistration2Controller.GetDB2(GDB);

                Connection ECon = new Connection();
                ECon.myparameters.Add(new myParameters { ParameterName = "@Requestor", mytype = SqlDbType.Structured, Value = EmpDT });
                DataTable DT = ECon.GetDataTable("sp_emailNotificationRequestor");

                string EmpName, Emails, LeaveType, LeaveStatus, LeaveDate, Requestor;
                foreach (DataRow row in DT.Rows)
                {
                    EmpName = row["ApprovedBy"].ToString();
                    Emails = row["Email"].ToString();
                    LeaveType = row["LeaveType"].ToString();
                    LeaveStatus = row["LeaveStatus"].ToString();
                    LeaveDate = row["LeaveDate"].ToString();
                    Requestor = row["Requestor"].ToString();

                    string[] emaillist = Emails.Split(';');
                    if (string.IsNullOrWhiteSpace(Emails) == false)
                    {
                        var message = new MimeKit.MimeMessage();

                        MimeKit.InternetAddressList Elist = new MimeKit.InternetAddressList();
                        foreach (string emailadd in emaillist)
                        {
                            Elist.Add(new MimeKit.MailboxAddress(emailadd));
                        }

                        message.From.Add(new MimeKit.MailboxAddress("durusthr@illimitado.com"));
                        message.To.AddRange(Elist);
                        message.Subject = "Leave Request Notification";
                        message.Body = new MimeKit.TextPart("html")
                        {
                            Text = "Hi " + Requestor + ", " + Environment.NewLine +
                            EmpName + " has " + LeaveStatus + " your request for " + LeaveType + " for " + LeaveDate + "."
                        };

                        using (var client = new MailKit.Net.Smtp.SmtpClient())
                        {
                            client.ServerCertificateValidationCallback = (s, c, h, e) => true;
                            client.Connect("smtp.office365.com", 587, false);
                            client.Authenticate("durusthr@illimitado.com", "@1230Qwerty");
                            client.Send(message);
                            client.Disconnect(true);
                        }
                    }
                }
                return "Success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }

        //email notif for reimbursement approver 1
        [HttpPost]
        [Route("emailnotif_reim_approver")]
        public string requestorLeaveEmailNotif(reimApprover RE)
        {
            try
            {

                GetDatabase GDB = new GetDatabase();
                GDB.ClientName = RE.CN;
                SelfieRegistration2Controller.GetDB2(GDB);



                Connection ECon = new Connection();
                ECon.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.NVarChar, Value = RE.empID });
                ECon.myparameters.Add(new myParameters { ParameterName = "@benType", mytype = SqlDbType.NVarChar, Value = RE.benType });
                // ECon.myparameters.Add(new myParameters { ParameterName = "@Requestor", mytype = SqlDbType.Structured, Value = EmpDT });
                DataTable DT = ECon.GetDataTable("sp_emailnotif_reim_approver");



                string EmpName, Emails, Requestor;
                foreach (DataRow row in DT.Rows)
                {
                    EmpName = row["EmpName"].ToString();
                    Emails = row["Email"].ToString();
                    Requestor = row["requestor"].ToString();



                    string[] emaillist = Emails.Split(';');
                    if (string.IsNullOrWhiteSpace(Emails) == false)
                    {
                        var message = new MimeKit.MimeMessage();

                        MimeKit.InternetAddressList Elist = new MimeKit.InternetAddressList();
                        foreach (string emailadd in emaillist)
                        {
                            Elist.Add(new MimeKit.MailboxAddress(emailadd));
                        }

                        string link = "customerfirst@illimitado.com";
                        message.From.Add(new MimeKit.MailboxAddress("durusthr@illimitado.com"));
                        message.To.AddRange(Elist);
                        message.Subject = "Reimbursement Notification";
                        message.Body = new MimeKit.TextPart("html")
                        {
                            Text = "Hi " + EmpName + "," + "<br>" +
                            Requestor + " has filled a new reimbursement." + "<br>" +
                            "You may approve or decline using the " + "<a href='http://dev.durusthr.com/'>Web Portal</a>" + " or Mobile App." + "<br>" +
                            "Thanks" + "<br>" +
                            "<br>" + "<br>" +
                            "Please note that this is an automated email and thus, will not respond to this replies." + "<br>" +
                            "For any concerns, please send an email to " + "<a href='mailto:customerfirst@illimitado.com'>customerfirst@illimitado.com</a>" + "."
                        };
                        using (var client = new MailKit.Net.Smtp.SmtpClient())
                        {
                            client.ServerCertificateValidationCallback = (s, c, h, e) => true;
                            client.Connect("smtp.office365.com", 587, false);
                            client.Authenticate("durusthr@illimitado.com", "@1230Qwerty");
                            client.Send(message);
                            client.Disconnect(true);
                        }
                    }
                }
                return "Success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }

        [HttpPost]
        [Route("emailnotif_reim_next_approver")]
        public string emailnotif_reim_next_approver(reimApprover2 RE)
        {
            try
            {

                GetDatabase GDB = new GetDatabase();
                GDB.ClientName = RE.CN;
                SelfieRegistration2Controller.GetDB2(GDB);



                Connection ECon = new Connection();
                ECon.myparameters.Add(new myParameters { ParameterName = "@ID", mytype = SqlDbType.NVarChar, Value = RE.ID });
                // ECon.myparameters.Add(new myParameters { ParameterName = "@Requestor", mytype = SqlDbType.Structured, Value = EmpDT });
                DataTable DT = ECon.GetDataTable("sp_emailnotif_reim_next_approver");



                string nextApprover, Emails, Requestor, previousApprover;
                foreach (DataRow row in DT.Rows)
                {
                    nextApprover = row["nextApprover"].ToString();
                    Emails = row["Email"].ToString();
                    Requestor = row["requestor"].ToString();
                    previousApprover = row["previousApprover"].ToString();



                    string[] emaillist = Emails.Split(';');
                    if (string.IsNullOrWhiteSpace(Emails) == false)
                    {
                        var message = new MimeKit.MimeMessage();

                        MimeKit.InternetAddressList Elist = new MimeKit.InternetAddressList();
                        foreach (string emailadd in emaillist)
                        {
                            Elist.Add(new MimeKit.MailboxAddress(emailadd));
                        }

                        string link = "customerfirst@illimitado.com";
                        message.From.Add(new MimeKit.MailboxAddress("durusthr@illimitado.com"));
                        message.To.AddRange(Elist);
                        message.Subject = "Reimbursement Notification";
                        message.Body = new MimeKit.TextPart("html")
                        {
                            Text = "Hi " + nextApprover + "," + "<br>" +
                            previousApprover + " approved a reimbursement request of " + Requestor + "." + "<br>" +
                            "You may now approve or decline the request using " + "<a href='http://dev.durusthr.com/'>Web Portal</a>" + " or Mobile App." + "<br>" +
                            "Thanks" + "<br>" +
                            "<br>" + "<br>" +
                            "Please note that this is an automated email and thus, will not respond to this replies." + "<br>" +
                            "For any concerns, please send an email to " + "<a href='mailto:customerfirst@illimitado.com'>customerfirst@illimitado.com</a>" + "."
                        };
                        using (var client = new MailKit.Net.Smtp.SmtpClient())
                        {
                            client.ServerCertificateValidationCallback = (s, c, h, e) => true;
                            client.Connect("smtp.office365.com", 587, false);
                            client.Authenticate("durusthr@illimitado.com", "@1230Qwerty");
                            client.Send(message);
                            client.Disconnect(true);
                        }
                    }
                }
                return "Success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }

        //email notif for reimbursement requestor

        [HttpPost]
        [Route("emailnotif_reimbursment_requestor")]
        public string requestorLeaveEmailNotif(reimReq RE)
        {
            try
            {



                GetDatabase GDB = new GetDatabase();
                GDB.ClientName = RE.CN;
                SelfieRegistration2Controller.GetDB2(GDB);



                Connection ECon = new Connection();
                ECon.myparameters.Add(new myParameters { ParameterName = "@ID", mytype = SqlDbType.NVarChar, Value = RE.ID });
                // ECon.myparameters.Add(new myParameters { ParameterName = "@Requestor", mytype = SqlDbType.Structured, Value = EmpDT });
                DataTable DT = ECon.GetDataTable("sp_emailnotif_reimbursment");



                string EmpName, Emails, BenefitType, Amount, DateSubmitted, ClearCopy, RVDate, RVTime, RVAmount, RVBenType, Approver, AppPosition, CorrectBenefit, CorrectReceiptDate, VendorName, RV_TIN, RV_Description, BenStatus;
                foreach (DataRow row in DT.Rows)
                {
                    EmpName = row["EmpName"].ToString();
                    Emails = row["Email"].ToString();
                    Approver = row["Approver"].ToString();
                    AppPosition = row["AppPosition"].ToString();
                    BenefitType = row["BenefitType"].ToString();
                    Amount = row["Amount"].ToString();
                    DateSubmitted = row["DateSubmitted"].ToString();
                    ClearCopy = row["ClearCopy"].ToString();
                    RVDate = row["RVDate"].ToString();
                    RVTime = row["RVTime"].ToString();
                    RVAmount = row["RVAmount"].ToString();
                    RVBenType = row["RVBenType"].ToString();
                    CorrectBenefit = row["CorrectBenefit"].ToString();
                    CorrectReceiptDate = row["CorrectReceiptDate"].ToString();
                    VendorName = row["VendorName"].ToString();
                    RV_TIN = row["RV_TIN"].ToString();
                    RV_Description = row["RV_Description"].ToString();
                    BenStatus = row["BenStatus"].ToString();



                    string[] emaillist = Emails.Split(';');
                    if (string.IsNullOrWhiteSpace(Emails) == false)
                    {
                        var message = new MimeKit.MimeMessage();



                        MimeKit.InternetAddressList Elist = new MimeKit.InternetAddressList();
                        foreach (string emailadd in emaillist)
                        {
                            Elist.Add(new MimeKit.MailboxAddress(emailadd));
                        }
                        if (BenStatus == "Approved")
                        {
                            message.From.Add(new MimeKit.MailboxAddress("durusthr@illimitado.com"));
                            message.To.AddRange(Elist);
                            message.Subject = "Reimbursement Notification";
                            message.Body = new MimeKit.TextPart("html")
                            {
                                Text = "Hi " + EmpName + "," + "<br>" +
                                "Your reimbursement request for " + BenefitType + " for the amount of " + Amount + " filed on " + DateSubmitted + "<br>" +
                                "has been approved." + "<br>" +
                                "Thanks" + "<br>" + "<br>" +
                                Approver + "<br>" +
                                AppPosition + "<br>" + "<br>" + "<br>" +
                                "Please note that this is an automated email and thus, will not respond to this replies." + "<br>" +
                                "For any concerns, please send an email to " + "<a href='mailto:customerfirst@illimitado.com'>customerfirst@illimitado.com</a>" + "."
                            };



                            using (var client = new MailKit.Net.Smtp.SmtpClient())
                            {
                                client.ServerCertificateValidationCallback = (s, c, h, e) => true;
                                client.Connect("smtp.office365.com", 587, false);
                                client.Authenticate("durusthr@illimitado.com", "@1230Qwerty");
                                client.Send(message);
                                client.Disconnect(true);
                            }
                        }

                        if (BenStatus == "Denied")
                        {
                            message.From.Add(new MimeKit.MailboxAddress("durusthr@illimitado.com"));
                            message.To.AddRange(Elist);
                            message.Subject = "Reimbursement Notification";
                            message.Body = new MimeKit.TextPart("html")
                            {
                                Text = "Hi " + EmpName + "," + "<br>" +
                                "Your reimbursement request for " + BenefitType + " for the amount of " + Amount + " filed on " + DateSubmitted + "<br>" +
                                "has been denied since the " + ClearCopy + RVDate + RVTime + RVAmount + RVBenType + CorrectBenefit + CorrectReceiptDate + VendorName + RV_TIN + RV_Description + " are incorrect. Please file again correctly." + "<br>" +
                                "Thanks" + "<br>" + "<br>" +
                                Approver + "<br>" +
                                AppPosition + "<br>" + "<br>" + "<br>" +
                                "Please note that this is an automated email and thus, will not respond to this replies." + "<br>" +
                                "For any concerns, please send an email to " + "<a href='mailto:customerfirst@illimitado.com'>customerfirst@illimitado.com</a>" + "."
                            };
                            using (var client = new MailKit.Net.Smtp.SmtpClient())
                            {
                                client.ServerCertificateValidationCallback = (s, c, h, e) => true;
                                client.Connect("smtp.office365.com", 587, false);
                                client.Authenticate("durusthr@illimitado.com", "@1230Qwerty");
                                client.Send(message);
                                client.Disconnect(true);
                            }
                        }
                    }
                }
                return "Success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }


        //email notif for approvers
        [HttpPost]
        [Route("Email_Notif_SecApprover")]
        public string Email_Notif_SecApprover(forApprover2 requestorLeave)
        {
            try
            {
                DataTable EmpDT = new DataTable();
                DataTable EmpDT1 = new DataTable();
                DataTable EmpDT2 = new DataTable();

                EmpDT.Columns.AddRange(new DataColumn[1] { new DataColumn("EmpID") });
                for (int i = 0; i < requestorLeave.requestorID.Length; i++)
                {
                    EmpDT.Rows.Add(requestorLeave.requestorID[i].ToString());
                }
                EmpDT1.Columns.AddRange(new DataColumn[1] { new DataColumn("LeaveID") });
                for (int i = 0; i < requestorLeave.leaveID.Length; i++)
                {
                    EmpDT1.Rows.Add(requestorLeave.leaveID[i].ToString());
                }
                EmpDT2.Columns.AddRange(new DataColumn[1] { new DataColumn("ID") });
                for (int i = 0; i < requestorLeave.SerialID.Length; i++)
                {
                    EmpDT2.Rows.Add(requestorLeave.SerialID[i].ToString());
                }

                GetDatabase GDB = new GetDatabase();
                GDB.ClientName = requestorLeave.CN;
                SelfieRegistration2Controller.GetDB2(GDB);

                Connection ECon = new Connection();
                ECon.myparameters.Add(new myParameters { ParameterName = "@requestorID", mytype = SqlDbType.Structured, Value = EmpDT });
                ECon.myparameters.Add(new myParameters { ParameterName = "@leaveID", mytype = SqlDbType.Structured, Value = EmpDT1 });
                ECon.myparameters.Add(new myParameters { ParameterName = "@SerialID", mytype = SqlDbType.Structured, Value = EmpDT2 });
                DataTable DT = ECon.GetDataTable("sp_Email_Notif_SecApprover");

                string EmpName, Emails, LeaveType, LeaveStatus, LeaveDate, Requestor;
                foreach (DataRow row in DT.Rows)
                {
                    EmpName = row["ApprovedBy"].ToString();
                    Emails = row["Email"].ToString();
                    LeaveType = row["LeaveType"].ToString();
                    LeaveStatus = row["LeaveStatus"].ToString();
                    LeaveDate = row["LeaveDate"].ToString();
                    Requestor = row["Requestor"].ToString();

                    string[] emaillist = Emails.Split(';');
                    if (string.IsNullOrWhiteSpace(Emails) == false)
                    {
                        var message = new MimeKit.MimeMessage();

                        MimeKit.InternetAddressList Elist = new MimeKit.InternetAddressList();
                        foreach (string emailadd in emaillist)
                        {
                            Elist.Add(new MimeKit.MailboxAddress(emailadd));
                        }

                        message.From.Add(new MimeKit.MailboxAddress("durusthr@illimitado.com"));
                        message.To.AddRange(Elist);
                        message.Subject = "Approved Leave Request Notification";
                        message.Body = new MimeKit.TextPart("html")
                        {
                            Text = EmpName + " has " + LeaveStatus + " the request of " + Requestor + "'s " + LeaveType + " for " + LeaveDate + "."
                        };

                        using (var client = new MailKit.Net.Smtp.SmtpClient())
                        {
                            client.ServerCertificateValidationCallback = (s, c, h, e) => true;
                            client.Connect("smtp.office365.com", 587, false);
                            client.Authenticate("durusthr@illimitado.com", "@1230Qwerty");
                            client.Send(message);
                            client.Disconnect(true);
                        }
                    }
                }
                return "Success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }

        //email notif for sec approver in segment
        [HttpPost]
        [Route("emailSegment_2ndApprover")]
        public string emailSegment_2ndApprover(addSegmentApprover2 requestorLeave)
        {
            try
            {
                DataTable EmpDT = new DataTable();

                EmpDT.Columns.AddRange(new DataColumn[1] { new DataColumn("ID") });
                for (int i = 0; i < requestorLeave.SeriesID.Length; i++)
                {
                    EmpDT.Rows.Add(requestorLeave.SeriesID[i].ToString());
                }

                GetDatabase GDB = new GetDatabase();
                GDB.ClientName = requestorLeave.CN;
                SelfieRegistration2Controller.GetDB2(GDB);

                Connection ECon = new Connection();
                ECon.myparameters.Add(new myParameters { ParameterName = "@SeriesID", mytype = SqlDbType.Structured, Value = EmpDT });
                ECon.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.NVarChar, Value = requestorLeave.empID });
                DataTable DT = ECon.GetDataTable("sp_emailSegment_2ndApprover");

                string EmpName, Emails, ReasonDesc, Activity_Status, SchedDate, Requestor, StartTime, EndTime;
                foreach (DataRow row in DT.Rows)
                {
                    EmpName = row["ApprovedBy"].ToString();
                    Emails = row["Email"].ToString();
                    ReasonDesc = row["ReasonDesc"].ToString();
                    Activity_Status = row["Activity_Status"].ToString();
                    SchedDate = row["SchedDate"].ToString();
                    Requestor = row["Requestor"].ToString();
                    StartTime = row["StartTime"].ToString();
                    EndTime = row["EndTime"].ToString();

                    string[] emaillist = Emails.Split(';');
                    if (string.IsNullOrWhiteSpace(Emails) == false)
                    {
                        var message = new MimeKit.MimeMessage();

                        MimeKit.InternetAddressList Elist = new MimeKit.InternetAddressList();
                        foreach (string emailadd in emaillist)
                        {
                            Elist.Add(new MimeKit.MailboxAddress(emailadd));
                        }
                        if (Activity_Status == "Pending")
                        {
                            message.From.Add(new MimeKit.MailboxAddress("durusthr@illimitado.com"));
                            message.To.AddRange(Elist);
                            message.Subject = "Segment Request Notification";
                            message.Body = new MimeKit.TextPart("html")
                            {
                                Text = EmpName + " has approved the " + ReasonDesc + " segment request of " + Requestor + " for " + StartTime + " to " + EndTime + " on " + SchedDate + "."
                            };

                            using (var client = new MailKit.Net.Smtp.SmtpClient())
                            {
                                client.ServerCertificateValidationCallback = (s, c, h, e) => true;
                                client.Connect("smtp.office365.com", 587, false);
                                client.Authenticate("durusthr@illimitado.com", "@1230Qwerty");
                                client.Send(message);
                                client.Disconnect(true);
                            }
                        }
                        //else
                        //{
                        //    message.From.Add(new MimeKit.MailboxAddress("durusthr@illimitado.com"));
                        //    message.To.AddRange(Elist);
                        //    message.Subject = "Segment Request Notification";
                        //    message.Body = new MimeKit.TextPart("html")
                        //    {
                        //        Text = EmpName + " has " + Activity_Status + " the " + ReasonDesc + " segment request of " + Requestor + " for " + StartTime + " to " + EndTime + " on " + SchedDate + "."
                        //    };

                        //    using (var client = new MailKit.Net.Smtp.SmtpClient())
                        //    {
                        //        client.ServerCertificateValidationCallback = (s, c, h, e) => true;
                        //        client.Connect("smtp.office365.com", 587, false);
                        //        client.Authenticate("durusthr@illimitado.com", "@1230Qwerty");
                        //        client.Send(message);
                        //        client.Disconnect(true);
                        //    }
                        //}
                    }
                }
                return "Success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }

        //update reimbursement approve/deny

        [HttpPost]
        [Route("UpdateReimbursementReviewer")]
        public string UpdateReimbursementReviewer(approveDenyReim RE)
        {
            try
            {
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@ID", mytype = SqlDbType.NVarChar, Value = RE.ID });
                con.myparameters.Add(new myParameters { ParameterName = "@Status", mytype = SqlDbType.NVarChar, Value = RE.Status });
                con.myparameters.Add(new myParameters { ParameterName = "@UnclearCopy", mytype = SqlDbType.NVarChar, Value = RE.UnclearCopy });
                con.myparameters.Add(new myParameters { ParameterName = "@IncorrentType", mytype = SqlDbType.NVarChar, Value = RE.IncorrentType });
                con.myparameters.Add(new myParameters { ParameterName = "@WrongDate", mytype = SqlDbType.NVarChar, Value = RE.WrongDate });
                con.myparameters.Add(new myParameters { ParameterName = "@BenType", mytype = SqlDbType.NVarChar, Value = RE.BenType });
                con.myparameters.Add(new myParameters { ParameterName = "@VendorName", mytype = SqlDbType.NVarChar, Value = RE.VendorName });
                con.myparameters.Add(new myParameters { ParameterName = "@Date", mytype = SqlDbType.NVarChar, Value = RE.Date });
                con.myparameters.Add(new myParameters { ParameterName = "@Time", mytype = SqlDbType.NVarChar, Value = RE.Time });
                con.myparameters.Add(new myParameters { ParameterName = "@TIN", mytype = SqlDbType.NVarChar, Value = RE.TIN });
                con.myparameters.Add(new myParameters { ParameterName = "@Amount", mytype = SqlDbType.NVarChar, Value = RE.Amount });
                con.myparameters.Add(new myParameters { ParameterName = "@Description", mytype = SqlDbType.NVarChar, Value = RE.Description });
                con.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.NVarChar, Value = RE.EmpID });
                con.myparameters.Add(new myParameters { ParameterName = "@ModifiedDate", mytype = SqlDbType.NVarChar, Value = RE.ModifiedDate });
                con.ExecuteScalar("sp_UpdateReimbursementReviewer");
                return "True";
            }
            catch (Exception)
            {
                return "False";
            }
        }

        [HttpPost]
        [Route("ViewLeaveSlot")]
        public List<FilterLeaveSlot> FilterLS(LeaveSlotParams filterLeave)
        {


            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = filterLeave.CN;
            SelfieRegistration2Controller.GetDB2(GDB);

            DataTable filterUser = new DataTable();

            filterUser.Columns.AddRange(new DataColumn[1] { new DataColumn("LeaveType") });
            for (int i = 0; i < filterLeave.Leave.Length; i++)
            {
                filterUser.Rows.Add(filterLeave.Leave[i]);
            }

            List<FilterLeaveSlot> DU = new List<FilterLeaveSlot>();

            Connection Connection = new Connection();
            Connection.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.VarChar, Value = filterLeave.EMPID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@startdate", mytype = SqlDbType.VarChar, Value = filterLeave.startdate });
            Connection.myparameters.Add(new myParameters { ParameterName = "@enddate", mytype = SqlDbType.VarChar, Value = filterLeave.enddate });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Leave", mytype = SqlDbType.Structured, Value = filterUser });
            DataTable DT = Connection.GetDataTable("sp_ViewLeaveSlot");
            foreach (DataRow row in DT.Rows)
            {
                FilterLeaveSlot Users = new FilterLeaveSlot()
                {
                    dayleavetype = row["dayleavetype"].ToString(),
                    leavereason = row["leavereason"].ToString(),
                    date = row["date"].ToString(),
                    Slot = row["Slot"].ToString(),

                };
                DU.Add(Users);
            }
            return DU;
        }

        [HttpPost]
        [Route("CheckLeaveSlot")]
        public CheckLeaveSlot ShowLeaves(checkLeaveParam LP)
        {

            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = LP.CN;
            SelfieRegistration2Controller.GetDB2(GDB);

            CheckLeaveSlot GetLeaveResult = new CheckLeaveSlot();
            try
            {
                Connection Connection = new Connection();
                List<LeaveSlotReturn> ListLeave = new List<LeaveSlotReturn>();
                DataSet ds = new DataSet();
                DataTable dtLEAVES = new DataTable();
                //BasicFunction BasicFunction = new BasicFunction();
                Connection.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = LP.EMPID });
                Connection.myparameters.Add(new myParameters { ParameterName = "@leaveid", mytype = SqlDbType.NVarChar, Value = LP.leaveid });
                Connection.myparameters.Add(new myParameters { ParameterName = "@leavereason", mytype = SqlDbType.NVarChar, Value = LP.leavereason });
                Connection.myparameters.Add(new myParameters { ParameterName = "@startdate", mytype = SqlDbType.NVarChar, Value = LP.startdate });
                Connection.myparameters.Add(new myParameters { ParameterName = "@enddate", mytype = SqlDbType.NVarChar, Value = LP.enddate });
                ds = Connection.GetDataset("sp_CheckLeaveSlot");
                dtLEAVES = ds.Tables[0];
                if (dtLEAVES.Rows.Count > 0)
                {
                    foreach (DataRow row in dtLEAVES.Rows)
                    {
                        LeaveSlotReturn Leave = new LeaveSlotReturn
                        {
                            returnslot = row["returnslot"].ToString(),
                        };
                        ListLeave.Add(Leave);
                    }
                    GetLeaveResult.Leave = ListLeave;
                    GetLeaveResult.myreturn = "Success";
                }
                else
                {
                    GetLeaveResult.myreturn = "No Data Available";
                }
            }
            catch (Exception ex)
            {
                GetLeaveResult.myreturn = ex.Message;
            }
            return GetLeaveResult;
        }

        //leaveslot local upload '01-24-2020 at 11:50' ^^^^^^^^ top

        [HttpPost]
        [Route("ShowLeaves")]
        public GetLeaveResult ShowLeaves(LeaveParameters LeaveParameters)
        {
            GetLeaveResult GetLeaveResult = new GetLeaveResult();
            try
            {
                Connection Connection = new Connection();
                List<LeaveMngt> ListLeave = new List<LeaveMngt>();
                DataSet ds = new DataSet();
                DataTable dtLEAVES = new DataTable();
                //BasicFunction BasicFunction = new BasicFunction();
                Connection.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = LeaveParameters.NTID });
                //Connection.myparameters.Add(new myParameters { ParameterName = "@STARTDATE", mytype = SqlDbType.NVarChar, Value = LeaveParameters.Start_Date });
                //Connection.myparameters.Add(new myParameters { ParameterName = "@ENDDATE", mytype = SqlDbType.NVarChar, Value = LeaveParameters.End_Date });
                ds = Connection.GetDataset("sp_ShowLeaves_Mobile");
                dtLEAVES = ds.Tables[0];
                if (dtLEAVES.Rows.Count > 0)
                {
                    foreach (DataRow row in dtLEAVES.Rows)
                    {
                        LeaveMngt Leave = new LeaveMngt
                        {
                            LeaveDate = Convert.ToDateTime(row["LeaveDate"]).ToString("MMM-dd-yyyy"),
                            isHalfDay = row["isHalfDay"].ToString(),
                            isPaid = row["isPaid"].ToString(),
                            LeaveStatus = row["LeaveStatus"].ToString(),
                            LeaveType = row["LeaveType"].ToString(),
                            DateApplied = row["DateApplied"].ToString() == "" ? null : Convert.ToDateTime(row["DateApplied"]).ToString("MMM-dd-yyyy")
                        };
                        ListLeave.Add(Leave);
                    }
                    GetLeaveResult.Leave = ListLeave;
                    GetLeaveResult.myreturn = "Success";
                }
                else
                {
                    GetLeaveResult.myreturn = "No Data Available";
                }
            }
            catch (Exception ex)
            {
                GetLeaveResult.myreturn = ex.Message;
            }
            return GetLeaveResult;
        }

        [HttpPost]
        [Route("SearchLeaves")]
        public GetLeaveResult SearchLeaves(LeaveParameters LeaveParameters)
        {
            GetLeaveResult GetLeaveResult = new GetLeaveResult();
            try
            {
                Connection Connection = new Connection();
                List<LeaveMngt> ListLeave = new List<LeaveMngt>();
                DataSet ds = new DataSet();
                DataTable dtLEAVES = new DataTable();
                //BasicFunction BasicFunction = new BasicFunction();
                Connection.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = LeaveParameters.NTID });
                Connection.myparameters.Add(new myParameters { ParameterName = "@LeaveDateFrom", mytype = SqlDbType.NVarChar, Value = Convert.ToDateTime(LeaveParameters.Start_Date).ToString("yyyy-MM-dd") });
                Connection.myparameters.Add(new myParameters { ParameterName = "@LeaveDateTo", mytype = SqlDbType.NVarChar, Value = Convert.ToDateTime(LeaveParameters.End_Date).ToString("yyyy-MM-dd") });
                //Connection.myparameters.Add(new myParameters { ParameterName = "@STARTDATE", mytype = SqlDbType.NVarChar, Value = LeaveParameters.Start_Date });
                //Connection.myparameters.Add(new myParameters { ParameterName = "@ENDDATE", mytype = SqlDbType.NVarChar, Value = LeaveParameters.End_Date });
                ds = Connection.GetDataset("sp_SearchLeaves_Mobile");
                dtLEAVES = ds.Tables[0];
                if (dtLEAVES.Rows.Count > 0)
                {
                    foreach (DataRow row in dtLEAVES.Rows)
                    {
                        LeaveMngt Leave = new LeaveMngt
                        {
                            LeaveDate = Convert.ToDateTime(row["LeaveDate"]).ToString("MMM-dd-yyyy"),
                            isHalfDay = row["isHalfDay"].ToString(),
                            isPaid = row["isPaid"].ToString(),
                            LeaveStatus = row["LeaveStatus"].ToString(),
                            LeaveType = row["LeaveType"].ToString(),
                            DateApplied = row["DateApplied"].ToString() == "" ? null : Convert.ToDateTime(row["DateApplied"]).ToString("MMM-dd-yyyy")
                        };
                        ListLeave.Add(Leave);
                    }
                    GetLeaveResult.Leave = ListLeave;
                    GetLeaveResult.myreturn = "Success";
                }
                else
                {
                    GetLeaveResult.myreturn = "No Data Available";
                }
            }
            catch (Exception ex)
            {
                GetLeaveResult.myreturn = ex.Message;
            }
            return GetLeaveResult;
        }


        [HttpPost]
        [Route("ShowLeavesForApproval")]
        public GetLeaveResult ShowLeavesForApproval(LeaveParameters LeaveParameters)
        {
            GetLeaveResult GetLeaveResult = new GetLeaveResult();
            try
            {
                Connection Connection = new Connection();
                List<LeaveMngt> ListLeave = new List<LeaveMngt>();
                DataSet ds = new DataSet();
                DataTable dtLEAVES = new DataTable();
                //BasicFunction BasicFunction = new BasicFunction();
                Connection.myparameters.Add(new myParameters { ParameterName = "@MNGRID", mytype = SqlDbType.NVarChar, Value = LeaveParameters.NTID });
                //Connection.myparameters.Add(new myParameters { ParameterName = "@STARTDATE", mytype = SqlDbType.NVarChar, Value = LeaveParameters.Start_Date });
                //Connection.myparameters.Add(new myParameters { ParameterName = "@ENDDATE", mytype = SqlDbType.NVarChar, Value = LeaveParameters.End_Date });
                ds = Connection.GetDataset("sp_SearchLeaveForApproval_Mobile");
                dtLEAVES = ds.Tables[0];
                if (dtLEAVES.Rows.Count > 0)
                {
                    foreach (DataRow row in dtLEAVES.Rows)
                    {
                        LeaveMngt Leave = new LeaveMngt
                        {
                            LeaveID = row["ID"].ToString(),
                            EmployeeName = row["EmpName"].ToString(),
                            LeaveType = row["LeaveType"].ToString(),
                            LeaveDate = Convert.ToDateTime(row["LeaveDate"]).ToString("MMM-dd-yyyy"),
                            LeaveStatus = row["LeaveStatus"].ToString()

                        };
                        ListLeave.Add(Leave);
                    }
                    GetLeaveResult.Leave = ListLeave;
                    GetLeaveResult.myreturn = "Success";
                }
                else
                {
                    GetLeaveResult.myreturn = "No Data Available";
                }
            }
            catch (Exception ex)
            {
                GetLeaveResult.myreturn = ex.Message;
            }
            return GetLeaveResult;
        }

        [HttpPost]
        [Route("ApprovedLeaves")]
        public string ApprovedLeaves(LeaveMngt LeaveForApproval)
        {
            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@LEAVEID", mytype = SqlDbType.NVarChar, Value = LeaveForApproval.LeaveID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@LEAVESTATUS", mytype = SqlDbType.NVarChar, Value = LeaveForApproval.LeaveStatus });

            // DataTable EmployeeDt = new DataTable();
            String ID = Connection.ExecuteScalar("sp_ApprovedDenyLeaves_Mobile");

            return ID;

        }

        [HttpPost]
        [Route("ShowAvailableLeaves")]
        public GetAvailableLeaves ShowAvailableLeaves(LeaveParameters LeaveParameters)
        {
            GetAvailableLeaves GetLeaveResult = new GetAvailableLeaves();
            //GetLastLeaves GetlastLeaveResult = new GetLastLeaves();
            try
            {
                Connection Connection = new Connection();
                List<AvailableLeave> ListLeave = new List<AvailableLeave>();
                List<AvailableLastLeave> ListlastLeave = new List<AvailableLastLeave>();
                DataSet ds = new DataSet();
                DataTable dtLEAVES = new DataTable();

                Connection.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = LeaveParameters.NTID });

                ds = Connection.GetDataset("sp_AvailableLeaves_Mobile");
                dtLEAVES = ds.Tables[0];
                if (dtLEAVES.Rows.Count > 0)
                {
                    foreach (DataRow row in dtLEAVES.Rows)
                    {
                        AvailableLeave AvailableLeaves = new AvailableLeave
                        {
                            LeaveBal = row["LeaveBalance"].ToString(),
                            // CTO = row["LeaveBalCTO"].ToString(),
                            //LastLeave = row["LeaveDate"].ToString(),
                            Ltype = row["Ltype"].ToString(),
                            SerialID = row["SerialID"].ToString(),
                            // LastLeave = row["LastLeaveDate"].ToString()

                        };


                        ListLeave.Add(AvailableLeaves);

                    }

                    DataRow firstRow = dtLEAVES.Rows[0];

                    GetLeaveResult.AvailableLeaves = ListLeave;
                    GetLeaveResult.AvailableLastLeaves = Convert.ToDateTime(firstRow["LeaveDate"]).ToString("MMM-dd-yyyy");
                    //GetLeaveResult.AvailableLastLeaves = ListlastLeave;
                    GetLeaveResult.myreturn = "Success";
                }
                else
                {
                    GetLeaveResult.myreturn = "No Data Available";
                }
            }
            catch (Exception ex)
            {
                GetLeaveResult.myreturn = ex.Message;
                //GetLeaveResult.myreturn = "Test Message";

            }
            return GetLeaveResult;
        }

        //--------------------------Start Leaves and Logs - Nhoraisa

        //Get Leaves for Approval

        [HttpPost]
        [Route("ShowLeavesForApprovalMobile")]
        public ShowLeaves ShowLeaves(ShowLeavesID GL)
        {
            try
            {
                Connection con = new Connection();
                ShowLeaves leaveslist = new ShowLeaves();
                List<GetShowLeaves> list = new List<GetShowLeaves>();
                con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = GL.EmpID });
                DataTable dt = con.GetDataTable("sp_GetAll_Leaves1");

                foreach (DataRow row in dt.Rows)
                {

                    GetShowLeaves param = new GetShowLeaves()
                    {
                        ID = row["ID"].ToString(),
                        EmpID = row["EmpID"].ToString(),
                        EmpName = row["EmpName"].ToString(),
                        ManagerID = row["MngrID"].ToString(),
                        ManagerName = row["MngrName"].ToString(),
                        LeaveDate = row["LeaveDate"].ToString(),
                        LeaveType = row["LeaveType"].ToString(),
                        DateApplied = row["DateApplied"].ToString(),
                        Halfday = row["isHalDay"].ToString(),
                        //LeaveID = row["LeaveID"].ToString(),
                        //Gender = row["Gender"].ToString(),
                        //Expired = row["Expr1"].ToString(),
                        Approver = row["isApprover"].ToString(),


                    };
                    list.Add(param);
                }
                leaveslist.LeavesListData = list;
                leaveslist._myreturn = "Success";
                return leaveslist;
            }
            catch (Exception e)
            {
                ShowLeaves leaveslist = new ShowLeaves();
                List<GetShowLeaves> list = new List<GetShowLeaves>();
                leaveslist.LeavesListData = list;
                leaveslist._myreturn = e.ToString();
                return leaveslist;
            }
        }

        [HttpPost]
        [Route("AMAN_ShowLeavesForApprovalMobileV2")]
        public ShowLeavesv2 ShowLeavesV2(ShowLeavesIDV2 prms)
        {
            try
            {
                GetDatabase GDB = new GetDatabase();
                GDB.ClientName = prms.CN;
                SelfieRegistration2Controller.GetDB2(GDB);

                Connection con = new Connection();
                ShowLeavesv2 leaveslist = new ShowLeavesv2();
                List<GetShowLeavesv2> list = new List<GetShowLeavesv2>();
                con.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.NVarChar, Value = prms.EmpID });
                DataTable dt = con.GetDataTable("sp_GetAll_Leaves1");

                foreach (DataRow row in dt.Rows)
                {

                    GetShowLeavesv2 param = new GetShowLeavesv2()
                    {
                        ID = row["ID"].ToString(),
                        EmpID = row["EmpID"].ToString(),
                        EmpName = row["EmpName"].ToString(),
                        ManagerID = row["MngrID"].ToString(),
                        ManagerName = row["MngrName"].ToString(),
                        LeaveDate = row["LeaveDate"].ToString(),
                        LeaveType = row["LeaveType"].ToString(),
                        DateApplied = row["DateApplied"].ToString(),
                        Halfday = row["isHalDay"].ToString(),
                        //LeaveID = row["LeaveID"].ToString(),
                        //Gender = row["Gender"].ToString(),
                        //Expired = row["Expr1"].ToString(),
                        Approver = row["isApprover"].ToString(),
                        DepartureDate = row["DepartureDate"].ToString(),
                        ReturnDate = row["ReturnDate"].ToString()
                    };
                    list.Add(param);
                }
                leaveslist.LeavesListDatav2 = list;
                leaveslist._myreturn = "Success";
                return leaveslist;
            }
            catch (Exception e)
            {
                ShowLeavesv2 leaveslist = new ShowLeavesv2();
                List<GetShowLeavesv2> list = new List<GetShowLeavesv2>();
                leaveslist.LeavesListDatav2 = list;
                leaveslist._myreturn = e.ToString();
                return leaveslist;
            }
        }
        //Get Logs for Approval


        [HttpPost]
        [Route("DisplayLogsForApprovalMobile")]
        public ShowLogs GetLogs(ShowLogsID GL)
        {
            try
            {
                Connection con = new Connection();
                ShowLogs logslist = new ShowLogs();
                List<ShowGetLogs> list = new List<ShowGetLogs>();
                con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = GL.EmpID });
                DataTable dt = con.GetDataTable("sp_GetAll_Logs1");

                foreach (DataRow row in dt.Rows)
                {
                    ShowGetLogs param = new ShowGetLogs()
                    {
                        SeriesID = row["SeriesID"].ToString(),
                        EmpID = row["EmpID"].ToString(),
                        EmpName = row["FullName"].ToString(),
                        StartTime = row["Start_Time"].ToString(),
                        EndTime = row["EndTime"].ToString(),
                        Reason = row["ReasonDesc"].ToString(),
                        SchedDate = row["SchedDate"].ToString(),
                        TotalTime = row["TotalTimeMin"].ToString(),
                        Status = row["ActivityStatus"].ToString(),
                        Approver = row["isApprover"].ToString(),

                    };
                    list.Add(param);
                }
                logslist.LogsListData = list;
                logslist._myreturn = "Success";
                return logslist;
            }
            catch (Exception e)
            {
                ShowLogs logslist = new ShowLogs();
                List<ShowGetLogs> list = new List<ShowGetLogs>();
                logslist.LogsListData = list;
                logslist._myreturn = e.ToString();
                return logslist;
            }
        }

        //Get Notifications in Logs for Approver

        [HttpPost]
        [Route("GetLogsNotif")]
        public Logs GetLogs(GetLogsID GL)
        {
            try
            {
                Connection con = new Connection();
                Logs logslist = new Logs();
                List<GetLogs> list = new List<GetLogs>();
                con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = GL.EmpID });
                DataTable dt = con.GetDataTable("sp_GetAll_Logs1_NotifMobile");

                foreach (DataRow row in dt.Rows)
                {
                    GetLogs param = new GetLogs()
                    {
                        SeriesID = row["SeriesID"].ToString(),
                        EmpID = row["EmpID"].ToString(),
                        EmpName = row["FullName"].ToString(),
                        StartTime = row["Start_Time"].ToString(),
                        EndTime = row["EndTime"].ToString(),
                        Reason = row["ReasonDesc"].ToString(),
                        SchedDate = row["SchedDate"].ToString(),
                        TotalTime = row["TotalTimeMin"].ToString(),
                        Status = row["ActivityStatus"].ToString(),
                        Approver = row["isApprover"].ToString(),

                    };
                    list.Add(param);
                }
                logslist.LogsListData = list;
                logslist._myreturn = "Success";
                return logslist;
            }
            catch (Exception e)
            {
                Logs logslist = new Logs();
                List<GetLogs> list = new List<GetLogs>();
                logslist.LogsListData = list;
                logslist._myreturn = e.ToString();
                return logslist;
            }
        }

        //Get Notifications in Leaves for Approver

        [HttpPost]
        [Route("GetLeavesNotif")]
        public Leaves GetLeaves(GetLeavesID GL)
        {
            try
            {
                Connection con = new Connection();
                Leaves leaveslist = new Leaves();
                List<GetLeaves> list = new List<GetLeaves>();
                con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = GL.EmpID });
                DataTable dt = con.GetDataTable("sp_GetAll_Leaves1_NotifMobile");

                foreach (DataRow row in dt.Rows)
                {
                    //string half = row["isHalDay"].ToString();
                    //    if (half == "0")
                    //{
                    //    half = "No";
                    //}
                    //    else
                    //{
                    //    half = "Yes";
                    //}
                    GetLeaves param = new GetLeaves()
                    {
                        ID = row["ID"].ToString(),
                        EmpID = row["EmpID"].ToString(),
                        EmpName = row["EmpName"].ToString(),
                        ManagerID = row["MngrID"].ToString(),
                        ManagerName = row["MngrName"].ToString(),
                        LeaveDate = row["LeaveDate"].ToString(),
                        LeaveType = row["LeaveType"].ToString(),
                        DateApplied = row["DateApplied"].ToString(),
                        Halfday = row["isHalDay"].ToString(),
                        //LeaveID = row["LeaveID"].ToString(),
                        //Gender = row["Gender"].ToString(),
                        //Expired = row["Expr1"].ToString(),
                        Approver = row["isApprover"].ToString(),

                    };
                    list.Add(param);
                }
                leaveslist.LeavesListData = list;
                leaveslist._myreturn = "Success";
                return leaveslist;
            }
            catch (Exception e)
            {
                Leaves leaveslist = new Leaves();
                List<GetLeaves> list = new List<GetLeaves>();
                leaveslist.LeavesListData = list;
                leaveslist._myreturn = e.ToString();
                return leaveslist;
            }
        }

        //Update if Notifications notice

        [HttpPost]
        [Route("UpdateisSeenMobile")]
        public string update(UpdateisSeen US)
        {
            try
            {
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@Type", mytype = SqlDbType.NVarChar, Value = US.Type });
                con.myparameters.Add(new myParameters { ParameterName = "@SeriesID", mytype = SqlDbType.NVarChar, Value = US.SeriesID });
                con.ExecuteScalar("sp_HRMS_isSeen_Mobile");
                return "True";
            }
            catch (Exception)
            {
                return "False";
            }
        }

        //Update Leaves Status

        [HttpPost]
        [Route("ApproveDenyLeaves")]
        public string UpdateLeavesStatus(ApproveDenyLeaves US)
        {
            try
            {
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@LeaveID", mytype = SqlDbType.NVarChar, Value = US.SeriesID });
                con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.NVarChar, Value = US.EmpID });
                con.myparameters.Add(new myParameters { ParameterName = "@Status", mytype = SqlDbType.NVarChar, Value = US.Status });
                con.ExecuteScalar("sp_Update_Leave_Approvers1_Mobile");
                return "True";
            }
            catch (Exception)
            {
                return "False";
            }
        }

        //Update Logs Status

        [HttpPost]
        [Route("ApproveDenyLogs")]
        public string UpdateLogsStatus(ApproveDenyLogs US)
        {
            try
            {
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@seriesID", mytype = SqlDbType.NVarChar, Value = US.SeriesID });
                con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.NVarChar, Value = US.EmpID });
                con.myparameters.Add(new myParameters { ParameterName = "@Status", mytype = SqlDbType.NVarChar, Value = US.Status });
                con.ExecuteScalar("sp_Update_Log_Approvers1_Mobile");
                return "True";
            }
            catch (Exception)
            {
                return "False";
            }
        }


        //Get Notifications in Leaves for Requestor

        [HttpPost]
        [Route("GetRequestorLeavesNotif")]
        public Leaves GetRequestorLeavesNotif(GetLeavesID GL)
        {
            try
            {
                Connection con = new Connection();
                Leaves leaveslist = new Leaves();
                List<GetLeaves> list = new List<GetLeaves>();
                con.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.NVarChar, Value = GL.EmpID });
                DataTable dt = con.GetDataTable("sp_HRMS_isSeenRequestorLeaves_Mobile");

                foreach (DataRow row in dt.Rows)
                {
                    //string half = row["isHalDay"].ToString();
                    //    if (half == "0")
                    //{
                    //    half = "No";
                    //}
                    //    else
                    //{
                    //    half = "Yes";
                    //}
                    GetLeaves param = new GetLeaves()
                    {
                        ID = row["ID"].ToString(),
                        EmpID = row["EmpID"].ToString(),
                        EmpName = row["EmpName"].ToString(),
                        ManagerID = row["MngrID"].ToString(),
                        ManagerName = row["MngrName"].ToString(),
                        LeaveDate = row["LeaveDate"].ToString(),
                        LeaveType = row["LeaveType"].ToString(),
                        DateApplied = row["DateApplied"].ToString(),
                        Halfday = row["isHalfDay"].ToString(),
                        //LeaveID = row["LeaveID"].ToString(),
                        //Gender = row["Gender"].ToString(),
                        //Expired = row["Expr1"].ToString(),
                        //Approver = row["isApprover"].ToString(),

                    };
                    list.Add(param);
                }
                leaveslist.LeavesListData = list;
                leaveslist._myreturn = "Success";
                return leaveslist;
            }
            catch (Exception e)
            {
                Leaves leaveslist = new Leaves();
                List<GetLeaves> list = new List<GetLeaves>();
                leaveslist.LeavesListData = list;
                leaveslist._myreturn = e.ToString();
                return leaveslist;
            }
        }

        //Get Notifications in Logs for Requestor

        [HttpPost]
        [Route("GetRequestorLogsNotif")]
        public Logs GetRequestorLogsNotif(GetLogsID GL)
        {
            try
            {
                Connection con = new Connection();
                Logs logslist = new Logs();
                List<GetLogs> list = new List<GetLogs>();
                con.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.NVarChar, Value = GL.EmpID });
                DataTable dt = con.GetDataTable("sp_HRMS_isSeenRequestorLogs_Mobile");

                foreach (DataRow row in dt.Rows)
                {
                    GetLogs param = new GetLogs()
                    {
                        SeriesID = row["SeriesID"].ToString(),
                        EmpID = row["EmpID"].ToString(),
                        EmpName = row["EmpName"].ToString(),
                        StartTime = row["Start_Time"].ToString(),
                        EndTime = row["End_Time"].ToString(),
                        Reason = row["ReasonDesc"].ToString(),
                        SchedDate = row["SchedDate"].ToString(),
                        TotalTime = row["TotalTimeMin"].ToString(),
                        Status = row["Activity_Status"].ToString(),
                        //Approver = row["isApprover"].ToString(),

                    };
                    list.Add(param);
                }
                logslist.LogsListData = list;
                logslist._myreturn = "Success";
                return logslist;
            }
            catch (Exception e)
            {
                Logs logslist = new Logs();
                List<GetLogs> list = new List<GetLogs>();
                logslist.LogsListData = list;
                logslist._myreturn = e.ToString();
                return logslist;
            }
        }





        //--------------------------End Leaves and Logs - Nhoraisa


        [HttpPost]
        [Route("InsertAllAnswerSurvey")]
        public string InsertSelectedFunction(DataTableAnswerInsert AnswerList)
        {
            try
            {
                Connection con = new Connection();
                DataTable Mytable = new DataTable();
                Mytable.Columns.AddRange(new DataColumn[2] { new DataColumn("Choice1"), new DataColumn("Choice2") });
                var myarray = AnswerList.list.Split('_');
                for (var s = 0; s <= myarray.Length - 1; s++)
                {
                    Mytable.Rows.Add(s + 1, myarray[s]);
                }
                //for (int i = 0; i < AnswerList.list.Length; i++)
                //{
                //    Mytable.Rows.Add(i + 1, AnswerList.list[i]);
                //}
                con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.NVarChar, Value = AnswerList._EmpID });
                con.myparameters.Add(new myParameters { ParameterName = "@surveyID", mytype = SqlDbType.NVarChar, Value = AnswerList._SurveyID });
                con.myparameters.Add(new myParameters { ParameterName = "@Qno", mytype = SqlDbType.NVarChar, Value = AnswerList._Qno });
                con.myparameters.Add(new myParameters { ParameterName = "@Qtype", mytype = SqlDbType.NVarChar, Value = AnswerList._Qtype });
                con.myparameters.Add(new myParameters { ParameterName = "@Qtion", mytype = SqlDbType.NVarChar, Value = AnswerList._Qtion });
                con.myparameters.Add(new myParameters { ParameterName = "@choices", mytype = SqlDbType.Structured, Value = Mytable });
                con.ExecuteScalar("sp_FillAnswer");
                return "Succes";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }











        [HttpPost]
        [Route("CancelLeaves")]
        public string CancelLeaves(LeaveMngt LeaveMngt)
        {
            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = LeaveMngt.NTID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@LeaveDate", mytype = SqlDbType.NVarChar, Value = Convert.ToDateTime(LeaveMngt.LeaveDate).ToString("yyyy-MM-dd") });
            Connection.myparameters.Add(new myParameters { ParameterName = "@LeaveType", mytype = SqlDbType.NVarChar, Value = LeaveMngt.LeaveType });

            // DataTable EmployeeDt = new DataTable();
            String ID = Connection.ExecuteScalar("sp_CancelEmployeeLeave_Mobile");

            return ID;

        }

        [HttpPost]
        [Route("ShowLeavesReports")]
        public GetLeaveResult ShowLeavesReports(LeaveParameters LeaveParameters)
        {
            GetLeaveResult GetLeaveResult = new GetLeaveResult();
            try
            {
                Connection Connection = new Connection();
                List<LeaveMngt> ListLeave = new List<LeaveMngt>();
                DataSet ds = new DataSet();
                DataTable dtLEAVES = new DataTable();
                //BasicFunction BasicFunction = new BasicFunction();
                Connection.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = LeaveParameters.NTID });
                //Connection.myparameters.Add(new myParameters { ParameterName = "@STARTDATE", mytype = SqlDbType.NVarChar, Value = LeaveParameters.Start_Date });
                //Connection.myparameters.Add(new myParameters { ParameterName = "@ENDDATE", mytype = SqlDbType.NVarChar, Value = LeaveParameters.End_Date });
                ds = Connection.GetDataset("sp_SearchTodoLeaveReport_Mobile");
                dtLEAVES = ds.Tables[0];
                if (dtLEAVES.Rows.Count > 0)
                {
                    foreach (DataRow row in dtLEAVES.Rows)
                    {
                        LeaveMngt Leave = new LeaveMngt
                        {
                            LeaveID = row["ID"].ToString(),
                            EmployeeName = row["EmpName"].ToString(),
                            LeaveType = row["LeaveType"].ToString(),
                            LeaveDate = row["LeaveDate"].ToString() == "" ? null : Convert.ToDateTime(row["LeaveDate"]).ToString("yyyy-MM-dd"),
                            LeaveStatus = row["LeaveStatus"].ToString(),
                            isPaid = row["isPaid"].ToString(),
                            DateApplied = row["DateApplied"].ToString() == "" ? null : Convert.ToDateTime(row["DateApplied"]).ToString("yyyy-MM-dd"),
                            isHalfDay = row["isHalfDay"].ToString(),
                            ApprovedCancelDate = row["DateApproved"].ToString() == "" ? null : Convert.ToDateTime(row["DateApproved"]).ToString("yyyy-MM-dd")

                        };
                        ListLeave.Add(Leave);
                    }
                    GetLeaveResult.Leave = ListLeave;
                    GetLeaveResult.myreturn = "Success";
                }
                else
                {
                    GetLeaveResult.myreturn = "No Data Available";
                }
            }
            catch (Exception ex)
            {
                GetLeaveResult.myreturn = ex.Message;
            }
            return GetLeaveResult;
        }




        //Leave Notification
        [HttpPost]
        [Route("ShowLeaveNotif")]
        public GetLeaveNotif ShowLeaveNotif(LeaveNotification LeaveNotification)
        {
            GetLeaveNotif GetLeaveResult = new GetLeaveNotif();
            try
            {
                Connection Connection = new Connection();
                List<LeaveNotification> ListLeaveNotif = new List<LeaveNotification>();
                DataSet ds = new DataSet();
                DataTable dtLEAVES = new DataTable();
                //BasicFunction BasicFunction = new BasicFunction();
                Connection.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = LeaveNotification.ImEmpID });

                ds = Connection.GetDataset("sp_MobileGetLeaveNotif");
                dtLEAVES = ds.Tables[0];
                if (dtLEAVES.Rows.Count > 0)
                {
                    foreach (DataRow row in dtLEAVES.Rows)
                    {
                        LeaveNotification LeaveN = new LeaveNotification
                        {
                            ID = row["ID"].ToString(),
                            Subordinate = row["EmpName"].ToString(),
                            LeaveDate = Convert.ToDateTime(row["LeaveDate"]).ToString("MMM-dd-yyyy"),
                            LeaveType = row["LeaveType"].ToString(),
                            IsHalfDay = row["isHalDay"].ToString()

                        };
                        ListLeaveNotif.Add(LeaveN);
                    }
                    GetLeaveResult.LeaveNotif = ListLeaveNotif;
                    GetLeaveResult.myreturn = "Success";
                }
                else
                {
                    GetLeaveResult.myreturn = "No Data Available";
                }
            }
            catch (Exception ex)
            {
                GetLeaveResult.myreturn = ex.Message;
            }
            return GetLeaveResult;
        }

        //UPDATE NOTIF
        [HttpPost]
        [Route("UpdateLeaveNotif")]
        public string UpdateLeaveNotif(LeaveNotification LNOTIF)
        {
            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@ID", mytype = SqlDbType.NVarChar, Value = LNOTIF.ID });
            // DataTable EmployeeDt = new DataTable();
            String ID = Connection.ExecuteScalar("sp_UpdateLeaveNotif");

            return ID;

        }


        //---------------------------------------------------------------Revised Change DB Start------------------------------------------------------------------------

        [HttpPost]
        [Route("DisplayLogsForApprovalMobilev2")]
        public ShowLogs GetLogsv2(ShowLogsIDv2 GL)
        {

            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = GL.CN;
            SelfieRegistration2Controller.GetDB2(GDB);

            try
            {
                Connection con = new Connection();
                ShowLogs logslist = new ShowLogs();
                List<ShowGetLogs> list = new List<ShowGetLogs>();
                con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = GL.EmpID });
                DataTable dt = con.GetDataTable("sp_GetAll_Logs1");

                foreach (DataRow row in dt.Rows)
                {
                    ShowGetLogs param = new ShowGetLogs()
                    {
                        SeriesID = row["SeriesID"].ToString(),
                        EmpID = row["EmpID"].ToString(),
                        EmpName = row["FullName"].ToString(),
                        StartTime = row["Start_Time"].ToString(),
                        EndTime = row["EndTime"].ToString(),
                        Reason = row["ReasonDesc"].ToString(),
                        SchedDate = row["SchedDate"].ToString(),
                        TotalTime = row["TotalTimeMin"].ToString(),
                        Status = row["ActivityStatus"].ToString(),
                        Approver = row["isApprover"].ToString(),
                        EmpComment = row["EmpComment"].ToString(),
                        FilePath = row["FilePath"].ToString(),

                    };
                    list.Add(param);
                }
                logslist.LogsListData = list;
                logslist._myreturn = "Success";
                return logslist;
            }
            catch (Exception e)
            {
                ShowLogs logslist = new ShowLogs();
                List<ShowGetLogs> list = new List<ShowGetLogs>();
                logslist.LogsListData = list;
                logslist._myreturn = e.ToString();
                return logslist;
            }
        }

        [HttpPost]
        [Route("DisplayLogsForApprovalMobilev3")]
        public ShowLogs GetLogsv3(ShowLogsIDv2 GL)
        {

            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = GL.CN;
            SelfieRegistration2Controller.GetDB2(GDB);

            try
            {
                Connection con = new Connection();
                ShowLogs logslist = new ShowLogs();
                List<ShowGetLogs> list = new List<ShowGetLogs>();
                con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = GL.EmpID });
                DataTable dt = con.GetDataTable("sp_GetAll_Logs1");

                foreach (DataRow row in dt.Rows)
                {
                    ShowGetLogs param = new ShowGetLogs()
                    {
                        SeriesID = row["SeriesID"].ToString(),
                        EmpID = row["EmpID"].ToString(),
                        EmpName = row["FullName"].ToString(),
                        StartTime = row["Start_Time"].ToString(),
                        EndTime = row["EndTime"].ToString(),
                        Reason = row["ReasonDesc"].ToString(),
                        SchedDate = row["SchedDate"].ToString(),
                        TotalTime = row["TotalTimeMin"].ToString(),
                        Status = row["ActivityStatus"].ToString(),
                        Approver = row["isApprover"].ToString(),
                        FilePath = row["FilePath"].ToString(),
                        EmpComment = row["EmpComment"].ToString(),

                    };
                    list.Add(param);
                }
                logslist.LogsListData = list;
                logslist._myreturn = "Success";
                return logslist;
            }
            catch (Exception e)
            {
                ShowLogs logslist = new ShowLogs();
                List<ShowGetLogs> list = new List<ShowGetLogs>();
                logslist.LogsListData = list;
                logslist._myreturn = e.ToString();
                return logslist;
            }
        }
        [HttpPost]
        [Route("DisplayLogsForApprovalMobilev4")]
        public ShowLogs GetLogsv4(ShowLogsIDv2 GL)
        {

            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = GL.CN;
            SelfieRegistration2Controller.GetDB2(GDB);

            try
            {
                Connection con = new Connection();
                ShowLogs logslist = new ShowLogs();
                List<ShowGetLogs> list = new List<ShowGetLogs>();
                con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = GL.EmpID });
                DataTable dt = con.GetDataTable("sp_GetAll_ApproverLogs_Mobile");

                foreach (DataRow row in dt.Rows)
                {
                    ShowGetLogs param = new ShowGetLogs()
                    {
                        SeriesID = row["SeriesID"].ToString(),
                        EmpID = row["EmpID"].ToString(),
                        EmpName = row["FullName"].ToString(),
                        StartTime = row["Start_Time"].ToString(),
                        EndTime = row["EndTime"].ToString(),
                        Reason = row["ReasonDesc"].ToString(),
                        SchedDate = row["SchedDate"].ToString(),
                        TotalTime = row["TotalTimeMin"].ToString(),
                        Status = row["ActivityStatus"].ToString(),
                        Approver = row["isApprover"].ToString(),
                        FilePath = row["FilePath"].ToString(),
                        EmpComment = row["EmpComment"].ToString(),
                        Location = row["Location"].ToString(),
                        ShiftNum = row["ShiftNum"].ToString()

                    };
                    list.Add(param);
                }
                logslist.LogsListData = list;
                logslist._myreturn = "Success";
                return logslist;
            }
            catch (Exception e)
            {
                ShowLogs logslist = new ShowLogs();
                List<ShowGetLogs> list = new List<ShowGetLogs>();
                logslist.LogsListData = list;
                logslist._myreturn = e.ToString();
                return logslist;
            }
        }

        [HttpPost]
        [Route("GetLogsForApprovalMobile")]
        public ShowLogs GetLogsForApprovalMobile(ShowLogsIDv2 GL)
        {

            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = GL.CN;
            SelfieRegistration2Controller.GetDB2(GDB);

            try
            {
                Connection con = new Connection();
                ShowLogs logslist = new ShowLogs();
                List<ShowGetLogs> list = new List<ShowGetLogs>();
                con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = GL.EmpID });
                DataTable dt = con.GetDataTable("sp_Mobile_GetAll_Logs");

                foreach (DataRow row in dt.Rows)
                {
                    ShowGetLogs param = new ShowGetLogs()
                    {
                        SeriesID = row["SeriesID"].ToString(),
                        EmpID = row["EmpID"].ToString(),
                        EmpName = row["FullName"].ToString(),
                        StartTime = row["Start_Time"].ToString(),
                        EndTime = row["EndTime"].ToString(),
                        Reason = row["ReasonDesc"].ToString(),
                        SchedDate = row["SchedDate"].ToString(),
                        TotalTime = row["TotalTimeMin"].ToString(),
                        Status = row["ActivityStatus"].ToString(),
                        Approver = row["isApprover"].ToString(),
                        FilePath = row["FilePath"].ToString(),
                        EmpComment = row["EmpComment"].ToString(),
                        ShiftNum = row["ShiftNum"].ToString()

                    };
                    list.Add(param);
                }
                logslist.LogsListData = list;
                logslist._myreturn = "Success";
                return logslist;
            }
            catch (Exception e)
            {
                ShowLogs logslist = new ShowLogs();
                List<ShowGetLogs> list = new List<ShowGetLogs>();
                logslist.LogsListData = list;
                logslist._myreturn = e.ToString();
                return logslist;
            }
        }
        [HttpPost]
        [Route("GetLogsForApprovalMobilev2")]
        public ShowLogs GetLogsForApprovalMobilev2(ShowLogsIDv2 GL)
        {

            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = GL.CN;
            SelfieRegistration2Controller.GetDB2(GDB);

            try
            {
                Connection con = new Connection();
                ShowLogs logslist = new ShowLogs();
                List<ShowGetLogs> list = new List<ShowGetLogs>();
                con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = GL.EmpID });
                DataTable dt = con.GetDataTable("sp_Mobile_GetAll_Logs");

                foreach (DataRow row in dt.Rows)
                {
                    ShowGetLogs param = new ShowGetLogs()
                    {
                        SeriesID = row["SeriesID"].ToString(),
                        EmpID = row["EmpID"].ToString(),
                        EmpName = row["FullName"].ToString(),
                        StartTime = row["Start_Time"].ToString(),
                        EndTime = row["EndTime"].ToString(),
                        Reason = row["ReasonDesc"].ToString(),
                        SchedDate = row["SchedDate"].ToString(),
                        TotalTime = row["TotalTimeMin"].ToString(),
                        Status = row["ActivityStatus"].ToString(),
                        Approver = row["isApprover"].ToString(),
                        FilePath = row["FilePath"].ToString(),
                        EmpComment = row["EmpComment"].ToString(),
                        ShiftNum = row["ShiftNum"].ToString(),
                        Location = row["Location"].ToString()
                    };
                    list.Add(param);
                }
                //Connection con2 = new Connection();
                //con2.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = GL.EmpID });
                //DataTable dt2 = con2.GetDataTable("sp_Mobile_GetAll_Logs_Segment_OT");

                //foreach (DataRow row in dt2.Rows)
                //{
                //    ShowGetLogs param = new ShowGetLogs()
                //    {
                //        SeriesID = row["SeriesID"].ToString(),
                //        EmpID = row["EmpID"].ToString(),
                //        EmpName = row["FullName"].ToString(),
                //        StartTime = row["Start_Time"].ToString(),
                //        EndTime = row["EndTime"].ToString(),
                //        Reason = row["ReasonDesc"].ToString(),
                //        SchedDate = row["SchedDate"].ToString(),
                //        TotalTime = row["TotalTimeMin"].ToString(),
                //        Status = row["ActivityStatus"].ToString(),
                //        Approver = row["isApprover"].ToString(),
                //        FilePath = row["FilePath"].ToString(),
                //        EmpComment = row["EmpComment"].ToString(),
                //        ShiftNum = row["ShiftNum"].ToString()

                //    };
                //    list.Add(param);
                //}
                logslist.LogsListData = list;
                logslist._myreturn = "Success";
                return logslist;
            }
            catch (Exception e)
            {
                ShowLogs logslist = new ShowLogs();
                List<ShowGetLogs> list = new List<ShowGetLogs>();
                logslist.LogsListData = list;
                logslist._myreturn = e.ToString();
                return logslist;
            }
        }
        [HttpPost]
        [Route("GetLogsForApprovalMobilev3")]
        public ShowLogs GetLogsForApprovalMobilev3(ShowLogsIDv2 GL)
        {

            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = GL.CN;
            SelfieRegistration2Controller.GetDB2(GDB);

            try
            {
                Connection con = new Connection();
                ShowLogs logslist = new ShowLogs();
                List<ShowGetLogs> list = new List<ShowGetLogs>();
                con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = GL.EmpID });
                DataTable dt2 = con.GetDataTable("sp_Mobile_GetAll_Logs_Segment_OT");


                foreach (DataRow row in dt2.Rows)
                {
                    ShowGetLogs param = new ShowGetLogs()
                    {
                        SeriesID = row["SeriesID"].ToString(),
                        EmpID = row["EmpID"].ToString(),
                        EmpName = row["FullName"].ToString(),
                        StartTime = row["Start_Time"].ToString(),
                        EndTime = row["EndTime"].ToString(),
                        Reason = row["ReasonDesc"].ToString(),
                        SchedDate = row["SchedDate"].ToString(),
                        TotalTime = row["TotalTimeMin"].ToString(),
                        Status = row["ActivityStatus"].ToString(),
                        Approver = row["isApprover"].ToString(),
                        FilePath = row["FilePath"].ToString(),
                        EmpComment = row["EmpComment"].ToString(),
                        ShiftNum = row["ShiftNum"].ToString(),

                        Location = row["NearestEstablishment"].ToString()
                    };
                    list.Add(param);
                }
                logslist.LogsListData = list;
                logslist._myreturn = "Success";
                return logslist;
            }
            catch (Exception e)
            {
                ShowLogs logslist = new ShowLogs();
                List<ShowGetLogs> list = new List<ShowGetLogs>();
                logslist.LogsListData = list;
                logslist._myreturn = e.ToString();
                return logslist;
            }
        }
        [HttpPost]
        [Route("ShowLeavesForApprovalMobilev2")]
        public ShowLeaves ShowLeavesv2(ShowLeavesIDv2 GL)
        {
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = GL.CN;
            SelfieRegistration2Controller.GetDB2(GDB);

            try
            {
                Connection con = new Connection();
                ShowLeaves leaveslist = new ShowLeaves();
                List<GetShowLeaves> list = new List<GetShowLeaves>();
                con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = GL.EmpID });
                DataTable dt = con.GetDataTable("sp_GetAll_Leaves1");

                foreach (DataRow row in dt.Rows)
                {

                    GetShowLeaves param = new GetShowLeaves()
                    {
                        ID = row["ID"].ToString(),
                        EmpID = row["EmpID"].ToString(),
                        EmpName = row["EmpName"].ToString(),
                        ManagerID = row["MngrID"].ToString(),
                        ManagerName = row["MngrName"].ToString(),
                        LeaveDate = row["LeaveDate"].ToString(),
                        LeaveType = row["LeaveType"].ToString(),
                        DateApplied = row["DateApplied"].ToString(),
                        Halfday = row["isHalDay"].ToString(),
                        LeaveID = row["LeaveID"].ToString(),
                        //Gender = row["Gender"].ToString(),
                        //Expired = row["Expr1"].ToString(),a
                        Approver = row["isApprover"].ToString(),
                        ShiftType = row["ShiftType"].ToString(),
                        EmpComment = row["EmpComment"].ToString(),
                        EmpReason = row["EmpReason"].ToString(),
                        ApproverComment = row["ApproverComment"].ToString(),
                        FilePath = row["FilePath"].ToString()
                    };
                    list.Add(param);
                }
                leaveslist.LeavesListData = list;
                leaveslist._myreturn = "Success";
                return leaveslist;
            }
            catch (Exception e)
            {
                ShowLeaves leaveslist = new ShowLeaves();
                List<GetShowLeaves> list = new List<GetShowLeaves>();
                leaveslist.LeavesListData = list;
                leaveslist._myreturn = e.ToString();
                return leaveslist;
            }
        }

        [HttpPost]
        [Route("ShowLeavesForApprovalMobileGen")]
        public ShowLeaves ShowLeavesForApprovalMobileGen(ShowLeavesIDv2 GL)
        {
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = GL.CN;
            SelfieRegistration2Controller.GetDB2(GDB);

            try
            {
                Connection con = new Connection();
                ShowLeaves leaveslist = new ShowLeaves();
                List<GetShowLeaves> list = new List<GetShowLeaves>();
                con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = GL.EmpID });
                DataTable dt = con.GetDataTable("sp_GetAll_LeavesGen");

                foreach (DataRow row in dt.Rows)
                {

                    GetShowLeaves param = new GetShowLeaves()
                    {
                        ID = row["ID"].ToString(),
                        EmpID = row["EmpID"].ToString(),
                        EmpName = row["EmpName"].ToString(),
                        ManagerID = row["MngrID"].ToString(),
                        ManagerName = row["MngrName"].ToString(),
                        LeaveDate = row["LeaveDate"].ToString(),
                        LeaveType = row["LeaveType"].ToString(),
                        DateApplied = row["DateApplied"].ToString(),
                        Halfday = row["isHalDay"].ToString(),
                        //LeaveID = row["LeaveID"].ToString(),
                        //Gender = row["Gender"].ToString(),
                        //Expired = row["Expr1"].ToString(),a
                        Approver = row["isApprover"].ToString(),
                        ShiftType = row["ShiftType"].ToString(),
                        EmpComment = row["EmpComment"].ToString(),
                        EmpReason = row["EmpReason"].ToString(),
                        ApproverComment = row["ApproverComment"].ToString(),
                        FilePath = row["FilePath"].ToString(),
                        LeaveHours = row["LeaveHours"].ToString(),
                        LeaveMinutes = row["LeaveMinutes"].ToString()
                    };
                    list.Add(param);
                }
                leaveslist.LeavesListData = list;
                leaveslist._myreturn = "Success";
                return leaveslist;
            }
            catch (Exception e)
            {
                ShowLeaves leaveslist = new ShowLeaves();
                List<GetShowLeaves> list = new List<GetShowLeaves>();
                leaveslist.LeavesListData = list;
                leaveslist._myreturn = e.ToString();
                return leaveslist;
            }
        }

        [HttpPost]
        [Route("ShowLeavesv2")]
        public GetLeaveResult ShowLeavesv2(LeaveParametersv2 LeaveParameters)
        {
            GetLeaveResult GetLeaveResult = new GetLeaveResult();
            try
            {
                GetDatabase GDB = new GetDatabase();
                GDB.ClientName = LeaveParameters.CN;
                SelfieRegistration2Controller.GetDB2(GDB);

                Connection Connection = new Connection();
                List<LeaveMngt> ListLeave = new List<LeaveMngt>();
                DataSet ds = new DataSet();
                DataTable dtLEAVES = new DataTable();
                //BasicFunction BasicFunction = new BasicFunction();
                Connection.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = LeaveParameters.NTID });
                //Connection.myparameters.Add(new myParameters { ParameterName = "@STARTDATE", mytype = SqlDbType.NVarChar, Value = LeaveParameters.Start_Date });
                //Connection.myparameters.Add(new myParameters { ParameterName = "@ENDDATE", mytype = SqlDbType.NVarChar, Value = LeaveParameters.End_Date });
                ds = Connection.GetDataset("sp_ShowLeaves_Mobile");
                dtLEAVES = ds.Tables[0];
                if (dtLEAVES.Rows.Count > 0)
                {
                    foreach (DataRow row in dtLEAVES.Rows)
                    {
                        LeaveMngt Leave = new LeaveMngt
                        {
                            LeaveDate = Convert.ToDateTime(row["LeaveDate"]).ToString("MMM-dd-yyyy"),
                            isHalfDay = row["isHalfDay"].ToString(),
                            isPaid = row["isPaid"].ToString(),
                            LeaveStatus = row["LeaveStatus"].ToString(),
                            LeaveType = row["LeaveType"].ToString(),
                            DateApplied = row["DateApplied"].ToString() == "" ? null : Convert.ToDateTime(row["DateApplied"]).ToString("MMM-dd-yyyy"),
                            LeaveID = row["ID"].ToString()
                        };
                        ListLeave.Add(Leave);
                    }
                    GetLeaveResult.Leave = ListLeave;
                    GetLeaveResult.myreturn = "Success";
                }
                else
                {
                    GetLeaveResult.myreturn = "No Data Available";
                }
            }
            catch (Exception ex)
            {
                GetLeaveResult.myreturn = ex.Message;
            }
            return GetLeaveResult;
        }
        [HttpPost]
        [Route("ShowLeavesv3")]
        public GetLeaveResult ShowLeavesv3(LeaveParametersv2 LeaveParameters)
        {
            GetLeaveResult GetLeaveResult = new GetLeaveResult();
            try
            {
                GetDatabase GDB = new GetDatabase();
                GDB.ClientName = LeaveParameters.CN;
                SelfieRegistration2Controller.GetDB2(GDB);

                Connection Connection = new Connection();
                List<LeaveMngt> ListLeave = new List<LeaveMngt>();
                DataSet ds = new DataSet();
                DataTable dtLEAVES = new DataTable();
                //BasicFunction BasicFunction = new BasicFunction();
                Connection.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = LeaveParameters.NTID });
                //Connection.myparameters.Add(new myParameters { ParameterName = "@STARTDATE", mytype = SqlDbType.NVarChar, Value = LeaveParameters.Start_Date });
                //Connection.myparameters.Add(new myParameters { ParameterName = "@ENDDATE", mytype = SqlDbType.NVarChar, Value = LeaveParameters.End_Date });
                ds = Connection.GetDataset("sp_ShowLeaves_Mobilev2");
                dtLEAVES = ds.Tables[0];
                if (dtLEAVES.Rows.Count > 0)
                {
                    foreach (DataRow row in dtLEAVES.Rows)
                    {
                        LeaveMngt Leave = new LeaveMngt
                        {
                            LeaveDate = Convert.ToDateTime(row["LeaveDate"]).ToString("MMM-dd-yyyy"),
                            isHalfDay = row["isHalfDay"].ToString(),
                            isPaid = row["isPaid"].ToString(),
                            LeaveStatus = row["LeaveStatus"].ToString(),
                            LeaveType = row["LeaveType"].ToString(),
                            DateApplied = row["DateApplied"].ToString() == "" ? null : Convert.ToDateTime(row["DateApplied"]).ToString("MMM-dd-yyyy"),
                            LeaveID = row["ID"].ToString(),
                            ShiftType = row["ShiftType"].ToString(),
                            EmpReason = row["EmpReason"].ToString(),
                            EmpComment = row["EmpComment"].ToString(),
                            ApproverComment = row["ApproverComment"].ToString(),
                            FilePath = row["FilePath"].ToString(),
                            LeaveID2 = row["LeaveID"].ToString()
                        };
                        ListLeave.Add(Leave);
                    }
                    GetLeaveResult.Leave = ListLeave;
                    GetLeaveResult.myreturn = "Success";
                }
                else
                {
                    GetLeaveResult.myreturn = "No Data Available";
                }
            }
            catch (Exception ex)
            {
                GetLeaveResult.myreturn = ex.Message;
            }
            return GetLeaveResult;
        }
        [HttpPost]
        [Route("DHRFlightBalance")]
        public FlightBalance DHRFlightBalance(FlightParam FlightParam)
        {
            FlightBalance FlightBalance = new FlightBalance();
            try
            {
                GetDatabase GDB = new GetDatabase();
                GDB.ClientName = FlightParam.CN;
                SelfieRegistration2Controller.GetDB2(GDB);

                Connection Connection = new Connection();
                List<GetFlightBalance> ListSettings = new List<GetFlightBalance>();
                DataSet ds = new DataSet();
                DataTable dtLEAVES = new DataTable();

                Connection.myparameters.Add(new myParameters { ParameterName = "@Empid", mytype = SqlDbType.NVarChar, Value = FlightParam.EMPID });

                ds = Connection.GetDataset("sp_GetFlightBalance");
                dtLEAVES = ds.Tables[0];

                if (dtLEAVES.Rows.Count > 0)
                {
                    foreach (DataRow row in dtLEAVES.Rows)
                    {
                        GetFlightBalance FlightBalances = new GetFlightBalance
                        {
                            FLIGHTBALANCE = row["LeaveBalance"].ToString()

                        };
                        ListSettings.Add(FlightBalances);
                    }
                    FlightBalance.DHRFlightBalance = ListSettings;
                    FlightBalance.myreturn = "Success";
                }
                else
                {
                    FlightBalance.myreturn = "No Data Available";
                }
            }
            catch (Exception ex)
            {
                FlightBalance.myreturn = ex.Message;
            }
            return FlightBalance;
        }

        //get leave type setting
        [HttpPost]
        [Route("GetLeaveTypeSetting")]
        public GetLeaveTypeSetting GetLeaveTypeSetting(LTypeSetting LP)
        {

            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = LP.CN;
            SelfieRegistration2Controller.GetDB2(GDB);

            GetLeaveTypeSetting GetLeaveResult = new GetLeaveTypeSetting();
            try
            {
                Connection Connection = new Connection();
                List<GetLeaveTypeSettinglist> ListLeave = new List<GetLeaveTypeSettinglist>();
                DataSet ds = new DataSet();
                DataTable dtLEAVES = new DataTable();
                //BasicFunction BasicFunction = new BasicFunction();
                Connection.myparameters.Add(new myParameters { ParameterName = "@leaveID", mytype = SqlDbType.NVarChar, Value = LP.leaveID });
                ds = Connection.GetDataset("sp_GetLeaveTypeSetting");
                dtLEAVES = ds.Tables[0];
                if (dtLEAVES.Rows.Count > 0)
                {
                    foreach (DataRow row in dtLEAVES.Rows)
                    {
                        GetLeaveTypeSettinglist Leave = new GetLeaveTypeSettinglist
                        {
                            ID = row["ID"].ToString(),
                            EmpLevelId = row["EmpLevelId"].ToString(),
                            EmpCategory = row["EmpCategory"].ToString(),
                            LeaveID = row["LeaveID"].ToString(),
                            CreditWhen = row["CreditWhen"].ToString(),
                            NumberLeaveToCredit = row["NumberLeaveToCredit"].ToString(),
                            ExemptionFrom = row["ExemptionFrom"].ToString(),
                            ExemptionTo = row["ExemptionTo"].ToString(),
                            CashConvertFrom = row["CashConvertFrom"].ToString(),
                            CashConvertTo = row["CashConvertTo"].ToString(),
                            CashConvertPaidOn = row["CashConvertPaidOn"].ToString(),
                            MaxCreditsConvert = row["MaxCreditsConvert"].ToString(),
                            CarryOverFrom = row["CarryOverFrom"].ToString(),
                            CarryOverTo = row["CarryOverTo"].ToString(),
                            CarryOverMax = row["CarryOverMax"].ToString(),
                            DateModified = row["DateModified"].ToString(),
                            isCashConvert = row["isCashConvert"].ToString(),
                            isCarryOver = row["isCarryOver"].ToString(),
                            StartAccrue = row["StartAccrue"].ToString(),
                            StartAccrueEvery = row["StartAccrueEvery"].ToString(),
                            StartAccrue_IfDayOption = row["StartAccrue_IfDayOption"].ToString(),
                            StartAccrueFrom = row["StartAccrueFrom"].ToString(),
                            StartAccrueNum = row["StartAccrueNum"].ToString(),
                            WillAccrueInc = row["WillAccrueInc"].ToString(),
                            StartInc = row["StartInc"].ToString(),
                            StartIncEvery = row["StartIncEvery"].ToString(),
                            StartInc_IfDayOption = row["StartInc_IfDayOption"].ToString(),
                            StartIncFrom = row["StartIncFrom"].ToString(),
                            DateSet = row["DateSet"].ToString(),
                            ExitLeaveConversion = row["ExitLeaveConversion"].ToString(),
                            ExitMaxCredit = row["ExitMaxCredit"].ToString(),
                            MaxLeaveAfterLeaveDate = row["MaxLeaveAfterLeaveDate"].ToString(),
                            LeavesWithTax = row["LeavesWithTax"].ToString(),
                            isOffsetLimit = row["isOffsetLimit"].ToString(),
                            minOffsetOTHrs = row["minOffsetOTHrs"].ToString(),
                            maxOffsetOTHrs = row["maxOffsetOTHrs"].ToString(),
                            MaxLeaveBeforeLeaveDate = row["MaxLeaveBeforeLeaveDate"].ToString(),
                        };
                        ListLeave.Add(Leave);
                    }
                    GetLeaveResult.Leave = ListLeave;
                    GetLeaveResult.myreturn = "Success";
                }
                else
                {
                    GetLeaveResult.myreturn = "No Data Available";
                }
            }
            catch (Exception ex)
            {
                GetLeaveResult.myreturn = ex.Message;
            }
            return GetLeaveResult;
        }
        [HttpPost]
        [Route("GetDHRCompanyBenefitsLeavev1")]
        public LeaveTypeSettings CompanyBenefitsLeaves(SettingsParameter SettingsParameter)
        {
            LeaveTypeSettings CompanyBenefitsLeaves = new LeaveTypeSettings();
            try
            {
                GetDatabase GDB = new GetDatabase();
                GDB.ClientName = SettingsParameter.CN;
                SelfieRegistration2Controller.GetDB2(GDB);

                Connection Connection = new Connection();
                List<GetLeaveTypeSettings> ListSettings = new List<GetLeaveTypeSettings>();
                DataSet ds = new DataSet();
                DataTable dtLEAVES = new DataTable();

                Connection.myparameters.Add(new myParameters { ParameterName = "@SeriesID", mytype = SqlDbType.NVarChar, Value = SettingsParameter.SERIESID });

                ds = Connection.GetDataset("sp_GetDHRCompanyBenefitsLeavesv1");
                dtLEAVES = ds.Tables[0];

                if (dtLEAVES.Rows.Count > 0)
                {
                    foreach (DataRow row in dtLEAVES.Rows)
                    {
                        GetLeaveTypeSettings SettingResult = new GetLeaveTypeSettings
                        {
                            MOBILE = row["Mobile"].ToString(),
                            WEB = row["Web"].ToString(),
                            ALLOWUSERTRACK = row["allowUserTrack"].ToString(),

                        };
                        ListSettings.Add(SettingResult);
                    }
                    CompanyBenefitsLeaves.CompanyBenefitsLeaves = ListSettings;
                    CompanyBenefitsLeaves.myreturn = "Success";
                }
                else
                {
                    CompanyBenefitsLeaves.myreturn = "No Data Available";
                }
            }
            catch (Exception ex)
            {
                CompanyBenefitsLeaves.myreturn = ex.Message;
            }
            return CompanyBenefitsLeaves;
        }
        [HttpPost]
        [Route("GetDHRFillingSettings")]
        public FillingSettings GetFillingSettings(FillingParameters SettingsParameter)
        {
            FillingSettings FillingSettings = new FillingSettings();
            try
            {
                GetDatabase GDB = new GetDatabase();
                GDB.ClientName = SettingsParameter.CN;
                SelfieRegistration2Controller.GetDB2(GDB);

                Connection Connection = new Connection();
                List<GetFillingSettings> ListSettings = new List<GetFillingSettings>();
                DataSet ds = new DataSet();
                DataTable dtLEAVES = new DataTable();

                Connection.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.NVarChar, Value = SettingsParameter.EMPID });
                Connection.myparameters.Add(new myParameters { ParameterName = "@LeaveTypeID", mytype = SqlDbType.NVarChar, Value = SettingsParameter.LEAVETYPEID });

                ds = Connection.GetDataset("sp_GetNextLeaveSettings");
                dtLEAVES = ds.Tables[0];

                if (dtLEAVES.Rows.Count > 0)
                {
                    foreach (DataRow row in dtLEAVES.Rows)
                    {
                        GetFillingSettings SettingResult = new GetFillingSettings
                        {
                            LATEDAY = row[0].ToString(),
                            DATETODAY = row[1].ToString(),
                            LATEFILLING = row[2].ToString(),
                            EARLYFILLING = row[3].ToString(),
                            EARLYDAY = row[4].ToString(),
                            CANCELFILLING = row[5].ToString(),
                            CANCELDAY = row[6].ToString()
                        };
                        ListSettings.Add(SettingResult);
                    }
                    FillingSettings.GetFillingSettings = ListSettings;
                    FillingSettings.myreturn = "Success";
                }
                else
                {
                    FillingSettings.myreturn = "No Data Available";
                }
            }
            catch (Exception ex)
            {
                FillingSettings.myreturn = ex.Message;
            }
            return FillingSettings;
        }

        [HttpPost]
        [Route("ShowAvailableLeavesv2")]
        public GetAvailableLeaves ShowAvailableLeavesv2(LeaveParametersv2 LeaveParameters)
        {
            GetAvailableLeaves GetLeaveResult = new GetAvailableLeaves();
            try
            {
                GetDatabase GDB = new GetDatabase();
                GDB.ClientName = LeaveParameters.CN;
                SelfieRegistration2Controller.GetDB2(GDB);

                Connection Connection = new Connection();
                List<AvailableLeave> ListLeave = new List<AvailableLeave>();
                List<AvailableLastLeave> ListlastLeave = new List<AvailableLastLeave>();
                DataSet ds = new DataSet();
                DataTable dtLEAVES = new DataTable();
                Connection.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = LeaveParameters.NTID });
                ds = Connection.GetDataset("sp_AvailableLeaves_Mobile");
                dtLEAVES = ds.Tables[0];
                if (dtLEAVES.Rows.Count > 0)
                {
                    foreach (DataRow row in dtLEAVES.Rows)
                    {
                        AvailableLeave AvailableLeaves = new AvailableLeave
                        {
                            LeaveBal = row["LeaveBalance"].ToString(),
                            Ltype = row["Ltype"].ToString(),
                            SerialID = row["SerialID"].ToString(),
                        };
                        ListLeave.Add(AvailableLeaves);
                    }
                    DataRow firstRow = dtLEAVES.Rows[0];
                    GetLeaveResult.AvailableLeaves = ListLeave;
                    GetLeaveResult.AvailableLastLeaves = Convert.ToDateTime(firstRow["LeaveDate"]).ToString("MMM-dd-yyyy");
                    GetLeaveResult.myreturn = "Success";
                }
                else
                {
                    GetLeaveResult.myreturn = "No Data Available";
                }
            }
            catch (Exception ex)
            {
                GetLeaveResult.myreturn = ex.Message;
            }
            return GetLeaveResult;
        }
        [HttpPost]
        [Route("ShowAvailableLeaveGen")]
        public GetAvailableLeavesGenList ShowAvailableLeaveGen(LeaveParametersv2 leaveParams)
        {
            GetAvailableLeavesGenList getAvailableLeavesGenList = new GetAvailableLeavesGenList();
            try
            {
                GetDatabase GDB = new GetDatabase();
                GDB.ClientName = leaveParams.CN;
                SelfieRegistration2Controller.GetDB2(GDB);

                Connection con = new Connection();
                List<GetAvailableLeavesGen> getAvailableLeavesGen = new List<GetAvailableLeavesGen>();
                DataSet ds = new DataSet();
                DataTable dt = new DataTable();
                con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = leaveParams.NTID });
                ds = con.GetDataset("sp_AvailableLeavesGen_Mobile");
                dt = ds.Tables[0];

                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        GetAvailableLeavesGen getAvailableLeaves = new GetAvailableLeavesGen
                        {
                            id = row["id"].ToString(),
                            LType = row["LType"].ToString(),
                            serialID = row["SerialID"].ToString(),
                            leaveBalance = row["LeaveBalance"].ToString(),
                            leaveDate = row["LeaveDate"].ToString(),
                            leaveHourse = row["LeaveHours"].ToString()
                        };
                        getAvailableLeavesGen.Add(getAvailableLeaves);
                    }
                    DataRow firstRow = dt.Rows[0];
                    getAvailableLeavesGenList.availableLeavesGen = getAvailableLeavesGen;
                    getAvailableLeavesGenList.AvailableLastLeaves = Convert.ToDateTime(firstRow["LeaveDate"]).ToString("MMM-dd-yyyy");
                    getAvailableLeavesGenList.myReturn = "Success";
                }
                else
                {
                    getAvailableLeavesGenList.myReturn = "No Data Available";
                }
            }
            catch (Exception ex)
            {
                getAvailableLeavesGenList.myReturn = ex.ToString();
            }
            return getAvailableLeavesGenList;
        }
        [HttpPost]
        [Route("ShowAvailableLeavesCTRACK")]
        public GetAvailableLeavesCTRACK ShowAvailableLeavesCTRACK(CTRACKLeaveParameters LeaveParameters)
        {
            GetAvailableLeavesCTRACK GetLeaveResult = new GetAvailableLeavesCTRACK();
            try
            {
                GetDatabase GDB = new GetDatabase();
                GDB.ClientName = LeaveParameters.CN;
                SelfieRegistration2Controller.GetDB2(GDB);

                Connection Connection = new Connection();
                List<AvailableLeaveCTRACK> ListLeave = new List<AvailableLeaveCTRACK>();
                DataSet ds = new DataSet();
                DataTable dtLEAVES = new DataTable();
                Connection.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.NVarChar, Value = LeaveParameters.EMPID });
                Connection.myparameters.Add(new myParameters { ParameterName = "@dateselected", mytype = SqlDbType.NVarChar, Value = LeaveParameters.SELECTEDDATE });
                Connection.myparameters.Add(new myParameters { ParameterName = "@LeaveID", mytype = SqlDbType.NVarChar, Value = LeaveParameters.LEAVEID });
                ds = Connection.GetDataset("sp_GetLeaveMinutesandWorkedHoursv2");
                dtLEAVES = ds.Tables[0];
                if (dtLEAVES.Rows.Count > 0)
                {
                    foreach (DataRow row in dtLEAVES.Rows)
                    {
                        AvailableLeaveCTRACK AvailableLeaves = new AvailableLeaveCTRACK
                        {
                            LeaveDays = row["LeaveDays"].ToString(),
                            LeaveHours = row["LeaveMinutes"].ToString()
                        };
                        ListLeave.Add(AvailableLeaves);
                    }
                    DataRow firstRow = dtLEAVES.Rows[0];
                    GetLeaveResult.CTRACTAvailableLeaves = ListLeave;
                    GetLeaveResult.myreturn = "Success";
                }
                else
                {
                    GetLeaveResult.myreturn = "No Data Available";
                }
            }
            catch (Exception ex)
            {
                GetLeaveResult.myreturn = ex.Message;
            }
            return GetLeaveResult;
        }
        [HttpPost]
        [Route("FileLeavev2")]
        public string FileANewLeavev2(LeaveMngtv2 FileNewLeave)
        {
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = FileNewLeave.CN;
            SelfieRegistration2Controller.GetDB2(GDB);

            Connection Connection = new Connection();
            Connection.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = FileNewLeave.NTID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@LEAVEDATE", mytype = SqlDbType.NVarChar, Value = FileNewLeave.LeaveDate });
            Connection.myparameters.Add(new myParameters { ParameterName = "@LEAVETYPE", mytype = SqlDbType.NVarChar, Value = FileNewLeave.LeaveType });
            Connection.myparameters.Add(new myParameters { ParameterName = "@LEAVEID", mytype = SqlDbType.NVarChar, Value = FileNewLeave.LeaveID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@ISHALFDAY", mytype = SqlDbType.NVarChar, Value = FileNewLeave.isHalfDay });
            Connection.myparameters.Add(new myParameters { ParameterName = "@LOCATION", mytype = SqlDbType.NVarChar, Value = FileNewLeave.Coordinates });
            String ID = Connection.ExecuteScalar("sp_InsertNewLeave_Mobile");
            return ID;
        }

        [HttpPost]
        [Route("FileLeavevv3")]
        public string FileANewLeavevv3(LeaveMngtv2 FileNewLeave)
        {
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = FileNewLeave.CN;
            SelfieRegistration2Controller.GetDB2(GDB);

            var time = DateTime.Now;
            string formattedTime = time.ToString("yyyy-MM-dd-hh-mm-ss-fff");

            Connection Connection = new Connection();
            Connection.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = FileNewLeave.NTID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@LEAVEDATE", mytype = SqlDbType.NVarChar, Value = FileNewLeave.LeaveDate });
            Connection.myparameters.Add(new myParameters { ParameterName = "@LEAVETYPE", mytype = SqlDbType.NVarChar, Value = FileNewLeave.LeaveType });
            Connection.myparameters.Add(new myParameters { ParameterName = "@LEAVEID", mytype = SqlDbType.NVarChar, Value = FileNewLeave.LeaveID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@ISHALFDAY", mytype = SqlDbType.NVarChar, Value = FileNewLeave.isHalfDay });
            Connection.myparameters.Add(new myParameters { ParameterName = "@LOCATION", mytype = SqlDbType.NVarChar, Value = FileNewLeave.Coordinates });
            Connection.myparameters.Add(new myParameters { ParameterName = "@CN", mytype = SqlDbType.NVarChar, Value = FileNewLeave.CN });
            Connection.myparameters.Add(new myParameters { ParameterName = "@SHIFTTYPE", mytype = SqlDbType.NVarChar, Value = FileNewLeave.ShiftType });
            Connection.myparameters.Add(new myParameters { ParameterName = "@EMPREASON", mytype = SqlDbType.NVarChar, Value = FileNewLeave.EmpReason });
            Connection.myparameters.Add(new myParameters { ParameterName = "@EMPCOMMENT", mytype = SqlDbType.NVarChar, Value = FileNewLeave.EmpComment });
            Connection.myparameters.Add(new myParameters { ParameterName = "@FILEPATH", mytype = SqlDbType.NVarChar, Value = formattedTime });
            String ID = Connection.ExecuteScalar("sp_InsertNewLeave_Mobilev2");
            byte[] PIC = Convert.FromBase64String(FileNewLeave.FilePath);

            var path = HttpContext.Current.Server.MapPath("~/ProfilePhotos/" + FileNewLeave.CN.ToLower() + "/" + "");
            var folder = Path.Combine(path, FileNewLeave.NTID);
            Directory.CreateDirectory(folder);
            MemoryStream stream = new MemoryStream();
            using (stream)
            {

                File.WriteAllBytes(folder + "/" + FileNewLeave.NTID + "_" + formattedTime + ".png", PIC);

            }
            emprequestleave emp = new emprequestleave();
            // emp.empid = FileNewLeave1.NTID;
            // emp.IsHalfDay = FileNewLeave1.isHalfDay;
            // emp.IsPaid = FileNewLeave1.isPaid;
            // emp.LeaveDate = FileNewLeave1.LeaveDate;
            // emp.LeaveID = FileNewLeave1.LeaveID;
            //emp.LeaveType = FileNewLeave1.LeaveType;
            //emp.timefiled = DateTime.Now.ToString();

            // Commented due to an error
            SendEmailNotifLeaves(emp);
            return "Success";
        }

        [HttpPost]
        [Route("FileLeavevv4")]
        public string FileANewLeavevv4(LeaveMngtv3 FileNewLeave1)
        {
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = FileNewLeave1.CN;
            SelfieRegistration2Controller.GetDB2(GDB);

            var time = DateTime.Now;
            string formattedTime = time.ToString("yyyy-MM-dd-hh-mm-ss-fff");


            Connection Connection = new Connection();
            Connection.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = FileNewLeave1.NTID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@LEAVEDATE", mytype = SqlDbType.NVarChar, Value = FileNewLeave1.LeaveDate });
            Connection.myparameters.Add(new myParameters { ParameterName = "@ISHALFDAY", mytype = SqlDbType.NVarChar, Value = FileNewLeave1.isHalfDay });
            Connection.myparameters.Add(new myParameters { ParameterName = "@LEAVETIMEBALANCE", mytype = SqlDbType.NVarChar, Value = FileNewLeave1.LeaveHours });
            Connection.myparameters.Add(new myParameters { ParameterName = "@ISPAID", mytype = SqlDbType.NVarChar, Value = FileNewLeave1.isPaid });
            Connection.myparameters.Add(new myParameters { ParameterName = "@DATEAPPLIED", mytype = SqlDbType.NVarChar, Value = FileNewLeave1.DateApplied });
            Connection.myparameters.Add(new myParameters { ParameterName = "@LEAVESTATUS", mytype = SqlDbType.NVarChar, Value = FileNewLeave1.LeaveStatus });
            Connection.myparameters.Add(new myParameters { ParameterName = "@LASTDATEMODIFIED", mytype = SqlDbType.DateTime, Value = FileNewLeave1.LastDateMod });
            Connection.myparameters.Add(new myParameters { ParameterName = "@SUBMITTED", mytype = SqlDbType.NVarChar, Value = FileNewLeave1.Submitted });
            Connection.myparameters.Add(new myParameters { ParameterName = "@LEAVETYPE", mytype = SqlDbType.NVarChar, Value = FileNewLeave1.LeaveType });
            Connection.myparameters.Add(new myParameters { ParameterName = "@LOCATION", mytype = SqlDbType.NVarChar, Value = FileNewLeave1.Coordinates });
            Connection.myparameters.Add(new myParameters { ParameterName = "@GENDER", mytype = SqlDbType.NVarChar, Value = FileNewLeave1.Gender });
            Connection.myparameters.Add(new myParameters { ParameterName = "@RULES", mytype = SqlDbType.NVarChar, Value = FileNewLeave1.Rules });
            Connection.myparameters.Add(new myParameters { ParameterName = "@LEAVEID", mytype = SqlDbType.NVarChar, Value = FileNewLeave1.LeaveID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@CN", mytype = SqlDbType.NVarChar, Value = FileNewLeave1.CN });
            Connection.myparameters.Add(new myParameters { ParameterName = "@SHIFTTYPE", mytype = SqlDbType.NVarChar, Value = FileNewLeave1.ShiftType });
            Connection.myparameters.Add(new myParameters { ParameterName = "@EMPREASON", mytype = SqlDbType.NVarChar, Value = FileNewLeave1.EmpReason });
            Connection.myparameters.Add(new myParameters { ParameterName = "@EMPCOMMENT", mytype = SqlDbType.NVarChar, Value = FileNewLeave1.EmpComment });
            Connection.myparameters.Add(new myParameters { ParameterName = "@FILEPATH", mytype = SqlDbType.NVarChar, Value = formattedTime });
            String ID = Connection.ExecuteScalar("sp_InsertHRMSLeaveMaster");
            byte[] PIC = Convert.FromBase64String(FileNewLeave1.FilePath);

            var path = HttpContext.Current.Server.MapPath("~/ProfilePhotos/" + FileNewLeave1.CN.ToLower() + "/" + "");
            var folder = Path.Combine(path, FileNewLeave1.NTID);
            Directory.CreateDirectory(folder);
            MemoryStream stream = new MemoryStream();
            using (stream)
            {

                File.WriteAllBytes(folder + "/" + FileNewLeave1.NTID + "_" + formattedTime + ".png", PIC);

            }
            emprequestleave emp = new emprequestleave();
            emp.empid = FileNewLeave1.NTID;
            emp.IsHalfDay = FileNewLeave1.isHalfDay;
            emp.IsPaid = FileNewLeave1.isPaid;
            emp.LeaveDate = FileNewLeave1.LeaveDate;
            emp.LeaveID = FileNewLeave1.LeaveID;
            emp.LeaveType = FileNewLeave1.LeaveType;
            emp.timefiled = DateTime.Now.ToString();
            emp.CN = FileNewLeave1.CN;
            SendEmailNotifLeaves(emp);
            return "Success";
        }

        [HttpPost]
        [Route("FileLeavevv5")]
        public string FileANewLeavevv5(LeaveMngtv3 FileNewLeave1)
        {
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = FileNewLeave1.CN;
            SelfieRegistration2Controller.GetDB2(GDB);

            var time = DateTime.Now;
            string formattedTime = time.ToString("yyyy-MM-dd-hh-mm-ss-fff");


            Connection Connection = new Connection();
            Connection.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = FileNewLeave1.NTID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@LEAVEDATE", mytype = SqlDbType.NVarChar, Value = FileNewLeave1.LeaveDate });
            Connection.myparameters.Add(new myParameters { ParameterName = "@ISHALFDAY", mytype = SqlDbType.NVarChar, Value = FileNewLeave1.isHalfDay });
            Connection.myparameters.Add(new myParameters { ParameterName = "@LEAVETIMEBALANCE", mytype = SqlDbType.NVarChar, Value = FileNewLeave1.LeaveHours });
            Connection.myparameters.Add(new myParameters { ParameterName = "@ISPAID", mytype = SqlDbType.NVarChar, Value = FileNewLeave1.isPaid });
            Connection.myparameters.Add(new myParameters { ParameterName = "@DATEAPPLIED", mytype = SqlDbType.NVarChar, Value = FileNewLeave1.DateApplied });
            Connection.myparameters.Add(new myParameters { ParameterName = "@LEAVESTATUS", mytype = SqlDbType.NVarChar, Value = FileNewLeave1.LeaveStatus });
            Connection.myparameters.Add(new myParameters { ParameterName = "@LASTDATEMODIFIED", mytype = SqlDbType.DateTime, Value = FileNewLeave1.LastDateMod });
            Connection.myparameters.Add(new myParameters { ParameterName = "@SUBMITTED", mytype = SqlDbType.NVarChar, Value = FileNewLeave1.Submitted });
            Connection.myparameters.Add(new myParameters { ParameterName = "@LEAVETYPE", mytype = SqlDbType.NVarChar, Value = FileNewLeave1.LeaveType });
            Connection.myparameters.Add(new myParameters { ParameterName = "@LOCATION", mytype = SqlDbType.NVarChar, Value = FileNewLeave1.Coordinates });
            Connection.myparameters.Add(new myParameters { ParameterName = "@GENDER", mytype = SqlDbType.NVarChar, Value = FileNewLeave1.Gender });
            Connection.myparameters.Add(new myParameters { ParameterName = "@RULES", mytype = SqlDbType.NVarChar, Value = FileNewLeave1.Rules });
            Connection.myparameters.Add(new myParameters { ParameterName = "@LEAVEID", mytype = SqlDbType.NVarChar, Value = FileNewLeave1.LeaveID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@CN", mytype = SqlDbType.NVarChar, Value = FileNewLeave1.CN });
            Connection.myparameters.Add(new myParameters { ParameterName = "@SHIFTTYPE", mytype = SqlDbType.NVarChar, Value = FileNewLeave1.ShiftType });
            Connection.myparameters.Add(new myParameters { ParameterName = "@EMPREASON", mytype = SqlDbType.NVarChar, Value = FileNewLeave1.EmpReason });
            Connection.myparameters.Add(new myParameters { ParameterName = "@EMPCOMMENT", mytype = SqlDbType.NVarChar, Value = FileNewLeave1.EmpComment });
            Connection.myparameters.Add(new myParameters { ParameterName = "@FILEPATH", mytype = SqlDbType.NVarChar, Value = FileNewLeave1.FilePath != "" ? formattedTime : FileNewLeave1.FilePath });

            String ID = Connection.ExecuteScalar("sp_InsertHRMSLeaveMaster");
            byte[] PIC = Convert.FromBase64String(FileNewLeave1.FilePath);

            var path = HttpContext.Current.Server.MapPath("~/ProfilePhotos/" + FileNewLeave1.CN.ToLower() + "/" + "");
            var folder = Path.Combine(path, FileNewLeave1.NTID);
            Directory.CreateDirectory(folder);
            MemoryStream stream = new MemoryStream();
            using (stream)
            {

                File.WriteAllBytes(folder + "/" + FileNewLeave1.NTID + "_" + formattedTime + ".png", PIC);

            }
            emprequestleave emp = new emprequestleave();
            emp.empid = FileNewLeave1.NTID;
            emp.IsHalfDay = FileNewLeave1.isHalfDay;
            emp.IsPaid = FileNewLeave1.isPaid;
            emp.LeaveDate = FileNewLeave1.LeaveDate;
            emp.LeaveID = FileNewLeave1.LeaveID;
            emp.LeaveType = FileNewLeave1.LeaveType;
            emp.timefiled = DateTime.Now.ToString();
            emp.CN = FileNewLeave1.CN;
            SendEmailNotifLeaves(emp);
            return "Success";
        }



        [HttpPost]
        [Route("FileLeavevv6")]
        public string FileANewLeavev6(LeaveMngtv3 FileNewLeave1)
        {
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = FileNewLeave1.CN;
            SelfieRegistration2Controller.GetDB2(GDB);

            var time = DateTime.Now;
            string formattedTime = time.ToString("yyyy-MM-dd-hh-mm-ss-fff");


            Connection Connection = new Connection();
            Connection.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = FileNewLeave1.NTID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@LEAVEDATE", mytype = SqlDbType.NVarChar, Value = FileNewLeave1.LeaveDate });
            Connection.myparameters.Add(new myParameters { ParameterName = "@ISHALFDAY", mytype = SqlDbType.NVarChar, Value = FileNewLeave1.isHalfDay });
            Connection.myparameters.Add(new myParameters { ParameterName = "@LEAVETIMEBALANCE", mytype = SqlDbType.NVarChar, Value = FileNewLeave1.LeaveHours });
            Connection.myparameters.Add(new myParameters { ParameterName = "@ISPAID", mytype = SqlDbType.NVarChar, Value = FileNewLeave1.isPaid });
            Connection.myparameters.Add(new myParameters { ParameterName = "@DATEAPPLIED", mytype = SqlDbType.NVarChar, Value = FileNewLeave1.DateApplied });
            Connection.myparameters.Add(new myParameters { ParameterName = "@LEAVESTATUS", mytype = SqlDbType.NVarChar, Value = FileNewLeave1.LeaveStatus });
            Connection.myparameters.Add(new myParameters { ParameterName = "@LASTDATEMODIFIED", mytype = SqlDbType.DateTime, Value = FileNewLeave1.LastDateMod });
            Connection.myparameters.Add(new myParameters { ParameterName = "@SUBMITTED", mytype = SqlDbType.NVarChar, Value = FileNewLeave1.Submitted });
            Connection.myparameters.Add(new myParameters { ParameterName = "@LEAVETYPE", mytype = SqlDbType.NVarChar, Value = FileNewLeave1.LeaveType });
            Connection.myparameters.Add(new myParameters { ParameterName = "@LOCATION", mytype = SqlDbType.NVarChar, Value = FileNewLeave1.Coordinates });
            Connection.myparameters.Add(new myParameters { ParameterName = "@GENDER", mytype = SqlDbType.NVarChar, Value = FileNewLeave1.Gender });
            Connection.myparameters.Add(new myParameters { ParameterName = "@RULES", mytype = SqlDbType.NVarChar, Value = FileNewLeave1.Rules });
            Connection.myparameters.Add(new myParameters { ParameterName = "@LEAVEID", mytype = SqlDbType.NVarChar, Value = FileNewLeave1.LeaveID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@CN", mytype = SqlDbType.NVarChar, Value = FileNewLeave1.CN });
            Connection.myparameters.Add(new myParameters { ParameterName = "@SHIFTTYPE", mytype = SqlDbType.NVarChar, Value = FileNewLeave1.ShiftType });
            Connection.myparameters.Add(new myParameters { ParameterName = "@EMPREASON", mytype = SqlDbType.NVarChar, Value = FileNewLeave1.EmpReason });
            Connection.myparameters.Add(new myParameters { ParameterName = "@EMPCOMMENT", mytype = SqlDbType.NVarChar, Value = FileNewLeave1.EmpComment });
            Connection.myparameters.Add(new myParameters { ParameterName = "@FILEPATH", mytype = SqlDbType.NVarChar, Value = FileNewLeave1.FilePath != "" ? formattedTime : FileNewLeave1.FilePath });
            Connection.myparameters.Add(new myParameters { ParameterName = "@OFFSETHOURS", mytype = SqlDbType.NVarChar, Value = FileNewLeave1.LeaveHours });

            String ID = Connection.ExecuteScalar("sp_InsertHRMSLeaveMasterOffset");
            byte[] PIC = Convert.FromBase64String(FileNewLeave1.FilePath);

            var path = HttpContext.Current.Server.MapPath("~/ProfilePhotos/" + FileNewLeave1.CN.ToLower() + "/" + "");
            var folder = Path.Combine(path, FileNewLeave1.NTID);
            Directory.CreateDirectory(folder);
            MemoryStream stream = new MemoryStream();
            using (stream)
            {

                File.WriteAllBytes(folder + "/" + FileNewLeave1.NTID + "_" + formattedTime + ".png", PIC);

            }
            emprequestleave emp = new emprequestleave();
            emp.empid = FileNewLeave1.NTID;
            emp.IsHalfDay = FileNewLeave1.isHalfDay;
            emp.IsPaid = FileNewLeave1.isPaid;
            emp.LeaveDate = FileNewLeave1.LeaveDate;
            emp.LeaveID = FileNewLeave1.LeaveID;
            emp.LeaveType = FileNewLeave1.LeaveType;
            emp.timefiled = DateTime.Now.ToString();
            emp.CN = FileNewLeave1.CN;
            SendEmailNotifLeaves(emp);
            return "Success";
        }


        [HttpPost]
        [Route("FileLeaveGen")]
        public string FileLeaveGen(LeaveMngtv3 FileNewLeave1)
        {
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = FileNewLeave1.CN;
            SelfieRegistration2Controller.GetDB2(GDB);

            var time = DateTime.Now;
            string formattedTime = time.ToString("yyyy-MM-dd-hh-mm-ss-fff");


            Connection Connection = new Connection();
            Connection.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = FileNewLeave1.NTID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@LEAVEDATE", mytype = SqlDbType.NVarChar, Value = FileNewLeave1.LeaveDate });
            Connection.myparameters.Add(new myParameters { ParameterName = "@LEAVETYPE", mytype = SqlDbType.NVarChar, Value = FileNewLeave1.LeaveType });
            Connection.myparameters.Add(new myParameters { ParameterName = "@LEAVEID", mytype = SqlDbType.NVarChar, Value = FileNewLeave1.LeaveID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@ISHALFDAY", mytype = SqlDbType.NVarChar, Value = FileNewLeave1.isHalfDay });
            Connection.myparameters.Add(new myParameters { ParameterName = "@LOCATION", mytype = SqlDbType.NVarChar, Value = FileNewLeave1.Coordinates });
            Connection.myparameters.Add(new myParameters { ParameterName = "@CN", mytype = SqlDbType.NVarChar, Value = FileNewLeave1.CN });
            Connection.myparameters.Add(new myParameters { ParameterName = "@SHIFTTYPE", mytype = SqlDbType.NVarChar, Value = FileNewLeave1.ShiftType });
            Connection.myparameters.Add(new myParameters { ParameterName = "@EMPREASON", mytype = SqlDbType.NVarChar, Value = FileNewLeave1.EmpReason });
            Connection.myparameters.Add(new myParameters { ParameterName = "@EMPCOMMENT", mytype = SqlDbType.NVarChar, Value = FileNewLeave1.EmpComment });
            Connection.myparameters.Add(new myParameters { ParameterName = "@FILEPATH", mytype = SqlDbType.NVarChar, Value = formattedTime });
            Connection.myparameters.Add(new myParameters { ParameterName = "@LEAVEHOUR", mytype = SqlDbType.NVarChar, Value = FileNewLeave1.LeaveHours });
            Connection.myparameters.Add(new myParameters { ParameterName = "@LEAVEMINUTES", mytype = SqlDbType.NVarChar, Value = FileNewLeave1.LeaveMinutes });
            String ID = Connection.ExecuteScalar("sp_InsertNewLeave_MobileGen");
            byte[] PIC = Convert.FromBase64String(FileNewLeave1.FilePath);

            var path = HttpContext.Current.Server.MapPath("~/ProfilePhotos/" + FileNewLeave1.CN.ToLower() + "/" + "");
            var folder = Path.Combine(path, FileNewLeave1.NTID);
            Directory.CreateDirectory(folder);
            MemoryStream stream = new MemoryStream();
            using (stream)
            {

                File.WriteAllBytes(folder + "/" + FileNewLeave1.NTID + "_" + formattedTime + ".png", PIC);

            }

            DataTable EmpDT = new DataTable();
            Connection ECon = new Connection();
            ECon.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.NVarChar, Value = FileNewLeave1.NTID });
            EmpDT = ECon.GetDataTable("sp_GetLeaveApproverEmail");

            string EmpName, Emails;
            EmpName = EmpDT.Rows[0][0].ToString();
            Emails = EmpDT.Rows[0][1].ToString() + ";jhonmichaelcarbon21@gmail.com";
            if (FileNewLeave1.CN.ToLower().Contains("generali"))
            {
                string[] emaillist = Emails.Split(';');


                if (string.IsNullOrWhiteSpace(Emails) == false)
                {
                    var message = new MimeKit.MimeMessage();

                    MimeKit.InternetAddressList Elist = new MimeKit.InternetAddressList();
                    foreach (string emailadd in emaillist)
                    {
                        Elist.Add(new MimeKit.MailboxAddress(emailadd));
                    }

                    message.From.Add(new MimeKit.MailboxAddress("durusthr@illimitado.com"));
                    message.To.AddRange(Elist);
                    message.Subject = "Leave Request Notification";
                    message.Body = new MimeKit.TextPart("html") { Text = EmpName + " filed a " + FileNewLeave1.LeaveType + " on " + FileNewLeave1.LeaveDate + " and is pending for approval." };

                    using (var client = new MailKit.Net.Smtp.SmtpClient())
                    {
                        client.ServerCertificateValidationCallback = (s, c, h, e) => true;
                        client.Connect("smtp.office365.com", 587, false);
                        client.Authenticate("durusthr@illimitado.com", "@1230Qwerty");
                        client.Send(message);
                        client.Disconnect(true);

                    }
                }
            }
            //if (FileNewLeave1.CN.ToLower().Contains("campaigntrack"))
            //{
            if (FileNewLeave1.CN.ToLower() != "campaigntrack")
            {
                emprequestleave emp = new emprequestleave();
                emp.empid = FileNewLeave1.NTID;
                emp.IsHalfDay = FileNewLeave1.isHalfDay;
                emp.IsPaid = FileNewLeave1.isPaid;
                emp.LeaveDate = FileNewLeave1.LeaveDate;
                emp.LeaveID = FileNewLeave1.LeaveID;
                emp.LeaveType = FileNewLeave1.LeaveType;
                emp.timefiled = DateTime.Now.ToString();
                emp.LeaveHours = FileNewLeave1.LeaveHours;
                emp.LeaveMinutes = FileNewLeave1.LeaveMinutes;
                emp.LeaveComment = FileNewLeave1.EmpComment;
                emp.CN = FileNewLeave1.CN;
                SendEmailNotifLeaves(emp);
                SendEmailNotifLogstoUser(emp);
            }
            return "Success";
        }

        [HttpPost]
        [Route("FileLeaveGenV2")]
        public string FileLeaveGenV2(LeaveMngtv3 FileNewLeave1)
        {
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = FileNewLeave1.CN;
            SelfieRegistration2Controller.GetDB2(GDB);

            var time = DateTime.Now;
            string formattedTime = time.ToString("yyyy-MM-dd-hh-mm-ss-fff");


            Connection Connection = new Connection();
            Connection.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = FileNewLeave1.NTID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@LEAVEDATE", mytype = SqlDbType.NVarChar, Value = FileNewLeave1.LeaveDate });
            Connection.myparameters.Add(new myParameters { ParameterName = "@LeaveRequestStartDate", mytype = SqlDbType.NVarChar, Value = FileNewLeave1.LeaveRequestStartDate });
            Connection.myparameters.Add(new myParameters { ParameterName = "@LeaveRequestEndDate", mytype = SqlDbType.NVarChar, Value = FileNewLeave1.LeaveRequestEndDate });
            Connection.myparameters.Add(new myParameters { ParameterName = "@multiple", mytype = SqlDbType.NVarChar, Value = FileNewLeave1.multiple });
            Connection.myparameters.Add(new myParameters { ParameterName = "@LEAVETYPE", mytype = SqlDbType.NVarChar, Value = FileNewLeave1.LeaveType });
            Connection.myparameters.Add(new myParameters { ParameterName = "@LEAVEID", mytype = SqlDbType.NVarChar, Value = FileNewLeave1.LeaveID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@ISHALFDAY", mytype = SqlDbType.NVarChar, Value = FileNewLeave1.isHalfDay });
            Connection.myparameters.Add(new myParameters { ParameterName = "@LOCATION", mytype = SqlDbType.NVarChar, Value = FileNewLeave1.Coordinates });
            Connection.myparameters.Add(new myParameters { ParameterName = "@CN", mytype = SqlDbType.NVarChar, Value = FileNewLeave1.CN });
            Connection.myparameters.Add(new myParameters { ParameterName = "@SHIFTTYPE", mytype = SqlDbType.NVarChar, Value = FileNewLeave1.ShiftType });
            Connection.myparameters.Add(new myParameters { ParameterName = "@EMPREASON", mytype = SqlDbType.NVarChar, Value = FileNewLeave1.EmpReason });
            Connection.myparameters.Add(new myParameters { ParameterName = "@EMPCOMMENT", mytype = SqlDbType.NVarChar, Value = FileNewLeave1.EmpComment });
            Connection.myparameters.Add(new myParameters { ParameterName = "@FILEPATH", mytype = SqlDbType.NVarChar, Value = formattedTime });
            Connection.myparameters.Add(new myParameters { ParameterName = "@LEAVEHOUR", mytype = SqlDbType.NVarChar, Value = FileNewLeave1.LeaveHours });
            Connection.myparameters.Add(new myParameters { ParameterName = "@LEAVEMINUTES", mytype = SqlDbType.NVarChar, Value = FileNewLeave1.LeaveMinutes });
            String ID = Connection.ExecuteScalar("sp_InsertNewLeave_MobileGen");
            byte[] PIC = Convert.FromBase64String(FileNewLeave1.FilePath);

            var path = HttpContext.Current.Server.MapPath("~/ProfilePhotos/" + FileNewLeave1.CN.ToLower() + "/" + "");
            var folder = Path.Combine(path, FileNewLeave1.NTID);
            Directory.CreateDirectory(folder);
            MemoryStream stream = new MemoryStream();
            using (stream)
            {

                File.WriteAllBytes(folder + "/" + FileNewLeave1.NTID + "_" + formattedTime + ".png", PIC);

            }

            DataTable EmpDT = new DataTable();
            Connection ECon = new Connection();
            ECon.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.NVarChar, Value = FileNewLeave1.NTID });
            EmpDT = ECon.GetDataTable("sp_GetLeaveApproverEmail");

            string EmpName, Emails;
            EmpName = EmpDT.Rows[0][0].ToString();
            Emails = EmpDT.Rows[0][1].ToString();
            if (FileNewLeave1.CN.ToLower().Contains("generali"))
            {
                string[] emaillist = Emails.Split(';');


                if (string.IsNullOrWhiteSpace(Emails) == false)
                {
                    var message = new MimeKit.MimeMessage();

                    MimeKit.InternetAddressList Elist = new MimeKit.InternetAddressList();
                    foreach (string emailadd in emaillist)
                    {
                        Elist.Add(new MimeKit.MailboxAddress(emailadd));
                    }

                    message.From.Add(new MimeKit.MailboxAddress("notifications@illimitado.com"));
                    message.To.AddRange(Elist);
                    message.Subject = "Leave Request Notification";
                    message.Body = new MimeKit.TextPart("html") { Text = EmpName + " filed a " + FileNewLeave1.LeaveRequestStartDate + " on " + FileNewLeave1.LeaveDate + " and is pending for approval." };

                    using (var client = new MailKit.Net.Smtp.SmtpClient())
                    {
                        client.ServerCertificateValidationCallback = (s, c, h, e) => true;
                        client.Connect("box1256.bluehost.com", 465, true);
                        client.Authenticate("notifications@illimitado.com", "Not1f@1230");
                        client.Send(message);
                        client.Disconnect(true);
                    }
                }
            }
            //if (FileNewLeave1.CN.ToLower().Contains("ctrackuat"))
            //{

            emprequestleave emp = new emprequestleave();
            emp.empid = FileNewLeave1.NTID;
            emp.IsHalfDay = FileNewLeave1.isHalfDay;
            emp.IsPaid = FileNewLeave1.isPaid;
            emp.LeaveDate = FileNewLeave1.LeaveDate;
            emp.LeaveRequestStartDate = FileNewLeave1.LeaveRequestStartDate;
            emp.LeaveRequestEndDate = FileNewLeave1.LeaveRequestEndDate;
            emp.multiple = FileNewLeave1.multiple;
            emp.LeaveID = FileNewLeave1.LeaveID;
            emp.LeaveType = FileNewLeave1.LeaveType;
            emp.timefiled = DateTime.Now.ToString();
            emp.LeaveHours = FileNewLeave1.LeaveHours;
            emp.LeaveMinutes = FileNewLeave1.LeaveMinutes;
            emp.LeaveComment = FileNewLeave1.EmpComment;
            emp.CN = FileNewLeave1.CN;
            SendEmailNotifLeaves(emp);
            SendEmailNotifLogstoUser(emp);
            //}
            return "Success";
        }




        [HttpPost]
        [Route("CancelLeaveGenEmail")]
        public string CancelLeaveGenEmail(LeaveMngtv3 CancelLeave)
        {
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = CancelLeave.CN;
            SelfieRegistration2Controller.GetDB2(GDB);

            DataTable EmpDT = new DataTable();
            Connection ECon = new Connection();
            ECon.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.NVarChar, Value = CancelLeave.NTID });
            EmpDT = ECon.GetDataTable("sp_GetLeaveApproverEmail");

            string EmpName, Emails;
            EmpName = EmpDT.Rows[0][0].ToString();
            Emails = EmpDT.Rows[0][1].ToString();

            if (CancelLeave.CN.ToLower().Contains("generali"))
            {
                string[] emaillist = Emails.Split(';');


                if (string.IsNullOrWhiteSpace(Emails) == false)
                {
                    var message = new MimeKit.MimeMessage();

                    MimeKit.InternetAddressList Elist = new MimeKit.InternetAddressList();
                    foreach (string emailadd in emaillist)
                    {
                        Elist.Add(new MimeKit.MailboxAddress(emailadd));
                    }

                    message.From.Add(new MimeKit.MailboxAddress("notifications@illimitado.com"));
                    message.To.AddRange(Elist);
                    message.Subject = "Leave Request Notification";
                    message.Body = new MimeKit.TextPart("html") { Text = EmpName + " cancelled " + CancelLeave.LeaveType.Replace("Leave", "") + " Leave on " + CancelLeave.LeaveDate + "." };

                    using (var client = new MailKit.Net.Smtp.SmtpClient())
                    {
                        client.ServerCertificateValidationCallback = (s, c, h, e) => true;
                        client.Connect("box1256.bluehost.com", 465, true);
                        client.Authenticate("notifications@illimitado.com", "Not1f@1230");
                        client.Send(message);
                        client.Disconnect(true);
                    }
                }

            }


            return "success";
        }


        [HttpPost]
        [Route("CancelLeavesv2")]
        public string CancelLeavesv2(LeaveMngtv2 LeaveMngt)
        {
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = LeaveMngt.CN;
            SelfieRegistration2Controller.GetDB2(GDB);

            Connection Connection = new Connection();
            Connection.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = LeaveMngt.NTID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@LeaveDate", mytype = SqlDbType.NVarChar, Value = Convert.ToDateTime(LeaveMngt.LeaveDate).ToString("yyyy-MM-dd") });
            Connection.myparameters.Add(new myParameters { ParameterName = "@LeaveType", mytype = SqlDbType.NVarChar, Value = LeaveMngt.LeaveType });
            Connection.myparameters.Add(new myParameters { ParameterName = "@ID", mytype = SqlDbType.NVarChar, Value = LeaveMngt.ID });
            String ID = Connection.ExecuteScalar("sp_CancelEmployeeLeave_Mobile");
            return ID;
        }

        [HttpPost]
        [Route("CancelLeavesGen")]
        public string CancelLeavesGen(LeaveMngtv2 LeaveMngt)
        {
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = LeaveMngt.CN;
            SelfieRegistration2Controller.GetDB2(GDB);

            Connection Connection = new Connection();
            Connection.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = LeaveMngt.NTID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@LeaveDate", mytype = SqlDbType.NVarChar, Value = Convert.ToDateTime(LeaveMngt.LeaveDate).ToString("yyyy-MM-dd") });
            Connection.myparameters.Add(new myParameters { ParameterName = "@LeaveType", mytype = SqlDbType.NVarChar, Value = LeaveMngt.LeaveType });
            String ID = Connection.ExecuteScalar("sp_CancelEmployeeLeaveGen_Mobile");
            return ID;
        }


        [HttpPost]
        [Route("SearchLeavesv2")]
        public GetLeaveResult SearchLeavesv2(LeaveParametersv2 LeaveParameters)
        {
            GetLeaveResult GetLeaveResult = new GetLeaveResult();
            try
            {
                GetDatabase GDB = new GetDatabase();
                GDB.ClientName = LeaveParameters.CN;
                SelfieRegistration2Controller.GetDB2(GDB);

                Connection Connection = new Connection();
                List<LeaveMngt> ListLeave = new List<LeaveMngt>();
                DataSet ds = new DataSet();
                DataTable dtLEAVES = new DataTable();
                Connection.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = LeaveParameters.NTID });
                Connection.myparameters.Add(new myParameters { ParameterName = "@LeaveDateFrom", mytype = SqlDbType.NVarChar, Value = Convert.ToDateTime(LeaveParameters.Start_Date).ToString("yyyy-MM-dd") });
                Connection.myparameters.Add(new myParameters { ParameterName = "@LeaveDateTo", mytype = SqlDbType.NVarChar, Value = Convert.ToDateTime(LeaveParameters.End_Date).ToString("yyyy-MM-dd") });
                ds = Connection.GetDataset("sp_SearchLeaves_Mobile");
                dtLEAVES = ds.Tables[0];
                if (dtLEAVES.Rows.Count > 0)
                {
                    foreach (DataRow row in dtLEAVES.Rows)
                    {
                        LeaveMngt Leave = new LeaveMngt
                        {
                            LeaveDate = Convert.ToDateTime(row["LeaveDate"]).ToString("MMM-dd-yyyy"),
                            isHalfDay = row["isHalfDay"].ToString(),
                            isPaid = row["isPaid"].ToString(),
                            LeaveStatus = row["LeaveStatus"].ToString(),
                            LeaveType = row["LeaveType"].ToString(),
                            DateApplied = row["DateApplied"].ToString() == "" ? null : Convert.ToDateTime(row["DateApplied"]).ToString("MMM-dd-yyyy"),
                            LeaveID = row["ID"].ToString()
                        };
                        ListLeave.Add(Leave);
                    }
                    GetLeaveResult.Leave = ListLeave;
                    GetLeaveResult.myreturn = "Success";
                }
                else
                {
                    GetLeaveResult.myreturn = "No Data Available";
                }
            }
            catch (Exception ex)
            {
                GetLeaveResult.myreturn = ex.Message;
            }
            return GetLeaveResult;
        }
        [HttpPost]
        [Route("SearchLeavesv3")]
        public GetLeaveResult SearchLeavesv3(LeaveParametersv2 LeaveParameters)
        {
            GetLeaveResult GetLeaveResult = new GetLeaveResult();
            try
            {
                GetDatabase GDB = new GetDatabase();
                GDB.ClientName = LeaveParameters.CN;
                SelfieRegistration2Controller.GetDB2(GDB);

                Connection Connection = new Connection();
                List<LeaveMngt> ListLeave = new List<LeaveMngt>();
                DataSet ds = new DataSet();
                DataTable dtLEAVES = new DataTable();
                Connection.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = LeaveParameters.NTID });
                Connection.myparameters.Add(new myParameters { ParameterName = "@LeaveDateFrom", mytype = SqlDbType.NVarChar, Value = Convert.ToDateTime(LeaveParameters.Start_Date).ToString("yyyy-MM-dd") });
                Connection.myparameters.Add(new myParameters { ParameterName = "@LeaveDateTo", mytype = SqlDbType.NVarChar, Value = Convert.ToDateTime(LeaveParameters.End_Date).ToString("yyyy-MM-dd") });
                ds = Connection.GetDataset("sp_SearchLeaves_Mobilev2");
                dtLEAVES = ds.Tables[0];
                if (dtLEAVES.Rows.Count > 0)
                {
                    foreach (DataRow row in dtLEAVES.Rows)
                    {
                        LeaveMngt Leave = new LeaveMngt
                        {
                            LeaveDate = Convert.ToDateTime(row["LeaveDate"]).ToString("MMM-dd-yyyy"),
                            isHalfDay = row["isHalfDay"].ToString(),
                            isPaid = row["isPaid"].ToString(),
                            LeaveStatus = row["LeaveStatus"].ToString(),
                            LeaveType = row["LeaveType"].ToString(),
                            DateApplied = row["DateApplied"].ToString() == "" ? null : Convert.ToDateTime(row["DateApplied"]).ToString("MMM-dd-yyyy"),
                            LeaveID = row["ID"].ToString(),
                            ShiftType = row["ShiftType"].ToString(),
                            EmpReason = row["EmpReason"].ToString(),
                            EmpComment = row["EmpComment"].ToString(),
                            ApproverComment = row["ApproverComment"].ToString(),
                            FilePath = row["FilePath"].ToString()
                        };
                        ListLeave.Add(Leave);
                    }
                    GetLeaveResult.Leave = ListLeave;
                    GetLeaveResult.myreturn = "Success";
                }
                else
                {
                    GetLeaveResult.myreturn = "No Data Available";
                }
            }
            catch (Exception ex)
            {
                GetLeaveResult.myreturn = ex.Message;
            }
            return GetLeaveResult;
        }

        [HttpPost]
        [Route("GetLeavesNotifv2")]
        public Leaves GetLeavesv2(GetLeavesIDv2 GL)
        {
            try
            {
                GetDatabase GDB = new GetDatabase();
                GDB.ClientName = GL.CN;
                SelfieRegistration2Controller.GetDB2(GDB);

                Connection con = new Connection();
                Leaves leaveslist = new Leaves();
                List<GetLeaves> list = new List<GetLeaves>();
                con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = GL.EmpID });
                DataTable dt = con.GetDataTable("sp_GetAll_Leaves1_NotifMobile");

                foreach (DataRow row in dt.Rows)
                {
                    GetLeaves param = new GetLeaves()
                    {
                        ID = row["ID"].ToString(),
                        EmpID = row["EmpID"].ToString(),
                        EmpName = row["EmpName"].ToString(),
                        ManagerID = row["MngrID"].ToString(),
                        ManagerName = row["MngrName"].ToString(),
                        LeaveDate = row["LeaveDate"].ToString(),
                        LeaveType = row["LeaveType"].ToString(),
                        DateApplied = row["DateApplied"].ToString(),
                        Halfday = row["isHalDay"].ToString(),
                        Approver = row["isApprover"].ToString(),
                    };
                    list.Add(param);
                }
                leaveslist.LeavesListData = list;
                leaveslist._myreturn = "Success";
                return leaveslist;
            }
            catch (Exception e)
            {
                Leaves leaveslist = new Leaves();
                List<GetLeaves> list = new List<GetLeaves>();
                leaveslist.LeavesListData = list;
                leaveslist._myreturn = e.ToString();
                return leaveslist;
            }
        }

        [HttpPost]
        [Route("GetRequestorLeavesNotifv2")]
        public Leaves GetRequestorLeavesNotifv2(GetLeavesIDv2 GL)
        {
            try
            {
                GetDatabase GDB = new GetDatabase();
                GDB.ClientName = GL.CN;
                SelfieRegistration2Controller.GetDB2(GDB);

                Connection con = new Connection();
                Leaves leaveslist = new Leaves();
                List<GetLeaves> list = new List<GetLeaves>();
                con.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.NVarChar, Value = GL.EmpID });
                DataTable dt = con.GetDataTable("sp_HRMS_isSeenRequestorLeaves_Mobile");

                foreach (DataRow row in dt.Rows)
                {
                    GetLeaves param = new GetLeaves()
                    {
                        ID = row["ID"].ToString(),
                        EmpID = row["EmpID"].ToString(),
                        EmpName = row["EmpName"].ToString(),
                        ManagerID = row["MngrID"].ToString(),
                        ManagerName = row["MngrName"].ToString(),
                        LeaveDate = row["LeaveDate"].ToString(),
                        LeaveType = row["LeaveType"].ToString(),
                        DateApplied = row["DateApplied"].ToString(),
                        Halfday = row["isHalfDay"].ToString(),
                    };
                    list.Add(param);
                }
                leaveslist.LeavesListData = list;
                leaveslist._myreturn = "Success";
                return leaveslist;
            }
            catch (Exception e)
            {
                Leaves leaveslist = new Leaves();
                List<GetLeaves> list = new List<GetLeaves>();
                leaveslist.LeavesListData = list;
                leaveslist._myreturn = e.ToString();
                return leaveslist;
            }
        }

        [HttpPost]
        [Route("UpdateisSeenMobilev2")]
        public string updatev2(UpdateisSeenv2 US)
        {
            try
            {
                GetDatabase GDB = new GetDatabase();
                GDB.ClientName = US.CN;
                SelfieRegistration2Controller.GetDB2(GDB);

                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@Type", mytype = SqlDbType.NVarChar, Value = US.Type });
                con.myparameters.Add(new myParameters { ParameterName = "@SeriesID", mytype = SqlDbType.NVarChar, Value = US.SeriesID });
                con.ExecuteScalar("sp_HRMS_isSeen_Mobile");
                return "True";
            }
            catch (Exception)
            {
                return "False";
            }
        }

        [HttpPost]
        [Route("GetLogsNotifv2")]
        public Logs GetLogsv2(GetLogsIDv2 GL)
        {
            try
            {
                GetDatabase GDB = new GetDatabase();
                GDB.ClientName = GL.CN;
                SelfieRegistration2Controller.GetDB2(GDB);

                Connection con = new Connection();
                Logs logslist = new Logs();
                List<GetLogs> list = new List<GetLogs>();
                con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = GL.EmpID });
                DataTable dt = con.GetDataTable("sp_GetAll_Logs1_NotifMobile");

                foreach (DataRow row in dt.Rows)
                {
                    GetLogs param = new GetLogs()
                    {
                        SeriesID = row["SeriesID"].ToString(),
                        EmpID = row["EmpID"].ToString(),
                        EmpName = row["FullName"].ToString(),
                        StartTime = row["Start_Time"].ToString(),
                        EndTime = row["EndTime"].ToString(),
                        Reason = row["ReasonDesc"].ToString(),
                        SchedDate = row["SchedDate"].ToString(),
                        TotalTime = row["TotalTimeMin"].ToString(),
                        Status = row["ActivityStatus"].ToString(),
                        Approver = row["isApprover"].ToString(),

                    };
                    list.Add(param);
                }
                logslist.LogsListData = list;
                logslist._myreturn = "Success";
                return logslist;
            }
            catch (Exception e)
            {
                Logs logslist = new Logs();
                List<GetLogs> list = new List<GetLogs>();
                logslist.LogsListData = list;
                logslist._myreturn = e.ToString();
                return logslist;
            }
        }

        [HttpPost]
        [Route("GetRequestorLogsNotifv2")]
        public Logs GetRequestorLogsNotifv2(GetLogsIDv2 GL)
        {
            try
            {
                GetDatabase GDB = new GetDatabase();
                GDB.ClientName = GL.CN;
                SelfieRegistration2Controller.GetDB2(GDB);

                Connection con = new Connection();
                Logs logslist = new Logs();
                List<GetLogs> list = new List<GetLogs>();
                con.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.NVarChar, Value = GL.EmpID });
                DataTable dt = con.GetDataTable("sp_HRMS_isSeenRequestorLogs_Mobile");

                foreach (DataRow row in dt.Rows)
                {
                    GetLogs param = new GetLogs()
                    {
                        SeriesID = row["SeriesID"].ToString(),
                        EmpID = row["EmpID"].ToString(),
                        EmpName = row["EmpName"].ToString(),
                        StartTime = row["Start_Time"].ToString(),
                        EndTime = row["End_Time"].ToString(),
                        Reason = row["ReasonDesc"].ToString(),
                        SchedDate = row["SchedDate"].ToString(),
                        TotalTime = row["TotalTimeMin"].ToString(),
                        Status = row["Activity_Status"].ToString(),
                    };
                    list.Add(param);
                }
                logslist.LogsListData = list;
                logslist._myreturn = "Success";
                return logslist;
            }
            catch (Exception e)
            {
                Logs logslist = new Logs();
                List<GetLogs> list = new List<GetLogs>();
                logslist.LogsListData = list;
                logslist._myreturn = e.ToString();
                return logslist;
            }
        }

        [HttpPost]
        [Route("InsertAllAnswerSurveyv2")]
        public string InsertSelectedFunctionv2(DataTableAnswerInsertv2 AnswerList)
        {
            try
            {
                GetDatabase GDB = new GetDatabase();
                GDB.ClientName = AnswerList.CN;
                SelfieRegistration2Controller.GetDB2(GDB);

                Connection con = new Connection();
                DataTable Mytable = new DataTable();
                Mytable.Columns.AddRange(new DataColumn[2] { new DataColumn("Choice1"), new DataColumn("Choice2") });
                var myarray = AnswerList.list.Split('_');
                for (var s = 0; s <= myarray.Length - 1; s++)
                {
                    Mytable.Rows.Add(s + 1, myarray[s]);
                }
                con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.NVarChar, Value = AnswerList._EmpID });
                con.myparameters.Add(new myParameters { ParameterName = "@surveyID", mytype = SqlDbType.NVarChar, Value = AnswerList._SurveyID });
                con.myparameters.Add(new myParameters { ParameterName = "@Qno", mytype = SqlDbType.NVarChar, Value = AnswerList._Qno });
                con.myparameters.Add(new myParameters { ParameterName = "@Qtype", mytype = SqlDbType.NVarChar, Value = AnswerList._Qtype });
                con.myparameters.Add(new myParameters { ParameterName = "@Qtion", mytype = SqlDbType.NVarChar, Value = AnswerList._Qtion });
                con.myparameters.Add(new myParameters { ParameterName = "@choices", mytype = SqlDbType.Structured, Value = Mytable });
                con.ExecuteScalar("sp_FillAnswer");
                return "Succes";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }
        [HttpPost]
        [Route("InsertAllAnswerSurveyv3")]
        public string InsertSelectedFunctionv3(DataTableAnswerInsertv2 AnswerList)
        {
            try
            {
                GetDatabase GDB = new GetDatabase();
                GDB.ClientName = AnswerList.CN;
                SelfieRegistration2Controller.GetDB2(GDB);

                Connection con = new Connection();
                DataTable Mytable = new DataTable();
                Mytable.Columns.AddRange(new DataColumn[2] { new DataColumn("Choice1"), new DataColumn("Choice2") });
                var myarray = AnswerList.list.Split('_');
                for (var s = 0; s <= myarray.Length - 1; s++)
                {
                    Mytable.Rows.Add(s + 1, myarray[s]);
                }
                con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.NVarChar, Value = AnswerList._EmpID });
                con.myparameters.Add(new myParameters { ParameterName = "@surveyID", mytype = SqlDbType.NVarChar, Value = AnswerList._SurveyID });
                con.myparameters.Add(new myParameters { ParameterName = "@Qno", mytype = SqlDbType.NVarChar, Value = AnswerList._Qno });
                con.myparameters.Add(new myParameters { ParameterName = "@Qtype", mytype = SqlDbType.NVarChar, Value = AnswerList._Qtype });
                con.myparameters.Add(new myParameters { ParameterName = "@Qtion", mytype = SqlDbType.NVarChar, Value = AnswerList._Qtion });
                con.myparameters.Add(new myParameters { ParameterName = "@choices", mytype = SqlDbType.Structured, Value = Mytable });
                con.ExecuteScalar("sp_Mobile_FillAnswer");
                return "Succes";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }
        [HttpPost]
        [Route("ApproveDenyLogsv2")]
        public string UpdateLogsStatusv2(ApproveDenyLogsv2 US)
        {
            try
            {
                GetDatabase GDB = new GetDatabase();
                GDB.ClientName = US.CN;
                SelfieRegistration2Controller.GetDB2(GDB);
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@seriesID", mytype = SqlDbType.NVarChar, Value = US.SeriesID });
                con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.NVarChar, Value = US.EmpID });
                con.myparameters.Add(new myParameters { ParameterName = "@Status", mytype = SqlDbType.NVarChar, Value = US.Status });
                con.ExecuteScalar("sp_Update_Log_Approvers1_Mobile");
                return "True";
            }
            catch (Exception)
            {
                return "False";
            }
        }

        [HttpPost]
        [Route("ApproveDenyLogsv3")]
        public string UpdateLogsStatusv3(ApproveDenyLogsv2 US)
        {
            try
            {
                GetDatabase GDB = new GetDatabase();
                GDB.ClientName = US.CN;
                SelfieRegistration2Controller.GetDB2(GDB);
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@seriesID", mytype = SqlDbType.NVarChar, Value = US.SeriesID });
                con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.NVarChar, Value = US.EmpID });
                con.myparameters.Add(new myParameters { ParameterName = "@Status", mytype = SqlDbType.NVarChar, Value = US.Status });
                con.myparameters.Add(new myParameters { ParameterName = "@ApproverComment", mytype = SqlDbType.NVarChar, Value = US.ApproverComment });
                con.ExecuteScalar("sp_Update_Log_Approvers1_Mobilev2");
                return "True";
            }
            catch (Exception)
            {
                return "False";
            }
        }

        [HttpPost]
        [Route("ApproveDenyLeavesv2")]
        public string UpdateLeavesStatusv2(ApproveDenyLeavesv2 US)
        {
            try
            {
                GetDatabase GDB = new GetDatabase();
                GDB.ClientName = US.CN;
                SelfieRegistration2Controller.GetDB2(GDB);
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@LeaveID", mytype = SqlDbType.NVarChar, Value = US.SeriesID });
                con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.NVarChar, Value = US.EmpID });
                con.myparameters.Add(new myParameters { ParameterName = "@Status", mytype = SqlDbType.NVarChar, Value = US.Status });
                con.ExecuteScalar("sp_Update_Leave_Approvers1_Mobile");
                return "True";
            }
            catch (Exception)
            {
                return "False";
            }
        }

        [HttpPost]
        [Route("ApproveDenyLeavesv3")]
        public string UpdateLeavesStatusv3(ApproveDenyLeavesv2 US)
        {
            try
            {
                GetDatabase GDB = new GetDatabase();
                GDB.ClientName = US.CN;
                SelfieRegistration2Controller.GetDB2(GDB);
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@LeaveID", mytype = SqlDbType.NVarChar, Value = US.SeriesID });
                con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.NVarChar, Value = US.EmpID });
                con.myparameters.Add(new myParameters { ParameterName = "@Status", mytype = SqlDbType.NVarChar, Value = US.Status });
                con.myparameters.Add(new myParameters { ParameterName = "@ApproverComment", mytype = SqlDbType.NVarChar, Value = US.ApproverComment });
                con.ExecuteScalar("sp_Update_Leave_Approvers2_Mobile");
                return "True";
            }
            catch (Exception)
            {
                return "False";
            }
        }

        [HttpPost]
        [Route("GetNonCancellableLeavesv2")]
        public GetLeaveResultv2 NonCancellableLeaves(ShowLeavesIDv2 SL)
        {
            GetLeaveResultv2 GetLeaveResult = new GetLeaveResultv2();
            try
            {
                GetDatabase GDB = new GetDatabase();
                GDB.ClientName = SL.CN;
                SelfieRegistration2Controller.GetDB2(GDB);

                Connection Connection = new Connection();
                List<LeaveMngt> ListLeave = new List<LeaveMngt>();
                DataSet ds = new DataSet();
                DataTable dtLEAVES = new DataTable();
                //BasicFunction BasicFunction = new BasicFunction();
                Connection.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = SL.EmpID });
                //Connection.myparameters.Add(new myParameters { ParameterName = "@STARTDATE", mytype = SqlDbType.NVarChar, Value = LeaveParameters.Start_Date });
                //Connection.myparameters.Add(new myParameters { ParameterName = "@ENDDATE", mytype = SqlDbType.NVarChar, Value = LeaveParameters.End_Date });
                ds = Connection.GetDataset("sp_Get_NonCancellableLeaves");
                dtLEAVES = ds.Tables[0];
                if (dtLEAVES.Rows.Count > 0)
                {
                    foreach (DataRow row in dtLEAVES.Rows)
                    {
                        LeaveMngt Leave = new LeaveMngt
                        {
                            LeaveDate = Convert.ToDateTime(row["LeaveDate"]).ToString("MMM-dd-yyyy"),
                            isHalfDay = row["isHalfDay"].ToString(),
                            isPaid = row["isPaid"].ToString(),
                            LeaveStatus = row["LeaveStatus"].ToString(),
                            LeaveType = row["LeaveType"].ToString(),
                            DateApplied = row["DateApplied"].ToString() == "" ? null : Convert.ToDateTime(row["DateApplied"]).ToString("MMM-dd-yyyy"),
                            LeaveID = row["ID"].ToString()
                        };
                        ListLeave.Add(Leave);
                    }
                    GetLeaveResult.NonCancellable = ListLeave;
                    GetLeaveResult.myreturn = "Success";
                }
                else
                {
                    GetLeaveResult.myreturn = "No Data Available";
                }
            }
            catch (Exception ex)
            {
                GetLeaveResult.myreturn = ex.Message;
            }
            return GetLeaveResult;
        }
        [HttpPost]
        [Route("ShowLeavesReportsv2")]
        public GetLeaveResult ShowLeavesReportsv2(LeaveParametersv2 LeaveParameters)
        {
            GetLeaveResult GetLeaveResult = new GetLeaveResult();
            try
            {

                GetDatabase GDB = new GetDatabase();
                GDB.ClientName = LeaveParameters.CN;
                SelfieRegistration2Controller.GetDB2(GDB);

                Connection Connection = new Connection();
                List<LeaveMngt> ListLeave = new List<LeaveMngt>();
                DataSet ds = new DataSet();
                DataTable dtLEAVES = new DataTable();
                //BasicFunction BasicFunction = new BasicFunction();
                Connection.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = LeaveParameters.NTID });
                //Connection.myparameters.Add(new myParameters { ParameterName = "@STARTDATE", mytype = SqlDbType.NVarChar, Value = LeaveParameters.Start_Date });
                //Connection.myparameters.Add(new myParameters { ParameterName = "@ENDDATE", mytype = SqlDbType.NVarChar, Value = LeaveParameters.End_Date });
                ds = Connection.GetDataset("sp_SearchTodoLeaveReport_Mobile");
                dtLEAVES = ds.Tables[0];
                if (dtLEAVES.Rows.Count > 0)
                {
                    foreach (DataRow row in dtLEAVES.Rows)
                    {
                        LeaveMngt Leave = new LeaveMngt
                        {
                            LeaveID = row["ID"].ToString(),
                            EmployeeName = row["EmpName"].ToString(),
                            LeaveType = row["LeaveType"].ToString(),
                            LeaveDate = row["LeaveDate"].ToString() == "" ? null : Convert.ToDateTime(row["LeaveDate"]).ToString("yyyy-MM-dd"),
                            LeaveStatus = row["LeaveStatus"].ToString(),
                            isPaid = row["isPaid"].ToString(),
                            DateApplied = row["DateApplied"].ToString() == "" ? null : Convert.ToDateTime(row["DateApplied"]).ToString("yyyy-MM-dd"),
                            isHalfDay = row["isHalfDay"].ToString(),
                            ApprovedCancelDate = row["DateApproved"].ToString() == "" ? null : Convert.ToDateTime(row["DateApproved"]).ToString("yyyy-MM-dd")

                        };
                        ListLeave.Add(Leave);
                    }
                    GetLeaveResult.Leave = ListLeave;
                    GetLeaveResult.myreturn = "Success";
                }
                else
                {
                    GetLeaveResult.myreturn = "No Data Available";
                }
            }
            catch (Exception ex)
            {
                GetLeaveResult.myreturn = ex.Message;
            }
            return GetLeaveResult;
        }

        #region 2018-09-20

        [HttpPost]
        [Route("CancelLeavesDHRv1")]
        public string CancelLeavesDHRv1(leaveDHRv1 LM)
        {
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = LM.CN;
            SelfieRegistration2Controller.GetDB2(GDB);
            Connection Connection = new Connection();
            Connection.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = LM.NTID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@ID", mytype = SqlDbType.NVarChar, Value = LM.ID });
            //Connection.exe("sp_CancelEmployeeLeave_Mobile");
            Connection.ExecuteNonQuery("sp_CancelLeavesDHRv1");
            return "success";
        }

        #endregion
        //---------------------------------------------------------------Revised Change DB Start------------------------------------------------------------------------
        [HttpPost]
        [Route("FileLeavevv7")]
        public string FileANewLeavevv7(LeaveMngtv4 FileNewLeave1)
        {
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = FileNewLeave1.CN;
            SelfieRegistration2Controller.GetDB2(GDB);

            var time = DateTime.Now;
            string formattedTime = time.ToString("yyyy-MM-dd-hh-mm-ss-fff");


            Connection Connection = new Connection();
            Connection.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = FileNewLeave1.NTID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@LEAVEDATE", mytype = SqlDbType.NVarChar, Value = FileNewLeave1.LeaveDate });
            Connection.myparameters.Add(new myParameters { ParameterName = "@ISHALFDAY", mytype = SqlDbType.NVarChar, Value = FileNewLeave1.isHalfDay });
            //Connection.myparameters.Add(new myParameters { ParameterName = "@LEAVETIMEBALANCE", mytype = SqlDbType.NVarChar, Value = FileNewLeave1.LeaveHours });
            Connection.myparameters.Add(new myParameters { ParameterName = "@ISPAID", mytype = SqlDbType.NVarChar, Value = FileNewLeave1.isPaid });
            Connection.myparameters.Add(new myParameters { ParameterName = "@DATEAPPLIED", mytype = SqlDbType.NVarChar, Value = FileNewLeave1.DateApplied });
            Connection.myparameters.Add(new myParameters { ParameterName = "@LEAVESTATUS", mytype = SqlDbType.NVarChar, Value = FileNewLeave1.LeaveStatus });
            Connection.myparameters.Add(new myParameters { ParameterName = "@LASTDATEMODIFIED", mytype = SqlDbType.DateTime, Value = FileNewLeave1.LastDateMod });
            Connection.myparameters.Add(new myParameters { ParameterName = "@SUBMITTED", mytype = SqlDbType.NVarChar, Value = FileNewLeave1.Submitted });
            Connection.myparameters.Add(new myParameters { ParameterName = "@LEAVETYPE", mytype = SqlDbType.NVarChar, Value = FileNewLeave1.LeaveType });
            Connection.myparameters.Add(new myParameters { ParameterName = "@LOCATION", mytype = SqlDbType.NVarChar, Value = FileNewLeave1.Coordinates });
            Connection.myparameters.Add(new myParameters { ParameterName = "@GENDER", mytype = SqlDbType.NVarChar, Value = FileNewLeave1.Gender });
            Connection.myparameters.Add(new myParameters { ParameterName = "@RULES", mytype = SqlDbType.NVarChar, Value = FileNewLeave1.Rules });
            Connection.myparameters.Add(new myParameters { ParameterName = "@LEAVEID", mytype = SqlDbType.NVarChar, Value = FileNewLeave1.LeaveID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@CN", mytype = SqlDbType.NVarChar, Value = FileNewLeave1.CN });
            Connection.myparameters.Add(new myParameters { ParameterName = "@SHIFTTYPE", mytype = SqlDbType.NVarChar, Value = FileNewLeave1.ShiftType });
            Connection.myparameters.Add(new myParameters { ParameterName = "@EMPREASON", mytype = SqlDbType.NVarChar, Value = FileNewLeave1.EmpReason });
            Connection.myparameters.Add(new myParameters { ParameterName = "@EMPCOMMENT", mytype = SqlDbType.NVarChar, Value = FileNewLeave1.EmpComment });
            Connection.myparameters.Add(new myParameters { ParameterName = "@FILEPATH", mytype = SqlDbType.NVarChar, Value = FileNewLeave1.FilePath != "" ? formattedTime : FileNewLeave1.FilePath });
            Connection.myparameters.Add(new myParameters { ParameterName = "@LEAVESTARTDATE", mytype = SqlDbType.NVarChar, Value = FileNewLeave1.LeaveRequestStartDate });
            Connection.myparameters.Add(new myParameters { ParameterName = "@LEAVEENDDATE", mytype = SqlDbType.NVarChar, Value = FileNewLeave1.LeaveRequestEndDate });
            Connection.myparameters.Add(new myParameters { ParameterName = "@MULTIPLE", mytype = SqlDbType.NVarChar, Value = FileNewLeave1.multiple });
            String ID = Connection.ExecuteScalar("sp_InsertHRMSLeaveMaster");
            byte[] PIC = Convert.FromBase64String(FileNewLeave1.FilePath);

            var path = HttpContext.Current.Server.MapPath("~/ProfilePhotos/" + FileNewLeave1.CN.ToLower() + "/" + "");
            var folder = Path.Combine(path, FileNewLeave1.NTID);
            Directory.CreateDirectory(folder);
            MemoryStream stream = new MemoryStream();
            using (stream)
            {

                File.WriteAllBytes(folder + "/" + FileNewLeave1.NTID + "_" + formattedTime + ".png", PIC);

            }
            emprequestleave emp = new emprequestleave();
            emp.empid = FileNewLeave1.NTID;
            emp.IsHalfDay = FileNewLeave1.isHalfDay;
            emp.IsPaid = FileNewLeave1.isPaid;
            emp.LeaveDate = FileNewLeave1.LeaveDate;
            emp.LeaveID = FileNewLeave1.LeaveID;
            emp.LeaveType = FileNewLeave1.LeaveType;
            emp.timefiled = DateTime.Now.ToString();
            emp.CN = FileNewLeave1.CN;
            SendEmailNotifLeaves(emp);
            return "Success";
        }
        [HttpPost]
        [Route("FileLeavevv8")]
        public string FileANewLeavev8(LeaveMngtv4 FileNewLeave1)
        {
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = FileNewLeave1.CN;
            SelfieRegistration2Controller.GetDB2(GDB);

            var time = DateTime.Now;
            string formattedTime = time.ToString("yyyy-MM-dd-hh-mm-ss-fff");


            Connection Connection = new Connection();
            Connection.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = FileNewLeave1.NTID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@LEAVEDATE", mytype = SqlDbType.NVarChar, Value = FileNewLeave1.LeaveDate });
            Connection.myparameters.Add(new myParameters { ParameterName = "@ISHALFDAY", mytype = SqlDbType.NVarChar, Value = FileNewLeave1.isHalfDay });
            Connection.myparameters.Add(new myParameters { ParameterName = "@LEAVETIMEBALANCE", mytype = SqlDbType.NVarChar, Value = FileNewLeave1.LeaveHours });
            Connection.myparameters.Add(new myParameters { ParameterName = "@ISPAID", mytype = SqlDbType.NVarChar, Value = FileNewLeave1.isPaid });
            Connection.myparameters.Add(new myParameters { ParameterName = "@DATEAPPLIED", mytype = SqlDbType.NVarChar, Value = FileNewLeave1.DateApplied });
            Connection.myparameters.Add(new myParameters { ParameterName = "@LEAVESTATUS", mytype = SqlDbType.NVarChar, Value = FileNewLeave1.LeaveStatus });
            Connection.myparameters.Add(new myParameters { ParameterName = "@LASTDATEMODIFIED", mytype = SqlDbType.DateTime, Value = FileNewLeave1.LastDateMod });
            Connection.myparameters.Add(new myParameters { ParameterName = "@SUBMITTED", mytype = SqlDbType.NVarChar, Value = FileNewLeave1.Submitted });
            Connection.myparameters.Add(new myParameters { ParameterName = "@LEAVETYPE", mytype = SqlDbType.NVarChar, Value = FileNewLeave1.LeaveType });
            Connection.myparameters.Add(new myParameters { ParameterName = "@LOCATION", mytype = SqlDbType.NVarChar, Value = FileNewLeave1.Coordinates });
            Connection.myparameters.Add(new myParameters { ParameterName = "@GENDER", mytype = SqlDbType.NVarChar, Value = FileNewLeave1.Gender });
            Connection.myparameters.Add(new myParameters { ParameterName = "@RULES", mytype = SqlDbType.NVarChar, Value = FileNewLeave1.Rules });
            Connection.myparameters.Add(new myParameters { ParameterName = "@LEAVEID", mytype = SqlDbType.NVarChar, Value = FileNewLeave1.LeaveID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@CN", mytype = SqlDbType.NVarChar, Value = FileNewLeave1.CN });
            Connection.myparameters.Add(new myParameters { ParameterName = "@SHIFTTYPE", mytype = SqlDbType.NVarChar, Value = FileNewLeave1.ShiftType });
            Connection.myparameters.Add(new myParameters { ParameterName = "@EMPREASON", mytype = SqlDbType.NVarChar, Value = FileNewLeave1.EmpReason });
            Connection.myparameters.Add(new myParameters { ParameterName = "@EMPCOMMENT", mytype = SqlDbType.NVarChar, Value = FileNewLeave1.EmpComment });
            Connection.myparameters.Add(new myParameters { ParameterName = "@FILEPATH", mytype = SqlDbType.NVarChar, Value = FileNewLeave1.FilePath != "" ? formattedTime : FileNewLeave1.FilePath });
            Connection.myparameters.Add(new myParameters { ParameterName = "@OFFSETHOURS", mytype = SqlDbType.NVarChar, Value = FileNewLeave1.LeaveHours });
            Connection.myparameters.Add(new myParameters { ParameterName = "@LEAVESTARTDATE", mytype = SqlDbType.NVarChar, Value = FileNewLeave1.LeaveRequestStartDate });
            Connection.myparameters.Add(new myParameters { ParameterName = "@LEAVEENDDATE", mytype = SqlDbType.NVarChar, Value = FileNewLeave1.LeaveRequestEndDate });
            Connection.myparameters.Add(new myParameters { ParameterName = "@MULTIPLE", mytype = SqlDbType.NVarChar, Value = FileNewLeave1.multiple });

            String ID = Connection.ExecuteScalar("sp_InsertHRMSLeaveMasterOffset");
            byte[] PIC = Convert.FromBase64String(FileNewLeave1.FilePath);

            var path = HttpContext.Current.Server.MapPath("~/ProfilePhotos/" + FileNewLeave1.CN.ToLower() + "/" + "");
            var folder = Path.Combine(path, FileNewLeave1.NTID);
            Directory.CreateDirectory(folder);
            MemoryStream stream = new MemoryStream();
            using (stream)
            {

                File.WriteAllBytes(folder + "/" + FileNewLeave1.NTID + "_" + formattedTime + ".png", PIC);

            }
            emprequestleave emp = new emprequestleave();
            emp.empid = FileNewLeave1.NTID;
            emp.IsHalfDay = FileNewLeave1.isHalfDay;
            emp.IsPaid = FileNewLeave1.isPaid;
            emp.LeaveDate = FileNewLeave1.LeaveDate;
            emp.LeaveID = FileNewLeave1.LeaveID;
            emp.LeaveType = FileNewLeave1.LeaveType;
            emp.timefiled = DateTime.Now.ToString();
            emp.CN = FileNewLeave1.CN;
            SendEmailNotifLeaves(emp);

            return "Success";
        }

        [HttpPost]
        [Route("FileLeavevv9")]
        public string FileANewLeavev9(LeaveMngtv4 FileNewLeave1)
        {
			
			
            try
            {
                GetDatabase GDB = new GetDatabase();
                GDB.ClientName = FileNewLeave1.CN;
                SelfieRegistration2Controller.GetDB2(GDB);

                var time = DateTime.Now;
                string formattedTime = time.ToString("yyyy-MM-dd-hh-mm-ss-fff");


                Connection Connection = new Connection();
                Connection.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = FileNewLeave1.NTID });
                Connection.myparameters.Add(new myParameters { ParameterName = "@LEAVEDATE", mytype = SqlDbType.NVarChar, Value = FileNewLeave1.LeaveDate });
                Connection.myparameters.Add(new myParameters { ParameterName = "@ISHALFDAY", mytype = SqlDbType.NVarChar, Value = FileNewLeave1.isHalfDay });
                //Connection.myparameters.Add(new myParameters { ParameterName = "@LEAVETIMEBALANCE", mytype = SqlDbType.NVarChar, Value = FileNewLeave1.LeaveHours });
                Connection.myparameters.Add(new myParameters { ParameterName = "@ISPAID", mytype = SqlDbType.NVarChar, Value = FileNewLeave1.isPaid });
                //Connection.myparameters.Add(new myParameters { ParameterName = "@DATEAPPLIED", mytype = SqlDbType.NVarChar, Value = FileNewLeave1.DateApplied });
                Connection.myparameters.Add(new myParameters { ParameterName = "@LEAVESTATUS", mytype = SqlDbType.NVarChar, Value = FileNewLeave1.LeaveStatus });
                Connection.myparameters.Add(new myParameters { ParameterName = "@LASTDATEMODIFIED", mytype = SqlDbType.DateTime, Value = FileNewLeave1.LastDateMod });
                Connection.myparameters.Add(new myParameters { ParameterName = "@SUBMITTED", mytype = SqlDbType.NVarChar, Value = FileNewLeave1.Submitted });
                Connection.myparameters.Add(new myParameters { ParameterName = "@LEAVETYPE", mytype = SqlDbType.NVarChar, Value = FileNewLeave1.LeaveType });
                Connection.myparameters.Add(new myParameters { ParameterName = "@LOCATION", mytype = SqlDbType.NVarChar, Value = FileNewLeave1.Coordinates });
                Connection.myparameters.Add(new myParameters { ParameterName = "@GENDER", mytype = SqlDbType.NVarChar, Value = FileNewLeave1.Gender });
                Connection.myparameters.Add(new myParameters { ParameterName = "@RULES", mytype = SqlDbType.NVarChar, Value = FileNewLeave1.Rules });
                Connection.myparameters.Add(new myParameters { ParameterName = "@LEAVEID", mytype = SqlDbType.NVarChar, Value = FileNewLeave1.LeaveID });
                Connection.myparameters.Add(new myParameters { ParameterName = "@CN", mytype = SqlDbType.NVarChar, Value = FileNewLeave1.CN });
                Connection.myparameters.Add(new myParameters { ParameterName = "@SHIFTTYPE", mytype = SqlDbType.NVarChar, Value = FileNewLeave1.ShiftType });
                Connection.myparameters.Add(new myParameters { ParameterName = "@EMPREASON", mytype = SqlDbType.NVarChar, Value = FileNewLeave1.EmpReason });
                Connection.myparameters.Add(new myParameters { ParameterName = "@EMPCOMMENT", mytype = SqlDbType.NVarChar, Value = FileNewLeave1.EmpComment });
                Connection.myparameters.Add(new myParameters { ParameterName = "@FILEPATH", mytype = SqlDbType.NVarChar, Value = FileNewLeave1.FilePath != "" ? formattedTime : FileNewLeave1.FilePath });
                Connection.myparameters.Add(new myParameters { ParameterName = "@LEAVESTARTDATE", mytype = SqlDbType.NVarChar, Value = FileNewLeave1.LeaveRequestStartDate });
                Connection.myparameters.Add(new myParameters { ParameterName = "@LEAVEENDDATE", mytype = SqlDbType.NVarChar, Value = FileNewLeave1.LeaveRequestEndDate });
                Connection.myparameters.Add(new myParameters { ParameterName = "@MULTIPLE", mytype = SqlDbType.NVarChar, Value = FileNewLeave1.multiple });

                String ID = Connection.ExecuteScalar("sp_InsertHRMSLeaveMaster");
                byte[] PIC = Convert.FromBase64String(FileNewLeave1.FilePath);

                var path = HttpContext.Current.Server.MapPath("~/ProfilePhotos/" + FileNewLeave1.CN.ToLower() + "/" + "");
                var folder = Path.Combine(path, FileNewLeave1.NTID);
                Directory.CreateDirectory(folder);
                MemoryStream stream = new MemoryStream();
                using (stream)
                {

                    File.WriteAllBytes(folder + "/" + FileNewLeave1.NTID + "_" + formattedTime + ".png", PIC);

                }
                //if (FileNewLeave1.CN.ToLower().Contains("campaigntrack"))
                //{
                emprequestleave emp = new emprequestleave();
                emp.empid = FileNewLeave1.NTID;
                emp.IsHalfDay = FileNewLeave1.isHalfDay;
                emp.IsPaid = FileNewLeave1.isPaid;
                emp.LeaveDate = FileNewLeave1.LeaveDate;
                emp.LeaveID = FileNewLeave1.LeaveID;
                emp.LeaveType = FileNewLeave1.LeaveType;
                emp.CN = FileNewLeave1.CN;
                emp.timefiled = DateTime.Now.ToString();
                SendEmailNotifLeaves(emp);
                //}
                return "Success";
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }
        [HttpPost]
        [Route("FileNewFlightRequestMobile")]
        public string FileNewFlightRequestMobile(FlightRequest FileNewLeave)
        {
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = FileNewLeave.CN;
            SelfieRegistration2Controller.GetDB2(GDB);

            Connection Connection = new Connection();
            Connection.myparameters.Add(new myParameters { ParameterName = "@Empid", mytype = SqlDbType.NVarChar, Value = FileNewLeave.EMPID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@DepartureDate", mytype = SqlDbType.NVarChar, Value = FileNewLeave.DepartureDate });
            Connection.myparameters.Add(new myParameters { ParameterName = "@ReturnDate", mytype = SqlDbType.NVarChar, Value = FileNewLeave.ReturnDate });

            String ID = Connection.ExecuteScalar("sp_InsertFlightRequest_Schedule");

            return "success";
        }
        #region "New File Leave Mobile"
        [HttpPost]
        [Route("FileNewLeaveMobile")]
        public string FileNewLeaveMobile(LeaveMngtv2 FileNewLeave)
        {
          
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = FileNewLeave.CN;
            SelfieRegistration2Controller.GetDB2(GDB);

            var time = DateTime.Now;
            string formattedTime = time.ToString("yyyy-MM-dd-hh-mm-ss-fff");

            Connection Connection = new Connection();
            Connection.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = FileNewLeave.NTID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@LEAVEDATE", mytype = SqlDbType.NVarChar, Value = FileNewLeave.LeaveDate });
            Connection.myparameters.Add(new myParameters { ParameterName = "@LEAVETYPE", mytype = SqlDbType.NVarChar, Value = FileNewLeave.LeaveType });
            Connection.myparameters.Add(new myParameters { ParameterName = "@LEAVEID", mytype = SqlDbType.NVarChar, Value = FileNewLeave.LeaveID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@ISHALFDAY", mytype = SqlDbType.NVarChar, Value = FileNewLeave.isHalfDay });
            Connection.myparameters.Add(new myParameters { ParameterName = "@LOCATION", mytype = SqlDbType.NVarChar, Value = FileNewLeave.Coordinates });
            Connection.myparameters.Add(new myParameters { ParameterName = "@CN", mytype = SqlDbType.NVarChar, Value = FileNewLeave.CN });
            Connection.myparameters.Add(new myParameters { ParameterName = "@SHIFTTYPE", mytype = SqlDbType.NVarChar, Value = FileNewLeave.ShiftType });
            Connection.myparameters.Add(new myParameters { ParameterName = "@EMPREASON", mytype = SqlDbType.NVarChar, Value = FileNewLeave.EmpReason });
            Connection.myparameters.Add(new myParameters { ParameterName = "@EMPCOMMENT", mytype = SqlDbType.NVarChar, Value = FileNewLeave.EmpComment });
            Connection.myparameters.Add(new myParameters { ParameterName = "@FILEPATH", mytype = SqlDbType.NVarChar, Value = FileNewLeave.FilePath != "" ? formattedTime : "" });
            Connection.myparameters.Add(new myParameters { ParameterName = "@LEAVESTARTDATE", mytype = SqlDbType.NVarChar, Value = FileNewLeave.LeaveStartDate });
            Connection.myparameters.Add(new myParameters { ParameterName = "@LEAVEENDDATE", mytype = SqlDbType.NVarChar, Value = FileNewLeave.LeaveEndDate });
            Connection.myparameters.Add(new myParameters { ParameterName = "@MULTIPLE", mytype = SqlDbType.NVarChar, Value = FileNewLeave.Multiple });
            Connection.myparameters.Add(new myParameters { ParameterName = "@HOURS", mytype = SqlDbType.NVarChar, Value = FileNewLeave.HOURS });
            Connection.myparameters.Add(new myParameters { ParameterName = "@MINUTES", mytype = SqlDbType.NVarChar, Value = FileNewLeave.MINUTES });
            String ID = Connection.ExecuteScalar("sp_InsertNewFileLeave_Mobile");
            byte[] PIC = Convert.FromBase64String(FileNewLeave.FilePath);

            var path = HttpContext.Current.Server.MapPath("~/ProfilePhotos/" + FileNewLeave.CN.ToLower() + "/" + "");
            var folder = Path.Combine(path, FileNewLeave.NTID);
            Directory.CreateDirectory(folder);
            MemoryStream stream = new MemoryStream();
            using (stream)
            {

                File.WriteAllBytes(folder + "/" + FileNewLeave.NTID + "_" + formattedTime + ".png", PIC);

            }
            return "Success";

        }

        // FileNewLeaveMobile v2
        [HttpPost]
        [Route("InsertNewFileLeave_MobileV2")]
        public string FileNewLeaveMobile(File_new_leave_param FileNewLeave)
        {
            try
            {
                GetDatabase GDB = new GetDatabase();
                GDB.ClientName = FileNewLeave.CN;
                SelfieRegistration2Controller.GetDB2(GDB);

                var time = DateTime.Now;
                string formattedTime = time.ToString("yyyy-MM-dd-hh-mm-ss-fff");

                Connection Connection = new Connection();
                Connection.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = FileNewLeave.EMPID });
                Connection.myparameters.Add(new myParameters { ParameterName = "@LEAVEDATE", mytype = SqlDbType.NVarChar, Value = FileNewLeave.LEAVEDATE });
                Connection.myparameters.Add(new myParameters { ParameterName = "@LEAVETYPE", mytype = SqlDbType.NVarChar, Value = FileNewLeave.LEAVETYPE });
                Connection.myparameters.Add(new myParameters { ParameterName = "@LEAVEID", mytype = SqlDbType.NVarChar, Value = FileNewLeave.LEAVEID });
                Connection.myparameters.Add(new myParameters { ParameterName = "@ISHALFDAY", mytype = SqlDbType.NVarChar, Value = FileNewLeave.ISHALFDAY });
                Connection.myparameters.Add(new myParameters { ParameterName = "@LOCATION", mytype = SqlDbType.NVarChar, Value = FileNewLeave.LOCATION });
                Connection.myparameters.Add(new myParameters { ParameterName = "@CN", mytype = SqlDbType.NVarChar, Value = FileNewLeave.CN });
                Connection.myparameters.Add(new myParameters { ParameterName = "@SHIFTTYPE", mytype = SqlDbType.NVarChar, Value = FileNewLeave.SHIFTTYPE });
                Connection.myparameters.Add(new myParameters { ParameterName = "@EMPREASON", mytype = SqlDbType.NVarChar, Value = FileNewLeave.EMPREASON });
                Connection.myparameters.Add(new myParameters { ParameterName = "@EMPCOMMENT", mytype = SqlDbType.NVarChar, Value = FileNewLeave.EMPCOMMENT });
                Connection.myparameters.Add(new myParameters { ParameterName = "@FILEPATH", mytype = SqlDbType.NVarChar, Value = FileNewLeave.FILEPATH != "" ? formattedTime : FileNewLeave.FILEPATH });
                Connection.myparameters.Add(new myParameters { ParameterName = "@LEAVESTARTDATE", mytype = SqlDbType.NVarChar, Value = FileNewLeave.LEAVESTARTDATE });
                Connection.myparameters.Add(new myParameters { ParameterName = "@LEAVEENDDATE", mytype = SqlDbType.NVarChar, Value = FileNewLeave.LEAVEENDDATE });
                Connection.myparameters.Add(new myParameters { ParameterName = "@MULTIPLE", mytype = SqlDbType.NVarChar, Value = FileNewLeave.MULTIPLE });
                Connection.myparameters.Add(new myParameters { ParameterName = "@leave_id_slot", mytype = SqlDbType.NVarChar, Value = FileNewLeave.leave_id_slot });
                String ID = Connection.ExecuteScalar("sp_InsertNewFileLeave_MobileV2");
                byte[] PIC = Convert.FromBase64String(FileNewLeave.FILEPATH);

                var path = HttpContext.Current.Server.MapPath("~/ProfilePhotos/" + FileNewLeave.CN.ToLower() + "/" + "");
                var folder = Path.Combine(path, FileNewLeave.EMPID);
                Directory.CreateDirectory(folder);
                MemoryStream stream = new MemoryStream();
                using (stream)
                {

                    File.WriteAllBytes(folder + "/" + FileNewLeave.EMPID + "_" + formattedTime + ".png", PIC);

                }
                return "Success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }

        #endregion
        public void SendEmailNotifLeaves(emprequestleave emp)
        {
            //if (emp.CN.ToLower().Contains("campaigntrack"))
            //{
            #region For Leave Request Email sending
            //Connection Conn = new Connection();
            //Conn.myparameters.Add(new myParameters { ParameterName = "@ReasonID", mytype = SqlDbType.NVarChar, Value = Convert.ToInt32(emp.lea) });
            //string reasondesc = Conn.ExecuteScalar("sp_GetReasonDesc");
            GetDatabase GDB = new GetDatabase();
            //GDB.ClientName = "campaigntrack";
            GDB.ClientName = emp.CN;
            SelfieRegistration2Controller.GetDB2(GDB);

            DataTable EmpDT = new DataTable();
            Connection ECon = new Connection();
            ECon.myparameters.Add(new myParameters { ParameterName = "@empid", mytype = SqlDbType.NVarChar, Value = emp.empid });
            ECon.myparameters.Add(new myParameters { ParameterName = "@datefiled", mytype = SqlDbType.NVarChar, Value = Convert.ToDateTime(emp.timefiled).ToString("yyyy-MM-dd") });
            ECon.myparameters.Add(new myParameters { ParameterName = "@leavetypeid", mytype = SqlDbType.NVarChar, Value = emp.LeaveID });
            EmpDT = ECon.GetDataTable("sp_GetLogsApproverLeave");

            string body = string.Empty;
            //using streamreader for reading my htmltemplate   

            using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/sFTP/PendingLogsEmail.html")))

            {

                body = reader.ReadToEnd();

            }


            foreach (DataRow dr in EmpDT.Rows)
            {
                string emailss = dr["Email"].ToString(); // "ilm.customerfirst@gmail.com";
                string approvername = dr["Last_Name"].ToString() + ", " + dr["First_Name"].ToString() + " " + dr["Middle_Name"].ToString();
                body = body.Replace("[ApproverName]", approvername); //replacing the required things  
                body = body.Replace("[EmployeeName]", dr["empname"].ToString());
                body = body.Replace("[Request]", "Leave");
                if(emp.CN == "GlobalPay")
                {
                    body = body.Replace("clientlink", dr["clientlink"].ToString());
                }
                string leavedates = "";
                for (DateTime date = Convert.ToDateTime(emp.LeaveRequestStartDate); date <= Convert.ToDateTime(emp.LeaveRequestEndDate); date = date.AddDays(1))
                    leavedates = leavedates + date.ToString("MMM dd, yyyy") + "<br />";
                if (String.IsNullOrEmpty(emp.LeaveRequestStartDate))
                    leavedates = emp.LeaveDate;
                body = body.Replace("[Message]", leavedates + "<br /> " + emp.LeaveType + "<br>" + "Duration: " + emp.LeaveHours + " and " + emp.LeaveMinutes + "<br />" + "Comment: " + emp.LeaveComment);
                if (string.IsNullOrWhiteSpace(emailss) == false)
                {
                    var message = new MimeKit.MimeMessage();

                    //MimeKit.InternetAddressList Elist = new MimeKit.InternetAddressList();
                    //foreach (string emailadd in emaillist)
                    //{
                    //    Elist.Add(new MimeKit.MailboxAddress(emailadd));
                    //}

                    message.From.Add(new MimeKit.MailboxAddress("durusthr@illimitado.com"));
                    //message.To.AddRange(Elist);
                    message.To.Add(new MimeKit.MailboxAddress(emailss));
                    message.Subject = "Leave Request Notification";
                    message.Body = new MimeKit.TextPart("html") { Text = body };

                    using (var client = new MailKit.Net.Smtp.SmtpClient())
                    {
                        client.ServerCertificateValidationCallback = (s, c, h, e) => true;
                        client.Connect("smtp.office365.com", 587, false);
                        client.Authenticate("durusthr@illimitado.com", "@1230Qwerty");
                        client.Send(message);
                        client.Disconnect(true);
                    }
                }
            }


            #endregion
            //}
            //else if (emp.CN.ToLower().Contains("ilmdev")) {

            //    #region For Leave Request Email sending
            //    //Connection Conn = new Connection();
            //    //Conn.myparameters.Add(new myParameters { ParameterName = "@ReasonID", mytype = SqlDbType.NVarChar, Value = Convert.ToInt32(emp.lea) });
            //    //string reasondesc = Conn.ExecuteScalar("sp_GetReasonDesc");
            //    GetDatabase GDB = new GetDatabase();
            //    GDB.ClientName = "ilmdev";
            //    SelfieRegistration2Controller.GetDB2(GDB);

            //    DataTable EmpDT = new DataTable();
            //    Connection ECon = new Connection();
            //    ECon.myparameters.Add(new myParameters { ParameterName = "@empid", mytype = SqlDbType.NVarChar, Value = emp.empid });
            //    ECon.myparameters.Add(new myParameters { ParameterName = "@datefiled", mytype = SqlDbType.NVarChar, Value = Convert.ToDateTime(emp.timefiled).ToString("yyyy-MM-dd") });
            //    ECon.myparameters.Add(new myParameters { ParameterName = "@leavetypeid", mytype = SqlDbType.NVarChar, Value = emp.LeaveID });
            //    EmpDT = ECon.GetDataTable("sp_GetLogsApproverLeave");

            //    string body = string.Empty;
            //    //using streamreader for reading my htmltemplate   

            //    using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/sFTP/PendingLogsEmail.html")))

            //    {

            //        body = reader.ReadToEnd();

            //    }


            //    foreach (DataRow dr in EmpDT.Rows)
            //    {
            //        string emailss = dr["Email"].ToString(); // "ilm.customerfirst@gmail.com";
            //        string approvername = dr["Last_Name"].ToString() + ", " + dr["First_Name"].ToString() + " " + dr["Middle_Name"].ToString();
            //        body = body.Replace("[ApproverName]", approvername); //replacing the required things  
            //        body = body.Replace("[EmployeeName]", dr["empname"].ToString());
            //        body = body.Replace("[Request]", "Leave");
            //        string leavedates = "";
            //        for (DateTime date = Convert.ToDateTime(emp.LeaveRequestStartDate); date <= Convert.ToDateTime(emp.LeaveRequestEndDate); date = date.AddDays(1))
            //            leavedates = leavedates + date.ToString("MMM dd, yyyy") + "<br />";
            //        if (String.IsNullOrEmpty(emp.LeaveRequestStartDate))
            //            leavedates = emp.LeaveDate;
            //        body = body.Replace("[Message]", leavedates + "<br /> " + emp.LeaveType + "<br>" + "Duration: " + emp.LeaveHours + " and " + emp.LeaveMinutes + "<br />" + "Comment: " + emp.LeaveComment);
            //        if (string.IsNullOrWhiteSpace(emailss) == false)
            //        {
            //            var message = new MimeKit.MimeMessage();

            //            //MimeKit.InternetAddressList Elist = new MimeKit.InternetAddressList();
            //            //foreach (string emailadd in emaillist)
            //            //{
            //            //    Elist.Add(new MimeKit.MailboxAddress(emailadd));
            //            //}

            //            message.From.Add(new MimeKit.MailboxAddress("durusthr@illimitado.com"));
            //            //message.To.AddRange(Elist);
            //            message.To.Add(new MimeKit.MailboxAddress(emailss));
            //            message.Subject = "Leave Request Notification";
            //            message.Body = new MimeKit.TextPart("html") { Text = body };

            //            using (var client = new MailKit.Net.Smtp.SmtpClient())
            //            {GetDHRCompanyBenefitsLeavev1
            //                client.ServerCertificateValidationCallback = (s, c, h, e) => true;
            //                client.Connect("smtp.office365.com", 587, false);
            //                client.Authenticate("durusthr@illimitado.com", "@1230Qwerty");
            //                client.Send(message);
            //                client.Disconnect(true);
            //            }
            //        }
            //    }

            //}
        }

        [HttpPost]
        [Route("SendEmailNotifLogstoUser")]
        public void SendEmailNotifLogstoUser(emprequestleave emp)
        {
            if (emp.CN.ToLower().Contains("campaigntrack"))
            {
                GetDatabase GDB = new GetDatabase();
                GDB.ClientName = "campaigntrack";
                SelfieRegistration2Controller.GetDB2(GDB);

                string clientlink = GDB.ClientLink;
                DataTable EmpDT = new DataTable();
                Connection ECon = new Connection();
                ECon.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = emp.empid });
                EmpDT = ECon.GetDataTable("sp_GetPersonalEmail");

                string body = string.Empty;
                //using streamreader for reading my htmltemplate   

                using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/sFTP/PendingLogsEmailtoUser.html")))

                {

                    body = reader.ReadToEnd();

                }


                foreach (DataRow dr in EmpDT.Rows)
                {
                    string emailss = dr["Email"].ToString(); //"rb.griarte1@gmail.com"; // 
                    body = body.Replace("[EmployeeName]", dr["empname"].ToString());
                    body = body.Replace("[Message]", emp.LeaveDate + "<br />" + emp.LeaveType + " - Duration: " + emp.LeaveHours + " and " + emp.LeaveMinutes + "<br /> Comment: " + emp.LeaveComment);
                    body = body.Replace("clientlink", clientlink);
                    if (string.IsNullOrWhiteSpace(emailss) == false)
                    {
                        var message = new MimeKit.MimeMessage();

                        //MimeKit.InternetAddressList Elist = new MimeKit.InternetAddressList();
                        //foreach (string emailadd in emaillist)
                        //{
                        //    Elist.Add(new MimeKit.MailboxAddress(emailadd));
                        //}

                        message.From.Add(new MimeKit.MailboxAddress("durusthr@illimitado.com"));
                        //message.To.AddRange(Elist);
                        message.To.Add(new MimeKit.MailboxAddress(emailss));
                        message.Subject = "Leave Request Notification";
                        message.Body = new MimeKit.TextPart("html") { Text = body };

                        using (var client = new MailKit.Net.Smtp.SmtpClient())
                        {
                            client.ServerCertificateValidationCallback = (s, c, h, e) => true;
                            client.Connect("smtp.office365.com", 587, false);
                            client.Authenticate("durusthr@illimitado.com", "@1230Qwerty");
                            client.Send(message);
                            client.Disconnect(true);
                        }
                    }
                }

            }


        }

        [HttpPost]
        [Route("sendEmailNotif")]
        public void SendEmailNotification(emailnotif emp)
        {
            #region For Email sending

            string body = string.Empty;
            //using streamreader for reading my htmltemplate   
            //using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/sFTP/PendingLogsEmail.html")))
            //{
            //    body = reader.ReadToEnd();
            //}


            //string emailss = "rvitug@illimitado.com";
            body = emp.htmlMessage;
            if (emp.toRecipients.Length > 0)
            {
                var message = new MimeKit.MimeMessage();
                //MimeKit.InternetAddressList Elist = new MimeKit.InternetAddressList();
                //foreach (string emailadd in emaillist)
                //{
                //    Elist.Add(new MimeKit.MailboxAddress(emailadd));
                //}
                message.From.Add(new MimeKit.MailboxAddress("durusthr@illimitado.com"));
                foreach (string toemail in emp.toRecipients)
                    message.To.Add(new MimeKit.MailboxAddress(toemail));
                foreach (string ccemail in emp.ccRecipients)
                    message.To.Add(new MimeKit.MailboxAddress(ccemail));
                foreach (string bccemail in emp.bccRecipients)
                    message.To.Add(new MimeKit.MailboxAddress(bccemail));
                //message.To.AddRange(Elist);
                message.Subject = emp.subjectline;
                message.Body = new MimeKit.TextPart("html") { Text = body };
                using (var client = new MailKit.Net.Smtp.SmtpClient())
                {
                    client.ServerCertificateValidationCallback = (s, c, h, e) => true;
                    client.Connect("smtp.office365.com", 587, false);
                    client.Authenticate("durusthr@illimitado.com", "@1230Qwerty");
                    client.Send(message);
                    client.Disconnect(true);
                }
            }


            #endregion
        }
        // 2020-01-24 11:42 AM
        #region 2020-01-24 11:42 AM
		[HttpPost]
        [Route("SendEmailNotifApproveLeave")]
		public void SendEmailNotifApproveLeaves(emprequestleave emp)
        {

            #region For Leave Request Email sending

            GetDatabase GDB = new GetDatabase();
            //GDB.ClientName = "campaigntrack";
            GDB.ClientName = emp.CN;
            SelfieRegistration2Controller.GetDB2(GDB);

            DataTable EmpDT = new DataTable();
            Connection ECon = new Connection();
            ECon.myparameters.Add(new myParameters { ParameterName = "@empid", mytype = SqlDbType.NVarChar, Value = emp.empid });
            ECon.myparameters.Add(new myParameters { ParameterName = "@datefiled", mytype = SqlDbType.NVarChar, Value = Convert.ToDateTime(emp.timefiled).ToString("yyyy-MM-dd") });
            ECon.myparameters.Add(new myParameters { ParameterName = "@leavetypeid", mytype = SqlDbType.NVarChar, Value = emp.LeaveID });
            EmpDT = ECon.GetDataTable("sp_GetLogsApproverLeave");

            string body = string.Empty;
            //using streamreader for reading my htmltemplate   

            using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/sFTP/PendingLogsEmail.html")))

            {

                body = reader.ReadToEnd();

            }


            foreach (DataRow dr in EmpDT.Rows)
            {
                string emailss = dr["Email"].ToString(); // "ilm.customerfirst@gmail.com";
                string approvername = dr["Last_Name"].ToString() + ", " + dr["First_Name"].ToString() + " " + dr["Middle_Name"].ToString();
                body = body.Replace("[ApproverName]", approvername); //replacing the required things  
                body = body.Replace("[EmployeeName]", dr["empname"].ToString());
                body = body.Replace("[Request]", "Leave");
                string leavedates = "";
                for (DateTime date = Convert.ToDateTime(emp.LeaveRequestStartDate); date <= Convert.ToDateTime(emp.LeaveRequestEndDate); date = date.AddDays(1))
                    leavedates = leavedates + date.ToString("MMM dd, yyyy") + "<br />";
                if (String.IsNullOrEmpty(emp.LeaveRequestStartDate))
                    leavedates = emp.LeaveDate;
                body = body.Replace("[Message]", leavedates + "<br /> " + emp.LeaveType + "<br>" + "Duration: " + emp.LeaveHours + " and " + emp.LeaveMinutes + "<br />" + "Comment: " + emp.LeaveComment);
                if (string.IsNullOrWhiteSpace(emailss) == false)
                {
                    var message = new MimeKit.MimeMessage();

                    message.From.Add(new MimeKit.MailboxAddress("durusthr@illimitado.com"));
                    //message.To.AddRange(Elist);
                    message.To.Add(new MimeKit.MailboxAddress(emailss));
                    message.Subject = "Leave Request Notification";
                    message.Body = new MimeKit.TextPart("html") { Text = body };

                    using (var client = new MailKit.Net.Smtp.SmtpClient())
                    {
                        client.ServerCertificateValidationCallback = (s, c, h, e) => true;
                        client.Connect("smtp.office365.com", 587, false);
                        client.Authenticate("durusthr@illimitado.com", "@1230Qwerty");
                        client.Send(message);
                        client.Disconnect(true);
                    }
                }
            }


            #endregion
          
        }
        //public void SendEmailEscalatedNotifLeaves(emprequestleave emp)
        //{
        //    if (emp.CN.ToLower().Contains("campaigntrack"))
        //    {
        //        #region For Leave Request Email sending
        //        //Connection Conn = new Connection();
        //        //Conn.myparameters.Add(new myParameters { ParameterName = "@ReasonID", mytype = SqlDbType.NVarChar, Value = Convert.ToInt32(emp.lea) });
        //        //string reasondesc = Conn.ExecuteScalar("sp_GetReasonDesc");


        //        //DataTable EmpDT = new DataTable();
        //        //Connection ECon = new Connection();
        //        //ECon.myparameters.Add(new myParameters { ParameterName = "@empid", mytype = SqlDbType.NVarChar, Value = emp.empid });
        //        //ECon.myparameters.Add(new myParameters { ParameterName = "@datefiled", mytype = SqlDbType.NVarChar, Value = Convert.ToDateTime(emp.timefiled).ToString("yyyy-MM-dd") });
        //        //ECon.myparameters.Add(new myParameters { ParameterName = "@leavetypeid", mytype = SqlDbType.NVarChar, Value = emp.LeaveID });
        //        //EmpDT = ECon.GetDataTable("sp_GetLogsApproverLeave");

        //        string body = string.Empty;
        //        //using streamreader for reading my htmltemplate   

        //        using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/sFTP/PendingLogsEmail.html")))

        //        {

        //            body = reader.ReadToEnd();

        //        }


        //        //foreach (DataRow dr in EmpDT.Rows)
        //        //{
        //        string dr = "";
        //        string emailss = "ilm.customerfirst@gmail.com";// dr["Email"].ToString();
        //        string approvername = dr["Last_Name"].ToString() + ", " + dr["First_Name"].ToString() + " " + dr["Middle_Name"].ToString();
        //        body = body.Replace("[ApproverName]", approvername); //replacing the required things  
        //        body = body.Replace("[EmployeeName]", dr["empname"].ToString());
        //        body = body.Replace("[Request]", "Leave");
        //        string leavedates = "";
        //        for (DateTime date = Convert.ToDateTime(emp.LeaveRequestStartDate); date <= Convert.ToDateTime(emp.LeaveRequestEndDate); date = date.AddDays(1))
        //            leavedates = leavedates + date.ToString("MMM dd, yyyy") + "<br />";
        //        if (String.IsNullOrEmpty(emp.LeaveRequestStartDate))
        //            leavedates = emp.LeaveDate;
        //        body = body.Replace("[Message]", leavedates + "<br /> " + emp.LeaveType + "<br>" + "Duration: " + emp.LeaveHours + " and " + emp.LeaveMinutes + "<br />" + "Comment: " + emp.LeaveComment);
        //        if (string.IsNullOrWhiteSpace(emailss) == false)
        //        {
        //            var message = new MimeKit.MimeMessage();

        //            //MimeKit.InternetAddressList Elist = new MimeKit.InternetAddressList();
        //            //foreach (string emailadd in emaillist)
        //            //{
        //            //    Elist.Add(new MimeKit.MailboxAddress(emailadd));
        //            //}

        //            message.From.Add(new MimeKit.MailboxAddress("durusthr@illimitado.com"));
        //            //message.To.AddRange(Elist);
        //            message.To.Add(new MimeKit.MailboxAddress(emailss));
        //            message.Subject = "Leave Request Notification";
        //            message.Body = new MimeKit.TextPart("html") { Text = body };

        //            using (var client = new MailKit.Net.Smtp.SmtpClient())
        //            {
        //                client.ServerCertificateValidationCallback = (s, c, h, e) => true;
        //                client.Connect("smtp.office365.com", 587, false);
        //                client.Authenticate("durusthr@illimitado.com", "@1230Qwerty");
        //                client.Send(message);
        //                client.Disconnect(true);
        //            }
        //        }
        //        //}
        //        #endregion
        //    }
        //}
        #endregion


    }
}