﻿using System.IO;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace illimitadoWepAPI.Controllers
{
    public class IDRegistrationController : ApiController
    {
        [HttpPost]
        [Route("UploadIDs")]
        public async Task<HttpResponseMessage> UploadIDs()
        {
            HttpRequest httpRequest = HttpContext.Current.Request;

            BinaryReader binaryReader = new BinaryReader(httpRequest.Files[0].InputStream);

            string NTID = httpRequest.Form["NTID"];
            byte[] Photo = binaryReader.ReadBytes(httpRequest.Files[0].ContentLength);
            string ID_Set = httpRequest.Form["ID_Set"];

            HttpResponseMessage msg;

            var path = HttpContext.Current.Server.MapPath("~/ProfilePhotos/");
            //var path = "http://uat-vm.southeastasia.cloudapp.azure.com:81/hrms_test/ProfilePhotos";
            var folder = Path.Combine(path, NTID);
            Directory.CreateDirectory(folder);

            try
            {
                File.WriteAllBytes(folder + "/" + NTID + "_" + ID_Set + ".png", Photo);
                msg = Request.CreateResponse(HttpStatusCode.Accepted);
            }

            catch
            {
                msg = Request.CreateResponse(HttpStatusCode.NotAcceptable);
            }

            return msg;
        }

        [HttpPost]
        [Route("UploadOR")]
        public async Task<HttpResponseMessage> UploadOR()
        {
            HttpRequest httpRequest = HttpContext.Current.Request;

            BinaryReader binaryReader = new BinaryReader(httpRequest.Files[0].InputStream);

            string NTID = httpRequest.Form["NTID"];
            byte[] Photo = binaryReader.ReadBytes(httpRequest.Files[0].ContentLength);

            HttpResponseMessage msg;

            var path = HttpContext.Current.Server.MapPath("~/ProfilePhotos/");
            //var path = "http://uat-vm.southeastasia.cloudapp.azure.com:81/hrms_test/ProfilePhotos";
            var folder = Path.Combine(path, NTID);
            Directory.CreateDirectory(folder);

            try
            {
                File.WriteAllBytes(folder + "/" + NTID + "_OR.png", Photo);
                msg = Request.CreateResponse(HttpStatusCode.Accepted);
            }

            catch
            {
                msg = Request.CreateResponse(HttpStatusCode.NotAcceptable);
            }

            return msg;
        }

        [HttpPost]
        [Route("UploadSignatures")]
        public async Task<HttpResponseMessage> UploadSignatures()
        {
            HttpRequest httpRequest = HttpContext.Current.Request;



            BinaryReader binaryReader0 = new BinaryReader(httpRequest.Files[0].InputStream);
            BinaryReader binaryReader1 = new BinaryReader(httpRequest.Files[1].InputStream);
            BinaryReader binaryReader2 = new BinaryReader(httpRequest.Files[2].InputStream);

            string NTID = httpRequest.Form["NTID"];
            byte[] sign_0 = binaryReader0.ReadBytes(httpRequest.Files[0].ContentLength);
            byte[] sign_1 = binaryReader1.ReadBytes(httpRequest.Files[1].ContentLength);
            byte[] sign_2 = binaryReader2.ReadBytes(httpRequest.Files[2].ContentLength);

            HttpResponseMessage msg;

            var path = HttpContext.Current.Server.MapPath("~/ProfilePhotos/");
            //var path = "http://uat-vm.southeastasia.cloudapp.azure.com:81/hrms_test/ProfilePhotos";
            var folder = Path.Combine(path, NTID);
            Directory.CreateDirectory(folder);
            try
            {
                File.WriteAllBytes(folder + "/" + NTID + "_sign_0.png", sign_0);
                File.WriteAllBytes(folder + "/" + NTID + "_sign_1.png", sign_1);
                File.WriteAllBytes(folder + "/" + NTID + "_sign_2.png", sign_2);

                msg = Request.CreateResponse(HttpStatusCode.Accepted);
            }
            catch
            {
                msg = Request.CreateResponse(HttpStatusCode.NotAcceptable);
            }

            return msg;
        }

        [HttpPost]
        [Route("UploadSignaturesv2")]
        public async Task<HttpResponseMessage> UploadSignaturesv2()
        {
            HttpRequest httpRequest = HttpContext.Current.Request;



            BinaryReader binaryReader0 = new BinaryReader(httpRequest.Files[0].InputStream);
            BinaryReader binaryReader1 = new BinaryReader(httpRequest.Files[1].InputStream);
            BinaryReader binaryReader2 = new BinaryReader(httpRequest.Files[2].InputStream);

            string NTID = httpRequest.Form["NTID"];
            string COMPANYNAME = httpRequest.Form["COMPANYNAME"];
            byte[] sign_0 = binaryReader0.ReadBytes(httpRequest.Files[0].ContentLength);
            byte[] sign_1 = binaryReader1.ReadBytes(httpRequest.Files[1].ContentLength);
            byte[] sign_2 = binaryReader2.ReadBytes(httpRequest.Files[2].ContentLength);

            HttpResponseMessage msg;

            var path = HttpContext.Current.Server.MapPath("~/ProfilePhotos/");
            //var path = "http://uat-vm.southeastasia.cloudapp.azure.com:81/hrms_test/ProfilePhotos";
            var folder = Path.Combine(path, COMPANYNAME.ToLower());
            var savePath = HttpContext.Current.Server.MapPath("~/ProfilePhotos/" + COMPANYNAME.ToLower() + "/");
            var saveFolder = Path.Combine(savePath, NTID);

            Directory.CreateDirectory(folder);

            Directory.CreateDirectory(saveFolder);

            try
            {
                File.WriteAllBytes(saveFolder + "/" + NTID + "_sign_0.png", sign_0);
                File.WriteAllBytes(saveFolder + "/" + NTID + "_sign_1.png", sign_1);
                File.WriteAllBytes(saveFolder + "/" + NTID + "_sign_2.png", sign_2);

                msg = Request.CreateResponse(HttpStatusCode.Accepted);
            }
            catch
            {
                msg = Request.CreateResponse(HttpStatusCode.NotAcceptable);
            }

            return msg;
        }

        [HttpPost]
        [Route("UploadIDsv2")]
        public async Task<HttpResponseMessage> UploadIDsv2()
        {
            HttpRequest httpRequest = HttpContext.Current.Request;

            BinaryReader binaryReader = new BinaryReader(httpRequest.Files[0].InputStream);

            string NTID = httpRequest.Form["NTID"];
            string COMPANYNAME = httpRequest.Form["COMPANYNAME"];
            byte[] Photo = binaryReader.ReadBytes(httpRequest.Files[0].ContentLength);
            string ID_Set = httpRequest.Form["ID_Set"];

            HttpResponseMessage msg;

            var path = HttpContext.Current.Server.MapPath("~/ProfilePhotos/");
            //var path = "http://uat-vm.southeastasia.cloudapp.azure.com:81/hrms_test/ProfilePhotos";
            var folder = Path.Combine(path, COMPANYNAME.ToLower());
            var savePath = HttpContext.Current.Server.MapPath("~/ProfilePhotos/" + COMPANYNAME.ToLower() + "/");
            var saveFolder = Path.Combine(savePath, NTID);
            Directory.CreateDirectory(folder);

            Directory.CreateDirectory(saveFolder);

            try
            {
                File.WriteAllBytes(saveFolder + "/" + NTID + "_" + ID_Set + ".png", Photo);
                msg = Request.CreateResponse(HttpStatusCode.Accepted);
            }

            catch
            {
                msg = Request.CreateResponse(HttpStatusCode.NotAcceptable);
            }

            return msg;
        }
    }
}