﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using illimitadoWepAPI.Models;
using System.Xml.Serialization;
using System.IO;
using System.Data;
using System.Data.SqlClient;
using illimitadoWepAPI.MyClass;
using static illimitadoWepAPI.Models.UserProfile;
using System.Net.Http.Headers;
using System.Web.Hosting;
using Excel = Microsoft.Office.Interop.Excel;
using Microsoft.Office.Interop.Excel;
using ExpertXls.ExcelLib;
using System.Threading.Tasks;
using System.Web;

namespace illimitadoWepAPI.Controllers
{
    public class ProfileController : ApiController
    {
        // GET api/<controller>
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/<controller>/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<controller>
        public void Post([FromBody]string value)
        {
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }


//        {
//"NTID": "i143",
//"CARDNAME": "123456",
//"BIRTHDAY": "123456",
//"PLACEOFBIRTH": "123456",
//"NATIONALITY": "123456",
//"NATCOUNTRY": "123456",
//"MOBILE": "123456",
//"PERMZIPCODE": "123456",
//"PERMHOUSENUMBER": "123456",
//"PERMBRGY": "123456",
//"PERMUNI": "123456",
//"PERMCITY": "123456",
//"PERMREGION": "123456",
//"PERMCOUNTRY": "123456",
//"PRESZIPCODE": "123456",
//"PRESHOUSENUMBER": "123456",
//"PRESHOUSEADDRESS": "123456",
//"PRESBRGY": "123456",
//"PRESMUNI": "123456",
//"PRESCITY": "123456",
//"PRESCOUNTRY": "123456",
//"EMAILADD": "123456",
//"EMPLOYERNAME": "123456",
//"TIN": "123456",
//"CIVILSTATUS": "123456",
//"MOTHSUFFIX": "123456",
//"MOTHFIRSTNAME": "123456",
//"MOTHLASTNAME": "123456",
//"OTHERDETAILS": "123456"
//}






        [HttpPost]
        [Route("InsertUserProfile")]
        public string UserProfile(UserProfile uProfile)
        {


            string dataDir = HostingEnvironment.MapPath("~/Files/");
            FileStream stream = new FileStream(dataDir + "Basic.xls", FileMode.Open);
            //ExcelWriter writer = new ExcelWriter(stream);


            string filename;
            filename = uProfile.NTID + "_" + DateTime.Now.Month.ToString().PadLeft(2, '0') + DateTime.Now.Day.ToString().PadLeft(2, '0') + DateTime.Now.Year.ToString().Substring(2, 2) + "_CRT-UPDATEOCT2016.xls";
            using (var fileStream = new FileStream(dataDir + filename, FileMode.Create, FileAccess.Write))
            {
                stream.CopyTo(fileStream);
            }
            stream.Close();

            ExcelWorkbookFormat workbookFormat = ExcelWorkbookFormat.Xls_2003;
            string testDocFile = dataDir + "Basic.xls";
            ExcelWorkbook workbook = new ExcelWorkbook(testDocFile);
            ExcelWorksheet firstWorksheet = workbook.Worksheets[1];
            firstWorksheet["B11"].Text = uProfile.LASTNAME;
            firstWorksheet["C11"].Text = uProfile.FIRSTNAME;
            firstWorksheet["D11"].Text = uProfile.MIDDLENAME;
            firstWorksheet["E11"].Text = uProfile.SUFFIX;
            firstWorksheet["F11"].Text = uProfile.CARDNAME;
            firstWorksheet["G11"].Text = Convert.ToDateTime(uProfile.BIRTHDAY).ToString("MM-dd-yyyy");
            firstWorksheet["H11"].Text = uProfile.PLACEOFBIRTH;
            firstWorksheet["I11"].Text = uProfile.NATIONALITY;
            firstWorksheet["J11"].Text = uProfile.NATCOUNTRY;
            firstWorksheet["K11"].Text = uProfile.GENDER;
            firstWorksheet["L11"].Text = uProfile.MOBILE;
            firstWorksheet["M11"].Text = uProfile.TELPHONE;
            firstWorksheet["N11"].Text = uProfile.PRESHOUSENUMBER;
            firstWorksheet["O11"].Text = uProfile.PRESHOUSEADDRESS;
            firstWorksheet["P11"].Text = uProfile.PRESCOUNTRY;
            firstWorksheet["Q11"].Text = uProfile.PRESREGION;
            firstWorksheet["R11"].Text = uProfile.PRESPROVINCE;
            firstWorksheet["S11"].Text = uProfile.PRESCITY;
            firstWorksheet["T11"].Text = uProfile.PRESZIPCODE;
            firstWorksheet["U11"].Text = uProfile.PERMHOUSENUMBER;
            firstWorksheet["V11"].Text = uProfile.PERMHOUSEADDRESS;
            firstWorksheet["W11"].Text = uProfile.PERMCOUNTRY;
            firstWorksheet["X11"].Text = uProfile.PERMREGION;
            firstWorksheet["Y11"].Text = uProfile.PERMPROVINCE;
            firstWorksheet["Z11"].Text = uProfile.PERMCITY;
            firstWorksheet["AA11"].Text = uProfile.PERMZIPCODE;
            firstWorksheet["AB11"].Text = uProfile.EMAILADD;
            firstWorksheet["AC11"].Text = uProfile.EMPLOYERNAME;
            firstWorksheet["AD11"].Text = uProfile.TIN;
            firstWorksheet["AE11"].Text = uProfile.CIVILSTATUS;
            firstWorksheet["AF11"].Text = uProfile.MOTHLASTNAME;
            firstWorksheet["AG11"].Text = uProfile.MOTHFIRSTNAME;
            firstWorksheet["AH11"].Text = uProfile.MOTHMIDDLENAME;
            firstWorksheet["AI11"].Text = uProfile.MOTHSUFFIX;
            firstWorksheet["AJ11"].Text = uProfile.NUMBEROFPADS;
            firstWorksheet["AK11"].Text = uProfile.OTHERDETAILS;


            Connection con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = uProfile.NTID });
            System.Data.DataTable dt = con.GetDataTable("sp_getEmpCompany");
            if (dt.Rows.Count > 0)
            {
                firstWorksheet["C3"].Text = dt.Rows[0][1].ToString();
                firstWorksheet["C6"].Text = dt.Rows[0][2].ToString();
                firstWorksheet["C7"].Text = dt.Rows[0][3].ToString();
            }

            System.Web.HttpResponse httpResponse = System.Web.HttpContext.Current.Response;

            httpResponse.Clear();
            httpResponse.ContentType = "Application/x-msexcel";
            httpResponse.AddHeader("Content-Disposition", String.Format("attachment; filename={0}", filename));
            string newDIR = HostingEnvironment.MapPath("~/sFTP/");
            workbook.Save(newDIR + filename);
            Connection Connection = new Connection();



            Connection.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = uProfile.NTID });
            //Connection.myparameters.Add(new myParameters { ParameterName = "@LastName", mytype = SqlDbType.NVarChar, Value = uProfile.LASTNAME });
            //Connection.myparameters.Add(new myParameters { ParameterName = "@FirstName", mytype = SqlDbType.NVarChar, Value = uProfile.FIRSTNAME });
            //Connection.myparameters.Add(new myParameters { ParameterName = "@MiddleName", mytype = SqlDbType.NVarChar, Value = uProfile.MIDDLENAME });
            //Connection.myparameters.Add(new myParameters { ParameterName = "@Suffix", mytype = SqlDbType.NVarChar, Value = uProfile.SUFFIX });
            Connection.myparameters.Add(new myParameters { ParameterName = "@CardNum", mytype = SqlDbType.NVarChar, Value = uProfile.CARDNAME });
            Connection.myparameters.Add(new myParameters { ParameterName = "@DOB", mytype = SqlDbType.NVarChar, Value = Convert.ToDateTime(uProfile.BIRTHDAY).ToString("yyyy-MM-dd") });
            Connection.myparameters.Add(new myParameters { ParameterName = "@POB", mytype = SqlDbType.NVarChar, Value = uProfile.PLACEOFBIRTH });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Natioanality", mytype = SqlDbType.NVarChar, Value = uProfile.NATIONALITY });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Country", mytype = SqlDbType.NVarChar, Value = uProfile.NATCOUNTRY });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Gender", mytype = SqlDbType.NVarChar, Value = uProfile.GENDER });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Mobile", mytype = SqlDbType.NVarChar, Value = uProfile.MOBILE });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Landline", mytype = SqlDbType.NVarChar, Value = uProfile.TELPHONE });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Per_Zipcode", mytype = SqlDbType.NVarChar, Value = uProfile.PERMZIPCODE });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Per_HouseNum", mytype = SqlDbType.NVarChar, Value = uProfile.PERMHOUSENUMBER });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Per_St_VIllage_Sub", mytype = SqlDbType.NVarChar, Value = uProfile.PERMHOUSEADDRESS });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Per_Brgy", mytype = SqlDbType.NVarChar, Value = uProfile.PERMBRGY });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Per_Muni", mytype = SqlDbType.NVarChar, Value = uProfile.PERMUNI });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Per_City", mytype = SqlDbType.NVarChar, Value = uProfile.PERMCITY });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Per_Region", mytype = SqlDbType.NVarChar, Value = uProfile.PERMREGION });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Per_Country", mytype = SqlDbType.NVarChar, Value = uProfile.PERMCOUNTRY });


            Connection.myparameters.Add(new myParameters { ParameterName = "@Pre_Zipcode", mytype = SqlDbType.NVarChar, Value = uProfile.PRESZIPCODE });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Pre_HouseNum", mytype = SqlDbType.NVarChar, Value = uProfile.PRESHOUSENUMBER });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Pre_St_VIllage_Sub", mytype = SqlDbType.NVarChar, Value = uProfile.PRESHOUSEADDRESS });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Pre_Brgy", mytype = SqlDbType.NVarChar, Value = uProfile.PRESBRGY });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Pre_Muni", mytype = SqlDbType.NVarChar, Value = uProfile.PRESMUNI });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Pre_City", mytype = SqlDbType.NVarChar, Value = uProfile.PRESCITY });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Pre_Region", mytype = SqlDbType.NVarChar, Value = uProfile.PRESREGION });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Pre_Country", mytype = SqlDbType.NVarChar, Value = uProfile.PRESCOUNTRY });


            Connection.myparameters.Add(new myParameters { ParameterName = "@email", mytype = SqlDbType.NVarChar, Value = uProfile.EMAILADD });
            Connection.myparameters.Add(new myParameters { ParameterName = "@EmployerName", mytype = SqlDbType.NVarChar, Value = uProfile.EMPLOYERNAME });
            Connection.myparameters.Add(new myParameters { ParameterName = "@TIN", mytype = SqlDbType.NVarChar, Value = uProfile.TIN });
            Connection.myparameters.Add(new myParameters { ParameterName = "@CivilStatus", mytype = SqlDbType.NVarChar, Value = uProfile.CIVILSTATUS });
            Connection.myparameters.Add(new myParameters { ParameterName = "@MothersMaidenLName", mytype = SqlDbType.NVarChar, Value = uProfile.MOTHSUFFIX });
            Connection.myparameters.Add(new myParameters { ParameterName = "@MothersFname", mytype = SqlDbType.NVarChar, Value = uProfile.MOTHFIRSTNAME });
            Connection.myparameters.Add(new myParameters { ParameterName = "@MothersMName", mytype = SqlDbType.NVarChar, Value = uProfile.MOTHLASTNAME });
            Connection.myparameters.Add(new myParameters { ParameterName = "@OtherDetails", mytype = SqlDbType.NVarChar, Value = uProfile.OTHERDETAILS });

            Connection.myparameters.Add(new myParameters { ParameterName = "@COMPANYNAME", mytype = SqlDbType.NVarChar, Value = uProfile.COMPANYNAME });
            Connection.myparameters.Add(new myParameters { ParameterName = "@SPECIALIZATION", mytype = SqlDbType.NVarChar, Value = uProfile.SPECIALIZATION });
            Connection.myparameters.Add(new myParameters { ParameterName = "@ROLEPOSITIONTITLE", mytype = SqlDbType.NVarChar, Value = uProfile.ROLEPOSITIONTITLE });
            Connection.myparameters.Add(new myParameters { ParameterName = "@POSITIONLEVEL", mytype = SqlDbType.NVarChar, Value = uProfile.POSITIONLEVEL });
            Connection.myparameters.Add(new myParameters { ParameterName = "@JOINEDFROM", mytype = SqlDbType.NVarChar, Value = uProfile.JOINEDFROM });
            Connection.myparameters.Add(new myParameters { ParameterName = "@JOINEDTO", mytype = SqlDbType.NVarChar, Value = uProfile.JOINEDTO });
            Connection.myparameters.Add(new myParameters { ParameterName = "@INDUSTRY", mytype = SqlDbType.NVarChar, Value = uProfile.INDUSTRY });
            Connection.myparameters.Add(new myParameters { ParameterName = "@COUNTRY2", mytype = SqlDbType.NVarChar, Value = uProfile.COUNTRY });
            Connection.myparameters.Add(new myParameters { ParameterName = "@EXPERIENCE", mytype = SqlDbType.NVarChar, Value = uProfile.EXPERIENCE });
            Connection.myparameters.Add(new myParameters { ParameterName = "@PositionTitle", mytype = SqlDbType.NVarChar, Value = uProfile.PositionTitle });
            Connection.myparameters.Add(new myParameters { ParameterName = "@MaritalStatus", mytype = SqlDbType.NVarChar, Value = uProfile.MaritalStatus });
            Connection.myparameters.Add(new myParameters { ParameterName = "@MarriedDate", mytype = SqlDbType.NVarChar, Value = Convert.ToDateTime(uProfile.Marrieddate).ToString("yyyy-MM-dd") });
            // DataTable EmployeeDt = new DataTable();
            String ID = Connection.ExecuteScalar("sp_Insert_EmployeeProfile_FromMobile");

            //System.Web.HttpResponse httpResponse = System.Web.HttpContext.Current.Response;

            //httpResponse.Clear();
            //httpResponse.ContentType = "Application/x-msexcel";
            //httpResponse.AddHeader("Content-Disposition", String.Format("attachment; filename={0}", filename));
            //string newDIR = HostingEnvironment.MapPath("~/sFTP/");
            //workbook.Save(newDIR + filename);
            return "Success";

            //return ID;

        }

        [Route("GetImages")]
        public  String Base64Image(UserProfile_Images ProfileImages)
       // public HttpResponseMessage DisplaySignature(string id)
        {
            string file_path;
            //if (id != "")
            //{

                file_path = Path.Combine(System.Web.HttpRuntime.AppDomainAppPath + "ProfilePhotos\\" + ProfileImages.EmpID + "\\", ProfileImages.EmpID + "_" + ProfileImages.ImageType + ".png");
            //}
            //else
            //{GetImages
            //    file_path = Path.Combine(System.Web.HttpRuntime.AppDomainAppPath + "ProfilePhotos\\Signature\\", "signaturenotfound.png");
            //}
            MemoryStream ms = new MemoryStream(File.ReadAllBytes(file_path));
            HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);
            byte[] imageBytes = ms.ToArray();
            string base64String = Convert.ToBase64String(imageBytes);

            //response.Content = new StreamContent(ms);
            //response.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("image/png");
            //return response;

            


            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = ProfileImages.EmpID});
            Connection.myparameters.Add(new myParameters { ParameterName = "@ImgType", mytype = SqlDbType.NVarChar, Value = ProfileImages.ImageType});
            Connection.myparameters.Add(new myParameters { ParameterName = "@Base", mytype = SqlDbType.NVarChar, Value = base64String });


            String ID = Connection.ExecuteScalar("sp_Insert_Base64_Mobile");

            //return ID;

            return base64String;

        }

        [HttpPost]
        [Route("Save_OR_reimbursement")]
        //{
        // "EmpID": "jeya",
        // "BenefitType": "BenefitType",
        //  "ORDateTIme": "BenefitType",
        // "ORAmount": "BenefitType",
        // "VendorName": "BenefitType",
        //  "VendorTin": "BenefitType"
        //}
        public String InsertOR_ReImbursement(OR_Reimbursement ProfileImages)
        // public HttpResponseMessage DisplaySignature(string id)
        {
            //string file_path;
            ////if (id != "")
            ////{
            //file_path = Path.Combine(System.Web.HttpRuntime.AppDomainAppPath + "ProfilePhotos\\" + ProfileImages.EmpID + "\\", ProfileImages.EmpID + "_" + "reimburse.png");
            ////}
            ////else
            ////{
            ////    file_path = Path.Combine(System.Web.HttpRuntime.AppDomainAppPath + "ProfilePhotos\\Signature\\", "signaturenotfound.png");
            ////}
            //MemoryStream ms = new MemoryStream(File.ReadAllBytes(file_path));
            //HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);
            //byte[] imageBytes = ms.ToArray();
            //string base64String = Convert.ToBase64String(imageBytes);

            //response.Content = new StreamContent(ms);
            //response.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("image/png");
            //return response;




            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = ProfileImages.EmpID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@BenefitType", mytype = SqlDbType.NVarChar, Value = ProfileImages.BenefitType });
            Connection.myparameters.Add(new myParameters { ParameterName = "@ORDateTime", mytype = SqlDbType.NVarChar, Value = ProfileImages.ORDateTIme });
            Connection.myparameters.Add(new myParameters { ParameterName = "@ORAmount", mytype = SqlDbType.NVarChar, Value = ProfileImages.ORAmount });
            Connection.myparameters.Add(new myParameters { ParameterName = "@VendorName", mytype = SqlDbType.NVarChar, Value = ProfileImages.VendorName });
            Connection.myparameters.Add(new myParameters { ParameterName = "@VendorTIN", mytype = SqlDbType.NVarChar, Value = ProfileImages.VendorTin });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Base", mytype = SqlDbType.NVarChar, Value = "" });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Description", mytype = SqlDbType.NVarChar, Value = ProfileImages.Description });
            Connection.myparameters.Add(new myParameters { ParameterName = "@BenefitID", mytype = SqlDbType.NVarChar, Value = ProfileImages.BenefitID });

            Connection.ExecuteScalar("sp_Insert_OR_Reimbursement_Mobile");

            //return ID;

            return "success";

        }






        [HttpPost]
        [Route("UpdateEmployeePersonalInfo")]
        //{
        // "EmpID": "jeya",
        // "BenefitType": "BenefitType",
        //  "ORDateTIme": "BenefitType",
        // "ORAmount": "BenefitType",
        // "VendorName": "BenefitType",
        //  "VendorTin": "BenefitType"
        //}
        public String UpdateEmployeeMaster(UpdateEmployeePersonalInfo EmployeeMaster)

        {
  


            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.EmpID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@HouseNumber", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.HouseNumber });
            Connection.myparameters.Add(new myParameters { ParameterName = "@StreetName", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.StreetName });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Barangay", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Barangay });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Town", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Town });
            Connection.myparameters.Add(new myParameters { ParameterName = "@City", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.City });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Region", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Region });
            Connection.myparameters.Add(new myParameters { ParameterName = "@ZipCode", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.ZipCode });
            Connection.myparameters.Add(new myParameters { ParameterName = "@BloodType", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.BloodType });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Gender", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Gender });
            Connection.myparameters.Add(new myParameters { ParameterName = "@DOB", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.DOB });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Citizenship", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Citizenship });
            Connection.myparameters.Add(new myParameters { ParameterName = "@NameofOrganization", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.NameofOrganization });
            Connection.myparameters.Add(new myParameters { ParameterName = "@MobileAreaCode", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.MobileAreaCode });
            Connection.myparameters.Add(new myParameters { ParameterName = "@MobileNumber", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.MobileNumber });
            Connection.myparameters.Add(new myParameters { ParameterName = "@HomeAreaCode", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.HomeAreaCode });
            Connection.myparameters.Add(new myParameters { ParameterName = "@HomeNumber", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.HomeNumber });
            Connection.myparameters.Add(new myParameters { ParameterName = "@PersonalEmail", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.PersonalEmail });
            Connection.myparameters.Add(new myParameters { ParameterName = "@EmrgName", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.EmrgName });
            Connection.myparameters.Add(new myParameters { ParameterName = "@EmrgNumber", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.EmrgNumber });
            Connection.myparameters.Add(new myParameters { ParameterName = "@EmrgRelationship", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.EmrgRelationship });
            Connection.myparameters.Add(new myParameters { ParameterName = "@OptionalEmail", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.OptionalEmail });
            Connection.myparameters.Add(new myParameters { ParameterName = "@ContactNo", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.ContactNo });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Province", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Province });
            Connection.myparameters.Add(new myParameters { ParameterName = "@EmrgFname", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.EmrgFname });
            Connection.myparameters.Add(new myParameters { ParameterName = "@EmrgMname", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.EmrgMname });
            Connection.myparameters.Add(new myParameters { ParameterName = "@EmrgLname", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.EmrgLname });




            String ID = Connection.ExecuteScalar("sp_UpdatePersonalInfo_Mobile");

            return ID;



        }

        [HttpPost]
        [Route("UpdateEmployeeInfo")]
        //{
        // "EmpID": "jeya",
        // "BenefitType": "BenefitType",
        //  "ORDateTIme": "BenefitType",
        // "ORAmount": "BenefitType",
        // "VendorName": "BenefitType",
        //  "VendorTin": "BenefitType"
        //}
        public String UpdateEmployeeInfo(UpdateEmployeeInfo EmployeeMaster)

        {



            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.EmpID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@First_Name", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.First_Name });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Middle_Name", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Middle_Name });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Last_Name", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Last_Name });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Ntlogin", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Ntlogin });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Alias", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Alias });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Deptcode", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Deptcode });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Deptname", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Deptname });
            Connection.myparameters.Add(new myParameters { ParameterName = "@BUnit", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.BUnit });
            Connection.myparameters.Add(new myParameters { ParameterName = "@BUnithead", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.BUnithead });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Workstation", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Workstation });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Supervisor", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Supervisor });
            Connection.myparameters.Add(new myParameters { ParameterName = "@DTransFrom", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.DTransFrom });
            Connection.myparameters.Add(new myParameters { ParameterName = "@DTransTo", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.DTransTo });
            Connection.myparameters.Add(new myParameters { ParameterName = "@LocFrom", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.LocFrom });
            Connection.myparameters.Add(new myParameters { ParameterName = "@LocTo", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.LocTo });
            Connection.myparameters.Add(new myParameters { ParameterName = "@CCChangeFrom", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.CCChangeFrom });
            Connection.myparameters.Add(new myParameters { ParameterName = "@CCChangeTo", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.CCChangeTo });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Email", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Email });
            Connection.myparameters.Add(new myParameters { ParameterName = "@WorkEmail", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.WorkEmail });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Extension", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Extension });
            Connection.myparameters.Add(new myParameters { ParameterName = "@JoinDate", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.JoinDate });
            Connection.myparameters.Add(new myParameters { ParameterName = "@ProdStart", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.ProdStart });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Tenure", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Tenure });
            Connection.myparameters.Add(new myParameters { ParameterName = "@TenureMnth", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.TenureMnth });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Batch", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Batch });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Class", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Class });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Skill", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Skill });
            Connection.myparameters.Add(new myParameters { ParameterName = "@JobDesc", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.JobDesc });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Promotion", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Promotion });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Status", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Status });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Emp_Name", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Emp_Name });
            Connection.myparameters.Add(new myParameters { ParameterName = "@approved", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.approved });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Employee_Level", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Employee_Level });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Employee_Category", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Employee_Category });
            Connection.myparameters.Add(new myParameters { ParameterName = "@BusinessSegment", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.BusinessSegment });
            Connection.myparameters.Add(new myParameters { ParameterName = "@IsApprove", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.IsApprove });
            Connection.myparameters.Add(new myParameters { ParameterName = "@ResignedDate", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.ResignedDate });




            String ID = Connection.ExecuteScalar("sp_UpdateEmployeeInfo_Mobile");

            return ID;



        }


        [HttpPost]
        [Route("UpdateEmpConfidentialInfo")]
        //{
        // "EmpID": "jeya",
        // "BenefitType": "BenefitType",
        //  "ORDateTIme": "BenefitType",
        // "ORAmount": "BenefitType",
        // "VendorName": "BenefitType",
        //  "VendorTin": "BenefitType"
        //}
        public String UpdateEmpConfidentialInfo(UpdateEmployeeConfiInfo EmployeeMaster)

        {



            Connection Connection = new Connection();
            Connection.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.EmpID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@TaxStatus", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.TaxStatus });
            Connection.myparameters.Add(new myParameters { ParameterName = "@SSS", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.SSS });
            Connection.myparameters.Add(new myParameters { ParameterName = "@TIN", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.TIN });
            Connection.myparameters.Add(new myParameters { ParameterName = "@PhilHealth", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.PhilHealth });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Pagibig", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Pagibig });
            Connection.myparameters.Add(new myParameters { ParameterName = "@DriversLicense", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.DriversLicense });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Passport", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Passport });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Unified", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Unified });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Postal", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Postal });
            Connection.myparameters.Add(new myParameters { ParameterName = "@BirthCertificate", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.BirthCertificate });
            Connection.myparameters.Add(new myParameters { ParameterName = "@SssImage", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.SssImage });
            Connection.myparameters.Add(new myParameters { ParameterName = "@TinImage", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.TinImage });
            Connection.myparameters.Add(new myParameters { ParameterName = "@PhilImage", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.PhilImage });
            Connection.myparameters.Add(new myParameters { ParameterName = "@PagImage", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.PagImage });
            Connection.myparameters.Add(new myParameters { ParameterName = "@DriverImage", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.DriverImage });
            Connection.myparameters.Add(new myParameters { ParameterName = "@PassportImage", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.PassportImage });
            Connection.myparameters.Add(new myParameters { ParameterName = "@UmidImage", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.UmidImage });
            Connection.myparameters.Add(new myParameters { ParameterName = "@PostalImage", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.PostalImage });
            Connection.myparameters.Add(new myParameters { ParameterName = "@BirthImage", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.BirthImage });
            Connection.myparameters.Add(new myParameters { ParameterName = "@DriversExpiry", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.DriversExpiry });
            Connection.myparameters.Add(new myParameters { ParameterName = "@PassportExpiry", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.PassportExpiry });
            Connection.myparameters.Add(new myParameters { ParameterName = "@PostalExpiry", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.PostalExpiry });
            Connection.myparameters.Add(new myParameters { ParameterName = "@SAGSD", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.SAGSD });
            Connection.myparameters.Add(new myParameters { ParameterName = "@SAGSDExpiry", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.SAGSDExpiry });
            Connection.myparameters.Add(new myParameters { ParameterName = "@NTC", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.NTC });
            Connection.myparameters.Add(new myParameters { ParameterName = "@NTCExpiry", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.NTCExpiry });
            Connection.myparameters.Add(new myParameters { ParameterName = "@BrgyClearance", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.BrgyClearance });
            Connection.myparameters.Add(new myParameters { ParameterName = "@BrgyClearanceExpiry", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.BrgyClearanceExpiry });
            Connection.myparameters.Add(new myParameters { ParameterName = "@PNPClearance", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.PNPClearance });
            Connection.myparameters.Add(new myParameters { ParameterName = "@PNPClearanceExpiry", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.PNPClearanceExpiry });
            Connection.myparameters.Add(new myParameters { ParameterName = "@NBIClearance", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.NBIClearance });
            Connection.myparameters.Add(new myParameters { ParameterName = "@NBIClearanceExpiry", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.NBIClearanceExpiry });
            Connection.myparameters.Add(new myParameters { ParameterName = "@NueroEvaluation", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.NueroEvaluation });
            Connection.myparameters.Add(new myParameters { ParameterName = "@NeuroEvaluationExpiry", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.NeuroEvaluationExpiry });
            Connection.myparameters.Add(new myParameters { ParameterName = "@DrugTest", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.DrugTest });
            Connection.myparameters.Add(new myParameters { ParameterName = "@DrugTestExpiry", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.DrugTestExpiry });




            String ID = Connection.ExecuteScalar("sp_UpdateEmployeeConfidentialInfo_Mobile");

            return ID;



        }




        ///

        //{
        //"LOGINID": "i143"
        //}
        [HttpPost]
        [Route("DisplayCompleteProfile")]
        public Information DisplayCompleteProfile(Login Log)
        {
            Information ListUs = new Information();

            List<Master_User_Info_Personal_Confi_Info> ListReturned = new List<Master_User_Info_Personal_Confi_Info>();
            System.Data.DataTable DTProfile = new System.Data.DataTable();

            Connection con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@empId", mytype = SqlDbType.NVarChar, Value = Log.LoginID });
            DTProfile = con.GetDataTable("sp_getCompleteProfile_Mobile");
            if (DTProfile.Rows.Count>0)
            {
                Master_User_Info_Personal_Confi_Info MPC = new Master_User_Info_Personal_Confi_Info();

                MPC.SeriesID = DTProfile.Rows[0]["SeriesID"].ToString();
                MPC.EmpID = DTProfile.Rows[0]["EmpID"].ToString();
                MPC.First_Name = DTProfile.Rows[0]["First_Name"].ToString();
                MPC.Middle_Name = DTProfile.Rows[0]["Middle_Name"].ToString();
                MPC.Last_Name = DTProfile.Rows[0]["Last_Name"].ToString();
                MPC.NTID = DTProfile.Rows[0]["NTID"].ToString();
                MPC.EmpName = DTProfile.Rows[0]["EmpName"].ToString();
                MPC.EmpType = DTProfile.Rows[0]["EmpType"].ToString();
                MPC.EmpLevel = DTProfile.Rows[0]["EmpLevel"].ToString();
                MPC.EmpStatus = DTProfile.Rows[0]["EmpStatus"].ToString();
                MPC.UserType = DTProfile.Rows[0]["UserType"].ToString();
                MPC.Gender = DTProfile.Rows[0]["Gender"].ToString();
                MPC.DateJoined = DTProfile.Rows[0]["DateJoined"].ToString();
                MPC.ContractEndDate = DTProfile.Rows[0]["ContractEndDate"].ToString();
                MPC.LeaveBal = DTProfile.Rows[0]["LeaveBal"].ToString();
                MPC.LeaveBalCTO = DTProfile.Rows[0]["LeaveBalCTO"].ToString();
                MPC.Country = DTProfile.Rows[0]["Country"].ToString();
                MPC.JobDesc = DTProfile.Rows[0]["JobDesc"].ToString();
                MPC.DeptCode = DTProfile.Rows[0]["DeptCode"].ToString();
                MPC.DeptName = DTProfile.Rows[0]["DeptName"].ToString();
                MPC.MngrID = DTProfile.Rows[0]["MngrID"].ToString();
                MPC.MngrName = DTProfile.Rows[0]["MngrName"].ToString();
                MPC.MngrNTID = DTProfile.Rows[0]["MngrNTID"].ToString();
                MPC.BusinessUnit = DTProfile.Rows[0]["BusinessUnit"].ToString();
                MPC.BusinessUnitHead = DTProfile.Rows[0]["BusinessUnitHead"].ToString();
                MPC.BusinessSegment = DTProfile.Rows[0]["BusinessSegment"].ToString();
                MPC.AccessLevel = DTProfile.Rows[0]["AccessLevel"].ToString();
                MPC.DOB = DTProfile.Rows[0]["DOB"].ToString();
                MPC.Alias = DTProfile.Rows[0]["Alias"].ToString();
                MPC.ExtensionNumber = DTProfile.Rows[0]["ExtensionNumber"].ToString();
                MPC.Skill = DTProfile.Rows[0]["Skill"].ToString();
                MPC.Batch = DTProfile.Rows[0]["Batch"].ToString();
                MPC.Class = DTProfile.Rows[0]["Class"].ToString();
                MPC.Workstation = DTProfile.Rows[0]["Workstation"].ToString();
                MPC.ProductionStartDate = DTProfile.Rows[0]["ProductionStartDate"].ToString();
                MPC.ModifiedBy = DTProfile.Rows[0]["ModifiedBy"].ToString();
                MPC.DateModified = DTProfile.Rows[0]["DateModified"].ToString();
                MPC.EmpPayHrsCat = DTProfile.Rows[0]["EmpPayHrsCat"].ToString();
                MPC.Salary = DTProfile.Rows[0]["Salary"].ToString();
                MPC.DailyRate = DTProfile.Rows[0]["DailyRate"].ToString();
                MPC.EcolaWageOrder = DTProfile.Rows[0]["EcolaWageOrder"].ToString();
                MPC.EcolaRate = DTProfile.Rows[0]["EcolaRate"].ToString();
                MPC.Transportation = DTProfile.Rows[0]["Transportation"].ToString();
                MPC.Rice = DTProfile.Rows[0]["Rice"].ToString();
                MPC.Communication = DTProfile.Rows[0]["Communication"].ToString();
                MPC.EcolaRegion = DTProfile.Rows[0]["EcolaRegion"].ToString();
                MPC.Branch = DTProfile.Rows[0]["Branch"].ToString();
                MPC.Wallet = DTProfile.Rows[0]["Wallet"].ToString();
                MPC.FaceID = DTProfile.Rows[0]["FaceID"].ToString();
                MPC.DTransFrom = DTProfile.Rows[0]["DTransFrom"].ToString();
                MPC.DTransTo = DTProfile.Rows[0]["DTransTo"].ToString();
                MPC.LocFrom = DTProfile.Rows[0]["LocFrom"].ToString();
                MPC.LocTo = DTProfile.Rows[0]["LocTo"].ToString();
                MPC.CCChangeFrom = DTProfile.Rows[0]["CCChangeFrom"].ToString();
                MPC.CCChangeTo = DTProfile.Rows[0]["CCChangeTo"].ToString();
                MPC.Email = DTProfile.Rows[0]["Email"].ToString();
                MPC.WorkEmail = DTProfile.Rows[0]["WorkEmail"].ToString();
                MPC.Extension = DTProfile.Rows[0]["Extension"].ToString();
                MPC.JoinDate = DTProfile.Rows[0]["JoinDate"].ToString();
                MPC.ProdStart = DTProfile.Rows[0]["ProdStart"].ToString();
                MPC.Tenure = DTProfile.Rows[0]["Tenure"].ToString();
                MPC.TenureMnth = DTProfile.Rows[0]["TenureMnth"].ToString();
                MPC.Batch1 = DTProfile.Rows[0]["Batch"].ToString();
                MPC.Class1 = DTProfile.Rows[0]["Class"].ToString();
                MPC.Skill1 = DTProfile.Rows[0]["Skill"].ToString();
                MPC.JobDesc1 = DTProfile.Rows[0]["JobDesc"].ToString();
                MPC.Promotion = DTProfile.Rows[0]["Promotion"].ToString();
                MPC.Status = DTProfile.Rows[0]["Status"].ToString();
                MPC.Emp_Name = DTProfile.Rows[0]["Emp_Name"].ToString();
                MPC.approved = DTProfile.Rows[0]["approved"].ToString();
                MPC.Employee_Level = DTProfile.Rows[0]["Employee_Level"].ToString();
                MPC.Employee_Category = DTProfile.Rows[0]["Employee_Category"].ToString();
                MPC.BusinessSegment1 = DTProfile.Rows[0]["BusinessSegment"].ToString();
                MPC.IsApprove = DTProfile.Rows[0]["IsApprove"].ToString();
                MPC.ResignedDate = DTProfile.Rows[0]["ResignedDate"].ToString();
                MPC.HouseNumber = DTProfile.Rows[0]["HouseNumber"].ToString();
                MPC.StreetName = DTProfile.Rows[0]["StreetName"].ToString();
                MPC.Barangay = DTProfile.Rows[0]["Barangay"].ToString();
                MPC.Town = DTProfile.Rows[0]["Town"].ToString();
                MPC.City = DTProfile.Rows[0]["City"].ToString();
                MPC.Region = DTProfile.Rows[0]["Region"].ToString();
                MPC.ZipCode = DTProfile.Rows[0]["ZipCode"].ToString();
                MPC.BloodType = DTProfile.Rows[0]["BloodType"].ToString();
                MPC.Gender1 = DTProfile.Rows[0]["Gender"].ToString();
                MPC.DOB1 = DTProfile.Rows[0]["DOB"].ToString();
                MPC.Citizenship = DTProfile.Rows[0]["Citizenship"].ToString();
                MPC.NameofOrganization = DTProfile.Rows[0]["NameofOrganization"].ToString();
                MPC.MobileAreaCode = DTProfile.Rows[0]["MobileAreaCode"].ToString();
                MPC.MobileNumber = DTProfile.Rows[0]["MobileNumber"].ToString();
                MPC.HomeAreaCode = DTProfile.Rows[0]["HomeAreaCode"].ToString();
                MPC.HomeNumber = DTProfile.Rows[0]["HomeNumber"].ToString();
                // PersonalEmail = dr["PersonalEmail"].ToString(),
                MPC.EmrgName = DTProfile.Rows[0]["EmrgName"].ToString();
                MPC.EmrgNumber = DTProfile.Rows[0]["EmrgNumber"].ToString();
                MPC.EmrgRelationship = DTProfile.Rows[0]["EmrgRelationship"].ToString();
                MPC.OptionalEmail = DTProfile.Rows[0]["OptionalEmail"].ToString();
                // ContactNo = dr["ContactNo"].ToString(),
                MPC.Province = DTProfile.Rows[0]["Province"].ToString();
                MPC.TaxStatus = DTProfile.Rows[0]["TaxStatus"].ToString();

                MPC.SSS = DTProfile.Rows[0]["SSS"].ToString();
                MPC.TIN = DTProfile.Rows[0]["TIN"].ToString();
                MPC.PhilHealth = DTProfile.Rows[0]["PhilHealth"].ToString();
                MPC.Pagibig = DTProfile.Rows[0]["Pagibig"].ToString();
                MPC.DriversLicense = DTProfile.Rows[0]["DriversLicense"].ToString();
                MPC.Passport = DTProfile.Rows[0]["Passport"].ToString();
                MPC.Unified = DTProfile.Rows[0]["Unified"].ToString();
                MPC.Postal = DTProfile.Rows[0]["Postal"].ToString();
                MPC.BirthCertificate = DTProfile.Rows[0]["BirthCertificate"].ToString();
                MPC.SssImage = DTProfile.Rows[0]["SssImage"].ToString();
                MPC.TinImage = DTProfile.Rows[0]["TinImage"].ToString();
                MPC.PhilImage = DTProfile.Rows[0]["PhilImage"].ToString();
                MPC.PagImage = DTProfile.Rows[0]["PagImage"].ToString();
                MPC.DriverImage = DTProfile.Rows[0]["DriverImage"].ToString();
                MPC.PassportImage = DTProfile.Rows[0]["PassportImage"].ToString();
                MPC.UmidImage = DTProfile.Rows[0]["UmidImage"].ToString();
                MPC.PostalImage = DTProfile.Rows[0]["PostalImage"].ToString();
                MPC.BirthImage = DTProfile.Rows[0]["BirthImage"].ToString();
                MPC.DriversExpiry = DTProfile.Rows[0]["DriversExpiry"].ToString();
                MPC.PassportExpiry = DTProfile.Rows[0]["PassportExpiry"].ToString();
                MPC.PostalExpiry = DTProfile.Rows[0]["PostalExpiry"].ToString();
                MPC.SAGSD = DTProfile.Rows[0]["SAGSD"].ToString();
                MPC.SAGSDExpiry = DTProfile.Rows[0]["SAGSDExpiry"].ToString();
                MPC.NTC = DTProfile.Rows[0]["NTC"].ToString();
                MPC.NTCExpiry = DTProfile.Rows[0]["NTCExpiry"].ToString();
                MPC.BrgyClearance = DTProfile.Rows[0]["BrgyClearance"].ToString();
                MPC.BrgyClearanceExpiry = DTProfile.Rows[0]["BrgyClearanceExpiry"].ToString();
                MPC.PNPClearance = DTProfile.Rows[0]["PNPClearance"].ToString();
                MPC.PNPClearanceExpiry = DTProfile.Rows[0]["PNPClearanceExpiry"].ToString();
                MPC.NBIClearance = DTProfile.Rows[0]["NBIClearance"].ToString();
                MPC.NBIClearanceExpiry = DTProfile.Rows[0]["NBIClearanceExpiry"].ToString();
                MPC.NueroEvaluation = DTProfile.Rows[0]["NueroEvaluation"].ToString();
                MPC.NeuroEvaluationExpiry = DTProfile.Rows[0]["NeuroEvaluationExpiry"].ToString();
                MPC.DrugTest = DTProfile.Rows[0]["DrugTest"].ToString();
                MPC.DrugTestExpiry = DTProfile.Rows[0]["DrugTestExpiry"].ToString();
                MPC.PlaceofBirth= DTProfile.Rows[0]["POB"].ToString();


                System.Data.DataTable DTContactNo = new System.Data.DataTable();
                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = Log.LoginID });
                DTContactNo = con.GetDataTable("sp_GetPersonalContact");


                List<ContactNo> Clist = new List<ContactNo>();
                for (int ii = 0; ii < DTContactNo.Rows.Count; ii++)
                {
                    ContactNo Contact = new ContactNo();
                    Contact.Contact = DTContactNo.Rows[ii]["Number"].ToString();
                    Contact.ContactType = DTContactNo.Rows[ii]["NumberType"].ToString();

                    Clist.Add(Contact);
                }
                MPC.ContactNo = Clist;


                System.Data.DataTable DTEmail = new System.Data.DataTable();
                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = Log.LoginID });
                DTEmail = con.GetDataTable("sp_GetPersonalEmail");
                List<PersonalEmail> EMaillist = new List<PersonalEmail>();
                for (int iii = 0; iii < DTEmail.Rows.Count; iii++)
                {
                    PersonalEmail Email = new PersonalEmail();
                    Email.Email = DTEmail.Rows[iii]["Email"].ToString();
                    Email.EmailType = DTEmail.Rows[iii]["EmailType"].ToString();

                    EMaillist.Add(Email);
                }
                MPC.PersonalEmail = EMaillist;

                ListReturned.Add(MPC);
            }
            ListUs.CompleteProfile = ListReturned;
            ListUs.myreturn = "Success";
            return ListUs;



                //Connection Connection = new Connection();

            //Connection.myparameters.Add(new myParameters { ParameterName = "@empId", mytype = SqlDbType.NVarChar, Value = userToSave.LoginID });

            //DataRow dr;

            //dr = Connection.GetSingleRow("sp_getCompleteProfile_Mobile");

            //Master_User_Info_Personal_Confi_Info userprofile = new Master_User_Info_Personal_Confi_Info
            //{
            //    SeriesID = dr["SeriesID"].ToString(),
            //    EmpID = dr["EmpID"].ToString(),
            //    First_Name = dr["First_Name"].ToString(),
            //    Middle_Name = dr["Middle_Name"].ToString(),
            //    Last_Name = dr["Last_Name"].ToString(),
            //    NTID = dr["NTID"].ToString(),
            //    EmpName = dr["EmpName"].ToString(),
            //    EmpType = dr["EmpType"].ToString(),
            //    EmpLevel = dr["EmpLevel"].ToString(),
            //    EmpStatus = dr["EmpStatus"].ToString(),
            //    UserType = dr["UserType"].ToString(),
            //    Gender = dr["Gender"].ToString(),
            //    DateJoined = dr["DateJoined"].ToString(),
            //    ContractEndDate = dr["ContractEndDate"].ToString(),
            //    LeaveBal = dr["LeaveBal"].ToString(),
            //    LeaveBalCTO = dr["LeaveBalCTO"].ToString(),
            //    Country = dr["Country"].ToString(),
            //    JobDesc = dr["JobDesc"].ToString(),
            //    DeptCode = dr["DeptCode"].ToString(),
            //    DeptName = dr["DeptName"].ToString(),
            //    MngrID = dr["MngrID"].ToString(),
            //    MngrName = dr["MngrName"].ToString(),
            //    MngrNTID = dr["MngrNTID"].ToString(),
            //    BusinessUnit = dr["BusinessUnit"].ToString(),
            //    BusinessUnitHead = dr["BusinessUnitHead"].ToString(),
            //    BusinessSegment = dr["BusinessSegment"].ToString(),
            //    AccessLevel = dr["AccessLevel"].ToString(),
            //    DOB = dr["DOB"].ToString(),
            //    Alias = dr["Alias"].ToString(),
            //    ExtensionNumber = dr["ExtensionNumber"].ToString(),
            //    Skill = dr["Skill"].ToString(),
            //    Batch = dr["Batch"].ToString(),
            //    Class = dr["Class"].ToString(),
            //    Workstation = dr["Workstation"].ToString(),
            //    ProductionStartDate = dr["ProductionStartDate"].ToString(),
            //    ModifiedBy = dr["ModifiedBy"].ToString(),
            //    DateModified = dr["DateModified"].ToString(),
            //    EmpPayHrsCat = dr["EmpPayHrsCat"].ToString(),
            //    Salary = dr["Salary"].ToString(),
            //    DailyRate = dr["DailyRate"].ToString(),
            //    EcolaWageOrder = dr["EcolaWageOrder"].ToString(),
            //    EcolaRate = dr["EcolaRate"].ToString(),
            //    Transportation = dr["Transportation"].ToString(),
            //    Rice = dr["Rice"].ToString(),
            //    Communication = dr["Communication"].ToString(),
            //    EcolaRegion = dr["EcolaRegion"].ToString(),
            //    Branch = dr["Branch"].ToString(),
            //    Wallet = dr["Wallet"].ToString(),
            //    FaceID = dr["FaceID"].ToString(),
            //    DTransFrom = dr["DTransFrom"].ToString(),
            //    DTransTo = dr["DTransTo"].ToString(),
            //    LocFrom = dr["LocFrom"].ToString(),
            //    LocTo = dr["LocTo"].ToString(),
            //    CCChangeFrom = dr["CCChangeFrom"].ToString(),
            //    CCChangeTo = dr["CCChangeTo"].ToString(),
            //    Email = dr["Email"].ToString(),
            //    WorkEmail = dr["WorkEmail"].ToString(),
            //    Extension = dr["Extension"].ToString(),
            //    JoinDate = dr["JoinDate"].ToString(),
            //    ProdStart = dr["ProdStart"].ToString(),
            //    Tenure = dr["Tenure"].ToString(),
            //    TenureMnth = dr["TenureMnth"].ToString(),
            //    Batch1 = dr["Batch"].ToString(),
            //    Class1 = dr["Class"].ToString(),
            //    Skill1 = dr["Skill"].ToString(),
            //    JobDesc1 = dr["JobDesc"].ToString(),
            //    Promotion = dr["Promotion"].ToString(),
            //    Status = dr["Status"].ToString(),
            //    Emp_Name = dr["Emp_Name"].ToString(),
            //    approved = dr["approved"].ToString(),
            //    Employee_Level = dr["Employee_Level"].ToString(),
            //    Employee_Category = dr["Employee_Category"].ToString(),
            //    BusinessSegment1 = dr["BusinessSegment"].ToString(),
            //    IsApprove = dr["IsApprove"].ToString(),
            //    ResignedDate = dr["ResignedDate"].ToString(),
            //    HouseNumber = dr["HouseNumber"].ToString(),
            //    StreetName = dr["StreetName"].ToString(),
            //    Barangay = dr["Barangay"].ToString(),
            //    Town = dr["Town"].ToString(),
            //    City = dr["City"].ToString(),
            //    Region = dr["Region"].ToString(),
            //    ZipCode = dr["ZipCode"].ToString(),
            //    BloodType = dr["BloodType"].ToString(),
            //    Gender1 = dr["Gender"].ToString(),
            //    DOB1 = dr["DOB"].ToString(),
            //    Citizenship = dr["Citizenship"].ToString(),
            //    NameofOrganization = dr["NameofOrganization"].ToString(),
            //    MobileAreaCode = dr["MobileAreaCode"].ToString(),
            //    MobileNumber = dr["MobileNumber"].ToString(),
            //    HomeAreaCode = dr["HomeAreaCode"].ToString(),
            //    HomeNumber = dr["HomeNumber"].ToString(),
            //   // PersonalEmail = dr["PersonalEmail"].ToString(),
            //    EmrgName = dr["EmrgName"].ToString(),
            //    EmrgNumber = dr["EmrgNumber"].ToString(),
            //    EmrgRelationship = dr["EmrgRelationship"].ToString(),
            //    OptionalEmail = dr["OptionalEmail"].ToString(),
            //   // ContactNo = dr["ContactNo"].ToString(),
            //    Province = dr["Province"].ToString(),
            //    TaxStatus = dr["TaxStatus"].ToString(),
            //    SSS = dr["SSS"].ToString(),
            //    TIN = dr["TIN"].ToString(),
            //    PhilHealth = dr["PhilHealth"].ToString(),
            //    Pagibig = dr["Pagibig"].ToString(),
            //    DriversLicense = dr["DriversLicense"].ToString(),
            //    Passport = dr["Passport"].ToString(),
            //    Unified = dr["Unified"].ToString(),
            //    Postal = dr["Postal"].ToString(),
            //    BirthCertificate = dr["BirthCertificate"].ToString(),
            //    SssImage = dr["SssImage"].ToString(),
            //    TinImage = dr["TinImage"].ToString(),
            //    PhilImage = dr["PhilImage"].ToString(),
            //    PagImage = dr["PagImage"].ToString(),
            //    DriverImage = dr["DriverImage"].ToString(),
            //    PassportImage = dr["PassportImage"].ToString(),
            //    UmidImage = dr["UmidImage"].ToString(),
            //    PostalImage = dr["PostalImage"].ToString(),
            //    BirthImage = dr["BirthImage"].ToString(),
            //    DriversExpiry = dr["DriversExpiry"].ToString(),
            //    PassportExpiry = dr["PassportExpiry"].ToString(),
            //    PostalExpiry = dr["PostalExpiry"].ToString(),
            //    SAGSD = dr["SAGSD"].ToString(),
            //    SAGSDExpiry = dr["SAGSDExpiry"].ToString(),
            //    NTC = dr["NTC"].ToString(),
            //    NTCExpiry = dr["NTCExpiry"].ToString(),
            //    BrgyClearance = dr["BrgyClearance"].ToString(),
            //    BrgyClearanceExpiry = dr["BrgyClearanceExpiry"].ToString(),
            //    PNPClearance = dr["PNPClearance"].ToString(),
            //    PNPClearanceExpiry = dr["PNPClearanceExpiry"].ToString(),
            //    NBIClearance = dr["NBIClearance"].ToString(),
            //    NBIClearanceExpiry = dr["NBIClearanceExpiry"].ToString(),
            //    NueroEvaluation = dr["NueroEvaluation"].ToString(),
            //    NeuroEvaluationExpiry = dr["NeuroEvaluationExpiry"].ToString(),
            //    DrugTest = dr["DrugTest"].ToString(),
            //    DrugTestExpiry = dr["DrugTestExpiry"].ToString()




            //};
            //return userprofile;

        }



        //public EmployerHistory DisplayEmploymentHistory(Login userToSave)
        //{
        //    Connection Connection = new Connection();

        //    Connection.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = userToSave.LoginID });

        //    DataRow dr;

        //    dr = Connection.GetSingleRow("sp_ShowEmployerHistory_Mobile");

        //    EmployerHistory EmployerHis = new EmployerHistory
        //    {
        //        Company_Name = dr["CompanyName"].ToString(),
        //        Work_From = dr["joinedFrom"].ToString(),
        //        Work_To = dr["joinedTo"].ToString()
        //    };
        //    return EmployerHis;

        //}

        //{     
        //"LoginID": "i004"
        //}
        [HttpPost]
        [Route("DisplayEmployerHistory")]
        public EmployerList SDisplayEmploymentHistory(Login userToSave)
        {
            EmployerList GetEmployerLists = new EmployerList();
            try
            {
                Connection Connection = new Connection();
                List<EmployerHistory> ListEmployers = new List<EmployerHistory>();
                DataSet ds = new DataSet();
                System.Data.DataTable dtLEAVES = new System.Data.DataTable();

                Connection.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = userToSave.LoginID });

                ds = Connection.GetDataset("sp_ShowEmployerHistory_Mobile");
                dtLEAVES = ds.Tables[0];
                if (dtLEAVES.Rows.Count > 0)
                {
                    foreach (DataRow row in dtLEAVES.Rows)
                    {
                        EmployerHistory AvailableLeaves = new EmployerHistory
                        {


                            Company_Name = row["CompanyName"].ToString(),
                            Work_From = row["joinedFrom"].ToString(),
                            Work_To = row["joinedTo"].ToString()


                        };
                        ListEmployers.Add(AvailableLeaves);
                    }
                    GetEmployerLists.EmployerListing = ListEmployers;
                    GetEmployerLists.myreturn = "Success";
                }
                else
                {
                    GetEmployerLists.myreturn = "No Data Available";
                }
            }
            catch (Exception ex)
            {
                GetEmployerLists.myreturn = ex.Message;
            }
            return GetEmployerLists;
        }





        [HttpPost]
        [Route("InsertEmployeeDependents")]
        //{
        // "EmpID": "jeya",
        // "BenefitType": "BenefitType",
        //  "ORDateTIme": "BenefitType",
        // "ORAmount": "BenefitType",
        // "VendorName": "BenefitType",
        //  "VendorTin": "BenefitType"
        //}
        public String InsertEmployeeDependents(DependentHistory DependentDetails)

        {



            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.NVarChar, Value = DependentDetails.EmpID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@RELATION", mytype = SqlDbType.NVarChar, Value = DependentDetails.Relationship });
            Connection.myparameters.Add(new myParameters { ParameterName = "@FIRST", mytype = SqlDbType.NVarChar, Value = DependentDetails.First });
            Connection.myparameters.Add(new myParameters { ParameterName = "@MID", mytype = SqlDbType.NVarChar, Value = DependentDetails.Mid });
            Connection.myparameters.Add(new myParameters { ParameterName = "@LAST", mytype = SqlDbType.NVarChar, Value = DependentDetails.Lastname });
            Connection.myparameters.Add(new myParameters { ParameterName = "@BIRTH", mytype = SqlDbType.NVarChar, Value = DependentDetails.Birth });
            Connection.myparameters.Add(new myParameters { ParameterName = "@COMMENT", mytype = SqlDbType.NVarChar, Value = DependentDetails.Comments });
            Connection.myparameters.Add(new myParameters { ParameterName = "@FILE", mytype = SqlDbType.NVarChar, Value = DependentDetails.DocFilename });
            Connection.myparameters.Add(new myParameters { ParameterName = "@EXTENSION", mytype = SqlDbType.NVarChar, Value = DependentDetails.Extension });

            String ID = Connection.ExecuteScalar("sp_InsertDependentV2");

            return ID;



        }














        //{     
        //"LoginID": "i004"
        //}
        [HttpPost]
        [Route("DisplayDependents")]
        public DependentList DisplayDependents(Login userToSave)
        {
            DependentList GetDependentLists = new DependentList();
            try
            {
                Connection Connection = new Connection();
                List<DependentHistory> ListDependents = new List<DependentHistory>();
                DataSet ds = new DataSet();
                System.Data.DataTable dtLEAVES = new System.Data.DataTable();

                Connection.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = userToSave.LoginID });
               
                ds = Connection.GetDataset("sp_ShowDependentList");
                dtLEAVES = ds.Tables[0];
                if (dtLEAVES.Rows.Count > 0)
                {
                    foreach (DataRow row in dtLEAVES.Rows)
                    {
                        DependentHistory AvailableLeaves = new DependentHistory
                        {

                            FullName = row["FullName"].ToString(),
                            DOB = row["DateOfBirth"].ToString(),
                            Relationship = row["Relation"].ToString()


                        };
                        ListDependents.Add(AvailableLeaves);
                    }
                    GetDependentLists.DependentListing = ListDependents;
                    GetDependentLists.myreturn = "Success";
                }
                else
                {
                    GetDependentLists.myreturn = "No Data Available";
                }
            }
            catch (Exception ex)
            {
                GetDependentLists.myreturn = ex.Message;
            }
            return GetDependentLists;
        }


        [HttpPost]
        [Route("InsertEmployeeHistory")]
        //{
        // "EmpID": "jeya",
        // "BenefitType": "BenefitType",
        //  "ORDateTIme": "BenefitType",
        // "ORAmount": "BenefitType",
        // "VendorName": "BenefitType",
        //  "VendorTin": "BenefitType"
        //}
        public String InsertEmployeeHistory(UserProfile uProfile)

        {



            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = uProfile.NTID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@COMPANYNAME", mytype = SqlDbType.NVarChar, Value = uProfile.COMPANYNAME });
            Connection.myparameters.Add(new myParameters { ParameterName = "@SPECIALIZATION", mytype = SqlDbType.NVarChar, Value = uProfile.SPECIALIZATION });
            Connection.myparameters.Add(new myParameters { ParameterName = "@ROLEPOSITIONTITLE", mytype = SqlDbType.NVarChar, Value = uProfile.ROLEPOSITIONTITLE });
            Connection.myparameters.Add(new myParameters { ParameterName = "@POSITIONLEVEL", mytype = SqlDbType.NVarChar, Value = uProfile.POSITIONLEVEL });
            Connection.myparameters.Add(new myParameters { ParameterName = "@JOINEDFROM", mytype = SqlDbType.NVarChar, Value = Convert.ToDateTime(uProfile.JOINEDFROM).ToString("yyyy-MM-dd") });
            Connection.myparameters.Add(new myParameters { ParameterName = "@JOINEDTO", mytype = SqlDbType.NVarChar, Value =  Convert.ToDateTime(uProfile.JOINEDTO).ToString("yyyy-MM-dd") });
            Connection.myparameters.Add(new myParameters { ParameterName = "@INDUSTRY", mytype = SqlDbType.NVarChar, Value = uProfile.INDUSTRY });
            Connection.myparameters.Add(new myParameters { ParameterName = "@COUNTRY", mytype = SqlDbType.NVarChar, Value = uProfile.COUNTRY });
            Connection.myparameters.Add(new myParameters { ParameterName = "@EXPERIENCE", mytype = SqlDbType.NVarChar, Value = uProfile.EXPERIENCE });
            Connection.myparameters.Add(new myParameters { ParameterName = "@STATUSDATE", mytype = SqlDbType.NVarChar, Value = "Inactive" });
            Connection.myparameters.Add(new myParameters { ParameterName = "@PositionTitle", mytype = SqlDbType.NVarChar, Value = uProfile.PositionTitle });

            String ID = Connection.ExecuteScalar("sp_InsertEmployeeHistory");

            return ID;



        }

        [HttpPost]
        [Route("UpdateReimburse")]
        public async Task<HttpResponseMessage> reimburseupdate()
        {
            var datenow = DateTime.Now.ToString("yyyyMMddHHmmss");
            HttpRequest httpRequest = HttpContext.Current.Request;

            BinaryReader binaryReader = new BinaryReader(httpRequest.Files[0].InputStream);

            string NTID = httpRequest.Form["NTID"];
            //string Name = httpRequest.Form["Name"];
            byte[] Photo = binaryReader.ReadBytes(httpRequest.Files[0].ContentLength);
            // string Set = httpRequest.Form["Set"];

            HttpResponseMessage msg;

            //var path = HttpContext.Current.Server.MapPath("~/Profile/");
            var path = HttpContext.Current.Server.MapPath("~/ProfilePhotos/");
            //var path = "http://uat-vm.southeastasia.cloudapp.azure.com:81/hrms_test/ProfilePhotos";
            var folder = Path.Combine(path, NTID);
            Directory.CreateDirectory(folder);

            MemoryStream stream = new MemoryStream();

            try
            {
                using (stream)
                {
                    File.WriteAllBytes(folder + "/" + NTID + "_reimburse-"+ datenow + ".png", Photo);
                }

                msg = Request.CreateResponse(HttpStatusCode.Accepted);
            }
            catch
            {
                msg = Request.CreateResponse(HttpStatusCode.NotAcceptable);
            }

            // return msg;

            ////HttpRequest httpRequest = HttpContext.Current.Request;

            ////BinaryReader binaryReader = new BinaryReader(httpRequest.Files[0].InputStream);

            ////string NTID = httpRequest.Form["NTID"];
            ////string Name = httpRequest.Form["Name"];
            ////byte[] Photo = binaryReader.ReadBytes(httpRequest.Files[0].ContentLength);
            ////reImburse.Image = reImburse.Image.Replace("data:image/png;base64,", "");
            ////reImburse.Image = reImburse.Image.Replace("data:image/jpeg;base64,", "");
            //string NTID = reImburse.EmpID;
            ////byte[] Photo = Convert.FromBase64String(reImburse.Image);
            ////string Set = httpRequest.Form["Set"];

            //HttpResponseMessage msg;

            ////var path = HttpContext.Current.Server.MapPath("~/Profile/");
            //var path = HttpContext.Current.Server.MapPath("~/ProfilePhotos/");
            ////var path = "http://uat-vm.southeastasia.cloudapp.azure.com:81/hrms_test/ProfilePhotos";
            //var folder = Path.Combine(path, NTID);
            //Directory.CreateDirectory(folder);

            //MemoryStream stream = new MemoryStream();

            //try
            //{
            //    using (stream)
            //    {
            //        //File.WriteAllBytes(folder + "/" + NTID + "_pic" + Set + ".png", Photo);
            //        File.WriteAllBytes(folder + "/" + NTID + "_reimburse" + ".png", reImburse.Byte);
            //    }

            //    msg = Request.CreateResponse(HttpStatusCode.Accepted);
            //}
            //catch
            //{
            //    msg = Request.CreateResponse(HttpStatusCode.NotAcceptable);
            //}

            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.NVarChar, Value = NTID });
            //Connection.myparameters.Add(new myParameters { ParameterName = "@Image", mytype = SqlDbType.NVarChar, Value = "http://192.168.1.250:82/ProfilePhotos/" + NTID + "/" + NTID + "_reimburse.png" });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Image", mytype = SqlDbType.NVarChar, Value = "https://ec2-54-255-237-199.ap-southeast-1.compute.amazonaws.com/ILM_WS_Live/ProfilePhotos/" + NTID + "/" + NTID + "_reimburse-" + datenow + ".png" });
            String ID = Connection.ExecuteScalar("sp_UpdateReimburse_Mobile");

            return msg;
        }

        [HttpPost]
        [Route("GetReimburse")]
        public ReimburseList ListReimbure()
        {
            ReimburseList ReimbursLists = new ReimburseList();

                Connection Connection = new Connection();
                List<ReimburseGet> ListReimburse = new List<ReimburseGet>();
                System.Data.DataTable DTREIMBURSE = new System.Data.DataTable();
                DTREIMBURSE = Connection.GetDataTable("sp_GetReimburseDetails_Mobile");
                //CultureInfo provider = CultureInfo.InvariantCulture;
                if (DTREIMBURSE.Rows.Count > 0)
                {
                    foreach (DataRow row in DTREIMBURSE.Rows)
                    {
                        ReimburseGet ReimburseGet = new ReimburseGet
                        {
                            Name = row["Name"].ToString(),
                            Required = row["Required"].ToString(),

                        };
                        ListReimburse.Add(ReimburseGet);
                    }
                ReimbursLists.ListReimburse = ListReimburse;
                ReimbursLists.myreturn = "Success";
                }
                else
                {
                ReimbursLists.myreturn = "No Data Available";
                }

                return ReimbursLists;
            }

        [HttpPost]
        [Route("GetSalaryInformation")]
        public SalaryList SalaryInformation(Master_User_Info_Personal_Confi_Info salary)
        {
            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@empId", mytype = SqlDbType.NVarChar, Value = salary.EmpID });

            DataRow dr;

            dr = Connection.GetSingleRow("sp_getCompleteProfile_Mobile");

            SalaryList salaryInfo = new SalaryList
            {
                Salary = dr["Salary"].ToString(),
                Days = dr["Days"].ToString(),
                DailyRate = dr["DailyRate"].ToString(),
                HourlyRate = dr["HourlyRate"].ToString(),
                PayrollScheme = dr["PayrollScheme"].ToString(),
                EcolaWageOrder = dr["EcolaWageOrder"].ToString(),
                EcolaRate = dr["EcolaRate"].ToString(),
                BankName = dr["BankName"].ToString(),
                BranchName = dr["BranchName"].ToString(),
                AccountNumber = dr["AccountNumber"].ToString()



            };
            return salaryInfo;

        }

        [HttpPost]
        [Route("GetCompanyProfile")]
        public CompanyProfileList CompanyProfileView()
        {
            CompanyProfileList CompanyProfileLists = new CompanyProfileList();

            Connection Connection = new Connection();
            List<GetCompanyProfileView> CompanyProfileView = new List<GetCompanyProfileView>();

            System.Data.DataTable DTCOMPANY = new System.Data.DataTable();
            DTCOMPANY = Connection.GetDataTable("sp_GetCompanyDetails_Mobile");
            //CultureInfo provider = CultureInfo.InvariantCulture;
            if (DTCOMPANY.Rows.Count > 0)
            {
                foreach (DataRow row in DTCOMPANY.Rows)
                {
                    GetCompanyProfileView CompanyProfileViewGet = new GetCompanyProfileView
                    {
                        BranchID = row["ID"].ToString(),
                        OfficeType = row["OfficeType"].ToString(),
                        OfficeName = row["OfficeName"].ToString(),
                        TradingName = row["TradingName"].ToString(),
                        CompanyRegistrationNo = row["CompanyRegistrationNo"].ToString(),
                        DateOfRegistration = row["DateOfRegistration"].ToString(),
                        AddUnitFloor = row["AddUnitFloor"].ToString(),
                        AddBldg = row["AddBldg"].ToString(),
                        AddStreet = row["AddStreet"].ToString(),
                        AddBarangay = row["AddBarangay"].ToString(),
                        AddMunicipality = row["AddMunicipality"].ToString(),
                        AddCity = row["AddCity"].ToString(),
                        AddRegion = row["AddRegion"].ToString(),
                        AddCountry = row["AddCountry"].ToString(),
                        AddZipCode = row["AddZipCode"].ToString(),
                        Sss = row["Sss"].ToString(),
                        Philhealth = row["Philhealth"].ToString(),
                        Tin = row["Tin"].ToString(),
                        RDOCode = row["RDOCode"].ToString(),
                        Pagibig = row["Pagibig"].ToString(),
                        PagibigCode = row["PagibigCode"].ToString()

                    };
                    CompanyProfileView.Add(CompanyProfileViewGet);
                }
                CompanyProfileLists.CompanyProfile = CompanyProfileView;
                CompanyProfileLists.myreturn = "Success";
            }
            else
            {
                CompanyProfileLists.myreturn = "No Data Available";
            }

            return CompanyProfileLists;
        }

        [HttpPost]
        [Route("GetCompanyProfileContact")]
        public CompanyProfileContactList CompanyProfileContactView(GetCompanyProfileView Com)
        {
            CompanyProfileContactList CompanyProfileContactLists = new CompanyProfileContactList();

            Connection Connection = new Connection();
            List<GetCompanyProfileContactView> CompanyProfileView = new List<GetCompanyProfileContactView>();
            System.Data.DataTable DTCOMPANY = new System.Data.DataTable();
            Connection.myparameters.Add(new myParameters { ParameterName = "@Codebranch", mytype = SqlDbType.NVarChar, Value = Com.BranchID });
            DTCOMPANY = Connection.GetDataTable("sp_GetCompanyContactDetails_Mobile");
            //CultureInfo provider = CultureInfo.InvariantCulture;
            if (DTCOMPANY.Rows.Count > 0)
            {
                foreach (DataRow row in DTCOMPANY.Rows)
                {
                    GetCompanyProfileContactView CompanyProfileContactViewGet = new GetCompanyProfileContactView
                    {
                        PhoneType = row["PhoneType"].ToString(),
                        ContactNumber = row["ContactNumber"].ToString(),

                    };
                    CompanyProfileView.Add(CompanyProfileContactViewGet);
                }
                CompanyProfileContactLists.GetCompanyProfileContactView = CompanyProfileView;
                CompanyProfileContactLists.myreturn = "Success";
            }
            else
            {
                CompanyProfileContactLists.myreturn = "No Data Available";
            }

            return CompanyProfileContactLists;
        }

        [HttpPost]
        [Route("GetCompanyProfileEmail")]
        public CompanyProfileEmailList CompanyProfileEmailView(GetCompanyProfileView Com)
        {
            CompanyProfileEmailList CompanyProfileEmailLists = new CompanyProfileEmailList();

            Connection Connection = new Connection();
            List<GetCompanyProfileEmailView> CompanyProfileView = new List<GetCompanyProfileEmailView>();
            System.Data.DataTable DTCOMPANY = new System.Data.DataTable();
            Connection.myparameters.Add(new myParameters { ParameterName = "@Codebranch", mytype = SqlDbType.NVarChar, Value = Com.BranchID });
            DTCOMPANY = Connection.GetDataTable("sp_GetCompanyEmailDetails_Mobile");
            //CultureInfo provider = CultureInfo.InvariantCulture;
            if (DTCOMPANY.Rows.Count > 0)
            {
                foreach (DataRow row in DTCOMPANY.Rows)
                {
                    GetCompanyProfileEmailView CompanyProfileEmailViewGet = new GetCompanyProfileEmailView
                    {
                        EmailType = row["EmailType"].ToString(),
                        Emailtxt = row["Emailtxt"].ToString(),

                    };
                    CompanyProfileView.Add(CompanyProfileEmailViewGet);
                }
                CompanyProfileEmailLists.GetCompanyProfileEmailView = CompanyProfileView;
                CompanyProfileEmailLists.myreturn = "Success";
            }
            else
            {
                CompanyProfileEmailLists.myreturn = "No Data Available";
            }

            return CompanyProfileEmailLists;
        }

        [HttpPost]
        [Route("InsertCarDueDetails")]
        public String InsertCarDueDetails(CarDueDetails CarDetails)
        {
            Connection Conn = new Connection();
            Conn.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = CarDetails.EMPID });
            Conn.myparameters.Add(new myParameters { ParameterName = "@CARBRANDID", mytype = SqlDbType.NVarChar, Value = CarDetails.CARBRANDID });
            Conn.myparameters.Add(new myParameters { ParameterName = "@CARMODELID", mytype = SqlDbType.NVarChar, Value = CarDetails.CARMODELID });
            Conn.myparameters.Add(new myParameters { ParameterName = "@CARVARIANTID", mytype = SqlDbType.NVarChar, Value = CarDetails.CARVARIANTID });
            Conn.myparameters.Add(new myParameters { ParameterName = "@CARMAKEYEAR", mytype = SqlDbType.NVarChar, Value = CarDetails.CARMAKEYEAR });
            Conn.myparameters.Add(new myParameters { ParameterName = "@CARPLATENO", mytype = SqlDbType.NVarChar, Value = CarDetails.CARPLATENO });
            Conn.myparameters.Add(new myParameters { ParameterName = "@MONTHPURCHASED", mytype = SqlDbType.NVarChar, Value = CarDetails.MONTHPURCHASED });
            Conn.myparameters.Add(new myParameters { ParameterName = "@YEARPURCHASED", mytype = SqlDbType.NVarChar, Value = CarDetails.YEARPURCHASED });
            Conn.myparameters.Add(new myParameters { ParameterName = "@ISINSURED", mytype = SqlDbType.NVarChar, Value = CarDetails.ISINSURED });
            Conn.myparameters.Add(new myParameters { ParameterName = "@INSURANCEEXPIRY", mytype = SqlDbType.NVarChar, Value = CarDetails.INSURANCEEXPIRY });
            Conn.myparameters.Add(new myParameters { ParameterName = "@INSURANCEAMOUNT", mytype = SqlDbType.NVarChar, Value = CarDetails.INSURANCEAMOUNT });
            Conn.myparameters.Add(new myParameters { ParameterName = "@ISREMIND", mytype = SqlDbType.NVarChar, Value = CarDetails.ISREMIND });
            Conn.myparameters.Add(new myParameters { ParameterName = "@ISCARLOAN", mytype = SqlDbType.NVarChar, Value = CarDetails.ISCARLOAN });
            Conn.myparameters.Add(new myParameters { ParameterName = "@PAYMENTDUEDATE", mytype = SqlDbType.NVarChar, Value = CarDetails.PAYMENTDUEDATE });
            Conn.myparameters.Add(new myParameters { ParameterName = "@MONTHLYAMORRIZATION", mytype = SqlDbType.NVarChar, Value = CarDetails.MONTHLYAMORRIZATION });
            Conn.myparameters.Add(new myParameters { ParameterName = "@MONTHLOANEND", mytype = SqlDbType.NVarChar, Value = CarDetails.MONTHLOANEND });
            Conn.myparameters.Add(new myParameters { ParameterName = "@YEARLOANEND", mytype = SqlDbType.NVarChar, Value = CarDetails.YEARLOANEND });
            String ID = Conn.ExecuteScalar("sp_InsertCarDueDetails_Mobile");
            return ID;
        }

        [HttpPost]
        [Route("InsertHomeTypeDetails")]
        public String InsertHomeTypeDetails(HomeTypeDetails HomeDetails)
        {
            Connection Conn = new Connection();
            Conn.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = HomeDetails.EMPID });
            Conn.myparameters.Add(new myParameters { ParameterName = "@HOMESTATUS", mytype = SqlDbType.NVarChar, Value = HomeDetails.HOMESTATUS });
            Conn.myparameters.Add(new myParameters { ParameterName = "@HOUSETYPEID", mytype = SqlDbType.NVarChar, Value = HomeDetails.HOUSETYPEID });
            Conn.myparameters.Add(new myParameters { ParameterName = "@PAYMENTTYPEID", mytype = SqlDbType.NVarChar, Value = HomeDetails.PAYMENTTYPEID });
            Conn.myparameters.Add(new myParameters { ParameterName = "@PAYMENTMONTH", mytype = SqlDbType.NVarChar, Value = HomeDetails.PAYMENTMONTH });
            Conn.myparameters.Add(new myParameters { ParameterName = "@PAYMENTDUEDATE", mytype = SqlDbType.NVarChar, Value = HomeDetails.PAYMENTDUEDATE });
            Conn.myparameters.Add(new myParameters { ParameterName = "@RENTAMOUNT", mytype = SqlDbType.NVarChar, Value = HomeDetails.RENTAMOUNT });
            Conn.myparameters.Add(new myParameters { ParameterName = "@ISHOAREMIND", mytype = SqlDbType.NVarChar, Value = HomeDetails.ISHOAREMIND });
            Conn.myparameters.Add(new myParameters { ParameterName = "@HOMETYPE", mytype = SqlDbType.NVarChar, Value = HomeDetails.HOMETYPE });
            Conn.myparameters.Add(new myParameters { ParameterName = "@HOAPAYMENTTYPEID", mytype = SqlDbType.NVarChar, Value = HomeDetails.HOAPAYMENTTYPEID });
            Conn.myparameters.Add(new myParameters { ParameterName = "@HOAPAYMENTMONTH", mytype = SqlDbType.NVarChar, Value = HomeDetails.HOAPAYMENTMONTH });
            Conn.myparameters.Add(new myParameters { ParameterName = "@HOAPAYMENTDUEDATE", mytype = SqlDbType.NVarChar, Value = HomeDetails.HOAPAYMENTDUEDATE });
            Conn.myparameters.Add(new myParameters { ParameterName = "@HOAFEE", mytype = SqlDbType.NVarChar, Value = HomeDetails.HOAFEE });
            Conn.myparameters.Add(new myParameters { ParameterName = "@BANKID", mytype = SqlDbType.NVarChar, Value = HomeDetails.BANKID });
            Conn.myparameters.Add(new myParameters { ParameterName = "@LOANTERMNUMBER", mytype = SqlDbType.NVarChar, Value = HomeDetails.LOANTERMNUMBER });
            Conn.myparameters.Add(new myParameters { ParameterName = "@LOANTERMID", mytype = SqlDbType.NVarChar, Value = HomeDetails.LOANTERMID });
            Conn.myparameters.Add(new myParameters { ParameterName = "@LOANSTARTMONTH", mytype = SqlDbType.NVarChar, Value = HomeDetails.LOANSTARTMONTH });
            Conn.myparameters.Add(new myParameters { ParameterName = "@LOANSTARTYEAR", mytype = SqlDbType.NVarChar, Value = HomeDetails.LOANSTARTYEAR });
            Conn.myparameters.Add(new myParameters { ParameterName = "@MORTPAYMENTTYPEID", mytype = SqlDbType.NVarChar, Value = HomeDetails.MORTPAYMENTTYPEID });
            Conn.myparameters.Add(new myParameters { ParameterName = "@MORTPAYMENTMONTH", mytype = SqlDbType.NVarChar, Value = HomeDetails.MORTPAYMENTMONTH });
            Conn.myparameters.Add(new myParameters { ParameterName = "@MORTPAYMENTDUEDATE", mytype = SqlDbType.NVarChar, Value = HomeDetails.MORTPAYMENTDUEDATE });
            Conn.myparameters.Add(new myParameters { ParameterName = "@MORTPAYMENTAMOUNT", mytype = SqlDbType.NVarChar, Value = HomeDetails.MORTPAYMENTAMOUNT });
            Conn.myparameters.Add(new myParameters { ParameterName = "@ISPAYINGFEE", mytype = SqlDbType.NVarChar, Value = HomeDetails.ISPAYINGFEE });
            Conn.myparameters.Add(new myParameters { ParameterName = "@SCHOOLNAME", mytype = SqlDbType.NVarChar, Value = HomeDetails.SCHOOLNAME });
            Conn.myparameters.Add(new myParameters { ParameterName = "@DOWNPAYMENTAMOUNT", mytype = SqlDbType.NVarChar, Value = HomeDetails.DOWNPAYMENTAMOUNT });
            Conn.myparameters.Add(new myParameters { ParameterName = "@1STQUARTERAMOUNT", mytype = SqlDbType.NVarChar, Value = HomeDetails.FSTQUARTERAMOUNT });
            Conn.myparameters.Add(new myParameters { ParameterName = "@1STQUARTERDUEDATE", mytype = SqlDbType.NVarChar, Value = HomeDetails.FSTQUARTERDUEDATE });
            Conn.myparameters.Add(new myParameters { ParameterName = "@2NDQUARTERAMOUNT", mytype = SqlDbType.NVarChar, Value = HomeDetails.SNDQUARTERAMOUNT });
            Conn.myparameters.Add(new myParameters { ParameterName = "@2NDQUARTERDUEDATE", mytype = SqlDbType.NVarChar, Value = HomeDetails.SNDQUARTERDUEDATE });
            Conn.myparameters.Add(new myParameters { ParameterName = "@3RDQUARTERAMOUNT", mytype = SqlDbType.NVarChar, Value = HomeDetails.TRDQUARTERAMOUNT });
            Conn.myparameters.Add(new myParameters { ParameterName = "@3RDQUARTERDUEDATE", mytype = SqlDbType.NVarChar, Value = HomeDetails.TRDQUARTERDUEDATE });
            Conn.myparameters.Add(new myParameters { ParameterName = "@4THQUARTERAMOUNT", mytype = SqlDbType.NVarChar, Value = HomeDetails.FTHQUARTERAMOUNT });
            Conn.myparameters.Add(new myParameters { ParameterName = "@4THQUARTERDUEDATE", mytype = SqlDbType.NVarChar, Value = HomeDetails.FTHQUARTERDUEDATE });
            String ID = Conn.ExecuteScalar("sp_InsertHomeTypeDetails_Mobile");
            return ID;
        }

        [HttpPost]
        [Route("InsertLoanAndServiceTypeDetails")]
        public String InserLoanAndServiceTypeDetails(LoanAndServiceTypeDetails LoanDetails)
        {
            Connection Conn = new Connection();
            Conn.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = LoanDetails.EMPID });
            Conn.myparameters.Add(new myParameters { ParameterName = "@ISOTHERLOAN", mytype = SqlDbType.NVarChar, Value = LoanDetails.ISOTHERLOAN });
            Conn.myparameters.Add(new myParameters { ParameterName = "@BANKID", mytype = SqlDbType.NVarChar, Value = LoanDetails.BANKID });
            Conn.myparameters.Add(new myParameters { ParameterName = "@LOANTERMNUMBER", mytype = SqlDbType.NVarChar, Value = LoanDetails.LOANTERMNUMBER });
            Conn.myparameters.Add(new myParameters { ParameterName = "@LOANTERMID", mytype = SqlDbType.NVarChar, Value = LoanDetails.LOANTERMID });
            Conn.myparameters.Add(new myParameters { ParameterName = "@LOANSTARTMONTH", mytype = SqlDbType.NVarChar, Value = LoanDetails.LOANSTARTMONTH });
            Conn.myparameters.Add(new myParameters { ParameterName = "@LOANSTARTYEAR", mytype = SqlDbType.NVarChar, Value = LoanDetails.LOANSTARTYEAR });
            Conn.myparameters.Add(new myParameters { ParameterName = "@PAYMENTTYPEID", mytype = SqlDbType.NVarChar, Value = LoanDetails.PAYMENTTYPEID });
            Conn.myparameters.Add(new myParameters { ParameterName = "@PAYMENTMONTH", mytype = SqlDbType.NVarChar, Value = LoanDetails.PAYMENTMONTH });
            Conn.myparameters.Add(new myParameters { ParameterName = "@PAYMENTDUEDATE", mytype = SqlDbType.NVarChar, Value = LoanDetails.PAYMENTDUEDATE });
            Conn.myparameters.Add(new myParameters { ParameterName = "@PAYMENTAMOUNT", mytype = SqlDbType.NVarChar, Value = LoanDetails.PAYMENTAMOUNT });
            Conn.myparameters.Add(new myParameters { ParameterName = "@SERVICETYPE", mytype = SqlDbType.NVarChar, Value = LoanDetails.SERVICETYPE });
            Conn.myparameters.Add(new myParameters { ParameterName = "@SPID", mytype = SqlDbType.NVarChar, Value = LoanDetails.SPID });
            Conn.myparameters.Add(new myParameters { ParameterName = "@ACCOUNTNO", mytype = SqlDbType.NVarChar, Value = LoanDetails.ACCOUNTNO });
            Conn.myparameters.Add(new myParameters { ParameterName = "@MONTHLYFEE", mytype = SqlDbType.NVarChar, Value = LoanDetails.MONTHLYFEE });
            Conn.myparameters.Add(new myParameters { ParameterName = "@DUEDATE", mytype = SqlDbType.NVarChar, Value = LoanDetails.DUEDATE });
            String ID = Conn.ExecuteScalar("sp_InsertLoanAndServiceTypeDetails_Mobile");
            return ID;
        }

        [HttpPost]
        [Route("GetCarDueDetails")]
        public CarDueDetails GetCarDueDetails(CarDueDetails cardue)
        {
            Connection Connection = new Connection();
            Connection.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = cardue.EMPID });
            DataRow dr;
            dr = Connection.GetSingleRow("sp_GetCarDueDetails_Mobile");
            CarDueDetails CarInfo = new CarDueDetails
            {
                EMPID = dr["empid"].ToString(),
                CARBRANDID = dr["carbrand"].ToString(),
                CARMODELID = dr["carmodel"].ToString(),
                CARVARIANTID = dr["carvariant"].ToString(),
                CARMAKEYEAR = dr["makeyear"].ToString(),
                CARPLATENO = dr["plateno"].ToString(),
                MONTHPURCHASED = dr["monthpurchased"].ToString(),
                YEARPURCHASED = dr["yearpurchased"].ToString(),
                ISINSURED = dr["isinsured"].ToString(),
                INSURANCEEXPIRY = dr["insuranceexpiry"].ToString(),
                INSURANCEAMOUNT = dr["insuranceamount"].ToString(),
                ISREMIND = dr["isremind"].ToString(),
                INSURANCEREMINDER = dr["insurancereminder"].ToString(),
                ISCARLOAN = dr["iscarloan"].ToString(),
                PAYMENTDUEDATE = dr["paymentduedate"].ToString(),
                MONTHLYAMORRIZATION = dr["monthlyamort"].ToString(),
                MONTHLOANEND = dr["monthloanend"].ToString(),
                YEARLOANEND = dr["yearloanend"].ToString()
            };
            return CarInfo;
        }

        [HttpPost]
        [Route("GetHomeTypeDetails")]
        public HomeTypeDetails GetHomeTypeDetails(HomeTypeDetails homeloan)
        {

            Connection Connection = new Connection();
            Connection.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = homeloan.EMPID });
            DataRow dr;
            dr = Connection.GetSingleRow("sp_GetHomeTypeDetails_Mobile");
            if (dr["type"].ToString() == "rented")
            {
                if (dr["ishoa"].ToString() == "yes")
                {
                    HomeTypeDetails HomeInfo = new HomeTypeDetails
                    {
                        HOMESTATUS = dr["type"].ToString(),
                        HOUSETYPEID = dr["house_type_id"].ToString(),
                        PAYMENTTYPEID = dr["payment_type_id"].ToString(),
                        PAYMENTMONTH = dr["payment_month"].ToString(),
                        PAYMENTDUEDATE = dr["payment_due_date"].ToString(),
                        RENTAMOUNT = dr["rent_amount"].ToString(),
                        ISHOAREMIND = dr["ishoa"].ToString(),
                        HOMETYPE = dr["home_type"].ToString(),
                        HOAPAYMENTTYPEID = dr["hoapaymenttype"].ToString(),
                        HOAPAYMENTMONTH = dr["hoapaymentmonth"].ToString(),
                        HOAPAYMENTDUEDATE = dr["hoapaymentduedate"].ToString(),
                        HOAFEE = dr["hoa_fee"].ToString()
                    };
                    return HomeInfo;
                }
                else
                {
                    HomeTypeDetails HomeInfo = new HomeTypeDetails
                    {
                        HOMESTATUS = dr["type"].ToString(),
                        ISHOAREMIND = dr["ishoa"].ToString(),
                        HOUSETYPEID = dr["house_type_id"].ToString(),
                        PAYMENTTYPEID = dr["payment_type_id"].ToString(),
                        PAYMENTMONTH = dr["payment_month"].ToString(),
                        PAYMENTDUEDATE = dr["payment_due_date"].ToString(),
                        RENTAMOUNT = dr["rent_amount"].ToString(),
                    };
                    return HomeInfo;
                }
            }
            else if (dr["type"].ToString() == "ownednotmort")
            {
                if (dr["ishoa"].ToString() == "yes")
                {
                    HomeTypeDetails HomeInfo = new HomeTypeDetails
                    {
                        ISHOAREMIND = dr["ishoa"].ToString(),
                        HOMETYPE = dr["home_type"].ToString(),
                        HOAPAYMENTTYPEID = dr["hoapaymenttype"].ToString(),
                        HOAPAYMENTMONTH = dr["hoapaymentmonth"].ToString(),
                        HOAPAYMENTDUEDATE = dr["hoapaymentduedate"].ToString(),
                        HOAFEE = dr["hoa_fee"].ToString()
                    };
                    return HomeInfo;
                }
                else
                {
                    HomeTypeDetails HomeInfo = new HomeTypeDetails
                    {
                        HOMESTATUS = dr["type"].ToString(),
                    };
                    return HomeInfo;
                }
            }
            else if (dr["type"].ToString() == "ownedmort")
            {
                if (dr["ishoa"].ToString() == "yes")
                {
                    HomeTypeDetails HomeInfo = new HomeTypeDetails
                    {
                        HOMESTATUS = dr["type"].ToString(),
                        BANKID = dr["bank_id"].ToString(),
                        LOANTERMNUMBER = dr["loan_term_number"].ToString(),
                        LOANTERMID = dr["loan_term_id"].ToString(),
                        LOANSTARTMONTH = dr["loan_start_month"].ToString(),
                        LOANSTARTYEAR = dr["loan_start_year"].ToString(),
                        MORTPAYMENTTYPEID = dr["payment_type_id"].ToString(),
                        MORTPAYMENTMONTH = dr["payment_month"].ToString(),
                        MORTPAYMENTDUEDATE = dr["payment_due_date"].ToString(),
                        MORTPAYMENTAMOUNT = dr["payment_amount"].ToString(),
                        ISHOAREMIND = dr["ishoa"].ToString(),
                        HOMETYPE = dr["home_type"].ToString(),
                        HOAPAYMENTTYPEID = dr["hoapaymenttype"].ToString(),
                        HOAPAYMENTMONTH = dr["hoapaymentmonth"].ToString(),
                        HOAPAYMENTDUEDATE = dr["hoapaymentduedate"].ToString(),
                        HOAFEE = dr["hoa_fee"].ToString()
                    };
                    return HomeInfo;
                }
                else
                {
                    HomeTypeDetails HomeInfo = new HomeTypeDetails
                    {
                        HOMESTATUS = dr["type"].ToString(),
                        ISHOAREMIND = dr["ishoa"].ToString(),
                        BANKID = dr["bank_id"].ToString(),
                        LOANTERMNUMBER = dr["loan_term_number"].ToString(),
                        LOANTERMID = dr["loan_term_id"].ToString(),
                        LOANSTARTMONTH = dr["loan_start_month"].ToString(),
                        LOANSTARTYEAR = dr["loan_start_year"].ToString(),
                        MORTPAYMENTTYPEID = dr["payment_type_id"].ToString(),
                        MORTPAYMENTMONTH = dr["payment_month"].ToString(),
                        MORTPAYMENTDUEDATE = dr["payment_due_date"].ToString(),
                        MORTPAYMENTAMOUNT = dr["payment_amount"].ToString(),
                    };
                    return HomeInfo;
                }
            }
            else if (dr["type"].ToString() == "withrel")
            {
                if (dr["ispaying"].ToString() == "yes")
                {
                    HomeTypeDetails HomeInfo = new HomeTypeDetails
                    {
                        HOMESTATUS = dr["type"].ToString(),
                        ISPAYINGFEE = dr["ispaying"].ToString(),
                        SCHOOLNAME = dr["school_name"].ToString(),
                        DOWNPAYMENTAMOUNT = dr["downpayment_amount"].ToString(),
                        FSTQUARTERAMOUNT = dr["1st_quarter_amount"].ToString(),
                        FSTQUARTERDUEDATE = dr["1st_quarter_due_date"].ToString(),
                        SNDQUARTERAMOUNT = dr["2nd_quarter_amount"].ToString(),
                        SNDQUARTERDUEDATE = dr["2nd_quarter_due_date"].ToString(),
                        TRDQUARTERAMOUNT = dr["3rd_quarter_amount"].ToString(),
                        TRDQUARTERDUEDATE = dr["3rd_quarter_due_date"].ToString(),
                        FTHQUARTERAMOUNT = dr["4th_quarter_amount"].ToString(),
                        FTHQUARTERDUEDATE = dr["4th_quarter_due_date"].ToString()
                    };
                    return HomeInfo;
                }
                else
                {
                    HomeTypeDetails HomeInfo = new HomeTypeDetails
                    {
                        HOMESTATUS = dr["type"].ToString(),
                        ISPAYINGFEE = dr["ispaying"].ToString()
                    };
                    return HomeInfo;
                }
            }
            else
            {
                HomeTypeDetails HomeInfo = new HomeTypeDetails
                {
                    HOMESTATUS = dr["type"].ToString(),
                };
                return HomeInfo;
            }
        }

        [HttpPost]
        [Route("GetLoanAndServiceTypeDetails")]
        public LoanAndServiceTypeDetails GetLoanAndServiceTypeDetails(LoanAndServiceTypeDetails loandetails)
        {
            Connection Connection = new Connection();
            Connection.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = loandetails.EMPID });
            DataRow dr;
            dr = Connection.GetSingleRow("sp_GetLoanAndServiceTypeDetails_Mobile");
            if (dr["hasloan"].ToString() == "yes")
            {
                if (dr["type"].ToString() == "landline")
                {
                    LoanAndServiceTypeDetails LoanInfo = new LoanAndServiceTypeDetails
                    {
                        ISOTHERLOAN = dr["hasloan"].ToString(),
                        BANKID = dr["bank_id"].ToString(),
                        LOANTERMNUMBER = dr["loan_term_number"].ToString(),
                        LOANTERMID = dr["loan_term_id"].ToString(),
                        LOANSTARTMONTH = dr["loan_start_month"].ToString(),
                        LOANSTARTYEAR = dr["loan_start_year"].ToString(),
                        PAYMENTTYPEID = dr["payment_type_id"].ToString(),
                        PAYMENTMONTH = dr["payment_month"].ToString(),
                        PAYMENTDUEDATE = dr["payment_due_date"].ToString(),
                        PAYMENTAMOUNT = dr["payment_amount"].ToString(),
                        SERVICETYPE = dr["type"].ToString(),
                        SPID = dr["SP_id"].ToString(),
                        ACCOUNTNO = dr["account_no"].ToString(),
                        MONTHLYFEE = dr["monthly_fee"].ToString(),
                        DUEDATE = dr["due_date"].ToString()
                    };
                    return LoanInfo;
                }
                else
                {
                    LoanAndServiceTypeDetails LoanInfo = new LoanAndServiceTypeDetails
                    {
                        SERVICETYPE = dr["type"].ToString()
                    };
                    return LoanInfo;
                }
            }
            else if (dr["hasloan"].ToString() == "no")
            {
                if (dr["type"].ToString() == "landline")
                {
                    LoanAndServiceTypeDetails LoanInfo = new LoanAndServiceTypeDetails
                    {
                        ISOTHERLOAN = dr["hasloan"].ToString(),
                        SERVICETYPE = dr["type"].ToString(),
                        SPID = dr["SP_id"].ToString(),
                        ACCOUNTNO = dr["account_no"].ToString(),
                        MONTHLYFEE = dr["monthly_fee"].ToString(),
                        DUEDATE = dr["due_date"].ToString()
                    };
                    return LoanInfo;
                }
                else
                {
                    LoanAndServiceTypeDetails LoanInfo = new LoanAndServiceTypeDetails
                    {
                        SERVICETYPE = dr["type"].ToString()
                    };
                    return LoanInfo;
                }
            }
            else
            {
                LoanAndServiceTypeDetails LoanInfo = new LoanAndServiceTypeDetails
                {
                    ISOTHERLOAN = dr["hasloan"].ToString()
                };
                return LoanInfo;
            }
        }


        //---------------------------------------------------------------Revised Change DB Start------------------------------------------------------------------------

        [HttpPost]
        [Route("DisplayCompleteProfilev2")]
        public Information DisplayCompleteProfilev2(Loginv2 Log)
        {

            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = Log.CN;
            SelfieRegistration2Controller.GetDB2(GDB);

            Information ListUs = new Information();

            List<Master_User_Info_Personal_Confi_Info> ListReturned = new List<Master_User_Info_Personal_Confi_Info>();
            System.Data.DataTable DTProfile = new System.Data.DataTable();

            Connection con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@empId", mytype = SqlDbType.NVarChar, Value = Log.LoginID });
            DTProfile = con.GetDataTable("sp_getCompleteProfile_Mobile");
            if (DTProfile.Rows.Count > 0)
            {
                Master_User_Info_Personal_Confi_Info MPC = new Master_User_Info_Personal_Confi_Info();

                MPC.SeriesID = DTProfile.Rows[0]["SeriesID"].ToString();
                MPC.EmpID = DTProfile.Rows[0]["EmpID"].ToString();
                MPC.First_Name = DTProfile.Rows[0]["First_Name"].ToString();
                MPC.Middle_Name = DTProfile.Rows[0]["Middle_Name"].ToString();
                MPC.Last_Name = DTProfile.Rows[0]["Last_Name"].ToString();
                MPC.NTID = DTProfile.Rows[0]["NTID"].ToString();
                MPC.EmpName = DTProfile.Rows[0]["EmpName"].ToString();
                MPC.EmpType = DTProfile.Rows[0]["EmpType"].ToString();
                MPC.EmpLevel = DTProfile.Rows[0]["EmpLevel"].ToString();
                MPC.EmpStatus = DTProfile.Rows[0]["EmpStatus"].ToString();
                MPC.UserType = DTProfile.Rows[0]["UserType"].ToString();
                MPC.Gender = DTProfile.Rows[0]["Gender"].ToString();
                MPC.DateJoined = DTProfile.Rows[0]["DateJoined"].ToString();
                MPC.ContractEndDate = DTProfile.Rows[0]["ContractEndDate"].ToString();
                MPC.LeaveBal = DTProfile.Rows[0]["LeaveBal"].ToString();
                MPC.LeaveBalCTO = DTProfile.Rows[0]["LeaveBalCTO"].ToString();
                MPC.Country = DTProfile.Rows[0]["Country"].ToString();
                MPC.JobDesc = DTProfile.Rows[0]["JobDesc"].ToString();
                MPC.DeptCode = DTProfile.Rows[0]["DeptCode"].ToString();
                MPC.DeptName = DTProfile.Rows[0]["DeptName"].ToString();
                MPC.MngrID = DTProfile.Rows[0]["MngrID"].ToString();
                MPC.MngrName = DTProfile.Rows[0]["MngrName"].ToString();
                MPC.MngrNTID = DTProfile.Rows[0]["MngrNTID"].ToString();
                MPC.BusinessUnit = DTProfile.Rows[0]["BusinessUnit"].ToString();
                MPC.BusinessUnitHead = DTProfile.Rows[0]["BusinessUnitHead"].ToString();
                MPC.BusinessSegment = DTProfile.Rows[0]["BusinessSegment"].ToString();
                MPC.AccessLevel = DTProfile.Rows[0]["AccessLevel"].ToString();
                MPC.DOB = DTProfile.Rows[0]["DOB"].ToString();
                MPC.Alias = DTProfile.Rows[0]["Alias"].ToString();
                MPC.ExtensionNumber = DTProfile.Rows[0]["ExtensionNumber"].ToString();
                MPC.Skill = DTProfile.Rows[0]["Skill"].ToString();
                MPC.Batch = DTProfile.Rows[0]["Batch"].ToString();
                MPC.Class = DTProfile.Rows[0]["Class"].ToString();
                MPC.Workstation = DTProfile.Rows[0]["Workstation"].ToString();
                MPC.ProductionStartDate = DTProfile.Rows[0]["ProductionStartDate"].ToString();
                MPC.ModifiedBy = DTProfile.Rows[0]["ModifiedBy"].ToString();
                MPC.DateModified = DTProfile.Rows[0]["DateModified"].ToString();
                MPC.EmpPayHrsCat = DTProfile.Rows[0]["EmpPayHrsCat"].ToString();
                MPC.Salary = DTProfile.Rows[0]["Salary"].ToString();
                MPC.DailyRate = DTProfile.Rows[0]["DailyRate"].ToString();
                MPC.EcolaWageOrder = DTProfile.Rows[0]["EcolaWageOrder"].ToString();
                MPC.EcolaRate = DTProfile.Rows[0]["EcolaRate"].ToString();
                MPC.Transportation = DTProfile.Rows[0]["Transportation"].ToString();
                MPC.Rice = DTProfile.Rows[0]["Rice"].ToString();
                MPC.Communication = DTProfile.Rows[0]["Communication"].ToString();
                MPC.EcolaRegion = DTProfile.Rows[0]["EcolaRegion"].ToString();
                MPC.Branch = DTProfile.Rows[0]["Branch"].ToString();
                MPC.Wallet = DTProfile.Rows[0]["Wallet"].ToString();
                MPC.FaceID = DTProfile.Rows[0]["FaceID"].ToString();
                MPC.DTransFrom = DTProfile.Rows[0]["DTransFrom"].ToString();
                MPC.DTransTo = DTProfile.Rows[0]["DTransTo"].ToString();
                MPC.LocFrom = DTProfile.Rows[0]["LocFrom"].ToString();
                MPC.LocTo = DTProfile.Rows[0]["LocTo"].ToString();
                MPC.CCChangeFrom = DTProfile.Rows[0]["CCChangeFrom"].ToString();
                MPC.CCChangeTo = DTProfile.Rows[0]["CCChangeTo"].ToString();
                MPC.Email = DTProfile.Rows[0]["Email"].ToString();
                MPC.WorkEmail = DTProfile.Rows[0]["WorkEmail"].ToString();
                MPC.Extension = DTProfile.Rows[0]["Extension"].ToString();
                MPC.JoinDate = DTProfile.Rows[0]["JoinDate"].ToString();
                MPC.ProdStart = DTProfile.Rows[0]["ProdStart"].ToString();
                MPC.Tenure = DTProfile.Rows[0]["Tenure"].ToString();
                MPC.TenureMnth = DTProfile.Rows[0]["TenureMnth"].ToString();
                MPC.Batch1 = DTProfile.Rows[0]["Batch"].ToString();
                MPC.Class1 = DTProfile.Rows[0]["Class"].ToString();
                MPC.Skill1 = DTProfile.Rows[0]["Skill"].ToString();
                MPC.JobDesc1 = DTProfile.Rows[0]["JobDesc"].ToString();
                MPC.Promotion = DTProfile.Rows[0]["Promotion"].ToString();
                MPC.Status = DTProfile.Rows[0]["Status"].ToString();
                MPC.Emp_Name = DTProfile.Rows[0]["Emp_Name"].ToString();
                MPC.approved = DTProfile.Rows[0]["approved"].ToString();
                MPC.Employee_Level = DTProfile.Rows[0]["Employee_Level"].ToString();
                MPC.Employee_Category = DTProfile.Rows[0]["Employee_Category"].ToString();
                MPC.BusinessSegment1 = DTProfile.Rows[0]["BusinessSegment"].ToString();
                MPC.IsApprove = DTProfile.Rows[0]["IsApprove"].ToString();
                MPC.ResignedDate = DTProfile.Rows[0]["ResignedDate"].ToString();
                MPC.HouseNumber = DTProfile.Rows[0]["HouseNumber"].ToString();
                MPC.StreetName = DTProfile.Rows[0]["StreetName"].ToString();
                MPC.Barangay = DTProfile.Rows[0]["Barangay"].ToString();
                MPC.Town = DTProfile.Rows[0]["Town"].ToString();
                MPC.City = DTProfile.Rows[0]["City"].ToString();
                MPC.Region = DTProfile.Rows[0]["Region"].ToString();
                MPC.ZipCode = DTProfile.Rows[0]["ZipCode"].ToString();
                MPC.BloodType = DTProfile.Rows[0]["BloodType"].ToString();
                MPC.Gender1 = DTProfile.Rows[0]["Gender"].ToString();
                MPC.DOB1 = DTProfile.Rows[0]["DOB"].ToString();
                MPC.Citizenship = DTProfile.Rows[0]["Citizenship"].ToString();
                MPC.NameofOrganization = DTProfile.Rows[0]["NameofOrganization"].ToString();
                MPC.MobileAreaCode = DTProfile.Rows[0]["MobileAreaCode"].ToString();
                MPC.MobileNumber = DTProfile.Rows[0]["MobileNumber"].ToString();
                MPC.HomeAreaCode = DTProfile.Rows[0]["HomeAreaCode"].ToString();
                MPC.HomeNumber = DTProfile.Rows[0]["HomeNumber"].ToString();
                // PersonalEmail = dr["PersonalEmail"].ToString(),
                MPC.EmrgName = DTProfile.Rows[0]["EmrgName"].ToString();
                MPC.EmrgNumber = DTProfile.Rows[0]["EmrgNumber"].ToString();
                MPC.EmrgRelationship = DTProfile.Rows[0]["EmrgRelationship"].ToString();
                MPC.OptionalEmail = DTProfile.Rows[0]["OptionalEmail"].ToString();
                // ContactNo = dr["ContactNo"].ToString(),
                MPC.Province = DTProfile.Rows[0]["Province"].ToString();
                MPC.TaxStatus = DTProfile.Rows[0]["TaxStatus"].ToString();

                MPC.SSS = DTProfile.Rows[0]["SSS"].ToString();
                MPC.TIN = DTProfile.Rows[0]["TIN"].ToString();
                MPC.PhilHealth = DTProfile.Rows[0]["PhilHealth"].ToString();
                MPC.Pagibig = DTProfile.Rows[0]["Pagibig"].ToString();
                MPC.DriversLicense = DTProfile.Rows[0]["DriversLicense"].ToString();
                MPC.Passport = DTProfile.Rows[0]["Passport"].ToString();
                MPC.Unified = DTProfile.Rows[0]["Unified"].ToString();
                MPC.Postal = DTProfile.Rows[0]["Postal"].ToString();
                MPC.BirthCertificate = DTProfile.Rows[0]["BirthCertificate"].ToString();
                MPC.SssImage = DTProfile.Rows[0]["SssImage"].ToString();
                MPC.TinImage = DTProfile.Rows[0]["TinImage"].ToString();
                MPC.PhilImage = DTProfile.Rows[0]["PhilImage"].ToString();
                MPC.PagImage = DTProfile.Rows[0]["PagImage"].ToString();
                MPC.DriverImage = DTProfile.Rows[0]["DriverImage"].ToString();
                MPC.PassportImage = DTProfile.Rows[0]["PassportImage"].ToString();
                MPC.UmidImage = DTProfile.Rows[0]["UmidImage"].ToString();
                MPC.PostalImage = DTProfile.Rows[0]["PostalImage"].ToString();
                MPC.BirthImage = DTProfile.Rows[0]["BirthImage"].ToString();
                MPC.DriversExpiry = DTProfile.Rows[0]["DriversExpiry"].ToString();
                MPC.PassportExpiry = DTProfile.Rows[0]["PassportExpiry"].ToString();
                MPC.PostalExpiry = DTProfile.Rows[0]["PostalExpiry"].ToString();
                MPC.SAGSD = DTProfile.Rows[0]["SAGSD"].ToString();
                MPC.SAGSDExpiry = DTProfile.Rows[0]["SAGSDExpiry"].ToString();
                MPC.NTC = DTProfile.Rows[0]["NTC"].ToString();
                MPC.NTCExpiry = DTProfile.Rows[0]["NTCExpiry"].ToString();
                MPC.BrgyClearance = DTProfile.Rows[0]["BrgyClearance"].ToString();
                MPC.BrgyClearanceExpiry = DTProfile.Rows[0]["BrgyClearanceExpiry"].ToString();
                MPC.PNPClearance = DTProfile.Rows[0]["PNPClearance"].ToString();
                MPC.PNPClearanceExpiry = DTProfile.Rows[0]["PNPClearanceExpiry"].ToString();
                MPC.NBIClearance = DTProfile.Rows[0]["NBIClearance"].ToString();
                MPC.NBIClearanceExpiry = DTProfile.Rows[0]["NBIClearanceExpiry"].ToString();
                MPC.NueroEvaluation = DTProfile.Rows[0]["NueroEvaluation"].ToString();
                MPC.NeuroEvaluationExpiry = DTProfile.Rows[0]["NeuroEvaluationExpiry"].ToString();
                MPC.DrugTest = DTProfile.Rows[0]["DrugTest"].ToString();
                MPC.DrugTestExpiry = DTProfile.Rows[0]["DrugTestExpiry"].ToString();
                MPC.PlaceofBirth = DTProfile.Rows[0]["POB"].ToString();


                System.Data.DataTable DTContactNo = new System.Data.DataTable();
                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = Log.LoginID });
                DTContactNo = con.GetDataTable("sp_GetPersonalContact");


                List<ContactNo> Clist = new List<ContactNo>();
                for (int ii = 0; ii < DTContactNo.Rows.Count; ii++)
                {
                    ContactNo Contact = new ContactNo();
                    Contact.Contact = DTContactNo.Rows[ii]["Number"].ToString();
                    Contact.ContactType = DTContactNo.Rows[ii]["NumberType"].ToString();

                    Clist.Add(Contact);
                }
                MPC.ContactNo = Clist;


                System.Data.DataTable DTEmail = new System.Data.DataTable();
                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = Log.LoginID });
                DTEmail = con.GetDataTable("sp_GetPersonalEmail");
                List<PersonalEmail> EMaillist = new List<PersonalEmail>();
                for (int iii = 0; iii < DTEmail.Rows.Count; iii++)
                {
                    PersonalEmail Email = new PersonalEmail();
                    Email.Email = DTEmail.Rows[iii]["Email"].ToString();
                    Email.EmailType = DTEmail.Rows[iii]["EmailType"].ToString();

                    EMaillist.Add(Email);
                }
                MPC.PersonalEmail = EMaillist;

                ListReturned.Add(MPC);
            }
            ListUs.CompleteProfile = ListReturned;
            ListUs.myreturn = "Success";
            return ListUs;
        }

        [HttpPost]
        [Route("DisplayCompleteProfilev3")]
        public Informationv2 DisplayCompleteProfilev3(Loginv2 Log)
        {

            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = Log.CN;
            SelfieRegistration2Controller.GetDB2(GDB);

            Informationv2 ListUs = new Informationv2();

            List<Master_User_Info_Personal_Confi_Infov3> ListReturned = new List<Master_User_Info_Personal_Confi_Infov3>();
            System.Data.DataTable DTProfile = new System.Data.DataTable();

            Connection con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@empId", mytype = SqlDbType.NVarChar, Value = Log.LoginID });
            DTProfile = con.GetDataTable("sp_getCompleteProfile_Mobilev2");
            if (DTProfile.Rows.Count > 0)
            {
                Master_User_Info_Personal_Confi_Infov3 MPC = new Master_User_Info_Personal_Confi_Infov3();

                MPC.SeriesID = DTProfile.Rows[0]["SeriesID"].ToString();
                MPC.EmpID = DTProfile.Rows[0]["EmpID"].ToString();
                MPC.First_Name = DTProfile.Rows[0]["First_Name"].ToString();
                MPC.Middle_Name = DTProfile.Rows[0]["Middle_Name"].ToString();
                MPC.Last_Name = DTProfile.Rows[0]["Last_Name"].ToString();
                MPC.NTID = DTProfile.Rows[0]["NTID"].ToString();
                MPC.EmpName = DTProfile.Rows[0]["EmpName"].ToString();
                MPC.EmpType = DTProfile.Rows[0]["EmpType"].ToString();
                MPC.EmpLevel = DTProfile.Rows[0]["EmpLevel"].ToString();
                MPC.EmpStatus = DTProfile.Rows[0]["EmpStatus"].ToString();
                MPC.UserType = DTProfile.Rows[0]["UserType"].ToString();
                MPC.Gender = DTProfile.Rows[0]["Gender"].ToString();
                MPC.DateJoined = DTProfile.Rows[0]["DateJoined"].ToString();
                MPC.ContractEndDate = DTProfile.Rows[0]["ContractEndDate"].ToString();
                MPC.LeaveBal = DTProfile.Rows[0]["LeaveBal"].ToString();
                MPC.LeaveBalCTO = DTProfile.Rows[0]["LeaveBalCTO"].ToString();
                MPC.Country = DTProfile.Rows[0]["Country"].ToString();
                MPC.JobDesc = DTProfile.Rows[0]["JobDesc"].ToString();
                MPC.DeptCode = DTProfile.Rows[0]["DeptCode"].ToString();
                MPC.DeptName = DTProfile.Rows[0]["DeptName"].ToString();
                MPC.MngrID = DTProfile.Rows[0]["MngrID"].ToString();
                MPC.MngrName = DTProfile.Rows[0]["MngrName"].ToString();
                MPC.MngrNTID = DTProfile.Rows[0]["MngrNTID"].ToString();
                MPC.BusinessUnit = DTProfile.Rows[0]["BusinessUnit"].ToString();
                MPC.BusinessUnitHead = DTProfile.Rows[0]["BusinessUnitHead"].ToString();
                MPC.BusinessSegment = DTProfile.Rows[0]["BusinessSegment"].ToString();
                MPC.AccessLevel = DTProfile.Rows[0]["AccessLevel"].ToString();
                MPC.DOB = DTProfile.Rows[0]["DOB"].ToString();
                MPC.Alias = DTProfile.Rows[0]["Alias"].ToString();
                MPC.ExtensionNumber = DTProfile.Rows[0]["ExtensionNumber"].ToString();
                MPC.Skill = DTProfile.Rows[0]["Skill"].ToString();
                MPC.Batch = DTProfile.Rows[0]["Batch"].ToString();
                MPC.Class = DTProfile.Rows[0]["Class"].ToString();
                MPC.Workstation = DTProfile.Rows[0]["Workstation"].ToString();
                MPC.ProductionStartDate = DTProfile.Rows[0]["ProductionStartDate"].ToString();
                MPC.ModifiedBy = DTProfile.Rows[0]["ModifiedBy"].ToString();
                MPC.DateModified = DTProfile.Rows[0]["DateModified"].ToString();
                MPC.EmpPayHrsCat = DTProfile.Rows[0]["EmpPayHrsCat"].ToString();
                MPC.Salary = DTProfile.Rows[0]["Salary"].ToString();
                MPC.DailyRate = DTProfile.Rows[0]["DailyRate"].ToString();
                MPC.EcolaWageOrder = DTProfile.Rows[0]["EcolaWageOrder"].ToString();
                MPC.EcolaRate = DTProfile.Rows[0]["EcolaRate"].ToString();
                MPC.Transportation = DTProfile.Rows[0]["Transportation"].ToString();
                MPC.Rice = DTProfile.Rows[0]["Rice"].ToString();
                MPC.Communication = DTProfile.Rows[0]["Communication"].ToString();
                MPC.EcolaRegion = DTProfile.Rows[0]["EcolaRegion"].ToString();
                MPC.Branch = DTProfile.Rows[0]["Branch"].ToString();
                MPC.Wallet = DTProfile.Rows[0]["Wallet"].ToString();
                MPC.FaceID = DTProfile.Rows[0]["FaceID"].ToString();
                MPC.DTransFrom = DTProfile.Rows[0]["DTransFrom"].ToString();
                MPC.DTransTo = DTProfile.Rows[0]["DTransTo"].ToString();
                MPC.LocFrom = DTProfile.Rows[0]["LocFrom"].ToString();
                MPC.LocTo = DTProfile.Rows[0]["LocTo"].ToString();
                MPC.CCChangeFrom = DTProfile.Rows[0]["CCChangeFrom"].ToString();
                MPC.CCChangeTo = DTProfile.Rows[0]["CCChangeTo"].ToString();
                MPC.Email = DTProfile.Rows[0]["Email"].ToString();
                MPC.WorkEmail = DTProfile.Rows[0]["WorkEmail"].ToString();
                MPC.Extension = DTProfile.Rows[0]["Extension"].ToString();
                MPC.JoinDate = DTProfile.Rows[0]["JoinDate"].ToString();
                MPC.ProdStart = DTProfile.Rows[0]["ProdStart"].ToString();
                MPC.Tenure = DTProfile.Rows[0]["Tenure"].ToString();
                MPC.TenureMnth = DTProfile.Rows[0]["TenureMnth"].ToString();
                MPC.Batch1 = DTProfile.Rows[0]["Batch"].ToString();
                MPC.Class1 = DTProfile.Rows[0]["Class"].ToString();
                MPC.Skill1 = DTProfile.Rows[0]["Skill"].ToString();
                MPC.JobDesc1 = DTProfile.Rows[0]["JobDesc"].ToString();
                MPC.Promotion = DTProfile.Rows[0]["Promotion"].ToString();
                MPC.Status = DTProfile.Rows[0]["Status"].ToString();
                MPC.Emp_Name = DTProfile.Rows[0]["Emp_Name"].ToString();
                MPC.approved = DTProfile.Rows[0]["approved"].ToString();
                MPC.Employee_Level = DTProfile.Rows[0]["Employee_Level"].ToString();
                MPC.Employee_Category = DTProfile.Rows[0]["Employee_Category"].ToString();
                MPC.BusinessSegment1 = DTProfile.Rows[0]["BusinessSegment"].ToString();
                MPC.IsApprove = DTProfile.Rows[0]["IsApprove"].ToString();
                MPC.ResignedDate = DTProfile.Rows[0]["ResignedDate"].ToString();
                MPC.HouseNumber = DTProfile.Rows[0]["HouseNumber"].ToString();
                MPC.StreetName = DTProfile.Rows[0]["StreetName"].ToString();
                MPC.Barangay = DTProfile.Rows[0]["Barangay"].ToString();
                MPC.Town = DTProfile.Rows[0]["Town"].ToString();
                MPC.City = DTProfile.Rows[0]["City"].ToString();
                MPC.Region = DTProfile.Rows[0]["Region"].ToString();
                MPC.ZipCode = DTProfile.Rows[0]["ZipCode"].ToString();
                MPC.BloodType = DTProfile.Rows[0]["BloodType"].ToString();
                MPC.Gender1 = DTProfile.Rows[0]["Gender"].ToString();
                MPC.DOB1 = DTProfile.Rows[0]["DOB"].ToString();
                MPC.Citizenship = DTProfile.Rows[0]["Citizenship"].ToString();
                MPC.NameofOrganization = DTProfile.Rows[0]["NameofOrganization"].ToString();
                MPC.MobileAreaCode = DTProfile.Rows[0]["MobileAreaCode"].ToString();
                MPC.MobileNumber = DTProfile.Rows[0]["MobileNumber"].ToString();
                MPC.HomeAreaCode = DTProfile.Rows[0]["HomeAreaCode"].ToString();
                MPC.HomeNumber = DTProfile.Rows[0]["HomeNumber"].ToString();
                // PersonalEmail = dr["PersonalEmail"].ToString(),
                MPC.EmrgName = DTProfile.Rows[0]["EmrgName"].ToString();
                MPC.EmrgNumber = DTProfile.Rows[0]["EmrgNumber"].ToString();
                MPC.EmrgRelationship = DTProfile.Rows[0]["EmrgRelationship"].ToString();
                MPC.OptionalEmail = DTProfile.Rows[0]["OptionalEmail"].ToString();
                // ContactNo = dr["ContactNo"].ToString(),
                MPC.Province = DTProfile.Rows[0]["Province"].ToString();
                MPC.TaxStatus = DTProfile.Rows[0]["TaxStatus"].ToString();

                MPC.SSS = DTProfile.Rows[0]["SSS"].ToString();
                MPC.TIN = DTProfile.Rows[0]["TIN"].ToString();
                MPC.PhilHealth = DTProfile.Rows[0]["PhilHealth"].ToString();
                MPC.Pagibig = DTProfile.Rows[0]["Pagibig"].ToString();
                MPC.DriversLicense = DTProfile.Rows[0]["DriversLicense"].ToString();
                MPC.Passport = DTProfile.Rows[0]["Passport"].ToString();
                MPC.Unified = DTProfile.Rows[0]["Unified"].ToString();
                MPC.Postal = DTProfile.Rows[0]["Postal"].ToString();
                MPC.BirthCertificate = DTProfile.Rows[0]["BirthCertificate"].ToString();
                MPC.SssImage = DTProfile.Rows[0]["SssImage"].ToString();
                MPC.TinImage = DTProfile.Rows[0]["TinImage"].ToString();
                MPC.PhilImage = DTProfile.Rows[0]["PhilImage"].ToString();
                MPC.PagImage = DTProfile.Rows[0]["PagImage"].ToString();
                MPC.DriverImage = DTProfile.Rows[0]["DriverImage"].ToString();
                MPC.PassportImage = DTProfile.Rows[0]["PassportImage"].ToString();
                MPC.UmidImage = DTProfile.Rows[0]["UmidImage"].ToString();
                MPC.PostalImage = DTProfile.Rows[0]["PostalImage"].ToString();
                MPC.BirthImage = DTProfile.Rows[0]["BirthImage"].ToString();
                MPC.DriversExpiry = DTProfile.Rows[0]["DriversExpiry"].ToString();
                MPC.PassportExpiry = DTProfile.Rows[0]["PassportExpiry"].ToString();
                MPC.PostalExpiry = DTProfile.Rows[0]["PostalExpiry"].ToString();
                MPC.SAGSD = DTProfile.Rows[0]["SAGSD"].ToString();
                MPC.SAGSDExpiry = DTProfile.Rows[0]["SAGSDExpiry"].ToString();
                MPC.NTC = DTProfile.Rows[0]["NTC"].ToString();
                MPC.NTCExpiry = DTProfile.Rows[0]["NTCExpiry"].ToString();
                MPC.BrgyClearance = DTProfile.Rows[0]["BrgyClearance"].ToString();
                MPC.BrgyClearanceExpiry = DTProfile.Rows[0]["BrgyClearanceExpiry"].ToString();
                MPC.PNPClearance = DTProfile.Rows[0]["PNPClearance"].ToString();
                MPC.PNPClearanceExpiry = DTProfile.Rows[0]["PNPClearanceExpiry"].ToString();
                MPC.NBIClearance = DTProfile.Rows[0]["NBIClearance"].ToString();
                MPC.NBIClearanceExpiry = DTProfile.Rows[0]["NBIClearanceExpiry"].ToString();
                MPC.NueroEvaluation = DTProfile.Rows[0]["NueroEvaluation"].ToString();
                MPC.NeuroEvaluationExpiry = DTProfile.Rows[0]["NeuroEvaluationExpiry"].ToString();
                MPC.DrugTest = DTProfile.Rows[0]["DrugTest"].ToString();
                MPC.DrugTestExpiry = DTProfile.Rows[0]["DrugTestExpiry"].ToString();
                MPC.PlaceofBirth = DTProfile.Rows[0]["POB"].ToString();
                MPC.Religion = DTProfile.Rows[0]["Religion"].ToString();
                MPC.Height = DTProfile.Rows[0]["Height"].ToString();
                MPC.Weight = DTProfile.Rows[0]["Weight"].ToString();
                MPC.MothersName = DTProfile.Rows[0]["MotherName"].ToString();
                MPC.FathersName = DTProfile.Rows[0]["FatherName"].ToString();

                System.Data.DataTable DTContactNo = new System.Data.DataTable();
                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = Log.LoginID });
                DTContactNo = con.GetDataTable("sp_GetPersonalContact");


                List<ContactNo> Clist = new List<ContactNo>();
                for (int ii = 0; ii < DTContactNo.Rows.Count; ii++)
                {
                    ContactNo Contact = new ContactNo();
                    Contact.Contact = DTContactNo.Rows[ii]["Number"].ToString();
                    Contact.ContactType = DTContactNo.Rows[ii]["NumberType"].ToString();

                    Clist.Add(Contact);
                }
                MPC.ContactNo = Clist;


                System.Data.DataTable DTEmail = new System.Data.DataTable();
                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = Log.LoginID });
                DTEmail = con.GetDataTable("sp_GetPersonalEmail");
                List<PersonalEmail> EMaillist = new List<PersonalEmail>();
                for (int iii = 0; iii < DTEmail.Rows.Count; iii++)
                {
                    PersonalEmail Email = new PersonalEmail();
                    Email.Email = DTEmail.Rows[iii]["Email"].ToString();
                    Email.EmailType = DTEmail.Rows[iii]["EmailType"].ToString();

                    EMaillist.Add(Email);
                }
                MPC.PersonalEmail = EMaillist;

                ListReturned.Add(MPC);
            }
            ListUs.CompleteProfile = ListReturned;
            ListUs.myreturn = "Success";
            return ListUs;
        }

        [Route("GetImagesv2")]
        public String Base64Imagev2(UserProfile_Imagesv2 ProfileImages)
        // public HttpResponseMessage DisplaySignature(string id)
        {
            try
            {
                string file_path;

                file_path = Path.Combine(System.Web.HttpRuntime.AppDomainAppPath + "ProfilePhotos\\" + ProfileImages.EmpID + "\\", ProfileImages.EmpID + "_" + ProfileImages.ImageType + ".png");

                MemoryStream ms = new MemoryStream(File.ReadAllBytes(file_path));
                HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);
                byte[] imageBytes = ms.ToArray();
                string base64String = Convert.ToBase64String(imageBytes);

                GetDatabase GDB = new GetDatabase();
                GDB.ClientName = ProfileImages.CN;
                SelfieRegistration2Controller.GetDB2(GDB);


                Connection Connection = new Connection();

                Connection.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = ProfileImages.EmpID });
                Connection.myparameters.Add(new myParameters { ParameterName = "@ImgType", mytype = SqlDbType.NVarChar, Value = ProfileImages.ImageType });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Base", mytype = SqlDbType.NVarChar, Value = base64String });

                String ID = Connection.ExecuteScalar("sp_Insert_Base64_Mobile");

                return base64String;
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }


        [HttpPost]
        [Route("InsertUserProfilev2")]
        public string UserProfilev2(UserProfilev2 uProfile)
        {
                        
            try
            {
                string dataDir = HostingEnvironment.MapPath("~/Files/");
                FileStream stream = new FileStream(dataDir + "Basic.xls", FileMode.Open);
                //ExcelWriter writer = new ExcelWriter(stream);


                string filename;
                filename = uProfile.NTID + "_" + DateTime.Now.Month.ToString().PadLeft(2, '0') + DateTime.Now.Day.ToString().PadLeft(2, '0') + DateTime.Now.Year.ToString().Substring(2, 2) + "_CRT-UPDATEOCT2016.xls";
                using (var fileStream = new FileStream(dataDir + filename, FileMode.Create, FileAccess.Write))
                {
                    stream.CopyTo(fileStream);
                }
                stream.Close();

                ExcelWorkbookFormat workbookFormat = ExcelWorkbookFormat.Xls_2003;
                string testDocFile = dataDir + "Basic.xls";
                ExcelWorkbook workbook = new ExcelWorkbook(testDocFile);
                ExcelWorksheet firstWorksheet = workbook.Worksheets[1];
                firstWorksheet["B11"].Text = uProfile.LASTNAME;
                firstWorksheet["C11"].Text = uProfile.FIRSTNAME;
                firstWorksheet["D11"].Text = uProfile.MIDDLENAME;
                firstWorksheet["E11"].Text = uProfile.SUFFIX;
                firstWorksheet["F11"].Text = uProfile.CARDNAME;
                firstWorksheet["G11"].Text = Convert.ToDateTime(uProfile.BIRTHDAY).ToString("MM-dd-yyyy");
                firstWorksheet["H11"].Text = uProfile.PLACEOFBIRTH;
                firstWorksheet["I11"].Text = uProfile.NATIONALITY;
                firstWorksheet["J11"].Text = uProfile.NATCOUNTRY;
                firstWorksheet["K11"].Text = uProfile.GENDER;
                firstWorksheet["L11"].Text = uProfile.MOBILE;
                firstWorksheet["M11"].Text = uProfile.TELPHONE;
                firstWorksheet["N11"].Text = uProfile.PRESHOUSENUMBER;
                firstWorksheet["O11"].Text = uProfile.PRESHOUSEADDRESS;
                firstWorksheet["P11"].Text = uProfile.PRESCOUNTRY;
                firstWorksheet["Q11"].Text = uProfile.PRESREGION;
                firstWorksheet["R11"].Text = uProfile.PRESPROVINCE;
                firstWorksheet["S11"].Text = uProfile.PRESCITY;
                firstWorksheet["T11"].Text = uProfile.PRESZIPCODE;
                firstWorksheet["U11"].Text = uProfile.PERMHOUSENUMBER;
                firstWorksheet["V11"].Text = uProfile.PERMHOUSEADDRESS;
                firstWorksheet["W11"].Text = uProfile.PERMCOUNTRY;
                firstWorksheet["X11"].Text = uProfile.PERMREGION;
                firstWorksheet["Y11"].Text = uProfile.PERMPROVINCE;
                firstWorksheet["Z11"].Text = uProfile.PERMCITY;
                firstWorksheet["AA11"].Text = uProfile.PERMZIPCODE;
                firstWorksheet["AB11"].Text = uProfile.EMAILADD;
                firstWorksheet["AC11"].Text = uProfile.EMPLOYERNAME;
                firstWorksheet["AD11"].Text = uProfile.TIN;
                firstWorksheet["AE11"].Text = uProfile.CIVILSTATUS;
                firstWorksheet["AF11"].Text = uProfile.MOTHLASTNAME;
                firstWorksheet["AG11"].Text = uProfile.MOTHFIRSTNAME;
                firstWorksheet["AH11"].Text = uProfile.MOTHMIDDLENAME;
                firstWorksheet["AI11"].Text = uProfile.MOTHSUFFIX;
                firstWorksheet["AJ11"].Text = uProfile.NUMBEROFPADS;
                firstWorksheet["AK11"].Text = uProfile.OTHERDETAILS;

                GetDatabase GDB = new GetDatabase();
                GDB.ClientName = uProfile.CN;
                SelfieRegistration2Controller.GetDB2(GDB);

                Connection Connection = new Connection();

                Connection.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = uProfile.NTID });
                Connection.myparameters.Add(new myParameters { ParameterName = "@CardNum", mytype = SqlDbType.NVarChar, Value = uProfile.CARDNAME });
                Connection.myparameters.Add(new myParameters { ParameterName = "@DOB", mytype = SqlDbType.NVarChar, Value = Convert.ToDateTime(uProfile.BIRTHDAY).ToString("yyyy-MM-dd") });
                Connection.myparameters.Add(new myParameters { ParameterName = "@POB", mytype = SqlDbType.NVarChar, Value = uProfile.PLACEOFBIRTH });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Natioanality", mytype = SqlDbType.NVarChar, Value = uProfile.NATIONALITY });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Country", mytype = SqlDbType.NVarChar, Value = uProfile.NATCOUNTRY });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Gender", mytype = SqlDbType.NVarChar, Value = uProfile.GENDER });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Mobile", mytype = SqlDbType.NVarChar, Value = uProfile.MOBILE });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Landline", mytype = SqlDbType.NVarChar, Value = uProfile.TELPHONE });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Per_Zipcode", mytype = SqlDbType.NVarChar, Value = uProfile.PERMZIPCODE });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Per_HouseNum", mytype = SqlDbType.NVarChar, Value = uProfile.PERMHOUSENUMBER });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Per_St_VIllage_Sub", mytype = SqlDbType.NVarChar, Value = uProfile.PERMHOUSEADDRESS });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Per_Brgy", mytype = SqlDbType.NVarChar, Value = uProfile.PERMBRGY });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Per_Muni", mytype = SqlDbType.NVarChar, Value = uProfile.PERMUNI });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Per_City", mytype = SqlDbType.NVarChar, Value = uProfile.PERMCITY });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Per_Region", mytype = SqlDbType.NVarChar, Value = uProfile.PERMREGION });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Per_Country", mytype = SqlDbType.NVarChar, Value = uProfile.PERMCOUNTRY });


                Connection.myparameters.Add(new myParameters { ParameterName = "@Pre_Zipcode", mytype = SqlDbType.NVarChar, Value = uProfile.PRESZIPCODE });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Pre_HouseNum", mytype = SqlDbType.NVarChar, Value = uProfile.PRESHOUSENUMBER });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Pre_St_VIllage_Sub", mytype = SqlDbType.NVarChar, Value = uProfile.PRESHOUSEADDRESS });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Pre_Brgy", mytype = SqlDbType.NVarChar, Value = uProfile.PRESBRGY });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Pre_Muni", mytype = SqlDbType.NVarChar, Value = uProfile.PRESMUNI });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Pre_City", mytype = SqlDbType.NVarChar, Value = uProfile.PRESCITY });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Pre_Region", mytype = SqlDbType.NVarChar, Value = uProfile.PRESREGION });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Pre_Country", mytype = SqlDbType.NVarChar, Value = uProfile.PRESCOUNTRY });


                Connection.myparameters.Add(new myParameters { ParameterName = "@email", mytype = SqlDbType.NVarChar, Value = uProfile.EMAILADD });
                Connection.myparameters.Add(new myParameters { ParameterName = "@EmployerName", mytype = SqlDbType.NVarChar, Value = uProfile.EMPLOYERNAME });
                Connection.myparameters.Add(new myParameters { ParameterName = "@TIN", mytype = SqlDbType.NVarChar, Value = uProfile.TIN });
                Connection.myparameters.Add(new myParameters { ParameterName = "@CivilStatus", mytype = SqlDbType.NVarChar, Value = uProfile.CIVILSTATUS });
                Connection.myparameters.Add(new myParameters { ParameterName = "@MothersMaidenLName", mytype = SqlDbType.NVarChar, Value = uProfile.MOTHSUFFIX });
                Connection.myparameters.Add(new myParameters { ParameterName = "@MothersFname", mytype = SqlDbType.NVarChar, Value = uProfile.MOTHFIRSTNAME });
                Connection.myparameters.Add(new myParameters { ParameterName = "@MothersMName", mytype = SqlDbType.NVarChar, Value = uProfile.MOTHLASTNAME });
                Connection.myparameters.Add(new myParameters { ParameterName = "@OtherDetails", mytype = SqlDbType.NVarChar, Value = uProfile.OTHERDETAILS });

                Connection.myparameters.Add(new myParameters { ParameterName = "@COMPANYNAME", mytype = SqlDbType.NVarChar, Value = uProfile.COMPANYNAME });
                Connection.myparameters.Add(new myParameters { ParameterName = "@SPECIALIZATION", mytype = SqlDbType.NVarChar, Value = uProfile.SPECIALIZATION });
                Connection.myparameters.Add(new myParameters { ParameterName = "@ROLEPOSITIONTITLE", mytype = SqlDbType.NVarChar, Value = uProfile.ROLEPOSITIONTITLE });
                Connection.myparameters.Add(new myParameters { ParameterName = "@POSITIONLEVEL", mytype = SqlDbType.NVarChar, Value = uProfile.POSITIONLEVEL });
                Connection.myparameters.Add(new myParameters { ParameterName = "@JOINEDFROM", mytype = SqlDbType.NVarChar, Value = uProfile.JOINEDFROM });
                Connection.myparameters.Add(new myParameters { ParameterName = "@JOINEDTO", mytype = SqlDbType.NVarChar, Value = uProfile.JOINEDTO });
                Connection.myparameters.Add(new myParameters { ParameterName = "@INDUSTRY", mytype = SqlDbType.NVarChar, Value = uProfile.INDUSTRY });
                Connection.myparameters.Add(new myParameters { ParameterName = "@COUNTRY2", mytype = SqlDbType.NVarChar, Value = uProfile.COUNTRY });
                Connection.myparameters.Add(new myParameters { ParameterName = "@EXPERIENCE", mytype = SqlDbType.NVarChar, Value = uProfile.EXPERIENCE });
                Connection.myparameters.Add(new myParameters { ParameterName = "@PositionTitle", mytype = SqlDbType.NVarChar, Value = uProfile.PositionTitle });
                Connection.myparameters.Add(new myParameters { ParameterName = "@MaritalStatus", mytype = SqlDbType.NVarChar, Value = uProfile.MaritalStatus });
                Connection.myparameters.Add(new myParameters { ParameterName = "@MarriedDate", mytype = SqlDbType.NVarChar, Value = Convert.ToDateTime(uProfile.Marrieddate).ToString("yyyy-MM-dd") });

                String ID = Connection.ExecuteScalar("sp_Insert_EmployeeProfile_FromMobile");

                System.Web.HttpResponse httpResponse = System.Web.HttpContext.Current.Response;

                httpResponse.Clear();
                httpResponse.ContentType = "Application/x-msexcel";
                httpResponse.AddHeader("Content-Disposition", String.Format("attachment; filename={0}", filename));
                string newDIR = HostingEnvironment.MapPath("~/sFTP/");
                workbook.Save(newDIR + filename);
                return "Success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }


        [HttpPost]
        [Route("InsertEmployeeDependentsv2")]
        public String InsertEmployeeDependentsv2(DependentHistoryv2 DependentDetails)

        {
            try
            {
                GetDatabase GDB = new GetDatabase();
                GDB.ClientName = DependentDetails.CN;
                SelfieRegistration2Controller.GetDB2(GDB);

                Connection Connection = new Connection();

                Connection.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.NVarChar, Value = DependentDetails.EmpID });
                Connection.myparameters.Add(new myParameters { ParameterName = "@RELATION", mytype = SqlDbType.NVarChar, Value = DependentDetails.Relationship });
                Connection.myparameters.Add(new myParameters { ParameterName = "@FIRST", mytype = SqlDbType.NVarChar, Value = DependentDetails.First });
                Connection.myparameters.Add(new myParameters { ParameterName = "@MID", mytype = SqlDbType.NVarChar, Value = DependentDetails.Mid });
                Connection.myparameters.Add(new myParameters { ParameterName = "@LAST", mytype = SqlDbType.NVarChar, Value = DependentDetails.Lastname });
                Connection.myparameters.Add(new myParameters { ParameterName = "@BIRTH", mytype = SqlDbType.NVarChar, Value = DependentDetails.Birth != "" ? Convert.ToDateTime(DependentDetails.Birth).ToString("yyyy -MM-dd") : DependentDetails.Birth });
                Connection.myparameters.Add(new myParameters { ParameterName = "@COMMENT", mytype = SqlDbType.NVarChar, Value = DependentDetails.Comments });
                Connection.myparameters.Add(new myParameters { ParameterName = "@FILE", mytype = SqlDbType.NVarChar, Value = DependentDetails.DocFilename });
                Connection.myparameters.Add(new myParameters { ParameterName = "@EXTENSION", mytype = SqlDbType.NVarChar, Value = DependentDetails.Extension });

                String ID = Connection.ExecuteScalar("sp_InsertDependentV2");

                return ID;
            }
            catch (Exception e)
            {
                return e.ToString();
            }
            
        }

        [HttpPost]
        [Route("GetCompanyProfilev2")]
        public CompanyProfileList CompanyProfileViewv2(Loginv2 log)
        {
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = log.CN;
            SelfieRegistration2Controller.GetDB2(GDB);

            CompanyProfileList CompanyProfileLists = new CompanyProfileList();

            Connection Connection = new Connection();
            List<GetCompanyProfileView> CompanyProfileView = new List<GetCompanyProfileView>();

            System.Data.DataTable DTCOMPANY = new System.Data.DataTable();
            DTCOMPANY = Connection.GetDataTable("sp_GetCompanyDetails_Mobile");
            //CultureInfo provider = CultureInfo.InvariantCulture;
            if (DTCOMPANY.Rows.Count > 0)
            {
                foreach (DataRow row in DTCOMPANY.Rows)
                {
                    GetCompanyProfileView CompanyProfileViewGet = new GetCompanyProfileView
                    {
                        BranchID = row["ID"].ToString(),
                        OfficeType = row["OfficeType"].ToString(),
                        OfficeName = row["OfficeName"].ToString(),
                        TradingName = row["TradingName"].ToString(),
                        CompanyRegistrationNo = row["CompanyRegistrationNo"].ToString(),
                        DateOfRegistration = row["DateOfRegistration"].ToString(),
                        AddUnitFloor = row["AddUnitFloor"].ToString(),
                        AddBldg = row["AddBldg"].ToString(),
                        AddStreet = row["AddStreet"].ToString(),
                        AddBarangay = row["AddBarangay"].ToString(),
                        AddMunicipality = row["AddMunicipality"].ToString(),
                        AddCity = row["AddCity"].ToString(),
                        AddRegion = row["AddRegion"].ToString(),
                        AddCountry = row["AddCountry"].ToString(),
                        AddZipCode = row["AddZipCode"].ToString(),
                        Sss = row["Sss"].ToString(),
                        Philhealth = row["Philhealth"].ToString(),
                        Tin = row["Tin"].ToString(),
                        RDOCode = row["RDOCode"].ToString(),
                        Pagibig = row["Pagibig"].ToString(),
                        PagibigCode = row["PagibigCode"].ToString()

                    };
                    CompanyProfileView.Add(CompanyProfileViewGet);
                }
                CompanyProfileLists.CompanyProfile = CompanyProfileView;
                CompanyProfileLists.myreturn = "Success";
            }
            else
            {
                CompanyProfileLists.myreturn = "No Data Available";
            }

            return CompanyProfileLists;
        }

        [HttpPost]
        [Route("DisplayDependentsv2")]
        public DependentList DisplayDependentsv2(Loginv2 userToSave)
        {
            DependentList GetDependentLists = new DependentList();
            try
            {
                GetDatabase GDB = new GetDatabase();
                GDB.ClientName = userToSave.CN;
                SelfieRegistration2Controller.GetDB2(GDB);

                Connection Connection = new Connection();
                List<DependentHistory> ListDependents = new List<DependentHistory>();
                DataSet ds = new DataSet();
                System.Data.DataTable dtLEAVES = new System.Data.DataTable();

                Connection.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = userToSave.LoginID });

                ds = Connection.GetDataset("sp_ShowDependentList");
                dtLEAVES = ds.Tables[0];
                if (dtLEAVES.Rows.Count > 0)
                {
                    foreach (DataRow row in dtLEAVES.Rows)
                    {
                        DependentHistory AvailableLeaves = new DependentHistory
                        {

                            FullName = row["FullName"].ToString(),
                            DOB = row["DateOfBirth"].ToString(),
                            Relationship = row["Relation"].ToString()


                        };
                        ListDependents.Add(AvailableLeaves);
                    }
                    GetDependentLists.DependentListing = ListDependents;
                    GetDependentLists.myreturn = "Success";
                }
                else
                {
                    GetDependentLists.myreturn = "No Data Available";
                }
            }
            catch (Exception ex)
            {
                GetDependentLists.myreturn = ex.Message;
            }
            return GetDependentLists;
        }

        [HttpPost]
        [Route("DisplayEmployerHistoryv2")]
        public EmployerList SDisplayEmploymentHistoryv2(Loginv2 userToSave)
        {
            EmployerList GetEmployerLists = new EmployerList();
            try
            {
                GetDatabase GDB = new GetDatabase();
                GDB.ClientName = userToSave.CN;
                SelfieRegistration2Controller.GetDB2(GDB);

                Connection Connection = new Connection();
                List<EmployerHistory> ListEmployers = new List<EmployerHistory>();
                DataSet ds = new DataSet();
                System.Data.DataTable dtLEAVES = new System.Data.DataTable();

                Connection.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = userToSave.LoginID });

                ds = Connection.GetDataset("sp_ShowEmployerHistory_Mobile");
                dtLEAVES = ds.Tables[0];
                if (dtLEAVES.Rows.Count > 0)
                {
                    foreach (DataRow row in dtLEAVES.Rows)
                    {
                        EmployerHistory AvailableLeaves = new EmployerHistory
                        {


                            Company_Name = row["CompanyName"].ToString(),
                            Work_From = row["joinedFrom"].ToString(),
                            Work_To = row["joinedTo"].ToString()


                        };
                        ListEmployers.Add(AvailableLeaves);
                    }
                    GetEmployerLists.EmployerListing = ListEmployers;
                    GetEmployerLists.myreturn = "Success";
                }
                else
                {
                    GetEmployerLists.myreturn = "No Data Available";
                }
            }
            catch (Exception ex)
            {
                GetEmployerLists.myreturn = ex.Message;
            }
            return GetEmployerLists;
        }

        [HttpPost]
        [Route("GetSalaryInformationv2")]
        public SalaryList SalaryInformationv2(Master_User_Info_Personal_Confi_Infov2 salary)
        {
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = salary.CN;
            SelfieRegistration2Controller.GetDB2(GDB);

            Connection Connection = new Connection();
            Connection.myparameters.Add(new myParameters { ParameterName = "@empId", mytype = SqlDbType.NVarChar, Value = salary.EmpID });
            DataRow dr;
            dr = Connection.GetSingleRow("sp_getCompleteProfile_Mobile");
            SalaryList salaryInfo = new SalaryList
            {
                Salary = dr["Salary"].ToString(),
                Days = dr["Days"].ToString(),
                DailyRate = dr["DailyRate"].ToString(),
                HourlyRate = dr["HourlyRate"].ToString(),
                PayrollScheme = dr["PayrollScheme"].ToString(),
                EcolaWageOrder = dr["EcolaWageOrder"].ToString(),
                EcolaRate = dr["EcolaRate"].ToString(),
                BankName = dr["BankName"].ToString(),
                BranchName = dr["BranchName"].ToString(),
                AccountNumber = dr["AccountNumber"].ToString()
            };
            return salaryInfo;
        }


        [HttpPost]
        [Route("UpdateEmployeePersonalInfov2")]
        public String UpdateEmployeeMasterv2(UpdateEmployeePersonalInfov2 EmployeeMaster)
        {
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = EmployeeMaster.CN;
            SelfieRegistration2Controller.GetDB2(GDB);

            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.EmpID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@HouseNumber", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.HouseNumber });
            Connection.myparameters.Add(new myParameters { ParameterName = "@StreetName", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.StreetName });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Barangay", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Barangay });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Town", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Town });
            Connection.myparameters.Add(new myParameters { ParameterName = "@City", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.City });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Region", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Region });
            Connection.myparameters.Add(new myParameters { ParameterName = "@ZipCode", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.ZipCode });
            Connection.myparameters.Add(new myParameters { ParameterName = "@BloodType", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.BloodType });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Gender", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Gender });
            Connection.myparameters.Add(new myParameters { ParameterName = "@DOB", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.DOB });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Citizenship", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Citizenship });
            Connection.myparameters.Add(new myParameters { ParameterName = "@NameofOrganization", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.NameofOrganization });
            Connection.myparameters.Add(new myParameters { ParameterName = "@MobileAreaCode", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.MobileAreaCode });
            Connection.myparameters.Add(new myParameters { ParameterName = "@MobileNumber", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.MobileNumber });
            Connection.myparameters.Add(new myParameters { ParameterName = "@HomeAreaCode", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.HomeAreaCode });
            Connection.myparameters.Add(new myParameters { ParameterName = "@HomeNumber", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.HomeNumber });
            Connection.myparameters.Add(new myParameters { ParameterName = "@PersonalEmail", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.PersonalEmail });
            Connection.myparameters.Add(new myParameters { ParameterName = "@EmrgName", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.EmrgName });
            Connection.myparameters.Add(new myParameters { ParameterName = "@EmrgNumber", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.EmrgNumber });
            Connection.myparameters.Add(new myParameters { ParameterName = "@EmrgRelationship", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.EmrgRelationship });
            Connection.myparameters.Add(new myParameters { ParameterName = "@OptionalEmail", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.OptionalEmail });
            Connection.myparameters.Add(new myParameters { ParameterName = "@ContactNo", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.ContactNo });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Province", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Province });
            Connection.myparameters.Add(new myParameters { ParameterName = "@EmrgFname", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.EmrgFname });
            Connection.myparameters.Add(new myParameters { ParameterName = "@EmrgMname", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.EmrgMname });
            Connection.myparameters.Add(new myParameters { ParameterName = "@EmrgLname", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.EmrgLname });
            String ID = Connection.ExecuteScalar("sp_UpdatePersonalInfo_Mobile");
            return ID;
        }

        [HttpPost]
        [Route("UpdateEmployeePersonalInfov3")]
        public String UpdateEmployeeMasterv3(UpdateEmployeePersonalInfov2 EmployeeMaster)
        {
            try
            {
                GetDatabase GDB = new GetDatabase();
                GDB.ClientName = EmployeeMaster.CN;
                SelfieRegistration2Controller.GetDB2(GDB);

                Connection Connection = new Connection();

                Connection.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.EmpID });
                Connection.myparameters.Add(new myParameters { ParameterName = "@HouseNumber", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.HouseNumber });
                Connection.myparameters.Add(new myParameters { ParameterName = "@StreetName", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.StreetName });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Barangay", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Barangay });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Town", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Town });
                Connection.myparameters.Add(new myParameters { ParameterName = "@City", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.City });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Region", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Region });
                Connection.myparameters.Add(new myParameters { ParameterName = "@ZipCode", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.ZipCode });
                Connection.myparameters.Add(new myParameters { ParameterName = "@BloodType", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.BloodType });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Gender", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Gender });
                Connection.myparameters.Add(new myParameters { ParameterName = "@DOB", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.DOB });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Citizenship", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Citizenship });
                Connection.myparameters.Add(new myParameters { ParameterName = "@NameofOrganization", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.NameofOrganization });
                Connection.myparameters.Add(new myParameters { ParameterName = "@MobileAreaCode", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.MobileAreaCode });
                Connection.myparameters.Add(new myParameters { ParameterName = "@MobileNumber", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.MobileNumber });
                Connection.myparameters.Add(new myParameters { ParameterName = "@HomeAreaCode", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.HomeAreaCode });
                Connection.myparameters.Add(new myParameters { ParameterName = "@HomeNumber", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.HomeNumber });
                Connection.myparameters.Add(new myParameters { ParameterName = "@PersonalEmail", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.PersonalEmail });
                Connection.myparameters.Add(new myParameters { ParameterName = "@EmrgName", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.EmrgName });
                Connection.myparameters.Add(new myParameters { ParameterName = "@EmrgNumber", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.EmrgNumber });
                Connection.myparameters.Add(new myParameters { ParameterName = "@EmrgRelationship", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.EmrgRelationship });
                Connection.myparameters.Add(new myParameters { ParameterName = "@OptionalEmail", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.OptionalEmail });
                Connection.myparameters.Add(new myParameters { ParameterName = "@ContactNo", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.ContactNo });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Province", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Province });
                Connection.myparameters.Add(new myParameters { ParameterName = "@EmrgFname", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.EmrgFname });
                Connection.myparameters.Add(new myParameters { ParameterName = "@EmrgMname", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.EmrgMname });
                Connection.myparameters.Add(new myParameters { ParameterName = "@EmrgLname", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.EmrgLname });
                String ID = Connection.ExecuteScalar("sp_HRMS_UpdatePersonalInfo");
                return ID;
            }
            catch (Exception e)
            {
                return e.ToString();
            }
            
        }

        [HttpPost]
        [Route("UpdateEmployeeInfov2")]
        public String UpdateEmployeeInfov2(UpdateEmployeeInfov2 EmployeeMaster)
        {
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = EmployeeMaster.CN;
            SelfieRegistration2Controller.GetDB2(GDB);

            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.EmpID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@First_Name", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.First_Name });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Middle_Name", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Middle_Name });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Last_Name", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Last_Name });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Ntlogin", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Ntlogin });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Alias", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Alias });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Deptcode", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Deptcode });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Deptname", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Deptname });
            Connection.myparameters.Add(new myParameters { ParameterName = "@BUnit", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.BUnit });
            Connection.myparameters.Add(new myParameters { ParameterName = "@BUnithead", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.BUnithead });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Workstation", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Workstation });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Supervisor", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Supervisor });
            Connection.myparameters.Add(new myParameters { ParameterName = "@DTransFrom", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.DTransFrom });
            Connection.myparameters.Add(new myParameters { ParameterName = "@DTransTo", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.DTransTo });
            Connection.myparameters.Add(new myParameters { ParameterName = "@LocFrom", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.LocFrom });
            Connection.myparameters.Add(new myParameters { ParameterName = "@LocTo", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.LocTo });
            Connection.myparameters.Add(new myParameters { ParameterName = "@CCChangeFrom", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.CCChangeFrom });
            Connection.myparameters.Add(new myParameters { ParameterName = "@CCChangeTo", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.CCChangeTo });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Email", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Email });
            Connection.myparameters.Add(new myParameters { ParameterName = "@WorkEmail", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.WorkEmail });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Extension", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Extension });
            Connection.myparameters.Add(new myParameters { ParameterName = "@JoinDate", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.JoinDate });
            Connection.myparameters.Add(new myParameters { ParameterName = "@ProdStart", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.ProdStart });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Tenure", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Tenure });
            Connection.myparameters.Add(new myParameters { ParameterName = "@TenureMnth", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.TenureMnth });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Batch", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Batch });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Class", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Class });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Skill", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Skill });
            Connection.myparameters.Add(new myParameters { ParameterName = "@JobDesc", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.JobDesc });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Promotion", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Promotion });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Status", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Status });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Emp_Name", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Emp_Name });
            Connection.myparameters.Add(new myParameters { ParameterName = "@approved", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.approved });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Employee_Level", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Employee_Level });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Employee_Category", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Employee_Category });
            Connection.myparameters.Add(new myParameters { ParameterName = "@BusinessSegment", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.BusinessSegment });
            Connection.myparameters.Add(new myParameters { ParameterName = "@IsApprove", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.IsApprove });
            Connection.myparameters.Add(new myParameters { ParameterName = "@ResignedDate", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.ResignedDate });
            String ID = Connection.ExecuteScalar("sp_UpdateEmployeeInfo_Mobile");
            return ID;
        }

        [HttpPost]
        [Route("UpdateEmployeeInfov4")]
        public String UpdateEmployeeInfov4(UpdateEmployeeInfov2 EmployeeMaster)
        {
            try
            {
                GetDatabase GDB = new GetDatabase();
                GDB.ClientName = EmployeeMaster.CN;
                SelfieRegistration2Controller.GetDB2(GDB);

                Connection Connection = new Connection();

                Connection.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.EmpID });
                Connection.myparameters.Add(new myParameters { ParameterName = "@First_Name", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.First_Name });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Middle_Name", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Middle_Name });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Last_Name", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Last_Name });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Ntlogin", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Ntlogin });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Alias", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Alias });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Deptcode", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Deptcode });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Deptname", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Deptname });
                Connection.myparameters.Add(new myParameters { ParameterName = "@BUnit", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.BUnit });
                Connection.myparameters.Add(new myParameters { ParameterName = "@BUnithead", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.BUnithead });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Workstation", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Workstation });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Supervisor", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Supervisor });
                Connection.myparameters.Add(new myParameters { ParameterName = "@DTransFrom", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.DTransFrom });
                Connection.myparameters.Add(new myParameters { ParameterName = "@DTransTo", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.DTransTo });
                Connection.myparameters.Add(new myParameters { ParameterName = "@LocFrom", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.LocFrom });
                Connection.myparameters.Add(new myParameters { ParameterName = "@LocTo", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.LocTo });
                Connection.myparameters.Add(new myParameters { ParameterName = "@CCChangeFrom", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.CCChangeFrom });
                Connection.myparameters.Add(new myParameters { ParameterName = "@CCChangeTo", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.CCChangeTo });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Email", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Email });
                Connection.myparameters.Add(new myParameters { ParameterName = "@WorkEmail", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.WorkEmail });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Extension", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Extension });
                Connection.myparameters.Add(new myParameters { ParameterName = "@JoinDate", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.JoinDate });
                Connection.myparameters.Add(new myParameters { ParameterName = "@ProdStart", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.ProdStart });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Tenure", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Tenure });
                Connection.myparameters.Add(new myParameters { ParameterName = "@TenureMnth", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.TenureMnth });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Batch", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Batch });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Class", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Class });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Skill", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Skill });
                Connection.myparameters.Add(new myParameters { ParameterName = "@JobDesc", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.JobDesc });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Promotion", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Promotion });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Status", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Status });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Emp_Name", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Emp_Name });
                Connection.myparameters.Add(new myParameters { ParameterName = "@approved", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.approved });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Employee_Level", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Employee_Level });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Employee_Category", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Employee_Category });
                Connection.myparameters.Add(new myParameters { ParameterName = "@BusinessSegment", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.BusinessSegment });
                Connection.myparameters.Add(new myParameters { ParameterName = "@IsApprove", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.IsApprove });
                Connection.myparameters.Add(new myParameters { ParameterName = "@ResignedDate", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.ResignedDate });

                Connection.myparameters.Add(new myParameters { ParameterName = "@Ref1FNAME", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Ref1FNAME });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Ref1LNAME", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Ref1LNAME });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Ref1Contact", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Ref1Contact });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Ref2FNAME", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Ref2FNAME });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Ref2LNAME", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Ref2LNAME });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Ref2Contact", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Ref2Contact });


                Connection.ExecuteNonQuery("sp_HRMS_UpdateEmployeeInfov2");
                return "Success"; 
            }
            catch (Exception e)
            {
                return e.ToString();
            }
            
        }

        [HttpPost]
        [Route("InsertNewEmployee")]
        public String InsertNewEmployee(NewEmployee EmployeeMaster)
        {
            try
            {
                GetDatabase GDB = new GetDatabase();
                GDB.ClientName = EmployeeMaster.CN;
                SelfieRegistration2Controller.GetDB2(GDB);

                Connection Connection = new Connection();
 
                Connection.myparameters.Add(new myParameters { ParameterName = "@CLIENTNAME", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.CN });
                Connection.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.EmpID });
                Connection.myparameters.Add(new myParameters { ParameterName = "@FIRST_NAME", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.First_Name });
                Connection.myparameters.Add(new myParameters { ParameterName = "@MIDDLE_NAME", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Middle_Name });
                Connection.myparameters.Add(new myParameters { ParameterName = "@LAST_NAME", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Last_Name });
                Connection.myparameters.Add(new myParameters { ParameterName = "@NAME", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Name });
                Connection.myparameters.Add(new myParameters { ParameterName = "@EMPTYPE", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.EmpType });
                Connection.myparameters.Add(new myParameters { ParameterName = "@EMPLEVEL", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.EmpLevel });
                Connection.myparameters.Add(new myParameters { ParameterName = "@NTID", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.NTID });
                Connection.myparameters.Add(new myParameters { ParameterName = "@USERTYPE", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.UserType });
                Connection.myparameters.Add(new myParameters { ParameterName = "@LEAVEBAL", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.LeaveBal });
                Connection.myparameters.Add(new myParameters { ParameterName = "@LEAVEBALCTO", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.LeaveBalCTO });
                Connection.myparameters.Add(new myParameters { ParameterName = "@ALIAS", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Alias });
                Connection.myparameters.Add(new myParameters { ParameterName = "@BADGE", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Badge });
                Connection.myparameters.Add(new myParameters { ParameterName = "@EXTENSIONNUMBER", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Extensionnumber });
                Connection.myparameters.Add(new myParameters { ParameterName = "@DEPTCODE", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.DeptCode });
                Connection.myparameters.Add(new myParameters { ParameterName = "@DEPTNAME", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.DeptName });
                Connection.myparameters.Add(new myParameters { ParameterName = "@MNGRID", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.MngrID });
                Connection.myparameters.Add(new myParameters { ParameterName = "@MNGRNAME", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.MngrName });
                Connection.myparameters.Add(new myParameters { ParameterName = "@MNGRNTID", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.MngrNTID });
                Connection.myparameters.Add(new myParameters { ParameterName = "@BUSINESSUNIT", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.BusinessUnit });
                Connection.myparameters.Add(new myParameters { ParameterName = "@BUSINESSUNITHEAD", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.BusinessUnitHead });
                Connection.myparameters.Add(new myParameters { ParameterName = "@BUSINESSSEGMENT", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.BusinessSegment });
                Connection.myparameters.Add(new myParameters { ParameterName = "@WORKSTATION", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.WorkStation });
                Connection.myparameters.Add(new myParameters { ParameterName = "@DATEJOINED", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.DateJoined });
                Connection.myparameters.Add(new myParameters { ParameterName = "@PRODUCTIONSTARTDATE", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.ProductionStartDate });
                Connection.myparameters.Add(new myParameters { ParameterName = "@BATCH", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Batch });
                Connection.myparameters.Add(new myParameters { ParameterName = "@CLASS", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Class });
                Connection.myparameters.Add(new myParameters { ParameterName = "@SKILL", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Skill });
                Connection.myparameters.Add(new myParameters { ParameterName = "@JOBDESC", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.JobDesc });
                Connection.myparameters.Add(new myParameters { ParameterName = "@EMPSTATUS", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.EmpStatus });
                Connection.myparameters.Add(new myParameters { ParameterName = "@EMPPAYHRSCAT", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.EmpPayHrsCat });
                Connection.myparameters.Add(new myParameters { ParameterName = "@BRANCH", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Branch });
                Connection.myparameters.Add(new myParameters { ParameterName = "@WALLET", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Wallet });
                Connection.myparameters.Add(new myParameters { ParameterName = "@TENURE", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Tenure });
                Connection.myparameters.Add(new myParameters { ParameterName = "@TENUREMONTH", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.TenureMonth });
                Connection.myparameters.Add(new myParameters { ParameterName = "@INDUSTRY", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Industry });
                Connection.myparameters.Add(new myParameters { ParameterName = "@SALARY", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Salary });
                Connection.myparameters.Add(new myParameters { ParameterName = "@WORKINGDAYS", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.WorkingDays });
                Connection.myparameters.Add(new myParameters { ParameterName = "@DAILYRATE", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.DailyRate });
                Connection.myparameters.Add(new myParameters { ParameterName = "@HOURLYRATE", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.HourlyRate });
                Connection.myparameters.Add(new myParameters { ParameterName = "@PAYROLLSCHEME", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.PayrollScheme });
                Connection.myparameters.Add(new myParameters { ParameterName = "@PREFFIX", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Preffix });
                Connection.myparameters.Add(new myParameters { ParameterName = "@SUFFIX", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Suffix });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Country", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Country });
                Connection.myparameters.Add(new myParameters { ParameterName = "@ContractStatus", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.ContractStatus });
                Connection.myparameters.Add(new myParameters { ParameterName = "@ContractNo", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.ContractNo });
                Connection.myparameters.Add(new myParameters { ParameterName = "@ContractEnd", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.ContractEnd });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Promotion", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Promotion });
                Connection.myparameters.Add(new myParameters { ParameterName = "@ROLE", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Role });

                Connection.ExecuteNonQuery("sp_InsertHRMSEmployeeMaster");
                return "Success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }

        }

        [HttpPost]
        [Route("UpdateEmployeeInfov3")]
        public String UpdateEmployeeInfov3(UpdateEmployeeInfov2 EmployeeMaster)
        {
            try
            {
                GetDatabase GDB = new GetDatabase();
                GDB.ClientName = EmployeeMaster.CN;
                SelfieRegistration2Controller.GetDB2(GDB);

                Connection Connection = new Connection();

                Connection.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.EmpID });
                Connection.myparameters.Add(new myParameters { ParameterName = "@First_Name", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.First_Name });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Middle_Name", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Middle_Name });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Last_Name", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Last_Name });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Ntlogin", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Ntlogin });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Alias", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Alias });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Deptcode", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Deptcode });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Deptname", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Deptname });
                Connection.myparameters.Add(new myParameters { ParameterName = "@BUnit", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.BUnit });
                Connection.myparameters.Add(new myParameters { ParameterName = "@BUnithead", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.BUnithead });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Workstation", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Workstation });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Supervisor", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Supervisor });
                Connection.myparameters.Add(new myParameters { ParameterName = "@DTransFrom", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.DTransFrom });
                Connection.myparameters.Add(new myParameters { ParameterName = "@DTransTo", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.DTransTo });
                Connection.myparameters.Add(new myParameters { ParameterName = "@LocFrom", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.LocFrom });
                Connection.myparameters.Add(new myParameters { ParameterName = "@LocTo", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.LocTo });
                Connection.myparameters.Add(new myParameters { ParameterName = "@CCChangeFrom", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.CCChangeFrom });
                Connection.myparameters.Add(new myParameters { ParameterName = "@CCChangeTo", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.CCChangeTo });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Email", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Email });
                Connection.myparameters.Add(new myParameters { ParameterName = "@WorkEmail", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.WorkEmail });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Extension", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Extension });
                Connection.myparameters.Add(new myParameters { ParameterName = "@JoinDate", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.JoinDate });
                Connection.myparameters.Add(new myParameters { ParameterName = "@ProdStart", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.ProdStart });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Tenure", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Tenure });
                Connection.myparameters.Add(new myParameters { ParameterName = "@TenureMnth", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.TenureMnth });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Batch", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Batch });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Class", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Class });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Skill", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Skill });
                Connection.myparameters.Add(new myParameters { ParameterName = "@JobDesc", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.JobDesc });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Promotion", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Promotion });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Status", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Status });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Emp_Name", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Emp_Name });
                Connection.myparameters.Add(new myParameters { ParameterName = "@approved", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.approved });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Employee_Level", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Employee_Level });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Employee_Category", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Employee_Category });
                Connection.myparameters.Add(new myParameters { ParameterName = "@BusinessSegment", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.BusinessSegment });
                Connection.myparameters.Add(new myParameters { ParameterName = "@IsApprove", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.IsApprove });
                Connection.myparameters.Add(new myParameters { ParameterName = "@ResignedDate", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.ResignedDate });
                String ID = Connection.ExecuteScalar("sp_HRMS_UpdateEmployeeInfo");
                return ID;
            }
            catch (Exception e)
            {
                return e.ToString();
            }

        }
        [HttpPost]
        [Route("UpdateEmpConfidentialInfov2")]
        public String UpdateEmpConfidentialInfov2(UpdateEmployeeConfiInfov2 EmployeeMaster)
        {
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = EmployeeMaster.CN;
            SelfieRegistration2Controller.GetDB2(GDB);

            Connection Connection = new Connection();
            Connection.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.EmpID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@TaxStatus", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.TaxStatus });
            Connection.myparameters.Add(new myParameters { ParameterName = "@SSS", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.SSS });
            Connection.myparameters.Add(new myParameters { ParameterName = "@TIN", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.TIN });
            Connection.myparameters.Add(new myParameters { ParameterName = "@PhilHealth", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.PhilHealth });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Pagibig", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Pagibig });
            Connection.myparameters.Add(new myParameters { ParameterName = "@DriversLicense", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.DriversLicense });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Passport", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Passport });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Unified", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Unified });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Postal", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Postal });
            Connection.myparameters.Add(new myParameters { ParameterName = "@BirthCertificate", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.BirthCertificate });
            Connection.myparameters.Add(new myParameters { ParameterName = "@SssImage", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.SssImage });
            Connection.myparameters.Add(new myParameters { ParameterName = "@TinImage", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.TinImage });
            Connection.myparameters.Add(new myParameters { ParameterName = "@PhilImage", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.PhilImage });
            Connection.myparameters.Add(new myParameters { ParameterName = "@PagImage", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.PagImage });
            Connection.myparameters.Add(new myParameters { ParameterName = "@DriverImage", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.DriverImage });
            Connection.myparameters.Add(new myParameters { ParameterName = "@PassportImage", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.PassportImage });
            Connection.myparameters.Add(new myParameters { ParameterName = "@UmidImage", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.UmidImage });
            Connection.myparameters.Add(new myParameters { ParameterName = "@PostalImage", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.PostalImage });
            Connection.myparameters.Add(new myParameters { ParameterName = "@BirthImage", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.BirthImage });
            Connection.myparameters.Add(new myParameters { ParameterName = "@DriversExpiry", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.DriversExpiry });
            Connection.myparameters.Add(new myParameters { ParameterName = "@PassportExpiry", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.PassportExpiry });
            Connection.myparameters.Add(new myParameters { ParameterName = "@PostalExpiry", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.PostalExpiry });
            Connection.myparameters.Add(new myParameters { ParameterName = "@SAGSD", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.SAGSD });
            Connection.myparameters.Add(new myParameters { ParameterName = "@SAGSDExpiry", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.SAGSDExpiry });
            Connection.myparameters.Add(new myParameters { ParameterName = "@NTC", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.NTC });
            Connection.myparameters.Add(new myParameters { ParameterName = "@NTCExpiry", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.NTCExpiry });
            Connection.myparameters.Add(new myParameters { ParameterName = "@BrgyClearance", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.BrgyClearance });
            Connection.myparameters.Add(new myParameters { ParameterName = "@BrgyClearanceExpiry", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.BrgyClearanceExpiry });
            Connection.myparameters.Add(new myParameters { ParameterName = "@PNPClearance", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.PNPClearance });
            Connection.myparameters.Add(new myParameters { ParameterName = "@PNPClearanceExpiry", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.PNPClearanceExpiry });
            Connection.myparameters.Add(new myParameters { ParameterName = "@NBIClearance", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.NBIClearance });
            Connection.myparameters.Add(new myParameters { ParameterName = "@NBIClearanceExpiry", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.NBIClearanceExpiry });
            Connection.myparameters.Add(new myParameters { ParameterName = "@NueroEvaluation", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.NueroEvaluation });
            Connection.myparameters.Add(new myParameters { ParameterName = "@NeuroEvaluationExpiry", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.NeuroEvaluationExpiry });
            Connection.myparameters.Add(new myParameters { ParameterName = "@DrugTest", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.DrugTest });
            Connection.myparameters.Add(new myParameters { ParameterName = "@DrugTestExpiry", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.DrugTestExpiry });
            String ID = Connection.ExecuteScalar("sp_UpdateEmployeeConfidentialInfo_Mobile");
            return ID;
        }
        [HttpPost]
        [Route("GetCompanyProfileContactv2")]
        public CompanyProfileContactList CompanyProfileContactViewv2(GetCompanyProfileViewv2 Com)
        {
            CompanyProfileContactList CompanyProfileContactLists = new CompanyProfileContactList();

            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = Com.CN;
            SelfieRegistration2Controller.GetDB2(GDB);

            Connection Connection = new Connection();
            List<GetCompanyProfileContactView> CompanyProfileView = new List<GetCompanyProfileContactView>();
            System.Data.DataTable DTCOMPANY = new System.Data.DataTable();
            Connection.myparameters.Add(new myParameters { ParameterName = "@Codebranch", mytype = SqlDbType.NVarChar, Value = Com.BranchID });
            DTCOMPANY = Connection.GetDataTable("sp_GetCompanyContactDetails_Mobile");
            //CultureInfo provider = CultureInfo.InvariantCulture;
            if (DTCOMPANY.Rows.Count > 0)
            {
                foreach (DataRow row in DTCOMPANY.Rows)
                {
                    GetCompanyProfileContactView CompanyProfileContactViewGet = new GetCompanyProfileContactView
                    {
                        PhoneType = row["PhoneType"].ToString(),
                        ContactNumber = row["ContactNumber"].ToString(),

                    };
                    CompanyProfileView.Add(CompanyProfileContactViewGet);
                }
                CompanyProfileContactLists.GetCompanyProfileContactView = CompanyProfileView;
                CompanyProfileContactLists.myreturn = "Success";
            }
            else
            {
                CompanyProfileContactLists.myreturn = "No Data Available";
            }

            return CompanyProfileContactLists;
        }

        [HttpPost]
        [Route("GetCompanyProfileEmailv2")]
        public CompanyProfileEmailList CompanyProfileEmailViewv2(GetCompanyProfileViewv2 Com)
        {
            CompanyProfileEmailList CompanyProfileEmailLists = new CompanyProfileEmailList();

            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = Com.CN;
            SelfieRegistration2Controller.GetDB2(GDB);

            Connection Connection = new Connection();
            List<GetCompanyProfileEmailView> CompanyProfileView = new List<GetCompanyProfileEmailView>();
            System.Data.DataTable DTCOMPANY = new System.Data.DataTable();
            Connection.myparameters.Add(new myParameters { ParameterName = "@Codebranch", mytype = SqlDbType.NVarChar, Value = Com.BranchID });
            DTCOMPANY = Connection.GetDataTable("sp_GetCompanyEmailDetails_Mobile");
            //CultureInfo provider = CultureInfo.InvariantCulture;
            if (DTCOMPANY.Rows.Count > 0)
            {
                foreach (DataRow row in DTCOMPANY.Rows)
                {
                    GetCompanyProfileEmailView CompanyProfileEmailViewGet = new GetCompanyProfileEmailView
                    {
                        EmailType = row["EmailType"].ToString(),
                        Emailtxt = row["Emailtxt"].ToString(),

                    };
                    CompanyProfileView.Add(CompanyProfileEmailViewGet);
                }
                CompanyProfileEmailLists.GetCompanyProfileEmailView = CompanyProfileView;
                CompanyProfileEmailLists.myreturn = "Success";
            }
            else
            {
                CompanyProfileEmailLists.myreturn = "No Data Available";
            }

            return CompanyProfileEmailLists;
        }
        [HttpPost]
        [Route("InsertUserProfilev3")]
        public string UserProfilev3(UserProfilev3 uProfile)
        {

            try
            {
                string dataDir = HostingEnvironment.MapPath("~/Files/");
                FileStream stream = new FileStream(dataDir + "Basic.xls", FileMode.Open);
                //ExcelWriter writer = new ExcelWriter(stream);


                string filename;
                filename = uProfile.NTID + "_" + DateTime.Now.Month.ToString().PadLeft(2, '0') + DateTime.Now.Day.ToString().PadLeft(2, '0') + DateTime.Now.Year.ToString().Substring(2, 2) + "_CRT-UPDATEOCT2016.xls";
                using (var fileStream = new FileStream(dataDir + filename, FileMode.Create, FileAccess.Write))
                {
                    stream.CopyTo(fileStream);
                }
                stream.Close();

                ExcelWorkbookFormat workbookFormat = ExcelWorkbookFormat.Xls_2003;
                string testDocFile = dataDir + "Basic.xls";
                ExcelWorkbook workbook = new ExcelWorkbook(testDocFile);
                ExcelWorksheet firstWorksheet = workbook.Worksheets[1];
                //int intRow = 11;
                //while (!(string.IsNullOrEmpty(firstWorksheet["B"+ intRow].Text)))
                //{
                //    intRow++;
                //}
                firstWorksheet["B11"].Text = uProfile.LASTNAME;
                firstWorksheet["C11"].Text = uProfile.FIRSTNAME;
                firstWorksheet["D11"].Text = uProfile.MIDDLENAME;
                firstWorksheet["E11"].Text = uProfile.SUFFIX;
                firstWorksheet["F11"].Text = uProfile.CARDNAME;
                firstWorksheet["G11"].Text = Convert.ToDateTime(uProfile.BIRTHDAY).ToString("MM-dd-yyyy");
                firstWorksheet["H11"].Text = uProfile.PLACEOFBIRTH;
                firstWorksheet["I11"].Text = uProfile.NATIONALITY;
                firstWorksheet["J11"].Text = uProfile.NATCOUNTRY;
                firstWorksheet["K11"].Text = uProfile.GENDER;
                firstWorksheet["L11"].Text = uProfile.MOBILE;
                firstWorksheet["M11"].Text = uProfile.TELPHONE;
                firstWorksheet["N11"].Text = uProfile.PRESHOUSENUMBER;
                firstWorksheet["O11"].Text = uProfile.PRESHOUSEADDRESS;
                firstWorksheet["P11"].Text = uProfile.PRESCOUNTRY;
                firstWorksheet["Q11"].Text = uProfile.PRESREGION;
                firstWorksheet["R11"].Text = uProfile.PRESPROVINCE;
                firstWorksheet["S11"].Text = uProfile.PRESCITY;
                firstWorksheet["T11"].Text = uProfile.PRESZIPCODE;
                firstWorksheet["U11"].Text = uProfile.PERMHOUSENUMBER;
                firstWorksheet["V11"].Text = uProfile.PERMHOUSEADDRESS;
                firstWorksheet["W11"].Text = uProfile.PERMCOUNTRY;
                firstWorksheet["X11"].Text = uProfile.PERMREGION;
                firstWorksheet["Y11"].Text = uProfile.PERMPROVINCE;
                firstWorksheet["Z11"].Text = uProfile.PERMCITY;
                firstWorksheet["AA11"].Text = uProfile.PERMZIPCODE;
                firstWorksheet["AB11"].Text = uProfile.EMAILADD;
                firstWorksheet["AC11"].Text = uProfile.EMPLOYERNAME;
                firstWorksheet["AD11"].Text = uProfile.TIN;
                firstWorksheet["AE11"].Text = uProfile.CIVILSTATUS;
                firstWorksheet["AF11"].Text = uProfile.MOTHLASTNAME;
                firstWorksheet["AG11"].Text = uProfile.MOTHFIRSTNAME;
                firstWorksheet["AH11"].Text = uProfile.MOTHMIDDLENAME;
                firstWorksheet["AI11"].Text = uProfile.MOTHSUFFIX;
                firstWorksheet["AJ11"].Text = uProfile.NUMBEROFPADS;
                firstWorksheet["AK11"].Text = uProfile.OTHERDETAILS;

                GetDatabase GDB = new GetDatabase();
                GDB.ClientName = uProfile.CN;
                SelfieRegistration2Controller.GetDB2(GDB);

                Connection Connection = new Connection();

                Connection.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = uProfile.NTID });
                Connection.myparameters.Add(new myParameters { ParameterName = "@CardNum", mytype = SqlDbType.NVarChar, Value = uProfile.CARDNAME });
                Connection.myparameters.Add(new myParameters { ParameterName = "@DOB", mytype = SqlDbType.NVarChar, Value = Convert.ToDateTime(uProfile.BIRTHDAY).ToString("yyyy-MM-dd") });
                Connection.myparameters.Add(new myParameters { ParameterName = "@POB", mytype = SqlDbType.NVarChar, Value = uProfile.PLACEOFBIRTH });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Natioanality", mytype = SqlDbType.NVarChar, Value = uProfile.NATIONALITY });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Country", mytype = SqlDbType.NVarChar, Value = uProfile.NATCOUNTRY });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Gender", mytype = SqlDbType.NVarChar, Value = uProfile.GENDER });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Mobile", mytype = SqlDbType.NVarChar, Value = uProfile.MOBILE });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Landline", mytype = SqlDbType.NVarChar, Value = uProfile.TELPHONE });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Per_Zipcode", mytype = SqlDbType.NVarChar, Value = uProfile.PERMZIPCODE });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Per_HouseNum", mytype = SqlDbType.NVarChar, Value = uProfile.PERMHOUSENUMBER });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Per_St_VIllage_Sub", mytype = SqlDbType.NVarChar, Value = uProfile.PERMHOUSEADDRESS });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Per_Brgy", mytype = SqlDbType.NVarChar, Value = uProfile.PERMBRGY });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Per_Muni", mytype = SqlDbType.NVarChar, Value = uProfile.PERMUNI });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Per_City", mytype = SqlDbType.NVarChar, Value = uProfile.PERMCITY });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Per_Region", mytype = SqlDbType.NVarChar, Value = uProfile.PERMREGION });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Per_Country", mytype = SqlDbType.NVarChar, Value = uProfile.PERMCOUNTRY });


                Connection.myparameters.Add(new myParameters { ParameterName = "@Pre_Zipcode", mytype = SqlDbType.NVarChar, Value = uProfile.PRESZIPCODE });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Pre_HouseNum", mytype = SqlDbType.NVarChar, Value = uProfile.PRESHOUSENUMBER });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Pre_St_VIllage_Sub", mytype = SqlDbType.NVarChar, Value = uProfile.PRESHOUSEADDRESS });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Pre_Brgy", mytype = SqlDbType.NVarChar, Value = uProfile.PRESBRGY });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Pre_Muni", mytype = SqlDbType.NVarChar, Value = uProfile.PRESMUNI });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Pre_City", mytype = SqlDbType.NVarChar, Value = uProfile.PRESCITY });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Pre_Region", mytype = SqlDbType.NVarChar, Value = uProfile.PRESREGION });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Pre_Country", mytype = SqlDbType.NVarChar, Value = uProfile.PRESCOUNTRY });


                Connection.myparameters.Add(new myParameters { ParameterName = "@email", mytype = SqlDbType.NVarChar, Value = uProfile.EMAILADD });
                Connection.myparameters.Add(new myParameters { ParameterName = "@EmployerName", mytype = SqlDbType.NVarChar, Value = uProfile.EMPLOYERNAME });
                Connection.myparameters.Add(new myParameters { ParameterName = "@TIN", mytype = SqlDbType.NVarChar, Value = uProfile.TIN });
                Connection.myparameters.Add(new myParameters { ParameterName = "@CivilStatus", mytype = SqlDbType.NVarChar, Value = uProfile.CIVILSTATUS });
                Connection.myparameters.Add(new myParameters { ParameterName = "@MothersMaidenLName", mytype = SqlDbType.NVarChar, Value = uProfile.MOTHSUFFIX });
                Connection.myparameters.Add(new myParameters { ParameterName = "@MothersFname", mytype = SqlDbType.NVarChar, Value = uProfile.MOTHFIRSTNAME });
                Connection.myparameters.Add(new myParameters { ParameterName = "@MothersMName", mytype = SqlDbType.NVarChar, Value = uProfile.MOTHLASTNAME });
                Connection.myparameters.Add(new myParameters { ParameterName = "@OtherDetails", mytype = SqlDbType.NVarChar, Value = uProfile.OTHERDETAILS });

                Connection.myparameters.Add(new myParameters { ParameterName = "@COMPANYNAME", mytype = SqlDbType.NVarChar, Value = uProfile.COMPANYNAME });
                Connection.myparameters.Add(new myParameters { ParameterName = "@SPECIALIZATION", mytype = SqlDbType.NVarChar, Value = uProfile.SPECIALIZATION });
                Connection.myparameters.Add(new myParameters { ParameterName = "@ROLEPOSITIONTITLE", mytype = SqlDbType.NVarChar, Value = uProfile.ROLEPOSITIONTITLE });
                Connection.myparameters.Add(new myParameters { ParameterName = "@POSITIONLEVEL", mytype = SqlDbType.NVarChar, Value = uProfile.POSITIONLEVEL });
                Connection.myparameters.Add(new myParameters { ParameterName = "@JOINEDFROM", mytype = SqlDbType.NVarChar, Value = uProfile.JOINEDFROM });
                Connection.myparameters.Add(new myParameters { ParameterName = "@JOINEDTO", mytype = SqlDbType.NVarChar, Value = uProfile.JOINEDTO });
                Connection.myparameters.Add(new myParameters { ParameterName = "@INDUSTRY", mytype = SqlDbType.NVarChar, Value = uProfile.INDUSTRY });
                Connection.myparameters.Add(new myParameters { ParameterName = "@COUNTRY2", mytype = SqlDbType.NVarChar, Value = uProfile.COUNTRY });
                Connection.myparameters.Add(new myParameters { ParameterName = "@EXPERIENCE", mytype = SqlDbType.NVarChar, Value = uProfile.EXPERIENCE });
                Connection.myparameters.Add(new myParameters { ParameterName = "@PositionTitle", mytype = SqlDbType.NVarChar, Value = uProfile.PositionTitle });
                Connection.myparameters.Add(new myParameters { ParameterName = "@MaritalStatus", mytype = SqlDbType.NVarChar, Value = uProfile.MaritalStatus });
                Connection.myparameters.Add(new myParameters { ParameterName = "@MarriedDate", mytype = SqlDbType.NVarChar, Value = Convert.ToDateTime(uProfile.Marrieddate).ToString("yyyy-MM-dd") });

                //ADDED NEW DATA START//

                Connection.myparameters.Add(new myParameters { ParameterName = "@BLOODTYPE", mytype = SqlDbType.NVarChar, Value = uProfile.BLOODTYPE });
                Connection.myparameters.Add(new myParameters { ParameterName = "@RELIGION", mytype = SqlDbType.NVarChar, Value = uProfile.RELIGION });
                Connection.myparameters.Add(new myParameters { ParameterName = "@SUFFIX", mytype = SqlDbType.NVarChar, Value = uProfile.SUFFIX });
                Connection.myparameters.Add(new myParameters { ParameterName = "@TOWN", mytype = SqlDbType.NVarChar, Value = uProfile.TOWN });
                Connection.myparameters.Add(new myParameters { ParameterName = "@HEIGHT", mytype = SqlDbType.NVarChar, Value = uProfile.HEIGHT });
                Connection.myparameters.Add(new myParameters { ParameterName = "@WEIGHT", mytype = SqlDbType.NVarChar, Value = uProfile.WEIGHT });

                //ADDED NEW DATA END//




                String ID = Connection.ExecuteScalar("sp_Insert_EmployeeProfile_FromMobilev3");

                System.Web.HttpResponse httpResponse = System.Web.HttpContext.Current.Response;

                httpResponse.Clear();
                httpResponse.ContentType = "Application/x-msexcel";
                httpResponse.AddHeader("Content-Disposition", String.Format("attachment; filename={0}", filename));
                string newDIR = HostingEnvironment.MapPath("~/sFTP/");
                workbook.Save(newDIR + filename);
                return "Success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }

        [Route("GetImagesv3")]
        public String Base64Imagev3(UserProfile_Imagesv2 ProfileImages)
        // public HttpResponseMessage DisplaySignature(string id)
        {
            try
            {
                //string file_path;

                //// file_path = Path.Combine(System.Web.HttpRuntime.AppDomainAppPath + "ProfilePhotos\\" + ProfileImages.EmpID + "\\", ProfileImages.EmpID + "_" + ProfileImages.ImageType + ".png");
                ////var savePath = HttpContext.Current.Server.MapPath("~/ProfilePhotos/" + ProfileImages.CN.ToLower() + "/" + ProfileImages.EmpID + "/");
                ////file_path = Path.Combine(savePath, ProfileImages.EmpID + "_" + ProfileImages.ImageType + ".png");
                //file_path = Path.Combine(System.Web.HttpRuntime.AppDomainAppPath + "ProfilePhotos\\" + ProfileImages.CN.ToLower() + "\\" + ProfileImages.EmpID + "\\", ProfileImages.EmpID + "_" + ProfileImages.ImageType + ".png");
                //// file_path = Path.Combine(savePath, ProfileImages.ImageType + ".png");
                //MemoryStream ms = new MemoryStream(File.ReadAllBytes(file_path));
                //HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);
                //byte[] imageBytes = ms.ToArray();
                var path = HttpContext.Current.Server.MapPath("~/ProfilePhotos/" + ProfileImages.CN + "/" + "");
                var folder = Path.Combine(path, ProfileImages.EmpID);
                //string base64String = Convert.ToBase64String(imageBytes);

                GetDatabase GDB = new GetDatabase();
                GDB.ClientName = ProfileImages.CN;
                SelfieRegistration2Controller.GetDB2(GDB);


                Connection Connection = new Connection();

                Connection.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = ProfileImages.EmpID });
                Connection.myparameters.Add(new myParameters { ParameterName = "@ImgType", mytype = SqlDbType.NVarChar, Value = ProfileImages.ImageType });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Base", mytype = SqlDbType.NVarChar, Value = ProfileImages.URL });

                String ID = Connection.ExecuteScalar("sp_Insert_Base64_Mobile");

                return ProfileImages.ImageType;
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }

        [Route("GetImagesv4")]
        public String Base64Imagev4(UserProfile_Imagesv2 ProfileImages)
        // public HttpResponseMessage DisplaySignature(string id)
        {
            try
            {
                //string file_path;

                //// file_path = Path.Combine(System.Web.HttpRuntime.AppDomainAppPath + "ProfilePhotos\\" + ProfileImages.EmpID + "\\", ProfileImages.EmpID + "_" + ProfileImages.ImageType + ".png");
                ////var savePath = HttpContext.Current.Server.MapPath("~/ProfilePhotos/" + ProfileImages.CN.ToLower() + "/" + ProfileImages.EmpID + "/");
                ////file_path = Path.Combine(savePath, ProfileImages.EmpID + "_" + ProfileImages.ImageType + ".png");
                //file_path = Path.Combine(System.Web.HttpRuntime.AppDomainAppPath + "ProfilePhotos\\" + ProfileImages.CN.ToLower() + "\\" + ProfileImages.EmpID + "\\", ProfileImages.EmpID + "_" + ProfileImages.ImageType + ".png");
                //// file_path = Path.Combine(savePath, ProfileImages.ImageType + ".png");
                //MemoryStream ms = new MemoryStream(File.ReadAllBytes(file_path));
                //HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);
                //byte[] imageBytes = ms.ToArray();
                var path = HttpContext.Current.Server.MapPath("~/ProfilePhotos/" + ProfileImages.CN + "/" + "");
                var folder = Path.Combine(path, ProfileImages.EmpID);
                //string base64String = Convert.ToBase64String(imageBytes);

                GetDatabase GDB = new GetDatabase();
                GDB.ClientName = ProfileImages.CN;
                SelfieRegistration2Controller.GetDB2(GDB);


                Connection Connection = new Connection();

                Connection.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = ProfileImages.EmpID });
                Connection.myparameters.Add(new myParameters { ParameterName = "@ImgType", mytype = SqlDbType.NVarChar, Value = ProfileImages.ImageType });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Base", mytype = SqlDbType.NVarChar, Value = ProfileImages.URL });
                Connection.myparameters.Add(new myParameters { ParameterName = "@IDnum", mytype = SqlDbType.NVarChar, Value = ProfileImages.Card1 });
                String ID = Connection.ExecuteScalar("sp_Insert_Base64_Mobilev2");

                return ProfileImages.ImageType;
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }

        [HttpPost]
        [Route("ExitProcessAuditTrail")]

        public ExitProcessAuditTrailResult DHRGetDependentv2(ExitProcessAuditTrailMod DH)
        {
            ExitProcessAuditTrailResult ExitProcessAuditTrailResult = new ExitProcessAuditTrailResult();
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = DH.CN;
            SelfieRegistration2Controller.GetDB2(GDB);

            try
            {
                Connection Connection = new Connection();
                List<ExitProcessAuditTrailMod> ListDependent = new List<ExitProcessAuditTrailMod>();
                DataSet ds = new DataSet();
                System.Data.DataTable dtLEAVES = new System.Data.DataTable();
                // Connection.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = DH.EmpID });
                ds = Connection.GetDataset("sp_ExitProcessAuditTrail");
                dtLEAVES = ds.Tables[0];
                if (dtLEAVES.Rows.Count > 0)
                {
                    foreach (DataRow row in dtLEAVES.Rows)
                    {
                        ExitProcessAuditTrailMod DP = new ExitProcessAuditTrailMod
                        {
                            Name = row["Name"].ToString(),
                            Title = row["Title"].ToString(),
                            TerminationType = row["TerminationType"].ToString(),
                            TerminationReason = row["TerminationReason"].ToString(),
                            EffectiveDate = row["EffectiveDate"].ToString(),
                            ImmediateSupervisor = row["ImmediateSupervisor"].ToString(),
                            ModifiedBy = row["ModifiedBy"].ToString(),
                            Department = row["Department"].ToString(),
                            ResignationReason = row["ResignationReason"].ToString(),
                        };
                        ListDependent.Add(DP);
                    }
                    ExitProcessAuditTrailResult.Dependent = ListDependent;
                    ExitProcessAuditTrailResult.myreturn = "Success";
                }
                else
                {
                    ExitProcessAuditTrailResult.myreturn = "No Data Available";
                }
            }
            catch (Exception ex)
            {
                ExitProcessAuditTrailResult.myreturn = ex.Message;
            }
            return ExitProcessAuditTrailResult;
        }

        #region Marvic

        [HttpPost]
        [Route("DHRCompanyContactv1")]
        //Company Contact 
        public string DHRCompanyContactv1(CompanyCE CE)
        {
            try
            {
                GetDatabase GDB = new GetDatabase();
                GDB.ClientName = CE.CN;
                SelfieRegistration2Controller.GetDB2(GDB);
                Connection Con = new Connection();
                Con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = CE.LoginID });
                Con.myparameters.Add(new myParameters { ParameterName = "@PHONETYPE", mytype = SqlDbType.NVarChar, Value = CE.ContactTypes });
                Con.myparameters.Add(new myParameters { ParameterName = "@CONTACTNUM", mytype = SqlDbType.NVarChar, Value = CE.ContactNumber });
                Con.ExecuteNonQuery("sp_InsertCompanyContactDHR");
                return "success";
            }
            catch (Exception e)
            {
                return "failed" + e;
            }
        }

        [HttpPost]
        [Route("DHRCompanyEmailv1")]
        //Company Email
        public string DHRCompanyEmailv1(CompanyCE CE)
        {
            try
            {
                GetDatabase GDB = new GetDatabase();
                GDB.ClientName = CE.CN;
                SelfieRegistration2Controller.GetDB2(GDB);
                Connection Con = new Connection();
                Con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = CE.LoginID });
                Con.myparameters.Add(new myParameters { ParameterName = "@EMAILTYPE", mytype = SqlDbType.NVarChar, Value = CE.EmailTypes });
                Con.myparameters.Add(new myParameters { ParameterName = "@EMAILTXT", mytype = SqlDbType.NVarChar, Value = CE.Email });
                Con.ExecuteNonQuery("sp_InsertCompanyEmailDHR");
                return "success";
            }
            catch (Exception e)
            {
                return "failed" + e;
            }
        }


        [HttpPost]
        [Route("UpdateComEmailAndContactv1")]
        //Company Email
        public string UpdateComEmailAndContactv1(CompanyCE CE)
        {
            try
            {
                GetDatabase GDB = new GetDatabase();
                GDB.ClientName = CE.CN;
                SelfieRegistration2Controller.GetDB2(GDB);
                Connection Con = new Connection();
                Con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = CE.LoginID });
                Con.ExecuteNonQuery("sp_UpdateComEmailAndContactv1");
                return "success";
            }
            catch (Exception e)
            {
                return "failed" + e;
            }
        }


        //insert dependent details with age
        [HttpPost]
        [Route("DHRAddDependentv1")]
        public String DHRAddDependentv1(DHRDEPv1 DependentDetails)

        {
            try
            {
                GetDatabase GDB = new GetDatabase();
                GDB.ClientName = DependentDetails.CN;
                SelfieRegistration2Controller.GetDB2(GDB);
                var time = DateTime.Now;
                string formattedTime = time.ToString("yyyy-MM-dd-hh-mm-ss-fff");
                Connection Connection = new Connection();

                Connection.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.VarChar, Value = DependentDetails.EmpID });
                Connection.myparameters.Add(new myParameters { ParameterName = "@RELATION", mytype = SqlDbType.VarChar, Value = DependentDetails.Relationship });
                Connection.myparameters.Add(new myParameters { ParameterName = "@FIRST", mytype = SqlDbType.VarChar, Value = DependentDetails.First });
                Connection.myparameters.Add(new myParameters { ParameterName = "@MID", mytype = SqlDbType.VarChar, Value = DependentDetails.Mid });
                Connection.myparameters.Add(new myParameters { ParameterName = "@LAST", mytype = SqlDbType.VarChar, Value = DependentDetails.Lastname });
                Connection.myparameters.Add(new myParameters { ParameterName = "@BIRTH", mytype = SqlDbType.VarChar, Value = DependentDetails.Birth });
                Connection.myparameters.Add(new myParameters { ParameterName = "@COMMENT", mytype = SqlDbType.VarChar, Value = DependentDetails.Comments });
                Connection.myparameters.Add(new myParameters { ParameterName = "@FILE", mytype = SqlDbType.VarChar, Value = DependentDetails.DocFilename });
                Connection.myparameters.Add(new myParameters { ParameterName = "@EXTENSION", mytype = SqlDbType.VarChar, Value = DependentDetails.Extension });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Age", mytype = SqlDbType.VarChar, Value = DependentDetails.Age });
                Connection.myparameters.Add(new myParameters { ParameterName = "@BASED64IMAGE", mytype = SqlDbType.VarChar, Value = (DependentDetails.BASED64IMAGE == null || DependentDetails.BASED64IMAGE.Contains("https") == true) ? "" : DependentDetails.CERTIFICATETYPE });
                Connection.myparameters.Add(new myParameters { ParameterName = "@CERTIFICATETYPE", mytype = SqlDbType.VarChar, Value = DependentDetails.CERTIFICATETYPE + "-" + formattedTime });
                Connection.myparameters.Add(new myParameters { ParameterName = "@COMPNAME", mytype = SqlDbType.VarChar, Value = DependentDetails.CN });
                Connection.ExecuteNonQuery("sp_InsertDependentDHRV1");

                byte[] PIC = Convert.FromBase64String(DependentDetails.BASED64IMAGE);
                var path = HttpContext.Current.Server.MapPath("~/ProfilePhotos/" + DependentDetails.CN + "/" + "");
                var folder = Path.Combine(path, DependentDetails.EmpID);
                Directory.CreateDirectory(folder);
                MemoryStream stream = new MemoryStream();
                using (stream)
                {

                    File.WriteAllBytes(folder + "/" + DependentDetails.CERTIFICATETYPE + "-" + formattedTime + ".png", PIC);

                }

                return "success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }

        }
        [HttpPost]
        [Route("UpdatedependentDHRv1")]
        public string UpdatedependentDHRv1(DHRDEPv1 DependentDetails)
        {
            try
            {
                GetDatabase GDB = new GetDatabase();
                GDB.ClientName = DependentDetails.CN;
                SelfieRegistration2Controller.GetDB2(GDB);
                var time = DateTime.Now;
                string formattedTime = time.ToString("yyyy-MM-dd-hh-mm-ss-fff");
                Connection Connection = new Connection();
                Connection.myparameters.Add(new myParameters { ParameterName = "@DEPENDENTID", mytype = SqlDbType.VarChar, Value = DependentDetails.dependentID });
                Connection.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.VarChar, Value = DependentDetails.EmpID });
                Connection.myparameters.Add(new myParameters { ParameterName = "@RELATION", mytype = SqlDbType.VarChar, Value = DependentDetails.Relationship });
                Connection.myparameters.Add(new myParameters { ParameterName = "@FIRST", mytype = SqlDbType.VarChar, Value = DependentDetails.First });
                Connection.myparameters.Add(new myParameters { ParameterName = "@MID", mytype = SqlDbType.VarChar, Value = DependentDetails.Mid });
                Connection.myparameters.Add(new myParameters { ParameterName = "@LAST", mytype = SqlDbType.VarChar, Value = DependentDetails.Lastname });
                Connection.myparameters.Add(new myParameters { ParameterName = "@BIRTH", mytype = SqlDbType.VarChar, Value = DependentDetails.Birth });
                Connection.myparameters.Add(new myParameters { ParameterName = "@COMMENT", mytype = SqlDbType.VarChar, Value = DependentDetails.Comments });
                Connection.myparameters.Add(new myParameters { ParameterName = "@FILE", mytype = SqlDbType.VarChar, Value = DependentDetails.DocFilename });
                Connection.myparameters.Add(new myParameters { ParameterName = "@EXTENSION", mytype = SqlDbType.VarChar, Value = DependentDetails.Extension });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Age", mytype = SqlDbType.VarChar, Value = DependentDetails.Age });
                Connection.myparameters.Add(new myParameters { ParameterName = "@BASED64IMAGE", mytype = SqlDbType.VarChar, Value = DependentDetails.CERTIFICATETYPE });
                Connection.myparameters.Add(new myParameters { ParameterName = "@CERTIFICATETYPE", mytype = SqlDbType.VarChar, Value = DependentDetails.CERTIFICATETYPE + "-" + formattedTime });
                Connection.myparameters.Add(new myParameters { ParameterName = "@COMPNAME", mytype = SqlDbType.VarChar, Value = DependentDetails.CN });
                Connection.ExecuteNonQuery("sp_UpdateDependentDHRv1");


                if (DependentDetails.BASED64IMAGE.Contains("https") == true)
                {

                    var webClient = new WebClient();
                    byte[] PIC = webClient.DownloadData(DependentDetails.BASED64IMAGE);
                    var path = HttpContext.Current.Server.MapPath("~/ProfilePhotos/" + DependentDetails.CN + "/" + "");
                    var folder = Path.Combine(path, DependentDetails.EmpID);
                    Directory.CreateDirectory(folder);
                    MemoryStream stream = new MemoryStream();
                    using (stream)
                    {

                        File.WriteAllBytes(folder + "/" + DependentDetails.CERTIFICATETYPE + "-" + formattedTime + ".png", PIC);

                    }
                }
                else
                {
                    byte[] PIC = Convert.FromBase64String(DependentDetails.BASED64IMAGE);
                    var path = HttpContext.Current.Server.MapPath("~/ProfilePhotos/" + DependentDetails.CN + "/" + "");
                    var folder = Path.Combine(path, DependentDetails.EmpID);
                    Directory.CreateDirectory(folder);
                    MemoryStream stream = new MemoryStream();
                    using (stream)
                    {

                        File.WriteAllBytes(folder + "/" + DependentDetails.CERTIFICATETYPE + "-" + formattedTime + ".png", PIC);

                    }
                }



                return "success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }

        }

        //GET EMPLOYEE HISTORY START

        [HttpPost]
        [Route("DHRGetEmpHistoryv1")]
        public List<DHREMPHv1> DHRGetEmpHistoryv1(DHREMPHv1 EmpHistory)
        {

            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = EmpHistory.CN;
            SelfieRegistration2Controller.GetDB2(GDB);

            Connection con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = EmpHistory.LoginID });
            System.Data.DataTable DT = new System.Data.DataTable();
            DT = con.GetDataTable("sp_DHRGetEmpHistoryv1");
            List<DHREMPHv1> ListReturned = new List<DHREMPHv1>();

            if (DT.Rows.Count > 0)
            {
                for (int i = 0; i < DT.Rows.Count; i++)
                {
                    DHREMPHv1 GetLog = new DHREMPHv1();
                    GetLog.CompanyName = DT.Rows[i][0].ToString();
                    GetLog.Specialization = DT.Rows[i][1].ToString();
                    GetLog.Role = DT.Rows[i][2].ToString();
                    GetLog.PositionTitle = DT.Rows[i][3].ToString();
                    GetLog.Positionlevel = DT.Rows[i][4].ToString();
                    GetLog.JoinedFrom = DT.Rows[i][5].ToString();
                    GetLog.JoinedTo = DT.Rows[i][6].ToString();
                    GetLog.Country = DT.Rows[i][7].ToString();
                    GetLog.Description = DT.Rows[i][8].ToString();
                    GetLog.Industry = DT.Rows[i][9].ToString();
                    ListReturned.Add(GetLog);
                }
            }

            return ListReturned;
        }


        //GET EMPLOYEE HISTORY END


        //GET DEPENDENT  START

        [HttpPost]
        [Route("DHRGetDependentv1")]
        public List<DHRDEPv1> DHRGetDependentv1(DHRDEPv1 Dep)
        {

            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = Dep.CN;
            SelfieRegistration2Controller.GetDB2(GDB);

            Connection con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = Dep.LoginID });
            System.Data.DataTable DT = new System.Data.DataTable();
            DT = con.GetDataTable("sp_DHRGetDependentv1");
            List<DHRDEPv1> ListReturned = new List<DHRDEPv1>();

            if (DT.Rows.Count > 0)
            {
                for (int i = 0; i < DT.Rows.Count; i++)
                {
                    DHRDEPv1 GetLog = new DHRDEPv1();
                    GetLog.Relationship = DT.Rows[i][0].ToString();
                    GetLog.First = DT.Rows[i][1].ToString();
                    GetLog.Lastname = DT.Rows[i][2].ToString();
                    GetLog.Mid = DT.Rows[i][3].ToString();
                    GetLog.Suffix = DT.Rows[i][4].ToString();
                    GetLog.Age = DT.Rows[i][5].ToString();
                    GetLog.DOB = DT.Rows[i][6].ToString();
                    ListReturned.Add(GetLog);
                }
            }

            return ListReturned;
        }


        //GET DEPENDENT  END


        [HttpPost]
        [Route("InsertUserProfilev4")]
        public string UserProfilev4(UserProfilev4 uProfile)
        {

            try
            {
                string dataDir = HostingEnvironment.MapPath("~/sFTP/");
                //FileStream stream = new FileStream(dataDir + "CRT-UPDATE.xls", FileMode.Open);
                //ExcelWriter writer = new ExcelWriter(stream);


                string filename;
                //filename = uProfile.NTID + "_" + DateTime.Now.Month.ToString().PadLeft(2, '0') + DateTime.Now.Day.ToString().PadLeft(2, '0') + DateTime.Now.Year.ToString().Substring(2, 2) + "_CRT-UPDATEOCT2016.xls";
                filename = "CRT-UPDATE.xls";
                //using (var fileStream = new FileStream(dataDir + filename, FileMode.Create, FileAccess.Write))
                //{
                //    stream.CopyTo(fileStream);
                //}
                //stream.Close();


                //firstWorksheet["C7"].Text = "Test";
                GetDatabase GDB = new GetDatabase();
                GDB.ClientName = uProfile.CN;
                SelfieRegistration2Controller.GetDB2(GDB);

                Connection Connection = new Connection();

                Connection.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = uProfile.NTID });
                Connection.myparameters.Add(new myParameters { ParameterName = "@CardNum", mytype = SqlDbType.NVarChar, Value = uProfile.CARDNAME });
                Connection.myparameters.Add(new myParameters { ParameterName = "@DOB", mytype = SqlDbType.NVarChar, Value = Convert.ToDateTime(uProfile.BIRTHDAY).ToString("yyyy-MM-dd") });
                Connection.myparameters.Add(new myParameters { ParameterName = "@POB", mytype = SqlDbType.NVarChar, Value = uProfile.PLACEOFBIRTH });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Natioanality", mytype = SqlDbType.NVarChar, Value = uProfile.NATIONALITY });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Country", mytype = SqlDbType.NVarChar, Value = uProfile.NATCOUNTRY });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Gender", mytype = SqlDbType.NVarChar, Value = uProfile.GENDER });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Mobile", mytype = SqlDbType.NVarChar, Value = uProfile.MOBILE });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Landline", mytype = SqlDbType.NVarChar, Value = uProfile.TELPHONE });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Per_Zipcode", mytype = SqlDbType.NVarChar, Value = uProfile.PERMZIPCODE });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Per_HouseNum", mytype = SqlDbType.NVarChar, Value = uProfile.PERMHOUSENUMBER });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Per_St_VIllage_Sub", mytype = SqlDbType.NVarChar, Value = uProfile.PERMHOUSEADDRESS });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Per_Brgy", mytype = SqlDbType.NVarChar, Value = uProfile.PERMBRGY });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Per_Muni", mytype = SqlDbType.NVarChar, Value = uProfile.PERMUNI });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Per_City", mytype = SqlDbType.NVarChar, Value = uProfile.PERMCITY });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Per_Region", mytype = SqlDbType.NVarChar, Value = uProfile.PERMREGION });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Per_Country", mytype = SqlDbType.NVarChar, Value = uProfile.PERMCOUNTRY });


                Connection.myparameters.Add(new myParameters { ParameterName = "@Pre_Zipcode", mytype = SqlDbType.NVarChar, Value = uProfile.PRESZIPCODE });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Pre_HouseNum", mytype = SqlDbType.NVarChar, Value = uProfile.PRESHOUSENUMBER });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Pre_St_VIllage_Sub", mytype = SqlDbType.NVarChar, Value = uProfile.PRESHOUSEADDRESS });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Pre_Brgy", mytype = SqlDbType.NVarChar, Value = uProfile.PRESBRGY });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Pre_Muni", mytype = SqlDbType.NVarChar, Value = uProfile.PRESMUNI });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Pre_City", mytype = SqlDbType.NVarChar, Value = uProfile.PRESCITY });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Pre_Region", mytype = SqlDbType.NVarChar, Value = uProfile.PRESREGION });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Pre_Country", mytype = SqlDbType.NVarChar, Value = uProfile.PRESCOUNTRY });


                Connection.myparameters.Add(new myParameters { ParameterName = "@email", mytype = SqlDbType.NVarChar, Value = uProfile.EMAILADD });
                Connection.myparameters.Add(new myParameters { ParameterName = "@EmployerName", mytype = SqlDbType.NVarChar, Value = uProfile.EMPLOYERNAME });
                Connection.myparameters.Add(new myParameters { ParameterName = "@TIN", mytype = SqlDbType.NVarChar, Value = uProfile.TIN });
                Connection.myparameters.Add(new myParameters { ParameterName = "@CivilStatus", mytype = SqlDbType.NVarChar, Value = uProfile.CIVILSTATUS });
                Connection.myparameters.Add(new myParameters { ParameterName = "@MothersMaidenLName", mytype = SqlDbType.NVarChar, Value = uProfile.MOTHSUFFIX });
                Connection.myparameters.Add(new myParameters { ParameterName = "@MothersFname", mytype = SqlDbType.NVarChar, Value = uProfile.MOTHFIRSTNAME });
                Connection.myparameters.Add(new myParameters { ParameterName = "@MothersMName", mytype = SqlDbType.NVarChar, Value = uProfile.MOTHLASTNAME });
                Connection.myparameters.Add(new myParameters { ParameterName = "@OtherDetails", mytype = SqlDbType.NVarChar, Value = uProfile.OTHERDETAILS });

                Connection.myparameters.Add(new myParameters { ParameterName = "@COMPANYNAME", mytype = SqlDbType.NVarChar, Value = uProfile.COMPANYNAME });
                Connection.myparameters.Add(new myParameters { ParameterName = "@SPECIALIZATION", mytype = SqlDbType.NVarChar, Value = uProfile.SPECIALIZATION });
                Connection.myparameters.Add(new myParameters { ParameterName = "@ROLEPOSITIONTITLE", mytype = SqlDbType.NVarChar, Value = uProfile.ROLEPOSITIONTITLE });
                Connection.myparameters.Add(new myParameters { ParameterName = "@POSITIONLEVEL", mytype = SqlDbType.NVarChar, Value = uProfile.POSITIONLEVEL });
                Connection.myparameters.Add(new myParameters { ParameterName = "@JOINEDFROM", mytype = SqlDbType.NVarChar, Value = uProfile.JOINEDFROM });
                Connection.myparameters.Add(new myParameters { ParameterName = "@JOINEDTO", mytype = SqlDbType.NVarChar, Value = uProfile.JOINEDTO });
                Connection.myparameters.Add(new myParameters { ParameterName = "@INDUSTRY", mytype = SqlDbType.NVarChar, Value = uProfile.INDUSTRY });
                Connection.myparameters.Add(new myParameters { ParameterName = "@COUNTRY2", mytype = SqlDbType.NVarChar, Value = uProfile.COUNTRY });
                Connection.myparameters.Add(new myParameters { ParameterName = "@EXPERIENCE", mytype = SqlDbType.NVarChar, Value = uProfile.EXPERIENCE });
                Connection.myparameters.Add(new myParameters { ParameterName = "@PositionTitle", mytype = SqlDbType.NVarChar, Value = uProfile.PositionTitle });
                Connection.myparameters.Add(new myParameters { ParameterName = "@MaritalStatus", mytype = SqlDbType.NVarChar, Value = uProfile.MaritalStatus });
                Connection.myparameters.Add(new myParameters { ParameterName = "@MarriedDate", mytype = SqlDbType.NVarChar, Value = uProfile.Marrieddate });

                //ADDED NEW DATA START//

                Connection.myparameters.Add(new myParameters { ParameterName = "@BLOODTYPE", mytype = SqlDbType.NVarChar, Value = uProfile.BLOODTYPE });
                Connection.myparameters.Add(new myParameters { ParameterName = "@RELIGION", mytype = SqlDbType.NVarChar, Value = uProfile.RELIGION });
                Connection.myparameters.Add(new myParameters { ParameterName = "@SUFFIX", mytype = SqlDbType.NVarChar, Value = uProfile.SUFFIX });
                Connection.myparameters.Add(new myParameters { ParameterName = "@TOWN", mytype = SqlDbType.NVarChar, Value = uProfile.TOWN });
                Connection.myparameters.Add(new myParameters { ParameterName = "@HEIGHT", mytype = SqlDbType.NVarChar, Value = uProfile.HEIGHT });
                Connection.myparameters.Add(new myParameters { ParameterName = "@WEIGHT", mytype = SqlDbType.NVarChar, Value = uProfile.WEIGHT });


                Connection.myparameters.Add(new myParameters { ParameterName = "@FATHERNAME", mytype = SqlDbType.NVarChar, Value = uProfile.FATHERNANME });
                Connection.myparameters.Add(new myParameters { ParameterName = "@MOTHERNAME", mytype = SqlDbType.NVarChar, Value = uProfile.MOTHERNAME });
                Connection.myparameters.Add(new myParameters { ParameterName = "@PREFFIX", mytype = SqlDbType.NVarChar, Value = uProfile.PREFFIX });

                //ADDED NEW DATA END//

                String ID = Connection.ExecuteScalar("sp_Insert_EmployeeProfile_FromMobilev4");


                ExcelWorkbookFormat workbookFormat = ExcelWorkbookFormat.Xls_2003;
                string testDocFile = dataDir + "CRT-UPDATE.xls";         
                ExcelWorkbook workbook = new ExcelWorkbook(testDocFile);
                System.Web.HttpResponse httpResponse = System.Web.HttpContext.Current.Response;
                httpResponse.Clear();
                httpResponse.ContentType = "Application/x-msexcel";
                httpResponse.AddHeader("Content-Disposition", String.Format("attachment; filename={0}", filename));
                ExcelWorksheet firstWorksheet = workbook.Worksheets[1];
                string newDIR = HostingEnvironment.MapPath("~/sFTP/");
                workbook.Save(newDIR + filename);
                //int count = firstWorksheet.UsedRange.Rows.Count;
                int intRow = 11;
                int empReg = 0;
                while (!(string.IsNullOrEmpty(firstWorksheet["B"+intRow].Text)))
                {
                    intRow++;
                    empReg++;
                }
                firstWorksheet["B" + intRow].Text = uProfile.LASTNAME;
                firstWorksheet["C" + intRow].Text = uProfile.FIRSTNAME;
                firstWorksheet["D" + intRow].Text = uProfile.MIDDLENAME;
                firstWorksheet["E" + intRow].Text = uProfile.SUFFIX;
                firstWorksheet["F" + intRow].Text = uProfile.CARDNAME;
                firstWorksheet["G"+intRow].Text = Convert.ToDateTime(uProfile.BIRTHDAY).ToString("MM-dd-yyyy");
                firstWorksheet["H"+intRow].Text = uProfile.PLACEOFBIRTH;
                firstWorksheet["I"+intRow].Text = uProfile.NATIONALITY;
                firstWorksheet["J"+intRow].Text = uProfile.NATCOUNTRY;
                firstWorksheet["K"+intRow].Text = uProfile.GENDER;
                firstWorksheet["L"+intRow].Text = uProfile.MOBILE;
                firstWorksheet["M"+intRow].Text = uProfile.TELPHONE;
                firstWorksheet["N"+intRow].Text = uProfile.PRESHOUSENUMBER;
                firstWorksheet["O"+intRow].Text = uProfile.PRESHOUSEADDRESS;
                firstWorksheet["P"+intRow].Text = uProfile.PRESCOUNTRY;
                firstWorksheet["Q"+intRow].Text = uProfile.PRESREGION;
                firstWorksheet["R"+intRow].Text = uProfile.PRESPROVINCE;
                firstWorksheet["S"+intRow].Text = uProfile.PRESCITY;
                firstWorksheet["T"+intRow].Text = uProfile.PRESZIPCODE;
                firstWorksheet["U"+intRow].Text = uProfile.PERMHOUSENUMBER;
                firstWorksheet["V"+intRow].Text = uProfile.PERMHOUSEADDRESS;
                firstWorksheet["W"+intRow].Text = uProfile.PERMCOUNTRY;
                firstWorksheet["X"+intRow].Text = uProfile.PERMREGION;
                firstWorksheet["Y"+intRow].Text = uProfile.PERMPROVINCE;
                firstWorksheet["Z"+intRow].Text = uProfile.PERMCITY;
                firstWorksheet["AA"+intRow].Text = uProfile.PERMZIPCODE;
                firstWorksheet["AB"+intRow].Text = uProfile.EMAILADD;
                firstWorksheet["AC"+intRow].Text = uProfile.EMPLOYERNAME;
                firstWorksheet["AD"+intRow].Text = uProfile.TIN;
                firstWorksheet["AE"+intRow].Text = uProfile.CIVILSTATUS;
                firstWorksheet["AF"+intRow].Text = uProfile.MOTHLASTNAME;
                firstWorksheet["AG"+intRow].Text = uProfile.MOTHFIRSTNAME;
                firstWorksheet["AH"+intRow].Text = uProfile.MOTHMIDDLENAME;
                firstWorksheet["AI"+intRow].Text = uProfile.MOTHSUFFIX;
                firstWorksheet["AJ"+intRow].Text = uProfile.NUMBEROFPADS;
                firstWorksheet["AK"+intRow].Text = uProfile.OTHERDETAILS;

              

                //GenerateEPayCard_BAOpeningForm(uProfile.CN);

                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = uProfile.NTID });
                System.Data.DataTable dt = con.GetDataTable("sp_getEmpCompany");
                if (dt.Rows.Count > 0)
                {
                    firstWorksheet["C3"].Text = dt.Rows[0][1].ToString();
                    //firstWorksheet["C6"].Text = dt.Rows[0][2].ToString();
                    firstWorksheet["C6"].Text = empReg.ToString();
                    firstWorksheet["C7"].Text = dt.Rows[0][3].ToString();
                }
                httpResponse = System.Web.HttpContext.Current.Response;

                httpResponse.Clear();
                httpResponse.ContentType = "Application/x-msexcel";
                httpResponse.AddHeader("Content-Disposition", String.Format("attachment; filename={0}", filename));
                newDIR = HostingEnvironment.MapPath("~/sFTP/");
                workbook.Save(newDIR + filename);
                return "Success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }
        [HttpPost]
        [Route("DisplayInsertTxtMsg")]
        public TxtInfoList DisplayInsertTxtMsg(ImpactTxtInfo GetTxtMsg)
        {

            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = "XDB_ImpactHack";
            SelfieRegistration2Controller.GetDB2(GDB);
            TxtInfoList ListUs = new TxtInfoList();

            List<ImpactTxtInfo> ListReturned = new List<ImpactTxtInfo>();
            System.Data.DataTable DTProfile = new System.Data.DataTable();

            Connection con = new Connection();
            DTProfile = con.GetDataTable("sp_GetMsg");
            if (DTProfile.Rows.Count > 0)
            {
                ImpactTxtInfo MPC = new ImpactTxtInfo();

                MPC.Number = DTProfile.Rows[0]["Number"].ToString();
                MPC.TxtMsg = DTProfile.Rows[0]["TxtMsg"].ToString();
            }
            ListUs.TxtInfoListing = ListReturned;
            ListUs.myreturn = "Success";
            return ListUs;
        }
        #region
        [HttpPost]
        [Route("InsertUserProfilev5")]
        public string UserProfilev5(UserProfilev4 uProfile)
        {

            try
            {
                string dataDir = HostingEnvironment.MapPath("~/sFTP/");
                //FileStream stream = new FileStream(dataDir + "CRT-UPDATE.xls", FileMode.Open);
                //ExcelWriter writer = new ExcelWriter(stream);


                string filename;
                //filename = uProfile.NTID + "_" + DateTime.Now.Month.ToString().PadLeft(2, '0') + DateTime.Now.Day.ToString().PadLeft(2, '0') + DateTime.Now.Year.ToString().Substring(2, 2) + "_CRT-UPDATEOCT2016.xls";
                filename = "CRT-UPDATE.xls";
                //using (var fileStream = new FileStream(dataDir + filename, FileMode.Create, FileAccess.Write))
                //{
                //    stream.CopyTo(fileStream);
                //}
                //stream.Close();


                //firstWorksheet["C7"].Text = "Test";
                GetDatabase GDB = new GetDatabase();
                GDB.ClientName = uProfile.CN;
                SelfieRegistration2Controller.GetDB2(GDB);

                Connection Connection = new Connection();

                Connection.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = uProfile.NTID });
                Connection.myparameters.Add(new myParameters { ParameterName = "@CardNum", mytype = SqlDbType.NVarChar, Value = uProfile.CARDNAME });
                Connection.myparameters.Add(new myParameters { ParameterName = "@DOB", mytype = SqlDbType.NVarChar, Value = Convert.ToDateTime(uProfile.BIRTHDAY).ToString("yyyy-MM-dd") });
                Connection.myparameters.Add(new myParameters { ParameterName = "@POB", mytype = SqlDbType.NVarChar, Value = uProfile.PLACEOFBIRTH });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Natioanality", mytype = SqlDbType.NVarChar, Value = uProfile.NATIONALITY });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Country", mytype = SqlDbType.NVarChar, Value = uProfile.NATCOUNTRY });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Gender", mytype = SqlDbType.NVarChar, Value = uProfile.GENDER });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Mobile", mytype = SqlDbType.NVarChar, Value = uProfile.MOBILE });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Landline", mytype = SqlDbType.NVarChar, Value = uProfile.TELPHONE });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Per_Zipcode", mytype = SqlDbType.NVarChar, Value = uProfile.PERMZIPCODE });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Per_HouseNum", mytype = SqlDbType.NVarChar, Value = uProfile.PERMHOUSENUMBER });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Per_St_VIllage_Sub", mytype = SqlDbType.NVarChar, Value = uProfile.PERMHOUSEADDRESS });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Per_Brgy", mytype = SqlDbType.NVarChar, Value = uProfile.PERMBRGY });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Per_Muni", mytype = SqlDbType.NVarChar, Value = uProfile.PERMUNI });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Per_City", mytype = SqlDbType.NVarChar, Value = uProfile.PERMCITY });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Per_Region", mytype = SqlDbType.NVarChar, Value = uProfile.PERMREGION });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Per_Country", mytype = SqlDbType.NVarChar, Value = uProfile.PERMCOUNTRY });


                Connection.myparameters.Add(new myParameters { ParameterName = "@Pre_Zipcode", mytype = SqlDbType.NVarChar, Value = uProfile.PRESZIPCODE });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Pre_HouseNum", mytype = SqlDbType.NVarChar, Value = uProfile.PRESHOUSENUMBER });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Pre_St_VIllage_Sub", mytype = SqlDbType.NVarChar, Value = uProfile.PRESHOUSEADDRESS });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Pre_Brgy", mytype = SqlDbType.NVarChar, Value = uProfile.PRESBRGY });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Pre_Muni", mytype = SqlDbType.NVarChar, Value = uProfile.PRESMUNI });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Pre_City", mytype = SqlDbType.NVarChar, Value = uProfile.PRESCITY });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Pre_Region", mytype = SqlDbType.NVarChar, Value = uProfile.PRESREGION });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Pre_Country", mytype = SqlDbType.NVarChar, Value = uProfile.PRESCOUNTRY });


                Connection.myparameters.Add(new myParameters { ParameterName = "@email", mytype = SqlDbType.NVarChar, Value = uProfile.EMAILADD });
                Connection.myparameters.Add(new myParameters { ParameterName = "@EmployerName", mytype = SqlDbType.NVarChar, Value = uProfile.EMPLOYERNAME });
                Connection.myparameters.Add(new myParameters { ParameterName = "@TIN", mytype = SqlDbType.NVarChar, Value = uProfile.TIN });
                Connection.myparameters.Add(new myParameters { ParameterName = "@CivilStatus", mytype = SqlDbType.NVarChar, Value = uProfile.CIVILSTATUS });
                Connection.myparameters.Add(new myParameters { ParameterName = "@MothersMaidenLName", mytype = SqlDbType.NVarChar, Value = uProfile.MOTHSUFFIX });
                Connection.myparameters.Add(new myParameters { ParameterName = "@MothersFname", mytype = SqlDbType.NVarChar, Value = uProfile.MOTHFIRSTNAME });
                Connection.myparameters.Add(new myParameters { ParameterName = "@MothersMName", mytype = SqlDbType.NVarChar, Value = uProfile.MOTHLASTNAME });
                Connection.myparameters.Add(new myParameters { ParameterName = "@OtherDetails", mytype = SqlDbType.NVarChar, Value = uProfile.OTHERDETAILS });

                Connection.myparameters.Add(new myParameters { ParameterName = "@COMPANYNAME", mytype = SqlDbType.NVarChar, Value = uProfile.COMPANYNAME });
                Connection.myparameters.Add(new myParameters { ParameterName = "@SPECIALIZATION", mytype = SqlDbType.NVarChar, Value = uProfile.SPECIALIZATION });
                Connection.myparameters.Add(new myParameters { ParameterName = "@ROLEPOSITIONTITLE", mytype = SqlDbType.NVarChar, Value = uProfile.ROLEPOSITIONTITLE });
                Connection.myparameters.Add(new myParameters { ParameterName = "@POSITIONLEVEL", mytype = SqlDbType.NVarChar, Value = uProfile.POSITIONLEVEL });
                Connection.myparameters.Add(new myParameters { ParameterName = "@JOINEDFROM", mytype = SqlDbType.NVarChar, Value = uProfile.JOINEDFROM });
                Connection.myparameters.Add(new myParameters { ParameterName = "@JOINEDTO", mytype = SqlDbType.NVarChar, Value = uProfile.JOINEDTO });
                Connection.myparameters.Add(new myParameters { ParameterName = "@INDUSTRY", mytype = SqlDbType.NVarChar, Value = uProfile.INDUSTRY });
                Connection.myparameters.Add(new myParameters { ParameterName = "@COUNTRY2", mytype = SqlDbType.NVarChar, Value = uProfile.COUNTRY });
                Connection.myparameters.Add(new myParameters { ParameterName = "@EXPERIENCE", mytype = SqlDbType.NVarChar, Value = uProfile.EXPERIENCE });
                Connection.myparameters.Add(new myParameters { ParameterName = "@PositionTitle", mytype = SqlDbType.NVarChar, Value = uProfile.PositionTitle });
                Connection.myparameters.Add(new myParameters { ParameterName = "@MaritalStatus", mytype = SqlDbType.NVarChar, Value = uProfile.MaritalStatus });
                Connection.myparameters.Add(new myParameters { ParameterName = "@MarriedDate", mytype = SqlDbType.NVarChar, Value = uProfile.Marrieddate });

                //ADDED NEW DATA START//

                Connection.myparameters.Add(new myParameters { ParameterName = "@BLOODTYPE", mytype = SqlDbType.NVarChar, Value = uProfile.BLOODTYPE });
                Connection.myparameters.Add(new myParameters { ParameterName = "@RELIGION", mytype = SqlDbType.NVarChar, Value = uProfile.RELIGION });
                Connection.myparameters.Add(new myParameters { ParameterName = "@SUFFIX", mytype = SqlDbType.NVarChar, Value = uProfile.SUFFIX });
                Connection.myparameters.Add(new myParameters { ParameterName = "@TOWN", mytype = SqlDbType.NVarChar, Value = uProfile.TOWN });
                Connection.myparameters.Add(new myParameters { ParameterName = "@HEIGHT", mytype = SqlDbType.NVarChar, Value = uProfile.HEIGHT });
                Connection.myparameters.Add(new myParameters { ParameterName = "@WEIGHT", mytype = SqlDbType.NVarChar, Value = uProfile.WEIGHT });


                Connection.myparameters.Add(new myParameters { ParameterName = "@FATHERNAME", mytype = SqlDbType.NVarChar, Value = uProfile.FATHERNANME });
                Connection.myparameters.Add(new myParameters { ParameterName = "@MOTHERNAME", mytype = SqlDbType.NVarChar, Value = uProfile.MOTHERNAME });
                Connection.myparameters.Add(new myParameters { ParameterName = "@PREFFIX", mytype = SqlDbType.NVarChar, Value = uProfile.PREFFIX });

                //ADDED NEW DATA END//

                //ADDED NEW DATA START COMPLETE ADDRESS
                Connection.myparameters.Add(new myParameters { ParameterName = "@PropertyType", mytype = SqlDbType.NVarChar, Value = uProfile.PREPROPTYPE });
                Connection.myparameters.Add(new myParameters { ParameterName = "@BldgNo", mytype = SqlDbType.NVarChar, Value = uProfile.PREBLDGNO });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Place", mytype = SqlDbType.NVarChar, Value = uProfile.PREPLACE });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Wing", mytype = SqlDbType.NVarChar, Value = uProfile.PREWING });
                Connection.myparameters.Add(new myParameters { ParameterName = "@UnitNo", mytype = SqlDbType.NVarChar, Value = uProfile.PREUNITNO });
                Connection.myparameters.Add(new myParameters { ParameterName = "@FloorNo", mytype = SqlDbType.NVarChar, Value = uProfile.PREFLOORNO });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Village", mytype = SqlDbType.NVarChar, Value = uProfile.PREVILLAGE });
                Connection.myparameters.Add(new myParameters { ParameterName = "@PhaseNo", mytype = SqlDbType.NVarChar, Value = uProfile.PREPHASENO });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Subdivision", mytype = SqlDbType.NVarChar, Value = uProfile.PRESUBDIVISION });
                Connection.myparameters.Add(new myParameters { ParameterName = "@RoomNo", mytype = SqlDbType.NVarChar, Value = uProfile.PREROOMNO });
                Connection.myparameters.Add(new myParameters { ParameterName = "@BlkNo", mytype = SqlDbType.NVarChar, Value = uProfile.PREBLKNO });
                Connection.myparameters.Add(new myParameters { ParameterName = "@LotNo", mytype = SqlDbType.NVarChar, Value = uProfile.PRELOTNO });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Street", mytype = SqlDbType.NVarChar, Value = uProfile.PRESTREET });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Brgyname", mytype = SqlDbType.NVarChar, Value = uProfile.PREBRGYNAME });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Region", mytype = SqlDbType.NVarChar, Value = uProfile.PRE_REGION });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Province", mytype = SqlDbType.NVarChar, Value = uProfile.PRE_PROVINCE });
                Connection.myparameters.Add(new myParameters { ParameterName = "@CityMuni", mytype = SqlDbType.NVarChar, Value = uProfile.PRE_CITYMUNI });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Zipcode", mytype = SqlDbType.NVarChar, Value = uProfile.PRE_ZIPCODE });
                Connection.myparameters.Add(new myParameters { ParameterName = "@isFalse", mytype = SqlDbType.NVarChar, Value = uProfile.ISSAME });
                Connection.myparameters.Add(new myParameters { ParameterName = "@PreCountry", mytype = SqlDbType.NVarChar, Value = uProfile.PRE_COUNTRY });

                Connection.myparameters.Add(new myParameters { ParameterName = "@PermaPropertyType", mytype = SqlDbType.NVarChar, Value = uProfile.PERPROPTYPE });
                Connection.myparameters.Add(new myParameters { ParameterName = "@PermaBldgNo", mytype = SqlDbType.NVarChar, Value = uProfile.PERBLDGNO });
                Connection.myparameters.Add(new myParameters { ParameterName = "@PermaPlace", mytype = SqlDbType.NVarChar, Value = uProfile.PERPLACE });
                Connection.myparameters.Add(new myParameters { ParameterName = "@PermaWing", mytype = SqlDbType.NVarChar, Value = uProfile.PERWING });
                Connection.myparameters.Add(new myParameters { ParameterName = "@PermaUnitNo", mytype = SqlDbType.NVarChar, Value = uProfile.PERUNITNO });
                Connection.myparameters.Add(new myParameters { ParameterName = "@PermaFloorNo", mytype = SqlDbType.NVarChar, Value = uProfile.PERFLOORNO });
                Connection.myparameters.Add(new myParameters { ParameterName = "@PermaVillage", mytype = SqlDbType.NVarChar, Value = uProfile.PERVILLAGE });
                Connection.myparameters.Add(new myParameters { ParameterName = "@PermaPhaseNo", mytype = SqlDbType.NVarChar, Value = uProfile.PERPHASENO });
                Connection.myparameters.Add(new myParameters { ParameterName = "@PermaSubdivision", mytype = SqlDbType.NVarChar, Value = uProfile.PERSUBDIVISION });
                Connection.myparameters.Add(new myParameters { ParameterName = "@PermaRoomNo", mytype = SqlDbType.NVarChar, Value = uProfile.PERROOMNO });
                Connection.myparameters.Add(new myParameters { ParameterName = "@PermaBlkNo", mytype = SqlDbType.NVarChar, Value = uProfile.PERBLKNO });
                Connection.myparameters.Add(new myParameters { ParameterName = "@PermaLotNo", mytype = SqlDbType.NVarChar, Value = uProfile.PERLOTNO });
                Connection.myparameters.Add(new myParameters { ParameterName = "@PermaStreet", mytype = SqlDbType.NVarChar, Value = uProfile.PERSTREET });
                Connection.myparameters.Add(new myParameters { ParameterName = "@PermaBrgyname", mytype = SqlDbType.NVarChar, Value = uProfile.PERBRGYNAME });
                Connection.myparameters.Add(new myParameters { ParameterName = "@PermaRegion", mytype = SqlDbType.NVarChar, Value = uProfile.PER_REGION });
                Connection.myparameters.Add(new myParameters { ParameterName = "@PermaProvince", mytype = SqlDbType.NVarChar, Value = uProfile.PER_PROVINCE });
                Connection.myparameters.Add(new myParameters { ParameterName = "@PermaCityMuni", mytype = SqlDbType.NVarChar, Value = uProfile.PER_CITYMUNI });
                Connection.myparameters.Add(new myParameters { ParameterName = "@PermaZipcode", mytype = SqlDbType.NVarChar, Value = uProfile.PER_ZIPCODE });
                Connection.myparameters.Add(new myParameters { ParameterName = "@PermaCountry", mytype = SqlDbType.NVarChar, Value = uProfile.PER_COUNTRY });
                //ADDED NEW DATA END COMPLETE ADDRESS
                Connection.myparameters.Add(new myParameters { ParameterName = "@Ref1FNAME", mytype = SqlDbType.NVarChar, Value = uProfile.Ref1FNAME });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Ref1LNAME", mytype = SqlDbType.NVarChar, Value = uProfile.Ref1LNAME });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Ref1Contact", mytype = SqlDbType.NVarChar, Value = uProfile.Ref1Contact });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Ref2FNAME", mytype = SqlDbType.NVarChar, Value = uProfile.Ref2FNAME });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Ref2LNAME", mytype = SqlDbType.NVarChar, Value = uProfile.Ref2LNAME });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Ref2Contact", mytype = SqlDbType.NVarChar, Value = uProfile.Ref2Contact });

                Connection.myparameters.Add(new myParameters { ParameterName = "@DateJoin", mytype = SqlDbType.NVarChar, Value = uProfile.DateJoin });
                Connection.myparameters.Add(new myParameters { ParameterName = "@PositionLvl", mytype = SqlDbType.NVarChar, Value = uProfile.PositionLvl });

                String ID = Connection.ExecuteScalar("sp_Insert_EmployeeProfile_FromMobilev5");


                ExcelWorkbookFormat workbookFormat = ExcelWorkbookFormat.Xls_2003;
                string testDocFile = dataDir + "CRT-UPDATE.xls";
                ExcelWorkbook workbook = new ExcelWorkbook(testDocFile);
                System.Web.HttpResponse httpResponse = System.Web.HttpContext.Current.Response;
                httpResponse.Clear();
                httpResponse.ContentType = "Application/x-msexcel";
                httpResponse.AddHeader("Content-Disposition", String.Format("attachment; filename={0}", filename));
                ExcelWorksheet firstWorksheet = workbook.Worksheets[1];
                string newDIR = HostingEnvironment.MapPath("~/sFTP/");
                workbook.Save(newDIR + filename);
                //int count = firstWorksheet.UsedRange.Rows.Count;
                int intRow = 11;
                int empReg = 0;
                while (!(string.IsNullOrEmpty(firstWorksheet["B" + intRow].Text)))
                {
                    intRow++;
                    empReg++;
                }
                firstWorksheet["B" + intRow].Text = uProfile.LASTNAME;
                firstWorksheet["C" + intRow].Text = uProfile.FIRSTNAME;
                firstWorksheet["D" + intRow].Text = uProfile.MIDDLENAME;
                firstWorksheet["E" + intRow].Text = uProfile.SUFFIX;
                firstWorksheet["F" + intRow].Text = uProfile.CARDNAME;
                firstWorksheet["G" + intRow].Text = Convert.ToDateTime(uProfile.BIRTHDAY).ToString("MM-dd-yyyy");
                firstWorksheet["H" + intRow].Text = uProfile.PLACEOFBIRTH;
                firstWorksheet["I" + intRow].Text = uProfile.NATIONALITY;
                firstWorksheet["J" + intRow].Text = uProfile.NATCOUNTRY;
                firstWorksheet["K" + intRow].Text = uProfile.GENDER;
                firstWorksheet["L" + intRow].Text = uProfile.MOBILE;
                firstWorksheet["M" + intRow].Text = uProfile.TELPHONE;
                firstWorksheet["N" + intRow].Text = uProfile.PRESHOUSENUMBER;
                firstWorksheet["O" + intRow].Text = uProfile.PRESHOUSEADDRESS;
                firstWorksheet["P" + intRow].Text = uProfile.PRESCOUNTRY;
                firstWorksheet["Q" + intRow].Text = uProfile.PRESREGION;
                firstWorksheet["R" + intRow].Text = uProfile.PRESPROVINCE;
                firstWorksheet["S" + intRow].Text = uProfile.PRESCITY;
                firstWorksheet["T" + intRow].Text = uProfile.PRESZIPCODE;
                firstWorksheet["U" + intRow].Text = uProfile.PERMHOUSENUMBER;
                firstWorksheet["V" + intRow].Text = uProfile.PERMHOUSEADDRESS;
                firstWorksheet["W" + intRow].Text = uProfile.PERMCOUNTRY;
                firstWorksheet["X" + intRow].Text = uProfile.PERMREGION;
                firstWorksheet["Y" + intRow].Text = uProfile.PERMPROVINCE;
                firstWorksheet["Z" + intRow].Text = uProfile.PERMCITY;
                firstWorksheet["AA" + intRow].Text = uProfile.PERMZIPCODE;
                firstWorksheet["AB" + intRow].Text = uProfile.EMAILADD;
                firstWorksheet["AC" + intRow].Text = uProfile.EMPLOYERNAME;
                firstWorksheet["AD" + intRow].Text = uProfile.TIN;
                firstWorksheet["AE" + intRow].Text = uProfile.CIVILSTATUS;
                firstWorksheet["AF" + intRow].Text = uProfile.MOTHLASTNAME;
                firstWorksheet["AG" + intRow].Text = uProfile.MOTHFIRSTNAME;
                firstWorksheet["AH" + intRow].Text = uProfile.MOTHMIDDLENAME;
                firstWorksheet["AI" + intRow].Text = uProfile.MOTHSUFFIX;
                firstWorksheet["AJ" + intRow].Text = uProfile.NUMBEROFPADS;
                firstWorksheet["AK" + intRow].Text = uProfile.OTHERDETAILS;



                //GenerateEPayCard_BAOpeningForm(uProfile.CN);

                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = uProfile.NTID });
                System.Data.DataTable dt = con.GetDataTable("sp_getEmpCompany");
                if (dt.Rows.Count > 0)
                {
                    firstWorksheet["C3"].Text = dt.Rows[0][1].ToString();
                    //firstWorksheet["C6"].Text = dt.Rows[0][2].ToString();
                    firstWorksheet["C6"].Text = empReg.ToString();
                    firstWorksheet["C7"].Text = dt.Rows[0][3].ToString();
                }
                httpResponse = System.Web.HttpContext.Current.Response;

                httpResponse.Clear();
                httpResponse.ContentType = "Application/x-msexcel";
                httpResponse.AddHeader("Content-Disposition", String.Format("attachment; filename={0}", filename));
                newDIR = HostingEnvironment.MapPath("~/sFTP/");
                workbook.Save(newDIR + filename);
                return "Success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }
        #endregion

        [HttpPost]
        [Route("GenerateEPayCard_BAOpeningForm")]
        public string GenerateEPayCard_BAOpeningForm()
        {
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = "";
            SelfieRegistration2Controller.GetDB2(GDB);

            System.Data.DataTable CompInfo = new System.Data.DataTable();
            System.Data.DataTable EmpInfo = new System.Data.DataTable();

            Connection con = new Connection();
            CompInfo = con.GetDataTable("sp_GetUBCompanyInfo");

            Connection conn = new Connection();
            EmpInfo = conn.GetDataTable("sp_GetEPayCardInformation");


            try
            {
                string dataDir = HostingEnvironment.MapPath("~/Files/");
                FileStream stream = new FileStream(dataDir + "ePaycard_BAOpeningForm.xlsx", FileMode.Open);
                //ExcelWriter writer = new ExcelWriter(stream);


                string filename;
                filename = DateTime.Now.Year.ToString()+"-ePaycard_BAOpeningForm.xlsx";
                using (var fileStream = new FileStream(dataDir + filename, FileMode.Create, FileAccess.Write))
                {
                    stream.CopyTo(fileStream);
                }
                stream.Close();

                ExcelWorkbookFormat workbookFormat = ExcelWorkbookFormat.Xls_2003;
                string testDocFile = dataDir + "ePaycard_BAOpeningForm.xlsx";
                ExcelWorkbook workbook = new ExcelWorkbook(testDocFile);
                ExcelWorksheet firstWorksheet = workbook.Worksheets[0];

                firstWorksheet["C25"].Text = CompInfo.Rows[0][0].ToString();
                firstWorksheet["C26"].Text = CompInfo.Rows[0][1].ToString();
                firstWorksheet["C27"].Text = CompInfo.Rows[0][2].ToString();
                firstWorksheet["C28"].Text = CompInfo.Rows[0][3].ToString();
                firstWorksheet["C29"].Text = CompInfo.Rows[0][4].ToString();
                firstWorksheet["C30"].Text = CompInfo.Rows[0][5].ToString();
                firstWorksheet["E30"].Text = CompInfo.Rows[0][6].ToString();



                int cellnum = 35;
                for (int i = 0; i < EmpInfo.Rows.Count; i++)
                {
                    firstWorksheet["B" + cellnum].Text = EmpInfo.Rows[i][0].ToString();
                    firstWorksheet["C" + cellnum].Text = EmpInfo.Rows[i][1].ToString();
                    firstWorksheet["D" + cellnum].Text = EmpInfo.Rows[i][2].ToString();
                    firstWorksheet["E" + cellnum].Text = EmpInfo.Rows[i][3].ToString();
                    firstWorksheet["F" + cellnum].Text = EmpInfo.Rows[i][4].ToString();
                    firstWorksheet["G" + cellnum].Text = EmpInfo.Rows[i][5].ToString();
                    firstWorksheet["H" + cellnum].Text = EmpInfo.Rows[i][6].ToString();
                    firstWorksheet["I" + cellnum].Text = EmpInfo.Rows[i][7].ToString();
                    firstWorksheet["J" + cellnum].Text = EmpInfo.Rows[i][8].ToString();
                    firstWorksheet["K" + cellnum].Text = EmpInfo.Rows[i][9].ToString();
                    firstWorksheet["L" + cellnum].Text = EmpInfo.Rows[i][10].ToString();
                    firstWorksheet["M" + cellnum].Text = EmpInfo.Rows[i][11].ToString();
                    firstWorksheet["O" + cellnum].Text = EmpInfo.Rows[i][12].ToString();
                    firstWorksheet["P" + cellnum].Text = EmpInfo.Rows[i][13].ToString();
                    cellnum++;
                }

                System.Web.HttpResponse httpResponse = System.Web.HttpContext.Current.Response;

                httpResponse.Clear();
                httpResponse.ContentType = "Application/x-msexcel";
                httpResponse.AddHeader("Content-Disposition", String.Format("attachment; filename={0}", filename));
                string newDIR = HostingEnvironment.MapPath("~/sFTP/");
                workbook.Save(newDIR + filename);

                return "Success";
            }
            catch (Exception e)
            {
                return "error " + e;
            }
        }

        
        #region DisplayComploteProfilev4
        [HttpPost]
        [Route("DisplayCompleteProfilev4")]
        public Informationv4 DisplayCompleteProfilev4(Loginv2 Log)
        {

            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = Log.CN;
            SelfieRegistration2Controller.GetDB2(GDB);

            Informationv4 ListUs = new Informationv4();

            List<Master_User_Info_Personal_Confi_Infov4> ListReturned = new List<Master_User_Info_Personal_Confi_Infov4>();
            System.Data.DataTable DTProfile = new System.Data.DataTable();

            Connection con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@empId", mytype = SqlDbType.NVarChar, Value = Log.LoginID });
            DTProfile = con.GetDataTable("sp_getCompleteProfile_Mobilev3");
            if (DTProfile.Rows.Count > 0)
            {
                Master_User_Info_Personal_Confi_Infov4 MPC = new Master_User_Info_Personal_Confi_Infov4();

                MPC.SeriesID = DTProfile.Rows[0]["SeriesID"].ToString();
                MPC.EmpID = DTProfile.Rows[0]["EmpID"].ToString();
                MPC.First_Name = DTProfile.Rows[0]["First_Name"].ToString();
                MPC.Middle_Name = DTProfile.Rows[0]["Middle_Name"].ToString();
                MPC.Last_Name = DTProfile.Rows[0]["Last_Name"].ToString();
                MPC.NTID = DTProfile.Rows[0]["NTID"].ToString();
                MPC.EmpName = DTProfile.Rows[0]["EmpName"].ToString();
                MPC.EmpType = DTProfile.Rows[0]["EmpType"].ToString();
                MPC.EmpLevel = DTProfile.Rows[0]["EmpLevel"].ToString();
                MPC.EmpStatus = DTProfile.Rows[0]["EmpStatus"].ToString();
                MPC.UserType = DTProfile.Rows[0]["UserType"].ToString();
                MPC.Gender = DTProfile.Rows[0]["Gender"].ToString();
                MPC.DateJoined = DTProfile.Rows[0]["DateJoined"].ToString();
                MPC.ContractEndDate = DTProfile.Rows[0]["ContractEndDate"].ToString();
                MPC.LeaveBal = DTProfile.Rows[0]["LeaveBal"].ToString();
                MPC.LeaveBalCTO = DTProfile.Rows[0]["LeaveBalCTO"].ToString();
                MPC.Country = DTProfile.Rows[0]["Country"].ToString();
                MPC.JobDesc = DTProfile.Rows[0]["JobDesc"].ToString();
                MPC.DeptCode = DTProfile.Rows[0]["DeptCode"].ToString();
                MPC.DeptName = DTProfile.Rows[0]["DeptName"].ToString();
                MPC.MngrID = DTProfile.Rows[0]["MngrID"].ToString();
                MPC.MngrName = DTProfile.Rows[0]["MngrName"].ToString();
                MPC.MngrNTID = DTProfile.Rows[0]["MngrNTID"].ToString();
                MPC.BusinessUnit = DTProfile.Rows[0]["BusinessUnit"].ToString();
                MPC.BusinessUnitHead = DTProfile.Rows[0]["BusinessUnitHead"].ToString();
                MPC.BusinessSegment = DTProfile.Rows[0]["BusinessSegment"].ToString();
                MPC.AccessLevel = DTProfile.Rows[0]["AccessLevel"].ToString();
                MPC.DOB = DTProfile.Rows[0]["DOB"].ToString();
                MPC.Alias = DTProfile.Rows[0]["Alias"].ToString();
                MPC.ExtensionNumber = DTProfile.Rows[0]["ExtensionNumber"].ToString();
                MPC.Skill = DTProfile.Rows[0]["Skill"].ToString();
                MPC.Batch = DTProfile.Rows[0]["Batch"].ToString();
                MPC.Class = DTProfile.Rows[0]["Class"].ToString();
                MPC.Workstation = DTProfile.Rows[0]["Workstation"].ToString();
                MPC.ProductionStartDate = DTProfile.Rows[0]["ProductionStartDate"].ToString();
                MPC.ModifiedBy = DTProfile.Rows[0]["ModifiedBy"].ToString();
                MPC.DateModified = DTProfile.Rows[0]["DateModified"].ToString();
                MPC.EmpPayHrsCat = DTProfile.Rows[0]["EmpPayHrsCat"].ToString();
                MPC.Salary = DTProfile.Rows[0]["Salary"].ToString();
                MPC.DailyRate = DTProfile.Rows[0]["DailyRate"].ToString();
                MPC.EcolaWageOrder = DTProfile.Rows[0]["EcolaWageOrder"].ToString();
                MPC.EcolaRate = DTProfile.Rows[0]["EcolaRate"].ToString();
                MPC.Transportation = DTProfile.Rows[0]["Transportation"].ToString();
                MPC.Rice = DTProfile.Rows[0]["Rice"].ToString();
                MPC.Communication = DTProfile.Rows[0]["Communication"].ToString();
                MPC.EcolaRegion = DTProfile.Rows[0]["EcolaRegion"].ToString();
                MPC.Branch = DTProfile.Rows[0]["Branch"].ToString();
                MPC.Wallet = DTProfile.Rows[0]["Wallet"].ToString();
                MPC.FaceID = DTProfile.Rows[0]["FaceID"].ToString();
                MPC.DTransFrom = DTProfile.Rows[0]["DTransFrom"].ToString();
                MPC.DTransTo = DTProfile.Rows[0]["DTransTo"].ToString();
                MPC.LocFrom = DTProfile.Rows[0]["LocFrom"].ToString();
                MPC.LocTo = DTProfile.Rows[0]["LocTo"].ToString();
                MPC.CCChangeFrom = DTProfile.Rows[0]["CCChangeFrom"].ToString();
                MPC.CCChangeTo = DTProfile.Rows[0]["CCChangeTo"].ToString();
                MPC.Email = DTProfile.Rows[0]["Email"].ToString();
                MPC.WorkEmail = DTProfile.Rows[0]["WorkEmail"].ToString();
                MPC.Extension = DTProfile.Rows[0]["Extension"].ToString();
                MPC.JoinDate = DTProfile.Rows[0]["JoinDate"].ToString();
                MPC.ProdStart = DTProfile.Rows[0]["ProdStart"].ToString();
                MPC.Tenure = DTProfile.Rows[0]["Tenure"].ToString();
                MPC.TenureMnth = DTProfile.Rows[0]["TenureMnth"].ToString();
                MPC.Batch1 = DTProfile.Rows[0]["Batch"].ToString();
                MPC.Class1 = DTProfile.Rows[0]["Class"].ToString();
                MPC.Skill1 = DTProfile.Rows[0]["Skill"].ToString();
                MPC.JobDesc1 = DTProfile.Rows[0]["JobDesc"].ToString();
                MPC.Promotion = DTProfile.Rows[0]["Promotion"].ToString();
                MPC.Status = DTProfile.Rows[0]["Status"].ToString();
                MPC.Emp_Name = DTProfile.Rows[0]["Emp_Name"].ToString();
                MPC.approved = DTProfile.Rows[0]["approved"].ToString();
                MPC.Employee_Level = DTProfile.Rows[0]["Employee_Level"].ToString();
                MPC.Employee_Category = DTProfile.Rows[0]["Employee_Category"].ToString();
                MPC.BusinessSegment1 = DTProfile.Rows[0]["BusinessSegment"].ToString();
                MPC.IsApprove = DTProfile.Rows[0]["IsApprove"].ToString();
                MPC.ResignedDate = DTProfile.Rows[0]["ResignedDate"].ToString();
                MPC.HouseNumber = DTProfile.Rows[0]["HouseNumber"].ToString();
                MPC.StreetName = DTProfile.Rows[0]["StreetName"].ToString();
                MPC.Barangay = DTProfile.Rows[0]["Barangay"].ToString();
                MPC.Town = DTProfile.Rows[0]["Town"].ToString();
                MPC.City = DTProfile.Rows[0]["City"].ToString();
                MPC.Region = DTProfile.Rows[0]["Region"].ToString();
                MPC.ZipCode = DTProfile.Rows[0]["ZipCode"].ToString();
                MPC.BloodType = DTProfile.Rows[0]["BloodType"].ToString();
                MPC.Gender1 = DTProfile.Rows[0]["Gender"].ToString();
                MPC.DOB1 = DTProfile.Rows[0]["DOB"].ToString();
                MPC.Citizenship = DTProfile.Rows[0]["Citizenship"].ToString();
                MPC.NameofOrganization = DTProfile.Rows[0]["NameofOrganization"].ToString();
                MPC.MobileAreaCode = DTProfile.Rows[0]["MobileAreaCode"].ToString();
                MPC.MobileNumber = DTProfile.Rows[0]["MobileNumber"].ToString();
                MPC.HomeAreaCode = DTProfile.Rows[0]["HomeAreaCode"].ToString();
                MPC.HomeNumber = DTProfile.Rows[0]["HomeNumber"].ToString();
                // PersonalEmail = dr["PersonalEmail"].ToString(),
                MPC.EmrgName = DTProfile.Rows[0]["EmrgName"].ToString();
                MPC.EmrgNumber = DTProfile.Rows[0]["EmrgNumber"].ToString();
                MPC.EmrgRelationship = DTProfile.Rows[0]["EmrgRelationship"].ToString();
                MPC.OptionalEmail = DTProfile.Rows[0]["OptionalEmail"].ToString();
                // ContactNo = dr["ContactNo"].ToString(),
                MPC.Province = DTProfile.Rows[0]["Province"].ToString();
                MPC.TaxStatus = DTProfile.Rows[0]["TaxStatus"].ToString();

                MPC.SSS = DTProfile.Rows[0]["SSS"].ToString();
                MPC.TIN = DTProfile.Rows[0]["TIN"].ToString();
                MPC.PhilHealth = DTProfile.Rows[0]["PhilHealth"].ToString();
                MPC.Pagibig = DTProfile.Rows[0]["Pagibig"].ToString();
                MPC.DriversLicense = DTProfile.Rows[0]["DriversLicense"].ToString();
                MPC.Passport = DTProfile.Rows[0]["Passport"].ToString();
                MPC.Unified = DTProfile.Rows[0]["Unified"].ToString();
                MPC.Postal = DTProfile.Rows[0]["Postal"].ToString();
                MPC.BirthCertificate = DTProfile.Rows[0]["BirthCertificate"].ToString();
                MPC.SssImage = DTProfile.Rows[0]["SssImage"].ToString();
                MPC.TinImage = DTProfile.Rows[0]["TinImage"].ToString();
                MPC.PhilImage = DTProfile.Rows[0]["PhilImage"].ToString();
                MPC.PagImage = DTProfile.Rows[0]["PagImage"].ToString();
                MPC.DriverImage = DTProfile.Rows[0]["DriverImage"].ToString();
                MPC.PassportImage = DTProfile.Rows[0]["PassportImage"].ToString();
                MPC.UmidImage = DTProfile.Rows[0]["UmidImage"].ToString();
                MPC.PostalImage = DTProfile.Rows[0]["PostalImage"].ToString();
                MPC.BirthImage = DTProfile.Rows[0]["BirthImage"].ToString();
                MPC.DriversExpiry = DTProfile.Rows[0]["DriversExpiry"].ToString();
                MPC.PassportExpiry = DTProfile.Rows[0]["PassportExpiry"].ToString();
                MPC.PostalExpiry = DTProfile.Rows[0]["PostalExpiry"].ToString();
                MPC.SAGSD = DTProfile.Rows[0]["SAGSD"].ToString();
                MPC.SAGSDExpiry = DTProfile.Rows[0]["SAGSDExpiry"].ToString();
                MPC.NTC = DTProfile.Rows[0]["NTC"].ToString();
                MPC.NTCExpiry = DTProfile.Rows[0]["NTCExpiry"].ToString();
                MPC.BrgyClearance = DTProfile.Rows[0]["BrgyClearance"].ToString();
                MPC.BrgyClearanceExpiry = DTProfile.Rows[0]["BrgyClearanceExpiry"].ToString();
                MPC.PNPClearance = DTProfile.Rows[0]["PNPClearance"].ToString();
                MPC.PNPClearanceExpiry = DTProfile.Rows[0]["PNPClearanceExpiry"].ToString();
                MPC.NBIClearance = DTProfile.Rows[0]["NBIClearance"].ToString();
                MPC.NBIClearanceExpiry = DTProfile.Rows[0]["NBIClearanceExpiry"].ToString();
                MPC.NueroEvaluation = DTProfile.Rows[0]["NueroEvaluation"].ToString();
                MPC.NeuroEvaluationExpiry = DTProfile.Rows[0]["NeuroEvaluationExpiry"].ToString();
                MPC.DrugTest = DTProfile.Rows[0]["DrugTest"].ToString();
                MPC.DrugTestExpiry = DTProfile.Rows[0]["DrugTestExpiry"].ToString();
                MPC.PlaceofBirth = DTProfile.Rows[0]["POB"].ToString();
                MPC.Religion = DTProfile.Rows[0]["Religion"].ToString();
                MPC.Height = DTProfile.Rows[0]["Height"].ToString();
                MPC.Weight = DTProfile.Rows[0]["Weight"].ToString();
                MPC.MothersName = DTProfile.Rows[0]["MotherName"].ToString();
                MPC.FathersName = DTProfile.Rows[0]["FatherName"].ToString();
                MPC.RegDate = DTProfile.Rows[0]["RegDate"].ToString();



                //ADDED COLUMN  START
                MPC.CONTACTNUMBER = DTProfile.Rows[0]["ContactNo"].ToString();
                MPC.INDUSTRY = DTProfile.Rows[0]["Industry"].ToString();
                MPC.MARITALSTATUS = DTProfile.Rows[0]["MaritalStatus"].ToString();
                if (DTProfile.Rows[0]["MaritalStatus"].ToString() == "Married")
                {
                    MPC.MARRIEDDATE = DTProfile.Rows[0]["MarriedDate"].ToString();
                }
                else
                {
                    MPC.MARRIEDDATE = "";
                }

                MPC.SUFFIX = DTProfile.Rows[0]["Suffix"].ToString();
                MPC.PREFFIX = DTProfile.Rows[0]["Preffix"].ToString();
                //ADDED COLUMN END

                System.Data.DataTable DTContactNo = new System.Data.DataTable();
                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = Log.LoginID });
                DTContactNo = con.GetDataTable("sp_getPersonalContactInfo");



                //Company Contact Number Start
                List<ContactNo> Clist = new List<ContactNo>();
                if (DTContactNo.Rows.Count > 0)
                {
                    for (int ii = 0; ii < DTContactNo.Rows.Count; ii++)
                    {
                        ContactNo Contact = new ContactNo();
                        Contact.Contact = DTContactNo.Rows[ii]["Number"].ToString();
                        Contact.ContactType = DTContactNo.Rows[ii]["NumberType"].ToString();
                        Clist.Add(Contact);
                    }
                }
                else
                {
                    ContactNo Contact = new ContactNo();
                    Contact.Contact = "";
                    Contact.ContactType = "";
                    Clist.Add(Contact);
                }
                MPC.ContactNo = Clist;
                //Company Contact Number End


                //Company Contact Email Start
                System.Data.DataTable DTEmail = new System.Data.DataTable();
                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = Log.LoginID });
                DTEmail = con.GetDataTable("sp_getPersonalEmailInfo");
                List<PersonalEmail> EMaillist = new List<PersonalEmail>();
                if (DTEmail.Rows.Count > 0)
                {
                    for (int iii = 0; iii < DTEmail.Rows.Count; iii++)
                    {
                        PersonalEmail Email = new PersonalEmail();
                        Email.Email = DTEmail.Rows[iii]["Email"].ToString();
                        Email.EmailType = DTEmail.Rows[iii]["EmailType"].ToString();
                        EMaillist.Add(Email);
                    }
                }
                else
                {
                    PersonalEmail Email = new PersonalEmail();
                    Email.Email = "";
                    Email.EmailType = "";
                    EMaillist.Add(Email);
                }
                MPC.PersonalEmail = EMaillist;
                //Company Contact Email End

                //COMPANY CONTACT START
                System.Data.DataTable DTCompContact = new System.Data.DataTable();
                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = Log.LoginID });
                DTCompContact = con.GetDataTable("sp_GetCompanyContactPhoneDHR");
                //DTCompEmail = con.GetDataTable("sp_GetCompanyEmailDHR");
                List<CompanyContact> CompanyContactList = new List<CompanyContact>();
                if (DTCompContact.Rows.Count > 0)
                {
                    for (int a = 0; a < DTCompContact.Rows.Count; a++)
                    {
                        CompanyContact CompanyC = new CompanyContact();
                        CompanyC.PhoneType = DTCompContact.Rows[a][0].ToString();
                        CompanyC.ContactNumber = DTCompContact.Rows[a][1].ToString();
                        CompanyC.ID = DTCompContact.Rows[a][2].ToString();
                        CompanyContactList.Add(CompanyC);
                    }
                }
                else
                {
                    CompanyContact CompanyC = new CompanyContact();
                    CompanyC.PhoneType = "";
                    CompanyC.ContactNumber = "";
                    CompanyC.ID = "";
                    CompanyContactList.Add(CompanyC);
                }


                MPC.CompanyContact = CompanyContactList;
                //COMPANY CONTACT END

                //COMPANY EMAIL START
                System.Data.DataTable DTCompEmail = new System.Data.DataTable();
                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = Log.LoginID });
                DTCompEmail = con.GetDataTable("sp_GetCompanyEmailDHR");
                //DTCompEmail = con.GetDataTable("sp_GetCompanyEmailDHR");
                List<CompanyEmail> CompanyEmailList = new List<CompanyEmail>();

                if (DTCompEmail.Rows.Count > 0)
                {
                    for (int aa = 0; aa < DTCompEmail.Rows.Count; aa++)
                    {
                        CompanyEmail CompanyE = new CompanyEmail();
                        CompanyE.EmailType = DTCompEmail.Rows[aa][0].ToString();
                        CompanyE.EmailData = DTCompEmail.Rows[aa][1].ToString();
                        CompanyE.ID = DTCompEmail.Rows[aa][2].ToString();
                        CompanyEmailList.Add(CompanyE);
                    }

                }
                else
                {
                    CompanyEmail CompanyE = new CompanyEmail();
                    CompanyE.EmailType = "";
                    CompanyE.EmailData = "";
                    CompanyE.ID = "";
                    CompanyEmailList.Add(CompanyE);
                }
                MPC.CompanyEmail = CompanyEmailList;
                //COMPANY EMAIL END 
                ListReturned.Add(MPC);
            }


            ListUs.CompleteProfile = ListReturned;
            ListUs.myreturn = "Success";
            return ListUs;
        }



        #endregion

        #region Roman 2019-09-02
        [HttpPost]
        [Route("DisplayCompleteProfilev5")]
        public Informationv4 DisplayCompleteProfilev5(Loginv2 Log)
        {
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = Log.CN;
            SelfieRegistration2Controller.GetDB2(GDB);

            Informationv4 ListUs = new Informationv4();

            List<Master_User_Info_Personal_Confi_Infov4> ListReturned = new List<Master_User_Info_Personal_Confi_Infov4>();
            System.Data.DataTable DTProfile = new System.Data.DataTable();

            Connection con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@empId", mytype = SqlDbType.NVarChar, Value = Log.LoginID });
            DTProfile = con.GetDataTable("sp_getCompleteProfile_Mobilev4");
            if (DTProfile.Rows.Count > 0)
            {
                Master_User_Info_Personal_Confi_Infov4 MPC = new Master_User_Info_Personal_Confi_Infov4();

                MPC.SeriesID = DTProfile.Rows[0]["SeriesID"].ToString();
                MPC.EmpID = DTProfile.Rows[0]["EmpID"].ToString();
                MPC.First_Name = DTProfile.Rows[0]["First_Name"].ToString();
                MPC.Middle_Name = DTProfile.Rows[0]["Middle_Name"].ToString();
                MPC.Last_Name = DTProfile.Rows[0]["Last_Name"].ToString();
                MPC.NTID = DTProfile.Rows[0]["NTID"].ToString();
                MPC.EmpName = DTProfile.Rows[0]["EmpName"].ToString();
                MPC.EmpType = DTProfile.Rows[0]["EmpType"].ToString();
                MPC.EmpLevel = DTProfile.Rows[0]["EmpLevel"].ToString();
                MPC.EmpStatus = DTProfile.Rows[0]["EmpStatus"].ToString();
                MPC.UserType = DTProfile.Rows[0]["UserType"].ToString();
                MPC.Gender = DTProfile.Rows[0]["Gender"].ToString();
                MPC.DateJoined = DTProfile.Rows[0]["DateJoined"].ToString();
                MPC.ContractEndDate = DTProfile.Rows[0]["ContractEndDate"].ToString();
                MPC.LeaveBal = DTProfile.Rows[0]["LeaveBal"].ToString();
                MPC.LeaveBalCTO = DTProfile.Rows[0]["LeaveBalCTO"].ToString();
                MPC.Country = DTProfile.Rows[0]["Country"].ToString();
                MPC.JobDesc = DTProfile.Rows[0]["JobDesc"].ToString();
                MPC.DeptCode = DTProfile.Rows[0]["DeptCode"].ToString();
                MPC.DeptName = DTProfile.Rows[0]["DeptName"].ToString();
                MPC.MngrID = DTProfile.Rows[0]["MngrID"].ToString();
                MPC.MngrName = DTProfile.Rows[0]["MngrName"].ToString();
                MPC.MngrNTID = DTProfile.Rows[0]["MngrNTID"].ToString();
                MPC.BusinessUnit = DTProfile.Rows[0]["BusinessUnit"].ToString();
                MPC.BusinessUnitHead = DTProfile.Rows[0]["BusinessUnitHead"].ToString();
                MPC.BusinessSegment = DTProfile.Rows[0]["BusinessSegment"].ToString();
                MPC.AccessLevel = DTProfile.Rows[0]["AccessLevel"].ToString();
                MPC.DOB = DTProfile.Rows[0]["DOB"].ToString();
                MPC.Alias = DTProfile.Rows[0]["Alias"].ToString();
                MPC.ExtensionNumber = DTProfile.Rows[0]["ExtensionNumber"].ToString();
                MPC.Skill = DTProfile.Rows[0]["Skill"].ToString();
                MPC.Batch = DTProfile.Rows[0]["Batch"].ToString();
                MPC.Class = DTProfile.Rows[0]["Class"].ToString();
                MPC.Workstation = DTProfile.Rows[0]["Workstation"].ToString();
                MPC.ProductionStartDate = DTProfile.Rows[0]["ProductionStartDate"].ToString();
                MPC.ModifiedBy = DTProfile.Rows[0]["ModifiedBy"].ToString();
                MPC.DateModified = DTProfile.Rows[0]["DateModified"].ToString();
                MPC.EmpPayHrsCat = DTProfile.Rows[0]["EmpPayHrsCat"].ToString();
                MPC.Salary = DTProfile.Rows[0]["Salary"].ToString();
                MPC.DailyRate = DTProfile.Rows[0]["DailyRate"].ToString();
                MPC.EcolaWageOrder = DTProfile.Rows[0]["EcolaWageOrder"].ToString();
                MPC.EcolaRate = DTProfile.Rows[0]["EcolaRate"].ToString();
                MPC.Transportation = DTProfile.Rows[0]["Transportation"].ToString();
                MPC.Rice = DTProfile.Rows[0]["Rice"].ToString();
                MPC.Communication = DTProfile.Rows[0]["Communication"].ToString();
                MPC.EcolaRegion = DTProfile.Rows[0]["EcolaRegion"].ToString();
                MPC.Branch = DTProfile.Rows[0]["Branch"].ToString();
                MPC.Wallet = DTProfile.Rows[0]["Wallet"].ToString();
                MPC.FaceID = DTProfile.Rows[0]["FaceID"].ToString();
                MPC.DTransFrom = DTProfile.Rows[0]["DTransFrom"].ToString();
                MPC.DTransTo = DTProfile.Rows[0]["DTransTo"].ToString();
                MPC.LocFrom = DTProfile.Rows[0]["LocFrom"].ToString();
                MPC.LocTo = DTProfile.Rows[0]["LocTo"].ToString();
                MPC.CCChangeFrom = DTProfile.Rows[0]["CCChangeFrom"].ToString();
                MPC.CCChangeTo = DTProfile.Rows[0]["CCChangeTo"].ToString();
                MPC.Email = DTProfile.Rows[0]["Email"].ToString();
                MPC.WorkEmail = DTProfile.Rows[0]["WorkEmail"].ToString();
                MPC.Extension = DTProfile.Rows[0]["Extension"].ToString();
                MPC.JoinDate = DTProfile.Rows[0]["JoinDate"].ToString();
                MPC.ProdStart = DTProfile.Rows[0]["ProdStart"].ToString();
                MPC.Tenure = DTProfile.Rows[0]["Tenure"].ToString();
                MPC.TenureMnth = DTProfile.Rows[0]["TenureMnth"].ToString();
                MPC.Batch1 = DTProfile.Rows[0]["Batch"].ToString();
                MPC.Class1 = DTProfile.Rows[0]["Class"].ToString();
                MPC.Skill1 = DTProfile.Rows[0]["Skill"].ToString();
                MPC.JobDesc1 = DTProfile.Rows[0]["JobDesc"].ToString();
                MPC.Promotion = DTProfile.Rows[0]["Promotion"].ToString();
                MPC.Status = DTProfile.Rows[0]["Status"].ToString();
                MPC.Emp_Name = DTProfile.Rows[0]["Emp_Name"].ToString();
                MPC.approved = DTProfile.Rows[0]["approved"].ToString();
                MPC.Employee_Level = DTProfile.Rows[0]["Employee_Level"].ToString();
                MPC.Employee_Category = DTProfile.Rows[0]["Employee_Category"].ToString();
                MPC.BusinessSegment1 = DTProfile.Rows[0]["BusinessSegment"].ToString();
                MPC.IsApprove = DTProfile.Rows[0]["IsApprove"].ToString();
                MPC.ResignedDate = DTProfile.Rows[0]["ResignedDate"].ToString();
                //MPC.HouseNumber = DTProfile.Rows[0]["HouseNumber"].ToString();
                //MPC.StreetName = DTProfile.Rows[0]["StreetName"].ToString();
                //MPC.Barangay = DTProfile.Rows[0]["Barangay"].ToString();
                //MPC.Town = DTProfile.Rows[0]["Town"].ToString();
                //MPC.City = DTProfile.Rows[0]["City"].ToString();
                //MPC.Region = DTProfile.Rows[0]["Region"].ToString();
                //MPC.ZipCode = DTProfile.Rows[0]["ZipCode"].ToString();
                MPC.BloodType = DTProfile.Rows[0]["BloodType"].ToString();
                MPC.Gender1 = DTProfile.Rows[0]["Gender"].ToString();
                MPC.DOB1 = DTProfile.Rows[0]["DOB"].ToString();
                MPC.Citizenship = DTProfile.Rows[0]["Citizenship"].ToString();
                MPC.NameofOrganization = DTProfile.Rows[0]["NameofOrganization"].ToString();
                MPC.MobileAreaCode = DTProfile.Rows[0]["MobileAreaCode"].ToString();
                MPC.MobileNumber = DTProfile.Rows[0]["MobileNumber"].ToString();
                MPC.HomeAreaCode = DTProfile.Rows[0]["HomeAreaCode"].ToString();
                MPC.HomeNumber = DTProfile.Rows[0]["HomeNumber"].ToString();
                // COMPLETE ADDRESS
                MPC.PrePropertyType = DTProfile.Rows[0]["PropType"].ToString();
                MPC.PreBldgNo = DTProfile.Rows[0]["BldgNum"].ToString();
                MPC.PrePlace = DTProfile.Rows[0]["Place"].ToString();
                MPC.PreWing = DTProfile.Rows[0]["Wing"].ToString();
                MPC.PreUnitNo = DTProfile.Rows[0]["UnitNum"].ToString();
                MPC.PreFloorNo = DTProfile.Rows[0]["FloorNum"].ToString();
                MPC.PreVillage = DTProfile.Rows[0]["Village"].ToString();
                MPC.PrePhaseNo = DTProfile.Rows[0]["PhaseNum"].ToString();
                MPC.PreSubdivision = DTProfile.Rows[0]["Subdivision"].ToString();
                MPC.PreRoomNo = DTProfile.Rows[0]["RoomNum"].ToString();
                MPC.PreBlkNo = DTProfile.Rows[0]["BlkNum"].ToString();
                MPC.PreLotNo = DTProfile.Rows[0]["LotNum"].ToString();
                MPC.isSame = DTProfile.Rows[0]["isSame"].ToString();
                MPC.PreStreet = DTProfile.Rows[0]["Street"].ToString();
                MPC.PreBrgy = DTProfile.Rows[0]["Barangay"].ToString();
                MPC.PreRegion = DTProfile.Rows[0]["Region"].ToString();
                MPC.PreProvince = DTProfile.Rows[0]["Province"].ToString();
                MPC.PreCityMuni = DTProfile.Rows[0]["City"].ToString();
                MPC.PreZipcode = DTProfile.Rows[0]["Zipcode"].ToString();
                MPC.PreCountry = DTProfile.Rows[0]["PreCountry"].ToString();
                MPC.PerPropertyType = DTProfile.Rows[0]["PermaPropType"].ToString();
                MPC.PerBldgNo = DTProfile.Rows[0]["PermaBldgNum"].ToString();
                MPC.PerPlace = DTProfile.Rows[0]["PermaPlace"].ToString();
                MPC.PerWing = DTProfile.Rows[0]["PermaWing"].ToString();
                MPC.PerUnitNo = DTProfile.Rows[0]["PermaUnitNum"].ToString();
                MPC.PerFloorNo = DTProfile.Rows[0]["PermaFloorNum"].ToString();
                MPC.PerVillage = DTProfile.Rows[0]["PermaVillage"].ToString();
                MPC.PerPhaseNo = DTProfile.Rows[0]["PermaPhaseNum"].ToString();
                MPC.PerSubdivision = DTProfile.Rows[0]["PermaSubdivision"].ToString();
                MPC.PerRoomNo = DTProfile.Rows[0]["PermaRoomNum"].ToString();
                MPC.PerBlkNo = DTProfile.Rows[0]["PermaBlkNum"].ToString();
                MPC.PerLotNo = DTProfile.Rows[0]["PermaLotNum"].ToString();
                MPC.PerStreet = DTProfile.Rows[0]["PermaStreet"].ToString();
                MPC.PerBrgy = DTProfile.Rows[0]["PermaBarangay"].ToString();
                MPC.PerRegion = DTProfile.Rows[0]["PermaRegion"].ToString();
                MPC.PerProvince = DTProfile.Rows[0]["PermaProvince"].ToString();
                MPC.PerCityMuni = DTProfile.Rows[0]["PermaCity"].ToString();
                MPC.PerZipcode = DTProfile.Rows[0]["PermaZipcode"].ToString();
                MPC.PerCountry = DTProfile.Rows[0]["PermaCountry"].ToString();

                MPC.Ref1FNAME = DTProfile.Rows[0]["Ref1FName"].ToString();
                MPC.Ref1LNAME = DTProfile.Rows[0]["Ref1LName"].ToString();
                MPC.Ref1Contact = DTProfile.Rows[0]["Ref1ContactNo"].ToString();
                MPC.Ref2FNAME = DTProfile.Rows[0]["Ref2FName"].ToString();
                MPC.Ref2LNAME = DTProfile.Rows[0]["Ref2LName"].ToString();
                MPC.Ref2Contact = DTProfile.Rows[0]["Ref2ContactNo"].ToString();
                // PersonalEmail = dr["PersonalEmail"].ToString(),
                MPC.EmrgName = DTProfile.Rows[0]["EmrgName"].ToString();
                MPC.EmrgNumber = DTProfile.Rows[0]["EmrgNumber"].ToString();
                MPC.EmrgRelationship = DTProfile.Rows[0]["EmrgRelationship"].ToString();
                MPC.OptionalEmail = DTProfile.Rows[0]["OptionalEmail"].ToString();
                // ContactNo = dr["ContactNo"].ToString(),
                MPC.Province = DTProfile.Rows[0]["Province"].ToString();
                MPC.TaxStatus = DTProfile.Rows[0]["TaxStatus"].ToString();

                MPC.SSS = DTProfile.Rows[0]["SSS"].ToString();
                MPC.TIN = DTProfile.Rows[0]["TIN"].ToString();
                MPC.PhilHealth = DTProfile.Rows[0]["PhilHealth"].ToString();
                MPC.Pagibig = DTProfile.Rows[0]["Pagibig"].ToString();
                MPC.DriversLicense = DTProfile.Rows[0]["DriversLicense"].ToString();
                MPC.Passport = DTProfile.Rows[0]["Passport"].ToString();
                MPC.Unified = DTProfile.Rows[0]["Unified"].ToString();
                MPC.Postal = DTProfile.Rows[0]["Postal"].ToString();
                MPC.BirthCertificate = DTProfile.Rows[0]["BirthCertificate"].ToString();
                MPC.SssImage = DTProfile.Rows[0]["SssImage"].ToString();
                MPC.TinImage = DTProfile.Rows[0]["TinImage"].ToString();
                MPC.PhilImage = DTProfile.Rows[0]["PhilImage"].ToString();
                MPC.PagImage = DTProfile.Rows[0]["PagImage"].ToString();
                MPC.DriverImage = DTProfile.Rows[0]["DriverImage"].ToString();
                MPC.PassportImage = DTProfile.Rows[0]["PassportImage"].ToString();
                MPC.UmidImage = DTProfile.Rows[0]["UmidImage"].ToString();
                MPC.PostalImage = DTProfile.Rows[0]["PostalImage"].ToString();
                MPC.BirthImage = DTProfile.Rows[0]["BirthImage"].ToString();
                MPC.DriversExpiry = DTProfile.Rows[0]["DriversExpiry"].ToString();
                MPC.PassportExpiry = DTProfile.Rows[0]["PassportExpiry"].ToString();
                MPC.PostalExpiry = DTProfile.Rows[0]["PostalExpiry"].ToString();
                MPC.SAGSD = DTProfile.Rows[0]["SAGSD"].ToString();
                MPC.SAGSDExpiry = DTProfile.Rows[0]["SAGSDExpiry"].ToString();
                MPC.NTC = DTProfile.Rows[0]["NTC"].ToString();
                MPC.NTCExpiry = DTProfile.Rows[0]["NTCExpiry"].ToString();
                MPC.BrgyClearance = DTProfile.Rows[0]["BrgyClearance"].ToString();
                MPC.BrgyClearanceExpiry = DTProfile.Rows[0]["BrgyClearanceExpiry"].ToString();
                MPC.PNPClearance = DTProfile.Rows[0]["PNPClearance"].ToString();
                MPC.PNPClearanceExpiry = DTProfile.Rows[0]["PNPClearanceExpiry"].ToString();
                MPC.NBIClearance = DTProfile.Rows[0]["NBIClearance"].ToString();
                MPC.NBIClearanceExpiry = DTProfile.Rows[0]["NBIClearanceExpiry"].ToString();
                MPC.NueroEvaluation = DTProfile.Rows[0]["NueroEvaluation"].ToString();
                MPC.NeuroEvaluationExpiry = DTProfile.Rows[0]["NeuroEvaluationExpiry"].ToString();
                MPC.DrugTest = DTProfile.Rows[0]["DrugTest"].ToString();
                MPC.DrugTestExpiry = DTProfile.Rows[0]["DrugTestExpiry"].ToString();
                MPC.PlaceofBirth = DTProfile.Rows[0]["POB"].ToString();
                MPC.Religion = DTProfile.Rows[0]["Religion"].ToString();
                MPC.Height = DTProfile.Rows[0]["Height"].ToString();
                MPC.Weight = DTProfile.Rows[0]["Weight"].ToString();
                MPC.MothersName = DTProfile.Rows[0]["MotherName"].ToString();
                MPC.FathersName = DTProfile.Rows[0]["FatherName"].ToString();
                MPC.RegDate = DTProfile.Rows[0]["RegDate"].ToString();

                MPC.BranchName = DTProfile.Rows[0]["OfficeName"].ToString();
                MPC.BadgeNo = DTProfile.Rows[0]["BadgeNumber"].ToString();
                MPC.EmpRole = DTProfile.Rows[0]["EmpRole"].ToString();
                MPC.ContractStat = DTProfile.Rows[0]["ContractStatus"].ToString();
                MPC.ContractNo = DTProfile.Rows[0]["ContractNo"].ToString();
                MPC.ContractEnd = DTProfile.Rows[0]["ContractEndDate"].ToString();


                //ADDED COLUMN  START
                MPC.CONTACTNUMBER = DTProfile.Rows[0]["ContactNo"].ToString();
                MPC.INDUSTRY = DTProfile.Rows[0]["Industry"].ToString();
                MPC.MARITALSTATUS = DTProfile.Rows[0]["MaritalStatus"].ToString();
                if (DTProfile.Rows[0]["MaritalStatus"].ToString() == "Married")
                {
                    MPC.MARRIEDDATE = DTProfile.Rows[0]["MarriedDate"].ToString();
                }
                else
                {
                    MPC.MARRIEDDATE = "";
                }

                MPC.SUFFIX = DTProfile.Rows[0]["Suffix"].ToString();
                MPC.PREFFIX = DTProfile.Rows[0]["Preffix"].ToString();
                //ADDED COLUMN END

                System.Data.DataTable DTContactNo = new System.Data.DataTable();
                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = Log.LoginID });
                DTContactNo = con.GetDataTable("sp_getPersonalContactInfo");



                //Company Contact Number Start
                List<ContactNo> Clist = new List<ContactNo>();
                if (DTContactNo.Rows.Count > 0)
                {
                    for (int ii = 0; ii < DTContactNo.Rows.Count; ii++)
                    {
                        ContactNo Contact = new ContactNo();
                        Contact.Contact = DTContactNo.Rows[ii]["Number"].ToString();
                        Contact.ContactType = DTContactNo.Rows[ii]["NumberType"].ToString();
                        Clist.Add(Contact);
                    }
                }
                else
                {
                    ContactNo Contact = new ContactNo();
                    Contact.Contact = "";
                    Contact.ContactType = "";
                    Clist.Add(Contact);
                }
                MPC.ContactNo = Clist;
                //Company Contact Number End


                //Company Contact Email Start
                System.Data.DataTable DTEmail = new System.Data.DataTable();
                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = Log.LoginID });
                DTEmail = con.GetDataTable("sp_getPersonalEmailInfo");
                List<PersonalEmail> EMaillist = new List<PersonalEmail>();
                if (DTEmail.Rows.Count > 0)
                {
                    for (int iii = 0; iii < DTEmail.Rows.Count; iii++)
                    {
                        PersonalEmail Email = new PersonalEmail();
                        Email.Email = DTEmail.Rows[iii]["Email"].ToString();
                        Email.EmailType = DTEmail.Rows[iii]["EmailType"].ToString();
                        EMaillist.Add(Email);
                    }
                }
                else
                {
                    PersonalEmail Email = new PersonalEmail();
                    Email.Email = "";
                    Email.EmailType = "";
                    EMaillist.Add(Email);
                }
                MPC.PersonalEmail = EMaillist;
                //Company Contact Email End

                //COMPANY CONTACT START
                System.Data.DataTable DTCompContact = new System.Data.DataTable();
                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = Log.LoginID });
                DTCompContact = con.GetDataTable("sp_GetCompanyContactPhoneDHR");
                //DTCompEmail = con.GetDataTable("sp_GetCompanyEmailDHR");
                List<CompanyContact> CompanyContactList = new List<CompanyContact>();
                if (DTCompContact.Rows.Count > 0)
                {
                    for (int a = 0; a < DTCompContact.Rows.Count; a++)
                    {
                        CompanyContact CompanyC = new CompanyContact();
                        CompanyC.PhoneType = DTCompContact.Rows[a][0].ToString();
                        CompanyC.ContactNumber = DTCompContact.Rows[a][1].ToString();
                        CompanyC.ID = DTCompContact.Rows[a][2].ToString();
                        CompanyContactList.Add(CompanyC);
                    }
                }
                else
                {
                    CompanyContact CompanyC = new CompanyContact();
                    CompanyC.PhoneType = "";
                    CompanyC.ContactNumber = "";
                    CompanyC.ID = "";
                    CompanyContactList.Add(CompanyC);
                }


                MPC.CompanyContact = CompanyContactList;
                //COMPANY CONTACT END

                //COMPANY EMAIL START
                System.Data.DataTable DTCompEmail = new System.Data.DataTable();
                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = Log.LoginID });
                DTCompEmail = con.GetDataTable("sp_GetCompanyEmailDHR");
                //DTCompEmail = con.GetDataTable("sp_GetCompanyEmailDHR");
                List<CompanyEmail> CompanyEmailList = new List<CompanyEmail>();

                if (DTCompEmail.Rows.Count > 0)
                {
                    for (int aa = 0; aa < DTCompEmail.Rows.Count; aa++)
                    {
                        CompanyEmail CompanyE = new CompanyEmail();
                        CompanyE.EmailType = DTCompEmail.Rows[aa][0].ToString();
                        CompanyE.EmailData = DTCompEmail.Rows[aa][1].ToString();
                        CompanyE.ID = DTCompEmail.Rows[aa][2].ToString();
                        CompanyEmailList.Add(CompanyE);
                    }

                }
                else
                {
                    CompanyEmail CompanyE = new CompanyEmail();
                    CompanyE.EmailType = "";
                    CompanyE.EmailData = "";
                    CompanyE.ID = "";
                    CompanyEmailList.Add(CompanyE);
                }
                MPC.CompanyEmail = CompanyEmailList;
                //COMPANY EMAIL END 
                ListReturned.Add(MPC);
            }


            ListUs.CompleteProfile = ListReturned;
            ListUs.myreturn = "Success";
            return ListUs;
        }
        #endregion
        #endregion

        #region Marvic 2018-09-03  
        [HttpPost]
        [Route("GetDHRCompIDLicensesv1")]
        public List<GovIDLicensesList> GetDHRCompIDLicensesv1(GovIDLicensesList GIL)
        {

            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = GIL.CN;
            SelfieRegistration2Controller.GetDB2(GDB);

            Connection con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = GIL.LoginID });
            System.Data.DataTable DT = new System.Data.DataTable();
            DT = con.GetDataTable("sp_GetDHRCompIDLicensesv1");
            List<GovIDLicensesList> ListReturned = new List<GovIDLicensesList>();
            if (DT.Rows.Count > 0)
            {
                for (int i = 0; i < DT.Rows.Count; i++)
                {
                    GovIDLicensesList GetLog = new GovIDLicensesList();
                    GetLog.ID = DT.Rows[i][0].ToString();
                    GetLog.CompanIDTypes = DT.Rows[i][2].ToString();
                    GetLog.IDCode = DT.Rows[i][3].ToString();
                    GetLog.IDIssued = DT.Rows[i][4].ToString();
                    GetLog.IDExpiryDate = DT.Rows[i][5].ToString();
                    GetLog.IDBased64Image = DT.Rows[i][6].ToString();
                    GetLog.IDisRequired = DT.Rows[i][9].ToString();
                    GetLog.UserTaxStatus = DT.Rows[i][10].ToString();
                    GetLog.URLLINK = DT.Rows[i][13].ToString();
                    
                    if (DT.Rows[i][7].ToString() == "1")
                    {
                        GetLog.GovTypes = "Government ID";
                    }
                    else
                    {
                        GetLog.GovTypes = "Government Licenses";
                    }
                    ListReturned.Add(GetLog);
                }
            }
            return ListReturned;
        }



        [HttpPost]
        [Route("AddUpdateDHRCompIDLicensesv1")]
        //Company Email
        public string AddUpdateDHRCompIDLicensesv1(GovIDLicenses GIL)
        {
            try
            {
                GetDatabase GDB = new GetDatabase();
                GDB.ClientName = GIL.CN;
                SelfieRegistration2Controller.GetDB2(GDB);
                Connection Con = new Connection();
                Con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = GIL.LoginID });
                Con.myparameters.Add(new myParameters { ParameterName = "@INPUTEDDATA", mytype = SqlDbType.NVarChar, Value = GIL.INPUTEDDATA });
                Con.myparameters.Add(new myParameters { ParameterName = "@EXPIRYDATE", mytype = SqlDbType.NVarChar, Value = GIL.EXPIRYDATE });
                Con.myparameters.Add(new myParameters { ParameterName = "@IDLICENSESTYPE", mytype = SqlDbType.NVarChar, Value = GIL.IDLICENSESTYPE });
                Con.ExecuteNonQuery("sp_AddUpdateDHRCompIDLicensesv1");
                return "success";
            }
            catch (Exception e)
            {
                return "failed" + e;
            }
        }


        [HttpPost]
        [Route("UpdateEmployeePersonalInfov4")]
        public String UpdateEmployeeMasterv4(UpdateEmployeePersonalInfov4 EmployeeMaster)
        {
            try
            {
                GetDatabase GDB = new GetDatabase();
                GDB.ClientName = EmployeeMaster.CN;
                SelfieRegistration2Controller.GetDB2(GDB);

                Connection Connection = new Connection();

                Connection.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.EmpID });
                Connection.myparameters.Add(new myParameters { ParameterName = "@HouseNumber", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.HouseNumber });
                Connection.myparameters.Add(new myParameters { ParameterName = "@StreetName", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.StreetName });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Barangay", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Barangay });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Town", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Town });
                Connection.myparameters.Add(new myParameters { ParameterName = "@City", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.City });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Region", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Region });
                Connection.myparameters.Add(new myParameters { ParameterName = "@ZipCode", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.ZipCode });
                Connection.myparameters.Add(new myParameters { ParameterName = "@BloodType", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.BloodType });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Gender", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Gender });
                Connection.myparameters.Add(new myParameters { ParameterName = "@DOB", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.DOB });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Citizenship", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Citizenship });
                Connection.myparameters.Add(new myParameters { ParameterName = "@NameofOrganization", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.NameofOrganization });
                Connection.myparameters.Add(new myParameters { ParameterName = "@MobileAreaCode", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.MobileAreaCode });
                Connection.myparameters.Add(new myParameters { ParameterName = "@MobileNumber", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.MobileNumber });
                Connection.myparameters.Add(new myParameters { ParameterName = "@HomeAreaCode", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.HomeAreaCode });
                Connection.myparameters.Add(new myParameters { ParameterName = "@HomeNumber", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.HomeNumber });
                Connection.myparameters.Add(new myParameters { ParameterName = "@PersonalEmail", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.PersonalEmail });
                Connection.myparameters.Add(new myParameters { ParameterName = "@EmrgName", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.EmrgName });
                Connection.myparameters.Add(new myParameters { ParameterName = "@EmrgNumber", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.EmrgNumber });
                Connection.myparameters.Add(new myParameters { ParameterName = "@EmrgRelationship", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.EmrgRelationship });
                Connection.myparameters.Add(new myParameters { ParameterName = "@OptionalEmail", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.OptionalEmail });
                Connection.myparameters.Add(new myParameters { ParameterName = "@ContactNo", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.ContactNo });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Province", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Province });
                Connection.myparameters.Add(new myParameters { ParameterName = "@EmrgFname", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.EmrgFname });
                Connection.myparameters.Add(new myParameters { ParameterName = "@EmrgMname", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.EmrgMname });
                Connection.myparameters.Add(new myParameters { ParameterName = "@EmrgLname", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.EmrgLname });

                Connection.myparameters.Add(new myParameters { ParameterName = "@FatherName", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.FatherName });
                Connection.myparameters.Add(new myParameters { ParameterName = "@MotherName", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.MotherName });
                String ID = Connection.ExecuteScalar("sp_HRMS_UpdatePersonalInfov4");
                return ID;
            }
            catch (Exception e)
            {
                return e.ToString();
            }

        }




        [HttpPost]
        [Route("UpdateEmployeePersonalInfov5")]
        public String UpdateEmployeeMasterv5(UpdateEmployeePersonalInfov5 EmployeeMaster)
        {
            try
            {
                GetDatabase GDB = new GetDatabase();
                GDB.ClientName = EmployeeMaster.CN;
                SelfieRegistration2Controller.GetDB2(GDB);

                Connection Connection = new Connection();

                Connection.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.EmpID });
                Connection.myparameters.Add(new myParameters { ParameterName = "@HouseNumber", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.HouseNumber });
                Connection.myparameters.Add(new myParameters { ParameterName = "@StreetName", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.StreetName });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Barangay", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Barangay });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Town", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Town });
                Connection.myparameters.Add(new myParameters { ParameterName = "@City", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.City });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Region", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Region });
                Connection.myparameters.Add(new myParameters { ParameterName = "@ZipCode", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.ZipCode });
                Connection.myparameters.Add(new myParameters { ParameterName = "@BloodType", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.BloodType });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Gender", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Gender });
                Connection.myparameters.Add(new myParameters { ParameterName = "@DOB", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.DOB });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Citizenship", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Citizenship });
                Connection.myparameters.Add(new myParameters { ParameterName = "@NameofOrganization", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.NameofOrganization });
                Connection.myparameters.Add(new myParameters { ParameterName = "@MobileAreaCode", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.MobileAreaCode });
                Connection.myparameters.Add(new myParameters { ParameterName = "@MobileNumber", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.MobileNumber });
                Connection.myparameters.Add(new myParameters { ParameterName = "@HomeAreaCode", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.HomeAreaCode });
                Connection.myparameters.Add(new myParameters { ParameterName = "@HomeNumber", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.HomeNumber });
                Connection.myparameters.Add(new myParameters { ParameterName = "@PersonalEmail", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.PersonalEmail });
                Connection.myparameters.Add(new myParameters { ParameterName = "@EmrgName", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.EmrgName });
                Connection.myparameters.Add(new myParameters { ParameterName = "@EmrgNumber", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.EmrgNumber });
                Connection.myparameters.Add(new myParameters { ParameterName = "@EmrgRelationship", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.EmrgRelationship });
                Connection.myparameters.Add(new myParameters { ParameterName = "@OptionalEmail", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.OptionalEmail });
                Connection.myparameters.Add(new myParameters { ParameterName = "@ContactNo", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.ContactNo });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Province", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Province });
                Connection.myparameters.Add(new myParameters { ParameterName = "@EmrgFname", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.EmrgFname });
                Connection.myparameters.Add(new myParameters { ParameterName = "@EmrgMname", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.EmrgMname });
                Connection.myparameters.Add(new myParameters { ParameterName = "@EmrgLname", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.EmrgLname });

                Connection.myparameters.Add(new myParameters { ParameterName = "@FatherName", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.FatherName });
                Connection.myparameters.Add(new myParameters { ParameterName = "@MotherName", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.MotherName });

                Connection.myparameters.Add(new myParameters { ParameterName = "@MaritalStatus", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.MaritalStatus });
                Connection.myparameters.Add(new myParameters { ParameterName = "@MarriedDate", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.MarriedDate });

                //Added Data
               
                Connection.myparameters.Add(new myParameters { ParameterName = "@weight", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.WEIGHT });
                Connection.myparameters.Add(new myParameters { ParameterName = "@height", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.HEIGHT });
                Connection.myparameters.Add(new myParameters { ParameterName = "@peremail", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.PEMAIL });
                Connection.myparameters.Add(new myParameters { ParameterName = "@perconnum", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.PCONTACT });
                Connection.myparameters.Add(new myParameters { ParameterName = "@RELIGION", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Religion });
                Connection.myparameters.Add(new myParameters { ParameterName = "@POB", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.PlaceofBirth });
                //Added Data 

                String ID = Connection.ExecuteScalar("sp_DHRUpdatePersonalInfov2");


                return ID;
            }
            catch (Exception e)
            {
                return e.ToString();
            }

        }

        #endregion
#region
        [HttpPost]
        [Route("UpdateEmployeePersonalInfov6")]
        public String UpdateEmployeeMasterv6(UpdateEmployeePersonalInfov6 EmployeeMaster)
        {
            try
            {
                GetDatabase GDB = new GetDatabase();
                GDB.ClientName = EmployeeMaster.CN;
                SelfieRegistration2Controller.GetDB2(GDB);

                Connection Connection = new Connection();

                Connection.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.EmpID });
                Connection.myparameters.Add(new myParameters { ParameterName = "@BloodType", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.BloodType });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Gender", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Gender });
                Connection.myparameters.Add(new myParameters { ParameterName = "@DOB", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.DOB });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Citizenship", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Citizenship });
                Connection.myparameters.Add(new myParameters { ParameterName = "@NCOUNTRY", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.NCountry }); 
                Connection.myparameters.Add(new myParameters { ParameterName = "@NameofOrganization", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.NameofOrganization });
                Connection.myparameters.Add(new myParameters { ParameterName = "@MobileAreaCode", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.MobileAreaCode });
                Connection.myparameters.Add(new myParameters { ParameterName = "@MobileNumber", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.MobileNumber });
                Connection.myparameters.Add(new myParameters { ParameterName = "@HomeAreaCode", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.HomeAreaCode });
                Connection.myparameters.Add(new myParameters { ParameterName = "@HomeNumber", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.HomeNumber });
                Connection.myparameters.Add(new myParameters { ParameterName = "@PersonalEmail", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.PersonalEmail });
                Connection.myparameters.Add(new myParameters { ParameterName = "@EmrgName", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.EmrgName });
                Connection.myparameters.Add(new myParameters { ParameterName = "@EmrgNumber", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.EmrgNumber });
                Connection.myparameters.Add(new myParameters { ParameterName = "@EmrgRelationship", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.EmrgRelationship });
                Connection.myparameters.Add(new myParameters { ParameterName = "@OptionalEmail", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.OptionalEmail });
                Connection.myparameters.Add(new myParameters { ParameterName = "@ContactNo", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.ContactNo });
                Connection.myparameters.Add(new myParameters { ParameterName = "@EmrgFname", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.EmrgFname });
                Connection.myparameters.Add(new myParameters { ParameterName = "@EmrgMname", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.EmrgMname });
                Connection.myparameters.Add(new myParameters { ParameterName = "@EmrgLname", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.EmrgLname });

                Connection.myparameters.Add(new myParameters { ParameterName = "@FatherName", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.FatherName });
                Connection.myparameters.Add(new myParameters { ParameterName = "@MotherName", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.MotherName });

                Connection.myparameters.Add(new myParameters { ParameterName = "@MaritalStatus", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.MaritalStatus });
                Connection.myparameters.Add(new myParameters { ParameterName = "@MarriedDate", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.MarriedDate });

                Connection.myparameters.Add(new myParameters { ParameterName = "@weight", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.WEIGHT });
                Connection.myparameters.Add(new myParameters { ParameterName = "@height", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.HEIGHT });
                Connection.myparameters.Add(new myParameters { ParameterName = "@peremail", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.PEMAIL });
                Connection.myparameters.Add(new myParameters { ParameterName = "@perconnum", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.PCONTACT });
                Connection.myparameters.Add(new myParameters { ParameterName = "@RELIGION", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Religion });
                Connection.myparameters.Add(new myParameters { ParameterName = "@POB", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.PlaceofBirth });

                // Added new Data
                Connection.myparameters.Add(new myParameters { ParameterName = "@PrePropertyType ", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.PREPropertyType });
                Connection.myparameters.Add(new myParameters { ParameterName = "@PreBldgNo", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.PREBldgNo });
                Connection.myparameters.Add(new myParameters { ParameterName = "@PrePlace", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.PREPlace });
                Connection.myparameters.Add(new myParameters { ParameterName = "@PreWing", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.PREWing });
                Connection.myparameters.Add(new myParameters { ParameterName = "@PreUnitNo", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.PREUnitNo });
                Connection.myparameters.Add(new myParameters { ParameterName = "@PreFloorNo", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.PREFloorNo });
                Connection.myparameters.Add(new myParameters { ParameterName = "@PreVillage", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.PREVillage });
                Connection.myparameters.Add(new myParameters { ParameterName = "@PrePhaseNo", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.PREPhaseNo });
                Connection.myparameters.Add(new myParameters { ParameterName = "@PreSubdivision", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.PRESubdivision });
                Connection.myparameters.Add(new myParameters { ParameterName = "@PreRoomNo", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.PRERoomNo });
                Connection.myparameters.Add(new myParameters { ParameterName = "@PreBlkNo", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.PREBlkNo });
                Connection.myparameters.Add(new myParameters { ParameterName = "@PreLotNo", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.PRELotNo });
                Connection.myparameters.Add(new myParameters { ParameterName = "@PreStreet", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.PREStreet });
                Connection.myparameters.Add(new myParameters { ParameterName = "@PreBrgyname", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.PREBrgyname });
                Connection.myparameters.Add(new myParameters { ParameterName = "@PreRegion", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.PRERegion });
                Connection.myparameters.Add(new myParameters { ParameterName = "@PreProvince", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.PREProvince });
                Connection.myparameters.Add(new myParameters { ParameterName = "@PreCityMuni", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.PRECityMuni });
                Connection.myparameters.Add(new myParameters { ParameterName = "@PreZipcode", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.PREZipcode });
                Connection.myparameters.Add(new myParameters { ParameterName = "@PreCountry", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.PRECountry });
                Connection.myparameters.Add(new myParameters { ParameterName = "@isSame", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.ISSAME });

                Connection.myparameters.Add(new myParameters { ParameterName = "@PermaPropertyType", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.PERMAPropertyType });
                Connection.myparameters.Add(new myParameters { ParameterName = "@PermaBldgNo", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.PERMABldgNo });
                Connection.myparameters.Add(new myParameters { ParameterName = "@PermaPlace", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.PERMAPlace });
                Connection.myparameters.Add(new myParameters { ParameterName = "@PermaWing", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.PERMAWing });
                Connection.myparameters.Add(new myParameters { ParameterName = "@PermaUnitNo", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.PERMAUnitNo });
                Connection.myparameters.Add(new myParameters { ParameterName = "@PermaFloorNo", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.PERMAFloorNo });
                Connection.myparameters.Add(new myParameters { ParameterName = "@PermaVillage", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.PERMAVillage });
                Connection.myparameters.Add(new myParameters { ParameterName = "@PermaPhaseNo", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.PERMAPhaseNo });
                Connection.myparameters.Add(new myParameters { ParameterName = "@PermaSubdivision", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.PERMASubdivision });
                Connection.myparameters.Add(new myParameters { ParameterName = "@PermaRoomNo", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.PERMARoomNo });
                Connection.myparameters.Add(new myParameters { ParameterName = "@PermaBlkNo", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.PERMABlkNo });
                Connection.myparameters.Add(new myParameters { ParameterName = "@PermaLotNo", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.PERMALotNo });
                Connection.myparameters.Add(new myParameters { ParameterName = "@PermaStreet", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.PERMAStreet });
                Connection.myparameters.Add(new myParameters { ParameterName = "@PermaBrgyname", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.PERMABrgyname });
                Connection.myparameters.Add(new myParameters { ParameterName = "@PermaRegion", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.PERMARegion });
                Connection.myparameters.Add(new myParameters { ParameterName = "@PermaProvince", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.PERMAProvince });
                Connection.myparameters.Add(new myParameters { ParameterName = "@PermaCityMuni", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.PERMACityMuni });
                Connection.myparameters.Add(new myParameters { ParameterName = "@PermaZipcode", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.PERMAZipcode });
                Connection.myparameters.Add(new myParameters { ParameterName = "@PermaCountry", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.PERMACountry });
                // Added end Data
                //Connection.myparameters.Add(new myParameters { ParameterName = "@Ref1FNAME", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Ref1FNAME });
                //Connection.myparameters.Add(new myParameters { ParameterName = "@Ref1LNAME", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Ref1LNAME });
                //Connection.myparameters.Add(new myParameters { ParameterName = "@Ref1Contact", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Ref1Contact });
                //Connection.myparameters.Add(new myParameters { ParameterName = "@Ref2FNAME", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Ref2FNAME });
                //Connection.myparameters.Add(new myParameters { ParameterName = "@Ref2LNAME", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Ref2LNAME });
                //Connection.myparameters.Add(new myParameters { ParameterName = "@Ref2Contact", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Ref2Contact });

                String ID = Connection.ExecuteScalar("sp_DHRUpdatePersonalInfov3");


                return ID;
            }
            catch (Exception e)
            {
                return e.ToString();
            }

        }

#endregion
        //---------------------------------------------------------------Revised Change DB End------------------------------------------------------------------------
        #region 2018-09-06
        [HttpPost]
        [Route("AddUpdateDHREmpHistoryv1")]
        //Company Email
        public string AddUpdateDHREmpHistoryv1(DHREMPHv1 EH)
        {
            try
            {
                GetDatabase GDB = new GetDatabase();
                GDB.ClientName = EH.CN;
                SelfieRegistration2Controller.GetDB2(GDB);
                Connection Con = new Connection();
                Con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = EH.LoginID });
                Con.myparameters.Add(new myParameters { ParameterName = "@CompanyName", mytype = SqlDbType.NVarChar, Value = EH.CompanyName });
                Con.myparameters.Add(new myParameters { ParameterName = "@Specialization", mytype = SqlDbType.NVarChar, Value = EH.Specialization });
                Con.myparameters.Add(new myParameters { ParameterName = "@Role", mytype = SqlDbType.NVarChar, Value = EH.Role });
                Con.myparameters.Add(new myParameters { ParameterName = "@PositionTitle", mytype = SqlDbType.NVarChar, Value = EH.PositionTitle });
                Con.myparameters.Add(new myParameters { ParameterName = "@Positionlevel", mytype = SqlDbType.NVarChar, Value = EH.Positionlevel });
                Con.myparameters.Add(new myParameters { ParameterName = "@JoinedFrom", mytype = SqlDbType.NVarChar, Value = EH.JoinedFrom });
                Con.myparameters.Add(new myParameters { ParameterName = "@JoinedTo", mytype = SqlDbType.NVarChar, Value = EH.JoinedTo });
                Con.myparameters.Add(new myParameters { ParameterName = "@Country", mytype = SqlDbType.NVarChar, Value = EH.Country });
                Con.myparameters.Add(new myParameters { ParameterName = "@Description", mytype = SqlDbType.NVarChar, Value = EH.Description });
                Con.myparameters.Add(new myParameters { ParameterName = "@Industry", mytype = SqlDbType.NVarChar, Value = EH.Industry });
                Con.ExecuteNonQuery("sp_AddUpdateDHREmpHistoryv1");
                return "success";
            }
            catch (Exception e)
            {
                return "failed" + e;
            }
        }

        [HttpPost]
        [Route("DHRGetEmpHistory")]

        public GetEmpHistory DHRGetEmpHistory(DHREMPHv1 DH)
        {
            GetEmpHistory GetDependentResult = new GetEmpHistory();
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = DH.CN;
            SelfieRegistration2Controller.GetDB2(GDB);

            try
            {
                Connection Connection = new Connection();
                List<DHREMPHv1> ListDependent = new List<DHREMPHv1>();
                DataSet ds = new DataSet();
                System.Data.DataTable dtLEAVES = new System.Data.DataTable();
                Connection.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = DH.LoginID });
                ds = Connection.GetDataset("sp_DHRGetEmpHistory");
                dtLEAVES = ds.Tables[0];
                if (dtLEAVES.Rows.Count > 0)
                {
                    foreach (DataRow row in dtLEAVES.Rows)
                    {
                        DHREMPHv1 DP = new DHREMPHv1
                        {
                            CompanyName = row["CompanyName"].ToString(),
                            Specialization = row["Specilization"].ToString(),
                            Role = row["RolePositionTItle"].ToString(),
                            PositionTitle = row["PositionTitle"].ToString(),
                            Positionlevel = row["PositionLevel"].ToString(),
                            JoinedFrom = row["JoinedFrom"].ToString(),
                            JoinedTo = row["JoinedTo"].ToString(),
                            Country = row["Country"].ToString(),
                            Description = row["ExperienceDesc"].ToString(),
                            Industry = row["Industry"].ToString(),
                            ID = row["ID"].ToString()
                        };
                        ListDependent.Add(DP);
                    }
                    GetDependentResult.EmployeeHistory = ListDependent;
                    GetDependentResult.myreturn = "Success";
                }
                else
                {
                    GetDependentResult.myreturn = "No Data Available";
                }
            }
            catch (Exception ex)
            {
                GetDependentResult.myreturn = ex.Message;
            }
            return GetDependentResult;
        }

        [HttpPost]
        [Route("UpdateDHREmpHistory")]
        //Company Email
        public string UpdateDHREmpHistory(DHREMPHv1 EH)
        {
            try
            {
                GetDatabase GDB = new GetDatabase();
                GDB.ClientName = EH.CN;
                SelfieRegistration2Controller.GetDB2(GDB);
                Connection Con = new Connection();
                Con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = EH.LoginID });
                Con.myparameters.Add(new myParameters { ParameterName = "@CompanyName", mytype = SqlDbType.NVarChar, Value = EH.CompanyName });
                Con.myparameters.Add(new myParameters { ParameterName = "@Specialization", mytype = SqlDbType.NVarChar, Value = EH.Specialization });
                Con.myparameters.Add(new myParameters { ParameterName = "@Role", mytype = SqlDbType.NVarChar, Value = EH.Role });
                Con.myparameters.Add(new myParameters { ParameterName = "@PositionTitle", mytype = SqlDbType.NVarChar, Value = EH.PositionTitle });
                Con.myparameters.Add(new myParameters { ParameterName = "@Positionlevel", mytype = SqlDbType.NVarChar, Value = EH.Positionlevel });
                Con.myparameters.Add(new myParameters { ParameterName = "@JoinedFrom", mytype = SqlDbType.NVarChar, Value = EH.JoinedFrom });
                Con.myparameters.Add(new myParameters { ParameterName = "@JoinedTo", mytype = SqlDbType.NVarChar, Value = EH.JoinedTo });
                Con.myparameters.Add(new myParameters { ParameterName = "@Country", mytype = SqlDbType.NVarChar, Value = EH.Country });
                Con.myparameters.Add(new myParameters { ParameterName = "@Description", mytype = SqlDbType.NVarChar, Value = EH.Description });
                Con.myparameters.Add(new myParameters { ParameterName = "@Industry", mytype = SqlDbType.NVarChar, Value = EH.Industry });
                Con.myparameters.Add(new myParameters { ParameterName = "@ID", mytype = SqlDbType.NVarChar, Value = EH.ID });
                Con.ExecuteNonQuery("sp_UpdateDHREmpHistory");
                return "success";
            }
            catch (Exception e)
            {
                return "failed" + e;
            }
        }
        [HttpPost]
        [Route("DeleteDHREmpHistory")]
        //Company Email
        public string DeleteDHREmpHistory(DHREMPHv1 EH)
        {
            try
            {
                GetDatabase GDB = new GetDatabase();
                GDB.ClientName = EH.CN;
                SelfieRegistration2Controller.GetDB2(GDB);
                Connection Con = new Connection();
                Con.myparameters.Add(new myParameters { ParameterName = "@ID", mytype = SqlDbType.NVarChar, Value = EH.ID });
                Con.ExecuteNonQuery("sp_DeleteDHREmpHistory");
                return "success";
            }
            catch (Exception e)
            {
                return "failed" + e;
            }
        }
        #endregion

        #region 2018-09-10
        [HttpPost]
        [Route("GetDHRAuditTrail")]
        public List<AuditTrailList> GetDHRAuditTrail(AuditTrailList GIL)
        {

            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = GIL.CN;
            SelfieRegistration2Controller.GetDB2(GDB);

            Connection con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@DATEFROM", mytype = SqlDbType.NVarChar, Value = GIL.DATEFROM });
            con.myparameters.Add(new myParameters { ParameterName = "@DATETO", mytype = SqlDbType.NVarChar, Value = GIL.DATETO });
            System.Data.DataTable DT = new System.Data.DataTable();
            DT = con.GetDataTable("sp_GetDHRAuditTrail");


            List<AuditTrailList> ListReturned = new List<AuditTrailList>();
            if (DT.Rows.Count > 0)
            {
                for (int i = 0; i < DT.Rows.Count; i++)
                {
                    AuditTrailList GetLog = new AuditTrailList();
                    GetLog.ID = DT.Rows[i][0].ToString();
                    GetLog.MODIFIEDBYNAME = DT.Rows[i][1].ToString();
                    GetLog.MODIFIEDBYEMPID = DT.Rows[i][2].ToString();
                    GetLog.CHANGEFROM = DT.Rows[i][3].ToString();
                    GetLog.CHANGETO = DT.Rows[i][4].ToString();
                    GetLog.MODIFIEDDATE = Convert.ToDateTime(DT.Rows[i][5].ToString()).ToString("yyyy-MM-dd");
                    GetLog.MODIFIEDTIME = Convert.ToDateTime(DT.Rows[i][5].ToString()).ToString("hh:mm tt");
                    GetLog.CATEGORY = DT.Rows[i][6].ToString();
                    ListReturned.Add(GetLog);
                }
            }
            return ListReturned;
        }
        [HttpPost]
        [Route("GetDHRAuditTrailv2")]
        public List<AuditTrailListv2> GetDHRAuditTrailv2(AuditTrailListv2 GIL)
        {

            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = GIL.CN;
            SelfieRegistration2Controller.GetDB2(GDB);

            Connection con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.NVarChar, Value = GIL.EMPID });
            con.myparameters.Add(new myParameters { ParameterName = "@DATEFROM", mytype = SqlDbType.NVarChar, Value = GIL.DATEFROM });
            con.myparameters.Add(new myParameters { ParameterName = "@DATETO", mytype = SqlDbType.NVarChar, Value = GIL.DATETO });
            System.Data.DataTable DT = new System.Data.DataTable();
            DT = con.GetDataTable("sp_GetDHRAuditTrailv2");


            List<AuditTrailListv2> ListReturned = new List<AuditTrailListv2>();
            if (DT.Rows.Count > 0)
            {
                for (int i = 0; i < DT.Rows.Count; i++)
                {
                    AuditTrailListv2 GetLog = new AuditTrailListv2();
                    GetLog.ID = DT.Rows[i][0].ToString();
                    GetLog.MODIFIEDBYNAME = DT.Rows[i][1].ToString();
                    GetLog.MODIFIEDBYEMPID = DT.Rows[i][2].ToString();
                    GetLog.CHANGEFROM = DT.Rows[i][3].ToString();
                    GetLog.CHANGETO = DT.Rows[i][4].ToString();
                    GetLog.MODIFIEDDATE = Convert.ToDateTime(DT.Rows[i][5].ToString()).ToString("yyyy-MM-dd");
                    GetLog.MODIFIEDTIME = Convert.ToDateTime(DT.Rows[i][5].ToString()).ToString("hh:mm tt");
                    GetLog.CATEGORY = DT.Rows[i][6].ToString();
                    ListReturned.Add(GetLog);
                }
            }
            return ListReturned;
        }
        #endregion
        #region 2018-09-26
        //GET DEPENDENT  START
        [HttpPost]
        [Route("DHRGetDependentv2")]

        public GetDependentResult DHRGetDependentv2(DHRDEPv1 DH)
        {
            GetDependentResult GetDependentResult = new GetDependentResult();
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = DH.CN;
            SelfieRegistration2Controller.GetDB2(GDB);

            try
            {
                Connection Connection = new Connection();
                List<DHRDEPv1> ListDependent = new List<DHRDEPv1>();
                DataSet ds = new DataSet();
                System.Data.DataTable dtLEAVES = new System.Data.DataTable();
                Connection.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = DH.EmpID });
                ds = Connection.GetDataset("sp_DHRGetDependentv1");
                dtLEAVES = ds.Tables[0];
                if (dtLEAVES.Rows.Count > 0)
                {
                    foreach (DataRow row in dtLEAVES.Rows)
                    {
                        DHRDEPv1 DP = new DHRDEPv1
                        {
                            Relationship = row["Relation"].ToString(),
                            First = row["FirstName"].ToString(),
                            Lastname = row["LastName"].ToString(),
                            Mid = row["MiddleName"].ToString(),
                            Suffix = row["SuffixName"].ToString(),
                            Age = row["age"].ToString(),
                            DOB = row["DateOfBirth"].ToString(),
                            BASED64IMAGE = row["Based64Image"].ToString(),
                            URLLINK = row["UrlLink"].ToString(),
                        };
                        ListDependent.Add(DP);
                    }
                    GetDependentResult.Dependent = ListDependent;
                    GetDependentResult.myreturn = "Success";
                }
                else
                {
                    GetDependentResult.myreturn = "No Data Available";
                }
            }
            catch (Exception ex)
            {
                GetDependentResult.myreturn = ex.Message;
            }
            return GetDependentResult;
        }

        [HttpPost]
        [Route("DHRGetDependentv3")]
        public List<DHRDEPv1> DHRGetDependentv3(DHRDEPv1 Dep)
        {

            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = Dep.CN;
            SelfieRegistration2Controller.GetDB2(GDB);

            Connection con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = Dep.LoginID });
            System.Data.DataTable DT = new System.Data.DataTable();
            DT = con.GetDataTable("sp_DHRGetDependentv1");
            List<DHRDEPv1> ListReturned = new List<DHRDEPv1>();

            if (DT.Rows.Count > 0)
            {
                for (int i = 0; i < DT.Rows.Count; i++)
                {
                    DHRDEPv1 GetLog = new DHRDEPv1();
                    GetLog.dependentID = DT.Rows[i][0].ToString();
                    GetLog.Relationship = DT.Rows[i][1].ToString();
                    GetLog.First = DT.Rows[i][2].ToString();
                    GetLog.Lastname = DT.Rows[i][3].ToString();
                    GetLog.Mid = DT.Rows[i][4].ToString();
                    GetLog.Suffix = DT.Rows[i][5].ToString();
                    GetLog.Age = DT.Rows[i][6].ToString();
                    GetLog.DOB = DT.Rows[i][7].ToString();
                    GetLog.BASED64IMAGE = DT.Rows[i][8].ToString();
                    GetLog.URLLINK = DT.Rows[i][9].ToString();
                    GetLog.FileName = DT.Rows[i][10].ToString();
                    ListReturned.Add(GetLog);
                }
            }

            return ListReturned;
        }
        [HttpPost]
        [Route("DeleteSelectedDependentv1")]
        public string DeleteSelectedDependentv1(DHRDEPv1 dep)
        {
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = dep.CN;
            SelfieRegistration2Controller.GetDB2(GDB);
            try
            {
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@DependentID", mytype = SqlDbType.NVarChar, Value = dep.dependentID });
                con.ExecuteScalar("sp_DeleteDependentv1");

                return "Success";
            }
            catch
            {
                return "Failed";
            }

        }
        //GET DEPENDENT  END

        #endregion
        #region 2018-09-27
        //GET DEPENDENT  START
        [HttpPost]
        [Route("DHRDelAddToken")]
        public string DHRDelAddToken(Token T)
        {
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = T.CN;
            SelfieRegistration2Controller.GetDB2(GDB);
            Connection Con = new Connection();
            Con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = T.EMPID });
            Con.myparameters.Add(new myParameters { ParameterName = "@TOKENID", mytype = SqlDbType.NVarChar, Value = T.TOKENID });
            Con.ExecuteNonQuery("sp_UpdateTokenIDv1");
            return "success";
        }


        [HttpPost]
        [Route("DHRGetDHRTOKEN")]
        public string DHRGetDHRTOKEN(Token T)
        {
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = T.CN;
            SelfieRegistration2Controller.GetDB2(GDB);
            Connection Con = new Connection();
            Con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = T.EMPID });             
            return Con.ExecuteScalar("sp_DHRGetDHRTOKEN").ToString();
        }

        [Route("DHRGetDHRTOKENV2")]
        public string DHRGetDHRTOKENV2(Token T)
        {
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = T.CN;
            SelfieRegistration2Controller.GetDB2(GDB);
            Connection Con = new Connection();
            Con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = T.EMPID });
            return Con.ExecuteScalar("sp_DHRGetDHRTOKENV2").ToString();
        }


        //GET EMPLOYEE HISTORY START

        [HttpPost]
        [Route("DHRGetEmpHistoryv2")]

        public GetEmpHistory DHRGetEmpHistoryv2(DHREMPHv1 DH)
        {
            GetEmpHistory GetDependentResult = new GetEmpHistory();
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = DH.CN;
            SelfieRegistration2Controller.GetDB2(GDB);

            try
            {
                Connection Connection = new Connection();
                List<DHREMPHv1> ListDependent = new List<DHREMPHv1>();
                DataSet ds = new DataSet();
                System.Data.DataTable dtLEAVES = new System.Data.DataTable();
                Connection.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = DH.LoginID });
                ds = Connection.GetDataset("sp_DHRGetEmpHistoryv1");
                dtLEAVES = ds.Tables[0];
                if (dtLEAVES.Rows.Count > 0)
                {
                    foreach (DataRow row in dtLEAVES.Rows)
                    {
                        DHREMPHv1 DP = new DHREMPHv1
                        {
                            CompanyName = row["CompanyName"].ToString(),
                            Specialization = row["Specilization"].ToString(),
                            Role = row["RolePositionTItle"].ToString(),
                            PositionTitle = row["PositionTitle"].ToString(),
                            Positionlevel = row["PositionLevel"].ToString(),
                            JoinedFrom = row["JoinedFrom"].ToString(),
                            JoinedTo = row["JoinedTo"].ToString(),
                            Country = row["Country"].ToString(),
                            Description = row["ExperienceDesc"].ToString(),
                            Industry = row["Industry"].ToString()
                        };
                        ListDependent.Add(DP);
                    }
                    GetDependentResult.EmployeeHistory = ListDependent;
                    GetDependentResult.myreturn = "Success";
                }
                else
                {
                    GetDependentResult.myreturn = "No Data Available";
                }
            }
            catch (Exception ex)
            {
                GetDependentResult.myreturn = ex.Message;
            }
            return GetDependentResult;
        }

        [HttpPost]
        [Route("DHRDelToken")]
        public string DHRDelToken(Token T)
        {
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = T.CN;
            SelfieRegistration2Controller.GetDB2(GDB);
            Connection Con = new Connection();
            Con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = T.EMPID });
            Con.ExecuteNonQuery("sp_DHRDelTokenv1");
            return "success";
        }
        #endregion
        #region 2018-10-05
        [HttpPost]
        [Route("GetDHRRequestFormv1")]

        public GetRequestForm GetDHRRequestFormv1(DHRRequestv1 DH)
        {
            GetRequestForm GetRFResult = new GetRequestForm();
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = DH.CN;
            SelfieRegistration2Controller.GetDB2(GDB);

            try
            {
                Connection Connection = new Connection();
                List<DHRRequestv1> ListRF = new List<DHRRequestv1>();
                DataSet ds = new DataSet();
                System.Data.DataTable dt = new System.Data.DataTable();
                Connection.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = DH.LOGINID });
                ds = Connection.GetDataset("sp_GetDHRRequestFormv1");
                dt = ds.Tables[0];
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        DHRRequestv1 DP = new DHRRequestv1
                        {
                            FORMID = row["ID"].ToString(),
                            FORMNAME = row["Forms"].ToString(),
                            CODENAME = row["CodeName"].ToString(),
                            PDFFILE = row["PDF"].ToString(),
                            FORMTYPES = row["Types"].ToString(),
                            DATECREATED = row["DateCreated"].ToString()
                        };
                        ListRF.Add(DP);
                    }
                    GetRFResult.RequestFormList = ListRF;
                    GetRFResult.myreturn = "Success";
                }
                else
                {
                    GetRFResult.myreturn = "No Data Available";
                }
            }
            catch (Exception ex)
            {
                GetRFResult.myreturn = ex.Message;
            }
            return GetRFResult;
        }
        #endregion

        #region 2018-10-06


        [HttpPost]
        [Route("GetDHRProfileSignatoryv1")]

        public GetPSv1 GetDHRProfileSignatoryv1(DHRPSv1 PS)
        {
            GetPSv1 GetPSResult = new GetPSv1();
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = PS.CN;
            SelfieRegistration2Controller.GetDB2(GDB);

            try
            {
                Connection Connection = new Connection();
                List<DHRPSv1> ListRF = new List<DHRPSv1>();
                DataSet ds = new DataSet();
                System.Data.DataTable dt = new System.Data.DataTable();
                Connection.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = PS.LOGINID });
                ds = Connection.GetDataset("sp_GetDHRProfileSignatoryv1");
                dt = ds.Tables[0];
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        DHRPSv1 DP = new DHRPSv1
                        {
                            FORMID = row["ID"].ToString(),
                            FORMNAME = row["Forms"].ToString(),
                            CODENAME = row["CodeName"].ToString(),
                            PDFFILE = row["PDF"].ToString(),
                            FORMTYPES = row["Types"].ToString(),
                            ISCHECKED = row["chk"].ToString(),
                            AUTOSIGNED = row["autosign"].ToString(),
                            SIGNWITHPERMIT = row["signwpermit"].ToString(),
                            PRINTTOSIGN = row["printtosign"].ToString()
                        };
                        ListRF.Add(DP);
                    }
                    GetPSResult.ProfileSignatoryList = ListRF;
                    GetPSResult.myreturn = "Success";
                }
                else
                {
                    GetPSResult.myreturn = "No Data Available";
                }
            }
            catch (Exception ex)
            {
                GetPSResult.myreturn = ex.Message;
            }
            return GetPSResult;
        }


        [HttpPost]
        [Route("UpdateDHRProfileSignatoryv1")]
        public string UpdateDHRProfileSignatoryv1(DHRPSv1 T)
        {
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = T.CN;
            SelfieRegistration2Controller.GetDB2(GDB);
            Connection Con = new Connection();
            Con.myparameters.Add(new myParameters { ParameterName = "@empid", mytype = SqlDbType.NVarChar, Value = T.LOGINID });
            Con.myparameters.Add(new myParameters { ParameterName = "@FORMID", mytype = SqlDbType.NVarChar, Value = T.FORMID });
            Con.myparameters.Add(new myParameters { ParameterName = "@chk", mytype = SqlDbType.NVarChar, Value = T.ISCHECKED });
            Con.myparameters.Add(new myParameters { ParameterName = "@autosign", mytype = SqlDbType.NVarChar, Value = T.AUTOSIGNED });
            Con.myparameters.Add(new myParameters { ParameterName = "@signwpermit", mytype = SqlDbType.NVarChar, Value = T.SIGNWITHPERMIT });
            Con.myparameters.Add(new myParameters { ParameterName = "@printtosign", mytype = SqlDbType.NVarChar, Value = T.PRINTTOSIGN });
            Con.ExecuteNonQuery("sp_UpdateDHRProfileSignatoryv1");

            return "success";
        }

        #endregion
        #region 2018-10-16
        [HttpPost]
        [Route("GetDHRNotifCompIDLicensesv1")]

        public GetGovIDLicensesNotif GetDHRNotifCompIDLicensesv1(IDNotifDetails DH)
        {
            GetGovIDLicensesNotif GetGovIDLicensesResult = new GetGovIDLicensesNotif();
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = DH.CN;
            SelfieRegistration2Controller.GetDB2(GDB);

            try
            {
                Connection Connection = new Connection();
                List<IDNotifDetails> ListNotifCompIDLicenses = new List<IDNotifDetails>();
                DataSet ds = new DataSet();
                System.Data.DataTable dt = new System.Data.DataTable();
                Connection.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = DH.LoginID });
                ds = Connection.GetDataset("sp_GetNotificationforGOVIDandLicences");
                dt = ds.Tables[0];
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        IDNotifDetails DP = new IDNotifDetails
                        {
                            type = row["idtype"].ToString(),
                            reminddate = row["reminddate"].ToString(),
                            remindmessage = row["remindmessage"].ToString(),
                            renewdate = row["renewreminddate"].ToString(),
                            renewmessage = row["renewremindmessage"].ToString(),
                            ExpireDate = row["expirationdate"].ToString(),
                            ID = row["ID"].ToString(),
                            remindtype = row["remindtype"].ToString()
                        };
                        ListNotifCompIDLicenses.Add(DP);
                    }
                    GetGovIDLicensesResult.GovIDLicensesNotifList = ListNotifCompIDLicenses;
                    GetGovIDLicensesResult.myreturn = "Success";
                }
                else
                {
                    GetGovIDLicensesResult.myreturn = "No Data Available";
                }
            }
            catch (Exception ex)
            {
                GetGovIDLicensesResult.myreturn = ex.Message;
            }
            return GetGovIDLicensesResult;
        }

        #endregion

        #region 2018-10-17
        [HttpPost]
        [Route("UpdateDHRReimbursev1")]
        public string UpdateDHRReimbursev1(Reemburse samp)
        {
            //string def = "/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxITEhUTExMWFhUXGBoaFxcYFx0fGhcgGh0eGB0YHiAfHSkgHR8lHx0YIjEhJSktLjAuGB8zODMtNygtLisBCgoKDg0OGxAQGy8mICUtLS0tLS0tLS0tLy0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLf/AABEIAL0BCwMBEQACEQEDEQH/xAAcAAACAgMBAQAAAAAAAAAAAAAFBgQHAAMIAgH/xABOEAACAQIDBAYGBwUFBAkFAAABAgMEEQAFIQYSMUEHEyJRYXEUMkKBkaEjM1JicoKxFUOSosEkU2Oy0TREwvAWNVRzg5PS4fEIFyWjs//EABsBAAIDAQEBAAAAAAAAAAAAAAAEAgMFBgEH/8QANxEAAQMCBAIIBwABBAMBAAAAAQACAwQRBRIhMUFREyIyYXGBkdEUQqGxweHw8RUjM1JDcoI0/9oADAMBAAIRAxEAPwCXX5qkdgTdibBRqSe4Aak+GBCj1tHVsm/MVpIe+X6w/kuN3n6xB8Djwmyk1pcbAJZkzWkgN4YzPL/ey8Ae8C36AeZxWZRwWhDhz3avNvuguaZ1NNrJISPsjRR7hx8zc4qLi5aLIY4RdoTf0f5JFTw/tetXsqf7HCeMr8pLfHdv3FuSnFmjG3KQIfWTCOPZBs3zOWpmeaVru59yjko7gOA/1wm5xcbldZBAyBgjZsP66hk48VytTY7Y/wBHpzUzL9NIvZU/ulP6M3PuGnfhyGPL1iuSxbEOmd0TD1R9T7KBmdGs0bxNwYEX7jyPuNj7sXEXFllRSGN4cEhbFbLisrjBOSkUCtJUWNjZCBug8rk8e4G3LFMbea06yckAM4pu/wClmWg9UMpg9GGgIVRLbk/q6Nz9a/jiHxGu2ibGAuyXL+t4aeqn5Zkw3mqMnqusHF6SZrSLy7JOvK3b0PHfxe14dssaopJac2kHnw9VGzvb19YJkaKUesjix89eI7iNDiaWQWPOGOoOBCm5evp1VBS+yzb03/dpq1/xaJ5uMCEN21zj0qslkHqA7kfduJoCPM3b82M+R2ZxK7jD6foKdrTudT5oCZbag2I1BHEeI7jiFk05zbWKO7XZxHmFJHNwroLI5A+vTlw531HjvD2gcOt67dd1yVQDRzEx9k/1kcyKgGSUvpE4D5jUL9HGdRTqeZ8e88z2RoCSPcIxpuvaWnfXy3d2RufwkyonZ2Z3Ys7ElmPEk8ScJXvuuuYxrGhrRYBa8Cknfo2qlk6/L5GstQpaM/ZkQXuPGwVv/DwxTusbLAxymzMEw4aHw/z90/8AR9nbzRNFMN2aFjHIO5kO6fMXHHDa5hNmBCp3pc2g62YUqHsQ6yeLkcPyg/Fj3YTnfc5QupwSkyMMztzt4ftV9ihbqdNhYUpop80nF0gBWFftyN2dPiFv95vs4vgb85WDjE5dlpmbnf8AHupWQQxVV5aWqaGvclpIpydyZjqSp1t+W9gPUGGGStcsWpw+aAZiLjmPzyTNl+2EtPIIK6Non5E+q/irDRh5HS+tsWJFO1HXJILqwOBClYELMCFmBCzAhUrQ5qKHLhXIqtVVMskauwv1SIzrYee5e3MtrcKBiuR5aNE7QUzZ5CH7AXVd5tm09S+/PI0jct46DyA0HuGF7k7rcbGxmjBYKCTgUrpn6P8AZVayRqioO5Q0+szHQSEa9UD8C1tbEDiwIuYABcrKqpnSOEUepKnbX7RNWzb1tyFOzDHyVe+w03jYX7tBywrI8vK6Ggom0sdvmO5/HgECxBPKxOi7ZDrmFZOv0Sn6JT7bA+ufuqeHefLViGO/WKwcXxDIDBGdeJ5dytLNlvGfLDa5hVxKNT54EKDslABmVUoH11FJfzVkW/vDD4YiRurmSatvwKr2PgPLGaF9AXwVTRkOjFXXVWUkMPIjUYk3fRLzluUh2oRFtqhUAJXxiZeTgWdfHS2vitj54bbKeK5qfD2uN49O5apsmYKZaOYTRjijECRfDkD5HdPni4OB2WVJE+M2cEX6Pcrq5Y6iq6xKSJgYjUTaboGjBF0ubmxJIF1HEgjHjrW1RCHF4yi55L7X7ByiF5aKrhrhGLukY3ZAPBd5t7S5tcE20udML9E09kre/wBTnjIEzCLpEkqri99MRDUw6ou3NdWLsNkSUEIzWuW7kf2OnPEk6iRh38x9kdrja1xIjbcrIa19bNkZtz7uaX81zKWplaaZt52OvcO5QOQHIYTcS43K62CBkLAxg0H9dRMeK5ZgQt9DVvDIkqGzxsGXzBvbyPD349BsbhVyxiVhY7Y6K36SVFroqyL6muiVj4SKADfxK7unej40Qbi64GWMxvLHbg2TFtdnq0dK8xtverGD7Tn1R7tSfBTiMj8rbq6jpjUTBg24+C55kcsSzElmJLE8SSbknzOM9d01oaABsFuy2heeWOGMXeRgo8L8SfAC5PgDj0C5sFCaVsUZkdsEw9I1cgeHLYD9BRgb5+3KRqT4qCfzO3dhiU5RlCwcNidPK6ok47f30SrhZdCm7KNuJAnUVsYq6c+y9jIvcQx4kcr6/eGLmTkb6rIq8Hil60fVP0/SYMsozbrsoqOuQavSytaVB3KW1PcN/jqd84ba8O2XNVFLLAbSC329UxZDtujsYpgYpV0ZHBVgfEHXEkunCGZWFwb4ELZgQswIXN4zFFRqOpDejmQvHIvGFmuTy4XLG+vrNfQ6Rc0OFlbDM6J2ZqEZtkUkIDgiSE6rKmq2PC9uHnw8cLuYWrcp6tkotseXsvGy+z82YVK08Wg4yyco05sfHkBzPhciTGX1VVZU5BlG6bNttoIVRMvouzSQaXH75wdWJ9oXub8ySe7EJX5jYbJvDaUU46WTtHbuHuUn+ljFORafxIRzZLLhUzfSXEKayEcW7kB7zzPIe7FjIsx7knW4mIWdXtHb3V1wbQxIoVVCqoAVQLAAaADww6uSc4uNzuvFdtIhQ4F4q5zHaBFY+eBCH7ObThMxWQjQxyIffY/8OBANknemqNO7TGf0ZXbtrWlt1Dnq74sDLJOWpDitRqBiWVUmdoR3YPIpcwqxHGzRxIN6eUabifZvw3m4AHuJ4KcWMZxWdV1IcMgCaNts/Sdkp6cBaSn7MSjgxAt1h7+4X5XPFjheWTMbDZbuF0Hw7M7h1j9By90L2azs0dTHUC9lNnA9pDow8e8eIGIxuyuuma6Fs0DmHy8UwZbsjSPVVGZzEDLkfrIo7aTOdSoHtIH4KNCSF4KRh12UdYrkI+mlIgbugm1G0EtbOZZNBwjS+ka93iTxJ5nwAARe8vNyuwo6RlNHkbvxPM/2yEYim089G+xvpT+kTr/Z0PZU/vWHL8APHvOnfi6KLMbnZYuK4j0I6KM9Y79w91o6VMk9HrOsUWjnG+O4MNHH6N+Y4JmZXX5qeDVPSwZDu37cEm4pWurB2DqjUUc1GNZYT6RTd+h7SDzJIv8A4xw1Tu0yrmMdprPEw46HxH6+yV9tttDWvGo+riXQd7H1m92i+49+PJusbKzCQ2GMu4n7IFFLfC5FlvMkDlY3R9QGCB64j6WQ9TSgjmfWfxAsfdG3fhmnZ8y57G6u5EDeGp8eARbNei6OSPrIXKTnV98kpIx1JbmpJ4kfDEpIQ7UJahxV1OMjxdv1Huq1znJqilfcnjZDyPFW8VYaH9e+2FHNLdCungqYp23jN/uoGPFettNUPG4eN2R14MpII94wAkahQexr25Xi4705U22EFUqxZpFvldEqohuyp4nd1tzO7oeanDLKjg5YFXgl+tAfI/g+6P0wrKRBNTSenUnJo9ZVA5Mo9a33dfujDIIIuFz8kb43ZXix7007PbXwVIFmF+6+PVBMYcd+BC5yliDCxGBCG5c9SlQKek7fWG3UnVWLG2g9k8SW4WBJ0GBCdc8ljyumOX0pBqJe1WTLpa4+rXuFjYDkvixOFpZLdVq3sLojKenl2G3f3+AVZ1B1xU3ZacxJctuW0LTSBF05kngoHFj/AM9wxMAk2CUmlbEwuKfsvrI4l6mnjkkCcerRnNzzbdB1OGgABZc7LI6R2ZylDNJP+zVH/kSf+nHqrWuozRt03gmHnE//AKcCEjZlUMXP0cg80b/TAhQIqgrPG5VgBvX7J5qw7u84ELbkOy1bmDv6JCWRWIMhIVBztvHibW0FzqMV5E6aqwAC27Q7FV9AFkqoPobgGRGDILm1iRqvvtj3IvPirhBqbLmqJo4aa7ySMFVOdzzv3AXJPIAnHuVQ+IJGqtbPWiyykGVUzBpGG9WTDi5Yep5EaW5KANSScLzyfKFsYPQ5z8RJ5e/lwSWThZdITYIvsZs16fI7yt1dHBrPITYNYX6sHvtqTyB7yMMxR/MVz2JVznO6KPcqZtjtH6U6xxL1dLCN2GMCwsBbfI5G2gHIeZxVLJnPctDDqAUzLu7R37u5LuK1pJl2G2Vaum1usCEdY/fz6tT9o/Ia8xeyOPOe5ZuI1wpmWHaO3ur5pqdI0VEUKigBVHAAaADDwFtAuNc4uJc43JS30j5J6VROFF5IvpE7yV9Zfetx52xXMzM1PYZU9BUAnY6H+7lQowiu2RPZrNzSVUU44I3bA5qdGHw18wMSY7K66VrKcTwuj48PHgtPSrlApMwZ1t1FSOujI4Xb1wPzdrycYbkbfULlqOfJdjuCDZBBJPIqxRyuCQGMcbPujmeyDyvivoiVojEo4gSNVclLnAWpiSenkp4YkEcAkRlUm1yQSLE2AFuPZvzOGQLCy517y9xc7cqx4ZQwBU3Bx6orVX0MUyGOVFdDxVhcf/PjjwgHQqccj43ZmGx7lWO0/RaReSia4/uXOv5HPHyb44WfB/1XQUmN/LOPMfkeyreqpnjcpIjI68VYEEe44WIsbFb7JGyNzMNwtWBTU/J85npX34JGQ8wPVbwZTof+bWx61xbqFRPTRTtyyC/3ThDndDXG9R/YqvlUx/Vufvg6fxcuDjDTJwe0ubq8Gkj60XWH1/aY46POEAVRDKo9WQSWDjkbEaeWvmeOGFikWVXVtSEUk4EJoyqlGT0xqZgDmNULRof93TvPjzPebLwBJqlkyDvWjh1CaqTXsjf2SRK5YlmJZmJLEm5JOpJ8ScI7rsw0NGUbBQpKVnYKgLMxsAOJJ5YsYb6JCqaGgvOybGyk01OI1I62QqGe2gLGwH4Vv79Tzw4xuULk6qoMz78OCZNrdopMvdaChtDHCq7zbqlnZhvEksCDe4JNrkk4omlcDYLYwvDYZYulk1udkCXpBzMf7zfzjj/9GKumfzWkcJpP+v1K9P0j5kB9cv8A5af6Y9Ez1B2EUgHZPql6q24rCxJdSfwDFwkcsx9DTg7H1QzMNsKuQbgYEtoAF1JOgHHFjXOO6RnhiYOqrE6QqlqUQZdAxSGKFS4UkGRmJuWI1PDe8S5J5Yoneb5QtbBKVhjMrhc3sLoNs3tdPS3Q/TQNo8MhupB42vfdPyPMYrZK5q0KzDYagbWdzH55plyvK4k62vyVQ8pjKtTSHtwX1PVg8jod29ju2U27OGw/MOquWlpHU8obMNOY4+Crc1JZm397f3jv79w29ftb19b3ve+EnNIOq7CnnjkYMmynbN5FLmFSKeLsoO1NLyjXv/EdQBzPgCRbFHfUrOxKvEYyN3TFthnsPVrl9EN2kh0JH75gblifaG9rf2jr3Y8lkvoNl7hdAYv96XtHbu/f2SlilbSLbM5BLWziGPQcXe2ka958eQHM+FyJMYXmwSlZVspo87vIc10Bk+VxU0KwxLZFHvJ5sTzJOpOH2tDRYLiZpnzPL3nUqbiSqWYELnvbnJfRKySMCyMesj/C5On5TvL7hjPkbldZdvh1T09OHHcaHyQDEE+n6jaKsyoGaITSZc29uE2LREHgRqLLfz6kYchfdtuS5HFqXo6nNsHa+fH381uybMqGrtBStUUM7D6J+sLoWtcKQzEe7Q9xvj0TBxtsoS4ZLAzpdHAbrzsLm1VNUy5ZXkzKRIjh9SjJ7Sta9jyP4SLYixzg/KVbVwwPpRPGLHT+8U79H0rGn3Wbe3SVDfasbX9/HDCxU04ELMCELz7Z6mq03Z4w1vVYaOv4WGo8uB54i5gdumKeqlp3XjNvt6Kp9qOjepp7vBeeLuA+kUeK+15rr4DCj4S3bVdJSYxFL1ZOqfp+vP1SRilbKzAhSocwnRQqTSqo4KsjADyANhguearMMbjctB8gmfZHLo44/wBrVo+iQ/2SI8Zn5SW7gQd3yL8ACdF7w0XK4ampn1EgjZ/gc0t5zmktTM88pu7H3KOSjuA/9+JOM9zi43K7engZBGGM2ChE48V6tvo+2J6uH0mdfppF7Ckaxqf0Zhx7hpzOHIY7alcni1f0ruiZ2R9T7BDNrcoJV01HcRofdi9YqHbSr6fRrXAf2intDWKBxt6soHdre45MRfsHC08fzBb2C1mR3QO2O3j+0ivKBhay6N0gaoNVVaaYsa1Iz1GmiESyYuAWNI8onsPRdfmVHHxBnQnyQ77fJTi1qQndonbpBq+szGpbkHCD8ihD8wcJym7yuswyPJSsHPX1/SXsVp5SKCulgkEsLsjrwZTr5HkR4HTHoJBuFCWJkrcjxcJjzKtizgpEINzMuAlj0R1GhaT7qjjfUabpN90tscJB1guUqoXUL7xO0PBTdoa6HL6f9mUTXb/e5/adiNVuPge4WXvxCWS3VanMMoTI74mbyH59kkYWXRqXleXS1EqwxLvO50HId7E8gOJOPQCTYKmedkLC950Cv/ZTZ6OigESasdZHtq7d/gOQHIYfYwMFguJq6p9TIXu8hyCM4mlVmBCzAhIPS9kvW0wqFHbgOvijWDfA7reQbFE7btvyWzgtT0c3RnZ334eyprCa61MnR9nApq1N76uX6KQHhZzoTy0a3uLYsidlcs7FKbpqc23Go/P0W/K9lHizoUa3CRSrMrd0SkSKfHlHf7WLXRnpBZZkVe34F2be2XzKPUNcokzbNYxfeYU9MQCd5rLHvgDiCwRtOQOLmi7iVlTOLIGRH/29k3bDVkKwrCLq6gXVgQw05g64sSSa8CFmBCzAheXcAEkgAC5J4DxwIAvsqW6Sc7oaiS1PEGlB7dQvZDW5AD6z8R918JzOaTp6rrMKpqmJt5DZvBv9t4eqSMULZWYF6ju2G0TVkwKjcgjG7DGNAqjS9uFzYeQsOWs5H5ys+gom0sdvmO5/HgEBxBPqwei/ZDrmFXMv0SH6JT+8YH1j91T8T5asQx36xWFi+IdGDDHud+4cvNW/JIANThtcukrbCthAJuLjAhVdBtOKWpMg+plXq51IuCp4MR90k+4sOeBegkG4QDaHLjBMU4oe1Ge9Tw15kcPnzwo5uU2XSU9T07Mx34oNUtpj1qJnWC9RTxOoHVgvwsBqT3gDv/1xeAFgyucHbp36HcqP7VSR03BDDLJ5aCPW2nBzp4eGJqq5duhVXUGSR5Dxd2c/mJb+uMwm5uvoEbMjGt5ABeIomZgqglmICgcSSbADxJwKTnBoJOwWl0dnEUaM0rNuKgHaLcN23Lxvw54mxmYpOrq2RMzXVgSbuS03URsr5hOoM8o/cqeCr5cu83Y20GL5H9GMrd1i0VK6ul6aXsjhz7vdIhOFV069wQs7KiKWZiAqjiSdABg32Xj3BjS52gCvXYLZJaGK7WaeQfSN9n/DXwHfzOvcA9FHkHeuMxGvNU/Tsjb3KasWrOWYELMCFmBC11MCujIwurAqw7wRYjARdetcWkEbhc3Z5ljU1RLA3GNiAftLxVvepBxmublNl31NOJ4myDj9+KgkY8V6sLP85lkypa+H69ENLUsPWCsQvWXGt9bju62/s4da+7L8VxlTSiKr6M9km48D/WQva6qaipcuoIW3XVfSJiLGzNcAaixG8Zf4BiLjkYArqeJtXVOcR1f6yPZNnklVRPNJb0ijeO0gAXfRyAVNtOG8bCwuFxKF5cNVVitEymkGTY8OSs6gl3o1bvGLllqRgQhO0W0VPRx78z2J9VBq7nuUf1Og5nEHvDRcpimpZah2Vg8+AVMbW7aVFaSpPVwX0iU8fFz7R8OHhzwnJKX+C6yiw2Km13dz9v66WsVrSWyngeRlRFLOxsqqLknuAwDXZQe9rGlzjYBPFL0WVTIrPLFGxFyhuSvgSNL+WLxTu5rGfjsIcQ1pI56JCxQttHNmMi9IMksl1poEaSZ720UFtwHvIB15C57r2RR5ys/Ea4U0enaO3umDLenekVFQ0UsYVQFWNkYKALWF93QYfAtouMc4uJJ3Kg5z0wQTi0fWRj766/ys2BRS+ufR1BIEu8bEkG40HE6jAhBcwkjYHddD5MDgQpGX5h6TTCl3WeoiP0G6CzOv2LDU2At5BTyOIPbmCZpagwuvwO61Vmy8kQDVjrCD7AIL++1wD4DePljwNDd1bLUPmNmhRv2lHECtPGF73bVj/wA+PwwF/JDKXi8p06KpHWlzasY3IhWJGPewckfEx/HHjjZhK9hjD6pjBzCXQMIrt0U2WqVjq4pG4Rln/hUkfzbuLIRd4WbisoZTOHPRPtdULQBq+dFOYVClYYyNYl+0/jwuePBR7RwzI8M8VztBSSVbg0k5Bv7DvVaVNQ8jtI7FnYksx4knnhI73XYsY1jQ1osAtWBSVy9Guxno6ipnX6dh2FP7pT/xkce4ad93IYsup3XJ4piPTnoo+yPr+uSfsXrGWYELMCFmBCzAhZgQqt6Zsl+qrFH+FJ8yjfHeHvXCtQ35l0WBVNi6E+I/Kq7Cy6ROPRrWoZZaKb6qrjKEdzAGxHcSCwv3hcXQOs63NYuNU+eESjdv2PskvaKWojrZFqwd9T1asQQrCPsgr4H1vz4tmaSs7CahkZsd1YWzsAGVxINWrKoE9+7FqPdvRj+LEoG2aqsZm6SosNgAPyrdo491FHcBi5ZKRNsOkmOG8VJaWUaF/wB2nl9s+WnjywvJOBoFtUODvls+XRv1Psqlrq2SZzJK7O7cWY6+XgPAaYVJJNyuoiiZE3KwWC0Y8ViN7M7LVFYSUASJfXmfRFtxt9o+A95GJsjL9kjWV8VMOtqeQ/tFZGz2Wxwgx5et2OklY4uW8IwRa3j6vDRr3w4yMM2XJ1dbLUuu86cBwRobF0zayKJHPrO/aZj3knU4sSao3KcteolWKPidSx4Io4sfAfMkDnjOa0uNgu9qqllPGXu/yrSp6iOngFNAv0YBBJ1Lk+szd5Pw5cMPtaGiwXEVE755DI/cpO2woKcUs8hghDCNrMI0BBOgIIGhuRiSpVXbK5ak0j763VUvxI1uAOHvwIRyHKkV2SlSSSZ0ZVjW7HXnYC/drwGBCP5T0TJAgmzepWnQ6iFGDSt4aAi/gob3Yi5wbqVdDTyTHLGLohXbZ09LGYMqpVp0OhlYXlfxNyf5ifIYoMxPZWvHhTY9ZTc8h7qusxqnkcvIxdjxLG5/58MAXr2tbo0WUORrDEgFS9waLq0ckgMGzjbws1VV3sePYI0//Tw8Tgm0ZZeYQ3pKvNyBP4SrNIFFzhMC66uSQMFynTYzKEpIRm1aOI/skHNydRIR7rjuHa47tmxaNtyuXmfJXziKPbn+Ut5vmktTM00zXdj7lHJVHID/AN+JOFXOLjcrpoIGQMDGDT+1UPHiuVm9F+xm9u1tQunGBDz7pSP8vx7sMwxfMVzmL4jvBGf/AGP491a2GlzizAhZgQswIWYELMCFmBCgZ5li1NPJA/CRSL9x4hvMGx92IubmFlbBMYZGyN4Fc4VNO0btG4s6MVYdxU2OM4i2i75j2vaHN2Oq8wysjK6mzKQynuKm4PuIGBevaHtLXbFWXtRnUYjp656dJ6SrUJUxEDsSKLAi+l7Bl1t9WtiL4eMnVDlxbaI9O6AmxGy05dlMUpilyuo31hDH0OY2dA1r7pJv7IHauPv4k17XbKmppZoHf7g8+B80F2023rJnalkjelUCzxHR38WPNT3Loe84omc7bgtrCKentn3d9ko2wsuiX1FJIABJJsABckngAOZ8MCiSALnZOdJstBRxipzV9wHWOlU3lltyIB8tARa4uRwxeyGwu9YVTirnu6Kl1P8A29vcoomaSV9MZYvooaVxvUSAWMfJzb1iAN6w7PZYWJAJZY9rtlh1dNLC4GTjrdWVkVRG8KtHaxA4YmlERwIVf5ZssKKktoZnsZWHfyQfdX5m5xXFHkHen8QrTUyX+UbD8+JQg4sSCWtti8sBpYUaSaZlVUUXJswY+6wNzyGBC17OdHQoUMuaVawBwPoIyGla2tri/kQob8QxFzw3dXwU0s5tG2/2U+fbWOnQw5ZTLTIeMrANK/K5vfXxYsfLCz6g/KugpcDY3WY3PIbeqTMxrXdi8js7nizEkn3n9MUC5NytY5IWZWCw7kIka+LQFnvdfVQmBJsASe4YtCzpXAG5RrI8pCxvWT6Rx36tftuNL+Njp5+RxaBbUrOkkMrsrU97ZSdVluVwnQmIzOO5nCtb4u/wxTUHYLVwJob0kh7h/eigbD7NxzK2YVvZooT2VI+vYG27b2lB0t7R7PfgjYGjM5RrqqSokEESjbVbQyVs5lfRRpGl9EXu8zxJ/oBhd7y83K3KKjbTR5BvxPM+yD4inE7dHGx3pb9fMv8AZ0OgP71h7P4Rz7+Hfa6KLMbnZY2K4h0A6OM9Y/Qe6uxRbDq5JfcCFmBCzAhZgQswIWYELMCFmBCpnpeyXqqlahR2Jx2vB1Fvmtj+VsJzts6/NdVglTniMR3bt4H2KQsULcTtsOBV0tVljmxkXrYCfZdbH/MEa3dv9+GIDcFhXP4zEWOZUt4aH8JHpZXRr9qORCQbEhkYaEXGoINxilwLStaB7Z4hfUFOe0lQ1ZlEVXNrPT1HU9YRYyIwGnjxX3oe84vvmjuViOiFLXhkexF7cv63ogezuz1RWPuQpcD1nOiJ5nv8Bc4oYwuNgtqprIqZt3nXlxKeYaRMvhkegiFXVIp36hh2EA9bqwPWt3KeRuxI3cNiMRi41K5mWtfWyhjzlbfb35qsquolmkM08jSytxdv0FtAPAADCrnly6Smoo4G2aEV2Wz16KoWZbleEiD20PEefMeIHjjxjy03UqylbUxFh34HkVaeVVK0k6CM3pKkb8DDgpOpj8LXuB3aeycaANxcLhnscxxa7cJ5Vr649UVVY6V0cWlpGH4JQ3yKr+uFhU9y6F2AO+V48x/lCqra+ja5CzJ+JFI/lY4kJ2lKyYNUN2sV62GzEMM0qYDvTxQL1V11UHrCxAI11VSR9wd+Jud1CWpOKny1DY5dASLpHnneRi7szseLMSSfMnCBN9V3DGNYMrRYLTI1sAQ51ghs8lzi0CyzJZMxTHl+yBKCSZwFIuFQgk/m4fC+L2x31Kx6jEA27WDXvWuky1ZZTDEAka6yuOS91z7RsQL8NTyti4ABZT3ukOpUPaOtFTJFTQ9mEMsUQHA7xCb1v08PM4qzZnLS6DoICTuVYmeZOMzzJgW6uho4kWaS9h2bv1anvIIufZGvMX9LbuuVVFUlkHRR9px/SA7ZbSCqZIoV6ukhG7DGBYaC2+Ry00A5DxJwtLJnPcujw2gFMy7u0d+7u90t4qWmmPYjZV66axusKWMr/wDAv3j8hr3XsjjznuWdiFc2lj07R2H5UvPNqM/o6qaOmpJPRI3KQJ6IWjCL2QQyKCd4C+rHjh4ADZcY5xcS5xuSotL081iErUUcTMpIYKzxkEHUENvWPLHqimDLunylawlpJ0Y8oysn6lCfhgQnPNekfLKaoemnqOrlS28DG5AuoYahSOBGBCIUO2OXTECOtp2J4L1qhvgTfAhGo5AwuCCO8G+BC9YELMCFmBCzAhANuMk9Lo5IgLuBvx/jXUD3i6+THFcjczbJygqfh52v4bHwK57wgu6UzJsxanninTjGwa3eODL71JHvx605SCqaiETROjPEf4TjtLstDJX+k9ekNHUxioMhtx0392+lzdWuebnQ8MNvjDzfguYpcQdSsMZF3A2AXvNjTVNLAYbrllNNZwAeskc9lXbmAS9rWuetB09mQa0tsNkq+eeKbpZO0RxTll2XPPGqKgpqQDswpoXH+IRxvzUaam5bjiwAAWCTkkdI7M43KaKSjSNQqKABj1QVI9I2zHodRvxj6CUkpbgh4tH/AFHhp7OEZmZT3FdjhVb08WV3ab9e9KWKlqJ42CzJZo2y2dt1XO9TPzikHasPM6259oe1bDEEluqVz+NUWYdOzh2vdMse23o46iosk0fZcE8xzHeCLEHmCMNrmlUWMxfRlhGBeELdkuaT0NQKmnI3rbro3qyKeKt8Ab8iMXRy5d1j4hhwm6zd02RwZfmlzSkUdZxamkP0ch4nqz8fVHLVRxxN0TX6tSUGJT0pyTi458f2kfOqWWGQxSoUYcjwPiCNCPEYrDC3daLqpswuw3CG4kqVNyquqd9YICWMhsEPq95b7thckjkNcWR3vos6vEeW5GqM59UrTRehxNdjrPJzYn2f005AAd+JSP4BVUFN/wCV3kh+xeRT1tdFHCLCNhJJIR2YgpuGPebjReZ7gCQRt4or5geoE6babQx7goKPSmjPbe+s7g3JJ5je1vzOvADFM0l+qNlq4Vh3RATSDrHYcv39knYoW4imzmRS1k4hi05u5Gka82Pf4DmfeRJjC42CVq6tlNHnd5DmV0DkuVRUsKwxLZVHvY82J5k4fa0NFguInnfM8vedSppOJKpcj7OJ6ZnMRsCJqwSEEaFTJ1jaH7t8CF01V7E5bIwdqKDfDBgyxhWuDcElbE6jngQuaekFzU5zVKDq1T1QJ+6RF8NMCFZuYdAEDa09bIv/AHkavf3qU+NsCEZ2P2cOz9FXVE7rLwcblxvBAQqm47JLMRz4jAhVzSbd7QZnUMlG7AgFuqiCKqLcDVn8SOLc8CFOqNrNqMv7VSjtGOJkhV0Hm8Y097YEJz2q6WZMvNNHPSB5ZadJZlWTcETOSNwXVr2sefdrgQvND08Ze2kkNRH47qsPk1/lgQmGh6VsnlNhVqp/xEdPmV3fngQqy25o4kqTLA6SU9ReWJ0IZTqQ6gjTRw2nK4GEZm5XLs8JqempwDu3Q/j+7kvYqWmnWgh/aGUTUtt6ejImg7yupKj3dYv5kw1EczC1czicQgqmz20O/jx91P2foeqoEpiQZquWOUoP3UaFWBbxJS35jb1TiyFhaNUhiVU2eQZdgLK2aSPdRR3DFqzltwIQzaPJY6uneCTgwurc0Yeqw8j8Rcc8Re0OFlfTVDoJBI3h9lzvmFFJBK8Mgs6NusP6jvBFiD3EYzyCDYruopWysD2bFaUYgggkEEEEcQRqCPEY8UyARYq1ss6RaJokNVFee1pCIgQSNN69uYANuV7csNtnFtVy82By9Iejtl4apWpKnI51lYU1dEsWjsCGC3vr67X4HlgMLN1BuK1gcG6EnuH4WpaXIn0TMpoyeHWwNb3nq1HzxHoWcCmhi1U3tRj6rYmyVJJpDm9G55KxCn4b5Pyx58PyKsGOgdqM+v6QDabY2aKxEkEmtw0bk+R9UW+OJMie0paqxGnnbaxUalzicL1VfCZ4vt3BkTxve7eejcdTwxda41WQH9G7NG5R83ylFj6+nlEsPwdPBhp+gPhzxUYyNlpxYg1ws/Q/RM0GXfsii66UD0+qFkQjWCPjY+PAnx3V9knE3HILcUvFG6rkLj2R/WSXluXz1c608ALyyHiToObOx5AcSf1Jtitrbp6pqGxNsFZGeVcOW037Mo2u5/2ucaM7EaoO7ut7I04kkeTSW6rV7hdAZD8RLtwHPv8AZJGFl0qlZZl8lRKsMS7zubAfqSeQA1Jx60EmwVU0zIWF7zoFfuyWzkdDAI01c6yPbV2/oBwA5DxucPsYGCy4isq31Mmd23AcgjmJpVB9sK7qKGqmHFIJGHmFO787YELnToLo+sziFuUaSP8AyFB82B92BC6hkcKCx4AEn3YELj3JMubMsxWLe3GqZXJa192+85Nri9teeBCsiXoazWD/AGSvW3hJJEfgLj54EK5M6yJamhejkY2eLcL8TcAWfXiQwB144ELmWsy/M8hqw43omFwsqi8Uy8StyLMDYEqdRobA2OBCuDo86YIa1lp6pRBUMQqMD9FKTyF9UYnQKSQdBe5AwIVjV+VU84tNBFLpb6SNW07u0DgQuY+mqngizSSKnijiSNIwVjUKu8V3ybKLXsw+GBCsrLOhWgmpKd2aaOZoY2co4sWKgnRlPM8BbAhGNsNjEiymOKG7GjXeUkC7Lxkvbv8AX81GKZ23bfktXCKnoqgNOztPZVFhJdijexmc+iVkUpNkvuSfgbQn3GzflxON2VwKSxCn+Igcwb7jxHurP2ayeOmrqhDrch4r8o29VR3KpDIB3JjQXDJ4wIWYELMCFXvSxsx1sfpcS/SRD6QD2043811PkT3DC88dxmC2sHreif0L9jt3H9qnsKLq1mBepi6Iisr11C3+805ZSeTJdfj27/kw63rNIXG1J6Kdsg4JaaAcCtiNCO48xhK5XW9GxwvZRqmFLcBibSUtNFGBshUtIvEC3li8PIWRLSsdwWtnkUaSOPzn/XEw9JPpGhP3RfkKoj5vWk+jxaQoeM8inQ25hW0F/a103TeZcGi5ScUDppRHHxQTa/O5KmZ55T2m4DkgHBB4D9bnnhVpLnXK6SWNtNCGM4f109UKLk2XxlBeurUDvIbfQoQDur5XA8WuToAMXSPyNsN1mUFL8ZKXv7I+vd7pJJvqdSeJPE+OE11gAGgXzAvVMyzNJqdi8EjRsRYlbajjbUcMehxGypmgjmFpBcJhpekjMU4ypJ+ONf8Ah3cWCd4SD8GpXbAjwPvdGKTpbqB9ZTRP+BmT9d7ExUHiEq/AIz2XkeIv7LdnXSBR11NJS1MNRGkoAZomQkWIbQt4juxMVDeSUfgMw7LgfUIb0e02TZfVNURVk12jMe5NEQBdla+8q29kD34kJ2HilX4RVt+W/gQrFzfOoKmlnjpaqBpXidY7yqLMykC/Mce7Fge07FKPpZmdphHkqFoujvPaKZaingu8dykkbxONQVJCkknQkariSo2RtukTaSn+upmNuclKwHxTdGBCaqrpeemoaGeogR56nfZ442KhI1cqri+8bsN0gE62bhgQpH/3gyWqiaOpWQKws0csO8D/AA7wwIVDZ2kDVrjLxJ1RkAgBvv6kWA5+twvra19cCF2NShgih9W3RvedtfngQuTdvJTV5xUheL1JiU/hIiB8tBgQutY0CgAcAAB7sCFjqCCDqDoR34EA2XOm1WTmkqpYPZU3TxRtV+A081OM57crrLu6KoE8DX8ePj/aoTiKbVrbP5qZqKCpveWkbqZu8xmwDHv9g3Pc+HoXZmri8VpuhqDbY6hWRTShlDDmMWrNWzAhZgQvhGBCofpC2Z9CqLoPoJbtH3Kfaj93LwI7jhCVmQ9y7LDK34iKzu0N+/v90q4qWovGx2Z+jZjSz3solCvr7Mn0bE+QYn3YcjOq5TEGXZfkmDpAofR6+oW1lLdYvlIN/wDzFh7sUSNs8hbWHz56Vrjw09P0k+aS5x6BZVSPzFGaLZaSWFZY5EIYG4a4IsSCOBB1Hhi0Rki4WbJXMjeWOBW3ZfYmWrq+pkskMY36iQMOyn2QeTNYgX4AMeVsTYwjdK1NY14yx8UY212hWodYoBuUkA3IUAsDbTft4jQdw8ScLSyZz3Lew2h+Gju7tHfu7vdI+YrvXGJM0UKwZ9FZeX1UGb0dPHJMtPW069UOt0ScCwBU8ybA6ag72lrHF8jOkFwsajrDQyFrhcFaajo2zBRdUjlHfHKNf4wuFzA9bTMZpXbkjxHtdCKrZWuj9ekm9yb3+W+IGNw3CaZX0z9nj7fdCZkKGzgqe5gQfgcR23TTXNd2TfwXkHApLMCFmBCzAhfCMCFupqh49Y3ZD3oxX9CMAJGyg+Nj+0AfEXRel2wzCP1auX8xD/5wcTEjxxSr8OpX7sHlp9lJrNtqidQlVDS1Kj++gBI8ipFj4jExO9KPwSmO1x5/pCimVObyZZu95hqZB/K1wPccTFTzCVfgA+V/qE07I5lkNJIJEo5klHCR/pN3xXtndPiFBxMVDeKVfgVQOyQfO33Vh0u32XScKlVP31Zf8wA+eJCZh4pR+F1TPkPlr9lUGz3RjUenwVHplHUIs6Su0ct3IDhzdd21zbhfniwOB2KUfDIw2c0jyXQ2PVWswIVb9MmS78UdWo1iO4/irHQ+5v8AOcL1DbjMt3A6jLIYj823iP0qkwoupTZ0bZosVV1MmsNSvVODwub7nzJX8+LYXWdbmsnGKbpYMw3br5cfdWrspMyh6Zzd4WKkniw4q3vUqffh5cemHAhZgQtU9SiC7uqjvZgP1x5dSDXO2CU9sMzyypp3glq4QTqpDhijD1Wstz594JHPFUjmOFiVoUUVVFIJGMPpuFRxFtND4jgfEeGEl2Y1F0BqkupGGGmxWBO3MwhPvSBmHpUFDWDjJCEkI+0vEe5utHuxOUagpPDJSGui8/dI+K1opq2RrpHQ0kI3pnk+jX8Q1ueQG6WJ5C+L4jpZYuJNGcOTPtbXJRwfsyna7HtVko4yObXTy0FxyUKuuuKp5PlC0cHoP/O//wCff2SK3DCy6I7Lxk+VtUzbguFGrt9kf6ngP/Y4YY26wqyoEQLuPBOW0NBClOewAqrYDuAGGgLLmXOLjcqHtfkq0K0ccDPFL6OrStGxQsxCi53SOauffiiaQtIstvCqFk8bnPHGwQ6l2ozOL1K6b85D/wCcNisTlOvwWM7KxNk80zippRKzwSgswAkh4hbC/YZRe+9hiN2dt1hVkHw0uRpW+rp2P12UUsneyHcP+Qn549MbTuFBlZUM7Lz6oVUZXlhNnoKyA/ajcMo+Lk/y4gYGck2zGKtvzX8QEGbLckdikeamJxoVnTge4ndQfPEDTjgU0zH5PmYD4XHutkewfW/7LmFHP5SWP8u/riBp3cCm2Y9Ee00j0PstFX0eZkn7gOO9JFP6kH5YiYXjgmmYvSO+a3iPa6DVWQVcfr00y+PVsR8QLYgWOG4TTKynf2Xj190NLAGx492Ipka7L7gQswIWYELMCF8Kg8hjyyLqXSZlPFbq5pUA4BJGUfAG2JAkbFUvgif2mg+QRml26zFOFUx8HVG/Vb/PExK8cUq/C6R3yehKnzdJFXJE8M8cMiOpVuyytZhbiGsPhiXTOIsVQMGhY4PjcQQb8D+EmjFK11gJ4gkHkRxHiMC8IB0KsvP9pZ1pabNKbc3mAhqQVJCsL2awIA7W8NeIdMO5yWZguPFHGyrMMl7cPwlio2/zJ/8AeSo7kRB/w3+eFjM88VvswqkHy38SUKqc8q5PXqZm85Wt8L2xEvcdymWUkDOyweiHFRe9te/EUwNNl9wIX22BFkGkHHF4WM4cE25RRtLlM0fFqdhMo7ke7X+Uvxxe7VixIXZKuw4myUJTYXxQNVsvOUXVmbP065RQiqYA19Yn0QI+ojNmub89VJH2t0eyTi17ujbYbrMpKc1093dkb+3mk5mJJJJJJuSTcknUknmThNdaAALBa5jpj0bqMhsFHo80mhuIpCoJuQLanhzGLwSNliSwxym7xdTaTNqmpngpnk3lmmjjI3V4O4U6he4nFjHEnVZ1ZTxRsu0Jp6UKrfzGUDhGqRj3LvH5scUTG7ytzB48tKDzJP4/CVMVLTV77J1UFFldM08qQoYw5Z2CgmTt8+J7XAYfiFmBcPiL89U899vTRJ20/TnSx3Sjiaob7b9iPzA9dvKy+eLEkguzsm0eYVUFSymKnWRX3WAiiZbi67vryBlvYne48eGBCSOlfZz0DMpFVQIpCJohbSzHVLcLBgwt3W78CEzZ1t/lQgRKagDSFF3lICxRsRqoA7T2OnK/fgQnDo6yCapoxLKk9HKHYBVMkQZdGVlW993W2vHdwITO+SZhGPo66QAfbCP8S6k/PAhIu1nSY1KerdqOta9igjvu9+84YoD90Anvtjy11Jr3N2NlKyWshrYBPJkSoj33Wjk3WYfaACLoTfW/LEDEw8E0zEKpuzz9/uh1VLkBYq/pdMw0NiJAP4TJ/rioxR81ox4jXgXLbjvH+F4GR5TJrDm8a+E67v8AmKfpjwwA7FXjGpW/8kfpcfe63Do5qHG9T1FLOvIpKbn+Uj54iad3BMMx2A9oEehQ+q2EzFONMzeKMrfo1/liBieOCaZilI757eIIQeqyyeP6yCVPF42UfMYgWkbhNMqIn9l4PmFDDDvxFXL7j1CzAhZgQnXo6kSdanLZT2KmMlD9l1HEeNgG/wDDGL4DqW81hY1CQ1s7d2m3skdY2Qsj6OjMjeBUlT8wcVPFjZadHL0sQcvcY3jZdT3DU/LERqmXEN3NkSptnqyT1KWc+PVMB8SAPniQY48Eu+sp2dp49UWpej3Mn/3fc/HIg/Qk4mIXnglX4vSN+a/gCpw6Ma3nJTA9xla//wDPHvw7+5U/65Tcneg91WtQh3rDieHvx41TmGUkq19k4khr0gb6uogaAjkSq7y/JXH58O20suSLznzd91W+dZYY2lhPrRsyH8pK3+WEAcrrLtJGNngDhxF1YVRTftilp5qZ09JgiEU9OzAMLcGW/IneIJ0IPG6kYYlZnsWrEw+rbROdFKNDxQObYfMV40j+5kP6McL9E/ktoYnSH5/ofZCMyyKrjB36WdfExPb42tgDHA6hEtZA5vVePUIBJA6+sjDzUj9RiyySD2nYpj6KqTrc3puYj35D+VCB/MVxbEs3EXaALM+qutqp5PtyyEeW8bfK2FXm7iV09KzJCxvID7KCsZYhRxYgDzOgxFXOcGguPDVWZtb0QmtrlmNR1dOIo1Ki7PdBuWUHsopUKb6672nfpjRfPXOzEkpC6Zej6HLhTy0obqnBjfea53xdgxPey30AA7GBRVp9CW0PpeWojG8lOeqbvKjWM/w2XzQ4EId/9QGz3X0C1Kjt0zXNuaPZWHuO43gA3fgQkToR2ny+jFR6YIkdbPFMU3pCPVaNSAW+yQB3t3YEI3tJ04ySN1OW05udFkkG85P3Y1v7rk/hwIXnpB2brcw9Gqpp46SJqaLrkqJGTdlBYuFjsSTwIFgceFwG6sjifKcrASe5LdBleWUhDBGr5R7Uo6unB7xHq8nkxAPdih1QPlWzT4HI7WU27hqfZTM62nq6rsyync4CNOzGB3bo4j8V8Lukc7crcp6Cng7LdeZ1P94IKUHdiCbLGleGp1PEY9DiqzAw8EQyXYqoqiDBAxH956qDx3z/AEucWsLzss2rbRRf8hF+W5TK8EeXD6fOZ2mH+7U0rvY8d1rmw/NuYYF29orAkyTOtBH5rMg23zSoqVhhn3Va4QTojliNQCVVSCQDpvHuwCUE2RJh00cZkPBEqvaWuCqKnLqOokMrxMgXcIZb83LAg2JB5ixxMgEahLxyPa7qvI9fwpD0tMy71Tkjwn/AmB/RkHxxDomHgmhiVVHtJfx1+6F1FFkZ0M9TTt9lgHt57oc/PETTt4JhmOVA7QB8vZav+ilC5AhzemufVSWyMfDV7/y4gaY8Cm2Y+PmZ6FTsuho8oZqmeqinqFUiGnhNzdha55gEXFyAACeJtiTIshuSl6zEHVreiiabcUEy7bWOMEjLIHlYlmllYFmZjcm3V3FyTpvY8dKy97KUWHVZYG57Dlqnemz6uZKN09HijqQ19yI3Qru2W5Yjm3L2cXsNxdZFTG6KVzHG9kzrktSw+krJT+HdT5oqnElQvv8A0UiP1jySfjkZv1JwIXtdkKMC3Up/CMCFz7lVLv1UI++GPknb/phGLUgLr8SOSJzu5OeeyGLq6hb3hkSTTidxgxX3gEeRw8uQULpVphHWtKvqTokqnkbjdNv4QfzYSmb1/FdbhVQDS2Pym35Vfux3t5SVYcGUkEe8a49aSFCdjJdwnvZhqiWAOKypDAlWtPJxGo9ruK4ZY64XPVcIiksFuzisr1Xd9NlI+9ut/mUnE0sghz6sjHaqFb8SJ/wgYEJh6M8xkaWvrJQt6akO6VUi+/duZP8AdW9+IuNgSrYWmSRreZASao0xmr6AjWxlJ1tfTJ/iq38H0h+S4nGLvASlfJkpnnu++i6KxorhUudIeQenZfPABdyu9H+NO0vxI3fJjgQqE6Ddo/RcxWJzaKpAibuD3vG3xuv5zgQulsxo0mikhkF0kRkYd4YFT8jgQudYujyhpGP7QrhIyk/2ekG8xsdN5zolxxUgceOK3StanqfDqifVrbDmdAicW1CUylMupYqRToZLdZOw8XYfLW19Dhd1Q47aLcp8EiZrIcx9AgNXVSSsXkdnc+07En4n9MUEk6lbMcbIxlYLDuWnApr3FGWYKoLMeCqCSfIDU4FFzg0XJ0TRRbB1G51tVJHRwji8zAH+G4+DEHFzYHHfRZVRjMEejOse7b1X1s9yik0pad6+YfvZuzED3gEa25dn82LA2Nnes901dVaDqju0/aD53tbmFZ2ZZzHH/dQ9hLdxsd5h4EkeGIunPBMU+DNGsmqCw0qrwGKC4lbEdOyMWAUmGRkZWQ7rKQykcQQbg+4gY8VrmhzS07HRWPta4nio6sXTrzG8gViLOn0VwRqbiS3ki40WOzNBXBVMPQyuj5GydaTZOkZVZow5te7an54kqFOXZulH7lfhgQtcuytI37pfhgQqm6VMlip6uPq1Cq0I4DmHa/yK4UqLhy6jAw10TuYP4SgMLrdCsrJJb5bQOP3VW6nybrT+pXDsHYC43GG2q3d9vsFa0Juo8sXLMXvAhZgQqD6PsuMk8r20jiP8TkAfIPhOnHWuuqxx4EIbzP2U3aDM6dEZJJBvW9Uat8Bw99sNFwC5yOmlk7IQ/aKX0rJqGpF7wM1O1+IAO6pPjZIz+fFUouA5aGGuLHuiPH8JCxUtROHRwtRM8tPAm8TZyx0SPiCWPK+mguTbhocXRLIxKxLeaJbRU1NE1qiuEjDikA4eBPa+e7iZkaEoyjmfs310QFtoaeL/AGelXeHCSU3b+p/mGIGXknGYbxe70TJs7Vv+xK6ocjfqJ1iFtOyu4LfOTHkh/wBte0EI+Oa0bDX0F0n4TXXp26IqTfr9+31cTt5FrIPkzYugHXWRjcmWmy8yPortw6uRUetrooVLyyLGo9pmAHzx4SBupsjc85Wi57lR1Z+xaepkqIKd6uVpGkUysVgjJO92VABYA8Lg+eKHVAGy2KfA5n6ynKPUqJnm2NbVXEkxVD+7j7K+Rsbt+YnC7pHO3W5T4bTwdltzzOqAAYgn1mBCJZPkNTVG0ELOObWsg82Nl9174k1jnbBLT1cMH/I63dx9Efm2doKL/rGtBk/7NT9p/Jja4B7yFHji4QAdorHlxmSQ5adnmfZQqjpCeJTHldHHSqdOtkAeZvE8gfMvifSMbslDRVdSc0p/vshUO005ffqqeGqbvmLk+QuxCjwAtg6cHgpf6PK3Vrkx5FtBBUyiFcjpSxBPZk3NBx4QnHrTG82sq5o6ymZnzm3ipNBU5VUcMtcNrdI6jt6Gxspdbi/PhiRhYeCobitWPn+gXysospQXkpcxg8bBwPEkM/zxEwsCZjxarOxB8h+kPjGz9970yrYD2OqN28LiIAfEeeIdFHzTP+p1pFhGL/3ejwrxX08sywmGmg6qGlQ8SesQsTbS+iaC9gOPHF7HA7bLGqonxuvIbuOpVo5WCIkvxsMTSqlYELMCFVfTdDrSv4Sr/kI/rhWpGxXR4A7/AJG+B+6rHCy6NP2y8t8om74quJh4BjGP6thunPVXKY6204dzH5VuZc940PgMMLEUnAhZgQuYY62VEdI5HRXtvhWI3rXsDbUjU6cNcZoJC7+WFj9XC9tkDkWxIxaFmvFjZPHRovpFHmVAbFigniHey6H5rD8Ti/tMIWM89DVNedkkTR21wuDdbksZaU+7KEpkNY8N99qkLMRowTdTTv3df52xc+4j0WTShrq8Z/LxtokSoOuKW7LXmN3LRIdDiYS7zZqsfMU6nIsthvrKzznxBLMPlKnwxKc9UBLYI3NO+Tu+5/SUsKrpU89GmfUtEtRLO9mbcVEUEu1t5joOA1XUkDTF0L2suSsbFqaapcxkY0FyTwUnPOlWd7rTRiFfttZn8wPVX+bHrqgnZV0+Bxt1lN+4aD3+yRa+ulmbfmkeRu92Jt4C/AeA0xSSTutmKFkQysAA7lHx4rV9VSSAASToANSfADngXhNhc7Joy3YWqdOtm3KWEamSc7th37t7j827i1sLjvosuoxeni0b1j3e/tde3zPJqPSKN8xmHtN2YAfeLEe5/PFoZGzfVZrqquqtGdUd3vuhOe7aZlVKU64U8XAR043BbuLX3jpyuB4Y8M/JWR4LcXedUVgNNTwQQ5vTheuUtHUQraWIaG7qAb8Rew4mxU2JxaQHN6yy43SQTHodbehWjPdj5YE6+FlqaUi6zRa2H3gCbeYuNNbcMLPic3XddHR4pFP1T1XcvZLYxUtROHRR/wBZJ/3cn6Yug7ayMa//AC+Y/KWc+CPIq2H0caL7z2yfO7fLHszzn0VWFUrHU3XG5upWWbUV1N9XUMyj2JfpF8hvdpR4KRjwTuCnNg0L+zorAqEjkmSGqyyCWcxJIzxEp61xutxPEHixw1ka7cLnG1MsRIjebJloclkkMfWIkMMWscEfqqftE2G82p1sOJ04kyAtoEu57nnM43KaVFtMeqK+4ELMCFXvTTDelhb7M4HuZH/qBheoHVC28CdadzebfyFT4wouqTrsNJehzRPswrKB4oHY/wCVcM0x3C53H2/8bvEK29nJd6njP3Rhpc4ieBCzAhcuBbkAC5OgA4nwA54zF9FJAFyph2Pqj25E6hOO9Kd3T8PEe+2GGRuIXP1ddAx3VN/BFNiZIKLMqZo5jJ1jdTIwWyWlG6oB/H1etzw5YYa3KsOoqDMb2sh21mV9TV1EPJZG3fwt2l/lIwk7quIXX0zhPTsdzH20UXZTaaTLZmO71lPKN2eLTtDhvLfTeFzodCCQeRDEbxsViV9GQc7d192oyhEInpzv0smqMPYv7J525C+vI6jXx7Mu2ylS1fTaP7Q+qWqu+6QOJ0GPG7q2pdljJVp9KAEctLSrwp6ZF8r6fogxGoPWAV2BMtC5/M/b/KSsULbX22BC+YEKdlWUVFS27BE8h57o7I82NlHvOPWtLtlTNUxQi8jrf3JMkuytJSANmdakZ49RD25T8iR57tvHF4g/7FY02NFxy07L959lGbb6ODs5ZRJAvBp5hvzEc7C5seerMPDEs7GbBLGkrKrWV3l/aIxmeRVMM8VZVyvVxD6xWO8NxuLIo7KkaNZQL2tzwysOxBS1tjs+tJUWjsYJR1kDDUFDra/PdvbyKnnhCVuVy7PDKhs8IPzDQ+/mtuwuRiqq1V/qYx1kxPDdX2Ty7RsPLe7sEbMzrKWJVXw8BI3Og9/JEdoqP9rLLmVM/WhCYzDbtRxoTusFtfUXcg/aNuFsXzNJF2rDwqeONxjlFr8fdKuQ51VUDl6SSwOrRNrFJ+Jb8fEEHxxUyYhaVXhTJNW6FNSSZbmeqsuX1ntRuR1Ep5lToL+Vjx7J44sMbX6t0SUVdU0ZyTDM36jz90b2ZyWLLpGnkqIpqjcKxQxNfjxdjxAsONgAL8TYYlHFkOYqnEMTFU0RsbYX4qsEJN2JuWJPxwq43N10lLF0UQbyRXZqg6+rghtcPIu8Pug7zfyg4GC7gF5WS9FA9/IfpOce3kMWa1jSxuyCTq1ZCCV6sBCN0203gx0PM6YbdKGmxXLU2GPqI87CL96sXJtqKOqt1M6M32D2X/haxxNsjXbFKz0c8H/I0jv4eqMYmllmBCzAhJ/SvDvZbIfsvG384U/InFM46hWng7rVbe+/2VGjCS7JOvRV2p6iHlLTOPgVH6McX056xWJjrbwNdyP3H6VldH9Rv0UR+6P0w4uVTJgQswIVd5TBMBu0FHFSqdDLIN+Yj9AfMsMRaxrdlfPVTTG8jiVG2g2QZgJKmV5nGvbNwPJfVX3AYkqEt53l4EJMY3WXVT3Eag+42wIX3pMAlalrVHZqqdW8ioBsfGzqLfdOE6gWdddTgUt4nR8jf1SLUQhhikGy1pYg8WRDZecwxurqXpy27Klr7oYX3wOfO4HELpqBh2M5mrkK6F0E1x4qcdj2izChAIenmniaOQG4Khg5W457oNjz+NvAzK5WS1IlgPPipnSLUl8xqT9llUeAVFH63PvwtKbvK6LCmZaRluNz9Ut4rT6Zsp2HrJl33UU8QFzJOd0Ad9vW+Nh44tbC4rNqMWp4dAcx7vfZFY6LLKXUJJmMo7huU4PnwYfx4vbA0b6rDqMZnk0Z1R3b+vtZD9odrcxHV3K01IrLvxU67p3LjeG9617X9XdHhi9ZJcXG5Q/bnZtKSsdUHYkVZFPfvaNrz7QY+8YSmuHLrMHbG+nBtqDYoIBilbAFlemwlQtXlkavqUBib8mg+K7p9+HoXXYFxWKQ9FUuA2Oo80j5vtDSU7Nl9TC1XTxMSrRm0kDX1jU7w3gPBhb1deCxe9h6pV1LTVcTRNFpf6hRaTbDLn//AB9PFLRwVN1mqJSN83Fgl95rA3I3ibC9rC5YSYGAWaqqp1S54fNrb0QqajrMlrAyGxHA/u6hL8CP6cVPuJoDnRusVrGCCuhzR6EeoPsj2Y5DFmUfpmWgByf7RSkgMjcyL2GvwPEa3GPXxZusxQo8RdTnoanhsf7gvORbKCj3q3M1WKOMHcjZlLyORYAAEjhew79dAL49ijLTdyhiVdFM3oodSeKMvlyUeV1FQVUSzA2K985tdfAbxI8Bi6V1mErMw+HpKpjTzufLVVbhBdwnDozCxzT1b+pS08kh8yNP5RJi+nHWusXG5LQBg+Y/b+CRqEsRvObsxLMe8nUn44rkN3Jygi6OEBSSMQTyYcn21rqawScuo9iXtr8T2h7iMWNlc3is+fDKabdtjzGn6+iecn6WYmsKmFoz9uPtL5kaMPdvYubUDiFjz4FI3WJ1+46H2TzlWeU1SLwTJJ3gHtDzU6j3jF7Xh2xWPNTywm0jSFC26h38vqh/hMf4Rvf0xGXsFW0DstTGe8LnkYQXdJs6Lp93MoR9tZF/kLf8OLYTZ6zMYbmpHdxB/H5VmdHp3YpI/wC7kdP4WK/0w8uNTZgQswIXxVA4YEIbn0QMZ8sCFXFYgKsPPAhDqxOsyMb3GnqmVD3hidP5/wCUYoqB1LrYwR5FTl5g+6RsJrrUz9H0AkmmiYdl4Sx80ZQPk7Yvpz1rLCxyMdE13EH7/wCFIikeKX0VW7AJmhPOF4z1l171JFyvC+vM3cXLp1j2Wpc2ijrZA8Msg7fVMLNu9m53lPd/82xU+FrjdaNLik9OzI2xHfwSvtdn8WTzCmoqSLrSt/SJSXYe7Q3/ADW8MGVrNgvRNNWOtI/Tlw9FA2IzKWvrkjrnao3wxXe0WMqCbqoG6LjThfhr3xZIS6yYq8Ojig6Rp1BVp5zlUSRWVQLeGL1iqvs6pleJge44EKJtT9LlWW1DfWKHgJ+0EuoJ8fo7/mOFqkaAroMBec72cLX+v7SZhVdKj2Q7UT0lPURRG3Wslm5x6MGI8SAovytfFjXlrSAkamhjnmY9/C+nPa33QC2K09ZaJKISMqcN5lW9uG8QL+PHE2E3SNZE0sJPJWfsgorBNlNTeRII1eGcn6SPXcC8NbfppwtZ1zA4WK5CCqfBJnZ6cLclXckTRStuOyOjMu+jFW0NuINxe3C+EQ4t2XZPp452BzxuL+q11BeRg0skkrDgZHZyPIsScemRx3XkNFDGbtCJvnkzUq0bNeJXDqDxWwI3Qfs3N7ciMeZzlyqbaSNs3TAa2t+/FDcRTSd9nIB+yKrvnqY4XI+x2CR7wzD34bpx1SuWxyQ9O0chf6leNrNhI6OBZ45mKmw3HUE6/eFv0xXLFl1BTmG4k6Zwic0eISXihbi+YELMCF6RiCGBII4EGxHkeIwLwgEWKYqTbitSN4nl66NkKFZRvGzAg2bRr2PMnFglcBZIPwunc4PaMpBvp7JbAxWtBHNiZCuYUpH96o/i7J/XE4+2EniIvSvHcrc2SXdqaxBw69z/ABnfPzJxoLhk2YELMCF//9k=";


            var datenow = DateTime.Now.ToString("yyyyMMddHHmmss");

            byte[] Photo = Convert.FromBase64String(samp.B64Image);
            //byte[] Photo = Convert.FromBase64String(SavePic.b64img);
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = samp.CN;
            SelfieRegistration2Controller.GetDB2(GDB);


            var path = HttpContext.Current.Server.MapPath("~/ProfilePhotos/" + samp.CN.ToLower() + "/" + "");
            var folder = Path.Combine(path, samp.NTID);
            Directory.CreateDirectory(folder);


            MemoryStream stream = new MemoryStream();

            try
            {
                File.WriteAllBytes(folder + "/" + samp.NTID + "_reimburse-" + datenow + ".png", Photo);
                Connection Connection = new Connection();
                Connection.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.NVarChar, Value = samp.NTID });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Image", mytype = SqlDbType.NVarChar, Value = "https://ws.durusthr.com/ILM_WS_Live/ProfilePhotos/" + samp.CN.ToLower() + "/" + samp.NTID + "/" + samp.NTID + "_reimburse-" + datenow + ".png" });
                String ID = Connection.ExecuteScalar("sp_UpdateReimburse_Mobile");

                return "success";
            }
            catch
            {
                return "error";
            }



        }

        [HttpPost]
        [Route("DHRGetDependentRelationv1")]

        public GetRelationResult DHRGetDependentRelationv1(DHRDEPRelationv1 DH)
        {
            GetRelationResult GetDependentResult = new GetRelationResult();
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = DH.CN;
            SelfieRegistration2Controller.GetDB2(GDB);

            try
            {
                Connection Connection = new Connection();
                List<DHRDEPRelationv1> ListRelation = new List<DHRDEPRelationv1>();
                DataSet ds = new DataSet();
                System.Data.DataTable dtLEAVES = new System.Data.DataTable();
                ds = Connection.GetDataset("sp_SearchDDRelationship");
                dtLEAVES = ds.Tables[0];
                if (dtLEAVES.Rows.Count > 0)
                {
                    foreach (DataRow row in dtLEAVES.Rows)
                    {
                        DHRDEPRelationv1 DP = new DHRDEPRelationv1
                        {
                            Relationship = row["SubSubject"].ToString()
                        };
                        ListRelation.Add(DP);
                    }
                    GetDependentResult.Relationship = ListRelation;
                    GetDependentResult.myreturn = "Success";
                }
                else
                {
                    GetDependentResult.myreturn = "No Data Available";
                }
            }
            catch (Exception ex)
            {
                GetDependentResult.myreturn = ex.Message;
            }
            return GetDependentResult;
        }

        [HttpPost]
        [Route("Save_OR_reimbursementv2")]
        public String Save_OR_reimbursementv2(OR_Reimbursement ProfileImages)

        {
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = ProfileImages.CN;
            SelfieRegistration2Controller.GetDB2(GDB);
            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = ProfileImages.EmpID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@BenefitType", mytype = SqlDbType.NVarChar, Value = ProfileImages.BenefitType });
            Connection.myparameters.Add(new myParameters { ParameterName = "@ORDate", mytype = SqlDbType.NVarChar, Value = ProfileImages.ORDate });
            Connection.myparameters.Add(new myParameters { ParameterName = "@ORTime", mytype = SqlDbType.NVarChar, Value = ProfileImages.ORTime });
            Connection.myparameters.Add(new myParameters { ParameterName = "@ORAmount", mytype = SqlDbType.NVarChar, Value = ProfileImages.ORAmount });
            Connection.myparameters.Add(new myParameters { ParameterName = "@VendorName", mytype = SqlDbType.NVarChar, Value = ProfileImages.VendorName });
            Connection.myparameters.Add(new myParameters { ParameterName = "@VendorTIN", mytype = SqlDbType.NVarChar, Value = ProfileImages.VendorTin });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Base", mytype = SqlDbType.NVarChar, Value = "" });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Description", mytype = SqlDbType.NVarChar, Value = ProfileImages.Description });
            Connection.myparameters.Add(new myParameters { ParameterName = "@BenefitID", mytype = SqlDbType.NVarChar, Value = ProfileImages.BenefitID });
            Connection.ExecuteNonQuery("sp_Insert_OR_Reimbursement_Mobile");
            return "success";

        }
        #endregion

        [HttpPost]
        [Route("GetEmpList")]
        public EmployeeList EmployeeList(Token token)
        {
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = token.CN;
            SelfieRegistration2Controller.GetDB2(GDB);

            EmployeeList EL = new EmployeeList();
            List<EmpList> EmpL = new List<EmpList>();
            try
            {
                Connection con = new Connection();
                System.Data.DataTable DT = con.GetDataTable("sp_EmpList");
                foreach (DataRow row in DT.Rows)
                {
                    EmpList E = new EmpList()
                    {
                        EmpID = row["EmpID"].ToString(),
                        EmpName = row["EmpName"].ToString()
                    };
                    EmpL.Add(E);
                }
                EL.EmpList = EmpL;
                EL._myreturn = "Success";
            }
            catch (Exception e)
            {
                EL.EmpList = EmpL;
                EL._myreturn = e.ToString();
            }

            return EL;
        }

        [HttpPost]
        [Route("DocumentIds")]
        public DocList DocumentId(EmpIdForIDs ID)
        {
            DocList DL = new DocList();
            List<DocumentId> DocutId = new List<DocumentId>();

            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = ID.CN;
            SelfieRegistration2Controller.GetDB2(GDB);

            Connection con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.NVarChar, Value = ID.empID });
            System.Data.DataTable DT = con.GetDataTable("sp_DocumentIds");
            foreach (DataRow row in DT.Rows)
            {
                DocumentId docID = new DocumentId()
                {
                    imgType = row["ImgType"].ToString(),
                    id = row["ID"].ToString(),
                    imgFilename = row["Filename"].ToString()
                };
                DocutId.Add(docID);
            }
            DL.DocumentId = DocutId;
            DL._myreturn = "Success";

            return DL;
        }
        [HttpPost]
        [Route("DocumentIds2")]
        public DocList2 DocumentId2(EmpIdForIDs2 ID)
        {
            DocList2 DL2 = new DocList2();
            List<DocumentId2> DocutId2 = new List<DocumentId2>();

            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = ID.CN;
            SelfieRegistration2Controller.GetDB2(GDB);

            Connection con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.NVarChar, Value = ID.empID });
            System.Data.DataTable DT = con.GetDataTable("sp_DocumentIds2");
            foreach (DataRow row in DT.Rows)
            {
                DocumentId2 docID2 = new DocumentId2()
                {
                    imgType = row["ImgType"].ToString(),
                    id = row["ID"].ToString(),
                    imgFilename = row["Filename"].ToString(),
                    source = row["Source"].ToString()
                };
                DocutId2.Add(docID2);
            }
            DL2.DocumentId2 = DocutId2;
            DL2._myreturn = "Success";

            return DL2;
        }
        [HttpPost]
        [Route("GetImage")]
        public string idImage(baseID ID)
        {
            try
            {
                GetDatabase GDB = new GetDatabase();
                GDB.ClientName = ID.CN;
                SelfieRegistration2Controller.GetDB2(GDB);

                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@imageID", mytype = SqlDbType.NVarChar, Value = ID.imageID });
                con.myparameters.Add(new myParameters { ParameterName = "@source", mytype = SqlDbType.NVarChar, Value = ID.source });
                string b64 = con.ExecuteScalar("sp_getBase64");
                return b64;
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }

        [HttpPost]
        [Route("UpdateFilename")]
        public string updateImgFilename(updateImgFilename UIF)
        {
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = UIF.CN;
            SelfieRegistration2Controller.GetDB2(GDB);

            try
            {
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@imgID", mytype = SqlDbType.NVarChar, Value = UIF.imgID });
                con.myparameters.Add(new myParameters { ParameterName = "@filename", mytype = SqlDbType.NVarChar, Value = UIF.filename });
                con.myparameters.Add(new myParameters { ParameterName = "@base64", mytype = SqlDbType.NVarChar, Value = UIF.base64 });
                con.ExecuteNonQuery("sp_UpdateImgFilename");
                return "Success";
            }
            catch (Exception e)
            {
                return "false";
            }
        }

        [HttpPost]
        [Route("UpdateFilenamev2")]
        public string updateImgFilenamev2(updateImgFilenamev2 UIF)
        {
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = UIF.CN;
            SelfieRegistration2Controller.GetDB2(GDB);

            try
            {
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@imgID", mytype = SqlDbType.NVarChar, Value = UIF.imgID });
                con.myparameters.Add(new myParameters { ParameterName = "@filename", mytype = SqlDbType.NVarChar, Value = UIF.filename });
                con.myparameters.Add(new myParameters { ParameterName = "@base64", mytype = SqlDbType.NVarChar, Value = UIF.base64 });
                con.myparameters.Add(new myParameters { ParameterName = "@userID", mytype = SqlDbType.NVarChar, Value = UIF.userid });
                con.ExecuteNonQuery("sp_UpdateImgFilename");
                return "Success";
            }
            catch (Exception e)
            {
                return "false";
            }
        }

        [HttpPost]
        [Route("convertBase64")]
        public string insertBase64(insertBase64 BS)
        {
            try
            {
                //string b64 = "/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxITEhUTExMWFhUXGBoaFxcYFx0fGhcgGh0eGB0YHiAfHSkgHR8lHx0YIjEhJSktLjAuGB8zODMtNygtLisBCgoKDg0OGxAQGy8mICUtLS0tLS0tLS0tLy0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLf/AABEIAL0BCwMBEQACEQEDEQH/xAAcAAACAgMBAQAAAAAAAAAAAAAFBgQHAAMIAgH/xABOEAACAQIDBAYGBwUFBAkFAAABAgMEEQAFIQYSMUEHEyJRYXEUMkKBkaEjM1JicoKxFUOSosEkU2Oy0TREwvAWNVRzg5PS4fEIFyWjs//EABsBAAIDAQEBAAAAAAAAAAAAAAAEAgMFBgEH/8QANxEAAQMCBAIIBwABBAMBAAAAAQACAwQRBRIhMUFREyIyYXGBkdEUQqGxweHw8RUjM1JDcoI0/9oADAMBAAIRAxEAPwCXX5qkdgTdibBRqSe4Aak+GBCj1tHVsm/MVpIe+X6w/kuN3n6xB8Djwmyk1pcbAJZkzWkgN4YzPL/ey8Ae8C36AeZxWZRwWhDhz3avNvuguaZ1NNrJISPsjRR7hx8zc4qLi5aLIY4RdoTf0f5JFTw/tetXsqf7HCeMr8pLfHdv3FuSnFmjG3KQIfWTCOPZBs3zOWpmeaVru59yjko7gOA/1wm5xcbldZBAyBgjZsP66hk48VytTY7Y/wBHpzUzL9NIvZU/ulP6M3PuGnfhyGPL1iuSxbEOmd0TD1R9T7KBmdGs0bxNwYEX7jyPuNj7sXEXFllRSGN4cEhbFbLisrjBOSkUCtJUWNjZCBug8rk8e4G3LFMbea06yckAM4pu/wClmWg9UMpg9GGgIVRLbk/q6Nz9a/jiHxGu2ibGAuyXL+t4aeqn5Zkw3mqMnqusHF6SZrSLy7JOvK3b0PHfxe14dssaopJac2kHnw9VGzvb19YJkaKUesjix89eI7iNDiaWQWPOGOoOBCm5evp1VBS+yzb03/dpq1/xaJ5uMCEN21zj0qslkHqA7kfduJoCPM3b82M+R2ZxK7jD6foKdrTudT5oCZbag2I1BHEeI7jiFk05zbWKO7XZxHmFJHNwroLI5A+vTlw531HjvD2gcOt67dd1yVQDRzEx9k/1kcyKgGSUvpE4D5jUL9HGdRTqeZ8e88z2RoCSPcIxpuvaWnfXy3d2RufwkyonZ2Z3Ys7ElmPEk8ScJXvuuuYxrGhrRYBa8Cknfo2qlk6/L5GstQpaM/ZkQXuPGwVv/DwxTusbLAxymzMEw4aHw/z90/8AR9nbzRNFMN2aFjHIO5kO6fMXHHDa5hNmBCp3pc2g62YUqHsQ6yeLkcPyg/Fj3YTnfc5QupwSkyMMztzt4ftV9ihbqdNhYUpop80nF0gBWFftyN2dPiFv95vs4vgb85WDjE5dlpmbnf8AHupWQQxVV5aWqaGvclpIpydyZjqSp1t+W9gPUGGGStcsWpw+aAZiLjmPzyTNl+2EtPIIK6Non5E+q/irDRh5HS+tsWJFO1HXJILqwOBClYELMCFmBCzAhUrQ5qKHLhXIqtVVMskauwv1SIzrYee5e3MtrcKBiuR5aNE7QUzZ5CH7AXVd5tm09S+/PI0jct46DyA0HuGF7k7rcbGxmjBYKCTgUrpn6P8AZVayRqioO5Q0+szHQSEa9UD8C1tbEDiwIuYABcrKqpnSOEUepKnbX7RNWzb1tyFOzDHyVe+w03jYX7tBywrI8vK6Ggom0sdvmO5/HgECxBPKxOi7ZDrmFZOv0Sn6JT7bA+ufuqeHefLViGO/WKwcXxDIDBGdeJ5dytLNlvGfLDa5hVxKNT54EKDslABmVUoH11FJfzVkW/vDD4YiRurmSatvwKr2PgPLGaF9AXwVTRkOjFXXVWUkMPIjUYk3fRLzluUh2oRFtqhUAJXxiZeTgWdfHS2vitj54bbKeK5qfD2uN49O5apsmYKZaOYTRjijECRfDkD5HdPni4OB2WVJE+M2cEX6Pcrq5Y6iq6xKSJgYjUTaboGjBF0ubmxJIF1HEgjHjrW1RCHF4yi55L7X7ByiF5aKrhrhGLukY3ZAPBd5t7S5tcE20udML9E09kre/wBTnjIEzCLpEkqri99MRDUw6ou3NdWLsNkSUEIzWuW7kf2OnPEk6iRh38x9kdrja1xIjbcrIa19bNkZtz7uaX81zKWplaaZt52OvcO5QOQHIYTcS43K62CBkLAxg0H9dRMeK5ZgQt9DVvDIkqGzxsGXzBvbyPD349BsbhVyxiVhY7Y6K36SVFroqyL6muiVj4SKADfxK7unej40Qbi64GWMxvLHbg2TFtdnq0dK8xtverGD7Tn1R7tSfBTiMj8rbq6jpjUTBg24+C55kcsSzElmJLE8SSbknzOM9d01oaABsFuy2heeWOGMXeRgo8L8SfAC5PgDj0C5sFCaVsUZkdsEw9I1cgeHLYD9BRgb5+3KRqT4qCfzO3dhiU5RlCwcNidPK6ok47f30SrhZdCm7KNuJAnUVsYq6c+y9jIvcQx4kcr6/eGLmTkb6rIq8Hil60fVP0/SYMsozbrsoqOuQavSytaVB3KW1PcN/jqd84ba8O2XNVFLLAbSC329UxZDtujsYpgYpV0ZHBVgfEHXEkunCGZWFwb4ELZgQswIXN4zFFRqOpDejmQvHIvGFmuTy4XLG+vrNfQ6Rc0OFlbDM6J2ZqEZtkUkIDgiSE6rKmq2PC9uHnw8cLuYWrcp6tkotseXsvGy+z82YVK08Wg4yyco05sfHkBzPhciTGX1VVZU5BlG6bNttoIVRMvouzSQaXH75wdWJ9oXub8ySe7EJX5jYbJvDaUU46WTtHbuHuUn+ljFORafxIRzZLLhUzfSXEKayEcW7kB7zzPIe7FjIsx7knW4mIWdXtHb3V1wbQxIoVVCqoAVQLAAaADww6uSc4uNzuvFdtIhQ4F4q5zHaBFY+eBCH7ObThMxWQjQxyIffY/8OBANknemqNO7TGf0ZXbtrWlt1Dnq74sDLJOWpDitRqBiWVUmdoR3YPIpcwqxHGzRxIN6eUabifZvw3m4AHuJ4KcWMZxWdV1IcMgCaNts/Sdkp6cBaSn7MSjgxAt1h7+4X5XPFjheWTMbDZbuF0Hw7M7h1j9By90L2azs0dTHUC9lNnA9pDow8e8eIGIxuyuuma6Fs0DmHy8UwZbsjSPVVGZzEDLkfrIo7aTOdSoHtIH4KNCSF4KRh12UdYrkI+mlIgbugm1G0EtbOZZNBwjS+ka93iTxJ5nwAARe8vNyuwo6RlNHkbvxPM/2yEYim089G+xvpT+kTr/Z0PZU/vWHL8APHvOnfi6KLMbnZYuK4j0I6KM9Y79w91o6VMk9HrOsUWjnG+O4MNHH6N+Y4JmZXX5qeDVPSwZDu37cEm4pWurB2DqjUUc1GNZYT6RTd+h7SDzJIv8A4xw1Tu0yrmMdprPEw46HxH6+yV9tttDWvGo+riXQd7H1m92i+49+PJusbKzCQ2GMu4n7IFFLfC5FlvMkDlY3R9QGCB64j6WQ9TSgjmfWfxAsfdG3fhmnZ8y57G6u5EDeGp8eARbNei6OSPrIXKTnV98kpIx1JbmpJ4kfDEpIQ7UJahxV1OMjxdv1Huq1znJqilfcnjZDyPFW8VYaH9e+2FHNLdCungqYp23jN/uoGPFettNUPG4eN2R14MpII94wAkahQexr25Xi4705U22EFUqxZpFvldEqohuyp4nd1tzO7oeanDLKjg5YFXgl+tAfI/g+6P0wrKRBNTSenUnJo9ZVA5Mo9a33dfujDIIIuFz8kb43ZXix7007PbXwVIFmF+6+PVBMYcd+BC5yliDCxGBCG5c9SlQKek7fWG3UnVWLG2g9k8SW4WBJ0GBCdc8ljyumOX0pBqJe1WTLpa4+rXuFjYDkvixOFpZLdVq3sLojKenl2G3f3+AVZ1B1xU3ZacxJctuW0LTSBF05kngoHFj/AM9wxMAk2CUmlbEwuKfsvrI4l6mnjkkCcerRnNzzbdB1OGgABZc7LI6R2ZylDNJP+zVH/kSf+nHqrWuozRt03gmHnE//AKcCEjZlUMXP0cg80b/TAhQIqgrPG5VgBvX7J5qw7u84ELbkOy1bmDv6JCWRWIMhIVBztvHibW0FzqMV5E6aqwAC27Q7FV9AFkqoPobgGRGDILm1iRqvvtj3IvPirhBqbLmqJo4aa7ySMFVOdzzv3AXJPIAnHuVQ+IJGqtbPWiyykGVUzBpGG9WTDi5Yep5EaW5KANSScLzyfKFsYPQ5z8RJ5e/lwSWThZdITYIvsZs16fI7yt1dHBrPITYNYX6sHvtqTyB7yMMxR/MVz2JVznO6KPcqZtjtH6U6xxL1dLCN2GMCwsBbfI5G2gHIeZxVLJnPctDDqAUzLu7R37u5LuK1pJl2G2Vaum1usCEdY/fz6tT9o/Ia8xeyOPOe5ZuI1wpmWHaO3ur5pqdI0VEUKigBVHAAaADDwFtAuNc4uJc43JS30j5J6VROFF5IvpE7yV9Zfetx52xXMzM1PYZU9BUAnY6H+7lQowiu2RPZrNzSVUU44I3bA5qdGHw18wMSY7K66VrKcTwuj48PHgtPSrlApMwZ1t1FSOujI4Xb1wPzdrycYbkbfULlqOfJdjuCDZBBJPIqxRyuCQGMcbPujmeyDyvivoiVojEo4gSNVclLnAWpiSenkp4YkEcAkRlUm1yQSLE2AFuPZvzOGQLCy517y9xc7cqx4ZQwBU3Bx6orVX0MUyGOVFdDxVhcf/PjjwgHQqccj43ZmGx7lWO0/RaReSia4/uXOv5HPHyb44WfB/1XQUmN/LOPMfkeyreqpnjcpIjI68VYEEe44WIsbFb7JGyNzMNwtWBTU/J85npX34JGQ8wPVbwZTof+bWx61xbqFRPTRTtyyC/3ThDndDXG9R/YqvlUx/Vufvg6fxcuDjDTJwe0ubq8Gkj60XWH1/aY46POEAVRDKo9WQSWDjkbEaeWvmeOGFikWVXVtSEUk4EJoyqlGT0xqZgDmNULRof93TvPjzPebLwBJqlkyDvWjh1CaqTXsjf2SRK5YlmJZmJLEm5JOpJ8ScI7rsw0NGUbBQpKVnYKgLMxsAOJJ5YsYb6JCqaGgvOybGyk01OI1I62QqGe2gLGwH4Vv79Tzw4xuULk6qoMz78OCZNrdopMvdaChtDHCq7zbqlnZhvEksCDe4JNrkk4omlcDYLYwvDYZYulk1udkCXpBzMf7zfzjj/9GKumfzWkcJpP+v1K9P0j5kB9cv8A5af6Y9Ez1B2EUgHZPql6q24rCxJdSfwDFwkcsx9DTg7H1QzMNsKuQbgYEtoAF1JOgHHFjXOO6RnhiYOqrE6QqlqUQZdAxSGKFS4UkGRmJuWI1PDe8S5J5Yoneb5QtbBKVhjMrhc3sLoNs3tdPS3Q/TQNo8MhupB42vfdPyPMYrZK5q0KzDYagbWdzH55plyvK4k62vyVQ8pjKtTSHtwX1PVg8jod29ju2U27OGw/MOquWlpHU8obMNOY4+Crc1JZm397f3jv79w29ftb19b3ve+EnNIOq7CnnjkYMmynbN5FLmFSKeLsoO1NLyjXv/EdQBzPgCRbFHfUrOxKvEYyN3TFthnsPVrl9EN2kh0JH75gblifaG9rf2jr3Y8lkvoNl7hdAYv96XtHbu/f2SlilbSLbM5BLWziGPQcXe2ka958eQHM+FyJMYXmwSlZVspo87vIc10Bk+VxU0KwxLZFHvJ5sTzJOpOH2tDRYLiZpnzPL3nUqbiSqWYELnvbnJfRKySMCyMesj/C5On5TvL7hjPkbldZdvh1T09OHHcaHyQDEE+n6jaKsyoGaITSZc29uE2LREHgRqLLfz6kYchfdtuS5HFqXo6nNsHa+fH381uybMqGrtBStUUM7D6J+sLoWtcKQzEe7Q9xvj0TBxtsoS4ZLAzpdHAbrzsLm1VNUy5ZXkzKRIjh9SjJ7Sta9jyP4SLYixzg/KVbVwwPpRPGLHT+8U79H0rGn3Wbe3SVDfasbX9/HDCxU04ELMCELz7Z6mq03Z4w1vVYaOv4WGo8uB54i5gdumKeqlp3XjNvt6Kp9qOjepp7vBeeLuA+kUeK+15rr4DCj4S3bVdJSYxFL1ZOqfp+vP1SRilbKzAhSocwnRQqTSqo4KsjADyANhguearMMbjctB8gmfZHLo44/wBrVo+iQ/2SI8Zn5SW7gQd3yL8ACdF7w0XK4ampn1EgjZ/gc0t5zmktTM88pu7H3KOSjuA/9+JOM9zi43K7engZBGGM2ChE48V6tvo+2J6uH0mdfppF7Ckaxqf0Zhx7hpzOHIY7alcni1f0ruiZ2R9T7BDNrcoJV01HcRofdi9YqHbSr6fRrXAf2intDWKBxt6soHdre45MRfsHC08fzBb2C1mR3QO2O3j+0ivKBhay6N0gaoNVVaaYsa1Iz1GmiESyYuAWNI8onsPRdfmVHHxBnQnyQ77fJTi1qQndonbpBq+szGpbkHCD8ihD8wcJym7yuswyPJSsHPX1/SXsVp5SKCulgkEsLsjrwZTr5HkR4HTHoJBuFCWJkrcjxcJjzKtizgpEINzMuAlj0R1GhaT7qjjfUabpN90tscJB1guUqoXUL7xO0PBTdoa6HL6f9mUTXb/e5/adiNVuPge4WXvxCWS3VanMMoTI74mbyH59kkYWXRqXleXS1EqwxLvO50HId7E8gOJOPQCTYKmedkLC950Cv/ZTZ6OigESasdZHtq7d/gOQHIYfYwMFguJq6p9TIXu8hyCM4mlVmBCzAhIPS9kvW0wqFHbgOvijWDfA7reQbFE7btvyWzgtT0c3RnZ334eyprCa61MnR9nApq1N76uX6KQHhZzoTy0a3uLYsidlcs7FKbpqc23Go/P0W/K9lHizoUa3CRSrMrd0SkSKfHlHf7WLXRnpBZZkVe34F2be2XzKPUNcokzbNYxfeYU9MQCd5rLHvgDiCwRtOQOLmi7iVlTOLIGRH/29k3bDVkKwrCLq6gXVgQw05g64sSSa8CFmBCzAheXcAEkgAC5J4DxwIAvsqW6Sc7oaiS1PEGlB7dQvZDW5AD6z8R918JzOaTp6rrMKpqmJt5DZvBv9t4eqSMULZWYF6ju2G0TVkwKjcgjG7DGNAqjS9uFzYeQsOWs5H5ys+gom0sdvmO5/HgEBxBPqwei/ZDrmFXMv0SH6JT+8YH1j91T8T5asQx36xWFi+IdGDDHud+4cvNW/JIANThtcukrbCthAJuLjAhVdBtOKWpMg+plXq51IuCp4MR90k+4sOeBegkG4QDaHLjBMU4oe1Ge9Tw15kcPnzwo5uU2XSU9T07Mx34oNUtpj1qJnWC9RTxOoHVgvwsBqT3gDv/1xeAFgyucHbp36HcqP7VSR03BDDLJ5aCPW2nBzp4eGJqq5duhVXUGSR5Dxd2c/mJb+uMwm5uvoEbMjGt5ABeIomZgqglmICgcSSbADxJwKTnBoJOwWl0dnEUaM0rNuKgHaLcN23Lxvw54mxmYpOrq2RMzXVgSbuS03URsr5hOoM8o/cqeCr5cu83Y20GL5H9GMrd1i0VK6ul6aXsjhz7vdIhOFV069wQs7KiKWZiAqjiSdABg32Xj3BjS52gCvXYLZJaGK7WaeQfSN9n/DXwHfzOvcA9FHkHeuMxGvNU/Tsjb3KasWrOWYELMCFmBC11MCujIwurAqw7wRYjARdetcWkEbhc3Z5ljU1RLA3GNiAftLxVvepBxmublNl31NOJ4myDj9+KgkY8V6sLP85lkypa+H69ENLUsPWCsQvWXGt9bju62/s4da+7L8VxlTSiKr6M9km48D/WQva6qaipcuoIW3XVfSJiLGzNcAaixG8Zf4BiLjkYArqeJtXVOcR1f6yPZNnklVRPNJb0ijeO0gAXfRyAVNtOG8bCwuFxKF5cNVVitEymkGTY8OSs6gl3o1bvGLllqRgQhO0W0VPRx78z2J9VBq7nuUf1Og5nEHvDRcpimpZah2Vg8+AVMbW7aVFaSpPVwX0iU8fFz7R8OHhzwnJKX+C6yiw2Km13dz9v66WsVrSWyngeRlRFLOxsqqLknuAwDXZQe9rGlzjYBPFL0WVTIrPLFGxFyhuSvgSNL+WLxTu5rGfjsIcQ1pI56JCxQttHNmMi9IMksl1poEaSZ720UFtwHvIB15C57r2RR5ys/Ea4U0enaO3umDLenekVFQ0UsYVQFWNkYKALWF93QYfAtouMc4uJJ3Kg5z0wQTi0fWRj766/ys2BRS+ufR1BIEu8bEkG40HE6jAhBcwkjYHddD5MDgQpGX5h6TTCl3WeoiP0G6CzOv2LDU2At5BTyOIPbmCZpagwuvwO61Vmy8kQDVjrCD7AIL++1wD4DePljwNDd1bLUPmNmhRv2lHECtPGF73bVj/wA+PwwF/JDKXi8p06KpHWlzasY3IhWJGPewckfEx/HHjjZhK9hjD6pjBzCXQMIrt0U2WqVjq4pG4Rln/hUkfzbuLIRd4WbisoZTOHPRPtdULQBq+dFOYVClYYyNYl+0/jwuePBR7RwzI8M8VztBSSVbg0k5Bv7DvVaVNQ8jtI7FnYksx4knnhI73XYsY1jQ1osAtWBSVy9Guxno6ipnX6dh2FP7pT/xkce4ad93IYsup3XJ4piPTnoo+yPr+uSfsXrGWYELMCFmBCzAhZgQqt6Zsl+qrFH+FJ8yjfHeHvXCtQ35l0WBVNi6E+I/Kq7Cy6ROPRrWoZZaKb6qrjKEdzAGxHcSCwv3hcXQOs63NYuNU+eESjdv2PskvaKWojrZFqwd9T1asQQrCPsgr4H1vz4tmaSs7CahkZsd1YWzsAGVxINWrKoE9+7FqPdvRj+LEoG2aqsZm6SosNgAPyrdo491FHcBi5ZKRNsOkmOG8VJaWUaF/wB2nl9s+WnjywvJOBoFtUODvls+XRv1Psqlrq2SZzJK7O7cWY6+XgPAaYVJJNyuoiiZE3KwWC0Y8ViN7M7LVFYSUASJfXmfRFtxt9o+A95GJsjL9kjWV8VMOtqeQ/tFZGz2Wxwgx5et2OklY4uW8IwRa3j6vDRr3w4yMM2XJ1dbLUuu86cBwRobF0zayKJHPrO/aZj3knU4sSao3KcteolWKPidSx4Io4sfAfMkDnjOa0uNgu9qqllPGXu/yrSp6iOngFNAv0YBBJ1Lk+szd5Pw5cMPtaGiwXEVE755DI/cpO2woKcUs8hghDCNrMI0BBOgIIGhuRiSpVXbK5ak0j763VUvxI1uAOHvwIRyHKkV2SlSSSZ0ZVjW7HXnYC/drwGBCP5T0TJAgmzepWnQ6iFGDSt4aAi/gob3Yi5wbqVdDTyTHLGLohXbZ09LGYMqpVp0OhlYXlfxNyf5ifIYoMxPZWvHhTY9ZTc8h7qusxqnkcvIxdjxLG5/58MAXr2tbo0WUORrDEgFS9waLq0ckgMGzjbws1VV3sePYI0//Tw8Tgm0ZZeYQ3pKvNyBP4SrNIFFzhMC66uSQMFynTYzKEpIRm1aOI/skHNydRIR7rjuHa47tmxaNtyuXmfJXziKPbn+Ut5vmktTM00zXdj7lHJVHID/AN+JOFXOLjcrpoIGQMDGDT+1UPHiuVm9F+xm9u1tQunGBDz7pSP8vx7sMwxfMVzmL4jvBGf/AGP491a2GlzizAhZgQswIWYELMCFmBCgZ5li1NPJA/CRSL9x4hvMGx92IubmFlbBMYZGyN4Fc4VNO0btG4s6MVYdxU2OM4i2i75j2vaHN2Oq8wysjK6mzKQynuKm4PuIGBevaHtLXbFWXtRnUYjp656dJ6SrUJUxEDsSKLAi+l7Bl1t9WtiL4eMnVDlxbaI9O6AmxGy05dlMUpilyuo31hDH0OY2dA1r7pJv7IHauPv4k17XbKmppZoHf7g8+B80F2023rJnalkjelUCzxHR38WPNT3Loe84omc7bgtrCKentn3d9ko2wsuiX1FJIABJJsABckngAOZ8MCiSALnZOdJstBRxipzV9wHWOlU3lltyIB8tARa4uRwxeyGwu9YVTirnu6Kl1P8A29vcoomaSV9MZYvooaVxvUSAWMfJzb1iAN6w7PZYWJAJZY9rtlh1dNLC4GTjrdWVkVRG8KtHaxA4YmlERwIVf5ZssKKktoZnsZWHfyQfdX5m5xXFHkHen8QrTUyX+UbD8+JQg4sSCWtti8sBpYUaSaZlVUUXJswY+6wNzyGBC17OdHQoUMuaVawBwPoIyGla2tri/kQob8QxFzw3dXwU0s5tG2/2U+fbWOnQw5ZTLTIeMrANK/K5vfXxYsfLCz6g/KugpcDY3WY3PIbeqTMxrXdi8js7nizEkn3n9MUC5NytY5IWZWCw7kIka+LQFnvdfVQmBJsASe4YtCzpXAG5RrI8pCxvWT6Rx36tftuNL+Njp5+RxaBbUrOkkMrsrU97ZSdVluVwnQmIzOO5nCtb4u/wxTUHYLVwJob0kh7h/eigbD7NxzK2YVvZooT2VI+vYG27b2lB0t7R7PfgjYGjM5RrqqSokEESjbVbQyVs5lfRRpGl9EXu8zxJ/oBhd7y83K3KKjbTR5BvxPM+yD4inE7dHGx3pb9fMv8AZ0OgP71h7P4Rz7+Hfa6KLMbnZY2K4h0A6OM9Y/Qe6uxRbDq5JfcCFmBCzAhZgQswIWYELMCFmBCpnpeyXqqlahR2Jx2vB1Fvmtj+VsJzts6/NdVglTniMR3bt4H2KQsULcTtsOBV0tVljmxkXrYCfZdbH/MEa3dv9+GIDcFhXP4zEWOZUt4aH8JHpZXRr9qORCQbEhkYaEXGoINxilwLStaB7Z4hfUFOe0lQ1ZlEVXNrPT1HU9YRYyIwGnjxX3oe84vvmjuViOiFLXhkexF7cv63ogezuz1RWPuQpcD1nOiJ5nv8Bc4oYwuNgtqprIqZt3nXlxKeYaRMvhkegiFXVIp36hh2EA9bqwPWt3KeRuxI3cNiMRi41K5mWtfWyhjzlbfb35qsquolmkM08jSytxdv0FtAPAADCrnly6Smoo4G2aEV2Wz16KoWZbleEiD20PEefMeIHjjxjy03UqylbUxFh34HkVaeVVK0k6CM3pKkb8DDgpOpj8LXuB3aeycaANxcLhnscxxa7cJ5Vr649UVVY6V0cWlpGH4JQ3yKr+uFhU9y6F2AO+V48x/lCqra+ja5CzJ+JFI/lY4kJ2lKyYNUN2sV62GzEMM0qYDvTxQL1V11UHrCxAI11VSR9wd+Jud1CWpOKny1DY5dASLpHnneRi7szseLMSSfMnCBN9V3DGNYMrRYLTI1sAQ51ghs8lzi0CyzJZMxTHl+yBKCSZwFIuFQgk/m4fC+L2x31Kx6jEA27WDXvWuky1ZZTDEAka6yuOS91z7RsQL8NTyti4ABZT3ukOpUPaOtFTJFTQ9mEMsUQHA7xCb1v08PM4qzZnLS6DoICTuVYmeZOMzzJgW6uho4kWaS9h2bv1anvIIufZGvMX9LbuuVVFUlkHRR9px/SA7ZbSCqZIoV6ukhG7DGBYaC2+Ry00A5DxJwtLJnPcujw2gFMy7u0d+7u90t4qWmmPYjZV66axusKWMr/wDAv3j8hr3XsjjznuWdiFc2lj07R2H5UvPNqM/o6qaOmpJPRI3KQJ6IWjCL2QQyKCd4C+rHjh4ADZcY5xcS5xuSotL081iErUUcTMpIYKzxkEHUENvWPLHqimDLunylawlpJ0Y8oysn6lCfhgQnPNekfLKaoemnqOrlS28DG5AuoYahSOBGBCIUO2OXTECOtp2J4L1qhvgTfAhGo5AwuCCO8G+BC9YELMCFmBCzAhANuMk9Lo5IgLuBvx/jXUD3i6+THFcjczbJygqfh52v4bHwK57wgu6UzJsxanninTjGwa3eODL71JHvx605SCqaiETROjPEf4TjtLstDJX+k9ekNHUxioMhtx0392+lzdWuebnQ8MNvjDzfguYpcQdSsMZF3A2AXvNjTVNLAYbrllNNZwAeskc9lXbmAS9rWuetB09mQa0tsNkq+eeKbpZO0RxTll2XPPGqKgpqQDswpoXH+IRxvzUaam5bjiwAAWCTkkdI7M43KaKSjSNQqKABj1QVI9I2zHodRvxj6CUkpbgh4tH/AFHhp7OEZmZT3FdjhVb08WV3ab9e9KWKlqJ42CzJZo2y2dt1XO9TPzikHasPM6259oe1bDEEluqVz+NUWYdOzh2vdMse23o46iosk0fZcE8xzHeCLEHmCMNrmlUWMxfRlhGBeELdkuaT0NQKmnI3rbro3qyKeKt8Ab8iMXRy5d1j4hhwm6zd02RwZfmlzSkUdZxamkP0ch4nqz8fVHLVRxxN0TX6tSUGJT0pyTi458f2kfOqWWGQxSoUYcjwPiCNCPEYrDC3daLqpswuw3CG4kqVNyquqd9YICWMhsEPq95b7thckjkNcWR3vos6vEeW5GqM59UrTRehxNdjrPJzYn2f005AAd+JSP4BVUFN/wCV3kh+xeRT1tdFHCLCNhJJIR2YgpuGPebjReZ7gCQRt4or5geoE6babQx7goKPSmjPbe+s7g3JJ5je1vzOvADFM0l+qNlq4Vh3RATSDrHYcv39knYoW4imzmRS1k4hi05u5Gka82Pf4DmfeRJjC42CVq6tlNHnd5DmV0DkuVRUsKwxLZVHvY82J5k4fa0NFguInnfM8vedSppOJKpcj7OJ6ZnMRsCJqwSEEaFTJ1jaH7t8CF01V7E5bIwdqKDfDBgyxhWuDcElbE6jngQuaekFzU5zVKDq1T1QJ+6RF8NMCFZuYdAEDa09bIv/AHkavf3qU+NsCEZ2P2cOz9FXVE7rLwcblxvBAQqm47JLMRz4jAhVzSbd7QZnUMlG7AgFuqiCKqLcDVn8SOLc8CFOqNrNqMv7VSjtGOJkhV0Hm8Y097YEJz2q6WZMvNNHPSB5ZadJZlWTcETOSNwXVr2sefdrgQvND08Ze2kkNRH47qsPk1/lgQmGh6VsnlNhVqp/xEdPmV3fngQqy25o4kqTLA6SU9ReWJ0IZTqQ6gjTRw2nK4GEZm5XLs8JqempwDu3Q/j+7kvYqWmnWgh/aGUTUtt6ejImg7yupKj3dYv5kw1EczC1czicQgqmz20O/jx91P2foeqoEpiQZquWOUoP3UaFWBbxJS35jb1TiyFhaNUhiVU2eQZdgLK2aSPdRR3DFqzltwIQzaPJY6uneCTgwurc0Yeqw8j8Rcc8Re0OFlfTVDoJBI3h9lzvmFFJBK8Mgs6NusP6jvBFiD3EYzyCDYruopWysD2bFaUYgggkEEEEcQRqCPEY8UyARYq1ss6RaJokNVFee1pCIgQSNN69uYANuV7csNtnFtVy82By9Iejtl4apWpKnI51lYU1dEsWjsCGC3vr67X4HlgMLN1BuK1gcG6EnuH4WpaXIn0TMpoyeHWwNb3nq1HzxHoWcCmhi1U3tRj6rYmyVJJpDm9G55KxCn4b5Pyx58PyKsGOgdqM+v6QDabY2aKxEkEmtw0bk+R9UW+OJMie0paqxGnnbaxUalzicL1VfCZ4vt3BkTxve7eejcdTwxda41WQH9G7NG5R83ylFj6+nlEsPwdPBhp+gPhzxUYyNlpxYg1ws/Q/RM0GXfsii66UD0+qFkQjWCPjY+PAnx3V9knE3HILcUvFG6rkLj2R/WSXluXz1c608ALyyHiToObOx5AcSf1Jtitrbp6pqGxNsFZGeVcOW037Mo2u5/2ucaM7EaoO7ut7I04kkeTSW6rV7hdAZD8RLtwHPv8AZJGFl0qlZZl8lRKsMS7zubAfqSeQA1Jx60EmwVU0zIWF7zoFfuyWzkdDAI01c6yPbV2/oBwA5DxucPsYGCy4isq31Mmd23AcgjmJpVB9sK7qKGqmHFIJGHmFO787YELnToLo+sziFuUaSP8AyFB82B92BC6hkcKCx4AEn3YELj3JMubMsxWLe3GqZXJa192+85Nri9teeBCsiXoazWD/AGSvW3hJJEfgLj54EK5M6yJamhejkY2eLcL8TcAWfXiQwB144ELmWsy/M8hqw43omFwsqi8Uy8StyLMDYEqdRobA2OBCuDo86YIa1lp6pRBUMQqMD9FKTyF9UYnQKSQdBe5AwIVjV+VU84tNBFLpb6SNW07u0DgQuY+mqngizSSKnijiSNIwVjUKu8V3ybKLXsw+GBCsrLOhWgmpKd2aaOZoY2co4sWKgnRlPM8BbAhGNsNjEiymOKG7GjXeUkC7Lxkvbv8AX81GKZ23bfktXCKnoqgNOztPZVFhJdijexmc+iVkUpNkvuSfgbQn3GzflxON2VwKSxCn+Igcwb7jxHurP2ayeOmrqhDrch4r8o29VR3KpDIB3JjQXDJ4wIWYELMCFXvSxsx1sfpcS/SRD6QD2043811PkT3DC88dxmC2sHreif0L9jt3H9qnsKLq1mBepi6Iisr11C3+805ZSeTJdfj27/kw63rNIXG1J6Kdsg4JaaAcCtiNCO48xhK5XW9GxwvZRqmFLcBibSUtNFGBshUtIvEC3li8PIWRLSsdwWtnkUaSOPzn/XEw9JPpGhP3RfkKoj5vWk+jxaQoeM8inQ25hW0F/a103TeZcGi5ScUDppRHHxQTa/O5KmZ55T2m4DkgHBB4D9bnnhVpLnXK6SWNtNCGM4f109UKLk2XxlBeurUDvIbfQoQDur5XA8WuToAMXSPyNsN1mUFL8ZKXv7I+vd7pJJvqdSeJPE+OE11gAGgXzAvVMyzNJqdi8EjRsRYlbajjbUcMehxGypmgjmFpBcJhpekjMU4ypJ+ONf8Ah3cWCd4SD8GpXbAjwPvdGKTpbqB9ZTRP+BmT9d7ExUHiEq/AIz2XkeIv7LdnXSBR11NJS1MNRGkoAZomQkWIbQt4juxMVDeSUfgMw7LgfUIb0e02TZfVNURVk12jMe5NEQBdla+8q29kD34kJ2HilX4RVt+W/gQrFzfOoKmlnjpaqBpXidY7yqLMykC/Mce7Fge07FKPpZmdphHkqFoujvPaKZaingu8dykkbxONQVJCkknQkariSo2RtukTaSn+upmNuclKwHxTdGBCaqrpeemoaGeogR56nfZ442KhI1cqri+8bsN0gE62bhgQpH/3gyWqiaOpWQKws0csO8D/AA7wwIVDZ2kDVrjLxJ1RkAgBvv6kWA5+twvra19cCF2NShgih9W3RvedtfngQuTdvJTV5xUheL1JiU/hIiB8tBgQutY0CgAcAAB7sCFjqCCDqDoR34EA2XOm1WTmkqpYPZU3TxRtV+A081OM57crrLu6KoE8DX8ePj/aoTiKbVrbP5qZqKCpveWkbqZu8xmwDHv9g3Pc+HoXZmri8VpuhqDbY6hWRTShlDDmMWrNWzAhZgQvhGBCofpC2Z9CqLoPoJbtH3Kfaj93LwI7jhCVmQ9y7LDK34iKzu0N+/v90q4qWovGx2Z+jZjSz3solCvr7Mn0bE+QYn3YcjOq5TEGXZfkmDpAofR6+oW1lLdYvlIN/wDzFh7sUSNs8hbWHz56Vrjw09P0k+aS5x6BZVSPzFGaLZaSWFZY5EIYG4a4IsSCOBB1Hhi0Rki4WbJXMjeWOBW3ZfYmWrq+pkskMY36iQMOyn2QeTNYgX4AMeVsTYwjdK1NY14yx8UY212hWodYoBuUkA3IUAsDbTft4jQdw8ScLSyZz3Lew2h+Gju7tHfu7vdI+YrvXGJM0UKwZ9FZeX1UGb0dPHJMtPW069UOt0ScCwBU8ybA6ag72lrHF8jOkFwsajrDQyFrhcFaajo2zBRdUjlHfHKNf4wuFzA9bTMZpXbkjxHtdCKrZWuj9ekm9yb3+W+IGNw3CaZX0z9nj7fdCZkKGzgqe5gQfgcR23TTXNd2TfwXkHApLMCFmBCzAhfCMCFupqh49Y3ZD3oxX9CMAJGyg+Nj+0AfEXRel2wzCP1auX8xD/5wcTEjxxSr8OpX7sHlp9lJrNtqidQlVDS1Kj++gBI8ipFj4jExO9KPwSmO1x5/pCimVObyZZu95hqZB/K1wPccTFTzCVfgA+V/qE07I5lkNJIJEo5klHCR/pN3xXtndPiFBxMVDeKVfgVQOyQfO33Vh0u32XScKlVP31Zf8wA+eJCZh4pR+F1TPkPlr9lUGz3RjUenwVHplHUIs6Su0ct3IDhzdd21zbhfniwOB2KUfDIw2c0jyXQ2PVWswIVb9MmS78UdWo1iO4/irHQ+5v8AOcL1DbjMt3A6jLIYj823iP0qkwoupTZ0bZosVV1MmsNSvVODwub7nzJX8+LYXWdbmsnGKbpYMw3br5cfdWrspMyh6Zzd4WKkniw4q3vUqffh5cemHAhZgQtU9SiC7uqjvZgP1x5dSDXO2CU9sMzyypp3glq4QTqpDhijD1Wstz594JHPFUjmOFiVoUUVVFIJGMPpuFRxFtND4jgfEeGEl2Y1F0BqkupGGGmxWBO3MwhPvSBmHpUFDWDjJCEkI+0vEe5utHuxOUagpPDJSGui8/dI+K1opq2RrpHQ0kI3pnk+jX8Q1ueQG6WJ5C+L4jpZYuJNGcOTPtbXJRwfsyna7HtVko4yObXTy0FxyUKuuuKp5PlC0cHoP/O//wCff2SK3DCy6I7Lxk+VtUzbguFGrt9kf6ngP/Y4YY26wqyoEQLuPBOW0NBClOewAqrYDuAGGgLLmXOLjcqHtfkq0K0ccDPFL6OrStGxQsxCi53SOauffiiaQtIstvCqFk8bnPHGwQ6l2ozOL1K6b85D/wCcNisTlOvwWM7KxNk80zippRKzwSgswAkh4hbC/YZRe+9hiN2dt1hVkHw0uRpW+rp2P12UUsneyHcP+Qn549MbTuFBlZUM7Lz6oVUZXlhNnoKyA/ajcMo+Lk/y4gYGck2zGKtvzX8QEGbLckdikeamJxoVnTge4ndQfPEDTjgU0zH5PmYD4XHutkewfW/7LmFHP5SWP8u/riBp3cCm2Y9Ee00j0PstFX0eZkn7gOO9JFP6kH5YiYXjgmmYvSO+a3iPa6DVWQVcfr00y+PVsR8QLYgWOG4TTKynf2Xj190NLAGx492Ipka7L7gQswIWYELMCF8Kg8hjyyLqXSZlPFbq5pUA4BJGUfAG2JAkbFUvgif2mg+QRml26zFOFUx8HVG/Vb/PExK8cUq/C6R3yehKnzdJFXJE8M8cMiOpVuyytZhbiGsPhiXTOIsVQMGhY4PjcQQb8D+EmjFK11gJ4gkHkRxHiMC8IB0KsvP9pZ1pabNKbc3mAhqQVJCsL2awIA7W8NeIdMO5yWZguPFHGyrMMl7cPwlio2/zJ/8AeSo7kRB/w3+eFjM88VvswqkHy38SUKqc8q5PXqZm85Wt8L2xEvcdymWUkDOyweiHFRe9te/EUwNNl9wIX22BFkGkHHF4WM4cE25RRtLlM0fFqdhMo7ke7X+Uvxxe7VixIXZKuw4myUJTYXxQNVsvOUXVmbP065RQiqYA19Yn0QI+ojNmub89VJH2t0eyTi17ujbYbrMpKc1093dkb+3mk5mJJJJJJuSTcknUknmThNdaAALBa5jpj0bqMhsFHo80mhuIpCoJuQLanhzGLwSNliSwxym7xdTaTNqmpngpnk3lmmjjI3V4O4U6he4nFjHEnVZ1ZTxRsu0Jp6UKrfzGUDhGqRj3LvH5scUTG7ytzB48tKDzJP4/CVMVLTV77J1UFFldM08qQoYw5Z2CgmTt8+J7XAYfiFmBcPiL89U899vTRJ20/TnSx3Sjiaob7b9iPzA9dvKy+eLEkguzsm0eYVUFSymKnWRX3WAiiZbi67vryBlvYne48eGBCSOlfZz0DMpFVQIpCJohbSzHVLcLBgwt3W78CEzZ1t/lQgRKagDSFF3lICxRsRqoA7T2OnK/fgQnDo6yCapoxLKk9HKHYBVMkQZdGVlW993W2vHdwITO+SZhGPo66QAfbCP8S6k/PAhIu1nSY1KerdqOta9igjvu9+84YoD90Anvtjy11Jr3N2NlKyWshrYBPJkSoj33Wjk3WYfaACLoTfW/LEDEw8E0zEKpuzz9/uh1VLkBYq/pdMw0NiJAP4TJ/rioxR81ox4jXgXLbjvH+F4GR5TJrDm8a+E67v8AmKfpjwwA7FXjGpW/8kfpcfe63Do5qHG9T1FLOvIpKbn+Uj54iad3BMMx2A9oEehQ+q2EzFONMzeKMrfo1/liBieOCaZilI757eIIQeqyyeP6yCVPF42UfMYgWkbhNMqIn9l4PmFDDDvxFXL7j1CzAhZgQnXo6kSdanLZT2KmMlD9l1HEeNgG/wDDGL4DqW81hY1CQ1s7d2m3skdY2Qsj6OjMjeBUlT8wcVPFjZadHL0sQcvcY3jZdT3DU/LERqmXEN3NkSptnqyT1KWc+PVMB8SAPniQY48Eu+sp2dp49UWpej3Mn/3fc/HIg/Qk4mIXnglX4vSN+a/gCpw6Ma3nJTA9xla//wDPHvw7+5U/65Tcneg91WtQh3rDieHvx41TmGUkq19k4khr0gb6uogaAjkSq7y/JXH58O20suSLznzd91W+dZYY2lhPrRsyH8pK3+WEAcrrLtJGNngDhxF1YVRTftilp5qZ09JgiEU9OzAMLcGW/IneIJ0IPG6kYYlZnsWrEw+rbROdFKNDxQObYfMV40j+5kP6McL9E/ktoYnSH5/ofZCMyyKrjB36WdfExPb42tgDHA6hEtZA5vVePUIBJA6+sjDzUj9RiyySD2nYpj6KqTrc3puYj35D+VCB/MVxbEs3EXaALM+qutqp5PtyyEeW8bfK2FXm7iV09KzJCxvID7KCsZYhRxYgDzOgxFXOcGguPDVWZtb0QmtrlmNR1dOIo1Ki7PdBuWUHsopUKb6672nfpjRfPXOzEkpC6Zej6HLhTy0obqnBjfea53xdgxPey30AA7GBRVp9CW0PpeWojG8lOeqbvKjWM/w2XzQ4EId/9QGz3X0C1Kjt0zXNuaPZWHuO43gA3fgQkToR2ny+jFR6YIkdbPFMU3pCPVaNSAW+yQB3t3YEI3tJ04ySN1OW05udFkkG85P3Y1v7rk/hwIXnpB2brcw9Gqpp46SJqaLrkqJGTdlBYuFjsSTwIFgceFwG6sjifKcrASe5LdBleWUhDBGr5R7Uo6unB7xHq8nkxAPdih1QPlWzT4HI7WU27hqfZTM62nq6rsyync4CNOzGB3bo4j8V8Lukc7crcp6Cng7LdeZ1P94IKUHdiCbLGleGp1PEY9DiqzAw8EQyXYqoqiDBAxH956qDx3z/AEucWsLzss2rbRRf8hF+W5TK8EeXD6fOZ2mH+7U0rvY8d1rmw/NuYYF29orAkyTOtBH5rMg23zSoqVhhn3Va4QTojliNQCVVSCQDpvHuwCUE2RJh00cZkPBEqvaWuCqKnLqOokMrxMgXcIZb83LAg2JB5ixxMgEahLxyPa7qvI9fwpD0tMy71Tkjwn/AmB/RkHxxDomHgmhiVVHtJfx1+6F1FFkZ0M9TTt9lgHt57oc/PETTt4JhmOVA7QB8vZav+ilC5AhzemufVSWyMfDV7/y4gaY8Cm2Y+PmZ6FTsuho8oZqmeqinqFUiGnhNzdha55gEXFyAACeJtiTIshuSl6zEHVreiiabcUEy7bWOMEjLIHlYlmllYFmZjcm3V3FyTpvY8dKy97KUWHVZYG57Dlqnemz6uZKN09HijqQ19yI3Qru2W5Yjm3L2cXsNxdZFTG6KVzHG9kzrktSw+krJT+HdT5oqnElQvv8A0UiP1jySfjkZv1JwIXtdkKMC3Up/CMCFz7lVLv1UI++GPknb/phGLUgLr8SOSJzu5OeeyGLq6hb3hkSTTidxgxX3gEeRw8uQULpVphHWtKvqTokqnkbjdNv4QfzYSmb1/FdbhVQDS2Pym35Vfux3t5SVYcGUkEe8a49aSFCdjJdwnvZhqiWAOKypDAlWtPJxGo9ruK4ZY64XPVcIiksFuzisr1Xd9NlI+9ut/mUnE0sghz6sjHaqFb8SJ/wgYEJh6M8xkaWvrJQt6akO6VUi+/duZP8AdW9+IuNgSrYWmSRreZASao0xmr6AjWxlJ1tfTJ/iq38H0h+S4nGLvASlfJkpnnu++i6KxorhUudIeQenZfPABdyu9H+NO0vxI3fJjgQqE6Ddo/RcxWJzaKpAibuD3vG3xuv5zgQulsxo0mikhkF0kRkYd4YFT8jgQudYujyhpGP7QrhIyk/2ekG8xsdN5zolxxUgceOK3StanqfDqifVrbDmdAicW1CUylMupYqRToZLdZOw8XYfLW19Dhd1Q47aLcp8EiZrIcx9AgNXVSSsXkdnc+07En4n9MUEk6lbMcbIxlYLDuWnApr3FGWYKoLMeCqCSfIDU4FFzg0XJ0TRRbB1G51tVJHRwji8zAH+G4+DEHFzYHHfRZVRjMEejOse7b1X1s9yik0pad6+YfvZuzED3gEa25dn82LA2Nnes901dVaDqju0/aD53tbmFZ2ZZzHH/dQ9hLdxsd5h4EkeGIunPBMU+DNGsmqCw0qrwGKC4lbEdOyMWAUmGRkZWQ7rKQykcQQbg+4gY8VrmhzS07HRWPta4nio6sXTrzG8gViLOn0VwRqbiS3ki40WOzNBXBVMPQyuj5GydaTZOkZVZow5te7an54kqFOXZulH7lfhgQtcuytI37pfhgQqm6VMlip6uPq1Cq0I4DmHa/yK4UqLhy6jAw10TuYP4SgMLrdCsrJJb5bQOP3VW6nybrT+pXDsHYC43GG2q3d9vsFa0Juo8sXLMXvAhZgQqD6PsuMk8r20jiP8TkAfIPhOnHWuuqxx4EIbzP2U3aDM6dEZJJBvW9Uat8Bw99sNFwC5yOmlk7IQ/aKX0rJqGpF7wM1O1+IAO6pPjZIz+fFUouA5aGGuLHuiPH8JCxUtROHRwtRM8tPAm8TZyx0SPiCWPK+mguTbhocXRLIxKxLeaJbRU1NE1qiuEjDikA4eBPa+e7iZkaEoyjmfs310QFtoaeL/AGelXeHCSU3b+p/mGIGXknGYbxe70TJs7Vv+xK6ocjfqJ1iFtOyu4LfOTHkh/wBte0EI+Oa0bDX0F0n4TXXp26IqTfr9+31cTt5FrIPkzYugHXWRjcmWmy8yPortw6uRUetrooVLyyLGo9pmAHzx4SBupsjc85Wi57lR1Z+xaepkqIKd6uVpGkUysVgjJO92VABYA8Lg+eKHVAGy2KfA5n6ynKPUqJnm2NbVXEkxVD+7j7K+Rsbt+YnC7pHO3W5T4bTwdltzzOqAAYgn1mBCJZPkNTVG0ELOObWsg82Nl9174k1jnbBLT1cMH/I63dx9Efm2doKL/rGtBk/7NT9p/Jja4B7yFHji4QAdorHlxmSQ5adnmfZQqjpCeJTHldHHSqdOtkAeZvE8gfMvifSMbslDRVdSc0p/vshUO005ffqqeGqbvmLk+QuxCjwAtg6cHgpf6PK3Vrkx5FtBBUyiFcjpSxBPZk3NBx4QnHrTG82sq5o6ymZnzm3ipNBU5VUcMtcNrdI6jt6Gxspdbi/PhiRhYeCobitWPn+gXysospQXkpcxg8bBwPEkM/zxEwsCZjxarOxB8h+kPjGz9970yrYD2OqN28LiIAfEeeIdFHzTP+p1pFhGL/3ejwrxX08sywmGmg6qGlQ8SesQsTbS+iaC9gOPHF7HA7bLGqonxuvIbuOpVo5WCIkvxsMTSqlYELMCFVfTdDrSv4Sr/kI/rhWpGxXR4A7/AJG+B+6rHCy6NP2y8t8om74quJh4BjGP6thunPVXKY6204dzH5VuZc940PgMMLEUnAhZgQuYY62VEdI5HRXtvhWI3rXsDbUjU6cNcZoJC7+WFj9XC9tkDkWxIxaFmvFjZPHRovpFHmVAbFigniHey6H5rD8Ti/tMIWM89DVNedkkTR21wuDdbksZaU+7KEpkNY8N99qkLMRowTdTTv3df52xc+4j0WTShrq8Z/LxtokSoOuKW7LXmN3LRIdDiYS7zZqsfMU6nIsthvrKzznxBLMPlKnwxKc9UBLYI3NO+Tu+5/SUsKrpU89GmfUtEtRLO9mbcVEUEu1t5joOA1XUkDTF0L2suSsbFqaapcxkY0FyTwUnPOlWd7rTRiFfttZn8wPVX+bHrqgnZV0+Bxt1lN+4aD3+yRa+ulmbfmkeRu92Jt4C/AeA0xSSTutmKFkQysAA7lHx4rV9VSSAASToANSfADngXhNhc7Joy3YWqdOtm3KWEamSc7th37t7j827i1sLjvosuoxeni0b1j3e/tde3zPJqPSKN8xmHtN2YAfeLEe5/PFoZGzfVZrqquqtGdUd3vuhOe7aZlVKU64U8XAR043BbuLX3jpyuB4Y8M/JWR4LcXedUVgNNTwQQ5vTheuUtHUQraWIaG7qAb8Rew4mxU2JxaQHN6yy43SQTHodbehWjPdj5YE6+FlqaUi6zRa2H3gCbeYuNNbcMLPic3XddHR4pFP1T1XcvZLYxUtROHRR/wBZJ/3cn6Yug7ayMa//AC+Y/KWc+CPIq2H0caL7z2yfO7fLHszzn0VWFUrHU3XG5upWWbUV1N9XUMyj2JfpF8hvdpR4KRjwTuCnNg0L+zorAqEjkmSGqyyCWcxJIzxEp61xutxPEHixw1ka7cLnG1MsRIjebJloclkkMfWIkMMWscEfqqftE2G82p1sOJ04kyAtoEu57nnM43KaVFtMeqK+4ELMCFXvTTDelhb7M4HuZH/qBheoHVC28CdadzebfyFT4wouqTrsNJehzRPswrKB4oHY/wCVcM0x3C53H2/8bvEK29nJd6njP3Rhpc4ieBCzAhcuBbkAC5OgA4nwA54zF9FJAFyph2Pqj25E6hOO9Kd3T8PEe+2GGRuIXP1ddAx3VN/BFNiZIKLMqZo5jJ1jdTIwWyWlG6oB/H1etzw5YYa3KsOoqDMb2sh21mV9TV1EPJZG3fwt2l/lIwk7quIXX0zhPTsdzH20UXZTaaTLZmO71lPKN2eLTtDhvLfTeFzodCCQeRDEbxsViV9GQc7d192oyhEInpzv0smqMPYv7J525C+vI6jXx7Mu2ylS1fTaP7Q+qWqu+6QOJ0GPG7q2pdljJVp9KAEctLSrwp6ZF8r6fogxGoPWAV2BMtC5/M/b/KSsULbX22BC+YEKdlWUVFS27BE8h57o7I82NlHvOPWtLtlTNUxQi8jrf3JMkuytJSANmdakZ49RD25T8iR57tvHF4g/7FY02NFxy07L959lGbb6ODs5ZRJAvBp5hvzEc7C5seerMPDEs7GbBLGkrKrWV3l/aIxmeRVMM8VZVyvVxD6xWO8NxuLIo7KkaNZQL2tzwysOxBS1tjs+tJUWjsYJR1kDDUFDra/PdvbyKnnhCVuVy7PDKhs8IPzDQ+/mtuwuRiqq1V/qYx1kxPDdX2Ty7RsPLe7sEbMzrKWJVXw8BI3Og9/JEdoqP9rLLmVM/WhCYzDbtRxoTusFtfUXcg/aNuFsXzNJF2rDwqeONxjlFr8fdKuQ51VUDl6SSwOrRNrFJ+Jb8fEEHxxUyYhaVXhTJNW6FNSSZbmeqsuX1ntRuR1Ep5lToL+Vjx7J44sMbX6t0SUVdU0ZyTDM36jz90b2ZyWLLpGnkqIpqjcKxQxNfjxdjxAsONgAL8TYYlHFkOYqnEMTFU0RsbYX4qsEJN2JuWJPxwq43N10lLF0UQbyRXZqg6+rghtcPIu8Pug7zfyg4GC7gF5WS9FA9/IfpOce3kMWa1jSxuyCTq1ZCCV6sBCN0203gx0PM6YbdKGmxXLU2GPqI87CL96sXJtqKOqt1M6M32D2X/haxxNsjXbFKz0c8H/I0jv4eqMYmllmBCzAhJ/SvDvZbIfsvG384U/InFM46hWng7rVbe+/2VGjCS7JOvRV2p6iHlLTOPgVH6McX056xWJjrbwNdyP3H6VldH9Rv0UR+6P0w4uVTJgQswIVd5TBMBu0FHFSqdDLIN+Yj9AfMsMRaxrdlfPVTTG8jiVG2g2QZgJKmV5nGvbNwPJfVX3AYkqEt53l4EJMY3WXVT3Eag+42wIX3pMAlalrVHZqqdW8ioBsfGzqLfdOE6gWdddTgUt4nR8jf1SLUQhhikGy1pYg8WRDZecwxurqXpy27Klr7oYX3wOfO4HELpqBh2M5mrkK6F0E1x4qcdj2izChAIenmniaOQG4Khg5W457oNjz+NvAzK5WS1IlgPPipnSLUl8xqT9llUeAVFH63PvwtKbvK6LCmZaRluNz9Ut4rT6Zsp2HrJl33UU8QFzJOd0Ad9vW+Nh44tbC4rNqMWp4dAcx7vfZFY6LLKXUJJmMo7huU4PnwYfx4vbA0b6rDqMZnk0Z1R3b+vtZD9odrcxHV3K01IrLvxU67p3LjeG9617X9XdHhi9ZJcXG5Q/bnZtKSsdUHYkVZFPfvaNrz7QY+8YSmuHLrMHbG+nBtqDYoIBilbAFlemwlQtXlkavqUBib8mg+K7p9+HoXXYFxWKQ9FUuA2Oo80j5vtDSU7Nl9TC1XTxMSrRm0kDX1jU7w3gPBhb1deCxe9h6pV1LTVcTRNFpf6hRaTbDLn//AB9PFLRwVN1mqJSN83Fgl95rA3I3ibC9rC5YSYGAWaqqp1S54fNrb0QqajrMlrAyGxHA/u6hL8CP6cVPuJoDnRusVrGCCuhzR6EeoPsj2Y5DFmUfpmWgByf7RSkgMjcyL2GvwPEa3GPXxZusxQo8RdTnoanhsf7gvORbKCj3q3M1WKOMHcjZlLyORYAAEjhew79dAL49ijLTdyhiVdFM3oodSeKMvlyUeV1FQVUSzA2K985tdfAbxI8Bi6V1mErMw+HpKpjTzufLVVbhBdwnDozCxzT1b+pS08kh8yNP5RJi+nHWusXG5LQBg+Y/b+CRqEsRvObsxLMe8nUn44rkN3Jygi6OEBSSMQTyYcn21rqawScuo9iXtr8T2h7iMWNlc3is+fDKabdtjzGn6+iecn6WYmsKmFoz9uPtL5kaMPdvYubUDiFjz4FI3WJ1+46H2TzlWeU1SLwTJJ3gHtDzU6j3jF7Xh2xWPNTywm0jSFC26h38vqh/hMf4Rvf0xGXsFW0DstTGe8LnkYQXdJs6Lp93MoR9tZF/kLf8OLYTZ6zMYbmpHdxB/H5VmdHp3YpI/wC7kdP4WK/0w8uNTZgQswIXxVA4YEIbn0QMZ8sCFXFYgKsPPAhDqxOsyMb3GnqmVD3hidP5/wCUYoqB1LrYwR5FTl5g+6RsJrrUz9H0AkmmiYdl4Sx80ZQPk7Yvpz1rLCxyMdE13EH7/wCFIikeKX0VW7AJmhPOF4z1l171JFyvC+vM3cXLp1j2Wpc2ijrZA8Msg7fVMLNu9m53lPd/82xU+FrjdaNLik9OzI2xHfwSvtdn8WTzCmoqSLrSt/SJSXYe7Q3/ADW8MGVrNgvRNNWOtI/Tlw9FA2IzKWvrkjrnao3wxXe0WMqCbqoG6LjThfhr3xZIS6yYq8Ojig6Rp1BVp5zlUSRWVQLeGL1iqvs6pleJge44EKJtT9LlWW1DfWKHgJ+0EuoJ8fo7/mOFqkaAroMBec72cLX+v7SZhVdKj2Q7UT0lPURRG3Wslm5x6MGI8SAovytfFjXlrSAkamhjnmY9/C+nPa33QC2K09ZaJKISMqcN5lW9uG8QL+PHE2E3SNZE0sJPJWfsgorBNlNTeRII1eGcn6SPXcC8NbfppwtZ1zA4WK5CCqfBJnZ6cLclXckTRStuOyOjMu+jFW0NuINxe3C+EQ4t2XZPp452BzxuL+q11BeRg0skkrDgZHZyPIsScemRx3XkNFDGbtCJvnkzUq0bNeJXDqDxWwI3Qfs3N7ciMeZzlyqbaSNs3TAa2t+/FDcRTSd9nIB+yKrvnqY4XI+x2CR7wzD34bpx1SuWxyQ9O0chf6leNrNhI6OBZ45mKmw3HUE6/eFv0xXLFl1BTmG4k6Zwic0eISXihbi+YELMCF6RiCGBII4EGxHkeIwLwgEWKYqTbitSN4nl66NkKFZRvGzAg2bRr2PMnFglcBZIPwunc4PaMpBvp7JbAxWtBHNiZCuYUpH96o/i7J/XE4+2EniIvSvHcrc2SXdqaxBw69z/ABnfPzJxoLhk2YELMCF//9k=";
                byte[] Photo = Convert.FromBase64String(BS.base64);
                var path = HttpContext.Current.Server.MapPath("~/ProfilePhotos/" + BS.CN.ToLower() + "/" + "");
                var folder = Path.Combine(path, BS.empID);
                Directory.CreateDirectory(folder);
                MemoryStream stream = new MemoryStream();
                using (stream)
                {
                    File.WriteAllBytes(folder + "/" + BS.empID + "_" + BS.filename + ".png", Photo);
                }
                return "Success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }

        [HttpPost]
        [Route("UploadDocImg")]
        public string insertImg(insertImg ins)
        {
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = ins.CN;
            SelfieRegistration2Controller.GetDB2(GDB);

            try
            {
                Connection con = new Connection();
                string imagePath = HttpContext.Current.Server.MapPath("~/ProfilePhotos/" + ins.CN.ToLower() + "/" + ins.empID + "/" + ins.empID + "_" + ins.filename + ".png");
                //string imagePath = HttpContext.Current.Server.MapPath("~/ProfilePhotos/ilmdev/i021/i021_sampleImage.png");
                byte[] imagebyte = System.IO.File.ReadAllBytes(imagePath);
                string base64 = Convert.ToBase64String(imagebyte);

                con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = ins.empID });
                con.myparameters.Add(new myParameters { ParameterName = "@imgType", mytype = SqlDbType.VarChar, Value = ins.imgType });
                con.myparameters.Add(new myParameters { ParameterName = "@filename", mytype = SqlDbType.VarChar, Value = ins.filename });
                con.myparameters.Add(new myParameters { ParameterName = "@base64", mytype = SqlDbType.VarChar, Value = base64 });
                con.ExecuteNonQuery("sp_uploadNewDocumentImg");
                return "Success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }

        [HttpPost]
        [Route("UploadDocImgv2")]
        public string insertImgv2(insertImgv2 ins)
        {
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = ins.CN;
            SelfieRegistration2Controller.GetDB2(GDB);

            try
            {
                Connection con = new Connection();
                string imagePath = HttpContext.Current.Server.MapPath("~/ProfilePhotos/" + ins.CN.ToLower() + "/" + ins.empID + "/" + ins.empID + "_" + ins.filename + ".png");
                //string imagePath = HttpContext.Current.Server.MapPath("~/ProfilePhotos/ilmdev/i021/i021_sampleImage.png");
                byte[] imagebyte = System.IO.File.ReadAllBytes(imagePath);
                string base64 = Convert.ToBase64String(imagebyte);

                con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = ins.empID });
                con.myparameters.Add(new myParameters { ParameterName = "@imgType", mytype = SqlDbType.VarChar, Value = ins.imgType });
                con.myparameters.Add(new myParameters { ParameterName = "@filename", mytype = SqlDbType.VarChar, Value = ins.filename });
                con.myparameters.Add(new myParameters { ParameterName = "@base64", mytype = SqlDbType.VarChar, Value = base64 });
                con.myparameters.Add(new myParameters { ParameterName = "@userID", mytype = SqlDbType.VarChar, Value = ins.userid });
                con.ExecuteNonQuery("sp_uploadNewDocumentImg");
                return "Success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }

        [HttpPost]
        [Route("GetEmpAccess")]
        public string EmpAccess(GetEmpAccess selectedfunctionlist)
        {
            try
            {
                GetDatabase GDB = new GetDatabase();
                GDB.ClientName = selectedfunctionlist.CN;
                SelfieRegistration2Controller.GetDB2(GDB);
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = selectedfunctionlist._EMPID });
                System.Data.DataTable dt = con.GetDataTable("sp_GetEmpidandAccess");
                string access = "False";
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (dt.Rows[i]["parameter1"].ToString() == "E201 Application")
                    {
                        access = "True";
                    }
                }
                return access;
            }
            catch (Exception e)
            {
                return e.ToString();
            }

        }

        [HttpPost]
        [Route("DeleteDocImage")]
        public string deletedocs(DeleteImage delImg)
        {
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = delImg.CN;
            SelfieRegistration2Controller.GetDB2(GDB);

            Connection con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@imageID", mytype = SqlDbType.VarChar, Value = delImg.imageID });
            System.Data.DataTable dt = con.GetDataTable("sp_deleteDocs");

            string imagePath = HttpContext.Current.Server.MapPath("~/ProfilePhotos/" + delImg.CN.ToLower() + "/" + delImg.empID + "/" + delImg.empID + "_" + delImg.filename + ".png");
            File.Delete(imagePath);

            return "Success";
        }

        [HttpPost]
        [Route("DelAWSImage")]
        public string delImage(DelImage delImage)
        {
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = delImage.CN;
            SelfieRegistration2Controller.GetDB2(GDB);

            try
            {
                byte[] Photo = Convert.FromBase64String(delImage.base64);
                var path = HttpContext.Current.Server.MapPath("~/ProfilePhotos/" + delImage.CN.ToLower() + "/" + "");
                var folder = Path.Combine(path, delImage.empID);
                Directory.CreateDirectory(folder);
                MemoryStream stream = new MemoryStream();
                using (stream)
                {
                    string filePath = HttpContext.Current.Server.MapPath("~/ProfilePhotos/" + delImage.CN.ToLower() + "/" + delImage.empID + "/" + delImage.empID + "_" + delImage.filename + ".png");
                    File.Delete(filePath);

                    File.WriteAllBytes(folder + "/" + delImage.empID + "_" + delImage.newfilename + ".png", Photo);
                }
                return "Success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }

        [HttpPost]
        [Route("GetnewImg")]
        public newImgList newImg(getnewimg nimg)
        {
            newImgList IL = new newImgList();
            List<newImg> DocutId = new List<newImg>();

            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = nimg.CN;
            SelfieRegistration2Controller.GetDB2(GDB);

            Connection con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.NVarChar, Value = nimg.empID });
            System.Data.DataTable DT = con.GetDataTable("sp_getNewImage");
            foreach (DataRow row in DT.Rows)
            {
                newImg docID = new newImg()
                {
                    ImgType = row["ImgType"].ToString(),
                    ID = row["ID"].ToString(),
                    Filename = row["Filename"].ToString()
                };
                DocutId.Add(docID);
            }
            IL.newImg = DocutId;
            IL._myreturn = "Success";

            return IL;
        }

        [HttpPost]
        [Route("InsertNewImage")]
        public string insNewImg(InsertNewImg INS)
        {
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = INS.CN;
            SelfieRegistration2Controller.GetDB2(GDB);

            Connection con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = INS.empID });
            con.myparameters.Add(new myParameters { ParameterName = "@Filename", mytype = SqlDbType.VarChar, Value = INS.Filename });
            con.myparameters.Add(new myParameters { ParameterName = "@ImgType", mytype = SqlDbType.VarChar, Value = INS.ImgType });
            System.Data.DataTable dt = con.GetDataTable("sp_insertNewImage");

            return "Success";
        }

        [HttpPost]
        [Route("DelnewImg")]
        public string DelnewImg(delNewImg del)
        {
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = del.CN;
            SelfieRegistration2Controller.GetDB2(GDB);

            Connection con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@ID", mytype = SqlDbType.VarChar, Value = del.ID });
            System.Data.DataTable dt = con.GetDataTable("sp_DelnewImg");

            return "Success";
        }

        [HttpPost]
        [Route("DelAllImg")]
        public string DelAllImg(delallnewimg del)
        {
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = del.CN;
            SelfieRegistration2Controller.GetDB2(GDB);

            Connection con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = del.empID });
            System.Data.DataTable dt = con.GetDataTable("sp_delAllImg");

            return "Success";
        }

        [HttpPost]
        [Route("update_IDsAndLicenses")]
        public string updateId(updateidslicenses prms)
        {
            byte[] Photo = Convert.FromBase64String(prms.base64);
            var path = HttpContext.Current.Server.MapPath("~/ProfilePhotos/" + prms.CN + "/" + "");
            var folder = Path.Combine(path, prms.EmpID);
            Directory.CreateDirectory(folder);
            MemoryStream stream = new MemoryStream();

            try
            {
                using (stream)
                {
                    //File.WriteAllBytes(folder + "/" + NTID + "_pic" + Set + ".png", Photo);
                    File.WriteAllBytes(folder + "/" + prms.FileName + ".png", Photo);
                }

                return "success";
            }
            catch
            {
                return "error";
            }

            return "success";
        }


        #region 2018-11-15 new webservice for updating govid and licenses

        [HttpPost]
        [Route("AddUpdateDHRCompIDLicensesv2")]
        //Company Email
        public string AddUpdateDHRCompIDLicensesv2(GovIDLicenses GIL)
        {
            try
            {
                GetDatabase GDB = new GetDatabase();
                GDB.ClientName = GIL.CN;
                SelfieRegistration2Controller.GetDB2(GDB);
                Connection Con = new Connection();
                Con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = GIL.LoginID });
                Con.myparameters.Add(new myParameters { ParameterName = "@INPUTEDDATA", mytype = SqlDbType.NVarChar, Value = GIL.INPUTEDDATA });
                Con.myparameters.Add(new myParameters { ParameterName = "@EXPIRYDATE", mytype = SqlDbType.NVarChar, Value = GIL.EXPIRYDATE });
                Con.myparameters.Add(new myParameters { ParameterName = "@IDLICENSESTYPE", mytype = SqlDbType.NVarChar, Value = GIL.IDLICENSESTYPE });
                Con.myparameters.Add(new myParameters { ParameterName = "@BASE64IMAGE", mytype = SqlDbType.NVarChar, Value = GIL.BASED64IMAGE });
                Con.myparameters.Add(new myParameters { ParameterName = "@CN", mytype = SqlDbType.NVarChar, Value = GIL.CN });

                Con.ExecuteNonQuery("sp_AddUpdateDHRCompIDLicensesv2");

                if(GIL.BASED64IMAGE != "")
                {
                    byte[] PIC = Convert.FromBase64String(GIL.BASED64IMAGE);

                    var path = HttpContext.Current.Server.MapPath("~/ProfilePhotos/" + GIL.CN.ToLower() + "/" + "");
                    var folder = Path.Combine(path, GIL.LoginID);
                    Directory.CreateDirectory(folder);
                    MemoryStream stream = new MemoryStream();
                    using (stream)
                    {

                        File.WriteAllBytes(folder + "/" + GIL.LoginID + "_" + GIL.IDLICENSESTYPE + ".png", PIC);

                    }
                }

                return "success";
            }
            catch (Exception e)
            {
                return "failed" + e;
            }
        }

        #endregion

        [HttpPost]
        [Route("AddUpdateDHRCompIDLicensesv3")]
        //Company Email
        public string AddUpdateDHRCompIDLicensesv3(GovIDLicenses GIL)
        {
            try
            {
                GetDatabase GDB = new GetDatabase();
                GDB.ClientName = GIL.CN;
                SelfieRegistration2Controller.GetDB2(GDB);
                Connection Con = new Connection();
                Con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = GIL.LoginID });
                Con.myparameters.Add(new myParameters { ParameterName = "@INPUTEDDATA", mytype = SqlDbType.NVarChar, Value = GIL.INPUTEDDATA });
                Con.myparameters.Add(new myParameters { ParameterName = "@EXPIRYDATE", mytype = SqlDbType.NVarChar, Value = GIL.EXPIRYDATE });
                Con.myparameters.Add(new myParameters { ParameterName = "@IDLICENSESTYPE", mytype = SqlDbType.NVarChar, Value = GIL.IDLICENSESTYPE });
                Con.myparameters.Add(new myParameters { ParameterName = "@BASE64IMAGE", mytype = SqlDbType.NVarChar, Value = GIL.BASED64IMAGE });
                Con.myparameters.Add(new myParameters { ParameterName = "@CN", mytype = SqlDbType.NVarChar, Value = GIL.CN });
                Con.myparameters.Add(new myParameters { ParameterName = "@IDISSUED", mytype = SqlDbType.NVarChar, Value = GIL.IDISSUED });

                Con.ExecuteNonQuery("sp_AddUpdateDHRCompIDLicensesv3");

                if (GIL.BASED64IMAGE != "")
                {
                    byte[] PIC = Convert.FromBase64String(GIL.BASED64IMAGE);

                    var path = HttpContext.Current.Server.MapPath("~/ProfilePhotos/" + GIL.CN.ToLower() + "/" + "");
                    var folder = Path.Combine(path, GIL.LoginID);
                    Directory.CreateDirectory(folder);
                    MemoryStream stream = new MemoryStream();
                    using (stream)
                    {

                        File.WriteAllBytes(folder + "/" + GIL.LoginID + "_" + GIL.IDLICENSESTYPE + ".png", PIC);

                    }
                }

                return "success";
            }
            catch (Exception e)
            {
                return "failed" + e;
            }
        }

        #region 2018-11-20
        //REQUEST FORM MODULE START 
        [HttpPost]
        [Route("DHRGetRequestModalv1")]

        public GetRequestVarv1 DHRGetRequestModalv1(GetRequestv1 DH)
        {
            GetRequestVarv1 GetRequestResult = new GetRequestVarv1();
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = DH.CN;
            SelfieRegistration2Controller.GetDB2(GDB);

            try
            {
                Connection Connection = new Connection();
                List<GetRequestv1> List = new List<GetRequestv1>();
                DataSet ds = new DataSet();
                System.Data.DataTable dt = new System.Data.DataTable();
                Connection.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = DH.LOGINID });
                Connection.myparameters.Add(new myParameters { ParameterName = "@FORMID", mytype = SqlDbType.Int, Value = DH.FORMID });
                ds = Connection.GetDataset("sp_DHRGetRequestModalv1");
                dt = ds.Tables[0];
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        GetRequestv1 DP = new GetRequestv1
                        {
                            ISMODALVIEW = row["STATUSTYPE"].ToString(),
                            APPROVERID = row["APPROVERID"].ToString()
                        };
                        List.Add(DP);
                    }
                    GetRequestResult.GetRequestInfo = List;
                    GetRequestResult.myreturn = "Success";
                }
                else
                {
                    GetRequestResult.myreturn = "No Data Available";
                }
            }
            catch (Exception ex)
            {
                GetRequestResult.myreturn = ex.Message;
            }
            return GetRequestResult;
        }
        //REQUEST FORM MODULE END


        //TO DO REQUEST FORM START 
        [HttpPost]
        [Route("DHRGetTodoRequestFormmv1")]

        public GetTodoRequestVarv1 DHRGetTodoRequestFormmv1(GetTodoRequestv1 DH)
        {
            GetTodoRequestVarv1 GetRequestResult = new GetTodoRequestVarv1();
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = DH.CN;
            SelfieRegistration2Controller.GetDB2(GDB);

            try
            {
                Connection Connection = new Connection();
                List<GetTodoRequestv1> List = new List<GetTodoRequestv1>();
                DataSet ds = new DataSet();
                System.Data.DataTable dt = new System.Data.DataTable();
                Connection.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = DH.LOGINID });
                ds = Connection.GetDataset("sp_SearchSignPermission");
                dt = ds.Tables[0];
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        GetTodoRequestv1 DP = new GetTodoRequestv1
                        {
                            ID = row["id"].ToString(),
                            DATEREQUESTED = row["Date Requested"].ToString(),
                            FORMCODE = row["Form"].ToString(),
                            REQUESTEDNAMEBY = row["EmployeeName"].ToString(),
                            APPROVED = row["Approve"].ToString(),
                            FORMID = row["ID"].ToString(),
                            FORMTYPES = row["Types"].ToString(),
                            DATEAPPROVED = row["dateApproved"].ToString(),
                            REQUESTEMPID = row["EmpRequestor"].ToString()
                        };
                        List.Add(DP);
                    }
                    GetRequestResult.GetTodoRequestInfo = List;
                    GetRequestResult.myreturn = "Success";
                }
                else
                {
                    GetRequestResult.myreturn = "No Data Available";
                }
            }
            catch (Exception ex)
            {
                GetRequestResult.myreturn = ex.Message;
            }
            return GetRequestResult;
        }

        //TO DO REQUEST FORM END

        //TO DO REQUEST FORM START 
        [HttpPost]
        [Route("DHRGetTodoRequestFormAuditTrailv1")]

        public GetTodoRequestAuditVarv1 DHRGetTodoRequestFormAuditTrailv1(GetTodoRequestAuditv1 DH)
        {
            GetTodoRequestAuditVarv1 GetRequestResult = new GetTodoRequestAuditVarv1();
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = DH.CN;
            SelfieRegistration2Controller.GetDB2(GDB);
            try
            {
                Connection Connection = new Connection();
                List<GetTodoRequestAuditv1> List = new List<GetTodoRequestAuditv1>();
                DataSet ds = new DataSet();
                System.Data.DataTable dt = new System.Data.DataTable();
                Connection.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = DH.LOGINID });
                ds = Connection.GetDataset("sp_SearchAuditTrail");
                dt = ds.Tables[0];
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        GetTodoRequestAuditv1 DP = new GetTodoRequestAuditv1
                        {
                            DATEREQUESTED = row["Date Requested"].ToString(),
                            EMPNAME = row["EmpName"].ToString(),
                            FORMID = row["FormID"].ToString(),
                            APPROVERID = row["EmpID"].ToString(),
                            FORMCODE = row["Form"].ToString(),
                            REQUESTEDNAMEBY = row["Employee Name"].ToString(),
                            APPROVED = row["Approve"].ToString(),
                            FORMTYPES = row["Types"].ToString(),
                            DATEAPPROVED = row["dateApproved"].ToString()
                        };
                        List.Add(DP);
                    }
                    GetRequestResult.GetTodoRequestAuditInfo = List;
                    GetRequestResult.myreturn = "Success";
                }
                else
                {
                    GetRequestResult.myreturn = "No Data Available";
                }
            }
            catch (Exception ex)
            {
                GetRequestResult.myreturn = ex.Message;
            }
            return GetRequestResult;
        }

        //TO DO REQUEST FORM END 


        //TODO REQUEST APPROVED DENIED START
        [HttpPost]
        [Route("ApprovedDeniedDHRTodoRequestv1")]
        public string ApprovedDeniedDHRTodoRequestv1(GetTodoRequestApprovedDenied T)
        {
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = T.CN;
            SelfieRegistration2Controller.GetDB2(GDB);

            string Status1 = "", Status2 = "";
            if (T.ISCHECKED == "True")
            {
                Status1 = "True";
                Status2 = "Approved";
            }
            else
            {
                Status1 = "False";
                Status2 = "";
            }
            Connection Con = new Connection();
            Con.myparameters.Add(new myParameters { ParameterName = "@ID", mytype = SqlDbType.Int, Value = T.ID });
            Con.myparameters.Add(new myParameters { ParameterName = "@STATUS1", mytype = SqlDbType.NVarChar, Value = Status1 });
            Con.myparameters.Add(new myParameters { ParameterName = "@STATUS2", mytype = SqlDbType.NVarChar, Value = Status2 });
            Con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = T.LOGINID });
            Con.ExecuteNonQuery("sp_ApprovedDeniedDHRTodoRequestv1");

            return "success";
        }
        //TODO REQUEST APPROVED DENIED END


        //ADDING TO DO REQUEST START
        [HttpPost]
        [Route("AddTodoRequestFormv1")]
        public string AddTodoRequestFormv1(GetTodoRequestApprovedDenied T)
        {
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = T.CN;
            SelfieRegistration2Controller.GetDB2(GDB);
            Connection Con = new Connection();
            Con.myparameters.Add(new myParameters { ParameterName = "@FORMID", mytype = SqlDbType.Int, Value = T.FORMID });
            Con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = T.LOGINID });
            Con.ExecuteNonQuery("AddTodoRequestFormv1");
            return "success";
        }
        //ADDING TO DO REQUEST END


        #endregion


        [HttpPost]
        [Route("GetReimbursementStatus")]
        public reimbursementStatusList GetReimbursementStatus(getnewimg nimg)
        {
            reimbursementStatusList IL = new reimbursementStatusList();
            List<reimbursementStatus> reimbstat = new List<reimbursementStatus>();

            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = nimg.CN;
            SelfieRegistration2Controller.GetDB2(GDB);

            Connection con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.NVarChar, Value = nimg.empID });
            System.Data.DataTable DT = con.GetDataTable("sp_GetReimburseReviewer_Mobile");
            foreach (DataRow row in DT.Rows)
            {
                reimbursementStatus restat = new reimbursementStatus()
                {
                    ID = row["ID"].ToString(),
                    EmpID = row["EmpID"].ToString(),
                    EmpName = row["EmpName"].ToString(),
                    BenefitType = row["BenefitType"].ToString(),
                    VendorName = row["VendorName"].ToString(),
                    TransactionDate = row["TransactionDate"].ToString(),
                    TransactionTime = row["TransactionTime"].ToString(),
                    VendorTIN = row["VendorTIN"].ToString(),
                    Amount = row["Amount"].ToString(),
                    Status = row["Status"].ToString(),
                    ModifiedBy = row["ModifiedBy"].ToString(),
                    ModifiedDate = row["ModifiedDate"].ToString(),
                    Image = row["reImage"].ToString(),
                    Description = row["reDescription"].ToString()
                };
                reimbstat.Add(restat);
            }
            IL.reimStatus = reimbstat;
            IL._myreturn = "Success";

            return IL;
        }

        [HttpPost]
        [Route("GetReimbursementDetails")]
        public reimbursementDetailsresult GetReimbursementDetails(getnewimg nimg)
        {
            reimbursementDetailsresult IL = new reimbursementDetailsresult();
            List<reimbursementDetails> reimbdetails = new List<reimbursementDetails>();

            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = nimg.CN;
            SelfieRegistration2Controller.GetDB2(GDB);

            Connection con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@type", mytype = SqlDbType.NVarChar, Value = nimg.Type });
            System.Data.DataTable DT = con.GetDataTable("sp_Get_reimburstment_Receipt_Details");
            foreach (DataRow row in DT.Rows)
            {
                reimbursementDetails details = new reimbursementDetails()
                {
                    ID = row["ID"].ToString(),
                    Name = row["Name"].ToString(),
                    Required = row["Required"].ToString(),
                    Status = row["Status"].ToString(),
                    ModifiedBy = row["ModifiedBy"].ToString(),
                    ModifiedDate = row["ModifiedDate"].ToString(),

                };
                reimbdetails.Add(details);
            }
            IL.reimDetails = reimbdetails;
            IL._myreturn = "Success";

            return IL;
        }
        #region "new onboard"
        [HttpPost]
        [Route("InsertDependentDetails")]
        public string InsertDependentDetails(DHRDEPv1 DependentDetails)
        {
            try
            {
                GetDatabase GDB = new GetDatabase();
                GDB.ClientName = DependentDetails.CN;
                SelfieRegistration2Controller.GetDB2(GDB);
                var time = DateTime.Now;
                string formattedTime = time.ToString("yyyy-MM-dd-hh-mm-ss-fff");
                Connection Connection = new Connection();
                Connection.myparameters.Add(new myParameters { ParameterName = "@DEPENDENTID", mytype = SqlDbType.VarChar, Value = DependentDetails.dependentID });
                Connection.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.VarChar, Value = DependentDetails.EmpID });
                Connection.myparameters.Add(new myParameters { ParameterName = "@RELATION", mytype = SqlDbType.VarChar, Value = DependentDetails.Relationship });
                Connection.myparameters.Add(new myParameters { ParameterName = "@FIRST", mytype = SqlDbType.VarChar, Value = DependentDetails.First });
                Connection.myparameters.Add(new myParameters { ParameterName = "@MID", mytype = SqlDbType.VarChar, Value = DependentDetails.Mid });
                Connection.myparameters.Add(new myParameters { ParameterName = "@LAST", mytype = SqlDbType.VarChar, Value = DependentDetails.Lastname });
                Connection.myparameters.Add(new myParameters { ParameterName = "@BIRTH", mytype = SqlDbType.VarChar, Value = DependentDetails.Birth });
                Connection.myparameters.Add(new myParameters { ParameterName = "@COMMENT", mytype = SqlDbType.VarChar, Value = DependentDetails.Comments });
                Connection.myparameters.Add(new myParameters { ParameterName = "@FILE", mytype = SqlDbType.VarChar, Value = DependentDetails.DocFilename });
                Connection.myparameters.Add(new myParameters { ParameterName = "@EXTENSION", mytype = SqlDbType.VarChar, Value = DependentDetails.Extension });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Age", mytype = SqlDbType.VarChar, Value = DependentDetails.Age });
                Connection.myparameters.Add(new myParameters { ParameterName = "@BASED64IMAGE", mytype = SqlDbType.VarChar, Value = DependentDetails.CERTIFICATETYPE });
                Connection.myparameters.Add(new myParameters { ParameterName = "@CERTIFICATETYPE", mytype = SqlDbType.VarChar, Value = DependentDetails.CERTIFICATETYPE + "-" + formattedTime });
                Connection.myparameters.Add(new myParameters { ParameterName = "@COMPNAME", mytype = SqlDbType.VarChar, Value = DependentDetails.CN });
                Connection.ExecuteNonQuery("sp_InsertDependentDHR_Mobile");



                byte[] PIC = Convert.FromBase64String(DependentDetails.BASED64IMAGE);
                var path = HttpContext.Current.Server.MapPath("~/ProfilePhotos/" + DependentDetails.CN + "/" + "");
                var folder = Path.Combine(path, DependentDetails.EmpID);
                Directory.CreateDirectory(folder);
                MemoryStream stream = new MemoryStream();
                using (stream)
                {

                    File.WriteAllBytes(folder + "/" + DependentDetails.CERTIFICATETYPE + "-" + formattedTime + ".png", PIC);

                }

                return "success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }

        }
        [HttpPost]
        [Route("GetEmplevelList")]
        public List<EmpLevelList> GetEmplevelList(GetEmpAccess GetEmpAccess)
        {
            GetDatabase GDB = new GetDatabase()
            {
                ClientName = GetEmpAccess.CN
            };

            SelfieRegistration2Controller.GetDB2(GDB);
            List<EmpLevelList> EmpLevelList = new List<EmpLevelList>();
            try
            {

                //try
                //{
                Connection Connection = new Connection();

                System.Data.DataTable dtEmpLevel = new System.Data.DataTable();
                //BasicFunction BasicFunction = new BasicFunction();
                dtEmpLevel = Connection.GetDataTable("sp_get_EmpLevelVer2");
                if (dtEmpLevel.Rows.Count > 0)
                {
                    for (int i = 0; i < dtEmpLevel.Rows.Count; i++)
                    {
                        EmpLevelList List = new EmpLevelList();
                        List.Level = dtEmpLevel.Rows[i][0].ToString();
                        EmpLevelList.Add(List);
                    }
                }

                return EmpLevelList;


            }
            catch (Exception ex)
            {

            }
            return EmpLevelList;

        }

        [HttpPost]
        [Route("GetRelationList")]
        public List<string> GetRelationList(GetEmpAccess GetEmpAccess)
        {
            GetDatabase GDB = new GetDatabase()
            {
                ClientName = GetEmpAccess.CN
            };

            SelfieRegistration2Controller.GetDB2(GDB);
            List<string> EmpRelationList = new List<string>();
            try
            {

                //try
                //{
                Connection Connection = new Connection();

                System.Data.DataTable dtEmpLevel = new System.Data.DataTable();
                //BasicFunction BasicFunction = new BasicFunction();
                dtEmpLevel = Connection.GetDataTable("sp_SearchDDRelationship");
                if (dtEmpLevel.Rows.Count > 0)
                {
                    for (int i = 0; i < dtEmpLevel.Rows.Count; i++)
                    {
                        EmpRelationList.Add(dtEmpLevel.Rows[i][0].ToString());
                    }
                }

                //return EmpRelationList;


            }
            catch (Exception ex)
            {
                EmpRelationList.Add(ex.ToString());
            }
            return EmpRelationList;

        }
        [HttpPost]
        [Route("GetNationality")]
        public List<string> NationalityList(GetNationality GetNationality)
        {
            GetDatabase GDB = new GetDatabase()
            {
                ClientName = GetNationality.CN
            };

            SelfieRegistration2Controller.GetDB2(GDB);
            List<string> NationalityList = new List<string>();
            try
            {

                //try
                //{
                Connection Connection = new Connection();

                System.Data.DataTable dtNationality = new System.Data.DataTable();
                //BasicFunction BasicFunction = new BasicFunction();
                dtNationality = Connection.GetDataTable("sp_GetNationality_Mobile");
                if (dtNationality.Rows.Count > 0)
                {
                    for (int i = 0; i < dtNationality.Rows.Count; i++)
                    {
                        NationalityList.Add(dtNationality.Rows[i][0].ToString());
                    }
                }

                //return EmpRelationList;


            }
            catch (Exception ex)
            {
                NationalityList.Add(ex.ToString());
            }
            return NationalityList;

        }
        [HttpPost]
        [Route("GetNationalityCountry")]
        public List<string> NationalityCountryList(GetNationality GetNationalityCountry)
        {
            GetDatabase GDB = new GetDatabase()
            {
                ClientName = GetNationalityCountry.CN
            };

            SelfieRegistration2Controller.GetDB2(GDB);
            List<string> NationalityCountryList = new List<string>();
            try
            {

                //try
                //{
                Connection Connection = new Connection();

                System.Data.DataTable dtNationalityCountry = new System.Data.DataTable();
                //BasicFunction BasicFunction = new BasicFunction();
                dtNationalityCountry = Connection.GetDataTable("sp_GetNationalityCountry_Mobile");
                if (dtNationalityCountry.Rows.Count > 0)
                {
                    for (int i = 0; i < dtNationalityCountry.Rows.Count; i++)
                    {
                        NationalityCountryList.Add(dtNationalityCountry.Rows[i][0].ToString());
                    }
                }

                //return EmpRelationList;


            }
            catch (Exception ex)
            {
                NationalityCountryList.Add(ex.ToString());
            }
            return NationalityCountryList;

        }
        [HttpPost]
        [Route("GetAllEmployeeList")]
        public List<AllEmployeeList> GetAllEmployeeList(GetEmpAccess GetEmpAccess)
        {
            GetDatabase GDB = new GetDatabase()
            {
                ClientName = GetEmpAccess.CN
            };

            SelfieRegistration2Controller.GetDB2(GDB);
            List<AllEmployeeList> EmpLevelList = new List<AllEmployeeList>();
            try
            {

                //try
                //{
                Connection Connection = new Connection();

                System.Data.DataTable dtEmpLevel = new System.Data.DataTable();
                //BasicFunction BasicFunction = new BasicFunction();
                dtEmpLevel = Connection.GetDataTable("sp_GetAllEmployee");
                if (dtEmpLevel.Rows.Count > 0)
                {
                    for (int i = 0; i < dtEmpLevel.Rows.Count; i++)
                    {
                        AllEmployeeList List = new AllEmployeeList();
                        List.EmpID = dtEmpLevel.Rows[i][0].ToString();
                        List.Name = dtEmpLevel.Rows[i][1].ToString();
                        List.Email = dtEmpLevel.Rows[i][2].ToString();
                        List.Image = dtEmpLevel.Rows[i][3].ToString();
                        EmpLevelList.Add(List);
                    }
                }

                return EmpLevelList;


            }
            catch (Exception ex)
            {
                AllEmployeeList List = new AllEmployeeList();
                //List. = ex.ToString();
                EmpLevelList.Add(List);
            }
            return EmpLevelList;

        }
        [HttpPost]
        [Route("GetPositionsList")]
        public List<string> GetPositionsList(GetEmpAccess GetEmpAccess)
        {
            GetDatabase GDB = new GetDatabase()
            {
                ClientName = GetEmpAccess.CN
            };

            SelfieRegistration2Controller.GetDB2(GDB);
            List<string> PositionsList = new List<string>();
            try
            {

                //try
                //{
                Connection Connection = new Connection();

                System.Data.DataTable dtPositions = new System.Data.DataTable();
                //BasicFunction BasicFunction = new BasicFunction();
                dtPositions = Connection.GetDataTable("sp_GetPosition_Mobile");
                if (dtPositions.Rows.Count > 0)
                {
                    for (int i = 0; i < dtPositions.Rows.Count; i++)
                    {
                        PositionsList.Add(dtPositions.Rows[i][0].ToString());
                    }
                }

                //return EmpRelationList;


            }
            catch (Exception ex)
            {
                PositionsList.Add(ex.ToString());
            }
            return PositionsList;

        }

        #endregion

    }
}