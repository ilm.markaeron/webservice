﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using illimitadoWepAPI.Models;
using System.Xml.Serialization;
using System.IO;
using System.Data;
using System.Data.SqlClient;
using illimitadoWepAPI.MyClass;
using static illimitadoWepAPI.Models.TimeKeeping;
using System.Web;

namespace illimitadoWepAPI.Controllers
{
    public class TimeKeepingController : ApiController
    {
        // GET: api/TimeKeeping
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/TimeKeeping/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/TimeKeeping
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/TimeKeeping/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/TimeKeeping/5
        public void Delete(int id)
        {
        }


        [HttpPost]
        [Route("timein")]
        public string TimeIN(Login userToTimeIN)
        {
            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.NVarChar, Value = userToTimeIN.LoginID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Coordinates", mytype = SqlDbType.NVarChar, Value = userToTimeIN.Coordinates });

            // DataTable EmployeeDt = new DataTable();
            String ID = Connection.ExecuteScalar("sp_TimeIn_Mobile");

            return ID;

        }
        /// <summary>
        /// {
        /// "NTID": "i00143"
        /// }
        /// </summary>
        /// <param name="DailyParameters"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("TimeINDisplay")]
        public GetDailyResult ShowDailyRecord(DailyParameters DailyParameters)
        {
            GetDailyResult GetDailyResult = new GetDailyResult();
            //try
            //{
            Connection Connection = new Connection();
            List<Daily> DailyList = new List<Daily>();
            DataTable dtDAILY = new DataTable();
            //BasicFunction BasicFunction = new BasicFunction();
            Connection.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.NVarChar, Value = DailyParameters.NTID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Coordinates", mytype = SqlDbType.NVarChar, Value = DailyParameters.Coordinates });
            Connection.myparameters.Add(new myParameters { ParameterName = "@NearestEstablishment", mytype = SqlDbType.NVarChar, Value = DailyParameters.NearestEstablishment });
            //    Connection.myparameters.Add(new myParameters { ParameterName = "@STARTDATE", mytype = SqlDbType.DateTime, Value = Convert.ToDateTime(DailyParameters.Start_Date) });
            //    Connection.myparameters.Add(new myParameters { ParameterName = "@ENDDATE", mytype = SqlDbType.DateTime, Value = BasicFunction.getenddate(DailyParameters.End_Date) });
            dtDAILY = Connection.GetDataTable("sp_TimeIn_Mobile");
            //CultureInfo provider = CultureInfo.InvariantCulture;
            if (dtDAILY.Rows.Count > 0)
            {
                foreach (DataRow row in dtDAILY.Rows)
                {
                    Daily Daily = new Daily
                    {
                        SchedDate = Convert.ToDateTime(row["SchedDate"]).ToString("MMM-dd-yyyy"),
                        //SchedDay = Convert.ToDateTime(row["SchedDate"]).DayOfWeek.ToString().Substring(0, 3),
                        SchedIN = row["Start_Time"].ToString(),
                        SchedOUT = row["End_Time"].ToString(),
                        //TotalTimeHour = BasicFunction.Decimal102(row["TotalTimeHour"].ToString()),
                        SchedType = row["ReasonDesc"].ToString().Replace("\r\n", string.Empty)
                        //DateFormat = Convert.ToDateTime(row["SchedDate"]).ToString()
                    };
                    DailyList.Add(Daily);
                }
                GetDailyResult.Daily = DailyList;
                GetDailyResult.myreturn = "Success";
            }
            else
            {
                GetDailyResult.myreturn = "No Data Available";
            }
            //}
            //catch
            //{
            //    GetDailyResult.myreturn = "Error Fetching Data";

            //}
            return GetDailyResult;
        }


        /// <summary>
        /// {
        ///"NTID": "EMP0003",
        ///"Start_Date": "May-05-2017"
        ///}
        /// </summary>
        /// <param name="DailyParameters"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("TimeOutDisplay")]
        public string TimeOutAndDisplay(DailyParameters DailyParameters)
        {
            //GetDailyResult GetDailyResult = new GetDailyResult();
            //try
            //{
            Connection Connection = new Connection();
            //List<Daily> DailyList = new List<Daily>();
            //DataTable dtDAILY = new DataTable();
            //BasicFunction BasicFunction = new BasicFunction();
            Connection.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.NVarChar, Value = DailyParameters.NTID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Coordinates", mytype = SqlDbType.NVarChar, Value = DailyParameters.Coordinates });
            Connection.myparameters.Add(new myParameters { ParameterName = "@NearestEstablishment", mytype = SqlDbType.NVarChar, Value = DailyParameters.NearestEstablishment });
            //Connection.myparameters.Add(new myParameters { ParameterName = "@LastSched", mytype = SqlDbType.NVarChar, Value = Convert.ToDateTime(DailyParameters.Start_Date).ToString("MMM-dd-yyyy") });
            String ID = Connection.ExecuteScalar("sp_TimeOut_Mobile");

            return ID;


            //    Connection.myparameters.Add(new myParameters { ParameterName = "@ENDDATE", mytype = SqlDbType.DateTime, Value = BasicFunction.getenddate(DailyParameters.End_Date) });
            //dtDAILY = Connection.GetDataTable("sp_TimeOut_Mobile");
            //CultureInfo provider = CultureInfo.InvariantCulture;
            //if (dtDAILY.Rows.Count > 0)
            //{
            //    foreach (DataRow row in dtDAILY.Rows)
            //    {
            //        Daily Daily = new Daily
            //        {
            //            SchedDate = Convert.ToDateTime(row["SchedDate"]).ToString("MMM-dd-yyyy"),
            //            SchedDay = Convert.ToDateTime(row["SchedDate"]).DayOfWeek.ToString().Substring(0, 3),
            //            SchedIN = row["Start_Time"].ToString(),
            //            SchedOUT = row["End_Time"].ToString(),
            //            TotalTimeHour = BasicFunction.Decimal102(row["TotalTimeHour"].ToString()),
            //            SchedType = row["ReasonDesc"].ToString()
            //            DateFormat = Convert.ToDateTime(row["SchedDate"]).ToString()
            //        };
            //        DailyList.Add(Daily);
            //    }
            //    GetDailyResult.Daily = DailyList;
            //    GetDailyResult.myreturn = "Success";
            //}
            //else
            //{
            //    GetDailyResult.myreturn = "No Data Available";
            //}
            //}
            //catch
            //{
            //    GetDailyResult.myreturn = "Error Fetching Data";

            //}
            //return GetDailyResult;
        }

        /// <summary>
        /// {
        ///"NTID": "EMP0003"
        ///}
        /// </summary>
        /// <param name="DailyParameters"></param>
        /// <returns></returns>

        [HttpPost]
        [Route("DisplayCurrentDayActivity")]
        public GetDailyResult DisplayCurrentDayActivity(DailyParameters DailyParameters)
        {
            GetDailyResult GetDailyResult = new GetDailyResult();
            //try
            //{
            Connection Connection = new Connection();
            List<Daily> DailyList = new List<Daily>();
            DataTable dtDAILY = new DataTable();
            //BasicFunction BasicFunction = new BasicFunction();
            Connection.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.NVarChar, Value = DailyParameters.NTID });
            dtDAILY = Connection.GetDataTable("sp_DisplayCurrentDayActivity_Mobile");
            //CultureInfo provider = CultureInfo.InvariantCulture;
            if (dtDAILY.Rows.Count > 0)
            {
                foreach (DataRow row in dtDAILY.Rows)
                {
                    Daily Daily = new Daily
                    {
                        SchedDate = Convert.ToDateTime(row["SchedDate"]).ToString("MMM-dd-yyyy"),
                        //SchedDay = Convert.ToDateTime(row["SchedDate"]).DayOfWeek.ToString().Substring(0, 3),
                        SchedIN = row["Start_Time"].ToString(),
                        SchedOUT = row["End_Time"].ToString(),
                        //TotalTimeHour = BasicFunction.Decimal102(row["TotalTimeHour"].ToString()),
                        SchedType = row["ReasonDesc"].ToString()
                        //DateFormat = Convert.ToDateTime(row["SchedDate"]).ToString()
                    };
                    DailyList.Add(Daily);
                }
                GetDailyResult.Daily = DailyList;
                GetDailyResult.myreturn = "Success";
            }
            else
            {
                GetDailyResult.myreturn = "No Data Available";
            }
            //}
            //catch
            //{
            //    GetDailyResult.myreturn = "Error Fetching Data";

            //}
            return GetDailyResult;
        }

        /// <summary>
        /// {
        ///"NTID": "EMPLOYEETEST",
        ///"ReasonID": "3",
        ///"StartOrEnd": "2"
        /// </summary>
        /// <param name="uBreak"></param>
        /// <returns></returns>

        [HttpPost]
        [Route("UserBreaks")]
        public string UserBreak(ForBreaks uBreak)
        {
            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.NVarChar, Value = uBreak.NTID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@ReasonID", mytype = SqlDbType.NVarChar, Value = uBreak.ReasonID });
          //  Connection.myparameters.Add(new myParameters { ParameterName = "@STARTOREND", mytype = SqlDbType.NVarChar, Value = uBreak.StartOrEnd });

            // DataTable EmployeeDt = new DataTable();
            String ID = Connection.ExecuteScalar("sp_InsertToActivityTrackerBreaks_Mobile");

            return ID;

        }



        //{
        //    "NTID": "EMP127",
        //    "FaceID": "10002"
        //}

        [HttpPost]
        [Route("UpdateFaceID")]
        public string UpdateFaceID(UpadateFaceID FaceIDUpdate)
        {
            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@NTID", mytype = SqlDbType.NVarChar, Value = FaceIDUpdate.NTID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@FaceID", mytype = SqlDbType.NVarChar, Value = FaceIDUpdate.FaceID });
   

            // DataTable EmployeeDt = new DataTable();
            String ID = Connection.ExecuteScalar("sp_UpdateEmployeeMasterFaceID_Mobile");

            return ID;

        }


        [HttpPost]
        [Route("ShowHolidays")]
        public GetHolidayResult GetAllHolidays(Daily dly)
        {
            GetHolidayResult GetHolidayResult = new GetHolidayResult();
            List<Holiday> Holidays = new List<Holiday>();
            try
            {
                Connection Connection = new Connection();
                DataTable dtHolidays = new DataTable();
                Connection.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.NVarChar, Value = dly.EmpID });
                dtHolidays = Connection.GetDataTable("sp_ShowHolidays");
                // CultureInfo provider = CultureInfo.InvariantCulture;
                foreach (DataRow row in dtHolidays.Rows)
                {
                    Holiday Holiday = new Holiday
                    {
                        HolidayName = row["HolDesc"].ToString(),
                        Type = row["HolStatus"].ToString(),
                        HolidayDate = Convert.ToDateTime(row["HolDate"]).ToString("MMM-dd-yyyy")
                    };

                    Holidays.Add(Holiday);
                    GetHolidayResult.Holidays = Holidays;
                    GetHolidayResult.myreturn = "Success";
                }
                return GetHolidayResult;
            }
            catch
            {
                GetHolidayResult.myreturn = "Error";
                return GetHolidayResult;
            }
        }


        [HttpPost]
        [Route("DisplayActivityByRange")]
        //{
        //"NTID": "i101",
        //"Start_Date": "2017-04-01",
        //"End_Date": "2017-04-30"
        //}
        public ListAllActivityResult DisplayActivityByRange(DailyParameters DailyParameters)
        {
            ListAllActivityResult GetActivityResult = new ListAllActivityResult();
            //try
            //{
            Connection Connection = new Connection();
            List<ListAllActivityByRange> AllActivity = new List<ListAllActivityByRange>();
            DataTable dtDAILY = new DataTable();
            //BasicFunction BasicFunction = new BasicFunction();
            Connection.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.NVarChar, Value = DailyParameters.NTID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@SchedDate1", mytype = SqlDbType.NVarChar, Value = DailyParameters.Start_Date });
            Connection.myparameters.Add(new myParameters { ParameterName = "@SchedDate2", mytype = SqlDbType.NVarChar, Value = DailyParameters.End_Date});
            dtDAILY = Connection.GetDataTable("sp_GetEmployeeActivity_Mobile");
            //CultureInfo provider = CultureInfo.InvariantCulture;
            if (dtDAILY.Rows.Count > 0)
            {
                foreach (DataRow row in dtDAILY.Rows)
                {
                    ListAllActivityByRange ListAllActivity = new ListAllActivityByRange
                    {
                        EmpID = row["EMPID"].ToString(),
                        //StartTIme = row["Schedule"].ToString() == "LEAVE" || row["Schedule"].ToString() == "RD" ? row["Schedule"].ToString() : row["Start_Time"].ToString(),
                        //StartEnd = row["Schedule"].ToString() == "LEAVE" || row["Schedule"].ToString() == "RD" ? row["Schedule"].ToString() : row["End_Time"].ToString(),
                        StartTIme = row["Start_Time"].ToString(),
                        StartEnd = row["End_Time"].ToString(),
                        ReasonID = row["ReasonID"].ToString(),
                        ReasonDesc = row["ReasonDesc"].ToString(),
                        IsPaid = row["IsPaid"].ToString(),
                        SchedDate = row["SchedDate"].ToString(),
                        ActivityStatus = row["Activity_Status"].ToString(),
                        HourWork = row["TotalTimeMin"].ToString(),
                        SeriesID = row["SeriesID"].ToString(),
                        //SchedDateStatus = row["Activity_Tag"].ToString() + "/" + row["ApprovedWork"].ToString()
                        SchedDateStatus = row["ApprovedWork"].ToString()
                    };
                    AllActivity.Add(ListAllActivity);
                }
                GetActivityResult.ListAllActivity = AllActivity;
                GetActivityResult.myreturn = "Success";
            }
            else
            {
                GetActivityResult.myreturn = "No Data Available";
            }

            return GetActivityResult;
        }

        //Nhoraisa get total hours worked

        [HttpPost]
        [Route("DisplayActivityByRangeAlternative")]
        public ListAllActivityResult DisplayActivityByRangeAlternative(DailyParameters DailyParameters)
        {
            ListAllActivityResult GetActivityResult = new ListAllActivityResult();
            //try
            //{
            Connection Connection = new Connection();
            List<ListAllActivityByRange> AllActivity = new List<ListAllActivityByRange>();
            DataTable dtDAILY = new DataTable();
            //BasicFunction BasicFunction = new BasicFunction();
            Connection.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.NVarChar, Value = DailyParameters.NTID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@SchedDate1", mytype = SqlDbType.NVarChar, Value = DailyParameters.Start_Date });
            Connection.myparameters.Add(new myParameters { ParameterName = "@SchedDate2", mytype = SqlDbType.NVarChar, Value = DailyParameters.End_Date });
            dtDAILY = Connection.GetDataTable("sp_GetEmployeeActivity_Mobile_Alternative");
            //CultureInfo provider = CultureInfo.InvariantCulture;
            if (dtDAILY.Rows.Count > 0)
            {
                foreach (DataRow row in dtDAILY.Rows)
                {
                    ListAllActivityByRange ListAllActivity = new ListAllActivityByRange
                    {
                        EmpID = row["EMPID"].ToString(),
                        StartTIme = row["Schedule"].ToString() == "LEAVE" || row["Schedule"].ToString() == "RD" ? row["Schedule"].ToString() : row["Start_Time"].ToString(),
                        StartEnd = row["Schedule"].ToString() == "LEAVE" || row["Schedule"].ToString() == "RD" ? row["Schedule"].ToString() : row["End_Time"].ToString(),
                        //StartTIme = row["Start_Time"].ToString(),
                        //StartEnd = row["End_Time"].ToString(),
                        ReasonID = row["ReasonID"].ToString(),
                        ReasonDesc = row["ReasonDesc"].ToString(),
                        IsPaid = row["IsPaid"].ToString(),
                        SchedDate = row["SchedDate"].ToString(),
                        ActivityStatus = row["Activity_Status"].ToString(),
                        HourWork = row["TotalTimeMin"].ToString(),
                        SeriesID = row["SeriesID"].ToString(),
                        //SchedDateStatus = row["Activity_Tag"].ToString() + "/" + row["ApprovedWork"].ToString()
                        SchedDateStatus = row["ApprovedWork"].ToString()
                    };
                    AllActivity.Add(ListAllActivity);
                }
                GetActivityResult.ListAllActivity = AllActivity;
                GetActivityResult.myreturn = "Success";
            }
            else
            {
                GetActivityResult.myreturn = "No Data Available";
            }

            return GetActivityResult;
        }

        // Nhoraisa Insert Survey

        [HttpPost]
        [Route("CarInformationInsertUpdate")]
        public string CarInformationInsertUpdate(CarInformation CarInsert)
        {
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = CarInsert.CN;
            SelfieRegistration2Controller.GetDB2(GDB);
            try
            {
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.NVarChar, Value = CarInsert._EmpID });
                con.myparameters.Add(new myParameters { ParameterName = "@Own", mytype = SqlDbType.NVarChar, Value = CarInsert._Own });
                con.myparameters.Add(new myParameters { ParameterName = "@isRemindedDue", mytype = SqlDbType.NVarChar, Value = CarInsert._isRemindedDue });
                con.myparameters.Add(new myParameters { ParameterName = "@Brand", mytype = SqlDbType.NVarChar, Value = CarInsert._Brand });
                con.myparameters.Add(new myParameters { ParameterName = "@Model", mytype = SqlDbType.NVarChar, Value = CarInsert._Model });
                con.myparameters.Add(new myParameters { ParameterName = "@Variant", mytype = SqlDbType.NVarChar, Value = CarInsert._Variant });
                con.myparameters.Add(new myParameters { ParameterName = "@DatePurchased", mytype = SqlDbType.NVarChar, Value = CarInsert._DatePurchased });
                con.myparameters.Add(new myParameters { ParameterName = "@YearMake", mytype = SqlDbType.NVarChar, Value = CarInsert._YearMake });
                con.myparameters.Add(new myParameters { ParameterName = "@PlateNum", mytype = SqlDbType.NVarChar, Value = CarInsert._PlateNum });
                con.myparameters.Add(new myParameters { ParameterName = "@isInsured", mytype = SqlDbType.NVarChar, Value = CarInsert._isInsured });
                con.myparameters.Add(new myParameters { ParameterName = "@PolicyExpDate", mytype = SqlDbType.NVarChar, Value = CarInsert._PolicyExpDate });
                con.myparameters.Add(new myParameters { ParameterName = "@PremiumAmount", mytype = SqlDbType.NVarChar, Value = CarInsert._PremiumAmount });
                con.myparameters.Add(new myParameters { ParameterName = "@isRemindedExp", mytype = SqlDbType.NVarChar, Value = CarInsert._isRemindedExp });
                con.myparameters.Add(new myParameters { ParameterName = "@isCarLoan", mytype = SqlDbType.NVarChar, Value = CarInsert._isCarLoan });
                con.myparameters.Add(new myParameters { ParameterName = "@DueDate", mytype = SqlDbType.NVarChar, Value = CarInsert._DueDate });
                con.myparameters.Add(new myParameters { ParameterName = "@AmortizationAmount", mytype = SqlDbType.NVarChar, Value = CarInsert._AmortizationAmount });
                con.myparameters.Add(new myParameters { ParameterName = "@LoanEndMonth", mytype = SqlDbType.NVarChar, Value = CarInsert._LoanEndMonth });
                con.myparameters.Add(new myParameters { ParameterName = "@LoanEndYear", mytype = SqlDbType.NVarChar, Value = CarInsert._LoanEndYear });
                con.myparameters.Add(new myParameters { ParameterName = "@Url", mytype = SqlDbType.NVarChar, Value = CarInsert._Url });

                con.ExecuteNonQuery("sp_InsertCarInformationv2");
                return "Success";
                //return "True";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }

        [HttpPost]
        [Route("CarInformationInsertUpdatev2")]
        public string CarInformationInsertUpdatev2(CarInformation CarInsert)
        {
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = CarInsert.CN;
            SelfieRegistration2Controller.GetDB2(GDB);
            try
            {
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.NVarChar, Value = CarInsert._EmpID });
                con.myparameters.Add(new myParameters { ParameterName = "@Own", mytype = SqlDbType.NVarChar, Value = CarInsert._Own });
                con.myparameters.Add(new myParameters { ParameterName = "@isRemindedDue", mytype = SqlDbType.NVarChar, Value = CarInsert._isRemindedDue });
                con.myparameters.Add(new myParameters { ParameterName = "@Brand", mytype = SqlDbType.NVarChar, Value = CarInsert._Brand });
                con.myparameters.Add(new myParameters { ParameterName = "@Model", mytype = SqlDbType.NVarChar, Value = CarInsert._Model });
                con.myparameters.Add(new myParameters { ParameterName = "@Variant", mytype = SqlDbType.NVarChar, Value = CarInsert._Variant });
                con.myparameters.Add(new myParameters { ParameterName = "@DatePurchased", mytype = SqlDbType.NVarChar, Value = CarInsert._DatePurchased });
                con.myparameters.Add(new myParameters { ParameterName = "@YearMake", mytype = SqlDbType.NVarChar, Value = CarInsert._YearMake });
                con.myparameters.Add(new myParameters { ParameterName = "@PlateNum", mytype = SqlDbType.NVarChar, Value = CarInsert._PlateNum });
                con.myparameters.Add(new myParameters { ParameterName = "@isInsured", mytype = SqlDbType.NVarChar, Value = CarInsert._isInsured });
                con.myparameters.Add(new myParameters { ParameterName = "@PolicyExpDate", mytype = SqlDbType.NVarChar, Value = CarInsert._PolicyExpDate });
                con.myparameters.Add(new myParameters { ParameterName = "@PremiumAmount", mytype = SqlDbType.NVarChar, Value = CarInsert._PremiumAmount });
                con.myparameters.Add(new myParameters { ParameterName = "@isRemindedExp", mytype = SqlDbType.NVarChar, Value = CarInsert._isRemindedExp });
                con.myparameters.Add(new myParameters { ParameterName = "@isCarLoan", mytype = SqlDbType.NVarChar, Value = CarInsert._isCarLoan });
                con.myparameters.Add(new myParameters { ParameterName = "@DueDate", mytype = SqlDbType.NVarChar, Value = CarInsert._DueDate });
                con.myparameters.Add(new myParameters { ParameterName = "@AmortizationAmount", mytype = SqlDbType.NVarChar, Value = CarInsert._AmortizationAmount });
                con.myparameters.Add(new myParameters { ParameterName = "@LoanEndMonth", mytype = SqlDbType.NVarChar, Value = CarInsert._LoanEndMonth });
                con.myparameters.Add(new myParameters { ParameterName = "@LoanEndYear", mytype = SqlDbType.NVarChar, Value = CarInsert._LoanEndYear });

                con.ExecuteNonQuery("sp_InsertCarInformation");
                return "Success";
                //return "True";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }


        [HttpPost]
        [Route("HomeInformationInsertUpdate")]
        public string HomeInformationInsertUpdate(HomeInformation HomeInsert)
        {
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = HomeInsert.CN;
            SelfieRegistration2Controller.GetDB2(GDB);
            try
            {
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.NVarChar, Value = HomeInsert._EmpID });
                con.myparameters.Add(new myParameters { ParameterName = "@HomeStatus", mytype = SqlDbType.NVarChar, Value = HomeInsert._HomeStatus });
                con.myparameters.Add(new myParameters { ParameterName = "@HouseType", mytype = SqlDbType.NVarChar, Value = HomeInsert._HouseType });
                con.myparameters.Add(new myParameters { ParameterName = "@PayOften", mytype = SqlDbType.NVarChar, Value = HomeInsert._PayOften });
                con.myparameters.Add(new myParameters { ParameterName = "@NextPayDue", mytype = SqlDbType.NVarChar, Value = HomeInsert._NextPayDue });
                con.myparameters.Add(new myParameters { ParameterName = "@PayDueDate", mytype = SqlDbType.NVarChar, Value = HomeInsert._PayDueDate });
                con.myparameters.Add(new myParameters { ParameterName = "@RentAmount", mytype = SqlDbType.NVarChar, Value = HomeInsert._RentAmount });
                con.myparameters.Add(new myParameters { ParameterName = "@isRemindedHoa", mytype = SqlDbType.NVarChar, Value = HomeInsert._isRemindedHoa });
                con.myparameters.Add(new myParameters { ParameterName = "@PayOftenHoa", mytype = SqlDbType.NVarChar, Value = HomeInsert._PayOftenHoa });
                con.myparameters.Add(new myParameters { ParameterName = "@NextPayDueHoa", mytype = SqlDbType.NVarChar, Value = HomeInsert._NextPayDueHoa });
                con.myparameters.Add(new myParameters { ParameterName = "@PayDueDateHoa", mytype = SqlDbType.NVarChar, Value = HomeInsert._PayDueDateHoa });
                con.myparameters.Add(new myParameters { ParameterName = "@HoaAmount", mytype = SqlDbType.NVarChar, Value = HomeInsert._HoaAmount });
                con.myparameters.Add(new myParameters { ParameterName = "@LoanBank", mytype = SqlDbType.NVarChar, Value = HomeInsert._LoanBank });
                con.myparameters.Add(new myParameters { ParameterName = "@LoanTerm", mytype = SqlDbType.NVarChar, Value = HomeInsert._LoanTerm });
                con.myparameters.Add(new myParameters { ParameterName = "@LoanTermMonth", mytype = SqlDbType.NVarChar, Value = HomeInsert._LoanTermMonth });
                con.myparameters.Add(new myParameters { ParameterName = "@LoanStartMonth", mytype = SqlDbType.NVarChar, Value = HomeInsert._LoanStartMonth });
                con.myparameters.Add(new myParameters { ParameterName = "@LoanStartYear", mytype = SqlDbType.NVarChar, Value = HomeInsert._LoanStartYear });
                con.myparameters.Add(new myParameters { ParameterName = "@PayOftenLoan", mytype = SqlDbType.NVarChar, Value = HomeInsert._PayOftenLoan });
                con.myparameters.Add(new myParameters { ParameterName = "@NextPayDueLoan", mytype = SqlDbType.NVarChar, Value = HomeInsert._NextPayDueLoan });
                con.myparameters.Add(new myParameters { ParameterName = "@PayDueDateLoan", mytype = SqlDbType.NVarChar, Value = HomeInsert._PayDueDateLoan });
                con.myparameters.Add(new myParameters { ParameterName = "@PayMonthlyAmount", mytype = SqlDbType.NVarChar, Value = HomeInsert._PayMonthlyAmount });
                con.myparameters.Add(new myParameters { ParameterName = "@Url", mytype = SqlDbType.NVarChar, Value = HomeInsert._Url });
                return con.ExecuteScalar("sp_InsertHouseInformationv2");
                //return "True";
            }
            catch (Exception)
            {
                return "False";
            }
        }

        [HttpPost]
        [Route("HomeInformationInsertUpdatev2")]
        public string HomeInformationInsertUpdatev2(HomeInformation HomeInsert)
        {
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = HomeInsert.CN;
            SelfieRegistration2Controller.GetDB2(GDB);
            try
            {
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.NVarChar, Value = HomeInsert._EmpID });
                con.myparameters.Add(new myParameters { ParameterName = "@HomeStatus", mytype = SqlDbType.NVarChar, Value = HomeInsert._HomeStatus });
                con.myparameters.Add(new myParameters { ParameterName = "@HouseType", mytype = SqlDbType.NVarChar, Value = HomeInsert._HouseType });
                con.myparameters.Add(new myParameters { ParameterName = "@PayOften", mytype = SqlDbType.NVarChar, Value = HomeInsert._PayOften });
                con.myparameters.Add(new myParameters { ParameterName = "@NextPayDue", mytype = SqlDbType.NVarChar, Value = HomeInsert._NextPayDue });
                con.myparameters.Add(new myParameters { ParameterName = "@PayDueDate", mytype = SqlDbType.NVarChar, Value = HomeInsert._PayDueDate });
                con.myparameters.Add(new myParameters { ParameterName = "@RentAmount", mytype = SqlDbType.NVarChar, Value = HomeInsert._RentAmount });
                con.myparameters.Add(new myParameters { ParameterName = "@isRemindedHoa", mytype = SqlDbType.NVarChar, Value = HomeInsert._isRemindedHoa });
                con.myparameters.Add(new myParameters { ParameterName = "@PayOftenHoa", mytype = SqlDbType.NVarChar, Value = HomeInsert._PayOftenHoa });
                con.myparameters.Add(new myParameters { ParameterName = "@NextPayDueHoa", mytype = SqlDbType.NVarChar, Value = HomeInsert._NextPayDueHoa });
                con.myparameters.Add(new myParameters { ParameterName = "@PayDueDateHoa", mytype = SqlDbType.NVarChar, Value = HomeInsert._PayDueDateHoa });
                con.myparameters.Add(new myParameters { ParameterName = "@HoaAmount", mytype = SqlDbType.NVarChar, Value = HomeInsert._HoaAmount });
                con.myparameters.Add(new myParameters { ParameterName = "@LoanBank", mytype = SqlDbType.NVarChar, Value = HomeInsert._LoanBank });
                con.myparameters.Add(new myParameters { ParameterName = "@LoanTerm", mytype = SqlDbType.NVarChar, Value = HomeInsert._LoanTerm });
                con.myparameters.Add(new myParameters { ParameterName = "@LoanTermMonth", mytype = SqlDbType.NVarChar, Value = HomeInsert._LoanTermMonth });
                con.myparameters.Add(new myParameters { ParameterName = "@LoanStartMonth", mytype = SqlDbType.NVarChar, Value = HomeInsert._LoanStartMonth });
                con.myparameters.Add(new myParameters { ParameterName = "@LoanStartYear", mytype = SqlDbType.NVarChar, Value = HomeInsert._LoanStartYear });
                con.myparameters.Add(new myParameters { ParameterName = "@PayOftenLoan", mytype = SqlDbType.NVarChar, Value = HomeInsert._PayOftenLoan });
                con.myparameters.Add(new myParameters { ParameterName = "@NextPayDueLoan", mytype = SqlDbType.NVarChar, Value = HomeInsert._NextPayDueLoan });
                con.myparameters.Add(new myParameters { ParameterName = "@PayDueDateLoan", mytype = SqlDbType.NVarChar, Value = HomeInsert._PayDueDateLoan });
                con.myparameters.Add(new myParameters { ParameterName = "@PayMonthlyAmount", mytype = SqlDbType.NVarChar, Value = HomeInsert._PayMonthlyAmount });
                return con.ExecuteScalar("sp_InsertHouseInformation");
                //return "True";
            }
            catch (Exception)
            {
                return "False";
            }
        }

        [HttpPost]
        [Route("SchoolFeeInformationInsertUpdate")]
        public string SchoolFeeInformationInsertUpdate(SchoolFeeInformation SchoolInsert)
        {
            try
            {
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.NVarChar, Value = SchoolInsert._EmpID });
                con.myparameters.Add(new myParameters { ParameterName = "@isPayingTuition", mytype = SqlDbType.NVarChar, Value = SchoolInsert._isPayingTuition });
                con.myparameters.Add(new myParameters { ParameterName = "@SchoolName", mytype = SqlDbType.NVarChar, Value = SchoolInsert._SchoolName });
                con.myparameters.Add(new myParameters { ParameterName = "@DownPaymentAmount", mytype = SqlDbType.NVarChar, Value = SchoolInsert._DownPaymentAmount });
                con.myparameters.Add(new myParameters { ParameterName = "@1stQAmount", mytype = SqlDbType.NVarChar, Value = SchoolInsert._1stQAmount });
                con.myparameters.Add(new myParameters { ParameterName = "@1stQDate", mytype = SqlDbType.NVarChar, Value = SchoolInsert._1stQDate });
                con.myparameters.Add(new myParameters { ParameterName = "@2ndQAmount", mytype = SqlDbType.NVarChar, Value = SchoolInsert._2ndQAmount });
                con.myparameters.Add(new myParameters { ParameterName = "@2ndQDate", mytype = SqlDbType.NVarChar, Value = SchoolInsert._2ndQDate });
                con.myparameters.Add(new myParameters { ParameterName = "@3rdQAmount", mytype = SqlDbType.NVarChar, Value = SchoolInsert._3rdQAmount });
                con.myparameters.Add(new myParameters { ParameterName = "@3rdQDate", mytype = SqlDbType.NVarChar, Value = SchoolInsert._3rdQDate });
                con.myparameters.Add(new myParameters { ParameterName = "@4thQAmount", mytype = SqlDbType.NVarChar, Value = SchoolInsert._4thQAmount });
                con.myparameters.Add(new myParameters { ParameterName = "@4thQDate", mytype = SqlDbType.NVarChar, Value = SchoolInsert._4thQDate });
                return con.ExecuteScalar("sp_InsertSchoolInformation");
                //return "True";
            }
            catch (Exception)
            {
                return "False";
            }
        }

        [HttpPost]
        [Route("SchoolFeeInformationInsertUpdatev2")]
        public string SchoolFeeInformationInsertUpdatev2(SchoolFeeInformation SchoolInsert)
        {
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = SchoolInsert.CN;
            SelfieRegistration2Controller.GetDB2(GDB);
            try
            {
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.NVarChar, Value = SchoolInsert._EmpID });
                con.myparameters.Add(new myParameters { ParameterName = "@isPayingTuition", mytype = SqlDbType.NVarChar, Value = SchoolInsert._isPayingTuition });
                con.myparameters.Add(new myParameters { ParameterName = "@SchoolName", mytype = SqlDbType.NVarChar, Value = SchoolInsert._SchoolName });
                con.myparameters.Add(new myParameters { ParameterName = "@DownPaymentAmount", mytype = SqlDbType.NVarChar, Value = SchoolInsert._DownPaymentAmount });
                con.myparameters.Add(new myParameters { ParameterName = "@1stQAmount", mytype = SqlDbType.NVarChar, Value = SchoolInsert._1stQAmount });
                con.myparameters.Add(new myParameters { ParameterName = "@1stQDate", mytype = SqlDbType.NVarChar, Value = SchoolInsert._1stQDate });
                con.myparameters.Add(new myParameters { ParameterName = "@2ndQAmount", mytype = SqlDbType.NVarChar, Value = SchoolInsert._2ndQAmount });
                con.myparameters.Add(new myParameters { ParameterName = "@2ndQDate", mytype = SqlDbType.NVarChar, Value = SchoolInsert._2ndQDate });
                con.myparameters.Add(new myParameters { ParameterName = "@3rdQAmount", mytype = SqlDbType.NVarChar, Value = SchoolInsert._3rdQAmount });
                con.myparameters.Add(new myParameters { ParameterName = "@3rdQDate", mytype = SqlDbType.NVarChar, Value = SchoolInsert._3rdQDate });
                con.myparameters.Add(new myParameters { ParameterName = "@4thQAmount", mytype = SqlDbType.NVarChar, Value = SchoolInsert._4thQAmount });
                con.myparameters.Add(new myParameters { ParameterName = "@4thQDate", mytype = SqlDbType.NVarChar, Value = SchoolInsert._4thQDate });
                return con.ExecuteScalar("sp_InsertSchoolInformation");
                //return "True";
            }
            catch (Exception)
            {
                return "False";
            }
        }

        [HttpPost]
        [Route("LoanInformationInsertUpdate")]
        public string LoanInformationInsertUpdate(LoanInformation LoanInsert)
        {
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = LoanInsert.CN;
            SelfieRegistration2Controller.GetDB2(GDB);
            try
            {
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.NVarChar, Value = LoanInsert._EmpID });
                con.myparameters.Add(new myParameters { ParameterName = "@isOtherLoan", mytype = SqlDbType.NVarChar, Value = LoanInsert._isOtherLoan });
                con.myparameters.Add(new myParameters { ParameterName = "@BankLoan", mytype = SqlDbType.NVarChar, Value = LoanInsert._BankLoan });
                con.myparameters.Add(new myParameters { ParameterName = "@LoanTermNum", mytype = SqlDbType.NVarChar, Value = LoanInsert._LoanTermNum });
                con.myparameters.Add(new myParameters { ParameterName = "@LoanTerm", mytype = SqlDbType.NVarChar, Value = LoanInsert._LoanTerm });
                con.myparameters.Add(new myParameters { ParameterName = "@LoanStartMonth", mytype = SqlDbType.NVarChar, Value = LoanInsert._LoanStartMonth });
                con.myparameters.Add(new myParameters { ParameterName = "@LoanStartYears", mytype = SqlDbType.NVarChar, Value = LoanInsert._LoanStartYears });
                con.myparameters.Add(new myParameters { ParameterName = "@PayOften", mytype = SqlDbType.NVarChar, Value = LoanInsert._PayOften });
                con.myparameters.Add(new myParameters { ParameterName = "@NextPayDue", mytype = SqlDbType.NVarChar, Value = LoanInsert._NextPayDue });
                con.myparameters.Add(new myParameters { ParameterName = "@PayDueDate", mytype = SqlDbType.NVarChar, Value = LoanInsert._PayDueDate });
                con.myparameters.Add(new myParameters { ParameterName = "@PayMonthlyAmount", mytype = SqlDbType.NVarChar, Value = LoanInsert._PayMonthlyAmount });
                con.myparameters.Add(new myParameters { ParameterName = "@Url", mytype = SqlDbType.NVarChar, Value = LoanInsert._Url });
                return con.ExecuteScalar("sp_InsertLoanInformationv2");
                //return "True";
            }
            catch (Exception)
            {
                return "False";
            }
        }

        [HttpPost]
        [Route("LoanInformationInsertUpdatev2")]
        public string LoanInformationInsertUpdatev2(LoanInformation LoanInsert)
        {
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = LoanInsert.CN;
            SelfieRegistration2Controller.GetDB2(GDB);
            try
            {
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.NVarChar, Value = LoanInsert._EmpID });
                con.myparameters.Add(new myParameters { ParameterName = "@isOtherLoan", mytype = SqlDbType.NVarChar, Value = LoanInsert._isOtherLoan });
                con.myparameters.Add(new myParameters { ParameterName = "@BankLoan", mytype = SqlDbType.NVarChar, Value = LoanInsert._BankLoan });
                con.myparameters.Add(new myParameters { ParameterName = "@LoanTermNum", mytype = SqlDbType.NVarChar, Value = LoanInsert._LoanTermNum });
                con.myparameters.Add(new myParameters { ParameterName = "@LoanTerm", mytype = SqlDbType.NVarChar, Value = LoanInsert._LoanTerm });
                con.myparameters.Add(new myParameters { ParameterName = "@LoanStartMonth", mytype = SqlDbType.NVarChar, Value = LoanInsert._LoanStartMonth });
                con.myparameters.Add(new myParameters { ParameterName = "@LoanStartYears", mytype = SqlDbType.NVarChar, Value = LoanInsert._LoanStartYears });
                con.myparameters.Add(new myParameters { ParameterName = "@PayOften", mytype = SqlDbType.NVarChar, Value = LoanInsert._PayOften });
                con.myparameters.Add(new myParameters { ParameterName = "@NextPayDue", mytype = SqlDbType.NVarChar, Value = LoanInsert._NextPayDue });
                con.myparameters.Add(new myParameters { ParameterName = "@PayDueDate", mytype = SqlDbType.NVarChar, Value = LoanInsert._PayDueDate });
                con.myparameters.Add(new myParameters { ParameterName = "@PayMonthlyAmount", mytype = SqlDbType.NVarChar, Value = LoanInsert._PayMonthlyAmount });
                return con.ExecuteScalar("sp_InsertLoanInformation");
                //return "True";
            }
            catch (Exception)
            {
                return "False";
            }
        }

        [HttpPost]
        [Route("UtilitiesInformationInsertUpdatev2")]
        public string UtilitiesInformationInsertUpdatev2(UtlitiesInformation LoanInsert)
        {
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = LoanInsert.CN;
            SelfieRegistration2Controller.GetDB2(GDB);
            try
            {
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.NVarChar, Value = LoanInsert._EmpID });
                con.myparameters.Add(new myParameters { ParameterName = "@ServiceType", mytype = SqlDbType.NVarChar, Value = LoanInsert._ServiceType });
                con.myparameters.Add(new myParameters { ParameterName = "@ProviderName", mytype = SqlDbType.NVarChar, Value = LoanInsert._ProviderName });
                con.myparameters.Add(new myParameters { ParameterName = "@AccountNumber", mytype = SqlDbType.NVarChar, Value = LoanInsert._AccountNumber });
                con.myparameters.Add(new myParameters { ParameterName = "@MonthlyFee", mytype = SqlDbType.NVarChar, Value = LoanInsert._MonthlyFee });
                con.myparameters.Add(new myParameters { ParameterName = "@DueDate", mytype = SqlDbType.NVarChar, Value = LoanInsert._DueDate });
                return con.ExecuteScalar("sp_InsertUtilitiesInformation");
                //return "True";
            }
            catch (Exception)
            {
                return "False";
            }
        }


        [HttpPost]
        [Route("UtilitiesInformationInsertUpdate")]
        public string UtilitiesInformationInsertUpdate(UtlitiesInformation LoanInsert)
        {
            try
            {
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.NVarChar, Value = LoanInsert._EmpID });
                con.myparameters.Add(new myParameters { ParameterName = "@ServiceType", mytype = SqlDbType.NVarChar, Value = LoanInsert._ServiceType });
                con.myparameters.Add(new myParameters { ParameterName = "@ProviderName", mytype = SqlDbType.NVarChar, Value = LoanInsert._ProviderName });
                con.myparameters.Add(new myParameters { ParameterName = "@AccountNumber", mytype = SqlDbType.NVarChar, Value = LoanInsert._AccountNumber });
                con.myparameters.Add(new myParameters { ParameterName = "@MonthlyFee", mytype = SqlDbType.NVarChar, Value = LoanInsert._MonthlyFee });
                con.myparameters.Add(new myParameters { ParameterName = "@DueDate", mytype = SqlDbType.NVarChar, Value = LoanInsert._DueDate });
                return con.ExecuteScalar("sp_InsertUtilitiesInformation");
                //return "True";
            }
            catch (Exception)
            {
                return "False";
            }
        }

        [HttpPost]
        [Route("DisplayLogsForApproval")]
        public GetDailyResult DisplayLogsForApproval(DailyParameters DailyParameters)
        {
            GetDailyResult GetDailyResult = new GetDailyResult();
            //try
            //{
            Connection Connection = new Connection();
            List<Daily> DailyList = new List<Daily>();
            DataTable dtDAILY = new DataTable();
            //BasicFunction BasicFunction = new BasicFunction();
            Connection.myparameters.Add(new myParameters { ParameterName = "@MNGRID", mytype = SqlDbType.NVarChar, Value = DailyParameters.NTID });
            dtDAILY = Connection.GetDataTable("sp_DisplayLogsForApproval_Mobile");
            //CultureInfo provider = CultureInfo.InvariantCulture;
            if (dtDAILY.Rows.Count > 0)
            {
                foreach (DataRow row in dtDAILY.Rows)
                {
                    Daily Daily = new Daily
                    {
                        SchedDate = Convert.ToDateTime(row["SchedDate"]).ToString("yyyy-MM-dd"),
                        EmpID = row["EmpID"].ToString(),
                        Name = row["EmpName"].ToString(),
                        Reason = row["ReasonDesc"].ToString(),
                        TotalTimeMin = row["TotalTimeMin"].ToString(),
                        SchedIN = row["Start_Time"].ToString(),
                        SchedOUT = row["End_Time"].ToString(),
                        SeriesID = row["SeriesID"].ToString()
                        

                    };
                    DailyList.Add(Daily);
                }
                GetDailyResult.Daily = DailyList;
                GetDailyResult.myreturn = "Success";
            }
            else
            {
                GetDailyResult.myreturn = "No Data Available";
            }
            //}
            //catch
            //{
            //    GetDailyResult.myreturn = "Error Fetching Data";

            //}
            return GetDailyResult;
        }



        [HttpPost]
        [Route("GetTotalHoursWrk")]
        //{
        //"NTID": "i101",
        //"Start_Date": "2017-04-01",
        //"End_Date": "2017-04-30"
        //}
        public ListAllActivityResult GetTotalHoursWrk(DailyParameters DailyParameters)
        {
            ListAllActivityResult GetActivityResult = new ListAllActivityResult();
            //try
            //{
            Connection Connection = new Connection();
            List<ListAllActivityByRange> AllActivity = new List<ListAllActivityByRange>();
            DataTable dtDAILY = new DataTable();
            //BasicFunction BasicFunction = new BasicFunction();
            Connection.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.NVarChar, Value = DailyParameters.NTID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@StartDate", mytype = SqlDbType.NVarChar, Value = Convert.ToDateTime(DailyParameters.Start_Date).ToString("yyyy-MM-dd") });
            Connection.myparameters.Add(new myParameters { ParameterName = "@EndDate", mytype = SqlDbType.NVarChar, Value = Convert.ToDateTime(DailyParameters.End_Date).ToString("yyyy-MM-dd") });
            dtDAILY = Connection.GetDataTable("sp_GetEmpTotalWorkHrs_Mobile");
            //CultureInfo provider = CultureInfo.InvariantCulture;
            if (dtDAILY.Rows.Count > 0)
            {
                foreach (DataRow row in dtDAILY.Rows)
                {
                    ListAllActivityByRange ListAllActivity = new ListAllActivityByRange
                    {
                        EmpID = row["EmpID"].ToString(),
                        SchedDate = row["SchedDate"].ToString(),
                        HourWork = row["HoursWorked"].ToString()
                    };
                    AllActivity.Add(ListAllActivity);
                }
                GetActivityResult.ListAllActivity = AllActivity;
                GetActivityResult.myreturn = "Success";
            }
            else
            {
                GetActivityResult.myreturn = "No Data Available";
            }

            return GetActivityResult;
        }

        //Nhoraisa get schedule view/ totaltime

        [HttpPost]
        [Route("GetTotalHoursWorkMobile")]
        public ListAllHoursworked GetTotalHoursWorkMobile(DailyParameters DailyParameters)
        {
            ListAllHoursworked GetActivityResult = new ListAllHoursworked();
            //try
            //{
            Connection Connection = new Connection();
            List<Hoursworked> AllActivity = new List<Hoursworked>();
            DataTable dtDAILY = new DataTable();
            //BasicFunction BasicFunction = new BasicFunction();
            Connection.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.NVarChar, Value = DailyParameters.NTID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@StartDate", mytype = SqlDbType.NVarChar, Value = Convert.ToDateTime(DailyParameters.Start_Date).ToString("yyyy-MM-dd") });
            Connection.myparameters.Add(new myParameters { ParameterName = "@EndDate", mytype = SqlDbType.NVarChar, Value = Convert.ToDateTime(DailyParameters.End_Date).ToString("yyyy-MM-dd") });
            dtDAILY = Connection.GetDataTable("sp_GetEmpTotalWorkHrs_Mobile1");
            //CultureInfo provider = CultureInfo.InvariantCulture;
            if (dtDAILY.Rows.Count > 0)
            {
                foreach (DataRow row in dtDAILY.Rows)
                {
                    Hoursworked ListAllActivity = new Hoursworked
                    {
                        EmpID = row["EmpID"].ToString(),
                        SchedDate = row["SchedDate"].ToString(),
                        HourWork = row["HoursWorked"].ToString(),
                        Day = row["Day"].ToString(),
                        Schedule = row["Schedule"].ToString(),
                        TotalTime = row["TotalTime"].ToString()
                    };
                    AllActivity.Add(ListAllActivity);
                }
                GetActivityResult.ListAllActivity = AllActivity;
                GetActivityResult.myreturn = "Success";
            }
            else
            {
                GetActivityResult.myreturn = "No Data Available";
            }

            return GetActivityResult;
        }



        [HttpPost]
        [Route("ApprovedLogs")]
        public string ApprovedLogs(Daily SeriesIDForApproval)
        {
            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@SeriesID", mytype = SqlDbType.NVarChar, Value = SeriesIDForApproval.SeriesID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Split", mytype = SqlDbType.NVarChar, Value = SeriesIDForApproval.ForApproval });
            // DataTable EmployeeDt = new DataTable();
            String ID = Connection.ExecuteScalar("sp_ApprovedLogs_Mobile");

            return ID;

        }

        [HttpPost]
        [Route("ApprovedLogsALL")]
        public string ApprovedLogsALL(Daily ALLForApproval)
        {
            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@MNGRID", mytype = SqlDbType.NVarChar, Value = ALLForApproval.EmpID });
            // DataTable EmployeeDt = new DataTable();
            String ID = Connection.ExecuteScalar("sp_ApprovedLogsALL_Mobile");

            return ID;

        }




        [HttpPost]
        [Route("DisplayBreakTypes")]
        public ListAllReasonBreakTypes DisplayDependents(Login userToSave)
        {
            ListAllReasonBreakTypes GetAllBreakTypes = new ListAllReasonBreakTypes();
            try
            {
                Connection Connection = new Connection();
                List<ListAllBreakReasonTypes> ListDependents = new List<ListAllBreakReasonTypes>();
                DataSet ds = new DataSet();
                DataTable dtLEAVES = new DataTable();

                //Connection.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = userToSave.LoginID });

                ds = Connection.GetDataset("sp_ShowAllbreaktyp");
                dtLEAVES = ds.Tables[0];
                if (dtLEAVES.Rows.Count > 0)
                {
                    foreach (DataRow row in dtLEAVES.Rows)
                    {
                        ListAllBreakReasonTypes AvailableLeaves = new ListAllBreakReasonTypes
                        {


                            ReasonID = row["ReasonID"].ToString(),
                            ReasonDesc = row["ReasonDesc"].ToString()


                        };
                        ListDependents.Add(AvailableLeaves);
                    }
                    GetAllBreakTypes.ListAllResonType = ListDependents;
                    GetAllBreakTypes.myreturn = "Success";
                }
                else
                {
                    GetAllBreakTypes.myreturn = "No Data Available";
                }
            }
            catch (Exception ex)
            {
                GetAllBreakTypes.myreturn = ex.Message;
            }
            return GetAllBreakTypes;
        }


        [HttpPost]
        [Route("AddSegments")]
        public string AddSegments(ListAllActivityByRange addSegment)
        {
            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = addSegment.EmpID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@STARTTIME", mytype = SqlDbType.NVarChar, Value = Convert.ToDateTime(addSegment.StartTIme).ToString("yyyy-MM-dd h:mm tt") });
            Connection.myparameters.Add(new myParameters { ParameterName = "@ENDTIME", mytype = SqlDbType.NVarChar, Value = Convert.ToDateTime(addSegment.StartEnd).ToString("yyyy-MM-dd h:mm tt") });
            Connection.myparameters.Add(new myParameters { ParameterName = "@REASONID", mytype = SqlDbType.NVarChar, Value = Convert.ToInt32(addSegment.ReasonID) });
            Connection.myparameters.Add(new myParameters { ParameterName = "@SCHEDDATE", mytype = SqlDbType.NVarChar, Value = addSegment.SchedDate });

            //  Connection.myparameters.Add(new myParameters { ParameterName = "@STARTOREND", mytype = SqlDbType.NVarChar, Value = uBreak.StartOrEnd });

        // DataTable EmployeeDt = new DataTable();
            String ID = Connection.ExecuteScalar("sp_AddSegment_Mobile");

            return ID;

        }

        [HttpPost]
        [Route("SplitSegments")]

        public string SplitSegments(ListAllActivityByRange splitSegment)
        {
            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = splitSegment.EmpID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@STARTTIME", mytype = SqlDbType.NVarChar, Value = Convert.ToDateTime(splitSegment.StartTIme).ToString("yyyy-MM-dd h:mm tt") });
            Connection.myparameters.Add(new myParameters { ParameterName = "@ENDTIME", mytype = SqlDbType.NVarChar, Value = Convert.ToDateTime(splitSegment.StartEnd).ToString("yyyy-MM-dd h:mm tt") });
            Connection.myparameters.Add(new myParameters { ParameterName = "@REASONID", mytype = SqlDbType.NVarChar, Value = Convert.ToInt32(splitSegment.ReasonID) });
            Connection.myparameters.Add(new myParameters { ParameterName = "@SCHEDDATE", mytype = SqlDbType.NVarChar, Value = splitSegment.SchedDate });
            Connection.myparameters.Add(new myParameters { ParameterName = "@SERIES", mytype = SqlDbType.NVarChar, Value = splitSegment.SeriesID });

            //  Connection.myparameters.Add(new myParameters { ParameterName = "@STARTOREND", mytype = SqlDbType.NVarChar, Value = uBreak.StartOrEnd });

            // DataTable EmployeeDt = new DataTable();
            String ID = Connection.ExecuteScalar("sp_SplitSegment_Mobile");

            return ID;

        }


        [HttpPost]
        [Route("DisplayAllReasons")]
        public ListAllReasonTypeResult DisplayReasons()
        {
            ListAllReasonTypeResult GetAllBreakTypes = new ListAllReasonTypeResult();
            try
            {
                Connection Connection = new Connection();
                List<ListAllReasonType> ListReason = new List<ListAllReasonType>();
                DataSet ds = new DataSet();
                DataTable dtReasons = new DataTable();

                //Connection.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = userToSave.LoginID });

                ds = Connection.GetDataset("sp_GetReason_Mobile");
                dtReasons = ds.Tables[0];
                if (dtReasons.Rows.Count > 0)
                {
                    foreach (DataRow row in dtReasons.Rows)
                    {
                        ListAllReasonType AvailableReasons = new ListAllReasonType
                        {


                            ReasonID = row["ReasonID"].ToString(),
                            ReasonDesc = row["ReasonDesc"].ToString().Replace("\r\n", string.Empty)


                        };
                        ListReason.Add(AvailableReasons);
                    }
                    GetAllBreakTypes.ListAllReasonTypes = ListReason;
                    GetAllBreakTypes.myreturn = "Success";
                }
                else
                {
                    GetAllBreakTypes.myreturn = "No Data Available";
                }
            }
            catch (Exception ex)
            {
                GetAllBreakTypes.myreturn = ex.Message;
            }
            return GetAllBreakTypes;
        }


        [HttpPost]
        [Route("GetVueSchedule")]
        //{
        //"NTID": "i101",
        //"Start_Date": "2017-04-01",
        //"End_Date": "2017-04-30"
        //}
        public ListAllActivityResult GetVueSchedule(DailyParameters DailyParameters)
        {
            ListAllActivityResult GetActivityResult = new ListAllActivityResult();
            //try
            //{
            Connection Connection = new Connection();
            List<ListAllActivityByRange> AllActivity = new List<ListAllActivityByRange>();
            DataTable dtDAILY = new DataTable();
            //BasicFunction BasicFunction = new BasicFunction();
            Connection.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.NVarChar, Value = DailyParameters.NTID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@StartDate", mytype = SqlDbType.NVarChar, Value = Convert.ToDateTime(DailyParameters.Start_Date).ToString("yyyy-MM-dd") });
            Connection.myparameters.Add(new myParameters { ParameterName = "@EndDate", mytype = SqlDbType.NVarChar, Value = Convert.ToDateTime(DailyParameters.End_Date).ToString("yyyy-MM-dd") });
            dtDAILY = Connection.GetDataTable("sp_Vue_GetSchedule");
            //CultureInfo provider = CultureInfo.InvariantCulture;
            if (dtDAILY.Rows.Count > 0)
            {
                foreach (DataRow row in dtDAILY.Rows)
                {
                    ListAllActivityByRange ListAllActivity = new ListAllActivityByRange
                    {
                        Day = row["Day"].ToString(),
                        StartTIme = row["SchedIN"].ToString() == "" ? null : Convert.ToDateTime(row["SchedIN"]).ToString("hh:mm tt"),
                        StartEnd = row["SchedOUT"].ToString() == "" ? null : Convert.ToDateTime(row["SchedOUT"]).ToString("hh:mm tt"),
                        SchedDate = Convert.ToDateTime(row["SchedDate"]).ToString("yyyy-MM-dd"),
                        HourWork = row["HoursWorked"].ToString()
                    };
                    AllActivity.Add(ListAllActivity);
                }
                GetActivityResult.ListAllActivity = AllActivity;
                GetActivityResult.myreturn = "Success";
            }
            else
            {
                GetActivityResult.myreturn = "No Data Available";
            }

            return GetActivityResult;
        }

        [HttpPost]
        [Route("DisplayVueCurrentDayActivity")]
        public GetDailyResult DisplayVueCurrentDayActivity(DailyParameters DailyParameters)
        {
            GetDailyResult GetDailyResult = new GetDailyResult();
            //try
            //{
            Connection Connection = new Connection();
            List<Daily> DailyList = new List<Daily>();
            DataTable dtDAILY = new DataTable();
            //BasicFunction BasicFunction = new BasicFunction();
            Connection.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.NVarChar, Value = DailyParameters.NTID });
            dtDAILY = Connection.GetDataTable("sp_DisplayCurrentDayActivity_Mobile");
            //CultureInfo provider = CultureInfo.InvariantCulture;
            if (dtDAILY.Rows.Count > 0)
            {
                foreach (DataRow row in dtDAILY.Rows)
                {
                    Daily Daily = new Daily
                    {
                        SchedDate = Convert.ToDateTime(row["SchedDate"]).ToString("MMM-dd-yyyy"),
                        //SchedDay = Convert.ToDateTime(row["SchedDate"]).DayOfWeek.ToString().Substring(0, 3),
                        SchedIN = row["Start_Time"].ToString() == "" ? null : Convert.ToDateTime(row["Start_Time"]).ToString("hh:mm tt"),
                        SchedOUT = row["End_Time"].ToString() == "" ? null : Convert.ToDateTime(row["End_Time"]).ToString("hh:mm tt"),
                        //TotalTimeHour = BasicFunction.Decimal102(row["TotalTimeHour"].ToString()),
                        SchedType = row["ReasonDesc"].ToString()
                        //DateFormat = Convert.ToDateTime(row["SchedDate"]).ToString()
                    };
                    DailyList.Add(Daily);
                }
                GetDailyResult.Daily = DailyList;
                GetDailyResult.myreturn = "Success";
            }
            else
            {
                GetDailyResult.myreturn = "No Data Available";
            }
            //}
            //catch
            //{
            //    GetDailyResult.myreturn = "Error Fetching Data";

            //}
            return GetDailyResult;
        }

        [HttpPost]
        [Route("DisplayVueActivitybySchedate")]
        public GetDailyResult DisplayVueActivitybySchedate(Daily DailyParameters)
        {
            GetDailyResult GetDailyResult = new GetDailyResult();
            //try
            //{
            Connection Connection = new Connection();
            List<Daily> DailyList = new List<Daily>();
            DataTable dtDAILY = new DataTable();
            //BasicFunction BasicFunction = new BasicFunction();
            Connection.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.NVarChar, Value = DailyParameters.EmpID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@SchedDate", mytype = SqlDbType.NVarChar, Value = DailyParameters.SchedDate });
            dtDAILY = Connection.GetDataTable("sp_Vue_DisplayActivitybySchedate");
            //CultureInfo provider = CultureInfo.InvariantCulture;
            if (dtDAILY.Rows.Count > 0)
            {
                foreach (DataRow row in dtDAILY.Rows)
                {
                    Daily Daily = new Daily
                    {
                        SchedDate = Convert.ToDateTime(row["SchedDate"]).ToString("MMM-dd-yyyy"),
                        //SchedDay = Convert.ToDateTime(row["SchedDate"]).DayOfWeek.ToString().Substring(0, 3),
                        SchedIN = row["Start_Time"].ToString() == "" ? null : Convert.ToDateTime(row["Start_Time"]).ToString("hh:mm tt"),
                        SchedOUT = row["End_Time"].ToString() == "" ? null : Convert.ToDateTime(row["End_Time"]).ToString("hh:mm tt"),
                        //TotalTimeHour = BasicFunction.Decimal102(row["TotalTimeHour"].ToString()),
                        SchedType = row["ReasonDesc"].ToString()
                        //DateFormat = Convert.ToDateTime(row["SchedDate"]).ToString()
                    };
                    DailyList.Add(Daily);
                }
                GetDailyResult.Daily = DailyList;
                GetDailyResult.myreturn = "Success";
            }
            else
            {
                GetDailyResult.myreturn = "No Data Available";
            }
            //}
            //catch
            //{
            //    GetDailyResult.myreturn = "Error Fetching Data";

            //}
            return GetDailyResult;
        }

        [HttpPost]
        [Route("ApprovedWorkedHours")]
        public string ApprovedWorkedHours(Daily WorkHours)
        {
            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = WorkHours.EmpID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@SCHEDATE", mytype = SqlDbType.NVarChar, Value = WorkHours.SchedDate });


            // DataTable EmployeeDt = new DataTable();
            String ID = Connection.ExecuteScalar("sp_ApprovedWorkHours_Mobile");

            return "success";

            //return ID;

        }


        [HttpPost]
        [Route("GetSchedule")]
        //{
        //"NTID": "i101",
        //"Start_Date": "2017-04-01",
        //"End_Date": "2017-04-30"
        //}
        public ListAllSchedulebyRangeResult GetSchedule(DailyParameters Schedulelist)
        {
            ListAllSchedulebyRangeResult GetScheduleResult = new ListAllSchedulebyRangeResult();
            //try
            //{
            Connection Connection = new Connection();
            List<ListAllSchedulebyRange> AllSchedule = new List<ListAllSchedulebyRange>();
            DataTable dtsched = new DataTable();
            //BasicFunction BasicFunction = new BasicFunction();
            Connection.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = Schedulelist.NTID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@StartDate", mytype = SqlDbType.NVarChar, Value = Convert.ToDateTime(Schedulelist.Start_Date).ToString("yyyy-MM-dd") });
            Connection.myparameters.Add(new myParameters { ParameterName = "@EndDate", mytype = SqlDbType.NVarChar, Value = Convert.ToDateTime(Schedulelist.End_Date).ToString("yyyy-MM-dd") });
            dtsched = Connection.GetDataTable("sp_GetSchedule");
            //CultureInfo provider = CultureInfo.InvariantCulture;
            if (dtsched.Rows.Count > 0)
            {
                foreach (DataRow row in dtsched.Rows)
                {
                    ListAllSchedulebyRange ListAllSchedule = new ListAllSchedulebyRange
                    {
                        EmpID = row["EmpID"].ToString(),
                        Start_Time = row["SchedIN"].ToString() == "" ? null : Convert.ToDateTime(row["SchedIN"]).ToString("hh:mm tt"),
                        End_Time = row["SchedOUT"].ToString() == "" ? null : Convert.ToDateTime(row["SchedOUT"]).ToString("hh:mm tt"),
                        SchedDate = Convert.ToDateTime(row["SchedDate"]).ToString("yyyy-MM-dd")

                    };
                    AllSchedule.Add(ListAllSchedule);
                }
                GetScheduleResult.ListAllSchedule = AllSchedule;
                GetScheduleResult.myreturn = "Success";
            }
            else
            {
                GetScheduleResult.myreturn = "No Data Available";
            }

            return GetScheduleResult;
        }


        [HttpPost]
        [Route("GetAllSwapRequest")]
        public ListAllSwapRequesteResult GetAllSwapRequest()
        {
            ListAllSwapRequesteResult GetAllSwap = new ListAllSwapRequesteResult();
            try
            {
                Connection Connection = new Connection();
                List<ListAllSwapRequest> ListAllSwap = new List<ListAllSwapRequest>();
                DataSet ds = new DataSet();
                DataTable dtSwapreq = new DataTable();

                //Connection.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = userToSave.LoginID });

                ds = Connection.GetDataset("sp_GetAllSwapRequest_Mobile");
                dtSwapreq = ds.Tables[0];
                if (dtSwapreq.Rows.Count > 0)
                {
                    foreach (DataRow row in dtSwapreq.Rows)
                    {
                        ListAllSwapRequest SwapRequest = new ListAllSwapRequest
                        {


                            EMPID = row["EmpID"].ToString(),
                            Schedule = row["ScheduleDate"].ToString(),
                            SwapType = row["SchedType"].ToString()
                            


                        };
                        ListAllSwap.Add(SwapRequest);
                    }
                    GetAllSwap.ListAllSwapRequests = ListAllSwap;
                    GetAllSwap.myreturn = "Success";
                }
                else
                {
                    GetAllSwap.myreturn = "No Data Available";
                }
            }
            catch (Exception ex)
            {
                GetAllSwap.myreturn = ex.Message;
            }
            return GetAllSwap;
        }

        [HttpPost]
        [Route("DisplayActivityByRangeAll")]
        //{
        //"NTID": "i101",
        //"Start_Date": "2017-04-01",
        //"End_Date": "2017-04-30"
        //}
        public ListAllActivityResult DisplayActivityByRangeAll()
        {
            ListAllActivityResult GetActivityResult = new ListAllActivityResult();
            //try
            //{

            Connection Connection = new Connection();
            List<ListAllActivityByRange> AllActivity = new List<ListAllActivityByRange>();
            DataTable dtDAILY = new DataTable();
            //BasicFunction BasicFunction = new BasicFunction();
            //Connection.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.NVarChar, Value = DailyParameters.NTID });
            //Connection.myparameters.Add(new myParameters { ParameterName = "@SchedDate1", mytype = SqlDbType.NVarChar, Value = DailyParameters.Start_Date });
            //Connection.myparameters.Add(new myParameters { ParameterName = "@SchedDate2", mytype = SqlDbType.NVarChar, Value = DailyParameters.End_Date });
            dtDAILY = Connection.GetDataTable("sp_GetEmployeeActivity_Mobile_All");
            //CultureInfo provider = CultureInfo.InvariantCulture;
            if (dtDAILY.Rows.Count > 0)
            {
                foreach (DataRow row in dtDAILY.Rows)
                {
                    ListAllActivityByRange ListAllActivity = new ListAllActivityByRange
                    {
                        EmpID = row["EMPID"].ToString(),
                        StartTIme = row["Start_Time"].ToString(),
                        StartEnd = row["End_Time"].ToString(),
                        ReasonID = row["ReasonID"].ToString(),
                        ReasonDesc = row["ReasonDesc"].ToString(),
                        IsPaid = row["IsPaid"].ToString(),
                        SchedDate = row["SchedDate"].ToString(),
                        ActivityStatus = row["Activity_Status"].ToString(),
                        HourWork = row["TotalTimeMin"].ToString(),
                        SeriesID = row["SeriesID"].ToString(),
                        SchedDateStatus = row["Activity_Tag"].ToString() + "/" + row["ApprovedWork"].ToString()
                    };
                    AllActivity.Add(ListAllActivity);
                }
                GetActivityResult.ListAllActivity = AllActivity;
                GetActivityResult.myreturn = "Success";
            }
            else
            {
                GetActivityResult.myreturn = "No Data Available";
            }

            return GetActivityResult;
        }

        [HttpPost]
        [Route("GetLastActivity")]
        //{
        //"NTID": "i101",
        //"Start_Date": "2017-04-01",
        //"End_Date": "2017-04-30"
        //}
        public ListLastActivityResult GetLastActivity(employee emp)
        {
            ListLastActivityResult GetActivityResult = new ListLastActivityResult();
            //try
            //{

            Connection Connection = new Connection();
            List<LastActivity> AllActivity = new List<LastActivity>();
            DataTable dtDAILY = new DataTable();
            //BasicFunction BasicFunction = new BasicFunction();
            Connection.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.NVarChar, Value = emp.empid });
            dtDAILY = Connection.GetDataTable("sp_getLastBreakID");
            //CultureInfo provider = CultureInfo.InvariantCulture;
            if (dtDAILY.Rows.Count > 0)
            {
                foreach (DataRow row in dtDAILY.Rows)
                {
                    LastActivity ListAllActivity = new LastActivity
                    {
                        LastReasonID = row["ReasonID"].ToString(),
                        LastType = row["Category"].ToString()
                    };
                    AllActivity.Add(ListAllActivity);
                }
                GetActivityResult.ListLastActivity = AllActivity;
                GetActivityResult.myreturn = "Success";
            }
            else
            {
                GetActivityResult.myreturn = "No Data Available";
            }

            return GetActivityResult;
        }
        

        //---------------------------------------------------------------Revised Change DB Start------------------------------------------------------------------------

        [HttpPost]
        [Route("GetLastActivityv2")]
        public ListLastActivityResult GetLastActivityv2(employeev2 emp)
        {
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = emp.CN;
            SelfieRegistration2Controller.GetDB2(GDB);

            ListLastActivityResult GetActivityResult = new ListLastActivityResult();
            Connection Connection = new Connection();
            List<LastActivity> AllActivity = new List<LastActivity>();
            DataTable dtDAILY = new DataTable();
            //BasicFunction BasicFunction = new BasicFunction();
            Connection.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.NVarChar, Value = emp.empid });
            dtDAILY = Connection.GetDataTable("sp_getLastBreakID");
            //CultureInfo provider = CultureInfo.InvariantCulture;
            if (dtDAILY.Rows.Count > 0)
            {
                foreach (DataRow row in dtDAILY.Rows)
                {
                    LastActivity ListAllActivity = new LastActivity
                    {
                        LastReasonID = row["ReasonID"].ToString(),
                        LastType = row["Category"].ToString()
                    };
                    AllActivity.Add(ListAllActivity);
                }
                GetActivityResult.ListLastActivity = AllActivity;
                GetActivityResult.myreturn = "Success";
            }
            else
            {
                GetActivityResult.myreturn = "No Data Available";
            }

            return GetActivityResult;
        }

        [HttpPost]
        [Route("DisplayActivityByRangeAlternativev2")]
        public ListAllActivityResult DisplayActivityByRangeAlternativev2(DailyParametersv2 DailyParameters)
        {
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = DailyParameters.CN;
            SelfieRegistration2Controller.GetDB2(GDB);

            ListAllActivityResult GetActivityResult = new ListAllActivityResult();
            //try
            //{
            Connection Connection = new Connection();
            List<ListAllActivityByRange> AllActivity = new List<ListAllActivityByRange>();
            DataTable dtDAILY = new DataTable();
            //BasicFunction BasicFunction = new BasicFunction();
            Connection.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.NVarChar, Value = DailyParameters.NTID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@SchedDate1", mytype = SqlDbType.NVarChar, Value = DailyParameters.Start_Date });
            Connection.myparameters.Add(new myParameters { ParameterName = "@SchedDate2", mytype = SqlDbType.NVarChar, Value = DailyParameters.End_Date });
            dtDAILY = Connection.GetDataTable("sp_GetEmployeeActivity_Mobile_Alternative");
            //CultureInfo provider = CultureInfo.InvariantCulture;
            if (dtDAILY.Rows.Count > 0)
            {
                foreach (DataRow row in dtDAILY.Rows)
                {
                    ListAllActivityByRange ListAllActivity = new ListAllActivityByRange
                    {
                        EmpID = row["EMPID"].ToString(),
                        StartTIme = row["Schedule"].ToString() == "LEAVE" || row["Schedule"].ToString() == "RD" ? row["Schedule"].ToString() : row["Start_Time"].ToString(),
                        StartEnd = row["Schedule"].ToString() == "LEAVE" || row["Schedule"].ToString() == "RD" ? row["Schedule"].ToString() : row["End_Time"].ToString(),
                        //StartTIme = row["Start_Time"].ToString(),
                        //StartEnd = row["End_Time"].ToString(),
                        ReasonID = row["ReasonID"].ToString(),
                        ReasonDesc = row["ReasonDesc"].ToString(),
                        IsPaid = row["IsPaid"].ToString(),
                        SchedDate = row["SchedDate"].ToString(),
                        ActivityStatus = row["Activity_Status"].ToString(),
                        HourWork = row["TotalTimeMin"].ToString(),
                        SeriesID = row["SeriesID"].ToString(),
                        //SchedDateStatus = row["Activity_Tag"].ToString() + "/" + row["ApprovedWork"].ToString()
                        SchedDateStatus = row["ApprovedWork"].ToString()
                    };
                    AllActivity.Add(ListAllActivity);
                }
                GetActivityResult.ListAllActivity = AllActivity;
                GetActivityResult.myreturn = "Success";
            }
            else
            {
                GetActivityResult.myreturn = "No Data Available";
            }

            return GetActivityResult;
        }


        [HttpPost]
        [Route("DisplayBreakTypesv2")]
        public ListAllReasonBreakTypes DisplayDependentsv2(Loginv2 userToSave)
        {
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = userToSave.CN;
            SelfieRegistration2Controller.GetDB2(GDB);

            ListAllReasonBreakTypes GetAllBreakTypes = new ListAllReasonBreakTypes();
            try
            {
                Connection Connection = new Connection();
                List<ListAllBreakReasonTypes> ListDependents = new List<ListAllBreakReasonTypes>();
                DataSet ds = new DataSet();
                DataTable dtLEAVES = new DataTable();

                //Connection.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = userToSave.LoginID });

                ds = Connection.GetDataset("sp_ShowAllbreaktyp");
                dtLEAVES = ds.Tables[0];
                if (dtLEAVES.Rows.Count > 0)
                {
                    foreach (DataRow row in dtLEAVES.Rows)
                    {
                        ListAllBreakReasonTypes AvailableLeaves = new ListAllBreakReasonTypes
                        {


                            ReasonID = row["ReasonID"].ToString(),
                            ReasonDesc = row["ReasonDesc"].ToString()


                        };
                        ListDependents.Add(AvailableLeaves);
                    }
                    GetAllBreakTypes.ListAllResonType = ListDependents;
                    GetAllBreakTypes.myreturn = "Success";
                }
                else
                {
                    GetAllBreakTypes.myreturn = "No Data Available";
                }
            }
            catch (Exception ex)
            {
                GetAllBreakTypes.myreturn = ex.Message;
            }
            return GetAllBreakTypes;
        }


        [HttpPost]
        [Route("UserBreaksv2")]
        public string UserBreakv2(ForBreaks uBreak)
        {

            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = uBreak.CN;
            SelfieRegistration2Controller.GetDB2(GDB);

            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.NVarChar, Value = uBreak.NTID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@ReasonID", mytype = SqlDbType.NVarChar, Value = uBreak.ReasonID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Coordinates", mytype = SqlDbType.NVarChar, Value = uBreak.Coordinates });
            Connection.myparameters.Add(new myParameters { ParameterName = "@NearestEstablishment", mytype = SqlDbType.NVarChar, Value = uBreak.NearestEstablishment });
            //  Connection.myparameters.Add(new myParameters { ParameterName = "@STARTOREND", mytype = SqlDbType.NVarChar, Value = uBreak.StartOrEnd });

            // DataTable EmployeeDt = new DataTable();
            String ID = Connection.ExecuteScalar("sp_InsertToActivityTrackerBreaks_Mobile");

            return ID;

        }


        [HttpPost]
        [Route("TimeINDisplayv2")]
        public GetDailyResult ShowDailyRecordv2(DailyParametersv2 DailyParameters)
        {
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = DailyParameters.CN;
            SelfieRegistration2Controller.GetDB2(GDB);

            GetDailyResult GetDailyResult = new GetDailyResult();
            Connection Connection = new Connection();
            List<Daily> DailyList = new List<Daily>();
            DataTable dtDAILY = new DataTable();
            //BasicFunction BasicFunction = new BasicFunction();
            Connection.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.NVarChar, Value = DailyParameters.NTID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Coordinates", mytype = SqlDbType.NVarChar, Value = DailyParameters.Coordinates });
            Connection.myparameters.Add(new myParameters { ParameterName = "@NearestEstablishment", mytype = SqlDbType.NVarChar, Value = DailyParameters.NearestEstablishment });
            dtDAILY = Connection.GetDataTable("sp_TimeIn_Mobile");
            if (dtDAILY.Rows.Count > 0)
            {
                foreach (DataRow row in dtDAILY.Rows)
                {
                    Daily Daily = new Daily
                    {
                        SchedDate = Convert.ToDateTime(row["SchedDate"]).ToString("MMM-dd-yyyy"),
                        SchedIN = row["Start_Time"].ToString(),
                        SchedOUT = row["End_Time"].ToString(),
                        SchedType = row["ReasonDesc"].ToString().Replace("\r\n", string.Empty)
                    };
                    DailyList.Add(Daily);
                }
                GetDailyResult.Daily = DailyList;
                GetDailyResult.myreturn = "Success";
            }
            else
            {
                GetDailyResult.myreturn = "No Data Available";
            }
            return GetDailyResult;
        }


        [HttpPost]
        [Route("TimeOutDisplayv2")]
        public string TimeOutAndDisplayv2(DailyParametersv2 DailyParameters)
        {
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = DailyParameters.CN;
            SelfieRegistration2Controller.GetDB2(GDB);

            Connection Connection = new Connection();
            Connection.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.NVarChar, Value = DailyParameters.NTID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Coordinates", mytype = SqlDbType.NVarChar, Value = DailyParameters.Coordinates });
            Connection.myparameters.Add(new myParameters { ParameterName = "@NearestEstablishment", mytype = SqlDbType.NVarChar, Value = DailyParameters.NearestEstablishment });
            String ID = Connection.ExecuteScalar("sp_TimeOut_Mobile");

            return ID;
        }


        [HttpPost]
        [Route("DisplayAllReasonsv2")]
        public ListAllReasonTypeResult DisplayReasonsv2(Loginv2 log)
        {
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = log.CN;
            SelfieRegistration2Controller.GetDB2(GDB);

            ListAllReasonTypeResult GetAllBreakTypes = new ListAllReasonTypeResult();
            try
            {
                Connection Connection = new Connection();
                List<ListAllReasonType> ListReason = new List<ListAllReasonType>();
                DataSet ds = new DataSet();
                DataTable dtReasons = new DataTable();

                //Connection.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = userToSave.LoginID });

                ds = Connection.GetDataset("sp_GetReason_Mobile");
                dtReasons = ds.Tables[0];
                if (dtReasons.Rows.Count > 0)
                {
                    foreach (DataRow row in dtReasons.Rows)
                    {
                        ListAllReasonType AvailableReasons = new ListAllReasonType
                        {


                            ReasonID = row["ReasonID"].ToString(),
                            ReasonDesc = row["ReasonDesc"].ToString().Replace("\r\n", string.Empty)


                        };
                        ListReason.Add(AvailableReasons);
                    }
                    GetAllBreakTypes.ListAllReasonTypes = ListReason;
                    GetAllBreakTypes.myreturn = "Success";
                }
                else
                {
                    GetAllBreakTypes.myreturn = "No Data Available";
                }
            }
            catch (Exception ex)
            {
                GetAllBreakTypes.myreturn = ex.Message;
            }
            return GetAllBreakTypes;
        }


        [HttpPost]
        [Route("GetTotalHoursWorkMobilev2")]
        public ListAllHoursworked GetTotalHoursWorkMobilev2(DailyParametersv2 DailyParameters)
        {
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = DailyParameters.CN;
            SelfieRegistration2Controller.GetDB2(GDB);

            ListAllHoursworked GetActivityResult = new ListAllHoursworked();
            Connection Connection = new Connection();
            List<Hoursworked> AllActivity = new List<Hoursworked>();
            DataTable dtDAILY = new DataTable();
            Connection.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.NVarChar, Value = DailyParameters.NTID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@StartDate", mytype = SqlDbType.NVarChar, Value = Convert.ToDateTime(DailyParameters.Start_Date).ToString("yyyy-MM-dd") });
            Connection.myparameters.Add(new myParameters { ParameterName = "@EndDate", mytype = SqlDbType.NVarChar, Value = Convert.ToDateTime(DailyParameters.End_Date).ToString("yyyy-MM-dd") });
            dtDAILY = Connection.GetDataTable("sp_GetEmpTotalWorkHrs_Mobile1");
            if (dtDAILY.Rows.Count > 0)
            {
                foreach (DataRow row in dtDAILY.Rows)
                {
                    Hoursworked ListAllActivity = new Hoursworked
                    {
                        EmpID = row["EmpID"].ToString(),
                        SchedDate = row["SchedDate"].ToString(),
                        HourWork = row["HoursWorked"].ToString(),
                        Day = row["Day"].ToString(),
                        Schedule = row["Schedule"].ToString(),
                        TotalTime = row["TotalTime"].ToString()
                    };
                    AllActivity.Add(ListAllActivity);
                }
                GetActivityResult.ListAllActivity = AllActivity;
                GetActivityResult.myreturn = "Success";
            }
            else
            {
                GetActivityResult.myreturn = "No Data Available";
            }
            return GetActivityResult;
        }

        [HttpPost]
        [Route("AddSegmentsv2")]
        public string AddSegmentsv2(ListAllActivityByRangev2 addSegment)
        {
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = addSegment.CN;
            SelfieRegistration2Controller.GetDB2(GDB);

            Connection Connection = new Connection();
            Connection.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = addSegment.EmpID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@STARTTIME", mytype = SqlDbType.NVarChar, Value = Convert.ToDateTime(addSegment.StartTIme).ToString("yyyy-MM-dd h:mm tt") });
            Connection.myparameters.Add(new myParameters { ParameterName = "@ENDTIME", mytype = SqlDbType.NVarChar, Value = Convert.ToDateTime(addSegment.StartEnd).ToString("yyyy-MM-dd h:mm tt") });
            Connection.myparameters.Add(new myParameters { ParameterName = "@REASONID", mytype = SqlDbType.NVarChar, Value = Convert.ToInt32(addSegment.ReasonID) });
            Connection.myparameters.Add(new myParameters { ParameterName = "@SCHEDDATE", mytype = SqlDbType.NVarChar, Value = addSegment.SchedDate });
            String ID = Connection.ExecuteScalar("sp_AddSegment_Mobile");
            return ID;
        }

        [HttpPost]
        [Route("AddSegmentsv3")]
        public string AddSegmentsv3(ListAllActivityByRangev2 addSegment)
        {
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = addSegment.CN;
            SelfieRegistration2Controller.GetDB2(GDB);
            var time = DateTime.Now;
            string formattedTime = time.ToString("yyyy-MM-dd-hh-mm-ss-fff");
            Connection Connection = new Connection();
            Connection.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = addSegment.EmpID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@STARTTIME", mytype = SqlDbType.NVarChar, Value = Convert.ToDateTime(addSegment.StartTIme).ToString("yyyy-MM-dd h:mm tt") });
            Connection.myparameters.Add(new myParameters { ParameterName = "@ENDTIME", mytype = SqlDbType.NVarChar, Value = Convert.ToDateTime(addSegment.StartEnd).ToString("yyyy-MM-dd h:mm tt") });
            Connection.myparameters.Add(new myParameters { ParameterName = "@REASONID", mytype = SqlDbType.NVarChar, Value = Convert.ToInt32(addSegment.ReasonID) });
            Connection.myparameters.Add(new myParameters { ParameterName = "@SCHEDDATE", mytype = SqlDbType.NVarChar, Value = addSegment.SchedDate });
            Connection.myparameters.Add(new myParameters { ParameterName = "@EMPCOMMENT", mytype = SqlDbType.NVarChar, Value = addSegment.EMPCOMMENT });
            Connection.myparameters.Add(new myParameters { ParameterName = "@APPROVERCOMMENT", mytype = SqlDbType.NVarChar, Value = addSegment.APPROVERCOMMENT });
            Connection.myparameters.Add(new myParameters { ParameterName = "@FILEPATH", mytype = SqlDbType.NVarChar, Value = formattedTime});
            Connection.myparameters.Add(new myParameters { ParameterName = "@CN", mytype = SqlDbType.NVarChar, Value = addSegment.CN.ToLower() });
            Connection.ExecuteNonQuery("sp_AddSegment_Mobilev2");

            byte[] PIC = Convert.FromBase64String(addSegment.FILEPATH);

            var path = HttpContext.Current.Server.MapPath("~/ProfilePhotos/" + addSegment.CN.ToLower() + "/" + "");
            var folder = Path.Combine(path, addSegment.EmpID);
            Directory.CreateDirectory(folder);
            MemoryStream stream = new MemoryStream();
            using (stream)
            {

                File.WriteAllBytes(folder + "/" + addSegment.EmpID + "_" + formattedTime + ".png", PIC);

            }
            return "Success";
        }
        [HttpPost]
        [Route("AddSegmentsv4")]
        public string AddSegmentsv4(ListAllActivityByRangev2 addSegment)
        {
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = addSegment.CN;
            SelfieRegistration2Controller.GetDB2(GDB);
            var time = DateTime.Now;
            string formattedTime = time.ToString("yyyy-MM-dd-hh-mm-ss");
            Connection Connection = new Connection();
            Connection.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = addSegment.EmpID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@STARTTIME", mytype = SqlDbType.NVarChar, Value = Convert.ToDateTime(addSegment.StartTIme).ToString("yyyy-MM-dd h:mm tt") });
            Connection.myparameters.Add(new myParameters { ParameterName = "@ENDTIME", mytype = SqlDbType.NVarChar, Value = Convert.ToDateTime(addSegment.StartEnd).ToString("yyyy-MM-dd h:mm tt") });
            Connection.myparameters.Add(new myParameters { ParameterName = "@REASONID", mytype = SqlDbType.NVarChar, Value = Convert.ToInt32(addSegment.ReasonID) });
            Connection.myparameters.Add(new myParameters { ParameterName = "@SCHEDDATE", mytype = SqlDbType.NVarChar, Value = addSegment.SchedDate });
            Connection.myparameters.Add(new myParameters { ParameterName = "@EMPCOMMENT", mytype = SqlDbType.NVarChar, Value = addSegment.EMPCOMMENT });
            Connection.myparameters.Add(new myParameters { ParameterName = "@APPROVERCOMMENT", mytype = SqlDbType.NVarChar, Value = addSegment.APPROVERCOMMENT });
            Connection.myparameters.Add(new myParameters { ParameterName = "@FILEPATH", mytype = SqlDbType.NVarChar, Value = formattedTime });
            Connection.myparameters.Add(new myParameters { ParameterName = "@CN", mytype = SqlDbType.NVarChar, Value = addSegment.CN.ToLower() });
            Connection.myparameters.Add(new myParameters { ParameterName = "@COORDINATES", mytype = SqlDbType.NVarChar, Value = addSegment.Coordinates.ToLower() });
            Connection.myparameters.Add(new myParameters { ParameterName = "@NEARESTESTABLISHMENT", mytype = SqlDbType.NVarChar, Value = addSegment.NearestEstablishment.ToLower() });
            Connection.ExecuteNonQuery("sp_AddSegment_Mobilev3");

            byte[] PIC = Convert.FromBase64String(addSegment.FILEPATH);

            var path = HttpContext.Current.Server.MapPath("~/ProfilePhotos/" + addSegment.CN.ToLower() + "/" + "");
            var folder = Path.Combine(path, addSegment.EmpID);
            Directory.CreateDirectory(folder);
            MemoryStream stream = new MemoryStream();
            using (stream)
            {

                File.WriteAllBytes(folder + "/" + addSegment.EmpID + "_" + formattedTime + ".png", PIC);

            }
            return "Success";
        }


        [HttpPost]
        [Route("ApprovedWorkedHoursv2")]
        public string ApprovedWorkedHoursv2(Dailyv2 WorkHours)
        {
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = WorkHours.CN;
            SelfieRegistration2Controller.GetDB2(GDB);

            Connection Connection = new Connection();
            Connection.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = WorkHours.EmpID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@SCHEDATE", mytype = SqlDbType.NVarChar, Value = WorkHours.SchedDate });
            String ID = Connection.ExecuteScalar("sp_ApprovedWorkHours_Mobile");
            return "success";
        }

        [HttpPost]
        [Route("SplitSegmentsv2")]

        public string SplitSegmentsv2(ListAllActivityByRangev2 splitSegment)
        {
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = splitSegment.CN;
            SelfieRegistration2Controller.GetDB2(GDB);

            Connection Connection = new Connection();
            Connection.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = splitSegment.EmpID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@STARTTIME", mytype = SqlDbType.NVarChar, Value = Convert.ToDateTime(splitSegment.StartTIme).ToString("yyyy-MM-dd h:mm tt") });
            Connection.myparameters.Add(new myParameters { ParameterName = "@ENDTIME", mytype = SqlDbType.NVarChar, Value = Convert.ToDateTime(splitSegment.StartEnd).ToString("yyyy-MM-dd h:mm tt") });
            Connection.myparameters.Add(new myParameters { ParameterName = "@REASONID", mytype = SqlDbType.NVarChar, Value = Convert.ToInt32(splitSegment.ReasonID) });
            Connection.myparameters.Add(new myParameters { ParameterName = "@SCHEDDATE", mytype = SqlDbType.NVarChar, Value = splitSegment.SchedDate });
            Connection.myparameters.Add(new myParameters { ParameterName = "@SERIES", mytype = SqlDbType.NVarChar, Value = splitSegment.SeriesID });
            String ID = Connection.ExecuteScalar("sp_SplitSegment_Mobile");
            return ID;
        }

        [HttpPost]
        [Route("DisplayActivityByRangev2")]
        public ListAllActivityResult DisplayActivityByRangev2(DailyParametersv2 DailyParameters)
        {
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = DailyParameters.CN;
            SelfieRegistration2Controller.GetDB2(GDB);

            ListAllActivityResult GetActivityResult = new ListAllActivityResult();
            //try
            //{
            Connection Connection = new Connection();
            List<ListAllActivityByRange> AllActivity = new List<ListAllActivityByRange>();
            DataTable dtDAILY = new DataTable();
            //BasicFunction BasicFunction = new BasicFunction();
            Connection.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.NVarChar, Value = DailyParameters.NTID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@SchedDate1", mytype = SqlDbType.NVarChar, Value = DailyParameters.Start_Date });
            Connection.myparameters.Add(new myParameters { ParameterName = "@SchedDate2", mytype = SqlDbType.NVarChar, Value = DailyParameters.End_Date });
            dtDAILY = Connection.GetDataTable("sp_GetEmployeeActivity_Mobile");
            //CultureInfo provider = CultureInfo.InvariantCulture;
            if (dtDAILY.Rows.Count > 0)
            {
                foreach (DataRow row in dtDAILY.Rows)
                {
                    ListAllActivityByRange ListAllActivity = new ListAllActivityByRange
                    {
                        EmpID = row["EMPID"].ToString(),
                        //StartTIme = row["Schedule"].ToString() == "LEAVE" || row["Schedule"].ToString() == "RD" ? row["Schedule"].ToString() : row["Start_Time"].ToString(),
                        //StartEnd = row["Schedule"].ToString() == "LEAVE" || row["Schedule"].ToString() == "RD" ? row["Schedule"].ToString() : row["End_Time"].ToString(),
                        StartTIme = row["Start_Time"].ToString(),
                        StartEnd = row["End_Time"].ToString(),
                        ReasonID = row["ReasonID"].ToString(),
                        ReasonDesc = row["ReasonDesc"].ToString(),
                        IsPaid = row["IsPaid"].ToString(),
                        SchedDate = row["SchedDate"].ToString(),
                        ActivityStatus = row["Activity_Status"].ToString(),
                        HourWork = row["TotalTimeMin"].ToString(),
                        SeriesID = row["SeriesID"].ToString(),
                        //SchedDateStatus = row["Activity_Tag"].ToString() + "/" + row["ApprovedWork"].ToString()
                        SchedDateStatus = row["ApprovedWork"].ToString()
                    };
                    AllActivity.Add(ListAllActivity);
                }
                GetActivityResult.ListAllActivity = AllActivity;
                GetActivityResult.myreturn = "Success";
            }
            else
            {
                GetActivityResult.myreturn = "No Data Available";
            }

            return GetActivityResult;
        }

        [HttpPost]
        [Route("UpdateFaceIDv2")]
        public string UpdateFaceIDv2(UpadateFaceIDv2 FaceIDUpdate)
        {

            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = FaceIDUpdate.CN;
            SelfieRegistration2Controller.GetDB2(GDB);

            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@NTID", mytype = SqlDbType.NVarChar, Value = FaceIDUpdate.NTID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@FaceID", mytype = SqlDbType.NVarChar, Value = FaceIDUpdate.FaceID });


            // DataTable EmployeeDt = new DataTable();
            String ID = Connection.ExecuteScalar("sp_UpdateEmployeeMasterFaceID_Mobile");

            return ID;

        }

        [HttpPost]
        [Route("ShowHolidaysv2")]
        public GetHolidayResult GetAllHolidaysv2(Dailyv2 dly)
        {
            GetHolidayResult GetHolidayResult = new GetHolidayResult();
            List<Holiday> Holidays = new List<Holiday>();
            try
            {
                GetDatabase GDB = new GetDatabase();
                GDB.ClientName = dly.CN;
                SelfieRegistration2Controller.GetDB2(GDB);

                Connection Connection = new Connection();
                DataTable dtHolidays = new DataTable();
                Connection.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.NVarChar, Value = dly.EmpID });
                dtHolidays = Connection.GetDataTable("sp_ShowHolidays");
                // CultureInfo provider = CultureInfo.InvariantCulture;
                foreach (DataRow row in dtHolidays.Rows)
                {
                    Holiday Holiday = new Holiday
                    {
                        HolidayName = row["HolDesc"].ToString(),
                        Type = row["HolStatus"].ToString(),
                        HolidayDate = Convert.ToDateTime(row["HolDate"]).ToString("MMM-dd-yyyy")
                    };

                    Holidays.Add(Holiday);
                    GetHolidayResult.Holidays = Holidays;
                    GetHolidayResult.myreturn = "Success";
                }
                return GetHolidayResult;
            }
            catch
            {
                GetHolidayResult.myreturn = "Error";
                return GetHolidayResult;
            }
        }

        [HttpPost]
        [Route("ShowHolidaysByRange")]
        public GetHolidayResult ShowHolidaysByRange(DailyParameters dailyParameters)
        {
            GetHolidayResult GetHolidayResult = new GetHolidayResult();
            List<Holiday> Holidays = new List<Holiday>();
            try
            {
                GetDatabase GDB = new GetDatabase();
                GDB.ClientName = dailyParameters.CN;
                SelfieRegistration2Controller.GetDB2(GDB);

                Connection Connection = new Connection();
                DataTable dtHolidays = new DataTable();
                Connection.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.NVarChar, Value = dailyParameters.NTID });
                Connection.myparameters.Add(new myParameters { ParameterName = "@DateFrom", mytype = SqlDbType.NVarChar, Value = dailyParameters.Start_Date });
                Connection.myparameters.Add(new myParameters { ParameterName = "@DateTo", mytype = SqlDbType.NVarChar, Value = dailyParameters.End_Date });
                dtHolidays = Connection.GetDataTable("sp_ShowHolidaysRange");
                // CultureInfo provider = CultureInfo.InvariantCulture;
                foreach (DataRow row in dtHolidays.Rows)
                {
                    Holiday Holiday = new Holiday
                    {
                        HolidayName = row["HolDesc"].ToString(),
                        Type = row["HolStatus"].ToString(),
                        HolidayDate = Convert.ToDateTime(row["HolDate"]).ToString("MMM-dd-yyyy")
                    };

                    Holidays.Add(Holiday);
                    GetHolidayResult.Holidays = Holidays;
                    GetHolidayResult.myreturn = "Success";
                }
                return GetHolidayResult;
            }
            catch
            {
                GetHolidayResult.myreturn = "Error";
                return GetHolidayResult;
            }
        }

        [HttpPost]
        [Route("TimeINDisplayv3")]
        public GetDailyResult ShowDailyRecordv3(DailyParametersv3 DailyParameters)
        {
            //GetDatabase GDB = new GetDatabase();
            //GDB.ClientName = DailyParameters.CN;
            //SelfieRegistration2Controller.GetDB2(GDB);

            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = DailyParameters.CN;
            SelfieRegistration2Controller.GetDB2(GDB);


            GetDailyResult GetDailyResult = new GetDailyResult();
            Connection Connection = new Connection();
            List<Daily> DailyList = new List<Daily>();
            DataTable dtDAILY = new DataTable();
            //Connection.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.NVarChar, Value = DailyParameters.NTID });
            //Connection.myparameters.Add(new myParameters { ParameterName = "@Coordinates", mytype = SqlDbType.NVarChar, Value = DailyParameters.Coordinates });
            //Connection.myparameters.Add(new myParameters { ParameterName = "@NearestEstablishment", mytype = SqlDbType.NVarChar, Value = DailyParameters.NearestEstablishment });
            //Connection.myparameters.Add(new myParameters { ParameterName = "@NearestEstablishment", mytype = SqlDbType.NVarChar, Value = DailyParameters.IpLocation });
            //Connection.myparameters.Add(new myParameters { ParameterName = "@NearestEstablishment", mytype = SqlDbType.NVarChar, Value = DailyParameters.Status });
            //Connection.myparameters.Add(new myParameters { ParameterName = "@NearestEstablishment", mytype = SqlDbType.Bit, Value = DailyParameters.RequestEarly });
            Connection.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = DailyParameters.NTID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@IPLOCATION", mytype = SqlDbType.NVarChar, Value = DailyParameters.IpLocation });
            Connection.myparameters.Add(new myParameters { ParameterName = "@COORDINATES", mytype = SqlDbType.NVarChar, Value = DailyParameters.Coordinates });
            Connection.myparameters.Add(new myParameters { ParameterName = "@NEARESTESTABLISHMENT", mytype = SqlDbType.NVarChar, Value = DailyParameters.NearestEstablishment });
            Connection.myparameters.Add(new myParameters { ParameterName = "@STATUS", mytype = SqlDbType.NVarChar, Value = DailyParameters.Status });
            Connection.myparameters.Add(new myParameters { ParameterName = "@REQUESTEARLY", mytype = SqlDbType.Bit, Value = DailyParameters.RequestEarly });


            dtDAILY = Connection.GetDataTable("sp_HRMS_TimeInRevised");
            if (dtDAILY.Rows.Count > 0)
            {
                foreach (DataRow row in dtDAILY.Rows)
                {
                    Daily Daily = new Daily
                    {
                        SchedDate = Convert.ToDateTime(row["SchedDate"]).ToString("MMM-dd-yyyy"),
                        SchedIN = row["Start_Time"].ToString(),
                        SchedOUT = row["End_Time"].ToString(),
                        SchedType = row["ReasonDesc"].ToString().Replace("\r\n", string.Empty)
                    };
                    DailyList.Add(Daily);
                }
                GetDailyResult.Daily = DailyList;
                GetDailyResult.myreturn = "Success";
            }
            else
            {
                GetDailyResult.myreturn = "No Data Available";
            }
            return GetDailyResult;
        }

        [HttpPost]
        [Route("TimeOutDisplayv3")]
        public string TimeOutAndDisplayv3(DailyParametersv3 DailyParameters)
        {
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = DailyParameters.CN;
            SelfieRegistration2Controller.GetDB2(GDB);

            Connection Connection = new Connection();
            Connection.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = DailyParameters.NTID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@REQUESTLATE", mytype = SqlDbType.NVarChar, Value = DailyParameters.RequestLate });
            //Connection.myparameters.Add(new myParameters { ParameterName = "@NearestEstablishment", mytype = SqlDbType.NVarChar, Value = DailyParameters.NearestEstablishment });
            //Connection.myparameters.Add(new myParameters { ParameterName = "@NearestEstablishment", mytype = SqlDbType.NVarChar, Value = DailyParameters.IpLocation });
            //Connection.myparameters.Add(new myParameters { ParameterName = "@NearestEstablishment", mytype = SqlDbType.NVarChar, Value = DailyParameters.Status });
            //Connection.myparameters.Add(new myParameters { ParameterName = "@NearestEstablishment", mytype = SqlDbType.Bit, Value = DailyParameters.RequestLate });
            //String ID = Connection.ExecuteScalar("sp_TimeOut_Mobile");

            String ID = Connection.ExecuteScalar("sp_TimeOutHRMSEmployee");



            return ID;
        }
        [HttpPost]
        [Route("TimeOutDisplayv4")]
        public string TimeOutAndDisplayv4(DailyParametersv3 DailyParameters)
        {
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = DailyParameters.CN;
            SelfieRegistration2Controller.GetDB2(GDB);

            Connection Connection = new Connection();
            Connection.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = DailyParameters.NTID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@REQUESTLATE", mytype = SqlDbType.NVarChar, Value = DailyParameters.RequestLate });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Coordinate", mytype = SqlDbType.NVarChar, Value = DailyParameters.Coordinates });
            Connection.myparameters.Add(new myParameters { ParameterName = "@NearestEstablishment", mytype = SqlDbType.NVarChar, Value = DailyParameters.NearestEstablishment });
            //Connection.myparameters.Add(new myParameters { ParameterName = "@NearestEstablishment", mytype = SqlDbType.NVarChar, Value = DailyParameters.Status });
            //Connection.myparameters.Add(new myParameters { ParameterName = "@NearestEstablishment", mytype = SqlDbType.Bit, Value = DailyParameters.RequestLate });
            //String ID = Connection.ExecuteScalar("sp_TimeOut_Mobile");

            String ID = Connection.ExecuteScalar("sp_TimeOutHRMSEmployeev2");



            return ID;
        }
        [HttpPost]
        [Route("CheckEarlyTimeIn")]
        public string CheckEarlyTimeIn(employeev2 emp)
        {
            try
            {
                GetDatabase GDB = new GetDatabase();
                GDB.ClientName = emp.CN;
                SelfieRegistration2Controller.GetDB2(GDB);

                Connection Connection = new Connection();
                Connection.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = emp.empid });
                return Connection.ExecuteScalar("sp_HRMS_CheckEarlyTimeIn");
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }

        [HttpPost]
        [Route("CheckLateTimeOut")]
        public string CheckLateTimeOut(employeev2 emp)
        {
            try
            {
                GetDatabase GDB = new GetDatabase();
                GDB.ClientName = emp.CN;
                SelfieRegistration2Controller.GetDB2(GDB);

                Connection Connection = new Connection();
                Connection.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = emp.empid });
                return Connection.ExecuteScalar("sp_HRMS_CheckLateTimeOut");
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }
        #region 2018-09-19
        [HttpPost]
        [Route("DisplayCurrentDayActivityv2")]
        public GetDailyResult DisplayCurrentDayActivityv2(DailyParameters DailyParameters)
        {
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = DailyParameters.CN;
            SelfieRegistration2Controller.GetDB2(GDB);

            GetDailyResult GetDailyResult = new GetDailyResult();
            //try
            //{
            Connection Connection = new Connection();
            List<Daily> DailyList = new List<Daily>();
            DataTable dtDAILY = new DataTable();
            //BasicFunction BasicFunction = new BasicFunction();
            Connection.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.NVarChar, Value = DailyParameters.NTID });
            dtDAILY = Connection.GetDataTable("sp_DisplayCurrentDayActivity_Mobile");
            //CultureInfo provider = CultureInfo.InvariantCulture;
            if (dtDAILY.Rows.Count > 0)
            {
                foreach (DataRow row in dtDAILY.Rows)
                {
                    Daily Daily = new Daily
                    {
                        SchedDate = Convert.ToDateTime(row["SchedDate"]).ToString("MMM-dd-yyyy"),
                        //SchedDay = Convert.ToDateTime(row["SchedDate"]).DayOfWeek.ToString().Substring(0, 3),
                        SchedIN = row["Start_Time"].ToString(),
                        SchedOUT = row["End_Time"].ToString(),
                        //TotalTimeHour = BasicFunction.Decimal102(row["TotalTimeHour"].ToString()),
                        SchedType = row["ReasonDesc"].ToString(),
                        ReasonID = row["ReasonID"].ToString()
                        //DateFormat = Convert.ToDateTime(row["SchedDate"]).ToString()
                    };
                    DailyList.Add(Daily);
                }
                GetDailyResult.Daily = DailyList;
                GetDailyResult.myreturn = "Success";
            }
            else
            {
                GetDailyResult.myreturn = "No Data Available";
            }
            //}
            //catch
            //{
            //    GetDailyResult.myreturn = "Error Fetching Data";

            //}
            return GetDailyResult;
        }
        #endregion


        #region 2018-09-24
        [HttpPost]
        [Route("CancelAddSegmentDHRv1")]
        public string CancelAddSegmentDHRv1(DailyParameters emp)
        {
            try
            {
                GetDatabase GDB = new GetDatabase();
                GDB.ClientName = emp.CN;
                SelfieRegistration2Controller.GetDB2(GDB);

                Connection Connection = new Connection();
                Connection.myparameters.Add(new myParameters { ParameterName = "@SeriesID", mytype = SqlDbType.NVarChar, Value = emp.SeriesID });
                Connection.ExecuteNonQuery("sp_CancelAddedSegment");
                return "success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }
        #endregion
        #region 2018-10-10
        [HttpPost]
        [Route("GetDHRSegmentSettingsv1")]

        public GetSegmentSetv1 GetDHRSegmentSettingsv1(DHRSegmentSetv1 PS)
        {
            GetSegmentSetv1 GetPSResult = new GetSegmentSetv1();
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = PS.CN;
            SelfieRegistration2Controller.GetDB2(GDB);

            try
            {
                Connection Connection = new Connection();
                List<DHRSegmentSetv1> ListRF = new List<DHRSegmentSetv1>();
                DataSet ds = new DataSet();
                System.Data.DataTable dt = new System.Data.DataTable();
                ds = Connection.GetDataset("sp_GetDHRSegmentSettingsv1");
                dt = ds.Tables[0];
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        DHRSegmentSetv1 DP = new DHRSegmentSetv1
                        {
                            ROWID = row["rowID"].ToString(),
                            SEGMENTID = row["segmentID"].ToString(),
                            SEGMENTPLATFORM = row["segmentPlatform"].ToString(),
                            ISFACIALRECOG = row["isFacialRecog"].ToString(),
                            ISGEOFENCING = row["isGeofencing"].ToString(),
                            ISGEOLOCATION = row["isGeolocation"].ToString(),
                            REASONDESC = row["ReasonDesc"].ToString(),
                            CATEGORY = row["Category"].ToString(),
                            REASONDESCRIPTION = row["ReasonDescription"].ToString(),
                            ALLOWDELETE = row["AllowDelete"].ToString()
                        };
                        ListRF.Add(DP);
                    }
                    GetPSResult.SegmentSettingsList = ListRF;
                    GetPSResult.myreturn = "Success";
                }
                else
                {
                    GetPSResult.myreturn = "No Data Available";
                }
            }
            catch (Exception ex)
            {
                GetPSResult.myreturn = ex.Message;
            }
            return GetPSResult;
        }

        #endregion

        #region 2018-10-15

        [HttpPost]
        [Route("AddDHRSegmentv1")]
        public string AddDHRSegmentv1(DHRSEGMENTV1 T)
        {
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = T.CN;
            SelfieRegistration2Controller.GetDB2(GDB);
            Connection con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@segmentID", mytype = SqlDbType.NVarChar, Value = T.SEGMENTID });
            con.myparameters.Add(new myParameters { ParameterName = "@segment", mytype = SqlDbType.NVarChar, Value = T.SEGMENT });
            con.myparameters.Add(new myParameters { ParameterName = "@segmentType", mytype = SqlDbType.NVarChar, Value = T.SEGMENTTYPE });
            con.myparameters.Add(new myParameters { ParameterName = "@segmentDesc", mytype = SqlDbType.NVarChar, Value = T.SEGMENTDESC });
            con.myparameters.Add(new myParameters { ParameterName = "@isActive", mytype = SqlDbType.NVarChar, Value = T.ISACTIVE });
            con.myparameters.Add(new myParameters { ParameterName = "@isMultiple", mytype = SqlDbType.NVarChar, Value = T.ISMULTIPLE });
            con.myparameters.Add(new myParameters { ParameterName = "@MODIFIEDBY", mytype = SqlDbType.NVarChar, Value = T.LOGINID });
            con.ExecuteNonQuery("sp_AddDHRSegmentv1");
            return "success";
        }


        [HttpPost]
        [Route("UpdateDHRSegmentv1")]
        public string UpdateDHRSegmentv1(DHRSEGMENTARRAYV1 T)
        {
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = T.CN;
            SelfieRegistration2Controller.GetDB2(GDB);

            Connection con = new Connection();
            con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@UID", mytype = SqlDbType.NVarChar, Value = T.ROWID });
            con.myparameters.Add(new myParameters { ParameterName = "@FACIALISACTIVE", mytype = SqlDbType.Bit, Value = T.FACIALISACTIVE });
            con.myparameters.Add(new myParameters { ParameterName = "@GEOISACTIVE", mytype = SqlDbType.Bit, Value = T.GEOISACTIVE });
            con.myparameters.Add(new myParameters { ParameterName = "@GEOLOCATIONISACTIVE", mytype = SqlDbType.Bit, Value = T.GEOLOCATIONISACTIVE });
            String ID = con.ExecuteScalar("sp_UpdateDHRSegmentv1");
            return "success";
        }

        [HttpPost]
        [Route("DelDHRSegmentv1")]
        public string DelDHRSegmentv1(DHRSEGMENTV1 T)
        {
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = T.CN;
            SelfieRegistration2Controller.GetDB2(GDB);

            Connection con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@segmentID", mytype = SqlDbType.NVarChar, Value = T.SEGMENTID });
            con.ExecuteNonQuery("sp_DelDHRSegmentv1");
            return "success";
        }

        #endregion

        [HttpPost]
        [Route("GetSwapSchedShift")]
        public SwapSchedList GetSwapSChed(SwapModel model)
        {
            SwapModel swapModel = new SwapModel();
            SwapSchedList swapSchedList = new SwapSchedList();

            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = model.CN;
            SelfieRegistration2Controller.GetDB2(GDB);
            // get empCat level 
            try
            {
                Connection con = new Connection();
                DataSet ds = new DataSet();
                DataTable dt = new DataTable();
                con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.NVarChar, Value = model.EmpID });
                ds = con.GetDataset("sp_getEmpCatLev");
                dt = ds.Tables[0];
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        swapModel.EmpLevel = row[0].ToString();
                        swapModel.EmpCat = row[1].ToString();
                    }
                }
            }
            catch { }
            //Get Swap Settings 
            try
            {
                Connection con = new Connection();
                DataSet ds = new DataSet();
                DataTable dt = new DataTable();
                con.myparameters.Add(new myParameters { ParameterName = "@emplevel", mytype = SqlDbType.NVarChar, Value = swapModel.EmpLevel });
                con.myparameters.Add(new myParameters { ParameterName = "@empcategory", mytype = SqlDbType.NVarChar, Value = swapModel.EmpCat });
                ds = con.GetDataset("sp_getSwapSettings");
                dt = ds.Tables[0];
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        swapModel.SwapID = row["swapID"].ToString();
                        swapModel.SystemLevel = row["System_Level"].ToString();
                        swapModel.SystemCategory = row["System_Category"].ToString();
                        swapModel.CanSwapWithLevel = row["CanSwapWith_Level"].ToString();
                        swapModel.CanSwapWithCategory = row["CanSwapWith_Category"].ToString();
                        swapModel.CanSwapFromBranch = row["CanSwapFrom_Branch"].ToString();
                        swapModel.CanSwapFromSegment = row["CanSwapFrom_BusinessSegment"].ToString();
                        swapModel.CanSwapFromBusinessUnit = row["CanSwapFrom_BusinessUnit"].ToString();
                        swapModel.ID = row["ID"].ToString();
                    }

                }
            }
            catch { }
            try
            {
                Connection con = new Connection();
                List<SwapSched> list = new List<SwapSched>();
                DataSet ds = new DataSet();
                DataTable dt = new DataTable();
                con.myparameters.Add(new myParameters { ParameterName = "@SCHEDDATE", mytype = SqlDbType.NVarChar, Value = model.RequestDate });
                con.myparameters.Add(new myParameters { ParameterName = "@empid", mytype = SqlDbType.NVarChar, Value = model.EmpID });
                con.myparameters.Add(new myParameters { ParameterName = "@type", mytype = SqlDbType.NVarChar, Value = model.Type });
                con.myparameters.Add(new myParameters { ParameterName = "@swapbsegment", mytype = SqlDbType.NVarChar, Value = swapModel.CanSwapFromSegment });
                con.myparameters.Add(new myParameters { ParameterName = "@swapbunit", mytype = SqlDbType.NVarChar, Value = swapModel.CanSwapFromBusinessUnit });
                con.myparameters.Add(new myParameters { ParameterName = "@swaplev", mytype = SqlDbType.NVarChar, Value = swapModel.CanSwapWithLevel });
                con.myparameters.Add(new myParameters { ParameterName = "@swapcat", mytype = SqlDbType.NVarChar, Value = swapModel.CanSwapWithCategory });
                con.myparameters.Add(new myParameters { ParameterName = "@swapbranch", mytype = SqlDbType.NVarChar, Value = swapModel.CanSwapFromBranch });
                ds = con.GetDataset("sp_getSameSchedRequest");
                dt = ds.Tables[0];
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        SwapSched sched = new SwapSched
                        {
                            Sched = row[0].ToString()
                        };
                        list.Add(sched);
                    }
                    swapSchedList.swapDateList = list;
                }
            }
            catch { }

            return swapSchedList;
        }
        [HttpPost]
        [Route("GetSwapWeekSched")]
        public ListWeekSchedResult GetSwapWeekSched(SwapModel model)
        {
            ListWeekSchedResult listWeekSchedResult = new ListWeekSchedResult();
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = model.CN;
            SelfieRegistration2Controller.GetDB2(GDB);
            try
            {
                Connection con = new Connection();
                List<WeekSched> List = new List<WeekSched>();
                DataSet ds = new DataSet();
                DataTable dt = new DataTable();
                con.myparameters.Add(new myParameters { ParameterName = "@SCHEDDATE", mytype = SqlDbType.NVarChar, Value = model.RequestDate });
                con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = model.EmpID });
                ds = con.GetDataset("sp_getWeekSched");
                dt = ds.Tables[0];
                foreach (DataRow row in dt.Rows)
                {
                    WeekSched sched = new WeekSched
                    {
                        SeriesID = row["SeriesID"].ToString(),
                        SchedDate = row["SchedDate"].ToString(),
                        SchedTitle = row[3].ToString(),
                        Schedule = row["Schedule"].ToString()
                    };
                    List.Add(sched);
                }
                listWeekSchedResult.ListWeekSched = List;
                listWeekSchedResult.myreturn = "Success";
            }
            catch
            {
                listWeekSchedResult.myreturn = "Error";
            }
            return listWeekSchedResult;
        }
        [HttpPost]
        [Route("GetSwapSchedRD")]
        public SwapSchedList GetSwapSchedRD(SwapModel model)
        {
            SwapModel swapModel = new SwapModel();
            SwapSchedList swapSchedList = new SwapSchedList();

            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = model.CN;
            SelfieRegistration2Controller.GetDB2(GDB);
            // get empCat level 
            try
            {
                Connection con = new Connection();
                DataSet ds = new DataSet();
                DataTable dt = new DataTable();
                con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.NVarChar, Value = model.EmpID });
                ds = con.GetDataset("sp_getEmpCatLev");
                dt = ds.Tables[0];
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        swapModel.EmpLevel = row[0].ToString();
                        swapModel.EmpCat = row[1].ToString();
                    }
                }
            }
            catch { }
            //Get Swap Settings 
            try
            {
                Connection con = new Connection();
                DataSet ds = new DataSet();
                DataTable dt = new DataTable();
                con.myparameters.Add(new myParameters { ParameterName = "@emplevel", mytype = SqlDbType.NVarChar, Value = swapModel.EmpLevel });
                con.myparameters.Add(new myParameters { ParameterName = "@empcategory", mytype = SqlDbType.NVarChar, Value = swapModel.EmpCat });
                ds = con.GetDataset("sp_getSwapSettings");
                dt = ds.Tables[0];
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        swapModel.SwapID = row["swapID"].ToString();
                        swapModel.SystemLevel = row["System_Level"].ToString();
                        swapModel.SystemCategory = row["System_Category"].ToString();
                        swapModel.CanSwapWithLevel = row["CanSwapWith_Level"].ToString();
                        swapModel.CanSwapWithCategory = row["CanSwapWith_Category"].ToString();
                        swapModel.CanSwapFromBranch = row["CanSwapFrom_Branch"].ToString();
                        swapModel.CanSwapFromSegment = row["CanSwapFrom_BusinessSegment"].ToString();
                        swapModel.CanSwapFromBusinessUnit = row["CanSwapFrom_BusinessUnit"].ToString();
                        swapModel.ID = row["ID"].ToString();
                    }
                }
            }
            catch { }
            try
            {
                Connection con = new Connection();
                List<SwapSched> list = new List<SwapSched>();
                DataSet ds = new DataSet();
                DataTable dt = new DataTable();
                con.myparameters.Add(new myParameters { ParameterName = "@SCHEDDATE", mytype = SqlDbType.NVarChar, Value = model.RequestDate });
                con.myparameters.Add(new myParameters { ParameterName = "@OLDSCHED", mytype = SqlDbType.NVarChar, Value = model.OldDate });
                con.myparameters.Add(new myParameters { ParameterName = "@empid", mytype = SqlDbType.NVarChar, Value = model.EmpID });
                con.myparameters.Add(new myParameters { ParameterName = "@swapbsegment", mytype = SqlDbType.NVarChar, Value = swapModel.CanSwapFromSegment });
                con.myparameters.Add(new myParameters { ParameterName = "@swapbunit", mytype = SqlDbType.NVarChar, Value = swapModel.CanSwapFromBusinessUnit });
                con.myparameters.Add(new myParameters { ParameterName = "@swaplev", mytype = SqlDbType.NVarChar, Value = swapModel.CanSwapWithLevel });
                con.myparameters.Add(new myParameters { ParameterName = "@swapcat", mytype = SqlDbType.NVarChar, Value = swapModel.CanSwapWithCategory });
                con.myparameters.Add(new myParameters { ParameterName = "@swapbranch", mytype = SqlDbType.NVarChar, Value = swapModel.CanSwapFromBranch });
                ds = con.GetDataset("sp_getOherRestDay");
                dt = ds.Tables[0];
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        SwapSched sched = new SwapSched
                        {
                            EmpID = row[0].ToString(),
                            Sched = row[1].ToString()
                        };
                        list.Add(sched);
                    }
                    swapSchedList.swapDateList = list;
                }
            }
            catch { }

            return swapSchedList;
        }
        [HttpPost]
        [Route("RequestSwapSched")]
        public string RequestSwapSched(SwapModel model)
        {
            SwapModel swapModel = new SwapModel();
            SwapSchedList swapSchedList = new SwapSchedList();

            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = model.CN;
            SelfieRegistration2Controller.GetDB2(GDB);
            // get empCat level 
            try
            {
                Connection con = new Connection();
                DataSet ds = new DataSet();
                DataTable dt = new DataTable();
                con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.NVarChar, Value = model.EmpID });
                ds = con.GetDataset("sp_getEmpCatLev");
                dt = ds.Tables[0];
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        swapModel.EmpLevel = row[0].ToString();
                        swapModel.EmpCat = row[1].ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
            //Get Swap Settings 
            try
            {
                Connection con = new Connection();
                DataSet ds = new DataSet();
                DataTable dt = new DataTable();
                con.myparameters.Add(new myParameters { ParameterName = "@emplevel", mytype = SqlDbType.NVarChar, Value = swapModel.EmpLevel });
                con.myparameters.Add(new myParameters { ParameterName = "@empcategory", mytype = SqlDbType.NVarChar, Value = swapModel.EmpCat });
                ds = con.GetDataset("sp_getSwapSettings");
                dt = ds.Tables[0];
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        swapModel.SwapID = row["swapID"].ToString();
                        swapModel.SystemLevel = row["System_Level"].ToString();
                        swapModel.SystemCategory = row["System_Category"].ToString();
                        swapModel.CanSwapWithLevel = row["CanSwapWith_Level"].ToString();
                        swapModel.CanSwapWithCategory = row["CanSwapWith_Category"].ToString();
                        swapModel.CanSwapFromBranch = row["CanSwapFrom_Branch"].ToString();
                        swapModel.CanSwapFromSegment = row["CanSwapFrom_BusinessSegment"].ToString();
                        swapModel.CanSwapFromBusinessUnit = row["CanSwapFrom_BusinessUnit"].ToString();
                        swapModel.ID = row["ID"].ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
            try
            {
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = model.EmpID });
                con.myparameters.Add(new myParameters { ParameterName = "@SCHEDTYPE", mytype = SqlDbType.NVarChar, Value = model.Type });
                con.myparameters.Add(new myParameters { ParameterName = "@DATETODAY", mytype = SqlDbType.NVarChar, Value = DateTime.Now.ToString("yyyy-MM-dd") });
                con.myparameters.Add(new myParameters { ParameterName = "@SCHEDDATE", mytype = SqlDbType.NVarChar, Value = model.RequestDate });
                con.myparameters.Add(new myParameters { ParameterName = "@OID", mytype = SqlDbType.NVarChar, Value = swapModel.SwapID });
                con.myparameters.Add(new myParameters { ParameterName = "@ORSCHED", mytype = SqlDbType.NVarChar, Value = model.OldDate });

                con.ExecuteScalar("sp_InsertSwapSchedRequest");
                return "Success";
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }
        [HttpPost]
        [Route("GetSwapSelectedSched")]
        public ListSwapSelectedSched GetSwapSelectedSched(SwapModel model)
        {
            ListSwapSelectedSched swapSelectedSched = new ListSwapSelectedSched();

            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = model.CN;
            SelfieRegistration2Controller.GetDB2(GDB);

            try
            {
                Connection con = new Connection();
                List<SwapSelectedSched> list = new List<SwapSelectedSched>();
                DataSet ds = new DataSet();
                DataTable dt = new DataTable();
                con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.NVarChar, Value = model.EmpID });
                con.myparameters.Add(new myParameters { ParameterName = "@TypeOfSched", mytype = SqlDbType.NVarChar, Value = model.SelectedType });
                ds = con.GetDataset("sp_GetLeaveSched");
                dt = ds.Tables[0];
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        SwapSelectedSched selectedSched = new SwapSelectedSched();

                        if (model.SelectedType == "Shift")
                        {
                            selectedSched.EmpID = row["EmpID"].ToString();
                            selectedSched.SchedDate = row["SchedDate"].ToString();
                            selectedSched.Break1 = row[4].ToString();
                            selectedSched.Lunch = row[5].ToString();
                            selectedSched.Break2 = row[6].ToString();
                            selectedSched.Shift = row[9].ToString();
                        }
                        else if (model.SelectedType == "Leave")
                        {
                            selectedSched.LeaveDate = row["LeaveDate"].ToString();
                        }
                        list.Add(selectedSched);
                    }
                    swapSelectedSched.ListSelectedSched = list;
                }
            }
            catch
            {

            }
            return swapSelectedSched;
        }
        [HttpPost]
        [Route("GetSwapSchedule")]
        public ListSwapSelectedSchedv2 GetSwapSchedule(SwapModelv2 model)
        {
            ListSwapSelectedSchedv2 swapSelectedSched = new ListSwapSelectedSchedv2();

            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = model.CN;
            SelfieRegistration2Controller.GetDB2(GDB);


            try
            {
                Connection con = new Connection();
                List<SwapSelectedSchedv2> list = new List<SwapSelectedSchedv2>();
                DataSet ds = new DataSet();
                DataTable dt = new DataTable();
                con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.NVarChar, Value = model.EmpID });
                con.myparameters.Add(new myParameters { ParameterName = "@SELECTEDDATE", mytype = SqlDbType.NVarChar, Value = model.SelectedDate });
                ds = con.GetDataset("sp_GetSwapTKSchedule");
                dt = ds.Tables[0];
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        SwapSelectedSchedv2 selectedSched = new SwapSelectedSchedv2();
                        selectedSched.Schedule = row[0].ToString();
                        list.Add(selectedSched);
                    }
                    swapSelectedSched.listSwapSelectedSchedv2 = list;
                }
            }
            catch (Exception ex)
            {
                swapSelectedSched.myReturn = ex.ToString();
            }
            return swapSelectedSched;
        }
        [HttpPost]
        [Route("GetSwapNewSchedule")]
        public ListSwapSelectedSchedv2 GetSwapNewSchedule(SwapModelv2 model)
        {
            ListSwapSelectedSchedv2 swapSelectedSched = new ListSwapSelectedSchedv2();

            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = model.CN;
            SelfieRegistration2Controller.GetDB2(GDB);


            try
            {
                Connection con = new Connection();
                List<SwapSelectedSchedv2> list = new List<SwapSelectedSchedv2>();
                DataSet ds = new DataSet();
                DataTable dt = new DataTable();
                con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.NVarChar, Value = model.EmpID });
                con.myparameters.Add(new myParameters { ParameterName = "@SELECTEDDATE", mytype = SqlDbType.NVarChar, Value = model.SelectedDate });
                ds = con.GetDataset("sp_GetTKSwapDdl");
                dt = ds.Tables[0];
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        SwapSelectedSchedv2 selectedSched = new SwapSelectedSchedv2();
                        selectedSched.NewSchedule = row[0].ToString();
                        selectedSched.EmpID = row[1].ToString();
                        list.Add(selectedSched);
                    }
                    swapSelectedSched.listSwapSelectedSchedv2 = list;
                }
            }
            catch (Exception ex)
            {
                swapSelectedSched.myReturn = ex.ToString();
            }
            return swapSelectedSched;
        }
        [HttpPost]
        [Route("SendSwapRequest")]
        public string SendSwapRequest(SwapModelv2 model)
        {
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = model.CN;
            SelfieRegistration2Controller.GetDB2(GDB);

            try
            {
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = model.EmpID });
                con.myparameters.Add(new myParameters { ParameterName = "@SELECTED", mytype = SqlDbType.NVarChar, Value = model.SelectedSched });
                con.myparameters.Add(new myParameters { ParameterName = "@ORSCHED", mytype = SqlDbType.NVarChar, Value = model.SelectedDate });
                con.ExecuteScalar("sp_SaveSwapTK");
                return "Success";
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }

        [HttpPost]
        [Route("AcceptDeclineSwap")]
        public string AcceptDeclineSwap(SwapModelv2 model)
        {
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = model.CN;
            SelfieRegistration2Controller.GetDB2(GDB);

            try
            {
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = model.EmpID });
                con.myparameters.Add(new myParameters { ParameterName = "@SWAPID", mytype = SqlDbType.NVarChar, Value = model.SwapID });
                con.myparameters.Add(new myParameters { ParameterName = "@SWAPSTATUS", mytype = SqlDbType.NVarChar, Value = model.SwapStatus });
                con.ExecuteScalar("[sp_SaveSwapHome]");
                return "Success";
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }

        [HttpPost]
        [Route("SendSwapRequestv2")]
        public ListAllSwapRequesteResult SendSwapRequestv2(SwapModelv2 model)
        {
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = model.CN;
            SelfieRegistration2Controller.GetDB2(GDB);
            ListAllSwapRequesteResult GetEmpIDResult = new ListAllSwapRequesteResult();
            List<ListAllSwapRequest> AllActivity = new List<ListAllSwapRequest>();
            try
            {
                Connection con = new Connection();
                DataTable dt = new DataTable();
                con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = model.EmpID });
                con.myparameters.Add(new myParameters { ParameterName = "@SELECTED", mytype = SqlDbType.NVarChar, Value = model.SelectedSched });
                con.myparameters.Add(new myParameters { ParameterName = "@ORSCHED", mytype = SqlDbType.NVarChar, Value = model.SelectedDate });
                dt = con.GetDataTable("sp_SaveSwapTKv2");
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        ListAllSwapRequest ListAllActivity = new ListAllSwapRequest
                        {
                            EMPID = row["EMPID"].ToString(),
                            SwapID = row["SWAPID"].ToString()
                        };
                        AllActivity.Add(ListAllActivity);
                    }
                    GetEmpIDResult.ListAllSwapRequests = AllActivity;
                    GetEmpIDResult.myreturn = "Success";
                }
                else
                {
                    GetEmpIDResult.myreturn = "No Data Available";
                }
                return GetEmpIDResult;
            }
            catch (Exception ex)
            {
                return GetEmpIDResult;
            }
        }
        [HttpPost]
        [Route("CheckSwapIfEnabled")]
        public string CheckSwapIfEnabled(SwapModelv2 model)
        {
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = model.CN;
            SelfieRegistration2Controller.GetDB2(GDB);
            string result = "";
            try
            {
                Connection con = new Connection();
                DataSet ds = new DataSet();
                DataTable dt = new DataTable();
                con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.NVarChar, Value = model.EmpID });
                ds = con.GetDataset("sp_GetTKSwapSet");
                dt = ds.Tables[0];
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        result = row[0].ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
            return result;
        }
        [HttpPost]
        [Route("GetAllAddedSegment")]
        public ListAllActivityResult GetAllAddedSegment(DailyParametersv2 dailyParametersv2)
        {
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = dailyParametersv2.CN;
            SelfieRegistration2Controller.GetDB2(GDB);

            ListAllActivityResult GetActivityResult = new ListAllActivityResult();

            Connection Connection = new Connection();
            List<ListAllActivityByRange> AllActivity = new List<ListAllActivityByRange>();
            DataTable dt = new DataTable();
            Connection.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.NVarChar, Value = dailyParametersv2.NTID });
            dt = Connection.GetDataTable("sp_GetAllAddedSegment");
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    ListAllActivityByRange ListAllActivity = new ListAllActivityByRange
                    {
                        SeriesID = row["SeriesID"].ToString(),
                        StartTIme = row["Start_Time"].ToString(),
                        StartEnd = row["End_Time"].ToString(),
                        ReasonID = row["ReasonID"].ToString(),
                        SchedDate = row["SchedDate"].ToString(),
                        ActivityStatus = row["Activity_Status"].ToString()
                    };
                    AllActivity.Add(ListAllActivity);
                }
                GetActivityResult.ListAllActivity = AllActivity;
                GetActivityResult.myreturn = "Success";
            }
            else
            {
                GetActivityResult.myreturn = "No Data Available";
            }

            return GetActivityResult;
        }

        [HttpPost]
        [Route("GetAllLeaveNotification")]
        public ListAllLeaveNotificationResult GetAllLeaveNotification(NotifParam NotifParam)
        {
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = NotifParam.CN;
            SelfieRegistration2Controller.GetDB2(GDB);

            ListAllLeaveNotificationResult GetActivityResult = new ListAllLeaveNotificationResult();

            Connection Connection = new Connection();
            List<ListAllLeaveNotification> AllActivity = new List<ListAllLeaveNotification>();
            DataTable dt = new DataTable();
            Connection.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = NotifParam.EMPID });
            dt = Connection.GetDataTable("sp_ShowLeavesNotification_Mobile");
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    ListAllLeaveNotification ListAllActivity = new ListAllLeaveNotification
                    {
                        SeriesID = row["SeriesID"].ToString(),
                        EmpID = row["EmpID"].ToString(),
                        EmpName = row["EmpName"].ToString(),
                        LeaveDate = row["LeaveDate"].ToString(),
                        LeaveStatus = row["LeaveStatus"].ToString(),
                        DateApproved = Convert.ToDateTime(row["DateApproved"].ToString()).ToString("yyyy-MM-dd hh:mm tt") ,
                        LeaveType = row["LeaveType"].ToString(),
                        NotifLike = row["NotifLike"].ToString(),
                        NotifType = row["NotifType"].ToString(),
                        isShow = row["isShow"].ToString(),
                        isSeen = row["isSeen"].ToString()
                    };
                    AllActivity.Add(ListAllActivity);
                }
                GetActivityResult.ListAllLeave = AllActivity;
                GetActivityResult.myreturn = "Success";
            }
            else
            {
                GetActivityResult.myreturn = "No Data Available";
            }

            return GetActivityResult;
        }

        [HttpPost]
        [Route("GetAllTimeNotification")]
        public ListAllTimeNotificationResult GetAllTimeNotification(NotifParam NotifParam)
        {
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = NotifParam.CN;
            SelfieRegistration2Controller.GetDB2(GDB);

            ListAllTimeNotificationResult GetActivityResult = new ListAllTimeNotificationResult();

            Connection Connection = new Connection();
            List<ListAllTimeNotification> AllActivity = new List<ListAllTimeNotification>();
            DataTable dt = new DataTable();
            Connection.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = NotifParam.EMPID });
            dt = Connection.GetDataTable("sp_ShowTimesheetNotification_Mobile");
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    ListAllTimeNotification ListAllActivity = new ListAllTimeNotification
                    {
                        SeriesID = row["SeriesID"].ToString(),
                        EmpID = row["EmpID"].ToString(),
                        EmpName = row["EmpName"].ToString(),
                        SchedDate = row["SchedDate"].ToString(),
                        StartTIme = row["StartTIme"].ToString(),
                        StartEnd = row["StartEnd"].ToString(),
                        ReasonName = row["ReasonName"].ToString(),
                        ActivityStatus = row["ActivityStatus"].ToString(),
                        NotifLike = row["NotifLike"].ToString(),
                        NotifType = row["NotifType"].ToString(),
                        isShow = row["isShow"].ToString(),
                        isSeen = row["isSeen"].ToString(),
                        DateApproved = Convert.ToDateTime(row["DateApproved"].ToString()).ToString("yyyy-MM-dd hh:mm tt")
                    };
                    AllActivity.Add(ListAllActivity);
                }
                GetActivityResult.ListAllTime = AllActivity;
                GetActivityResult.myreturn = "Success";
            }
            else
            {
                GetActivityResult.myreturn = "No Data Available";
            }

            return GetActivityResult;
        }
        [HttpPost]
        [Route("GetAllCountNotification")]
        public ListAllCountNotificationResult GetAllCountNotification(NotifParam NotifParam)
        {
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = NotifParam.CN;
            SelfieRegistration2Controller.GetDB2(GDB);

            ListAllCountNotificationResult GetActivityResult = new ListAllCountNotificationResult();

            Connection Connection = new Connection();
            List<ListAllCountNotification> AllActivity = new List<ListAllCountNotification>();
            DataTable dt = new DataTable();
            Connection.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = NotifParam.EMPID });
            dt = Connection.GetDataTable("sp_NotificationCountMobile");
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    ListAllCountNotification ListAllActivity = new ListAllCountNotification
                    {
                        Time = row["time"].ToString(),
                        Leave = row["leave"].ToString(),
                        News = row["news"].ToString(),
                        Greet = row["greet"].ToString(),
                        All = row["totalcount"].ToString()
                    };
                    AllActivity.Add(ListAllActivity);
                }
                GetActivityResult.ListAllCount = AllActivity;
                GetActivityResult.myreturn = "Success";
            }
            else
            {
                GetActivityResult.myreturn = "No Data Available";
            }

            return GetActivityResult;
        }

        [HttpPost]
        [Route("CheckMCAcceestype")]

        public string CheckMCAcceestype(checkMC MCcheck)
        {

            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = MCcheck.CN;
            SelfieRegistration2Controller.GetDB2(GDB);
            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = MCcheck.EMPID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@PARAM", mytype = SqlDbType.NVarChar, Value = MCcheck.PARAM });

            //  Connection.myparameters.Add(new myParameters { ParameterName = "@STARTOREND", mytype = SqlDbType.NVarChar, Value = uBreak.StartOrEnd });

            // DataTable EmployeeDt = new DataTable();
            String Value = Connection.ExecuteScalar("sp_MC_AccessType_Mobile");

            return Value;

        }
        //---------------------------------------------------------------Revised Change DB End------------------------------------------------------------------------
    }
}

