﻿using System;
using System.Web.Http;
using System.Data;
using illimitadoWepAPI.MyClass;
using System.Configuration;
using System.Reflection;
using System.Collections.Generic;
using ExpertXls.ExcelLib;
using System.IO;
using System.Web.Hosting;
using System.Web;
using System.Net.Mail;
using System.Net;
using illimitadoWepAPI.Models;

namespace illimitadoWepAPI.Controllers.PerfAppraisalController
{

    public class PerfAppraisalController : ApiController
    {
        private object group_id;

        public void tempconnectionstring()
        {
            var settings = ConfigurationManager.ConnectionStrings["connectionstring"];
            var fi = typeof(ConfigurationElement).GetField(
                          "_bReadOnly",
                          BindingFlags.Instance | BindingFlags.NonPublic);
            fi.SetValue(settings, false);
            //--Local--//
            //settings.ConnectionString = "Data Source=192.168.2.33,1444;Database=PerfApp_DB;User Id=sa;Password=securedmgt@1230;";
            //--AWS--//
            settings.ConnectionString = "Data Source=livedb-ilm.c3srcolvclfy.ap-southeast-1.rds.amazonaws.com,1412; Database = PerfApp_DB;User Id=ILM_LiveDBUser;Password=ILMV3ry53cur3dMGTpa55w0rd2020;";

        }

        //isAuthEscalate return 
        [HttpPost]
        [Route("isAuthEscalate")]
        public GetisAuthEscalate isAuthEscalate(isAuthEscalate VU)
        {

            GetisAuthEscalate isAuthEscalateresult = new GetisAuthEscalate();
            List<isAuthEscalateLists> isAuthEscalateDetails = new List<isAuthEscalateLists>();
            tempconnectionstring();

            Connection Connection = new Connection();


            Connection.myparameters.Add(new myParameters { ParameterName = "@company", mytype = SqlDbType.NVarChar, Value = VU.company });
            Connection.myparameters.Add(new myParameters { ParameterName = "@position", mytype = SqlDbType.NVarChar, Value = VU.position });
            Connection.myparameters.Add(new myParameters { ParameterName = "@level", mytype = SqlDbType.NVarChar, Value = VU.level });

            DataTable dtuserList = new DataTable();

            dtuserList = Connection.GetDataTable("isAuthEscalate");
            if (dtuserList.Rows.Count > 0)
            {
                foreach (DataRow row in dtuserList.Rows)
                {
                    isAuthEscalateLists SelectSupervisorList = new isAuthEscalateLists
                    {

                        result = row["result"].ToString(),

                    };
                    isAuthEscalateDetails.Add(SelectSupervisorList);
                }
                isAuthEscalateresult.result = isAuthEscalateDetails;
                isAuthEscalateresult.myreturn = "Success";
            }
            else
            {
                isAuthEscalateresult.myreturn = "Error";
            }
            return isAuthEscalateresult;
        }

        //isReviewer return 
        [HttpPost]
        [Route("isReviewer")]
        public GetisReviewer isReviewer(isReviewer VU)
        {

            GetisReviewer isReviewerresult = new GetisReviewer();
            List<isReviewerLists> isReviewerDetails = new List<isReviewerLists>();
            tempconnectionstring();

            Connection Connection = new Connection();


            Connection.myparameters.Add(new myParameters { ParameterName = "@cID", mytype = SqlDbType.NVarChar, Value = VU.cID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@position", mytype = SqlDbType.NVarChar, Value = VU.position });
            Connection.myparameters.Add(new myParameters { ParameterName = "@level", mytype = SqlDbType.NVarChar, Value = VU.level });

            DataTable dtuserList = new DataTable();

            dtuserList = Connection.GetDataTable("isReviewer");
            if (dtuserList.Rows.Count > 0)
            {
                foreach (DataRow row in dtuserList.Rows)
                {
                    isReviewerLists isReviewerList = new isReviewerLists
                    {
                        result = row["result"].ToString(),
                    };
                    isReviewerDetails.Add(isReviewerList);
                }
                isReviewerresult.result = isReviewerDetails;
                isReviewerresult.myreturn = "Success";
            }
            else
            {
                isReviewerresult.myreturn = "Error";
            }
            return isReviewerresult;
        }


        //InsertSupervisor return 
        [HttpPost]
        [Route("InsertSupervisor")]
        public GetInsertSupervisor InsertSupervisor(InsertSupervisor VU)
        {

            GetInsertSupervisor InsertSupervisorresult = new GetInsertSupervisor();
            List<InsertSupervisorLists> InsertSupervisorDetails = new List<InsertSupervisorLists>();
            tempconnectionstring();

            Connection Connection = new Connection();


            Connection.myparameters.Add(new myParameters { ParameterName = "@cID", mytype = SqlDbType.NVarChar, Value = VU.cID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@sup", mytype = SqlDbType.NVarChar, Value = VU.sup });
            Connection.myparameters.Add(new myParameters { ParameterName = "@supLvl", mytype = SqlDbType.NVarChar, Value = VU.supLvl });
            Connection.myparameters.Add(new myParameters { ParameterName = "@supesc", mytype = SqlDbType.NVarChar, Value = VU.supesc });

            DataTable dtuserList = new DataTable();

            dtuserList = Connection.GetDataTable("InsertSupervisor");
            if (dtuserList.Rows.Count > 0)
            {
                foreach (DataRow row in dtuserList.Rows)
                {
                    InsertSupervisorLists InsertSupervisorList = new InsertSupervisorLists
                    {


                    };
                    InsertSupervisorDetails.Add(InsertSupervisorList);
                }
                InsertSupervisorresult.result = InsertSupervisorDetails;
                InsertSupervisorresult.myreturn = "Success";
            }
            else
            {
                InsertSupervisorresult.myreturn = "Error";
            }
            return InsertSupervisorresult;
        }

        //DeleteSupervisor return 
        [HttpPost]
        [Route("DeleteSupervisor")]
        public GetDeleteSupervisor DeleteSupervisor(DeleteSupervisor VU)
        {

            GetDeleteSupervisor DeleteSupervisorresult = new GetDeleteSupervisor();
            List<DeleteSupervisorLists> DeleteSupervisorDetails = new List<DeleteSupervisorLists>();
            tempconnectionstring();

            Connection Connection = new Connection();


            Connection.myparameters.Add(new myParameters { ParameterName = "@cID", mytype = SqlDbType.NVarChar, Value = VU.cID });
          
            DataTable dtuserList = new DataTable();

            dtuserList = Connection.GetDataTable("DeleteSupervisor");
            if (dtuserList.Rows.Count > 0)
            {
                foreach (DataRow row in dtuserList.Rows)
                {
                    DeleteSupervisorLists DeleteSupervisorList = new DeleteSupervisorLists
                    {


                    };
                    DeleteSupervisorDetails.Add(DeleteSupervisorList);
                }
                DeleteSupervisorresult.result = DeleteSupervisorDetails;
                DeleteSupervisorresult.myreturn = "Success";
            }
            else
            {
                DeleteSupervisorresult.myreturn = "Error";
            }
            return DeleteSupervisorresult;
        }

        //SelectSupervisor return 
        [HttpPost]
        [Route("SelectSupervisor")]
        public GetSelectSupervisor SelectSupervisor(SelectSupervisor VU)
        {

            GetSelectSupervisor SelectSupervisorresult = new GetSelectSupervisor();
            List<SelectSupervisorLists> SelectSupervisorDetails = new List<SelectSupervisorLists>();
            tempconnectionstring();

            Connection Connection = new Connection();


            Connection.myparameters.Add(new myParameters { ParameterName = "@cID", mytype = SqlDbType.NVarChar, Value = VU.cID });

            DataTable dtuserList = new DataTable();

            dtuserList = Connection.GetDataTable("SelectSupervisor");
            if (dtuserList.Rows.Count > 0)
            {
                foreach (DataRow row in dtuserList.Rows)
                {
                    SelectSupervisorLists SelectSupervisorList = new SelectSupervisorLists
                    {

                        company = row["company"].ToString(),
                        supervisor = row["supervisor"].ToString(),
                        supervisorLevel = row["supervisorLevel"].ToString(),
                        supEscalate = row["supEscalate"].ToString(),

                    };
                    SelectSupervisorDetails.Add(SelectSupervisorList);
                }
                SelectSupervisorresult.result = SelectSupervisorDetails;
                SelectSupervisorresult.myreturn = "Success";
            }
            else
            {
                SelectSupervisorresult.myreturn = "Error";
            }
            return SelectSupervisorresult;
        }

        //CountNotificationV2 return 
        [HttpPost]
        [Route("CountNotificationV2")]
        public GetCountNotificationV2 CountNotificationV2(CountNotificationV2 VU)
        {

            GetCountNotificationV2 CountNotificationV2result = new GetCountNotificationV2();
            List<CountNotificationV2Lists> CountNotificationV2Details = new List<CountNotificationV2Lists>();
            tempconnectionstring();

            Connection Connection = new Connection();


            Connection.myparameters.Add(new myParameters { ParameterName = "@cID", mytype = SqlDbType.NVarChar, Value = VU.cID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.NVarChar, Value = VU.empID });

            DataTable dtuserList = new DataTable();

            dtuserList = Connection.GetDataTable("CountNotificationV2");
            if (dtuserList.Rows.Count > 0)
            {
                foreach (DataRow row in dtuserList.Rows)
                {
                    CountNotificationV2Lists CountNotificationV2List = new CountNotificationV2Lists
                    {

                        totalNotif = row["totalNotif"].ToString(),
                        rev1Total = row["rev1Total"].ToString(),
                        rev2Total = row["rev2Total"].ToString(),
                        rev1PlanTotal = row["rev1PlanTotal"].ToString(),
                        rev2PlanTotal = row["rev2PlanTotal"].ToString(),
                        rev1PerfReviewer = row["rev1PerfReviewer"].ToString(),
                        rev2PerfReviewer = row["rev2PerfReviewer"].ToString(),
                        escPlan1 = row["escPlan1"].ToString(),
                        escPlan2 = row["escPlan2"].ToString(),
                        escAck1 = row["escAck1"].ToString(),
                        escAck2 = row["escAck2"].ToString(),


                    };
                    CountNotificationV2Details.Add(CountNotificationV2List);
                }
                CountNotificationV2result.result = CountNotificationV2Details;
                CountNotificationV2result.myreturn = "Success";
            }
            else
            {
                CountNotificationV2result.myreturn = "Error";
            }
            return CountNotificationV2result;
        }

        //CountPerfView return 
        [HttpPost]
        [Route("CountPerfView")]
        public GetCountPerfView CountPerfView(CountPerfView VU)
        {

            GetCountPerfView CountPerfViewresult = new GetCountPerfView();
            List<CountPerfViewLists> CountPerfViewDetails = new List<CountPerfViewLists>();
            tempconnectionstring();

            Connection Connection = new Connection();


            Connection.myparameters.Add(new myParameters { ParameterName = "@cID", mytype = SqlDbType.NVarChar, Value = VU.cID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.NVarChar, Value = VU.empID });

            DataTable dtuserList = new DataTable();

            dtuserList = Connection.GetDataTable("CountPerfView");
            if (dtuserList.Rows.Count > 0)
            {
                foreach (DataRow row in dtuserList.Rows)
                {
                    CountPerfViewLists CountPerfViewList = new CountPerfViewLists
                    {

                        notifCount = row["notifCount"].ToString(),

                    };
                    CountPerfViewDetails.Add(CountPerfViewList);
                }
                CountPerfViewresult.result = CountPerfViewDetails;
                CountPerfViewresult.myreturn = "Success";
            }
            else
            {
                CountPerfViewresult.myreturn = "Error";
            }
            return CountPerfViewresult;
        }

        //GetAckData return 
        [HttpPost]
        [Route("AckDataResult")]
        public GetAckDataResult ApraisalAckDataResult(GetAckData getAct)
        {
            
            GetAckDataResult GetAckDataResult = new GetAckDataResult();
            List<AckDataResult> ActDetails = new List<AckDataResult>();
            tempconnectionstring();
            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@group_id", mytype = SqlDbType.NVarChar, Value = getAct.group_id });
            Connection.myparameters.Add(new myParameters { ParameterName = "@cID", mytype = SqlDbType.NVarChar, Value = getAct.cID });

            DataTable dtuserList = new DataTable();

            dtuserList = Connection.GetDataTable("GetAckData");
            if (dtuserList.Rows.Count > 0)
            {
                foreach (DataRow row in dtuserList.Rows)
                {
                    AckDataResult AckDataresults = new AckDataResult
                    {
                        id = row["id"].ToString(),
                        group_id = row["group_id"].ToString(),
                        ack_lvl = row["ack_lvl"].ToString(),
                        status = row["status"].ToString(),
                        creator_id = row["creator_id"].ToString(),
                        reviewer1 = row["reviewer1"].ToString(),
                        reviewer2 = row["reviewer2"].ToString(),
                        date_req = row["date_req"].ToString(),
                        date_rev1 = row["date_rev1"].ToString(),
                        date_rev2 = row["date_rev2"].ToString(),
                        date_done = row["date_done"].ToString(),
                        escalated = row["escalated"].ToString(),
                        modified_by = row["modified_by"].ToString()
                    };
                    ActDetails.Add(AckDataresults);
                }
                GetAckDataResult.result = ActDetails;
                GetAckDataResult.myreturn = "Success";
            }
            else
            {
                GetAckDataResult.myreturn = "Error";
            }
            return GetAckDataResult;
        }
        //GetAckPlanData return 
        [HttpPost]
        [Route("GetAckPlanData")]
        public GetAckPlan ApraisalAckDataResult(GetAckPlanData getAct)
        {

            GetAckPlan GetAckDataResult = new GetAckPlan();
            List<AckPlanDataResultList> GetAckPlanDataDetails = new List<AckPlanDataResultList>();
            tempconnectionstring();
            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@cID", mytype = SqlDbType.NVarChar, Value = getAct.cID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@group_id", mytype = SqlDbType.NVarChar, Value = getAct.group_id });

            DataTable dtuserList = new DataTable();

            dtuserList = Connection.GetDataTable("GetAckPlanData");
            if (dtuserList.Rows.Count > 0)
            {
                foreach (DataRow row in dtuserList.Rows)
                {
                    AckPlanDataResultList AckPlanDataResultLists = new AckPlanDataResultList
                    {
                        id = row["id"].ToString(),
                        group_id = row["group_id"].ToString(),
                        ack_lvl = row["ack_lvl"].ToString(),
                        status = row["status"].ToString(),
                        creator_id = row["creator_id"].ToString(),
                        reviewer1 = row["reviewer1"].ToString(),
                        reviewer2 = row["reviewer2"].ToString(),
                        date_req = row["date_req"].ToString(),
                        date_rev1 = row["date_rev1"].ToString(),
                        date_rev2 = row["date_rev2"].ToString(),
                        date_done = row["date_done"].ToString(),
                        escalated = row["escalated"].ToString(),
                    };
                    GetAckPlanDataDetails.Add(AckPlanDataResultLists);
                }
                GetAckDataResult.result = GetAckPlanDataDetails;
                GetAckDataResult.myreturn = "Success";
            }
            else
            {
                GetAckDataResult.myreturn = "Error";
            }
            return GetAckDataResult;
        }



        //GetComGoals return 
        [HttpPost]
        [Route("GetComGoalsGroup")]
        public GetComGoals ApraisalGetComGoalsresult(GetComGoalsGroup getAct)
        {
            

            GetComGoals GetComGoalsresult = new GetComGoals();
            List<GetComGoalsLists> ComGoalDetails = new List<GetComGoalsLists>();
            tempconnectionstring();
            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@ID", mytype = SqlDbType.NVarChar, Value = getAct.ID });

            DataTable dtuserList = new DataTable();

            dtuserList = Connection.GetDataTable("GetComGoalsGroup");
            if (dtuserList.Rows.Count > 0)
            {
                foreach (DataRow row in dtuserList.Rows)
                {
                    GetComGoalsLists GetComGoalsLists = new GetComGoalsLists
                    {
                        id = row["id"].ToString(),
                        group_id = row["group_id"].ToString(),
                        goal_name = row["goal_name"].ToString(),
                        goal_desc = row["goal_desc"].ToString(),
                        result = row["result"].ToString(),
                        weight = row["weight"].ToString(),
                        emp_comment = row["emp_comment"].ToString(),
                        r1_str_com = row["r1_str_com"].ToString(),
                        r1_dnp_com = row["r1_dnp_com"].ToString(),
                        r1_date_created = row["r1_date_created"].ToString(),
                        r2_str_com = row["r2_str_com"].ToString(),
                        r2_dnp_com = row["r2_dnp_com"].ToString(),
                        r2_date_created = row["r2_date_created"].ToString(),
                    };
                    ComGoalDetails.Add(GetComGoalsLists);
                }
                GetComGoalsresult.result = ComGoalDetails;
                GetComGoalsresult.myreturn = "Success";
            }
            else
            {
                GetComGoalsresult.myreturn = "Error";
            }
            return GetComGoalsresult;
        }

        //validuse return 
        [HttpPost]
        [Route("ValidateUser")]
        public GetValidUsers ApraisalGetValidUser(ValidateUser VU)
        {
            GetValidUsers validUseresult = new GetValidUsers();
            List<GetValidUserLists> ValiduserDetails = new List<GetValidUserLists>();
            tempconnectionstring();

            Connection Connection = new Connection();
            Connection.myparameters.Add(new myParameters { ParameterName = "@company", mytype = SqlDbType.NVarChar, Value = VU.company });
            Connection.myparameters.Add(new myParameters { ParameterName = "@empId", mytype = SqlDbType.NVarChar, Value = VU.empId });
          

            DataTable dtuserList = new DataTable();

            dtuserList = Connection.GetDataTable("ValidateUser");
            if (dtuserList.Rows.Count > 0)
            {
                foreach (DataRow row in dtuserList.Rows)
                {
                    GetValidUserLists ValidUserLists = new GetValidUserLists
                    {
                        EmpID = row["EmpID"].ToString(),
                        First_Name = row["Emp_FName"].ToString(),
                        Middle_Name = row["Emp_MName"].ToString(),
                        Last_Name = row["Emp_LName"].ToString(),
                        EmpLevel = row["EmpLevel"].ToString(),
                        MngrID = row["ImSup_ID"].ToString(),
                        MngrName = row["ImSup"].ToString(),
                
                    };
                    ValiduserDetails.Add(ValidUserLists);
                }
                validUseresult.result = ValiduserDetails;
                validUseresult.myreturn = "Success";
            }
            else
            {
                validUseresult.myreturn = "Error";
            }
            return validUseresult;
        }
        [HttpPost]
        [Route("ValidateUserV2")]
        public string ApraisalGetValidUserv2(ValidateUser VU)
        {
            try
            {
                tempconnectionstring();

                Connection Connection = new Connection();
                Connection.myparameters.Add(new myParameters { ParameterName = "@CN", mytype = SqlDbType.NVarChar, Value = VU.company });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Username", mytype = SqlDbType.NVarChar, Value = VU.empId });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Password", mytype = SqlDbType.NVarChar, Value = VU.password });

                return Connection.ExecuteScalar("sp_PerfApp_Login").ToString();
            }
            catch (Exception e)
            {
                return e.ToString();
            }
            //GetValidUsers validUseresult = new GetValidUsers();
            //List<GetValidUserLists> ValiduserDetails = new List<GetValidUserLists>();
            //try
            //{
            //    tempconnectionstring();

            //    Connection Connection = new Connection();
            //    Connection.myparameters.Add(new myParameters { ParameterName = "@CN", mytype = SqlDbType.NVarChar, Value = VU.company });
            //    Connection.myparameters.Add(new myParameters { ParameterName = "@Username", mytype = SqlDbType.NVarChar, Value = VU.empId });
            //    Connection.myparameters.Add(new myParameters { ParameterName = "@Password", mytype = SqlDbType.NVarChar, Value = VU.password });

            //    DataTable dtuserList = Connection.GetDataTable("sp_PerfApp_Login");

            //    foreach (DataRow row in dtuserList.Rows)
            //    {
            //        GetValidUserLists ValidUserLists = new GetValidUserLists
            //        {
                     
            //            ClientID = row["ClientID"].ToString(),
            //            EmpID = row["EmpID"].ToString(),
            //            First_Name = row["First_Name"].ToString(),
            //            Middle_Name = row["Middle_Name"].ToString(),
            //            Last_Name = row["Last_Name"].ToString(),
            //            EmpLevel = row["EmpLevel"].ToString(),
            //            MngrID = row["MngrID"].ToString(),
            //            MngrName = row["MngrName"].ToString(),
                 
            //        };
            //        ValiduserDetails.Add(ValidUserLists);
            //    }
            //    validUseresult.myreturn = "Success";
            //}
            //catch (Exception e)
            //{
            //    validUseresult.result = ValiduserDetails;
            //    validUseresult.myreturn = e.ToString();
            //}
            //return validUseresult;
        }

        //validuse return 
        [HttpPost]
        [Route("ValidateUserId")]
        public GetValidUsersId ApraisalGetValidUserId(ValidateUserId VU)
        {

            GetValidUsersId validUseresult = new GetValidUsersId();
            List<GetValidUserListsId> ValiduserDetails = new List<GetValidUserListsId>();
            tempconnectionstring();

            Connection Connection = new Connection();
            Connection.myparameters.Add(new myParameters { ParameterName = "@ID", mytype = SqlDbType.NVarChar, Value = VU.ID });

            DataTable dtuserList = new DataTable();

            dtuserList = Connection.GetDataTable("pa_CheckEmpAccount");
            if (dtuserList.Rows.Count > 0)
            {
                foreach (DataRow row in dtuserList.Rows)
                {
                    GetValidUserListsId ValidUserListsId = new GetValidUserListsId
                    {
                        Emp_ID = row["Emp_ID"].ToString(),
                        First_Name = row["First_Name"].ToString(),
                        Middle_Name = row["Middle_Name"].ToString(),
                        Last_Name = row["Last_Name"].ToString(),
                        EmpLevel = row["EmpLevel"].ToString(),
                        MngrID = row["MngrID"].ToString(),
                        MngrName = row["ImSup_Name"].ToString()

                    };
                    ValiduserDetails.Add(ValidUserListsId);
                }
                validUseresult.result = ValiduserDetails;
                validUseresult.myreturn = "Success";
            }
            else
            {
                validUseresult.myreturn = "Error";
            }
            return validUseresult;
        }


        //validuse return 
        [HttpPost]
        [Route("GetUpperHierarchy")]
        public GetUpperHierarchy AppraisalGetUpperHierarchy(UpperHierarchy VU)
        {

            GetUpperHierarchy UpperHierarchyresult = new GetUpperHierarchy();
            List<GetUpperHierarchyLists> ValiduserDetails = new List<GetUpperHierarchyLists>();
            tempconnectionstring();

            Connection Connection = new Connection();
            Connection.myparameters.Add(new myParameters { ParameterName = "@Emp_ID", mytype = SqlDbType.NVarChar, Value = VU.Emp_ID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@cID", mytype = SqlDbType.NVarChar, Value = VU.cID });

            DataTable dtuserList = new DataTable();

            dtuserList = Connection.GetDataTable("sp_GetUpperHierarchy");
            if (dtuserList.Rows.Count > 0)
            {
                foreach (DataRow row in dtuserList.Rows)
                {
                    GetUpperHierarchyLists UpperHierarchy = new GetUpperHierarchyLists
                    {
                        Emp_ID = row["Emp_ID"].ToString(),
                        ImSup_ID = row["ImSup_ID"].ToString(),
                    };
                    ValiduserDetails.Add(UpperHierarchy);
                }
                UpperHierarchyresult.result = ValiduserDetails;
                UpperHierarchyresult.myreturn = "Success";
            }
            else
            {
                UpperHierarchyresult.myreturn = "Error";
            }
            return UpperHierarchyresult;
        }

        //EscalateReviewer return 
        [HttpPost]
        [Route("EscalateReviewer")]
        public GetEscalateReviewer EscalateReviewer(EscalateReviewer VU)
        {

            GetEscalateReviewer EscalateReviewerresult = new GetEscalateReviewer();
            List<EscalateReviewerLists> EscalateReviewerDetails = new List<EscalateReviewerLists>();
            tempconnectionstring();

            Connection Connection = new Connection();


            Connection.myparameters.Add(new myParameters { ParameterName = "@cID", mytype = SqlDbType.NVarChar, Value = VU.cID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@emp", mytype = SqlDbType.NVarChar, Value = VU.emp });

            DataTable dtuserList = new DataTable();

            dtuserList = Connection.GetDataTable("EscalateReviewer");
            if (dtuserList.Rows.Count > 0)
            {
                foreach (DataRow row in dtuserList.Rows)
                {
                    EscalateReviewerLists EscalateReviewerList = new EscalateReviewerLists
                    {

                        id = row["id"].ToString(),
                        esc = row["esc"].ToString(),
                        getmessage = row["getmessage"].ToString(),
                        

                
                    };
                    EscalateReviewerDetails.Add(EscalateReviewerList);
                }
                EscalateReviewerresult.result = EscalateReviewerDetails;
                EscalateReviewerresult.myreturn = "Success";
            }
            else
            {
                EscalateReviewerresult.myreturn = "Error";
            }
            return EscalateReviewerresult;
        }

        //SelectEmpDetails return 
        [HttpPost]
        [Route("SelectEmpDetails")]
        public GetSelectEmpDetails SelectEmpDetails(SelectEmpDetails VU)
        {

            GetSelectEmpDetails SelectEmpDetailsresult = new GetSelectEmpDetails();
            List<SelectEmpDetailsLists> SelectEmpDetailsDetails = new List<SelectEmpDetailsLists>();
            tempconnectionstring();

            Connection Connection = new Connection();


            Connection.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.NVarChar, Value = VU.empID });

            DataTable dtuserList = new DataTable();

            dtuserList = Connection.GetDataTable("SelectEmpDetails");
            if (dtuserList.Rows.Count > 0)
            {
                foreach (DataRow row in dtuserList.Rows)
                {
                    SelectEmpDetailsLists SelectEmpDetailsList = new SelectEmpDetailsLists
                    {

                        clientID = row["clientID"].ToString(),
                        EmpID = row["EmpID"].ToString(),
                        First_Name = row["First_Name"].ToString(),
                        Middle_Name = row["Middle_Name"].ToString(),
                        Last_Name = row["Last_Name"].ToString(),
                        EmpLevel = row["EmpLevel"].ToString(),
                        MngrID = row["MngrID"].ToString(),
                        MngrName = row["MngrName"].ToString(),
                        username = row["username"].ToString(),
                        password = row["password"].ToString(),
                        InActiveDate = row["InActiveDate"].ToString(),

                    };
                    SelectEmpDetailsDetails.Add(SelectEmpDetailsList);
                }
                SelectEmpDetailsresult.result = SelectEmpDetailsDetails;
                SelectEmpDetailsresult.myreturn = "Success";
            }
            else
            {
                SelectEmpDetailsresult.myreturn = "Error";
            }
            return SelectEmpDetailsresult;
        }

        //InitDataCompany return 
        [HttpPost]
        [Route("InitDataCompany")]
        public GetInitDataCompany InitDataCompany(InitDataCompany VU)
        {

            GetInitDataCompany InitDataCompanyresult = new GetInitDataCompany();
            List<InitDataCompanyLists> InitDataCompanyDetails = new List<InitDataCompanyLists>();
            tempconnectionstring();

            Connection Connection = new Connection();


            Connection.myparameters.Add(new myParameters { ParameterName = "@cID", mytype = SqlDbType.NVarChar, Value = VU.cID });

            DataTable dtuserList = new DataTable();

            dtuserList = Connection.GetDataTable("InitDataCompany");
            if (dtuserList.Rows.Count > 0)
            {
                foreach (DataRow row in dtuserList.Rows)
                {
                    InitDataCompanyLists InitDataCompanyList = new InitDataCompanyLists
                    {
                        

                    };
                    InitDataCompanyDetails.Add(InitDataCompanyList);
                }
                InitDataCompanyresult.result = InitDataCompanyDetails;
                InitDataCompanyresult.myreturn = "Success";
            }
            else
            {
                InitDataCompanyresult.myreturn = "Error";
            }
            return InitDataCompanyresult;
        }


        //GetAllRev1Plan return 
        [HttpPost]
        [Route("GetAllRev1Plan")]
        public GetGetAllRev1Plan GetAllRev1Plan(GetAllRev1Plan VU)
        {

            GetGetAllRev1Plan GetAllRev1Planresult = new GetGetAllRev1Plan();
            List<GetAllRev1PlanLists> GetAllRev1PlanDetails = new List<GetAllRev1PlanLists>();
            tempconnectionstring();

            Connection Connection = new Connection();


            Connection.myparameters.Add(new myParameters { ParameterName = "@cID", mytype = SqlDbType.NVarChar, Value = VU.cID });

            DataTable dtuserList = new DataTable();

            dtuserList = Connection.GetDataTable("GetAllRev1Plan");
            if (dtuserList.Rows.Count > 0)
            {
                foreach (DataRow row in dtuserList.Rows)
                {
                    GetAllRev1PlanLists EscalateReviewerList = new GetAllRev1PlanLists
                    {
                        creator_id = row["creator_id"].ToString(),
                        escalated = row["escalated"].ToString(),


                    };
                    GetAllRev1PlanDetails.Add(EscalateReviewerList);
                }
                GetAllRev1Planresult.result = GetAllRev1PlanDetails;
                GetAllRev1Planresult.myreturn = "Success";
            }
            else
            {
                GetAllRev1Planresult.myreturn = "Error";
            }
            return GetAllRev1Planresult;
        }


        //CountNotifRev return 
        [HttpPost]
        [Route("CountNotifRev2")]
        public GetCountNotifRev2 CountNotifRev2(CountNotifRev2 VU)
        {

            GetCountNotifRev2 CountNotifRevresult = new GetCountNotifRev2();
            List<CountNotifRev2Lists> CountNotifRevDetails = new List<CountNotifRev2Lists>();
            tempconnectionstring();

            Connection Connection = new Connection();


            Connection.myparameters.Add(new myParameters { ParameterName = "@cID", mytype = SqlDbType.NVarChar, Value = VU.cID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.NVarChar, Value = VU.empID });

            DataTable dtuserList = new DataTable();

            dtuserList = Connection.GetDataTable("CountNotifRev2");
            if (dtuserList.Rows.Count > 0)
            {
                foreach (DataRow row in dtuserList.Rows)
                {
                    CountNotifRev2Lists CountNotifRevList = new CountNotifRev2Lists
                    {
                        
                        notifCountRev2 = row["notifCountRev2"].ToString(),


                    };
                    CountNotifRevDetails.Add(CountNotifRevList);
                }
                CountNotifRevresult.result = CountNotifRevDetails;
                CountNotifRevresult.myreturn = "Success";
            }
            else
            {
                CountNotifRevresult.myreturn = "Error";
            }
            return CountNotifRevresult;
        }

        //CountNotifRev return 
        [HttpPost]
        [Route("CountNotifRev1")]
        public GetCountNotifRev1 CountNotifRev1(CountNotifRev1 VU)
        {

            GetCountNotifRev1 CountNotifRev1result = new GetCountNotifRev1();
            List<CountNotifRev1Lists> CountNotifRev1Details = new List<CountNotifRev1Lists>();
            tempconnectionstring();

            Connection Connection = new Connection();


            Connection.myparameters.Add(new myParameters { ParameterName = "@cID", mytype = SqlDbType.NVarChar, Value = VU.cID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.NVarChar, Value = VU.empID });

            DataTable dtuserList = new DataTable();

            dtuserList = Connection.GetDataTable("CountNotifRev1");
            if (dtuserList.Rows.Count > 0)
            {
                foreach (DataRow row in dtuserList.Rows)
                {
                    CountNotifRev1Lists CountNotifRevList = new CountNotifRev1Lists
                    {

                        notifCountRev1 = row["notifCountRev1"].ToString(),


                    };
                    CountNotifRev1Details.Add(CountNotifRevList);
                }
                CountNotifRev1result.result = CountNotifRev1Details;
                CountNotifRev1result.myreturn = "Success";
            }
            else
            {
                CountNotifRev1result.myreturn = "Error";
            }
            return CountNotifRev1result;
        }

        //CountRev1Plan return Modified by Ren
        [HttpPost]
        [Route("CountRev1Plan")]
        public GetCountRev1Plan CountRev1Plan(CountRev1Plan VU)
        {

            GetCountRev1Plan CountRev1Planresult = new GetCountRev1Plan();
            List<CountRev1PlanLists> CountRev1PlanDetails = new List<CountRev1PlanLists>();
            tempconnectionstring();

            Connection Connection = new Connection();


            Connection.myparameters.Add(new myParameters { ParameterName = "@cID", mytype = SqlDbType.NVarChar, Value = VU.cID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.NVarChar, Value = VU.empID });

            DataTable dtuserList = new DataTable();

            dtuserList = Connection.GetDataTable("CountRev1Plan");
            if (dtuserList.Rows.Count > 0)
            {
                foreach (DataRow row in dtuserList.Rows)
                {
                    CountRev1PlanLists CountRev1PlanList = new CountRev1PlanLists
                    {

                        notifcount = row["notifcount"].ToString(),


                    };
                    CountRev1PlanDetails.Add(CountRev1PlanList);
                }
                CountRev1Planresult.result = CountRev1PlanDetails;
                CountRev1Planresult.myreturn = "Success";
            }
            else
            {
                CountRev1Planresult.myreturn = "Error";
            }
            return CountRev1Planresult;
        }

        //CountRev2Plan return modified by REN
        [HttpPost]
        [Route("CountRev2Plan")]
        public GetCountRev2Plan CountRev2Plan(CountRev2Plan VU)
        {

            GetCountRev2Plan CountRev2Planresult = new GetCountRev2Plan();
            List<CountRev2PlanLists> CountRev2PlanDetails = new List<CountRev2PlanLists>();
            tempconnectionstring();

            Connection Connection = new Connection();


            Connection.myparameters.Add(new myParameters { ParameterName = "@cID", mytype = SqlDbType.NVarChar, Value = VU.cID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.NVarChar, Value = VU.empID });

            DataTable dtuserList = new DataTable();

            dtuserList = Connection.GetDataTable("CountRev2Plan");
            if (dtuserList.Rows.Count > 0)
            {
                foreach (DataRow row in dtuserList.Rows)
                {
                    CountRev2PlanLists CountRev2PlanList = new CountRev2PlanLists
                    {

                        notifcount = row["notifcount"].ToString(),


                    };
                    CountRev2PlanDetails.Add(CountRev2PlanList);
                }
                CountRev2Planresult.result = CountRev2PlanDetails;
                CountRev2Planresult.myreturn = "Success";
            }
            else
            {
                CountRev2Planresult.myreturn = "Error";
            }
            return CountRev2Planresult;
        }


        //CountRev1Reviewer return 
        [HttpPost]
        [Route("CountRev1Reviewer")]
        public GetCountRev1Reviewer CountRev1Reviewer(CountRev1Reviewer VU)
        {

            GetCountRev1Reviewer CountRev1Reviewerresult = new GetCountRev1Reviewer();
            List<CountRev1ReviewerLists> CountRev1ReviewerDetails = new List<CountRev1ReviewerLists>();
            tempconnectionstring();

            Connection Connection = new Connection();


            Connection.myparameters.Add(new myParameters { ParameterName = "@cID", mytype = SqlDbType.NVarChar, Value = VU.cID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.NVarChar, Value = VU.empID });

            DataTable dtuserList = new DataTable();

            dtuserList = Connection.GetDataTable("CountRev1Reviewer");
            if (dtuserList.Rows.Count > 0)
            {
                foreach (DataRow row in dtuserList.Rows)
                {
                    CountRev1ReviewerLists CountRev1ReviewerList = new CountRev1ReviewerLists
                    {

                        notifcount = row["notifcount"].ToString(),


                    };
                    CountRev1ReviewerDetails.Add(CountRev1ReviewerList);
                }
                CountRev1Reviewerresult.result = CountRev1ReviewerDetails;
                CountRev1Reviewerresult.myreturn = "Success";
            }
            else
            {
                CountRev1Reviewerresult.myreturn = "Error";
            }
            return CountRev1Reviewerresult;
        }

        //CountRev2Reviewer return modified by Ren
        [HttpPost]
        [Route("CountRev2Reviewer")]
        public GetCountRev2Reviewer CountRev2Reviewer(CountRev2Reviewer VU)
        {

            GetCountRev2Reviewer CountRev2Reviewerresult = new GetCountRev2Reviewer();
            List<CountRev2ReviewerLists> CountRev2ReviewerDetails = new List<CountRev2ReviewerLists>();
            tempconnectionstring();

            Connection Connection = new Connection();


            Connection.myparameters.Add(new myParameters { ParameterName = "@cID", mytype = SqlDbType.NVarChar, Value = VU.cID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.NVarChar, Value = VU.empID });

            DataTable dtuserList = new DataTable();

            dtuserList = Connection.GetDataTable("CountRev2Reviewer");
            if (dtuserList.Rows.Count > 0)
            {
                foreach (DataRow row in dtuserList.Rows)
                {
                    CountRev2ReviewerLists CountRev1ReviewerList = new CountRev2ReviewerLists
                    {

                        notifcount = row["notifcount"].ToString(),


                    };
                    CountRev2ReviewerDetails.Add(CountRev1ReviewerList);
                }
                CountRev2Reviewerresult.result = CountRev2ReviewerDetails;
                CountRev2Reviewerresult.myreturn = "Success";
            }
            else
            {
                CountRev2Reviewerresult.myreturn = "Error";
            }
            return CountRev2Reviewerresult;
        }



        //CountNotificationPerfAppraisal return 
        [HttpPost]
        [Route("CountNotificationPerfAppraisal")]
        public GetCountNotificationPerfAppraisal CountNotificationPerfAppraisal(CountNotificationPerfAppraisal VU)
        {

            GetCountNotificationPerfAppraisal CountNotificationPerfAppraisalresult = new GetCountNotificationPerfAppraisal();
            List<CountNotificationPerfAppraisalLists> CountNotificationPerfAppraisalDetails = new List<CountNotificationPerfAppraisalLists>();
            tempconnectionstring();

            Connection Connection = new Connection();


            Connection.myparameters.Add(new myParameters { ParameterName = "@cID", mytype = SqlDbType.NVarChar, Value = VU.cID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.NVarChar, Value = VU.empID });

            DataTable dtuserList = new DataTable();

            dtuserList = Connection.GetDataTable("CountNotificationPerfAppraisal");
            if (dtuserList.Rows.Count > 0)
            {
                foreach (DataRow row in dtuserList.Rows)
                {
                    CountNotificationPerfAppraisalLists CountNotificationPerfAppraisalList = new CountNotificationPerfAppraisalLists
                    {

                        notifCount = row["notifCount"].ToString(),


                    };
                    CountNotificationPerfAppraisalDetails.Add(CountNotificationPerfAppraisalList);
                }
                CountNotificationPerfAppraisalresult.result = CountNotificationPerfAppraisalDetails;
                CountNotificationPerfAppraisalresult.myreturn = "Success";
            }
            else
            {
                CountNotificationPerfAppraisalresult.myreturn = "Error";
            }
            return CountNotificationPerfAppraisalresult;
        }


        //GetAllRev1 return 
        [HttpPost]
        [Route("GetAllRev1")]
        public GetGetAllRev1 GetAllRev1(GetAllRev1 VU)
        {

            GetGetAllRev1 GetAllRev1result = new GetGetAllRev1();
            List<GetAllRev1Lists> GetAllRev1Details = new List<GetAllRev1Lists>();
            tempconnectionstring();

            Connection Connection = new Connection();


            Connection.myparameters.Add(new myParameters { ParameterName = "@cID", mytype = SqlDbType.NVarChar, Value = VU.cID });

            DataTable dtuserList = new DataTable();

            dtuserList = Connection.GetDataTable("GetAllRev1");
            if (dtuserList.Rows.Count > 0)
            {
                foreach (DataRow row in dtuserList.Rows)
                {
                    GetAllRev1Lists GetAllRev1List = new GetAllRev1Lists
                    {
                        ack_lvl = row["ack_lvl"].ToString(),
                        creator_id = row["creator_id"].ToString(),
                        escalated = row["escalated"].ToString(),


                    };
                    GetAllRev1Details.Add(GetAllRev1List);
                }
                GetAllRev1result.result = GetAllRev1Details;
                GetAllRev1result.myreturn = "Success";
            }
            else
            {
                GetAllRev1result.myreturn = "Error";
            }
            return GetAllRev1result;
        }

        //GetAllRev2 return 
        [HttpPost]
        [Route("GetAllRev2")]
        public GetGetAllRev2 GetAllRev2(GetAllRev2 VU)
        {

            GetGetAllRev2 GetAllRev2result = new GetGetAllRev2();
            List<GetAllRev2Lists> GetAllRev2Details = new List<GetAllRev2Lists>();
            tempconnectionstring();

            Connection Connection = new Connection();


            Connection.myparameters.Add(new myParameters { ParameterName = "@cID", mytype = SqlDbType.NVarChar, Value = VU.cID });

            DataTable dtuserList = new DataTable();

            dtuserList = Connection.GetDataTable("GetAllRev2");
            if (dtuserList.Rows.Count > 0)
            {
                foreach (DataRow row in dtuserList.Rows)
                {
                    GetAllRev2Lists GetAllRev2List = new GetAllRev2Lists
                    {
                        ack_lvl = row["ack_lvl"].ToString(),
                        creator_id = row["creator_id"].ToString(),
                        escalated = row["escalated"].ToString(),


                    };
                    GetAllRev2Details.Add(GetAllRev2List);
                }
                GetAllRev2result.result = GetAllRev2Details;
                GetAllRev2result.myreturn = "Success";
            }
            else
            {
                GetAllRev2result.myreturn = "Error";
            }
            return GetAllRev2result;
        }
        //ADDED BY RHEN
        [HttpPost]
        [Route("CountNotificationV3")]
        public ClassCountNotif CountNotif(CountParams param)
        {

            ClassCountNotif GetCompanyName = new ClassCountNotif();
            List<CountNotifReturns> returnNotif = new List<CountNotifReturns>();
            tempconnectionstring();
            Connection Connection = new Connection();
            Connection.myparameters.Add(new myParameters { ParameterName = "@cID", mytype = SqlDbType.NVarChar, Value = param.cID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.NVarChar, Value = param.empID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@datetoday", mytype = SqlDbType.NVarChar, Value = param.datetoday });


            DataTable dtuserList = new DataTable();
            dtuserList = Connection.GetDataTable("CountNotificationV3");
            if (dtuserList.Rows.Count > 0)
            {
                foreach (DataRow row in dtuserList.Rows)
                {
                    CountNotifReturns NotifCount = new CountNotifReturns
                    {
                        totalNotif = row["totalNotif"].ToString(),
                        reviewer1 = row["reviewer1"].ToString(),
                        reviewer2 = row["reviewer2"].ToString(),

                    };
                    returnNotif.Add(NotifCount);
                }
                GetCompanyName.returnCountNotif = returnNotif;
                GetCompanyName.myreturn = "Success";
            }
            else
            {
                GetCompanyName.myreturn = "Error";
            }
            return GetCompanyName;
        }

        //ADDED BY RHEN
        [HttpPost]
        [Route("NotifReviewer1")]
        public ClassRev1Count CountRev1(CountParams param)
        {

            ClassRev1Count GetRev1Count = new ClassRev1Count();
            List<CountRev1NotifReturns> returnNotif = new List<CountRev1NotifReturns>();
            tempconnectionstring();
            Connection Connection = new Connection();
            Connection.myparameters.Add(new myParameters { ParameterName = "@cID", mytype = SqlDbType.NVarChar, Value = param.cID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.NVarChar, Value = param.empID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@datetoday", mytype = SqlDbType.NVarChar, Value = param.datetoday });


            DataTable dtuserList = new DataTable();
            dtuserList = Connection.GetDataTable("NotifReviewer1");
            if (dtuserList.Rows.Count > 0)
            {
                foreach (DataRow row in dtuserList.Rows)
                {
                    CountRev1NotifReturns NotifCount = new CountRev1NotifReturns
                    {
                        Reviewer1 = row["Reviewer1"].ToString()

                    };
                    returnNotif.Add(NotifCount);
                }
                GetRev1Count.returnRev1 = returnNotif;
                GetRev1Count.myreturn = "Success";
            }
            else
            {
                GetRev1Count.myreturn = "Error";
            }
            return GetRev1Count;
        }

        //ADDED BY RHEN
        [HttpPost]
        [Route("NotifReviewer2")]
        public ClassRev2Count CountNotifRev2(CountParams param)
        {

            ClassRev2Count GetNotif2 = new ClassRev2Count();
            List<CountRev2NotifReturns> returnNotif2 = new List<CountRev2NotifReturns>();
            tempconnectionstring();
            Connection Connection = new Connection();
            Connection.myparameters.Add(new myParameters { ParameterName = "@cID", mytype = SqlDbType.NVarChar, Value = param.cID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.NVarChar, Value = param.empID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@datetoday", mytype = SqlDbType.NVarChar, Value = param.datetoday });


            DataTable dtuserList = new DataTable();
            dtuserList = Connection.GetDataTable("NotifReviewer2");
            if (dtuserList.Rows.Count > 0)
            {
                foreach (DataRow row in dtuserList.Rows)
                {
                    CountRev2NotifReturns NotifCount2 = new CountRev2NotifReturns
                    {
                        
                        Reviewer2 = row["Reviewer2"].ToString()

                    };
                    returnNotif2.Add(NotifCount2);
                }
                GetNotif2.returnRev2 = returnNotif2;
                GetNotif2.myreturn = "Success";
            }
            else
            {
                GetNotif2.myreturn = "Error";
            }
            return GetNotif2;
        }


        //GetAllRev2Plan return 
        [HttpPost]
        [Route("GetAllRev2Plan")]
        public GetGetAllNullRev1Plan GetAllNullRev1Plan(GetAllNullRev1Plan VU)
        {

            GetGetAllNullRev1Plan GetAllNullRev1Planresult = new GetGetAllNullRev1Plan();
            List<GetAllNullRev1PlanLists> GetAllNullRev1PlanDetails = new List<GetAllNullRev1PlanLists>();
            tempconnectionstring();

            Connection Connection = new Connection();


            Connection.myparameters.Add(new myParameters { ParameterName = "@cID", mytype = SqlDbType.NVarChar, Value = VU.cID });

            DataTable dtuserList = new DataTable();

            dtuserList = Connection.GetDataTable("GetAllRev2Plan");
            if (dtuserList.Rows.Count > 0)
            {
                foreach (DataRow row in dtuserList.Rows)
                {
                    GetAllNullRev1PlanLists GetAllNullRev1PlanList = new GetAllNullRev1PlanLists
                    {

                        creator_id = row["creator_id"].ToString(),
                        escalated = row["escalated"].ToString(),


                    };
                    GetAllNullRev1PlanDetails.Add(GetAllNullRev1PlanList);
                }
                GetAllNullRev1Planresult.result = GetAllNullRev1PlanDetails;
                GetAllNullRev1Planresult.myreturn = "Success";
            }
            else
            {
                GetAllNullRev1Planresult.myreturn = "Error";
            }
            return GetAllNullRev1Planresult;
        }

        //GetGoalPlan return 
        [HttpPost]
        [Route("GetGoalPlan")]
        public GetGetGoalPlan AppraisalGetGoalPlan(GetGoalPlan VU)
        {

            GetGetGoalPlan GetGoalPlanresult = new GetGetGoalPlan();
            List<GetGoalPlanLists> GetGoalPlanDetails = new List<GetGoalPlanLists>();
            tempconnectionstring();

            Connection Connection = new Connection();
            Connection.myparameters.Add(new myParameters { ParameterName = "@ID", mytype = SqlDbType.NVarChar, Value = VU.ID });

            DataTable dtuserList = new DataTable();

            dtuserList = Connection.GetDataTable("GetGoalPlan");
            if (dtuserList.Rows.Count > 0)
            {
                foreach (DataRow row in dtuserList.Rows)
                {
                    GetGoalPlanLists getGoalPlan = new GetGoalPlanLists
                    {
                        id = row["id"].ToString(),
                        group_id = row["group_id"].ToString(),
                        goal_name = row["goal_name"].ToString(),
                        weight = row["weight"].ToString(),
                        description = row["description"].ToString(),
                        com_missed = row["com_missed"].ToString(),
                        com_met = row["com_met"].ToString(),
                        com_exceeded = row["com_exceeded"].ToString(),
                        com_dept = row["com_dept"].ToString(),
                        goal_type = row["goal_type"].ToString(),
                        rev1_comment = row["rev1_comment"].ToString(),
                        rev2_comment = row["rev2_comment"].ToString()

                    };
                    GetGoalPlanDetails.Add(getGoalPlan);
                }
                GetGoalPlanresult.result = GetGoalPlanDetails;
                GetGoalPlanresult.myreturn = "Success";
            }
            else
            {
                GetGoalPlanresult.myreturn = "Error";
            }
            return GetGoalPlanresult;
        }


        //GetRevs return 
        [HttpPost]
        [Route("GetRevs")]
        public GetGetRevs GetRevs(GetRevs VU)
        {

            GetGetRevs GetRevsresult = new GetGetRevs();
            List<GetRevsLists> GetRevsDetails = new List<GetRevsLists>();
            tempconnectionstring();

            Connection Connection = new Connection();
            Connection.myparameters.Add(new myParameters { ParameterName = "@cID", mytype = SqlDbType.NVarChar, Value = VU.cID });

            DataTable dtuserList = new DataTable();

            dtuserList = Connection.GetDataTable("GetRevs");
            if (dtuserList.Rows.Count > 0)
            {
                foreach (DataRow row in dtuserList.Rows)
                {
                    GetRevsLists GetRevsList = new GetRevsLists
                    {
                        reviewerTitle = row["reviewerTitle"].ToString(),
                        reviewerLevel = row["reviewerLevel"].ToString(),

                    };
                    GetRevsDetails.Add(GetRevsList);
                }
                GetRevsresult.result = GetRevsDetails;
                GetRevsresult.myreturn = "Success";
            }
            else
            {
                GetRevsresult.myreturn = "Error";
            }
            return GetRevsresult;
        }


        //sp_GetLowerNotif return 
        [HttpPost]
        [Route("sp_GetLowerNotif")]
        public Getsp_GetLowerNotif sp_GetLowerNotif(sp_GetLowerNotif VU)
        {

            Getsp_GetLowerNotif sp_GetLowerNotifresult = new Getsp_GetLowerNotif();
            List<sp_GetLowerNotifLists> sp_GetLowerNotifDetails = new List<sp_GetLowerNotifLists>();
            tempconnectionstring();

            Connection Connection = new Connection();
            Connection.myparameters.Add(new myParameters { ParameterName = "@Emp_ID", mytype = SqlDbType.NVarChar, Value = VU.Emp_ID });

            DataTable dtuserList = new DataTable();

            dtuserList = Connection.GetDataTable("sp_GetLowerNotif");
            if (dtuserList.Rows.Count > 0)
            {
                foreach (DataRow row in dtuserList.Rows)
                {
                    sp_GetLowerNotifLists sp_GetLowerNotifList = new sp_GetLowerNotifLists
                    {
                        Emp_ID = row["Emp_ID"].ToString(),

                    };
                    sp_GetLowerNotifDetails.Add(sp_GetLowerNotifList);
                }
                sp_GetLowerNotifresult.result = sp_GetLowerNotifDetails;
                sp_GetLowerNotifresult.myreturn = "Success";
            }
            else
            {
                sp_GetLowerNotifresult.myreturn = "Error";
            }
            return sp_GetLowerNotifresult;
        }

        //deleteRevs return 
        [HttpPost]
        [Route("deleteRevs")]
        public GetdeleteRevs deleteRevs(deleteRevs VU)
        {

            GetdeleteRevs deleteRevsresult = new GetdeleteRevs();
            List<deleteRevsLists> deleteRevsDetails = new List<deleteRevsLists>();
            tempconnectionstring();

            Connection Connection = new Connection();
            Connection.myparameters.Add(new myParameters { ParameterName = "@cID", mytype = SqlDbType.NVarChar, Value = VU.cID });

            DataTable dtuserList = new DataTable();

            dtuserList = Connection.GetDataTable("deleteRevs");
            if (dtuserList.Rows.Count > 0)
            {
                foreach (DataRow row in dtuserList.Rows)
                {
                    deleteRevsLists deleteRevsList = new deleteRevsLists
                    {

                    };
                    deleteRevsDetails.Add(deleteRevsList);
                }
                deleteRevsresult.result = deleteRevsDetails;
                deleteRevsresult.myreturn = "Success";
            }
            else
            {
                deleteRevsresult.myreturn = "Error";
            }
            return deleteRevsresult;
        }

        //insertRevs return 
        [HttpPost]
        [Route("insertRevs")]
        public GetinsertRevs insertRevs(insertRevs VU)
        {

            GetinsertRevs insertRevsresult = new GetinsertRevs();
            List<insertRevsLists> insertRevsDetails = new List<insertRevsLists>();
            tempconnectionstring();

            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@cID", mytype = SqlDbType.NVarChar, Value = VU.cID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@RTitle", mytype = SqlDbType.NVarChar, Value = VU.RTitle });
            Connection.myparameters.Add(new myParameters { ParameterName = "@RLevel", mytype = SqlDbType.NVarChar, Value = VU.RLevel });


            DataTable dtuserList = new DataTable();

            dtuserList = Connection.GetDataTable("insertRevs");
            if (dtuserList.Rows.Count > 0)
            {
                foreach (DataRow row in dtuserList.Rows)
                {
                    insertRevsLists insertRevsList = new insertRevsLists
                    {

                    };
                    insertRevsDetails.Add(insertRevsList);
                }
                insertRevsresult.result = insertRevsDetails;
                insertRevsresult.myreturn = "Success";
            }
            else
            {
                insertRevsresult.myreturn = "Error";
            }
            return insertRevsresult;
        }

        //insertRevsEscalate return 
        [HttpPost]
        [Route("insertRevsEscalate")]
        public GetinsertRevsEscalate insertRevsEscalate(insertRevsEscalate VU)
        {

            GetinsertRevsEscalate insertRevsEscalateresult = new GetinsertRevsEscalate();
            List<insertRevsEscalateLists> insertRevsEscalateDetails = new List<insertRevsEscalateLists>();
            tempconnectionstring();

            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@cID", mytype = SqlDbType.NVarChar, Value = VU.cID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@RTitle", mytype = SqlDbType.NVarChar, Value = VU.RTitle });
            Connection.myparameters.Add(new myParameters { ParameterName = "@RLevel", mytype = SqlDbType.NVarChar, Value = VU.RLevel });


            DataTable dtuserList = new DataTable();

            dtuserList = Connection.GetDataTable("insertRevsEscalate");
            if (dtuserList.Rows.Count > 0)
            {
                foreach (DataRow row in dtuserList.Rows)
                {
                    insertRevsEscalateLists insertRevsEscalateList = new insertRevsEscalateLists
                    {

                    };
                    insertRevsEscalateDetails.Add(insertRevsEscalateList);
                }
                insertRevsEscalateresult.result = insertRevsEscalateDetails;
                insertRevsEscalateresult.myreturn = "Success";
            }
            else
            {
                insertRevsEscalateresult.myreturn = "Error";
            }
            return insertRevsEscalateresult;
        }


        //deleteRevsEscalate return 
        [HttpPost]
        [Route("deleteRevsEscalate")]
        public GetdeleteRevsEscalate deleteRevsEscalate(deleteRevsEscalate VU)
        {

            GetdeleteRevsEscalate deleteRevsEscalateresult = new GetdeleteRevsEscalate();
            List<deleteRevsEscalateLists> deleteRevsEscalateDetails = new List<deleteRevsEscalateLists>();
            tempconnectionstring();

            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@cID", mytype = SqlDbType.NVarChar, Value = VU.cID });


            DataTable dtuserList = new DataTable();

            dtuserList = Connection.GetDataTable("deleteRevsEscalate");
            if (dtuserList.Rows.Count > 0)
            {
                foreach (DataRow row in dtuserList.Rows)
                {
                    deleteRevsEscalateLists deleteRevsEscalateList = new deleteRevsEscalateLists
                    {

                    };
                    deleteRevsEscalateDetails.Add(deleteRevsEscalateList);
                }
                deleteRevsEscalateresult.result = deleteRevsEscalateDetails;
                deleteRevsEscalateresult.myreturn = "Success";
            }
            else
            {
                deleteRevsEscalateresult.myreturn = "Error";
            }
            return deleteRevsEscalateresult;
        }


        //GetRevsEscalate return 
        [HttpPost]
        [Route("GetRevsEscalate")]
        public GetGetRevsEscalate GetRevsEscalate(GetRevsEscalate VU)
        {

            GetGetRevsEscalate GetRevsEscalateresult = new GetGetRevsEscalate();
            List<GetRevsEscalateLists> GetRevsEscalateDetails = new List<GetRevsEscalateLists>();
            tempconnectionstring();

            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@cID", mytype = SqlDbType.NVarChar, Value = VU.cID });


            DataTable dtuserList = new DataTable();

            dtuserList = Connection.GetDataTable("GetRevsEscalate");
            if (dtuserList.Rows.Count > 0)
            {
                foreach (DataRow row in dtuserList.Rows)
                {
                    GetRevsEscalateLists GetRevsEscalateList = new GetRevsEscalateLists
                    {
                        reviewerTitle = row["reviewerTitle"].ToString(),
                        reviewerLevel = row["reviewerLevel"].ToString(),

                    };
                    GetRevsEscalateDetails.Add(GetRevsEscalateList);
                }
                GetRevsEscalateresult.result = GetRevsEscalateDetails;
                GetRevsEscalateresult.myreturn = "Success";
            }
            else
            {
                GetRevsEscalateresult.myreturn = "Error";
            }
            return GetRevsEscalateresult;
        }

        //UpdateSettings1 return 
        [HttpPost]
        [Route("UpdateSettings1")]
        public GetUpdateSettings1 UpdateSettings1(UpdateSettings1 VU)
        {

            GetUpdateSettings1 UpdateSettings1result = new GetUpdateSettings1();
            List<UpdateSettings1Lists> UpdateSettings1Details = new List<UpdateSettings1Lists>();
            tempconnectionstring();

            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@cID", mytype = SqlDbType.NVarChar, Value = VU.cID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@rev1_days", mytype = SqlDbType.NVarChar, Value = VU.rev1_days });
            Connection.myparameters.Add(new myParameters { ParameterName = "@rev2_days", mytype = SqlDbType.NVarChar, Value = VU.rev2_days });
            Connection.myparameters.Add(new myParameters { ParameterName = "@message", mytype = SqlDbType.NVarChar, Value = VU.message });
            Connection.myparameters.Add(new myParameters { ParameterName = "@message_days", mytype = SqlDbType.NVarChar, Value = VU.message_days });
            Connection.myparameters.Add(new myParameters { ParameterName = "@rev1_mode", mytype = SqlDbType.NVarChar, Value = VU.rev1_mode });
            Connection.myparameters.Add(new myParameters { ParameterName = "@rev2_mode", mytype = SqlDbType.NVarChar, Value = VU.rev2_mode });
            Connection.myparameters.Add(new myParameters { ParameterName = "@message_mode", mytype = SqlDbType.NVarChar, Value = VU.message_mode });


            DataTable dtuserList = new DataTable();

            dtuserList = Connection.GetDataTable("UpdateSettings1");
            if (dtuserList.Rows.Count > 0)
            {
                foreach (DataRow row in dtuserList.Rows)
                {
                    UpdateSettings1Lists UpdateSettings1List = new UpdateSettings1Lists
                    {

                    };
                    UpdateSettings1Details.Add(UpdateSettings1List);
                }
                UpdateSettings1result.result = UpdateSettings1Details;
                UpdateSettings1result.myreturn = "Success";
            }
            else
            {
                UpdateSettings1result.myreturn = "Error";
            }
            return UpdateSettings1result;
        }

        //SetGoalGroup return 
        [HttpPost]
        [Route("SetGoalPlan")]
        public GetSetGoalGroup AppraisalGetSetGoalGroupy(SetGoalGroup VU)
        {

            GetSetGoalGroup SetGoalGroupresult = new GetSetGoalGroup();
            List<SetGoalGroupLists> SetGoalGroupDetails = new List<SetGoalGroupLists>();
            tempconnectionstring();

            Connection Connection = new Connection();
            Connection.myparameters.Add(new myParameters { ParameterName = "@group_id", mytype = SqlDbType.NVarChar, Value = VU.group_id });
            Connection.myparameters.Add(new myParameters { ParameterName = "@goal_name", mytype = SqlDbType.NVarChar, Value = VU.goal_name });
            Connection.myparameters.Add(new myParameters { ParameterName = "@weight", mytype = SqlDbType.NVarChar, Value = VU.weight });
            Connection.myparameters.Add(new myParameters { ParameterName = "@description", mytype = SqlDbType.NVarChar, Value = VU.description });
            Connection.myparameters.Add(new myParameters { ParameterName = "@com_missed", mytype = SqlDbType.NVarChar, Value = VU.com_missed });
            Connection.myparameters.Add(new myParameters { ParameterName = "@com_met", mytype = SqlDbType.NVarChar, Value = VU.com_met });
            Connection.myparameters.Add(new myParameters { ParameterName = "@com_exceeded", mytype = SqlDbType.NVarChar, Value = VU.com_exceeded });
            Connection.myparameters.Add(new myParameters { ParameterName = "@com_dept", mytype = SqlDbType.NVarChar, Value = VU.com_dept });
            Connection.myparameters.Add(new myParameters { ParameterName = "@goal_type", mytype = SqlDbType.NVarChar, Value = VU.goal_type });

            DataTable dtuserList = new DataTable();

            dtuserList = Connection.GetDataTable("SetGoalPlan");
            if (dtuserList.Rows.Count > 0)
            {
                foreach (DataRow row in dtuserList.Rows)
                {
                    SetGoalGroupLists setGoalGroup = new SetGoalGroupLists
                    {
                        
                    };
                    SetGoalGroupDetails.Add(setGoalGroup);
                }
                SetGoalGroupresult.result = SetGoalGroupDetails;
                SetGoalGroupresult.myreturn = "Success";
            }
            else
            {
                SetGoalGroupresult.myreturn = "Error";
            }
            return SetGoalGroupresult;
        }

        //DeleteGoal return 
        [HttpPost]
        [Route("DeleteGoal")]
        public GetDeleteGoal AppraisalGetDeleteGoal(DeleteGoal VU)
        {

            GetDeleteGoal GetDeleteGoalresult = new GetDeleteGoal();
            List<GetDeleteGoalLists> GetDeleteGoalDetails = new List<GetDeleteGoalLists>();
            tempconnectionstring();

            Connection Connection = new Connection();
            Connection.myparameters.Add(new myParameters { ParameterName = "@group_id", mytype = SqlDbType.NVarChar, Value = VU.group_id });

            DataTable dtuserList = new DataTable();

            dtuserList = Connection.GetDataTable("DeleteGoal");
            if (dtuserList.Rows.Count > 0)
            {
                foreach (DataRow row in dtuserList.Rows)
                {
                    GetDeleteGoalLists deleteGoal = new GetDeleteGoalLists
                    {
                        
                    };
                    GetDeleteGoalDetails.Add(deleteGoal);
                }
                GetDeleteGoalresult.result = GetDeleteGoalDetails;
                GetDeleteGoalresult.myreturn = "Success";
            }
            else
            {
                GetDeleteGoalresult.myreturn = "Error";
            }
            return GetDeleteGoalresult;
        }

        //GetGroup return 
        [HttpPost]
        [Route("GetReviewer")]
        public GetGetReviewer GetReviewer(GetReviewer GG)
        {

            GetGetReviewer GetReviewerresult = new GetGetReviewer();
            List<GetReviewerLists> GetReviewerDetails = new List<GetReviewerLists>();
            tempconnectionstring();
            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@company", mytype = SqlDbType.NVarChar, Value = GG.company });
            Connection.myparameters.Add(new myParameters { ParameterName = "@reviewerLevel", mytype = SqlDbType.NVarChar, Value = GG.reviewerLevel });

            DataTable dtuserList = new DataTable();

            dtuserList = Connection.GetDataTable("GetReviewer");
            if (dtuserList.Rows.Count > 0)
            {
                foreach (DataRow row in dtuserList.Rows)
                {
                    GetReviewerLists GetReviewerList = new GetReviewerLists
                    {
                        id = row["id"].ToString(),
                        company = row["company"].ToString(),
                        reviewerTitle = row["reviewerTitle"].ToString(),
                        reviewerLevel = row["reviewerLevel"].ToString()
                    };
                    GetReviewerDetails.Add(GetReviewerList);
                }
                GetReviewerresult.result = GetReviewerDetails;
                GetReviewerresult.myreturn = "Success";
            }
            else
            {
                GetReviewerresult.myreturn = "Error";
            }
            return GetReviewerresult;
        }


        //GetReviewerEscalate return 
        [HttpPost]
        [Route("GetReviewerEscalate")]
        public GetGetReviewerEscalate GetReviewerEscalate(GetReviewerEscalate GG)
        {

            GetGetReviewerEscalate GetReviewerEscalateresult = new GetGetReviewerEscalate();
            List<GetReviewerEscalateLists> GetReviewerEscalateDetails = new List<GetReviewerEscalateLists>();
            tempconnectionstring();
            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@company", mytype = SqlDbType.NVarChar, Value = GG.company });
            Connection.myparameters.Add(new myParameters { ParameterName = "@reviewerLevel", mytype = SqlDbType.NVarChar, Value = GG.reviewerLevel });

            DataTable dtuserList = new DataTable();

            dtuserList = Connection.GetDataTable("GetReviewerEscalate");
            if (dtuserList.Rows.Count > 0)
            {
                foreach (DataRow row in dtuserList.Rows)
                {
                    GetReviewerEscalateLists GetReviewerEscalateList = new GetReviewerEscalateLists
                    {
                        id = row["id"].ToString(),
                        company = row["company"].ToString(),
                        reviewerTitle = row["reviewerTitle"].ToString(),
                        reviewerLevel = row["reviewerLevel"].ToString()
                    };
                    GetReviewerEscalateDetails.Add(GetReviewerEscalateList);
                }
                GetReviewerEscalateresult.result = GetReviewerEscalateDetails;
                GetReviewerEscalateresult.myreturn = "Success";
            }
            else
            {
                GetReviewerEscalateresult.myreturn = "Error";
            }
            return GetReviewerEscalateresult;
        }


        //GetGroup return 
        [HttpPost]
        [Route("GetGroup")]
        public GetGetGroup GetGroup(GetGroup GG)
        {

            GetGetGroup GetGroupresult = new GetGetGroup();
            List<GetGroupLists> GetGroupDetails = new List<GetGroupLists>();
            tempconnectionstring();
            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@ID", mytype = SqlDbType.NVarChar, Value = GG.ID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@date_start", mytype = SqlDbType.NVarChar, Value = GG.date_start });
            Connection.myparameters.Add(new myParameters { ParameterName = "@date_end", mytype = SqlDbType.NVarChar, Value = GG.date_end });

            DataTable dtuserList = new DataTable();

            dtuserList = Connection.GetDataTable("GetGroup");
            if (dtuserList.Rows.Count > 0)
            {
                foreach (DataRow row in dtuserList.Rows)
                {
                    GetGroupLists GetGroupList = new GetGroupLists
                    {
                        id = row["id"].ToString(),
                        //notif = row["notif"].ToString(),
                        doer_id = row["doer_id"].ToString(),
                        //result_fin = row["result_fin"].ToString(),
                        date_created = row["date_created"].ToString(),
                        // approved = row["approved"].ToString(),
                        // emp_edit = row["emp_edit"].ToString(),
                        // rev1_edit = row["rev1_edit"].ToString()
                        start_pending = row["start_pending"].ToString(),
                        mid_pending = row["mid_pending"].ToString(),
                        end_pending = row["end_pending"].ToString()

                    };
                    GetGroupDetails.Add(GetGroupList);
                }
                GetGroupresult.result = GetGroupDetails;
                GetGroupresult.myreturn = "Success";
            }
            else
            {
                GetGroupresult.myreturn = "Error";
            }
            return GetGroupresult;
        }

        //GetComUserHeir return 
        [HttpPost]
        [Route("GetComUserHeir")]
        public GetGetComUserHeir GetComUserHeir(GetComUserHeir GG)
        {
            GetGetComUserHeir GetComUserHeirresult = new GetGetComUserHeir();
            List<GetComUserHeirLists> GetComUserHeirDetails = new List<GetComUserHeirLists>();

            tempconnectionstring();
            Connection Connection = new Connection();
            Connection.myparameters.Add(new myParameters { ParameterName = "@empId", mytype = SqlDbType.NVarChar, Value = GG.empId });
            Connection.myparameters.Add(new myParameters { ParameterName = "@company", mytype = SqlDbType.NVarChar, Value = GG.company});
            DataTable dtuserList = new DataTable();

            dtuserList = Connection.GetDataTable("GetComUserHeir");
            if (dtuserList.Rows.Count > 0)
            {
                foreach (DataRow row in dtuserList.Rows)
                {
                    GetComUserHeirLists GetComUserHeirList = new GetComUserHeirLists
                    {
                        EmpID = row["EmpID"].ToString()
                    };
                    GetComUserHeirDetails.Add(GetComUserHeirList);
                }
                GetComUserHeirresult.result = GetComUserHeirDetails;
                GetComUserHeirresult.myreturn = "Success";
            }
            else
            {
                GetComUserHeirresult.myreturn = "Error";
            }
            return GetComUserHeirresult;
        }


        //GetComUserHeir return 
        [HttpPost]
        [Route("GetComUserHeirNotif")]
        public GetGetComUserHeirNotif GetComUserHeirNotif(GetComUserHeirNotif GG)
        {
            

            GetGetComUserHeirNotif GetComUserHeirNotifresult = new GetGetComUserHeirNotif();
            List<GetComUserHeirNotifLists> GetComUserHeirNotifDetails = new List<GetComUserHeirNotifLists>();
            tempconnectionstring();
            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@ID", mytype = SqlDbType.NVarChar, Value = GG.ID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@company", mytype = SqlDbType.NVarChar, Value = GG.company });
            DataTable dtuserList = new DataTable();

            dtuserList = Connection.GetDataTable("GetComUserHeirNotif");
            if (dtuserList.Rows.Count > 0)
            {
                foreach (DataRow row in dtuserList.Rows)
                {
                    GetComUserHeirNotifLists GetComUserHeirNotifList = new GetComUserHeirNotifLists
                    {
                        Emp_ID = row["EmpID"].ToString(),
                        Emp_FName = row["Emp_FName"].ToString(),
                        Emp_MName = row["Emp_MName"].ToString(),
                        Emp_LName = row["Emp_LName"].ToString(),
                        EmpLevel = row["EmpLevel"].ToString(),
                        ImSup_ID = row["MngrID"].ToString(),
                        ImSup_Name = row["ImSup_Name"].ToString(),
                        

                    };
                    GetComUserHeirNotifDetails.Add(GetComUserHeirNotifList);
                }
                GetComUserHeirNotifresult.result = GetComUserHeirNotifDetails;
                GetComUserHeirNotifresult.myreturn = "Success";
            }
            else
            {
                GetComUserHeirNotifresult.myreturn = "Error";
            }
            return GetComUserHeirNotifresult;
        }


        //SelectAll return 
        [HttpPost]
        [Route("SelectAll")]
        public GetSelectAll SelectAll(SelectAll GG)
        {
            

            GetSelectAll GetSelectAllresult = new GetSelectAll();
            List<GetSelectAllLists> GetSelectAllDetails = new List<GetSelectAllLists>();
            tempconnectionstring();
            Connection Connection = new Connection();

            
            DataTable dtuserList = new DataTable();

            dtuserList = Connection.GetDataTable("SelectAll");
            if (dtuserList.Rows.Count > 0)
            {
                foreach (DataRow row in dtuserList.Rows)
                {
                    GetSelectAllLists GetSelectAllList = new GetSelectAllLists
                    {
                        Emp_ID = row["Emp_ID"].ToString(),
                        Emp_FName = row["Emp_FName"].ToString(),
                        Emp_MName = row["Emp_MName"].ToString(),
                        Emp_LName = row["Emp_LName"].ToString(),
                        ImSup_ID = row["ImSup_ID"].ToString(),
                        ImSup_Name = row["ImSup_Name"].ToString(),
                        EmpLevel = row["EmpLevel"].ToString(),
                        


                    };
                    GetSelectAllDetails.Add(GetSelectAllList);
                }
                GetSelectAllresult.result = GetSelectAllDetails;
                GetSelectAllresult.myreturn = "Success";
            }
            else
            {
                GetSelectAllresult.myreturn = "Error";
            }
            return GetSelectAllresult;
        }

        //GetDates return 
        [HttpPost]
        [Route("GetDates")]
        public GetGetDates GetDates(GetDates GD)
        {
            

            GetGetDates GetGetDatesresult = new GetGetDates();
            List<GetDatesLists> GetGetDatesDetails = new List<GetDatesLists>();
            tempconnectionstring();
            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@id", mytype = SqlDbType.NVarChar, Value = GD.id });

            DataTable dtuserList = new DataTable();

            dtuserList = Connection.GetDataTable("GetDates");
            if (dtuserList.Rows.Count > 0)
            {
                foreach (DataRow row in dtuserList.Rows)
                {
                    GetDatesLists GetDatesList = new GetDatesLists
                    {
                        id = row["id"].ToString(),
                        name = row["name"].ToString(),
                        date_start = row["date_start"].ToString(),
                        date_end = row["date_end"].ToString(),

                    };
                    GetGetDatesDetails.Add(GetDatesList);
                }
                GetGetDatesresult.result = GetGetDatesDetails;
                GetGetDatesresult.myreturn = "Success";
            }
            else
            {
                GetGetDatesresult.myreturn = "Error";
            }
            return GetGetDatesresult;
        }



        //GetNotif return 
        [HttpPost]
        [Route("GetNotif")]
        public GetGetNotif GetNotif(GetNotif GD)
        {
            

            GetGetNotif GetGetNotifresult = new GetGetNotif();
            List<GetNotifLists> GetGetNotifDetails = new List<GetNotifLists>();
            tempconnectionstring();
            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@id", mytype = SqlDbType.NVarChar, Value = GD.id });
            
            DataTable dtuserList = new DataTable();

            dtuserList = Connection.GetDataTable("GetNotif");
            if (dtuserList.Rows.Count > 0)
            {
                foreach (DataRow row in dtuserList.Rows)
                {
                    GetNotifLists GetNotifList = new GetNotifLists
                    {
                        count = row["count"].ToString()

                    };
                    GetGetNotifDetails.Add(GetNotifList);
                }
                GetGetNotifresult.result = GetGetNotifDetails;
                GetGetNotifresult.myreturn = "Success";
            }
            else
            {
                GetGetNotifresult.myreturn = "Error";
            }
            return GetGetNotifresult;
        }

        //GetPending return 
        [HttpPost]
        [Route("GetPending")]
        public GetGetPending GetPending(GetPending GD)
        {
            

            GetGetPending GetGetPendingresult = new GetGetPending();
            List<GetPendingLists> GetGetPendingDetails = new List<GetPendingLists>();
            tempconnectionstring();
            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@id", mytype = SqlDbType.NVarChar, Value = GD.id });

            DataTable dtuserList = new DataTable();

            dtuserList = Connection.GetDataTable("GetPending");
            if (dtuserList.Rows.Count > 0)
            {
                foreach (DataRow row in dtuserList.Rows)
                {
                    GetPendingLists GetPendingList = new GetPendingLists
                    {
                        count = row["count"].ToString()

                    };
                    GetGetPendingDetails.Add(GetPendingList);
                }
                GetGetPendingresult.result = GetGetPendingDetails;
                GetGetPendingresult.myreturn = "Success";
            }
            else
            {
                GetGetPendingresult.myreturn = "Error";
            }
            return GetGetPendingresult;
        }


        //GetAllPendingRev1 return 
        [HttpPost]
        [Route("GetAllPendingRev1")]
        public GetGetAllPendingRev1 GetAllPendingRev1(GetAllPendingRev1 GD)
        {
            

            GetGetAllPendingRev1 GetGetAllPendingRev1result = new GetGetAllPendingRev1();
            List<GetAllPendingRev1Lists> GetGetAllPendingRev1Details = new List<GetAllPendingRev1Lists>();
            tempconnectionstring();
            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@date", mytype = SqlDbType.NVarChar, Value = GD.date });

            DataTable dtuserList = new DataTable();

            dtuserList = Connection.GetDataTable("GetAllPendingRev1");
            if (dtuserList.Rows.Count > 0)
            {
                foreach (DataRow row in dtuserList.Rows)
                {
                    GetAllPendingRev1Lists GetAllPendingRev1List = new GetAllPendingRev1Lists
                    {
                        id = row["id"].ToString(),
                        group_id = row["group_id"].ToString(),
                        ack_lvl = row["ack_lvl"].ToString(),
                        status = row["status"].ToString(),
                        creator_id = row["creator_id"].ToString(),
                        doer_id = row["doer_id"].ToString(),
                        final_id = row["final_id"].ToString(),
                        doer_status = row["doer_status"].ToString(),
                        date_req = row["date_req"].ToString(),
                        date_ack = row["date_ack"].ToString(),
                        escalated = row["escalated"].ToString(),

                    };
                    GetGetAllPendingRev1Details.Add(GetAllPendingRev1List);
                }
                GetGetAllPendingRev1result.result = GetGetAllPendingRev1Details;
                GetGetAllPendingRev1result.myreturn = "Success";
            }
            else
            {
                GetGetAllPendingRev1result.myreturn = "Error";
            }
            return GetGetAllPendingRev1result;
        }

        //UpdateCommentPlan return 
        [HttpPost]
        [Route("UpdateCommentPlan")]
        public GetUpdateCommentPlan UpdateCommentPlan(UpdateCommentPlan GD)
        {


            GetUpdateCommentPlan UpdateCommentPlanresult = new GetUpdateCommentPlan();
            List<UpdateCommentPlanLists> UpdateCommentPlanDetails = new List<UpdateCommentPlanLists>();
            tempconnectionstring();
            Connection Connection = new Connection();
            
            Connection.myparameters.Add(new myParameters { ParameterName = "@goal_id", mytype = SqlDbType.NVarChar, Value = GD.goal_id });
            Connection.myparameters.Add(new myParameters { ParameterName = "@comment", mytype = SqlDbType.NVarChar, Value = GD.comment });
            Connection.myparameters.Add(new myParameters { ParameterName = "@com_lvl", mytype = SqlDbType.NVarChar, Value = GD.com_lvl });

            DataTable dtuserList = new DataTable();

            dtuserList = Connection.GetDataTable("UpdateCommentPlan");
            if (dtuserList.Rows.Count > 0)
            {
                foreach (DataRow row in dtuserList.Rows)
                {
                    UpdateCommentPlanLists UpdateCommentPlanList = new UpdateCommentPlanLists
                    {
                        

                    };
                    UpdateCommentPlanDetails.Add(UpdateCommentPlanList);
                }
                UpdateCommentPlanresult.result = UpdateCommentPlanDetails;
                UpdateCommentPlanresult.myreturn = "Success";
            }
            else
            {
                UpdateCommentPlanresult.myreturn = "Error";
            }
            return UpdateCommentPlanresult;
        }



        //InitNewCompanyAccounts return 
        [HttpPost]
        [Route("InitNewCompanyAccounts")]
        public GetInitNewCompanyAccounts InitNewCompanyAccounts(InitNewCompanyAccounts NA)
        {


            GetInitNewCompanyAccounts InitNewCompanyAccountsresult = new GetInitNewCompanyAccounts();
            List<InitNewCompanyAccountsLists> InitNewCompanyAccountsDetails = new List<InitNewCompanyAccountsLists>();
            tempconnectionstring();
            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@company", mytype = SqlDbType.NVarChar, Value = NA.company });
            Connection.myparameters.Add(new myParameters { ParameterName = "@companyId", mytype = SqlDbType.NVarChar, Value = NA.companyId });

            DataTable dtuserList = new DataTable();

            dtuserList = Connection.GetDataTable("InitNewCompanyAccounts");
            if (dtuserList.Rows.Count > 0)
            {
                foreach (DataRow row in dtuserList.Rows)
                {
                    InitNewCompanyAccountsLists InitNewCompanyAccountsList = new InitNewCompanyAccountsLists
                    {


                    };
                    InitNewCompanyAccountsDetails.Add(InitNewCompanyAccountsList);
                }
                InitNewCompanyAccountsresult.result = InitNewCompanyAccountsDetails;
                InitNewCompanyAccountsresult.myreturn = "Success";
            }
            else
            {
                InitNewCompanyAccountsresult.myreturn = "Error";
            }
            return InitNewCompanyAccountsresult;
        }




        //AddCommentPlan return 
        [HttpPost]
        [Route("AddCommentPlan")]
        public GetAddCommentPlan AddCommentPlan(AddCommentPlan GD)
        {


            GetAddCommentPlan AddCommentPlanresult = new GetAddCommentPlan();
            List<AddCommentPlanLists> AddCommentPlanDetails = new List<AddCommentPlanLists>();
            tempconnectionstring();
            Connection Connection = new Connection();
            
            Connection.myparameters.Add(new myParameters { ParameterName = "@goal_id", mytype = SqlDbType.NVarChar, Value = GD.goal_id });
            Connection.myparameters.Add(new myParameters { ParameterName = "@comment", mytype = SqlDbType.NVarChar, Value = GD.comment });
            Connection.myparameters.Add(new myParameters { ParameterName = "@com_lvl", mytype = SqlDbType.NVarChar, Value = GD.com_lvl });
            Connection.myparameters.Add(new myParameters { ParameterName = "@date_created", mytype = SqlDbType.NVarChar, Value = GD.date_created });

            DataTable dtuserList = new DataTable();

            dtuserList = Connection.GetDataTable("AddCommentPlan");
            if (dtuserList.Rows.Count > 0)
            {
                foreach (DataRow row in dtuserList.Rows)
                {
                    AddCommentPlanLists AddCommentPlanList = new AddCommentPlanLists
                    {


                    };
                    AddCommentPlanDetails.Add(AddCommentPlanList);
                }
                AddCommentPlanresult.result = AddCommentPlanDetails;
                AddCommentPlanresult.myreturn = "Success";
            }
            else
            {
                AddCommentPlanresult.myreturn = "Error";
            }
            return AddCommentPlanresult;
        }

        //DeleteCommentPlan return 
        [HttpPost]
        [Route("DeleteCommentPlan")]
        public GetDeleteCommentPlan DeleteCommentPlan(DeleteCommentPlan GD)
        {


            GetDeleteCommentPlan GetDeleteCommentPlanresult = new GetDeleteCommentPlan();
            List<DeleteCommentPlanLists> DeleteCommentPlanDetails = new List<DeleteCommentPlanLists>();
            tempconnectionstring();
            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@id", mytype = SqlDbType.NVarChar, Value = GD.id });
            Connection.myparameters.Add(new myParameters { ParameterName = "@level", mytype = SqlDbType.NVarChar, Value = GD.level });

            DataTable dtuserList = new DataTable();

            dtuserList = Connection.GetDataTable("DeleteCommentPlan");
            if (dtuserList.Rows.Count > 0)
            {
                foreach (DataRow row in dtuserList.Rows)
                {
                    DeleteCommentPlanLists DeleteCommentPlanList = new DeleteCommentPlanLists
                    {
                   
                    };
                    DeleteCommentPlanDetails.Add(DeleteCommentPlanList);
                }
                GetDeleteCommentPlanresult.result = DeleteCommentPlanDetails;
                GetDeleteCommentPlanresult.myreturn = "Success";
            }
            else
            {
                GetDeleteCommentPlanresult.myreturn = "Error";
            }
            return GetDeleteCommentPlanresult;
        }


        //DeleteComment return 
        [HttpPost]
        [Route("DeleteComment")]
        public GetDeleteComment DeleteComment(DeleteComment GD)
        {


            GetDeleteComment GetDeleteCommentresult = new GetDeleteComment();
            List<DeleteCommentLists> DeleteCommentDetails = new List<DeleteCommentLists>();
            tempconnectionstring();
            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@id", mytype = SqlDbType.NVarChar, Value = GD.id });
            Connection.myparameters.Add(new myParameters { ParameterName = "@level", mytype = SqlDbType.NVarChar, Value = GD.level });

            DataTable dtuserList = new DataTable();

            dtuserList = Connection.GetDataTable("DeleteComment");
            if (dtuserList.Rows.Count > 0)
            {
                foreach (DataRow row in dtuserList.Rows)
                {
                    DeleteCommentLists DeleteCommentList = new DeleteCommentLists
                    {

                    };
                    DeleteCommentDetails.Add(DeleteCommentList);
                }
                GetDeleteCommentresult.result = DeleteCommentDetails;
                GetDeleteCommentresult.myreturn = "Success";
            }
            else
            {
                GetDeleteCommentresult.myreturn = "Error";
            }
            return GetDeleteCommentresult;
        }

        //GetAllPendingRev2 return 
        [HttpPost]
        [Route("GetAllPendingRev2")]
        public GetGetAllPendingRev2 GetAllPendingRev2(GetAllPendingRev2 GD)
        {
           

            GetGetAllPendingRev2 GetGetAllPendingRev2result = new GetGetAllPendingRev2();
            List<GetAllPendingRev2Lists> GetGetAllPendingRev2Details = new List<GetAllPendingRev2Lists>();
            tempconnectionstring();
            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@date", mytype = SqlDbType.NVarChar, Value = GD.date });

            DataTable dtuserList = new DataTable();

            dtuserList = Connection.GetDataTable("GetAllPendingRev2");
            if (dtuserList.Rows.Count > 0)
            {
                foreach (DataRow row in dtuserList.Rows)
                {
                    GetAllPendingRev2Lists GetAllPendingRev2List = new GetAllPendingRev2Lists
                    {
                        id = row["id"].ToString(),
                        group_id = row["group_id"].ToString(),
                        ack_lvl = row["ack_lvl"].ToString(),
                        status = row["status"].ToString(),
                        creator_id = row["creator_id"].ToString(),
                        doer_id = row["doer_id"].ToString(),
                        final_id = row["final_id"].ToString(),
                        doer_status = row["doer_status"].ToString(),
                        date_req = row["date_req"].ToString(),
                        date_ack = row["date_ack"].ToString(),
                        escalated = row["escalated"].ToString(),

                    };
                    GetGetAllPendingRev2Details.Add(GetAllPendingRev2List);
                }
                GetGetAllPendingRev2result.result = GetGetAllPendingRev2Details;
                GetGetAllPendingRev2result.myreturn = "Success";
            }
            else
            {
                GetGetAllPendingRev2result.myreturn = "Error";
            }
            return GetGetAllPendingRev2result;
        }



        //GetDate return 
        [HttpPost]
        [Route("SetDate")]
        public GetSetDate SetDate(SetDate UG)
        {
            

            GetSetDate SetDateresult = new GetSetDate();
            List<SetDateLists> SetDateDetails = new List<SetDateLists>();
            tempconnectionstring();
            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@current_date", mytype = SqlDbType.Date, Value = UG.current_date });
            Connection.myparameters.Add(new myParameters { ParameterName = "@company", mytype = SqlDbType.NVarChar, Value = UG.company });

            DataTable dtuserList = new DataTable();

            dtuserList = Connection.GetDataTable("SetDate");
            if (dtuserList.Rows.Count > 0)
            {
                foreach (DataRow row in dtuserList.Rows)
                {
                    SetDateLists SetDateList = new SetDateLists
                    {
                        id = row["id"].ToString(),
                        name = row["name"].ToString(),
                        date_id = row["date_id"].ToString(),
                        date_start = row["date_start"].ToString(),
                        date_end = row["date_end"].ToString(),

                    };
                    SetDateDetails.Add(SetDateList);
                }
                SetDateresult.result = SetDateDetails;
                SetDateresult.myreturn = "Success";
            }
            else
            {
                SetDateresult.myreturn = "Error";
            }
            return SetDateresult;
        }



        //GetSettings return 
        [HttpPost]
        [Route("GetSettings")]
        public GetGetSettings GetSettings(GetSettings UG)
        {
           

            GetGetSettings GetSettingsresult = new GetGetSettings();
            List<GetSettingsLists> GetSettingsDetails = new List<GetSettingsLists>();
            tempconnectionstring();
            Connection Connection = new Connection();


            Connection.myparameters.Add(new myParameters { ParameterName = "@cID", mytype = SqlDbType.NVarChar, Value = UG.cID });

            DataTable dtuserList = new DataTable();

            dtuserList = Connection.GetDataTable("GetSettings");
            if (dtuserList.Rows.Count > 0)
            {
                foreach (DataRow row in dtuserList.Rows)
                {
                    GetSettingsLists GetSettingsList = new GetSettingsLists
                    {
                        
                        company = row["company"].ToString(),
                        rev1_days = row["rev1_days"].ToString(),
                        rev2_days = row["rev2_days"].ToString(),
                        message = row["message"].ToString(),
                        message_days = row["message_days"].ToString(),
                        rev1_mode = row["rev1_mode"].ToString(),
                        rev2_mode = row["rev2_mode"].ToString(),
                        message_mode = row["message_mode"].ToString()

                    };
                    GetSettingsDetails.Add(GetSettingsList);
                }
                GetSettingsresult.result = GetSettingsDetails;
                GetSettingsresult.myreturn = "Success";
            }
            else
            {
                GetSettingsresult.myreturn = "Error";
            }
            return GetSettingsresult;
        }



        //GetPositions return 
        [HttpPost]
        [Route("GetPositions")]
        public GetGetPositions GetPositions(GetPositions GD)
        {
           

            GetGetPositions GetGetPositionsresult = new GetGetPositions();
            List<GetPositionsLists> GetPositionsDetails = new List<GetPositionsLists>();
            tempconnectionstring();
            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@CN", mytype = SqlDbType.NVarChar, Value = GD.CN });

            DataTable dtuserList = new DataTable();

            dtuserList = Connection.GetDataTable("GetPositions");
            if (dtuserList.Rows.Count > 0)
            {
                foreach (DataRow row in dtuserList.Rows)
                {
                    GetPositionsLists GetPositionsList = new GetPositionsLists
                    {
                        positions = row["positions"].ToString()
                    };
                    GetPositionsDetails.Add(GetPositionsList);
                }
                GetGetPositionsresult.result = GetPositionsDetails;
                GetGetPositionsresult.myreturn = "Success";
            }
            else
            {
                GetGetPositionsresult.myreturn = "Error";
            }
            return GetGetPositionsresult;
        }



        //GetResult return 
        [HttpPost]
        [Route("GetResult")]
        public GetGetResult GetResult(GetResult GD)
        {
            

            GetGetResult GetGetResultresult = new GetGetResult();
            List<GetResultLists> GetResultDetails = new List<GetResultLists>();
            tempconnectionstring();
            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@company", mytype = SqlDbType.NVarChar, Value = GD.company });

            DataTable dtuserList = new DataTable();

            dtuserList = Connection.GetDataTable("GetResult");
            if (dtuserList.Rows.Count > 0)
            {
                foreach (DataRow row in dtuserList.Rows)
                {
                    GetResultLists GetResultList = new GetResultLists
                    {
                        id = row["id"].ToString(),
                        name = row["name"].ToString(),
                        weight = row["weight"].ToString(),

                    };
                    GetResultDetails.Add(GetResultList);
                }
                GetGetResultresult.result = GetResultDetails;
                GetGetResultresult.myreturn = "Success";
            }
            else
            {
                GetGetResultresult.myreturn = "Error";
            }
            return GetGetResultresult;
        }

        //FilterReports return 
        [HttpPost]
        [Route("FilterReports")]
        public GetFilterReports FilterReports(FilterReports GR)
        {


            GetFilterReports FilterReportsresult = new GetFilterReports();
            List<FilterReportsLists> FilterReportsDetails = new List<FilterReportsLists>();
            tempconnectionstring();
            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@self", mytype = SqlDbType.NVarChar, Value = GR.self });
            Connection.myparameters.Add(new myParameters { ParameterName = "@lvl1", mytype = SqlDbType.NVarChar, Value = GR.lvl1 });
            Connection.myparameters.Add(new myParameters { ParameterName = "@lvl2", mytype = SqlDbType.NVarChar, Value = GR.lvl2 });

            DataTable dtuserList = new DataTable();

            dtuserList = Connection.GetDataTable("FilterReports");
            if (dtuserList.Rows.Count > 0)
            {
                foreach (DataRow row in dtuserList.Rows)
                {
                    FilterReportsLists FilterReportsList = new FilterReportsLists
                    {

                        EmpID = row["EmpID"].ToString(),
                        Name = row["Name"].ToString(),
                        MngrName = row["MngrName"].ToString(),
                        reviewerLevel = row["reviewerLevel"].ToString(),

                    };
                    FilterReportsDetails.Add(FilterReportsList);
                }
                FilterReportsresult.result = FilterReportsDetails;
                FilterReportsresult.myreturn = "Success";
            }
            else
            {
                FilterReportsresult.myreturn = "Error";
            }
            return FilterReportsresult;
        }

        //SelfReport return 
        [HttpPost]
        [Route("SelfReport")]
        public GetSelfReport SelfReport(SelfReport GR)
        {


            GetSelfReport SelfReportresult = new GetSelfReport();
            List<SelfReportLists> SelfReportDetails = new List<SelfReportLists>();
            tempconnectionstring();
            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@cID", mytype = SqlDbType.NVarChar, Value = GR.cID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.NVarChar, Value = GR.empID });

            DataTable dtuserList = new DataTable();

            dtuserList = Connection.GetDataTable("SelfReport");
            if (dtuserList.Rows.Count > 0)
            {
                foreach (DataRow row in dtuserList.Rows)
                {
                    SelfReportLists SelfReportList = new SelfReportLists
                    {
                        name = row["name"].ToString(),
                        total = row["total"].ToString(),
                        pending = row["pending"].ToString(),
                        done = row["done"].ToString(),

                    };
                    SelfReportDetails.Add(SelfReportList);
                }
                SelfReportresult.result = SelfReportDetails;
                SelfReportresult.myreturn = "Success";
            }
            else
            {
                SelfReportresult.myreturn = "Error";
            }
            return SelfReportresult;
        }

        //DeleteAckPlanData return 
        [HttpPost]
        [Route("DeleteAckPlanData")]
        public GetDeleteAckPlanData DeleteAckPlanData(DeleteAckPlanData GR)
        {


            GetDeleteAckPlanData DeleteAckPlanDataresult = new GetDeleteAckPlanData();
            List<DeleteAckPlanDataLists> DeleteAckPlanDataDetails = new List<DeleteAckPlanDataLists>();
            tempconnectionstring();
            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@id", mytype = SqlDbType.NVarChar, Value = GR.id });

            DataTable dtuserList = new DataTable();

            dtuserList = Connection.GetDataTable("DeleteAckPlanData");
            if (dtuserList.Rows.Count > 0)
            {
                foreach (DataRow row in dtuserList.Rows)
                {
                    DeleteAckPlanDataLists FilterReportsList = new DeleteAckPlanDataLists
                    {
                        

                    };
                    DeleteAckPlanDataDetails.Add(FilterReportsList);
                }
                DeleteAckPlanDataresult.result = DeleteAckPlanDataDetails;
                DeleteAckPlanDataresult.myreturn = "Success";
            }
            else
            {
                DeleteAckPlanDataresult.myreturn = "Error";
            }
            return DeleteAckPlanDataresult;
        }


        //DeleteAckData return 
        [HttpPost]
        [Route("DeleteAckData")]
        public GetDeleteAckData DeleteAckData(DeleteAckData GR)
        {


            GetDeleteAckData DeleteAckDataresult = new GetDeleteAckData();
            List<DeleteAckDataLists> DeleteAckDataDetails = new List<DeleteAckDataLists>();
            tempconnectionstring();
            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@id", mytype = SqlDbType.NVarChar, Value = GR.id });
            Connection.myparameters.Add(new myParameters { ParameterName = "@ack_lvl", mytype = SqlDbType.NVarChar, Value = GR.ack_lvl });
            Connection.myparameters.Add(new myParameters { ParameterName = "@modified_by", mytype = SqlDbType.NVarChar, Value = GR.modified_by });

            DataTable dtuserList = new DataTable();

            dtuserList = Connection.GetDataTable("DeleteAckData");
            if (dtuserList.Rows.Count > 0)
            {
                foreach (DataRow row in dtuserList.Rows)
                {
                    DeleteAckDataLists DeleteAckDataList = new DeleteAckDataLists
                    {


                    };
                    DeleteAckDataDetails.Add(DeleteAckDataList);
                }
                DeleteAckDataresult.result = DeleteAckDataDetails;
                DeleteAckDataresult.myreturn = "Success";
            }
            else
            {
                DeleteAckDataresult.myreturn = "Error";
            }
            return DeleteAckDataresult;
        }


        
        //DeleteAckData return 
        [HttpPost]
        [Route("UnlockReviewer1")]
        public GetUnlockReviewer1 UnlockReviewer1(UnlockReviewer1 GR)
        {


            GetUnlockReviewer1 UnlockReviewer1result = new GetUnlockReviewer1();
            List<UnlockReviewer1Lists> UnlockReviewer1Details = new List<UnlockReviewer1Lists>();
            tempconnectionstring();
            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@group_id", mytype = SqlDbType.NVarChar, Value = GR.group_id });
            Connection.myparameters.Add(new myParameters { ParameterName = "@ack_lvl", mytype = SqlDbType.NVarChar, Value = GR.ack_lvl });
            Connection.myparameters.Add(new myParameters { ParameterName = "@modified_by", mytype = SqlDbType.NVarChar, Value = GR.modified_by });

            DataTable dtuserList = new DataTable();

            dtuserList = Connection.GetDataTable("UnlockReviewer1");
            if (dtuserList.Rows.Count > 0)
            {
                foreach (DataRow row in dtuserList.Rows)
                {
                    UnlockReviewer1Lists UnlockReviewer1List = new UnlockReviewer1Lists
                    {


                    };
                    UnlockReviewer1Details.Add(UnlockReviewer1List);
                }
                UnlockReviewer1result.result = UnlockReviewer1Details;
                UnlockReviewer1result.myreturn = "Success";
            }
            else
            {
                UnlockReviewer1result.myreturn = "Error";
            }
            return UnlockReviewer1result;
        }



        //GetReport return 
        [HttpPost]
        [Route("GetReport")]
        public GetGetReport GetReport(GetReport GR)
        {
            

            GetGetReport GetReportresult = new GetGetReport();
            List<GetReportLists> GetReportDetails = new List<GetReportLists>();
            tempconnectionstring();
            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@cID", mytype = SqlDbType.NVarChar, Value = GR.cID });

            DataTable dtuserList = new DataTable();

            dtuserList = Connection.GetDataTable("GetReport");
            if (dtuserList.Rows.Count > 0)
            {
                foreach (DataRow row in dtuserList.Rows)
                {
                    GetReportLists GetReportList = new GetReportLists
                    {

                        total = row["total"].ToString(),
                        pending = row["pending"].ToString(),
                        done = row["done"].ToString(),
                        Name = row["Name"].ToString(),

                    };
                    GetReportDetails.Add(GetReportList);
                }
                GetReportresult.result = GetReportDetails;
                GetReportresult.myreturn = "Success";
            }
            else
            {
                GetReportresult.myreturn = "Error";
            }
            return GetReportresult;
        }

        //GetReports return 
        [HttpPost]
        [Route("GetReports")]
        public GetGetReports GetReports(GetReports GR)
        {


            GetGetReports GetReportsresult = new GetGetReports();
            List<GetReportsLists> GetReportsDetails = new List<GetReportsLists>();
            tempconnectionstring();
            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@cID", mytype = SqlDbType.NVarChar, Value = GR.cID });

            DataTable dtuserList = new DataTable();

            dtuserList = Connection.GetDataTable("GetReports");
            if (dtuserList.Rows.Count > 0)
            {
                foreach (DataRow row in dtuserList.Rows)
                {
                    GetReportsLists GetReportsList = new GetReportsLists
                    {

                        EmpID = row["EmpID"].ToString(),
                        Name = row["Name"].ToString(),
                        ISName = row["ISName"].ToString(),
                        total_midyear = row["total_midyear"].ToString(),
                        pending_midyear = row["pending_midyear"].ToString(),
                        done_midyear = row["done_midyear"].ToString(),
                        total_endyear = row["total_endyear"].ToString(),
                        pending_endyear = row["pending_endyear"].ToString(),
                        done_endyear = row["done_endyear"].ToString(),
                        done = row["done"].ToString(),
                        level = row["level"].ToString(),


                    };
                    GetReportsDetails.Add(GetReportsList);
                }
                GetReportsresult.result = GetReportsDetails;
                GetReportsresult.myreturn = "Success";
            }
            else
            {
                GetReportsresult.myreturn = "Error";
            }
            return GetReportsresult;
        }

        //GetReportRev1 return 
        [HttpPost]
        [Route("GetReportRev1")]
        public GetGetReportRev1 GetReportRev1(GetReportRev1 GR)
        {
            

            GetGetReportRev1 GetReportRev1result = new GetGetReportRev1();
            List<GetReportRev1Lists> GetReportRev1Details = new List<GetReportRev1Lists>();
            tempconnectionstring();
            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@cID", mytype = SqlDbType.NVarChar, Value = GR.cID });

            DataTable dtuserList = new DataTable();

            dtuserList = Connection.GetDataTable("GetReportRev1");
            if (dtuserList.Rows.Count > 0)
            {
                foreach (DataRow row in dtuserList.Rows)
                {
                    GetReportRev1Lists GetReportRev1List = new GetReportRev1Lists
                    {
                        
                        total = row["total"].ToString(),
                        pending = row["pending"].ToString(),
                        done = row["done"].ToString(),

                    };
                    GetReportRev1Details.Add(GetReportRev1List);
                }
                GetReportRev1result.result = GetReportRev1Details;
                GetReportRev1result.myreturn = "Success";
            }
            else
            {
                GetReportRev1result.myreturn = "Error";
            }
            return GetReportRev1result;
        }

        //GetReportRev2 return 
        [HttpPost]
        [Route("GetReportRev2")]
        public GetGetReportRev2 GetReportRev2(GetReportRev2 GR)
        {
           

            GetGetReportRev2 GetReportRev2result = new GetGetReportRev2();
            List<GetReportRev2Lists> GetReportRev2Details = new List<GetReportRev2Lists>();
            tempconnectionstring();

            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@cID", mytype = SqlDbType.NVarChar, Value = GR.cID });

            DataTable dtuserList = new DataTable();

            dtuserList = Connection.GetDataTable("GetReportRev2");
            if (dtuserList.Rows.Count > 0)
            {
                foreach (DataRow row in dtuserList.Rows)
                {
                    GetReportRev2Lists GetReportRev2List = new GetReportRev2Lists
                    {

                        total = row["total"].ToString(),
                        pending = row["pending"].ToString(),
                        done = row["done"].ToString(),

                    };
                    GetReportRev2Details.Add(GetReportRev2List);
                }
                GetReportRev2result.result = GetReportRev2Details;
                GetReportRev2result.myreturn = "Success";
            }
            else
            {
                GetReportRev2result.myreturn = "Error";
            }
            return GetReportRev2result;
        }

        //GetReportPlan return 
        [HttpPost]
        [Route("GetReportPlan")]
        public GetGetReportPlan GetReportPlan(GetReportPlan GR)
        {


            GetGetReportPlan GetReportPlanresult = new GetGetReportPlan();
            List<GetReportPlanLists> GetReportPlanDetails = new List<GetReportPlanLists>();
            tempconnectionstring();

            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@cID", mytype = SqlDbType.NVarChar, Value = GR.cID });

            DataTable dtuserList = new DataTable();

            dtuserList = Connection.GetDataTable("Report_SelfAppraisal");
            if (dtuserList.Rows.Count > 0)
            {
                foreach (DataRow row in dtuserList.Rows)
                {
                    GetReportPlanLists GetReportPlanList = new GetReportPlanLists
                    {

                        name = row["name"].ToString(),
                        total = row["total"].ToString(),
                        pending = row["pending"].ToString(),
                        done = row["done"].ToString(),
                        


                    };
                    GetReportPlanDetails.Add(GetReportPlanList);
                }
                GetReportPlanresult.result = GetReportPlanDetails;
                GetReportPlanresult.myreturn = "Success";
            }
            else
            {
                GetReportPlanresult.myreturn = "Error";
            }
            return GetReportPlanresult;
        }


        //GetReportsPlan return 
        [HttpPost]
        [Route("GetReportsPlan")]
        public GetGetReportsPlan GetReportsPlan(GetReportsPlan GR)
        {


            GetGetReportsPlan GetReportsPlanresult = new GetGetReportsPlan();
            List<GetReportsPlanLists> GetReportsPlanDetails = new List<GetReportsPlanLists>();
            tempconnectionstring();

            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@cID", mytype = SqlDbType.NVarChar, Value = GR.cID });

            DataTable dtuserList = new DataTable();

            dtuserList = Connection.GetDataTable("Report_SearchFilter");
            if (dtuserList.Rows.Count > 0)
            {
                foreach (DataRow row in dtuserList.Rows)
                {
                    GetReportsPlanLists GetReportsPlanList = new GetReportsPlanLists
                    {
                        EmpID = row["EmpID"].ToString(),
                        name = row["name"].ToString(),
                        ISName = row["ISName"].ToString(),
                        total_goalsettings = row["total_goalsettings"].ToString(),
                        pending_goalsettings = row["pending_goalsettings"].ToString(),
                        done_goalsettings = row["done_goalsettings"].ToString(),
                        total_midyear = row["total_midyear"].ToString(),
                        pending_midyear = row["pending_midyear"].ToString(),
                        done_midyear = row["done_midyear"].ToString(),
                        total_endyear = row["total_endyear"].ToString(),
                        pending_endyear = row["pending_endyear"].ToString(),
                        done_endyear = row["done_endyear"].ToString(),
                        done = row["done"].ToString(),
                        level = row["level"].ToString(),


                    };
                    GetReportsPlanDetails.Add(GetReportsPlanList);
                }
                GetReportsPlanresult.result = GetReportsPlanDetails;
                GetReportsPlanresult.myreturn = "Success";
            }
            else
            {
                GetReportsPlanresult.myreturn = "Error";
            }
            return GetReportsPlanresult;
        }


        //GetReportRev1Plan return 
        [HttpPost]
        [Route("GetReportRev1Plan")]
        public GetGetReportRev1Plan GetReportRev1Plan(GetReportRev1Plan GR)
        {


            GetGetReportRev1Plan GetReportRev1Planresult = new GetGetReportRev1Plan();
            List<GetReportRev1PlanLists> GetReportRev1PlanDetails = new List<GetReportRev1PlanLists>();
            tempconnectionstring();

            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@cID", mytype = SqlDbType.NVarChar, Value = GR.cID });

            DataTable dtuserList = new DataTable();

            dtuserList = Connection.GetDataTable("Report_Reviewer1");
            if (dtuserList.Rows.Count > 0)
            {
                foreach (DataRow row in dtuserList.Rows)
                {
                    GetReportRev1PlanLists GetReportsPlanList = new GetReportRev1PlanLists
                    {

                        name = row["name"].ToString(),
                        total = row["total"].ToString(),
                        pending = row["pending"].ToString(),
                        done = row["done"].ToString(),


                    };
                    GetReportRev1PlanDetails.Add(GetReportsPlanList);
                }
                GetReportRev1Planresult.result = GetReportRev1PlanDetails;
                GetReportRev1Planresult.myreturn = "Success";
            }
            else
            {
                GetReportRev1Planresult.myreturn = "Error";
            }
            return GetReportRev1Planresult;
        }

        //GetReportRev2Plan return 
        [HttpPost]
        [Route("GetReportRev2Plan")]
        public GetGetReportRev2Plan GetReportRev2Plan(GetReportRev2Plan GR)
        {


            GetGetReportRev2Plan GetReportRev2Planresult = new GetGetReportRev2Plan();
            List<GetReportRev2PlanLists> GetReportRev2PlanDetails = new List<GetReportRev2PlanLists>();
            tempconnectionstring();

            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@cID", mytype = SqlDbType.NVarChar, Value = GR.cID });

            DataTable dtuserList = new DataTable();

            dtuserList = Connection.GetDataTable("Report_Reviewer2");
            if (dtuserList.Rows.Count > 0)
            {
                foreach (DataRow row in dtuserList.Rows)
                {
                    GetReportRev2PlanLists GetReportRev2PlanList = new GetReportRev2PlanLists
                    {

                        name = row["name"].ToString(),
                        total = row["total"].ToString(),
                        pending = row["pending"].ToString(),
                        done = row["done"].ToString(),


                    };
                    GetReportRev2PlanDetails.Add(GetReportRev2PlanList);
                }
                GetReportRev2Planresult.result = GetReportRev2PlanDetails;
                GetReportRev2Planresult.myreturn = "Success";
            }
            else
            {
                GetReportRev2Planresult.myreturn = "Error";
            }
            return GetReportRev2Planresult;
        }

        //GetRating return 
        [HttpPost]
        [Route("GetRating")]
        public GetGetRating GetRating(GetRating GD)
        {
            

            GetGetRating GetGetRatingresult = new GetGetRating();
            List<GetRatingLists> GetRatingDetails = new List<GetRatingLists>();
            tempconnectionstring();
            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@company", mytype = SqlDbType.NVarChar, Value = GD.company });

            DataTable dtuserList = new DataTable();

            dtuserList = Connection.GetDataTable("GetRating");
            if (dtuserList.Rows.Count > 0)
            {
                foreach (DataRow row in dtuserList.Rows)
                {
                    GetRatingLists GetRatingList = new GetRatingLists
                    {
                        id = row["id"].ToString(),
                        name = row["name"].ToString(),
                        company = row["company"].ToString(),

                    };
                    GetRatingDetails.Add(GetRatingList);
                }
                GetGetRatingresult.result = GetRatingDetails;
                GetGetRatingresult.myreturn = "Success";
            }
            else
            {
                GetGetRatingresult.myreturn = "Error";
            }
            return GetGetRatingresult;
        }
        //GetScoreCard return 
        [HttpPost]
        [Route("GetScoreCard")]
        public GetGetScoreCard GetScoreCard(GetScoreCard GD)
        {
            

            GetGetScoreCard GetGetScoreCardresult = new GetGetScoreCard();
            List<GetScoreCardLists> GetScoreCardDetails = new List<GetScoreCardLists>();
            tempconnectionstring();
            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@company", mytype = SqlDbType.NVarChar, Value = GD.company });

            DataTable dtuserList = new DataTable();

            dtuserList = Connection.GetDataTable("GetScoreCard");
            if (dtuserList.Rows.Count > 0)
            {
                foreach (DataRow row in dtuserList.Rows)
                {
                    GetScoreCardLists GetScoreCardList = new GetScoreCardLists
                    {
                        id = row["id"].ToString(),
                        score_eval = row["score_eval"].ToString(),
                        weight = row["weight"].ToString(),
                        rating_id = row["rating_id"].ToString(),
                        result_id = row["result_id"].ToString(),
                        company = row["company"].ToString(),
                        alt_id = row["alt_id"].ToString(),

                    };
                    GetScoreCardDetails.Add(GetScoreCardList);
                }
                GetGetScoreCardresult.result = GetScoreCardDetails;
                GetGetScoreCardresult.myreturn = "Success";
            }
            else
            {
                GetGetScoreCardresult.myreturn = "Error";
            }
            return GetGetScoreCardresult;
        }
        //GetGoals return 
        [HttpPost]
        [Route("GetGoals")]
        public GetGetGoals GetGoals(GetGoals GD)
        {
           

            GetGetGoals GetGetGoalsresult = new GetGetGoals();
            List<GetGoalsLists> GetGoalsDetails = new List<GetGoalsLists>();
            tempconnectionstring();
            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@ID", mytype = SqlDbType.NVarChar, Value = GD.ID });
            DataTable dtuserList = new DataTable();

            dtuserList = Connection.GetDataTable("GetGoals");
            if (dtuserList.Rows.Count > 0)
            {
                foreach (DataRow row in dtuserList.Rows)
                {
                    GetGoalsLists GetGoalsList = new GetGoalsLists
                    {
                        id = row["id"].ToString(),
                      group_id = row["group_id"].ToString(),
                      goal_desc = row["goal_desc"].ToString(),
                      weight = row["weight"].ToString(),
                      com_missed = row["com_missed"].ToString(),
                      com_met = row["com_met"].ToString(),
                      com_exceeded = row["com_exceeded"].ToString(),
                      com_dept = row["com_dept"].ToString(),
                      start_rev1_com = row["start_rev1_com"].ToString(),
                      start_rev2_com = row["start_rev2_com"].ToString(),
                      mid_results = row["mid_results"].ToString(),
                      mid_remaining_plan = row["mid_remaining_plan"].ToString(),
                      mid_rev1_com = row["mid_rev1_com"].ToString(),
                      mid_rev2_com = row["mid_rev2_com"].ToString(),
                      end_results = row["end_results"].ToString(),
                      end_init_achievement = row["end_init_achievement"].ToString(),
                      end_rev1_com = row["end_rev1_com"].ToString(),
                      end_rev2_com = row["end_rev2_com"].ToString(),
                      end_final_achievement = row["end_final_achievement"].ToString(),
                      goal_type = row["goal_type"].ToString()
                    };
                    GetGoalsDetails.Add(GetGoalsList);
                }
                GetGetGoalsresult.result = GetGoalsDetails;
                GetGetGoalsresult.myreturn = "Success";
            }
            else
            {
                GetGetGoalsresult.myreturn = "Error";
            }
            return GetGetGoalsresult;
        }


        //GetDates return 
        [HttpPost]
        [Route("isAuth")]
        public GetisAuth isAuth(isAuth IA)
        {
            

            GetisAuth GetisAuthresult = new GetisAuth();
            List<isAuthLists> GetisAuthDetails = new List<isAuthLists>();
            tempconnectionstring();
            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@company", mytype = SqlDbType.NVarChar, Value = IA.company });
            Connection.myparameters.Add(new myParameters { ParameterName = "@position", mytype = SqlDbType.NVarChar, Value = IA.position });
            Connection.myparameters.Add(new myParameters { ParameterName = "@level", mytype = SqlDbType.NVarChar, Value = IA.level });

            DataTable dtuserList = new DataTable();

            dtuserList = Connection.GetDataTable("isAuth");
            if (dtuserList.Rows.Count > 0)
            {
                foreach (DataRow row in dtuserList.Rows)
                {
                    isAuthLists isAuthList = new isAuthLists
                    {
                        result = row["result"].ToString()

                    };
                    GetisAuthDetails.Add(isAuthList);
                }
                GetisAuthresult.result = GetisAuthDetails;
                GetisAuthresult.myreturn = "Success";
            }
            else
            {
                GetisAuthresult.myreturn = "Error";
            }
            return GetisAuthresult;
        }


        //UpdateGoals return 
        [HttpPost]
        [Route("UpdateGoals")]
        public GetUpdateGoals UpdateGoals(UpdateGoals UG)
        {
            

            GetUpdateGoals GetUpdateGoalsresult = new GetUpdateGoals();
            List<UpdateGoalsLists> UpdateGoalsDetails = new List<UpdateGoalsLists>();
            tempconnectionstring();
            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@id", mytype = SqlDbType.NVarChar, Value = UG.id });
            Connection.myparameters.Add(new myParameters { ParameterName = "@goal_desc", mytype = SqlDbType.NVarChar, Value = UG.goal_desc });
            Connection.myparameters.Add(new myParameters { ParameterName = "@weight", mytype = SqlDbType.NVarChar, Value = UG.weight });
            Connection.myparameters.Add(new myParameters { ParameterName = "@com_missed", mytype = SqlDbType.NVarChar, Value = UG.com_missed });
            Connection.myparameters.Add(new myParameters { ParameterName = "@com_met", mytype = SqlDbType.NVarChar, Value = UG.com_met });
            Connection.myparameters.Add(new myParameters { ParameterName = "@com_exceeded", mytype = SqlDbType.NVarChar, Value = UG.com_exceeded });
            Connection.myparameters.Add(new myParameters { ParameterName = "@com_dept", mytype = SqlDbType.NVarChar, Value = UG.com_dept });
            Connection.myparameters.Add(new myParameters { ParameterName = "@start_rev1_com", mytype = SqlDbType.NVarChar, Value = UG.start_rev1_com });
            Connection.myparameters.Add(new myParameters { ParameterName = "@start_rev2_com", mytype = SqlDbType.NVarChar, Value = UG.start_rev2_com });
            Connection.myparameters.Add(new myParameters { ParameterName = "@mid_results", mytype = SqlDbType.NVarChar, Value = UG.mid_results });
            Connection.myparameters.Add(new myParameters { ParameterName = "@mid_remaining_plan", mytype = SqlDbType.NVarChar, Value = UG.mid_remaining_plan });
            Connection.myparameters.Add(new myParameters { ParameterName = "@mid_rev1_com", mytype = SqlDbType.NVarChar, Value = UG.mid_rev1_com });
            Connection.myparameters.Add(new myParameters { ParameterName = "@mid_rev2_com", mytype = SqlDbType.NVarChar, Value = UG.mid_rev2_com });
            Connection.myparameters.Add(new myParameters { ParameterName = "@end_results", mytype = SqlDbType.NVarChar, Value = UG.end_results });
            Connection.myparameters.Add(new myParameters { ParameterName = "@end_init_achievement", mytype = SqlDbType.NVarChar, Value = UG.end_init_achievement });
            Connection.myparameters.Add(new myParameters { ParameterName = "@end_rev1_com", mytype = SqlDbType.NVarChar, Value = UG.end_rev1_com });
            Connection.myparameters.Add(new myParameters { ParameterName = "@end_rev2_com", mytype = SqlDbType.NVarChar, Value = UG.end_rev2_com });
            Connection.myparameters.Add(new myParameters { ParameterName = "@end_final_achievement", mytype = SqlDbType.NVarChar, Value = UG.end_final_achievement });

            DataTable dtuserList = new DataTable();

            dtuserList = Connection.GetDataTable("UpdateGoals");
            if (dtuserList.Rows.Count > 0)
            {
                foreach (DataRow row in dtuserList.Rows)
                {
                    UpdateGoalsLists UpdateGoalsList = new UpdateGoalsLists
                    {

                    };
                    UpdateGoalsDetails.Add(UpdateGoalsList);
                }
                GetUpdateGoalsresult.result = UpdateGoalsDetails;
                GetUpdateGoalsresult.myreturn = "Success";
            }
            else
            {
                GetUpdateGoalsresult.myreturn = "Error";
            }
            return GetUpdateGoalsresult;
        }

        //UpdateGoalPlan return 
        [HttpPost]
        [Route("UpdateGoalPlan")]
        public GetUpdateGoalPlan UpdateGoalPlan(UpdateGoalPlan UG)
        {


            GetUpdateGoalPlan UpdateGoalPlanresult = new GetUpdateGoalPlan();
            List<UpdateGoalPlanLists> UpdateGoalPlanDetails = new List<UpdateGoalPlanLists>();
            tempconnectionstring();
            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@id", mytype = SqlDbType.NVarChar, Value = UG.id });
            Connection.myparameters.Add(new myParameters { ParameterName = "@goal_name", mytype = SqlDbType.NVarChar, Value = UG.goal_name });
            Connection.myparameters.Add(new myParameters { ParameterName = "@weight", mytype = SqlDbType.NVarChar, Value = UG.weight });
            Connection.myparameters.Add(new myParameters { ParameterName = "@description", mytype = SqlDbType.NVarChar, Value = UG.description });
            Connection.myparameters.Add(new myParameters { ParameterName = "@com_missed", mytype = SqlDbType.NVarChar, Value = UG.com_missed });
            Connection.myparameters.Add(new myParameters { ParameterName = "@com_met", mytype = SqlDbType.NVarChar, Value = UG.com_met });
            Connection.myparameters.Add(new myParameters { ParameterName = "@com_exceeded", mytype = SqlDbType.NVarChar, Value = UG.com_exceeded });
            Connection.myparameters.Add(new myParameters { ParameterName = "@com_dept", mytype = SqlDbType.NVarChar, Value = UG.com_dept });
            Connection.myparameters.Add(new myParameters { ParameterName = "@goal_type", mytype = SqlDbType.NVarChar, Value = UG.goal_type });

            DataTable dtuserList = new DataTable();

            dtuserList = Connection.GetDataTable("UpdateGoalPlan");
            if (dtuserList.Rows.Count > 0)
            {
                foreach (DataRow row in dtuserList.Rows)
                {
                    UpdateGoalPlanLists UpdateGoalPlanList = new UpdateGoalPlanLists
                    {


                    };
                    UpdateGoalPlanDetails.Add(UpdateGoalPlanList);
                }
                UpdateGoalPlanresult.result = UpdateGoalPlanDetails;
                UpdateGoalPlanresult.myreturn = "Success";
            }
            else
            {
                UpdateGoalPlanresult.myreturn = "Error";
            }
            return UpdateGoalPlanresult;
        }


        
        [HttpPost]
        [Route("ValidateCompany")]
        public GetValidateCompany ValidateCompany(ValidateCompany VC)
        {

            GetValidateCompany ValidateCompanyresult = new GetValidateCompany();
            List<ValidateCompanyLists> ValidateCompanyDetails = new List<ValidateCompanyLists>();
            tempconnectionstring();
            Connection Connection = new Connection();
            Connection.myparameters.Add(new myParameters { ParameterName = "@company", mytype = SqlDbType.NVarChar, Value = VC.company });
            DataTable dtuserList = new DataTable();

            dtuserList = Connection.GetDataTable("pa_CheckClientExist");
            if (dtuserList.Rows.Count > 0)
            {
                foreach (DataRow row in dtuserList.Rows)
                {
                    ValidateCompanyLists ValidateCompanyList = new ValidateCompanyLists
                    {
                        ClientID = row["ClientID"].ToString(),
                        DB_Settings = row["DB_Settings"].ToString()
                    };
                    ValidateCompanyDetails.Add(ValidateCompanyList);
                }
                ValidateCompanyresult.result = ValidateCompanyDetails;
                ValidateCompanyresult.myreturn = "Success";
            }
            else
            {
                ValidateCompanyresult.myreturn = "Error";
            }
            return ValidateCompanyresult;
        }

        //CheckCompanyAccountExist return 
        [HttpPost]
        [Route("CheckCompanyAccountExist")]
        public GetCheckCompanyAccountExist CheckCompanyAccountExist(CheckCompanyAccountExist VC)
        {

            GetCheckCompanyAccountExist CheckCompanyAccountExistresult = new GetCheckCompanyAccountExist();
            List<CheckCompanyAccountExistLists> CheckCompanyAccountExistDetails = new List<CheckCompanyAccountExistLists>();
            tempconnectionstring();
            Connection Connection = new Connection();
            Connection.myparameters.Add(new myParameters { ParameterName = "@company", mytype = SqlDbType.NVarChar, Value = VC.company });
            DataTable dtuserList = new DataTable();

            dtuserList = Connection.GetDataTable("CheckCompanyAccountExist");
            if (dtuserList.Rows.Count > 0)
            {
                foreach (DataRow row in dtuserList.Rows)
                {
                    CheckCompanyAccountExistLists CheckCompanyAccountExistList = new CheckCompanyAccountExistLists
                    {
                        result = row["result"].ToString()
                    };
                    CheckCompanyAccountExistDetails.Add(CheckCompanyAccountExistList);
                }
                CheckCompanyAccountExistresult.result = CheckCompanyAccountExistDetails;
                CheckCompanyAccountExistresult.myreturn = "Success";
            }
            else
            {
                CheckCompanyAccountExistresult.myreturn = "Error";
            }
            return CheckCompanyAccountExistresult;
        }

        //GetDate return 
        [HttpPost]
        [Route("GetDate")]
        public GetGetDate GetDate(GetDate UG)
        {
            

            GetGetDate GetDateresult = new GetGetDate();
            List<GetDateLists> GetDateDetails = new List<GetDateLists>();
            tempconnectionstring();
            Connection Connection = new Connection();
            Connection.myparameters.Add(new myParameters { ParameterName = "@company", mytype = SqlDbType.NVarChar, Value = UG.company });

            DataTable dtuserList = new DataTable();

            dtuserList = Connection.GetDataTable("GetDate");
            if (dtuserList.Rows.Count > 0)
            {
                foreach (DataRow row in dtuserList.Rows)
                {
                    GetDateLists GetDateList = new GetDateLists
                    {
                        id = row["id"].ToString(),
                        name = row["name"].ToString(),
                        date_id = row["date_id"].ToString(),
                        date_start = row["date_start"].ToString(),
                        date_end = row["date_end"].ToString(),

                    };
                    GetDateDetails.Add(GetDateList);
                }
                GetDateresult.result = GetDateDetails;
                GetDateresult.myreturn = "Success";
            }
            else
            {
                GetDateresult.myreturn = "Error";
            }
            return GetDateresult;
        }

        //GetRev1 return 
        [HttpPost]
        [Route("GetRev1")]
        public GetGetRev1 GetRev1(GetRev1 GR)
        {
            

            GetGetRev1 GetRev1result = new GetGetRev1();
            List<GetRev1Lists> GetRev1Details = new List<GetRev1Lists>();
            tempconnectionstring();
            Connection Connection = new Connection();


            DataTable dtuserList = new DataTable();

            dtuserList = Connection.GetDataTable("GetRev1");
            if (dtuserList.Rows.Count > 0)
            {
                foreach (DataRow row in dtuserList.Rows)
                {
                    GetRev1Lists GetRev1List = new GetRev1Lists
                    {
                        id = row["id"].ToString()

                    };
                    GetRev1Details.Add(GetRev1List);
                }
                GetRev1result.result = GetRev1Details;
                GetRev1result.myreturn = "Success";
            }
            else
            {
                GetRev1result.myreturn = "Error";
            }
            return GetRev1result;
        }

        //GetRev2 return 
        [HttpPost]
        [Route("GetRev2")]
        public GetGetRev2 GetRev2(GetRev2 GR)
        {
           

            GetGetRev2 GetRev2result = new GetGetRev2();
            List<GetRev2Lists> GetRev2Details = new List<GetRev2Lists>();
            tempconnectionstring();
            Connection Connection = new Connection();


            DataTable dtuserList = new DataTable();

            dtuserList = Connection.GetDataTable("GetRev2");
            if (dtuserList.Rows.Count > 0)
            {
                foreach (DataRow row in dtuserList.Rows)
                {
                    GetRev2Lists GetRev2List = new GetRev2Lists
                    {
                        id = row["id"].ToString()

                    };
                    GetRev2Details.Add(GetRev2List);
                }
                GetRev2result.result = GetRev2Details;
                GetRev2result.myreturn = "Success";
            }
            else
            {
                GetRev2result.myreturn = "Error";
            }
            return GetRev2result;
        }

        //SetGoals return 
        [HttpPost]
        [Route("SetGoals")]
        public GetSetGoals SetGoals(SetGoals SG)
        {
            

            GetSetGoals SetGoalsresult = new GetSetGoals();
            List<SetGoalsLists> SetGoalsDetails = new List<SetGoalsLists>();
            tempconnectionstring();
            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@group_id", mytype = SqlDbType.NVarChar, Value = SG.group_id });
            Connection.myparameters.Add(new myParameters { ParameterName = "@goal_desc", mytype = SqlDbType.NVarChar, Value = SG.goal_desc });
            Connection.myparameters.Add(new myParameters { ParameterName = "@weight", mytype = SqlDbType.NVarChar, Value = SG.weight });
            Connection.myparameters.Add(new myParameters { ParameterName = "@com_missed", mytype = SqlDbType.NVarChar, Value = SG.com_missed });
            Connection.myparameters.Add(new myParameters { ParameterName = "@com_met", mytype = SqlDbType.NVarChar, Value = SG.com_met });
            Connection.myparameters.Add(new myParameters { ParameterName = "@com_exceeded", mytype = SqlDbType.NVarChar, Value = SG.com_exceeded });
            Connection.myparameters.Add(new myParameters { ParameterName = "@com_dept", mytype = SqlDbType.NVarChar, Value = SG.com_dept });
            Connection.myparameters.Add(new myParameters { ParameterName = "@start_rev1_com", mytype = SqlDbType.NVarChar, Value = SG.start_rev1_com});
            Connection.myparameters.Add(new myParameters { ParameterName = "@start_rev2_com", mytype = SqlDbType.NVarChar, Value = SG.start_rev2_com});
            Connection.myparameters.Add(new myParameters { ParameterName = "@mid_results", mytype = SqlDbType.NVarChar, Value = SG.mid_results });
            Connection.myparameters.Add(new myParameters { ParameterName = "@mid_remaining_plan", mytype = SqlDbType.NVarChar, Value = SG.mid_remaining_plan });
            Connection.myparameters.Add(new myParameters { ParameterName = "@mid_rev1_com", mytype = SqlDbType.NVarChar, Value = SG.mid_rev1_com});
            Connection.myparameters.Add(new myParameters { ParameterName = "@mid_rev2_com", mytype = SqlDbType.NVarChar, Value = SG.mid_rev2_com});
            Connection.myparameters.Add(new myParameters { ParameterName = "@end_results", mytype = SqlDbType.NVarChar, Value = SG.end_results });
            Connection.myparameters.Add(new myParameters { ParameterName = "@end_init_achievement", mytype = SqlDbType.NVarChar, Value = SG.end_init_achievement });
            Connection.myparameters.Add(new myParameters { ParameterName = "@end_rev1_com", mytype = SqlDbType.NVarChar, Value = SG.end_rev1_com });
            Connection.myparameters.Add(new myParameters { ParameterName = "@end_rev2_com", mytype = SqlDbType.NVarChar, Value = SG.end_rev2_com});
            Connection.myparameters.Add(new myParameters { ParameterName = "@end_final_achievement", mytype = SqlDbType.NVarChar, Value = SG.end_final_achievement });
            Connection.myparameters.Add(new myParameters { ParameterName = "@goal_type", mytype = SqlDbType.NVarChar, Value = SG.goal_type });

            DataTable dtuserList = new DataTable();

            dtuserList = Connection.GetDataTable("SetGoals");
            if (dtuserList.Rows.Count > 0)
            {
                foreach (DataRow row in dtuserList.Rows)
                {
                    SetGoalsLists SetGoalsList = new SetGoalsLists
                    {
                    //   id = row['id'].ToString(),
                    //   group_id = row['group_id'].ToString(),
                    //   goal_desc = row['goal_desc'].ToString(),
                    //   weight = row['weight'].ToString(),
                    //   com_missed = row['com_missed'].ToString(),
                    //   com_met = row['com_met'].ToString(),
                    //   com_exceeded = row['com_exceeded'].ToString(),
                    //   com_dept = row['com_dept'].ToString(),
                    //   start_rev1_comment = row['start_rev1_comment'].ToString(),
                    //   start_rev2_comment = row['start_rev2_comment'].ToString(),
                    //   mid_results = row['mid_results'].ToString(),
                    //   mid_remaining_plan = row['mid_remaining_plan'].ToString(),
                    //   mid_rev1_comment = row['mid_rev1_comment'].ToString(),
                    //   mid_rev2_comment = row['mid_rev2_comment'].ToString(),
                    //   end_results = row['end_results'].ToString(),
                    //   end_init_achievement = row['end_init_achievement'].ToString(),
                    //   end_rev1_comment = row['end_rev1_comment'].ToString(),
                    //   end_rev2_comment = row['end_rev2_comment'].ToString(),
                    //   end_final_achievement = row['end_final_achievement'].ToString(),
                    //   goal_type = row['goal_type'].ToString()
                    };
                    SetGoalsDetails.Add(SetGoalsList);
                }
                SetGoalsresult.result = SetGoalsDetails;
                SetGoalsresult.myreturn = "Success";
            }
            else
            {
                SetGoalsresult.myreturn = "Error";
            }
            return SetGoalsresult;
        }
        //GetMaxGroupId return 
        [HttpPost]
        [Route("GetMaxGroupId")]
        public GetGetMaxGroupId GetMaxGroupId(GetMaxGroupId GM)
        {
            

            GetGetMaxGroupId GetMaxGroupIdresult = new GetGetMaxGroupId();
            List<GetMaxGroupIdLists> GetMaxGroupIdDetails = new List<GetMaxGroupIdLists>();
            tempconnectionstring();
            Connection Connection = new Connection();


            DataTable dtuserList = new DataTable();

            dtuserList = Connection.GetDataTable("GetMaxGroupId");
            if (dtuserList.Rows.Count > 0)
            {
                foreach (DataRow row in dtuserList.Rows)
                {
                    GetMaxGroupIdLists GetMaxGroupIdList = new GetMaxGroupIdLists
                    {
                        max = row["max"].ToString(),
                        
                    };
                    GetMaxGroupIdDetails.Add(GetMaxGroupIdList);
                }
                GetMaxGroupIdresult.result = GetMaxGroupIdDetails;
                GetMaxGroupIdresult.myreturn = "Success";
            }
            else
            {
                GetMaxGroupIdresult.myreturn = "Error";
            }
            return GetMaxGroupIdresult;
        }
    
        //SetGroup SP
        [HttpPost]
        [Route("SetGroup")]
        public GetSetGroup SetGroup(SetGroup SG)
        {


            GetSetGroup SetGroupresult = new GetSetGroup();
            List<SetGroupLists> SetGroupDetails = new List<SetGroupLists>();
            tempconnectionstring();
            Connection Connection = new Connection();
            
            //Connection.myparameters.Add(new myParameters { ParameterName = "@notif", mytype = SqlDbType.NVarChar, Value = SG.notif });
            Connection.myparameters.Add(new myParameters { ParameterName = "@doer_id", mytype = SqlDbType.NVarChar, Value = SG.doer_id });
            //Connection.myparameters.Add(new myParameters { ParameterName = "@result_fin", mytype = SqlDbType.NVarChar, Value = SG.result_fin });
            Connection.myparameters.Add(new myParameters { ParameterName = "@date_created", mytype = SqlDbType.NVarChar, Value = SG.date_created });
            //Connection.myparameters.Add(new myParameters { ParameterName = "@emp_edit", mytype = SqlDbType.NVarChar, Value = SG.emp_edit });
            //Connection.myparameters.Add(new myParameters { ParameterName = "@rev1_edit", mytype = SqlDbType.NVarChar, Value = SG.rev1_edit });

            DataTable dtuserList = new DataTable();

            dtuserList = Connection.GetDataTable("SetGroup");
            if (dtuserList.Rows.Count > 0)
            {
                foreach (DataRow row in dtuserList.Rows)
                {
                    SetGroupLists SetGroupLists = new SetGroupLists
                    {
                        id = row["id"].ToString()
                    };
                    SetGroupDetails.Add(SetGroupLists);
                }
                SetGroupresult.result = SetGroupDetails;
                SetGroupresult.myreturn = "Success";
            }
            else
            {
                SetGroupresult.myreturn = "Error";
            }
            return SetGroupresult;
        }

        //SetAck SP
        [HttpPost]
        [Route("SetAck")]
        public string SetAck(SetAck SA)
        {
           

            //GetDatabase GDB = new GetDatabase();
            //GDB.ClientName = ClientData.CN;
            tempconnectionstring();
            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@group_id", mytype = SqlDbType.NVarChar, Value = SA.group_id });
            Connection.myparameters.Add(new myParameters { ParameterName = "@ack_lvl", mytype = SqlDbType.NVarChar, Value = SA.ack_lvl });
            Connection.myparameters.Add(new myParameters { ParameterName = "@status", mytype = SqlDbType.NVarChar, Value = SA.status });
            Connection.myparameters.Add(new myParameters { ParameterName = "@creator_id", mytype = SqlDbType.NVarChar, Value = SA.creator_id });
            Connection.myparameters.Add(new myParameters { ParameterName = "@date_req", mytype = SqlDbType.NVarChar, Value = SA.date_req });
            Connection.myparameters.Add(new myParameters { ParameterName = "@cID", mytype = SqlDbType.NVarChar, Value = SA.cID });

            // if (string.IsNullOrEmpty(SA.date_ack))
            // {
            //    Connection.myparameters.Add(new myParameters { ParameterName = "@date_ack", mytype = SqlDbType.NVarChar, Value = DBNull.Value });
            // }else
            // {
            //      Connection.myparameters.Add(new myParameters { ParameterName = "@date_ack", mytype = SqlDbType.NVarChar, Value = SA.date_ack });
            // }

            Connection.ExecuteNonQuery("SetAck");
            return "success";

        }

        //SetAckPlan SP
        [HttpPost]
        [Route("SetAckPlan")]
        public string SetAckPlan(SetAckPlan SA)
        {


            //GetDatabase GDB = new GetDatabase();
            //GDB.ClientName = ClientData.CN;
            tempconnectionstring();
            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@group_id", mytype = SqlDbType.NVarChar, Value = SA.group_id });
            Connection.myparameters.Add(new myParameters { ParameterName = "@status", mytype = SqlDbType.NVarChar, Value = SA.status });
            Connection.myparameters.Add(new myParameters { ParameterName = "@creator_id", mytype = SqlDbType.NVarChar, Value = SA.creator_id });
            Connection.myparameters.Add(new myParameters { ParameterName = "@date_req", mytype = SqlDbType.NVarChar, Value = SA.date_req });
            Connection.myparameters.Add(new myParameters { ParameterName = "@cID", mytype = SqlDbType.NVarChar, Value = SA.cID });

            Connection.ExecuteNonQuery("SetAckPlan");
            return "success";

        }

        //SetAckPlan SP
        [HttpPost]
        [Route("SetAckPlanRev1")]
        public string SetAckPlanRev1(SetAckPlanRev1 SA)
        {


            //GetDatabase GDB = new GetDatabase();
            //GDB.ClientName = ClientData.CN;
            tempconnectionstring();
            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@group_id", mytype = SqlDbType.NVarChar, Value = SA.group_id });
            Connection.myparameters.Add(new myParameters { ParameterName = "@status", mytype = SqlDbType.NVarChar, Value = SA.status });
            Connection.myparameters.Add(new myParameters { ParameterName = "@reviewer1", mytype = SqlDbType.NVarChar, Value = SA.reviewer1 });
            Connection.myparameters.Add(new myParameters { ParameterName = "@date_rev1", mytype = SqlDbType.NVarChar, Value = SA.date_rev1 });

            Connection.ExecuteNonQuery("SetAckPlanRev1");
            return "success";

        }

        //SetAckPlan SP
        [HttpPost]
        [Route("SetAckPlanRev2")]
        public string SetAckPlanRev2(SetAckPlanRev2 SA)
        {


            //GetDatabase GDB = new GetDatabase();
            //GDB.ClientName = ClientData.CN;
            tempconnectionstring();
            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@group_id", mytype = SqlDbType.NVarChar, Value = SA.group_id });
            Connection.myparameters.Add(new myParameters { ParameterName = "@status", mytype = SqlDbType.NVarChar, Value = SA.status });
            Connection.myparameters.Add(new myParameters { ParameterName = "@reviewer2", mytype = SqlDbType.NVarChar, Value = SA.reviewer2 });
            Connection.myparameters.Add(new myParameters { ParameterName = "@date_rev2", mytype = SqlDbType.NVarChar, Value = SA.date_rev2 });

            Connection.ExecuteNonQuery("SetAckPlanRev2");
            return "success";

        }


        //SetComment SP
        [HttpPost]
        [Route("SetComment")]
        public string SetComment(SetComment SC)
        {
            

            //GetDatabase GDB = new GetDatabase();
            //GDB.ClientName = ClientData.CN;
            tempconnectionstring();
            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@goal_id", mytype = SqlDbType.NVarChar, Value = SC.goal_id });
            Connection.myparameters.Add(new myParameters { ParameterName = "@str", mytype = SqlDbType.NVarChar, Value = SC.str });
            Connection.myparameters.Add(new myParameters { ParameterName = "@dnp", mytype = SqlDbType.NVarChar, Value = SC.dnp });
            Connection.myparameters.Add(new myParameters { ParameterName = "@com_lvl", mytype = SqlDbType.NVarChar, Value = SC.com_lvl });
            Connection.myparameters.Add(new myParameters { ParameterName = "@date", mytype = SqlDbType.NVarChar, Value = SC.date });
            
            Connection.ExecuteNonQuery("SetComment");
            return "success";

        }


        //UpdateComment SP
        [HttpPost]
        [Route("UpdateComment")]
        public string UpdateComment(UpdateComment UP)
        {


            //GetDatabase GDB = new GetDatabase();
            //GDB.ClientName = ClientData.CN;
            tempconnectionstring();
            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@goal_id", mytype = SqlDbType.NVarChar, Value = UP.goal_id });
            Connection.myparameters.Add(new myParameters { ParameterName = "@com_level", mytype = SqlDbType.NVarChar, Value = UP.com_level });
            Connection.myparameters.Add(new myParameters { ParameterName = "@str", mytype = SqlDbType.NVarChar, Value = UP.str });
            Connection.myparameters.Add(new myParameters { ParameterName = "@dnp", mytype = SqlDbType.NVarChar, Value = UP.dnp });

            Connection.ExecuteNonQuery("UpdateComment");
            return "success";

        }


        //UpdateSettings SP
        [HttpPost]
        [Route("UpdateSettings")]
        public string UpdateSettings(UpdateSettings UP)
        {
            

            //GetDatabase GDB = new GetDatabase();
            //GDB.ClientName = ClientData.CN;
            tempconnectionstring();
            Connection Connection = new Connection();
            

            Connection.myparameters.Add(new myParameters { ParameterName = "@cID", mytype = SqlDbType.NVarChar, Value = UP.cID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@rev1_days", mytype = SqlDbType.NVarChar, Value = UP.rev1_days });
            Connection.myparameters.Add(new myParameters { ParameterName = "@rev2_days", mytype = SqlDbType.NVarChar, Value = UP.rev2_days });
            Connection.myparameters.Add(new myParameters { ParameterName = "@message", mytype = SqlDbType.NVarChar, Value = UP.message });
            Connection.myparameters.Add(new myParameters { ParameterName = "@message_days", mytype = SqlDbType.NVarChar, Value = UP.message_days });
            Connection.myparameters.Add(new myParameters { ParameterName = "@rev1_mode", mytype = SqlDbType.NVarChar, Value = UP.rev1_mode });
            Connection.myparameters.Add(new myParameters { ParameterName = "@rev2_mode", mytype = SqlDbType.NVarChar, Value = UP.rev2_mode });
            Connection.myparameters.Add(new myParameters { ParameterName = "@message_mode", mytype = SqlDbType.NVarChar, Value = UP.message_mode });

            Connection.ExecuteNonQuery("UpdateSettings1");
            return "success";

        }

        //UpdateAckGroup SP
        [HttpPost]
        [Route("UpdateGroupApprove")]
        public string UpdateGroupApprove(UpdateGroupApprove UP)
        {


            //GetDatabase GDB = new GetDatabase();
            //GDB.ClientName = ClientData.CN;
            tempconnectionstring();
            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@id", mytype = SqlDbType.NVarChar, Value = UP.id });

            Connection.ExecuteNonQuery("UpdateGroupApprove");
            return "success";

        }


        //UpdateAck SP
        [HttpPost]
        [Route("UpdateAck")]
        public string UpdateAck(UpdateAck UP)
        {
            

            //GetDatabase GDB = new GetDatabase();
            //GDB.ClientName = ClientData.CN;
            tempconnectionstring();
            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@group_id", mytype = SqlDbType.NVarChar, Value = UP.group_id });
            Connection.myparameters.Add(new myParameters { ParameterName = "@ack_lvl", mytype = SqlDbType.NVarChar, Value = UP.ack_lvl });
            Connection.myparameters.Add(new myParameters { ParameterName = "@status", mytype = SqlDbType.NVarChar, Value = UP.status });
            Connection.myparameters.Add(new myParameters { ParameterName = "@creator_id", mytype = SqlDbType.NVarChar, Value = UP.creator_id });
            Connection.myparameters.Add(new myParameters { ParameterName = "@reviewer1", mytype = SqlDbType.NVarChar, Value = UP.reviewer1 });
            Connection.myparameters.Add(new myParameters { ParameterName = "@reviewer2", mytype = SqlDbType.NVarChar, Value = UP.reviewer2 });
            Connection.myparameters.Add(new myParameters { ParameterName = "@date_req", mytype = SqlDbType.NVarChar, Value = UP.date_req });
            Connection.myparameters.Add(new myParameters { ParameterName = "@date_rev1", mytype = SqlDbType.NVarChar, Value = UP.date_rev1 });
            Connection.myparameters.Add(new myParameters { ParameterName = "@date_rev2", mytype = SqlDbType.NVarChar, Value = UP.date_rev2 });
            Connection.myparameters.Add(new myParameters { ParameterName = "@date_done", mytype = SqlDbType.NVarChar, Value = UP.date_done });
            Connection.myparameters.Add(new myParameters { ParameterName = "@escalated", mytype = SqlDbType.NVarChar, Value = UP.escalated });
            Connection.myparameters.Add(new myParameters { ParameterName = "@modified_by", mytype = SqlDbType.NVarChar, Value = UP.modified_by });

            Connection.ExecuteNonQuery("UpdateAck");
            return "success";

        }

        //UpdateAckPlan SP
        [HttpPost]
        [Route("UpdateAckPlan")]
        public string UpdateAckPlan(UpdateAckPlan UP)
        {


            //GetDatabase GDB = new GetDatabase();
            //GDB.ClientName = ClientData.CN;
            tempconnectionstring();
            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@id", mytype = SqlDbType.NVarChar, Value = UP.id });
            Connection.myparameters.Add(new myParameters { ParameterName = "@date_done", mytype = SqlDbType.NVarChar, Value = UP.date_done });

            Connection.ExecuteNonQuery("UpdateAckPlan");
            return "success";

        }

        //UpdateRev1 SP
        [HttpPost]
        [Route("UpdateRev1")]
        public string UpdateRev1(UpdateRev1 UP)
        {
            

            //GetDatabase GDB = new GetDatabase();
            //GDB.ClientName = ClientData.CN;
            tempconnectionstring();
            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@id", mytype = SqlDbType.NVarChar, Value = UP.id });
            Connection.myparameters.Add(new myParameters { ParameterName = "@ack_lvl", mytype = SqlDbType.NVarChar, Value = UP.ack_lvl });
            Connection.myparameters.Add(new myParameters { ParameterName = "@reviewer1", mytype = SqlDbType.NVarChar, Value = UP.reviewer1 });
            Connection.myparameters.Add(new myParameters { ParameterName = "@date_rev1", mytype = SqlDbType.NVarChar, Value = UP.date_rev1 });
            Connection.myparameters.Add(new myParameters { ParameterName = "@status", mytype = SqlDbType.NVarChar, Value = UP.status });
            Connection.myparameters.Add(new myParameters { ParameterName = "@modified_by", mytype = SqlDbType.NVarChar, Value = UP.modified_by });

            Connection.ExecuteNonQuery("UpdateRev1");
            return "success";

        }


        //UpdatePlanRev1 SP
        [HttpPost]
        [Route("UpdatePlanRev1")]
        public string UpdatePlanRev1(UpdatePlanRev1 UP)
        {


            //GetDatabase GDB = new GetDatabase();
            //GDB.ClientName = ClientData.CN;
            tempconnectionstring();
            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@id", mytype = SqlDbType.NVarChar, Value = UP.id });
            Connection.myparameters.Add(new myParameters { ParameterName = "@ack_lvl", mytype = SqlDbType.NVarChar, Value = UP.ack_lvl });
            Connection.myparameters.Add(new myParameters { ParameterName = "@reviewer1", mytype = SqlDbType.NVarChar, Value = UP.reviewer1 });
            Connection.myparameters.Add(new myParameters { ParameterName = "@date_rev1", mytype = SqlDbType.NVarChar, Value = UP.date_rev1 });
            Connection.myparameters.Add(new myParameters { ParameterName = "@status", mytype = SqlDbType.NVarChar, Value = UP.status });

            Connection.ExecuteNonQuery("UpdatePlanRev1");
            return "success";

        }


        //UpdateRev1 SP
        [HttpPost]
        [Route("UpdateRev2")]
        public string UpdateRev2(UpdateRev2 UP)
        {
           

            //GetDatabase GDB = new GetDatabase();
            //GDB.ClientName = ClientData.CN;
            tempconnectionstring();
            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@id", mytype = SqlDbType.NVarChar, Value = UP.id });
            Connection.myparameters.Add(new myParameters { ParameterName = "@ack_lvl", mytype = SqlDbType.NVarChar, Value = UP.ack_lvl });
            Connection.myparameters.Add(new myParameters { ParameterName = "@reviewer2", mytype = SqlDbType.NVarChar, Value = UP.reviewer2 });
            Connection.myparameters.Add(new myParameters { ParameterName = "@date_rev2", mytype = SqlDbType.NVarChar, Value = UP.date_rev2 });
            Connection.myparameters.Add(new myParameters { ParameterName = "@status", mytype = SqlDbType.NVarChar, Value = UP.status });
            Connection.myparameters.Add(new myParameters { ParameterName = "@modified_by", mytype = SqlDbType.NVarChar, Value = UP.modified_by });

            Connection.ExecuteNonQuery("UpdateRev2");
            return "success";

        }

        //UpdatePlanRev2 SP
        [HttpPost]
        [Route("UpdatePlanRev2")]
        public string UpdatePlanRev2(UpdatePlanRev2 UP)
        {


            //GetDatabase GDB = new GetDatabase();
            //GDB.ClientName = ClientData.CN;
            tempconnectionstring();
            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@id", mytype = SqlDbType.NVarChar, Value = UP.id });
            Connection.myparameters.Add(new myParameters { ParameterName = "@ack_lvl", mytype = SqlDbType.NVarChar, Value = UP.ack_lvl });
            Connection.myparameters.Add(new myParameters { ParameterName = "@reviewer2", mytype = SqlDbType.NVarChar, Value = UP.reviewer2 });
            Connection.myparameters.Add(new myParameters { ParameterName = "@date_rev2", mytype = SqlDbType.NVarChar, Value = UP.date_rev2 });
            Connection.myparameters.Add(new myParameters { ParameterName = "@status", mytype = SqlDbType.NVarChar, Value = UP.status });

            Connection.ExecuteNonQuery("UpdatePlanRev2");
            return "success";

        }


        //UpdateDate SP
        [HttpPost]
        [Route("UpdateDate")]
        public string UpdateDate(UpdateDate UD)
        {
            

            //GetDatabase GDB = new GetDatabase();
            //GDB.ClientName = ClientData.CN;
            tempconnectionstring();
            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@id", mytype = SqlDbType.NVarChar, Value = UD.id });
            Connection.myparameters.Add(new myParameters { ParameterName = "@date_start", mytype = SqlDbType.NVarChar, Value = UD.date_start });
            Connection.myparameters.Add(new myParameters { ParameterName = "@date_end", mytype = SqlDbType.NVarChar, Value = UD.date_end });

            Connection.ExecuteNonQuery("UpdateDate");
            return "success";

    }
   
        //UpdateRating SP
        [HttpPost]
        [Route("UpdateRating")]
        public string UpdateRating(UpdateRating UR)
        {
            

            //GetDatabase GDB = new GetDatabase();
            //GDB.ClientName = ClientData.CN;
            tempconnectionstring();
            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@id", mytype = SqlDbType.NVarChar, Value = UR.id });
            Connection.myparameters.Add(new myParameters { ParameterName = "@name", mytype = SqlDbType.NVarChar, Value = UR.name });

            Connection.ExecuteNonQuery("UpdateRating");
            return "success";

        }
        //UpdateResult SP
        [HttpPost]
        [Route("UpdateResult")]
        public string UpdateResult(UpdateResult UR)
        {
            

            //GetDatabase GDB = new GetDatabase();
            //GDB.ClientName = ClientData.CN;
            tempconnectionstring();
            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@id", mytype = SqlDbType.NVarChar, Value = UR.id });
            Connection.myparameters.Add(new myParameters { ParameterName = "@name", mytype = SqlDbType.NVarChar, Value = UR.name });

            Connection.ExecuteNonQuery("UpdateResult");
            return "success";

        }
        //UpdateScore SP
        [HttpPost]
        [Route("UpdateScore")]
        public string UpdateScore(UpdateScore US)
        {
            

            //GetDatabase GDB = new GetDatabase();
            //GDB.ClientName = ClientData.CN;
            tempconnectionstring();
            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@id", mytype = SqlDbType.NVarChar, Value = US.id });
            Connection.myparameters.Add(new myParameters { ParameterName = "@score_eval", mytype = SqlDbType.NVarChar, Value = US.score_eval });
            Connection.myparameters.Add(new myParameters { ParameterName = "@weight", mytype = SqlDbType.NVarChar, Value = US.weight });

            Connection.ExecuteNonQuery("UpdateScore");
            return "success";

        }



        [HttpPost]
        [Route("GetMobileEmpAcces")]
        public getMobileAccess GetMobileEmpAcces(employee emp)
        {
            

            getMobileAccess ListUs = new getMobileAccess();
            DataTable DT = new DataTable();
            Connection con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = emp.empid });
            DT = con.GetDataTable("sp_GetEmpidandAccess");
            string access = "False";
            for (int i = 0; i < DT.Rows.Count; i++)
            {
                if (DT.Rows[i]["parameter1"].ToString() == "Mobile Time In")
                {
                    access = "True";
                }
            }
            ListUs.isAccess = access;

            return ListUs;
            //Get EmpAccess  End
        }

        //REN 1/15/2020
        [HttpPost]
        [Route("UnlockEmployee")]
        public string UnlockEmployee(UnlockEmp UE)
        {
            tempconnectionstring();
            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@group_id", mytype = SqlDbType.Int, Value = UE.group_id });
            Connection.myparameters.Add(new myParameters { ParameterName = "@ack_lvl", mytype = SqlDbType.Int, Value = UE.ack_lvl });


            Connection.ExecuteNonQuery("UnlockEmployee");
            return "success";

        }

        //REN 1/15/2020
        [HttpPost]
        [Route("UnlockRev1")]
        public string UnlockReviewer1(UnlockFunc UF)
        {
            tempconnectionstring();
            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@group_id", mytype = SqlDbType.NVarChar, Value = UF.groupID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@ack_lvl", mytype = SqlDbType.NVarChar, Value = UF.ackLvl });


            Connection.ExecuteNonQuery("UnlockReviewer1");
            return "success";

        }




        //K.Ber InsertViewData SP
        [HttpPost]
        [Route("InsertViewData")]
        public string InsertViewData(InsertViewData VD)
        {


            //GetDatabase GDB = new GetDatabase();
            //GDB.ClientName = ClientData.CN;
            tempconnectionstring();
            Connection Connection = new Connection();
            //@modifiedby varchar(20), @company varchar(20), @viewdate varchar(20)
            Connection.myparameters.Add(new myParameters { ParameterName = "@company", mytype = SqlDbType.NVarChar, Value = VD.company });
            Connection.myparameters.Add(new myParameters { ParameterName = "@viewdate", mytype = SqlDbType.NVarChar, Value = VD.viewdate });
            Connection.myparameters.Add(new myParameters { ParameterName = "@modifiedby", mytype = SqlDbType.NVarChar, Value = VD.modifiedby });

            Connection.ExecuteNonQuery("sp_ins_viewdate");
            return "success";

        }




        //K.Ber SetExemption SP
        [HttpPost]
        [Route("SetExemption")]
        public string SetExemption(SetExemption SE)
        {


            //GetDatabase GDB = new GetDatabase();
            //GDB.ClientName = ClientData.CN;
            tempconnectionstring();
            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@empid", mytype = SqlDbType.NVarChar, Value = SE.empid });
            Connection.myparameters.Add(new myParameters { ParameterName = "@empname", mytype = SqlDbType.NVarChar, Value = SE.empname });
            Connection.myparameters.Add(new myParameters { ParameterName = "@goalsettings_from", mytype = SqlDbType.NVarChar, Value = SE.goalsettings_from });
            Connection.myparameters.Add(new myParameters { ParameterName = "@goalsettings_to", mytype = SqlDbType.NVarChar, Value = SE.goalsettings_to });
            Connection.myparameters.Add(new myParameters { ParameterName = "@midyear_from", mytype = SqlDbType.NVarChar, Value = SE.midyear_from });
            Connection.myparameters.Add(new myParameters { ParameterName = "@midyear_to", mytype = SqlDbType.NVarChar, Value = SE.midyear_to });
            Connection.myparameters.Add(new myParameters { ParameterName = "@yearend_from", mytype = SqlDbType.NVarChar, Value = SE.yearend_from });
            Connection.myparameters.Add(new myParameters { ParameterName = "@yearend_to", mytype = SqlDbType.NVarChar, Value = SE.yearend_to });
            Connection.myparameters.Add(new myParameters { ParameterName = "@company", mytype = SqlDbType.NVarChar, Value = SE.company });
            Connection.myparameters.Add(new myParameters { ParameterName = "@modified_by", mytype = SqlDbType.NVarChar, Value = SE.modified_by });

            Connection.ExecuteNonQuery("SetExemption");
            return "success";

        }

        //K.Ber DeleteExemption SP
        [HttpPost]
        [Route("DeleteExemption")]
        public string DeleteExemption(DeleteExemption DE)
        {


            //GetDatabase GDB = new GetDatabase();
            //GDB.ClientName = ClientData.CN;
            tempconnectionstring();
            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@cID", mytype = SqlDbType.NVarChar, Value = DE.cID });

            Connection.ExecuteNonQuery("DeleteExemption");
            return "success";

        }





        //K.Ber
        //GetExemption return 
        [HttpPost]
        [Route("GetExemption")]
        public GetGetExemption GetExemption(GetExemption GE)
        {


            GetGetExemption GetExemption = new GetGetExemption();
            List<GetExemptionLists> GetExemptionDetails = new List<GetExemptionLists>();
            tempconnectionstring();
            Connection Connection = new Connection();
            Connection.myparameters.Add(new myParameters { ParameterName = "@cID", mytype = SqlDbType.NVarChar, Value = GE.cID  });

            DataTable dtuserList = new DataTable();

            dtuserList = Connection.GetDataTable("GetExemption");
            if (dtuserList.Rows.Count > 0)
            {
                foreach (DataRow row in dtuserList.Rows)
                {
                    GetExemptionLists GetExemptionList = new GetExemptionLists
                    {
                        empid = row["empid"].ToString(),
                        goalsettings_from = row["goalsettings_from"].ToString(),
                        goalsettings_to = row["goalsettings_to"].ToString(),
                        midyear_from = row["midyear_from"].ToString(),
                        midyear_to = row["midyear_to"].ToString(),
                        yearend_from = row["yearend_from"].ToString(),
                        yearend_to = row["yearend_to"].ToString(),
                        company = row["company"].ToString(),
                        modified_by = row["modified_by"].ToString()
                        
                    };
                    GetExemptionDetails.Add(GetExemptionList);
                }
                GetExemption.result = GetExemptionDetails;
                GetExemption.myreturn = "Success";
            }
            else
            {
                GetExemption.myreturn = "Error";
            }
            return GetExemption;
        }



        //K.Ber
        //GetExemption return 
        [HttpPost]
        [Route("GetExemptionById")]
        public GetGetExemptionById GetExemptionById(GetExemptionById GE)
        {


            GetGetExemptionById GetExemptionById = new GetGetExemptionById();
            List<GetExemptionByIdLists> GetExemptionByIdDetails = new List<GetExemptionByIdLists>();
            tempconnectionstring();
            Connection Connection = new Connection();
            Connection.myparameters.Add(new myParameters { ParameterName = "@company", mytype = SqlDbType.NVarChar, Value = GE.company });
            Connection.myparameters.Add(new myParameters { ParameterName = "@empid", mytype = SqlDbType.NVarChar, Value = GE.empid });

            DataTable dtuserList = new DataTable();

            dtuserList = Connection.GetDataTable("GetExemptionById");
            if (dtuserList.Rows.Count > 0)
            {
                foreach (DataRow row in dtuserList.Rows)
                {
                    GetExemptionByIdLists GetExemptionByIdList = new GetExemptionByIdLists
                    {
                        goalsettings_from = row["goalsettings_from"].ToString(),
                        goalsettings_to = row["goalsettings_to"].ToString(),
                        midyear_from = row["midyear_from"].ToString(),
                        midyear_to = row["midyear_to"].ToString(),
                        yearend_from = row["yearend_from"].ToString(),
                        yearend_to = row["yearend_to"].ToString(),
                        modified_by = row["modified_by"].ToString()

                    };
                    GetExemptionByIdDetails.Add(GetExemptionByIdList);
                }
                GetExemptionById.result = GetExemptionByIdDetails;
                GetExemptionById.myreturn = "Success";
            }
            else
            {
                GetExemptionById.myreturn = "Error";
            }
            return GetExemptionById;
        }




        //K.Ber
        //SelectViewDate return 
        [HttpPost]
        [Route("SelectViewDate")]
        public GetSelectViewDate SelectViewDate(SelectViewDate SV)
        {


            GetSelectViewDate SelectViewDate = new GetSelectViewDate();
            List<SelectViewDateLists> SelectViewDateDetails = new List<SelectViewDateLists>();
            tempconnectionstring();
            Connection Connection = new Connection();
            Connection.myparameters.Add(new myParameters { ParameterName = "@company", mytype = SqlDbType.NVarChar, Value = SV.company });

            DataTable dtuserList = new DataTable();

            dtuserList = Connection.GetDataTable("sp_sel_viewdate");
            if (dtuserList.Rows.Count > 0)
            {
                foreach (DataRow row in dtuserList.Rows)
                {
                    SelectViewDateLists SelectViewDateList = new SelectViewDateLists
                    {
                        view_date = row["view_date"].ToString(),

                    };
                    SelectViewDateDetails.Add(SelectViewDateList);
                }
                SelectViewDate.result = SelectViewDateDetails;
                SelectViewDate.myreturn = "Success";
            }
            else
            {
                SelectViewDate.myreturn = "Error";
            }
            return SelectViewDate;
        }




        //REN 1/23/2020
        [HttpPost]
        [Route("ws_AuditTrail")]
        public GetAuditTrail GetAuditTrailPerfApp(GetAuditTrailPerfApp AT)
        {

            GetAuditTrail GetReportsPlanresult = new GetAuditTrail();
            List<GetAuditTrailList> GetAuditTrailDetails = new List<GetAuditTrailList>();
            tempconnectionstring();

            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@cID", mytype = SqlDbType.NVarChar, Value = AT.cID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.NVarChar, Value = AT.empID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@auditType", mytype = SqlDbType.NVarChar, Value = AT.auditType });


            DataTable dtuserList = new DataTable();

            dtuserList = Connection.GetDataTable("sp_AuditTrail");
            if (dtuserList.Rows.Count > 0)
            {
                foreach (DataRow row in dtuserList.Rows)
                {
                    GetAuditTrailList GetReportsPlanList = new GetAuditTrailList
                    {
                        id = row["id"].ToString(),
                        name = row["name"].ToString(),
                        session = row["session"].ToString(),
                        date_req = row["date_req"].ToString(),
                        ack1 = row["ack1"].ToString(),
                        date_ack1 = row["date_ack1"].ToString(),
                        ack2 = row["ack2"].ToString(),
                        date_ack2 = row["date_ack2"].ToString(),
                        date_of_change = row["date_of_change"].ToString(),
                        what_change = row["what_change"].ToString(),
                        change_from = row["change_from"].ToString(),
                        change_to = row["change_to"].ToString(),
                        
                    };
                    GetAuditTrailDetails.Add(GetReportsPlanList);
                }
                GetReportsPlanresult.result = GetAuditTrailDetails;
                GetReportsPlanresult.myreturn = "Success";
            }
            else
            {
                GetReportsPlanresult.myreturn = "Error";
            }
            return GetReportsPlanresult;
        }







        //NEW WS 1/28/2020 START

        [HttpPost]
        [Route("ws_filterExemption")]
        public getFilterExemption FilterExemption(ExemptionParams EP)
        {
            getFilterExemption FilterExemptionresult = new getFilterExemption();
            List<FilterExemptionLists> FilterReportsDetails = new List<FilterExemptionLists>();
            tempconnectionstring();
            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@company", mytype = SqlDbType.NVarChar, Value = EP.company });
            Connection.myparameters.Add(new myParameters { ParameterName = "@empid", mytype = SqlDbType.NVarChar, Value = EP.empid });

            DataTable dtuserList = new DataTable();

            dtuserList = Connection.GetDataTable("sp_filterUserExemption");
            if (dtuserList.Rows.Count > 0)
            {
                foreach (DataRow row in dtuserList.Rows)
                {
                    FilterExemptionLists FilteredExemption = new FilterExemptionLists
                    {

                        empid = row["empid"].ToString(),
                        empname = row["empname"].ToString(),
                        goalsettings_to = row["goalsettings_to"].ToString(),
                        goalsettings_from = row["goalsettings_from"].ToString(),
                        midyear_from = row["midyear_from"].ToString(),
                        midyear_to = row["midyear_to"].ToString(),
                        yearend_from = row["yearend_from"].ToString(),
                        yearend_to = row["yearend_to"].ToString(),
                    };
                    FilterReportsDetails.Add(FilteredExemption);
                }
                FilterExemptionresult.result = FilterReportsDetails;
                FilterExemptionresult.myreturn = "Success";
            }
            else
            {
                FilterExemptionresult.myreturn = "Error";
            }
            return FilterExemptionresult;
        }


        [HttpPost]
        [Route("ws_getUserExemption")]
        public getUserExemption UserExemption(UserExemptionParams EP)
        {
            getUserExemption UserExemptionresult = new getUserExemption();
            List<UserExemptionList> FilterReportsDetails = new List<UserExemptionList>();
            tempconnectionstring();
            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@company", mytype = SqlDbType.NVarChar, Value = EP.company });

            DataTable dtuserList = new DataTable();

            dtuserList = Connection.GetDataTable("sp_getUserExemption");
            if (dtuserList.Rows.Count > 0)
            {
                foreach (DataRow row in dtuserList.Rows)
                {
                    UserExemptionList UserExemption = new UserExemptionList
                    {

                        EmpID = row["EmpID"].ToString(),
                        EmpName = row["EmpName"].ToString(),

                    };
                    FilterReportsDetails.Add(UserExemption);
                }
                UserExemptionresult.result = FilterReportsDetails;
                UserExemptionresult.myreturn = "Success";
            }
            else
            {
                UserExemptionresult.myreturn = "Error";
            }
            return UserExemptionresult;
        }



        //NEW WS 1/28/2020 END

        //1/29/2020// REN

        [HttpPost]
        [Route("ws_AuditTrailSearch")]
        public List<SearchAuditList> ATSearchPanel(AuditParams param)
        {
            DataTable EmpID = new DataTable();
            DataTable Session = new DataTable();
            EmpID.Columns.AddRange(new DataColumn[1] { new DataColumn("empid") });

            Session.Columns.AddRange(new DataColumn[1] { new DataColumn("session") });

            for (int i = 0; i < param.empid.Length; i++)
            {
                EmpID.Rows.Add(param.empid[i]);
            }
            for (int j = 0; j < param.session.Length; j++)
            {
                Session.Rows.Add(param.session[j]);
            }


            List<SearchAuditList> SPR = new List<SearchAuditList>();
            tempconnectionstring();
            Connection con = new Connection();

            con.myparameters.Add(new myParameters { ParameterName = "@user", mytype = SqlDbType.VarChar, Value = param.user });
            con.myparameters.Add(new myParameters { ParameterName = "@cID", mytype = SqlDbType.VarChar, Value = param.cID });
            con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.Structured, Value = EmpID });
            con.myparameters.Add(new myParameters { ParameterName = "@session", mytype = SqlDbType.Structured, Value = Session });
            con.myparameters.Add(new myParameters { ParameterName = "@auditType", mytype = SqlDbType.VarChar, Value = param.auditType });
            con.myparameters.Add(new myParameters { ParameterName = "@dateFrom", mytype = SqlDbType.VarChar, Value = param.dateFrom });
            con.myparameters.Add(new myParameters { ParameterName = "@dateTo", mytype = SqlDbType.VarChar, Value = param.dateTo });


            DataTable DT = con.GetDataTable("sp_AuditTrailSearch");
            foreach (DataRow row in DT.Rows)
            {
                SearchAuditList Users = new SearchAuditList()
                {

                    id = row["id"].ToString(),
                    name = row["name"].ToString(),
                    session = row["session"].ToString(),
                    date_req = row["date_req"].ToString(),
                    ack1 = row["ack1"].ToString(),
                    date_ack1 = row["date_ack1"].ToString(),
                    ack2 = row["ack2"].ToString(),
                    date_ack2 = row["date_ack2"].ToString(),
                    date_of_change = row["date_of_change"].ToString(),
                    what_change = row["what_change"].ToString(),
                    change_from = row["change_from"].ToString(),
                    change_to = row["change_to"].ToString(),

                };
                SPR.Add(Users);
            }
            return SPR;
        }

        //1/29/2020



        //2/7/2020
        //Exemption Notification 
        [HttpPost]
        [Route("ws_ExemptionReviewer1")]
        public ClassExemRev1Count CountExemRev1(CountParams param)
        {

            ClassExemRev1Count getExemp1 = new ClassExemRev1Count();
            List<ExempRev1> exempNotif1 = new List<ExempRev1>();
            tempconnectionstring();
            Connection Connection = new Connection();
            Connection.myparameters.Add(new myParameters { ParameterName = "@cID", mytype = SqlDbType.NVarChar, Value = param.cID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.NVarChar, Value = param.empID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@datetoday", mytype = SqlDbType.NVarChar, Value = param.datetoday });


            DataTable dtuserList = new DataTable();
            dtuserList = Connection.GetDataTable("ExemptionReviewer1");
            if (dtuserList.Rows.Count > 0)
            {
                foreach (DataRow row in dtuserList.Rows)
                {
                    ExempRev1 Exemp1 = new ExempRev1
                    {
                        ERev1 = row["ERev1"].ToString()

                    };
                    exempNotif1.Add(Exemp1);
                }
                getExemp1.returnERev1 = exempNotif1;
                getExemp1.myreturn = "Success";
            }
            else
            {
                getExemp1.myreturn = "Error";
            }
            return getExemp1;
        }

        //ADDED BY RHEN
        [HttpPost]
        [Route("ws_ExemptionReviewer2")]
        public ClassExemRev2Count CountExemRev2(CountParams param)
        {

            ClassExemRev2Count getExemp2 = new ClassExemRev2Count();
            List<ExempRev2> exempNotif2 = new List<ExempRev2>();
            tempconnectionstring();
            Connection Connection = new Connection();
            Connection.myparameters.Add(new myParameters { ParameterName = "@cID", mytype = SqlDbType.NVarChar, Value = param.cID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.NVarChar, Value = param.empID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@datetoday", mytype = SqlDbType.NVarChar, Value = param.datetoday });


            DataTable dtuserList = new DataTable();
            dtuserList = Connection.GetDataTable("ExemptionReviewer2");
            if (dtuserList.Rows.Count > 0)
            {
                foreach (DataRow row in dtuserList.Rows)
                {
                    ExempRev2 Exemp2 = new ExempRev2
                    {
                        ERev2 = row["ERev2"].ToString()

                    };
                    exempNotif2.Add(Exemp2);
                }
                getExemp2.returnERev2 = exempNotif2;
                getExemp2.myreturn = "Success";
            }
            else
            {
                getExemp2.myreturn = "Error";
            }
            return getExemp2;
        }

        //2/7/2020
        //Exemption Notification END

        [HttpPost]
        [Route("ws_PerfViewAuditTrail")]
        public string perfviewAuditTrail(insertPerfView pvAT)
        {
            //GetDatabase GDB = new GetDatabase();
            //GDB.ClientName = ClientData.CN;
            tempconnectionstring();
            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@groupid", mytype = SqlDbType.NVarChar, Value = pvAT.groupid });
            Connection.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.NVarChar, Value = pvAT.empID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@session", mytype = SqlDbType.NVarChar, Value = pvAT.session });
            Connection.myparameters.Add(new myParameters { ParameterName = "@what_change", mytype = SqlDbType.NVarChar, Value = pvAT.what_change });
            Connection.myparameters.Add(new myParameters { ParameterName = "@change_from", mytype = SqlDbType.NVarChar, Value = pvAT.change_from });
            Connection.myparameters.Add(new myParameters { ParameterName = "@change_to", mytype = SqlDbType.NVarChar, Value = pvAT.change_to });
            Connection.myparameters.Add(new myParameters { ParameterName = "@cID", mytype = SqlDbType.NVarChar, Value = pvAT.cID });

            Connection.ExecuteNonQuery("PerfViewAuditTrail");
            return "success";


        }



    }
}