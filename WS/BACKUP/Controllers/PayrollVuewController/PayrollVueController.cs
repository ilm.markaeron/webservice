﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using illimitadoWepAPI.MyClass;
using illimitadoWepAPI.Models.ScheduleDemoModels;
using illimitadoWepAPI.Controllers.SchedulerDemo;
using illimitadoWepAPI.Models.PayrollVueModels;
using ExpertXls.ExcelLib;
using System.IO;
using System.Web.Hosting;
using System.Web;
using System.Net.Mail;
using System.Net;
using System.Data;
using System.Configuration;
using System.Reflection;
using illimitadoWepAPI.Models;
using System.Text;
//using System.Web.Http.Cors;

namespace illimitadoWepAPI.Controllers.PayrollVuewController
{
    //[EnableCors(origins: "8080", headers: "*", methods: "*")]
    public class PayrollVueController : ApiController
    {
        public string GetDBILMMain()
        {
            // com.connectionstring = ConfigurationManager.ConnectionStrings["connectionstring"].ConnectionString.ToString();
            var settings = ConfigurationManager.ConnectionStrings["connectionstring"];
            var fi = typeof(ConfigurationElement).GetField(
                          "_bReadOnly",
                          BindingFlags.Instance | BindingFlags.NonPublic);
            fi.SetValue(settings, false);
            // settings.ConnectionString = "Data Source=tcp:ilmuat.c3srcolvclfy.ap-southeast-1.rds.amazonaws.com,1400;Database=ILM_Main;User Id=devuatuser;Password=ILMV3ry53cur3dMGTpa55w0rd2020;";
            settings.ConnectionString = "Data Source=livedb-ilm.c3srcolvclfy.ap-southeast-1.rds.amazonaws.com,1412;Database=ILM_Main;User Id=ILM_LiveDBUser;Password=ILMV3ry53cur3dMGTpa55w0rd2020;";

            return settings.ToString();
        }

        public string GetDBILMDevSvr(PayrollVueClients clients)
        {
            // com.connectionstring = ConfigurationManager.ConnectionStrings["connectionstring"].ConnectionString.ToString();
            var settings = ConfigurationManager.ConnectionStrings["connectionstring"];
            var fi = typeof(ConfigurationElement).GetField(
                          "_bReadOnly",
                          BindingFlags.Instance | BindingFlags.NonPublic);
            fi.SetValue(settings, false);
            // settings.ConnectionString = "Data Source=tcp:ilmuat.c3srcolvclfy.ap-southeast-1.rds.amazonaws.com,1400;Database=ILM_Main;User Id=devuatuser;Password=ILMV3ry53cur3dMGTpa55w0rd2020;";
            string client = clients.clientname;
            settings.ConnectionString = "Data Source=livedb-ilm.c3srcolvclfy.ap-southeast-1.rds.amazonaws.com,1412;Database=" + client + ";User Id=ILM_LiveDBUser;Password=ILMV3ry53cur3dMGTpa55w0rd2020;";

            return settings.ToString();
        }


        [HttpPost]
        [Route("GetClientList")]
        public List<getClientList> GetClientList(getClientList clientList)
        {
            //GetDatabase GDB = new GetDatabase();
            //GDB.ClientName = "ILM";
            //SelfieRegistration2Controller.GetDB2(GDB);

            GetDBILMMain();

            Connection con = new Connection();

            DataTable DT = new DataTable();
            DT = con.GetDataTable("sp_GetClientList");
            string x = ConfigurationManager.ConnectionStrings["connectionstring"].ToString();
            List<getClientList> ListReturned = new List<getClientList>();

            for (int i = 0; i < DT.Rows.Count; i++)
            {

                getClientList Get = new getClientList();
                Get.ClientName = DT.Rows[i]["ClientName"].ToString();
                Get.DB_Settings = DT.Rows[i]["DB_Settings"].ToString();

                ListReturned.Add(Get);
            }
            return ListReturned;

        }
        //public void tempconnectionstring()
        //{
        //    var settings = ConfigurationManager.ConnectionStrings["connectionstring"];
        //    var fi = typeof(ConfigurationElement).GetField(
        //                  "_bReadOnly",
        //                  BindingFlags.Instance | BindingFlags.NonPublic);
        //    fi.SetValue(settings, false);
        //    //--Local--//
        //    //settings.ConnectionString = "Data Source=192.168.2.33,1444;Database=CRT_Dbase2019-03-29;User Id=DevUser;Password=securedmgt@1230;";
        //    //--AWS--//
        //    settings.ConnectionString = "Data Source=livedb-ilm.c3srcolvclfy.ap-southeast-1.rds.amazonaws.com,1412; Database = CRT_Dbase;User Id=ILM_LiveDBUser;Password=ILMV3ry53cur3dMGTpa55w0rd2020;";

        //}
        //[HttpPost]
        //[Route("CRT_EmailValidationForImportv2")]
        //public List<DuplicateUsers> DuplicateUsers(UserBulkUpload UBP)
        //{
        //    DataTable UserList = new DataTable();
        //    UserList.Columns.AddRange(new DataColumn[10] { new DataColumn("EmpID"), new DataColumn("FName"), new DataColumn("MName"), new DataColumn("LName"), new DataColumn("AccountType"), new DataColumn("TIN"), new DataColumn("Birthdate"), new DataColumn("Suffix"), new DataColumn("Site"), new DataColumn("RowNum") });
        //    for (int i = 0; i < UBP.employeeID.Length; i++)
        //    {
        //        UserList.Rows.Add(UBP.rowNo[i], UBP.employeeID[i], UBP.fName[i], UBP.mName[i], UBP.lName[i], UBP.Suffix[i], UBP.birthDate[i], UBP.site[i], UBP.tin[i], UBP.accountType[i]);
        //    }
        //    List<DuplicateUsers> DU = new List<DuplicateUsers>();
        //    tempconnectionstring();
        //    Connection con = new Connection();
        //    con.myparameters.Add(new myParameters { ParameterName = "@CompanyName", mytype = SqlDbType.VarChar, Value = UBP.CN });
        //    con.myparameters.Add(new myParameters { ParameterName = "@IMPORT", mytype = SqlDbType.Structured, Value = UserList });
        //    DataTable DT = con.GetDataTable("");
        //    foreach (DataRow row in DT.Rows)
        //    {
        //        DuplicateUsers Users = new DuplicateUsers()
        //        {
        //            RowID = row["RowID"].ToString(),
        //            EmployeeID = row["StartingID"].ToString(),
        //            Conflict = row["Conflict"].ToString()
        //        };
        //        DU.Add(Users);
        //    }
        //    return DU;
        //}

        [HttpPost]
        [Route("PV_DeletePayslip")]
        public string deletePayslip(deleteUpload param)
        {
            try
            {
                PayrollVueClients pvc = new PayrollVueClients();
                pvc.clientname = param.CN;
                GetDBILMDevSvr(pvc);

                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@PayOutDate", mytype = SqlDbType.VarChar, Value = param.PayOutDate });
                con.ExecuteNonQuery("sp_DeleteUploadedFileForPayslip");
                return "Success";
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }

        [HttpPost]
        [Route("PV_UploadLeave")]
        public string uploadLeave(insertLeave param)
        {
            try
            {
                PayrollVueClients pvc = new PayrollVueClients();
                pvc.clientname = param.CN;
                GetDBILMDevSvr(pvc);

                DataTable importList = new DataTable();
                importList.Columns.AddRange(new DataColumn[14]
                {
                  new DataColumn("EmpID"),
                  new DataColumn("LeaveDate"),
                  new DataColumn("isHalfDay"),
                  new DataColumn("LeaveType"),
                  new DataColumn("isPaid"),
                  new DataColumn("ApprovedBy"),
                  new DataColumn("DateApproved"),
                  new DataColumn("DateApplied"),
                  new DataColumn("LeaveStatus"),
                  new DataColumn("EmpReason"),
                  new DataColumn("EmpComment"),
                  new DataColumn("ApproverComment"),
                  new DataColumn("LeaveHours"),
                  new DataColumn("LeaveMinutes")
                });
                for (int i = 0; i < param.EmpID.Length; i++)
                {
                    importList.Rows.Add(param.EmpID[i],
                    param.LeaveDate[i],
                    param.isHalfDay[i],
                    param.LeaveType[i],
                    param.isPaid[i],
                    param.ApprovedBy[i],
                    param.DateApproved[i],
                    param.DateApplied[i],
                    param.LeaveStatus[i],
                    param.EmpReason[i],
                    param.EmpComment[i],
                    param.ApproverComment[i],
                    param.LeaveHours[i],
                    param.LeaveMinutes[i]);
                }
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@Data", mytype = SqlDbType.Structured, Value = importList });
                con.ExecuteNonQuery("sp_PV_InsertLeave");
                return "Success";
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }


        [HttpPost]
        [Route("PV_Validations")]
        public List<Validation_List> Validation(getValidationList param)
        {
            List<Validation_List> List = new List<Validation_List>();
            PayrollVueClients pvc = new PayrollVueClients();
            pvc.clientname = param.CN;
            GetDBILMDevSvr(pvc);
            Connection con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@option", mytype = SqlDbType.VarChar, Value = param.option });
            DataTable DT = con.GetDataTable("sp_PV_ListValidation");
            foreach (DataRow row in DT.Rows)
            {
                Validation_List E = new Validation_List()
                {
                    validation = row[0].ToString(),
                };
                List.Add(E);
            }
            return List;
        }


        [HttpPost]
        [Route("UploadDeductionsCategory")]
        public string DeductionsUploadCategory(DeductionsUploadCategory UBP)
        {

            try
            {
                PayrollVueClients pvc = new PayrollVueClients();
                pvc.clientname = UBP.CN;
                GetDBILMDevSvr(pvc);

                Connection con = new Connection();
                DataTable DeductionCategoryList = new DataTable();
                DeductionCategoryList.Columns.AddRange(new DataColumn[6] { new DataColumn("DeductionsCatCode"), new DataColumn("DeductionsCategory"), new DataColumn("CreatedOn"), new DataColumn("CreatedBy"), new DataColumn("ModifiedOn"), new DataColumn("ModifiedBy") });

                for (int i = 0; i < UBP.DeductionsCatCode.Length; i++)
                {
                    DeductionCategoryList.Rows.Add(UBP.DeductionsCatCode[i], UBP.DeductionsCategory[i], UBP.CreatedOn[i], UBP.CreatedBy[i], UBP.ModifiedOn[i], UBP.ModifiedBy[i]);
                }
                //List<DeductionsUploadCategory> DU = new List<DeductionsUploadCategory>();


                con.myparameters.Add(new myParameters { ParameterName = "@CompanyName", mytype = SqlDbType.VarChar, Value = UBP.CN });
                con.myparameters.Add(new myParameters { ParameterName = "@IMPORT", mytype = SqlDbType.Structured, Value = DeductionCategoryList });
                //DataTable DT = con.GetDataTable("sp_UploadDeductions");
                con.ExecuteScalar("sp_UploadDeductions");
                //foreach (DataRow row in DT.Rows)
                //{
                //    DuplicateUsers Users = new DuplicateUsers()
                //    {
                //        RowID = row["RowID"].ToString(),
                //        EmployeeID = row["StartingID"].ToString(),
                //        Conflict = row["Conflict"].ToString()
                //    };
                //    DU.Add(Users);
                //}
                return "Success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }

        [HttpPost]
        [Route("UploadDeductionsType")]
        public string DeductionsUploadType(DeductionsUploadType UBP)
        {
            try
            {
                PayrollVueClients pvc = new PayrollVueClients();
                pvc.clientname = UBP.CN;
                GetDBILMDevSvr(pvc);

                Connection con = new Connection();
                DataTable DeductionTypeList = new DataTable();
                DeductionTypeList.Columns.AddRange(new DataColumn[10] { new DataColumn("DeductionCode"), new DataColumn("DeductionDescription"), new DataColumn("DeductionCategoryID"), new DataColumn("IsTaxable"), new DataColumn("IsBonus"), new DataColumn("CreatedOn"), new DataColumn("CreatedBy"), new DataColumn("ModifiedOn"), new DataColumn("ModifiedBy"), new DataColumn("Active") });
                for (int i = 0; i < UBP.DeductionCode.Length; i++)
                {
                    DeductionTypeList.Rows.Add(
                        UBP.DeductionCode[i],
                        UBP.DeductionDescription[i],
                        UBP.DeductionCategoryID[i],
                        UBP.IsTaxable[i],
                        UBP.IsBonus[i],
                        UBP.CreatedOn[i],
                        UBP.CreatedBy[i],
                        UBP.ModifiedOn[i],
                        UBP.ModifiedBy[i],
                        UBP.Active[i]);
                }
                //List<DeductionsUploadType> DU = new List<DeductionsUploadType>();

                con.myparameters.Add(new myParameters { ParameterName = "@CompanyName", mytype = SqlDbType.VarChar, Value = UBP.CN });
                con.myparameters.Add(new myParameters { ParameterName = "@IMPORT", mytype = SqlDbType.Structured, Value = DeductionTypeList });
                con.ExecuteScalar("sp_UploadDeductionsType");
                //foreach (DataRow row in DT.Rows)
                //{
                //    DuplicateUsers Users = new DuplicateUsers()
                //    {
                //        RowID = row["RowID"].ToString(),
                //        EmployeeID = row["StartingID"].ToString(),
                //        Conflict = row["Conflict"].ToString()
                //    };
                //    DU.Add(Users);
                //}
                return "success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }

        [HttpPost]
        [Route("DeductionEmployee")]
        public string DeductionEmployee(DeductionEmployee UBP)
        {

            try
            {
                PayrollVueClients pvc = new PayrollVueClients();
                pvc.clientname = UBP.CN;
                GetDBILMDevSvr(pvc);

                Connection con = new Connection();
                DataTable DeductionEmployee = new DataTable();
                DeductionEmployee.Columns.AddRange(new DataColumn[25] {
                new DataColumn("EmpID"),
                new DataColumn("DeductionTypeID"),
                new DataColumn("OneTime"),
                new DataColumn("DeductionAmount"),

                new DataColumn("StartDate"),
                new DataColumn("EndDate"),
                new DataColumn("NoEndDate"),
                new DataColumn("PaymentTermsID"),
                new DataColumn("IsPause"),

                new DataColumn("IsResume"),
                new DataColumn("IsStop"),
                new DataColumn("IsDone"),
                new DataColumn("UpdateAmount"),
                new DataColumn("UpdateAmountOn"),

                new DataColumn("UpdateAmountBy"),
                new DataColumn("PausedOn"),
                new DataColumn("PausedBy"),
                new DataColumn("ResumedOn"),
                new DataColumn("ResumedBy"),

                new DataColumn("StoppedOn"),
                new DataColumn("StoppedBy"),
                new DataColumn("CreatedOn"),
                new DataColumn("CreatedBy"),
                new DataColumn("ModifiedOn"),

                new DataColumn("ModifiedBy")
                });

                for (int i = 0; i < UBP.DeductionTypeID.Length; i++)
                {
                    DeductionEmployee.Rows.Add(
                        UBP.EmpID[i],
                        UBP.DeductionTypeID[i],
                        UBP.OneTime[i],
                        UBP.DeductionAmount[i],

                        UBP.StartDate[i],
                        UBP.EndDate[i],
                        UBP.NoEndDate[i],
                        UBP.PaymentTermsID[i],
                        UBP.IsPause[i],

                        UBP.IsResume[i],
                        UBP.IsStop[i],
                        UBP.IsDone[i],
                        UBP.UpdateAmount[i],
                        UBP.UpdateAmountOn[i],

                        UBP.UpdateAmountBy[i],
                        UBP.PausedOn[i],
                        UBP.PausedBy[i],
                        UBP.ResumedOn[i],
                        UBP.ResumedBy[i],

                        UBP.StoppedOn[i],
                        UBP.StoppedBy[i],
                        UBP.CreatedOn[i],
                        UBP.CreatedBy[i],
                        UBP.ModifiedOn[i],

                        UBP.ModifiedBy[i]
                        );

                }

                con.myparameters.Add(new myParameters { ParameterName = "@CompanyName", mytype = SqlDbType.VarChar, Value = UBP.CN });
                con.myparameters.Add(new myParameters { ParameterName = "@IMPORT", mytype = SqlDbType.Structured, Value = DeductionEmployee });
                con.ExecuteNonQuery("sp_UploadDeductionsEmployee");

                return "Success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }
        /// dito yung last
        [HttpPost]
        [Route("DeductionEmployeeDetails")]
        public string DeductionEmployeeDetails(DeductionEmployeeDetails UBP)
        {
            try
            {
                PayrollVueClients pvc = new PayrollVueClients();
                pvc.clientname = UBP.CN;
                GetDBILMDevSvr(pvc);

                Connection con = new Connection();
                DataTable DeductionEmployeeDetails = new DataTable();
                DeductionEmployeeDetails.Columns.AddRange(new DataColumn[8] {
                new DataColumn("EmployeeDeductionDetailsID") ,
                new DataColumn("EmployeeDeductionID") ,
                new DataColumn("DeductionAmount"),
                new DataColumn("PayOutDate") ,
                new DataColumn("ProcessedOn") ,
                new DataColumn("ProcessedBy") ,
                new DataColumn("ClosedOn") ,
                new DataColumn("ClosedBy")});

                for (int i = 0; i < UBP.EmployeeDeductionID.Length; i++)
                {
                    DeductionEmployeeDetails.Rows.Add(
                        UBP.EmployeeDeductionDetailsID[i],
                        UBP.EmployeeDeductionID[i],
                        UBP.DeductionAmount[i],
                        UBP.PayOutDate[i],
                        UBP.ProcessedOn[i],
                        UBP.ProcessedBy[i],
                        UBP.ClosedOn[i],
                        UBP.ClosedBy[i]);
                }

                con.myparameters.Add(new myParameters { ParameterName = "@CompanyName", mytype = SqlDbType.VarChar, Value = UBP.CN });
                con.myparameters.Add(new myParameters { ParameterName = "@IMPORT", mytype = SqlDbType.Structured, Value = DeductionEmployeeDetails });
                con.ExecuteNonQuery("sp_UploadEmployeeDeductionDetails");

                return "Success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }
        [HttpPost]
        [Route("EarningsCategory")]
        public string EarningsCategoryList(EarningsCategoryList UBP)
        {
            try
            {
                PayrollVueClients pvc = new PayrollVueClients();
                pvc.clientname = UBP.CN;
                GetDBILMDevSvr(pvc);

                Connection con = new Connection();
                DataTable EarningsCategoryList = new DataTable();
                EarningsCategoryList.Columns.AddRange(new DataColumn[6] {


                new DataColumn("EarningsCatCode") ,
                new DataColumn("EarningsCategory") ,
                new DataColumn("CreatedOn") ,
                new DataColumn("CreatedBy") ,
                new DataColumn("ModifiedOn"),
                new DataColumn("ModifiedBy") });

                for (int i = 0; i < UBP.EarningsCatCode.Length; i++)
                {
                    EarningsCategoryList.Rows.Add(

                        UBP.EarningsCatCode[i],
                        UBP.EarningsCategory[i],
                        UBP.CreatedOn[i],
                        UBP.CreatedBy[i],
                        UBP.ModifiedOn[i],
                        UBP.ModifiedBy[i]);

                }

                con.myparameters.Add(new myParameters { ParameterName = "@CompanyName", mytype = SqlDbType.VarChar, Value = UBP.CN });
                con.myparameters.Add(new myParameters { ParameterName = "@IMPORT", mytype = SqlDbType.Structured, Value = EarningsCategoryList });
                con.ExecuteNonQuery("sp_UploadEarningCategory");

                return "Success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }

        [HttpPost]
        [Route("EarningsType")]
        public string EarningsTypeList(EarningsTypeList UBP)
        {
            try
            {
                PayrollVueClients pvc = new PayrollVueClients();
                pvc.clientname = UBP.CN;
                GetDBILMDevSvr(pvc);

                Connection con = new Connection();
                DataTable EarningsTypeList = new DataTable();
                EarningsTypeList.Columns.AddRange(new DataColumn[10] {

                new DataColumn("EarningsCode") ,
                new DataColumn("EarningsDescription") ,
                new DataColumn("EarningsCategoryID") ,
                new DataColumn("IsTaxable") ,
                new DataColumn("IsBonus"),
                new DataColumn("AddTo"),
                new DataColumn("CreatedOn") ,
                new DataColumn("CreatedBy"),
                new DataColumn("ModifiedOn"),
                new DataColumn("ModifiedBy")});

                for (int i = 0; i < UBP.EarningsCode.Length; i++)
                {
                    EarningsTypeList.Rows.Add(

                        UBP.EarningsCode[i],
                        UBP.EarningsDescription[i],
                        UBP.EarningsCategoryID[i],
                        UBP.IsTaxable[i],
                        UBP.IsBonus[i],
                        UBP.AddTo[i],
                        UBP.CreatedOn[i],
                        UBP.CreatedBy[i],
                        UBP.ModifiedOn[i],
                        UBP.ModifiedBy[i]);

                }

                con.myparameters.Add(new myParameters { ParameterName = "@CompanyName", mytype = SqlDbType.VarChar, Value = UBP.CN });
                con.myparameters.Add(new myParameters { ParameterName = "@IMPORT", mytype = SqlDbType.Structured, Value = EarningsTypeList });
                con.ExecuteNonQuery("sp_UploadEarningType");

                return "Success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }

        [HttpPost]
        [Route("EmployeeEarnings")]
        public string EmployeeEarningsList(EmployeeEarningsList UBP)
        {
            try
            {
                PayrollVueClients pvc = new PayrollVueClients();
                pvc.clientname = UBP.CN;
                GetDBILMDevSvr(pvc);

                Connection con = new Connection();
                DataTable EmployeeEarningsList = new DataTable();
                EmployeeEarningsList.Columns.AddRange(new DataColumn[25] {

                new DataColumn("EmpID") ,
                new DataColumn("EarningsTypeID") ,
                new DataColumn("OneTime") ,
                new DataColumn("EarningsAmount") ,
                new DataColumn("StartDate"),
                new DataColumn("EndDate"),
                new DataColumn("NoEndDate") ,
                new DataColumn("PaymentTermsID"),
                new DataColumn("IsPause"),
                new DataColumn("IsResume") ,

                new DataColumn("IsStop") ,
                new DataColumn("IsDone") ,
                new DataColumn("PausedOn") ,
                new DataColumn("PausedBy") ,
                new DataColumn("ResumedOn"),
                new DataColumn("ResumedBy"),
                new DataColumn("StoppedOn") ,
                new DataColumn("StoppedBy"),
                new DataColumn("UpdateAmount"),
                new DataColumn("UpdateAmountOn") ,

                new DataColumn("UpdateAmountBy") ,
                new DataColumn("CreatedOn") ,
                new DataColumn("CreatedBy") ,
                new DataColumn("ModifiedOn") ,
                new DataColumn("ModifiedBy")});

                for (int i = 0; i < UBP.EmpID.Length; i++)
                {
                    EmployeeEarningsList.Rows.Add(

                        UBP.EmpID[i],
                        UBP.EarningsTypeID[i],
                        UBP.OneTime[i],
                        UBP.EarningsAmount[i],
                        UBP.StartDate[i],
                        UBP.EndDate[i],
                        UBP.NoEndDate[i],
                        UBP.PaymentTermsID[i],
                        UBP.IsPause[i],
                        UBP.IsResume[i],
                        UBP.IsStop[i],
                        UBP.IsDone[i],
                        UBP.PausedOn[i],
                        UBP.PausedBy[i],
                        UBP.ResumedOn[i],
                        UBP.ResumedBy[i],
                        UBP.StoppedOn[i],
                        UBP.StoppedBy[i],
                        UBP.UpdateAmount[i],
                        UBP.UpdateAmountOn[i],
                        UBP.UpdateAmountBy[i],
                        UBP.CreatedOn[i],
                        UBP.CreatedBy[i],
                        UBP.ModifiedOn[i],
                        UBP.ModifiedBy[i]);

                }

                con.myparameters.Add(new myParameters { ParameterName = "@CompanyName", mytype = SqlDbType.VarChar, Value = UBP.CN });
                con.myparameters.Add(new myParameters { ParameterName = "@IMPORT", mytype = SqlDbType.Structured, Value = EmployeeEarningsList });
                con.ExecuteNonQuery("sp_UploadEmployeeEarnings");

                return "Success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }

        
        [HttpPost]
        [Route("EmployeeEarningsDetails")]
        public string EmployeeEarningsDetailsList(EmployeeEarningsDetailsList UBP)
        {
            try
            {
                PayrollVueClients pvc = new PayrollVueClients();
                pvc.clientname = UBP.CN;
                GetDBILMDevSvr(pvc);

                Connection con = new Connection();
                DataTable EmployeeEarningsDetailsList = new DataTable();
                EmployeeEarningsDetailsList.Columns.AddRange(new DataColumn[8] {
                new DataColumn("EmployeeEarningsDetailsID"),
                new DataColumn("EmployeeEarningsID") ,
                new DataColumn("EarningsAmount") ,
                new DataColumn("PayOutDate") ,
                new DataColumn("ProcessedOn") ,
                new DataColumn("ProcessedBy"),
                new DataColumn("ClosedOn"),
                new DataColumn("ClosedBy")});

                for (int i = 0; i < UBP.EmployeeEarningsID.Length; i++)
                {
                    EmployeeEarningsDetailsList.Rows.Add(
                        UBP.EmployeeEarningsDetailsID[i],
                        UBP.EmployeeEarningsID[i],
                        UBP.EarningsAmount[i],
                        UBP.PayOutDate[i],
                        UBP.ProcessedOn[i],
                        UBP.ProcessedBy[i],
                        UBP.ClosedOn[i],
                        UBP.ClosedBy[i]);

                }

                con.myparameters.Add(new myParameters { ParameterName = "@CompanyName", mytype = SqlDbType.VarChar, Value = UBP.CN });
                con.myparameters.Add(new myParameters { ParameterName = "@IMPORT", mytype = SqlDbType.Structured, Value = EmployeeEarningsDetailsList });
                con.ExecuteNonQuery("sp_UploadEmployeeEarningDetails");

                return "Success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }

        [HttpPost]
        [Route("LoanLookUp")]
        public string LoanList(LoanList UBP)
        {
            try
            {
                PayrollVueClients pvc = new PayrollVueClients();
                pvc.clientname = UBP.CN;
                GetDBILMDevSvr(pvc);

                Connection con = new Connection();
                DataTable LoanList = new DataTable();
                LoanList.Columns.AddRange(new DataColumn[2] {

                new DataColumn("Loan_Amount") ,
                new DataColumn("Loan_Term") });

                for (int i = 0; i < UBP.Loan_Term.Length; i++)
                {
                    LoanList.Rows.Add(
                        UBP.Loan_Amount[i],
                        UBP.Loan_Term[i]);

                }

                con.myparameters.Add(new myParameters { ParameterName = "@CompanyName", mytype = SqlDbType.VarChar, Value = UBP.CN });
                con.myparameters.Add(new myParameters { ParameterName = "@IMPORT", mytype = SqlDbType.Structured, Value = LoanList });
                con.ExecuteNonQuery("sp_UploadLoans");

                return "Success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }

        [HttpPost]
        [Route("LoanNames")]
        public string LoanNameList(LoanNameList UBP)
        {
            try
            {
                PayrollVueClients pvc = new PayrollVueClients();
                pvc.clientname = UBP.CN;
                GetDBILMDevSvr(pvc);

                Connection con = new Connection();
                DataTable LoanNameList = new DataTable();
                LoanNameList.Columns.AddRange(new DataColumn[8] {

                new DataColumn("LoanNameCode") ,
                new DataColumn("LoanName") ,
                new DataColumn("LoanTypeID") ,
                new DataColumn("CreatedOn") ,
                new DataColumn("CreatedBy") ,
                new DataColumn("ModifiedOn") ,
                new DataColumn("ModifiedBy") ,
                new DataColumn("Active") });

                for (int i = 0; i < UBP.LoanNameCode.Length; i++)
                {
                    LoanNameList.Rows.Add(
                        UBP.LoanNameCode[i],
                        UBP.LoanName[i],
                        UBP.LoanTypeID[i],
                        UBP.CreatedOn[i],
                        UBP.CreatedBy[i],
                        UBP.ModifiedOn[i],
                        UBP.ModifiedBy[i],
                        UBP.Active[i]);

                }

                con.myparameters.Add(new myParameters { ParameterName = "@CompanyName", mytype = SqlDbType.VarChar, Value = UBP.CN });
                con.myparameters.Add(new myParameters { ParameterName = "@IMPORT", mytype = SqlDbType.Structured, Value = LoanNameList });
                con.ExecuteNonQuery("sp_UploadLoanNames");

                return "Success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }

        [HttpPost]
        [Route("LoanTypes")]
        public string LoanTypeList(LoanTypeList UBP)
        {
            try
            {
                PayrollVueClients pvc = new PayrollVueClients();
                pvc.clientname = UBP.CN;
                GetDBILMDevSvr(pvc);

                Connection con = new Connection();
                DataTable LoanTypeList = new DataTable();
                LoanTypeList.Columns.AddRange(new DataColumn[6] {

                new DataColumn("LoanTypeCode"),
                new DataColumn("LoanTypeName"),
                new DataColumn("CreatedOn"),
                new DataColumn("CreatedBy"),
                new DataColumn("ModifiedOn"),
                new DataColumn("ModifiedBy")});

                for (int i = 0; i < UBP.LoanTypeCode.Length; i++)
                {
                    LoanTypeList.Rows.Add(
                        UBP.LoanTypeCode[i],
                        UBP.LoanTypeName[i],
                        UBP.CreatedOn[i],
                        UBP.CreatedBy[i],
                        UBP.ModifiedOn[i],
                        UBP.ModifiedBy[i]);

                }

                con.myparameters.Add(new myParameters { ParameterName = "@CompanyName", mytype = SqlDbType.VarChar, Value = UBP.CN });
                con.myparameters.Add(new myParameters { ParameterName = "@IMPORT", mytype = SqlDbType.Structured, Value = LoanTypeList });
                con.ExecuteNonQuery("sp_UploadLoanTypes");

                return "Success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }

        [HttpPost]
        [Route("EmployeeLoans")]
        public string EmployeeLoanList(EmployeeLoanList UBP)
        {
            try
            {
                PayrollVueClients pvc = new PayrollVueClients();
                pvc.clientname = UBP.CN;
                GetDBILMDevSvr(pvc);

                Connection con = new Connection();
                DataTable EmployeeLoanList = new DataTable();
                EmployeeLoanList.Columns.AddRange(new DataColumn[27] {

                new DataColumn("EmpID"),
                new DataColumn("LoanTypeID"),
                new DataColumn("LoanNameID"),
                new DataColumn("PaymentTermsID"),
                new DataColumn("LoanDate"),

                new DataColumn("PrincipalAmount"),
                new DataColumn("WithInterest"),
                new DataColumn("BeginningBalance"),
                new DataColumn("AmortizationAmount"),
                new DataColumn("StatusID"),

                new DataColumn("StartDate"),
                new DataColumn("EndDate"),
                new DataColumn("IsPause"),
                new DataColumn("IsResume"),
                new DataColumn("IsStop"),


                new DataColumn("PausedOn"),
                new DataColumn("PausedBy"),
                new DataColumn("ResumedOn"),
                new DataColumn("ResumedBy"),
                new DataColumn("StoppedOn"),

                new DataColumn("StoppedBy"),
                new DataColumn("CreatedOn"),
                new DataColumn("CreatedBy"),
                new DataColumn("ModifiedOn"),
                new DataColumn("ModifiedBy"),

                new DataColumn("ClosedOn"),
                new DataColumn("ClosedBy") });

                for (int i = 0; i < UBP.EmpID.Length; i++)
                {
                    EmployeeLoanList.Rows.Add(



                        UBP.EmpID[i],
                        UBP.LoanTypeID[i],
                        UBP.LoanNameID[i],
                        UBP.PaymentTermsID[i],
                        UBP.LoanDate[i],

                        UBP.PrincipalAmount[i],
                        UBP.WithInterest[i],
                        UBP.BeginningBalance[i],
                        UBP.AmortizationAmount[i],
                        UBP.StatusID[i],

                        UBP.StartDate[i],
                        UBP.EndDate[i],
                        UBP.IsPause[i],
                        UBP.IsResume[i],
                        UBP.IsStop[i],

                        UBP.PausedOn[i],
                        UBP.PausedBy[i],
                        UBP.ResumedOn[i],
                        UBP.ResumedBy[i],
                        UBP.StoppedOn[i],

                        UBP.StoppedBy[i],
                        UBP.CreatedOn[i],
                        UBP.CreatedBy[i],
                        UBP.ModifiedOn[i],
                        UBP.ModifiedBy[i],

                        UBP.ClosedOn[i],
                        UBP.ClosedBy[i]);

                }

                con.myparameters.Add(new myParameters { ParameterName = "@CompanyName", mytype = SqlDbType.VarChar, Value = UBP.CN });
                con.myparameters.Add(new myParameters { ParameterName = "@IMPORT", mytype = SqlDbType.Structured, Value = EmployeeLoanList });
                con.ExecuteNonQuery("sp_UploadEmployeeLoan");

                return "Success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }

        [HttpPost]
        [Route("EmployeeLoans_Gen")]
        public string EmployeeLoanListGen(EmployeeLoanList UBP)
        {
            try
            {
                PayrollVueClients pvc = new PayrollVueClients();
                pvc.clientname = UBP.CN;
                GetDBILMDevSvr(pvc);

                Connection con = new Connection();
                DataTable EmployeeLoanList = new DataTable();
                EmployeeLoanList.Columns.AddRange(new DataColumn[28] {

                new DataColumn("EmpID"),
                new DataColumn("LoanTypeID"),
                new DataColumn("LoanNameID"),
                new DataColumn("PaymentTermsID"),
                new DataColumn("LoanDate"),

                new DataColumn("PrincipalAmount"),
                new DataColumn("WithInterest"),
                new DataColumn("BeginningBalance"),
                new DataColumn("AmortizationAmount"),
                new DataColumn("StatusID"),

                new DataColumn("StartDate"),
                new DataColumn("EndDate"),
                new DataColumn("IsPause"),
                new DataColumn("IsResume"),
                new DataColumn("IsStop"),
                new DataColumn("IsDone"),


                new DataColumn("PausedOn"),
                new DataColumn("PausedBy"),
                new DataColumn("ResumedOn"),
                new DataColumn("ResumedBy"),
                new DataColumn("StoppedOn"),

                new DataColumn("StoppedBy"),
                new DataColumn("CreatedOn"),
                new DataColumn("CreatedBy"),
                new DataColumn("ModifiedOn"),
                new DataColumn("ModifiedBy"),

                new DataColumn("ClosedOn"),
                new DataColumn("ClosedBy") });

                for (int i = 0; i < UBP.EmpID.Length; i++)
                {
                    EmployeeLoanList.Rows.Add(



                        UBP.EmpID[i],
                        UBP.LoanTypeID[i],
                        UBP.LoanNameID[i],
                        UBP.PaymentTermsID[i],
                        UBP.LoanDate[i],

                        UBP.PrincipalAmount[i],
                        UBP.WithInterest[i],
                        UBP.BeginningBalance[i],
                        UBP.AmortizationAmount[i],
                        UBP.StatusID[i],

                        UBP.StartDate[i],
                        UBP.EndDate[i],
                        UBP.IsPause[i],
                        UBP.IsResume[i],
                        UBP.IsStop[i],
                        UBP.IsDone[i],

                        UBP.PausedOn[i],
                        UBP.PausedBy[i],
                        UBP.ResumedOn[i],
                        UBP.ResumedBy[i],
                        UBP.StoppedOn[i],

                        UBP.StoppedBy[i],
                        UBP.CreatedOn[i],
                        UBP.CreatedBy[i],
                        UBP.ModifiedOn[i],
                        UBP.ModifiedBy[i],

                        UBP.ClosedOn[i],
                        UBP.ClosedBy[i]);

                }

                con.myparameters.Add(new myParameters { ParameterName = "@CompanyName", mytype = SqlDbType.VarChar, Value = UBP.CN });
                con.myparameters.Add(new myParameters { ParameterName = "@IMPORT", mytype = SqlDbType.Structured, Value = EmployeeLoanList });
                con.ExecuteNonQuery("sp_UploadEmployeeLoan");

                return "Success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }

        [HttpPost]
        [Route("EmployeeLoanDetails")]
        public string EmployeeLoanDetailsList(EmployeeLoanDetailsList UBP)
        {
            try
            {
                PayrollVueClients pvc = new PayrollVueClients();
                pvc.clientname = UBP.CN;
                GetDBILMDevSvr(pvc);

                Connection con = new Connection();
                DataTable EmployeeLoanDetailsList = new DataTable();
                EmployeeLoanDetailsList.Columns.AddRange(new DataColumn[8] {

                new DataColumn("EmployeeLoanDetailsID"),
                new DataColumn("EmployeeLoanID"),
                new DataColumn("AmortizationAmount"),
                new DataColumn("DateDeducted"),
                new DataColumn("PaymentTermsID"),
                new DataColumn("PayOutDate"),
                new DataColumn("ProcessedOn"),
                new DataColumn("ProcessedBy")});

                for (int i = 0; i < UBP.EmployeeLoanID.Length; i++)
                {
                    EmployeeLoanDetailsList.Rows.Add(

                        UBP.EmployeeLoanDetailsID[i],
                        UBP.EmployeeLoanID[i],
                        UBP.AmortizationAmount[i],
                        UBP.DateDeducted[i],
                        UBP.PaymentTermsID[i],
                        UBP.PayOutDate[i],
                        UBP.ProcessedOn[i],
                        UBP.ProcessedBy[i]);

                }

                con.myparameters.Add(new myParameters { ParameterName = "@CompanyName", mytype = SqlDbType.VarChar, Value = UBP.CN });
                con.myparameters.Add(new myParameters { ParameterName = "@IMPORT", mytype = SqlDbType.Structured, Value = EmployeeLoanDetailsList });
                con.ExecuteNonQuery("sp_UploadEmployeeLoanDetails");

                return "Success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }

        [HttpPost]
        [Route("EmployeeLogHours")]
        public string EmployeeLogHoursList(EmployeeLogHoursList UBP)
        {
            try
            {
                PayrollVueClients pvc = new PayrollVueClients();
                pvc.clientname = UBP.CN;
                GetDBILMDevSvr(pvc);

                Connection con = new Connection();
                DataTable EmployeeLogHoursList = new DataTable();
                EmployeeLogHoursList.Columns.AddRange(new DataColumn[23] {

                new DataColumn("EmployeeLogHoursID"),
                new DataColumn("EmpID"),
                new DataColumn("Basicpay"),
                new DataColumn("PayOutSchemeID"),
                new DataColumn("HoursWorked"),
                new DataColumn("Tardiness"),

                new DataColumn("Absences"),
                new DataColumn("Undertime"),
                new DataColumn("LWOP"),
                new DataColumn("HourlyRate"),
                new DataColumn("EfficiencyRate"),

                new DataColumn("PaymentTermsID"),
                new DataColumn("PayOutDate"),
                new DataColumn("IsDone"),
                new DataColumn("IsAdded"),
                new DataColumn("AddedOn"),

                new DataColumn("AddedBy"),
                new DataColumn("ProcessedOn"),
                new DataColumn("ProcessedBy"),
                new DataColumn("ClosedOn"),
                new DataColumn("ClosedBy"),

                new DataColumn("CutOffStartDate"),
                new DataColumn("CutOffEndDate")});

                for (int i = 0; i < UBP.EmpID.Length; i++)
                {
                    EmployeeLogHoursList.Rows.Add(

                        UBP.EmployeeLogHoursID[i],
                        UBP.EmpID[i],
                        UBP.Basicpay[i],
                        UBP.PayOutSchemeID[i],
                        UBP.HoursWorked[i],
                        UBP.Tardiness[i],

                        UBP.Absences[i],
                        UBP.Undertime[i],
                        UBP.LWOP[i],
                        UBP.HourlyRate[i],
                        UBP.EfficiencyRate[i],

                        UBP.PaymentTermsID[i],
                        UBP.PayOutDate[i],
                        UBP.IsDone[i],
                        UBP.IsAdded[i],
                        UBP.AddedOn[i],

                        UBP.AddedBy[i],
                        UBP.ProcessedOn[i],
                        UBP.ProcessedBy[i],
                        UBP.ClosedOn[i],
                        UBP.ClosedBy[i],

                        UBP.CutOffStartDate[i],
                        UBP.CutOffEndDate[i]);

                }

                con.myparameters.Add(new myParameters { ParameterName = "@CompanyName", mytype = SqlDbType.VarChar, Value = UBP.CN });
                con.myparameters.Add(new myParameters { ParameterName = "@IMPORT", mytype = SqlDbType.Structured, Value = EmployeeLogHoursList });
                con.ExecuteNonQuery("sp_UploadEmployeeLogHours");

                return "Success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }

        //[HttpPost]
        //[Route("EmployeeLogHoursDetailsList")]
        //public string EmployeeLogHoursDetailsList(EmployeeLogHoursDetailsList UBP)
        //{
        //    try
        //    {
        //        GetDBILMDevSvr();
        //        Connection con = new Connection();
        //        DataTable EmployeeLogHoursDetailsList = new DataTable();
        //        EmployeeLogHoursDetailsList.Columns.AddRange(new DataColumn[11] {

        //        new DataColumn("EmployeeLogHoursID"),
        //        new DataColumn("HoursworkedAmount"),
        //        new DataColumn("TardinessAmount"),
        //        new DataColumn("AbsencesAmount"),
        //        new DataColumn("UndertimeAmount"),

        //        new DataColumn("LWOPAmount"),
        //        new DataColumn("PaymentTermsID"),
        //        new DataColumn("PayOutDate"),
        //        new DataColumn("ProcessedOn"),
        //        new DataColumn("ProcessedBy"),
        //        new DataColumn("RowNum") });

        //        for (int i = 0; i < UBP.EmployeeLogHoursID.Length; i++)
        //        {
        //            EmployeeLogHoursDetailsList.Rows.Add(

        //                UBP.EmployeeLogHoursID[i],
        //                UBP.HoursworkedAmount[i],
        //                UBP.TardinessAmount[i],
        //                UBP.AbsencesAmount[i],
        //                UBP.UndertimeAmount[i],
        //                UBP.LWOPAmount[i],
        //                UBP.PaymentTermsID[i],
        //                UBP.PayOutDate[i],
        //                UBP.ProcessedOn[i],
        //                UBP.ProcessedBy[i],
        //                UBP.RowNum[i]);

        //        }

        //        con.myparameters.Add(new myParameters { ParameterName = "@CompanyName", mytype = SqlDbType.VarChar, Value = UBP.CN });
        //        con.myparameters.Add(new myParameters { ParameterName = "@IMPORT", mytype = SqlDbType.Structured, Value = EmployeeLogHoursDetailsList });
        //        DataTable DT = con.GetDataTable("sp_UploadEmployeeLogHoursDetails");

        //        return "Success";
        //    }
        //    catch (Exception e)
        //    {
        //        return e.ToString();
        //    }
        //}

        [HttpPost]
        [Route("EmployeeHourDetailsList")]
        public string EmployeeHourDetailsList(EmployeeHourDetailsList UBP)
        {
            try
            {
                PayrollVueClients pvc = new PayrollVueClients();
                pvc.clientname = UBP.CN;
                GetDBILMDevSvr(pvc);

                Connection con = new Connection();
                DataTable EmployeeHourDetailsList = new DataTable();
                EmployeeHourDetailsList.Columns.AddRange(new DataColumn[11] {

                new DataColumn("EmployeeLogHoursDetailsID"),
                new DataColumn("EmployeeLogHoursID"),
                new DataColumn("HoursworkedAmount"),
                new DataColumn("TardinessAmount"),
                new DataColumn("AbsencesAmount"),
                new DataColumn("UndertimeAmount"),

                new DataColumn("LWOPAmount"),
                new DataColumn("PaymentTermsID"),
                new DataColumn("PayOutDate"),
                new DataColumn("ProcessedOn"),
                new DataColumn("ProcessedBy")});

                for (int i = 0; i < UBP.EmployeeLogHoursID.Length; i++)
                {
                    EmployeeHourDetailsList.Rows.Add(
                        UBP.EmployeeLogHoursDetailsID[i],
                        UBP.EmployeeLogHoursID[i],
                        UBP.HoursworkedAmount[i],
                        UBP.TardinessAmount[i],
                        UBP.AbsencesAmount[i],
                        UBP.UndertimeAmount[i],

                        UBP.LWOPAmount[i],
                        UBP.PaymentTermsID[i],
                        UBP.PayOutDate[i],
                        UBP.ProcessedOn[i],
                        UBP.ProcessedBy[i]);

                }

                con.myparameters.Add(new myParameters { ParameterName = "@CompanyName", mytype = SqlDbType.VarChar, Value = UBP.CN });
                con.myparameters.Add(new myParameters { ParameterName = "@IMPORT", mytype = SqlDbType.Structured, Value = EmployeeHourDetailsList });
                con.ExecuteNonQuery("sp_UploadEmployeeLogHoursDetails");

                return "Success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }


        [HttpPost]
        [Route("Overtime")]
        public string OvertimeList(OvertimeList UBP)
        {
            try
            {
                PayrollVueClients pvc = new PayrollVueClients();
                pvc.clientname = UBP.CN;
                GetDBILMDevSvr(pvc);

                Connection con = new Connection();
                DataTable OvertimeList = new DataTable();
                OvertimeList.Columns.AddRange(new DataColumn[7] {


                new DataColumn("OvertimeCode"),
                new DataColumn("OvertimeDescription"),

                new DataColumn("Rate"),
                new DataColumn("CreatedOn"),
                new DataColumn("CreatedBy"),
                new DataColumn("ModifiedOn"),
                new DataColumn("ModifiedBy") });

                for (int i = 0; i < UBP.OvertimeCode.Length; i++)
                {
                    OvertimeList.Rows.Add(

                        UBP.OvertimeCode[i],
                        UBP.OvertimeDescription[i],
                        UBP.Rate[i],
                        UBP.CreatedOn[i],
                        UBP.CreatedBy[i],
                        UBP.ModifiedOn[i],
                        UBP.ModifiedBy[i]);

                }

                con.myparameters.Add(new myParameters { ParameterName = "@CompanyName", mytype = SqlDbType.VarChar, Value = UBP.CN });
                con.myparameters.Add(new myParameters { ParameterName = "@IMPORT", mytype = SqlDbType.Structured, Value = OvertimeList });
                con.ExecuteNonQuery("sp_UploadOvertime");

                return "Success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }

        [HttpPost]
        [Route("EmployeeOvertime")]
        public string EmployeeOvertimeList(EmployeeOvertimeList UBP)
        {
            try
            {
                PayrollVueClients pvc = new PayrollVueClients();
                pvc.clientname = UBP.CN;
                GetDBILMDevSvr(pvc);

                Connection con = new Connection();
                DataTable EmployeeOvertimeList = new DataTable();
                EmployeeOvertimeList.Columns.AddRange(new DataColumn[16] {

                new DataColumn("EmployeeOvertimeID"),
                new DataColumn("EmpID"),
                new DataColumn("OvertimeID"),
                new DataColumn("OvertimeHours"),
                new DataColumn("CompanyRate"),
                new DataColumn("HourlyRate"),

                new DataColumn("PaymentTermsID"),
                new DataColumn("PayOutDate"),
                new DataColumn("IsDone"),
                new DataColumn("IsAdded"),
                new DataColumn("AddedOn"),

                new DataColumn("AddedBy"),
                new DataColumn("ProcessedOn"),
                new DataColumn("ProcessedBy"),
                new DataColumn("ClosedOn"),
                new DataColumn("ClosedBy") });

                for (int i = 0; i < UBP.EmpID.Length; i++)
                {
                    EmployeeOvertimeList.Rows.Add(
                        UBP.EmployeeOvertimeID[i],
                        UBP.EmpID[i],
                        UBP.OvertimeID[i],
                        UBP.OvertimeHours[i],
                        UBP.CompanyRate[i],
                        UBP.HourlyRate[i],

                        UBP.PaymentTermsID[i],
                        UBP.PayOutDate[i],
                        UBP.IsDone[i],
                        UBP.IsAdded[i],
                        UBP.AddedOn[i],

                        UBP.AddedBy[i],
                        UBP.ProcessedOn[i],
                        UBP.ProcessedBy[i],
                        UBP.ClosedOn[i],
                        UBP.ClosedBy[i]);

                }

                con.myparameters.Add(new myParameters { ParameterName = "@CompanyName", mytype = SqlDbType.VarChar, Value = UBP.CN });
                con.myparameters.Add(new myParameters { ParameterName = "@IMPORT", mytype = SqlDbType.Structured, Value = EmployeeOvertimeList });
                con.ExecuteNonQuery("sp_UploadEmployeeOvertime");

                return "Success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }

        [HttpPost]
        [Route("EmployeeOvertimeDetails")]
        public string EmployeeOvertimeDetailsList(EmployeeOvertimeDetailsList UBP)
        {
            try
            {
                PayrollVueClients pvc = new PayrollVueClients();
                pvc.clientname = UBP.CN;
                GetDBILMDevSvr(pvc);

                Connection con = new Connection();
                DataTable EmployeeOvertimeDetailsList = new DataTable();
                EmployeeOvertimeDetailsList.Columns.AddRange(new DataColumn[7] {

                new DataColumn("EmployeeOvertimeDetailsID"),
                new DataColumn("EmployeeOvertimeID"),
                new DataColumn("OvertimeAmount"),
                new DataColumn("PaymentTermsID"),
                new DataColumn("PayOutDate"),
                new DataColumn("ProcessedOn"),

                new DataColumn("ProcessedBy")});

                for (int i = 0; i < UBP.EmployeeOvertimeID.Length; i++)
                {
                    EmployeeOvertimeDetailsList.Rows.Add(
                        UBP.EmployeeOvertimeDetailsID[i],
                        UBP.EmployeeOvertimeID[i],
                        UBP.OvertimeAmount[i],
                        UBP.PaymentTermsID[i],
                        UBP.PayOutDate[i],
                        UBP.ProcessedOn[i],
                        UBP.ProcessedBy[i]);

                }

                con.myparameters.Add(new myParameters { ParameterName = "@CompanyName", mytype = SqlDbType.VarChar, Value = UBP.CN });
                con.myparameters.Add(new myParameters { ParameterName = "@IMPORT", mytype = SqlDbType.Structured, Value = EmployeeOvertimeDetailsList });
                con.ExecuteNonQuery("sp_UploadEmployeeOvertimeDetails");

                return "Success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }

        [HttpPost]
        [Route("EmployeePagibigDetails")]
        public string EmployeePagibigDetails(EmployeePagibigDetails UBP)
        {
            try
            {
                PayrollVueClients pvc = new PayrollVueClients();
                pvc.clientname = UBP.CN;
                GetDBILMDevSvr(pvc);

                Connection con = new Connection();
                DataTable EmployeePagibigDetails = new DataTable();
                EmployeePagibigDetails.Columns.AddRange(new DataColumn[9] {

                new DataColumn("EmployeePagIbigID"),
                new DataColumn("EmpID"),
                new DataColumn("GrossPay"),
                new DataColumn("PagIbigAmount"),
                new DataColumn("PagIbigEmployerAmount"),
                new DataColumn("PaymentTermsID"),
                new DataColumn("PayOutDate"),
                new DataColumn("ProcessedOn"),
                new DataColumn("ProcessedBy")});

                for (int i = 0; i < UBP.EmpID.Length; i++)
                {
                    EmployeePagibigDetails.Rows.Add(

                        UBP.EmployeePagIbigID[i],
                        UBP.EmpID[i],
                        UBP.GrossPay[i],
                        UBP.PagIbigAmount[i],
                        UBP.PagIbigEmployerAmount[i],
                        UBP.PaymentTermsID[i],
                        UBP.PayOutDate[i],
                        UBP.ProcessedOn[i],
                        UBP.ProcessedBy[i]);

                }

                con.myparameters.Add(new myParameters { ParameterName = "@CompanyName", mytype = SqlDbType.VarChar, Value = UBP.CN });
                con.myparameters.Add(new myParameters { ParameterName = "@IMPORT", mytype = SqlDbType.Structured, Value = EmployeePagibigDetails });
                con.ExecuteNonQuery("sp_UploadEmployeePAGIBIGDetails");

                return "Success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }

        [HttpPost]
        [Route("EmployeePHILHEALTHDetails")]
        public string EmployeePHILHEALTHDetails(EmployeePHILHEALTHDetails UBP)
        {
            try
            {
                PayrollVueClients pvc = new PayrollVueClients();
                pvc.clientname = UBP.CN;
                GetDBILMDevSvr(pvc);

                Connection con = new Connection();
                DataTable EmployeePHILHEALTHDetails = new DataTable();
                EmployeePHILHEALTHDetails.Columns.AddRange(new DataColumn[9] {

                new DataColumn("EmployeePhilHealthID"),
                new DataColumn("EmpID"),
                new DataColumn("GrossPay"),
                new DataColumn("PhilHealthAmount"),
                new DataColumn("PhilHealthEmployerAmount"),
                new DataColumn("PaymentTermsID"),
                new DataColumn("PayOutDate"),
                new DataColumn("ProcessedOn"),
                new DataColumn("ProcessedBy") });

                for (int i = 0; i < UBP.EmpID.Length; i++)
                {
                    EmployeePHILHEALTHDetails.Rows.Add(
                        UBP.EmployeePhilHealthID[i],
                        UBP.EmpID[i],
                        UBP.GrossPay[i],
                        UBP.PhilHealthAmount[i],
                        UBP.PhilHealthEmployerAmount[i],
                        UBP.PaymentTermsID[i],
                        UBP.PayOutDate[i],
                        UBP.ProcessedOn[i],
                        UBP.ProcessedBy[i]);

                }

                con.myparameters.Add(new myParameters { ParameterName = "@CompanyName", mytype = SqlDbType.VarChar, Value = UBP.CN });
                con.myparameters.Add(new myParameters { ParameterName = "@IMPORT", mytype = SqlDbType.Structured, Value = EmployeePHILHEALTHDetails });
                con.ExecuteNonQuery("sp_UploadEmployeePHILHEALTHDetails");

                return "Success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }

        [HttpPost]
        [Route("EmployeeSSSDetails")]
        public string EmployeeSSSDetails(EmployeeSSSDetails UBP)
        {
            try
            {
                PayrollVueClients pvc = new PayrollVueClients();
                pvc.clientname = UBP.CN;
                GetDBILMDevSvr(pvc);

                Connection con = new Connection();
                DataTable EmployeeSSSDetails = new DataTable();
                EmployeeSSSDetails.Columns.AddRange(new DataColumn[10] {
                new DataColumn("EmployeeSSSID"),
                new DataColumn("EmpID"),
                new DataColumn("GrossPay"),
                new DataColumn("SSSAmount"),
                new DataColumn("SSSEmployerAmount"),
                new DataColumn("ECAmount"),
                new DataColumn("PaymentTermsID"),
                new DataColumn("PayOutDate"),
                new DataColumn("ProcessedOn"),
                new DataColumn("ProcessedBy")});

                for (int i = 0; i < UBP.EmpID.Length; i++)
                {
                    EmployeeSSSDetails.Rows.Add(
                        UBP.EmployeeSSSID[i],
                        UBP.EmpID[i],
                        UBP.GrossPay[i],
                        UBP.SSSAmount[i],
                        UBP.SSSEmployerAmount[i],
                        UBP.ECAmount[i],
                        UBP.PaymentTermsID[i],
                        UBP.PayOutDate[i],
                        UBP.ProcessedOn[i],
                        UBP.ProcessedBy[i]);

                }

                con.myparameters.Add(new myParameters { ParameterName = "@CompanyName", mytype = SqlDbType.VarChar, Value = UBP.CN });
                con.myparameters.Add(new myParameters { ParameterName = "@IMPORT", mytype = SqlDbType.Structured, Value = EmployeeSSSDetails });
                con.ExecuteNonQuery("sp_UploadEmployeeSSSDetails");

                return "Success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }

        [HttpPost]
        [Route("EmployeeNetpay")]
        public string EmployeeNetpayList(EmployeeNetpayList UBP)
        {
            try
            {
                PayrollVueClients pvc = new PayrollVueClients();
                pvc.clientname = UBP.CN;
                GetDBILMDevSvr(pvc);

                Connection con = new Connection();
                DataTable EmployeeNetpayList = new DataTable();
                EmployeeNetpayList.Columns.AddRange(new DataColumn[33] {


                new DataColumn("EmployeeNetPayID"),
                new DataColumn("EmpID"),
                new DataColumn("PayoutSchemeID"),
                new DataColumn("PaymentTermsID"),
                new DataColumn("PayOutDate"),
                new DataColumn("BasicPay"),

                new DataColumn("HoursWorkedAmount"),
                new DataColumn("TardinessAmount"),
                new DataColumn("AbsencesAmount"),
                new DataColumn("UndertimeAmount"),
                new DataColumn("LWOP"),

                new DataColumn("EfficiencyRate"),
                new DataColumn("GrossPay"),
                new DataColumn("TaxableIncome"),
                new DataColumn("OvertimeGrossPay"),
                new DataColumn("SSSAmount"),

                new DataColumn("PhilHealthAmount"),
                new DataColumn("PagIbigAmount"),
                new DataColumn("EarningsTotal"),
                new DataColumn("EarningsGrossPay"),
                new DataColumn("EarningsTaxableAmount"),

                new DataColumn("EarningsNonTaxableAmount"),
                new DataColumn("EarningsTaxableBonus"),
                new DataColumn("EarningsNonTaxableBonus"),
                new DataColumn("DeductionsTotal"),
                new DataColumn("DeductionsGrossPay"),

                new DataColumn("DeductionsTaxableAmount"),
                new DataColumn("DeductionsNonTaxableAmount"),
                new DataColumn("DeductionsTaxableBonus"),
                new DataColumn("DeductionsNonTaxableBonus"),
                new DataColumn("TotalLoanAmount"),

                new DataColumn("WHTax"),
                new DataColumn("NetPay") });

                for (int i = 0; i < UBP.EmpID.Length; i++)
                {
                    EmployeeNetpayList.Rows.Add(


                        UBP.EmployeeNetPayID[i],
                        UBP.EmpID[i],
                        UBP.PayoutSchemeID[i],
                        UBP.PaymentTermsID[i],
                        UBP.PayOutDate[i],
                        UBP.BasicPay[i],

                        UBP.HoursWorkedAmount[i],
                        UBP.TardinessAmount[i],
                        UBP.AbsencesAmount[i],
                        UBP.UndertimeAmount[i],
                        UBP.LWOP[i],

                        UBP.EfficiencyRate[i],
                        UBP.GrossPay[i],
                        UBP.TaxableIncome[i],
                        UBP.OvertimeGrossPay[i],
                        UBP.SSSAmount[i],

                        UBP.PhilHealthAmount[i],
                        UBP.PagIbigAmount[i],
                        UBP.EarningsTotal[i],
                        UBP.EarningsGrossPay[i],
                        UBP.EarningsTaxableAmount[i],

                        UBP.EarningsNonTaxableAmount[i],
                        UBP.EarningsTaxableBonus[i],
                        UBP.EarningsNonTaxableBonus[i],
                        UBP.DeductionsTotal[i],
                        UBP.DeductionsGrossPay[i],

                        UBP.DeductionsTaxableAmount[i],
                        UBP.DeductionsNonTaxableAmount[i],
                        UBP.DeductionsTaxableBonus[i],
                        UBP.DeductionsNonTaxableBonus[i],
                        UBP.TotalLoanAmount[i],

                        UBP.WHTax[i],
                        UBP.NetPay[i]);

                }

                con.myparameters.Add(new myParameters { ParameterName = "@CompanyName", mytype = SqlDbType.VarChar, Value = UBP.CN });
                con.myparameters.Add(new myParameters { ParameterName = "@IMPORT", mytype = SqlDbType.Structured, Value = EmployeeNetpayList });
                con.ExecuteNonQuery("sp_UploadEmployeeNetpay");

                return "Success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }

        //PAPA P 2/18/2020
        [HttpPost]
        [Route("ws_payroll_audittrail")]
        public string InsertAuditTrail(InsertAT param)
        {
            PayrollVueClients pvc = new PayrollVueClients();
            pvc.clientname = param.client;
            GetDBILMMain();
            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@emp", mytype = SqlDbType.NVarChar, Value = param.emp });
            Connection.myparameters.Add(new myParameters { ParameterName = "@client", mytype = SqlDbType.NVarChar, Value = param.client });
            Connection.myparameters.Add(new myParameters { ParameterName = "@ipaddress", mytype = SqlDbType.NVarChar, Value = param.ipaddress });
            Connection.myparameters.Add(new myParameters { ParameterName = "@status", mytype = SqlDbType.NVarChar, Value = param.status });


            Connection.ExecuteNonQuery("sp_payroll_audittrail");
            return "success";

        }

        [HttpPost]
        [Route("ws_get_payroll_AT")]
        public getPayrollAT getAuditTrail(PayrollAuditTrail param)
        {
            PayrollVueClients pvc = new PayrollVueClients();
            pvc.clientname = param.client;
            GetDBILMMain();
            Connection Connection = new Connection();

            DataTable dtuserList = new DataTable();
            getPayrollAT PayrollATResults = new getPayrollAT();
            List<PayrollAuditTrail> AuditTrailList = new List<PayrollAuditTrail>();


            dtuserList = Connection.GetDataTable("sp_get_payroll_AT");
            if (dtuserList.Rows.Count > 0)
            {
                foreach (DataRow row in dtuserList.Rows)
                {
                    PayrollAuditTrail PAT = new PayrollAuditTrail
                    {

                        empid = row["empid"].ToString(),
                        empname = row["empname"].ToString(),
                        client = row["client"].ToString(),
                        status = row["status"].ToString(),
                        datestamp = row["datestamp"].ToString(),
                        ipaddress = row["ipaddress"].ToString(),
                    };
                    AuditTrailList.Add(PAT);
                }
                PayrollATResults.result = AuditTrailList;
                PayrollATResults.myreturn = "Success";
            }
            else
            {
                PayrollATResults.myreturn = "Error";
            }
            return PayrollATResults;
        }

        [HttpPost]
        [Route("GetEmployeeEarningsLastID")]
        public string GetEmployeeEarningsLastID(PayrollVueClients UBP )
        {
            try
            {
                PayrollVueClients pvc = new PayrollVueClients();
                pvc.clientname = UBP.clientname;
                GetDBILMDevSvr(pvc);



                Connection con = new Connection();
                String id = con.ExecuteScalar("sp_GetEmployeeEarningsLastID");

                return id;
            }
            catch (Exception e)
            {
                return e.ToString();
            }

        }

        [HttpPost]
        [Route("EmployeeEarningsV2")]
        public string EmployeeEarningsListV2(EmployeeEarningsListV2 UBP )
        {
            try
            {
                PayrollVueClients pvc = new PayrollVueClients();
                pvc.clientname = UBP.CN;
                GetDBILMDevSvr(pvc);
               // GetEmployeeEarningsLastID(pvc);
               

                for (int i = 0; i < UBP.EmpID.Length; i++)
                {
                   
                DataTable EmployeeEarningsList = new DataTable();
                EmployeeEarningsList.Columns.AddRange(new DataColumn[25] {



                new DataColumn("EmpID") ,//
                new DataColumn("EarningsTypeID") ,
                new DataColumn("OneTime") ,//
                new DataColumn("EarningsAmount") ,//
                new DataColumn("StartDate"),
                new DataColumn("EndDate"),
                new DataColumn("NoEndDate") ,
                new DataColumn("PaymentTermsID"),
                new DataColumn("IsPause"),
                new DataColumn("IsResume") ,

                new DataColumn("IsStop") ,//
                new DataColumn("IsDone") ,
                new DataColumn("PausedOn") ,
                new DataColumn("PausedBy") ,
                new DataColumn("ResumedOn"),
                new DataColumn("ResumedBy"),
                new DataColumn("StoppedOn") ,
                new DataColumn("StoppedBy"),
                new DataColumn("UpdateAmount"),
                new DataColumn("UpdateAmountOn") ,

                new DataColumn("UpdateAmountBy") ,
                new DataColumn("CreatedOn") ,
                new DataColumn("CreatedBy") ,
                new DataColumn("ModifiedOn") ,
                new DataColumn("ModifiedBy")

                //new DataColumn("EmpName") ,//
                //new DataColumn("EarningsName"),//
                //new DataColumn("FinalDeduction")
                });//

                    //for (int i = 0; i < UBP.EmpID.Length; i++)
                    //{
                    EmployeeEarningsList.Rows.Add(

                        UBP.EmpID[i],
                        UBP.EarningsTypeID[i],
                        UBP.OneTime[i],
                        UBP.EarningsAmount[i],
                        UBP.StartDate,
                        UBP.EndDate,
                        "",
                        "",
                        "",
                        "",
                        UBP.IsStop[i],
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        DateTime.Today,
                        "",
                        "",
                        ""



                        //UBP.EmpName[i],
                        //UBP.EarningsName[i],
                        //UBP.FinalDeduction[i]
                        );

                    //}
                    GetDBILMDevSvr(pvc);
                    Connection con = new Connection();
                    con.myparameters.Add(new myParameters { ParameterName = "@CompanyName", mytype = SqlDbType.VarChar, Value = UBP.CN });
                    con.myparameters.Add(new myParameters { ParameterName = "@IMPORT", mytype = SqlDbType.Structured, Value = EmployeeEarningsList });
                    con.ExecuteNonQuery("sp_UploadEmployeeEarnings");

                    //return id;


                    String lastid = GetEmployeeEarningsLastID(pvc);
                    int finalid = Int32.Parse(lastid);

                    Connection con2 = new Connection();
                DataTable EmployeeEarningsDetailsList = new DataTable();
                EmployeeEarningsDetailsList.Columns.AddRange(new DataColumn[8] {
                new DataColumn("EmployeeEarningsDetailsID"),
                new DataColumn("EmployeeEarningsID") ,
                new DataColumn("EarningsAmount") ,
                new DataColumn("PayOutDate") ,
                new DataColumn("ProcessedOn") ,
                new DataColumn("ProcessedBy"),
                new DataColumn("ClosedOn"),
                new DataColumn("ClosedBy")});

                    String randomizedtext = Guid.NewGuid().ToString();

                    //for (int i = 0; i < UBP.EmpID.Length; i++)
                    //    {
                    EmployeeEarningsDetailsList.Rows.Add(
                            randomizedtext,
                            (finalid),
                            UBP.EarningsAmount[i],
                            UBP.CreatedOn[0],
                            "",
                            "",
                            "",
                            "");

                    //}

                    con2.myparameters.Add(new myParameters { ParameterName = "@CompanyName", mytype = SqlDbType.VarChar, Value = UBP.CN });
                    con2.myparameters.Add(new myParameters { ParameterName = "@IMPORT", mytype = SqlDbType.Structured, Value = EmployeeEarningsDetailsList });
                    con2.ExecuteNonQuery("sp_UploadEmployeeEarningDetails");


                }
                return "Success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }

        }

        //[HttpPost]
        //[Route("adjustsample")]
        //public string AdjustSample(EmployeeEarningsList UBP)
        //{
        //    try
        //    {
        //        PayrollVueClients pvc = new PayrollVueClients();
        //        pvc.clientname = UBP.CN;
        //        GetDBILMDevSvr(pvc);

        //        Connection con = new Connection();
        //        DataTable EmployeeEarningsList = new DataTable();
        //        EmployeeEarningsList.Columns.AddRange(new DataColumn[6] {

        //        new DataColumn("EmpID") ,
        //        new DataColumn("EmpName") ,
        //        new DataColumn("AdjustmentType"),

        //        new DataColumn("OneTime") ,
        //        new DataColumn("AdjustmentAmount") ,
        //        new DataColumn("Checker")});

        //        for (int i = 0; i < UBP.EmpID.Length; i++)
        //        {
        //            EmployeeEarningsList.Rows.Add(

        //                UBP.EmpID[i],
        //                UBP.EmpName[i],
        //                UBP.AdjustmentType[i],
        //                UBP.OneTime[i],
        //                UBP.AdjustmentAmount[i],
        //                UBP.Checker[i]
        //                );

        //        }

        //        con.myparameters.Add(new myParameters { ParameterName = "@CompanyName", mytype = SqlDbType.VarChar, Value = UBP.CN });
        //        con.myparameters.Add(new myParameters { ParameterName = "@IMPORT", mytype = SqlDbType.Structured, Value = EmployeeEarningsList });
        //        con.ExecuteNonQuery("sp_UploadEmployeeEarnings");

        //        return "Success";
        //    }
        //    catch (Exception e)
        //    {
        //        return e.ToString();
        //    }
        //}

        [HttpPost]
        [Route("EmployeeLoansLastID")]
        public string EmployeeLoansLastID(PayrollVueClients UBP)
        {
            try
            {
                PayrollVueClients pvc = new PayrollVueClients();
                pvc.clientname = UBP.clientname;
                GetDBILMDevSvr(pvc);



                Connection con = new Connection();
                String id = con.ExecuteScalar("sp_GetEmployeeLoansLastID");

                return id;
            }
            catch (Exception e)
            {
                return e.ToString();
            }

        }

        [HttpPost]
        [Route("EmployeeLoansV2")]
        public string EmployeeLoanListV2(EmployeeLoanListV2 UBP)
        {
            try
            {
                PayrollVueClients pvc = new PayrollVueClients();
                pvc.clientname = UBP.CN;
              
                
            
              
                GetDBILMDevSvr(pvc);

                for (int i = 0; i < UBP.EmpID.Length; i++)
                {
                   
                    DataTable EmployeeLoanList = new DataTable();
                    EmployeeLoanList.Columns.AddRange(new DataColumn[27] {

                new DataColumn("EmpID"),//
                new DataColumn("LoanTypeID"),
                new DataColumn("LoanNameID"),
                new DataColumn("PaymentTermsID"),
                new DataColumn("LoanDate"),

                new DataColumn("PrincipalAmount"),
                new DataColumn("WithInterest"),
                new DataColumn("BeginningBalance"),//
                new DataColumn("AmortizationAmount"),//
                new DataColumn("StatusID"),

                new DataColumn("StartDate"),
                new DataColumn("EndDate"),
                new DataColumn("IsPause"),
                new DataColumn("IsResume"),
                new DataColumn("IsStop"),


                new DataColumn("PausedOn"),
                new DataColumn("PausedBy"),
                new DataColumn("ResumedOn"),
                new DataColumn("ResumedBy"),
                new DataColumn("StoppedOn"),

                new DataColumn("StoppedBy"),
                new DataColumn("CreatedOn"),
                new DataColumn("CreatedBy"),
                new DataColumn("ModifiedOn"),
                new DataColumn("ModifiedBy"),

                new DataColumn("ClosedOn"),
                new DataColumn("ClosedBy") });
                    //for (int i = 0; i < UBP.EmpID.Length; i++)
                    //{
                    EmployeeLoanList.Rows.Add(



                        UBP.EmpID[i],
                        UBP.LoanTypeID[i],
                        "",
                        "",
                        "",

                        "",
                        "",
                        UBP.BeginningBalance[i],
                        UBP.AmortizationAmount[i],
                        "",

                        UBP.StartDate,
                        UBP.EndDate,
                        "",
                        "",
                        "",

                        "",
                        "",
                        "",
                        "",
                        "",

                        "",
                        DateTime.Today,
                        "",
                        "",
                        "",

                        "",
                        "");
                    //    }

                    //}
                    GetDBILMDevSvr(pvc);
                    Connection con = new Connection();
                    con.myparameters.Add(new myParameters { ParameterName = "@CompanyName", mytype = SqlDbType.VarChar, Value = UBP.CN });
                    con.myparameters.Add(new myParameters { ParameterName = "@IMPORT", mytype = SqlDbType.Structured, Value = EmployeeLoanList });
                    con.ExecuteNonQuery("sp_UploadEmployeeLoan");

                    GetDBILMDevSvr(pvc);
                    String lastid = EmployeeLoansLastID(pvc);
                    int finalid = Int32.Parse(lastid);
                    
                    DataTable EmployeeLoanDetailsList = new DataTable();
                    EmployeeLoanDetailsList.Columns.AddRange(new DataColumn[8] {

                new DataColumn("EmployeeLoanDetailsID"),
                new DataColumn("EmployeeLoanID"),
                new DataColumn("AmortizationAmount"),
                new DataColumn("DateDeducted"),
                new DataColumn("PaymentTermsID"),
                new DataColumn("PayOutDate"),
                new DataColumn("ProcessedOn"),
                new DataColumn("ProcessedBy")});



                    //for (int i = 0; i < UBP.EmpID.Length; i++)
                    //{
                    String randomizedtext = Guid.NewGuid().ToString();

                    EmployeeLoanDetailsList.Rows.Add(

                    randomizedtext,
                    (finalid),
                    UBP.AmortizationAmount[i],
                    "",
                    "",
                    UBP.CreatedOn[0],
                    "",
                    "");

                    // }
                    GetDBILMDevSvr(pvc);
                    Connection con2 = new Connection();
                    con2.myparameters.Add(new myParameters { ParameterName = "@CompanyName", mytype = SqlDbType.VarChar, Value = UBP.CN });
                    con2.myparameters.Add(new myParameters { ParameterName = "@IMPORT", mytype = SqlDbType.Structured, Value = EmployeeLoanDetailsList });
                    con2.ExecuteNonQuery("sp_UploadEmployeeLoanDetails");


                  
                }
                return "Success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }

        [HttpPost]
        [Route("DeductionEmployeeLastID")]
        public string DeductionEmployeeLastID(PayrollVueClients UBP)
        {
            try
            {
                PayrollVueClients pvc = new PayrollVueClients();
                pvc.clientname = UBP.clientname;
                GetDBILMDevSvr(pvc);



                Connection con = new Connection();
                String id = con.ExecuteScalar("sp_GetEmployeeDeductionLastID");

                return id;
            }
            catch (Exception e)
            {
                return e.ToString();
            }

        }

        [HttpPost]
        [Route("DeductionEmployeeV2")]
        public string DeductionEmployeeV2(DeductionEmployeeV2 UBP)
        {

            try
            {
                PayrollVueClients pvc = new PayrollVueClients();
                pvc.clientname = UBP.CN;
             
               

                for (int i = 0; i < UBP.EmpID.Length; i++)
                {

                  
                DataTable DeductionEmployee = new DataTable();
                DeductionEmployee.Columns.AddRange(new DataColumn[25] {
                new DataColumn("EmpID"),//
                new DataColumn("DeductionTypeID"),
                new DataColumn("OneTime"),//
                new DataColumn("DeductionAmount"),//

                new DataColumn("StartDate"),
                new DataColumn("EndDate"),
                new DataColumn("NoEndDate"),
                new DataColumn("PaymentTermsID"),
                new DataColumn("IsPause"),//

                new DataColumn("IsResume"),
                new DataColumn("IsStop"),//
                new DataColumn("IsDone"),
                new DataColumn("UpdateAmount"),
                new DataColumn("UpdateAmountOn"),

                new DataColumn("UpdateAmountBy"),
                new DataColumn("PausedOn"),
                new DataColumn("PausedBy"),
                new DataColumn("ResumedOn"),
                new DataColumn("ResumedBy"),

                new DataColumn("StoppedOn"),
                new DataColumn("StoppedBy"),
                new DataColumn("CreatedOn"),
                new DataColumn("CreatedBy"),
                new DataColumn("ModifiedOn"),

                new DataColumn("ModifiedBy")
                });

                //for (int i = 0; i < UBP.EmpID.Length; i++)
                //{
                DeductionEmployee.Rows.Add(
                    UBP.EmpID[i],//
                    UBP.DeductionTypeID[i],
                    UBP.OneTime[i],//
                    UBP.DeductionAmount[i],//

                    UBP.StartDate,
                    UBP.EndDate,
                    "",
                    "",
                    UBP.IsPause[i],//

                    "",
                    UBP.IsStop[i],//
                    "",
                    "",
                    "",

                    "",
                    "",
                    "",
                    "",
                    "",

                    "",
                    "",
                    DateTime.Today,
                    "",
                    "",

                    ""
                    );

                    //}
                    GetDBILMDevSvr(pvc);
                    Connection con = new Connection();
                    con.myparameters.Add(new myParameters { ParameterName = "@CompanyName", mytype = SqlDbType.VarChar, Value = UBP.CN });
                con.myparameters.Add(new myParameters { ParameterName = "@IMPORT", mytype = SqlDbType.Structured, Value = DeductionEmployee });
                con.ExecuteNonQuery("sp_UploadDeductionsEmployee");

                String randomizedtext = Guid.NewGuid().ToString();
                    GetDBILMDevSvr(pvc);
                    String lastid = DeductionEmployeeLastID(pvc);
                    int finalid = Int32.Parse(lastid);

                   
                 
                DataTable DeductionEmployeeDetails = new DataTable();
                DeductionEmployeeDetails.Columns.AddRange(new DataColumn[8] {
                new DataColumn("EmployeeDeductionDetailsID") ,
                new DataColumn("EmployeeDeductionID") ,
                new DataColumn("DeductionAmount"),
                new DataColumn("PayOutDate") ,
                new DataColumn("ProcessedOn") ,
                new DataColumn("ProcessedBy") ,
                new DataColumn("ClosedOn") ,
                new DataColumn("ClosedBy")});

                //for (int i = 0; i < UBP.EmpID.Length; i++)
                //{
                DeductionEmployeeDetails.Rows.Add(
                    randomizedtext,
                    ( finalid),
                    UBP.DeductionAmount[i],
                    UBP.CreatedOn[0],
                    "",
                    "",
                    "",
                    "");
                    //}
                    GetDBILMDevSvr(pvc);
                    Connection con2 = new Connection();
                    con2.myparameters.Add(new myParameters { ParameterName = "@CompanyName", mytype = SqlDbType.VarChar, Value = UBP.CN });
                con2.myparameters.Add(new myParameters { ParameterName = "@IMPORT", mytype = SqlDbType.Structured, Value = DeductionEmployeeDetails });
                con2.ExecuteNonQuery("sp_UploadEmployeeDeductionDetails");

            }

                return "Success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }


        [HttpPost]
        [Route("EmployeeAdjustment")]
        public string EmployeeAdjustment(EmployeeAdjustmentsListV2 UBP)
        {

            try
            {
                for (var i = 0; i < UBP.EmpID.Length; i++)
                {
                    Double samp = 0;
                    samp = Convert.ToDouble(UBP.AdjustedAmount[i]);
                    Console.WriteLine(samp);
                    if (samp > 0)
                    {
                        PayrollVueClients pvc = new PayrollVueClients();
                        pvc.clientname = UBP.CN;
                        GetDBILMDevSvr(pvc);
                        GetEmployeeEarningsLastID(pvc);
                        String lastid = GetEmployeeEarningsLastID(pvc);
                        int finalid = Int32.Parse(lastid);



                        Connection con = new Connection();
                        DataTable EmployeeEarningsList = new DataTable();
                        EmployeeEarningsList.Columns.AddRange(new DataColumn[25] {



                            new DataColumn("EmpID") ,//
                            new DataColumn("EarningsTypeID") ,
                            new DataColumn("OneTime") ,//
                            new DataColumn("EarningsAmount") ,//
                            new DataColumn("StartDate"),
                            new DataColumn("EndDate"),
                            new DataColumn("NoEndDate") ,
                            new DataColumn("PaymentTermsID"),
                            new DataColumn("IsPause"),
                            new DataColumn("IsResume") ,

                            new DataColumn("IsStop") ,//
                            new DataColumn("IsDone") ,
                            new DataColumn("PausedOn") ,
                            new DataColumn("PausedBy") ,
                            new DataColumn("ResumedOn"),
                            new DataColumn("ResumedBy"),
                            new DataColumn("StoppedOn") ,
                            new DataColumn("StoppedBy"),
                            new DataColumn("UpdateAmount"),
                            new DataColumn("UpdateAmountOn") ,

                            new DataColumn("UpdateAmountBy") ,
                            new DataColumn("CreatedOn") ,
                            new DataColumn("CreatedBy") ,
                            new DataColumn("ModifiedOn") ,
                            new DataColumn("ModifiedBy")

                            //new DataColumn("EmpName") ,//
                            //new DataColumn("EarningsName"),//
                            //new DataColumn("FinalDeduction")
                            });//

                        //for (int i = 0; i < UBP.EmpID.Length; i++)
                        //{
                        EmployeeEarningsList.Rows.Add(

                            UBP.EmpID[i],
                            UBP.AdjustmentTypeID[i],
                            UBP.OneTime[i],
                            UBP.AdjustedAmount[i],
                            UBP.StartDate,
                            UBP.EndDate,
                            "",
                            "",
                            "",
                            "",
                            "",
                            "",
                            "",
                            "",
                            "",
                            "",
                            "",
                            "",
                            "",
                            "",
                            "",
                            DateTime.Today,
                            "",
                            "",
                            ""



                            //UBP.EmpName[i],
                            //UBP.EarningsName[i],
                            //UBP.FinalDeduction[i]
                            );

                        //}

                        con.myparameters.Add(new myParameters { ParameterName = "@CompanyName", mytype = SqlDbType.VarChar, Value = UBP.CN });
                        con.myparameters.Add(new myParameters { ParameterName = "@IMPORT", mytype = SqlDbType.Structured, Value = EmployeeEarningsList });
                        con.ExecuteNonQuery("sp_UploadEmployeeEarnings");

                        //return id;




                        Connection con2 = new Connection();
                        DataTable EmployeeEarningsDetailsList = new DataTable();
                        EmployeeEarningsDetailsList.Columns.AddRange(new DataColumn[8] {
                            new DataColumn("EmployeeEarningsDetailsID"),
                            new DataColumn("EmployeeEarningsID") ,
                            new DataColumn("EarningsAmount") ,
                            new DataColumn("PayOutDate") ,
                            new DataColumn("ProcessedOn") ,
                            new DataColumn("ProcessedBy"),
                            new DataColumn("ClosedOn"),
                            new DataColumn("ClosedBy")});

                        String randomizedtext = Guid.NewGuid().ToString();

                        //for (int i = 0; i < UBP.EmpID.Length; i++)
                        //{
                        EmployeeEarningsDetailsList.Rows.Add(
                                randomizedtext,
                                (1 + finalid),
                                UBP.AdjustedAmount[i],
                                UBP.CreatedOn[0],
                                "",
                                "",
                                "",
                                "");

                        //}

                        con2.myparameters.Add(new myParameters { ParameterName = "@CompanyName", mytype = SqlDbType.VarChar, Value = UBP.CN });
                        con2.myparameters.Add(new myParameters { ParameterName = "@IMPORT", mytype = SqlDbType.Structured, Value = EmployeeEarningsDetailsList });
                        con2.ExecuteNonQuery("sp_UploadEmployeeEarningDetails");
                    }
                    else
                    {
                        PayrollVueClients pvc = new PayrollVueClients();
                        pvc.clientname = UBP.CN;
                        GetDBILMDevSvr(pvc);
                        DeductionEmployeeLastID(pvc);
                        String lastid = DeductionEmployeeLastID(pvc);
                        int finalid = Int32.Parse(lastid);

                        Connection con = new Connection();
                        DataTable DeductionEmployee = new DataTable();
                        DeductionEmployee.Columns.AddRange(new DataColumn[25] {
                            new DataColumn("EmpID"),//
                            new DataColumn("DeductionTypeID"),
                            new DataColumn("OneTime"),//
                            new DataColumn("DeductionAmount"),//

                            new DataColumn("StartDate"),
                            new DataColumn("EndDate"),
                            new DataColumn("NoEndDate"),
                            new DataColumn("PaymentTermsID"),
                            new DataColumn("IsPause"),//

                            new DataColumn("IsResume"),
                            new DataColumn("IsStop"),//
                            new DataColumn("IsDone"),
                            new DataColumn("UpdateAmount"),
                            new DataColumn("UpdateAmountOn"),

                            new DataColumn("UpdateAmountBy"),
                            new DataColumn("PausedOn"),
                            new DataColumn("PausedBy"),
                            new DataColumn("ResumedOn"),
                            new DataColumn("ResumedBy"),

                            new DataColumn("StoppedOn"),
                            new DataColumn("StoppedBy"),
                            new DataColumn("CreatedOn"),
                            new DataColumn("CreatedBy"),
                            new DataColumn("ModifiedOn"),

                            new DataColumn("ModifiedBy")
                            });

                        //for (int i = 0; i < UBP.EmpID.Length; i++)
                        //{
                        DeductionEmployee.Rows.Add(
                            UBP.EmpID[i],//
                            UBP.AdjustmentTypeID[i],
                            UBP.OneTime[i],//
                            UBP.AdjustedAmount[i],//

                            UBP.StartDate,
                            UBP.EndDate,
                            "",
                            "",
                            "",//

                            "",
                            "",//
                            "",
                            "",
                            "",

                            "",
                            "",
                            "",
                            "",
                            "",

                            "",
                            "",
                            DateTime.Today,
                            "",
                            "",

                            ""
                            );

                        //}

                        con.myparameters.Add(new myParameters { ParameterName = "@CompanyName", mytype = SqlDbType.VarChar, Value = UBP.CN });
                        con.myparameters.Add(new myParameters { ParameterName = "@IMPORT", mytype = SqlDbType.Structured, Value = DeductionEmployee });
                        con.ExecuteNonQuery("sp_UploadDeductionsEmployee");

                        String randomizedtext = Guid.NewGuid().ToString();



                        Connection con2 = new Connection();
                        DataTable DeductionEmployeeDetails = new DataTable();
                        DeductionEmployeeDetails.Columns.AddRange(new DataColumn[8] {
                            new DataColumn("EmployeeDeductionDetailsID") ,
                            new DataColumn("EmployeeDeductionID") ,
                            new DataColumn("DeductionAmount"),
                            new DataColumn("PayOutDate") ,
                            new DataColumn("ProcessedOn") ,
                            new DataColumn("ProcessedBy") ,
                            new DataColumn("ClosedOn") ,
                            new DataColumn("ClosedBy")});

                        //for (int i = 0; i < UBP.EmpID.Length; i++)
                        //{
                        DeductionEmployeeDetails.Rows.Add(
                            randomizedtext,
                            (1 + finalid),
                            UBP.AdjustedAmount[i],
                            UBP.CreatedOn[0],
                            "",
                            "",
                            "",
                            "");
                        //}

                        con2.myparameters.Add(new myParameters { ParameterName = "@CompanyName", mytype = SqlDbType.VarChar, Value = UBP.CN });
                        con2.myparameters.Add(new myParameters { ParameterName = "@IMPORT", mytype = SqlDbType.Structured, Value = DeductionEmployeeDetails });
                        con2.ExecuteNonQuery("sp_UploadEmployeeDeductionDetails");
                    }
                }





                return "Success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }



        [HttpPost]
        [Route("PayrolRegister")]
        public string PayrollRegister(PayrolRegister UBP)
        {
            try
            {
                PayrollVueClients pvc = new PayrollVueClients();
                pvc.clientname = UBP.CN;
                GetDBILMDevSvr(pvc);


                Connection con = new Connection();
                DataTable NetPayList = new DataTable();
                NetPayList.Columns.AddRange(new DataColumn[33] {



                new DataColumn("EmployeeNetPayID"),//
                new DataColumn("EmpID"),//
                new DataColumn("PayoutSchemeID"),
                new DataColumn("PaymentTermsID"),
                new DataColumn("PayOutDate"),
                new DataColumn("BasicPay"),

                new DataColumn("HoursWorkedAmount"),
                new DataColumn("TardinessAmount"),//
                new DataColumn("AbsencesAmount"),
                new DataColumn("UndertimeAmount"),//
                new DataColumn("LWOP"),//

                new DataColumn("EfficiencyRate"),//
                new DataColumn("GrossPay"),//
                new DataColumn("TaxableIncome"),//
                new DataColumn("OvertimeGrossPay"),
                new DataColumn("SSSAmount"),

                new DataColumn("PhilHealthAmount"),
                new DataColumn("PagIbigAmount"),
                new DataColumn("EarningsTotal"),
                new DataColumn("EarningsGrossPay"),
                new DataColumn("EarningsTaxableAmount"),

                new DataColumn("EarningsNonTaxableAmount"),
                new DataColumn("EarningsTaxableBonus"),
                new DataColumn("EarningsNonTaxableBonus"),
                new DataColumn("DeductionsTotal"),
                new DataColumn("DeductionsGrossPay"),

                new DataColumn("DeductionsTaxableAmount"),
                new DataColumn("DeductionsNonTaxableAmount"),
                new DataColumn("DeductionsTaxableBonus"),
                new DataColumn("DeductionsNonTaxableBonus"),
                new DataColumn("TotalLoanAmount"),

                new DataColumn("WHTax"),
                new DataColumn("NetPay")//

                });



                for (int i = 0; i < UBP.NPEmpID.Length; i++)
                {
                    String randomizedtext = Guid.NewGuid().ToString();
                    NetPayList.Rows.Add(
                        randomizedtext,//
                        UBP.NPEmpID[i],//
                        "",
                        "",
                        UBP.NPPayOutDate[0],
                        UBP.NPBasicPay[i],

                        "",
                        UBP.NPTardinessAmount[i],//
                        "",
                        UBP.NPUndertimeAmount[i],//
                        UBP.NPLWOP[i],//

                        "",
                        UBP.NPGrossPay[i],//
                        UBP.NPTaxableIncome[i],//
                        "",
                        "",

                        "",
                        "",
                        "",
                        "",
                        "",

                        "",
                        "",
                        "",
                        "",
                        "",

                        "",
                        "",
                        "",
                        "",
                        "",

                        UBP.NPWHTax[i],
                        UBP.NPNetPay[i]//
                        );

                }

                con.myparameters.Add(new myParameters { ParameterName = "@CompanyName", mytype = SqlDbType.VarChar, Value = UBP.CN });
                con.myparameters.Add(new myParameters { ParameterName = "@IMPORT", mytype = SqlDbType.Structured, Value = NetPayList });
                con.ExecuteNonQuery("sp_UploadEmployeeNetpay");

                for (int i = 0; i < UBP.ELEmpID.Length; i++)
                {
                    String randomizedtext = Guid.NewGuid().ToString();
                    var rdtext = randomizedtext;

                    //DateTime pdate = new DateTime(UBP.ELDPayOutDate[]);
                    //DateTime sdate = new DateTime(UBP.ELCutOffStartDate);
                    //DateTime edate = new DateTime(UBP.ELCutOffEndDate);

                    GetDBILMDevSvr(pvc);
                    Connection con2 = new Connection();
                    DataTable EmployeeLogHoursList = new DataTable();
                    EmployeeLogHoursList.Columns.AddRange(new DataColumn[23] {

                new DataColumn("EmployeeLogHoursID"),
                new DataColumn("EmpID"),
                new DataColumn("Basicpay"),
                new DataColumn("PayOutSchemeID"),
                new DataColumn("HoursWorked"),

                new DataColumn("Tardiness"),
                new DataColumn("Absences"),
                new DataColumn("Undertime"),
                new DataColumn("LWOP"),
                new DataColumn("HourlyRate"),

                new DataColumn("EfficiencyRate"),
                new DataColumn("PaymentTermsID"),
                new DataColumn("PayOutDate"),
                new DataColumn("IsDone"),
                new DataColumn("IsAdded"),

                new DataColumn("AddedOn"),
                new DataColumn("AddedBy"),
                new DataColumn("ProcessedOn"),
                new DataColumn("ProcessedBy"),
                new DataColumn("ClosedOn"),

                new DataColumn("ClosedBy"),
                new DataColumn("CutOffStartDate"),
                new DataColumn("CutOffEndDate")});




                    EmployeeLogHoursList.Rows.Add(
                        rdtext,//
                        UBP.ELEmpID[i],//
                        UBP.ELBasicpay[i],//
                        "",
                        "",

                        UBP.ELTardiness[i],//
                        UBP.ELAbsences[i],//
                        UBP.ELUndertime[i],//
                        UBP.ELLWOP[i],//
                        UBP.ELHourlyRate[i],//

                        "",
                        "",
                        UBP.ELPayOutDate[0],
                        "",
                        "",

                        "",
                        "",
                        "",
                        "",
                        "",

                        "",
                        UBP.ELCutOffStartDate[0],
                        UBP.ELCutOffEndDate[0]
                        );



                    con2.myparameters.Add(new myParameters { ParameterName = "@CompanyName", mytype = SqlDbType.VarChar, Value = UBP.CN });
                    con2.myparameters.Add(new myParameters { ParameterName = "@IMPORT", mytype = SqlDbType.Structured, Value = EmployeeLogHoursList });
                    con2.ExecuteNonQuery("sp_UploadEmployeeLogHours");

                    GetDBILMDevSvr(pvc);
                    Connection con3 = new Connection();
                    DataTable EmployeeLogHoursDetailsList = new DataTable();
                    EmployeeLogHoursDetailsList.Columns.AddRange(new DataColumn[11] {

                    new DataColumn("EmployeeLogHoursDetailsID"),
                    new DataColumn("EmployeeLogHoursID"),
                    new DataColumn("HoursworkedAmount"),
                    new DataColumn("TardinessAmount"),
                    new DataColumn("AbsencesAmount"),
                    new DataColumn("UndertimeAmount"),

                    new DataColumn("LWOPAmount"),
                    new DataColumn("PaymentTermsID"),
                    new DataColumn("PayOutDate"),
                    new DataColumn("ProcessedOn"),
                    new DataColumn("ProcessedBy")});


                    EmployeeLogHoursDetailsList.Rows.Add(
                        rdtext,//
                        rdtext,//
                        "",
                        UBP.ELDTardinessAmount[i],//
                        UBP.ELDAbsencesAmount[i],//
                        UBP.ELDUndertimeAmount[i],//
                        UBP.ELDLWOPAmount[i],//
                        "",
                        UBP.ELDPayOutDate[0],
                        "",
                        ""
                        );



                    con3.myparameters.Add(new myParameters { ParameterName = "@CompanyName", mytype = SqlDbType.VarChar, Value = UBP.CN });
                    con3.myparameters.Add(new myParameters { ParameterName = "@IMPORT", mytype = SqlDbType.Structured, Value = EmployeeLogHoursDetailsList });
                    con3.ExecuteNonQuery("sp_UploadEmployeeLogHoursDetails");

                }
                GetDBILMDevSvr(pvc);
                Connection con4 = new Connection();
                DataTable EmployeePagibigDetails = new DataTable();
                EmployeePagibigDetails.Columns.AddRange(new DataColumn[9] {

                new DataColumn("EmployeePagIbigID"),
                new DataColumn("EmpID"),
                new DataColumn("GrossPay"),
                new DataColumn("PagIbigAmount"),
                new DataColumn("PagIbigEmployerAmount"),
                new DataColumn("PaymentTermsID"),
                new DataColumn("PayOutDate"),
                new DataColumn("ProcessedOn"),
                new DataColumn("ProcessedBy")});

                for (int i = 0; i < UBP.EPIEmpID.Length; i++)
                {
                    String randomizedtext = Guid.NewGuid().ToString();
                    EmployeePagibigDetails.Rows.Add(

                        randomizedtext,//
                        UBP.EPIEmpID[i],//
                        UBP.EPIGrossPay[i],//
                        UBP.EPIPagIbigAmount[i],//
                        UBP.EPIPagIbigEmployerAmount[i],//
                        "",
                        UBP.EPIPayOutDate[0],
                        "",
                        ""
                        );

                }

                con4.myparameters.Add(new myParameters { ParameterName = "@CompanyName", mytype = SqlDbType.VarChar, Value = UBP.CN });
                con4.myparameters.Add(new myParameters { ParameterName = "@IMPORT", mytype = SqlDbType.Structured, Value = EmployeePagibigDetails });
                con4.ExecuteNonQuery("sp_UploadEmployeePAGIBIGDetails");

                GetDBILMDevSvr(pvc);
                Connection con5 = new Connection();
                DataTable EmployeePHILHEALTHDetails = new DataTable();
                EmployeePHILHEALTHDetails.Columns.AddRange(new DataColumn[9] {

                new DataColumn("EmployeePhilHealthID"),
                new DataColumn("EmpID"),
                new DataColumn("GrossPay"),
                new DataColumn("PhilHealthAmount"),
                new DataColumn("PhilHealthEmployerAmount"),
                new DataColumn("PaymentTermsID"),
                new DataColumn("PayOutDate"),
                new DataColumn("ProcessedOn"),
                new DataColumn("ProcessedBy") });

                for (int i = 0; i < UBP.EPHEmpID.Length; i++)
                {
                    String randomizedtext = Guid.NewGuid().ToString();
                    EmployeePHILHEALTHDetails.Rows.Add(
                        randomizedtext,//
                        UBP.EPHEmpID[i],//
                        UBP.EPHGrossPay[i],//
                        UBP.EPHPhilHealthAmount[i],//
                        UBP.EPHPhilHealthEmployerAmount[i],//
                        "",
                        UBP.EPHPayOutDate[0],
                        "",
                        ""
                        );

                }

                con5.myparameters.Add(new myParameters { ParameterName = "@CompanyName", mytype = SqlDbType.VarChar, Value = UBP.CN });
                con5.myparameters.Add(new myParameters { ParameterName = "@IMPORT", mytype = SqlDbType.Structured, Value = EmployeePHILHEALTHDetails });
                con5.ExecuteNonQuery("sp_UploadEmployeePHILHEALTHDetails");

                GetDBILMDevSvr(pvc);
                Connection con6 = new Connection();
                DataTable EmployeeSSSDetails = new DataTable();
                EmployeeSSSDetails.Columns.AddRange(new DataColumn[10] {
                new DataColumn("EmployeeSSSID"),
                new DataColumn("EmpID"),
                new DataColumn("GrossPay"),
                new DataColumn("SSSAmount"),
                new DataColumn("SSSEmployerAmount"),
                new DataColumn("ECAmount"),
                new DataColumn("PaymentTermsID"),
                new DataColumn("PayOutDate"),
                new DataColumn("ProcessedOn"),
                new DataColumn("ProcessedBy")});

                for (int i = 0; i < UBP.SSSEmpID.Length; i++)
                {
                    String randomizedtext = Guid.NewGuid().ToString();
                    EmployeeSSSDetails.Rows.Add(
                        randomizedtext,//
                        UBP.SSSEmpID[i],//
                        UBP.SSSGrossPay[i],//
                        UBP.SSSSSSAmount[i],//
                        UBP.SSSSSSEmployerAmount[i],//
                        UBP.SSSECAmount[i],//
                        "",
                        UBP.SSSPayOutDate[0],
                        "",
                        ""
                        );

                }

                con6.myparameters.Add(new myParameters { ParameterName = "@CompanyName", mytype = SqlDbType.VarChar, Value = UBP.CN });
                con6.myparameters.Add(new myParameters { ParameterName = "@IMPORT", mytype = SqlDbType.Structured, Value = EmployeeSSSDetails });
                con6.ExecuteNonQuery("sp_UploadEmployeeSSSDetails");



                GetDBILMDevSvr(pvc);
                Connection con7 = new Connection();


                for (var i = 0; i < UBP.EOEmpID.Length; i++)
                {
                    DataTable EmployeeOvertimeList = new DataTable();
                    EmployeeOvertimeList.Columns.AddRange(new DataColumn[16] {

                        new DataColumn("EmployeeOvertimeID"),
                        new DataColumn("EmpID"),
                        new DataColumn("OvertimeID"),
                        new DataColumn("OvertimeHours"),
                        new DataColumn("CompanyRate"),
                        new DataColumn("HourlyRate"),

                        new DataColumn("PaymentTermsID"),
                        new DataColumn("PayOutDate"),
                        new DataColumn("IsDone"),
                        new DataColumn("IsAdded"),
                        new DataColumn("AddedOn"),

                        new DataColumn("AddedBy"),
                        new DataColumn("ProcessedOn"),
                        new DataColumn("ProcessedBy"),
                        new DataColumn("ClosedOn"),
                        new DataColumn("ClosedBy") });


                    String randomizedtext = Guid.NewGuid().ToString();
                    EmployeeOvertimeList.Rows.Add(
                        randomizedtext,
                        UBP.EOEmpID[i],
                        UBP.EOOvertimeID[i],
                        UBP.EOOvertimeHours[i],
                        UBP.EOCompanyRate[i],
                        "",

                        "",
                        UBP.EOPayOutDate[0],
                        "",
                        "",
                        "",

                        "",
                        DateTime.Today,
                        "",
                        "",
                        "");


                    con7.myparameters.Clear();
                    con7.myparameters.Add(new myParameters { ParameterName = "@CompanyName", mytype = SqlDbType.VarChar, Value = UBP.CN });
                    con7.myparameters.Add(new myParameters { ParameterName = "@IMPORT", mytype = SqlDbType.Structured, Value = EmployeeOvertimeList });
                    con7.ExecuteNonQuery("sp_UploadEmployeeOvertime");


                    GetDBILMDevSvr(pvc);
                    Connection con8 = new Connection();
                    DataTable EmployeeOvertimeDetailsList = new DataTable();
                    EmployeeOvertimeDetailsList.Columns.AddRange(new DataColumn[7] {

                        new DataColumn("EmployeeOvertimeDetailsID"),
                        new DataColumn("EmployeeOvertimeID"),
                        new DataColumn("OvertimeAmount"),
                        new DataColumn("PaymentTermsID"),
                        new DataColumn("PayOutDate"),
                        new DataColumn("ProcessedOn"),

                        new DataColumn("ProcessedBy")});


                    EmployeeOvertimeDetailsList.Rows.Add(
                        randomizedtext,
                        randomizedtext,
                        UBP.EOOvertimeAmount[i],
                        "",
                        UBP.EOPayOutDate[0],
                        DateTime.Today,
                        "");


                    con8.myparameters.Clear();
                    con8.myparameters.Add(new myParameters { ParameterName = "@CompanyName", mytype = SqlDbType.VarChar, Value = UBP.CN });
                    con8.myparameters.Add(new myParameters { ParameterName = "@IMPORT", mytype = SqlDbType.Structured, Value = EmployeeOvertimeDetailsList });
                    con8.ExecuteNonQuery("sp_UploadEmployeeOvertimeDetails");

                }
                if (UBP.CN == "Octacrom")
                {
                    GetDBILMDevSvr(pvc);
                    Connection con9 = new Connection();
                    con9.myparameters.Clear();
                    con9.myparameters.Add(new myParameters { ParameterName = "@PayOuDate", mytype = SqlDbType.VarChar, Value = UBP.payoutdate });
                    con9.myparameters.Add(new myParameters { ParameterName = "@CutOffStarDate", mytype = SqlDbType.VarChar, Value = UBP.cutoffstart });
                    con9.myparameters.Add(new myParameters { ParameterName = "@CutOffEndDate", mytype = SqlDbType.VarChar, Value = UBP.cutoffend });
                    con9.ExecuteNonQuery("sp_UploadEmployeeLogHours_Alternative");
                }


                return "Success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }




    }
}