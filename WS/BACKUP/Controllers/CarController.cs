﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using illimitadoWepAPI.Models;
using System.Xml.Serialization;
using System.IO;
using System.Data;
using System.Data.SqlClient;
using illimitadoWepAPI.MyClass;

namespace illimitadoWepAPI.Controllers
{
    public class CarController : ApiController
    {

        [HttpPost]
        [Route("getDetails")]
        public carlistfinal CDetails()
        {
            carlistfinal res = new carlistfinal();
            Connection con = new Connection();
            DataTable dt = new DataTable();
            dt = con.GetDataTable("sp_Get_CarDetails");
            List<getCarDetails> final = new List<getCarDetails>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                getCarDetails temp = new getCarDetails();
                temp.ID = dt.Rows[i]["ID"].ToString();
                temp.brandname = dt.Rows[i]["brand"].ToString();
                temp.modelname = dt.Rows[i]["model"].ToString();
                temp.variantname = dt.Rows[i]["variant"].ToString();
                temp.type = dt.Rows[i]["type"].ToString();
                final.Add(temp);
            }
            res.CarResult = final;
            res.myreturn = "success";
            return res;
        }
        [HttpPost]
        [Route("getBankDetails")]
        public banklistfinal getBanks()
        {
            banklistfinal res = new banklistfinal();
            Connection con = new Connection();
            DataTable dt = new DataTable();
            dt = con.GetDataTable("sp_getBankNames");
            List<BankList> final = new List<BankList>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                BankList temp = new BankList();
                temp.BankID = dt.Rows[i]["ID"].ToString();
                temp.BankName = dt.Rows[i]["BankName"].ToString();
                final.Add(temp);
            }
            res.BankResult = final;
            res.myreturn = "success";
            return res;
        }
        #region 2018-09-07


        [HttpPost]
        [Route("GetDHRBrandDetails")]
        public BrandList GetDHRBrandDetails(ListResult LR)
        {
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = "ILMDEV";
            SelfieRegistration2Controller.GetDB2(GDB);


            BrandList res = new BrandList();
            Connection con = new Connection();
            DataTable dt = new DataTable();
            con.myparameters.Add(new myParameters { ParameterName = "@TYPE", mytype = SqlDbType.NVarChar, Value = LR.TYPE });
            con.myparameters.Add(new myParameters { ParameterName = "@COUNT", mytype = SqlDbType.NVarChar, Value = "1" });
            con.myparameters.Add(new myParameters { ParameterName = "@BRAND", mytype = SqlDbType.NVarChar, Value = "0" });
            con.myparameters.Add(new myParameters { ParameterName = "@MODEL", mytype = SqlDbType.NVarChar, Value = "0" });

            dt = con.GetDataTable("sp_GetDHRVehicleDetails");

            List<GetBrand> final = new List<GetBrand>();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                GetBrand temp = new GetBrand();

                temp.Brand = dt.Rows[i]["brand"].ToString();
                final.Add(temp);
            }
            res.Brand = final;
            res.myreturn = "success";
            return res;
        }





        [HttpPost]
        [Route("GetDHRModelDetails")]
        public ModelList GetDHRModelDetails(ListResult LR)
        {
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = "ILMDEV";
            SelfieRegistration2Controller.GetDB2(GDB);


            ModelList res = new ModelList();
            Connection con = new Connection();
            DataTable dt = new DataTable();
            con.myparameters.Add(new myParameters { ParameterName = "@TYPE", mytype = SqlDbType.NVarChar, Value = LR.TYPE });
            con.myparameters.Add(new myParameters { ParameterName = "@COUNT", mytype = SqlDbType.NVarChar, Value = "2" });
            con.myparameters.Add(new myParameters { ParameterName = "@BRAND", mytype = SqlDbType.NVarChar, Value = LR.BRAND });
            con.myparameters.Add(new myParameters { ParameterName = "@MODEL", mytype = SqlDbType.NVarChar, Value = "0" });

            dt = con.GetDataTable("sp_GetDHRVehicleDetails");

            List<GetModel> final = new List<GetModel>();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                GetModel temp = new GetModel();

                temp.Model = dt.Rows[i]["model"].ToString();
                final.Add(temp);
            }
            res.Model = final;
            res.myreturn = "success";
            return res;
        }



        [HttpPost]
        [Route("GetDHRVariantDetails")]
        public VariantList GetDHRVariantDetails(ListResult LR)
        {
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = "ILMDEV";
            SelfieRegistration2Controller.GetDB2(GDB);


            VariantList res = new VariantList();
            Connection con = new Connection();
            DataTable dt = new DataTable();
            con.myparameters.Add(new myParameters { ParameterName = "@TYPE", mytype = SqlDbType.NVarChar, Value = LR.TYPE });
            con.myparameters.Add(new myParameters { ParameterName = "@COUNT", mytype = SqlDbType.NVarChar, Value = "3" });
            con.myparameters.Add(new myParameters { ParameterName = "@BRAND", mytype = SqlDbType.NVarChar, Value = LR.BRAND });
            con.myparameters.Add(new myParameters { ParameterName = "@MODEL", mytype = SqlDbType.NVarChar, Value = LR.MODEL });

            dt = con.GetDataTable("sp_GetDHRVehicleDetails");

            List<GetVariant> final = new List<GetVariant>();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                GetVariant temp = new GetVariant();

                temp.Variant = dt.Rows[i]["variant"].ToString();
                final.Add(temp);
            }
            res.Variant = final;
            res.myreturn = "success";
            return res;
        }
        #endregion
    }

}