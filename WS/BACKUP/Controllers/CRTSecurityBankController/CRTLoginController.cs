﻿using System;
using System.Web.Http;
using illimitadoWepAPI.Models.CRTSecurityBankModel;
using System.Data;
using illimitadoWepAPI.MyClass;
using System.Configuration;
using System.Reflection;
using System.Collections.Generic;
using ExpertXls.ExcelLib;
using System.IO;
using System.Web.Hosting;
using static illimitadoWepAPI.Models.AlphaListModel;
using static illimitadoWepAPI.Models.UserProfile;
using System.Web;
using System.Net.Mail;
using System.Net;
using iTextSharp.text.pdf;
using iTextSharp.text;
using System.Globalization;
using System.Net.Http;
using System.Text.RegularExpressions;
using Excel = Microsoft.Office.Interop.Excel;
using System.Runtime.InteropServices;
using System.Diagnostics;
using Renci.SshNet;
using Renci.SshNet.Sftp;
using System.Data.OleDb;

namespace illimitadoWepAPI.Controllers.CRTSecurityBankController
{
    public class CRTLoginController : ApiController
    {
        public static string GetDB2(GetDatabase GetDatabase)
        {
            var settings = ConfigurationManager.ConnectionStrings["CRT_SecurityBankDBconnectionstring"]; var fi = typeof(ConfigurationElement).GetField(
               "_bReadOnly",
               BindingFlags.Instance | BindingFlags.NonPublic);
            fi.SetValue(settings, false);
            settings.ConnectionString = "Data Source=livedb-ilm.c3srcolvclfy.ap-southeast-1.rds.amazonaws.com,1412;Database=ILM_Main;User Id=ILM_LiveDBUser;Password=ILMV3ry53cur3dMGTpa55w0rd2020;";



            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@ClientName", mytype = SqlDbType.NVarChar, Value = GetDatabase.ClientName });
            DataRow dr;
            dr = Connection.GetSingleRow("sp_CheckClientDB");

            GetDatabase dab = new GetDatabase
            {
                ClientName = dr["DB_Settings"].ToString()
            };

            if (dab.ClientName == "ILM_Live")
            {
                settings.ConnectionString = "Data Source=livedb-ilm.c3srcolvclfy.ap-southeast-1.rds,.amazonaws.com,1412;Database=ILM_Live;User Id=ILM_LiveDBUser;Password=ILMV3ry53cur3dMGTpa55w0rd2020;";
            }
            else if (dab.ClientName == "StarDB")
            {
                settings.ConnectionString = "Data Source=livedb-ilm.c3srcolvclfy.ap-southeast-1.rds.amazonaws.com,1412;Database=StarDB;User Id=ILM_LiveDBUser;Password=ILMV3ry53cur3dMGTpa55w0rd2020;";
            }
            else if (dab.ClientName == "PetBoweDB")
            {
                settings.ConnectionString = "Data Source=tcp:ilmuat.c3srcolvclfy.ap-southeast-1.rds.amazonaws.com,1433;Database=PetBoweDB;User Id=ILMUATSA;Password=ILMV3ry53cur3dMGTpa55w0rd2020;";
            }
            else if (dab.ClientName == "UnionBankDB")
            {
                settings.ConnectionString = "Data Source=tcp:ilmuat.c3srcolvclfy.ap-southeast-1.rds.amazonaws.com,1433;Database=UnionBankDB;User Id=ILMUATSA;Password=ILMV3ry53cur3dMGTpa55w0rd2020;";
            }
            else if (dab.ClientName == "ILM_UAT")
            {
                settings.ConnectionString = "Data Source=tcp:ilmuat.c3srcolvclfy.ap-southeast-1.rds.amazonaws.com,1433;Database=ILM_UAT;User Id=ILMUATSA;Password=ILMV3ry53cur3dMGTpa55w0rd2020;";
            }
            else
            {
                //settings.ConnectionString = "Data Source=livedb-ilm.c3srcolvclfy.ap-southeast-1.rds.amazonaws.com,1412;Database=" + dab.ClientName + ";User Id=ILM_LiveDBUser;Password=ILMV3ry53cur3dMGTpa55w0rd2020;";
                //settings.ConnectionString = "Data Source=192.168.2.33,1444;Database=" + dab.ClientName + ";User Id=DevUser;Password=securedmgt@1230;";
                settings.ConnectionString = "Data Source=192.168.2.33,1444;Database=CRT_Dbase2019-03-29;User Id=ILM_LiveDBUser;Password=ILMV3ry53cur3dMGTpa55w0rd2020;";
            }

            return settings.ToString();
        }

        public void tempconnectionstring()
        {
            var settings = ConfigurationManager.ConnectionStrings["connectionstring"];
            var fi = typeof(ConfigurationElement).GetField(
                          "_bReadOnly",
                          BindingFlags.Instance | BindingFlags.NonPublic);
            fi.SetValue(settings, false);
            //--Local--//
            //settings.ConnectionString = "Data Source=192.168.2.33,1444;Database=CRT_Dbase2019-03-29;User Id=DevUser;Password=securedmgt@1230;";
            //--AWS--//
            settings.ConnectionString = "Data Source=livedb-ilm.c3srcolvclfy.ap-southeast-1.rds.amazonaws.com,1412; Database = CRT_Dbase;User Id=ILM_LiveDBUser;Password=ILMV3ry53cur3dMGTpa55w0rd2020;";

        }
        public void tempconnectionstringv2()
        {
            var settings = ConfigurationManager.ConnectionStrings["connectionstring"];
            var fi = typeof(ConfigurationElement).GetField(
                          "_bReadOnly",
                          BindingFlags.Instance | BindingFlags.NonPublic);
            fi.SetValue(settings, false);
            //--Local--//
            //settings.ConnectionString = "Data Source=192.168.2.33,1444;Database=CRT_Dbase2019-03-29;User Id=DevUser;Password=securedmgt@1230;";
            //--AWS--//
            settings.ConnectionString = "Data Source=livedb-ilm.c3srcolvclfy.ap-southeast-1.rds.amazonaws.com,1412; Database = CRT_DbaseUAT;User Id=ILM_LiveDBUser;Password=ILMV3ry53cur3dMGTpa55w0rd2020;";

        }
        //Login (Eto yung sa login)
        [HttpPost]
        [Route("CRTLogin")]
        public string CRTLoginContainer(CRTLogin login)
        {
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = login.CN;
            tempconnectionstring();

            try
            {
                string valid = "";
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@CN", mytype = SqlDbType.NVarChar, Value = login.CN });
                con.myparameters.Add(new myParameters { ParameterName = "@UserID", mytype = SqlDbType.NVarChar, Value = login.UserID });
                con.myparameters.Add(new myParameters { ParameterName = "@Password", mytype = SqlDbType.NVarChar, Value = login.Password });
                valid = con.ExecuteScalar("sp_CRT_Login").ToString();
                return valid;
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }
        [HttpPost]
        [Route("CRT_InsertClientData")]
        public string CRT_InsertClientData(InsertClientData ClientData)
        {
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = ClientData.CN;
            tempconnectionstring();
            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@CN", mytype = SqlDbType.NVarChar, Value = ClientData.CN });
            Connection.myparameters.Add(new myParameters { ParameterName = "@UserName", mytype = SqlDbType.NVarChar, Value = ClientData.UserName });
            Connection.myparameters.Add(new myParameters { ParameterName = "@CompanyAlias", mytype = SqlDbType.NVarChar, Value = ClientData.CompanyAlias });
            Connection.myparameters.Add(new myParameters { ParameterName = "@DateofIncorporation", mytype = SqlDbType.NVarChar, Value = ClientData.DateofIncorporation });
            Connection.myparameters.Add(new myParameters { ParameterName = "@AddressFloor", mytype = SqlDbType.NVarChar, Value = ClientData.AddressFloor });
            Connection.myparameters.Add(new myParameters { ParameterName = "@AddressBuilding", mytype = SqlDbType.NVarChar, Value = ClientData.AddressBuilding });
            Connection.myparameters.Add(new myParameters { ParameterName = "@AddressStreet", mytype = SqlDbType.NVarChar, Value = ClientData.AddressStreet });
            Connection.myparameters.Add(new myParameters { ParameterName = "@AddressBrgy", mytype = SqlDbType.NVarChar, Value = ClientData.AddressBrgy });
            Connection.myparameters.Add(new myParameters { ParameterName = "@AddressMunicipality", mytype = SqlDbType.NVarChar, Value = ClientData.AddressMunicipality });
            Connection.myparameters.Add(new myParameters { ParameterName = "@AddressProvince", mytype = SqlDbType.NVarChar, Value = ClientData.AddressProvince });
            Connection.myparameters.Add(new myParameters { ParameterName = "@AddressRegion", mytype = SqlDbType.NVarChar, Value = ClientData.AddressRegion });
            Connection.myparameters.Add(new myParameters { ParameterName = "@AddressCountry", mytype = SqlDbType.NVarChar, Value = ClientData.AddressCountry });
            Connection.myparameters.Add(new myParameters { ParameterName = "@AddressZipCode", mytype = SqlDbType.NVarChar, Value = ClientData.AddressZipCode });
            Connection.myparameters.Add(new myParameters { ParameterName = "@NationalityCountry", mytype = SqlDbType.NVarChar, Value = ClientData.NationalityCountry });
            Connection.myparameters.Add(new myParameters { ParameterName = "@CompanyEQCustomerNumber", mytype = SqlDbType.NVarChar, Value = ClientData.CompanyEQCustomerNumber });
            Connection.myparameters.Add(new myParameters { ParameterName = "@TotalNumberOfEmployees", mytype = SqlDbType.NVarChar, Value = ClientData.TotalNumberOfEmployees });
            Connection.myparameters.Add(new myParameters { ParameterName = "@AccountType", mytype = SqlDbType.NVarChar, Value = ClientData.AccountType });
            //Connection.myparameters.Add(new myParameters { ParameterName = "@ContactType", mytype = SqlDbType.NVarChar, Value = ClientData.ContactType });
            Connection.myparameters.Add(new myParameters { ParameterName = "@ContactNumber", mytype = SqlDbType.NVarChar, Value = ClientData.ContactNumber });
            //Connection.myparameters.Add(new myParameters { ParameterName = "@EmailType", mytype = SqlDbType.NVarChar, Value = ClientData.EmailType });
            Connection.myparameters.Add(new myParameters { ParameterName = "@EmailAddress", mytype = SqlDbType.NVarChar, Value = ClientData.EmailAddress });
            //Connection.myparameters.Add(new myParameters { ParameterName = "@Website", mytype = SqlDbType.NVarChar, Value = ClientData.Website });
            //Connection.myparameters.Add(new myParameters { ParameterName = "@SSS", mytype = SqlDbType.NVarChar, Value = ClientData.SSS });
            //Connection.myparameters.Add(new myParameters { ParameterName = "@PhilHealth", mytype = SqlDbType.NVarChar, Value = ClientData.PhilHealth });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Tin", mytype = SqlDbType.NVarChar, Value = ClientData.Tin });
            //Connection.myparameters.Add(new myParameters { ParameterName = "@TinRDO", mytype = SqlDbType.NVarChar, Value = ClientData.TinRDO });
            //Connection.myparameters.Add(new myParameters { ParameterName = "@PagIbig", mytype = SqlDbType.NVarChar, Value = ClientData.PagIbig });
            //Connection.myparameters.Add(new myParameters { ParameterName = "@PagibigBranch", mytype = SqlDbType.NVarChar, Value = ClientData.PagibigBranch });
            Connection.myparameters.Add(new myParameters { ParameterName = "@FirstName", mytype = SqlDbType.NVarChar, Value = ClientData.FirstName });
            Connection.myparameters.Add(new myParameters { ParameterName = "@MiddleName", mytype = SqlDbType.NVarChar, Value = ClientData.MiddleName });
            Connection.myparameters.Add(new myParameters { ParameterName = "@LastName", mytype = SqlDbType.NVarChar, Value = ClientData.LastName });
            Connection.myparameters.Add(new myParameters { ParameterName = "@RepresentativeTitle", mytype = SqlDbType.NVarChar, Value = ClientData.RepresentativeTitle });
            Connection.myparameters.Add(new myParameters { ParameterName = "@RepresentativeContactNumber", mytype = SqlDbType.NVarChar, Value = ClientData.RepresentativeContactNumber });
            Connection.myparameters.Add(new myParameters { ParameterName = "@isDeleted", mytype = SqlDbType.NVarChar, Value = ClientData.isDeleted });
            Connection.myparameters.Add(new myParameters { ParameterName = "@CreatedById", mytype = SqlDbType.NVarChar, Value = ClientData.CreatedById });
            Connection.myparameters.Add(new myParameters { ParameterName = "@PDF_Status", mytype = SqlDbType.NVarChar, Value = ClientData.PDF_Status });

            Connection.ExecuteNonQuery("sp_CRT_AddClient");
            return "success";
        }

        [HttpPost]
        [Route("ShowCrt_CLientList")]
        public GetClientDetails GetAllClient(InsertClientData admin)
        {
            GetClientDetails GetClientDetails = new GetClientDetails();
            List<ClientDetails> ClientDetails = new List<ClientDetails>();
            tempconnectionstring();
            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@CN", mytype = SqlDbType.NVarChar, Value = admin.CN });

            DataTable dtClientList = new DataTable();
            dtClientList = Connection.GetDataTable("sp_ClientList");
            if (dtClientList.Rows.Count > 0)
            {
                foreach (DataRow row in dtClientList.Rows)
                {
                    ClientDetails Clientdetail = new ClientDetails
                    {
                        ClientId = row["ClientId"].ToString(),
                        CN = row["CompanyName"].ToString(),
                        UserName = row["UserName"].ToString(),
                        CompanyAlias = row["CompanyAlias"].ToString(),
                        DateofIncorporation = row["DateofIncorporation"].ToString(),
                        AddressFloor = row["Floor"].ToString(),
                        AddressBuilding = row["Building"].ToString(),
                        AddressStreet = row["Street"].ToString(),
                        AddressBrgy = row["Barangay"].ToString(),
                        AddressMunicipality = row["Municipality"].ToString(),
                        AddressProvince = row["Province"].ToString(),
                        AddressRegion = row["Region"].ToString(),
                        AddressCountry = row["Country"].ToString(),
                        AddressZipCode = row["ZipCode"].ToString(),
                        NationalityCountry = row["NationalityCountry"].ToString(),
                        CompanyEQCustomerNumber = row["CompanyEQCustomerNumber"].ToString(),
                        TotalNumberOfEmployees = row["TotalNumberOfEmployees"].ToString(),
                        AccountType = row["AccountType"].ToString(),
                        ContactType = row["ContactType"].ToString(),
                        ContactNumber = row["ContactNumber"].ToString(),
                        EmailType = row["EmailType"].ToString(),
                        EmailAddress = row["Email"].ToString(),
                        Website = row["Website"].ToString(),
                        SSS = row["SSS"].ToString(),
                        PhilHealth = row["Philhealth"].ToString(),
                        Tin = row["TIN"].ToString(),
                        TinRDO = row["TINRdo"].ToString(),
                        PagIbig = row["PagIbig"].ToString(),
                        PagibigBranch = row["PagibigBranch"].ToString(),
                        FirstName = row["First_Name"].ToString(),
                        MiddleName = row["Middle_Name"].ToString(),
                        LastName = row["Last_Name"].ToString(),
                        RepresentativeTitle = row["Representative_Title"].ToString(),
                        RepresentativeContactNumber = row["Representative_Contact_Number"].ToString(),
                        isDeleted = row["isDeleted"].ToString()
                    };
                    ClientDetails.Add(Clientdetail);
                }
                GetClientDetails.Clientdetails = ClientDetails;
                GetClientDetails.myreturn = "Success";
            }
            else
            {
                GetClientDetails.myreturn = "Error";
            }
            return GetClientDetails;
        }

        [HttpPost]
        [Route("CRT_UpdateClientData")]
        public string CRT_UpdateClientData(UpdateClientData ClientData)
        {
            try
            {
                tempconnectionstring();
                Connection Connection = new Connection();

                Connection.myparameters.Add(new myParameters { ParameterName = "@ClientId", mytype = SqlDbType.NVarChar, Value = ClientData.ClientId });
                Connection.myparameters.Add(new myParameters { ParameterName = "@CN", mytype = SqlDbType.NVarChar, Value = ClientData.CN });
                Connection.myparameters.Add(new myParameters { ParameterName = "@UserName", mytype = SqlDbType.NVarChar, Value = ClientData.UserName });
                Connection.myparameters.Add(new myParameters { ParameterName = "@CompanyAlias", mytype = SqlDbType.NVarChar, Value = ClientData.CompanyAlias });
                Connection.myparameters.Add(new myParameters { ParameterName = "@DateofIncorporation", mytype = SqlDbType.NVarChar, Value = ClientData.DateofIncorporation });
                Connection.myparameters.Add(new myParameters { ParameterName = "@AddressFloor", mytype = SqlDbType.NVarChar, Value = ClientData.AddressFloor });
                Connection.myparameters.Add(new myParameters { ParameterName = "@AddressBuilding", mytype = SqlDbType.NVarChar, Value = ClientData.AddressBuilding });
                Connection.myparameters.Add(new myParameters { ParameterName = "@AddressStreet", mytype = SqlDbType.NVarChar, Value = ClientData.AddressStreet });
                Connection.myparameters.Add(new myParameters { ParameterName = "@AddressBrgy", mytype = SqlDbType.NVarChar, Value = ClientData.AddressBrgy });
                Connection.myparameters.Add(new myParameters { ParameterName = "@AddressMunicipality", mytype = SqlDbType.NVarChar, Value = ClientData.AddressMunicipality });
                Connection.myparameters.Add(new myParameters { ParameterName = "@AddressProvince", mytype = SqlDbType.NVarChar, Value = ClientData.AddressProvince });
                Connection.myparameters.Add(new myParameters { ParameterName = "@AddressRegion", mytype = SqlDbType.NVarChar, Value = ClientData.AddressRegion });
                Connection.myparameters.Add(new myParameters { ParameterName = "@AddressCountry", mytype = SqlDbType.NVarChar, Value = ClientData.AddressCountry });
                Connection.myparameters.Add(new myParameters { ParameterName = "@AddressZipCode", mytype = SqlDbType.NVarChar, Value = ClientData.AddressZipCode });
                Connection.myparameters.Add(new myParameters { ParameterName = "@NationalityCountry", mytype = SqlDbType.NVarChar, Value = ClientData.NationalityCountry });
                Connection.myparameters.Add(new myParameters { ParameterName = "@CompanyEQCustomerNumber", mytype = SqlDbType.NVarChar, Value = ClientData.CompanyEQCustomerNumber });
                Connection.myparameters.Add(new myParameters { ParameterName = "@TotalNumberOfEmployees", mytype = SqlDbType.NVarChar, Value = ClientData.TotalNumberOfEmployees });
                Connection.myparameters.Add(new myParameters { ParameterName = "@AccountType", mytype = SqlDbType.NVarChar, Value = ClientData.AccountType });
                //Connection.myparameters.Add(new myParameters { ParameterName = "@ContactType", mytype = SqlDbType.NVarChar, Value = ClientData.ContactType });
                Connection.myparameters.Add(new myParameters { ParameterName = "@ContactNumber", mytype = SqlDbType.NVarChar, Value = ClientData.ContactNumber });
                //Connection.myparameters.Add(new myParameters { ParameterName = "@EmailType", mytype = SqlDbType.NVarChar, Value = ClientData.EmailType });
                Connection.myparameters.Add(new myParameters { ParameterName = "@EmailAddress", mytype = SqlDbType.NVarChar, Value = ClientData.EmailAddress });
                //Connection.myparameters.Add(new myParameters { ParameterName = "@Website", mytype = SqlDbType.NVarChar, Value = ClientData.Website });
                //Connection.myparameters.Add(new myParameters { ParameterName = "@SSS", mytype = SqlDbType.NVarChar, Value = ClientData.SSS });
                //Connection.myparameters.Add(new myParameters { ParameterName = "@PhilHealth", mytype = SqlDbType.NVarChar, Value = ClientData.PhilHealth });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Tin", mytype = SqlDbType.NVarChar, Value = ClientData.Tin });
                //Connection.myparameters.Add(new myParameters { ParameterName = "@TinRDO", mytype = SqlDbType.NVarChar, Value = ClientData.TinRDO });
                //Connection.myparameters.Add(new myParameters { ParameterName = "@PagIbig", mytype = SqlDbType.NVarChar, Value = ClientData.PagIbig });
                //Connection.myparameters.Add(new myParameters { ParameterName = "@PagibigBranch", mytype = SqlDbType.NVarChar, Value = ClientData.PagibigBranch });
                Connection.myparameters.Add(new myParameters { ParameterName = "@FirstName", mytype = SqlDbType.NVarChar, Value = ClientData.FirstName });
                Connection.myparameters.Add(new myParameters { ParameterName = "@MiddleName", mytype = SqlDbType.NVarChar, Value = ClientData.MiddleName });
                Connection.myparameters.Add(new myParameters { ParameterName = "@LastName", mytype = SqlDbType.NVarChar, Value = ClientData.LastName });
                Connection.myparameters.Add(new myParameters { ParameterName = "@RepresentativeTitle", mytype = SqlDbType.NVarChar, Value = ClientData.RepresentativeTitle });
                Connection.myparameters.Add(new myParameters { ParameterName = "@RepresentativeContactNumber", mytype = SqlDbType.NVarChar, Value = ClientData.RepresentativeContactNumber });
                Connection.myparameters.Add(new myParameters { ParameterName = "@ModifiedById", mytype = SqlDbType.NVarChar, Value = ClientData.ModifiedById });
                Connection.myparameters.Add(new myParameters { ParameterName = "@PDF_Status", mytype = SqlDbType.NVarChar, Value = ClientData.PDF_Status });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Salad_Info", mytype = SqlDbType.NVarChar, Value = ClientData.Salad_Info });
                Connection.myparameters.Add(new myParameters { ParameterName = "@CClientID", mytype = SqlDbType.NVarChar, Value = ClientData.CClientID });
                Connection.myparameters.Add(new myParameters { ParameterName = "@lastAlias", mytype = SqlDbType.NVarChar, Value = ClientData.lastAlias });
                Connection.myparameters.Add(new myParameters { ParameterName = "@SaladSigID", mytype = SqlDbType.NVarChar, Value = ClientData.SaladSigID });

                Connection.ExecuteNonQuery("sp_CRT_Update");
                return "success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }
        [HttpPost]
        [Route("CRT_DeleteClientData")]
        public string CRT_DeleteClientData(ClientDetails ClientData)
        {
            //GetDatabase GDB = new GetDatabase();
            //GDB.ClientName = ClientData.CN;
            tempconnectionstring();
            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@ClientId", mytype = SqlDbType.NVarChar, Value = ClientData.ClientId });

            Connection.ExecuteNonQuery("sp_CRT_Delete");
            return "success";
        }
        [HttpPost]
        [Route("CRT_UserNameValidation")]
        public string CRT_UseerNameValidation(InsertClientData ClientData)
        {
            string valid = "";
            //GetDatabase GDB = new GetDatabase();
            //GDB.ClientName = ClientData.CN;
            tempconnectionstring();
            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@UserName", mytype = SqlDbType.NVarChar, Value = ClientData.UserName });
            valid = Connection.ExecuteScalar("sp_CRT_UsernameValidation").ToString();

            return valid;
        }
        [HttpPost]
        [Route("CRT_EmailValidation")]
        public string CRT_EmailValidation(UpdateClientData ClientData)
        {
            string valid = "";
            //GetDatabase GDB = new GetDatabase();
            //GDB.ClientName = ClientData.CN;
            tempconnectionstring();
            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@EmailAddress", mytype = SqlDbType.NVarChar, Value = ClientData.EmailAddress });
            valid = Connection.ExecuteScalar("sp_CRT_EmailVerify").ToString();

            return valid;
        }
        [HttpPost]
        [Route("CRT_ForgotPassword")]
        public string CRT_ForgotPassword(InsertClientData ClientData)
        {
            string valid = "";
            //GetDatabase GDB = new GetDatabase();
            //GDB.ClientName = ClientData.CN;
            tempconnectionstring();
            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@EmailAddress", mytype = SqlDbType.NVarChar, Value = ClientData.EmailAddress });
            valid = Connection.ExecuteScalar("sp_CRT_ForgotPassword_Code").ToString();

            return valid;
        }
        [HttpPost]
        [Route("CRT_CodeVerification")]
        public string CRT_CodeVerification(InsertClientData ClientData)
        {
            string valid = "";
            //GetDatabase GDB = new GetDatabase();
            //GDB.ClientName = ClientData.CN;
            tempconnectionstring();
            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@EmailAddress", mytype = SqlDbType.NVarChar, Value = ClientData.EmailAddress });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Code", mytype = SqlDbType.NVarChar, Value = ClientData.Code });
            valid = Connection.ExecuteScalar("sp_CRT_CodeVerification").ToString();

            return valid;
        }
        [HttpPost]
        [Route("CRT_UpdatePassword")]
        public string CRT_UpdatePassword(InsertClientData ClientData)
        {
            //string valid = "";
            //GetDatabase GDB = new GetDatabase();
            //GDB.ClientName = ClientData.CN;
            tempconnectionstring();
            Connection Connection = new Connection();
            Connection.myparameters.Add(new myParameters { ParameterName = "@Code", mytype = SqlDbType.NVarChar, Value = ClientData.Code });
            Connection.myparameters.Add(new myParameters { ParameterName = "@EmailAddress", mytype = SqlDbType.NVarChar, Value = ClientData.EmailAddress });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Password", mytype = SqlDbType.NVarChar, Value = ClientData.Password });
            //Connection.ExecuteScalar("sp_CRT_UpdatePassword").ToString();
            Connection.ExecuteNonQuery("sp_CRT_UpdatePassword");
            return "success";
        }
        //Login (Eto yung sa login ng User)
        [HttpPost]
        [Route("CRTUserLogin")]
        public string CRTUserLogin(CRTLogin login)
        {
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = login.CN;
            tempconnectionstring();

            try
            {
                string valid = "";
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@CN", mytype = SqlDbType.NVarChar, Value = login.CN });
                con.myparameters.Add(new myParameters { ParameterName = "@UserID", mytype = SqlDbType.NVarChar, Value = login.UserID });
                con.myparameters.Add(new myParameters { ParameterName = "@Password", mytype = SqlDbType.NVarChar, Value = login.Password });
                valid = con.ExecuteScalar("sp_CRT_UserLogin").ToString();
                return valid;
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }
        [HttpPost]
        [Route("CRTDisplayNewUser")]
        public GetDisplayNewUser CRTDisplayNewUser(CRTLogin login)
        {

            GetDisplayNewUser GetDisplayNewUsers = new GetDisplayNewUser();
            List<DisplayNewUser> UserDetails = new List<DisplayNewUser>();
            tempconnectionstring();
            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@CN", mytype = SqlDbType.NVarChar, Value = login.CN });

            DataTable dtuserList = new DataTable();

            dtuserList = Connection.GetDataTable("sp_CRT_DisplayNewUser");
            if (dtuserList.Rows.Count > 0)
            {
                foreach (DataRow row in dtuserList.Rows)
                {
                    DisplayNewUser Displaynewusers = new DisplayNewUser
                    {
                        //NumberOfUser
                        //isSent
                        //Password
                        //Email
                        Branch = row["Branch"].ToString(),
                        //NumberOfUser = row["NumberOfUser"].ToString(),
                        StartingID = row["StartingID"].ToString(),
                        AccountType = row["AccountType"].ToString(),
                        DateHired = row["DateHired"].ToString(),
                        //isSent = row["isSent"].ToString(),
                        //Password = row["Password"].ToString(),
                        //Email = row["Email"].ToString(),
                        FirstName = row["FirstName"].ToString(),
                        MiddleName = row["MiddleName"].ToString(),
                        LastName = row["LastName"].ToString(),
                        Suffix = row["Suffix"].ToString()
                    };
                    UserDetails.Add(Displaynewusers);
                }
                GetDisplayNewUsers.Displaynewuser = UserDetails;
                GetDisplayNewUsers.myreturn = "Success";
            }
            else
            {
                GetDisplayNewUsers.myreturn = "Error";
            }
            return GetDisplayNewUsers;
        }
        [HttpPost]
        [Route("CRTAddUserClient")]
        public List<DisplayNewUser> CRTAddUserClient(DisplayNewUser newuser)
        {
            //string valid = "";
            //GetDatabase GDB = new GetDatabase();
            //GDB.ClientName = login.CN; @CompanyName
            tempconnectionstring();
            Connection con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@FirstName", mytype = SqlDbType.NVarChar, Value = newuser.FirstName });
            con.myparameters.Add(new myParameters { ParameterName = "@MiddleName", mytype = SqlDbType.NVarChar, Value = newuser.MiddleName });
            con.myparameters.Add(new myParameters { ParameterName = "@LastName", mytype = SqlDbType.NVarChar, Value = newuser.LastName });
            con.myparameters.Add(new myParameters { ParameterName = "@Suffix", mytype = SqlDbType.NVarChar, Value = newuser.Suffix });
            con.myparameters.Add(new myParameters { ParameterName = "@Branch", mytype = SqlDbType.NVarChar, Value = newuser.Branch });
            //con.myparameters.Add(new myParameters { ParameterName = "@NumberOfUser", mytype = SqlDbType.NVarChar, Value = newuser.NumberOfUser });
            con.myparameters.Add(new myParameters { ParameterName = "@CompanyName", mytype = SqlDbType.NVarChar, Value = newuser.CompanyName });
            con.myparameters.Add(new myParameters { ParameterName = "@CreatedBy", mytype = SqlDbType.NVarChar, Value = newuser.CreatedBy });
            con.myparameters.Add(new myParameters { ParameterName = "@StartingID", mytype = SqlDbType.NVarChar, Value = newuser.StartingID });
            con.myparameters.Add(new myParameters { ParameterName = "@Datehired", mytype = SqlDbType.NVarChar, Value = newuser.DateHired });
            con.myparameters.Add(new myParameters { ParameterName = "@AccountType", mytype = SqlDbType.NVarChar, Value = newuser.AccountType });
            //con.myparameters.Add(new myParameters { ParameterName = "@StartingIDCharacter", mytype = SqlDbType.NVarChar, Value = newuser.StartingIDCharacter });


            DataTable DT = new DataTable();
            DT = con.GetDataTable("sp_CRT_AddUserClient");
            List<DisplayNewUser> ListReturned = new List<DisplayNewUser>();
            if (DT.Rows.Count > 0)
            {
                foreach (DataRow row in DT.Rows)
                {
                    DisplayNewUser Displaynewusers = new DisplayNewUser
                    {

                        // NumberOfUser = row["NumberOfUser"].ToString(),
                        CompanyName = row["CompanyName"].ToString(),
                        CreatedBy = row["CreatedBy"].ToString(),
                        StartingID = row["StartingID"].ToString(),
                        AccountType = row["AccountType"].ToString(),
                        DateHired = row["DateHired"].ToString(),
                        //isSent = row["isSent"].ToString(),
                        //Password = row["Password"].ToString(),
                        //Email = row["Email"].ToString(),
                        FirstName = row["FirstName"].ToString(),
                        MiddleName = row["MiddleName"].ToString(),
                        LastName = row["LastName"].ToString(),
                        Branch = row["Branch"].ToString(),
                        //DateAdded = row["DateAdded"].ToString()

                    };
                    ListReturned.Add(Displaynewusers);
                }
            }
            return ListReturned;
        }
        //[HttpPost]
        //[Route("CRT_BranchRDOSearch")]
        //public string CRT_BranchRDOSearch(GetRDO RdoData)
        //{
        //    string valid = "";
        //    //GetDatabase GDB = new GetDatabase();
        //    //GDB.ClientName = ClientData.CN;
        //    tempconnectionstring();
        //    Connection Connection = new Connection();

        //    Connection.myparameters.Add(new myParameters { ParameterName = "@Branch", mytype = SqlDbType.NVarChar, Value = RdoData.branch });
        //    valid = Connection.ExecuteScalar("sp_CRT_BranchRDOSearch").ToString();

        //    return valid;
        //}
        [HttpPost]
        [Route("CRT_BranchRDOSearch")]
        public List<GetRDO> CRT_BranchRDOSearch(DisplayNewUser newuser)
        {
            //GetDatabase GDB = new GetDatabase();
            //GDB.ClientName = login.CN;
            tempconnectionstring();
            Connection con = new Connection();
            DataTable DT = new DataTable();
            DT = con.GetDataTable("sp_CRT_BranchRDOSearch");
            List<GetRDO> ListReturned = new List<GetRDO>();
            if (DT.Rows.Count > 0)
            {
                foreach (DataRow row in DT.Rows)
                {
                    GetRDO DisplayBranch = new GetRDO
                    {
                        code = row["code"].ToString(),
                        branch = row["branch"].ToString()
                    };
                    ListReturned.Add(DisplayBranch);
                }
            }

            return ListReturned;
        }
        [HttpPost]
        [Route("CRT_BranchPagibigSearch")]
        public List<GetPagibig> CRT_BranchPagibigSearch(GetPagibig pagibigdata)
        {
            //string valid = "";
            //GetDatabase GDB = new GetDatabase();
            //GDB.ClientName = ClientData.CN;
            tempconnectionstring();
            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@Region", mytype = SqlDbType.NVarChar, Value = pagibigdata.region });
            //valid = Connection.ExecuteScalar("sp_CRT_BranchPagibigSearch").ToString();

            //return valid;
            DataTable DT = new DataTable();
            DT = Connection.GetDataTable("sp_CRT_BranchPagibigSearch");
            List<GetPagibig> ListReturned = new List<GetPagibig>();
            if (DT.Rows.Count > 0)
            {
                foreach (DataRow row in DT.Rows)
                {
                    GetPagibig Displaynewusers = new GetPagibig
                    {
                        code = row["code"].ToString(),
                        branch = row["branch"].ToString()
                    };
                    ListReturned.Add(Displaynewusers);
                }
            }

            return ListReturned;
        }
        [HttpPost]
        [Route("CRT_ZipcodeSearch")]
        public List<GetZipcodeArea> CRT_BranchPagibigSearch(GetZipcodeArea zipcode)
        {
            //string valid = "";
            //GetDatabase GDB = new GetDatabase();
            //GDB.ClientName = ClientData.CN;
            tempconnectionstring();
            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@Zipcode", mytype = SqlDbType.NVarChar, Value = zipcode.Zipcode });
            //valid = Connection.ExecuteScalar("sp_CRT_BranchPagibigSearch").ToString();

            //return valid;
            DataTable DT = new DataTable();
            DT = Connection.GetDataTable("sp_CRT_ZipcodeSearch");
            List<GetZipcodeArea> ListReturned = new List<GetZipcodeArea>();
            if (DT.Rows.Count > 0)
            {
                foreach (DataRow row in DT.Rows)
                {
                    GetZipcodeArea DisplayAreacode = new GetZipcodeArea
                    {
                        Country = row["Country"].ToString(),
                        Region = row["Region"].ToString(),
                        Province = row["Province"].ToString(),
                        Municipal = row["Municipal"].ToString(),
                    };
                    ListReturned.Add(DisplayAreacode);
                }
            }

            return ListReturned;
        }
        [HttpPost]
        [Route("CRT_BranchPagibigRegionSearch")]
        public List<GetPagibig> CRT_BranchPagibigRegionSearch(DisplayNewUser newuser)
        {
            //GetDatabase GDB = new GetDatabase();
            //GDB.ClientName = login.CN;
            tempconnectionstring();
            Connection con = new Connection();
            DataTable DT = new DataTable();
            DT = con.GetDataTable("sp_CRT_BranchPagibigRegionSearch");
            List<GetPagibig> ListReturned = new List<GetPagibig>();
            if (DT.Rows.Count > 0)
            {
                foreach (DataRow row in DT.Rows)
                {
                    GetPagibig Displaynewusers = new GetPagibig
                    {
                        region = row["region"].ToString()
                    };
                    ListReturned.Add(Displaynewusers);
                }
            }
            return ListReturned;
        }
        //[HttpPost]
        //[Route("CRT_BranchPagibigSearch")]
        //public List<GetPagibig> CRT_BranchPagibigSearch(DisplayNewUser newuser)
        //{
        //    //GetDatabase GDB = new GetDatabase();
        //    //GDB.ClientName = login.CN;
        //    tempconnectionstring();
        //    Connection con = new Connection();
        //    DataTable DT = new DataTable();
        //    DT = con.GetDataTable("sp_CRT_BranchPagibigSearch");
        //    List<GetPagibig> ListReturned = new List<GetPagibig>();
        //    if (DT.Rows.Count > 0)
        //    {
        //        foreach (DataRow row in DT.Rows)
        //        {
        //            GetPagibig Displaynewusers = new GetPagibig
        //            {
        //                region = row["region"].ToString(),
        //                code = row["code"].ToString(),
        //                branch = row["branch"].ToString()
        //            };
        //            ListReturned.Add(Displaynewusers);
        //        }
        //    }
        //    return ListReturned;
        //}
        [HttpPost]
        [Route("CRT_Nationality")]
        public List<GetNationality> CRT_Nationality(DisplayNewUser newuser)
        {
            //GetDatabase GDB = new GetDatabase();
            //GDB.ClientName = login.CN;
            tempconnectionstring();
            Connection con = new Connection();
            DataTable DT = new DataTable();
            DT = con.GetDataTable("sp_CRT_Nationality");
            List<GetNationality> ListReturned = new List<GetNationality>();
            if (DT.Rows.Count > 0)
            {
                foreach (DataRow row in DT.Rows)
                {
                    GetNationality Displaynationality = new GetNationality
                    {
                        Nationality = row["Nationality"].ToString()
                    };
                    ListReturned.Add(Displaynationality);
                }
            }

            return ListReturned;
        }
        [HttpPost]
        [Route("CRT_UpdateNewAddedUserClient")]
        public string CRT_UpdateNewAddedUserClient(DisplayNewUser updatedata)
        {
            //string valid = "";
            //GetDatabase GDB = new GetDatabase();
            //GDB.ClientName = ClientData.CN;
            tempconnectionstring();
            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@CN", mytype = SqlDbType.NVarChar, Value = updatedata.CN });
            Connection.myparameters.Add(new myParameters { ParameterName = "@StartingID", mytype = SqlDbType.NVarChar, Value = updatedata.StartingID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@FirstName", mytype = SqlDbType.NVarChar, Value = updatedata.FirstName });
            Connection.myparameters.Add(new myParameters { ParameterName = "@MiddleName", mytype = SqlDbType.NVarChar, Value = updatedata.MiddleName });
            Connection.myparameters.Add(new myParameters { ParameterName = "@LastName", mytype = SqlDbType.NVarChar, Value = updatedata.LastName });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Suffix", mytype = SqlDbType.NVarChar, Value = updatedata.Suffix });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Email", mytype = SqlDbType.NVarChar, Value = updatedata.Email });
            Connection.myparameters.Add(new myParameters { ParameterName = "@DateHired", mytype = SqlDbType.NVarChar, Value = updatedata.DateHired });
            Connection.myparameters.Add(new myParameters { ParameterName = "@AccountType", mytype = SqlDbType.NVarChar, Value = updatedata.AccountType });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Password", mytype = SqlDbType.NVarChar, Value = updatedata.Password });

            //Connection.ExecuteScalar("sp_CRT_UpdatePassword").ToString();
            Connection.ExecuteNonQuery("sp_CRT_UpdateNewAddedUserClient");
            return "success";
        }
        [HttpPost]
        [Route("CRT_DeleteUser")]
        public string CRT_DeleteUser(DisplayNewUser updatedata)
        {
            //GetDatabase GDB = new GetDatabase();
            //GDB.ClientName = ClientData.CN;
            tempconnectionstring();
            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@CN", mytype = SqlDbType.NVarChar, Value = updatedata.CN });
            Connection.myparameters.Add(new myParameters { ParameterName = "@StartingID", mytype = SqlDbType.NVarChar, Value = updatedata.StartingID });

            Connection.ExecuteNonQuery("sp_CRT_DeleteUser");
            return "success";
        }
        [HttpPost]
        [Route("CRT_SentVerification")]
        public string CRT_SentVerification(DisplayNewUser updatedata)
        {
            //GetDatabase GDB = new GetDatabase();
            //GDB.ClientName = ClientData.CN;
            tempconnectionstring();
            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@StartingID", mytype = SqlDbType.NVarChar, Value = updatedata.StartingID });

            Connection.ExecuteNonQuery("sp_CRT_SentVerification");
            return "success";
        }
        [HttpPost]
        [Route("CRT_ViewAllSentEmail")]
        public GetDisplayNewUser CRT_ViewAllSentEmail(CRTLogin login)
        {

            GetDisplayNewUser GetDisplayNewUsers = new GetDisplayNewUser();
            List<DisplayNewUser> UserDetails = new List<DisplayNewUser>();
            tempconnectionstring();
            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@CompanyName", mytype = SqlDbType.NVarChar, Value = login.CN });

            DataTable dtuserList = new DataTable();
            dtuserList = Connection.GetDataTable("sp_CRT_ViewAllSentEmail");
            if (dtuserList.Rows.Count > 0)
            {
                foreach (DataRow row in dtuserList.Rows)
                {
                    DisplayNewUser Displaynewusers = new DisplayNewUser
                    {
                        StartingID = row["StartingID"].ToString(),
                        FirstName = row["FirstName"].ToString(),
                        MiddleName = row["MiddleName"].ToString(),
                        LastName = row["LastName"].ToString(),
                        AccountType = row["AccountType"].ToString(),
                        Email = row["Email"].ToString(),
                        DateHired = row["DateHired"].ToString(),
                        Branch = row["Branch"].ToString(),
                        DateAdded = row["DateAdded"].ToString(),
                        DataEntryStatus = row["DataEntryStatus"].ToString(),
                        CRTStatus = row["CRTStatus"].ToString(),
                        CRTDateSent = row["CRTDateSent"].ToString()
                    };
                    UserDetails.Add(Displaynewusers);
                }
                GetDisplayNewUsers.Displaynewuser = UserDetails;
                GetDisplayNewUsers.myreturn = "Success";
            }
            else
            {
                GetDisplayNewUsers.myreturn = "Error";
            }
            return GetDisplayNewUsers;
        }
        [HttpPost]
        [Route("GenerateCRTPAYROLL")]
        public string GenerateCRTPAYROLL(ClientDetails clientdata)
        {

            tempconnectionstring();
            Connection con = new Connection();

            con.myparameters.Add(new myParameters { ParameterName = "@CLientID", mytype = SqlDbType.NVarChar, Value = clientdata.ClientId });
            con.myparameters.Add(new myParameters { ParameterName = "@Branch", mytype = SqlDbType.NVarChar, Value = clientdata.Branch });

            DataTable EmpData = con.GetDataTable("sp_CRT_PayrollFile");

            string Companyname = EmpData.Rows[0][36].ToString();
            string Site = EmpData.Rows[0][45].ToString();
            string Count = EmpData.Rows[0][46].ToString();
            string AccountType = EmpData.Rows[0][44].ToString();
            if (AccountType == "Easy Savings")
            {
                AccountType = "ES";
            }
            else
            {
                AccountType = "AA";
            }

            //Naming Convention is "Company Name_Site_Date_Count". Count means pang ilan file siya today. 

            try
            {
                string dataDir = HostingEnvironment.MapPath("~/Files/");
                //FileStream stream = new FileStream(dataDir + "CRTVlookup.xls", FileMode.Open);
                FileStream stream = new FileStream(dataDir + "newcrttemplate.xls", FileMode.Open);
                string filename;
                string datebuild = DateTime.Now.ToString("MMddyyyy");
                string timebuild = DateTime.Now.ToString("hh-mmtt");

                filename = Companyname + "_" + Site + "_" + datebuild + "_" + AccountType + "_" + Count + ".xls";
                using (var fileStream = new FileStream(dataDir + filename, FileMode.Create, FileAccess.Write))
                {
                    stream.CopyTo(fileStream);
                }
                stream.Close();



                ExcelWorkbookFormat workbookFormat = ExcelWorkbookFormat.Xls_2003;
                //string testDocFile = dataDir + "CrtSampleFileExport.xls";
                string testDocFile = dataDir + "newcrttemplate.xls";
                ExcelWorkbook workbook = new ExcelWorkbook(testDocFile);
                //ExcelWorksheet firstWorksheet = workbook.Worksheets[0];
                ExcelWorksheet firstWorksheet = workbook.Worksheets[1];

                //string companyname = "";
                if (EmpData.Rows.Count > 0)
                {
                    //companynamefile = EmpData.Rows[0][36].ToString();//Company Name
                    firstWorksheet["C" + 3].Text = EmpData.Rows[0][36].ToString();//Company Name
                    firstWorksheet["C" + 4].Text = EmpData.Rows[0][37].ToString();//Date of Incorporation
                    firstWorksheet["C" + 5].Text = EmpData.Rows[0][38].ToString();//Complete Company Address
                    firstWorksheet["C" + 6].Text = EmpData.Rows[0][39].ToString();//Nationality of Company
                    firstWorksheet["C" + 7].Text = EmpData.Rows[0][40].ToString();//Contact Number
                    firstWorksheet["C" + 8].Text = EmpData.Rows[0][41].ToString();//Company Tin
                                                                                  ////firstWorksheet["C" + 9].Text = EmpData.Rows[0][42].ToString();//Company CIF Number
                    firstWorksheet["C" + 10].Text = EmpData.Rows[0][42].ToString();//Company EQ Customer Number
                    firstWorksheet["C" + 11].Text = EmpData.Rows[0][43].ToString();//Total Number of Employees
                    firstWorksheet["C" + 12].Text = EmpData.Rows[0][44].ToString();//Account Type
                }


                int cellnum = 19;
                for (int i = 0; i < EmpData.Rows.Count; i++)
                {
                    string myDate = EmpData.Rows[i][5].ToString();
                    DateTime myDate2 = DateTime.Parse(myDate);
                    firstWorksheet["B" + cellnum].Text = EmpData.Rows[i][0].ToString();
                    firstWorksheet["C" + cellnum].Text = EmpData.Rows[i][1].ToString();
                    firstWorksheet["D" + cellnum].Text = EmpData.Rows[i][2].ToString();
                    firstWorksheet["E" + cellnum].Text = EmpData.Rows[i][3].ToString();
                    firstWorksheet["F" + cellnum].Text = EmpData.Rows[i][4].ToString();
                    firstWorksheet["G" + cellnum].DateTimeValue = myDate2;
                    firstWorksheet["H" + cellnum].Text = EmpData.Rows[i][6].ToString();
                    firstWorksheet["I" + cellnum].Text = EmpData.Rows[i][7].ToString();
                    firstWorksheet["J" + cellnum].Text = EmpData.Rows[i][8].ToString();
                    firstWorksheet["K" + cellnum].Text = EmpData.Rows[i][9].ToString();
                    firstWorksheet["L" + cellnum].Text = EmpData.Rows[i][10].ToString();
                    firstWorksheet["M" + cellnum].Text = EmpData.Rows[i][11].ToString();
                    firstWorksheet["N" + cellnum].Text = EmpData.Rows[i][12].ToString();
                    firstWorksheet["O" + cellnum].Text = EmpData.Rows[i][13].ToString();
                    firstWorksheet["P" + cellnum].Text = EmpData.Rows[i][14].ToString();
                    firstWorksheet["Q" + cellnum].Text = EmpData.Rows[i][15].ToString();
                    firstWorksheet["R" + cellnum].Text = EmpData.Rows[i][16].ToString();
                    firstWorksheet["S" + cellnum].Text = EmpData.Rows[i][17].ToString();
                    firstWorksheet["T" + cellnum].Text = EmpData.Rows[i][18].ToString();
                    firstWorksheet["U" + cellnum].Text = EmpData.Rows[i][19].ToString();
                    firstWorksheet["V" + cellnum].Text = EmpData.Rows[i][20].ToString();
                    firstWorksheet["W" + cellnum].Text = EmpData.Rows[i][21].ToString();
                    firstWorksheet["X" + cellnum].Text = EmpData.Rows[i][22].ToString();
                    firstWorksheet["Y" + cellnum].Text = EmpData.Rows[i][23].ToString();
                    firstWorksheet["Z" + cellnum].Text = EmpData.Rows[i][24].ToString();
                    firstWorksheet["AA" + cellnum].Text = EmpData.Rows[i][25].ToString();
                    firstWorksheet["AB" + cellnum].Text = EmpData.Rows[i][26].ToString();
                    firstWorksheet["AC" + cellnum].Text = EmpData.Rows[i][27].ToString();
                    firstWorksheet["AD" + cellnum].Text = EmpData.Rows[i][28].ToString();
                    firstWorksheet["AE" + cellnum].Text = EmpData.Rows[i][29].ToString();
                    firstWorksheet["AF" + cellnum].Text = EmpData.Rows[i][30].ToString();
                    firstWorksheet["AG" + cellnum].Text = EmpData.Rows[i][31].ToString();
                    firstWorksheet["AH" + cellnum].Text = EmpData.Rows[i][32].ToString();
                    firstWorksheet["AI" + cellnum].Text = EmpData.Rows[i][33].ToString();
                    firstWorksheet["AJ" + cellnum].Text = EmpData.Rows[i][34].ToString();
                    firstWorksheet["AK" + cellnum].Text = EmpData.Rows[i][35].ToString();

                    cellnum++;
                }


                //https://http://ws.durusthr.com/ILM_WS_LIVE/sFTP/CrtSampleFileExport_12-41-PM.xls
                HttpResponse httpResponse = System.Web.HttpContext.Current.Response;

                httpResponse.Clear();
                httpResponse.ContentType = "Application/x-msexcel";
                httpResponse.AddHeader("Content-Disposition", String.Format("attachment; filename={0}", filename));
                string newDIR = HttpContext.Current.Server.MapPath("~/sFTP/");
                string onlineDIR = "http://ws.durusthr.com/ilm_ws_live/sFTP/";

                workbook.Save(newDIR + filename);

                //return "Success";
                return onlineDIR + filename;
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }
        public string LocalGenerateCRTPAYROLL(InsertClientData clientdata)
        {

            tempconnectionstring();
            Connection con = new Connection();

            con.myparameters.Add(new myParameters { ParameterName = "@CLientID", mytype = SqlDbType.NVarChar, Value = clientdata.UserName });

            DataTable EmpData = con.GetDataTable("sp_CRT_PayrollFile");

            try
            {
                string dataDir = HostingEnvironment.MapPath("~/Files/");
                FileStream stream = new FileStream(dataDir + "CrtSampleFileExport.xls", FileMode.Open);

                string timebuild = DateTime.Now.ToString("hh-mm-tt");

                string filename;
                filename = "CrtSampleFileExport_" + timebuild + ".xls";
                using (var fileStream = new FileStream(dataDir + filename, FileMode.Create, FileAccess.Write))
                {
                    stream.CopyTo(fileStream);
                }
                stream.Close();

                ExcelWorkbookFormat workbookFormat = ExcelWorkbookFormat.Xls_2003;
                string testDocFile = dataDir + "CrtSampleFileExport.xls";
                ExcelWorkbook workbook = new ExcelWorkbook(testDocFile);
                ExcelWorksheet firstWorksheet = workbook.Worksheets[0];
                //string companyname = "";
                if (EmpData.Rows.Count > 0)
                {
                    //companyname = EmpData.Rows[0][36].ToString();//Company Name
                    firstWorksheet["C" + 3].Text = EmpData.Rows[0][36].ToString();//Company Name
                    firstWorksheet["C" + 4].Text = EmpData.Rows[0][37].ToString();//Date of Incorporation
                    firstWorksheet["C" + 5].Text = EmpData.Rows[0][38].ToString();//Complete Company Address
                    firstWorksheet["C" + 6].Text = EmpData.Rows[0][39].ToString();//Nationality of Company
                    firstWorksheet["C" + 7].Text = EmpData.Rows[0][40].ToString();//Contact Number
                    firstWorksheet["C" + 8].Text = EmpData.Rows[0][41].ToString();//Company Tin
                                                                                  ////firstWorksheet["C" + 9].Text = EmpData.Rows[0][42].ToString();//Company CIF Number
                    firstWorksheet["C" + 10].Text = EmpData.Rows[0][42].ToString();//Company EQ Customer Number
                    firstWorksheet["C" + 11].Text = EmpData.Rows[0][43].ToString();//Total Number of Employees
                    firstWorksheet["C" + 12].Text = EmpData.Rows[0][44].ToString();//Account Type

                }
                int cellnum = 19;
                for (int i = 0; i < EmpData.Rows.Count; i++)
                {
                    firstWorksheet["B" + cellnum].Text = EmpData.Rows[i][0].ToString();
                    firstWorksheet["C" + cellnum].Text = EmpData.Rows[i][1].ToString();
                    firstWorksheet["D" + cellnum].Text = EmpData.Rows[i][2].ToString();
                    firstWorksheet["E" + cellnum].Text = EmpData.Rows[i][3].ToString();
                    firstWorksheet["F" + cellnum].Text = EmpData.Rows[i][4].ToString();
                    firstWorksheet["G" + cellnum].Text = EmpData.Rows[i][5].ToString();
                    firstWorksheet["H" + cellnum].Text = EmpData.Rows[i][6].ToString();
                    firstWorksheet["I" + cellnum].Text = EmpData.Rows[i][7].ToString();
                    firstWorksheet["J" + cellnum].Text = EmpData.Rows[i][8].ToString();
                    firstWorksheet["K" + cellnum].Text = EmpData.Rows[i][9].ToString();
                    firstWorksheet["L" + cellnum].Text = EmpData.Rows[i][10].ToString();
                    firstWorksheet["M" + cellnum].Text = EmpData.Rows[i][11].ToString();
                    firstWorksheet["N" + cellnum].Text = EmpData.Rows[i][12].ToString();
                    firstWorksheet["O" + cellnum].Text = EmpData.Rows[i][13].ToString();
                    firstWorksheet["P" + cellnum].Text = EmpData.Rows[i][14].ToString();
                    firstWorksheet["Q" + cellnum].Text = EmpData.Rows[i][15].ToString();
                    firstWorksheet["R" + cellnum].Text = EmpData.Rows[i][16].ToString();
                    firstWorksheet["S" + cellnum].Text = EmpData.Rows[i][17].ToString();
                    firstWorksheet["T" + cellnum].Text = EmpData.Rows[i][18].ToString();
                    firstWorksheet["U" + cellnum].Text = EmpData.Rows[i][19].ToString();
                    firstWorksheet["V" + cellnum].Text = EmpData.Rows[i][20].ToString();
                    firstWorksheet["W" + cellnum].Text = EmpData.Rows[i][21].ToString();
                    firstWorksheet["X" + cellnum].Text = EmpData.Rows[i][22].ToString();
                    firstWorksheet["Y" + cellnum].Text = EmpData.Rows[i][23].ToString();
                    firstWorksheet["Z" + cellnum].Text = EmpData.Rows[i][24].ToString();
                    firstWorksheet["AA" + cellnum].Text = EmpData.Rows[i][25].ToString();
                    firstWorksheet["AB" + cellnum].Text = EmpData.Rows[i][26].ToString();
                    firstWorksheet["AC" + cellnum].Text = EmpData.Rows[i][27].ToString();
                    firstWorksheet["AD" + cellnum].Text = EmpData.Rows[i][28].ToString();
                    firstWorksheet["AE" + cellnum].Text = EmpData.Rows[i][29].ToString();
                    firstWorksheet["AF" + cellnum].Text = EmpData.Rows[i][30].ToString();
                    firstWorksheet["AG" + cellnum].Text = EmpData.Rows[i][31].ToString();
                    firstWorksheet["AH" + cellnum].Text = EmpData.Rows[i][32].ToString();
                    firstWorksheet["AI" + cellnum].Text = EmpData.Rows[i][33].ToString();
                    firstWorksheet["AJ" + cellnum].Text = EmpData.Rows[i][34].ToString();
                    firstWorksheet["AK" + cellnum].Text = EmpData.Rows[i][35].ToString();

                    cellnum++;
                }
                //https://http://ws.durusthr.com/ILM_WS_LIVE/sFTP/CrtSampleFileExport_12-41-PM.xls
                HttpResponse httpResponse = System.Web.HttpContext.Current.Response;

                httpResponse.Clear();
                httpResponse.ContentType = "Application/x-msexcel";
                httpResponse.AddHeader("Content-Disposition", String.Format("attachment; filename={0}", filename));
                string newDIR = HttpContext.Current.Server.MapPath("~/sFTP/");
                string onlineDIR = "http://ws.durusthr.com/ilm_ws_live/sFTP/";

                workbook.Save(newDIR + filename);

                //return "Success";
                return filename;
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }
        [HttpPost]
        [Route("CRT_Automation")]
        public string CRT_Automation(InsertClientData ICD)
        {
            try
            {
                tempconnectionstring();
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@ClientID", mytype = SqlDbType.NVarChar, Value = ICD.UserName });

                string clientid = con.ExecuteScalar("sp_CRT_UpdateCRTTable");
                InsertClientData ic = new InsertClientData();
                ic.UserName = clientid;
                string attachmentpath = LocalGenerateCRTPAYROLL(ic);

                //con.ExecuteNonQuery("sp_CRT_UpdateCRTTable");
                //string attachmentpath = LocalGenerateCRTPAYROLL(ICD);

                string emailaccount = "durusthr@illimitado.com";
                string emailpassword = "@1230Qwerty";
                string loopemail = ICD.EmailAddress;
                string prefix = HttpContext.Current.Server.MapPath("~/sFTP/");
                using (MailMessage mm = new MailMessage(emailaccount, loopemail))
                {
                    MailAddress aliasmail = new MailAddress("durusthr@illimitado.com", "durusthr@illimitado.com");
                    MailAddress aliasreplymail = new MailAddress("durusthr@illimitado.com");
                    mm.From = aliasmail;
                    mm.Subject = "Notifications";
                    mm.Body = "";
                    mm.Attachments.Add(new Attachment(prefix + attachmentpath));
                    mm.IsBodyHtml = false;
                    SmtpClient smtp = new SmtpClient();
                    smtp.Host = "box1256.bluehost.com";
                    smtp.EnableSsl = true;
                    NetworkCredential NetworkCred = new NetworkCredential(emailaccount, emailpassword);
                    smtp.UseDefaultCredentials = true;
                    smtp.Credentials = NetworkCred;
                    smtp.Port = 587;
                    smtp.Send(mm);
                }
                return "Succes";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }
        [HttpPost]
        [Route("CRT_EmailValidationForImport")]
        public string CRT_EmailValidationForImport(DisplayNewUser updatedata)
        {
            string valid = "";

            tempconnectionstring();
            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@StartingID", mytype = SqlDbType.NVarChar, Value = updatedata.StartingID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@FirstName", mytype = SqlDbType.NVarChar, Value = updatedata.FirstName });
            Connection.myparameters.Add(new myParameters { ParameterName = "@MiddleName", mytype = SqlDbType.NVarChar, Value = updatedata.MiddleName });
            Connection.myparameters.Add(new myParameters { ParameterName = "@LastName", mytype = SqlDbType.NVarChar, Value = updatedata.LastName });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Suffix", mytype = SqlDbType.NVarChar, Value = updatedata.Suffix });
            Connection.myparameters.Add(new myParameters { ParameterName = "@DateHired", mytype = SqlDbType.NVarChar, Value = updatedata.DateHired });
            Connection.myparameters.Add(new myParameters { ParameterName = "@AccountType", mytype = SqlDbType.NVarChar, Value = updatedata.AccountType });
            //Connection.myparameters.Add(new myParameters { ParameterName = "@Password", mytype = SqlDbType.NVarChar, Value = updatedata.Password });
            Connection.myparameters.Add(new myParameters { ParameterName = "@CompanyName", mytype = SqlDbType.NVarChar, Value = updatedata.CompanyName });
            Connection.myparameters.Add(new myParameters { ParameterName = "@CreatedBy", mytype = SqlDbType.NVarChar, Value = updatedata.CreatedBy });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Branch", mytype = SqlDbType.NVarChar, Value = updatedata.Branch });


            valid = Connection.ExecuteScalar("sp_CRT_EmailValidationForImport").ToString();
            return valid;
        }
        [HttpPost]
        [Route("CRT_ShowFullNameAndEmail")]
        public List<SHowclientFullNameAndEmail> CRT_ShowFullNameAndEmail(ClientDetails updatedata)
        {

            tempconnectionstring();
            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@UserName", mytype = SqlDbType.NVarChar, Value = updatedata.UserName });

            DataTable DT = new DataTable();
            DT = Connection.GetDataTable("sp_CRT_ShowFullNameAndEmail");
            List<SHowclientFullNameAndEmail> ListReturned = new List<SHowclientFullNameAndEmail>();
            if (DT.Rows.Count > 0)
            {
                foreach (DataRow row in DT.Rows)
                {
                    SHowclientFullNameAndEmail Displaynewusers = new SHowclientFullNameAndEmail
                    {
                        FullName = row["FullName"].ToString(),
                        Email = row["Email"].ToString()
                    };
                    ListReturned.Add(Displaynewusers);
                }
            }

            return ListReturned;
        }
        [HttpPost]
        [Route("CRT_SendingCrtSettings_daily")]
        public string CRT_SendingCrtSettings_daily(ClientSettingDaily updatedata)
        {
            string valid = "";

            tempconnectionstring();
            Connection Connection = new Connection();
            Connection.myparameters.Add(new myParameters { ParameterName = "@UserName", mytype = SqlDbType.NVarChar, Value = updatedata.UserName });
            Connection.myparameters.Add(new myParameters { ParameterName = "@CN", mytype = SqlDbType.NVarChar, Value = updatedata.CN });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Setting", mytype = SqlDbType.NVarChar, Value = updatedata.Setting });
            Connection.myparameters.Add(new myParameters { ParameterName = "@CountDays", mytype = SqlDbType.NVarChar, Value = updatedata.CountDays });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Monday", mytype = SqlDbType.NVarChar, Value = updatedata.Monday });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Tuesday", mytype = SqlDbType.NVarChar, Value = updatedata.Tuesday });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Wednesday", mytype = SqlDbType.NVarChar, Value = updatedata.Wednesday });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Thursday", mytype = SqlDbType.NVarChar, Value = updatedata.Thursday });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Friday", mytype = SqlDbType.NVarChar, Value = updatedata.Friday });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Saturday", mytype = SqlDbType.NVarChar, Value = updatedata.Saturday });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Sunday", mytype = SqlDbType.NVarChar, Value = updatedata.Sunday });
            Connection.myparameters.Add(new myParameters { ParameterName = "@StartDate", mytype = SqlDbType.NVarChar, Value = updatedata.StartDate });
            Connection.myparameters.Add(new myParameters { ParameterName = "@EndDate", mytype = SqlDbType.NVarChar, Value = updatedata.EndDate });
            Connection.myparameters.Add(new myParameters { ParameterName = "@StartTime", mytype = SqlDbType.NVarChar, Value = updatedata.StartTime });
            Connection.myparameters.Add(new myParameters { ParameterName = "@EndTime", mytype = SqlDbType.NVarChar, Value = updatedata.EndTime });
            Connection.myparameters.Add(new myParameters { ParameterName = "@res", mytype = SqlDbType.NVarChar, Value = updatedata.recipient });

            valid = Connection.ExecuteScalar("sp_CRT_SendingCrtSettings_daily").ToString();
            //valid = "success";
            return valid;
        }
        [HttpPost]
        [Route("CRT_SendingCrtSettings_weekly")]
        public string CRT_SendingCrtSettings_weekly(ClientSettingWeekly updatedata)
        {
            string valid = "";

            tempconnectionstring();
            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@UserName", mytype = SqlDbType.NVarChar, Value = updatedata.UserName });
            Connection.myparameters.Add(new myParameters { ParameterName = "@CountWeeks", mytype = SqlDbType.NVarChar, Value = updatedata.CountWeeks });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Monday", mytype = SqlDbType.NVarChar, Value = updatedata.Monday });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Tuesday", mytype = SqlDbType.NVarChar, Value = updatedata.Tuesday });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Wednesday", mytype = SqlDbType.NVarChar, Value = updatedata.Wednesday });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Thursday", mytype = SqlDbType.NVarChar, Value = updatedata.Thursday });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Friday", mytype = SqlDbType.NVarChar, Value = updatedata.Friday });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Saturday", mytype = SqlDbType.NVarChar, Value = updatedata.Saturday });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Sunday", mytype = SqlDbType.NVarChar, Value = updatedata.Sunday });
            Connection.myparameters.Add(new myParameters { ParameterName = "@StartDate", mytype = SqlDbType.NVarChar, Value = updatedata.StartDate });
            Connection.myparameters.Add(new myParameters { ParameterName = "@EndDate", mytype = SqlDbType.NVarChar, Value = updatedata.EndDate });
            Connection.myparameters.Add(new myParameters { ParameterName = "@StartTime", mytype = SqlDbType.NVarChar, Value = updatedata.StartTime });
            Connection.myparameters.Add(new myParameters { ParameterName = "@EndTime", mytype = SqlDbType.NVarChar, Value = updatedata.EndTime });
            Connection.myparameters.Add(new myParameters { ParameterName = "@res", mytype = SqlDbType.NVarChar, Value = updatedata.recipient });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Setting", mytype = SqlDbType.NVarChar, Value = updatedata.Setting });
            Connection.myparameters.Add(new myParameters { ParameterName = "@CN", mytype = SqlDbType.NVarChar, Value = updatedata.CN });


            valid = Connection.ExecuteScalar("sp_CRT_SendingCrtSettings_weekly").ToString();
            //valid = "success";
            return valid;
        }
        [HttpPost]
        [Route("CRT_SendingCrtSettings_monthly")]
        public string CRT_SendingCrtSettings_monthly(ClientSettingMonthlyEditable updatedata)
        {
            string valid = "";

            tempconnectionstring();
            Connection Connection = new Connection();


            Connection.myparameters.Add(new myParameters { ParameterName = "@isActivated1st", mytype = SqlDbType.NVarChar, Value = updatedata.isActivated1st });
            Connection.myparameters.Add(new myParameters { ParameterName = "@CountDays", mytype = SqlDbType.NVarChar, Value = updatedata.CountDays });
            Connection.myparameters.Add(new myParameters { ParameterName = "@OrdinalDays", mytype = SqlDbType.NVarChar, Value = updatedata.OrdinalDays });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Days", mytype = SqlDbType.NVarChar, Value = updatedata.Days });
            Connection.myparameters.Add(new myParameters { ParameterName = "@CountMonths", mytype = SqlDbType.NVarChar, Value = updatedata.CountMonths });
            Connection.myparameters.Add(new myParameters { ParameterName = "@ ", mytype = SqlDbType.NVarChar, Value = updatedata.StartDate });
            Connection.myparameters.Add(new myParameters { ParameterName = "@EndDate", mytype = SqlDbType.NVarChar, Value = updatedata.EndDate });
            Connection.myparameters.Add(new myParameters { ParameterName = "@StartTime", mytype = SqlDbType.NVarChar, Value = updatedata.StartTime });
            Connection.myparameters.Add(new myParameters { ParameterName = "@EndTime", mytype = SqlDbType.NVarChar, Value = updatedata.EndTime });
            Connection.myparameters.Add(new myParameters { ParameterName = "@res", mytype = SqlDbType.NVarChar, Value = updatedata.recipient });


            valid = Connection.ExecuteScalar("sp_CRT_SendingCrtSettings_monthly").ToString();
            //valid = "success";
            return valid;
        }
        [HttpPost]
        [Route("CRT_SendingCrtSettings_yearly")]
        public string CRT_SendingCrtSettings_yearly(ClientSettingYearlyEditable updatedata)
        {
            string valid = "";

            tempconnectionstring();
            Connection Connection = new Connection();


            Connection.myparameters.Add(new myParameters { ParameterName = "@isActivated1st", mytype = SqlDbType.NVarChar, Value = updatedata.isActivated1st });
            Connection.myparameters.Add(new myParameters { ParameterName = "@CountYears", mytype = SqlDbType.NVarChar, Value = updatedata.CountYears });
            Connection.myparameters.Add(new myParameters { ParameterName = "@OrdinalDay", mytype = SqlDbType.NVarChar, Value = updatedata.OrdinalDay });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Days", mytype = SqlDbType.NVarChar, Value = updatedata.Days });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Day", mytype = SqlDbType.NVarChar, Value = updatedata.Day });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Month", mytype = SqlDbType.NVarChar, Value = updatedata.Month });
            Connection.myparameters.Add(new myParameters { ParameterName = "@StartDate", mytype = SqlDbType.NVarChar, Value = updatedata.StartDate });
            Connection.myparameters.Add(new myParameters { ParameterName = "@EndDate", mytype = SqlDbType.NVarChar, Value = updatedata.EndDate });
            Connection.myparameters.Add(new myParameters { ParameterName = "@StartTime", mytype = SqlDbType.NVarChar, Value = updatedata.StartTime });
            Connection.myparameters.Add(new myParameters { ParameterName = "@EndTime", mytype = SqlDbType.NVarChar, Value = updatedata.EndTime });
            Connection.myparameters.Add(new myParameters { ParameterName = "@res", mytype = SqlDbType.NVarChar, Value = updatedata.recipient });


            valid = Connection.ExecuteScalar("sp_CRT_SendingCrtSettings_yearly").ToString();
            //valid = "success";
            return valid;
        }
        [HttpPost]
        [Route("CRT_DisplayMainCRTSetting")]
        public List<ClientSettingDaily> CRT_DisplayMainCrtSettingsTable(ClientSettingDaily updatedata)
        {

            tempconnectionstring();
            Connection Connection = new Connection();

            //Connection.myparameters.Add(new myParameters { ParameterName = "@UserName", mytype = SqlDbType.NVarChar, Value = updatedata.UserName });
            Connection.myparameters.Add(new myParameters { ParameterName = "@CN", mytype = SqlDbType.NVarChar, Value = updatedata.CN });

            DataTable DT = new DataTable();
            DT = Connection.GetDataTable("sp_CRT_DisplayMainCrtSettingsTable");
            List<ClientSettingDaily> ListReturned = new List<ClientSettingDaily>();
            if (DT.Rows.Count > 0)
            {
                foreach (DataRow row in DT.Rows)
                {
                    ClientSettingDaily Displaynewusers = new ClientSettingDaily
                    {
                        ID = row["ID"].ToString(),
                        Value = row["Value"].ToString(),
                        CountDays = row["CountDays"].ToString(),
                        Monday = row["Monday"].ToString(),
                        Tuesday = row["Tuesday"].ToString(),
                        Wednesday = row["Wednesday"].ToString(),
                        Thursday = row["Thursday"].ToString(),
                        Friday = row["Friday"].ToString(),
                        Saturday = row["Saturday"].ToString(),
                        Sunday = row["Sunday"].ToString(),
                        StartDate = row["StartDate"].ToString(),
                        EndDate = row["EndDate"].ToString(),
                        StartTime = row["StartTime"].ToString(),
                        EndTime = row["EndTime"].ToString()
                    };
                    ListReturned.Add(Displaynewusers);
                }
            }

            return ListReturned;
        }
        //crt settings
        [HttpPost]
        [Route("CRT_Setting_GetCategory")]
        public List<ClientSettingDaily> CRT_Setting_GetCategory(ClientSettingDaily updatedata)
        {

            tempconnectionstring();
            Connection Connection = new Connection();

            //Connection.myparameters.Add(new myParameters { ParameterName = "@UserName", mytype = SqlDbType.NVarChar, Value = updatedata.UserName });
            Connection.myparameters.Add(new myParameters { ParameterName = "@CN", mytype = SqlDbType.NVarChar, Value = updatedata.CN });

            DataTable DT = new DataTable();
            DT = Connection.GetDataTable("SP_CRT_Setting_GetCategory");
            List<ClientSettingDaily> ListReturned = new List<ClientSettingDaily>();
            if (DT.Rows.Count > 0)
            {
                foreach (DataRow row in DT.Rows)
                {
                    ClientSettingDaily Displaynewusers = new ClientSettingDaily
                    {
                        category = row["category"].ToString(),
                        setting = row["setting"].ToString(),
                        recipient = row["recipient"].ToString()
                    };
                    ListReturned.Add(Displaynewusers);
                }
            }

            return ListReturned;
        }
        [HttpPost]
        [Route("CRT_Setting_GetDaily")]
        public List<ClientSettingDaily> CRT_Setting_GetDaily(ClientSettingDaily updatedata)
        {

            tempconnectionstring();
            Connection Connection = new Connection();

            //Connection.myparameters.Add(new myParameters { ParameterName = "@UserName", mytype = SqlDbType.NVarChar, Value = updatedata.UserName });
            Connection.myparameters.Add(new myParameters { ParameterName = "@CN", mytype = SqlDbType.NVarChar, Value = updatedata.CN });

            DataTable DT = new DataTable();
            DT = Connection.GetDataTable("SP_CRT_Setting_GetDaily");
            List<ClientSettingDaily> ListReturned = new List<ClientSettingDaily>();
            if (DT.Rows.Count > 0)
            {
                foreach (DataRow row in DT.Rows)
                {
                    ClientSettingDaily Displaynewusers = new ClientSettingDaily
                    {
                        CountDays = row["CountDays"].ToString(),
                        Monday = row["Monday"].ToString(),
                        Tuesday = row["Tuesday"].ToString(),
                        Wednesday = row["Wednesday"].ToString(),
                        Thursday = row["Thursday"].ToString(),
                        Friday = row["Friday"].ToString(),
                        Saturday = row["Saturday"].ToString(),
                        Sunday = row["Sunday"].ToString(),
                        StartDate = row["StartDate"].ToString(),
                        EndDate = row["EndDate"].ToString(),
                        StartTime = row["StartTime"].ToString(),
                        EndTime = row["EndTime"].ToString()
                    };
                    ListReturned.Add(Displaynewusers);
                }
            }

            return ListReturned;
        }
        [HttpPost]
        [Route("CRT_Setting_GetWeekly")]
        public List<ClientSettingDaily> CRT_Setting_GetWeekly(ClientSettingDaily updatedata)
        {

            tempconnectionstring();
            Connection Connection = new Connection();

            //Connection.myparameters.Add(new myParameters { ParameterName = "@UserName", mytype = SqlDbType.NVarChar, Value = updatedata.UserName });
            Connection.myparameters.Add(new myParameters { ParameterName = "@CN", mytype = SqlDbType.NVarChar, Value = updatedata.CN });

            DataTable DT = new DataTable();
            DT = Connection.GetDataTable("SP_CRT_Setting_GetWeekly");
            List<ClientSettingDaily> ListReturned = new List<ClientSettingDaily>();
            if (DT.Rows.Count > 0)
            {
                foreach (DataRow row in DT.Rows)
                {
                    ClientSettingDaily Displaynewusers = new ClientSettingDaily
                    {
                        CountWeeks = row["CountWeeks"].ToString(),
                        Monday = row["Monday"].ToString(),
                        Tuesday = row["Tuesday"].ToString(),
                        Wednesday = row["Wednesday"].ToString(),
                        Thursday = row["Thursday"].ToString(),
                        Friday = row["Friday"].ToString(),
                        Saturday = row["Saturday"].ToString(),
                        Sunday = row["Sunday"].ToString(),
                        StartDate = row["StartDate"].ToString(),
                        EndDate = row["EndDate"].ToString(),
                        StartTime = row["StartTime"].ToString(),
                        EndTime = row["EndTime"].ToString()
                    };
                    ListReturned.Add(Displaynewusers);
                }
            }

            return ListReturned;
        }
        //crt settings end


        [HttpPost]
        [Route("Mobile_Register_PersonalInformation")]
        public string Mobile_Register_PersonalInformation(MobileRegisterPersonalInfoTable RegisterPersonalInfo)
        {
            //GetDatabase GDB = new GetDatabase();
            //GDB.ClientName = ClientData.CN;
            tempconnectionstring();
            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@SiteAllocation", mytype = SqlDbType.NVarChar, Value = RegisterPersonalInfo.SiteAllocation });
            Connection.myparameters.Add(new myParameters { ParameterName = "@LastName", mytype = SqlDbType.NVarChar, Value = RegisterPersonalInfo.LastName });
            Connection.myparameters.Add(new myParameters { ParameterName = "@FirstName", mytype = SqlDbType.NVarChar, Value = RegisterPersonalInfo.FirstName });
            Connection.myparameters.Add(new myParameters { ParameterName = "@MiddleName", mytype = SqlDbType.NVarChar, Value = RegisterPersonalInfo.MiddleName });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Suffix", mytype = SqlDbType.NVarChar, Value = RegisterPersonalInfo.Suffix });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Prefix", mytype = SqlDbType.NVarChar, Value = RegisterPersonalInfo.Prefix });
            Connection.myparameters.Add(new myParameters { ParameterName = "@DateOfBirth", mytype = SqlDbType.NVarChar, Value = RegisterPersonalInfo.DateOfBirth });
            Connection.myparameters.Add(new myParameters { ParameterName = "@PlaceOfBirth", mytype = SqlDbType.NVarChar, Value = RegisterPersonalInfo.PlaceOfBirth });
            Connection.myparameters.Add(new myParameters { ParameterName = "@MobileNumber", mytype = SqlDbType.NVarChar, Value = RegisterPersonalInfo.MobileNumber });
            Connection.myparameters.Add(new myParameters { ParameterName = "@CivilStatus", mytype = SqlDbType.NVarChar, Value = RegisterPersonalInfo.CivilStatus });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Nationality", mytype = SqlDbType.NVarChar, Value = RegisterPersonalInfo.Nationality });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Gender", mytype = SqlDbType.NVarChar, Value = RegisterPersonalInfo.Gender });
            Connection.myparameters.Add(new myParameters { ParameterName = "@DateCreated", mytype = SqlDbType.NVarChar, Value = RegisterPersonalInfo.DateCreated });
            Connection.ExecuteNonQuery("sp_Mobile_Register_PersonalInformation");
            return "success";
        }
        [HttpPost]
        [Route("Mobile_Register_MothersInfo_PersonalInformation")]
        public string Mobile_Register_MothersInfo_PersonalInformation(MobileRegisterMothersInfoTable RegisterMothersInfo)
        {
            //GetDatabase GDB = new GetDatabase();
            //GDB.ClientName = ClientData.CN;
            tempconnectionstring();
            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@Mothers_FirstName", mytype = SqlDbType.NVarChar, Value = RegisterMothersInfo.Mothers_FirstName });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Mothers_MiddleName", mytype = SqlDbType.NVarChar, Value = RegisterMothersInfo.Mothers_MiddleName });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Mothers_LastName", mytype = SqlDbType.NVarChar, Value = RegisterMothersInfo.Mothers_LastName });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Mothers_Suffix", mytype = SqlDbType.NVarChar, Value = RegisterMothersInfo.Mothers_Suffix });
            Connection.myparameters.Add(new myParameters { ParameterName = "@DateCreated", mytype = SqlDbType.NVarChar, Value = RegisterMothersInfo.DateCreated });
            Connection.ExecuteNonQuery("sp_Mobile_Register_MothersInfo_PersonalInformation");
            return "success";
        }
        //[HttpPost]
        //[Route("Mobile_Login")]
        //public string Mobile_Login(CRTMobileLogin MobileLogin)
        //{
        //    string valid="";
        //    //GetDatabase GDB = new GetDatabase();
        //    //GDB.ClientName = ClientData.CN;
        //    tempconnectionstring();
        //    Connection Connection = new Connection();

        //    Connection.myparameters.Add(new myParameters { ParameterName = "@EmployeeNumber", mytype = SqlDbType.NVarChar, Value = MobileLogin.EmployeeNumber });

        //    //Connection.ExecuteNonQuery("sp_Mobile_Login");
        //    //return "success";
        //    valid = Connection.ExecuteScalar("sp_Mobile_Login").ToString();
        //    //valid = "success";
        //    return valid;
        //}

        //[HttpPost]
        //[Route("Mobile_Login")]
        //public GetCRTMobileLogin Mobile_Login(CRTMobileLogin MobileLogin)
        //{
        //    GetCRTMobileLogin GetAllGetAllLocationNotif = new GetCRTMobileLogin();

        //    tempconnectionstring();
        //    Connection Connection = new Connection();
        //    Connection.myparameters.Add(new myParameters { ParameterName = "@StartingID", mytype = SqlDbType.NVarChar, Value = MobileLogin.StartingID });         

        //    DataTable DT = new DataTable();
        //    DT = Connection.GetDataTable("sp_Mobile_Login");
        //    List<CRTMobileLogin> ListReturned = new List<CRTMobileLogin>();
        //    if (DT.Rows.Count > 0)
        //    {
        //        foreach (DataRow row in DT.Rows)
        //        {
        //            CRTMobileLogin Displaynewusers = new CRTMobileLogin
        //            {                       
        //                StartingID = row["StartingID"].ToString(),
        //                FirstName = row["FirstName"].ToString(),
        //                MiddleName = row["MiddleName"].ToString(),
        //                LastName = row["LastName"].ToString(),
        //                DateHired = row["DateHired"].ToString()

        //            };
        //            ListReturned.Add(Displaynewusers);
        //        }
        //        GetAllGetAllLocationNotif.Displayloginname = ListReturned;
        //        GetAllGetAllLocationNotif.myreturn = "Success";
        //    }
        //    return GetAllGetAllLocationNotif;
        //}

        [HttpPost]
        [Route("Mobile_Login_Company")]
        public string Mobile_Login_Company(CRTMobileLoginCompany MobileLoginCompany)
        {
            try
            {
                tempconnectionstring();
                Connection Connection = new Connection();

                Connection.myparameters.Add(new myParameters { ParameterName = "@CompanyName", mytype = SqlDbType.NVarChar, Value = MobileLoginCompany.CompanyName });
                Connection.myparameters.Add(new myParameters { ParameterName = "@StartingID", mytype = SqlDbType.NVarChar, Value = MobileLoginCompany.StartingID });
                Connection.myparameters.Add(new myParameters { ParameterName = "@FirstName", mytype = SqlDbType.NVarChar, Value = MobileLoginCompany.FirstName });
                Connection.myparameters.Add(new myParameters { ParameterName = "@MiddleName", mytype = SqlDbType.NVarChar, Value = MobileLoginCompany.MiddleName });
                Connection.myparameters.Add(new myParameters { ParameterName = "@LastName", mytype = SqlDbType.NVarChar, Value = MobileLoginCompany.LastName });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Birthday", mytype = SqlDbType.NVarChar, Value = MobileLoginCompany.Birthday });
                return Connection.ExecuteScalar("sp_Mobile_Loging_Company");
            }
            catch (Exception e)
            {
                return e.ToString();
            }

        }

        [HttpPost]
        [Route("Mobile_Login_Companyv2")]
        public string Mobile_Login_CompanyV2(CRTMobileLoginCompanyV2 MobileLoginCompany)
        {
            try
            {
                tempconnectionstring();
                Connection Connection = new Connection();

                Connection.myparameters.Add(new myParameters { ParameterName = "@CompanyName", mytype = SqlDbType.NVarChar, Value = MobileLoginCompany.CompanyName });
                Connection.myparameters.Add(new myParameters { ParameterName = "@StartingID", mytype = SqlDbType.NVarChar, Value = MobileLoginCompany.StartingID });
                Connection.myparameters.Add(new myParameters { ParameterName = "@FirstName", mytype = SqlDbType.NVarChar, Value = MobileLoginCompany.FirstName });
                Connection.myparameters.Add(new myParameters { ParameterName = "@MiddleName", mytype = SqlDbType.NVarChar, Value = MobileLoginCompany.MiddleName });
                Connection.myparameters.Add(new myParameters { ParameterName = "@LastName", mytype = SqlDbType.NVarChar, Value = MobileLoginCompany.LastName });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Birthday", mytype = SqlDbType.NVarChar, Value = MobileLoginCompany.Birthday });
                return Connection.ExecuteScalar("sp_Mobile_Loging_Companyv2");
            }
            catch (Exception e)
            {
                return e.ToString();
            }

        }

        [HttpPost]
        [Route("Mobile_Company_Verification")]
        public string Mobile_Company_Verification(CRTMobileCompanyVerification MobileCompanyVerification)
        {
            try
            {

                var settings = ConfigurationManager.ConnectionStrings["connectionstring"];
                var fi = typeof(ConfigurationElement).GetField(
                              "_bReadOnly",
                              BindingFlags.Instance | BindingFlags.NonPublic);
                fi.SetValue(settings, false);
                settings.ConnectionString = "Data Source=livedb-ilm.c3srcolvclfy.ap-southeast-1.rds.amazonaws.com,1412;Database=ILM_Main;User Id=ILM_LiveDBUser;Password=ILMV3ry53cur3dMGTpa55w0rd2020;";

                Connection Connection = new Connection();
                Connection.myparameters.Add(new myParameters { ParameterName = "@DBNAME", mytype = SqlDbType.NVarChar, Value = MobileCompanyVerification.CompanyAlias });
                var exist = Connection.ExecuteScalar("sp_IsDBExist").ToString();

                if (exist == "Existing")
                {
                    return "durusthr";
                }
                else
                {
                    tempconnectionstring();
                    Connection = new Connection();
                    Connection.myparameters.Add(new myParameters { ParameterName = "@CompanyAlias", mytype = SqlDbType.NVarChar, Value = MobileCompanyVerification.CompanyAlias });
                    var exists = Connection.ExecuteScalar("sp_Mobile_Company_Verification");
                    if (exists == "True")
                    {
                        return "crt";
                    }
                    else
                    {
                        return "none";
                    }
                }
            }

            catch (Exception e)
            {
                return e.ToString();
            }
        }

        [HttpPost]
        [Route("CRT_DeleteGenerateFiles")]
        public string DeleteGenerateFiles(ClientSettingDaily generateid)
        {
            tempconnectionstring();
            Connection Connection = new Connection();
            Connection.myparameters.Add(new myParameters { ParameterName = "@CN", mytype = SqlDbType.NVarChar, Value = generateid.CN });


            Connection.ExecuteNonQuery("sp_CRT_DeleteGenerateFiles");
            return "success";
        }
        [HttpPost]
        [Route("CRT_GenerateCRTInsertEmployee")]
        public string GenerateCRTInsertEmployee(UpdateNewUser generateid)
        {
            //string valid = "";
            //GetDatabase GDB = new GetDatabase();
            //GDB.ClientName = ClientData.CN;
            try
            {
                tempconnectionstring();
                Connection Connection = new Connection();

                Connection.myparameters.Add(new myParameters { ParameterName = "@StartingID", mytype = SqlDbType.NVarChar, Value = generateid.StartingID });
                Connection.myparameters.Add(new myParameters { ParameterName = "@CN", mytype = SqlDbType.NVarChar, Value = generateid.CN });

                Connection.ExecuteNonQuery("sp_CRT_GenerateCRTInsertEmployee");
                return "success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }

        }




        [HttpPost]
        [Route("CRT_InsertEmployeev6")]
        public string CRT_InsertEmployeev6(MobileInsertEmployeev2 InsertData)
        {
            try
            {
                tempconnectionstring();
                Connection Connection = new Connection();

                var pic1 = "";
                var pic2 = "";

                var sign1 = "";
                var sign2 = "";
                var sign3 = "";

                if (InsertData.ID1Image.Contains("https") != true)
                {
                    byte[] Photo = Convert.FromBase64String(InsertData.ID1Image);
                    var savePath = HttpContext.Current.Server.MapPath("~/ProfilePhotos/CRT/" + InsertData.CompanyName + "/" + InsertData.StartingID + "/");
                    var saveFolder = Path.Combine(savePath);


                    Directory.CreateDirectory(saveFolder);
                    pic1 = ("ProfilePhotos/CRT/" + InsertData.CompanyName + "/" + InsertData.StartingID + "/" + InsertData.StartingID + "_" + InsertData.ID1 + ".png").ToString();

                    try
                    {
                        File.WriteAllBytes(saveFolder + InsertData.StartingID + "_" + InsertData.ID1 + ".png", Photo);
                    }

                    catch (Exception e)
                    {
                        return e.ToString();
                    }
                }

                if (InsertData.ID2Image.Contains("https") != true)
                {
                    byte[] Photov2 = Convert.FromBase64String(InsertData.ID2Image);
                    var savePathv2 = HttpContext.Current.Server.MapPath("~/ProfilePhotos/CRT/" + InsertData.CompanyName + "/" + InsertData.StartingID + "/");
                    var saveFolderv2 = Path.Combine(savePathv2);

                    Directory.CreateDirectory(saveFolderv2);
                    pic2 = ("ProfilePhotos/CRT/" + InsertData.CompanyName + "/" + InsertData.StartingID + "/" + InsertData.StartingID + "_" + InsertData.ID2 + ".png").ToString();

                    try
                    {
                        File.WriteAllBytes(saveFolderv2 + InsertData.StartingID + "_" + InsertData.ID2 + ".png", Photov2);
                    }

                    catch (Exception e)
                    {
                        return e.ToString();
                    }
                }


                if (InsertData.Signature1.Contains("https") != true)
                {
                    byte[] Signav1 = Convert.FromBase64String(InsertData.Signature1);
                    var signaPathv1 = HttpContext.Current.Server.MapPath("~/ProfilePhotos/CRT/" + InsertData.CompanyName + "/" + InsertData.StartingID + "/");
                    var savesignaFolderv1 = Path.Combine(signaPathv1);

                    Directory.CreateDirectory(savesignaFolderv1);
                    sign1 = ("ProfilePhotos/CRT/" + InsertData.CompanyName + "/" + InsertData.StartingID + "/" + InsertData.StartingID + "_sign_1" + ".png").ToString();

                    try
                    {
                        File.WriteAllBytes(savesignaFolderv1 + InsertData.StartingID + "_sign_1" + ".png", Signav1);
                    }

                    catch (Exception e)
                    {
                        return e.ToString();
                    }
                }

                if (InsertData.Signature2.Contains("https") != true)
                {
                    byte[] Signav2 = Convert.FromBase64String(InsertData.Signature2);
                    var signaPathv2 = HttpContext.Current.Server.MapPath("~/ProfilePhotos/CRT/" + InsertData.CompanyName + "/" + InsertData.StartingID + "/");
                    var savesignaFolderv2 = Path.Combine(signaPathv2);

                    Directory.CreateDirectory(savesignaFolderv2);
                    sign2 = ("ProfilePhotos/CRT/" + InsertData.CompanyName + "/" + InsertData.StartingID + "/" + "_sign_2" + ".png").ToString();

                    try
                    {
                        File.WriteAllBytes(savesignaFolderv2 + InsertData.StartingID + "_sign_2" + ".png", Signav2);
                    }

                    catch (Exception e)
                    {
                        return e.ToString();
                    }
                }

                if (InsertData.Signature3.Contains("https") != true)
                {
                    byte[] Signav3 = Convert.FromBase64String(InsertData.Signature3);
                    var signaPathv3 = HttpContext.Current.Server.MapPath("~/ProfilePhotos/CRT/" + InsertData.CompanyName + "/" + InsertData.StartingID + "/");
                    var savesignaFolderv3 = Path.Combine(signaPathv3);

                    Directory.CreateDirectory(savesignaFolderv3);
                    sign3 = ("ProfilePhotos/CRT/" + InsertData.CompanyName + "/" + InsertData.StartingID + "/" + "_sign_3" + ".png").ToString();

                    try
                    {
                        File.WriteAllBytes(savesignaFolderv3 + InsertData.StartingID + "_sign_3" + ".png", Signav3);
                    }

                    catch (Exception e)
                    {
                        return e.ToString();
                    }
                }

                Connection.myparameters.Add(new myParameters { ParameterName = "@CompanyName", mytype = SqlDbType.NVarChar, Value = InsertData.CompanyName });
                Connection.myparameters.Add(new myParameters { ParameterName = "@EmployeeID", mytype = SqlDbType.NVarChar, Value = InsertData.EmployeeID });
                Connection.myparameters.Add(new myParameters { ParameterName = "@StartingID", mytype = SqlDbType.NVarChar, Value = InsertData.StartingID });
                //Connection.myparameters.Add(new myParameters { ParameterName = "@SiteAllocation", mytype = SqlDbType.NVarChar, Value = InsertData.SiteAllocation });
                Connection.myparameters.Add(new myParameters { ParameterName = "@LastName", mytype = SqlDbType.NVarChar, Value = InsertData.LastName });
                Connection.myparameters.Add(new myParameters { ParameterName = "@FirstName", mytype = SqlDbType.NVarChar, Value = InsertData.FirstName });
                Connection.myparameters.Add(new myParameters { ParameterName = "@MiddleName", mytype = SqlDbType.NVarChar, Value = InsertData.MiddleName });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Suffix", mytype = SqlDbType.NVarChar, Value = InsertData.Suffix });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Prefix", mytype = SqlDbType.NVarChar, Value = InsertData.Prefix });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Birthday", mytype = SqlDbType.NVarChar, Value = InsertData.Birthday });
                Connection.myparameters.Add(new myParameters { ParameterName = "@PlaceOfBirth", mytype = SqlDbType.NVarChar, Value = InsertData.PlaceOfBirth });
                Connection.myparameters.Add(new myParameters { ParameterName = "@MobileNumber", mytype = SqlDbType.NVarChar, Value = InsertData.MobileNumber });
                Connection.myparameters.Add(new myParameters { ParameterName = "@LandlineNumber", mytype = SqlDbType.NVarChar, Value = InsertData.LandlineNumber });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Email", mytype = SqlDbType.NVarChar, Value = InsertData.Email });
                Connection.myparameters.Add(new myParameters { ParameterName = "@CivilStatus", mytype = SqlDbType.NVarChar, Value = InsertData.CivilStatus });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Nationality", mytype = SqlDbType.NVarChar, Value = InsertData.Nationality });
                Connection.myparameters.Add(new myParameters { ParameterName = "@NationalityCountry", mytype = SqlDbType.NVarChar, Value = InsertData.NationalityCountry });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Gender", mytype = SqlDbType.NVarChar, Value = InsertData.Gender });

                Connection.myparameters.Add(new myParameters { ParameterName = "@Position", mytype = SqlDbType.NVarChar, Value = InsertData.Position });
                Connection.myparameters.Add(new myParameters { ParameterName = "@DateHired", mytype = SqlDbType.NVarChar, Value = InsertData.DateHired });
                Connection.myparameters.Add(new myParameters { ParameterName = "@EmploymentStatus", mytype = SqlDbType.NVarChar, Value = InsertData.EmploymentStatus });
                Connection.myparameters.Add(new myParameters { ParameterName = "@EndOfContract", mytype = SqlDbType.NVarChar, Value = InsertData.EndOfContract });

                Connection.myparameters.Add(new myParameters { ParameterName = "@ID1", mytype = SqlDbType.NVarChar, Value = InsertData.ID1 });
                Connection.myparameters.Add(new myParameters { ParameterName = "@ID1Image", mytype = SqlDbType.NVarChar, Value = pic1 });
                Connection.myparameters.Add(new myParameters { ParameterName = "@ID2", mytype = SqlDbType.NVarChar, Value = InsertData.ID2 });
                Connection.myparameters.Add(new myParameters { ParameterName = "@ID2Image", mytype = SqlDbType.NVarChar, Value = pic2 });

                Connection.myparameters.Add(new myParameters { ParameterName = "@PropertyType", mytype = SqlDbType.NVarChar, Value = InsertData.PropertyType });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Phase", mytype = SqlDbType.NVarChar, Value = InsertData.Phase });
                Connection.myparameters.Add(new myParameters { ParameterName = "@ZipCode", mytype = SqlDbType.NVarChar, Value = InsertData.ZipCode });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Block", mytype = SqlDbType.NVarChar, Value = InsertData.Block });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Lot", mytype = SqlDbType.NVarChar, Value = InsertData.Lot });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Street", mytype = SqlDbType.NVarChar, Value = InsertData.Street });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Village", mytype = SqlDbType.NVarChar, Value = InsertData.Village });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Subdivision", mytype = SqlDbType.NVarChar, Value = InsertData.Subdivision });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Barangay", mytype = SqlDbType.NVarChar, Value = InsertData.Barangay });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Municipality", mytype = SqlDbType.NVarChar, Value = InsertData.Municipality });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Province", mytype = SqlDbType.NVarChar, Value = InsertData.Province });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Region", mytype = SqlDbType.NVarChar, Value = InsertData.Region });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Country", mytype = SqlDbType.NVarChar, Value = InsertData.Country });
                Connection.myparameters.Add(new myParameters { ParameterName = "@BldgNo", mytype = SqlDbType.NVarChar, Value = InsertData.BldgNo });
                Connection.myparameters.Add(new myParameters { ParameterName = "@NameOfPlace", mytype = SqlDbType.NVarChar, Value = InsertData.NameOfPlace });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Wing", mytype = SqlDbType.NVarChar, Value = InsertData.Wing });
                Connection.myparameters.Add(new myParameters { ParameterName = "@UnitNo", mytype = SqlDbType.NVarChar, Value = InsertData.UnitNo });
                Connection.myparameters.Add(new myParameters { ParameterName = "@BldgName", mytype = SqlDbType.NVarChar, Value = InsertData.BldgName });
                Connection.myparameters.Add(new myParameters { ParameterName = "@FloorNo", mytype = SqlDbType.NVarChar, Value = InsertData.FloorNo });
                Connection.myparameters.Add(new myParameters { ParameterName = "@RoomAptNo", mytype = SqlDbType.NVarChar, Value = InsertData.RoomAptNo });

                Connection.myparameters.Add(new myParameters { ParameterName = "@PropertyTypeP", mytype = SqlDbType.NVarChar, Value = InsertData.PropertyTypeP });
                Connection.myparameters.Add(new myParameters { ParameterName = "@PhasePermanent", mytype = SqlDbType.NVarChar, Value = InsertData.PhasePermanent });
                Connection.myparameters.Add(new myParameters { ParameterName = "@ZipCodePermanent", mytype = SqlDbType.NVarChar, Value = InsertData.ZipCodePermanent });
                Connection.myparameters.Add(new myParameters { ParameterName = "@BlockPermanent", mytype = SqlDbType.NVarChar, Value = InsertData.BlockPermanent });
                Connection.myparameters.Add(new myParameters { ParameterName = "@LotPermanent", mytype = SqlDbType.NVarChar, Value = InsertData.LotPermanent });
                Connection.myparameters.Add(new myParameters { ParameterName = "@StreetPermanent", mytype = SqlDbType.NVarChar, Value = InsertData.StreetPermanent });
                Connection.myparameters.Add(new myParameters { ParameterName = "@VillagePermanent", mytype = SqlDbType.NVarChar, Value = InsertData.VillagePermanent });
                Connection.myparameters.Add(new myParameters { ParameterName = "@SubdivisionPermanent", mytype = SqlDbType.NVarChar, Value = InsertData.SubdivisionPermanent });
                Connection.myparameters.Add(new myParameters { ParameterName = "@BarangayPermanent", mytype = SqlDbType.NVarChar, Value = InsertData.BarangayPermanent });
                Connection.myparameters.Add(new myParameters { ParameterName = "@MunicipalityPermanent", mytype = SqlDbType.NVarChar, Value = InsertData.MunicipalityPermanent });
                Connection.myparameters.Add(new myParameters { ParameterName = "@ProvincePermanent", mytype = SqlDbType.NVarChar, Value = InsertData.ProvincePermanent });
                Connection.myparameters.Add(new myParameters { ParameterName = "@RegionPermanent", mytype = SqlDbType.NVarChar, Value = InsertData.RegionPermanent });
                Connection.myparameters.Add(new myParameters { ParameterName = "@CountryPermanent", mytype = SqlDbType.NVarChar, Value = InsertData.CountryPermanent });
                Connection.myparameters.Add(new myParameters { ParameterName = "@BldgNoPermanent", mytype = SqlDbType.NVarChar, Value = InsertData.BldgNoPermanent });
                Connection.myparameters.Add(new myParameters { ParameterName = "@NameOfPlacePermanent", mytype = SqlDbType.NVarChar, Value = InsertData.NameOfPlacePermanent });
                Connection.myparameters.Add(new myParameters { ParameterName = "@WingPermanent", mytype = SqlDbType.NVarChar, Value = InsertData.WingPermanent });
                Connection.myparameters.Add(new myParameters { ParameterName = "@UnitNoPermanent", mytype = SqlDbType.NVarChar, Value = InsertData.UnitNoPermanent });
                Connection.myparameters.Add(new myParameters { ParameterName = "@BldgNamePermanent", mytype = SqlDbType.NVarChar, Value = InsertData.BldgNamePermanent });
                Connection.myparameters.Add(new myParameters { ParameterName = "@FloorNoPermanent", mytype = SqlDbType.NVarChar, Value = InsertData.FloorNoPermanent });
                Connection.myparameters.Add(new myParameters { ParameterName = "@RoomAptNoPermanent", mytype = SqlDbType.NVarChar, Value = InsertData.RoomAptNoPermanent });

                Connection.myparameters.Add(new myParameters { ParameterName = "@First_Name", mytype = SqlDbType.NVarChar, Value = InsertData.First_Name });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Middle_Name", mytype = SqlDbType.NVarChar, Value = InsertData.Middle_Name });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Last_Name", mytype = SqlDbType.NVarChar, Value = InsertData.Last_Name });
                Connection.myparameters.Add(new myParameters { ParameterName = "@SuffixMother", mytype = SqlDbType.NVarChar, Value = InsertData.SuffixMother });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Employeetin", mytype = SqlDbType.NVarChar, Value = InsertData.EmployeeTIN });

                Connection.myparameters.Add(new myParameters { ParameterName = "@Ref1_FirstName", mytype = SqlDbType.NVarChar, Value = InsertData.Ref1_FirstName });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Ref1_MiddleName", mytype = SqlDbType.NVarChar, Value = InsertData.Ref1_MiddleName });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Ref1_LastName", mytype = SqlDbType.NVarChar, Value = InsertData.Ref1_LastName });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Ref1_MobileNumber", mytype = SqlDbType.NVarChar, Value = InsertData.Ref1_MobileNumber });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Ref2_FirstName", mytype = SqlDbType.NVarChar, Value = InsertData.Ref2_FirstName });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Ref2_MiddleName", mytype = SqlDbType.NVarChar, Value = InsertData.Ref2_MiddleName });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Ref2_LastName", mytype = SqlDbType.NVarChar, Value = InsertData.Ref2_LastName });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Ref2_MobileNumber", mytype = SqlDbType.NVarChar, Value = InsertData.Ref2_MobileNumber });

                Connection.myparameters.Add(new myParameters { ParameterName = "@Signature1", mytype = SqlDbType.NVarChar, Value = sign1 });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Signature2", mytype = SqlDbType.NVarChar, Value = sign2 });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Signature3", mytype = SqlDbType.NVarChar, Value = sign3 });

                Connection.myparameters.Add(new myParameters { ParameterName = "@Site", mytype = SqlDbType.NVarChar, Value = InsertData.Site });

                Connection.ExecuteNonQuery("sp_CRT_InsertEmployeev6");
                return "success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }

        [HttpPost]
        [Route("CRT_InsertEmployeev7")]
        public string CRT_InsertEmployeev7(MobileInsertEmployeev2 InsertData)
        {
            try
            {
                tempconnectionstring();
                Connection Connection = new Connection();

                var pic1 = "";
                var pic2 = "";

                var sign1 = "";
                var sign2 = "";
                var sign3 = "";

                if (InsertData.ID1Image.Contains("https") != true)
                {
                    byte[] Photo = Convert.FromBase64String(InsertData.ID1Image);
                    var savePath = HttpContext.Current.Server.MapPath("~/ProfilePhotos/CRT/" + InsertData.CompanyName + "/" + InsertData.StartingID + "/");
                    var saveFolder = Path.Combine(savePath);


                    Directory.CreateDirectory(saveFolder);
                    pic1 = ("ProfilePhotos/CRT/" + InsertData.CompanyName + "/" + InsertData.StartingID + "/" + InsertData.StartingID + "_" + InsertData.ID1 + ".png").ToString();

                    try
                    {
                        File.WriteAllBytes(saveFolder + InsertData.StartingID + "_" + InsertData.ID1 + ".png", Photo);
                    }

                    catch (Exception e)
                    {
                        return e.ToString();
                    }
                }

                if (InsertData.ID2Image.Contains("https") != true)
                {
                    byte[] Photov2 = Convert.FromBase64String(InsertData.ID2Image);
                    var savePathv2 = HttpContext.Current.Server.MapPath("~/ProfilePhotos/CRT/" + InsertData.CompanyName + "/" + InsertData.StartingID + "/");
                    var saveFolderv2 = Path.Combine(savePathv2);

                    Directory.CreateDirectory(saveFolderv2);
                    pic2 = ("ProfilePhotos/CRT/" + InsertData.CompanyName + "/" + InsertData.StartingID + "/" + InsertData.StartingID + "_" + InsertData.ID2 + ".png").ToString();

                    try
                    {
                        File.WriteAllBytes(saveFolderv2 + InsertData.StartingID + "_" + InsertData.ID2 + ".png", Photov2);
                    }

                    catch (Exception e)
                    {
                        return e.ToString();
                    }
                }


                if (InsertData.Signature1.Contains("https") != true)
                {
                    byte[] Signav1 = Convert.FromBase64String(InsertData.Signature1);
                    var signaPathv1 = HttpContext.Current.Server.MapPath("~/ProfilePhotos/CRT/" + InsertData.CompanyName + "/" + InsertData.StartingID + "/");
                    var savesignaFolderv1 = Path.Combine(signaPathv1);

                    Directory.CreateDirectory(savesignaFolderv1);
                    sign1 = ("ProfilePhotos/CRT/" + InsertData.CompanyName + "/" + InsertData.StartingID + "/" + InsertData.StartingID + "_sign_1" + ".png").ToString();

                    try
                    {
                        File.WriteAllBytes(savesignaFolderv1 + InsertData.StartingID + "_sign_1" + ".png", Signav1);
                    }

                    catch (Exception e)
                    {
                        return e.ToString();
                    }
                }

                if (InsertData.Signature2.Contains("https") != true)
                {
                    byte[] Signav2 = Convert.FromBase64String(InsertData.Signature2);
                    var signaPathv2 = HttpContext.Current.Server.MapPath("~/ProfilePhotos/CRT/" + InsertData.CompanyName + "/" + InsertData.StartingID + "/");
                    var savesignaFolderv2 = Path.Combine(signaPathv2);

                    Directory.CreateDirectory(savesignaFolderv2);
                    sign2 = ("ProfilePhotos/CRT/" + InsertData.CompanyName + "/" + InsertData.StartingID + "/" + "_sign_2" + ".png").ToString();

                    try
                    {
                        File.WriteAllBytes(savesignaFolderv2 + InsertData.StartingID + "_sign_2" + ".png", Signav2);
                    }

                    catch (Exception e)
                    {
                        return e.ToString();
                    }
                }

                if (InsertData.Signature3.Contains("https") != true)
                {
                    byte[] Signav3 = Convert.FromBase64String(InsertData.Signature3);
                    var signaPathv3 = HttpContext.Current.Server.MapPath("~/ProfilePhotos/CRT/" + InsertData.CompanyName + "/" + InsertData.StartingID + "/");
                    var savesignaFolderv3 = Path.Combine(signaPathv3);

                    Directory.CreateDirectory(savesignaFolderv3);
                    sign3 = ("ProfilePhotos/CRT/" + InsertData.CompanyName + "/" + InsertData.StartingID + "/" + "_sign_3" + ".png").ToString();

                    try
                    {
                        File.WriteAllBytes(savesignaFolderv3 + InsertData.StartingID + "_sign_3" + ".png", Signav3);
                    }

                    catch (Exception e)
                    {
                        return e.ToString();
                    }
                }

                Connection.myparameters.Add(new myParameters { ParameterName = "@CompanyName", mytype = SqlDbType.NVarChar, Value = InsertData.CompanyName });
                Connection.myparameters.Add(new myParameters { ParameterName = "@EmployeeID", mytype = SqlDbType.NVarChar, Value = InsertData.EmployeeID });
                Connection.myparameters.Add(new myParameters { ParameterName = "@StartingID", mytype = SqlDbType.NVarChar, Value = InsertData.StartingID });
                //Connection.myparameters.Add(new myParameters { ParameterName = "@SiteAllocation", mytype = SqlDbType.NVarChar, Value = InsertData.SiteAllocation });
                Connection.myparameters.Add(new myParameters { ParameterName = "@LastName", mytype = SqlDbType.NVarChar, Value = InsertData.LastName });
                Connection.myparameters.Add(new myParameters { ParameterName = "@FirstName", mytype = SqlDbType.NVarChar, Value = InsertData.FirstName });
                Connection.myparameters.Add(new myParameters { ParameterName = "@MiddleName", mytype = SqlDbType.NVarChar, Value = InsertData.MiddleName });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Suffix", mytype = SqlDbType.NVarChar, Value = InsertData.Suffix });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Prefix", mytype = SqlDbType.NVarChar, Value = InsertData.Prefix });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Birthday", mytype = SqlDbType.NVarChar, Value = InsertData.Birthday });
                Connection.myparameters.Add(new myParameters { ParameterName = "@PlaceOfBirth", mytype = SqlDbType.NVarChar, Value = InsertData.PlaceOfBirth });
                Connection.myparameters.Add(new myParameters { ParameterName = "@MobileNumber", mytype = SqlDbType.NVarChar, Value = InsertData.MobileNumber });
                Connection.myparameters.Add(new myParameters { ParameterName = "@LandlineNumber", mytype = SqlDbType.NVarChar, Value = InsertData.LandlineNumber });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Email", mytype = SqlDbType.NVarChar, Value = InsertData.Email });
                Connection.myparameters.Add(new myParameters { ParameterName = "@CivilStatus", mytype = SqlDbType.NVarChar, Value = InsertData.CivilStatus });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Nationality", mytype = SqlDbType.NVarChar, Value = InsertData.Nationality });
                Connection.myparameters.Add(new myParameters { ParameterName = "@NationalityCountry", mytype = SqlDbType.NVarChar, Value = InsertData.NationalityCountry });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Gender", mytype = SqlDbType.NVarChar, Value = InsertData.Gender });

                Connection.myparameters.Add(new myParameters { ParameterName = "@Position", mytype = SqlDbType.NVarChar, Value = InsertData.Position });
                Connection.myparameters.Add(new myParameters { ParameterName = "@DateHired", mytype = SqlDbType.NVarChar, Value = InsertData.DateHired });
                Connection.myparameters.Add(new myParameters { ParameterName = "@EmploymentStatus", mytype = SqlDbType.NVarChar, Value = InsertData.EmploymentStatus });
                Connection.myparameters.Add(new myParameters { ParameterName = "@EndOfContract", mytype = SqlDbType.NVarChar, Value = InsertData.EndOfContract });

                Connection.myparameters.Add(new myParameters { ParameterName = "@ID1", mytype = SqlDbType.NVarChar, Value = InsertData.ID1 });
                Connection.myparameters.Add(new myParameters { ParameterName = "@ID1Image", mytype = SqlDbType.NVarChar, Value = pic1 });
                Connection.myparameters.Add(new myParameters { ParameterName = "@ID2", mytype = SqlDbType.NVarChar, Value = InsertData.ID2 });
                Connection.myparameters.Add(new myParameters { ParameterName = "@ID2Image", mytype = SqlDbType.NVarChar, Value = pic2 });

                Connection.myparameters.Add(new myParameters { ParameterName = "@PropertyType", mytype = SqlDbType.NVarChar, Value = InsertData.PropertyType });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Phase", mytype = SqlDbType.NVarChar, Value = InsertData.Phase });
                Connection.myparameters.Add(new myParameters { ParameterName = "@ZipCode", mytype = SqlDbType.NVarChar, Value = InsertData.ZipCode });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Block", mytype = SqlDbType.NVarChar, Value = InsertData.Block });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Lot", mytype = SqlDbType.NVarChar, Value = InsertData.Lot });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Street", mytype = SqlDbType.NVarChar, Value = InsertData.Street });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Village", mytype = SqlDbType.NVarChar, Value = InsertData.Village });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Subdivision", mytype = SqlDbType.NVarChar, Value = InsertData.Subdivision });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Barangay", mytype = SqlDbType.NVarChar, Value = InsertData.Barangay });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Municipality", mytype = SqlDbType.NVarChar, Value = InsertData.Municipality });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Province", mytype = SqlDbType.NVarChar, Value = InsertData.Province });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Region", mytype = SqlDbType.NVarChar, Value = InsertData.Region });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Country", mytype = SqlDbType.NVarChar, Value = InsertData.Country });
                Connection.myparameters.Add(new myParameters { ParameterName = "@BldgNo", mytype = SqlDbType.NVarChar, Value = InsertData.BldgNo });
                Connection.myparameters.Add(new myParameters { ParameterName = "@NameOfPlace", mytype = SqlDbType.NVarChar, Value = InsertData.NameOfPlace });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Wing", mytype = SqlDbType.NVarChar, Value = InsertData.Wing });
                Connection.myparameters.Add(new myParameters { ParameterName = "@UnitNo", mytype = SqlDbType.NVarChar, Value = InsertData.UnitNo });
                Connection.myparameters.Add(new myParameters { ParameterName = "@BldgName", mytype = SqlDbType.NVarChar, Value = InsertData.BldgName });
                Connection.myparameters.Add(new myParameters { ParameterName = "@FloorNo", mytype = SqlDbType.NVarChar, Value = InsertData.FloorNo });
                Connection.myparameters.Add(new myParameters { ParameterName = "@RoomAptNo", mytype = SqlDbType.NVarChar, Value = InsertData.RoomAptNo });

                Connection.myparameters.Add(new myParameters { ParameterName = "@PropertyTypeP", mytype = SqlDbType.NVarChar, Value = InsertData.PropertyTypeP });
                Connection.myparameters.Add(new myParameters { ParameterName = "@PhasePermanent", mytype = SqlDbType.NVarChar, Value = InsertData.PhasePermanent });
                Connection.myparameters.Add(new myParameters { ParameterName = "@ZipCodePermanent", mytype = SqlDbType.NVarChar, Value = InsertData.ZipCodePermanent });
                Connection.myparameters.Add(new myParameters { ParameterName = "@BlockPermanent", mytype = SqlDbType.NVarChar, Value = InsertData.BlockPermanent });
                Connection.myparameters.Add(new myParameters { ParameterName = "@LotPermanent", mytype = SqlDbType.NVarChar, Value = InsertData.LotPermanent });
                Connection.myparameters.Add(new myParameters { ParameterName = "@StreetPermanent", mytype = SqlDbType.NVarChar, Value = InsertData.StreetPermanent });
                Connection.myparameters.Add(new myParameters { ParameterName = "@VillagePermanent", mytype = SqlDbType.NVarChar, Value = InsertData.VillagePermanent });
                Connection.myparameters.Add(new myParameters { ParameterName = "@SubdivisionPermanent", mytype = SqlDbType.NVarChar, Value = InsertData.SubdivisionPermanent });
                Connection.myparameters.Add(new myParameters { ParameterName = "@BarangayPermanent", mytype = SqlDbType.NVarChar, Value = InsertData.BarangayPermanent });
                Connection.myparameters.Add(new myParameters { ParameterName = "@MunicipalityPermanent", mytype = SqlDbType.NVarChar, Value = InsertData.MunicipalityPermanent });
                Connection.myparameters.Add(new myParameters { ParameterName = "@ProvincePermanent", mytype = SqlDbType.NVarChar, Value = InsertData.ProvincePermanent });
                Connection.myparameters.Add(new myParameters { ParameterName = "@RegionPermanent", mytype = SqlDbType.NVarChar, Value = InsertData.RegionPermanent });
                Connection.myparameters.Add(new myParameters { ParameterName = "@CountryPermanent", mytype = SqlDbType.NVarChar, Value = InsertData.CountryPermanent });
                Connection.myparameters.Add(new myParameters { ParameterName = "@BldgNoPermanent", mytype = SqlDbType.NVarChar, Value = InsertData.BldgNoPermanent });
                Connection.myparameters.Add(new myParameters { ParameterName = "@NameOfPlacePermanent", mytype = SqlDbType.NVarChar, Value = InsertData.NameOfPlacePermanent });
                Connection.myparameters.Add(new myParameters { ParameterName = "@WingPermanent", mytype = SqlDbType.NVarChar, Value = InsertData.WingPermanent });
                Connection.myparameters.Add(new myParameters { ParameterName = "@UnitNoPermanent", mytype = SqlDbType.NVarChar, Value = InsertData.UnitNoPermanent });
                Connection.myparameters.Add(new myParameters { ParameterName = "@BldgNamePermanent", mytype = SqlDbType.NVarChar, Value = InsertData.BldgNamePermanent });
                Connection.myparameters.Add(new myParameters { ParameterName = "@FloorNoPermanent", mytype = SqlDbType.NVarChar, Value = InsertData.FloorNoPermanent });
                Connection.myparameters.Add(new myParameters { ParameterName = "@RoomAptNoPermanent", mytype = SqlDbType.NVarChar, Value = InsertData.RoomAptNoPermanent });

                Connection.myparameters.Add(new myParameters { ParameterName = "@First_Name", mytype = SqlDbType.NVarChar, Value = InsertData.First_Name });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Middle_Name", mytype = SqlDbType.NVarChar, Value = InsertData.Middle_Name });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Last_Name", mytype = SqlDbType.NVarChar, Value = InsertData.Last_Name });
                Connection.myparameters.Add(new myParameters { ParameterName = "@SuffixMother", mytype = SqlDbType.NVarChar, Value = InsertData.SuffixMother });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Employeetin", mytype = SqlDbType.NVarChar, Value = InsertData.EmployeeTIN });

                Connection.myparameters.Add(new myParameters { ParameterName = "@Ref1_FirstName", mytype = SqlDbType.NVarChar, Value = InsertData.Ref1_FirstName });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Ref1_MiddleName", mytype = SqlDbType.NVarChar, Value = InsertData.Ref1_MiddleName });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Ref1_LastName", mytype = SqlDbType.NVarChar, Value = InsertData.Ref1_LastName });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Ref1_MobileNumber", mytype = SqlDbType.NVarChar, Value = InsertData.Ref1_MobileNumber });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Ref2_FirstName", mytype = SqlDbType.NVarChar, Value = InsertData.Ref2_FirstName });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Ref2_MiddleName", mytype = SqlDbType.NVarChar, Value = InsertData.Ref2_MiddleName });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Ref2_LastName", mytype = SqlDbType.NVarChar, Value = InsertData.Ref2_LastName });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Ref2_MobileNumber", mytype = SqlDbType.NVarChar, Value = InsertData.Ref2_MobileNumber });

                Connection.myparameters.Add(new myParameters { ParameterName = "@Signature1", mytype = SqlDbType.NVarChar, Value = sign1 });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Signature2", mytype = SqlDbType.NVarChar, Value = sign2 });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Signature3", mytype = SqlDbType.NVarChar, Value = sign3 });

                Connection.myparameters.Add(new myParameters { ParameterName = "@Site", mytype = SqlDbType.NVarChar, Value = InsertData.Site });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Rank", mytype = SqlDbType.NVarChar, Value = InsertData.Rank });

                Connection.myparameters.Add(new myParameters { ParameterName = "@NumberofPads", mytype = SqlDbType.NVarChar, Value = InsertData.NumberofPads });

                Connection.ExecuteNonQuery("sp_CRT_InsertEmployeev7");
                return "success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }

        [HttpPost]
        [Route("CRT_OnboardingValidation")]
        public string OnboardValidate(GetEmployeeID Params)
        {
            try
            {
                tempconnectionstring();
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@CompanyAlias", mytype = SqlDbType.VarChar, Value = Params.CompanyAlias });
                con.myparameters.Add(new myParameters { ParameterName = "@EmployeeID", mytype = SqlDbType.VarChar, Value = Params.EmployeeID });
                return con.ExecuteScalar("sp_CRT_OnboardingValidation");
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }

        [HttpPost]
        [Route("CRTCheckEmail")]
        public string CRTCheckEmail(CRTCheckEmail Params)
        {
            try
            {
                tempconnectionstring();
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@CompanyAlias", mytype = SqlDbType.VarChar, Value = Params.CompanyAlias });
                con.myparameters.Add(new myParameters { ParameterName = "@EmployeeID", mytype = SqlDbType.VarChar, Value = Params.EmployeeID });
                con.myparameters.Add(new myParameters { ParameterName = "@Email", mytype = SqlDbType.VarChar, Value = Params.Email });
                return con.ExecuteScalar("sp_checkPersonalEmail").ToString();
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }

        [HttpPost]
        [Route("CRT_CompanyCheck")]
        public string CRT_CompanyCheck(GenerateompanyCheck generatecompany)
        {
            string valid = "";
            //GetDatabase GDB = new GetDatabase();
            //GDB.ClientName = ClientData.CN;
            tempconnectionstring();
            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@CompanyName", mytype = SqlDbType.NVarChar, Value = generatecompany.CompanyName });

            Connection.ExecuteNonQuery("sp_CRT_CompanyCheck");
            return "success";
        }

        [HttpPost]
        [Route("CRT_Count_CRTSTatus")]
        public string Count_CRTStatus()
        {
            string valid = "";
            tempconnectionstring();
            Connection Connection = new Connection();

            //Connection.ExecuteNonQuery("sp_CRT_Count_CRTStatus");
            //return "success";
            valid = Connection.ExecuteScalar("sp_CRT_Count_CRTStatus").ToString();
            return valid;
        }

        [HttpPost]
        [Route("CRT_Count_DataEntryStatus")]
        public string Count_DataEntryStatus()
        {
            string valid = "";
            tempconnectionstring();
            Connection Connection = new Connection();

            //Connection.ExecuteNonQuery("sp_CRT_Count_DataEntryStatus");
            //return "success";
            valid = Connection.ExecuteScalar("sp_CRT_Count_DataEntryStatus").ToString();
            return valid;
        }

        [HttpPost]
        [Route("CRT_GenerateCount")]
        public string CRT_GenerateCount(CRTGenerateCount GenerateCount)
        {
            string valid = "";
            tempconnectionstring();
            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@CompanyName", mytype = SqlDbType.NVarChar, Value = GenerateCount.CompanyName });

            //Connection.ExecuteNonQuery("sp_CRT_GenerateCount");
            //return "success";
            valid = Connection.ExecuteScalar("sp_CRT_GenerateCount").ToString();
            return valid;
        }

        [HttpPost]
        [Route("Mobile_ZipCodeSearch")]
        public GetMobileZipCode Mobile_ZipCodeSearch(MobileZipCodeSearch MobileZipSearch)
        {
            GetMobileZipCode GetZipCodeNotification = new GetMobileZipCode();

            tempconnectionstring();
            Connection Connection = new Connection();
            Connection.myparameters.Add(new myParameters { ParameterName = "@ZipCode", mytype = SqlDbType.NVarChar, Value = MobileZipSearch.ZipCode });

            DataTable DT = new DataTable();
            DT = Connection.GetDataTable("sp_Mobile_ZipCodeSearch");
            List<MobileZipCodeSearch> ListReturned = new List<MobileZipCodeSearch>();
            if (DT.Rows.Count > 0)
            {
                foreach (DataRow row in DT.Rows)
                {
                    MobileZipCodeSearch Displaynewusers = new MobileZipCodeSearch
                    {
                        ZipCode = row["ZipCode"].ToString(),
                        Country = row["Country"].ToString(),
                        Region = row["Region"].ToString(),
                        Province = row["Province"].ToString(),
                        Municipal = row["Municipal"].ToString(),
                        ProvCode = row["ProvCode"].ToString()

                    };
                    ListReturned.Add(Displaynewusers);
                }
                GetZipCodeNotification.Displayzipcode = ListReturned;
                GetZipCodeNotification.myreturn = "Success";
            }
            return GetZipCodeNotification;
        }

        #region Added By Akram
        [HttpPost]
        [Route("CRTSendNewClientEmail")]
        public string SendNewClientEmail(SendNewClientEmail SNCE)
        {
            try
            {
                tempconnectionstring();

                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@UserName", mytype = SqlDbType.NVarChar, Value = SNCE.UserName });
                DataTable DT = con.GetDataTable("sp_CRT_SendEmailToClient");

                if (DT.Rows.Count > 0)
                {
                    var message = new MimeKit.MimeMessage();
                    message.From.Add(new MimeKit.MailboxAddress("DurustHR", "durusthr@illimitado.com"));
                    message.To.Add(new MimeKit.MailboxAddress(DT.Rows[0]["Email"].ToString()));
                    message.Subject = "Notifications";
                    var messBody = "<label>Welcome to DurustHR CRT Admin portal, in parternship with Security Bank. Thank you for using DurustHR.<br>" +
                        "Feel free to visit http://sbccrt.durusthr.com to login to your account.<br><br>" +
                        "Here are your account details:<br>" +
                        "<b>Company Name: </b>" + DT.Rows[0]["CompanyName"].ToString() + "<br>" +
                        "<b>Company Alias: </b>" + DT.Rows[0]["CompanyAlias"].ToString() + "<br>" +
                        "<b>Username: </b>" + DT.Rows[0]["UserName"].ToString() + "<br>" +
                        "<b>Temporary Password: </b>" + DT.Rows[0]["Password"].ToString() + "<br><br>" +
                        "Access http://sbccrt.durusthr.com anytime and anywhere with Chrome. Enjoy your account.</label>";
                    message.Body = new MimeKit.TextPart("html") { Text = messBody };

                    using (var client = new MailKit.Net.Smtp.SmtpClient())
                    {
                        client.ServerCertificateValidationCallback = (s, c, h, e) => true;
                        client.Connect("box1256.bluehost.com", 465, true);
                        client.Authenticate("durusthr@illimitado.com", "@1230Qwerty");
                        client.Send(message);
                        client.Disconnect(true);
                    }
                }
                return "Success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }

        [HttpPost]
        [Route("CRTForgotPassword")]
        public string ForgotPassword(ForgotPassword FP)
        {
            try
            {
                tempconnectionstring();
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@EmailAddress", mytype = SqlDbType.VarChar, Value = FP.Email });
                var RanCode = con.ExecuteScalar("sp_CRT_ForgotPassword_Code");
                var message = new MimeKit.MimeMessage();
                message.From.Add(new MimeKit.MailboxAddress("password.recovery@illimitado.com"));
                message.To.Add(new MimeKit.MailboxAddress(FP.Email));
                message.Subject = "Reset Password";
                var messBody = "Please use this generated code as your temporary password: " + RanCode;
                message.Body = new MimeKit.TextPart("html") { Text = messBody };

                using (var client = new MailKit.Net.Smtp.SmtpClient())
                {
                    client.ServerCertificateValidationCallback = (s, c, h, e) => true;
                    client.Connect("mail.illimitado.com", 465, true);
                    client.Authenticate("password.recovery@illimitado.com", "p@ssw0rd1LM");
                    client.Send(message);
                    client.Disconnect(true);
                }
                return "Success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }

        [HttpPost]
        [Route("GenerateSaladPDFForms")]
        public string MergePDF(SaladForms SF)
        {
            try
            {
                DataTable DTSI = new DataTable();
                DTSI.Columns.AddRange(new DataColumn[1] { new DataColumn("StartingID") });
                for (int i = 0; i < SF.StartingIDs.Length; i++)
                {
                    DTSI.Rows.Add(SF.StartingIDs[i]);
                }

                var MergedPDF = HttpContext.Current.Server.MapPath("~/pdf/") + "Salary_Credit_Agreement_Form.pdf";
                using (FileStream stream = new FileStream(MergedPDF, FileMode.Create))
                {
                    Document document = new Document();
                    PdfCopy pdf = new PdfCopy(document, stream);
                    PdfReader newreader = null;
                    try
                    {
                        document.Open();
                        tempconnectionstring();
                        Connection con = new Connection();
                        con.myparameters.Add(new myParameters { ParameterName = "@CN", mytype = SqlDbType.VarChar, Value = SF.CN });
                        con.myparameters.Add(new myParameters { ParameterName = "@STARTINGIDS", mytype = SqlDbType.Structured, Value = DTSI });
                        DataTable DT = con.GetDataTable("sp_CRT_GenerateSalad_Forms");
                        foreach (DataRow row in DT.Rows)
                        {
                            #region Generate SALAD FILE
                            var SALADFILE = "Salary_Credit_Agreement_Form_-_DIRECT_2016_10_06_No_MoA_New";
                            PdfReader reader = new PdfReader(HttpContext.Current.Server.MapPath("~/pdf/") + SALADFILE + ".pdf");
                            var newFile = HttpContext.Current.Server.MapPath("pdf") + "/" + "Salary_Credit_Agreement_Form_" + "ToBeMerged" + ".pdf";
                            PdfStamper stamper = new PdfStamper(reader, new FileStream(newFile, FileMode.Create));
                            AcroFields fields = stamper.AcroFields;

                            // Page 3
                            fields.SetField("id1", row["id1"].ToString());

                            Byte[] ID1Bit = null;
                            Image ID1 = null;
                            var ID1ContentByte = stamper.GetOverContent(3); // 3 represents page number
                            AcroFields.FieldPosition fieldpos = fields.GetFieldPositions("img1")[0];
                            Rectangle rect = fieldpos.position;
                            float xxxx = rect.Left;
                            float yyyy = rect.Bottom;
                            try
                            {
                                string img1url = "http://ws.durusthr.com/ilm_ws_live/" + row["img1"].ToString();
                                using (var webClient = new WebClient())
                                {
                                    ID1Bit = webClient.DownloadData(img1url);
                                }
                                ID1 = Image.GetInstance(ID1Bit); // convert to byte array
                                ID1.SetAbsolutePosition(xxxx, yyyy); // set position inside field to place picture 
                                ID1.ScaleAbsolute(rect); // set absolute size of image as per size of field itself
                                ID1ContentByte.AddImage(ID1); // add image to field
                            }
                            catch (Exception)
                            {

                            }


                            fields.SetField("id2", row["id2"].ToString());

                            Byte[] ID2Bit = null;
                            Image ID2 = null;
                            var ID2ContentByte = stamper.GetOverContent(3); // 3 represents page number
                            fieldpos = fields.GetFieldPositions("img2")[0];
                            rect = fieldpos.position;
                            xxxx = rect.Left;
                            yyyy = rect.Bottom;
                            try
                            {
                                string img2url = "http://ws.durusthr.com/ilm_ws_live/" + row["img2"].ToString();
                                using (var webClient = new WebClient())
                                {
                                    ID2Bit = webClient.DownloadData(img2url);
                                }
                                ID2 = Image.GetInstance(ID2Bit); // convert to byte array
                                ID2.SetAbsolutePosition(xxxx, yyyy); // set position inside field to place picture 
                                ID2.ScaleAbsolute(rect); // set absolute size of image as per size of field itself
                                ID2ContentByte.AddImage(ID2); // add image to field
                            }
                            catch (Exception)
                            {

                            }


                            // Page 2
                            Byte[] EmpsignBit = null;
                            Image Empsign = null;
                            var EmpsignContentByte = stamper.GetOverContent(2); // 2 represents page number
                            fieldpos = fields.GetFieldPositions("signature")[0];
                            rect = fieldpos.position;
                            xxxx = rect.Left;
                            yyyy = rect.Bottom;
                            //string signpath = row["Signature"].ToString().Substring(0, (row["Signature"].ToString().Length) - 11) + row["StartingID"].ToString() + "_sign_1.png";
                            //EmpsignBit = File.ReadAllBytes(HttpContext.Current.Server.MapPath("ProfilePhotos") + signpath); // get image
                            try
                            {
                                string someUrl = "http://ws.durusthr.com/ilm_ws_live/" + row["Signature"].ToString();
                                using (var webClient = new WebClient())
                                {
                                    EmpsignBit = webClient.DownloadData(someUrl);
                                }
                                Empsign = Image.GetInstance(EmpsignBit); // convert to byte array
                                Empsign.SetAbsolutePosition(xxxx, yyyy); // set position inside field to place picture 
                                Empsign.ScaleAbsolute(rect); // set absolute size of image as per size of field itself
                                EmpsignContentByte.AddImage(Empsign); // add image to field
                            }
                            catch (Exception)
                            {

                            }

                            fields.SetField("printedname", row["FirstName"].ToString() + " " + row["MiddleName"].ToString() + ' ' + row["LastName"].ToString());

                            // Page 1
                            fields.SetField("lastName", row["LastName"].ToString());
                            fields.SetField("firstName", row["FirstName"].ToString());
                            fields.SetField("middleName", row["MiddleName"].ToString());
                            fields.SetField("birthMonth", row["birthMonth"].ToString());
                            fields.SetField("birthDay", row["birthDay"].ToString());
                            fields.SetField("birthYear", row["birthYear"].ToString());
                            fields.SetField("placeOfBirth", row["PlaceOfBirth"].ToString());
                            if (row["Gender"].ToString() == "Male")
                            {
                                fields.SetField("maleCheck", "Yes", true); // checkbox options : "Yes":"No", true
                            }
                            else
                            {
                                fields.SetField("femaleCheck", "Yes", true); // checkbox options : "Yes":"No", true
                            }
                            fields.SetField("citizenship", row["Nationality"].ToString());

                            if (row["CivilStatus"].ToString() == "Single")
                            {
                                fields.SetField("singleCheck", "Yes", true); // checkbox options : "Yes":"No", true
                            }
                            else if (row["CivilStatus"].ToString() == "Married")
                            {
                                fields.SetField("marriedCheck", "Yes", true); // checkbox options : "Yes":"No", true
                            }
                            else if (row["CivilStatus"].ToString() == "Legally Separated")
                            {
                                fields.SetField("legallySeparatedCheck", "Yes", true); // checkbox options : "Yes":"No", true
                            }
                            else if (row["CivilStatus"].ToString() == "Annulled")
                            {
                                fields.SetField("annulledCheck", "Yes", true); // checkbox options : "Yes":"No", true
                            }
                            else if (row["CivilStatus"].ToString() == "Widow")
                            {
                                fields.SetField("widowCheck", "Yes", true); // checkbox options : "Yes":"No", true
                            }
                            fields.SetField("tinSSSgsis", row["EmployeeTIN"].ToString());
                            fields.SetField("presentHomeAddress", row["PresentHomeAddress"].ToString());
                            fields.SetField("permanentHomeAddress", row["PermanentHomeAddress"].ToString());
                            fields.SetField("landlinePresent", row["LandlinePresent"].ToString());
                            fields.SetField("landlinePermanent", row["LandlinePermanent"].ToString());
                            fields.SetField("mobileRequired", row["MobileNoRequired"].ToString());
                            fields.SetField("mobileAlternative", row["MobileNoAlternative"].ToString());
                            fields.SetField("emailPersonal", row["EmailPersonal"].ToString());
                            fields.SetField("emailWork", row["EmailWork"].ToString());
                            fields.SetField("employerName", row["CompanyName"].ToString());
                            fields.SetField("occupation", row["Position"].ToString());
                            fields.SetField("dateOfHire", row["DateHired"].ToString());
                            fields.SetField("salaryCheck", "Yes", true); // checkbox options : "Yes":"No", true // always checked
                            if (row["EmploymentStatus"].ToString() == "Permanent/Regular")
                            {
                                fields.SetField("permanentEmploymentCheck", "Yes", true); // checkbox options : "Yes":"No", true
                                fields.SetField("contractualDate", ""); // shows if contractualEmployment is checked: contains contract end date
                            }
                            else if (row["EmploymentStatus"].ToString() == "Contractual/Project Hire")
                            {
                                fields.SetField("contractualEmploymentCheck", "Yes", true); // checkbox options : "Yes":"No", true
                                fields.SetField("contractualDate", row["EndOfContract"].ToString()); // shows if contractualEmployment is checked: contains contract end date
                            }
                            else
                            {
                                fields.SetField("othersEmploymentCheck", "Yes", true); // checkbox options : "Yes":"No", true
                                fields.SetField("othersEmploymentStatus", row["EmploymentStatus"].ToString());
                            }
                            fields.SetField("referencePerson1", row["Reference1Name"].ToString());
                            fields.SetField("referencePerson2", row["Reference2Name"].ToString());
                            fields.SetField("referenceContact1", row["Reference1Contact"].ToString());
                            fields.SetField("referenceContact2", row["Reference2Contact"].ToString());

                            if (row["Rank"].ToString() == "Staff/Rank and File")
                            {
                                fields.SetField("rankAndFile", "Yes", true); // checkbox options : "Yes":"No", true
                            }
                            else if (row["Rank"].ToString() == "Supervisor")
                            {
                                fields.SetField("supervisor", "Yes", true); // checkbox options : "Yes":"No", true
                            }
                            else if (row["Rank"].ToString() == "Manager/Junior Officer")
                            {
                                fields.SetField("manager", "Yes", true); // checkbox options : "Yes":"No", true
                            }
                            else if (row["Rank"].ToString() == "Executives")
                            {
                                fields.SetField("executive", "Yes", true); // checkbox options : "Yes":"No", true
                            }

                            fields.SetField("Site", row["Branch"].ToString());



                            stamper.FormFlattening = true;
                            stamper.Close();
                            #endregion
                            newreader = new PdfReader(HttpContext.Current.Server.MapPath("~/pdf/") + "Salary_Credit_Agreement_Form_" + "ToBeMerged" + ".pdf");
                            pdf.AddDocument(newreader);
                            newreader.Close();
                        }
                    }
                    catch (Exception)
                    {
                        if (newreader != null)
                        {
                            newreader.Close();
                        }
                    }
                    finally
                    {
                        if (document != null)
                        {
                            document.Close();
                        }
                    }
                }
                return "http://ws.durusthr.com/ilm_ws_live/pdf/" + "Salary_Credit_Agreement_Form.pdf";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }

        [HttpPost]
        [Route("CRTSaladEmailSwitchStatus")]
        public string SaladEmailSwitchStatus()
        {
            try
            {
                tempconnectionstring();
                Connection con = new Connection();
                return con.ExecuteScalar("sp_GetSaladEmailSwitchStatus").ToString();
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }

        [HttpPost]
        [Route("CRTSaladEmailSwitch")]
        public string SaladEmailSwitch()
        {
            try
            {
                tempconnectionstring();
                Connection con = new Connection();
                return con.ExecuteScalar("sp_GetSaladEmailSwitch").ToString();
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }

        [HttpPost]
        [Route("GenerateAndSendSALAD")]
        public string GenerateAndSendSALAD(SaladParams SP)
        {
            try
            {
                tempconnectionstring();
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@StartingID", mytype = SqlDbType.VarChar, Value = SP.StartingID });
                con.myparameters.Add(new myParameters { ParameterName = "@CN", mytype = SqlDbType.VarChar, Value = SP.CN });
                DataTable DT = con.GetDataTable("sp_CRT_GenerateSalad");
                foreach (DataRow row in DT.Rows)
                {
                    #region Generate SALAD FILE
                    var SALADFILE = "Salary_Credit_Agreement_Form_-_DIRECT_2016_10_06_No_MoA";
                    PdfReader reader = new PdfReader(HttpContext.Current.Server.MapPath("~/pdf/") + SALADFILE + ".pdf");
                    var newFile = HttpContext.Current.Server.MapPath("pdf") + "/" + "Salary_Credit_Agreement_Form_" + SP.StartingID + ".pdf";
                    PdfStamper stamper = new PdfStamper(reader, new FileStream(newFile, FileMode.Create));
                    AcroFields fields = stamper.AcroFields;



                    // Page 3
                    fields.SetField("id1", row["id1"].ToString());

                    Byte[] ID1Bit = null;
                    Image ID1 = null;
                    var ID1ContentByte = stamper.GetOverContent(3); // 3 represents page number
                    AcroFields.FieldPosition fieldpos = fields.GetFieldPositions("img1")[0];
                    Rectangle rect = fieldpos.position;
                    float xxxx = rect.Left;
                    float yyyy = rect.Bottom;
                    try
                    {
                        string img1url = "http://ws.durusthr.com/ilm_ws_live/" + row["img1"].ToString();
                        using (var webClient = new WebClient())
                        {
                            ID1Bit = webClient.DownloadData(img1url);
                        }
                        ID1 = Image.GetInstance(ID1Bit); // convert to byte array
                        ID1.SetAbsolutePosition(xxxx, yyyy); // set position inside field to place picture 
                        ID1.ScaleAbsolute(rect); // set absolute size of image as per size of field itself
                        ID1ContentByte.AddImage(ID1); // add image to field
                    }
                    catch (Exception)
                    {

                    }

                    fields.SetField("id2", row["id2"].ToString());

                    Byte[] ID2Bit = null;
                    Image ID2 = null;
                    var ID2ContentByte = stamper.GetOverContent(3); // 3 represents page number
                    fieldpos = fields.GetFieldPositions("img2")[0];
                    rect = fieldpos.position;
                    xxxx = rect.Left;
                    yyyy = rect.Bottom;
                    try
                    {
                        string img2url = "http://ws.durusthr.com/ilm_ws_live/" + row["img2"].ToString();
                        using (var webClient = new WebClient())
                        {
                            ID2Bit = webClient.DownloadData(img2url);
                        }
                        ID2 = Image.GetInstance(ID2Bit); // convert to byte array
                        ID2.SetAbsolutePosition(xxxx, yyyy); // set position inside field to place picture 
                        ID2.ScaleAbsolute(rect); // set absolute size of image as per size of field itself
                        ID2ContentByte.AddImage(ID2); // add image to field
                    }
                    catch (Exception)
                    {

                    }

                    // Page 2
                    Byte[] EmpsignBit = null;
                    Image Empsign = null;
                    var EmpsignContentByte = stamper.GetOverContent(2); // 2 represents page number
                    fieldpos = fields.GetFieldPositions("signature")[0];
                    rect = fieldpos.position;
                    xxxx = rect.Left;
                    yyyy = rect.Bottom;
                    try
                    {
                        string someUrl = "http://ws.durusthr.com/ilm_ws_live/" + row["Signature"].ToString();
                        using (var webClient = new WebClient())
                        {
                            EmpsignBit = webClient.DownloadData(someUrl); // get image
                        }
                        Empsign = Image.GetInstance(EmpsignBit); // convert to byte array
                        Empsign.SetAbsolutePosition(xxxx, yyyy); // set position inside field to place picture 
                        Empsign.ScaleAbsolute(rect); // set absolute size of image as per size of field itself
                        EmpsignContentByte.AddImage(Empsign); // add image to field
                    }
                    catch (Exception)
                    {

                    }

                    fields.SetField("printedname", row["FirstName"].ToString() + " " + row["MiddleName"].ToString() + ' ' + row["LastName"].ToString());


                    // Page 1
                    fields.SetField("lastName", row["LastName"].ToString());
                    fields.SetField("firstName", row["FirstName"].ToString());
                    fields.SetField("middleName", row["MiddleName"].ToString());
                    fields.SetField("birthMonth", row["birthMonth"].ToString());
                    fields.SetField("birthDay", row["birthDay"].ToString());
                    fields.SetField("birthYear", row["birthYear"].ToString());
                    fields.SetField("placeOfBirth", row["PlaceOfBirth"].ToString());
                    if (row["Gender"].ToString() == "Male")
                    {
                        fields.SetField("maleCheck", "Yes", true); // checkbox options : "Yes":"No", true
                    }
                    else
                    {
                        fields.SetField("femaleCheck", "Yes", true); // checkbox options : "Yes":"No", true
                    }
                    fields.SetField("citizenship", row["Nationality"].ToString());

                    if (row["CivilStatus"].ToString() == "Single")
                    {
                        fields.SetField("singleCheck", "Yes", true); // checkbox options : "Yes":"No", true
                    }
                    else if (row["CivilStatus"].ToString() == "Married")
                    {
                        fields.SetField("marriedCheck", "Yes", true); // checkbox options : "Yes":"No", true
                    }
                    else if (row["CivilStatus"].ToString() == "Legally Separated")
                    {
                        fields.SetField("legallySeparatedCheck", "Yes", true); // checkbox options : "Yes":"No", true
                    }
                    else if (row["CivilStatus"].ToString() == "Annulled")
                    {
                        fields.SetField("annulledCheck", "Yes", true); // checkbox options : "Yes":"No", true
                    }
                    else if (row["CivilStatus"].ToString() == "Widow")
                    {
                        fields.SetField("widowCheck", "Yes", true); // checkbox options : "Yes":"No", true
                    }
                    fields.SetField("tinSSSgsis", row["EmployeeTIN"].ToString());
                    fields.SetField("presentHomeAddress", row["PresentHomeAddress"].ToString());
                    fields.SetField("permanentHomeAddress", row["PermanentHomeAddress"].ToString());
                    fields.SetField("landlinePresent", row["LandlinePresent"].ToString());
                    fields.SetField("landlinePermanent", row["LandlinePermanent"].ToString());
                    fields.SetField("mobileRequired", row["MobileNoRequired"].ToString());
                    fields.SetField("mobileAlternative", row["MobileNoAlternative"].ToString());
                    fields.SetField("emailPersonal", row["EmailPersonal"].ToString());
                    fields.SetField("emailWork", row["EmailWork"].ToString());
                    fields.SetField("employerName", row["CompanyName"].ToString());
                    fields.SetField("occupation", row["Position"].ToString());
                    fields.SetField("dateOfHire", row["DateHired"].ToString());
                    fields.SetField("salaryCheck", "Yes", true); // checkbox options : "Yes":"No", true // always checked
                    if (row["EmploymentStatus"].ToString() == "Permanent/Regular")
                    {
                        fields.SetField("permanentEmploymentCheck", "Yes", true); // checkbox options : "Yes":"No", true
                        fields.SetField("contractualDate", ""); // shows if contractualEmployment is checked: contains contract end date
                    }
                    else if (row["EmploymentStatus"].ToString() == "Contractual/Project Hire")
                    {
                        fields.SetField("contractualEmploymentCheck", "Yes", true); // checkbox options : "Yes":"No", true
                        fields.SetField("contractualDate", row["EndOfContract"].ToString()); // shows if contractualEmployment is checked: contains contract end date
                    }
                    else
                    {
                        fields.SetField("othersEmploymentCheck", "Yes", true); // checkbox options : "Yes":"No", true
                        fields.SetField("othersEmploymentStatus", row["EmploymentStatus"].ToString());
                    }
                    fields.SetField("referencePerson1", row["Reference1Name"].ToString());
                    fields.SetField("referencePerson2", row["Reference2Name"].ToString());
                    fields.SetField("referenceContact1", row["Reference1Contact"].ToString());
                    fields.SetField("referenceContact2", row["Reference2Contact"].ToString());

                    stamper.FormFlattening = true;
                    stamper.Close();
                    #endregion

                    #region Send To Email
                    try
                    {
                        string prefix = HttpContext.Current.Server.MapPath("~/pdf/");
                        var message = new MimeKit.MimeMessage();
                        message.From.Add(new MimeKit.MailboxAddress("DurustHR", "durusthr@illimitado.com"));
                        if (row["EmailWork"] == DBNull.Value || row["EmailWork"].ToString() == "")
                        {
                            if (row["EmailPersonal"] == DBNull.Value || row["EmailPersonal"].ToString() == "")
                            {
                                message.To.Add(new MimeKit.MailboxAddress("durusthr@illimitado.com"));
                            }
                            else
                            {
                                message.To.Add(new MimeKit.MailboxAddress(row["EmailPersonal"].ToString()));
                            }
                        }
                        else
                        {
                            message.To.Add(new MimeKit.MailboxAddress(row["EmailWork"].ToString()));
                        }

                        message.Subject = "Salary Credit Agreement";
                        var builder = new MimeKit.BodyBuilder();

                        // Set the plain-text version of the message text
                        builder.TextBody = "Please see attachments.";

                        // We may also want to attach a calendar event for Monica's party...
                        builder.Attachments.Add(prefix + "Salary_Credit_Agreement_Form_" + SP.StartingID + ".pdf");

                        // Now we just need to set the message body and we're done
                        message.Body = builder.ToMessageBody();

                        using (var client = new MailKit.Net.Smtp.SmtpClient())
                        {
                            client.ServerCertificateValidationCallback = (s, c, h, e) => true;
                            client.Connect("box1256.bluehost.com", 465, true);
                            client.Authenticate("durusthr@illimitado.com", "@1230Qwerty");
                            client.Send(message);
                            client.Disconnect(true);
                        }
                        return "Success";
                    }
                    catch (Exception e)
                    {
                        return e.ToString();
                    }
                    #endregion
                }
                return "Success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
            //try
            //{
            //    tempconnectionstring();
            //    Connection con = new Connection();
            //    con.myparameters.Add(new myParameters { ParameterName = "@StartingID", mytype = SqlDbType.VarChar, Value = SP.StartingID });
            //    con.myparameters.Add(new myParameters { ParameterName = "@CN", mytype = SqlDbType.VarChar, Value = SP.CN });
            //    DataTable DT = con.GetDataTable("sp_CRT_GenerateSalad");
            //    foreach (DataRow row in DT.Rows)
            //    {
            //        #region Generate SALAD FILE
            //        var SALADFILE = "Salary_Credit_Agreement_Form_-_DIRECT_2016_10_06_No_MoA";
            //        PdfReader reader = new PdfReader(HttpContext.Current.Server.MapPath("~/pdf/") + SALADFILE + ".pdf");
            //        var newFile = HttpContext.Current.Server.MapPath("pdf") + "/" + "Salary_Credit_Agreement_Form_" + SP.StartingID + ".pdf";
            //        PdfStamper stamper = new PdfStamper(reader, new FileStream(newFile, FileMode.Create));
            //        AcroFields fields = stamper.AcroFields;

            //        // Page 2
            //        //Byte[] EmpsignBit = null;
            //        //Image Empsign = null;
            //        //var EmpsignContentByte = stamper.GetOverContent(2); // 2 represents page number
            //        //AcroFields.FieldPosition fieldpos = fields.GetFieldPositions("signature")[0];
            //        //Rectangle rect = fieldpos.position;
            //        //float xxxx = rect.Left;
            //        //float yyyy = rect.Bottom;
            //        ////string signpath = row["Signature"].ToString().Substring(0, (row["Signature"].ToString().Length) - 11) + row["StartingID"].ToString() + "_sign_1.png";
            //        ////EmpsignBit = File.ReadAllBytes(HttpContext.Current.Server.MapPath("ProfilePhotos") + signpath); // get image
            //        //EmpsignBit = File.ReadAllBytes(HttpContext.Current.Server.MapPath("ProfilePhotos") + row["Signature"].ToString().Substring(13)); // get image
            //        //Empsign = Image.GetInstance(EmpsignBit); // convert to byte array
            //        //Empsign.SetAbsolutePosition(xxxx, yyyy); // set position inside field to place picture 
            //        //Empsign.ScaleAbsolute(rect); // set absolute size of image as per size of field itself
            //        //EmpsignContentByte.AddImage(Empsign); // add image to field
            //        //fields.SetField("printedname", row["FirstName"].ToString() + " " + row["MiddleName"].ToString() + ' ' + row["LastName"].ToString());

            //        // Page 1
            //        fields.SetField("lastName", row["LastName"].ToString());
            //        fields.SetField("firstName", row["FirstName"].ToString());
            //        fields.SetField("middleName", row["MiddleName"].ToString());
            //        fields.SetField("birthMonth", row["birthMonth"].ToString());
            //        fields.SetField("birthDay", row["birthDay"].ToString());
            //        fields.SetField("birthYear", row["birthYear"].ToString());
            //        fields.SetField("placeOfBirth", row["PlaceOfBirth"].ToString());
            //        if (row["Gender"].ToString() == "Male")
            //        {
            //            fields.SetField("maleCheck", "Yes", true); // checkbox options : "Yes":"No", true
            //        }
            //        else
            //        {
            //            fields.SetField("femaleCheck", "Yes", true); // checkbox options : "Yes":"No", true
            //        }
            //        fields.SetField("citizenship", row["Nationality"].ToString());

            //        if (row["CivilStatus"].ToString() == "Single")
            //        {
            //            fields.SetField("singleCheck", "Yes", true); // checkbox options : "Yes":"No", true
            //        }
            //        else if (row["CivilStatus"].ToString() == "Married")
            //        {
            //            fields.SetField("marriedCheck", "Yes", true); // checkbox options : "Yes":"No", true
            //        }
            //        else if (row["CivilStatus"].ToString() == "Legally Separated")
            //        {
            //            fields.SetField("legallyseparatedCheck", "Yes", true); // checkbox options : "Yes":"No", true
            //        }
            //        else if (row["CivilStatus"].ToString() == "Annulled")
            //        {
            //            fields.SetField("annulledCheck", "Yes", true); // checkbox options : "Yes":"No", true
            //        }
            //        else if (row["CivilStatus"].ToString() == "Widow")
            //        {
            //            fields.SetField("widowCheck", "Yes", true); // checkbox options : "Yes":"No", true
            //        }
            //        fields.SetField("tinSSSgsis", row["EmployeeTIN"].ToString());
            //        fields.SetField("presentHomeAddress", row["PresentHomeAddress"].ToString());
            //        fields.SetField("permanentHomeAddress", row["PermanentHomeAddress"].ToString());
            //        fields.SetField("landlinePresent", row["LandlinePresent"].ToString());
            //        fields.SetField("landlinePermanent", row["LandlinePermanent"].ToString());
            //        fields.SetField("mobileRequired", row["MobileNoRequired"].ToString());
            //        fields.SetField("mobileAlternative", row["MobileNoAlternative"].ToString());
            //        fields.SetField("emailPersonal", row["EmailPersonal"].ToString());
            //        fields.SetField("emailWork", row["EmailWork"].ToString());
            //        fields.SetField("employerName", row["CompanyName"].ToString());
            //        fields.SetField("occupation", row["Position"].ToString());
            //        fields.SetField("dateOfHire", row["DateHired"].ToString());
            //        fields.SetField("salaryCheck", "Yes", true); // checkbox options : "Yes":"No", true // always checked
            //        if (row["EmploymentStatus"].ToString() == "Permanent/Regular")
            //        {
            //            fields.SetField("permanentEmploymentCheck", "Yes", true); // checkbox options : "Yes":"No", true
            //            fields.SetField("contractualDate", ""); // shows if contractualEmployment is checked: contains contract end date
            //        }
            //        else if (row["EmploymentStatus"].ToString() == "Contractual/Project Hire")
            //        {
            //            fields.SetField("contractualEmploymentCheck", "Yes", true); // checkbox options : "Yes":"No", true
            //            fields.SetField("contractualDate", row["EndOfContract"].ToString()); // shows if contractualEmployment is checked: contains contract end date
            //        }
            //        fields.SetField("referencePerson1", row["Reference1Name"].ToString());
            //        fields.SetField("referencePerson2", row["Reference2Name"].ToString());
            //        fields.SetField("referenceContact1", row["Reference1Contact"].ToString());
            //        fields.SetField("referenceContact2", row["Reference2Contact"].ToString());
            //        stamper.FormFlattening = true;
            //        stamper.Close();
            //        #endregion

            //        #region Send To Email
            //        try
            //        {
            //            string prefix = HttpContext.Current.Server.MapPath("~/pdf/");
            //            var message = new MimeKit.MimeMessage();
            //            message.From.Add(new MimeKit.MailboxAddress("DurustHR", "durusthr@illimitado.com"));
            //            if (row["EmailWork"] == DBNull.Value || row["EmailWork"].ToString() == "")
            //            {
            //                if (row["EmailPersonal"] == DBNull.Value || row["EmailPersonal"].ToString() == "")
            //                {
            //                    message.To.Add(new MimeKit.MailboxAddress("durusthr@illimitado.com"));
            //                }
            //                else
            //                {
            //                    message.To.Add(new MimeKit.MailboxAddress(row["EmailPersonal"].ToString()));
            //                }
            //            }
            //            else
            //            {
            //                message.To.Add(new MimeKit.MailboxAddress(row["EmailWork"].ToString()));
            //            }

            //            message.Subject = "Salary Credit Agreement";
            //            var builder = new MimeKit.BodyBuilder();

            //            // Set the plain-text version of the message text
            //            builder.TextBody = "Please see attachments.";

            //            // We may also want to attach a calendar event for Monica's party...
            //            builder.Attachments.Add(prefix + "Salary_Credit_Agreement_Form_" + SP.StartingID + ".pdf");

            //            // Now we just need to set the message body and we're done
            //            message.Body = builder.ToMessageBody();

            //            using (var client = new MailKit.Net.Smtp.SmtpClient())
            //            {
            //                client.ServerCertificateValidationCallback = (s, c, h, e) => true;
            //                client.Connect("box1256.bluehost.com", 465, true);
            //                client.Authenticate("durusthr@illimitado.com", "Secured@1230");
            //                client.Send(message);
            //                client.Disconnect(true);
            //            }
            //            return "Success";
            //        }
            //        catch (Exception e)
            //        {
            //            return e.ToString();
            //        }
            //        #endregion
            //    }
            //    return "Success";
            //}
            //catch (Exception e)
            //{
            //    return e.ToString();
            //}
        }
        #endregion

        [HttpPost]
        [Route("CRT_UpdatePasswordUserName")]
        public string CRT_UpdatePasswordUserName(InsertClientData ClientData)
        {
            //string valid = "";
            //GetDatabase GDB = new GetDatabase();
            //GDB.ClientName = ClientData.CN;
            tempconnectionstring();
            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@UserName", mytype = SqlDbType.NVarChar, Value = ClientData.UserName });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Password", mytype = SqlDbType.NVarChar, Value = ClientData.Password });
            //Connection.ExecuteScalar("sp_CRT_UpdatePasswordUserName").ToString();
            Connection.ExecuteNonQuery("sp_CRT_UpdatePasswordUserName");
            return "success";
        }
        [HttpPost]
        [Route("CRT_CheckPasswordDefault")]
        public string CRT_CheckPasswordDefault(InsertClientData ClientData)
        {
            try
            {
                string valid = "";
                //GetDatabase GDB = new GetDatabase();
                //GDB.ClientName = ClientData.CN;
                tempconnectionstring();
                Connection Connection = new Connection();

                Connection.myparameters.Add(new myParameters { ParameterName = "@UserName", mytype = SqlDbType.NVarChar, Value = ClientData.UserName });
                valid = Connection.ExecuteScalar("sp_CRT_CheckPasswordDefault").ToString();
                //Connection.ExecuteNonQuery("sp_CRT_CheckPasswordDefault");
                return valid;
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }
        [HttpPost]
        [Route("Mobile_BranchSearch")]
        public string Mobile_BranchSearch(MobileBranchSearch MBS)
        {
            try
            {
                tempconnectionstring();
                Connection Connection = new Connection();
                Connection.myparameters.Add(new myParameters { ParameterName = "@StartingID", mytype = SqlDbType.NVarChar, Value = MBS.StartingID });
                return Connection.ExecuteScalar("sp_Mobile_BranchSearch");
            }
            catch (Exception e)
            {
                return e.ToString();
            }

        }
        [HttpPost]
        [Route("Mobile_GetAllData")]
        public GetMobileAllData Mobile_GetAllData(MobileGetAllData MGAD)
        {
            GetMobileAllData GetAllDataNotification = new GetMobileAllData();

            tempconnectionstring();
            Connection Connection = new Connection();
            Connection.myparameters.Add(new myParameters { ParameterName = "@CompanyName", mytype = SqlDbType.NVarChar, Value = MGAD.CompanyName });
            Connection.myparameters.Add(new myParameters { ParameterName = "@StartingID", mytype = SqlDbType.NVarChar, Value = MGAD.StartingID });

            DataTable DT = new DataTable();
            DT = Connection.GetDataTable("sp_Mobile_GetAllData");
            List<MobileGetAllData> ListReturned = new List<MobileGetAllData>();
            if (DT.Rows.Count > 0)
            {
                foreach (DataRow row in DT.Rows)
                {
                    MobileGetAllData Displaynewdata = new MobileGetAllData
                    {
                        StartingID = row["StartingID"].ToString(),
                        Branch = row["Branch"].ToString(),
                        LastName = row["LastName"].ToString(),
                        FirstName = row["FirstName"].ToString(),
                        MiddleName = row["MiddleName"].ToString(),
                        Suffix = row["Suffix"].ToString(),
                        Prefix = row["Prefix"].ToString(),
                        Birthday = row["DateHired"].ToString(),
                        PlaceOfBirth = row["PlaceOfBirth"].ToString(),
                        MobileNumber = row["MobileNumber"].ToString(),
                        LandlineNumber = row["LandlineNumber"].ToString(),
                        Email = row["Email"].ToString(),
                        CivilStatus = row["CivilStatus"].ToString(),
                        Nationality = row["Nationality"].ToString(),
                        NationalityCountry = row["NationalityCountry"].ToString(),
                        Gender = row["Gender"].ToString(),
                        Position = row["Position"].ToString(),
                        DateHired2 = row["DateHired2"].ToString(),
                        EmploymentStatus = row["EmploymentStatus"].ToString(),
                        EndOfContract = row["EndOfContract"].ToString(),
                        PropertyType = row["PropertyType"].ToString(),
                        ID1 = row["ID1"].ToString(),
                        ID1Image = row["ID1Image"].ToString(),
                        ID2 = row["ID2"].ToString(),
                        ID2Image = row["ID2Image"].ToString(),
                        Phase = row["Phase"].ToString(),
                        ZipCode = row["ZipCode"].ToString(),
                        Block = row["Block"].ToString(),
                        Lot = row["Lot"].ToString(),
                        Street = row["Street"].ToString(),
                        Village = row["Village"].ToString(),
                        Subdivision = row["Subdivision"].ToString(),
                        Barangay = row["Barangay"].ToString(),
                        Municipality = row["Municipality"].ToString(),
                        Province = row["Province"].ToString(),
                        Region = row["Region"].ToString(),
                        Country = row["Country"].ToString(),
                        BldgNo = row["BldgNo"].ToString(),
                        NameOfPlace = row["NameOfPlace"].ToString(),
                        Wing = row["Wing"].ToString(),
                        UnitNo = row["UnitNo"].ToString(),
                        BldgName = row["BldgName"].ToString(),
                        FloorNo = row["FloorNo"].ToString(),
                        RoomAptNo = row["RoomAptNo"].ToString(),
                        PropertyTypePermanent = row["PropertyTypePermanent"].ToString(),
                        PhasePermanent = row["PhasePermanent"].ToString(),
                        ZipCodePermanent = row["ZipCodePermanent"].ToString(),
                        BlockPermanent = row["BlockPermanent"].ToString(),
                        LotPermanent = row["LotPermanent"].ToString(),
                        StreetPermanent = row["StreetPermanent"].ToString(),
                        VillagePermanent = row["VillagePermanent"].ToString(),
                        SubdivisionPermanent = row["SubdivisionPermanent"].ToString(),
                        BarangayPermanent = row["BarangayPermanent"].ToString(),
                        MunicipalityPermanent = row["MunicipalityPermanent"].ToString(),
                        ProvincePermanent = row["ProvincePermanent"].ToString(),
                        RegionPermanent = row["RegionPermanent"].ToString(),
                        CountryPermanent = row["CountryPermanent"].ToString(),
                        BldgNoPermanent = row["BldgNoPermanent"].ToString(),
                        NameOfPlacePermanent = row["NameOfPlacePermanent"].ToString(),
                        WingPermanent = row["WingPermanent"].ToString(),
                        UnitNoPermanent = row["UnitNoPermanent"].ToString(),
                        BldgNamePermanent = row["BldgNamePermanent"].ToString(),
                        FloorNoPermanent = row["FloorNoPermanent"].ToString(),
                        RoomAptNoPermanent = row["RoomAptNoPermanent"].ToString(),
                        First_Name = row["First_Name"].ToString(),
                        Middle_Name = row["Middle_Name"].ToString(),
                        Last_Name = row["Last_Name"].ToString(),
                        SuffixMother = row["SuffixMother"].ToString(),
                        EmployeeTIN = row["EmployeeTIN"].ToString(),
                        Ref1_FirstName = row["Ref1_FirstName"].ToString(),
                        Ref1_MiddleName = row["Ref1_MiddleName"].ToString(),
                        Ref1_LastName = row["Ref1_LastName"].ToString(),
                        Ref1_MobileNumber = row["Ref1_MobileNumber"].ToString(),
                        Ref2_FirstName = row["Ref2_FirstName"].ToString(),
                        Ref2_MiddleName = row["Ref2_MiddleName"].ToString(),
                        Ref2_LastName = row["Ref2_LastName"].ToString(),
                        Ref2_MobileNumber = row["Ref2_MobileNumber"].ToString(),
                        Signature1 = row["Signature1"].ToString(),
                        Signature2 = row["Signature2"].ToString(),
                        Signature3 = row["Signature3"].ToString(),
                        Rank = row["Rank"].ToString()
                    };
                    ListReturned.Add(Displaynewdata);
                }
                GetAllDataNotification.Displayalldata = ListReturned;
                GetAllDataNotification.myreturn = "Success";
            }
            return GetAllDataNotification;
        }

        [HttpPost]
        [Route("Mobile_GetMunicipality")]
        public List<GetMunicipality> Mobile_GetMunicipality(GetMunicipality GM)
        {
            tempconnectionstring();
            Connection con = new Connection();
            DataTable DT = new DataTable();
            DT = con.GetDataTable("sp_Mobile_GetMunicipality");
            List<GetMunicipality> ListReturned = new List<GetMunicipality>();
            if (DT.Rows.Count > 0)
            {
                foreach (DataRow row in DT.Rows)
                {
                    GetMunicipality Displaymunicipality = new GetMunicipality
                    {
                        Municipal = row["Municipal"].ToString()
                    };
                    ListReturned.Add(Displaymunicipality);
                }
            }

            return ListReturned;
        }
        [HttpPost]
        [Route("CRT_CompanySearch")]
        public List<GetCompanyName> CRT_CompanySearch(ClientDetails GM)
        {
            tempconnectionstring();
            Connection con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@CompanyAlias", mytype = SqlDbType.NVarChar, Value = GM.CompanyAlias });
            DataTable DT = new DataTable();
            DT = con.GetDataTable("sp_CRT_CompanySearch");
            List<GetCompanyName> ListReturned = new List<GetCompanyName>();
            if (DT.Rows.Count > 0)
            {
                foreach (DataRow row in DT.Rows)
                {
                    GetCompanyName DisplayCompanyName = new GetCompanyName
                    {
                        CompanyName = row["CompanyName"].ToString()
                    };
                    ListReturned.Add(DisplayCompanyName);
                }
            }

            return ListReturned;
        }

        [HttpPost]
        [Route("Mobile_ReadOnly")]
        public GetMobileReadOnly Mobile_ReadOnly(MobileGetAllData MGAD)
        {
            GetMobileReadOnly GetReadOnlyNotification = new GetMobileReadOnly();

            tempconnectionstring();
            Connection Connection = new Connection();
            Connection.myparameters.Add(new myParameters { ParameterName = "@StartingID", mytype = SqlDbType.NVarChar, Value = MGAD.StartingID });

            DataTable DT = new DataTable();
            DT = Connection.GetDataTable("sp_Mobile_ReadOnly");
            List<MobileGetAllData> ListReturned = new List<MobileGetAllData>();
            if (DT.Rows.Count > 0)
            {
                foreach (DataRow row in DT.Rows)
                {
                    MobileGetAllData Displaynewdata = new MobileGetAllData
                    {
                        StartingID = row["StartingID"].ToString(),
                        Branch = row["Branch"].ToString(),
                        LastName = row["LastName"].ToString(),
                        FirstName = row["FirstName"].ToString(),
                        MiddleName = row["MiddleName"].ToString(),
                        Suffix = row["Suffix"].ToString(),
                        Prefix = row["Prefix"].ToString(),
                        DateHired2 = row["DateHired2"].ToString(),
                        PlaceOfBirth = row["PlaceOfBirth"].ToString(),
                        Birthday = row["Birthday"].ToString(),
                        MobileNumber = row["MobileNumber"].ToString(),
                        LandlineNumber = row["LandlineNumber"].ToString(),
                        Email = row["Email"].ToString(),
                        CivilStatus = row["CivilStatus"].ToString(),
                        Nationality = row["Nationality"].ToString(),
                        NationalityCountry = row["NationalityCountry"].ToString(),
                        Gender = row["Gender"].ToString(),
                        PropertyType = row["PropertyType"].ToString(),
                        Phase = row["Phase"].ToString(),
                        ZipCode = row["ZipCode"].ToString(),
                        Block = row["Block"].ToString(),
                        Lot = row["Lot"].ToString(),
                        Street = row["Street"].ToString(),
                        Village = row["Village"].ToString(),
                        Subdivision = row["Subdivision"].ToString(),
                        Barangay = row["Barangay"].ToString(),
                        Municipality = row["Municipality"].ToString(),
                        Province = row["Province"].ToString(),
                        Region = row["Region"].ToString(),
                        Country = row["Country"].ToString(),
                        BldgNo = row["BldgNo"].ToString(),
                        NameOfPlace = row["NameOfPlace"].ToString(),
                        Wing = row["Wing"].ToString(),
                        UnitNo = row["UnitNo"].ToString(),
                        BldgName = row["BldgName"].ToString(),
                        FloorNo = row["FloorNo"].ToString(),
                        RoomAptNo = row["RoomAptNo"].ToString(),
                        PropertyTypePermanent = row["PropertyTypePermanent"].ToString(),
                        PhasePermanent = row["PhasePermanent"].ToString(),
                        ZipCodePermanent = row["ZipCodePermanent"].ToString(),
                        BlockPermanent = row["BlockPermanent"].ToString(),
                        LotPermanent = row["LotPermanent"].ToString(),
                        StreetPermanent = row["StreetPermanent"].ToString(),
                        VillagePermanent = row["VillagePermanent"].ToString(),
                        SubdivisionPermanent = row["SubdivisionPermanent"].ToString(),
                        BarangayPermanent = row["BarangayPermanent"].ToString(),
                        MunicipalityPermanent = row["MunicipalityPermanent"].ToString(),
                        ProvincePermanent = row["ProvincePermanent"].ToString(),
                        RegionPermanent = row["RegionPermanent"].ToString(),
                        CountryPermanent = row["CountryPermanent"].ToString(),
                        BldgNoPermanent = row["BldgNoPermanent"].ToString(),
                        NameOfPlacePermanent = row["NameOfPlacePermanent"].ToString(),
                        WingPermanent = row["WingPermanent"].ToString(),
                        UnitNoPermanent = row["UnitNoPermanent"].ToString(),
                        BldgNamePermanent = row["BldgNamePermanent"].ToString(),
                        FloorNoPermanent = row["FloorNoPermanent"].ToString(),
                        RoomAptNoPermanent = row["RoomAptNoPermanent"].ToString(),
                        First_Name = row["First_Name"].ToString(),
                        Middle_Name = row["Middle_Name"].ToString(),
                        Last_Name = row["Last_Name"].ToString(),
                        SuffixMother = row["SuffixMother"].ToString(),
                        EmployeeTIN = row["EmployeeTIN"].ToString()
                    };
                    ListReturned.Add(Displaynewdata);
                }
                GetReadOnlyNotification.Displayalldata = ListReturned;
                GetReadOnlyNotification.myreturn = "Success";
            }
            return GetReadOnlyNotification;
        }
        //Brandon generate salad START
        [HttpPost]
        [Route("GenerateCRTSALAD")]
        public string GenerateCRTSALAD(ClientDetails clientdata)
        {

            tempconnectionstring();
            Connection con = new Connection();

            con.myparameters.Add(new myParameters { ParameterName = "@CLientID", mytype = SqlDbType.NVarChar, Value = clientdata.ClientId });

            DataTable EmpData = con.GetDataTable("sp_CRT_GenerateInsertSalad");


            try
            {
                string dataDir = HostingEnvironment.MapPath("~/Files/");
                FileStream stream = new FileStream(dataDir + "CRT_Salad_List.xls", FileMode.Open);

                string filename;
                string companynamefile = clientdata.ClientId.ToString();
                string datebuild = DateTime.Now.ToString("MMddyyyy");
                string timebuild = DateTime.Now.ToString("hh-mmtt");

                //filename = "CrtSampleFileExport_" + datebuild + "_" +timebuild + ".xls";


                filename = companynamefile + "_" + "Salad" + datebuild + "_" + timebuild + ".xls";
                using (var fileStream = new FileStream(dataDir + filename, FileMode.Create, FileAccess.Write))
                {
                    stream.CopyTo(fileStream);
                }
                stream.Close();



                ExcelWorkbookFormat workbookFormat = ExcelWorkbookFormat.Xls_2003;
                string testDocFile = dataDir + "CRT_Salad_List.xls";
                ExcelWorkbook workbook = new ExcelWorkbook(testDocFile);
                ExcelWorksheet firstWorksheet = workbook.Worksheets[0];


                int cellnum = 2;
                for (int i = 0; i < EmpData.Rows.Count; i++)
                {
                    firstWorksheet["A" + cellnum].Text = EmpData.Rows[i][0].ToString();
                    firstWorksheet["B" + cellnum].Text = EmpData.Rows[i][1].ToString();
                    firstWorksheet["C" + cellnum].Text = EmpData.Rows[i][2].ToString();
                    firstWorksheet["D" + cellnum].Text = EmpData.Rows[i][3].ToString();
                    firstWorksheet["E" + cellnum].Text = EmpData.Rows[i][4].ToString();
                    firstWorksheet["F" + cellnum].Text = EmpData.Rows[i][5].ToString();
                    firstWorksheet["G" + cellnum].Text = EmpData.Rows[i][6].ToString();
                    firstWorksheet["H" + cellnum].Text = EmpData.Rows[i][7].ToString();
                    firstWorksheet["I" + cellnum].Text = EmpData.Rows[i][8].ToString();
                    firstWorksheet["J" + cellnum].Text = EmpData.Rows[i][9].ToString();
                    firstWorksheet["K" + cellnum].Text = EmpData.Rows[i][10].ToString();
                    firstWorksheet["L" + cellnum].Text = EmpData.Rows[i][11].ToString();
                    firstWorksheet["M" + cellnum].Text = EmpData.Rows[i][12].ToString();
                    firstWorksheet["N" + cellnum].Text = EmpData.Rows[i][13].ToString();
                    firstWorksheet["O" + cellnum].Text = EmpData.Rows[i][14].ToString();
                    firstWorksheet["P" + cellnum].Text = EmpData.Rows[i][15].ToString();
                    firstWorksheet["Q" + cellnum].Text = EmpData.Rows[i][16].ToString();
                    firstWorksheet["R" + cellnum].Text = EmpData.Rows[i][17].ToString();
                    firstWorksheet["S" + cellnum].Text = EmpData.Rows[i][18].ToString();
                    firstWorksheet["T" + cellnum].Text = EmpData.Rows[i][19].ToString();
                    firstWorksheet["U" + cellnum].Text = EmpData.Rows[i][20].ToString();
                    firstWorksheet["V" + cellnum].Text = EmpData.Rows[i][21].ToString();
                    firstWorksheet["W" + cellnum].Text = EmpData.Rows[i][22].ToString();
                    firstWorksheet["X" + cellnum].Text = EmpData.Rows[i][23].ToString();
                    firstWorksheet["Y" + cellnum].Text = EmpData.Rows[i][24].ToString();
                    firstWorksheet["Z" + cellnum].Text = EmpData.Rows[i][25].ToString();
                    firstWorksheet["AA" + cellnum].Text = EmpData.Rows[i][26].ToString();
                    firstWorksheet["AB" + cellnum].Text = EmpData.Rows[i][27].ToString();
                    firstWorksheet["AC" + cellnum].Text = EmpData.Rows[i][28].ToString();
                    firstWorksheet["AD" + cellnum].Text = EmpData.Rows[i][29].ToString();
                    firstWorksheet["AE" + cellnum].Text = EmpData.Rows[i][30].ToString();
                    firstWorksheet["AF" + cellnum].Text = EmpData.Rows[i][31].ToString();
                    firstWorksheet["AG" + cellnum].Text = EmpData.Rows[i][32].ToString();
                    firstWorksheet["AH" + cellnum].Text = EmpData.Rows[i][33].ToString();
                    firstWorksheet["AI" + cellnum].Text = EmpData.Rows[i][34].ToString();
                    firstWorksheet["AJ" + cellnum].Text = EmpData.Rows[i][35].ToString();
                    firstWorksheet["AK" + cellnum].Text = EmpData.Rows[i][36].ToString();
                    firstWorksheet["AL" + cellnum].Text = EmpData.Rows[i][37].ToString();
                    firstWorksheet["AM" + cellnum].Text = EmpData.Rows[i][38].ToString();
                    firstWorksheet["AN" + cellnum].Text = EmpData.Rows[i][39].ToString();
                    firstWorksheet["AO" + cellnum].Text = EmpData.Rows[i][40].ToString();
                    firstWorksheet["AP" + cellnum].Text = EmpData.Rows[i][41].ToString();
                    firstWorksheet["AQ" + cellnum].Text = EmpData.Rows[i][42].ToString();
                    firstWorksheet["AR" + cellnum].Text = EmpData.Rows[i][43].ToString();
                    firstWorksheet["AS" + cellnum].Text = EmpData.Rows[i][44].ToString();
                    firstWorksheet["AT" + cellnum].Text = EmpData.Rows[i][45].ToString();
                    firstWorksheet["AU" + cellnum].Text = EmpData.Rows[i][46].ToString();

                    cellnum++;
                }



                //https://http://ws.durusthr.com/ILM_WS_LIVE/sFTP/CrtSampleFileExport_12-41-PM.xls
                HttpResponse httpResponse = System.Web.HttpContext.Current.Response;

                httpResponse.Clear();
                httpResponse.ContentType = "Application/x-msexcel";
                httpResponse.AddHeader("Content-Disposition", String.Format("attachment; filename={0}", filename));
                string newDIR = HttpContext.Current.Server.MapPath("~/sFTP/");
                string onlineDIR = "http://ws.durusthr.com/ilm_ws_live/sFTP/";

                workbook.Save(newDIR + filename);

                //return "Success";
                return onlineDIR + filename;
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }
        //Brandon generate salad END
        //Brandon salad generate pass start
        [HttpPost]
        [Route("CRT_GenerateCRTInsertSaladEmployee")]
        public string GenerateCRTInsertSaladEmployee(UpdateNewUser generateid)
        {
            try
            {
                tempconnectionstring();
                Connection Connection = new Connection();

                Connection.myparameters.Add(new myParameters { ParameterName = "@StartingID", mytype = SqlDbType.NVarChar, Value = generateid.StartingID });
                Connection.myparameters.Add(new myParameters { ParameterName = "@CN", mytype = SqlDbType.NVarChar, Value = generateid.CN });

                Connection.ExecuteNonQuery("sp_CRT_GenerateSalad");
                return "success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }
        //brandon salad generate pass end
        //brandon add admin user
        [HttpPost]
        [Route("CRTAddAdminUser")]
        public string CRTAddAdminUser(CRTAddAdminUsers adminuser)
        {
            try
            {
                string valid = "";
                tempconnectionstring();
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@ID", mytype = SqlDbType.NVarChar, Value = adminuser.ID });
                con.myparameters.Add(new myParameters { ParameterName = "@FirstName", mytype = SqlDbType.NVarChar, Value = adminuser.FirstName });
                con.myparameters.Add(new myParameters { ParameterName = "@MiddleName", mytype = SqlDbType.NVarChar, Value = adminuser.MiddleName });
                con.myparameters.Add(new myParameters { ParameterName = "@LastName", mytype = SqlDbType.NVarChar, Value = adminuser.LastName });
                con.myparameters.Add(new myParameters { ParameterName = "@Title", mytype = SqlDbType.NVarChar, Value = adminuser.Title });
                con.myparameters.Add(new myParameters { ParameterName = "@Role", mytype = SqlDbType.NVarChar, Value = adminuser.Role });
                con.myparameters.Add(new myParameters { ParameterName = "@Area", mytype = SqlDbType.NVarChar, Value = adminuser.Area });
                con.myparameters.Add(new myParameters { ParameterName = "@Branch", mytype = SqlDbType.NVarChar, Value = adminuser.Branch });
                con.myparameters.Add(new myParameters { ParameterName = "@Department", mytype = SqlDbType.NVarChar, Value = adminuser.Department });
                con.myparameters.Add(new myParameters { ParameterName = "@Sub_Department", mytype = SqlDbType.NVarChar, Value = adminuser.Sub_Department });
                con.myparameters.Add(new myParameters { ParameterName = "@User_Type", mytype = SqlDbType.NVarChar, Value = adminuser.User_Type });
                con.myparameters.Add(new myParameters { ParameterName = "@Email", mytype = SqlDbType.NVarChar, Value = adminuser.Email });
                con.myparameters.Add(new myParameters { ParameterName = "@Phone", mytype = SqlDbType.NVarChar, Value = adminuser.Phone });


                valid = con.ExecuteScalar("sp_CRT_Add_AdminUser").ToString();
                return valid;
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }
        //brandon add admin user end
        //brandondisplayuser

        [HttpPost]
        [Route("CRTDisplayAdminUser")]
        public List<CRTAddAdminUsersDisplay> CRTDisplayAdminUser(CRTAddAdminUsersDisplay updatedata)
        {

            tempconnectionstring();
            Connection Connection = new Connection();

            DataTable DT = new DataTable();
            DT = Connection.GetDataTable("sp_CRT_Display_AdminUser");
            List<CRTAddAdminUsersDisplay> ListReturned = new List<CRTAddAdminUsersDisplay>();
            if (DT.Rows.Count > 0)
            {
                foreach (DataRow row in DT.Rows)
                {
                    CRTAddAdminUsersDisplay Displaynewusers = new CRTAddAdminUsersDisplay
                    {
                        ID = row["ID"].ToString(),
                        FirstName = row["FirstName"].ToString(),
                        MiddleName = row["MiddleName"].ToString(),
                        LastName = row["LastName"].ToString(),
                        Title = row["Title"].ToString(),
                        Role = row["Role"].ToString(),
                        Area = row["Area"].ToString(),
                        Branch = row["Branch"].ToString(),
                        Department = row["Department"].ToString(),
                        Sub_Department = row["Sub_Department"].ToString(),
                        UserType = row["UserType"].ToString(),
                        Email = row["Email"].ToString(),
                        PhoneNumber = row["PhoneNumber"].ToString()
                    };
                    ListReturned.Add(Displaynewusers);
                }
            }

            return ListReturned;
        }
        //brandondisplayuser
        //brandon update admin user
        [HttpPost]
        [Route("CRTUpdateAdminUser")]
        public string CRTUpdateAdminUser(CRTAddAdminUsers adminuser)
        {
            try
            {
                string valid = "";
                tempconnectionstring();
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@ID", mytype = SqlDbType.NVarChar, Value = adminuser.ID });
                con.myparameters.Add(new myParameters { ParameterName = "@FirstName", mytype = SqlDbType.NVarChar, Value = adminuser.FirstName });
                con.myparameters.Add(new myParameters { ParameterName = "@MiddleName", mytype = SqlDbType.NVarChar, Value = adminuser.MiddleName });
                con.myparameters.Add(new myParameters { ParameterName = "@LastName", mytype = SqlDbType.NVarChar, Value = adminuser.LastName });
                con.myparameters.Add(new myParameters { ParameterName = "@Title", mytype = SqlDbType.NVarChar, Value = adminuser.Title });
                con.myparameters.Add(new myParameters { ParameterName = "@Role", mytype = SqlDbType.NVarChar, Value = adminuser.Role });
                con.myparameters.Add(new myParameters { ParameterName = "@Area", mytype = SqlDbType.NVarChar, Value = adminuser.Area });
                con.myparameters.Add(new myParameters { ParameterName = "@Branch", mytype = SqlDbType.NVarChar, Value = adminuser.Branch });
                con.myparameters.Add(new myParameters { ParameterName = "@Department", mytype = SqlDbType.NVarChar, Value = adminuser.Department });
                con.myparameters.Add(new myParameters { ParameterName = "@Sub_Department", mytype = SqlDbType.NVarChar, Value = adminuser.Sub_Department });
                con.myparameters.Add(new myParameters { ParameterName = "@User_Type", mytype = SqlDbType.NVarChar, Value = adminuser.User_Type });
                con.myparameters.Add(new myParameters { ParameterName = "@Email", mytype = SqlDbType.NVarChar, Value = adminuser.Email });
                con.myparameters.Add(new myParameters { ParameterName = "@Phone", mytype = SqlDbType.NVarChar, Value = adminuser.Phone });
                con.myparameters.Add(new myParameters { ParameterName = "@lastID", mytype = SqlDbType.NVarChar, Value = adminuser.lastID });

                valid = con.ExecuteScalar("sp_CRT_Update_AdminUser").ToString();
                return valid;
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }
        //brandon update admin user end
        //brandon search salad client start
        //[HttpPost]
        //[Route("CRT_GetEmployeeID")]
        //public List<GetEmployeeID> GetEmployeeID(CRTPublicParam CRTPP)
        //{

        //    tempconnectionstring();
        //    Connection Connection = new Connection();

        //    Connection.myparameters.Add(new myParameters { ParameterName = "@Com", mytype = SqlDbType.NVarChar, Value = CRTPP.CClientID });

        //    DataTable DT = new DataTable();
        //    DT = Connection.GetDataTable("sp_CRT_SaladCompanySearch");
        //    List<CRTSaladCompanySearchDisplay> ListReturned = new List<CRTSaladCompanySearchDisplay>();
        //    if (DT.Rows.Count > 0)
        //    {
        //        foreach (DataRow row in DT.Rows)
        //        {
        //            CRTSaladCompanySearchDisplay Displaynewusers = new CRTSaladCompanySearchDisplay
        //            {
        //                Generated = row["Generated"].ToString(),
        //                GeneratedPDF = row["GeneratedPDF"].ToString(),
        //                CompanyAlias = row["CompanyAlias"].ToString(),
        //                CompanyName = row["CompanyName"].ToString(),
        //                Branch = row["Branch"].ToString(),
        //                StartingID = row["StartingID"].ToString(),
        //                LastName = row["LastName"].ToString(),
        //                FirstName = row["FirstName"].ToString(),
        //                MiddleName = row["MiddleName"].ToString(),
        //                Suffix = row["Suffix"].ToString(),
        //                CardName = row["CardName"].ToString(),
        //                Birthday = row["Birthday"].ToString(),
        //                PlaceofBirth = row["PlaceofBirth"].ToString(),
        //                CRTStatus = row["CRTStatus"].ToString(),
        //                CRTDateSent = row["CRTDateSent"].ToString()
        //            };
        //            ListReturned.Add(Displaynewusers);
        //        }
        //    }

        //    return ListReturned;
        //}
        [HttpPost]
        [Route("CRTSaladCompanySearch")]
        public List<CRTSaladCompanySearchDisplay> CRTSaladCompanySearch(CRTPublicParam CRTPP)
        {

            tempconnectionstring();
            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@CClientID", mytype = SqlDbType.NVarChar, Value = CRTPP.CClientID });

            DataTable DT = new DataTable();
            DT = Connection.GetDataTable("sp_CRT_SaladCompanySearch");
            List<CRTSaladCompanySearchDisplay> ListReturned = new List<CRTSaladCompanySearchDisplay>();
            if (DT.Rows.Count > 0)
            {
                foreach (DataRow row in DT.Rows)
                {
                    CRTSaladCompanySearchDisplay Displaynewusers = new CRTSaladCompanySearchDisplay
                    {
                        Generated = row["Generated"].ToString(),
                        GeneratedPDF = row["GeneratedPDF"].ToString(),
                        CompanyAlias = row["CompanyAlias"].ToString(),
                        CompanyName = row["CompanyName"].ToString(),
                        Branch = row["Branch"].ToString(),
                        StartingID = row["StartingID"].ToString(),
                        LastName = row["LastName"].ToString(),
                        FirstName = row["FirstName"].ToString(),
                        MiddleName = row["MiddleName"].ToString(),
                        Suffix = row["Suffix"].ToString(),
                        CardName = row["CardName"].ToString(),
                        Birthday = row["Birthday"].ToString(),
                        PlaceofBirth = row["PlaceofBirth"].ToString(),
                        CRTStatus = row["CRTStatus"].ToString(),
                        CRTDateSent = row["CRTDateSent"].ToString()
                    };
                    ListReturned.Add(Displaynewusers);
                }
            }

            return ListReturned;
        }
        //Akram baka hindi kayo makapag upload etong nasa taas yung original na webservice testing ko lang kong nasa pagitan nito START
        //[HttpPost]
        //[Route("GenerateCRTSALADINFO")]
        //public string GenerateCRTSALADINFO(SaladItemsCRT clientdata)
        //{

        //    DataTable DT = new DataTable();
        //    DT.Columns.AddRange(new DataColumn[1] { new DataColumn("StartingID") });
        //    for (int i = 0; i < clientdata._startingid.Length; i++)
        //    {
        //        DT.Rows.Add(clientdata._startingid[i]);
        //    }

        //    tempconnectionstring();
        //    Connection con = new Connection();

        //    con.myparameters.Add(new myParameters { ParameterName = "@CN", mytype = SqlDbType.VarChar, Value = clientdata._cn });
        //    con.myparameters.Add(new myParameters { ParameterName = "@IMPORT", mytype = SqlDbType.Structured, Value = DT });

        //    DataTable EmpData = con.GetDataTable("sp_CRT_GenerateSaladInfo");


        //    try
        //    {
        //        string dataDir = HostingEnvironment.MapPath("~/Files/");
        //        //FileStream stream = new FileStream(dataDir + "newsaladtemplate.xls", FileMode.Open);
        //        FileStream stream = new FileStream(dataDir + "newsaladtemplateforupload.xls", FileMode.Open);

        //        string filename;
        //        //string companynamefile = clientdata.ClientId.ToString();
        //        string datebuild = DateTime.Now.ToString("MMddyyyy");
        //        string timebuild = DateTime.Now.ToString("hh-mmtt");

        //        //filename = "CrtSampleFileExport_" + datebuild + "_" +timebuild + ".xls";


        //        filename = /*companynamefile + "_" +*/ "Salad" + datebuild + "_" + timebuild + ".xls";
        //        //ilename = "testing" + "_" + "Salad"+ ".xls";
        //        using (var fileStream = new FileStream(dataDir + filename, FileMode.Create, FileAccess.Write))
        //        {
        //            stream.CopyTo(fileStream);
        //        }
        //        stream.Close();



        //        ExcelWorkbookFormat workbookFormat = ExcelWorkbookFormat.Xls_2003;
        //        //string testDocFile = dataDir + "newsaladtemplate.xls";
        //        string testDocFile = dataDir + "newsaladtemplateforupload.xls";
        //        ExcelWorkbook workbook = new ExcelWorkbook(testDocFile);
        //        ExcelWorksheet firstWorksheet = workbook.Worksheets[0];


        //        int cellnum = 2;
        //        foreach (DataRow row in EmpData.Rows)
        //        {
        //            firstWorksheet["A" + cellnum].Text = row["CRTSaladDateEntry"].ToString();
        //            firstWorksheet["B" + cellnum].DateTimeValue = DateTime.Now;
        //            firstWorksheet["C" + cellnum].Text = row["CompanyName"].ToString();
        //            firstWorksheet["D" + cellnum].Text = row["Branch"].ToString();
        //            firstWorksheet["E" + cellnum].Text = row["LastName"].ToString();
        //            firstWorksheet["F" + cellnum].Text = row["FirstName"].ToString();
        //            firstWorksheet["G" + cellnum].Text = row["MiddleName"].ToString();
        //            firstWorksheet["H" + cellnum].Text = row["Suffix"].ToString();
        //            firstWorksheet["I" + cellnum].Text = row["CardName"].ToString();
        //            firstWorksheet["J" + cellnum].Text = row["DateHired"].ToString();
        //            firstWorksheet["K" + cellnum].Text = row["PlaceofBirth"].ToString();
        //            firstWorksheet["L" + cellnum].Text = row["Nationality"].ToString();
        //            firstWorksheet["M" + cellnum].Text = row["NationalityCountry"].ToString();
        //            firstWorksheet["N" + cellnum].Text = row["Gender"].ToString();
        //            firstWorksheet["O" + cellnum].Text = row["MobileNumber"].ToString();
        //            firstWorksheet["P" + cellnum].Text = row["LandlineNumber"].ToString();
        //            firstWorksheet["Q" + cellnum].Text = row["HouseNumber"].ToString();
        //            firstWorksheet["R" + cellnum].Text = row["StreetrVillageSubdivisionBrgy"].ToString();
        //            firstWorksheet["S" + cellnum].Text = row["Country"].ToString();
        //            firstWorksheet["T" + cellnum].Text = row["Region"].ToString();
        //            firstWorksheet["U" + cellnum].Text = row["Province"].ToString();
        //            firstWorksheet["V" + cellnum].Text = row["CityMunicipality"].ToString();
        //            firstWorksheet["W" + cellnum].Text = row["ZipCode"].ToString();
        //            firstWorksheet["X" + cellnum].Text = row["PermanentHouseNumber"].ToString();
        //            firstWorksheet["Y" + cellnum].Text = row["PermanentStreetrVillageSubdivisionBrgy"].ToString();
        //            firstWorksheet["Z" + cellnum].Text = row["PermanentCountry"].ToString();
        //            firstWorksheet["AA" + cellnum].Text = row["PermanentRegion"].ToString();
        //            firstWorksheet["AB" + cellnum].Text = row["PermanentProvince"].ToString();
        //            firstWorksheet["AC" + cellnum].Text = row["PermanentCityMunicipality"].ToString();
        //            firstWorksheet["AD" + cellnum].Text = row["PermanentZipCode"].ToString();
        //            firstWorksheet["AE" + cellnum].Text = row["EmailAddress"].ToString();
        //            firstWorksheet["AF" + cellnum].Text = row["CompanyName"].ToString();
        //            firstWorksheet["AG" + cellnum].Text = row["EmployeeTIN"].ToString();
        //            firstWorksheet["AH" + cellnum].Text = row["CivilStatus"].ToString();
        //            firstWorksheet["AI" + cellnum].Text = row["MotherLastName"].ToString();
        //            firstWorksheet["AJ" + cellnum].Text = row["MotherFirstName"].ToString();
        //            firstWorksheet["AK" + cellnum].Text = row["MotherMiddleName"].ToString();
        //            firstWorksheet["AL" + cellnum].Text = row["MotherSuffix"].ToString();
        //            firstWorksheet["AM" + cellnum].Text = row["NumberofPads"].ToString();
        //            firstWorksheet["AN" + cellnum].Text = row["OtherDetails"].ToString();
        //            firstWorksheet["AO" + cellnum].Text = row["Position"].ToString();
        //            firstWorksheet["AP" + cellnum].Text = row["Rank"].ToString();
        //            firstWorksheet["AQ" + cellnum].Text = row["DateHired"].ToString();
        //            firstWorksheet["AR" + cellnum].Text = row["EmploymentStatus"].ToString();
        //            firstWorksheet["AS" + cellnum].Text = row["EndofContract"].ToString();
        //            firstWorksheet["AT" + cellnum].Text = row["RefOneName"].ToString();
        //            firstWorksheet["AU" + cellnum].Text = row["RefOneMobileNumber"].ToString();
        //            firstWorksheet["AV" + cellnum].Text = row["RefTwoName"].ToString();
        //            firstWorksheet["AW" + cellnum].Text = row["RefTwoMobileNumber"].ToString();

        //            //firstWorksheet["A" + cellnum].Text = row["CompanyName"].ToString();
        //            //firstWorksheet["B" + cellnum].Text = row["Branch"].ToString();
        //            //firstWorksheet["C" + cellnum].Text = row["LastName"].ToString();
        //            //firstWorksheet["D" + cellnum].Text = row["FirstName"].ToString();
        //            //firstWorksheet["E" + cellnum].Text = row["MiddleName"].ToString();
        //            //firstWorksheet["F" + cellnum].Text = row["Suffix"].ToString();
        //            //firstWorksheet["G" + cellnum].Text = row["CardName"].ToString();
        //            //firstWorksheet["H" + cellnum].Text = row["DateHired"].ToString();
        //            //firstWorksheet["I" + cellnum].Text = row["PlaceofBirth"].ToString();
        //            //firstWorksheet["J" + cellnum].Text = row["Nationality"].ToString();
        //            //firstWorksheet["K" + cellnum].Text = row["NationalityCountry"].ToString();
        //            //firstWorksheet["L" + cellnum].Text = row["Gender"].ToString();
        //            //firstWorksheet["M" + cellnum].Text = row["MobileNumber"].ToString();
        //            //firstWorksheet["N" + cellnum].Text = row["LandlineNumber"].ToString();
        //            //firstWorksheet["O" + cellnum].Text = row["HouseNumber"].ToString();
        //            //firstWorksheet["P" + cellnum].Text = row["StreetrVillageSubdivisionBrgy"].ToString();
        //            //firstWorksheet["Q" + cellnum].Text = row["Country"].ToString();
        //            //firstWorksheet["R" + cellnum].Text = row["Region"].ToString();
        //            //firstWorksheet["S" + cellnum].Text = row["Province"].ToString();
        //            //firstWorksheet["T" + cellnum].Text = row["CityMunicipality"].ToString();
        //            //firstWorksheet["U" + cellnum].Text = row["ZipCode"].ToString();
        //            //firstWorksheet["V" + cellnum].Text = row["PermanentHouseNumber"].ToString();
        //            //firstWorksheet["W" + cellnum].Text = row["PermanentStreetrVillageSubdivisionBrgy"].ToString();
        //            //firstWorksheet["X" + cellnum].Text = row["PermanentCountry"].ToString();
        //            //firstWorksheet["Y" + cellnum].Text = row["PermanentRegion"].ToString();
        //            //firstWorksheet["Z" + cellnum].Text = row["PermanentProvince"].ToString();
        //            //firstWorksheet["AA" + cellnum].Text = row["PermanentCityMunicipality"].ToString();
        //            //firstWorksheet["AB" + cellnum].Text = row["PermanentZipCode"].ToString();
        //            //firstWorksheet["AC" + cellnum].Text = row["EmailAddress"].ToString();
        //            //firstWorksheet["AD" + cellnum].Text = row["CompanyName"].ToString();
        //            //firstWorksheet["AE" + cellnum].Text = row["EmployeeTIN"].ToString();
        //            //firstWorksheet["AF" + cellnum].Text = row["CivilStatus"].ToString();
        //            //firstWorksheet["AG" + cellnum].Text = row["MotherLastName"].ToString();
        //            //firstWorksheet["AH" + cellnum].Text = row["MotherFirstName"].ToString();
        //            //firstWorksheet["AI" + cellnum].Text = row["MotherMiddleName"].ToString();
        //            //firstWorksheet["AJ" + cellnum].Text = row["MotherSuffix"].ToString();
        //            //firstWorksheet["AK" + cellnum].Text = row["NumberofPads"].ToString();
        //            //firstWorksheet["AL" + cellnum].Text = row["OtherDetails"].ToString();
        //            //firstWorksheet["AM" + cellnum].Text = row["Position"].ToString();
        //            //firstWorksheet["AN" + cellnum].Text = row["DateHired"].ToString();
        //            //firstWorksheet["AO" + cellnum].Text = row["EmploymentStatus"].ToString();
        //            //firstWorksheet["AP" + cellnum].Text = row["EndofContract"].ToString();
        //            ////firstWorksheet[AO" + cellnum].Text = row["RefOneFirstName"].ToString();
        //            ////firstWorksheet["AP" + cellnum].Text = row["RefOneMiddleName"].ToString();
        //            ////firstWorksheet["AQ" + cellnum].Text = row["RefOneLastName"].ToString();
        //            //firstWorksheet["AQ" + cellnum].Text = row["RefOneName"].ToString();
        //            //firstWorksheet["AR" + cellnum].Text = row["RefOneMobileNumber"].ToString();
        //            //firstWorksheet["AS" + cellnum].Text = row["RefTwoName"].ToString();
        //            ////firstWorksheet["AS" + cellnum].Text = row["RefTwoFirstName"].ToString();
        //            ////firstWorksheet["AT" + cellnum].Text = row["RefTwoMiddleName"].ToString();
        //            ////firstWorksheet["AU" + cellnum].Text = row["RefTwoLastName"].ToString();
        //            //firstWorksheet["AT" + cellnum].Text = row["RefTwoMobileNumber"].ToString();

        //            cellnum++;
        //        }

        //        //https://http://ws.durusthr.com/ILM_WS_LIVE/sFTP/CrtSampleFileExport_12-41-PM.xls
        //        HttpResponse httpResponse = System.Web.HttpContext.Current.Response;

        //        httpResponse.Clear();
        //        httpResponse.ContentType = "Application/x-msexcel";
        //        httpResponse.AddHeader("Content-Disposition", String.Format("attachment; filename={0}", filename));
        //        string newDIR = HttpContext.Current.Server.MapPath("~/sFTP/");
        //        string onlineDIR = "http://ws.durusthr.com/ilm_ws_live/sFTP/";

        //        workbook.Save(newDIR + filename);

        //        //return "Success";
        //        return onlineDIR + filename;
        //    }
        //    catch (Exception e)
        //    {
        //        return e.ToString();
        //    }
        //}
        //Akram baka hindi kayo makapag upload etong nasa taas yung original na webservice testing ko lang kong nasa pagitan nito END
        [HttpPost]
        [Route("GenerateCRTSALADINFO")]
        public string GenerateCRTSALADINFO(SaladItemsCRT clientdata)
        {

            DataTable DT = new DataTable();
            DT.Columns.AddRange(new DataColumn[1] { new DataColumn("StartingID") });
            for (int i = 0; i < clientdata._startingid.Length; i++)
            {
                DT.Rows.Add(clientdata._startingid[i]);
            }

            tempconnectionstring();
            Connection con = new Connection();

            con.myparameters.Add(new myParameters { ParameterName = "@CN", mytype = SqlDbType.VarChar, Value = clientdata._cn });
            con.myparameters.Add(new myParameters { ParameterName = "@IMPORT", mytype = SqlDbType.Structured, Value = DT });

            DataTable EmpData = con.GetDataTable("sp_CRT_GenerateSaladInfo");


            try
            {
                string dataDir = HostingEnvironment.MapPath("~/Files/");
                //FileStream stream = new FileStream(dataDir + "newsaladtemplate.xls", FileMode.Open);
                FileStream stream = new FileStream(dataDir + "newsaladtemplateforupload.xls", FileMode.Open);

                string filename;
                //string companynamefile = clientdata.ClientId.ToString();
                string datebuild = DateTime.Now.ToString("MMddyyyy");
                string timebuild = DateTime.Now.ToString("hh-mmtt");

                //filename = "CrtSampleFileExport_" + datebuild + "_" +timebuild + ".xls";


                filename = /*companynamefile + "_" +*/ "Salad" + datebuild + "_" + timebuild + ".xls";
                //ilename = "testing" + "_" + "Salad"+ ".xls";
                using (var fileStream = new FileStream(dataDir + filename, FileMode.Create, FileAccess.Write))
                {
                    stream.CopyTo(fileStream);
                }
                stream.Close();



                ExcelWorkbookFormat workbookFormat = ExcelWorkbookFormat.Xls_2003;
                //string testDocFile = dataDir + "newsaladtemplate.xls";
                string testDocFile = dataDir + "newsaladtemplateforupload.xls";
                ExcelWorkbook workbook = new ExcelWorkbook(testDocFile);
                ExcelWorksheet firstWorksheet = workbook.Worksheets[0];


                int cellnum = 2;
                foreach (DataRow row in EmpData.Rows)
                {
                    firstWorksheet["A" + cellnum].Text = row["CRTSaladDateEntry"].ToString();
                    firstWorksheet["B" + cellnum].DateTimeValue = DateTime.Now;
                    firstWorksheet["C" + cellnum].Text = row["CompanyName"].ToString();
                    firstWorksheet["D" + cellnum].Text = row["Branch"].ToString();
                    firstWorksheet["E" + cellnum].Text = row["LastName"].ToString();
                    firstWorksheet["F" + cellnum].Text = row["FirstName"].ToString();
                    firstWorksheet["G" + cellnum].Text = row["MiddleName"].ToString();
                    firstWorksheet["H" + cellnum].Text = row["Suffix"].ToString();
                    firstWorksheet["I" + cellnum].Text = row["CardName"].ToString();
                    firstWorksheet["J" + cellnum].Text = row["Birthday"].ToString();
                    firstWorksheet["K" + cellnum].Text = row["PlaceofBirth"].ToString();
                    firstWorksheet["L" + cellnum].Text = row["Nationality"].ToString();
                    firstWorksheet["M" + cellnum].Text = row["NationalityCountry"].ToString();
                    firstWorksheet["N" + cellnum].Text = row["Gender"].ToString();
                    firstWorksheet["O" + cellnum].Text = row["MobileNumber"].ToString();
                    firstWorksheet["P" + cellnum].Text = row["LandlineNumber"].ToString();
                    firstWorksheet["Q" + cellnum].Text = row["HouseNumber"].ToString();
                    firstWorksheet["R" + cellnum].Text = row["StreetrVillageSubdivisionBrgy"].ToString();
                    firstWorksheet["S" + cellnum].Text = row["Country"].ToString();
                    firstWorksheet["T" + cellnum].Text = row["Region"].ToString();
                    firstWorksheet["U" + cellnum].Text = row["Province"].ToString();
                    firstWorksheet["V" + cellnum].Text = row["CityMunicipality"].ToString();
                    firstWorksheet["W" + cellnum].Text = row["ZipCode"].ToString();
                    firstWorksheet["X" + cellnum].Text = row["PermanentHouseNumber"].ToString();
                    firstWorksheet["Y" + cellnum].Text = row["PermanentStreetrVillageSubdivisionBrgy"].ToString();
                    firstWorksheet["Z" + cellnum].Text = row["PermanentCountry"].ToString();
                    firstWorksheet["AA" + cellnum].Text = row["PermanentRegion"].ToString();
                    firstWorksheet["AB" + cellnum].Text = row["PermanentProvince"].ToString();
                    firstWorksheet["AC" + cellnum].Text = row["PermanentCityMunicipality"].ToString();
                    firstWorksheet["AD" + cellnum].Text = row["PermanentZipCode"].ToString();
                    firstWorksheet["AE" + cellnum].Text = row["EmailAddress"].ToString();
                    firstWorksheet["AF" + cellnum].Text = row["EmployerName"].ToString();
                    firstWorksheet["AG" + cellnum].Text = row["EmployeeTIN"].ToString();
                    firstWorksheet["AH" + cellnum].Text = row["CivilStatus"].ToString();
                    firstWorksheet["AI" + cellnum].Text = row["MotherLastName"].ToString();
                    firstWorksheet["AJ" + cellnum].Text = row["MotherFirstName"].ToString();
                    firstWorksheet["AK" + cellnum].Text = row["MotherMiddleName"].ToString();
                    firstWorksheet["AL" + cellnum].Text = row["MotherSuffix"].ToString();
                    firstWorksheet["AM" + cellnum].Text = row["NumberofPads"].ToString();
                    firstWorksheet["AN" + cellnum].Text = row["OtherDetails"].ToString();
                    firstWorksheet["AO" + cellnum].Text = row["Position"].ToString();
                    firstWorksheet["AP" + cellnum].Text = row["Rank"].ToString();
                    firstWorksheet["AQ" + cellnum].Text = row["DateHired"].ToString();
                    firstWorksheet["AR" + cellnum].Text = row["EmploymentStatus"].ToString();
                    firstWorksheet["AS" + cellnum].Text = row["EndofContract"].ToString();
                    firstWorksheet["AT" + cellnum].Text = row["RefOneName"].ToString();
                    firstWorksheet["AU" + cellnum].Text = row["RefOneMobileNumber"].ToString();
                    firstWorksheet["AV" + cellnum].Text = row["RefTwoName"].ToString();
                    firstWorksheet["AW" + cellnum].Text = row["RefTwoMobileNumber"].ToString();
                    firstWorksheet["AX" + cellnum].Text = row["DateAdded"].ToString();

                    //firstWorksheet["A" + cellnum].Text = row["CompanyName"].ToString();
                    //firstWorksheet["B" + cellnum].Text = row["Branch"].ToString();
                    //firstWorksheet["C" + cellnum].Text = row["LastName"].ToString();
                    //firstWorksheet["D" + cellnum].Text = row["FirstName"].ToString();
                    //firstWorksheet["E" + cellnum].Text = row["MiddleName"].ToString();
                    //firstWorksheet["F" + cellnum].Text = row["Suffix"].ToString();
                    //firstWorksheet["G" + cellnum].Text = row["CardName"].ToString();
                    //firstWorksheet["H" + cellnum].Text = row["DateHired"].ToString();
                    //firstWorksheet["I" + cellnum].Text = row["PlaceofBirth"].ToString();
                    //firstWorksheet["J" + cellnum].Text = row["Nationality"].ToString();
                    //firstWorksheet["K" + cellnum].Text = row["NationalityCountry"].ToString();
                    //firstWorksheet["L" + cellnum].Text = row["Gender"].ToString();
                    //firstWorksheet["M" + cellnum].Text = row["MobileNumber"].ToString();
                    //firstWorksheet["N" + cellnum].Text = row["LandlineNumber"].ToString();
                    //firstWorksheet["O" + cellnum].Text = row["HouseNumber"].ToString();
                    //firstWorksheet["P" + cellnum].Text = row["StreetrVillageSubdivisionBrgy"].ToString();
                    //firstWorksheet["Q" + cellnum].Text = row["Country"].ToString();
                    //firstWorksheet["R" + cellnum].Text = row["Region"].ToString();
                    //firstWorksheet["S" + cellnum].Text = row["Province"].ToString();
                    //firstWorksheet["T" + cellnum].Text = row["CityMunicipality"].ToString();
                    //firstWorksheet["U" + cellnum].Text = row["ZipCode"].ToString();
                    //firstWorksheet["V" + cellnum].Text = row["PermanentHouseNumber"].ToString();
                    //firstWorksheet["W" + cellnum].Text = row["PermanentStreetrVillageSubdivisionBrgy"].ToString();
                    //firstWorksheet["X" + cellnum].Text = row["PermanentCountry"].ToString();
                    //firstWorksheet["Y" + cellnum].Text = row["PermanentRegion"].ToString();
                    //firstWorksheet["Z" + cellnum].Text = row["PermanentProvince"].ToString();
                    //firstWorksheet["AA" + cellnum].Text = row["PermanentCityMunicipality"].ToString();
                    //firstWorksheet["AB" + cellnum].Text = row["PermanentZipCode"].ToString();
                    //firstWorksheet["AC" + cellnum].Text = row["EmailAddress"].ToString();
                    //firstWorksheet["AD" + cellnum].Text = row["CompanyName"].ToString();
                    //firstWorksheet["AE" + cellnum].Text = row["EmployeeTIN"].ToString();
                    //firstWorksheet["AF" + cellnum].Text = row["CivilStatus"].ToString();
                    //firstWorksheet["AG" + cellnum].Text = row["MotherLastName"].ToString();
                    //firstWorksheet["AH" + cellnum].Text = row["MotherFirstName"].ToString();
                    //firstWorksheet["AI" + cellnum].Text = row["MotherMiddleName"].ToString();
                    //firstWorksheet["AJ" + cellnum].Text = row["MotherSuffix"].ToString();
                    //firstWorksheet["AK" + cellnum].Text = row["NumberofPads"].ToString();
                    //firstWorksheet["AL" + cellnum].Text = row["OtherDetails"].ToString();
                    //firstWorksheet["AM" + cellnum].Text = row["Position"].ToString();
                    //firstWorksheet["AN" + cellnum].Text = row["DateHired"].ToString();
                    //firstWorksheet["AO" + cellnum].Text = row["EmploymentStatus"].ToString();
                    //firstWorksheet["AP" + cellnum].Text = row["EndofContract"].ToString();
                    ////firstWorksheet[AO" + cellnum].Text = row["RefOneFirstName"].ToString();
                    ////firstWorksheet["AP" + cellnum].Text = row["RefOneMiddleName"].ToString();
                    ////firstWorksheet["AQ" + cellnum].Text = row["RefOneLastName"].ToString();
                    //firstWorksheet["AQ" + cellnum].Text = row["RefOneName"].ToString();
                    //firstWorksheet["AR" + cellnum].Text = row["RefOneMobileNumber"].ToString();
                    //firstWorksheet["AS" + cellnum].Text = row["RefTwoName"].ToString();
                    ////firstWorksheet["AS" + cellnum].Text = row["RefTwoFirstName"].ToString();
                    ////firstWorksheet["AT" + cellnum].Text = row["RefTwoMiddleName"].ToString();
                    ////firstWorksheet["AU" + cellnum].Text = row["RefTwoLastName"].ToString();
                    //firstWorksheet["AT" + cellnum].Text = row["RefTwoMobileNumber"].ToString();

                    cellnum++;
                }

                //https://http://ws.durusthr.com/ILM_WS_LIVE/sFTP/CrtSampleFileExport_12-41-PM.xls
                HttpResponse httpResponse = System.Web.HttpContext.Current.Response;

                httpResponse.Clear();
                httpResponse.ContentType = "Application/x-msexcel";
                httpResponse.AddHeader("Content-Disposition", String.Format("attachment; filename={0}", filename));
                string newDIR = HttpContext.Current.Server.MapPath("~/sFTP/");
                string onlineDIR = "http://ws.durusthr.com/ilm_ws_live/sFTP/";

                workbook.Save(newDIR + filename);

                //return "Success";
                return onlineDIR + filename;
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }



        //Scheduler Start
        [HttpPost]
        [Route("CRTSubjectsv2")] //Listahan ng Subjects sa Labels
        public subjectsV2 schedulerSubjectsV2(subjectsV2 sb)
        {
            try
            {
                tempconnectionstring();
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@ClientID", mytype = SqlDbType.NVarChar, Value = sb.CN });
                subjectsV2 subjects = new subjectsV2();
                List<subjectlistv2> list = new List<subjectlistv2>();
                //DataSet ds = new DataSet();
                DataTable dt = con.GetDataTable("sp_SP_ShowSubjectsV2");

                foreach (DataRow row in dt.Rows)
                {
                    subjectlistv2 param = new subjectlistv2()
                    {
                        _SID = row["SID"].ToString(),
                        _Subject = row["Subject"].ToString(),
                        _Status = Convert.ToBoolean(row["Status"].ToString()),
                        _DefaultName = row["DefaultName"].ToString(),
                        _ModifiedBy = row["ModifiedBy"].ToString(),
                        _ModifiedDate = row["ModifiedDate"].ToString(),
                        _Edit = false
                    };
                    list.Add(param);
                }
                subjects.subjectlist = list;
                subjects._myreturn = "Success";
                return subjects;
            }
            catch (Exception e)
            {
                subjectsV2 subjects = new subjectsV2();
                List<subjectlistv2> list = new List<subjectlistv2>();
                subjects.subjectlist = list;
                subjects._myreturn = e.ToString();
                return subjects;
            }
        }
        [HttpPost]
        [Route("CRTUpdateSubjectv2")] // insert and update ng Subject. pag pinasahan ng 0 yung @SUBJECTID magiinsert siya ng panibago else iuupdate niya. yung @DEFAULTNAME original na name nung Subject
        public string UpdateSubject(subjectlistv2 subjectlist)
        {
            try
            {
                tempconnectionstring();
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@USERID", mytype = SqlDbType.NVarChar, Value = subjectlist._ModifiedBy });
                con.myparameters.Add(new myParameters { ParameterName = "@SUBJECTID", mytype = SqlDbType.NVarChar, Value = subjectlist._SID });
                con.myparameters.Add(new myParameters { ParameterName = "@SUBJECTNAME", mytype = SqlDbType.NVarChar, Value = subjectlist._Subject });
                con.myparameters.Add(new myParameters { ParameterName = "@DEFAULTNAME", mytype = SqlDbType.NVarChar, Value = subjectlist._DefaultName });
                con.myparameters.Add(new myParameters { ParameterName = "@CLIENTID", mytype = SqlDbType.NVarChar, Value = subjectlist.CN });
                con.ExecuteScalar("sp_SP_InsertUpdateSubjectV2");
                return "Success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }
        [HttpPost]
        [Route("CRTSubjectStatusv2")] //Toggle ng checkbox for Subject = Active/Inactive
        public string SubjectStatusv2(subjectlistv2 subjectlist)
        {
            try
            {
                tempconnectionstring();
                Connection con = new Connection();

                con.myparameters.Add(new myParameters { ParameterName = "@USERID", mytype = SqlDbType.NVarChar, Value = subjectlist._ModifiedBy });
                con.myparameters.Add(new myParameters { ParameterName = "@SUBJECTID", mytype = SqlDbType.NVarChar, Value = subjectlist._SID });
                con.ExecuteNonQuery("sp_SP_ActiveInactiveSubjectV2");
                return "Success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }
        [HttpPost]
        [Route("CRTSubSubjectStatus")] //Toggle ng checkbox for SubSubject = Active/Inactive
        public string SubSubjectStatus(subsubjectlist subsubjectlist)
        {
            try
            {
                tempconnectionstring();
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@USERID", mytype = SqlDbType.NVarChar, Value = subsubjectlist._ModifiedBy });
                con.myparameters.Add(new myParameters { ParameterName = "@SUBSUBJECTID", mytype = SqlDbType.NVarChar, Value = subsubjectlist._RowID });
                con.ExecuteNonQuery("sp_SP_ActiveInactiveSubSubjectList");
                return "Success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }
        [HttpPost]
        [Route("CRTSubSubjectsv22")] //Listahan ng SubSubjects sa labels, pasahan ng value 0 yung parameter para magdisplay lahat ng subsubjects else selected subsubjects lang lalabas
        public subsubjectsv2 schedulerSubSubjectss(subjectlistv2 subject)
        {
            try
            {
                tempconnectionstring();
                Connection con = new Connection();
                subsubjectsv2 subsubjects = new subsubjectsv2();
                List<subsubjectlist> list = new List<subsubjectlist>();
                //DataSet ds = new DataSet();
                con.myparameters.Add(new myParameters { ParameterName = "@SUBJECTID", mytype = SqlDbType.NVarChar, Value = subject._SID });
                con.myparameters.Add(new myParameters { ParameterName = "@ClientID", mytype = SqlDbType.NVarChar, Value = subject.CN });
                DataTable dt = con.GetDataTable("sp_SP_SelectShowSubSubjectsV22");

                foreach (DataRow row in dt.Rows)
                {
                    subsubjectlist param = new subsubjectlist()
                    {
                        _RowID = row["RowID"].ToString(),
                        _SID = row["SID"].ToString(),
                        _Subject = row["SubSubject"].ToString(),
                        _Status = Convert.ToBoolean(row["Status"].ToString()),
                        _ModifiedBy = row["ModifiedBy"].ToString(),
                        _ModifiedDate = row["ModifiedDate"].ToString(),
                        _Edit = false
                    };
                    list.Add(param);
                }
                subsubjects.subsubjectlist = list;
                subsubjects._myreturn = "Success";
                return subsubjects;
            }
            catch (Exception e)
            {
                subsubjectsv2 subsubjects = new subsubjectsv2();
                List<subsubjectlist> list = new List<subsubjectlist>();
                subsubjects.subsubjectlist = list;
                subsubjects._myreturn = e.ToString();
                return subsubjects;
            }
        }
        [HttpPost]
        [Route("CRTListSimulatorv2")]
        public subsubjectsv2 ListSimulator(subjectlistv2 sl)
        {
            try
            {
                tempconnectionstring();
                Connection con = new Connection();
                subsubjectsv2 subsubjects = new subsubjectsv2();
                List<subsubjectlist> list = new List<subsubjectlist>();
                con.myparameters.Add(new myParameters { ParameterName = "@SUBJECTID", mytype = SqlDbType.NVarChar, Value = sl._SID });
                con.myparameters.Add(new myParameters { ParameterName = "@ClientID", mytype = SqlDbType.NVarChar, Value = sl.CN });
                DataTable dt = con.GetDataTable("sp_SP_SubSubjectSimulatorv2");

                foreach (DataRow row in dt.Rows)
                {
                    subsubjectlist param = new subsubjectlist()
                    {
                        _RowID = row["RowID"].ToString(),
                        _Subject = row["SubSubject"].ToString()
                    };
                    list.Add(param);
                }
                subsubjects.subsubjectlist = list;
                subsubjects._myreturn = "Success";
                return subsubjects;
            }
            catch (Exception e)
            {
                subsubjectsv2 subsubjects = new subsubjectsv2();
                List<subsubjectlist> list = new List<subsubjectlist>();
                subsubjects.subsubjectlist = list;
                subsubjects._myreturn = e.ToString();
                return subsubjects;
            }
        }
        [HttpPost]
        [Route("CRTUpdateSubSubjectv2")] //Insert and Update ng SubSubjects, pasahan ng 0 yung @SUBSUBJECTID para mag insert ng panibagong subsubject else iuupdate niya yung selected subsubject
        public string UpdateSubSubjectv2(subsubjectlist subsubjectlist)
        {
            try
            {
                tempconnectionstring();
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@USERID", mytype = SqlDbType.NVarChar, Value = subsubjectlist._ModifiedBy });
                con.myparameters.Add(new myParameters { ParameterName = "@SUBJECTID", mytype = SqlDbType.NVarChar, Value = subsubjectlist._SID });
                con.myparameters.Add(new myParameters { ParameterName = "@SUBSUBJECTID", mytype = SqlDbType.NVarChar, Value = subsubjectlist._RowID });
                con.myparameters.Add(new myParameters { ParameterName = "@SUBSUBJECTNAME", mytype = SqlDbType.NVarChar, Value = subsubjectlist._Subject });
                con.myparameters.Add(new myParameters { ParameterName = "@CLIENTID", mytype = SqlDbType.NVarChar, Value = subsubjectlist.CN });
                con.ExecuteScalar("sp_SP_InsertUpdateSubSubjectListV2");
                return "Success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }
        //scheduler End

        #region Revised
        [HttpPost]
        [Route("CRTAddAdminUserv2")]
        public string CRTAddAdminUserv2(CRTAddAdminUsersv2 adminuser)
        {
            try
            {
                tempconnectionstring();
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@ID", mytype = SqlDbType.VarChar, Value = adminuser.ID });
                con.myparameters.Add(new myParameters { ParameterName = "@UserID", mytype = SqlDbType.VarChar, Value = adminuser.UserID });
                con.myparameters.Add(new myParameters { ParameterName = "@FirstName", mytype = SqlDbType.VarChar, Value = adminuser.FirstName });
                con.myparameters.Add(new myParameters { ParameterName = "@MiddleName", mytype = SqlDbType.VarChar, Value = adminuser.MiddleName });
                con.myparameters.Add(new myParameters { ParameterName = "@LastName", mytype = SqlDbType.VarChar, Value = adminuser.LastName });
                con.myparameters.Add(new myParameters { ParameterName = "@Title", mytype = SqlDbType.VarChar, Value = adminuser.Title });
                con.myparameters.Add(new myParameters { ParameterName = "@Role", mytype = SqlDbType.VarChar, Value = adminuser.Role });
                con.myparameters.Add(new myParameters { ParameterName = "@Area", mytype = SqlDbType.VarChar, Value = adminuser.Area });
                con.myparameters.Add(new myParameters { ParameterName = "@Branch", mytype = SqlDbType.VarChar, Value = adminuser.Branch });
                con.myparameters.Add(new myParameters { ParameterName = "@Department", mytype = SqlDbType.VarChar, Value = adminuser.Department });
                con.myparameters.Add(new myParameters { ParameterName = "@SubDepartment", mytype = SqlDbType.VarChar, Value = adminuser.SubDepartment });
                con.myparameters.Add(new myParameters { ParameterName = "@UserType", mytype = SqlDbType.VarChar, Value = adminuser.UserType });
                con.myparameters.Add(new myParameters { ParameterName = "@Email", mytype = SqlDbType.VarChar, Value = adminuser.Email });
                con.myparameters.Add(new myParameters { ParameterName = "@Phone", mytype = SqlDbType.VarChar, Value = adminuser.Phone });
                con.myparameters.Add(new myParameters { ParameterName = "@CClientID", mytype = SqlDbType.VarChar, Value = adminuser.CClientID });
                return con.ExecuteScalar("sp_CRT_Add_AdminUser1").ToString();
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }
        [HttpPost]
        [Route("CRTDisplayAdminUserv2")]
        public List<CRTAddAdminUsersDisplay> CRTDisplayAdminUserv2(CRTPublicParam CRTPP)
        {

            tempconnectionstring();
            Connection con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@CClientID", mytype = SqlDbType.VarChar, Value = CRTPP.CClientID });
            DataTable DT = con.GetDataTable("sp_CRT_Display_AdminUserv2");
            List<CRTAddAdminUsersDisplay> ListReturned = new List<CRTAddAdminUsersDisplay>();
            if (DT.Rows.Count > 0)
            {
                foreach (DataRow row in DT.Rows)
                {
                    CRTAddAdminUsersDisplay Displaynewusers = new CRTAddAdminUsersDisplay
                    {
                        ID = row["ID"].ToString(),
                        UserID = row["UserID"].ToString(),
                        FirstName = row["FirstName"].ToString(),
                        MiddleName = row["MiddleName"].ToString(),
                        LastName = row["LastName"].ToString(),
                        Title = row["Title"].ToString(),
                        Role = row["Role"].ToString(),
                        Area = row["Area"].ToString(),
                        Branch = row["Branch"].ToString(),
                        Department = row["Department"].ToString(),
                        Sub_Department = row["SubDepartment"].ToString(),
                        UserType = row["UserType"].ToString(),
                        Email = row["Email"].ToString(),
                        PhoneNumber = row["PhoneNumber"].ToString()
                    };
                    ListReturned.Add(Displaynewusers);
                }
            }
            return ListReturned;
        }
        [HttpPost]
        [Route("CRTUpdateAdminUserv2")]
        public string CRTUpdateAdminUserv2(CRTUpdateAdminUsersv2 adminuser)
        {
            try
            {
                DataTable DT = new DataTable();
                DT.Columns.AddRange(new DataColumn[1] { new DataColumn("ID") });
                for (int i = 0; i < adminuser.ID.Length; i++)
                {
                    DT.Rows.Add(adminuser.ID[i]);
                }
                tempconnectionstring();
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@ID", mytype = SqlDbType.Structured, Value = DT });
                con.myparameters.Add(new myParameters { ParameterName = "@UserID", mytype = SqlDbType.VarChar, Value = adminuser.UserID });
                con.myparameters.Add(new myParameters { ParameterName = "@FirstName", mytype = SqlDbType.VarChar, Value = adminuser.FirstName });
                con.myparameters.Add(new myParameters { ParameterName = "@MiddleName", mytype = SqlDbType.VarChar, Value = adminuser.MiddleName });
                con.myparameters.Add(new myParameters { ParameterName = "@LastName", mytype = SqlDbType.VarChar, Value = adminuser.LastName });
                con.myparameters.Add(new myParameters { ParameterName = "@Title", mytype = SqlDbType.VarChar, Value = adminuser.Title });
                con.myparameters.Add(new myParameters { ParameterName = "@Role", mytype = SqlDbType.VarChar, Value = adminuser.Role });
                con.myparameters.Add(new myParameters { ParameterName = "@Area", mytype = SqlDbType.VarChar, Value = adminuser.Area });
                con.myparameters.Add(new myParameters { ParameterName = "@Branch", mytype = SqlDbType.VarChar, Value = adminuser.Branch });
                con.myparameters.Add(new myParameters { ParameterName = "@Department", mytype = SqlDbType.VarChar, Value = adminuser.Department });
                con.myparameters.Add(new myParameters { ParameterName = "@SubDepartment", mytype = SqlDbType.VarChar, Value = adminuser.SubDepartment });
                con.myparameters.Add(new myParameters { ParameterName = "@UserType", mytype = SqlDbType.VarChar, Value = adminuser.UserType });
                con.myparameters.Add(new myParameters { ParameterName = "@Email", mytype = SqlDbType.VarChar, Value = adminuser.Email });
                con.myparameters.Add(new myParameters { ParameterName = "@Phone", mytype = SqlDbType.VarChar, Value = adminuser.Phone });
                con.myparameters.Add(new myParameters { ParameterName = "@UpdateType", mytype = SqlDbType.VarChar, Value = adminuser.UpdateType });
                con.myparameters.Add(new myParameters { ParameterName = "@CClientID", mytype = SqlDbType.VarChar, Value = adminuser.CClientID });
                return con.ExecuteScalar("sp_CRT_Update_AdminUserv2").ToString();
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }
        [HttpPost]
        [Route("CRTLoginv2")]
        public List<AdminLoginInfo> AdminLoginInfo(CRTLogin Login)
        {
            List<AdminLoginInfo> ListReturned = new List<AdminLoginInfo>();
            AdminLoginInfo ALI = new AdminLoginInfo();
            try
            {
                tempconnectionstring();
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@CN", mytype = SqlDbType.NVarChar, Value = Login.CN });
                con.myparameters.Add(new myParameters { ParameterName = "@UserID", mytype = SqlDbType.NVarChar, Value = Login.UserID });
                con.myparameters.Add(new myParameters { ParameterName = "@Password", mytype = SqlDbType.NVarChar, Value = Login.Password });
                DataTable DT = con.GetDataTable("sp_CRT_Loginv2");
                foreach (DataRow row in DT.Rows)
                {
                    ALI = new AdminLoginInfo()
                    {
                        UserID = row["UserID"].ToString(),
                        FirstName = row["FirstName"].ToString(),
                        LastName = row["LastName"].ToString(),
                        UserType = row["UserType"].ToString(),
                        CClientID = row["CClientID"].ToString(),
                        _myreturn = "Success"
                    };
                    ListReturned.Add(ALI);
                }
                return ListReturned;
            }
            catch (Exception e)
            {
                ALI._myreturn = e.ToString();
                ListReturned.Add(ALI);
            }
            return ListReturned;
        }
        [HttpPost]
        [Route("CRT_EmailValidationv2")]
        public string CRT_EmailValidationv2(CRTAddAdminUsersv2 ClientData)
        {
            try
            {
                tempconnectionstring();
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@EmailAddress", mytype = SqlDbType.VarChar, Value = ClientData.Email });
                con.myparameters.Add(new myParameters { ParameterName = "@CClientID", mytype = SqlDbType.VarChar, Value = ClientData.CClientID });
                return con.ExecuteScalar("sp_CRT_EmailVerifyv2").ToString();
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }
        [HttpPost]
        [Route("CRT_InsertClientDatav2")]
        public string CRT_InsertClientDatav2(InsertClientDatav2 ClientData)
        {
            try
            {
                tempconnectionstring();
                Connection Connection = new Connection();

                Connection.myparameters.Add(new myParameters { ParameterName = "@CN", mytype = SqlDbType.NVarChar, Value = ClientData.CN });
                Connection.myparameters.Add(new myParameters { ParameterName = "@UserName", mytype = SqlDbType.NVarChar, Value = ClientData.UserName });
                Connection.myparameters.Add(new myParameters { ParameterName = "@CompanyAlias", mytype = SqlDbType.NVarChar, Value = ClientData.CompanyAlias });
                Connection.myparameters.Add(new myParameters { ParameterName = "@DateofIncorporation", mytype = SqlDbType.NVarChar, Value = ClientData.DateofIncorporation });
                Connection.myparameters.Add(new myParameters { ParameterName = "@AddressFloor", mytype = SqlDbType.NVarChar, Value = ClientData.AddressFloor });
                Connection.myparameters.Add(new myParameters { ParameterName = "@AddressBuilding", mytype = SqlDbType.NVarChar, Value = ClientData.AddressBuilding });
                Connection.myparameters.Add(new myParameters { ParameterName = "@AddressStreet", mytype = SqlDbType.NVarChar, Value = ClientData.AddressStreet });
                Connection.myparameters.Add(new myParameters { ParameterName = "@AddressBrgy", mytype = SqlDbType.NVarChar, Value = ClientData.AddressBrgy });
                Connection.myparameters.Add(new myParameters { ParameterName = "@AddressMunicipality", mytype = SqlDbType.NVarChar, Value = ClientData.AddressMunicipality });
                Connection.myparameters.Add(new myParameters { ParameterName = "@AddressProvince", mytype = SqlDbType.NVarChar, Value = ClientData.AddressProvince });
                Connection.myparameters.Add(new myParameters { ParameterName = "@AddressRegion", mytype = SqlDbType.NVarChar, Value = ClientData.AddressRegion });
                Connection.myparameters.Add(new myParameters { ParameterName = "@AddressCountry", mytype = SqlDbType.NVarChar, Value = ClientData.AddressCountry });
                Connection.myparameters.Add(new myParameters { ParameterName = "@AddressZipCode", mytype = SqlDbType.NVarChar, Value = ClientData.AddressZipCode });
                Connection.myparameters.Add(new myParameters { ParameterName = "@NationalityCountry", mytype = SqlDbType.NVarChar, Value = ClientData.NationalityCountry });
                Connection.myparameters.Add(new myParameters { ParameterName = "@CompanyEQCustomerNumber", mytype = SqlDbType.NVarChar, Value = ClientData.CompanyEQCustomerNumber });
                Connection.myparameters.Add(new myParameters { ParameterName = "@TotalNumberOfEmployees", mytype = SqlDbType.NVarChar, Value = ClientData.TotalNumberOfEmployees });
                Connection.myparameters.Add(new myParameters { ParameterName = "@AccountType", mytype = SqlDbType.NVarChar, Value = ClientData.AccountType });
                Connection.myparameters.Add(new myParameters { ParameterName = "@ContactNumber", mytype = SqlDbType.NVarChar, Value = ClientData.ContactNumber });
                Connection.myparameters.Add(new myParameters { ParameterName = "@EmailAddress", mytype = SqlDbType.NVarChar, Value = ClientData.EmailAddress });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Tin", mytype = SqlDbType.NVarChar, Value = ClientData.Tin });
                Connection.myparameters.Add(new myParameters { ParameterName = "@FirstName", mytype = SqlDbType.NVarChar, Value = ClientData.FirstName });
                Connection.myparameters.Add(new myParameters { ParameterName = "@MiddleName", mytype = SqlDbType.NVarChar, Value = ClientData.MiddleName });
                Connection.myparameters.Add(new myParameters { ParameterName = "@LastName", mytype = SqlDbType.NVarChar, Value = ClientData.LastName });
                Connection.myparameters.Add(new myParameters { ParameterName = "@RepresentativeTitle", mytype = SqlDbType.NVarChar, Value = ClientData.RepresentativeTitle });
                Connection.myparameters.Add(new myParameters { ParameterName = "@RepresentativeContactNumber", mytype = SqlDbType.NVarChar, Value = ClientData.RepresentativeContactNumber });
                Connection.myparameters.Add(new myParameters { ParameterName = "@isDeleted", mytype = SqlDbType.NVarChar, Value = ClientData.isDeleted });
                Connection.myparameters.Add(new myParameters { ParameterName = "@CreatedById", mytype = SqlDbType.NVarChar, Value = ClientData.CreatedById });
                Connection.myparameters.Add(new myParameters { ParameterName = "@PDF_Status", mytype = SqlDbType.NVarChar, Value = ClientData.PDF_Status });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Salad_Info", mytype = SqlDbType.NVarChar, Value = ClientData.Salad_Info });
                Connection.myparameters.Add(new myParameters { ParameterName = "@CClientID", mytype = SqlDbType.NVarChar, Value = ClientData.CClientID });
                Connection.myparameters.Add(new myParameters { ParameterName = "@SaladSigID", mytype = SqlDbType.NVarChar, Value = ClientData.SaladSigID });

                return Connection.ExecuteScalar("sp_CRT_AddClientv2");
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }
        [HttpPost]
        [Route("CRTSendNewClientEmailv2")]
        public string SendNewClientEmailv2(SendNewClientEmailv2 SNCE)
        {
            try
            {
                tempconnectionstring();

                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@UserName", mytype = SqlDbType.NVarChar, Value = SNCE.UserName });
                con.myparameters.Add(new myParameters { ParameterName = "@CClientID", mytype = SqlDbType.NVarChar, Value = SNCE.CClientID });
                DataTable DT = con.GetDataTable("sp_CRT_SendEmailToClientv2");

                if (DT.Rows.Count > 0)
                {
                    var message = new MimeKit.MimeMessage();
                    message.From.Add(new MimeKit.MailboxAddress("DurustHR", "durusthr@illimitado.com"));
                    message.To.Add(new MimeKit.MailboxAddress(DT.Rows[0]["Email"].ToString()));
                    message.Subject = "Notifications";
                    var messBody = "<label>Welcome to DurustHR CRT Admin portal, in parternship with Security Bank. Thank you for using DurustHR.<br>" +
                        "Feel free to visit https://sbccrt.durusthr.com to login to your account.<br><br>" +
                        "Here are your account details:<br>" +
                        "<b>Company Name: </b>" + DT.Rows[0]["CompanyName"].ToString() + "<br>" +
                        "<b>Company Alias: </b>" + DT.Rows[0]["CompanyAlias"].ToString() + "<br>" +
                        "<b>Username: </b>" + DT.Rows[0]["UserName"].ToString() + "<br>" +
                        "<b>Temporary Password: </b>" + DT.Rows[0]["Password"].ToString() + "<br><br>" +
                        "Access https://sbccrt.durusthr.com anytime and anywhere with Chrome. Enjoy your account.</label>";
                    message.Body = new MimeKit.TextPart("html") { Text = messBody };

                    using (var client = new MailKit.Net.Smtp.SmtpClient())
                    {
                        client.ServerCertificateValidationCallback = (s, c, h, e) => true;
                        client.Connect("box1256.bluehost.com", 465, true);
                        client.Authenticate("durusthr@illimitado.com", "@1230Qwerty");
                        client.Send(message);
                        client.Disconnect(true);
                    }
                }
                return "Success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }
        [HttpPost]
        [Route("CRT_UserNameValidationv2")]
        public string CRT_UserNameValidationv2(InsertClientDatav2 ClientData)
        {
            try
            {
                tempconnectionstring();
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@UserName", mytype = SqlDbType.NVarChar, Value = ClientData.UserName });
                con.myparameters.Add(new myParameters { ParameterName = "@CClientID", mytype = SqlDbType.NVarChar, Value = ClientData.CClientID });
                return con.ExecuteScalar("sp_CRT_UsernameValidationv2").ToString();
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }
        [HttpPost]
        [Route("CRT_AliasValidationv2")]
        public string CRT_AliasValidationv2(InsertClientDatav2 ClientData)
        {
            try
            {
                tempconnectionstring();
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@Alias", mytype = SqlDbType.NVarChar, Value = ClientData.CompanyAlias });
                con.myparameters.Add(new myParameters { ParameterName = "@CClientID", mytype = SqlDbType.NVarChar, Value = ClientData.CClientID });
                return con.ExecuteScalar("sp_CRT_AliasValidationv2").ToString();
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }

        [HttpPost]
        [Route("CRTUserLoginv2")]
        public string CRTUserLoginv2(CRTLogin login)
        {
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = login.CN;
            tempconnectionstring();

            try
            {
                string valid = "";
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@CN", mytype = SqlDbType.NVarChar, Value = login.CN });
                con.myparameters.Add(new myParameters { ParameterName = "@UserID", mytype = SqlDbType.NVarChar, Value = login.UserID });
                con.myparameters.Add(new myParameters { ParameterName = "@Password", mytype = SqlDbType.NVarChar, Value = login.Password });
                valid = con.ExecuteScalar("sp_CRT_UserLoginv2").ToString();
                return valid;
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }
        [HttpPost]
        [Route("CRTSubjectLabelsv2")]
        public subjectlabellistv2 SubjectLabelList(subjectlabelv2 sb)
        {
            try
            {
                tempconnectionstring();
                Connection con = new Connection();
                subjectlabellistv2 SubjectLabelList = new subjectlabellistv2();
                List<subjectlabelv2> SL = new List<subjectlabelv2>();
                con.myparameters.Add(new myParameters { ParameterName = "@CClientID", mytype = SqlDbType.NVarChar, Value = sb.CClientID });
                DataTable dt = con.GetDataTable("sp_SP_LabelSubjectsV2");
                foreach (DataRow row in dt.Rows)
                {
                    subjectlabelv2 param = new subjectlabelv2()
                    {
                        _title = row["Title"].ToString(),
                        _role = row["Role"].ToString(),
                        _area = row["Area"].ToString(),
                        _branch = row["Branch"].ToString(),
                        _department = row["Department"].ToString(),
                        _subdepartment = row["SubDepartment"].ToString(),
                    };
                    SL.Add(param);
                }
                SubjectLabelList.subjectlabel = SL;
                SubjectLabelList._myreturn = "Success";
                return SubjectLabelList;
            }
            catch (Exception e)
            {
                subjectlabellistv2 SubjectLabelList = new subjectlabellistv2();
                List<subjectlabelv2> SL = new List<subjectlabelv2>();
                SubjectLabelList.subjectlabel = SL;
                SubjectLabelList._myreturn = e.ToString();
                return SubjectLabelList;
            }
        }
        #endregion

        [HttpPost]
        [Route("Mobile_GetPDFStatus")]
        public string GetMobile_PDFStatus(GetPDFStatus GPDF)
        {
            try
            {
                tempconnectionstring();
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@CompanyName", mytype = SqlDbType.NVarChar, Value = GPDF.CompanyName });
                return con.ExecuteScalar("sp_Mobile_GetPDFStatus").ToString();
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }
        [HttpPost]
        [Route("CRTGetRegions")]
        public List<Regions> Regions()
        {
            List<Regions> Reg = new List<Regions>();
            tempconnectionstring();
            Connection con = new Connection();
            DataTable DT = con.GetDataTable("sp_CRT_GetRegions");
            foreach (DataRow row in DT.Rows)
            {
                Regions R = new Regions()
                {
                    RegionID = row["RegionID"].ToString(),
                    RegionName = row["RegionName"].ToString()
                };
                Reg.Add(R);
            }
            return Reg;
        }
        [HttpPost]
        [Route("CRTGetProvinces")]
        public List<Provinces> Provinces(Regions RegionParam)
        {
            List<Provinces> Prov = new List<Provinces>();
            tempconnectionstring();
            Connection con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@RegionID", mytype = SqlDbType.NVarChar, Value = RegionParam.RegionID });
            DataTable DT = con.GetDataTable("sp_CRT_GetProvinces");
            foreach (DataRow row in DT.Rows)
            {
                Provinces P = new Provinces()
                {
                    ProvinceID = row["ProvinceID"].ToString(),
                    ProvinceName = row["ProvinceName"].ToString()
                };
                Prov.Add(P);
            }
            return Prov;
        }
        [HttpPost]
        [Route("CRTGetProvincesv2")]
        public List<Provinces> Provincesv2(Regionsv2 RegionParam)
        {
            List<Provinces> Prov = new List<Provinces>();
            tempconnectionstring();
            Connection con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@RegionID", mytype = SqlDbType.NVarChar, Value = RegionParam.RegionID });
            con.myparameters.Add(new myParameters { ParameterName = "@companyalias", mytype = SqlDbType.NVarChar, Value = RegionParam.CompanyAlias });
            DataTable DT = con.GetDataTable("sp_CRT_GetProvincesv2");
            foreach (DataRow row in DT.Rows)
            {
                Provinces P = new Provinces()
                {
                    ProvinceID = row["ProvinceID"].ToString(),
                    ProvinceName = row["ProvinceName"].ToString()
                };
                Prov.Add(P);
            }
            return Prov;
        }
        [HttpPost]
        [Route("CRTGetCityMun")]
        public List<CityMunicipalities> CityMun(Provinces ProvParam)
        {
            List<CityMunicipalities> CityMun = new List<CityMunicipalities>();
            tempconnectionstring();
            Connection con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@ProvinceID", mytype = SqlDbType.NVarChar, Value = ProvParam.ProvinceID });
            DataTable DT = con.GetDataTable("sp_CRT_GetCityMun");
            foreach (DataRow row in DT.Rows)
            {
                CityMunicipalities CM = new CityMunicipalities()
                {
                    CityMunID = row["CityMunID"].ToString(),
                    CityMunName = row["CityMunName"].ToString()
                };
                CityMun.Add(CM);
            }
            return CityMun;
        }
        [HttpPost]
        [Route("CRTGetCityMunv2")]
        public List<CityMunicipalities> CityMunv2(Provincesv2 ProvParam)
        {
            List<CityMunicipalities> CityMun = new List<CityMunicipalities>();
            tempconnectionstring();
            Connection con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@ProvinceID", mytype = SqlDbType.NVarChar, Value = ProvParam.ProvinceID });
            con.myparameters.Add(new myParameters { ParameterName = "@companyalias", mytype = SqlDbType.NVarChar, Value = ProvParam.CompanyAlias });
            DataTable DT = con.GetDataTable("sp_CRT_GetCityMunv2");
            foreach (DataRow row in DT.Rows)
            {
                CityMunicipalities CM = new CityMunicipalities()
                {
                    CityMunID = row["CityMunID"].ToString(),
                    CityMunName = row["CityMunName"].ToString()
                };
                CityMun.Add(CM);
            }
            return CityMun;
        }
        [HttpPost]
        [Route("CRTCheckOnboardStatus")]
        public string CheckOnboardStatus(CRTMobileLoginCompany Param)
        {
            try
            {
                tempconnectionstring();
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@CompanyAlias", mytype = SqlDbType.VarChar, Value = Param.CompanyName });
                con.myparameters.Add(new myParameters { ParameterName = "@StartingID", mytype = SqlDbType.VarChar, Value = Param.StartingID });
                return con.ExecuteScalar("sp_CRT_CheckOnboardStatus").ToString();
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }

        [HttpPost]
        [Route("Mobile_GetAccountType")]
        public string Mobile_GetAccountType(GetAccountType GAT)
        {
            try
            {
                tempconnectionstring();
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@CompanyName", mytype = SqlDbType.VarChar, Value = GAT.CompanyName });
                con.myparameters.Add(new myParameters { ParameterName = "@StartingID", mytype = SqlDbType.VarChar, Value = GAT.StartingID });
                return con.ExecuteScalar("sp_Mobile_GetAccountType").ToString();
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }

        [HttpPost]
        [Route("Mobile_GetCompanyLandline")]
        public string Mobile_GetCompanyLandline(GetLandlineNumber GLN)
        {
            try
            {
                tempconnectionstring();
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@CompanyAlias", mytype = SqlDbType.VarChar, Value = GLN.CompanyAlias });
                return con.ExecuteScalar("sp_Mobile_GetCompanyLandline").ToString();
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }
        [HttpPost]
        [Route("CRTAddUserClientv2")]
        public string CRTAddUserClientv2(DisplayNewUserv2 newuser)
        {
            try
            {
                tempconnectionstring();
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@FirstName", mytype = SqlDbType.VarChar, Value = newuser.FirstName });
                con.myparameters.Add(new myParameters { ParameterName = "@MiddleName", mytype = SqlDbType.VarChar, Value = newuser.MiddleName });
                con.myparameters.Add(new myParameters { ParameterName = "@LastName", mytype = SqlDbType.VarChar, Value = newuser.LastName });
                con.myparameters.Add(new myParameters { ParameterName = "@Suffix", mytype = SqlDbType.VarChar, Value = newuser.Suffix });
                con.myparameters.Add(new myParameters { ParameterName = "@Branch", mytype = SqlDbType.VarChar, Value = newuser.Branch });
                con.myparameters.Add(new myParameters { ParameterName = "@CompanyName", mytype = SqlDbType.VarChar, Value = newuser.CompanyName });
                con.myparameters.Add(new myParameters { ParameterName = "@CreatedBy", mytype = SqlDbType.VarChar, Value = newuser.CreatedBy });
                con.myparameters.Add(new myParameters { ParameterName = "@StartingID", mytype = SqlDbType.VarChar, Value = newuser.StartingID });
                con.myparameters.Add(new myParameters { ParameterName = "@Datehired", mytype = SqlDbType.VarChar, Value = newuser.DateHired });
                con.myparameters.Add(new myParameters { ParameterName = "@AccountType", mytype = SqlDbType.VarChar, Value = newuser.AccountType });
                con.myparameters.Add(new myParameters { ParameterName = "@TIN", mytype = SqlDbType.VarChar, Value = newuser.EmployeeTIN });
                return con.ExecuteScalar("sp_CRT_AddUserClientv2");
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }
        [HttpPost]
        [Route("CRT_UpdateNewAddedUserClientv2")]
        public string CRT_UpdateNewAddedUserClientv2(DisplayNewUserv2 updatedata)
        {
            //string valid = "";
            //GetDatabase GDB = new GetDatabase();
            //GDB.ClientName = ClientData.CN;
            tempconnectionstring();
            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@CN", mytype = SqlDbType.NVarChar, Value = updatedata.CN });
            Connection.myparameters.Add(new myParameters { ParameterName = "@StartingID", mytype = SqlDbType.NVarChar, Value = updatedata.StartingID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@FirstName", mytype = SqlDbType.NVarChar, Value = updatedata.FirstName });
            Connection.myparameters.Add(new myParameters { ParameterName = "@MiddleName", mytype = SqlDbType.NVarChar, Value = updatedata.MiddleName });
            Connection.myparameters.Add(new myParameters { ParameterName = "@LastName", mytype = SqlDbType.NVarChar, Value = updatedata.LastName });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Suffix", mytype = SqlDbType.NVarChar, Value = updatedata.Suffix });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Email", mytype = SqlDbType.NVarChar, Value = updatedata.Email });
            Connection.myparameters.Add(new myParameters { ParameterName = "@DateHired", mytype = SqlDbType.NVarChar, Value = updatedata.DateHired });
            Connection.myparameters.Add(new myParameters { ParameterName = "@AccountType", mytype = SqlDbType.NVarChar, Value = updatedata.AccountType });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Password", mytype = SqlDbType.NVarChar, Value = updatedata.Password });
            Connection.myparameters.Add(new myParameters { ParameterName = "@TIN", mytype = SqlDbType.NVarChar, Value = updatedata.EmployeeTIN });

            //Connection.ExecuteScalar("sp_CRT_UpdatePassword").ToString();
            Connection.ExecuteNonQuery("sp_CRT_UpdateNewAddedUserClientv2");
            return "success";
        }
        [HttpPost]
        [Route("CRT_EmailValidationForImportv2")]
        public List<DuplicateUsers> DuplicateUsers(UserBulkUpload UBP)
        {
            DataTable UserList = new DataTable();
            UserList.Columns.AddRange(new DataColumn[10] { new DataColumn("EmpID"), new DataColumn("FName"), new DataColumn("MName"), new DataColumn("LName"), new DataColumn("AccountType"), new DataColumn("TIN"), new DataColumn("Birthdate"), new DataColumn("Suffix"), new DataColumn("Site"), new DataColumn("RowNum") });
            for (int i = 0; i < UBP.employeeID.Length; i++)
            {
                UserList.Rows.Add(UBP.rowNo[i], UBP.employeeID[i], UBP.fName[i], UBP.mName[i], UBP.lName[i], UBP.Suffix[i], UBP.birthDate[i], UBP.site[i], UBP.tin[i], UBP.accountType[i]);
            }
            List<DuplicateUsers> DU = new List<DuplicateUsers>();
            tempconnectionstring();
            Connection con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@CompanyName", mytype = SqlDbType.VarChar, Value = UBP.CN });
            con.myparameters.Add(new myParameters { ParameterName = "@IMPORT", mytype = SqlDbType.Structured, Value = UserList });
            DataTable DT = con.GetDataTable("sp_CRT_EmailValidationForImportv2");
            foreach (DataRow row in DT.Rows)
            {
                DuplicateUsers Users = new DuplicateUsers()
                {
                    RowID = row["RowID"].ToString(),
                    EmployeeID = row["StartingID"].ToString(),
                    Conflict = row["Conflict"].ToString()
                };
                DU.Add(Users);
            }
            //if (DT.Rows.Count > 0)
            //{
            //var message = new MimeKit.MimeMessage();
            //message.From.Add(new MimeKit.MailboxAddress("DurustHR", "durusthr@illimitado.com"));
            //message.To.Add(new MimeKit.MailboxAddress("btorres@illimitado.com"));
            //message.Subject = "Notification Upload From" + " " + UBP.CN;
            //var messBody = "The Company" + " " + UBP.CN + " " + "Has Uploaded a File";
            ////"Feel free to visit http://sbccrt.durusthr.com to login to your account.<br><br>" +
            ////"Here are your account details:<br>" +
            ////"<b>Company Name: </b>" + DT.Rows[0]["CompanyName"].ToString() + "<br>" +
            ////"<b>Company Alias: </b>" + DT.Rows[0]["CompanyAlias"].ToString() + "<br>" +
            ////"<b>Username: </b>" + DT.Rows[0]["UserName"].ToString() + "<br>" +
            ////"<b>Temporary Password: </b>" + DT.Rows[0]["Password"].ToString() + "<br><br>" +
            ////"Access http://sbccrt.durusthr.com anytime and anywhere with Chrome. Enjoy your account.</label>";
            //message.Body = new MimeKit.TextPart("html") { Text = messBody };

            //using (var client = new MailKit.Net.Smtp.SmtpClient())
            //{
            //    client.ServerCertificateValidationCallback = (s, c, h, e) => true;
            //    client.Connect("box1256.bluehost.com", 465, true);
            //    client.Authenticate("durusthr@illimitado.com", "Secured@1230");
            //    //client.Authenticate("btorres@illimitado.com", "btorres@1230");
            //    client.Send(message);
            //    client.Disconnect(true);
            //}
            //}


            return DU;
        }
        /////Testing////
        [HttpPost]
        [Route("CRT_EmailValidationForImportv2v2")]
        public List<DuplicateUsers> DuplicateUsers222(UserBulkUpload UBP)
        {
            TimeZoneInfo timeInfo = TimeZoneInfo.FindSystemTimeZoneById("Taipei Standard Time");

            //DateTime currentTime = TimeZoneInfo.ConvertTime(UBP.birthDate[i], TimeZoneInfo.FindSystemTimeZoneById("Central Standard Time"));
            DataTable UserList = new DataTable();
            UserList.Columns.AddRange(new DataColumn[10] { new DataColumn("EmpID"), new DataColumn("FName"), new DataColumn("MName"), new DataColumn("LName"), new DataColumn("AccountType"), new DataColumn("TIN"), new DataColumn("Birthdate"), new DataColumn("Suffix"), new DataColumn("Site"), new DataColumn("RowNum") });
            for (int i = 0; i < UBP.employeeID.Length; i++)
            {
                UserList.Rows.Add(UBP.rowNo[i], UBP.employeeID[i], UBP.fName[i], UBP.mName[i], UBP.lName[i], UBP.Suffix[i], TimeZoneInfo.ConvertTimeFromUtc(Convert.ToDateTime(UBP.birthDate[i]), timeInfo)/*UBP.birthDate[i]*/, UBP.site[i], UBP.tin[i], UBP.accountType[i]);
            }
            List<DuplicateUsers> DU = new List<DuplicateUsers>();
            tempconnectionstring();
            Connection con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@Timezone", mytype = SqlDbType.VarChar, Value = DateTime.UtcNow });
            con.myparameters.Add(new myParameters { ParameterName = "@CompanyName", mytype = SqlDbType.VarChar, Value = UBP.CN });
            con.myparameters.Add(new myParameters { ParameterName = "@IMPORT", mytype = SqlDbType.Structured, Value = UserList });
            DataTable DT = con.GetDataTable("sp_CRT_EmailValidationForImportv4v2");
            foreach (DataRow row in DT.Rows)
            {
                DuplicateUsers Users = new DuplicateUsers()
                {
                    RowID = row["RowID"].ToString(),
                    EmployeeID = row["StartingID"].ToString(),
                    Conflict = row["Conflict"].ToString()
                };
                DU.Add(Users);
            }
            return DU;
        }
        ///// testing////
        [HttpPost]
        [Route("CRT_EmailValidationForImportv3")]
        public List<DuplicateUsers> DuplicateUsersv2(UserBulkUpload UBP)
        {
            DataTable UserList = new DataTable();
            UserList.Columns.AddRange(new DataColumn[10] { new DataColumn("EmpID"), new DataColumn("FName"), new DataColumn("MName"), new DataColumn("LName"), new DataColumn("AccountType"), new DataColumn("TIN"), new DataColumn("Birthdate"), new DataColumn("Suffix"), new DataColumn("Site"), new DataColumn("RowNum") });
            for (int i = 0; i < UBP.employeeID.Length; i++)
            {
                UserList.Rows.Add(UBP.rowNo[i], UBP.employeeID[i], UBP.fName[i], UBP.mName[i], UBP.lName[i], UBP.Suffix[i], UBP.birthDate[i], UBP.site[i], UBP.tin[i], UBP.accountType[i]);
            }
            List<DuplicateUsers> DU = new List<DuplicateUsers>();
            tempconnectionstring();
            Connection con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@CompanyName", mytype = SqlDbType.VarChar, Value = UBP.CN });
            con.myparameters.Add(new myParameters { ParameterName = "@IMPORT", mytype = SqlDbType.Structured, Value = UserList });
            DataTable DT = con.GetDataTable("[sp_CRT_EmailValidationForImportv4");
            foreach (DataRow row in DT.Rows)
            {
                DuplicateUsers Users = new DuplicateUsers()
                {
                    RowID = row["RowID"].ToString(),
                    EmployeeID = row["StartingID"].ToString(),
                    Conflict = row["Conflict"].ToString()
                };
                DU.Add(Users);
            }
            return DU;
        }
        [HttpPost]
        [Route("CRTDisplayNewUserv2")]
        public GetDisplayNewUserv2 CRTDisplayNewUserv2(CRTLogin login)
        {

            GetDisplayNewUserv2 GetDisplayNewUsers = new GetDisplayNewUserv2();
            List<DisplayNewUserv2> UserDetails = new List<DisplayNewUserv2>();
            tempconnectionstring();
            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@CN", mytype = SqlDbType.NVarChar, Value = login.CN });

            DataTable dtuserList = new DataTable();

            dtuserList = Connection.GetDataTable("sp_CRT_DisplayNewUser");
            if (dtuserList.Rows.Count > 0)
            {
                foreach (DataRow row in dtuserList.Rows)
                {
                    DisplayNewUserv2 Displaynewusers = new DisplayNewUserv2
                    {
                        Branch = row["Branch"].ToString(),
                        NumberOfUser = row["NumberOfUser"].ToString(),
                        StartingID = row["StartingID"].ToString(),
                        AccountType = row["AccountType"].ToString(),
                        DateHired = row["DateHired"].ToString(),
                        isSent = row["isSent"].ToString(),
                        Password = row["Password"].ToString(),
                        Email = row["Email"].ToString(),
                        FirstName = row["FirstName"].ToString(),
                        MiddleName = row["MiddleName"].ToString(),
                        LastName = row["LastName"].ToString(),
                        Suffix = row["Suffix"].ToString(),
                        EmployeeTIN = row["EmployeeTIN"].ToString()
                    };
                    UserDetails.Add(Displaynewusers);
                }
                GetDisplayNewUsers.Displaynewuser = UserDetails;
                GetDisplayNewUsers.myreturn = "Success";
            }
            else
            {
                GetDisplayNewUsers.myreturn = "Error";
            }
            return GetDisplayNewUsers;
        }
        [HttpPost]
        [Route("GenerateAndSendSALADv2")]
        public string GenerateAndSendSALADv2(SaladParams SP)
        {
            try
            {
                tempconnectionstring();
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@StartingID", mytype = SqlDbType.VarChar, Value = SP.StartingID });
                con.myparameters.Add(new myParameters { ParameterName = "@CN", mytype = SqlDbType.VarChar, Value = SP.CN });
                DataTable DT = con.GetDataTable("sp_CRT_GenerateSalad");
                foreach (DataRow row in DT.Rows)
                {
                    #region Generate SALAD FILE
                    var SALADFILE = "Salary_Credit_Agreement_Form_-_DIRECT_2016_10_06_No_MoA_New";
                    PdfReader reader = new PdfReader(HttpContext.Current.Server.MapPath("~/pdf/") + SALADFILE + ".pdf");
                    var newFile = HttpContext.Current.Server.MapPath("pdf") + "/" + "Salary_Credit_Agreement_Form_" + SP.StartingID + ".pdf";
                    PdfStamper stamper = new PdfStamper(reader, new FileStream(newFile, FileMode.Create));
                    AcroFields fields = stamper.AcroFields;



                    // Page 3
                    fields.SetField("id1", row["id1"].ToString());

                    Byte[] ID1Bit = null;
                    Image ID1 = null;
                    var ID1ContentByte = stamper.GetOverContent(3); // 3 represents page number
                    AcroFields.FieldPosition fieldpos = fields.GetFieldPositions("img1")[0];
                    Rectangle rect = fieldpos.position;
                    float xxxx = rect.Left;
                    float yyyy = rect.Bottom;
                    try
                    {
                        string img1url = "http://ws.durusthr.com/ilm_ws_live/" + row["img1"].ToString();
                        using (var webClient = new WebClient())
                        {
                            ID1Bit = webClient.DownloadData(img1url);
                        }
                        ID1 = Image.GetInstance(ID1Bit); // convert to byte array
                        ID1.SetAbsolutePosition(xxxx, yyyy); // set position inside field to place picture 
                        ID1.ScaleAbsolute(rect); // set absolute size of image as per size of field itself
                        ID1ContentByte.AddImage(ID1); // add image to field
                    }
                    catch (Exception)
                    {

                    }

                    fields.SetField("id2", row["id2"].ToString());

                    Byte[] ID2Bit = null;
                    Image ID2 = null;
                    var ID2ContentByte = stamper.GetOverContent(3); // 3 represents page number
                    fieldpos = fields.GetFieldPositions("img2")[0];
                    rect = fieldpos.position;
                    xxxx = rect.Left;
                    yyyy = rect.Bottom;
                    try
                    {
                        string img2url = "http://ws.durusthr.com/ilm_ws_live/" + row["img2"].ToString();
                        using (var webClient = new WebClient())
                        {
                            ID2Bit = webClient.DownloadData(img2url);
                        }
                        ID2 = Image.GetInstance(ID2Bit); // convert to byte array
                        ID2.SetAbsolutePosition(xxxx, yyyy); // set position inside field to place picture 
                        ID2.ScaleAbsolute(rect); // set absolute size of image as per size of field itself
                        ID2ContentByte.AddImage(ID2); // add image to field
                    }
                    catch (Exception)
                    {

                    }

                    // Page 2
                    Byte[] EmpsignBit = null;
                    Image Empsign = null;
                    var EmpsignContentByte = stamper.GetOverContent(2); // 2 represents page number
                    fieldpos = fields.GetFieldPositions("signature")[0];
                    rect = fieldpos.position;
                    xxxx = rect.Left;
                    yyyy = rect.Bottom;
                    try
                    {
                        string someUrl = "http://ws.durusthr.com/ilm_ws_live/" + row["Signature"].ToString();
                        using (var webClient = new WebClient())
                        {
                            EmpsignBit = webClient.DownloadData(someUrl); // get image
                        }
                        Empsign = Image.GetInstance(EmpsignBit); // convert to byte array
                        Empsign.SetAbsolutePosition(xxxx, yyyy); // set position inside field to place picture 
                        Empsign.ScaleAbsolute(rect); // set absolute size of image as per size of field itself
                        EmpsignContentByte.AddImage(Empsign); // add image to field
                    }
                    catch (Exception)
                    {

                    }

                    fields.SetField("printedname", row["FirstName"].ToString() + " " + row["MiddleName"].ToString() + ' ' + row["LastName"].ToString());


                    // Page 1
                    fields.SetField("lastName", row["LastName"].ToString());
                    fields.SetField("firstName", row["FirstName"].ToString());
                    fields.SetField("middleName", row["MiddleName"].ToString());
                    fields.SetField("birthMonth", row["birthMonth"].ToString());
                    fields.SetField("birthDay", row["birthDay"].ToString());
                    fields.SetField("birthYear", row["birthYear"].ToString());
                    fields.SetField("placeOfBirth", row["PlaceOfBirth"].ToString());
                    if (row["Gender"].ToString() == "Male")
                    {
                        fields.SetField("maleCheck", "Yes", true); // checkbox options : "Yes":"No", true
                    }
                    else
                    {
                        fields.SetField("femaleCheck", "Yes", true); // checkbox options : "Yes":"No", true
                    }
                    fields.SetField("citizenship", row["Nationality"].ToString());

                    if (row["CivilStatus"].ToString() == "Single")
                    {
                        fields.SetField("singleCheck", "Yes", true); // checkbox options : "Yes":"No", true
                    }
                    else if (row["CivilStatus"].ToString() == "Married")
                    {
                        fields.SetField("marriedCheck", "Yes", true); // checkbox options : "Yes":"No", true
                    }
                    else if (row["CivilStatus"].ToString() == "Legally Separated")
                    {
                        fields.SetField("legallySeparatedCheck", "Yes", true); // checkbox options : "Yes":"No", true
                    }
                    else if (row["CivilStatus"].ToString() == "Annulled")
                    {
                        fields.SetField("annulledCheck", "Yes", true); // checkbox options : "Yes":"No", true
                    }
                    else if (row["CivilStatus"].ToString() == "Widow")
                    {
                        fields.SetField("widowCheck", "Yes", true); // checkbox options : "Yes":"No", true
                    }
                    fields.SetField("tinSSSgsis", row["EmployeeTIN"].ToString());
                    fields.SetField("presentHomeAddress", row["PresentHomeAddress"].ToString());
                    fields.SetField("permanentHomeAddress", row["PermanentHomeAddress"].ToString());
                    fields.SetField("landlinePresent", row["LandlinePresent"].ToString());
                    fields.SetField("landlinePermanent", row["LandlinePermanent"].ToString());
                    fields.SetField("mobileRequired", row["MobileNoRequired"].ToString());
                    fields.SetField("mobileAlternative", row["MobileNoAlternative"].ToString());
                    fields.SetField("emailPersonal", row["EmailPersonal"].ToString());
                    fields.SetField("emailWork", row["EmailWork"].ToString());
                    fields.SetField("employerName", row["CompanyName"].ToString());
                    fields.SetField("occupation", row["Position"].ToString());
                    fields.SetField("dateOfHire", row["DateHired"].ToString());
                    fields.SetField("salaryCheck", "Yes", true); // checkbox options : "Yes":"No", true // always checked
                    if (row["EmploymentStatus"].ToString() == "Permanent/Regular")
                    {
                        fields.SetField("permanentEmploymentCheck", "Yes", true); // checkbox options : "Yes":"No", true
                        fields.SetField("contractualDate", ""); // shows if contractualEmployment is checked: contains contract end date
                    }
                    else if (row["EmploymentStatus"].ToString() == "Contractual/Project Hire")
                    {
                        fields.SetField("contractualEmploymentCheck", "Yes", true); // checkbox options : "Yes":"No", true
                        fields.SetField("contractualDate", row["EndOfContract"].ToString()); // shows if contractualEmployment is checked: contains contract end date
                    }
                    else
                    {
                        fields.SetField("othersEmploymentCheck", "Yes", true); // checkbox options : "Yes":"No", true
                        fields.SetField("othersEmploymentStatus", row["EmploymentStatus"].ToString());
                    }
                    fields.SetField("referencePerson1", row["Reference1Name"].ToString());
                    fields.SetField("referencePerson2", row["Reference2Name"].ToString());
                    fields.SetField("referenceContact1", row["Reference1Contact"].ToString());
                    fields.SetField("referenceContact2", row["Reference2Contact"].ToString());

                    if (row["Rank"].ToString() == "Staff/Rank and File")
                    {
                        fields.SetField("rankAndFile", "Yes", true); // checkbox options : "Yes":"No", true
                    }
                    else if (row["Rank"].ToString() == "Supervisor")
                    {
                        fields.SetField("supervisor", "Yes", true); // checkbox options : "Yes":"No", true
                    }
                    else if (row["Rank"].ToString() == "Manager/Junior Officer")
                    {
                        fields.SetField("manager", "Yes", true); // checkbox options : "Yes":"No", true
                    }
                    else if (row["Rank"].ToString() == "Executives")
                    {
                        fields.SetField("executive", "Yes", true); // checkbox options : "Yes":"No", true
                    }

                    fields.SetField("Site", row["Branch"].ToString());

                    stamper.FormFlattening = true;
                    stamper.Close();
                    #endregion

                    #region Send To Email
                    try
                    {
                        string prefix = HttpContext.Current.Server.MapPath("~/pdf/");
                        var message = new MimeKit.MimeMessage();
                        message.From.Add(new MimeKit.MailboxAddress("DurustHR", "durusthr@illimitado.com"));
                        if (row["EmailWork"] == DBNull.Value || row["EmailWork"].ToString() == "")
                        {
                            if (row["EmailPersonal"] == DBNull.Value || row["EmailPersonal"].ToString() == "")
                            {
                                message.To.Add(new MimeKit.MailboxAddress("durusthr@illimitado.com"));
                            }
                            else
                            {
                                message.To.Add(new MimeKit.MailboxAddress(row["EmailPersonal"].ToString()));
                            }
                        }
                        else
                        {
                            message.To.Add(new MimeKit.MailboxAddress(row["EmailWork"].ToString()));
                        }

                        message.Subject = "Salary Credit Agreement";
                        var builder = new MimeKit.BodyBuilder();

                        // Set the plain-text version of the message text
                        builder.TextBody = "Please see attachments.";

                        // We may also want to attach a calendar event for Monica's party...
                        builder.Attachments.Add(prefix + "Salary_Credit_Agreement_Form_" + SP.StartingID + ".pdf");

                        // Now we just need to set the message body and we're done
                        message.Body = builder.ToMessageBody();

                        using (var client = new MailKit.Net.Smtp.SmtpClient())
                        {
                            client.ServerCertificateValidationCallback = (s, c, h, e) => true;
                            client.Connect("box1256.bluehost.com", 465, true);
                            client.Authenticate("durusthr@illimitado.com", "@1230Qwerty");
                            client.Send(message);
                            client.Disconnect(true);
                        }
                        return "Success";
                    }
                    catch (Exception e)
                    {
                        return e.ToString();
                    }
                    #endregion
                }
                return "Success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }
        [HttpPost]
        [Route("ShowCrt_CLientListv3")]
        public GetClientDetailsv2 GetAllClientv3(InsertClientDatav2 admin)
        {
            GetClientDetailsv2 GetClientDetails = new GetClientDetailsv2();
            List<ClientDetailsv2> ClientDetails = new List<ClientDetailsv2>();
            tempconnectionstring();
            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@CN", mytype = SqlDbType.NVarChar, Value = admin.CN });
            Connection.myparameters.Add(new myParameters { ParameterName = "@CClientID", mytype = SqlDbType.NVarChar, Value = admin.CClientID });
            DataTable dtClientList = new DataTable();
            dtClientList = Connection.GetDataTable("sp_ClientListv2");
            if (dtClientList.Rows.Count > 0)
            {
                foreach (DataRow row in dtClientList.Rows)
                {
                    ClientDetailsv2 Clientdetail = new ClientDetailsv2
                    {
                        ClientId = row["ClientId"].ToString(),
                        CN = row["CompanyName"].ToString(),
                        UserName = row["UserName"].ToString(),
                        CompanyAlias = row["CompanyAlias"].ToString(),
                        DateofIncorporation = row["DateofIncorporation"].ToString(),
                        AddressFloor = row["Floor"].ToString(),
                        AddressBuilding = row["Building"].ToString(),
                        AddressStreet = row["Street"].ToString(),
                        AddressBrgy = row["Barangay"].ToString(),
                        AddressMunicipality = row["Municipality"].ToString(),
                        AddressProvince = row["Province"].ToString(),
                        AddressRegion = row["Region"].ToString(),
                        AddressCountry = row["Country"].ToString(),
                        AddressZipCode = row["ZipCode"].ToString(),
                        NationalityCountry = row["NationalityCountry"].ToString(),
                        CompanyEQCustomerNumber = row["CompanyEQCustomerNumber"].ToString(),
                        TotalNumberOfEmployees = row["TotalNumberOfEmployees"].ToString(),
                        AccountType = row["AccountType"].ToString(),
                        ContactType = row["ContactType"].ToString(),
                        ContactNumber = row["ContactNumber"].ToString(),
                        EmailType = row["EmailType"].ToString(),
                        EmailAddress = row["Email"].ToString(),
                        Website = row["Website"].ToString(),
                        SSS = row["SSS"].ToString(),
                        PhilHealth = row["Philhealth"].ToString(),
                        Tin = row["TIN"].ToString(),
                        TinRDO = row["TINRdo"].ToString(),
                        PagIbig = row["PagIbig"].ToString(),
                        PagibigBranch = row["PagibigBranch"].ToString(),
                        FirstName = row["First_Name"].ToString(),
                        MiddleName = row["Middle_Name"].ToString(),
                        LastName = row["Last_Name"].ToString(),
                        RepresentativeTitle = row["Representative_Title"].ToString(),
                        RepresentativeContactNumber = row["Representative_Contact_Number"].ToString(),
                        isDeleted = row["isDeleted"].ToString(),
                        SaladInfo = Convert.ToBoolean(row["salad_Status"].ToString()),
                        SaladPDF = Convert.ToBoolean(row["PDF_Status"].ToString()),
                        SaladSigID = Convert.ToBoolean(row["SaladSigID"].ToString())
                    };
                    ClientDetails.Add(Clientdetail);
                }
                GetClientDetails.Clientdetails = ClientDetails;
                GetClientDetails.myreturn = "Success";
            }
            else
            {
                GetClientDetails.myreturn = "Error";
            }
            return GetClientDetails;
        }
        [HttpPost]
        [Route("Mobile_Company_Verificationv2")]
        public CRTMobileCompanyDetails Mobile_Company_Details(CRTMobileCompanyVerification MobileCompanyVerification)
        {
            CRTMobileCompanyDetails CRTMCD = new CRTMobileCompanyDetails();
            try
            {
                var settings = ConfigurationManager.ConnectionStrings["connectionstring"];
                var fi = typeof(ConfigurationElement).GetField(
                              "_bReadOnly",
                              BindingFlags.Instance | BindingFlags.NonPublic);
                fi.SetValue(settings, false);
                settings.ConnectionString = "Data Source=livedb-ilm.c3srcolvclfy.ap-southeast-1.rds.amazonaws.com,1412;Database=ILM_Main;User Id=ILM_LiveDBUser;Password=ILMV3ry53cur3dMGTpa55w0rd2020;";

                Connection Connection = new Connection();
                Connection.myparameters.Add(new myParameters { ParameterName = "@DBNAME", mytype = SqlDbType.NVarChar, Value = MobileCompanyVerification.CompanyAlias });
                var exist = Connection.ExecuteScalar("sp_IsDBExist").ToString();

                if (exist == "Existing")
                {
                    CRTMCD.CompanyType = "durusthr";
                    // return "durusthr";
                }
                else
                {
                    tempconnectionstring();
                    Connection = new Connection();
                    Connection.myparameters.Add(new myParameters { ParameterName = "@CompanyAlias", mytype = SqlDbType.NVarChar, Value = MobileCompanyVerification.CompanyAlias });
                    DataTable DT = Connection.GetDataTable("sp_Mobile_Company_Verification");
                    foreach (DataRow row in DT.Rows)
                    {
                        if (row["Validity"].ToString() == "True")
                        {
                            CRTMCD.CompanyType = "crt";
                            CRTMCD.CompanyName = row["CompanyName"].ToString();
                        }
                        else
                        {
                            CRTMCD.CompanyType = "none";
                        }
                    }
                }
            }

            catch (Exception e)
            {
                CRTMCD.CompanyType = e.ToString();
            }
            return CRTMCD;
        }
        [HttpPost]
        [Route("CRT_TINValidation")]
        public string CRT_TINValidation(CRTTINValidation ClientData)
        {
            try
            {
                tempconnectionstring();
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@EmployeeTIN", mytype = SqlDbType.NVarChar, Value = ClientData.TIN });
                return con.ExecuteScalar("sp_CRT_CheckEmployeeTIN").ToString();
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }
        [HttpPost]
        [Route("CRT_InsertClientDatav4")]
        public List<CRTComRepv2> ComRepv2(InsertClientDatav3 ClientData)
        {
            List<CRTComRepv2> ComRep = new List<CRTComRepv2>();
            tempconnectionstring();
            Connection con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@CN", mytype = SqlDbType.NVarChar, Value = ClientData.CN });
            con.myparameters.Add(new myParameters { ParameterName = "@CompanyAlias", mytype = SqlDbType.NVarChar, Value = ClientData.CompanyAlias });
            con.myparameters.Add(new myParameters { ParameterName = "@DateofIncorporation", mytype = SqlDbType.NVarChar, Value = ClientData.DateofIncorporation });
            con.myparameters.Add(new myParameters { ParameterName = "@AddressFloor", mytype = SqlDbType.NVarChar, Value = ClientData.AddressFloor });
            con.myparameters.Add(new myParameters { ParameterName = "@AddressBuilding", mytype = SqlDbType.NVarChar, Value = ClientData.AddressBuilding });
            con.myparameters.Add(new myParameters { ParameterName = "@AddressStreet", mytype = SqlDbType.NVarChar, Value = ClientData.AddressStreet });
            con.myparameters.Add(new myParameters { ParameterName = "@AddressBrgy", mytype = SqlDbType.NVarChar, Value = ClientData.AddressBrgy });
            con.myparameters.Add(new myParameters { ParameterName = "@AddressMunicipality", mytype = SqlDbType.NVarChar, Value = ClientData.AddressMunicipality });
            con.myparameters.Add(new myParameters { ParameterName = "@AddressProvince", mytype = SqlDbType.NVarChar, Value = ClientData.AddressProvince });
            con.myparameters.Add(new myParameters { ParameterName = "@AddressRegion", mytype = SqlDbType.NVarChar, Value = ClientData.AddressRegion });
            con.myparameters.Add(new myParameters { ParameterName = "@AddressCountry", mytype = SqlDbType.NVarChar, Value = ClientData.AddressCountry });
            con.myparameters.Add(new myParameters { ParameterName = "@AddressZipCode", mytype = SqlDbType.NVarChar, Value = ClientData.AddressZipCode });
            con.myparameters.Add(new myParameters { ParameterName = "@NationalityCountry", mytype = SqlDbType.NVarChar, Value = ClientData.NationalityCountry });
            con.myparameters.Add(new myParameters { ParameterName = "@CompanyEQCustomerNumber", mytype = SqlDbType.NVarChar, Value = ClientData.CompanyEQCustomerNumber });
            con.myparameters.Add(new myParameters { ParameterName = "@TotalNumberOfEmployees", mytype = SqlDbType.NVarChar, Value = ClientData.TotalNumberOfEmployees });
            con.myparameters.Add(new myParameters { ParameterName = "@AccountType", mytype = SqlDbType.NVarChar, Value = ClientData.AccountType });
            con.myparameters.Add(new myParameters { ParameterName = "@ContactNumber", mytype = SqlDbType.NVarChar, Value = ClientData.ContactNumber });
            con.myparameters.Add(new myParameters { ParameterName = "@Tin", mytype = SqlDbType.NVarChar, Value = ClientData.Tin });
            con.myparameters.Add(new myParameters { ParameterName = "@isDeleted", mytype = SqlDbType.NVarChar, Value = ClientData.isDeleted });
            con.myparameters.Add(new myParameters { ParameterName = "@CreatedById", mytype = SqlDbType.NVarChar, Value = ClientData.CreatedById });
            con.myparameters.Add(new myParameters { ParameterName = "@PDF_Status", mytype = SqlDbType.NVarChar, Value = ClientData.PDF_Status });
            con.myparameters.Add(new myParameters { ParameterName = "@Salad_Info", mytype = SqlDbType.NVarChar, Value = ClientData.Salad_Info });
            con.myparameters.Add(new myParameters { ParameterName = "@CClientID", mytype = SqlDbType.NVarChar, Value = ClientData.CClientID });
            con.myparameters.Add(new myParameters { ParameterName = "@SaladSigID", mytype = SqlDbType.NVarChar, Value = ClientData.SaladSigID });
            con.myparameters.Add(new myParameters { ParameterName = "@PosRank", mytype = SqlDbType.Bit, Value = ClientData.PosRank });
            con.myparameters.Add(new myParameters { ParameterName = "@NameOfTeam", mytype = SqlDbType.NVarChar, Value = ClientData.NameOfTeam });
            DataTable Params = new DataTable();
            Params.Columns.AddRange(new DataColumn[7] { new DataColumn("Username"), new DataColumn("Firstname"), new DataColumn("Middlename"), new DataColumn("Lastname"), new DataColumn("RepTitle"), new DataColumn("RepContact"), new DataColumn("RepEmail") });
            for (int i = 0; i < ClientData.UserName.Length; i++)
            {
                Params.Rows.Add(ClientData.UserName[i], ClientData.FirstName[i], ClientData.MiddleName[i], ClientData.LastName[i], ClientData.RepresentativeTitle[i], ClientData.RepresentativeContactNumber[i], ClientData.EmailAddress[i]);
            }
            con.myparameters.Add(new myParameters { ParameterName = "@ComRepInfo", mytype = SqlDbType.Structured, Value = Params });
            DataTable DT = con.GetDataTable("sp_CRT_AddClientv4");
            foreach (DataRow row in DT.Rows)
            {
                CRTComRepv2 CR = new CRTComRepv2()
                {
                    item = row["item"].ToString(),
                    reason = row["reason"].ToString()
                };
                ComRep.Add(CR);
            }
            return ComRep;
        }

        [HttpPost]
        [Route("CRT_InsertClientDatav3")]
        public List<CRTComRep> ComRep(InsertClientDatav3 ClientData)
        {
            List<CRTComRep> ComRep = new List<CRTComRep>();
            tempconnectionstring();
            Connection con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@CN", mytype = SqlDbType.NVarChar, Value = ClientData.CN });
            con.myparameters.Add(new myParameters { ParameterName = "@CompanyAlias", mytype = SqlDbType.NVarChar, Value = ClientData.CompanyAlias });
            con.myparameters.Add(new myParameters { ParameterName = "@DateofIncorporation", mytype = SqlDbType.NVarChar, Value = ClientData.DateofIncorporation });
            con.myparameters.Add(new myParameters { ParameterName = "@AddressFloor", mytype = SqlDbType.NVarChar, Value = ClientData.AddressFloor });
            con.myparameters.Add(new myParameters { ParameterName = "@AddressBuilding", mytype = SqlDbType.NVarChar, Value = ClientData.AddressBuilding });
            con.myparameters.Add(new myParameters { ParameterName = "@AddressStreet", mytype = SqlDbType.NVarChar, Value = ClientData.AddressStreet });
            con.myparameters.Add(new myParameters { ParameterName = "@AddressBrgy", mytype = SqlDbType.NVarChar, Value = ClientData.AddressBrgy });
            con.myparameters.Add(new myParameters { ParameterName = "@AddressMunicipality", mytype = SqlDbType.NVarChar, Value = ClientData.AddressMunicipality });
            con.myparameters.Add(new myParameters { ParameterName = "@AddressProvince", mytype = SqlDbType.NVarChar, Value = ClientData.AddressProvince });
            con.myparameters.Add(new myParameters { ParameterName = "@AddressRegion", mytype = SqlDbType.NVarChar, Value = ClientData.AddressRegion });
            con.myparameters.Add(new myParameters { ParameterName = "@AddressCountry", mytype = SqlDbType.NVarChar, Value = ClientData.AddressCountry });
            con.myparameters.Add(new myParameters { ParameterName = "@AddressZipCode", mytype = SqlDbType.NVarChar, Value = ClientData.AddressZipCode });
            con.myparameters.Add(new myParameters { ParameterName = "@NationalityCountry", mytype = SqlDbType.NVarChar, Value = ClientData.NationalityCountry });
            con.myparameters.Add(new myParameters { ParameterName = "@CompanyEQCustomerNumber", mytype = SqlDbType.NVarChar, Value = ClientData.CompanyEQCustomerNumber });
            con.myparameters.Add(new myParameters { ParameterName = "@TotalNumberOfEmployees", mytype = SqlDbType.NVarChar, Value = ClientData.TotalNumberOfEmployees });
            con.myparameters.Add(new myParameters { ParameterName = "@AccountType", mytype = SqlDbType.NVarChar, Value = ClientData.AccountType });
            con.myparameters.Add(new myParameters { ParameterName = "@ContactNumber", mytype = SqlDbType.NVarChar, Value = ClientData.ContactNumber });
            con.myparameters.Add(new myParameters { ParameterName = "@Tin", mytype = SqlDbType.NVarChar, Value = ClientData.Tin });
            con.myparameters.Add(new myParameters { ParameterName = "@isDeleted", mytype = SqlDbType.NVarChar, Value = ClientData.isDeleted });
            con.myparameters.Add(new myParameters { ParameterName = "@CreatedById", mytype = SqlDbType.NVarChar, Value = ClientData.CreatedById });
            con.myparameters.Add(new myParameters { ParameterName = "@PDF_Status", mytype = SqlDbType.NVarChar, Value = ClientData.PDF_Status });
            con.myparameters.Add(new myParameters { ParameterName = "@Salad_Info", mytype = SqlDbType.NVarChar, Value = ClientData.Salad_Info });
            con.myparameters.Add(new myParameters { ParameterName = "@CClientID", mytype = SqlDbType.NVarChar, Value = ClientData.CClientID });
            con.myparameters.Add(new myParameters { ParameterName = "@SaladSigID", mytype = SqlDbType.NVarChar, Value = ClientData.SaladSigID });

            DataTable Params = new DataTable();
            Params.Columns.AddRange(new DataColumn[7] { new DataColumn("Username"), new DataColumn("Firstname"), new DataColumn("Middlename"), new DataColumn("Lastname"), new DataColumn("RepTitle"), new DataColumn("RepContact"), new DataColumn("RepEmail") });
            for (int i = 0; i < ClientData.UserName.Length; i++)
            {
                Params.Rows.Add(ClientData.UserName[i], ClientData.FirstName[i], ClientData.MiddleName[i], ClientData.LastName[i], ClientData.RepresentativeTitle[i], ClientData.RepresentativeContactNumber[i], ClientData.EmailAddress[i]);
            }
            con.myparameters.Add(new myParameters { ParameterName = "@ComRepInfo", mytype = SqlDbType.Structured, Value = Params });
            DataTable DT = con.GetDataTable("sp_CRT_AddClientv3");
            foreach (DataRow row in DT.Rows)
            {
                CRTComRep CR = new CRTComRep()
                {
                    item = row["item"].ToString(),
                    reason = row["reason"].ToString()
                };
                ComRep.Add(CR);
            }
            return ComRep;
        }
        [HttpPost]
        [Route("CRTUserLoginv3")]
        public string CRTUserLoginv3(CRTLogin login)
        {
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = login.CN;
            tempconnectionstring();

            try
            {
                string valid = "";
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@CN", mytype = SqlDbType.NVarChar, Value = login.CN });
                con.myparameters.Add(new myParameters { ParameterName = "@UserID", mytype = SqlDbType.NVarChar, Value = login.UserID });
                con.myparameters.Add(new myParameters { ParameterName = "@Password", mytype = SqlDbType.NVarChar, Value = login.Password });
                valid = con.ExecuteScalar("sp_CRT_UserLoginv3").ToString();
                return valid;
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }
        [HttpPost]
        [Route("CRT_CheckPasswordDefaultv2")]
        public string CRT_CheckPasswordDefaultv2(InsertClientData ClientData)
        {
            try
            {
                tempconnectionstring();
                Connection Connection = new Connection();
                Connection.myparameters.Add(new myParameters { ParameterName = "@UserName", mytype = SqlDbType.NVarChar, Value = ClientData.UserName });
                return Connection.ExecuteScalar("sp_CRT_CheckPasswordDefaultv2").ToString();
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }
        [HttpPost]
        [Route("CRT_UpdatePasswordUserNamev2")]
        public string CRT_UpdatePasswordUserNamev2(InsertClientData ClientData)
        {
            try
            {
                tempconnectionstring();
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@UserName", mytype = SqlDbType.NVarChar, Value = ClientData.UserName });
                con.myparameters.Add(new myParameters { ParameterName = "@Password", mytype = SqlDbType.NVarChar, Value = ClientData.Password });
                return con.ExecuteScalar("sp_CRT_UpdatePasswordUserNamev2");
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }
        [HttpPost]
        [Route("CRTSendNewClientEmailv3")]
        public string SendNewClientEmailv3(SendNewClientEmailv2 SNCE)
        {
            try
            {
                tempconnectionstring();

                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@UserName", mytype = SqlDbType.NVarChar, Value = SNCE.UserName });
                con.myparameters.Add(new myParameters { ParameterName = "@CClientID", mytype = SqlDbType.NVarChar, Value = SNCE.CClientID });
                con.myparameters.Add(new myParameters { ParameterName = "@CompanyAlias", mytype = SqlDbType.NVarChar, Value = SNCE.CompanyAlias });
                DataTable DT = con.GetDataTable("sp_CRT_SendEmailToClientv3");

                if (DT.Rows.Count > 0)
                {
                    //    var message = new MimeKit.MimeMessage();
                    //    message.From.Add(new MimeKit.MailboxAddress("DurustHR", "durusthr@illimitado.com"));
                    //    message.To.Add(new MimeKit.MailboxAddress(DT.Rows[0]["Email"].ToString()));
                    //    message.Subject = "Notifications";
                    //    var messBody = "<label>Welcome to DurustHR CRT Admin portal, in parternship with Security Bank. Thank you for using DurustHR.<br>" +
                    //        "Feel free to visit https://sbccrt.durusthr.com to login to your account.<br><br>" +
                    //        "Here are your account details:<br>" +
                    //        "<b>Company Name: </b>" + DT.Rows[0]["CompanyName"].ToString() + "<br>" +
                    //        "<b>Company Alias: </b>" + DT.Rows[0]["CompanyAlias"].ToString() + "<br>" +
                    //        "<b>Username: </b>" + DT.Rows[0]["UserName"].ToString() + "<br>" +
                    //        "<b>Temporary Password: </b>" + DT.Rows[0]["Password"].ToString() + "<br><br>" +
                    //        "Access https://sbccrt.durusthr.com anytime and anywhere with Chrome. Enjoy your account.</label>";
                    //    message.Body = new MimeKit.TextPart("html") { Text = messBody };

                    //    using (var client = new MailKit.Net.Smtp.SmtpClient())
                    //    {
                    //        client.ServerCertificateValidationCallback = (s, c, h, e) => true;
                    //        client.Connect("smtp.office365.com", 465, true);
                    //        client.Authenticate("durusthr@illimitado.com", "@1230Qwerty");
                    //        client.Send(message);
                    //        client.Disconnect(true);
                    //    }
                    //}


                    string emailss = DT.Rows[0]["Email"].ToString();//"rb.griarte1@gmail.com"; // 

                    if (string.IsNullOrWhiteSpace(emailss) == false)
                    {
                        var message = new MimeKit.MimeMessage();

                        //MimeKit.InternetAddressList Elist = new MimeKit.InternetAddressList();
                        //foreach (string emailadd in emaillist)
                        //{
                        //    Elist.Add(new MimeKit.MailboxAddress(emailadd));
                        //}

                        message.From.Add(new MimeKit.MailboxAddress("durusthr@illimitado.com"));
                        //message.To.AddRange(Elist);
                        message.To.Add(new MimeKit.MailboxAddress(emailss));
                        //message.Subject = "Leave Request Notification";
                        //message.Body = new MimeKit.TextPart("html") { Text = "test" };
                        message.Subject = "Notifications";
                        var messBody = "<label>Welcome to DurustHR CRT Admin portal, in parternship with Security Bank. Thank you for using DurustHR.<br>" +
                            "Feel free to visit https://sbccrt.durusthr.com to login to your account.<br><br>" +
                            "Here are your account details:<br>" +
                            "<b>Company Name: </b>" + DT.Rows[0]["CompanyName"].ToString() + "<br>" +
                            "<b>Company Alias: </b>" + DT.Rows[0]["CompanyAlias"].ToString() + "<br>" +
                            "<b>Username: </b>" + DT.Rows[0]["UserName"].ToString() + "<br>" +
                            "<b>Temporary Password: </b>" + DT.Rows[0]["Password"].ToString() + "<br><br>" +
                            "Access https://sbccrt.durusthr.com anytime and anywhere with Chrome. Enjoy your account.</label>";
                        message.Body = new MimeKit.TextPart("html") { Text = messBody };

                        using (var client = new MailKit.Net.Smtp.SmtpClient())
                        {
                            client.ServerCertificateValidationCallback = (s, c, h, e) => true;
                            client.Connect("smtp.office365.com", 587, false);
                            client.Authenticate("durusthr@illimitado.com", "@1230Qwerty");
                            client.Send(message);
                            client.Disconnect(true);
                        }
                    }
                }

                return "Success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }
        [HttpPost]
        [Route("ShowCrt_CLientListv4")]
        public GetClientDetailsv2 GetAllClientv4(InsertClientDatav2 admin)
        {
            GetClientDetailsv2 GetClientDetails = new GetClientDetailsv2();
            List<ClientDetailsv2> ClientDetails = new List<ClientDetailsv2>();
            tempconnectionstring();
            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@CN", mytype = SqlDbType.NVarChar, Value = admin.CN });
            Connection.myparameters.Add(new myParameters { ParameterName = "@CClientID", mytype = SqlDbType.NVarChar, Value = admin.CClientID });
            DataTable dtClientList = new DataTable();
            dtClientList = Connection.GetDataTable("sp_ClientListv3");
            if (dtClientList.Rows.Count > 0)
            {
                foreach (DataRow row in dtClientList.Rows)
                {
                    ClientDetailsv2 Clientdetail = new ClientDetailsv2
                    {
                        ClientId = row["ClientId"].ToString(),
                        CN = row["CompanyName"].ToString(),
                        UserName = row["UserName"].ToString(),
                        CompanyAlias = row["CompanyAlias"].ToString(),
                        DateofIncorporation = row["DateofIncorporation"].ToString(),
                        AddressFloor = row["Floor"].ToString(),
                        AddressBuilding = row["Building"].ToString(),
                        AddressStreet = row["Street"].ToString(),
                        AddressBrgy = row["Barangay"].ToString(),
                        AddressMunicipality = row["Municipality"].ToString(),
                        AddressProvince = row["Province"].ToString(),
                        AddressRegion = row["Region"].ToString(),
                        AddressCountry = row["Country"].ToString(),
                        AddressZipCode = row["ZipCode"].ToString(),
                        NationalityCountry = row["NationalityCountry"].ToString(),
                        CompanyEQCustomerNumber = row["CompanyEQCustomerNumber"].ToString(),
                        TotalNumberOfEmployees = row["TotalNumberOfEmployees"].ToString(),
                        AccountType = row["AccountType"].ToString(),
                        ContactType = row["ContactType"].ToString(),
                        ContactNumber = row["ContactNumber"].ToString(),
                        EmailType = row["EmailType"].ToString(),
                        EmailAddress = row["Email"].ToString(),
                        Website = row["Website"].ToString(),
                        SSS = row["SSS"].ToString(),
                        PhilHealth = row["Philhealth"].ToString(),
                        Tin = row["TIN"].ToString(),
                        TinRDO = row["TINRdo"].ToString(),
                        PagIbig = row["PagIbig"].ToString(),
                        PagibigBranch = row["PagibigBranch"].ToString(),
                        FirstName = row["First_Name"].ToString(),
                        MiddleName = row["Middle_Name"].ToString(),
                        LastName = row["Last_Name"].ToString(),
                        RepresentativeTitle = row["Representative_Title"].ToString(),
                        RepresentativeContactNumber = row["Representative_Contact_Number"].ToString(),
                        isDeleted = row["isDeleted"].ToString(),
                        SaladInfo = Convert.ToBoolean(row["salad_Status"].ToString()),
                        SaladPDF = Convert.ToBoolean(row["PDF_Status"].ToString()),
                        SaladSigID = Convert.ToBoolean(row["SaladSigID"].ToString()),
                        posRank = row["PosRankStat"].ToString(),
                        NameOfTeam = row["ContactInfo"].ToString()
                    };
                    ClientDetails.Add(Clientdetail);
                }
                GetClientDetails.Clientdetails = ClientDetails;
                GetClientDetails.myreturn = "Success";
            }
            else
            {
                GetClientDetails.myreturn = "Error";
            }
            return GetClientDetails;
        }
        [HttpPost]
        [Route("CRT_ComRepList")]
        public List<CompanyRepInfo> ComRepInfo(GetLandlineNumber Param)
        {
            List<CompanyRepInfo> CRI = new List<CompanyRepInfo>();
            tempconnectionstring();
            Connection con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@CompanyAlias", mytype = SqlDbType.NVarChar, Value = Param.CompanyAlias });
            DataTable DT = con.GetDataTable("sp_CRT_GetComRepInfo");
            foreach (DataRow row in DT.Rows)
            {
                CompanyRepInfo C = new CompanyRepInfo()
                {
                    ComRepID = row["CompanyRepresentativeId"].ToString(),
                    Username = row["Username"].ToString(),
                    FirstName = row["First_Name"].ToString(),
                    MiddleName = row["Middle_Name"].ToString(),
                    LastName = row["Last_Name"].ToString(),
                    RepTitle = row["Representative_Title"].ToString(),
                    RepEmail = row["RepEmail"].ToString(),
                    RepContact = row["Representative_Contact_Number"].ToString()
                };
                CRI.Add(C);
            }
            return CRI;
        }
        [HttpPost]
        [Route("CRT_UpdateClientDatav2")]
        public List<CRTComRep> CRT_UpdateClientDatav2(UpdateClientDatav2 ClientData)
        {
            List<CRTComRep> ComRep = new List<CRTComRep>();
            try
            {
                tempconnectionstring();
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@ClientId", mytype = SqlDbType.NVarChar, Value = ClientData.ClientId });
                con.myparameters.Add(new myParameters { ParameterName = "@CN", mytype = SqlDbType.NVarChar, Value = ClientData.CN });
                con.myparameters.Add(new myParameters { ParameterName = "@CompanyAlias", mytype = SqlDbType.NVarChar, Value = ClientData.CompanyAlias });
                con.myparameters.Add(new myParameters { ParameterName = "@DateofIncorporation", mytype = SqlDbType.NVarChar, Value = ClientData.DateofIncorporation });
                con.myparameters.Add(new myParameters { ParameterName = "@AddressFloor", mytype = SqlDbType.NVarChar, Value = ClientData.AddressFloor });
                con.myparameters.Add(new myParameters { ParameterName = "@AddressBuilding", mytype = SqlDbType.NVarChar, Value = ClientData.AddressBuilding });
                con.myparameters.Add(new myParameters { ParameterName = "@AddressStreet", mytype = SqlDbType.NVarChar, Value = ClientData.AddressStreet });
                con.myparameters.Add(new myParameters { ParameterName = "@AddressBrgy", mytype = SqlDbType.NVarChar, Value = ClientData.AddressBrgy });
                con.myparameters.Add(new myParameters { ParameterName = "@AddressMunicipality", mytype = SqlDbType.NVarChar, Value = ClientData.AddressMunicipality });
                con.myparameters.Add(new myParameters { ParameterName = "@AddressProvince", mytype = SqlDbType.NVarChar, Value = ClientData.AddressProvince });
                con.myparameters.Add(new myParameters { ParameterName = "@AddressRegion", mytype = SqlDbType.NVarChar, Value = ClientData.AddressRegion });
                con.myparameters.Add(new myParameters { ParameterName = "@AddressCountry", mytype = SqlDbType.NVarChar, Value = ClientData.AddressCountry });
                con.myparameters.Add(new myParameters { ParameterName = "@AddressZipCode", mytype = SqlDbType.NVarChar, Value = ClientData.AddressZipCode });
                con.myparameters.Add(new myParameters { ParameterName = "@NationalityCountry", mytype = SqlDbType.NVarChar, Value = ClientData.NationalityCountry });
                con.myparameters.Add(new myParameters { ParameterName = "@CompanyEQCustomerNumber", mytype = SqlDbType.NVarChar, Value = ClientData.CompanyEQCustomerNumber });
                con.myparameters.Add(new myParameters { ParameterName = "@TotalNumberOfEmployees", mytype = SqlDbType.NVarChar, Value = ClientData.TotalNumberOfEmployees });
                con.myparameters.Add(new myParameters { ParameterName = "@AccountType", mytype = SqlDbType.NVarChar, Value = ClientData.AccountType });

                con.myparameters.Add(new myParameters { ParameterName = "@Tin", mytype = SqlDbType.NVarChar, Value = ClientData.Tin });
                con.myparameters.Add(new myParameters { ParameterName = "@ModifiedById", mytype = SqlDbType.NVarChar, Value = ClientData.ModifiedById });
                con.myparameters.Add(new myParameters { ParameterName = "@PDF_Status", mytype = SqlDbType.NVarChar, Value = ClientData.PDF_Status });
                con.myparameters.Add(new myParameters { ParameterName = "@Salad_Info", mytype = SqlDbType.NVarChar, Value = ClientData.Salad_Info });
                con.myparameters.Add(new myParameters { ParameterName = "@CClientID", mytype = SqlDbType.NVarChar, Value = ClientData.CClientID });
                con.myparameters.Add(new myParameters { ParameterName = "@lastAlias", mytype = SqlDbType.NVarChar, Value = ClientData.lastAlias });
                con.myparameters.Add(new myParameters { ParameterName = "@SaladSigID", mytype = SqlDbType.NVarChar, Value = ClientData.SaladSigID });
                DataTable Params = new DataTable();
                Params.Columns.AddRange(new DataColumn[8] { new DataColumn("RepID"), new DataColumn("Username"), new DataColumn("Firstname"), new DataColumn("Middlename"), new DataColumn("Lastname"), new DataColumn("RepTitle"), new DataColumn("RepContact"), new DataColumn("RepEmail") });
                for (int i = 0; i < ClientData.RepID.Length; i++)
                {
                    Params.Rows.Add(ClientData.RepID[i], ClientData.UserName[i], ClientData.FirstName[i], ClientData.MiddleName[i], ClientData.LastName[i], ClientData.RepresentativeTitle[i], ClientData.RepresentativeContactNumber[i], ClientData.EmailAddress[i]);
                }
                con.myparameters.Add(new myParameters { ParameterName = "@ComRepInfo", mytype = SqlDbType.Structured, Value = Params });
                DataTable DT = con.GetDataTable("sp_CRT_Updatev2");
                foreach (DataRow row in DT.Rows)
                {
                    CRTComRep CR = new CRTComRep()
                    {
                        item = row["item"].ToString(),
                        reason = row["reason"].ToString()
                    };
                    ComRep.Add(CR);
                }
            }
            catch (Exception e)
            {

            }
            return ComRep;
        }
        [HttpPost]
        [Route("CRTForgotPasswordv2")]
        public string ForgotPasswordv2(ForgotPassword FP)
        {
            try
            {
                tempconnectionstring();
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@EmailAddress", mytype = SqlDbType.VarChar, Value = FP.Email });
                var RanCode = con.ExecuteScalar("sp_CRT_ForgotPassword_Codev2");
                var message = new MimeKit.MimeMessage();
                message.From.Add(new MimeKit.MailboxAddress("password.recovery@illimitado.com"));
                message.To.Add(new MimeKit.MailboxAddress(FP.Email));
                message.Subject = "Reset Password";
                var messBody = "Please use this generated code as your temporary password: " + RanCode;
                message.Body = new MimeKit.TextPart("html") { Text = messBody };

                using (var client = new MailKit.Net.Smtp.SmtpClient())
                {
                    client.ServerCertificateValidationCallback = (s, c, h, e) => true;
                    client.Connect("mail.illimitado.com", 465, true);
                    client.Authenticate("password.recovery@illimitado.com", "p@ssw0rd1LM");
                    client.Send(message);
                    client.Disconnect(true);
                }
                return "Success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }
        [HttpPost]
        [Route("CRT_EmailValidationv3")]
        public string CRT_EmailValidationv3(CRTAddAdminUsersv2 ClientData)
        {
            try
            {
                tempconnectionstring();
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@EmailAddress", mytype = SqlDbType.VarChar, Value = ClientData.Email });
                return con.ExecuteScalar("sp_CRT_EmailVerifyv3").ToString();
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }
        [HttpPost]
        [Route("CRT_CodeVerificationv2")]
        public string CRT_CodeVerificationv2(InsertClientData ClientData)
        {
            string valid = "";
            //GetDatabase GDB = new GetDatabase();
            //GDB.ClientName = ClientData.CN;
            tempconnectionstring();
            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@EmailAddress", mytype = SqlDbType.NVarChar, Value = ClientData.EmailAddress });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Code", mytype = SqlDbType.NVarChar, Value = ClientData.Code });
            valid = Connection.ExecuteScalar("sp_CRT_CodeVerificationv2").ToString();

            return valid;
        }
        [HttpPost]
        [Route("CRT_UpdatePasswordv2")]
        public string CRT_UpdatePasswordv2(InsertClientData ClientData)
        {
            //string valid = "";
            //GetDatabase GDB = new GetDatabase();
            //GDB.ClientName = ClientData.CN;
            tempconnectionstring();
            Connection Connection = new Connection();
            Connection.myparameters.Add(new myParameters { ParameterName = "@Code", mytype = SqlDbType.NVarChar, Value = ClientData.Code });
            Connection.myparameters.Add(new myParameters { ParameterName = "@EmailAddress", mytype = SqlDbType.NVarChar, Value = ClientData.EmailAddress });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Password", mytype = SqlDbType.NVarChar, Value = ClientData.Password });
            //Connection.ExecuteScalar("sp_CRT_UpdatePassword").ToString();
            Connection.ExecuteNonQuery("sp_CRT_UpdatePasswordv2");
            return "success";
        }
        [HttpPost]
        [Route("GenerateCRTRevised")]
        public string GenerateCRTPAYROLLRevised(CRTGenerationParams Params)
        {
            try
            {
                DataTable DTParam = new DataTable();
                DTParam.Columns.AddRange(new DataColumn[1] { new DataColumn("startingids") });
                for (int i = 0; i < Params.StartingIDS.Length; i++)
                {
                    DTParam.Rows.Add(Params.StartingIDS[i]);
                }

                tempconnectionstring();
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@StartingIDS", mytype = SqlDbType.Structured, Value = DTParam });
                con.myparameters.Add(new myParameters { ParameterName = "@CompanyAlias", mytype = SqlDbType.VarChar, Value = Params.CompanyAlias });
                con.myparameters.Add(new myParameters { ParameterName = "@Branch", mytype = SqlDbType.VarChar, Value = Params.Branch });
                DataTable EmpData = con.GetDataTable("sp_CRT_GenerateCRTRevised");

                string Companyname = EmpData.Rows[0]["CompanyName"].ToString();
                string Site = EmpData.Rows[0]["Branch"].ToString();
                string Count = EmpData.Rows[0]["NumberofGenerate"].ToString();
                string AccountType = EmpData.Rows[0]["AccountType"].ToString();
                if (AccountType == "Easy Savings")
                {
                    AccountType = "ES";
                }
                else
                {
                    AccountType = "AA";
                }

                //Naming Convention is "Company Name_Site_Date_Count". Count means pang ilan file siya today. 
                string dataDir = HostingEnvironment.MapPath("~/Files/");

                FileStream stream = new FileStream(dataDir + "newcrttemplate.xls", FileMode.Open);
                string filename;
                string datebuild = DateTime.Now.ToString("MMddyyyy");
                string timebuild = DateTime.Now.ToString("hh-mmtt");

                filename = Companyname + "_" + Site + "_" + datebuild + "_" + AccountType + "_" + Count + ".xls";
                using (var fileStream = new FileStream(dataDir + filename, FileMode.Create, FileAccess.Write))
                {
                    stream.CopyTo(fileStream);
                }
                stream.Close();
                ExcelWorkbookFormat workbookFormat = ExcelWorkbookFormat.Xls_2003;
                string testDocFile = dataDir + "newcrttemplate.xls";
                ExcelWorkbook workbook = new ExcelWorkbook(testDocFile);
                ExcelWorksheet firstWorksheet = workbook.Worksheets[1];

                if (EmpData.Rows.Count > 0)
                {
                    firstWorksheet["C" + 3].Text = EmpData.Rows[0]["CompanyName"].ToString();//Company Name
                    firstWorksheet["C" + 4].Text = EmpData.Rows[0]["DateofIncorporation"].ToString();//Date of Incorporation
                    firstWorksheet["C" + 5].Text = EmpData.Rows[0]["CompleteCompanyAddress"].ToString();//Complete Company Address
                    firstWorksheet["C" + 6].Text = EmpData.Rows[0]["CompanyNationality"].ToString();//Nationality of Company
                    firstWorksheet["C" + 7].Text = EmpData.Rows[0]["ContactNumber"].ToString();//Contact Number
                    firstWorksheet["C" + 8].Text = EmpData.Rows[0]["TIN"].ToString();//Company Tin
                    firstWorksheet["C" + 10].Text = EmpData.Rows[0]["CompanyEQNumber"].ToString();//Company EQ Customer Number
                    firstWorksheet["C" + 11].Text = EmpData.Rows.Count.ToString();//Total Number of Employees
                    firstWorksheet["C" + 12].Text = EmpData.Rows[0]["AccountType"].ToString();//Account Type
                }

                int cellnum = 19;

                for (int i = 0; i < EmpData.Rows.Count; i++)
                {
                    firstWorksheet["A" + cellnum].Text = (i + 1).ToString();
                    firstWorksheet["B" + cellnum].Text = EmpData.Rows[i]["LastName"].ToString();
                    firstWorksheet["C" + cellnum].Text = EmpData.Rows[i]["FirstName"].ToString();
                    firstWorksheet["D" + cellnum].Text = EmpData.Rows[i]["MiddleName"].ToString();
                    firstWorksheet["E" + cellnum].Text = EmpData.Rows[i]["Suffix"].ToString();
                    firstWorksheet["F" + cellnum].Text = EmpData.Rows[i]["CardName"].ToString();
                    firstWorksheet["G" + cellnum].DateTimeValue = DateTime.Parse(EmpData.Rows[i]["Birthday"].ToString());
                    firstWorksheet["H" + cellnum].Text = EmpData.Rows[i]["PlaceOfBirth"].ToString();
                    firstWorksheet["I" + cellnum].Text = EmpData.Rows[i]["Nationality"].ToString();
                    firstWorksheet["J" + cellnum].Text = EmpData.Rows[i]["NationalityCountry"].ToString();
                    firstWorksheet["K" + cellnum].Text = EmpData.Rows[i]["Gender"].ToString();
                    firstWorksheet["L" + cellnum].Text = EmpData.Rows[i]["MobileNumber"].ToString();
                    firstWorksheet["M" + cellnum].Text = EmpData.Rows[i]["LandlineNumber"].ToString();
                    firstWorksheet["N" + cellnum].Text = EmpData.Rows[i]["HouseNumber"].ToString();
                    firstWorksheet["O" + cellnum].Text = EmpData.Rows[i]["StreetVillageSubdivisionBrgyPresent"].ToString();
                    firstWorksheet["P" + cellnum].Text = EmpData.Rows[i]["Country"].ToString();
                    firstWorksheet["Q" + cellnum].Text = EmpData.Rows[i]["Region"].ToString();
                    firstWorksheet["R" + cellnum].Text = EmpData.Rows[i]["Province"].ToString();
                    firstWorksheet["S" + cellnum].Text = EmpData.Rows[i]["Municipality"].ToString();
                    firstWorksheet["T" + cellnum].Text = EmpData.Rows[i]["ZipCode"].ToString();
                    firstWorksheet["U" + cellnum].Text = EmpData.Rows[i]["HouseNumber2"].ToString();
                    firstWorksheet["V" + cellnum].Text = EmpData.Rows[i]["StreetVillageSubdivisionBrgyPermanent"].ToString();
                    firstWorksheet["W" + cellnum].Text = EmpData.Rows[i]["CountryPermanent"].ToString();
                    firstWorksheet["X" + cellnum].Text = EmpData.Rows[i]["RegionPermanent"].ToString();
                    firstWorksheet["Y" + cellnum].Text = EmpData.Rows[i]["ProvincePermanent"].ToString();
                    firstWorksheet["Z" + cellnum].Text = EmpData.Rows[i]["MunicipalityPermanent"].ToString();
                    firstWorksheet["AA" + cellnum].Text = EmpData.Rows[i]["ZipcodePermanent"].ToString();
                    firstWorksheet["AB" + cellnum].Text = EmpData.Rows[i]["Email"].ToString();
                    firstWorksheet["AC" + cellnum].Text = EmpData.Rows[i]["EmployerName"].ToString();
                    firstWorksheet["AD" + cellnum].Text = EmpData.Rows[i]["EmployeeTIN"].ToString();
                    firstWorksheet["AE" + cellnum].Text = EmpData.Rows[i]["CivilStatus"].ToString();
                    firstWorksheet["AF" + cellnum].Text = EmpData.Rows[i]["Last_Name"].ToString();
                    firstWorksheet["AG" + cellnum].Text = EmpData.Rows[i]["First_Name"].ToString();
                    firstWorksheet["AH" + cellnum].Text = EmpData.Rows[i]["Middle_Name"].ToString();
                    firstWorksheet["AI" + cellnum].Text = EmpData.Rows[i]["SuffixMother"].ToString();
                    firstWorksheet["AJ" + cellnum].Text = EmpData.Rows[i]["NumberOfPads"].ToString();
                    firstWorksheet["AK" + cellnum].Text = EmpData.Rows[i]["OtherDetails"].ToString();

                    cellnum++;
                }


                //https://http://ws.durusthr.com/ILM_WS_LIVE/sFTP/CrtSampleFileExport_12-41-PM.xls
                HttpResponse httpResponse = System.Web.HttpContext.Current.Response;

                httpResponse.Clear();
                httpResponse.ContentType = "Application/x-msexcel";
                httpResponse.AddHeader("Content-Disposition", String.Format("attachment; filename={0}", filename));
                string newDIR = HttpContext.Current.Server.MapPath("~/sFTP/");
                string onlineDIR = "http://ws.durusthr.com/ilm_ws_live/sFTP/";

                workbook.Save(newDIR + filename);

                //return "Success";
                return onlineDIR + filename;
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }
        [HttpPost]
        [Route("GenerateCRTRevisedv2")]
        public string GenerateCRTPAYROLLRevisedv2(CRTGenerationParams Params)
        {
            FileStream stream = null;
            try
            {
                //DataTable DTParam = new DataTable();
                //DTParam.Columns.AddRange(new DataColumn[1] { new DataColumn("startingids") });
                //for (int i = 0; i < Params.StartingIDS.Length; i++)
                //{
                //    DTParam.Rows.Add(Params.StartingIDS[i]);
                //}

                tempconnectionstring();
                Connection con = new Connection();
                //con.myparameters.Add(new myParameters { ParameterName = "@StartingIDS", mytype = SqlDbType.Structured, Value = DTParam });
                con.myparameters.Add(new myParameters { ParameterName = "@AccountType", mytype = SqlDbType.VarChar, Value = Params.CompanyAlias });
                con.myparameters.Add(new myParameters { ParameterName = "@Branch", mytype = SqlDbType.VarChar, Value = Params.Branch });
                //DataTable Sheet7Data = con.GetDataTable("sp_GenerateCRTSheet7");
                DataTable Sheet7Data = con.GetDataTable("sp_GetSheet7Custom");

                //Naming Convention is "Company Name_Site_Date_Count". Count means pang ilan file siya today. 
                string dataDir = HostingEnvironment.MapPath("~/Files/");

                stream = new FileStream(dataDir + "CRTTemplate.xls", FileMode.Open);
                string filename;
                string datebuild = DateTime.Now.ToString("MMddyyyy");
                string timebuild = DateTime.Now.ToString("hh-mmtt");

                string Companyname = Sheet7Data.Rows[0]["CompanyName"].ToString();
                string Site = Sheet7Data.Rows[0]["Branch"].ToString();
                // string Site = Params.Branch;
                string Count = Sheet7Data.Rows[0]["NumberofGenerate"].ToString();
                string AccountType = Sheet7Data.Rows[0]["AccountType"].ToString();
                if (AccountType == "Easy Savings")
                {
                    AccountType = "ES";
                }
                else
                {
                    AccountType = "AA";
                }

                filename = Companyname + "_" + Site + "_" + datebuild + "_" + AccountType + "_" + Count + ".xls";
                using (var fileStream = new FileStream(dataDir + filename, FileMode.Create, FileAccess.Write))
                //using (var fileStream = new FileStream(dataDir + filename, FileMode.Create, FileAccess.Write))
                {
                    stream.CopyTo(fileStream);
                }
                stream.Close();
                ExcelWorkbookFormat workbookFormat = ExcelWorkbookFormat.Xls_2003;
                string testDocFile = dataDir + "CRTTemplate.xls";
                ExcelWorkbook workbook = new ExcelWorkbook(testDocFile);
                ExcelWorksheet firstWorksheet = workbook.Worksheets[0];

                #region Sheet7

                ExcelWorksheet Sheet7 = workbook.Worksheets[1];
                Sheet7.WorksheetSecurity.ProtectWorksheet("sbc01");

                //if (Sheet7Data.Rows.Count > 0)
                //{
                //    Sheet7["A" + 1].Text = Sheet7Data.Rows[0]["Company CIF Number"].ToString();
                //    Sheet7["B" + 1].Text = Sheet7Data.Rows[0]["Company EQ"].ToString();
                //    Sheet7["C" + 1].Text = Sheet7Data.Rows.Count.ToString();
                //    Sheet7["D" + 1].Text = Sheet7Data.Rows[0]["CompanyName"].ToString();
                //}

                int x = 2;
                for (int i = 0; i < Sheet7Data.Rows.Count; i++, x++)
                {
                    Sheet7["A" + x].Text = Sheet7Data.Rows[i]["LastName"].ToString();
                    Sheet7["B" + x].Text = Sheet7Data.Rows[i]["FirstName"].ToString();
                    Sheet7["C" + x].Text = Sheet7Data.Rows[i]["MiddleName"].ToString();
                    Sheet7["D" + x].Text = Sheet7Data.Rows[i]["Suffix"].ToString();
                    Sheet7["E" + x].Text = Sheet7Data.Rows[i]["FullName"].ToString();
                    Sheet7["F" + x].Text = Sheet7Data.Rows[i]["DateOfBirth"].ToString();
                    Sheet7["G" + x].Text = Sheet7Data.Rows[i]["PlaceOfBirth"].ToString();
                    Sheet7["H" + x].Text = Sheet7Data.Rows[i]["Nationality"].ToString();
                    Sheet7["I" + x].Text = Sheet7Data.Rows[i]["NationalityCountry"].ToString();
                    Sheet7["J" + x].Text = Sheet7Data.Rows[i]["Gender"].ToString();
                    Sheet7["K" + x].Text = Sheet7Data.Rows[i]["MobileNumber"].ToString();
                    Sheet7["L" + x].Text = Sheet7Data.Rows[i]["LandlineNumber"].ToString();
                    Sheet7["M" + x].Text = Sheet7Data.Rows[i]["HouseNumberPresent"].ToString();
                    Sheet7["N" + x].Text = Sheet7Data.Rows[i]["StreetVillageSubdivisionBrgyPresent"].ToString();
                    Sheet7["O" + x].Text = Sheet7Data.Rows[i]["CityMunicipality"].ToString();
                    Sheet7["P" + x].Text = Sheet7Data.Rows[i]["Province"].ToString();
                    Sheet7["Q" + x].Text = Sheet7Data.Rows[i]["Region"].ToString();
                    Sheet7["R" + x].Text = Sheet7Data.Rows[i]["Country"].ToString();
                    Sheet7["S" + x].Text = Sheet7Data.Rows[i]["ZipCode"].ToString();
                    Sheet7["T" + x].Text = Sheet7Data.Rows[i]["HouseNumberPermanent"].ToString();
                    Sheet7["U" + x].Text = Sheet7Data.Rows[i]["StreetVillageSubdivisionBrgyPermanent"].ToString();
                    Sheet7["V" + x].Text = Sheet7Data.Rows[i]["CityMunicipalityPermanent"].ToString();
                    Sheet7["W" + x].Text = Sheet7Data.Rows[i]["ProvincePermanent"].ToString();
                    Sheet7["X" + x].Text = Sheet7Data.Rows[i]["RegionPermanent"].ToString();
                    Sheet7["Y" + x].Text = Sheet7Data.Rows[i]["CountryPermanent"].ToString();
                    Sheet7["Z" + x].Text = Sheet7Data.Rows[i]["ZipCodePermanent"].ToString();
                    Sheet7["AA" + x].Text = Sheet7Data.Rows[i]["Email"].ToString();
                    Sheet7["AB" + x].Text = Sheet7Data.Rows[i]["CompanyName"].ToString();
                    Sheet7["AC" + x].Text = Sheet7Data.Rows[i]["EmployeeTIN"].ToString();
                    Sheet7["AD" + x].Text = Sheet7Data.Rows[i]["CivilStatus"].ToString();
                    Sheet7["AE" + x].Text = Sheet7Data.Rows[i]["MotherLastName"].ToString();
                    Sheet7["AF" + x].Text = Sheet7Data.Rows[i]["MotherFirstName"].ToString();
                    Sheet7["AG" + x].Text = Sheet7Data.Rows[i]["MotherMiddleName"].ToString();
                    Sheet7["AH" + x].Text = Sheet7Data.Rows[i]["SuffixMother"].ToString();
                    Sheet7["AI" + x].Text = Sheet7Data.Rows[i]["NumberOfPads"].ToString();
                    Sheet7["AJ" + x].Text = Sheet7Data.Rows[i]["EmployeeID"].ToString();
                }
                #endregion


                tempconnectionstring();
                con = new Connection();
                //con.myparameters.Add(new myParameters { ParameterName = "@StartingIDS", mytype = SqlDbType.Structured, Value = DTParam });
                con.myparameters.Add(new myParameters { ParameterName = "@AccountType", mytype = SqlDbType.VarChar, Value = Params.CompanyAlias });
                con.myparameters.Add(new myParameters { ParameterName = "@Branch", mytype = SqlDbType.VarChar, Value = Params.Branch });
                //DataTable EmpData = con.GetDataTable("sp_CRT_GenerateCRTRevisedv2");
                DataTable EmpData = con.GetDataTable("sp_GetMainSheetCustom");
                if (EmpData.Rows.Count > 0)
                {
                    firstWorksheet["C" + 3].Text = EmpData.Rows[0]["CompanyName"].ToString();//Company Name
                    firstWorksheet["C" + 4].Text = EmpData.Rows[0]["DateofIncorporation"].ToString();//Date of Incorporation
                    firstWorksheet["C" + 5].Text = EmpData.Rows[0]["CompleteCompanyAddress"].ToString();//Complete Company Address
                    firstWorksheet["C" + 6].Text = EmpData.Rows[0]["CompanyNationality"].ToString();//Nationality of Company
                    firstWorksheet["C" + 7].Text = EmpData.Rows[0]["ContactNumber"].ToString();//Contact Number
                    firstWorksheet["C" + 8].Text = EmpData.Rows[0]["TIN"].ToString();//Company Tin
                    firstWorksheet["C" + 10].Text = EmpData.Rows[0]["CompanyEQNumber"].ToString();//Company EQ Customer Number
                    firstWorksheet["C" + 11].Text = EmpData.Rows.Count.ToString();//Total Number of Employees
                    firstWorksheet["C" + 12].Text = EmpData.Rows[0]["AccountType"].ToString();//Account Type
                }

                int cellnum = 19;

                for (int i = 0; i < EmpData.Rows.Count; i++)
                {
                    firstWorksheet["A" + cellnum].Text = (i + 1).ToString();
                    firstWorksheet["B" + cellnum].Text = EmpData.Rows[i]["LastName"].ToString();
                    firstWorksheet["C" + cellnum].Text = EmpData.Rows[i]["FirstName"].ToString();
                    firstWorksheet["D" + cellnum].Text = EmpData.Rows[i]["MiddleName"].ToString();
                    firstWorksheet["E" + cellnum].Text = EmpData.Rows[i]["Suffix"].ToString();
                    firstWorksheet["F" + cellnum].Text = EmpData.Rows[i]["CardName"].ToString();
                    firstWorksheet["G" + cellnum].DateTimeValue = DateTime.Parse(EmpData.Rows[i]["Birthday"].ToString(), CultureInfo.InvariantCulture);
                    //firstWorksheet["G" + cellnum].DateTimeValue = DateTime.ParseExact(EmpData.Rows[i]["Birthday"].ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture) CultureInfo.CreateSpecificCulture("fr-FR"); 
                    firstWorksheet["H" + cellnum].Text = EmpData.Rows[i]["PlaceOfBirth"].ToString();
                    firstWorksheet["I" + cellnum].Text = EmpData.Rows[i]["Nationality"].ToString();
                    firstWorksheet["J" + cellnum].Text = EmpData.Rows[i]["NationalityCountry"].ToString();
                    firstWorksheet["K" + cellnum].Text = EmpData.Rows[i]["Gender"].ToString();
                    firstWorksheet["L" + cellnum].Text = EmpData.Rows[i]["MobileNumber"].ToString();
                    firstWorksheet["M" + cellnum].Text = EmpData.Rows[i]["LandlineNumber"].ToString();
                    firstWorksheet["N" + cellnum].Text = EmpData.Rows[i]["HouseNumber"].ToString();
                    firstWorksheet["O" + cellnum].Text = EmpData.Rows[i]["StreetVillageSubdivisionBrgyPresent"].ToString();
                    firstWorksheet["P" + cellnum].Text = EmpData.Rows[i]["Country"].ToString();
                    firstWorksheet["Q" + cellnum].Text = EmpData.Rows[i]["Region"].ToString();
                    firstWorksheet["R" + cellnum].Text = EmpData.Rows[i]["Province"].ToString();
                    firstWorksheet["S" + cellnum].Text = EmpData.Rows[i]["Municipality"].ToString();
                    firstWorksheet["T" + cellnum].Text = EmpData.Rows[i]["ZipCode"].ToString();
                    firstWorksheet["U" + cellnum].Text = EmpData.Rows[i]["HouseNumber2"].ToString();
                    firstWorksheet["V" + cellnum].Text = EmpData.Rows[i]["StreetVillageSubdivisionBrgyPermanent"].ToString();
                    firstWorksheet["W" + cellnum].Text = EmpData.Rows[i]["CountryPermanent"].ToString();
                    firstWorksheet["X" + cellnum].Text = EmpData.Rows[i]["RegionPermanent"].ToString();
                    firstWorksheet["Y" + cellnum].Text = EmpData.Rows[i]["ProvincePermanent"].ToString();
                    firstWorksheet["Z" + cellnum].Text = EmpData.Rows[i]["MunicipalityPermanent"].ToString();
                    firstWorksheet["AA" + cellnum].Text = EmpData.Rows[i]["ZipcodePermanent"].ToString();
                    firstWorksheet["AB" + cellnum].Text = EmpData.Rows[i]["Email"].ToString();
                    firstWorksheet["AC" + cellnum].Text = EmpData.Rows[i]["EmployerName"].ToString();
                    firstWorksheet["AD" + cellnum].Text = EmpData.Rows[i]["EmployeeTIN"].ToString();
                    firstWorksheet["AE" + cellnum].Text = EmpData.Rows[i]["CivilStatus"].ToString();
                    firstWorksheet["AF" + cellnum].Text = EmpData.Rows[i]["Last_Name"].ToString();
                    firstWorksheet["AG" + cellnum].Text = EmpData.Rows[i]["First_Name"].ToString();
                    firstWorksheet["AH" + cellnum].Text = EmpData.Rows[i]["Middle_Name"].ToString();
                    firstWorksheet["AI" + cellnum].Text = EmpData.Rows[i]["SuffixMother"].ToString();
                    firstWorksheet["AJ" + cellnum].Text = EmpData.Rows[i]["NumberOfPads"].ToString();
                    firstWorksheet["AK" + cellnum].Text = EmpData.Rows[i]["OtherDetails"].ToString();

                    cellnum++;
                }


                HttpResponse httpResponse = System.Web.HttpContext.Current.Response;

                httpResponse.Clear();
                httpResponse.ContentType = "Application/x-msexcel";
                httpResponse.AddHeader("Content-Disposition", String.Format("attachment; filename={0}", filename));
                string newDIR = HttpContext.Current.Server.MapPath("~/sFTP/");
                string onlineDIR = "http://ws.durusthr.com/ILM_WS_Live/sFTP/";
                //filename = filename.Replace(',', '_');
                //filename = filename.Replace(" ", String.Empty);
                workbook.Save(newDIR + filename);
                stream.Close();
                //return "Success";
                return onlineDIR + filename;
            }
            catch (Exception e)
            {
                stream.Close();
                return e.ToString();
            }
        }

        [HttpPost]
        [Route("CRT_ExtractData")]
        public string ExtractEmployeeData(CRTGenerationParams Params)

        {
            FileStream stream = null;
            try
            {
                tempconnectionstring();
                Connection con = new Connection();

                con.myparameters.Add(new myParameters { ParameterName = "@CompanyAlias", mytype = SqlDbType.VarChar, Value = Params.CompanyAlias });
                DataTable EmpData = con.GetDataTable("sp_Extract_EmployeeData");
                //Naming Convention is "Company Name_Site_Date_Count". Count means pang ilan file siya today. 
                string dataDir = HostingEnvironment.MapPath("~/Files/");

                stream = new FileStream(dataDir + "EmployeeList.xls", FileMode.Open);
                string filename;
                string datebuild = DateTime.Now.ToString("MMddyyyy");
                string timebuild = DateTime.Now.ToString("hh-mmtt");


                filename = "Employee_List" + "_" + datebuild + "_" + timebuild + ".xls";
                //filename = "Employee_List.xls";
                using (var fileStream = new FileStream(dataDir + filename, FileMode.Create, FileAccess.Write))
                {
                    stream.CopyTo(fileStream);
                }

                ExcelWorkbookFormat workbookFormat = ExcelWorkbookFormat.Xls_2003;
                string testDocFile = dataDir + filename;
                ExcelWorkbook workbook = new ExcelWorkbook(testDocFile);
                ExcelWorksheet firstWorksheet = workbook.Worksheets[0];

                int cellnum = 2;

                for (int i = 0; i < EmpData.Rows.Count; i++)
                {
                    firstWorksheet["A" + cellnum].Text = EmpData.Rows[i]["StartingID"].ToString();
                    firstWorksheet["B" + cellnum].Text = EmpData.Rows[i]["FirstName"].ToString();
                    firstWorksheet["C" + cellnum].Text = EmpData.Rows[i]["MiddleName"].ToString();
                    firstWorksheet["D" + cellnum].Text = EmpData.Rows[i]["LastName"].ToString();
                    firstWorksheet["E" + cellnum].Text = EmpData.Rows[i]["Suffix"].ToString();
                    firstWorksheet["F" + cellnum].Text = EmpData.Rows[i]["DateHired"].ToString();
                    firstWorksheet["G" + cellnum].Text = EmpData.Rows[i]["Branch"].ToString();
                    firstWorksheet["H" + cellnum].Text = EmpData.Rows[i]["AccountType"].ToString();
                    firstWorksheet["I" + cellnum].Text = EmpData.Rows[i]["EmployeeTIN"].ToString();
                    cellnum++;
                }


                HttpResponse httpResponse = System.Web.HttpContext.Current.Response;

                httpResponse.Clear();
                httpResponse.ContentType = "Application/x-msexcel";
                httpResponse.AddHeader("Content-Disposition", String.Format("attachment; filename={0}", filename));
                string newDIR = HttpContext.Current.Server.MapPath("~/sFTP/");
                string onlineDIR = "http://ws.durusthr.com/ILM_WS_Live/sFTP/";
                //filename = filename.Replace(',', '_');
                //filename = filename.Replace(" ", String.Empty);
                workbook.Save(newDIR + filename);
                stream.Close();
                //return "Success";
                return onlineDIR + filename;
            }
            catch (Exception e)
            {
                stream.Close();
                return e.ToString();
            }
        }
        [HttpPost]
        [Route("CRT_TINValidationv2")]
        public string CRT_TINValidationv2(CRTTINValidationv2 ClientData)
        {
            try
            {
                tempconnectionstring();
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@EmployeeTIN", mytype = SqlDbType.NVarChar, Value = ClientData.TIN });
                con.myparameters.Add(new myParameters { ParameterName = "@CompanyAlias", mytype = SqlDbType.NVarChar, Value = ClientData.CN });
                return con.ExecuteScalar("sp_CRT_CheckEmployeeTINv2").ToString();
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }
        [HttpPost]
        [Route("CRT_SendFile")]
        public string ReceiveFile()
        {
            try
            {
                HttpResponseMessage result = null;
                var httpRequest = HttpContext.Current.Request;
                string datebuild = DateTime.Now.ToString("MMddyyyy");
                string timebuild = DateTime.Now.ToString("hh-mmtt");
                if (httpRequest.Files.Count > 0)
                {
                    var docfiles = new List<string>();
                    foreach (string file in httpRequest.Files)
                    {
                        var postedFile = httpRequest.Files[file];
                        var filePath = HttpContext.Current.Server.MapPath("~/sFTP/" + datebuild + timebuild + "_" + postedFile.FileName);
                        postedFile.SaveAs(filePath);
                        docfiles.Add(filePath);
                    }
                    result = Request.CreateResponse(HttpStatusCode.Created, docfiles);
                }
                else
                {
                    result = Request.CreateResponse(HttpStatusCode.BadRequest);
                }
                return result.ToString();
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }
        [HttpPost]
        [Route("CRT_SendFilev2")]
        public string ReceiveFilev2()
        {
            try
            {
                HttpResponseMessage result = null;
                var httpRequest = HttpContext.Current.Request;
                string datebuild = DateTime.Now.ToString("MMddyyyy");
                string timebuild = DateTime.Now.ToString("hh-mmtt");
                var newfilename = "";
                if (httpRequest.Files.Count > 0)
                {
                    var docfiles = new List<string>();
                    foreach (string file in httpRequest.Files)
                    {
                        var postedFile = httpRequest.Files[file];
                        var FileName = postedFile.FileName.ToString();
                        var tempfilename = FileName.Substring(0, FileName.Length - 4);
                        newfilename = tempfilename + "xls";
                        var filePath = HttpContext.Current.Server.MapPath("~/sFTP/" + datebuild + timebuild + "_" + newfilename);
                        postedFile.SaveAs(filePath);
                        docfiles.Add(filePath);
                    }
                    result = Request.CreateResponse(HttpStatusCode.Created, docfiles);
                }
                else
                {
                    result = Request.CreateResponse(HttpStatusCode.BadRequest);
                }
                // return result.ToString();
                return "http://ws.durusthr.com/ILM_WS_Live/sFTP/" + datebuild + timebuild + "_" + newfilename;
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }
        [HttpPost]
        [Route("ExtractGenerateCRTSALADINFO")]
        public string ExractGenerateCRTSALADINFO(SaladItemsCRT clientdata)
        {


            tempconnectionstring();
            Connection con = new Connection();

            con.myparameters.Add(new myParameters { ParameterName = "@CN", mytype = SqlDbType.VarChar, Value = clientdata._cn });
            con.myparameters.Add(new myParameters { ParameterName = "@CclientID", mytype = SqlDbType.VarChar, Value = clientdata.CclientID });

            DataTable EmpData = con.GetDataTable("sp_ExtractSaladInfo");


            try
            {
                string dataDir = HostingEnvironment.MapPath("~/Files/");
                //FileStream stream = new FileStream(dataDir + "newsaladtemplate.xls", FileMode.Open);
                FileStream stream = new FileStream(dataDir + "newsaladtemplate.xls", FileMode.Open);

                string filename;
                //string companynamefile = clientdata.ClientId.ToString();
                string datebuild = DateTime.Now.ToString("MMddyyyy");
                string timebuild = DateTime.Now.ToString("hh-mmtt");

                //filename = "CrtSampleFileExport_" + datebuild + "_" +timebuild + ".xls";


                filename = /*companynamefile + "_" +*/ "ExtractAllSalad" + datebuild + "_" + timebuild + ".xls";
                using (var fileStream = new FileStream(dataDir + filename, FileMode.Create, FileAccess.Write))
                {
                    stream.CopyTo(fileStream);
                }
                stream.Close();



                ExcelWorkbookFormat workbookFormat = ExcelWorkbookFormat.Xls_2003;
                //string testDocFile = dataDir + "newsaladtemplate.xls";
                string testDocFile = dataDir + "newsaladtemplateforupload.xls";
                ExcelWorkbook workbook = new ExcelWorkbook(testDocFile);
                ExcelWorksheet firstWorksheet = workbook.Worksheets[0];


                int cellnum = 2;
                foreach (DataRow row in EmpData.Rows)
                {
                    firstWorksheet["A" + cellnum].Text = row["CRTSaladDateEntry"].ToString();
                    firstWorksheet["B" + cellnum].Text = row["GenerationDate"].ToString();
                    firstWorksheet["C" + cellnum].Text = row["CompanyName"].ToString();
                    firstWorksheet["D" + cellnum].Text = row["Branch"].ToString();
                    firstWorksheet["E" + cellnum].Text = row["LastName"].ToString();
                    firstWorksheet["F" + cellnum].Text = row["FirstName"].ToString();
                    firstWorksheet["G" + cellnum].Text = row["MiddleName"].ToString();
                    firstWorksheet["H" + cellnum].Text = row["Suffix"].ToString();
                    firstWorksheet["I" + cellnum].Text = row["CardName"].ToString();
                    firstWorksheet["J" + cellnum].Text = row["Birthday"].ToString();
                    firstWorksheet["K" + cellnum].Text = row["PlaceofBirth"].ToString();
                    firstWorksheet["L" + cellnum].Text = row["Nationality"].ToString();
                    firstWorksheet["M" + cellnum].Text = row["NationalityCountry"].ToString();
                    firstWorksheet["N" + cellnum].Text = row["Gender"].ToString();
                    firstWorksheet["O" + cellnum].Text = row["MobileNumber"].ToString();
                    firstWorksheet["P" + cellnum].Text = row["LandlineNumber"].ToString();
                    firstWorksheet["Q" + cellnum].Text = row["HouseNumber"].ToString();
                    firstWorksheet["R" + cellnum].Text = row["StreetrVillageSubdivisionBrgy"].ToString();
                    firstWorksheet["S" + cellnum].Text = row["Country"].ToString();
                    firstWorksheet["T" + cellnum].Text = row["Region"].ToString();
                    firstWorksheet["U" + cellnum].Text = row["Province"].ToString();
                    firstWorksheet["V" + cellnum].Text = row["CityMunicipality"].ToString();
                    firstWorksheet["W" + cellnum].Text = row["ZipCode"].ToString();
                    firstWorksheet["X" + cellnum].Text = row["PermanentHouseNumber"].ToString();
                    firstWorksheet["Y" + cellnum].Text = row["PermanentStreetrVillageSubdivisionBrgy"].ToString();
                    firstWorksheet["Z" + cellnum].Text = row["PermanentCountry"].ToString();
                    firstWorksheet["AA" + cellnum].Text = row["PermanentRegion"].ToString();
                    firstWorksheet["AB" + cellnum].Text = row["PermanentProvince"].ToString();
                    firstWorksheet["AC" + cellnum].Text = row["PermanentCityMunicipality"].ToString();
                    firstWorksheet["AD" + cellnum].Text = row["PermanentZipCode"].ToString();
                    firstWorksheet["AE" + cellnum].Text = row["EmailAddress"].ToString();
                    firstWorksheet["AF" + cellnum].Text = row["EmployerName"].ToString();
                    firstWorksheet["AG" + cellnum].Text = row["EmployeeTIN"].ToString();
                    firstWorksheet["AH" + cellnum].Text = row["CivilStatus"].ToString();
                    firstWorksheet["AI" + cellnum].Text = row["MotherLastName"].ToString();
                    firstWorksheet["AJ" + cellnum].Text = row["MotherFirstName"].ToString();
                    firstWorksheet["AK" + cellnum].Text = row["MotherMiddleName"].ToString();
                    firstWorksheet["AL" + cellnum].Text = row["MotherSuffix"].ToString();
                    firstWorksheet["AM" + cellnum].Text = row["NumberofPads"].ToString();
                    firstWorksheet["AN" + cellnum].Text = row["OtherDetails"].ToString();
                    firstWorksheet["AO" + cellnum].Text = row["Position"].ToString();
                    firstWorksheet["AP" + cellnum].Text = row["Rank"].ToString();
                    firstWorksheet["AQ" + cellnum].Text = row["DateHired"].ToString();
                    firstWorksheet["AR" + cellnum].Text = row["EmploymentStatus"].ToString();
                    firstWorksheet["AS" + cellnum].Text = row["EndofContract"].ToString();
                    firstWorksheet["AT" + cellnum].Text = row["RefOneName"].ToString();
                    firstWorksheet["AU" + cellnum].Text = row["RefOneMobileNumber"].ToString();
                    firstWorksheet["AV" + cellnum].Text = row["RefTwoName"].ToString();
                    firstWorksheet["AW" + cellnum].Text = row["RefTwoMobileNumber"].ToString();
                    firstWorksheet["AX" + cellnum].Text = row["DateAdded"].ToString();

                    cellnum++;
                }

                //https://http://ws.durusthr.com/ILM_WS_LIVE/sFTP/CrtSampleFileExport_12-41-PM.xls
                HttpResponse httpResponse = System.Web.HttpContext.Current.Response;

                httpResponse.Clear();
                httpResponse.ContentType = "Application/x-msexcel";
                httpResponse.AddHeader("Content-Disposition", String.Format("attachment; filename={0}", filename));
                string newDIR = HttpContext.Current.Server.MapPath("~/sFTP/");
                string onlineDIR = "http://ws.durusthr.com/ilm_ws_live/sFTP/";

                workbook.Save(newDIR + filename);

                //return "Success";
                return onlineDIR + filename;
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }
        [HttpPost]
        [Route("CRT_CheckPasswordDefaultv3")]
        public string CRT_CheckPasswordDefaultv3(InsertClientData ClientData)
        {
            try
            {
                tempconnectionstring();
                Connection Connection = new Connection();
                Connection.myparameters.Add(new myParameters { ParameterName = "@UserName", mytype = SqlDbType.VarChar, Value = ClientData.UserName });
                Connection.myparameters.Add(new myParameters { ParameterName = "@CompanyID", mytype = SqlDbType.VarChar, Value = ClientData.CompanyAlias });
                return Connection.ExecuteScalar("sp_CRT_CheckPasswordDefaultv3").ToString();
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }

        [HttpPost]
        [Route("Mobile_Company_Verificationv3")]
        public CRTMobileCompanyDetails Mobile_Company_Detailsv3(CRTMobileCompanyVerification MobileCompanyVerification)
        {
            CRTMobileCompanyDetails CRTMCD = new CRTMobileCompanyDetails();
            try
            {
                var settings = ConfigurationManager.ConnectionStrings["connectionstring"];
                var fi = typeof(ConfigurationElement).GetField(
                              "_bReadOnly",
                              BindingFlags.Instance | BindingFlags.NonPublic);
                fi.SetValue(settings, false);
                settings.ConnectionString = "Data Source=livedb-ilm.c3srcolvclfy.ap-southeast-1.rds.amazonaws.com,1412;Database=ILM_Main;User Id=ILM_LiveDBUser;Password=ILMV3ry53cur3dMGTpa55w0rd2020;";

                Connection Connection = new Connection();
                Connection.myparameters.Add(new myParameters { ParameterName = "@DBNAME", mytype = SqlDbType.NVarChar, Value = MobileCompanyVerification.CompanyAlias });
                var exist = Connection.ExecuteScalar("sp_IsDBExist").ToString();

                if (exist == "Existing")
                {
                    CRTMCD.CompanyType = "durusthr";
                    // return "durusthr";
                }
                else
                {
                    tempconnectionstring();
                    Connection = new Connection();
                    Connection.myparameters.Add(new myParameters { ParameterName = "@CompanyAlias", mytype = SqlDbType.NVarChar, Value = MobileCompanyVerification.CompanyAlias });
                    DataTable DT = Connection.GetDataTable("sp_Mobile_Company_Verification");
                    foreach (DataRow row in DT.Rows)
                    {
                        if (row["Validity"].ToString() == "True")
                        {
                            CRTMCD.CompanyType = "crt";
                            CRTMCD.CompanyName = row["CompanyName"].ToString();
                            CRTMCD.ContactInfo = row["ContactInfo"].ToString();
                        }
                        else
                        {
                            CRTMCD.CompanyType = "none";
                        }
                    }
                }
            }

            catch (Exception e)
            {
                CRTMCD.CompanyType = e.ToString();
            }
            return CRTMCD;
        }
        [HttpPost]
        [Route("CRT_UpdateClientDatav3")]
        public List<CRTComRep> CRT_UpdateClientDatav3(UpdateClientDatav3 ClientData)
        {
            List<CRTComRep> ComRep = new List<CRTComRep>();
            try
            {
                tempconnectionstring();
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@ClientId", mytype = SqlDbType.NVarChar, Value = ClientData.ClientId });
                con.myparameters.Add(new myParameters { ParameterName = "@CN", mytype = SqlDbType.NVarChar, Value = ClientData.CN });
                con.myparameters.Add(new myParameters { ParameterName = "@CompanyAlias", mytype = SqlDbType.NVarChar, Value = ClientData.CompanyAlias });
                con.myparameters.Add(new myParameters { ParameterName = "@DateofIncorporation", mytype = SqlDbType.NVarChar, Value = ClientData.DateofIncorporation });
                con.myparameters.Add(new myParameters { ParameterName = "@AddressFloor", mytype = SqlDbType.NVarChar, Value = ClientData.AddressFloor });
                con.myparameters.Add(new myParameters { ParameterName = "@AddressBuilding", mytype = SqlDbType.NVarChar, Value = ClientData.AddressBuilding });
                con.myparameters.Add(new myParameters { ParameterName = "@AddressStreet", mytype = SqlDbType.NVarChar, Value = ClientData.AddressStreet });
                con.myparameters.Add(new myParameters { ParameterName = "@AddressBrgy", mytype = SqlDbType.NVarChar, Value = ClientData.AddressBrgy });
                con.myparameters.Add(new myParameters { ParameterName = "@AddressMunicipality", mytype = SqlDbType.NVarChar, Value = ClientData.AddressMunicipality });
                con.myparameters.Add(new myParameters { ParameterName = "@AddressProvince", mytype = SqlDbType.NVarChar, Value = ClientData.AddressProvince });
                con.myparameters.Add(new myParameters { ParameterName = "@AddressRegion", mytype = SqlDbType.NVarChar, Value = ClientData.AddressRegion });
                con.myparameters.Add(new myParameters { ParameterName = "@AddressCountry", mytype = SqlDbType.NVarChar, Value = ClientData.AddressCountry });
                con.myparameters.Add(new myParameters { ParameterName = "@AddressZipCode", mytype = SqlDbType.NVarChar, Value = ClientData.AddressZipCode });
                con.myparameters.Add(new myParameters { ParameterName = "@NationalityCountry", mytype = SqlDbType.NVarChar, Value = ClientData.NationalityCountry });
                con.myparameters.Add(new myParameters { ParameterName = "@CompanyEQCustomerNumber", mytype = SqlDbType.NVarChar, Value = ClientData.CompanyEQCustomerNumber });
                con.myparameters.Add(new myParameters { ParameterName = "@TotalNumberOfEmployees", mytype = SqlDbType.NVarChar, Value = ClientData.TotalNumberOfEmployees });
                con.myparameters.Add(new myParameters { ParameterName = "@AccountType", mytype = SqlDbType.NVarChar, Value = ClientData.AccountType });

                con.myparameters.Add(new myParameters { ParameterName = "@Tin", mytype = SqlDbType.NVarChar, Value = ClientData.Tin });
                con.myparameters.Add(new myParameters { ParameterName = "@ModifiedById", mytype = SqlDbType.NVarChar, Value = ClientData.ModifiedById });
                con.myparameters.Add(new myParameters { ParameterName = "@PDF_Status", mytype = SqlDbType.VarChar, Value = ClientData.PDF_Status });
                con.myparameters.Add(new myParameters { ParameterName = "@Salad_Info", mytype = SqlDbType.VarChar, Value = ClientData.Salad_Info });
                con.myparameters.Add(new myParameters { ParameterName = "@CClientID", mytype = SqlDbType.VarChar, Value = ClientData.CClientID });
                con.myparameters.Add(new myParameters { ParameterName = "@lastAlias", mytype = SqlDbType.VarChar, Value = ClientData.lastAlias });
                con.myparameters.Add(new myParameters { ParameterName = "@SaladSigID", mytype = SqlDbType.VarChar, Value = ClientData.SaladSigID });
                con.myparameters.Add(new myParameters { ParameterName = "@NameOfTeam", mytype = SqlDbType.VarChar, Value = ClientData.NameOfTeam });
                con.myparameters.Add(new myParameters { ParameterName = "@PosRank", mytype = SqlDbType.Bit, Value = ClientData.PosRank });
                DataTable Params = new DataTable();
                Params.Columns.AddRange(new DataColumn[8] { new DataColumn("RepID"), new DataColumn("Username"), new DataColumn("Firstname"), new DataColumn("Middlename"), new DataColumn("Lastname"), new DataColumn("RepTitle"), new DataColumn("RepContact"), new DataColumn("RepEmail") });
                for (int i = 0; i < ClientData.RepID.Length; i++)
                {
                    Params.Rows.Add(ClientData.RepID[i], ClientData.UserName[i], ClientData.FirstName[i], ClientData.MiddleName[i], ClientData.LastName[i], ClientData.RepresentativeTitle[i], ClientData.RepresentativeContactNumber[i], ClientData.EmailAddress[i]);
                }
                con.myparameters.Add(new myParameters { ParameterName = "@ComRepInfo", mytype = SqlDbType.Structured, Value = Params });
                DataTable DT = con.GetDataTable("sp_CRT_Updatev3");
                foreach (DataRow row in DT.Rows)
                {
                    CRTComRep CR = new CRTComRep()
                    {
                        item = row["item"].ToString(),
                        reason = row["reason"].ToString()
                    };
                    ComRep.Add(CR);
                }
            }
            catch (Exception e)
            {

            }
            return ComRep;
        }
        #region Testing Purpose Only
        [HttpPost]
        [Route("CRT_EmailValidationForImportv2Testing")]
        public List<DuplicateUsers> DuplicateUsersTesting(UserBulkUpload UBP)
        {
            //var regexItem = new Regex("-");
            DataTable UserList = new DataTable();
            UserList.Columns.AddRange(new DataColumn[10] { new DataColumn("EmpID"), new DataColumn("FName"), new DataColumn("MName"), new DataColumn("LName"), new DataColumn("AccountType"), new DataColumn("TIN"), new DataColumn("Birthdate"), new DataColumn("Suffix"), new DataColumn("Site"), new DataColumn("RowNum") });
            for (int i = 0; i < UBP.employeeID.Length; i++)
            {
                UserList.Rows.Add(UBP.rowNo[i],
                    UBP.employeeID[i],
                    //UBP.fName[i].Replace('-', ' '),
                    //UBP.mName[i].Replace('-', ' '),
                    //UBP.lName[i].Replace('-', ' '),
                    //Regex.Replace(UBP.fName[i], "-", " "),
                    //Regex.Replace(UBP.mName[i], "-", " "),
                    //Regex.Replace(UBP.lName[i], "-", " "),
                    Regex.Replace(UBP.fName[i], @"[\d-#$%^&*@)(_+=!><:;|`~.,?\[\]\/]", " "),
                    Regex.Replace(UBP.mName[i], @"[\d-#$%^&*@)(_+=!><:;|`~.,?\[\]\/]", " "),
                    Regex.Replace(UBP.lName[i], @"[\d-#$%^&*@)(_+=!><:;|`~.,?\[\]\/]", " "),
                    UBP.Suffix[i],
                    UBP.birthDate[i],
                    UBP.site[i],
                    UBP.tin[i],
                    UBP.accountType[i]);
            }
            List<DuplicateUsers> DU = new List<DuplicateUsers>();
            tempconnectionstring();
            Connection con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@CompanyName", mytype = SqlDbType.VarChar, Value = UBP.CN });
            con.myparameters.Add(new myParameters { ParameterName = "@IMPORT", mytype = SqlDbType.Structured, Value = UserList });
            DataTable DT = con.GetDataTable("sp_CRT_EmailValidationForImportv2");
            foreach (DataRow row in DT.Rows)
            {
                DuplicateUsers Users = new DuplicateUsers()
                {
                    RowID = row["RowID"].ToString(),
                    EmployeeID = row["StartingID"].ToString(),
                    Conflict = row["Conflict"].ToString()
                };
                DU.Add(Users);
            }
            return DU;
        }
        #endregion
        [HttpPost]
        [Route("GenerateCRTRevisedv3")]
        public string GenerateCRTPAYROLLRevisedv3(CRTGenerationParams Params)

        {
            FileStream stream = null;
            try
            {
                DataTable DTParam = new DataTable();
                DTParam.Columns.AddRange(new DataColumn[1] { new DataColumn("startingids") });
                for (int i = 0; i < Params.StartingIDS.Length; i++)
                {
                    DTParam.Rows.Add(Params.StartingIDS[i]);
                }

                tempconnectionstring();
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@StartingIDS", mytype = SqlDbType.Structured, Value = DTParam });
                con.myparameters.Add(new myParameters { ParameterName = "@CompanyAlias", mytype = SqlDbType.VarChar, Value = Params.CompanyAlias });
                con.myparameters.Add(new myParameters { ParameterName = "@Branch", mytype = SqlDbType.VarChar, Value = Params.Branch });
                DataTable Sheet7Data = con.GetDataTable("sp_GetSheet7");

                //Naming Convention is "Company Name_Site_Date_Count". Count means pang ilan file siya today. 
                string dataDir = HostingEnvironment.MapPath("~/Files/");

                stream = new FileStream(dataDir + "CRTTemplate.xls", FileMode.Open);
                string filename;
                string datebuild = DateTime.Now.ToString("MMddyyyy");
                string timebuild = DateTime.Now.ToString("hh-mmtt");

                string Companyname = Sheet7Data.Rows[0]["CompanyName"].ToString();
                string Site = Sheet7Data.Rows[0]["Branch"].ToString();
                // string Site = Params.Branch;
                string Count = Sheet7Data.Rows[0]["NumberofGenerate"].ToString();
                string AccountType = Sheet7Data.Rows[0]["AccountType"].ToString();
                if (AccountType == "Easy Savings")
                {
                    AccountType = "ES";
                }
                else
                {
                    AccountType = "AA";
                }

                filename = Companyname + "_" + Site + "_" + datebuild + "_" + AccountType + "_" + Count + ".xls";
                using (var fileStream = new FileStream(dataDir + filename, FileMode.Create, FileAccess.Write))
                //using (var fileStream = new FileStream(dataDir + filename, FileMode.Create, FileAccess.Write))
                {
                    stream.CopyTo(fileStream);
                }
                stream.Close();
                ExcelWorkbookFormat workbookFormat = ExcelWorkbookFormat.Xls_2003;
                string testDocFile = dataDir + "CRTTemplate.xls";
                ExcelWorkbook workbook = new ExcelWorkbook(testDocFile);
                ExcelWorksheet firstWorksheet = workbook.Worksheets[0];

                #region Sheet7

                ExcelWorksheet Sheet7 = workbook.Worksheets[1];
                Sheet7.WorksheetSecurity.ProtectWorksheet("sbc01");

                //if (Sheet7Data.Rows.Count > 0)
                //{
                //    Sheet7["A" + 1].Text = Sheet7Data.Rows[0]["Company CIF Number"].ToString();
                //    Sheet7["B" + 1].Text = Sheet7Data.Rows[0]["Company EQ"].ToString();
                //    Sheet7["C" + 1].Text = Sheet7Data.Rows.Count.ToString();
                //    Sheet7["D" + 1].Text = Sheet7Data.Rows[0]["CompanyName"].ToString();
                //}

                int x = 2;
                for (int i = 0; i < Sheet7Data.Rows.Count; i++, x++)
                {
                    Sheet7["A" + x].Text = Sheet7Data.Rows[i]["LastName"].ToString();
                    Sheet7["B" + x].Text = Sheet7Data.Rows[i]["FirstName"].ToString();
                    Sheet7["C" + x].Text = Sheet7Data.Rows[i]["MiddleName"].ToString();
                    Sheet7["D" + x].Text = Sheet7Data.Rows[i]["Suffix"].ToString();
                    Sheet7["E" + x].Text = Sheet7Data.Rows[i]["FullName"].ToString();
                    Sheet7["F" + x].Text = Sheet7Data.Rows[i]["DateOfBirth"].ToString();
                    Sheet7["G" + x].Text = Sheet7Data.Rows[i]["PlaceOfBirth"].ToString();
                    Sheet7["H" + x].Text = Sheet7Data.Rows[i]["Nationality"].ToString();
                    Sheet7["I" + x].Text = Sheet7Data.Rows[i]["NationalityCountry"].ToString();
                    Sheet7["J" + x].Text = Sheet7Data.Rows[i]["Gender"].ToString();
                    Sheet7["K" + x].Text = Sheet7Data.Rows[i]["MobileNumber"].ToString();
                    Sheet7["L" + x].Text = Sheet7Data.Rows[i]["LandlineNumber"].ToString();
                    Sheet7["M" + x].Text = Sheet7Data.Rows[i]["HouseNumberPresent"].ToString();
                    Sheet7["N" + x].Text = Sheet7Data.Rows[i]["StreetVillageSubdivisionBrgyPresent"].ToString();
                    Sheet7["O" + x].Text = Sheet7Data.Rows[i]["CityMunicipality"].ToString();
                    Sheet7["P" + x].Text = Sheet7Data.Rows[i]["Province"].ToString();
                    Sheet7["Q" + x].Text = Sheet7Data.Rows[i]["Region"].ToString();
                    Sheet7["R" + x].Text = Sheet7Data.Rows[i]["Country"].ToString();
                    Sheet7["S" + x].Text = Sheet7Data.Rows[i]["ZipCode"].ToString();
                    Sheet7["T" + x].Text = Sheet7Data.Rows[i]["HouseNumberPermanent"].ToString();
                    Sheet7["U" + x].Text = Sheet7Data.Rows[i]["StreetVillageSubdivisionBrgyPermanent"].ToString();
                    Sheet7["V" + x].Text = Sheet7Data.Rows[i]["CityMunicipalityPermanent"].ToString();
                    Sheet7["W" + x].Text = Sheet7Data.Rows[i]["ProvincePermanent"].ToString();
                    Sheet7["X" + x].Text = Sheet7Data.Rows[i]["RegionPermanent"].ToString();
                    Sheet7["Y" + x].Text = Sheet7Data.Rows[i]["CountryPermanent"].ToString();
                    Sheet7["Z" + x].Text = Sheet7Data.Rows[i]["ZipCodePermanent"].ToString();
                    Sheet7["AA" + x].Text = Sheet7Data.Rows[i]["Email"].ToString();
                    Sheet7["AB" + x].Text = Sheet7Data.Rows[i]["CompanyName"].ToString();
                    Sheet7["AC" + x].Text = Sheet7Data.Rows[i]["EmployeeTIN"].ToString();
                    Sheet7["AD" + x].Text = Sheet7Data.Rows[i]["CivilStatus"].ToString();
                    Sheet7["AE" + x].Text = Sheet7Data.Rows[i]["MotherLastName"].ToString();
                    Sheet7["AF" + x].Text = Sheet7Data.Rows[i]["MotherFirstName"].ToString();
                    Sheet7["AG" + x].Text = Sheet7Data.Rows[i]["MotherMiddleName"].ToString();
                    Sheet7["AH" + x].Text = Sheet7Data.Rows[i]["SuffixMother"].ToString();
                    Sheet7["AI" + x].Text = Sheet7Data.Rows[i]["NumberOfPads"].ToString();
                    Sheet7["AJ" + x].Text = Sheet7Data.Rows[i]["EmployeeID"].ToString();
                }
                #endregion


                tempconnectionstring();
                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@StartingIDS", mytype = SqlDbType.Structured, Value = DTParam });
                con.myparameters.Add(new myParameters { ParameterName = "@CompanyAlias", mytype = SqlDbType.VarChar, Value = Params.CompanyAlias });
                con.myparameters.Add(new myParameters { ParameterName = "@Branch", mytype = SqlDbType.VarChar, Value = Params.Branch });
                DataTable EmpData = con.GetDataTable("sp_GetMainSheet");

                if (EmpData.Rows.Count > 0)
                {
                    firstWorksheet["C" + 3].Text = EmpData.Rows[0]["CompanyName"].ToString();//Company Name
                    firstWorksheet["C" + 4].Text = EmpData.Rows[0]["DateofIncorporation"].ToString();//Date of Incorporation
                    firstWorksheet["C" + 5].Text = EmpData.Rows[0]["CompleteCompanyAddress"].ToString();//Complete Company Address
                    firstWorksheet["C" + 6].Text = EmpData.Rows[0]["CompanyNationality"].ToString();//Nationality of Company
                    firstWorksheet["C" + 7].Text = EmpData.Rows[0]["ContactNumber"].ToString();//Contact Number
                    firstWorksheet["C" + 8].Text = EmpData.Rows[0]["TIN"].ToString();//Company Tin
                    firstWorksheet["C" + 10].Text = EmpData.Rows[0]["CompanyEQNumber"].ToString();//Company EQ Customer Number
                    firstWorksheet["C" + 11].Text = EmpData.Rows.Count.ToString();//Total Number of Employees
                    firstWorksheet["C" + 12].Text = EmpData.Rows[0]["AccountType"].ToString();//Account Type
                }

                int cellnum = 19;

                for (int i = 0; i < EmpData.Rows.Count; i++)
                {
                    firstWorksheet["A" + cellnum].Text = (i + 1).ToString();
                    firstWorksheet["B" + cellnum].Text = EmpData.Rows[i]["LastName"].ToString();
                    firstWorksheet["C" + cellnum].Text = EmpData.Rows[i]["FirstName"].ToString();
                    firstWorksheet["D" + cellnum].Text = EmpData.Rows[i]["MiddleName"].ToString();
                    firstWorksheet["E" + cellnum].Text = EmpData.Rows[i]["Suffix"].ToString();
                    firstWorksheet["F" + cellnum].Text = EmpData.Rows[i]["CardName"].ToString();
                    firstWorksheet["G" + cellnum].DateTimeValue = DateTime.Parse(EmpData.Rows[i]["Birthday"].ToString(), CultureInfo.InvariantCulture);
                    //firstWorksheet["G" + cellnum].DateTimeValue = DateTime.ParseExact(EmpData.Rows[i]["Birthday"].ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture) CultureInfo.CreateSpecificCulture("fr-FR"); 
                    firstWorksheet["H" + cellnum].Text = EmpData.Rows[i]["PlaceOfBirth"].ToString();
                    firstWorksheet["I" + cellnum].Text = EmpData.Rows[i]["Nationality"].ToString();
                    firstWorksheet["J" + cellnum].Text = EmpData.Rows[i]["NationalityCountry"].ToString();
                    firstWorksheet["K" + cellnum].Text = EmpData.Rows[i]["Gender"].ToString();
                    firstWorksheet["L" + cellnum].Text = EmpData.Rows[i]["MobileNumber"].ToString();
                    firstWorksheet["M" + cellnum].Text = EmpData.Rows[i]["LandlineNumber"].ToString();
                    firstWorksheet["N" + cellnum].Text = EmpData.Rows[i]["HouseNumber"].ToString();
                    firstWorksheet["O" + cellnum].Text = EmpData.Rows[i]["StreetVillageSubdivisionBrgyPresent"].ToString();
                    firstWorksheet["P" + cellnum].Text = EmpData.Rows[i]["Country"].ToString();
                    firstWorksheet["Q" + cellnum].Text = EmpData.Rows[i]["Region"].ToString();
                    firstWorksheet["R" + cellnum].Text = EmpData.Rows[i]["Province"].ToString();
                    firstWorksheet["S" + cellnum].Text = EmpData.Rows[i]["Municipality"].ToString();
                    firstWorksheet["T" + cellnum].Text = EmpData.Rows[i]["ZipCode"].ToString();
                    firstWorksheet["U" + cellnum].Text = EmpData.Rows[i]["HouseNumber2"].ToString();
                    firstWorksheet["V" + cellnum].Text = EmpData.Rows[i]["StreetVillageSubdivisionBrgyPermanent"].ToString();
                    firstWorksheet["W" + cellnum].Text = EmpData.Rows[i]["CountryPermanent"].ToString();
                    firstWorksheet["X" + cellnum].Text = EmpData.Rows[i]["RegionPermanent"].ToString();
                    firstWorksheet["Y" + cellnum].Text = EmpData.Rows[i]["ProvincePermanent"].ToString();
                    firstWorksheet["Z" + cellnum].Text = EmpData.Rows[i]["MunicipalityPermanent"].ToString();
                    firstWorksheet["AA" + cellnum].Text = EmpData.Rows[i]["ZipcodePermanent"].ToString();
                    firstWorksheet["AB" + cellnum].Text = EmpData.Rows[i]["Email"].ToString();
                    firstWorksheet["AC" + cellnum].Text = EmpData.Rows[i]["EmployerName"].ToString();
                    firstWorksheet["AD" + cellnum].Text = EmpData.Rows[i]["EmployeeTIN"].ToString();
                    firstWorksheet["AE" + cellnum].Text = EmpData.Rows[i]["CivilStatus"].ToString();
                    firstWorksheet["AF" + cellnum].Text = EmpData.Rows[i]["Last_Name"].ToString();
                    firstWorksheet["AG" + cellnum].Text = EmpData.Rows[i]["First_Name"].ToString();
                    firstWorksheet["AH" + cellnum].Text = EmpData.Rows[i]["Middle_Name"].ToString();
                    firstWorksheet["AI" + cellnum].Text = EmpData.Rows[i]["SuffixMother"].ToString();
                    firstWorksheet["AJ" + cellnum].Text = EmpData.Rows[i]["NumberOfPads"].ToString();
                    firstWorksheet["AK" + cellnum].Text = EmpData.Rows[i]["OtherDetails"].ToString();

                    cellnum++;
                }


                HttpResponse httpResponse = System.Web.HttpContext.Current.Response;

                httpResponse.Clear();
                httpResponse.ContentType = "Application/x-msexcel";
                httpResponse.AddHeader("Content-Disposition", String.Format("attachment; filename={0}", filename));
                string newDIR = HttpContext.Current.Server.MapPath("~/sFTP/");
                string onlineDIR = "http://ws.durusthr.com/ILM_WS_Live/sFTP/";
                //filename = filename.Replace(',', '_');
                //filename = filename.Replace(" ", String.Empty);
                workbook.Save(newDIR + filename);
                stream.Close();
                //return "Success";
                return onlineDIR + filename;
            }
            catch (Exception e)
            {
                stream.Close();
                return e.ToString();
            }
        }

        [HttpPost]
        [Route("GenerateCRTRevisedv4")]
        public string GenerateCRTPAYROLLRevisedv4(CRTGenerationParams Params)

        {
            FileStream stream = null;
            try
            {
                DataTable DTParam = new DataTable();
                DTParam.Columns.AddRange(new DataColumn[1] { new DataColumn("startingids") });
                for (int i = 0; i < Params.StartingIDS.Length; i++)
                {
                    DTParam.Rows.Add(Params.StartingIDS[i]);
                }

                tempconnectionstring();
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@StartingIDS", mytype = SqlDbType.Structured, Value = DTParam });
                con.myparameters.Add(new myParameters { ParameterName = "@CompanyAlias", mytype = SqlDbType.VarChar, Value = Params.CompanyAlias });
                con.myparameters.Add(new myParameters { ParameterName = "@Branch", mytype = SqlDbType.VarChar, Value = Params.Branch });
                DataTable Sheet7Data = con.GetDataTable("sp_GetSheet7");

                //Naming Convention is "Company Name_Site_Date_Count". Count means pang ilan file siya today. 
                string dataDir = HostingEnvironment.MapPath("~/Files/");

                stream = new FileStream(dataDir + "CRTTemplate.xls", FileMode.Open);
                string filename;
                string datebuild = DateTime.Now.ToString("MMddyyyy");
                string timebuild = DateTime.Now.ToString("hh-mmtt");

                string Companyname = Sheet7Data.Rows[0]["CompanyName"].ToString();
                string Site = Sheet7Data.Rows[0]["Branch"].ToString();
                // string Site = Params.Branch;
                string Count = Sheet7Data.Rows[0]["NumberofGenerate"].ToString();
                string AccountType = Sheet7Data.Rows[0]["AccountType"].ToString();
                if (AccountType == "Easy Savings")
                {
                    AccountType = "ES";
                }
                else
                {
                    AccountType = "AA";
                }

                filename = Companyname + "_" + Site + "_" + datebuild + "_" + AccountType + "_" + Count + ".xls";
                using (var fileStream = new FileStream(dataDir + filename, FileMode.Create, FileAccess.Write))
                //using (var fileStream = new FileStream(dataDir + filename, FileMode.Create, FileAccess.Write))
                {
                    stream.CopyTo(fileStream);
                }
                stream.Close();
                ExcelWorkbookFormat workbookFormat = ExcelWorkbookFormat.Xls_2003;
                string testDocFile = dataDir + "CRTTemplate.xls";
                ExcelWorkbook workbook = new ExcelWorkbook(testDocFile);
                ExcelWorksheet firstWorksheet = workbook.Worksheets[0];

                #region Sheet7

                ExcelWorksheet Sheet7 = workbook.Worksheets[1];
                Sheet7.WorksheetSecurity.ProtectWorksheet("sbc01");

                //if (Sheet7Data.Rows.Count > 0)
                //{
                //    Sheet7["A" + 1].Text = Sheet7Data.Rows[0]["Company CIF Number"].ToString();
                //    Sheet7["B" + 1].Text = Sheet7Data.Rows[0]["Company EQ"].ToString();
                //    Sheet7["C" + 1].Text = Sheet7Data.Rows.Count.ToString();
                //    Sheet7["D" + 1].Text = Sheet7Data.Rows[0]["CompanyName"].ToString();
                //}

                int x = 2;
                for (int i = 0; i < Sheet7Data.Rows.Count; i++, x++)
                {
                    Sheet7["A" + x].Text = Sheet7Data.Rows[i]["LastName"].ToString();
                    Sheet7["B" + x].Text = Sheet7Data.Rows[i]["FirstName"].ToString();
                    Sheet7["C" + x].Text = Sheet7Data.Rows[i]["MiddleName"].ToString();
                    Sheet7["D" + x].Text = Sheet7Data.Rows[i]["Suffix"].ToString();
                    Sheet7["E" + x].Text = Sheet7Data.Rows[i]["FullName"].ToString();
                    Sheet7["F" + x].Text = Sheet7Data.Rows[i]["DateOfBirth"].ToString();
                    Sheet7["G" + x].Text = Sheet7Data.Rows[i]["PlaceOfBirth"].ToString();
                    Sheet7["H" + x].Text = Sheet7Data.Rows[i]["Nationality"].ToString();
                    Sheet7["I" + x].Text = Sheet7Data.Rows[i]["NationalityCountry"].ToString();
                    Sheet7["J" + x].Text = Sheet7Data.Rows[i]["Gender"].ToString();
                    Sheet7["K" + x].Text = Sheet7Data.Rows[i]["MobileNumber"].ToString();
                    Sheet7["L" + x].Text = Sheet7Data.Rows[i]["LandlineNumber"].ToString();
                    Sheet7["M" + x].Text = Sheet7Data.Rows[i]["HouseNumberPresent"].ToString();
                    Sheet7["N" + x].Text = Sheet7Data.Rows[i]["StreetVillageSubdivisionBrgyPresent"].ToString();
                    Sheet7["O" + x].Text = Sheet7Data.Rows[i]["CityMunicipality"].ToString();
                    Sheet7["P" + x].Text = Sheet7Data.Rows[i]["Province"].ToString();
                    Sheet7["Q" + x].Text = Sheet7Data.Rows[i]["Region"].ToString();
                    Sheet7["R" + x].Text = Sheet7Data.Rows[i]["Country"].ToString();
                    Sheet7["S" + x].Text = Sheet7Data.Rows[i]["ZipCode"].ToString();
                    Sheet7["T" + x].Text = Sheet7Data.Rows[i]["HouseNumberPermanent"].ToString();
                    Sheet7["U" + x].Text = Sheet7Data.Rows[i]["StreetVillageSubdivisionBrgyPermanent"].ToString();
                    Sheet7["V" + x].Text = Sheet7Data.Rows[i]["CityMunicipalityPermanent"].ToString();
                    Sheet7["W" + x].Text = Sheet7Data.Rows[i]["ProvincePermanent"].ToString();
                    Sheet7["X" + x].Text = Sheet7Data.Rows[i]["RegionPermanent"].ToString();
                    Sheet7["Y" + x].Text = Sheet7Data.Rows[i]["CountryPermanent"].ToString();
                    Sheet7["Z" + x].Text = Sheet7Data.Rows[i]["ZipCodePermanent"].ToString();
                    Sheet7["AA" + x].Text = Sheet7Data.Rows[i]["Email"].ToString();
                    Sheet7["AB" + x].Text = Sheet7Data.Rows[i]["CompanyName"].ToString();
                    Sheet7["AC" + x].Text = Sheet7Data.Rows[i]["EmployeeTIN"].ToString();
                    Sheet7["AD" + x].Text = Sheet7Data.Rows[i]["CivilStatus"].ToString();
                    Sheet7["AE" + x].Text = Sheet7Data.Rows[i]["MotherLastName"].ToString();
                    Sheet7["AF" + x].Text = Sheet7Data.Rows[i]["MotherFirstName"].ToString();
                    Sheet7["AG" + x].Text = Sheet7Data.Rows[i]["MotherMiddleName"].ToString();
                    Sheet7["AH" + x].Text = Sheet7Data.Rows[i]["SuffixMother"].ToString();
                    Sheet7["AI" + x].Text = Sheet7Data.Rows[i]["NumberOfPads"].ToString();
                    Sheet7["AJ" + x].Text = Sheet7Data.Rows[i]["EmployeeID"].ToString();
                }
                #endregion

                tempconnectionstring();
                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@StartingIDS", mytype = SqlDbType.Structured, Value = DTParam });
                con.myparameters.Add(new myParameters { ParameterName = "@CompanyAlias", mytype = SqlDbType.VarChar, Value = Params.CompanyAlias });
                con.myparameters.Add(new myParameters { ParameterName = "@Branch", mytype = SqlDbType.VarChar, Value = Params.Branch });
                DataTable EmpData = con.GetDataTable("sp_GetMainSheet");

                if (EmpData.Rows.Count > 0)
                {
                    firstWorksheet["C" + 3].Text = EmpData.Rows[0]["CompanyName"].ToString();//Company Name
                    firstWorksheet["C" + 4].Text = EmpData.Rows[0]["DateofIncorporation"].ToString();//Date of Incorporation
                    firstWorksheet["C" + 5].Text = EmpData.Rows[0]["CompleteCompanyAddress"].ToString();//Complete Company Address
                    firstWorksheet["C" + 6].Text = EmpData.Rows[0]["CompanyNationality"].ToString();//Nationality of Company
                    firstWorksheet["C" + 7].Text = EmpData.Rows[0]["ContactNumber"].ToString();//Contact Number
                    firstWorksheet["C" + 8].Text = EmpData.Rows[0]["TIN"].ToString();//Company Tin
                    firstWorksheet["C" + 10].Text = EmpData.Rows[0]["CompanyEQNumber"].ToString();//Company EQ Customer Number
                    firstWorksheet["C" + 11].Text = EmpData.Rows.Count.ToString();//Total Number of Employees
                    firstWorksheet["C" + 12].Text = EmpData.Rows[0]["AccountType"].ToString();//Account Type
                }

                int cellnum = 19;

                for (int i = 0; i < EmpData.Rows.Count; i++)
                {
                    firstWorksheet["A" + cellnum].Text = (i + 1).ToString();
                    firstWorksheet["B" + cellnum].Text = EmpData.Rows[i]["LastName"].ToString();
                    firstWorksheet["C" + cellnum].Text = EmpData.Rows[i]["FirstName"].ToString();
                    firstWorksheet["D" + cellnum].Text = EmpData.Rows[i]["MiddleName"].ToString();
                    firstWorksheet["E" + cellnum].Text = EmpData.Rows[i]["Suffix"].ToString();
                    firstWorksheet["F" + cellnum].Text = EmpData.Rows[i]["CardName"].ToString();
                    firstWorksheet["G" + cellnum].DateTimeValue = DateTime.Parse(EmpData.Rows[i]["Birthday"].ToString(), CultureInfo.InvariantCulture);
                    //firstWorksheet["G" + cellnum].DateTimeValue = DateTime.ParseExact(EmpData.Rows[i]["Birthday"].ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture) CultureInfo.CreateSpecificCulture("fr-FR"); 
                    firstWorksheet["H" + cellnum].Text = EmpData.Rows[i]["PlaceOfBirth"].ToString();
                    firstWorksheet["I" + cellnum].Text = EmpData.Rows[i]["Nationality"].ToString();
                    firstWorksheet["J" + cellnum].Text = EmpData.Rows[i]["NationalityCountry"].ToString();
                    firstWorksheet["K" + cellnum].Text = EmpData.Rows[i]["Gender"].ToString();
                    firstWorksheet["L" + cellnum].Text = EmpData.Rows[i]["MobileNumber"].ToString();
                    firstWorksheet["M" + cellnum].Text = EmpData.Rows[i]["LandlineNumber"].ToString();
                    firstWorksheet["N" + cellnum].Text = EmpData.Rows[i]["HouseNumber"].ToString();
                    firstWorksheet["O" + cellnum].Text = EmpData.Rows[i]["StreetVillageSubdivisionBrgyPresent"].ToString();
                    firstWorksheet["P" + cellnum].Text = EmpData.Rows[i]["Country"].ToString();
                    firstWorksheet["Q" + cellnum].Text = EmpData.Rows[i]["Region"].ToString();
                    firstWorksheet["R" + cellnum].Text = EmpData.Rows[i]["Province"].ToString();
                    firstWorksheet["S" + cellnum].Text = EmpData.Rows[i]["Municipality"].ToString();
                    firstWorksheet["T" + cellnum].Text = EmpData.Rows[i]["ZipCode"].ToString();
                    firstWorksheet["U" + cellnum].Text = EmpData.Rows[i]["HouseNumber2"].ToString();
                    firstWorksheet["V" + cellnum].Text = EmpData.Rows[i]["StreetVillageSubdivisionBrgyPermanent"].ToString();
                    firstWorksheet["W" + cellnum].Text = EmpData.Rows[i]["CountryPermanent"].ToString();
                    firstWorksheet["X" + cellnum].Text = EmpData.Rows[i]["RegionPermanent"].ToString();
                    firstWorksheet["Y" + cellnum].Text = EmpData.Rows[i]["ProvincePermanent"].ToString();
                    firstWorksheet["Z" + cellnum].Text = EmpData.Rows[i]["MunicipalityPermanent"].ToString();
                    firstWorksheet["AA" + cellnum].Text = EmpData.Rows[i]["ZipcodePermanent"].ToString();
                    firstWorksheet["AB" + cellnum].Text = EmpData.Rows[i]["Email"].ToString();
                    firstWorksheet["AC" + cellnum].Text = EmpData.Rows[i]["EmployerName"].ToString();
                    firstWorksheet["AD" + cellnum].Text = EmpData.Rows[i]["EmployeeTIN"].ToString();
                    firstWorksheet["AE" + cellnum].Text = EmpData.Rows[i]["CivilStatus"].ToString();
                    firstWorksheet["AF" + cellnum].Text = EmpData.Rows[i]["Last_Name"].ToString();
                    firstWorksheet["AG" + cellnum].Text = EmpData.Rows[i]["First_Name"].ToString();
                    firstWorksheet["AH" + cellnum].Text = EmpData.Rows[i]["Middle_Name"].ToString();
                    firstWorksheet["AI" + cellnum].Text = EmpData.Rows[i]["SuffixMother"].ToString();
                    firstWorksheet["AJ" + cellnum].Text = EmpData.Rows[i]["NumberOfPads"].ToString();
                    firstWorksheet["AK" + cellnum].Text = EmpData.Rows[i]["OtherDetails"].ToString();

                    cellnum++;
                }


                HttpResponse httpResponse = System.Web.HttpContext.Current.Response;

                httpResponse.Clear();
                httpResponse.ContentType = "Application/x-msexcel";
                httpResponse.AddHeader("Content-Disposition", String.Format("attachment; filename={0}", filename));
                string newDIR = HttpContext.Current.Server.MapPath("~/sFTP/");
                string onlineDIR = "http://ws.durusthr.com/ILM_WS_Live/sFTP/";
                //filename = filename.Replace(',', '_');
                //filename = filename.Replace(" ", String.Empty);
                workbook.Save(newDIR + filename);
                stream.Close();
                //return "Success";

                if (Sheet7Data.Rows.Count > 0)
                {
                    return onlineDIR + filename;
                }
                else
                {
                    return "No Rows";
                }
            }
            catch (Exception e)
            {
                stream.Close();
                return "Error: " + e.ToString();
            }
        }

        [HttpPost]
        [Route("GenerateCRTRevisedv5")]
        public string GenerateCRTPAYROLLRevisedv5(CRTGenerationParams Params)

        {
            FileStream stream = null;
            try
            {
                DataTable DTParam = new DataTable();
                DTParam.Columns.AddRange(new DataColumn[1] { new DataColumn("startingids") });
                for (int i = 0; i < Params.StartingIDS.Length; i++)
                {
                    DTParam.Rows.Add(Params.StartingIDS[i]);
                }

                tempconnectionstring();
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@StartingIDS", mytype = SqlDbType.Structured, Value = DTParam });
                con.myparameters.Add(new myParameters { ParameterName = "@CompanyAlias", mytype = SqlDbType.VarChar, Value = Params.CompanyAlias });
                con.myparameters.Add(new myParameters { ParameterName = "@Branch", mytype = SqlDbType.VarChar, Value = Params.Branch });
                DataTable Sheet7Data = con.GetDataTable("sp_GetSheet7Nov7");

                //Naming Convention is "Company Name_Site_Date_Count". Count means pang ilan file siya today. 
                string dataDir = HostingEnvironment.MapPath("~/Files/");

                stream = new FileStream(dataDir + "CRTTemplate.xls", FileMode.Open);
                string filename;
                string datebuild = DateTime.Now.ToString("MMddyyyy");
                string timebuild = DateTime.Now.ToString("hh-mmtt");

                string Companyname = Sheet7Data.Rows[0]["CompanyName"].ToString();
                string Site = Sheet7Data.Rows[0]["Branch"].ToString();
                // string Site = Params.Branch;
                string Count = Sheet7Data.Rows[0]["NumberofGenerate"].ToString();
                string AccountType = Sheet7Data.Rows[0]["AccountType"].ToString();
                if (AccountType == "Easy Savings")
                {
                    AccountType = "ES";
                }
                else
                {
                    AccountType = "AA";
                }

                filename = Companyname + "_" + Site + "_" + datebuild + "_" + AccountType + "_" + Count + ".xls";
                using (var fileStream = new FileStream(dataDir + filename, FileMode.Create, FileAccess.Write))
                //using (var fileStream = new FileStream(dataDir + filename, FileMode.Create, FileAccess.Write))
                {
                    stream.CopyTo(fileStream);
                }
                stream.Close();
                ExcelWorkbookFormat workbookFormat = ExcelWorkbookFormat.Xls_2003;
                string testDocFile = dataDir + "CRTTemplate.xls";
                ExcelWorkbook workbook = new ExcelWorkbook(testDocFile);
                ExcelWorksheet firstWorksheet = workbook.Worksheets[0];

                #region Sheet7

                ExcelWorksheet Sheet7 = workbook.Worksheets[1];
                Sheet7.WorksheetSecurity.ProtectWorksheet("sbc01");

                //if (Sheet7Data.Rows.Count > 0)
                //{
                //    Sheet7["A" + 1].Text = Sheet7Data.Rows[0]["Company CIF Number"].ToString();
                //    Sheet7["B" + 1].Text = Sheet7Data.Rows[0]["Company EQ"].ToString();
                //    Sheet7["C" + 1].Text = Sheet7Data.Rows.Count.ToString();
                //    Sheet7["D" + 1].Text = Sheet7Data.Rows[0]["CompanyName"].ToString();
                //}

                int x = 2;
                for (int i = 0; i < Sheet7Data.Rows.Count; i++, x++)
                {
                    Sheet7["A" + x].Text = Sheet7Data.Rows[i]["LastName"].ToString();
                    Sheet7["B" + x].Text = Sheet7Data.Rows[i]["FirstName"].ToString();
                    Sheet7["C" + x].Text = Sheet7Data.Rows[i]["MiddleName"].ToString();
                    Sheet7["D" + x].Text = Sheet7Data.Rows[i]["Suffix"].ToString();
                    Sheet7["E" + x].Text = Sheet7Data.Rows[i]["FullName"].ToString();
                    Sheet7["F" + x].Text = Sheet7Data.Rows[i]["DateOfBirth"].ToString();
                    Sheet7["G" + x].Text = Sheet7Data.Rows[i]["PlaceOfBirth"].ToString();
                    Sheet7["H" + x].Text = Sheet7Data.Rows[i]["Nationality"].ToString();
                    Sheet7["I" + x].Text = Sheet7Data.Rows[i]["NationalityCountry"].ToString();
                    Sheet7["J" + x].Text = Sheet7Data.Rows[i]["Gender"].ToString();
                    Sheet7["K" + x].Text = Sheet7Data.Rows[i]["MobileNumber"].ToString();
                    Sheet7["L" + x].Text = Sheet7Data.Rows[i]["LandlineNumber"].ToString();
                    Sheet7["M" + x].Text = Sheet7Data.Rows[i]["HouseNumberPresent"].ToString();
                    Sheet7["N" + x].Text = Sheet7Data.Rows[i]["StreetVillageSubdivisionBrgyPresent"].ToString();
                    Sheet7["O" + x].Text = Sheet7Data.Rows[i]["CityMunicipality"].ToString();
                    Sheet7["P" + x].Text = Sheet7Data.Rows[i]["Province"].ToString();
                    Sheet7["Q" + x].Text = Sheet7Data.Rows[i]["Region"].ToString();
                    Sheet7["R" + x].Text = Sheet7Data.Rows[i]["Country"].ToString();
                    Sheet7["S" + x].Text = Sheet7Data.Rows[i]["ZipCode"].ToString();
                    Sheet7["T" + x].Text = Sheet7Data.Rows[i]["HouseNumberPermanent"].ToString();
                    Sheet7["U" + x].Text = Sheet7Data.Rows[i]["StreetVillageSubdivisionBrgyPermanent"].ToString();
                    Sheet7["V" + x].Text = Sheet7Data.Rows[i]["CityMunicipalityPermanent"].ToString();
                    Sheet7["W" + x].Text = Sheet7Data.Rows[i]["ProvincePermanent"].ToString();
                    Sheet7["X" + x].Text = Sheet7Data.Rows[i]["RegionPermanent"].ToString();
                    Sheet7["Y" + x].Text = Sheet7Data.Rows[i]["CountryPermanent"].ToString();
                    Sheet7["Z" + x].Text = Sheet7Data.Rows[i]["ZipCodePermanent"].ToString();
                    Sheet7["AA" + x].Text = Sheet7Data.Rows[i]["Email"].ToString();
                    Sheet7["AB" + x].Text = Sheet7Data.Rows[i]["CompanyName"].ToString();
                    Sheet7["AC" + x].Text = Sheet7Data.Rows[i]["EmployeeTIN"].ToString();
                    Sheet7["AD" + x].Text = Sheet7Data.Rows[i]["CivilStatus"].ToString();
                    Sheet7["AE" + x].Text = Sheet7Data.Rows[i]["MotherLastName"].ToString();
                    Sheet7["AF" + x].Text = Sheet7Data.Rows[i]["MotherFirstName"].ToString();
                    Sheet7["AG" + x].Text = Sheet7Data.Rows[i]["MotherMiddleName"].ToString();
                    Sheet7["AH" + x].Text = Sheet7Data.Rows[i]["SuffixMother"].ToString();
                    Sheet7["AI" + x].Text = Sheet7Data.Rows[i]["NumberOfPads"].ToString();
                    Sheet7["AJ" + x].Text = Sheet7Data.Rows[i]["EmployeeID"].ToString();
                }
                #endregion

                tempconnectionstring();
                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@StartingIDS", mytype = SqlDbType.Structured, Value = DTParam });
                con.myparameters.Add(new myParameters { ParameterName = "@CompanyAlias", mytype = SqlDbType.VarChar, Value = Params.CompanyAlias });
                con.myparameters.Add(new myParameters { ParameterName = "@Branch", mytype = SqlDbType.VarChar, Value = Params.Branch });
                DataTable EmpData = con.GetDataTable("sp_GetMainSheetNov7");

                if (EmpData.Rows.Count > 0)
                {
                    firstWorksheet["C" + 3].Text = EmpData.Rows[0]["CompanyName"].ToString();//Company Name
                    firstWorksheet["C" + 4].Text = EmpData.Rows[0]["DateofIncorporation"].ToString();//Date of Incorporation
                    firstWorksheet["C" + 5].Text = EmpData.Rows[0]["CompleteCompanyAddress"].ToString();//Complete Company Address
                    firstWorksheet["C" + 6].Text = EmpData.Rows[0]["CompanyNationality"].ToString();//Nationality of Company
                    firstWorksheet["C" + 7].Text = EmpData.Rows[0]["ContactNumber"].ToString();//Contact Number
                    firstWorksheet["C" + 8].Text = EmpData.Rows[0]["TIN"].ToString();//Company Tin
                    firstWorksheet["C" + 10].Text = EmpData.Rows[0]["CompanyEQNumber"].ToString();//Company EQ Customer Number
                    firstWorksheet["C" + 11].Text = EmpData.Rows.Count.ToString();//Total Number of Employees
                    firstWorksheet["C" + 12].Text = EmpData.Rows[0]["AccountType"].ToString();//Account Type
                }

                int cellnum = 19;

                for (int i = 0; i < EmpData.Rows.Count; i++)
                {
                    firstWorksheet["A" + cellnum].Text = (i + 1).ToString();
                    firstWorksheet["B" + cellnum].Text = EmpData.Rows[i]["LastName"].ToString();
                    firstWorksheet["C" + cellnum].Text = EmpData.Rows[i]["FirstName"].ToString();
                    firstWorksheet["D" + cellnum].Text = EmpData.Rows[i]["MiddleName"].ToString();
                    firstWorksheet["E" + cellnum].Text = EmpData.Rows[i]["Suffix"].ToString();
                    firstWorksheet["F" + cellnum].Text = EmpData.Rows[i]["CardName"].ToString();
                    firstWorksheet["G" + cellnum].DateTimeValue = DateTime.Parse(EmpData.Rows[i]["Birthday"].ToString(), CultureInfo.InvariantCulture);
                    //firstWorksheet["G" + cellnum].DateTimeValue = DateTime.ParseExact(EmpData.Rows[i]["Birthday"].ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture) CultureInfo.CreateSpecificCulture("fr-FR"); 
                    firstWorksheet["H" + cellnum].Text = EmpData.Rows[i]["PlaceOfBirth"].ToString();
                    firstWorksheet["I" + cellnum].Text = EmpData.Rows[i]["Nationality"].ToString();
                    firstWorksheet["J" + cellnum].Text = EmpData.Rows[i]["NationalityCountry"].ToString();
                    firstWorksheet["K" + cellnum].Text = EmpData.Rows[i]["Gender"].ToString();
                    firstWorksheet["L" + cellnum].Text = EmpData.Rows[i]["MobileNumber"].ToString();
                    firstWorksheet["M" + cellnum].Text = EmpData.Rows[i]["LandlineNumber"].ToString();
                    firstWorksheet["N" + cellnum].Text = EmpData.Rows[i]["HouseNumber"].ToString();
                    firstWorksheet["O" + cellnum].Text = EmpData.Rows[i]["StreetVillageSubdivisionBrgyPresent"].ToString();
                    firstWorksheet["P" + cellnum].Text = EmpData.Rows[i]["Country"].ToString();
                    firstWorksheet["Q" + cellnum].Text = EmpData.Rows[i]["Region"].ToString();
                    firstWorksheet["R" + cellnum].Text = EmpData.Rows[i]["Province"].ToString();
                    firstWorksheet["S" + cellnum].Text = EmpData.Rows[i]["Municipality"].ToString();
                    firstWorksheet["T" + cellnum].Text = EmpData.Rows[i]["ZipCode"].ToString();
                    firstWorksheet["U" + cellnum].Text = EmpData.Rows[i]["HouseNumber2"].ToString();
                    firstWorksheet["V" + cellnum].Text = EmpData.Rows[i]["StreetVillageSubdivisionBrgyPermanent"].ToString();
                    firstWorksheet["W" + cellnum].Text = EmpData.Rows[i]["CountryPermanent"].ToString();
                    firstWorksheet["X" + cellnum].Text = EmpData.Rows[i]["RegionPermanent"].ToString();
                    firstWorksheet["Y" + cellnum].Text = EmpData.Rows[i]["ProvincePermanent"].ToString();
                    firstWorksheet["Z" + cellnum].Text = EmpData.Rows[i]["MunicipalityPermanent"].ToString();
                    firstWorksheet["AA" + cellnum].Text = EmpData.Rows[i]["ZipcodePermanent"].ToString();
                    firstWorksheet["AB" + cellnum].Text = EmpData.Rows[i]["Email"].ToString();
                    firstWorksheet["AC" + cellnum].Text = EmpData.Rows[i]["EmployerName"].ToString();
                    firstWorksheet["AD" + cellnum].Text = EmpData.Rows[i]["EmployeeTIN"].ToString();
                    firstWorksheet["AE" + cellnum].Text = EmpData.Rows[i]["CivilStatus"].ToString();
                    firstWorksheet["AF" + cellnum].Text = EmpData.Rows[i]["Last_Name"].ToString();
                    firstWorksheet["AG" + cellnum].Text = EmpData.Rows[i]["First_Name"].ToString();
                    firstWorksheet["AH" + cellnum].Text = EmpData.Rows[i]["Middle_Name"].ToString();
                    firstWorksheet["AI" + cellnum].Text = EmpData.Rows[i]["SuffixMother"].ToString();
                    firstWorksheet["AJ" + cellnum].Text = EmpData.Rows[i]["NumberOfPads"].ToString();
                    firstWorksheet["AK" + cellnum].Text = EmpData.Rows[i]["OtherDetails"].ToString();

                    cellnum++;
                }


                HttpResponse httpResponse = System.Web.HttpContext.Current.Response;

                httpResponse.Clear();
                httpResponse.ContentType = "Application/x-msexcel";
                httpResponse.AddHeader("Content-Disposition", String.Format("attachment; filename={0}", filename));
                string newDIR = HttpContext.Current.Server.MapPath("~/sFTP/");
                string onlineDIR = "http://ws.durusthr.com/ILM_WS_Live/sFTP/";
                //filename = filename.Replace(',', '_');
                //filename = filename.Replace(" ", String.Empty);
                workbook.Save(newDIR + filename);
                stream.Close();
                //return "Success";

                if (Sheet7Data.Rows.Count > 0)
                {
                    return onlineDIR + filename;
                }
                else
                {
                    return "No Rows";
                }
            }
            catch (Exception e)
            {
                stream.Close();
                return "Error: " + e.ToString();
            }
        }

        [HttpPost]
        [Route("GenerateCRTRevisedv6")]
        public string GenerateCRTPAYROLLRevisedv6(CRTGenerationParams Params)

        {
            FileStream stream = null;
            try
            {
                DataTable DTParam = new DataTable();
                DTParam.Columns.AddRange(new DataColumn[1] { new DataColumn("startingids") });
                for (int i = 0; i < Params.StartingIDS.Length; i++)
                {
                    DTParam.Rows.Add(Params.StartingIDS[i]);
                }

                tempconnectionstring();
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@StartingIDS", mytype = SqlDbType.Structured, Value = DTParam });
                con.myparameters.Add(new myParameters { ParameterName = "@CompanyAlias", mytype = SqlDbType.VarChar, Value = Params.CompanyAlias });
                con.myparameters.Add(new myParameters { ParameterName = "@Branch", mytype = SqlDbType.VarChar, Value = Params.Branch });
                DataTable Sheet7Data = con.GetDataTable("sp_GetSheet7");

                //Naming Convention is "Company Name_Site_Date_Count". Count means pang ilan file siya today. 
                string dataDir = HostingEnvironment.MapPath("~/Files/");

                stream = new FileStream(dataDir + "CRTTemplatev2.xls", FileMode.Open);
                string filename;
                string datebuild = DateTime.Now.ToString("MMddyyyy");
                string timebuild = DateTime.Now.ToString("hh-mmtt");

                string Companyname = Sheet7Data.Rows[0]["CompanyName"].ToString();
                string Site = Sheet7Data.Rows[0]["Branch"].ToString();
                // string Site = Params.Branch;
                string Count = Sheet7Data.Rows[0]["NumberofGenerate"].ToString();
                string AccountType = Sheet7Data.Rows[0]["AccountType"].ToString();
                if (AccountType == "Easy Savings")
                {
                    AccountType = "ES";
                }
                else
                {
                    AccountType = "AA";
                }

                filename = Companyname + "_" + Site + "_" + datebuild + "_" + AccountType + "_" + Count + ".xls";
                using (var fileStream = new FileStream(dataDir + filename, FileMode.Create, FileAccess.Write))
                //using (var fileStream = new FileStream(dataDir + filename, FileMode.Create, FileAccess.Write))
                {
                    stream.CopyTo(fileStream);
                }
                stream.Close();
                ExcelWorkbookFormat workbookFormat = ExcelWorkbookFormat.Xls_2003;
                string testDocFile = dataDir + "CRTTemplatev2.xls";
                ExcelWorkbook workbook = new ExcelWorkbook(testDocFile);
                ExcelWorksheet firstWorksheet = workbook.Worksheets[2];

                #region Sheet7

                ExcelWorksheet Sheet7 = workbook.Worksheets[3];
                //Sheet7.WorksheetSecurity.ProtectWorksheet("sbc01");

                //if (Sheet7Data.Rows.Count > 0)
                //{
                //    Sheet7["A" + 1].Text = Sheet7Data.Rows[0]["Company CIF Number"].ToString();
                //    Sheet7["B" + 1].Text = Sheet7Data.Rows[0]["Company EQ"].ToString();
                //    Sheet7["C" + 1].Text = Sheet7Data.Rows.Count.ToString();
                //    Sheet7["D" + 1].Text = Sheet7Data.Rows[0]["CompanyName"].ToString();
                //}

                int x = 2;
                for (int i = 0; i < Sheet7Data.Rows.Count; i++, x++)
                {
                    Sheet7["A" + x].Text = Sheet7Data.Rows[i]["LastName"].ToString();
                    Sheet7["B" + x].Text = Sheet7Data.Rows[i]["FirstName"].ToString();
                    Sheet7["C" + x].Text = Sheet7Data.Rows[i]["MiddleName"].ToString();
                    Sheet7["D" + x].Text = Sheet7Data.Rows[i]["Suffix"].ToString();
                    Sheet7["E" + x].Text = Sheet7Data.Rows[i]["FullName"].ToString();
                    Sheet7["F" + x].Text = Sheet7Data.Rows[i]["DateOfBirth"].ToString();
                    Sheet7["G" + x].Text = Sheet7Data.Rows[i]["PlaceOfBirth"].ToString();
                    Sheet7["H" + x].Text = Sheet7Data.Rows[i]["Nationality"].ToString();
                    Sheet7["I" + x].Text = Sheet7Data.Rows[i]["NationalityCountry"].ToString();
                    Sheet7["J" + x].Text = Sheet7Data.Rows[i]["Gender"].ToString();
                    Sheet7["K" + x].Text = Sheet7Data.Rows[i]["MobileNumber"].ToString();
                    Sheet7["L" + x].Text = Sheet7Data.Rows[i]["LandlineNumber"].ToString();
                    Sheet7["M" + x].Text = Sheet7Data.Rows[i]["HouseNumberPresent"].ToString();
                    Sheet7["N" + x].Text = Sheet7Data.Rows[i]["StreetVillageSubdivisionBrgyPresent"].ToString();
                    Sheet7["O" + x].Text = Sheet7Data.Rows[i]["CityMunicipality"].ToString();
                    Sheet7["P" + x].Text = Sheet7Data.Rows[i]["Region"].ToString();
                    Sheet7["Q" + x].Text = Sheet7Data.Rows[i]["Province"].ToString();
                    Sheet7["R" + x].Text = Sheet7Data.Rows[i]["Country"].ToString();
                    Sheet7["S" + x].Text = Sheet7Data.Rows[i]["ZipCode"].ToString();
                    Sheet7["T" + x].Text = Sheet7Data.Rows[i]["HouseNumberPermanent"].ToString();
                    Sheet7["U" + x].Text = Sheet7Data.Rows[i]["StreetVillageSubdivisionBrgyPermanent"].ToString();
                    Sheet7["V" + x].Text = Sheet7Data.Rows[i]["CityMunicipalityPermanent"].ToString();
                    Sheet7["W" + x].Text = Sheet7Data.Rows[i]["RegionPermanent"].ToString();
                    Sheet7["X" + x].Text = Sheet7Data.Rows[i]["ProvincePermanent"].ToString();
                    Sheet7["Y" + x].Text = Sheet7Data.Rows[i]["CountryPermanent"].ToString();
                    Sheet7["Z" + x].Text = Sheet7Data.Rows[i]["ZipCodePermanent"].ToString();
                    Sheet7["AA" + x].Text = Sheet7Data.Rows[i]["Email"].ToString();
                    Sheet7["AB" + x].Text = Sheet7Data.Rows[i]["CompanyName"].ToString();
                    Sheet7["AC" + x].Text = Sheet7Data.Rows[i]["EmployeeTIN"].ToString();
                    Sheet7["AD" + x].Text = Sheet7Data.Rows[i]["CivilStatus"].ToString();
                    Sheet7["AE" + x].Text = Sheet7Data.Rows[i]["MotherLastName"].ToString();
                    Sheet7["AF" + x].Text = Sheet7Data.Rows[i]["MotherFirstName"].ToString();
                    Sheet7["AG" + x].Text = Sheet7Data.Rows[i]["MotherMiddleName"].ToString();
                    Sheet7["AH" + x].Text = Sheet7Data.Rows[i]["SuffixMother"].ToString();
                    Sheet7["AI" + x].Text = Sheet7Data.Rows[i]["NumberOfPads"].ToString();
                    Sheet7["AJ" + x].Text = Sheet7Data.Rows[i]["EmployeeID"].ToString();
                }
                #endregion

                tempconnectionstring();
                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@StartingIDS", mytype = SqlDbType.Structured, Value = DTParam });
                con.myparameters.Add(new myParameters { ParameterName = "@CompanyAlias", mytype = SqlDbType.VarChar, Value = Params.CompanyAlias });
                con.myparameters.Add(new myParameters { ParameterName = "@Branch", mytype = SqlDbType.VarChar, Value = Params.Branch });
                DataTable EmpData = con.GetDataTable("sp_GetMainSheet");

                if (EmpData.Rows.Count > 0)
                {
                    firstWorksheet["C" + 3].Text = EmpData.Rows[0]["CompanyName"].ToString();//Company Name
                    //firstWorksheet["C" + 4].Text = EmpData.Rows[0]["DateofIncorporation"].ToString();//Date of Incorporation
                    //firstWorksheet["C" + 5].Text = EmpData.Rows[0]["CompleteCompanyAddress"].ToString();//Complete Company Address
                    //firstWorksheet["C" + 6].Text = EmpData.Rows[0]["CompanyNationality"].ToString();//Nationality of Company
                    //firstWorksheet["C" + 7].Text = EmpData.Rows[0]["ContactNumber"].ToString();//Contact Number
                    //firstWorksheet["C" + 8].Text = EmpData.Rows[0]["TIN"].ToString();//Company Tin
                    firstWorksheet["C" + 5].Text = EmpData.Rows[0]["CompanyEQNumber"].ToString();//Company EQ Customer Number
                    firstWorksheet["C" + 6].Text = EmpData.Rows.Count.ToString();//Total Number of Employees
                    //firstWorksheet["C" + 12].Text = EmpData.Rows[0]["AccountType"].ToString();//Account Type
                }

                int cellnum = 10;

                for (int i = 0; i < EmpData.Rows.Count; i++)
                {
                    firstWorksheet["A" + cellnum].Text = (i + 1).ToString();
                    firstWorksheet["B" + cellnum].Text = EmpData.Rows[i]["LastName"].ToString();
                    firstWorksheet["C" + cellnum].Text = EmpData.Rows[i]["FirstName"].ToString();
                    firstWorksheet["D" + cellnum].Text = EmpData.Rows[i]["MiddleName"].ToString();
                    firstWorksheet["E" + cellnum].Text = EmpData.Rows[i]["Suffix"].ToString();
                    firstWorksheet["F" + cellnum].Text = EmpData.Rows[i]["CardName"].ToString();

                    firstWorksheet["G" + cellnum].Text = EmpData.Rows[i]["EmployeeTIN"].ToString();

                    firstWorksheet["H" + cellnum].DateTimeValue = DateTime.Parse(EmpData.Rows[i]["Birthday"].ToString(), CultureInfo.InvariantCulture);
                    //firstWorksheet["G" + cellnum].DateTimeValue = DateTime.ParseExact(EmpData.Rows[i]["Birthday"].ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture) CultureInfo.CreateSpecificCulture("fr-FR"); 
                    firstWorksheet["I" + cellnum].Text = EmpData.Rows[i]["PlaceOfBirth"].ToString();
                    firstWorksheet["J" + cellnum].Text = EmpData.Rows[i]["Nationality"].ToString();
                    firstWorksheet["K" + cellnum].Text = EmpData.Rows[i]["NationalityCountry"].ToString();
                    firstWorksheet["L" + cellnum].Text = EmpData.Rows[i]["Gender"].ToString();
                    firstWorksheet["M" + cellnum].Text = EmpData.Rows[i]["MobileNumber"].ToString();
                    firstWorksheet["N" + cellnum].Text = EmpData.Rows[i]["LandlineNumber"].ToString();
                    firstWorksheet["O" + cellnum].Text = EmpData.Rows[i]["HouseNumber"].ToString();
                    firstWorksheet["P" + cellnum].Text = EmpData.Rows[i]["StreetVillageSubdivisionBrgyPresent"].ToString();
                    firstWorksheet["Q" + cellnum].Text = EmpData.Rows[i]["Country"].ToString();
                    firstWorksheet["R" + cellnum].Text = EmpData.Rows[i]["Region"].ToString();
                    firstWorksheet["S" + cellnum].Text = EmpData.Rows[i]["Province"].ToString();
                    firstWorksheet["T" + cellnum].Text = EmpData.Rows[i]["Municipality"].ToString();
                    firstWorksheet["U" + cellnum].Text = EmpData.Rows[i]["ZipCode"].ToString();
                    firstWorksheet["V" + cellnum].Text = EmpData.Rows[i]["HouseNumber2"].ToString();
                    firstWorksheet["W" + cellnum].Text = EmpData.Rows[i]["StreetVillageSubdivisionBrgyPermanent"].ToString();
                    firstWorksheet["X" + cellnum].Text = EmpData.Rows[i]["CountryPermanent"].ToString();
                    firstWorksheet["Y" + cellnum].Text = EmpData.Rows[i]["RegionPermanent"].ToString();
                    firstWorksheet["Z" + cellnum].Text = EmpData.Rows[i]["ProvincePermanent"].ToString();
                    firstWorksheet["AA" + cellnum].Text = EmpData.Rows[i]["MunicipalityPermanent"].ToString();
                    firstWorksheet["AB" + cellnum].Text = EmpData.Rows[i]["ZipcodePermanent"].ToString();
                    firstWorksheet["AC" + cellnum].Text = EmpData.Rows[i]["Email"].ToString();
                    firstWorksheet["AD" + cellnum].Text = EmpData.Rows[i]["EmployerName"].ToString();

                    firstWorksheet["AE" + cellnum].Text = EmpData.Rows[i]["CivilStatus"].ToString();
                    firstWorksheet["AF" + cellnum].Text = EmpData.Rows[i]["Last_Name"].ToString();
                    firstWorksheet["AG" + cellnum].Text = EmpData.Rows[i]["First_Name"].ToString();
                    firstWorksheet["AH" + cellnum].Text = EmpData.Rows[i]["Middle_Name"].ToString();
                    firstWorksheet["AI" + cellnum].Text = EmpData.Rows[i]["SuffixMother"].ToString();
                    firstWorksheet["AJ" + cellnum].Text = EmpData.Rows[i]["NumberOfPads"].ToString();
                    firstWorksheet["AK" + cellnum].Text = EmpData.Rows[i]["OtherDetails"].ToString();

                    cellnum++;
                }


                HttpResponse httpResponse = System.Web.HttpContext.Current.Response;

                httpResponse.Clear();
                httpResponse.ContentType = "Application/x-msexcel";
                httpResponse.AddHeader("Content-Disposition", String.Format("attachment; filename={0}", filename));
                string newDIR = HttpContext.Current.Server.MapPath("~/sFTP/");
                string onlineDIR = "http://ws.durusthr.com/ILM_WS_Live/sFTP/";
                //filename = filename.Replace(',', '_');
                //filename = filename.Replace(" ", String.Empty);
                workbook.Save(newDIR + filename);
                stream.Close();
                //return "Success";

                if (Sheet7Data.Rows.Count > 0)
                {
                    return onlineDIR + filename;
                }
                else
                {
                    return "No Rows";
                }
            }
            catch (Exception e)
            {
                stream.Close();
                return "Error: " + e.ToString();
            }
        }

        #region Dashboard
        [HttpPost]
        [Route("DashboardCount")]
        public GetDashboardCount DashboardCount(DashboardCount DashboardCountWall)
        {
            GetDashboardCount GetDashboardCount = new GetDashboardCount();

            tempconnectionstring();
            Connection Connection = new Connection();
            //Connection.myparameters.Add(new myParameters { ParameterName = "@ZipCode", mytype = SqlDbType.NVarChar, Value = MobileZipSearch.ZipCode });
            Connection.myparameters.Add(new myParameters { ParameterName = "@CN", mytype = SqlDbType.NVarChar, Value = DashboardCountWall.CN });

            DataTable DT = new DataTable();
            DT = Connection.GetDataTable("sp_DashboardCount");
            List<DashboardCount> ListReturned = new List<DashboardCount>();
            if (DT.Rows.Count > 0)
            {
                foreach (DataRow row in DT.Rows)
                {
                    DashboardCount Displaynewusers = new DashboardCount
                    {
                        TotalUpload = row["TotalUpload"].ToString(),
                        CompleteDataEntry = row["CompleteDataEntry"].ToString(),
                        IncompleteDataEntry = row["IncompleteDataEntry"].ToString(),
                        CRTGenerated = row["CRTGenerated"].ToString(),
                        RoundedComplete = row["RoundedComplete"].ToString(),
                        RoundedIncomplete = row["RoundedIncomplete"].ToString()
                    };
                    ListReturned.Add(Displaynewusers);
                }
                GetDashboardCount.DisplayTotalCount = ListReturned;
                //GetDashboardCount.myreturn = "Success";
            }
            return GetDashboardCount;
        }
        [HttpPost]
        [Route("GetAllClientDashboard")]
        public GetDashboardClientDetails GetAllClientDashboard(InsertClientDatav2 admin)
        {
            GetDashboardClientDetails GetClientDetails = new GetDashboardClientDetails();
            List<DashboardClientDetails> ClientDetails = new List<DashboardClientDetails>();
            tempconnectionstring();
            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@CN", mytype = SqlDbType.NVarChar, Value = admin.CN });
            //Connection.myparameters.Add(new myParameters { ParameterName = "@CClientID", mytype = SqlDbType.NVarChar, Value = admin.CClientID });
            DataTable dtClientList = new DataTable();
            dtClientList = Connection.GetDataTable("sp_DashboardSiteList");
            if (dtClientList.Rows.Count > 0)
            {
                foreach (DataRow row in dtClientList.Rows)
                {
                    DashboardClientDetails Clientdetail = new DashboardClientDetails
                    {
                        CompanyName = row["CompanyName"].ToString(),
                        Branch = row["Branch"].ToString(),
                        Complete = row["Complete"].ToString(),
                        Incomplete = row["Incomplete"].ToString()
                    };
                    ClientDetails.Add(Clientdetail);
                }
                GetClientDetails.Clientdetails = ClientDetails;
                GetClientDetails.myreturn = "Success";
            }
            else
            {
                GetClientDetails.myreturn = "Error";
            }
            return GetClientDetails;
        }
        [HttpPost]
        [Route("ExtractDataDashboard")]
        public string ExtractEmployeeDataDashboard(CRTDashboardGeneration Params)

        {
            FileStream stream = null;
            try
            {
                tempconnectionstring();
                Connection con = new Connection();

                con.myparameters.Add(new myParameters { ParameterName = "@CN", mytype = SqlDbType.VarChar, Value = Params.CN });
                con.myparameters.Add(new myParameters { ParameterName = "@Site", mytype = SqlDbType.VarChar, Value = Params.Site });
                con.myparameters.Add(new myParameters { ParameterName = "@result", mytype = SqlDbType.VarChar, Value = Params.Result });
                DataTable EmpData = con.GetDataTable("sp_DashboardGenerate");
                //Wala pang naming convention
                string dataDir = HostingEnvironment.MapPath("~/Files/");

                stream = new FileStream(dataDir + "EmployeeListDashboard.xls", FileMode.Open);
                string filename;
                string typeoflist = Params.Result.ToString();
                string datebuild = DateTime.Now.ToString("MMddyyyy");
                //string timebuild = DateTime.Now.ToString("hh-mmtt");


                filename = "Employee_List" + "_" + typeoflist + "_" + datebuild + /*"_" + timebuild +*/ ".xls";
                using (var fileStream = new FileStream(dataDir + filename, FileMode.Create, FileAccess.Write))
                {
                    stream.CopyTo(fileStream);
                }

                ExcelWorkbookFormat workbookFormat = ExcelWorkbookFormat.Xls_2003;
                string testDocFile = dataDir + filename;
                ExcelWorkbook workbook = new ExcelWorkbook(testDocFile);
                ExcelWorksheet firstWorksheet = workbook.Worksheets[0];

                int cellnum = 2;

                for (int i = 0; i < EmpData.Rows.Count; i++)
                {
                    firstWorksheet["A" + cellnum].Text = EmpData.Rows[i]["StartingID"].ToString();
                    firstWorksheet["B" + cellnum].Text = EmpData.Rows[i]["LastName"].ToString();
                    firstWorksheet["C" + cellnum].Text = EmpData.Rows[i]["FirstName"].ToString();
                    //firstWorksheet["C" + cellnum].Text = EmpData.Rows[i]["MiddleName"].ToString();
                    cellnum++;
                }


                HttpResponse httpResponse = System.Web.HttpContext.Current.Response;

                httpResponse.Clear();
                httpResponse.ContentType = "Application/x-msexcel";
                httpResponse.AddHeader("Content-Disposition", String.Format("attachment; filename={0}", filename));
                string newDIR = HttpContext.Current.Server.MapPath("~/sFTP/");
                string onlineDIR = "http://ws.durusthr.com/ILM_WS_Live/sFTP/";
                //filename = filename.Replace(',', '_');
                //filename = filename.Replace(" ", String.Empty);
                workbook.Save(newDIR + filename);
                stream.Close();
                //return "Success";
                return onlineDIR + filename;
            }
            catch (Exception e)
            {
                stream.Close();
                return e.ToString();
            }
        }
        #endregion

        [HttpPost]
        [Route("SendCRTUpdate")]
        public string SendCRTUpdate()
        {
            FileStream stream = null;
            try
            {
                tempconnectionstring();
                Connection con = new Connection();
                DataTable EmpData = con.GetDataTable("sp_CRT_CheckEmptyFields");

                string dataDir = HostingEnvironment.MapPath("~/Files/");

                stream = new FileStream(dataDir + "CRTReportUpdate.xls", FileMode.Open);
                string filename;
                string datebuild = DateTime.Now.ToString("MMddyyyy");
                string timebuild = DateTime.Now.ToString("hh-mmtt");
                filename = "CRT_ReportUpdate" + "_" + datebuild + "_" + timebuild + ".xls";
                //filename = "Employee_List.xls";
                using (var fileStream = new FileStream(dataDir + filename, FileMode.Create, FileAccess.Write))
                {
                    stream.CopyTo(fileStream);
                }

                ExcelWorkbookFormat workbookFormat = ExcelWorkbookFormat.Xls_2003;
                string testDocFile = dataDir + filename;
                ExcelWorkbook workbook = new ExcelWorkbook(testDocFile);
                ExcelWorksheet firstWorksheet = workbook.Worksheets[0];

                int cellnum = 2;

                for (int i = 0; i < EmpData.Rows.Count; i++)
                {
                    firstWorksheet["A" + cellnum].Text = EmpData.Rows[i]["CompanyName"].ToString();
                    firstWorksheet["B" + cellnum].Text = EmpData.Rows[i]["StartingID"].ToString();
                    firstWorksheet["C" + cellnum].Text = EmpData.Rows[i]["Gender"].ToString();
                    cellnum++;
                }
                HttpResponse httpResponse = System.Web.HttpContext.Current.Response;
                httpResponse.Clear();
                httpResponse.ContentType = "Application/x-msexcel";
                httpResponse.AddHeader("Content-Disposition", String.Format("attachment; filename={0}", filename));
                string newDIR = HttpContext.Current.Server.MapPath("~/sFTP/");
                string onlineDIR = "http://ws.durusthr.com/ILM_WS_Live/sFTP/";
                workbook.Save(newDIR + filename);
                stream.Close();
                string emailaccount = "durusthr@illimitado.com";
                string emailpassword = "@1230Qwerty";
                string prefix = HttpContext.Current.Server.MapPath("~/sFTP/");
                using (MailMessage mm = new MailMessage(emailaccount, "jsangco@illimitado.com"))
                {
                    MailAddress aliasmail = new MailAddress("durusthr@illimitado.com", "durusthr@illimitado.com");
                    MailAddress aliasreplymail = new MailAddress("durusthr@illimitado.com");
                    mm.From = aliasmail;
                    mm.Subject = "Notifications";
                    mm.Body = "";
                    mm.Attachments.Add(new Attachment(prefix + filename));
                    //mm.Attachments.Add(new Attachment(newDIR + filename));
                    mm.IsBodyHtml = false;
                    SmtpClient smtp = new SmtpClient();
                    smtp.Host = "box1256.bluehost.com";
                    smtp.EnableSsl = true;
                    NetworkCredential NetworkCred = new NetworkCredential(emailaccount, emailpassword);
                    smtp.UseDefaultCredentials = true;
                    smtp.Credentials = NetworkCred;
                    smtp.Port = 587;
                    smtp.Send(mm);
                }
                using (MailMessage mm = new MailMessage(emailaccount, "eya@illimitado.com"))
                {
                    MailAddress aliasmail = new MailAddress("durusthr@illimitado.com", "durusthr@illimitado.com");
                    MailAddress aliasreplymail = new MailAddress("durusthr@illimitado.com");
                    mm.From = aliasmail;
                    mm.Subject = "Notifications";
                    mm.Body = "";
                    mm.Attachments.Add(new Attachment(prefix + filename));
                    //mm.Attachments.Add(new Attachment(newDIR + filename));
                    mm.IsBodyHtml = false;
                    SmtpClient smtp = new SmtpClient();
                    smtp.Host = "box1256.bluehost.com";
                    smtp.EnableSsl = true;
                    NetworkCredential NetworkCred = new NetworkCredential(emailaccount, emailpassword);
                    smtp.UseDefaultCredentials = true;
                    smtp.Credentials = NetworkCred;
                    smtp.Port = 587;
                    smtp.Send(mm);
                }
                using (MailMessage mm = new MailMessage(emailaccount, "polo@illimitado.com"))
                {
                    MailAddress aliasmail = new MailAddress("durusthr@illimitado.com", "durusthr@illimitado.com");
                    MailAddress aliasreplymail = new MailAddress("durusthr@illimitado.com");
                    mm.From = aliasmail;
                    mm.Subject = "Notifications";
                    mm.Body = "";
                    mm.Attachments.Add(new Attachment(prefix + filename));
                    //mm.Attachments.Add(new Attachment(newDIR + filename));
                    mm.IsBodyHtml = false;
                    SmtpClient smtp = new SmtpClient();
                    smtp.Host = "box1256.bluehost.com";
                    smtp.EnableSsl = true;
                    NetworkCredential NetworkCred = new NetworkCredential(emailaccount, emailpassword);
                    smtp.UseDefaultCredentials = true;
                    smtp.Credentials = NetworkCred;
                    smtp.Port = 587;
                    smtp.Send(mm);
                }
                return "Success";
            }
            catch (Exception e)
            {
                stream.Close();
                return e.ToString();
            }
        }
        [HttpPost]
        [Route("CRTCheckMobileNum")]
        public string CheckMobileNum(CRTCheckMobNum param)
        {
            try
            {
                tempconnectionstring();
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@companyalias", mytype = SqlDbType.VarChar, Value = param.CompanyAlias });
                con.myparameters.Add(new myParameters { ParameterName = "@mobilenumber", mytype = SqlDbType.VarChar, Value = param.ContactNumber });
                con.myparameters.Add(new myParameters { ParameterName = "@employeeid", mytype = SqlDbType.VarChar, Value = param.EmployeeID });
                return con.ExecuteScalar("sp_CRT_CheckMobileNum").ToString();
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }
        [HttpPost]
        [Route("CRTUpdateTnC")]
        public string UpdateTermsandConditions(CRTUpdateTnC param)
        {
            try
            {
                tempconnectionstring();
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@companyalias", mytype = SqlDbType.VarChar, Value = param.CompanyAlias });
                con.myparameters.Add(new myParameters { ParameterName = "@employeeid", mytype = SqlDbType.VarChar, Value = param.EmployeeID });
                con.myparameters.Add(new myParameters { ParameterName = "@tnc", mytype = SqlDbType.VarChar, Value = param.TNC });
                con.ExecuteNonQuery("sp_CRT_UpdateTnC");
                return "Success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }


        //9/18/2019 START
        [HttpPost]
        [Route("sp_CRT_UserSearch")]
        public ClassGetCRTUser CRT_UserSearch(SP_Param login)
        {

            ClassGetCRTUser GetDisplaySearchedUsers = new ClassGetCRTUser();
            List<SP_Returns> UserDetails = new List<SP_Returns>();
            tempconnectionstring();
            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@site", mytype = SqlDbType.NVarChar, Value = login.site });
            Connection.myparameters.Add(new myParameters { ParameterName = "@id", mytype = SqlDbType.NVarChar, Value = login.id });
            Connection.myparameters.Add(new myParameters { ParameterName = "@company", mytype = SqlDbType.NVarChar, Value = login.company });
            DataTable dtuserList = new DataTable();
            dtuserList = Connection.GetDataTable("sp_CRT_UserSearch");
            if (dtuserList.Rows.Count > 0)
            {
                foreach (DataRow row in dtuserList.Rows)
                {
                    SP_Returns DisplaySearchResult = new SP_Returns
                    {
                        id = row["id"].ToString(),
                        fn = row["fn"].ToString(),
                        mn = row["mn"].ToString(),
                        ln = row["ln"].ToString(),
                        suffix = row["suffix"].ToString(),
                        dob = row["dob"].ToString(),
                        site = row["site"].ToString(),
                        acctype = row["acctype"].ToString(),
                        empTIN = row["empTIN"].ToString(),

                    };
                    UserDetails.Add(DisplaySearchResult);
                }
                GetDisplaySearchedUsers.DisplaySearchResult = UserDetails;
                GetDisplaySearchedUsers.myreturn = "Success";
            }
            else
            {
                GetDisplaySearchedUsers.myreturn = "Error";
            }
            return GetDisplaySearchedUsers;
        }

        [HttpPost]
        [Route("sp_CRT_getSite")]
        public ClassGetSite CRT_getSite(SP_Param_getSite param)
        {

            ClassGetSite GetSiteAllocation = new ClassGetSite();
            List<SP_CRT_getSite_Returns> returnSite = new List<SP_CRT_getSite_Returns>();
            tempconnectionstring();
            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@company", mytype = SqlDbType.NVarChar, Value = param.company });
            DataTable dtuserList = new DataTable();
            dtuserList = Connection.GetDataTable("sp_CRT_getSite");
            if (dtuserList.Rows.Count > 0)
            {
                foreach (DataRow row in dtuserList.Rows)
                {
                    SP_CRT_getSite_Returns displaySite = new SP_CRT_getSite_Returns
                    {
                        site = row["site"].ToString()
                    };
                    returnSite.Add(displaySite);
                }
                GetSiteAllocation.returnSite = returnSite;
                GetSiteAllocation.myreturn = "Success";
            }
            else
            {
                GetSiteAllocation.myreturn = "Error";
            }
            return GetSiteAllocation;
        }

        [HttpPost]
        [Route("sp_CRT_getEmployee")]
        public ClassGetEmp CRT_getEmp(SP_Param_getSite param)
        {

            ClassGetEmp GetEmployeeDetails = new ClassGetEmp();
            List<SP_CRT_getEmp_Returns> returnEmployee = new List<SP_CRT_getEmp_Returns>();
            tempconnectionstring();
            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@company", mytype = SqlDbType.NVarChar, Value = param.company });
            DataTable dtuserList = new DataTable();
            dtuserList = Connection.GetDataTable("sp_CRT_getEmployee");
            if (dtuserList.Rows.Count > 0)
            {
                foreach (DataRow row in dtuserList.Rows)
                {
                    SP_CRT_getEmp_Returns displayEmp = new SP_CRT_getEmp_Returns
                    {
                        id = row["id"].ToString(),
                        empname = row["empname"].ToString()
                    };
                    returnEmployee.Add(displayEmp);
                }
                GetEmployeeDetails.returnEmp = returnEmployee;
                GetEmployeeDetails.myreturn = "Success";
            }
            else
            {
                GetEmployeeDetails.myreturn = "Error";
            }
            return GetEmployeeDetails;
        }
        [HttpPost]
        [Route("sp_CRT_GetCompanyName")]
        public ClassGetCompanyName CRT_getClientName(SP_Param_getClient param)
        {

            ClassGetCompanyName GetCompanyName = new ClassGetCompanyName();
            List<SP_CRT_getCompanyName_Returns> returnCN = new List<SP_CRT_getCompanyName_Returns>();
            tempconnectionstring();
            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@clientID", mytype = SqlDbType.NVarChar, Value = param.clientID });
            DataTable dtuserList = new DataTable();
            dtuserList = Connection.GetDataTable("sp_CRT_GetCompanyName");
            if (dtuserList.Rows.Count > 0)
            {
                foreach (DataRow row in dtuserList.Rows)
                {
                    SP_CRT_getCompanyName_Returns displayEmp = new SP_CRT_getCompanyName_Returns
                    {
                        CompanyName = row["CompanyName"].ToString()
                    };
                    returnCN.Add(displayEmp);
                }
                GetCompanyName.returnCN = returnCN;
                GetCompanyName.myreturn = "Success";
            }
            else
            {
                GetCompanyName.myreturn = "Error";
            }
            return GetCompanyName;
        }
        //9/18/2019 END

        //NEW SP 9/19/2019
        [HttpPost]
        [Route("GetEmpExclusion")]
        public ClassGetExclusion CRT_getEmpExclusion(param_GetExclusion param)
        {

            ClassGetExclusion GetEmployeeID = new ClassGetExclusion();
            List<GetExclusionReturns> returnEmpID = new List<GetExclusionReturns>();
            tempconnectionstring();
            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@CompanyName", mytype = SqlDbType.NVarChar, Value = param.CompanyName });
            DataTable dtuserList = new DataTable();
            dtuserList = Connection.GetDataTable("getEmployeeForExclusion");
            if (dtuserList.Rows.Count > 0)
            {
                foreach (DataRow row in dtuserList.Rows)
                {
                    GetExclusionReturns displayEmp = new GetExclusionReturns
                    {
                        EmployeeID = row["EmployeeID"].ToString()
                    };
                    returnEmpID.Add(displayEmp);
                }
                GetEmployeeID.returnEmployeeID = returnEmpID;
                GetEmployeeID.myreturn = "Success";
            }
            else
            {
                GetEmployeeID.myreturn = "Error";
            }
            return GetEmployeeID;
        }
        //NEW END 9/19/2019

        // UpdateTableTest
        [HttpPost]
        [Route("CRT_UpdateExclusion")]
        public List<UpdateExc> EmpExclusion(ExclusionParams UserExc)
        {
            DataTable UserExclusion = new DataTable();
            UserExclusion.Columns.AddRange(new DataColumn[2] { new DataColumn("EmployeeID"), new DataColumn("Exclusion Type") });
            for (int i = 0; i < UserExc.EmployeeID.Length; i++)
            {
                UserExclusion.Rows.Add(UserExc.EmployeeID[i], UserExc.ExclusionType[i]);
            }
            List<UpdateExc> DU = new List<UpdateExc>();
            tempconnectionstring();
            Connection con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@company", mytype = SqlDbType.VarChar, Value = UserExc.company });
            con.myparameters.Add(new myParameters { ParameterName = "@IMPORT", mytype = SqlDbType.Structured, Value = UserExclusion });
            DataTable DT = con.GetDataTable("sp_CRT_UpdateExclusion");
            foreach (DataRow row in DT.Rows)
            {
                UpdateExc Users = new UpdateExc()
                {
                    RowID = row["RowID"].ToString(),
                    EmployeeID = row["EmployeeID"].ToString(),
                    ExclusionType = row["ExclusionType"].ToString()
                };
                DU.Add(Users);
            }
            return DU;
        }
        //9/20/2019

        //09/23/2019
        // SearchFilter
        [HttpPost]
        [Route("CRT_FilterUser")]
        public List<FilteredUsers> NewSearchFilter(SearchFilterParams searchUser)
        {

            DataTable filterUser1 = new DataTable();
            DataTable filterUser = new DataTable();

            filterUser1.Columns.AddRange(new DataColumn[1] { new DataColumn("site") });
            filterUser.Columns.AddRange(new DataColumn[1] { new DataColumn("empID") });
            for (int i = 0; i < searchUser.empID.Length; i++)
            {
                filterUser.Rows.Add(searchUser.empID[i]);
            }

            for (int i = 0; i < searchUser.site.Length; i++)
            {
                filterUser1.Rows.Add(searchUser.site[i]);
            }


            List<FilteredUsers> FU = new List<FilteredUsers>();
            tempconnectionstring();
            Connection con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@company", mytype = SqlDbType.VarChar, Value = searchUser.company });
            con.myparameters.Add(new myParameters { ParameterName = "@site", mytype = SqlDbType.Structured, Value = filterUser1 });
            con.myparameters.Add(new myParameters { ParameterName = "@ID", mytype = SqlDbType.Structured, Value = filterUser });
            DataTable DT = con.GetDataTable("sp_CRT_UserSearch");
            foreach (DataRow row in DT.Rows)
            {
                FilteredUsers Users = new FilteredUsers()
                {
                    StartingID = row["id"].ToString(),
                    FirstName = row["fn"].ToString(),
                    MiddleName = row["mn"].ToString(),
                    LastName = row["ln"].ToString(),
                    Suffix = row["suffix"].ToString(),
                    DateHired = row["dob"].ToString(),
                    Branch = row["site"].ToString(),
                    AccountType = row["acctype"].ToString(),
                    EmployeeTIN = row["empTIN"].ToString()
                };
                FU.Add(Users);
            }
            return FU;
        }

        //09/23/2019

        //REN 09/25/2019
        [HttpPost]
        [Route("CRT_RemoveExclusion")]
        public List<UpdateExc> RemoveExclusion(ExclusionParams UserExc)
        {
            DataTable UserExclusion = new DataTable();
            UserExclusion.Columns.AddRange(new DataColumn[2] { new DataColumn("EmployeeID"), new DataColumn("Exclusion Type") });
            for (int i = 0; i < UserExc.EmployeeID.Length; i++)
            {
                UserExclusion.Rows.Add(UserExc.EmployeeID[i], UserExc.ExclusionType[i]);
            }
            List<UpdateExc> DU = new List<UpdateExc>();
            tempconnectionstring();
            Connection con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@company", mytype = SqlDbType.VarChar, Value = UserExc.company });
            con.myparameters.Add(new myParameters { ParameterName = "@IMPORT", mytype = SqlDbType.Structured, Value = UserExclusion });
            DataTable DT = con.GetDataTable("sp_CRT_RemoveExclusion");
            foreach (DataRow row in DT.Rows)
            {
                UpdateExc Users = new UpdateExc()
                {
                    RowID = row["RowID"].ToString(),
                    EmployeeID = row["EmployeeID"].ToString(),
                    ExclusionType = row["ExclusionType"].ToString()
                };
                DU.Add(Users);
            }
            return DU;
        }

        [HttpPost]
        [Route("CRT_Get_AutoLogin")]
        public List<getList_AutoLogin> listLogin(autoLogin user)
        {



            List<getList_AutoLogin> DU = new List<getList_AutoLogin>();
            tempconnectionstring();
            Connection con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@id", mytype = SqlDbType.VarChar, Value = user.id });

            DataTable DT = con.GetDataTable("sp_auto_login_Get");
            foreach (DataRow row in DT.Rows)
            {
                getList_AutoLogin Users = new getList_AutoLogin()
                {
                    company = row["company"].ToString(),
                    username = row["username"].ToString(),
                    password = row["password"].ToString(),

                };
                DU.Add(Users);
            }
            return DU;
        }
        //end

        //Exclusion FIlter added by REN
        [HttpPost]
        [Route("CRT_ExclusionFilter")]
        public List<FilterExclusionReturn> FilterExclusion(ExclusionFilterParams filterExclusion)
        {

            DataTable filterUser1 = new DataTable();
            DataTable filterUser = new DataTable();

            filterUser1.Columns.AddRange(new DataColumn[1] { new DataColumn("exclusionType") });
            filterUser.Columns.AddRange(new DataColumn[1] { new DataColumn("empID") });
            for (int i = 0; i < filterExclusion.EmployeeID.Length; i++)
            {
                filterUser.Rows.Add(filterExclusion.EmployeeID[i]);
            }

            for (int i = 0; i < filterExclusion.ExclusionType.Length; i++)
            {
                filterUser1.Rows.Add(filterExclusion.ExclusionType[i]);
            }

            List<FilterExclusionReturn> DU = new List<FilterExclusionReturn>();
            tempconnectionstring();
            Connection con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@company", mytype = SqlDbType.VarChar, Value = filterExclusion.company });
            con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.Structured, Value = filterUser });
            con.myparameters.Add(new myParameters { ParameterName = "@exclusionType", mytype = SqlDbType.Structured, Value = filterUser1 });
            DataTable DT = con.GetDataTable("sp_CRT_ExclusionFilter");
            foreach (DataRow row in DT.Rows)
            {
                FilterExclusionReturn Users = new FilterExclusionReturn()
                {
                    EmployeeID = row["EmployeeID"].ToString(),
                    CompanyAlias = row["CompanyName"].ToString(),
                    OpenedAccount = row["ExclusionType"].ToString(),

                };
                DU.Add(Users);
            }
            return DU;
        }

        //Added by REN  9/27/2019 Delete Exclusion
        [HttpPost]
        [Route("CRT_EmpDeleteExclusion")]
        public List<DeleteExc> EmpDeleteExclusion(EmpDeleteExclusionParams DeleteUserExc)
        {

            DataTable DeleteExclusion = new DataTable();


            DeleteExclusion.Columns.AddRange(new DataColumn[1] { new DataColumn("ExclusionType") });

            for (int i = 0; i < DeleteUserExc.ExclusionType.Length; i++)
            {
                DeleteExclusion.Rows.Add(DeleteUserExc.ExclusionType[i]);
            }

            List<DeleteExc> DU = new List<DeleteExc>();
            tempconnectionstring();
            Connection con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = DeleteUserExc.empID });
            con.myparameters.Add(new myParameters { ParameterName = "@company", mytype = SqlDbType.VarChar, Value = DeleteUserExc.company });
            con.myparameters.Add(new myParameters { ParameterName = "@IMPORT", mytype = SqlDbType.Structured, Value = DeleteExclusion });
            DataTable DT = con.GetDataTable("sp_CRT_DeleteExclusion");
            foreach (DataRow row in DT.Rows)
            {
                DeleteExc Users = new DeleteExc()
                {
                    ExclusionType = row["ExclusionType"].ToString()
                };
                DU.Add(Users);
            }
            return DU;
        }
        //ADDED BY REN
        //Extract exclusion param ID

        [HttpPost]
        [Route("CRT_ExclusionExtractID")]
        public string ExtractEmployeeData(CRTExclusionExtractID Params)
        {
            DataTable EmpIdDataTable = new DataTable();


            EmpIdDataTable.Columns.AddRange(new DataColumn[1] { new DataColumn("EmployeeID") });

            for (int i = 0; i < Params.EmpID.Length; i++)
            {
                EmpIdDataTable.Rows.Add(Params.EmpID[i]);
            }

            FileStream stream = null;
            try
            {
                tempconnectionstring();
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@company", mytype = SqlDbType.VarChar, Value = Params.company });
                con.myparameters.Add(new myParameters { ParameterName = "@IMPORT", mytype = SqlDbType.Structured, Value = EmpIdDataTable });
                DataTable ExclusionData = con.GetDataTable("sp_CRT_ExtractExclusionID");
                ////Naming Convention is "Company Name_Site_Date_Count".Count means pang ilan file siya today. 
                string dataDir = HostingEnvironment.MapPath("~/Files/");

                stream = new FileStream(dataDir + "Exclusion.xls", FileMode.Open);
                string filename;
                string datebuild = DateTime.Now.ToString("MMddyyyy");
                string timebuild = DateTime.Now.ToString("hh-mmtt");


                filename = Params.company + "_Exclusion_" + datebuild + "_" + timebuild + ".xls";

                using (var fileStream = new FileStream(dataDir + filename, FileMode.Create, FileAccess.Write))
                {
                    stream.CopyTo(fileStream);
                }

                ExcelWorkbookFormat workbookFormat = ExcelWorkbookFormat.Xls_2003;
                string testDocFile = dataDir + filename;
                ExcelWorkbook workbook = new ExcelWorkbook(testDocFile);
                ExcelWorksheet firstWorksheet = workbook.Worksheets[0];

                int cellnum = 2;

                for (int i = 0; i < ExclusionData.Rows.Count; i++)
                {
                    firstWorksheet["A" + cellnum].Text = ExclusionData.Rows[i]["CompanyName"].ToString();
                    firstWorksheet["B" + cellnum].Text = ExclusionData.Rows[i]["EmployeeID"].ToString();
                    firstWorksheet["C" + cellnum].Text = ExclusionData.Rows[i]["ExclusionType"].ToString();
                    cellnum++;
                }


                HttpResponse httpResponse = System.Web.HttpContext.Current.Response;

                httpResponse.Clear();
                httpResponse.ContentType = "Application/x-msexcel";
                httpResponse.AddHeader("Content-Disposition", String.Format("attachment; filename={0}", filename));
                string newDIR = HttpContext.Current.Server.MapPath("~/sFTP/");
                string onlineDIR = "http://ws.durusthr.com/ILM_WS_Live/sFTP/";
                filename = filename.Replace(',', '_');
                filename = filename.Replace(" ", String.Empty);
                workbook.Save(newDIR + filename);
                stream.Close();

                return onlineDIR + filename;
            }
            catch (Exception e)
            {
                stream.Close();
                return e.ToString();
            }
        }

        //27/09/2019


        //Extract exclusion param ID

        //Extract exclusion
        [HttpPost]
        [Route("CRT_ExclusionExtract")]
        public string ExtractExclusionData(CRTExclusionExtract Params)

        {
            FileStream stream = null;
            try
            {
                tempconnectionstring();
                Connection con = new Connection();

                con.myparameters.Add(new myParameters { ParameterName = "@company", mytype = SqlDbType.VarChar, Value = Params.company });
                DataTable ExclusionData = con.GetDataTable("sp_CRT_ExtractExclusion");
                ////Naming Convention is "Company Name_Site_Date_Count".Count means pang ilan file siya today. 
                string dataDir = HostingEnvironment.MapPath("~/Files/");

                stream = new FileStream(dataDir + "Exclusion.xls", FileMode.Open);
                string filename;
                string datebuild = DateTime.Now.ToString("MMddyyyy");
                string timebuild = DateTime.Now.ToString("hh-mmtt");


                filename = Params.company + "_Exclusion_" + datebuild + "_" + timebuild + ".xls";

                using (var fileStream = new FileStream(dataDir + filename, FileMode.Create, FileAccess.Write))
                {
                    stream.CopyTo(fileStream);
                }

                ExcelWorkbookFormat workbookFormat = ExcelWorkbookFormat.Xls_2003;
                string testDocFile = dataDir + filename;
                ExcelWorkbook workbook = new ExcelWorkbook(testDocFile);
                ExcelWorksheet firstWorksheet = workbook.Worksheets[0];

                int cellnum = 2;

                for (int i = 0; i < ExclusionData.Rows.Count; i++)
                {
                    firstWorksheet["A" + cellnum].Text = ExclusionData.Rows[i]["CompanyName"].ToString();
                    firstWorksheet["B" + cellnum].Text = ExclusionData.Rows[i]["EmployeeID"].ToString();
                    firstWorksheet["C" + cellnum].Text = ExclusionData.Rows[i]["ExclusionType"].ToString();
                    cellnum++;
                }



                HttpResponse httpResponse = System.Web.HttpContext.Current.Response;

                httpResponse.Clear();
                httpResponse.ContentType = "Application/x-msexcel";
                httpResponse.AddHeader("Content-Disposition", String.Format("attachment; filename={0}", filename));
                string newDIR = HttpContext.Current.Server.MapPath("~/sFTP/");
                string onlineDIR = "http://ws.durusthr.com/ILM_WS_Live/sFTP/";
                filename = filename.Replace(',', '_');
                filename = filename.Replace(" ", String.Empty);
                workbook.Save(newDIR + filename);
                stream.Close();

                return onlineDIR + filename;
            }
            catch (Exception e)
            {
                stream.Close();
                return e.ToString();
            }
        }

        //EXTRACT Exclusion



        [HttpPost]
        [Route("GenerateCRTEmployee")]
        public string GenerateCRTEmployee(CRTGenerationParams Params)
        {
            FileStream stream = null;
            try
            {

                tempconnectionstring();
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@CompanyAlias", mytype = SqlDbType.VarChar, Value = Params.CompanyAlias });
                DataTable Sheet7Data = con.GetDataTable("sp_GetEnrollmentSheet");

                //Naming Convention is "Company Name_Site_Date_Count". Count means pang ilan file siya today. 
                string dataDir = HostingEnvironment.MapPath("~/Files/");

                stream = new FileStream(dataDir + "Enrollment Template.xls", FileMode.Open);
                string filename;
                string datebuild = DateTime.Now.ToString("MMddyyyy");
                string timebuild = DateTime.Now.ToString("hh-mmtt");

                //string Companyname = Sheet7Data.Rows[0]["CompanyName"].ToString();
                string Companyname = "Sample Company";
                // string Site = Sheet7Data.Rows[0]["Branch"].ToString();
                // string Site = Params.Branch;
                // string Count = Sheet7Data.Rows[0]["NumberofGenerate"].ToString();
                // string AccountType = Sheet7Data.Rows[0]["AccountType"].ToString();
                // if (AccountType == "Easy Savings")
                // {
                //     AccountType = "ES";
                // }
                // else
                // {
                //     AccountType = "AA";
                // }

                //filename = Companyname + "_" + Site + "_" + datebuild + "_" + AccountType + "_" + Count + ".xls";
                filename = Companyname + "_" + datebuild + "_" + timebuild + ".xls";
                using (var fileStream = new FileStream(dataDir + filename, FileMode.Create, FileAccess.Write))
                //using (var fileStream = new FileStream(dataDir + filename, FileMode.Create, FileAccess.Write))
                {
                    stream.CopyTo(fileStream);
                }
                stream.Close();
                ExcelWorkbookFormat workbookFormat = ExcelWorkbookFormat.Xls_2003;
                string testDocFile = dataDir + "Enrollment Template.xls";
                ExcelWorkbook workbook = new ExcelWorkbook(testDocFile);
                ExcelWorksheet firstWorksheet = workbook.Worksheets[0];

                #region Sheet7

                ExcelWorksheet Sheet7 = workbook.Worksheets[1];

                int x = 11;
                for (int i = 0; i < Sheet7Data.Rows.Count; i++, x++)
                {
                    Sheet7["F" + x].Text = Sheet7Data.Rows[i]["Last Name"].ToString();
                    Sheet7["G" + x].Text = Sheet7Data.Rows[i]["First Name"].ToString();
                    Sheet7["H" + x].Text = Sheet7Data.Rows[i]["Middle Initial"].ToString();
                    Sheet7["I" + x].Text = Sheet7Data.Rows[i]["Birthdate"].ToString();
                    Sheet7["O" + x].Text = Sheet7Data.Rows[i]["Gender"].ToString();
                }

                #endregion

                HttpResponse httpResponse = System.Web.HttpContext.Current.Response;

                httpResponse.Clear();
                httpResponse.ContentType = "Application/x-msexcel";
                httpResponse.AddHeader("Content-Disposition", String.Format("attachment; filename={0}", filename));
                string newDIR = HttpContext.Current.Server.MapPath("~/sFTP/");
                string onlineDIR = "http://ws.durusthr.com/ILM_WS_Live/sFTP/";
                //filename = filename.Replace(',', '_');
                //filename = filename.Replace(" ", String.Empty);
                workbook.Save(newDIR + filename);
                stream.Close();
                //return "Success";
                return onlineDIR + filename;
            }
            catch (Exception e)
            {
                stream.Close();
                return e.ToString();
            }
        }
        public string downloadfile(string filename)
        {
            string ftp_ip = "ftp://54.92.167.178";
            string username = "AloricaUser1";
            string password = "S3cured@123o";

            String FilePath;
            FilePath = HostingEnvironment.MapPath("~/sFTP/Alorica_FTP/");

            string nameonly = filename.Substring(filename.Length - 1, 1).ToLower() == "s" ? filename.Substring(0, filename.Length - 4) : filename.Substring(0, filename.Length - 5);
            string newfilename = "UA_" + nameonly + ".xls";

            Stream responseStream = new MemoryStream();
            //FileStream outputStream = new FileStream(FilePath + "\\" + "UA_" + nameonly + ".xls", FileMode.Create);
            FileStream outputStream = null;

            try
            {
                FtpWebRequest ftpRequest = (FtpWebRequest)WebRequest.Create(ftp_ip + "/Uploads/" + filename);
                ftpRequest.Method = WebRequestMethods.Ftp.DownloadFile;

                // FTP Login
                ftpRequest.Credentials = new NetworkCredential(username, password);

                FtpWebResponse ftpResponse = (FtpWebResponse)ftpRequest.GetResponse();

                responseStream = ftpResponse.GetResponseStream();
                long cl = ftpResponse.ContentLength;

                int bufferSize = 2048;
                int readCount;

                byte[] buffer = new byte[bufferSize];

                readCount = responseStream.Read(buffer, 0, bufferSize);

                using (outputStream = new FileStream(FilePath + "\\" + "UA_" + nameonly + ".xls", FileMode.Create))
                {
                    while (readCount > 0)
                    {
                        outputStream.Write(buffer, 0, readCount);
                        readCount = responseStream.Read(buffer, 0, bufferSize);
                    }
                }

                responseStream.Close();
                outputStream.Close();
                ftpResponse.Close();

                return "done";
            }
            catch (Exception e)
            {
                responseStream.Close();
                outputStream.Close();

                return e.ToString();
            }
        }

        public string uploadfile(string filename)
        {
            string ftp_ip = "ftp://54.92.167.178";
            string username = "AloricaUser1";
            string password = "S3cured@123o";

            string nameonly = filename.Substring(filename.Length - 1, 1).ToLower() == "s" ? filename.Substring(0, filename.Length - 4) : filename.Substring(0, filename.Length - 5);

            string datebuild = DateTime.Now.ToString("MMddyyyy");
            string timebuild = DateTime.Now.ToString("hh-mmtt");

            string FilePath = HostingEnvironment.MapPath("~/sFTP/Alorica_FTP/");

            System.IO.FileInfo fi = new System.IO.FileInfo(FilePath + "UA_" + nameonly + ".xls");

            Stream rs = new MemoryStream();
            //FileStream fs = fi.OpenRead();
            FileStream fs = null;

            try
            {
                string uploadname = nameonly + "_" + datebuild + "_" + timebuild + ".xls";
                FtpWebRequest ftpClient = (FtpWebRequest)FtpWebRequest.Create(ftp_ip + "/Archives/" + uploadname);
                ftpClient.Credentials = new System.Net.NetworkCredential(username, password);
                ftpClient.Method = System.Net.WebRequestMethods.Ftp.UploadFile;
                ftpClient.UseBinary = true;
                ftpClient.KeepAlive = true;

                ftpClient.ContentLength = fi.Length;
                byte[] buffer1 = new byte[4097];
                int bytes = 0;
                int total_bytes = (int)fi.Length;

                rs = ftpClient.GetRequestStream();

                using (fs = fi.OpenRead())
                {
                    while (total_bytes > 0)
                    {
                        bytes = fs.Read(buffer1, 0, buffer1.Length);
                        rs.Write(buffer1, 0, bytes);
                        total_bytes = total_bytes - bytes;
                    }
                }

                fs.Close();
                rs.Close();
                FtpWebResponse uploadResponse = (FtpWebResponse)ftpClient.GetResponse();
                string value = uploadResponse.StatusDescription;
                uploadResponse.Close();

                return "done";
            }
            catch (Exception e)
            {
                rs.Close();
                fs.Close();

                return e.ToString();
            }
        }
        [HttpPost]
        [Route("CRT_SaladInfoSearch")]
        public List<CRTSaladCompanySearchDisplay> CRTSaladInfoSearch(CRTPublicParam CRTPP)
        {

            tempconnectionstring();
            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@CClientID", mytype = SqlDbType.NVarChar, Value = CRTPP.CClientID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@CompanyName", mytype = SqlDbType.NVarChar, Value = CRTPP.CompanyName });
            Connection.myparameters.Add(new myParameters { ParameterName = "@to", mytype = SqlDbType.NVarChar, Value = CRTPP.to });
            Connection.myparameters.Add(new myParameters { ParameterName = "@from", mytype = SqlDbType.NVarChar, Value = CRTPP.from });
            Connection.myparameters.Add(new myParameters { ParameterName = "@crtStatus", mytype = SqlDbType.NVarChar, Value = CRTPP.crtStatus });

            DataTable DT = new DataTable();
            DT = Connection.GetDataTable("SearchSaladInfo");
            List<CRTSaladCompanySearchDisplay> ListReturned = new List<CRTSaladCompanySearchDisplay>();
            if (DT.Rows.Count > 0)
            {
                foreach (DataRow row in DT.Rows)
                {
                    CRTSaladCompanySearchDisplay Displaynewusers = new CRTSaladCompanySearchDisplay
                    {
                        Generated = row["Generated"].ToString(),
                        GeneratedPDF = row["GeneratedPDF"].ToString(),
                        CompanyAlias = row["CompanyAlias"].ToString(),
                        CompanyName = row["CompanyName"].ToString(),
                        Branch = row["Branch"].ToString(),
                        StartingID = row["StartingID"].ToString(),
                        LastName = row["LastName"].ToString(),
                        FirstName = row["FirstName"].ToString(),
                        MiddleName = row["MiddleName"].ToString(),
                        Suffix = row["Suffix"].ToString(),
                        CardName = row["CardName"].ToString(),
                        Birthday = row["Birthday"].ToString(),
                        PlaceofBirth = row["PlaceofBirth"].ToString(),
                        CRTStatus = row["CRTStatus"].ToString(),
                        CRTDateSent = row["CRTDateSent"].ToString()
                    };
                    ListReturned.Add(Displaynewusers);
                }
            }

            return ListReturned;
        }

        [DllImport("user32.dll", SetLastError = true)]
        static extern uint GetWindowThreadProcessId(IntPtr hWnd, out uint lpdwProcessId);
        [HttpPost]
        [Route("CRT_ManualUploadAutomation")]
        public string ManualUploadAutomation()
        {

            //GET FILE FROM FTP
            Stream responseStream1 = new MemoryStream();

            //PROCESSED FILE
            Stream stream = new MemoryStream();

            string ftp_ip = "ftp://54.92.167.178";
            string username = "AloricaUser1";
            string password = "S3cured@123o";

            try
            {
                //GET FILE FROM FTP
                FtpWebRequest ftpRequest1 = (FtpWebRequest)WebRequest.Create(ftp_ip + "/Uploads/");
                ftpRequest1.Method = WebRequestMethods.Ftp.ListDirectory;

                ftpRequest1.Credentials = new NetworkCredential(username, password);
                FtpWebResponse ftpResponse1 = (FtpWebResponse)ftpRequest1.GetResponse();

                // Stream responseStream1 = ftpResponse1.GetResponseStream();
                responseStream1 = ftpResponse1.GetResponseStream();
                StreamReader reader = new StreamReader(responseStream1);
                string retstring = "";
                retstring = reader.ReadToEnd().ToString();

                String[] filenames = retstring.Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);
                //string ret = "";

                var folder = Path.Combine(HostingEnvironment.MapPath("~/sFTP/"), "Alorica_FTP");
                Directory.CreateDirectory(folder);

                foreach (string s in filenames)
                {
                    string datebuild = DateTime.Now.ToString("MMddyyyy");
                    string timebuild = DateTime.Now.ToString("hh-mmtt");

                    string nameonly = s.Substring(s.Length - 1, 1).ToLower() == "s" ? s.Substring(0, s.Length - 4) : s.Substring(0, s.Length - 5);
                    string newfilename = "UA_" + nameonly + ".xls";

                    downloadfile(s);
                    uploadfile(s);

                    //Excel.Application xlApp = new Excel.Application();
                    var xlApp = new Microsoft.Office.Interop.Excel.Application();
                    string dataDir = HostingEnvironment.MapPath("~/sFTP/Alorica_FTP/");
                    //string file = "SampleManualUpload.xlsx";
                    //string extension = Path.GetExtension(file);
                    //string newfilename;
                    //if (extension == ".xlsx")
                    //{
                    //    var temp = file.Substring(0, file.Length - 4);
                    //    newfilename = "new_" + temp + "xls";
                    //    var app = new Microsoft.Office.Interop.Excel.Application();
                    //    var wb = app.Workbooks.Open(dataDir + file);
                    //    wb.SaveAs(Filename: dataDir + newfilename, FileFormat: Microsoft.Office.Interop.Excel.XlFileFormat.xlOpenXMLWorkbook);
                    //    wb.Close();
                    //    app.Quit();
                    //}
                    //else
                    //{
                    //    newfilename = file;
                    //}
                    //Excel.Workbook xlWorkbook = xlApp.Workbooks.Open(@"C:\Users\E56626\Desktop\Teddy\VS2012\Sandbox\sandbox_test - Copy - Copy.xlsx");

                    //Excel.Workbook xlWorkbook = xlApp.Workbooks.Open(dataDir + newfilename);
                    // var xlWorkbook = xlApp.Workbooks.Open(dataDir + newfilename);

                    var xlWorkbooks = xlApp.Workbooks;
                    var xlWorkbook = xlWorkbooks.Open(dataDir + newfilename);
                    //Excel._Worksheet xlWorksheet = xlWorkbook.Sheets[1];
                    var xlWorksheet = xlWorkbook.Sheets[1];
                    //Excel.Range xlRange = xlWorksheet.UsedRange;
                    var xlRange = xlWorksheet.UsedRange;

                    int rowCount = xlRange.Rows.Count;
                    int colCount = xlRange.Columns.Count;

                    DataTable UserList = new DataTable();
                    UserList.Columns.AddRange(new DataColumn[10] { new DataColumn("EmpID"), new DataColumn("FName"), new DataColumn("MName"), new DataColumn("LName"), new DataColumn("AccountType"), new DataColumn("TIN"), new DataColumn("Birthdate"), new DataColumn("Suffix"), new DataColumn("Site"), new DataColumn("RowNum") });

                    if (rowCount > 1)
                    {
                        for (int i = 2; i <= rowCount; i++)
                        {
                            string middlename = "";
                            string suffix = "";

                            middlename = xlRange.Cells[i, 3].Value == null ? "." : xlRange.Cells[i, 3].Value.ToString();
                            suffix = xlRange.Cells[i, 5].Value == null ? "-" : xlRange.Cells[i, 5].Value.ToString();

                            UserList.Rows.Add
                            (i.ToString(), // rowid
                                xlRange.Cells[i, 1].Value.ToString(), // empid
                                xlRange.Cells[i, 2].Value.ToString(), // fname
                                middlename, // middlename
                                xlRange.Cells[i, 4].Value.ToString(), //lastname
                                suffix, // suffix
                                xlRange.Cells[i, 6].Value.ToString(), //bday
                                xlRange.Cells[i, 7].Value.ToString(), //branch
                                xlRange.Cells[i, 9].Value.ToString(), // tin
                                xlRange.Cells[i, 8].Value.ToString() // account type
                            );
                        }
                        tempconnectionstring();
                        Connection con = new Connection();
                        con.myparameters.Add(new myParameters { ParameterName = "@CompanyName", mytype = SqlDbType.VarChar, Value = "lbs" });
                        con.myparameters.Add(new myParameters { ParameterName = "@IMPORT", mytype = SqlDbType.Structured, Value = UserList });
                        DataTable DT = con.GetDataTable("sp_CRT_ManualUploadAutomation");

                        string dataDir2 = HostingEnvironment.MapPath("~/sFTP/Alorica_FTP/manual_upload.xls");
                        stream = new FileStream(dataDir2, FileMode.Open);

                        string filename = "manualupload" + "_" + datebuild + "_" + timebuild + ".xls";
                        using (var fileStream = new FileStream(dataDir2 + filename, FileMode.Create, FileAccess.Write))
                        {
                            stream.CopyTo(fileStream);
                        }
                        stream.Close();

                        ExcelWorkbookFormat workbookFormat = ExcelWorkbookFormat.Xls_2003;
                        string testDocFile = dataDir2;
                        ExcelWorkbook workbook = new ExcelWorkbook(testDocFile);
                        ExcelWorksheet firstWorksheet = workbook.Worksheets["Sheet1"];

                        int cellnum = 2;

                        for (int i = 0; i < DT.Rows.Count; i++)
                        {
                            firstWorksheet["A" + cellnum].Text = DT.Rows[i]["RowID"].ToString();
                            firstWorksheet["B" + cellnum].Text = DT.Rows[i]["StartingID"].ToString();
                            firstWorksheet["C" + cellnum].Text = DT.Rows[i]["Conflict"].ToString();

                            cellnum++;
                        }

                        string newDIR = HttpContext.Current.Server.MapPath("~/sFTP/Alorica_FTP/");
                        workbook.Save(newDIR + filename);

                        uint processId = 0;

                        if (xlApp != null)
                        {
                            if (xlApp.Workbooks != null)
                            {
                                if (xlApp.Workbooks.Count > 0)
                                {
                                    GetWindowThreadProcessId(new IntPtr(xlApp.Hwnd), out processId);

                                    // All your actions here
                                    xlWorkbook.Close(true, dataDir + newfilename, null);
                                    xlWorkbooks.Close();
                                    xlApp.Workbooks.Close();
                                    xlApp.Quit();

                                }
                            }
                        }

                        try
                        {
                            if (processId != 0)
                            {
                                Process excelProcess = Process.GetProcessById((int)processId);
                                excelProcess.CloseMainWindow();
                                excelProcess.Refresh();
                                excelProcess.Kill();
                            }
                        }
                        catch
                        {
                            // Process was already killed
                        }

                        string emailaccount = "durusthr@illimitado.com";
                        string emailpassword = "@1230Qwerty";
                        string prefix = HttpContext.Current.Server.MapPath("~/sFTP/Alorica_FTP/");
                        using (MailMessage mm = new MailMessage(emailaccount, "jsangco@illimitado.com"))
                        {
                            MailAddress aliasmail = new MailAddress("durusthr@illimitado.com", "durusthr@illimitado.com");
                            MailAddress aliasreplymail = new MailAddress("durusthr@illimitado.com");
                            mm.From = aliasmail;
                            mm.Subject = "Notifications";
                            mm.Body = "Successfully uploaded " + (rowCount - 1).ToString() + " records on " + DateTime.Now.ToString("dddd, dd MMMM yyyy hh:mm tt");

                            if (DT.Rows.Count >= 1)
                            {
                                mm.Body = "We are unable to upload the file due to conflicts. Please see attachment for detailed info.";
                                mm.Attachments.Add(new Attachment(prefix + filename));
                            }

                            //mm.Attachments.Add(new Attachment(newDIR + filename));
                            mm.IsBodyHtml = false;
                            SmtpClient smtp = new SmtpClient();
                            smtp.Host = "box1256.bluehost.com";
                            smtp.EnableSsl = true;
                            NetworkCredential NetworkCred = new NetworkCredential(emailaccount, emailpassword);
                            smtp.UseDefaultCredentials = true;
                            smtp.Credentials = NetworkCred;
                            smtp.Port = 587;
                            smtp.Send(mm);
                        }

                        //DELETE FILE FROM FTP
                        FtpWebRequest request = (FtpWebRequest)WebRequest.Create(ftp_ip + "/Uploads/" + s);
                        request.Credentials = new NetworkCredential(username, password);
                        request.Method = WebRequestMethods.Ftp.DeleteFile;
                        FtpWebResponse response = (FtpWebResponse)request.GetResponse();
                        //return response.StatusDescription;

                    }
                    else
                    {
                        string emailaccount = "durusthr@illimitado.com";
                        string emailpassword = "@1230Qwerty";
                        string prefix = HttpContext.Current.Server.MapPath("~/sFTP/Alorica_FTP/");
                        using (MailMessage mm = new MailMessage(emailaccount, "jsangco@illimitado.com"))
                        {
                            MailAddress aliasmail = new MailAddress("durusthr@illimitado.com", "durusthr@illimitado.com");
                            MailAddress aliasreplymail = new MailAddress("durusthr@illimitado.com");
                            mm.From = aliasmail;
                            mm.Subject = "Notifications";
                            mm.Body = "The file uploaded has no records.";
                            //mm.Attachments.Add(new Attachment(newDIR + filename));
                            mm.IsBodyHtml = false;
                            SmtpClient smtp = new SmtpClient();
                            smtp.Host = "box1256.bluehost.com";
                            smtp.EnableSsl = true;
                            NetworkCredential NetworkCred = new NetworkCredential(emailaccount, emailpassword);
                            smtp.UseDefaultCredentials = true;
                            smtp.Credentials = NetworkCred;
                            smtp.Port = 587;
                            smtp.Send(mm);
                        }

                        uint processId = 0;

                        if (xlApp != null)
                        {
                            if (xlApp.Workbooks != null)
                            {
                                if (xlApp.Workbooks.Count > 0)
                                {
                                    GetWindowThreadProcessId(new IntPtr(xlApp.Hwnd), out processId);

                                    // All your actions here
                                    xlWorkbook.Close(true, dataDir + newfilename, null);
                                    xlWorkbooks.Close();
                                    xlApp.Workbooks.Close();
                                    xlApp.Quit();

                                }
                            }
                        }

                        try
                        {
                            if (processId != 0)
                            {
                                Process excelProcess = Process.GetProcessById((int)processId);
                                excelProcess.CloseMainWindow();
                                excelProcess.Refresh();
                                excelProcess.Kill();
                            }
                        }
                        catch
                        {
                            // Process was already killed
                        }

                        FtpWebRequest request = (FtpWebRequest)WebRequest.Create(ftp_ip + "/Uploads/" + s);
                        request.Credentials = new NetworkCredential(username, password);
                        request.Method = WebRequestMethods.Ftp.DeleteFile;
                        FtpWebResponse response = (FtpWebResponse)request.GetResponse();
                    }
                }
                reader.Close();
                ftpResponse1.Close();
                responseStream1.Close();
                //return ret;
                return "success";
            }
            catch (Exception e)
            {
                responseStream1.Close();
                stream.Close();

                return e.ToString();
            }
        }

        public string downloadFilev2(string s)
        {
            string host = @"34.202.217.120";
            string username = "AloricaUser1";
            string password = @"S3cured@123o";

            string datebuild = DateTime.Now.ToString("MMddyyyy");
            string timebuild = DateTime.Now.ToString("hh-mmtt");

            string nameonly = s.Substring(s.Length - 1, 1).ToLower() == "s" ? s.Substring(0, s.Length - 4) : s.Substring(0, s.Length - 5);
            string newfilename = "UA_" + nameonly + ".xls";

            // PATH TO FILE AT SFTP SERVER
            string pathRemoteFile = "/Upload/" + s;

            // PATH WHERE THE FILE WILL BE SAVED
            string pathLocalFile = HostingEnvironment.MapPath("~/sFTP/Alorica_FTP/");

            using (SftpClient sftp = new SftpClient(host, username, password))
            {
                try
                {
                    sftp.Connect();

                    using (Stream fileStream = File.OpenWrite(pathLocalFile + newfilename))
                    {
                        sftp.DownloadFile(pathRemoteFile, fileStream);
                        fileStream.Close();
                    }

                    sftp.Disconnect();
                    return "success";
                }
                catch (Exception e)
                {
                    return e.ToString();
                }
            }
        }

        public string uploadFileV2(string s)
        {
            string host = @"34.202.217.120";
            string username = "AloricaUser1";
            string password = @"S3cured@123o";

            string datebuild = DateTime.Now.ToString("MMddyyyy");
            //string timebuild = DateTime.Now.ToString("hh-mmtt");

            string zoneid = "Singapore Standard Time";
            TimeZoneInfo tzi = TimeZoneInfo.FindSystemTimeZoneById(zoneid);
            DateTime timeresult = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, tzi);

            string nameonly = s.Substring(s.Length - 1, 1).ToLower() == "s" ? s.Substring(0, s.Length - 4) : s.Substring(0, s.Length - 5);
            string newfilename = nameonly + "_" + datebuild + "_" + timeresult + ".xls";

            // PATH WHERE THE FILE WILL BE UPLOADED
            string pathDestination = "/Archive";

            // PATH OF FILE TO UPLOAD
            string sourceFile = HostingEnvironment.MapPath("~/sFTP/Alorica_FTP/" + "UA_" + nameonly + ".xls");

            using (SftpClient sftp = new SftpClient(host, username, password))
            {
                try
                {
                    sftp.Connect();
                    sftp.ChangeDirectory(pathDestination);

                    using (FileStream fs = new FileStream(sourceFile, FileMode.Open))
                    {
                        sftp.BufferSize = 4 * 1024;
                        sftp.UploadFile(fs, newfilename);

                        fs.Close();
                    }

                    sftp.Disconnect();

                    return "Success";
                }
                catch (Exception e)
                {
                    return e.ToString();
                }
            }
        }

        public string deleteFileV2(string s)
        {
            string host = @"34.202.217.120";
            string username = "AloricaUser1";
            string password = @"S3cured@123o";

            using (SftpClient sftp = new SftpClient(host, username, password))
            {
                string pathFileToDelete = "/Upload/" + s;
                try
                {
                    sftp.Connect();

                    sftp.DeleteFile(pathFileToDelete);

                    sftp.Disconnect();
                    return "success";
                }
                catch (Exception e)
                {
                    return e.ToString();
                }
            }
        }

        [HttpPost]
        [Route("TestTime")]
        public string testTime()
        {
            string zoneid = "Singapore Standard Time";
            TimeZoneInfo tzi = TimeZoneInfo.FindSystemTimeZoneById(zoneid);
            DateTime timeresult = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, tzi);

            return "converted time: " + timeresult;
        }

        [HttpPost]
        [Route("SFTP_FileChecker")]
        public string filechecheker()
        {
            Stream stream = new MemoryStream();

            try
            {
                string host = @"54.92.167.178";
                string username = "AloricaUser1";
                string password = @"S3cured@123o";

                var emailadd = new List<string>();
                emailadd.Add("velascomikeec@gmail.com");
                emailadd.Add("mikee.gameacc@gmail.com");
                emailadd.Add("jsangco@illimitado.com");
                emailadd.Add("kenjie031596@outlook.com");
                emailadd.ToArray();

                string remoteDirectory = "/Upload";
                var counter = 0;
                var str = "";

                //added by brandon 12/2/2019
                DateTime filesDate = File.GetLastAccessTime(remoteDirectory);


                using (SftpClient sftp = new SftpClient(host, username, password))
                {
                    try
                    {
                        sftp.Connect();

                        var files = sftp.ListDirectory(remoteDirectory);
                        //counter = sftp.GetDirCount
                        foreach (var file in files)
                        {
                            if (file.Name.ToString() == "." || file.Name.ToString() == "..")
                            {
                                counter = 0;
                            }
                            else
                            {
                                counter += 1;
                                //this is the  old
                                //str += file.LastWriteTimeUtc.ToString() + "(" + file.Name.ToString() + "), " ;

                                //Added by brandon 12/2/2019
                                str += file.LastWriteTimeUtc.ToString() + "(" + file.Name.ToString() + ") Date and Time Added(" + filesDate + "), ";
                            }
                        }

                        sftp.Disconnect();

                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("An exception has been caught " + e.ToString());
                    }

                    foreach (var email in emailadd)
                    {
                        string emailaccount = "durusthr@illimitado.com";
                        string emailpassword = "@1230Qwerty";
                        string prefix = HttpContext.Current.Server.MapPath("~/sFTP/Alorica_FTP/");
                        using (MailMessage mm = new MailMessage(emailaccount, email))
                        {
                            MailAddress aliasmail = new MailAddress("durusthr@illimitado.com", "durusthr@illimitado.com");
                            MailAddress aliasreplymail = new MailAddress("durusthr@illimitado.com");
                            mm.From = aliasmail;
                            mm.Subject = "Notifications";
                            //mm.Body = "Successfully uploaded " + (rowCount - 1).ToString() + " records on " + DateTime.Now.ToString("dddd, dd MMMM yyyy hh:mm tt");
                            if (counter == 0)
                            {
                                mm.Body = "No. of file/s uploaded: " + counter;
                            }
                            else
                            {
                                mm.Body = "No. of file/s uploaded: " + counter + "Filename: " + str;
                            }

                            //mm.Attachments.Add(new Attachment(newDIR + filename));
                            mm.IsBodyHtml = false;
                            SmtpClient smtp = new SmtpClient();
                            smtp.Host = "box1256.bluehost.com";
                            smtp.EnableSsl = true;
                            NetworkCredential NetworkCred = new NetworkCredential(emailaccount, emailpassword);
                            smtp.UseDefaultCredentials = true;
                            smtp.Credentials = NetworkCred;
                            smtp.Port = 587;
                            smtp.Send(mm);
                        }
                    }
                }
                return "success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }

        [HttpPost]
        [Route("CRT_ManualUploadAutomationV2")]
        public string ManualUploadAutomationV2()
        {
            Stream stream = new MemoryStream();

            try
            {
                string host = @"54.92.167.178";
                string username = "AloricaUser1";
                string password = @"S3cured@123o";

                var emailadd = new List<string>();
                emailadd.Add("velascomikeec@gmail.com");
                emailadd.Add("mikee.gameacc@gmail.com");
                emailadd.Add("jsangco@illimitado.com");
                emailadd.Add("eya@illimitado.com");
                emailadd.Add("polo@illimitado.com");
                emailadd.Add("payrollphilippines@alorica.com");
                emailadd.Add("macristine.manalili@alorica.com");
                emailadd.Add("kenjie031596@outlook.com");
                emailadd.Add("Michael.Figueroa@alorica.com");
                emailadd.Add("Monchito.Dantes@alorica.com");
                emailadd.Add("bharat@illimitado.com");
                emailadd.Add("PayrollPhilippines@alorica.com");
                emailadd.Add("noreakram22@gmail.com");
                emailadd.ToArray();

                string zoneid = "Singapore Standard Time";
                TimeZoneInfo tzi = TimeZoneInfo.FindSystemTimeZoneById(zoneid);
                DateTime timeresult = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, tzi);

                //return "converted time: " + timeresult;

                string remoteDirectory = "/Upload";

                //GET FILENAME FROM SFTP SERVER DIRECTORY
                string retstring = "";
                using (SftpClient sftp = new SftpClient(host, username, password))
                {
                    try
                    {
                        sftp.Connect();

                        var files = sftp.ListDirectory(remoteDirectory);

                        foreach (var file in files)
                        {
                            //Console.WriteLine(file.Name);
                            string file1 = file.Name.ToString();
                            retstring = retstring + "\r\n" + file1;
                        }

                        sftp.Disconnect();
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("An exception has been caught " + e.ToString());
                    }
                }

                String[] filenames1 = retstring.Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);

                var filenames = new List<string>(filenames1);
                filenames.Remove(".");
                filenames.Remove("..");
                filenames.ToArray();

                //CREATE LOCAL FOLDER FOR DOWNLOADED FILES
                var folder = Path.Combine(HostingEnvironment.MapPath("~/sFTP/"), "Alorica_FTP");
                Directory.CreateDirectory(folder);

                foreach (string s in filenames)
                {
                    string datebuild = DateTime.Now.ToString("MMddyyyy");
                    string timebuild = DateTime.Now.ToString("hh-mmtt");

                    string nameonly = s.Substring(s.Length - 1, 1).ToLower() == "s" ? s.Substring(0, s.Length - 4) : s.Substring(0, s.Length - 5);
                    string newfilename = "UA_" + nameonly + ".xls";

                    downloadFilev2(s);
                    uploadFileV2(s);

                    var xlApp = new Microsoft.Office.Interop.Excel.Application();
                    string dataDir = HostingEnvironment.MapPath("~/sFTP/Alorica_FTP/");

                    var xlWorkbooks = xlApp.Workbooks;
                    var xlWorkbook = xlWorkbooks.Open(dataDir + newfilename);
                    var xlWorksheet = xlWorkbook.Sheets[1];
                    var xlRange = xlWorksheet.UsedRange;
                    int rowCount = xlRange.Rows.Count;
                    int colCount = xlRange.Columns.Count;

                    DataTable UserList = new DataTable();
                    UserList.Columns.AddRange(new DataColumn[10] { new DataColumn("EmpID"), new DataColumn("FName"), new DataColumn("MName"), new DataColumn("LName"), new DataColumn("AccountType"), new DataColumn("TIN"), new DataColumn("Birthdate"), new DataColumn("Suffix"), new DataColumn("Site"), new DataColumn("RowNum") });

                    if (rowCount > 1)
                    {
                        for (int i = 2; i <= rowCount; i++)
                        {
                            string middlename = "";
                            string suffix = "";

                            middlename = xlRange.Cells[i, 3].Value == null ? "." : xlRange.Cells[i, 3].Value.ToString();
                            suffix = xlRange.Cells[i, 5].Value == null ? "-" : xlRange.Cells[i, 5].Value.ToString();

                            UserList.Rows.Add
                            (i.ToString(), // rowid
                                xlRange.Cells[i, 1].Value.ToString(), // empid
                                xlRange.Cells[i, 2].Value.ToString(), // fname
                                middlename, // middlename
                                xlRange.Cells[i, 4].Value.ToString(), //lastname
                                suffix, // suffix
                                xlRange.Cells[i, 6].Value.ToString(), //bday
                                xlRange.Cells[i, 7].Value.ToString(), //branch
                                xlRange.Cells[i, 9].Value.ToString(), // tin
                                xlRange.Cells[i, 8].Value.ToString() // account type
                            );
                        }
                        tempconnectionstring();
                        Connection con = new Connection();
                        con.myparameters.Add(new myParameters { ParameterName = "@CompanyName", mytype = SqlDbType.VarChar, Value = "alorica" });
                        con.myparameters.Add(new myParameters { ParameterName = "@IMPORT", mytype = SqlDbType.Structured, Value = UserList });
                        DataTable DT = con.GetDataTable("sp_CRT_ManualUploadAutomation");

                        string dataDir2 = HostingEnvironment.MapPath("~/sFTP/Alorica_FTP/manual_upload.xls");
                        stream = new FileStream(dataDir2, FileMode.Open);

                        string filename = "manualupload" + "_" + datebuild + "_" + timebuild + ".xls";
                        using (var fileStream = new FileStream(dataDir2 + filename, FileMode.Create, FileAccess.Write))
                        {
                            stream.CopyTo(fileStream);
                        }
                        stream.Close();

                        ExcelWorkbookFormat workbookFormat = ExcelWorkbookFormat.Xls_2003;
                        string testDocFile = dataDir2;
                        ExcelWorkbook workbook = new ExcelWorkbook(testDocFile);
                        ExcelWorksheet firstWorksheet = workbook.Worksheets["Sheet1"];

                        int cellnum = 2;

                        for (int i = 0; i < DT.Rows.Count; i++)
                        {
                            firstWorksheet["A" + cellnum].Text = DT.Rows[i]["RowID"].ToString();
                            firstWorksheet["B" + cellnum].Text = DT.Rows[i]["StartingID"].ToString();
                            firstWorksheet["C" + cellnum].Text = DT.Rows[i]["Conflict"].ToString();

                            cellnum++;
                        }

                        string newDIR = HttpContext.Current.Server.MapPath("~/sFTP/Alorica_FTP/");
                        workbook.Save(newDIR + filename);

                        uint processId = 0;

                        if (xlApp != null)
                        {
                            if (xlApp.Workbooks != null)
                            {
                                if (xlApp.Workbooks.Count > 0)
                                {
                                    GetWindowThreadProcessId(new IntPtr(xlApp.Hwnd), out processId);

                                    // All your actions here
                                    xlWorkbook.Close(true, dataDir + newfilename, null);
                                    xlWorkbooks.Close();
                                    xlApp.Workbooks.Close();
                                    xlApp.Quit();

                                }
                            }
                        }

                        try
                        {
                            if (processId != 0)
                            {
                                Process excelProcess = Process.GetProcessById((int)processId);
                                excelProcess.CloseMainWindow();
                                excelProcess.Refresh();
                                excelProcess.Kill();
                            }
                        }
                        catch
                        {
                            // Process was already killed
                        }

                        foreach (var email in emailadd)
                        {
                            string emailaccount = "durusthr@illimitado.com";
                            string emailpassword = "@1230Qwerty";
                            string prefix = HttpContext.Current.Server.MapPath("~/sFTP/Alorica_FTP/");
                            using (MailMessage mm = new MailMessage(emailaccount, email))
                            {
                                MailAddress aliasmail = new MailAddress("durusthr@illimitado.com", "durusthr@illimitado.com");
                                MailAddress aliasreplymail = new MailAddress("durusthr@illimitado.com");
                                mm.From = aliasmail;
                                mm.Subject = "Notifications";
                                //mm.Body = "Successfully uploaded " + (rowCount - 1).ToString() + " records on " + DateTime.Now.ToString("dddd, dd MMMM yyyy hh:mm tt");
                                mm.Body = "Successfully uploaded " + (rowCount - 1).ToString() + " records on " + timeresult;

                                if (DT.Rows.Count >= 1)
                                {
                                    mm.Body = "We are unable to upload the file due to conflicts. Please see attachment for detailed info.";
                                    mm.Attachments.Add(new Attachment(prefix + filename));
                                }

                                //mm.Attachments.Add(new Attachment(newDIR + filename));
                                mm.IsBodyHtml = false;
                                SmtpClient smtp = new SmtpClient();
                                smtp.Host = "box1256.bluehost.com";
                                smtp.EnableSsl = true;
                                NetworkCredential NetworkCred = new NetworkCredential(emailaccount, emailpassword);
                                smtp.UseDefaultCredentials = true;
                                smtp.Credentials = NetworkCred;
                                smtp.Port = 587;
                                smtp.Send(mm);
                            }
                        }

                        //DELETE FILE ON SFTP
                        deleteFileV2(s);
                        stream.Close();
                    }
                    else
                    {
                        foreach (var email in emailadd)
                        {
                            string emailaccount = "durusthr@illimitado.com";
                            string emailpassword = "@1230Qwerty";
                            string prefix = HttpContext.Current.Server.MapPath("~/sFTP/Alorica_FTP/");
                            using (MailMessage mm = new MailMessage(emailaccount, email))
                            {
                                MailAddress aliasmail = new MailAddress("durusthr@illimitado.com", "durusthr@illimitado.com");
                                MailAddress aliasreplymail = new MailAddress("durusthr@illimitado.com");
                                mm.From = aliasmail;
                                mm.Subject = "Notifications";
                                mm.Body = "The file uploaded has no records.";
                                //mm.Attachments.Add(new Attachment(newDIR + filename));
                                mm.IsBodyHtml = false;
                                SmtpClient smtp = new SmtpClient();
                                smtp.Host = "box1256.bluehost.com";
                                smtp.EnableSsl = true;
                                NetworkCredential NetworkCred = new NetworkCredential(emailaccount, emailpassword);
                                smtp.UseDefaultCredentials = true;
                                smtp.Credentials = NetworkCred;
                                smtp.Port = 587;
                                smtp.Send(mm);
                            }
                        }

                        uint processId = 0;

                        if (xlApp != null)
                        {
                            if (xlApp.Workbooks != null)
                            {
                                if (xlApp.Workbooks.Count > 0)
                                {
                                    GetWindowThreadProcessId(new IntPtr(xlApp.Hwnd), out processId);

                                    // All your actions here
                                    xlWorkbook.Close(true, dataDir + newfilename, null);
                                    xlWorkbooks.Close();
                                    xlApp.Workbooks.Close();
                                    xlApp.Quit();

                                }
                            }
                        }

                        try
                        {
                            if (processId != 0)
                            {
                                Process excelProcess = Process.GetProcessById((int)processId);
                                excelProcess.CloseMainWindow();
                                excelProcess.Refresh();
                                excelProcess.Kill();
                            }
                        }
                        catch
                        {
                            // Process was already killed
                        }

                        // DELETE FILE ON SFTP
                        deleteFileV2(s);
                    }
                }
                stream.Close();
                return "success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }

        //automationv3
        [HttpPost]
        [Route("CRT_ManualUploadAutomationV4")]
        public string ManualUploadAutomationV4()
        {
            Stream stream = new MemoryStream();

            try
            {
                string host = @"34.202.217.120";
                string username = "AloricaUser1";
                string password = @"S3cured@123o";

                var emailadd = new List<string>();
                //emailadd.Add("velascomikeec@gmail.com");
                //emailadd.Add("mikee.gameacc@gmail.com");
                emailadd.Add("jsangco@illimitado.com");
                //emailadd.Add("eya@illimitado.com");
                //emailadd.Add("polo@illimitado.com");
                //emailadd.Add("payrollphilippines@alorica.com");
                //emailadd.Add("macristine.manalili@alorica.com");
                //emailadd.Add("kenjie031596@outlook.com");
                //emailadd.Add("Michael.Figueroa@alorica.com");
                //emailadd.Add("Monchito.Dantes@alorica.com");
                //emailadd.Add("bharat@illimitado.com");
                //emailadd.Add("PayrollPhilippines@alorica.com");
                emailadd.Add("noreakram22@gmail.com");
                emailadd.ToArray();

                //return "converted time: " + timeresult;

                string remoteDirectory = "/Upload";

                //GET FILENAME FROM SFTP SERVER DIRECTORY
                string retstring = "";
                using (SftpClient sftp = new SftpClient(host, username, password))
                {
                    try
                    {
                        sftp.Connect();

                        var files = sftp.ListDirectory(remoteDirectory);

                        foreach (var file in files)
                        {
                            //Console.WriteLine(file.Name);
                            string file1 = file.Name.ToString();
                            retstring = retstring + "\r\n" + file1;
                        }

                        sftp.Disconnect();
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("An exception has been caught " + e.ToString());
                    }
                }

                String[] filenames1 = retstring.Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);
                string nameonly;

                var filenames = new List<string>(filenames1);
                filenames.Remove(".");
                filenames.Remove("..");
                filenames.ToArray();
                string nameFile = filenames[0].ToString();
                //CREATE LOCAL FOLDER FOR DOWNLOADED FILES
                var folder = Path.Combine(HostingEnvironment.MapPath("~/sFTP/"), "Alorica_FTP");
                Directory.CreateDirectory(folder);

                foreach (string s in filenames)
                {
                    string datebuild = DateTime.Now.ToString("MMddyyyy");
                    //string timebuild = DateTime.Now.ToString("hh-mmtt");

                    string zoneid = "Singapore Standard Time";
                    TimeZoneInfo tzi = TimeZoneInfo.FindSystemTimeZoneById(zoneid);
                    DateTime timeresult = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, tzi);

                    nameonly = s.Substring(s.Length - 1, 1).ToLower() == "s" ? s.Substring(0, s.Length - 4) : s.Substring(0, s.Length - 5);
                    string newfilename = "UA_" + nameonly + ".xls";

                    downloadFilev2(s);
                    uploadFileV2(s);

                    var xlApp = new Microsoft.Office.Interop.Excel.Application();
                    string dataDir = HostingEnvironment.MapPath("~/sFTP/Alorica_FTP/");

                    var xlWorkbooks = xlApp.Workbooks;
                    var xlWorkbook = xlWorkbooks.Open(dataDir + newfilename);
                    var xlWorksheet = xlWorkbook.Sheets[1];
                    var xlRange = xlWorksheet.UsedRange;
                    int rowCount = xlRange.Rows.Count;
                    int colCount = xlRange.Columns.Count;
                    int successCount = 0;
                    int conflictCount = 0;
                    int userConflict = 0;
                    DataTable UserList = new DataTable();
                    UserList.Columns.AddRange(new DataColumn[10] { new DataColumn("EmpID"), new DataColumn("FName"), new DataColumn("MName"), new DataColumn("LName"), new DataColumn("AccountType"), new DataColumn("TIN"), new DataColumn("Birthdate"), new DataColumn("Suffix"), new DataColumn("Site"), new DataColumn("RowNum") });

                    if (rowCount > 1)
                    {
                        for (int i = 2; i <= rowCount; i++)
                        {
                            string middlename = "";
                            string suffix = "";

                            middlename = xlRange.Cells[i, 3].Value == null ? "." : xlRange.Cells[i, 3].Value.ToString();
                            suffix = xlRange.Cells[i, 5].Value == null ? "-" : xlRange.Cells[i, 5].Value.ToString();

                            UserList.Rows.Add
                            (i.ToString(), // rowid
                                xlRange.Cells[i, 1].Value.ToString(), // empid
                                xlRange.Cells[i, 2].Value.ToString(), // fname
                                middlename, // middlename
                                xlRange.Cells[i, 4].Value.ToString(), //lastname
                                suffix, // suffix
                                xlRange.Cells[i, 6].Value.ToString(), //bday
                                xlRange.Cells[i, 7].Value.ToString(), //branch
                                xlRange.Cells[i, 9].Value.ToString(), // tin
                                xlRange.Cells[i, 8].Value.ToString() // account type
                            );
                        }
                        tempconnectionstring();
                        Connection con = new Connection();
                        con.myparameters.Add(new myParameters { ParameterName = "@CompanyName", mytype = SqlDbType.VarChar, Value = "rrj" });
                        con.myparameters.Add(new myParameters { ParameterName = "@IMPORT", mytype = SqlDbType.Structured, Value = UserList });
                        DataTable DT = con.GetDataTable("sp_CRT_ManualUploadAutomationv3");

                        string dataDir2 = HostingEnvironment.MapPath("~/sFTP/Alorica_FTP/manual_upload.xls");
                        stream = new FileStream(dataDir2, FileMode.Open);

                        string filename = "manualupload" + "_" + datebuild + "_" + timeresult.ToString("HHmmsstt") + ".xls";
                        using (var fileStream = new FileStream(Path.Combine(dataDir2 + filename), FileMode.Create, FileAccess.Write))
                        {
                            stream.CopyTo(fileStream);
                        }
                        stream.Close();

                        ExcelWorkbookFormat workbookFormat = ExcelWorkbookFormat.Xls_2003;
                        string testDocFile = dataDir2;
                        ExcelWorkbook workbook = new ExcelWorkbook(testDocFile);
                        ExcelWorksheet firstWorksheet = workbook.Worksheets["Sheet1"];

                        int cellnum = 2;

                        for (int i = 0; i < DT.Rows.Count; i++)
                        {
                            string a = DT.Rows[i]["Conflict"].ToString();

                            if (a == "success")
                            {
                                // successCount =Int32.Parse(DT.Rows[i]["StartingID"].ToString());
                                successCount++;
                            }

                            else if (a == "distinct")
                            {
                                userConflict = Int32.Parse(DT.Rows[i]["StartingID"].ToString());
                            }

                            else
                            {
                                firstWorksheet["A" + cellnum].Text = DT.Rows[i]["RowID"].ToString();
                                firstWorksheet["B" + cellnum].Text = DT.Rows[i]["StartingID"].ToString();
                                firstWorksheet["C" + cellnum].Text = DT.Rows[i]["Conflict"].ToString();
                                conflictCount++;
                                cellnum++;
                            }



                        }

                        string newDIR = HttpContext.Current.Server.MapPath("~/sFTP/Alorica_FTP/");
                        workbook.Save(newDIR + filename);

                        uint processId = 0;

                        if (xlApp != null)
                        {
                            if (xlApp.Workbooks != null)
                            {
                                if (xlApp.Workbooks.Count > 0)
                                {
                                    GetWindowThreadProcessId(new IntPtr(xlApp.Hwnd), out processId);

                                    // All your actions here
                                    xlWorkbook.Close(true, dataDir + newfilename, null);
                                    xlWorkbooks.Close();
                                    xlApp.Workbooks.Close();
                                    xlApp.Quit();

                                }
                            }
                        }

                        try
                        {
                            if (processId != 0)
                            {
                                Process excelProcess = Process.GetProcessById((int)processId);
                                excelProcess.CloseMainWindow();
                                excelProcess.Refresh();
                                excelProcess.Kill();
                            }
                        }
                        catch
                        {
                            // Process was already killed
                        }
                        int c = rowCount - (userConflict + 1);
                        foreach (var email in emailadd)
                        {
                            //time stamp for email
                            string zoneidforemail = "Singapore Standard Time";
                            TimeZoneInfo tziforemail = TimeZoneInfo.FindSystemTimeZoneById(zoneidforemail);
                            DateTime timeresultforemail = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, tziforemail);

                            // string emailaccount = "durusthr@illimitado.com";
                            // string emailpassword = "Secured@1230";
                            string emailaccount = "durusthr@illimitado.com";
                            string emailpassword = "@1230Qwerty";
                            string prefix = HttpContext.Current.Server.MapPath("~/sFTP/Alorica_FTP/");
                            using (MailMessage mm = new MailMessage(emailaccount, email))
                            {
                                MailAddress aliasmail = new MailAddress("durusthr@illimitado.com", "durusthr@illimitado.com");
                                MailAddress aliasreplymail = new MailAddress("durusthr@illimitado.com");
                                mm.From = aliasmail;
                                mm.Subject = "Notifications";
                                //mm.Body = "Successfully uploaded " + (rowCount - 1).ToString() + " records on " + DateTime.Now.ToString("dddd, dd MMMM yyyy hh:mm tt");
                                mm.Body = "Uploaded " + c + "  record/s. " + userConflict + " user/s with conflict/s in file: '" + s + "'" + " on" + timeresultforemail + ".";

                                if (successCount >= 1 && conflictCount >= 1)
                                {

                                    mm.Body = "Uploaded " + c + "  record/s. " + userConflict + " user/s with conflict/s in file: '" + s + "' on " + timeresultforemail + "." + "Please see attached for more details.";
                                    mm.Attachments.Add(new Attachment(prefix + filename));
                                }
                                else if (successCount >= 1 && conflictCount < 1)
                                {
                                    mm.Body = "Uploaded " + c + "  record/s. " + userConflict + " user/s with conflict/s in file: '" + s + "' on " + timeresultforemail + ".";
                                    //mm.Attachments.Add(new Attachment(prefix + filename));
                                }
                                else
                                {
                                    mm.Body = "Uploaded " + c + "  record/s. " + userConflict + " user/s with conflict/s in file: '" + s + "' on " + timeresultforemail + ". Please see attached for more details.";
                                    mm.Attachments.Add(new Attachment(prefix + filename));
                                }

                                //mm.Attachments.Add(new Attachment(newDIR + filename));
                                mm.IsBodyHtml = false;
                                SmtpClient smtp = new SmtpClient();
                                smtp.Host = "smtp.office365.com";
                                smtp.EnableSsl = true;
                                NetworkCredential NetworkCred = new NetworkCredential(emailaccount, emailpassword);
                                smtp.UseDefaultCredentials = true;
                                smtp.Credentials = NetworkCred;
                                smtp.Port = 587;
                                smtp.Send(mm);
                            }
                        }

                        //DELETE FILE ON SFTP
                        deleteFileV2(s);
                        stream.Close();
                    }
                    else
                    {
                        foreach (var email in emailadd)
                        {
                            //time stamp for email
                            string zoneidforemail = "Singapore Standard Time";
                            TimeZoneInfo tziforemail = TimeZoneInfo.FindSystemTimeZoneById(zoneidforemail);
                            DateTime timeresultforemail = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, tziforemail);

                            string emailaccount = "durusthr@illimitado.com";
                            string emailpassword = "@1230Qwerty";
                            string prefix = HttpContext.Current.Server.MapPath("~/sFTP/Alorica_FTP/");
                            using (MailMessage mm = new MailMessage(emailaccount, email))
                            {
                                MailAddress aliasmail = new MailAddress("durusthr@illimitado.com", "durusthr@illimitado.com");
                                MailAddress aliasreplymail = new MailAddress("durusthr@illimitado.com");
                                mm.From = aliasmail;
                                mm.Subject = "Notifications";
                                mm.Body = "The file " + s + " uploaded on " + timeresultforemail + " has no records. ";
                                //mm.Attachments.Add(new Attachment(newDIR + filename));
                                mm.IsBodyHtml = false;
                                SmtpClient smtp = new SmtpClient();
                                smtp.Host = "smtp.office365.com";
                                smtp.EnableSsl = true;
                                NetworkCredential NetworkCred = new NetworkCredential(emailaccount, emailpassword);
                                smtp.UseDefaultCredentials = true;
                                smtp.Credentials = NetworkCred;
                                smtp.Port = 587;
                                smtp.Send(mm);
                            }
                        }

                        uint processId = 0;

                        if (xlApp != null)
                        {
                            if (xlApp.Workbooks != null)
                            {
                                if (xlApp.Workbooks.Count > 0)
                                {
                                    GetWindowThreadProcessId(new IntPtr(xlApp.Hwnd), out processId);

                                    // All your actions here
                                    xlWorkbook.Close(true, dataDir + newfilename, null);
                                    xlWorkbooks.Close();
                                    xlApp.Workbooks.Close();
                                    xlApp.Quit();

                                }
                            }
                        }

                        try
                        {
                            if (processId != 0)
                            {
                                Process excelProcess = Process.GetProcessById((int)processId);
                                excelProcess.CloseMainWindow();
                                excelProcess.Refresh();
                                excelProcess.Kill();
                            }
                        }
                        catch
                        {
                            // Process was already killed
                        }

                        // DELETE FILE ON SFTP
                        deleteFileV2(s);
                    }
                }
                stream.Close();
                return "success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }



        [HttpPost]
        [Route("CRT_AutoLogin")]
        public string CRT_autoLogin(autoLogin newuser)
        {
            try
            {
                tempconnectionstring();
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@company", mytype = SqlDbType.VarChar, Value = newuser.company });
                con.myparameters.Add(new myParameters { ParameterName = "@username", mytype = SqlDbType.VarChar, Value = newuser.username });
                con.myparameters.Add(new myParameters { ParameterName = "@password", mytype = SqlDbType.VarChar, Value = newuser.password });
                return con.ExecuteScalar("sp_AutoLogin");
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }
        /// added 
        [HttpPost]
        [Route("ExtractGenerateCRTSALADINFOv2")]
        public string ExtractGenerateCRTSALADINFOv2(SaladItemsCRT clientdata)
        {


            tempconnectionstring();
            Connection con = new Connection();

            con.myparameters.Add(new myParameters { ParameterName = "@CN", mytype = SqlDbType.VarChar, Value = clientdata._cn });
            con.myparameters.Add(new myParameters { ParameterName = "@CclientID", mytype = SqlDbType.VarChar, Value = clientdata.CclientID });

            DataTable EmpData = con.GetDataTable("sp_Get_SaladInfo");


            try
            {
                string dataDir = HostingEnvironment.MapPath("~/Files/");
                //FileStream stream = new FileStream(dataDir + "newsaladtemplate.xls", FileMode.Open);
                FileStream stream = new FileStream(dataDir + "newsaladtemplate.xls", FileMode.Open);

                string filename;
                //string companynamefile = clientdata.ClientId.ToString();
                string datebuild = DateTime.Now.ToString("MMddyyyy");
                string timebuild = DateTime.Now.ToString("hh-mmtt");

                //filename = "CrtSampleFileExport_" + datebuild + "_" +timebuild + ".xls";


                filename = /*companynamefile + "_" +*/ "ExtractAllSalad" + datebuild + "_" + timebuild + ".xls";
                using (var fileStream = new FileStream(dataDir + filename, FileMode.Create, FileAccess.Write))
                {
                    stream.CopyTo(fileStream);
                }
                stream.Close();



                ExcelWorkbookFormat workbookFormat = ExcelWorkbookFormat.Xls_2003;
                //string testDocFile = dataDir + "newsaladtemplate.xls";
                string testDocFile = dataDir + "newsaladtemplateforupload.xls";
                ExcelWorkbook workbook = new ExcelWorkbook(testDocFile);
                ExcelWorksheet firstWorksheet = workbook.Worksheets[0];


                int cellnum = 2;
                foreach (DataRow row in EmpData.Rows)
                {
                    firstWorksheet["A" + cellnum].Text = row["CRTSaladDateEntry"].ToString();
                    firstWorksheet["B" + cellnum].Text = row["GenerationDate"].ToString();
                    firstWorksheet["C" + cellnum].Text = row["CompanyName"].ToString();
                    firstWorksheet["D" + cellnum].Text = row["Branch"].ToString();
                    firstWorksheet["E" + cellnum].Text = row["LastName"].ToString();
                    firstWorksheet["F" + cellnum].Text = row["FirstName"].ToString();
                    firstWorksheet["G" + cellnum].Text = row["MiddleName"].ToString();
                    firstWorksheet["H" + cellnum].Text = row["Suffix"].ToString();
                    firstWorksheet["I" + cellnum].Text = row["CardName"].ToString();
                    firstWorksheet["J" + cellnum].Text = row["Birthday"].ToString();
                    firstWorksheet["K" + cellnum].Text = row["PlaceofBirth"].ToString();
                    firstWorksheet["L" + cellnum].Text = row["Nationality"].ToString();
                    firstWorksheet["M" + cellnum].Text = row["NationalityCountry"].ToString();
                    firstWorksheet["N" + cellnum].Text = row["Gender"].ToString();
                    firstWorksheet["O" + cellnum].Text = row["MobileNumber"].ToString();
                    firstWorksheet["P" + cellnum].Text = row["LandlineNumber"].ToString();
                    firstWorksheet["Q" + cellnum].Text = row["HouseNumber"].ToString();
                    firstWorksheet["R" + cellnum].Text = row["StreetrVillageSubdivisionBrgy"].ToString();
                    firstWorksheet["S" + cellnum].Text = row["Country"].ToString();
                    firstWorksheet["T" + cellnum].Text = row["Region"].ToString();
                    firstWorksheet["U" + cellnum].Text = row["Province"].ToString();
                    firstWorksheet["V" + cellnum].Text = row["CityMunicipality"].ToString();
                    firstWorksheet["W" + cellnum].Text = row["ZipCode"].ToString();
                    firstWorksheet["X" + cellnum].Text = row["PermanentHouseNumber"].ToString();
                    firstWorksheet["Y" + cellnum].Text = row["PermanentStreetrVillageSubdivisionBrgy"].ToString();
                    firstWorksheet["Z" + cellnum].Text = row["PermanentCountry"].ToString();
                    firstWorksheet["AA" + cellnum].Text = row["PermanentRegion"].ToString();
                    firstWorksheet["AB" + cellnum].Text = row["PermanentProvince"].ToString();
                    firstWorksheet["AC" + cellnum].Text = row["PermanentCityMunicipality"].ToString();
                    firstWorksheet["AD" + cellnum].Text = row["PermanentZipCode"].ToString();
                    firstWorksheet["AE" + cellnum].Text = row["EmailAddress"].ToString();
                    firstWorksheet["AF" + cellnum].Text = row["EmployerName"].ToString();
                    firstWorksheet["AG" + cellnum].Text = row["EmployeeTIN"].ToString();
                    firstWorksheet["AH" + cellnum].Text = row["CivilStatus"].ToString();
                    firstWorksheet["AI" + cellnum].Text = row["MotherLastName"].ToString();
                    firstWorksheet["AJ" + cellnum].Text = row["MotherFirstName"].ToString();
                    firstWorksheet["AK" + cellnum].Text = row["MotherMiddleName"].ToString();
                    firstWorksheet["AL" + cellnum].Text = row["MotherSuffix"].ToString();
                    firstWorksheet["AM" + cellnum].Text = row["NumberofPads"].ToString();
                    firstWorksheet["AN" + cellnum].Text = row["OtherDetails"].ToString();
                    firstWorksheet["AO" + cellnum].Text = row["Position"].ToString();
                    firstWorksheet["AP" + cellnum].Text = row["Rank"].ToString();
                    firstWorksheet["AQ" + cellnum].Text = row["DateHired"].ToString();
                    firstWorksheet["AR" + cellnum].Text = row["EmploymentStatus"].ToString();
                    firstWorksheet["AS" + cellnum].Text = row["EndofContract"].ToString();
                    firstWorksheet["AT" + cellnum].Text = row["RefOneName"].ToString();
                    firstWorksheet["AU" + cellnum].Text = row["RefOneMobileNumber"].ToString();
                    firstWorksheet["AV" + cellnum].Text = row["RefTwoName"].ToString();
                    firstWorksheet["AW" + cellnum].Text = row["RefTwoMobileNumber"].ToString();
                    firstWorksheet["AX" + cellnum].Text = row["DateAdded"].ToString();

                    cellnum++;
                }

                //https://http://ws.durusthr.com/ILM_WS_LIVE/sFTP/CrtSampleFileExport_12-41-PM.xls
                HttpResponse httpResponse = System.Web.HttpContext.Current.Response;

                httpResponse.Clear();
                httpResponse.ContentType = "Application/x-msexcel";
                httpResponse.AddHeader("Content-Disposition", String.Format("attachment; filename={0}", filename));
                string newDIR = HttpContext.Current.Server.MapPath("~/sFTP/");
                string onlineDIR = "http://ws.durusthr.com/ilm_ws_live/sFTP/";

                workbook.Save(newDIR + filename);

                //return "Success";
                return onlineDIR + filename;
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }
        [HttpPost]
        [Route("GenerateCRTSALADINFOv3")]
        public string GenerateCRTSALADINFOv3(SaladItemsCRT clientdata)
        {

            DataTable DT = new DataTable();
            DT.Columns.AddRange(new DataColumn[1] { new DataColumn("StartingID") });
            for (int i = 0; i < clientdata._startingid.Length; i++)
            {
                DT.Rows.Add(clientdata._startingid[i]);
            }

            tempconnectionstring();
            Connection con = new Connection();

            con.myparameters.Add(new myParameters { ParameterName = "@CN", mytype = SqlDbType.VarChar, Value = clientdata._cn });
            con.myparameters.Add(new myParameters { ParameterName = "@IMPORT", mytype = SqlDbType.Structured, Value = DT });

            DataTable EmpData = con.GetDataTable("sp_CRT_GenerateSaladInfov3");


            try
            {
                string dataDir = HostingEnvironment.MapPath("~/Files/");
                //FileStream stream = new FileStream(dataDir + "newsaladtemplate.xls", FileMode.Open);
                FileStream stream = new FileStream(dataDir + "newsaladtemplateforupload.xls", FileMode.Open);

                string filename;
                //string companynamefile = clientdata.ClientId.ToString();
                string datebuild = DateTime.Now.ToString("MMddyyyy");
                string timebuild = DateTime.Now.ToString("hh-mmtt");

                //filename = "CrtSampleFileExport_" + datebuild + "_" +timebuild + ".xls";


                filename = /*companynamefile + "_" +*/ "Salad" + datebuild + "_" + timebuild + ".xls";
                //ilename = "testing" + "_" + "Salad"+ ".xls";
                using (var fileStream = new FileStream(dataDir + filename, FileMode.Create, FileAccess.Write))
                {
                    stream.CopyTo(fileStream);
                }
                stream.Close();



                ExcelWorkbookFormat workbookFormat = ExcelWorkbookFormat.Xls_2003;
                //string testDocFile = dataDir + "newsaladtemplate.xls";
                string testDocFile = dataDir + "newsaladtemplateforupload.xls";
                ExcelWorkbook workbook = new ExcelWorkbook(testDocFile);
                ExcelWorksheet firstWorksheet = workbook.Worksheets[0];


                int cellnum = 2;
                foreach (DataRow row in EmpData.Rows)
                {
                    firstWorksheet["A" + cellnum].Text = row["CRTSaladDateEntry"].ToString();
                    firstWorksheet["B" + cellnum].DateTimeValue = DateTime.Now;
                    firstWorksheet["C" + cellnum].Text = row["CompanyName"].ToString();
                    firstWorksheet["D" + cellnum].Text = row["Branch"].ToString();
                    firstWorksheet["E" + cellnum].Text = row["LastName"].ToString();
                    firstWorksheet["F" + cellnum].Text = row["FirstName"].ToString();
                    firstWorksheet["G" + cellnum].Text = row["MiddleName"].ToString();
                    firstWorksheet["H" + cellnum].Text = row["Suffix"].ToString();
                    firstWorksheet["I" + cellnum].Text = row["CardName"].ToString();
                    firstWorksheet["J" + cellnum].Text = row["Birthday"].ToString();
                    firstWorksheet["K" + cellnum].Text = row["PlaceofBirth"].ToString();
                    firstWorksheet["L" + cellnum].Text = row["Nationality"].ToString();
                    firstWorksheet["M" + cellnum].Text = row["NationalityCountry"].ToString();
                    firstWorksheet["N" + cellnum].Text = row["Gender"].ToString();
                    firstWorksheet["O" + cellnum].Text = row["MobileNumber"].ToString();
                    firstWorksheet["P" + cellnum].Text = row["LandlineNumber"].ToString();
                    firstWorksheet["Q" + cellnum].Text = row["HouseNumber"].ToString();
                    firstWorksheet["R" + cellnum].Text = row["StreetrVillageSubdivisionBrgy"].ToString();
                    firstWorksheet["S" + cellnum].Text = row["Country"].ToString();
                    firstWorksheet["T" + cellnum].Text = row["Region"].ToString();
                    firstWorksheet["U" + cellnum].Text = row["Province"].ToString();
                    firstWorksheet["V" + cellnum].Text = row["CityMunicipality"].ToString();
                    firstWorksheet["W" + cellnum].Text = row["ZipCode"].ToString();
                    firstWorksheet["X" + cellnum].Text = row["PermanentHouseNumber"].ToString();
                    firstWorksheet["Y" + cellnum].Text = row["PermanentStreetrVillageSubdivisionBrgy"].ToString();
                    firstWorksheet["Z" + cellnum].Text = row["PermanentCountry"].ToString();
                    firstWorksheet["AA" + cellnum].Text = row["PermanentRegion"].ToString();
                    firstWorksheet["AB" + cellnum].Text = row["PermanentProvince"].ToString();
                    firstWorksheet["AC" + cellnum].Text = row["PermanentCityMunicipality"].ToString();
                    firstWorksheet["AD" + cellnum].Text = row["PermanentZipCode"].ToString();
                    firstWorksheet["AE" + cellnum].Text = row["EmailAddress"].ToString();
                    firstWorksheet["AF" + cellnum].Text = row["EmployerName"].ToString();
                    firstWorksheet["AG" + cellnum].Text = row["EmployeeTIN"].ToString();
                    firstWorksheet["AH" + cellnum].Text = row["CivilStatus"].ToString();
                    firstWorksheet["AI" + cellnum].Text = row["MotherLastName"].ToString();
                    firstWorksheet["AJ" + cellnum].Text = row["MotherFirstName"].ToString();
                    firstWorksheet["AK" + cellnum].Text = row["MotherMiddleName"].ToString();
                    firstWorksheet["AL" + cellnum].Text = row["MotherSuffix"].ToString();
                    firstWorksheet["AM" + cellnum].Text = row["NumberofPads"].ToString();
                    firstWorksheet["AN" + cellnum].Text = row["OtherDetails"].ToString();
                    firstWorksheet["AO" + cellnum].Text = row["Position"].ToString();
                    firstWorksheet["AP" + cellnum].Text = row["Rank"].ToString();
                    firstWorksheet["AQ" + cellnum].Text = row["DateHired"].ToString();
                    firstWorksheet["AR" + cellnum].Text = row["EmploymentStatus"].ToString();
                    firstWorksheet["AS" + cellnum].Text = row["EndofContract"].ToString();
                    firstWorksheet["AT" + cellnum].Text = row["RefOneName"].ToString();
                    firstWorksheet["AU" + cellnum].Text = row["RefOneMobileNumber"].ToString();
                    firstWorksheet["AV" + cellnum].Text = row["RefTwoName"].ToString();
                    firstWorksheet["AW" + cellnum].Text = row["RefTwoMobileNumber"].ToString();
                    firstWorksheet["AX" + cellnum].Text = row["DateAdded"].ToString();

                    //firstWorksheet["A" + cellnum].Text = row["CompanyName"].ToString();
                    //firstWorksheet["B" + cellnum].Text = row["Branch"].ToString();
                    //firstWorksheet["C" + cellnum].Text = row["LastName"].ToString();
                    //firstWorksheet["D" + cellnum].Text = row["FirstName"].ToString();
                    //firstWorksheet["E" + cellnum].Text = row["MiddleName"].ToString();
                    //firstWorksheet["F" + cellnum].Text = row["Suffix"].ToString();
                    //firstWorksheet["G" + cellnum].Text = row["CardName"].ToString();
                    //firstWorksheet["H" + cellnum].Text = row["DateHired"].ToString();
                    //firstWorksheet["I" + cellnum].Text = row["PlaceofBirth"].ToString();
                    //firstWorksheet["J" + cellnum].Text = row["Nationality"].ToString();
                    //firstWorksheet["K" + cellnum].Text = row["NationalityCountry"].ToString();
                    //firstWorksheet["L" + cellnum].Text = row["Gender"].ToString();
                    //firstWorksheet["M" + cellnum].Text = row["MobileNumber"].ToString();
                    //firstWorksheet["N" + cellnum].Text = row["LandlineNumber"].ToString();
                    //firstWorksheet["O" + cellnum].Text = row["HouseNumber"].ToString();
                    //firstWorksheet["P" + cellnum].Text = row["StreetrVillageSubdivisionBrgy"].ToString();
                    //firstWorksheet["Q" + cellnum].Text = row["Country"].ToString();
                    //firstWorksheet["R" + cellnum].Text = row["Region"].ToString();
                    //firstWorksheet["S" + cellnum].Text = row["Province"].ToString();
                    //firstWorksheet["T" + cellnum].Text = row["CityMunicipality"].ToString();
                    //firstWorksheet["U" + cellnum].Text = row["ZipCode"].ToString();
                    //firstWorksheet["V" + cellnum].Text = row["PermanentHouseNumber"].ToString();
                    //firstWorksheet["W" + cellnum].Text = row["PermanentStreetrVillageSubdivisionBrgy"].ToString();
                    //firstWorksheet["X" + cellnum].Text = row["PermanentCountry"].ToString();
                    //firstWorksheet["Y" + cellnum].Text = row["PermanentRegion"].ToString();
                    //firstWorksheet["Z" + cellnum].Text = row["PermanentProvince"].ToString();
                    //firstWorksheet["AA" + cellnum].Text = row["PermanentCityMunicipality"].ToString();
                    //firstWorksheet["AB" + cellnum].Text = row["PermanentZipCode"].ToString();
                    //firstWorksheet["AC" + cellnum].Text = row["EmailAddress"].ToString();
                    //firstWorksheet["AD" + cellnum].Text = row["CompanyName"].ToString();
                    //firstWorksheet["AE" + cellnum].Text = row["EmployeeTIN"].ToString();
                    //firstWorksheet["AF" + cellnum].Text = row["CivilStatus"].ToString();
                    //firstWorksheet["AG" + cellnum].Text = row["MotherLastName"].ToString();
                    //firstWorksheet["AH" + cellnum].Text = row["MotherFirstName"].ToString();
                    //firstWorksheet["AI" + cellnum].Text = row["MotherMiddleName"].ToString();
                    //firstWorksheet["AJ" + cellnum].Text = row["MotherSuffix"].ToString();
                    //firstWorksheet["AK" + cellnum].Text = row["NumberofPads"].ToString();
                    //firstWorksheet["AL" + cellnum].Text = row["OtherDetails"].ToString();
                    //firstWorksheet["AM" + cellnum].Text = row["Position"].ToString();
                    //firstWorksheet["AN" + cellnum].Text = row["DateHired"].ToString();
                    //firstWorksheet["AO" + cellnum].Text = row["EmploymentStatus"].ToString();
                    //firstWorksheet["AP" + cellnum].Text = row["EndofContract"].ToString();
                    ////firstWorksheet[AO" + cellnum].Text = row["RefOneFirstName"].ToString();
                    ////firstWorksheet["AP" + cellnum].Text = row["RefOneMiddleName"].ToString();
                    ////firstWorksheet["AQ" + cellnum].Text = row["RefOneLastName"].ToString();
                    //firstWorksheet["AQ" + cellnum].Text = row["RefOneName"].ToString();
                    //firstWorksheet["AR" + cellnum].Text = row["RefOneMobileNumber"].ToString();
                    //firstWorksheet["AS" + cellnum].Text = row["RefTwoName"].ToString();
                    ////firstWorksheet["AS" + cellnum].Text = row["RefTwoFirstName"].ToString();
                    ////firstWorksheet["AT" + cellnum].Text = row["RefTwoMiddleName"].ToString();
                    ////firstWorksheet["AU" + cellnum].Text = row["RefTwoLastName"].ToString();
                    //firstWorksheet["AT" + cellnum].Text = row["RefTwoMobileNumber"].ToString();

                    cellnum++;
                }

                //https://http://ws.durusthr.com/ILM_WS_LIVE/sFTP/CrtSampleFileExport_12-41-PM.xls
                HttpResponse httpResponse = System.Web.HttpContext.Current.Response;

                httpResponse.Clear();
                httpResponse.ContentType = "Application/x-msexcel";
                httpResponse.AddHeader("Content-Disposition", String.Format("attachment; filename={0}", filename));
                string newDIR = HttpContext.Current.Server.MapPath("~/sFTP/");
                string onlineDIR = "http://ws.durusthr.com/ilm_ws_live/sFTP/";

                workbook.Save(newDIR + filename);

                //return "Success";
                return onlineDIR + filename;
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }
        [HttpPost]
        [Route("CRT_FilterUserv2")]
        public List<FilteredUsers> NewSearchFilterv2(SearchFilterParams searchUser)
        {

            DataTable filterUser1 = new DataTable();
            DataTable filterUser = new DataTable();

            filterUser1.Columns.AddRange(new DataColumn[1] { new DataColumn("site") });
            filterUser.Columns.AddRange(new DataColumn[1] { new DataColumn("empID") });
            for (int i = 0; i < searchUser.empID.Length; i++)
            {
                filterUser.Rows.Add(searchUser.empID[i]);
            }

            for (int i = 0; i < searchUser.site.Length; i++)
            {
                filterUser1.Rows.Add(searchUser.site[i]);
            }


            List<FilteredUsers> FU = new List<FilteredUsers>();
            tempconnectionstring();
            Connection con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@company", mytype = SqlDbType.VarChar, Value = searchUser.company });
            con.myparameters.Add(new myParameters { ParameterName = "@site", mytype = SqlDbType.Structured, Value = filterUser1 });
            con.myparameters.Add(new myParameters { ParameterName = "@ID", mytype = SqlDbType.Structured, Value = filterUser });
            DataTable DT = con.GetDataTable("sp_CRT_UserSearchv2");
            foreach (DataRow row in DT.Rows)
            {
                FilteredUsers Users = new FilteredUsers()
                {
                    StartingID = row["id"].ToString(),
                    FirstName = row["fn"].ToString(),
                    MiddleName = row["mn"].ToString(),
                    LastName = row["ln"].ToString(),
                    Suffix = row["suffix"].ToString(),
                    DateHired = row["dob"].ToString(),
                    Branch = row["site"].ToString(),
                    AccountType = row["acctype"].ToString(),
                    EmployeeTIN = row["empTIN"].ToString()
                };
                FU.Add(Users);
            }
            return FU;
        }
        ////testing for email alarm for jobs 
        [HttpPost]
        [Route("CRT_ManualUploadAutomationWithUploadAlarm")]
        public string ManualUploadAutomationWithUploadAlarm()
        {
            Stream stream = new MemoryStream();

            try
            {
                string host = @"54.92.167.178";
                string username = "AloricaUser1";
                string password = @"S3cured@123o";

                var emailadd = new List<string>();
                //emailadd.Add("velascomikeec@gmail.com");
                //emailadd.Add("mikee.gameacc@gmail.com");
                emailadd.Add("jsangco@illimitado.com");
                //emailadd.Add("eya@illimitado.com");
                //emailadd.Add("polo@illimitado.com");
                //emailadd.Add("payrollphilippines@alorica.com");
                //emailadd.Add("macristine.manalili@alorica.com");
                emailadd.Add("kenjie031596@outlook.com");
                emailadd.ToArray();

                string remoteDirectory = "/Upload";

                //GET FILENAME FROM SFTP SERVER DIRECTORY
                string retstring = "";
                using (SftpClient sftp = new SftpClient(host, username, password))
                {
                    try
                    {
                        sftp.Connect();

                        var files = sftp.ListDirectory(remoteDirectory);

                        foreach (var file in files)
                        {
                            //Console.WriteLine(file.Name);
                            string file1 = file.Name.ToString();
                            retstring = retstring + "\r\n" + file1;
                        }

                        sftp.Disconnect();
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("An exception has been caught " + e.ToString());
                    }
                }

                String[] filenames1 = retstring.Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);

                var filenames = new List<string>(filenames1);
                filenames.Remove(".");
                filenames.Remove("..");
                filenames.ToArray();

                //CREATE LOCAL FOLDER FOR DOWNLOADED FILES
                //var folder = Path.Combine(HostingEnvironment.MapPath("~/sFTP/"), "Alorica_FTP");
                //Directory.CreateDirectory(folder);

                foreach (string s in filenames)
                {
                    string datebuild = DateTime.Now.ToString("MMddyyyy");
                    string timebuild = DateTime.Now.ToString("hh-mmtt");

                    string nameonly = s.Substring(s.Length - 1, 1).ToLower() == "s" ? s.Substring(0, s.Length - 4) : s.Substring(0, s.Length - 5);
                    string newfilename = "UA_" + nameonly + ".xls";

                    //downloadFilev2(s);
                    //uploadFileV2(s);

                    var xlApp = new Microsoft.Office.Interop.Excel.Application();
                    string dataDir = HostingEnvironment.MapPath("~/sFTP/Alorica_FTP/");

                    var xlWorkbooks = xlApp.Workbooks;
                    var xlWorkbook = xlWorkbooks.Open(dataDir + newfilename);
                    var xlWorksheet = xlWorkbook.Sheets[1];
                    var xlRange = xlWorksheet.UsedRange;
                    int rowCount = xlRange.Rows.Count;
                    int colCount = xlRange.Columns.Count;

                    DataTable UserList = new DataTable();
                    UserList.Columns.AddRange(new DataColumn[10] { new DataColumn("EmpID"), new DataColumn("FName"), new DataColumn("MName"), new DataColumn("LName"), new DataColumn("AccountType"), new DataColumn("TIN"), new DataColumn("Birthdate"), new DataColumn("Suffix"), new DataColumn("Site"), new DataColumn("RowNum") });

                    try
                    {
                        if (rowCount > 1)
                        {
                            for (int i = 2; i <= rowCount; i++)
                            {
                                string middlename = "";
                                string suffix = "";

                                middlename = xlRange.Cells[i, 3].Value == null ? "." : xlRange.Cells[i, 3].Value.ToString();
                                suffix = xlRange.Cells[i, 5].Value == null ? "-" : xlRange.Cells[i, 5].Value.ToString();

                                UserList.Rows.Add
                                (i.ToString(), // rowid
                                    xlRange.Cells[i, 1].Value.ToString(), // empid
                                    xlRange.Cells[i, 2].Value.ToString(), // fname
                                    middlename, // middlename
                                    xlRange.Cells[i, 4].Value.ToString(), //lastname
                                    suffix, // suffix
                                    xlRange.Cells[i, 6].Value.ToString(), //bday
                                    xlRange.Cells[i, 7].Value.ToString(), //branch
                                    xlRange.Cells[i, 9].Value.ToString(), // tin
                                    xlRange.Cells[i, 8].Value.ToString() // account type
                                );
                            }
                            tempconnectionstring();
                            Connection con = new Connection();
                            con.myparameters.Add(new myParameters { ParameterName = "@CompanyName", mytype = SqlDbType.VarChar, Value = "alorica" });
                            con.myparameters.Add(new myParameters { ParameterName = "@IMPORT", mytype = SqlDbType.Structured, Value = UserList });
                            DataTable DT = con.GetDataTable("sp_CRTNotificationUploadAlert");

                            string dataDir2 = HostingEnvironment.MapPath("~/sFTP/Alorica_FTP/manual_upload.xls");
                            stream = new FileStream(dataDir2, FileMode.Open);

                            string filename = "manualupload" + "_" + datebuild + "_" + timebuild + ".xls";
                            //using (var fileStream = new FileStream(dataDir2 + filename, FileMode.Create, FileAccess.Write))
                            //{
                            //    stream.CopyTo(fileStream);
                            //}
                            //stream.Close();

                            ExcelWorkbookFormat workbookFormat = ExcelWorkbookFormat.Xls_2003;
                            string testDocFile = dataDir2;
                            ExcelWorkbook workbook = new ExcelWorkbook(testDocFile);
                            ExcelWorksheet firstWorksheet = workbook.Worksheets["Sheet1"];

                            int cellnum = 2;

                            for (int i = 0; i < DT.Rows.Count; i++)
                            {
                                firstWorksheet["A" + cellnum].Text = DT.Rows[i]["RowID"].ToString();
                                firstWorksheet["B" + cellnum].Text = DT.Rows[i]["StartingID"].ToString();
                                firstWorksheet["C" + cellnum].Text = DT.Rows[i]["Conflict"].ToString();

                                cellnum++;
                            }

                            //string newDIR = HttpContext.Current.Server.MapPath("~/sFTP/Alorica_FTP/");
                            //workbook.Save(newDIR + filename);

                            uint processId = 0;

                            if (xlApp != null)
                            {
                                if (xlApp.Workbooks != null)
                                {
                                    if (xlApp.Workbooks.Count > 0)
                                    {
                                        GetWindowThreadProcessId(new IntPtr(xlApp.Hwnd), out processId);

                                        // All your actions here
                                        xlWorkbook.Close(true, dataDir + newfilename, null);
                                        xlWorkbooks.Close();
                                        xlApp.Workbooks.Close();
                                        xlApp.Quit();

                                    }
                                }
                            }

                            try
                            {
                                if (processId != 0)
                                {
                                    Process excelProcess = Process.GetProcessById((int)processId);
                                    excelProcess.CloseMainWindow();
                                    excelProcess.Refresh();
                                    excelProcess.Kill();
                                }
                            }
                            catch
                            {
                                // Process was already killed
                            }

                            foreach (var email in emailadd)
                            {
                                string emailaccount = "durusthr@illimitado.com";
                                string emailpassword = "@1230Qwerty";
                                string prefix = HttpContext.Current.Server.MapPath("~/sFTP/Alorica_FTP/");
                                using (MailMessage mm = new MailMessage(emailaccount, email))
                                {
                                    MailAddress aliasmail = new MailAddress("durusthr@illimitado.com", "durusthr@illimitado.com");
                                    MailAddress aliasreplymail = new MailAddress("durusthr@illimitado.com");
                                    mm.From = aliasmail;
                                    mm.Subject = "Notifications";
                                    mm.Body = "Successfully uploaded " + (rowCount - 1).ToString() + " records on " + DateTime.Now.ToString("dddd, dd MMMM yyyy hh:mm tt");

                                    //if (DT.Rows.Count >= 1)
                                    //{
                                    //    mm.Body = "We are unable to upload the file due to conflicts. Please see attachment for detailed info.";
                                    //    mm.Attachments.Add(new Attachment(prefix + filename));
                                    //}

                                    //mm.Attachments.Add(new Attachment(newDIR + filename));
                                    mm.IsBodyHtml = false;
                                    SmtpClient smtp = new SmtpClient();
                                    smtp.Host = "box1256.bluehost.com";
                                    smtp.EnableSsl = true;
                                    NetworkCredential NetworkCred = new NetworkCredential(emailaccount, emailpassword);
                                    smtp.UseDefaultCredentials = true;
                                    smtp.Credentials = NetworkCred;
                                    smtp.Port = 587;
                                    smtp.Send(mm);
                                }
                            }

                            //DELETE FILE ON SFTP
                            //deleteFileV2(s);
                            stream.Close();
                        }
                        else
                        {
                            //dito yung lumang foearch
                            uint processId = 0;

                            if (xlApp != null)
                            {
                                if (xlApp.Workbooks != null)
                                {
                                    if (xlApp.Workbooks.Count > 0)
                                    {
                                        GetWindowThreadProcessId(new IntPtr(xlApp.Hwnd), out processId);

                                        // All your actions here
                                        xlWorkbook.Close(true, dataDir + newfilename, null);
                                        xlWorkbooks.Close();
                                        xlApp.Workbooks.Close();
                                        xlApp.Quit();

                                    }
                                }
                            }

                            try
                            {
                                if (processId != 0)
                                {
                                    Process excelProcess = Process.GetProcessById((int)processId);
                                    excelProcess.CloseMainWindow();
                                    excelProcess.Refresh();
                                    excelProcess.Kill();
                                }
                            }
                            catch
                            {
                                // Process was already killed
                            }

                            // DELETE FILE ON SFTP
                            //deleteFileV2(s);
                        }
                        stream.Close();
                        return "success";

                    }
                    //dito
                    catch
                    {
                        foreach (var email in emailadd)
                        {
                            string emailaccount = "durusthr@illimitado.com";
                            string emailpassword = "@1230Qwerty";
                            string prefix = HttpContext.Current.Server.MapPath("~/sFTP/Alorica_FTP/");
                            using (MailMessage mm = new MailMessage(emailaccount, email))
                            {
                                MailAddress aliasmail = new MailAddress("durusthr@illimitado.com", "durusthr@illimitado.com");
                                MailAddress aliasreplymail = new MailAddress("durusthr@illimitado.com");
                                mm.From = aliasmail;
                                mm.Subject = "Notifications";
                                mm.Body = "No file uploaded";
                                //mm.Attachments.Add(new Attachment(newDIR + filename));
                                mm.IsBodyHtml = false;
                                SmtpClient smtp = new SmtpClient();
                                smtp.Host = "box1256.bluehost.com";
                                smtp.EnableSsl = true;
                                NetworkCredential NetworkCred = new NetworkCredential(emailaccount, emailpassword);
                                smtp.UseDefaultCredentials = true;
                                smtp.Credentials = NetworkCred;
                                smtp.Port = 587;
                                smtp.Send(mm);
                            }
                        }
                    }
                }
                stream.Close();
                return "No Upload";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }
        [HttpPost]
        [Route("crt_Tst123")]
        public string test123()
        {
            try
            {
                string body = string.Empty;
                //using streamreader for reading my htmltemplate   

                using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/sFTP/PendingLogsEmail.html")))

                {

                    body = reader.ReadToEnd();

                }


                string emailss = "noreakram22@gmail.com"; // "ilm.customerfirst@gmail.com";
                string approvername = "noreakram22@gmail.com";
                body = body.Replace("[ApproverName]", approvername); //replacing the required things  
                body = body.Replace("[EmployeeName]", "noreakram22@gmail.com");
                body = body.Replace("[Request]", "Leave");
                string leavedates = "";

                leavedates = "noreakram22@gmail.com";


                body = "noreakram22@gmail.com";

                var message = new MimeKit.MimeMessage();

                //MimeKit.InternetAddressList Elist = new MimeKit.InternetAddressList();
                //foreach (string emailadd in emaillist)
                //{
                //    Elist.Add(new MimeKit.MailboxAddress(emailadd));
                //}

                message.From.Add(new MimeKit.MailboxAddress("durusthr@illimitado.com"));
                //message.To.AddRange(Elist);
                message.To.Add(new MimeKit.MailboxAddress(emailss));
                message.Subject = "Leave Request Notification";
                message.Body = new MimeKit.TextPart("html") { Text = body };

                using (var client = new MailKit.Net.Smtp.SmtpClient())
                {
                    client.ServerCertificateValidationCallback = (s, c, h, e) => true;
                    client.Connect("smtp.office365.com", 587, false);
                    client.Authenticate("durusthr@illimitado.com", "@1230Qwerty");
                    client.Send(message);
                    client.Disconnect(true);
                }

            }

            catch (Exception ex)
            {
                return ex.ToString();
            }

            return "qwert@box1256.bluehost.com";
        }

        /// Manual Upload tatanggapin kahit may mali 
        /// Author Brandon 
        [HttpPost]
        [Route("CRT_ManualUploadAutomationV3")]
        public string ManualUploadAutomationV3()
        {
            Stream stream = new MemoryStream();

            try
            {
                string host = @"54.92.167.178";
                string username = "AloricaUser1";
                string password = @"S3cured@123o";

                var emailadd = new List<string>();
                //emailadd.Add("velascomikeec@gmail.com");
                //emailadd.Add("mikee.gameacc@gmail.com");
                emailadd.Add("jsangco@illimitado.com");
                //emailadd.Add("eya@illimitado.com");
                //emailadd.Add("polo@illimitado.com");
                //emailadd.Add("payrollphilippines@alorica.com");
                //emailadd.Add("macristine.manalili@alorica.com");
                emailadd.Add("kenjie031596@outlook.com");
                //emailadd.Add("Michael.Figueroa@alorica.com");
                //emailadd.Add("Monchito.Dantes@alorica.com");
                //emailadd.Add("bharat@illimitado.com");
                //emailadd.Add("PayrollPhilippines@alorica.com");
                //emailadd.Add("noreakram22@gmail.com");
                //emailadd.ToArray();

                string zoneid = "Singapore Standard Time";
                TimeZoneInfo tzi = TimeZoneInfo.FindSystemTimeZoneById(zoneid);
                DateTime timeresult = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, tzi);

                //return "converted time: " + timeresult;

                string remoteDirectory = "/Upload";

                //GET FILENAME FROM SFTP SERVER DIRECTORY
                string retstring = "";
                using (SftpClient sftp = new SftpClient(host, username, password))
                {
                    try
                    {
                        sftp.Connect();

                        var files = sftp.ListDirectory(remoteDirectory);

                        foreach (var file in files)
                        {
                            //Console.WriteLine(file.Name);
                            string file1 = file.Name.ToString();
                            retstring = retstring + "\r\n" + file1;
                        }

                        sftp.Disconnect();
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("An exception has been caught " + e.ToString());
                    }
                }

                String[] filenames1 = retstring.Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);

                var filenames = new List<string>(filenames1);
                filenames.Remove(".");
                filenames.Remove("..");
                filenames.ToArray();

                //CREATE LOCAL FOLDER FOR DOWNLOADED FILES
                var folder = Path.Combine(HostingEnvironment.MapPath("~/sFTP/"), "Alorica_FTP");
                Directory.CreateDirectory(folder);

                foreach (string s in filenames)
                {
                    string datebuild = DateTime.Now.ToString("MMddyyyy");
                    string timebuild = DateTime.Now.ToString("hh-mmtt");

                    string nameonly = s.Substring(s.Length - 1, 1).ToLower() == "s" ? s.Substring(0, s.Length - 4) : s.Substring(0, s.Length - 5);
                    string newfilename = "UA_" + nameonly + ".xls";

                    downloadFilev2(s);
                    uploadFileV2(s);

                    var xlApp = new Microsoft.Office.Interop.Excel.Application();
                    string dataDir = HostingEnvironment.MapPath("~/sFTP/Alorica_FTP/");

                    var xlWorkbooks = xlApp.Workbooks;
                    var xlWorkbook = xlWorkbooks.Open(dataDir + newfilename);
                    var xlWorksheet = xlWorkbook.Sheets[1];
                    var xlRange = xlWorksheet.UsedRange;
                    int rowCount = xlRange.Rows.Count;
                    int colCount = xlRange.Columns.Count;

                    DataTable UserList = new DataTable();
                    UserList.Columns.AddRange(new DataColumn[10] { new DataColumn("EmpID"), new DataColumn("FName"), new DataColumn("MName"), new DataColumn("LName"), new DataColumn("AccountType"), new DataColumn("TIN"), new DataColumn("Birthdate"), new DataColumn("Suffix"), new DataColumn("Site"), new DataColumn("RowNum") });

                    if (rowCount > 1)
                    {
                        for (int i = 2; i <= rowCount; i++)
                        {
                            string middlename = "";
                            string suffix = "";

                            middlename = xlRange.Cells[i, 3].Value == null ? "." : xlRange.Cells[i, 3].Value.ToString();
                            suffix = xlRange.Cells[i, 5].Value == null ? "-" : xlRange.Cells[i, 5].Value.ToString();

                            UserList.Rows.Add
                            (i.ToString(), // rowid
                                xlRange.Cells[i, 1].Value.ToString(), // empid
                                xlRange.Cells[i, 2].Value.ToString(), // fname
                                middlename, // middlename
                                xlRange.Cells[i, 4].Value.ToString(), //lastname
                                suffix, // suffix
                                xlRange.Cells[i, 6].Value.ToString(), //bday
                                xlRange.Cells[i, 7].Value.ToString(), //branch
                                xlRange.Cells[i, 9].Value.ToString(), // tin
                                xlRange.Cells[i, 8].Value.ToString() // account type
                            );
                        }
                        tempconnectionstring();
                        Connection con = new Connection();
                        con.myparameters.Add(new myParameters { ParameterName = "@CompanyName", mytype = SqlDbType.VarChar, Value = "alorica" });
                        con.myparameters.Add(new myParameters { ParameterName = "@IMPORT", mytype = SqlDbType.Structured, Value = UserList });
                        DataTable DT = con.GetDataTable("sp_CRT_ManualUploadAutomation");

                        string dataDir2 = HostingEnvironment.MapPath("~/sFTP/Alorica_FTP/manual_upload.xls");
                        stream = new FileStream(dataDir2, FileMode.Open);

                        string filename = "manualupload" + "_" + datebuild + "_" + timebuild + ".xls";
                        using (var fileStream = new FileStream(dataDir2 + filename, FileMode.Create, FileAccess.Write))
                        {
                            stream.CopyTo(fileStream);
                        }
                        stream.Close();

                        ExcelWorkbookFormat workbookFormat = ExcelWorkbookFormat.Xls_2003;
                        string testDocFile = dataDir2;
                        ExcelWorkbook workbook = new ExcelWorkbook(testDocFile);
                        ExcelWorksheet firstWorksheet = workbook.Worksheets["Sheet1"];

                        int cellnum = 2;

                        for (int i = 0; i < DT.Rows.Count; i++)
                        {
                            firstWorksheet["A" + cellnum].Text = DT.Rows[i]["RowID"].ToString();
                            firstWorksheet["B" + cellnum].Text = DT.Rows[i]["StartingID"].ToString();
                            firstWorksheet["C" + cellnum].Text = DT.Rows[i]["Conflict"].ToString();

                            cellnum++;
                        }

                        string newDIR = HttpContext.Current.Server.MapPath("~/sFTP/Alorica_FTP/");
                        workbook.Save(newDIR + filename);

                        uint processId = 0;

                        if (xlApp != null)
                        {
                            if (xlApp.Workbooks != null)
                            {
                                if (xlApp.Workbooks.Count > 0)
                                {
                                    GetWindowThreadProcessId(new IntPtr(xlApp.Hwnd), out processId);

                                    // All your actions here
                                    xlWorkbook.Close(true, dataDir + newfilename, null);
                                    xlWorkbooks.Close();
                                    xlApp.Workbooks.Close();
                                    xlApp.Quit();

                                }
                            }
                        }

                        try
                        {
                            if (processId != 0)
                            {
                                Process excelProcess = Process.GetProcessById((int)processId);
                                excelProcess.CloseMainWindow();
                                excelProcess.Refresh();
                                excelProcess.Kill();
                            }
                        }
                        catch
                        {
                            // Process was already killed
                        }

                        foreach (var email in emailadd)
                        {
                            string emailaccount = "durusthr@illimitado.com";
                            string emailpassword = "@1230Qwerty";
                            string prefix = HttpContext.Current.Server.MapPath("~/sFTP/Alorica_FTP/");
                            using (MailMessage mm = new MailMessage(emailaccount, email))
                            {
                                MailAddress aliasmail = new MailAddress("durusthr@illimitado.com", "durusthr@illimitado.com");
                                MailAddress aliasreplymail = new MailAddress("durusthr@illimitado.com");
                                mm.From = aliasmail;
                                mm.Subject = "Notifications";
                                //mm.Body = "Successfully uploaded " + (rowCount - 1).ToString() + " records on " + DateTime.Now.ToString("dddd, dd MMMM yyyy hh:mm tt");
                                mm.Body = "Successfully uploaded " + (rowCount - 1).ToString() + " records on " + timeresult;

                                if (DT.Rows.Count >= 1)
                                {
                                    mm.Body = "We are unable to upload the file due to conflicts. Please see attachment for detailed info.";
                                    mm.Attachments.Add(new Attachment(prefix + filename));
                                }

                                //mm.Attachments.Add(new Attachment(newDIR + filename));
                                mm.IsBodyHtml = false;
                                SmtpClient smtp = new SmtpClient();
                                smtp.Host = "box1256.bluehost.com";
                                smtp.EnableSsl = true;
                                NetworkCredential NetworkCred = new NetworkCredential(emailaccount, emailpassword);
                                smtp.UseDefaultCredentials = true;
                                smtp.Credentials = NetworkCred;
                                smtp.Port = 587;
                                smtp.Send(mm);
                            }
                        }

                        //DELETE FILE ON SFTP
                        deleteFileV2(s);
                        stream.Close();
                    }
                    else
                    {
                        foreach (var email in emailadd)
                        {
                            string emailaccount = "durusthr@illimitado.com";
                            string emailpassword = "@1230Qwerty";
                            string prefix = HttpContext.Current.Server.MapPath("~/sFTP/Alorica_FTP/");
                            using (MailMessage mm = new MailMessage(emailaccount, email))
                            {
                                MailAddress aliasmail = new MailAddress("durusthr@illimitado.com", "durusthr@illimitado.com");
                                MailAddress aliasreplymail = new MailAddress("durusthr@illimitado.com");
                                mm.From = aliasmail;
                                mm.Subject = "Notifications";
                                mm.Body = "The file uploaded has no records.";
                                //mm.Attachments.Add(new Attachment(newDIR + filename));
                                mm.IsBodyHtml = false;
                                SmtpClient smtp = new SmtpClient();
                                smtp.Host = "box1256.bluehost.com";
                                smtp.EnableSsl = true;
                                NetworkCredential NetworkCred = new NetworkCredential(emailaccount, emailpassword);
                                smtp.UseDefaultCredentials = true;
                                smtp.Credentials = NetworkCred;
                                smtp.Port = 587;
                                smtp.Send(mm);
                            }
                        }

                        uint processId = 0;

                        if (xlApp != null)
                        {
                            if (xlApp.Workbooks != null)
                            {
                                if (xlApp.Workbooks.Count > 0)
                                {
                                    GetWindowThreadProcessId(new IntPtr(xlApp.Hwnd), out processId);

                                    // All your actions here
                                    xlWorkbook.Close(true, dataDir + newfilename, null);
                                    xlWorkbooks.Close();
                                    xlApp.Workbooks.Close();
                                    xlApp.Quit();

                                }
                            }
                        }

                        try
                        {
                            if (processId != 0)
                            {
                                Process excelProcess = Process.GetProcessById((int)processId);
                                excelProcess.CloseMainWindow();
                                excelProcess.Refresh();
                                excelProcess.Kill();
                            }
                        }
                        catch
                        {
                            // Process was already killed
                        }

                        // DELETE FILE ON SFTP
                        deleteFileV2(s);
                    }
                }
                stream.Close();
                return "success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }
        [HttpPost]
        [Route("CRT_ManualUploadAutomationV6")]
        public string ManualUploadAutomationV7()
        {
            Stream stream = new MemoryStream();

            try
            {
                string host = @"34.202.217.120";
                string username = "AloricaUser1";
                string password = @"S3cured@123o";

                var emailadd = new List<string>();
                emailadd.Add("velascomikeec@gmail.com");
                emailadd.Add("mikee.gameacc@gmail.com");
                emailadd.Add("jsangco@illimitado.com");
                emailadd.Add("eya@illimitado.com");
                emailadd.Add("polo@illimitado.com");
                emailadd.Add("payrollphilippines@alorica.com");
                emailadd.Add("macristine.manalili@alorica.com");
                emailadd.Add("kenjie031596@outlook.com");
                emailadd.Add("Michael.Figueroa@alorica.com");
                emailadd.Add("Monchito.Dantes@alorica.com");
                emailadd.Add("bharat@illimitado.com");
                emailadd.Add("PayrollPhilippines@alorica.com");
                emailadd.Add("noreakram22@gmail.com");
                emailadd.Add("jpetre@illimitado.com");
                emailadd.ToArray();

                //return "converted time: " + timeresult;

                string remoteDirectory = "/Upload";

                //GET FILENAME FROM SFTP SERVER DIRECTORY
                string retstring = "";
                using (SftpClient sftp = new SftpClient(host, username, password))
                {
                    try
                    {
                        sftp.Connect();

                        var files = sftp.ListDirectory(remoteDirectory);

                        foreach (var file in files)
                        {
                            //Console.WriteLine(file.Name);
                            string file1 = file.Name.ToString();
                            retstring = retstring + "\r\n" + file1;
                        }

                        sftp.Disconnect();
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("An exception has been caught " + e.ToString());
                    }
                }

                String[] filenames1 = retstring.Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);
                string nameonly;

                var filenames = new List<string>(filenames1);
                filenames.Remove(".");
                filenames.Remove("..");
                filenames.ToArray();
                string nameFile = filenames[0].ToString();
                //CREATE LOCAL FOLDER FOR DOWNLOADED FILES
                var folder = Path.Combine(HostingEnvironment.MapPath("~/sFTP/"), "Alorica_FTP");
                Directory.CreateDirectory(folder);

                foreach (string s in filenames)
                {
                    string datebuild = DateTime.Now.ToString("MMddyyyy");
                    //string timebuild = DateTime.Now.ToString("hh-mmtt");

                    string zoneid = "Singapore Standard Time";
                    TimeZoneInfo tzi = TimeZoneInfo.FindSystemTimeZoneById(zoneid);
                    DateTime timeresult = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, tzi);

                    nameonly = s.Substring(s.Length - 1, 1).ToLower() == "s" ? s.Substring(0, s.Length - 4) : s.Substring(0, s.Length - 5);
                    string newfilename = "UA_" + nameonly + ".xls";

                    downloadFilev2(s);
                    uploadFileV2(s);

                    var ds = new DataSet();
                    string dataDir = HostingEnvironment.MapPath("~/sFTP/Alorica_FTP/");
                    var fileUpload = dataDir + newfilename;
                    var connectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + fileUpload + ";Extended Properties=\"Excel 12.0;IMEX=1;HDR=NO;TypeGuessRows=0;ImportMixedTypes=Text\""; ;
                    using (var conn = new OleDbConnection(connectionString))
                    {
                        conn.Open();

                        var sheets = conn.GetOleDbSchemaTable(System.Data.OleDb.OleDbSchemaGuid.Tables, new object[] { null, null, null, "TABLE" });
                        using (var cmd = conn.CreateCommand())
                        {
                            cmd.CommandText = "SELECT * FROM [" + sheets.Rows[0]["TABLE_NAME"].ToString() + "] ";

                            var adapter = new OleDbDataAdapter(cmd);

                            adapter.Fill(ds);


                        }
                        int rowCount = ds.Tables[0].Rows.Count;
                        int successCount = 0;
                        int conflictCount = 0;
                        int userConflict = 0;
                        DataTable UserList = new DataTable();
                        UserList.Columns.AddRange(new DataColumn[10] { new DataColumn("EmpID"), new DataColumn("FName"), new DataColumn("MName"), new DataColumn("LName"), new DataColumn("AccountType"), new DataColumn("TIN"), new DataColumn("Birthdate"), new DataColumn("Suffix"), new DataColumn("Site"), new DataColumn("RowNum") });
                        DataRow dRow;

                        if (rowCount > 1)
                        {
                            for (int i = 1; i < rowCount; i++)
                            {
                                dRow = ds.Tables[0].Rows[i];
                                //string a = dRow.ItemArray.GetValue(1).ToString();
                                string middlename = "";
                                string suffix = "";

                                middlename = dRow.ItemArray.GetValue(2) == null ? "." : dRow.ItemArray.GetValue(2).ToString();
                                suffix = dRow.ItemArray.GetValue(4) == null ? "-" : dRow.ItemArray.GetValue(4).ToString();

                                UserList.Rows.Add
                                (i.ToString(), // rowid
                                    dRow.ItemArray.GetValue(0).ToString(), // empid
                                    dRow.ItemArray.GetValue(1).ToString(), // fname
                                    middlename, // middlename
                                    dRow.ItemArray.GetValue(3).ToString(), //lastname
                                    suffix, // suffix
                                    dRow.ItemArray.GetValue(5).ToString(), //bday
                                    dRow.ItemArray.GetValue(6).ToString(), //branch
                                    dRow.ItemArray.GetValue(8).ToString(), // tin
                                    dRow.ItemArray.GetValue(7).ToString() // account type
                                );
                            }
                            tempconnectionstring();
                            Connection con = new Connection();
                            con.myparameters.Add(new myParameters { ParameterName = "@CompanyName", mytype = SqlDbType.VarChar, Value = "alorica" });
                            con.myparameters.Add(new myParameters { ParameterName = "@IMPORT", mytype = SqlDbType.Structured, Value = UserList });
                            DataTable DT = con.GetDataTable("sp_CRT_ManualUploadAutomationv3");

                            string dataDir2 = HostingEnvironment.MapPath("~/sFTP/Alorica_FTP/manual_upload.xls");
                            stream = new FileStream(dataDir2, FileMode.Open);

                            string filename = "manualupload" + "_" + datebuild + "_" + timeresult.ToString("HHmmsstt") + ".xls";
                            using (var fileStream = new FileStream(Path.Combine(dataDir2 + filename), FileMode.Create, FileAccess.Write))
                            {
                                stream.CopyTo(fileStream);
                            }
                            stream.Close();

                            ExcelWorkbookFormat workbookFormat = ExcelWorkbookFormat.Xls_2003;
                            string testDocFile = dataDir2;
                            ExcelWorkbook workbook = new ExcelWorkbook(testDocFile);
                            ExcelWorksheet firstWorksheet = workbook.Worksheets["Sheet1"];

                            int cellnum = 2;

                            for (int i = 0; i < DT.Rows.Count; i++)
                            {
                                string a = DT.Rows[i]["Conflict"].ToString();

                                if (a == "success")
                                {
                                    // successCount =Int32.Parse(DT.Rows[i]["StartingID"].ToString());
                                    successCount++;
                                }

                                else if (a == "distinct")
                                {
                                    userConflict = Int32.Parse(DT.Rows[i]["StartingID"].ToString());
                                }

                                else
                                {
                                    firstWorksheet["A" + cellnum].Text = DT.Rows[i]["RowID"].ToString();
                                    firstWorksheet["B" + cellnum].Text = DT.Rows[i]["StartingID"].ToString();
                                    firstWorksheet["C" + cellnum].Text = DT.Rows[i]["Conflict"].ToString();
                                    conflictCount++;
                                    cellnum++;
                                }



                            }

                            string newDIR = HttpContext.Current.Server.MapPath("~/sFTP/Alorica_FTP/");
                            workbook.Save(newDIR + filename);





                            int c = rowCount - (userConflict + 1);
                            foreach (var email in emailadd)
                            {
                                //time stamp for email
                                string zoneidforemail = "Singapore Standard Time";
                                TimeZoneInfo tziforemail = TimeZoneInfo.FindSystemTimeZoneById(zoneidforemail);
                                DateTime timeresultforemail = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, tziforemail);

                                // string emailaccount = "durusthr@illimitado.com";
                                // string emailpassword = "Secured@1230";
                                string emailaccount = "durusthr@illimitado.com";
                                string emailpassword = "@1230Qwerty";
                                string prefix = HttpContext.Current.Server.MapPath("~/sFTP/Alorica_FTP/");
                                using (MailMessage mm = new MailMessage(emailaccount, email))
                                {
                                    MailAddress aliasmail = new MailAddress("durusthr@illimitado.com", "durusthr@illimitado.com");
                                    MailAddress aliasreplymail = new MailAddress("durusthr@illimitado.com");
                                    mm.From = aliasmail;
                                    mm.Subject = "Notifications";
                                    //mm.Body = "Successfully uploaded " + (rowCount - 1).ToString() + " records on " + DateTime.Now.ToString("dddd, dd MMMM yyyy hh:mm tt");
                                    mm.Body = "Uploaded " + c + "  record/s. " + userConflict + " user/s with conflict/s in file: '" + s + "'" + " on" + timeresultforemail + ".";

                                    if (successCount >= 1 && conflictCount >= 1)
                                    {

                                        mm.Body = "Uploaded " + c + "  record/s. " + userConflict + " user/s with conflict/s in file: '" + s + "' on " + timeresultforemail + "." + "Please see attached for more details.";
                                        mm.Attachments.Add(new Attachment(prefix + filename));
                                    }
                                    else if (successCount >= 1 && conflictCount < 1)
                                    {
                                        mm.Body = "Uploaded " + c + "  record/s. " + userConflict + " user/s with conflict/s in file: '" + s + "' on " + timeresultforemail + ".";
                                        //mm.Attachments.Add(new Attachment(prefix + filename));
                                    }
                                    else
                                    {
                                        mm.Body = "Uploaded " + c + "  record/s. " + userConflict + " user/s with conflict/s in file: '" + s + "' on " + timeresultforemail + ". Please see attached for more details.";
                                        mm.Attachments.Add(new Attachment(prefix + filename));
                                    }

                                    //mm.Attachments.Add(new Attachment(newDIR + filename));
                                    mm.IsBodyHtml = false;
                                    SmtpClient smtp = new SmtpClient();
                                    smtp.Host = "smtp.office365.com";
                                    smtp.EnableSsl = true;
                                    NetworkCredential NetworkCred = new NetworkCredential(emailaccount, emailpassword);
                                    smtp.UseDefaultCredentials = true;
                                    smtp.Credentials = NetworkCred;
                                    smtp.Port = 587;
                                    smtp.Send(mm);
                                }
                            }

                            //DELETE FILE ON SFTP
                            deleteFileV2(s);
                            stream.Close();
                        }
                        else
                        {
                            foreach (var email in emailadd)
                            {
                                //time stamp for email
                                string zoneidforemail = "Singapore Standard Time";
                                TimeZoneInfo tziforemail = TimeZoneInfo.FindSystemTimeZoneById(zoneidforemail);
                                DateTime timeresultforemail = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, tziforemail);

                                string emailaccount = "durusthr@illimitado.com";
                                string emailpassword = "@1230Qwerty";
                                string prefix = HttpContext.Current.Server.MapPath("~/sFTP/Alorica_FTP/");
                                using (MailMessage mm = new MailMessage(emailaccount, email))
                                {
                                    MailAddress aliasmail = new MailAddress("durusthr@illimitado.com", "durusthr@illimitado.com");
                                    MailAddress aliasreplymail = new MailAddress("durusthr@illimitado.com");
                                    mm.From = aliasmail;
                                    mm.Subject = "Notifications";
                                    mm.Body = "The file " + s + " uploaded on " + timeresultforemail + " has no records. ";
                                    //mm.Attachments.Add(new Attachment(newDIR + filename));
                                    mm.IsBodyHtml = false;
                                    SmtpClient smtp = new SmtpClient();
                                    smtp.Host = "smtp.office365.com";
                                    smtp.EnableSsl = true;
                                    NetworkCredential NetworkCred = new NetworkCredential(emailaccount, emailpassword);
                                    smtp.UseDefaultCredentials = true;
                                    smtp.Credentials = NetworkCred;
                                    smtp.Port = 587;
                                    smtp.Send(mm);
                                }
                            }


                            // DELETE FILE ON SFTP
                            deleteFileV2(s);
                        }
                    }
                    stream.Close();

                }
            }
            catch (Exception e)
            {
                return e.ToString();
            }
            return "success";
        }
    }
}
