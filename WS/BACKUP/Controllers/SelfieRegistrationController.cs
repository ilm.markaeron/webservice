﻿using illimitadoWepAPI.Models;
using illimitadoWepAPI.MyClass;
using Microsoft.ProjectOxford.Face;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Hosting;
using System.Web.Http;

namespace illimitadoWepAPI.Controllers
{
    public class SelfieRegistrationController : ApiController
    {



        [HttpPost]
        [Route("UploadProfile")]
        public async Task<HttpResponseMessage> UploadProfile()
        {
            HttpRequest httpRequest = HttpContext.Current.Request;

            BinaryReader binaryReader = new BinaryReader(httpRequest.Files[0].InputStream);

            string NTID = httpRequest.Form["NTID"];
            //string Name = httpRequest.Form["Name"];
            byte[] Photo = binaryReader.ReadBytes(httpRequest.Files[0].ContentLength);
            string Set = httpRequest.Form["Set"];

            HttpResponseMessage msg;

            //var path = HttpContext.Current.Server.MapPath("~/Profile/");
            var path = HttpContext.Current.Server.MapPath("~/ProfilePhotos/");
            //var path = "http://uat-vm.southeastasia.cloudapp.azure.com:81/hrms_test/ProfilePhotos";
            var folder = Path.Combine(path, NTID);
            Directory.CreateDirectory(folder);

            MemoryStream stream = new MemoryStream();

            try
            {
                using (stream)
                {
                    File.WriteAllBytes(folder + "/" + NTID + "_pic" + Set + ".png", Photo);
                }

                msg = Request.CreateResponse(HttpStatusCode.Accepted);
            }
            catch
            {
                msg = Request.CreateResponse(HttpStatusCode.NotAcceptable);
            }

            return msg;
        }

        [HttpPost]
        [Route("UploadProfilev2")]
        public async Task<HttpResponseMessage> UploadProfilev2()
        {
            HttpRequest httpRequest = HttpContext.Current.Request;

            BinaryReader binaryReader = new BinaryReader(httpRequest.Files[0].InputStream);

            string NTID = httpRequest.Form["NTID"];
            string COMPANYNANME = httpRequest.Form["COMPANYNAME"];
            //string Name = httpRequest.Form["Name"];
            byte[] Photo = binaryReader.ReadBytes(httpRequest.Files[0].ContentLength);
            string Set = httpRequest.Form["Set"];

            HttpResponseMessage msg;

            //var path = HttpContext.Current.Server.MapPath("~/Profile/");
            var path = HttpContext.Current.Server.MapPath("~/ProfilePhotos/");
            //var path = "http://uat-vm.southeastasia.cloudapp.azure.com:81/hrms_test/ProfilePhotos";
            var folder = Path.Combine(path, COMPANYNANME.ToLower());
            var savePath = HttpContext.Current.Server.MapPath("~/ProfilePhotos/" + COMPANYNANME.ToLower() + "/");
            var saveFolder = Path.Combine(savePath, NTID);
            Directory.CreateDirectory(folder);
            Directory.CreateDirectory(saveFolder);

            MemoryStream stream = new MemoryStream();

            try
            {
                using (stream)
                {
                    File.WriteAllBytes(saveFolder + "/" + NTID + "_pic" + Set + ".png", Photo);
                }

                msg = Request.CreateResponse(HttpStatusCode.Accepted);
            }
            catch
            {
                msg = Request.CreateResponse(HttpStatusCode.NotAcceptable);
            }

            return msg;
        }

        [HttpPost]
        [Route("UploadProfilev3")]
        public async Task<HttpResponseMessage> UploadProfilev3()
        {
            HttpRequest httpRequest = HttpContext.Current.Request;

            BinaryReader binaryReader = new BinaryReader(httpRequest.Files[0].InputStream);

            string NTID = httpRequest.Form["NTID"];
            string COMPANYNANME = httpRequest.Form["COMPANYNAME"];
            //string Name = httpRequest.Form["Name"];
            byte[] Photo = binaryReader.ReadBytes(httpRequest.Files[0].ContentLength);
            string Set = httpRequest.Form["Set"];

            HttpResponseMessage msg;

            //var path = HttpContext.Current.Server.MapPath("~/Profile/");  
            var path = HttpContext.Current.Server.MapPath("~/ProfilePhotos/");
            //var path = "http://uat-vm.southeastasia.cloudapp.azure.com:81/hrms_test/ProfilePhotos";
            var folder = Path.Combine(path, COMPANYNANME.ToLower());
            var savePath = HttpContext.Current.Server.MapPath("~/ProfilePhotos/" + COMPANYNANME.ToLower() + "/");
            var saveFolder = Path.Combine(savePath, NTID);
            Directory.CreateDirectory(folder);
            Directory.CreateDirectory(saveFolder);

            MemoryStream stream = new MemoryStream();

            try
            {
                using (stream)
                {
                    string filePath = saveFolder + "/" + NTID + "_pic" + Set + ".png";
                    File.WriteAllBytes(filePath, Photo);
                    bool isFileExist = File.Exists(filePath);
                    if (isFileExist)
                    {
                        File.Replace(filePath, filePath, filePath + ".bck");
                    }
                    else
                    {
                        File.WriteAllBytes(filePath, Photo);
                    }
                }

                msg = Request.CreateResponse(HttpStatusCode.Accepted);
            }
            catch
            {
                msg = Request.CreateResponse(HttpStatusCode.NotAcceptable);
            }

            return msg;
        }



        [HttpPost]
        [Route("DetectFace")]
        public async Task<HttpResponseMessage> DetectFace()
        {
            HttpRequest httpRequest = HttpContext.Current.Request;
            BinaryReader binaryReader = new BinaryReader(httpRequest.Files[0].InputStream);
            byte[] Photo = binaryReader.ReadBytes(httpRequest.Files[0].ContentLength);
            //MemoryStream inputStream = new MemoryStream(Photo);
            Stream inputStream = new MemoryStream(Photo);

            try
            {
                FaceServiceClient faceServiceClient = new FaceServiceClient("bc6cf65c8dab414f92383ed4830341dd", "https://southeastasia.api.cognitive.microsoft.com/face/v1.0");
                string personGroupId = "ilmdevs";
                var faces = await faceServiceClient.DetectAsync(inputStream);
                var faceIds = faces.Select(face => face.FaceId).ToArray();

                try
                {
                    var results = await faceServiceClient.IdentifyAsync(personGroupId, faceIds);

                    if (results.Count() > 1)
                    {
                        return Request.CreateResponse(HttpStatusCode.BadRequest, "Multiple faces detected!");
                        //return "Multiple faces detected!";
                        //return Request.CreateResponse(HttpStatusCode.NotAcceptable);
                    }
                    else
                    {
                        var candidateId = results.FirstOrDefault().Candidates.FirstOrDefault().PersonId;
                        var person = await faceServiceClient.GetPersonAsync(personGroupId, candidateId);
                        return Request.CreateResponse(HttpStatusCode.Accepted, person.PersonId);
                        //return person.Name;
                        //return Request.CreateResponse(HttpStatusCode.Accepted);
                    }
                }
                catch
                {
                    return Request.CreateResponse(HttpStatusCode.NotAcceptable, "Error");
                    //return "Error";
                    //return Request.CreateResponse(HttpStatusCode.NotAcceptable);
                }
            }
            catch
            {
                return Request.CreateResponse(HttpStatusCode.NotAcceptable, "Error");
                //return "Error";
                //return Request.CreateResponse(HttpStatusCode.NotAcceptable);
            }
        }



        [HttpPost]
        [Route("RegisterFace")]
        public async Task<string> RegisterFace(SelfieRegistration selfieReg)
        {
            string name = selfieReg.Name;
            string ntid = selfieReg.NTID;

            try
            {
                FaceServiceClient faceServiceClient = new FaceServiceClient("bc6cf65c8dab414f92383ed4830341dd", "https://southeastasia.api.cognitive.microsoft.com/face/v1.0");
                var person = await faceServiceClient.CreatePersonAsync("ilmdevs", name);

                Guid persondID = person.PersonId;

                //var path = HttpContext.Current.Server.MapPath("~/Profile/");
                var path = HttpContext.Current.Server.MapPath("~/ProfilePhotos/");
                //var path = "http://uat-vm.southeastasia.cloudapp.azure.com:81/hrms_test/ProfilePhotos";
                var folder = Path.Combine(path, ntid);

                Stream stream1 = File.OpenRead(folder + "/" + ntid + "_pic1.png");
                Stream stream2 = File.OpenRead(folder + "/" + ntid + "_pic2.png");

                await faceServiceClient.AddPersonFaceAsync("ilmdevs", persondID, stream1);
                await faceServiceClient.AddPersonFaceAsync("ilmdevs", persondID, stream2);

                await faceServiceClient.TrainPersonGroupAsync("ilmdevs");

                string guidStr = persondID.ToString("D");

                return guidStr;
            }
            catch
            {
                return "null";
            }
        }

        [HttpPost]
        [Route("CreatePersonGroup")]
        public async Task<string> CreatePersonGroup(CreatePersonGroup createpersonid)
        {
            string Createpersonid = createpersonid.PersonGroupID;
            string CreatePersonName = createpersonid.Name;
            try
            {
                FaceServiceClient faceServiceClient = new FaceServiceClient("bc6cf65c8dab414f92383ed4830341dd", "https://southeastasia.api.cognitive.microsoft.com/face/v1.0");
                await faceServiceClient.CreatePersonGroupAsync(Createpersonid, CreatePersonName);

                return "";
            }
            catch
            {
                return "null";
            }
        }
        ///////



        //[HttpPost]
        //[Route("RegisterLargeFace")]
        //public async Task<string> RegisterLargeFace(SelfieRegistration selfiereg)
        //{
        //    string name = selfiereg.Name;
        //    string ntid = selfiereg.NTID;
        //    string largePersonGroup = selfiereg.PersonGroup;
        //    try
        //    {
        //        FaceServiceClient faceServiceClient = new FaceServiceClient("9305a0a148eb4aa79532fb63c42dc378", "https://westcentralus.api.cognitive.microsoft.com/face/v1.0");
        //        var person = await faceServiceClient.CreatePersonInLargePersonGroupAsync(largePersonGroup, name);
        //        Guid personID = person.PersonId;

        //        var path = HttpContext.Current.Server.MapPath("~/ProfilePhotos/");
        //        var folder = Path.Combine(path, ntid);

        //        Stream stream1 = File.OpenRead(folder + "/" + ntid + "_pic1.png");
        //        Stream stream2 = File.OpenRead(folder + "/" + ntid + "_pic2.png");

        //        await faceServiceClient.AddPersonFaceInLargePersonGroupAsync(largePersonGroup, personID, stream1);
        //        await faceServiceClient.AddPersonFaceInLargePersonGroupAsync(largePersonGroup, personID, stream2);

        //        await faceServiceClient.TrainLargePersonGroupAsync(largePersonGroup);

        //        string guidStr = personID.ToString("D");

        //        return guidStr;
        //    }
        //    catch
        //    {
        //        return "null";
        //    }
        //}
        [HttpPost]
        [Route("CreateLargePersonGroup")]
        public async Task<string> CreateLargePersonGroup(CreateLargePersonGroup LargePersonGroupID)
        {
            string createLargePersonGroupID = LargePersonGroupID.LargePersonGroupID;
            try
            {
                FaceServiceClient faceServiceClient = new FaceServiceClient("bc6cf65c8dab414f92383ed4830341dd", "https://southeastasia.api.cognitive.microsoft.com/face/v1.0");
                await faceServiceClient.CreateLargePersonGroupAsync(createLargePersonGroupID, "Group1");

                return "";
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }


        [HttpPost]
        [Route("DeletePerson")]
        public async Task<string> DeletePerson(DeletePerson _deletePerson)
        {
            try
            {
                FaceServiceClient faceServiceClient = new FaceServiceClient("bc6cf65c8dab414f92383ed4830341dd", "https://southeastasia.api.cognitive.microsoft.com/face/v1.0");
                await faceServiceClient.DeletePersonAsync("ilmdevs", _deletePerson.PersonID);
                return "";
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        [HttpPost]
        [Route("DeleteProfile")]
        public async Task<HttpResponseMessage> DeleteProfile()
        {
            HttpRequest httpRequest = HttpContext.Current.Request;

            string NTID = httpRequest.Form["NTID"];
            string COMPANYNANME = httpRequest.Form["COMPANYNAME"];
            string Set = httpRequest.Form["Set"];

            HttpResponseMessage msg;

            //var path = HttpContext.Current.Server.MapPath("~/Profile/");  
            var path = HttpContext.Current.Server.MapPath("~/ProfilePhotos/");
            //var path = "http://uat-vm.southeastasia.cloudapp.azure.com:81/hrms_test/ProfilePhotos";
            var folder = Path.Combine(path, COMPANYNANME.ToLower());
            var savePath = HttpContext.Current.Server.MapPath("~/ProfilePhotos/" + COMPANYNANME.ToLower() + "/");
            var saveFolder = Path.Combine(savePath, NTID);
            Directory.CreateDirectory(folder);
            Directory.CreateDirectory(saveFolder);

            MemoryStream stream = new MemoryStream();

            try
            {
                using (stream)
                {
                    string filePath = saveFolder + "/" + NTID + "_pic" + Set + ".png";
                    File.Delete(filePath);
                }

                msg = Request.CreateResponse(HttpStatusCode.Accepted);
            }
            catch
            {
                msg = Request.CreateResponse(HttpStatusCode.NotAcceptable);
            }

            return msg;
        }
        [HttpPost]
        [Route("DeleteAllFaceCredentials")]
        public async Task<HttpResponseMessage> DeleteAllFaceCredentials(DeletePersonCredentials personCredentials)
        {
            HttpResponseMessage msg;

            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = personCredentials.CN;
            SelfieRegistration2Controller.GetDB2(GDB);

            Connection Connection2 = new Connection();
            DataTable DT = new DataTable();
            Connection2.myparameters.Add(new myParameters { ParameterName = "@empId", mytype = SqlDbType.NVarChar, Value = personCredentials.EmpID });
            DT = Connection2.GetDataTable("spLogin_Mobile_test");
            UserProfilev2 User = new UserProfilev2();
            if (DT.Rows.Count > 0)
            {
                User = new UserProfilev2();
                User.FaceID = DT.Rows[0]["FaceID"].ToString();

                //try
                //{
                //    FaceServiceClient faceServiceClient = new FaceServiceClient("bc6cf65c8dab414f92383ed4830341dd", "https://southeastasia.api.cognitive.microsoft.com/face/v1.0");
                //    //await faceServiceClient.DeletePersonAsync("ilmdevs", _deletePerson.PersonID);
                //    await faceServiceClient.DeletePersonAsync(personCredentials.CN.ToLower(), Guid.Parse(User.FaceID));
                //}
                //catch
                //{

                //}

                //var path = HttpContext.Current.Server.MapPath("~/Profile/");  
                var path = HttpContext.Current.Server.MapPath("~/ProfilePhotos/");
                //var path = "http://uat-vm.southeastasia.cloudapp.azure.com:81/hrms_test/ProfilePhotos";
                var folder = Path.Combine(path, personCredentials.CN.ToLower());
                var savePath = HttpContext.Current.Server.MapPath("~/ProfilePhotos/" + personCredentials.CN.ToLower() + "/");
                var saveFolder = Path.Combine(savePath, personCredentials.EmpID);
                Directory.CreateDirectory(folder);
                Directory.CreateDirectory(saveFolder);

                MemoryStream stream = new MemoryStream();

                Connection Connection = new Connection();
                Connection.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = personCredentials.EmpID });
                Connection.ExecuteNonQuery("sp_Delete_AllBase64");

                Connection con = new Connection();
                DataTable dt = new DataTable();
                con.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.NVarChar, Value = personCredentials.EmpID });
                con.ExecuteScalar("sp_DeleteFaceID");
                try
                {
                    using (stream)
                    {
                        string filePath = saveFolder;
                        File.Delete(filePath);
                    }
                    msg = Request.CreateResponse(HttpStatusCode.Accepted);
                }
                catch
                { }
            }
            msg = Request.CreateResponse(HttpStatusCode.Accepted);
            return msg;
        }
    }
}