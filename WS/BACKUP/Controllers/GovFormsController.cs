﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using illimitadoWepAPI.Models;
using System.Xml.Serialization;
using System.IO;
using System.Data;
using System.Data.SqlClient;
using illimitadoWepAPI.MyClass;
using static illimitadoWepAPI.Models.Loan;
using iTextSharp.text.pdf;
using System.Web;
using illimitadoWepAPI.App_Start;
using System.Globalization;
using iTextSharp.text.pdf.parser;
using System.Text;
using iTextSharp.text;
using Aspose.Pdf.Text;
using System.Text.RegularExpressions;
using System.Configuration;
using System.Reflection;

namespace illimitadoWepAPI.Controllers
{
    public class GovFormsController : ApiController
    {
        GovForms GovForms = new GovForms();
        PayslipMethod PayMtd = new PayslipMethod();
        string formtype = "Maternity Notification MAT 1";
        Connection con2 = new Connection();
        public string ext = DateTime.Now.ToString("yyyy-MM-dd");
        static string globalB64 = "";
        private string getNumeric(string input)
        {
            return new string(input.Where(char.IsDigit).ToArray());
        }
        public string getmonth(string monthword)
        {
            var getmonth = new Dictionary<string, string>();
            string outmonth;
            string final;
            getmonth.Add("January", "01");
            getmonth.Add("February", "02");
            getmonth.Add("March", "03");
            getmonth.Add("April", "04");
            getmonth.Add("May", "05");
            getmonth.Add("June", "06");
            getmonth.Add("July", "07");
            getmonth.Add("August", "08");
            getmonth.Add("September", "09");
            getmonth.Add("October", "10");
            getmonth.Add("November", "11");
            getmonth.Add("December", "12");

            if (getmonth.TryGetValue(monthword, out outmonth))
            {
                final = outmonth;
            }
            else
            {
                final = "01";
            }
            return final;
        }
        public string[] submonth(string x, int sub, int yr)
        {
            string[] finx = new string[2];
            DateTime tempdt = Convert.ToDateTime("01" + "-" + x + "-" + DateTime.Now.Year.ToString());
            finx[0] = tempdt.AddMonths(-sub).ToString("MM");
            finx[1] = tempdt.AddMonths(-sub).ToString("yyyy");
            if (finx[0].Count() == 1)
            {
                finx[0] = "0" + finx[0];
            }
            return finx;
        }

        #region "SSSForm_MaternityNotification_MAT1"
        [HttpPost]
        [Route("SSSForm_MaternityNotification_MAT1")]

        public string SSSForm_MaternityNotification_MAT1(UserProfile uProfile)
        {
            //string path = Server.MapPath("pdf");

            //string url = "http://localhost:2390/pdf/" + "GovForms Templates/SSSForm_MaternityNotification_MAT1.pdf";
            string path = HttpContext.Current.Server.MapPath("pdf");
            var pdfReader1 = new PdfReader(path + "/GovForms Templates/SSSForm_MaternityNotification_MAT1.pdf");

            AcroFields af = pdfReader1.AcroFields;
            AcroFields.Item item;

            string formFile = path + "/GovForms Templates//SSSForm_MaternityNotification_MAT1.pdf";
            string newFile = path + "/GovForms-Employee/SSSForm_MaternityNotification_MAT1-" + uProfile.NTID + ".pdf";

            PdfReader reader = new PdfReader(formFile);
            PdfStamper stamper = new PdfStamper(reader, new FileStream(newFile, FileMode.Create));

            var forms = stamper.AcroFields;
            try // try catch for uProfile.NTID if session expires: redirects to login page
            {
                //sss
                try
                {
                    string fullSSN = GovForms.getPagibigMPL(uProfile.NTID).Rows[0][1].ToString().Replace("-", "");
                    char[] SSNbd = fullSSN.ToCharArray();
                    forms.SetFieldProperty("untitled1", "textsize", 14f, null);
                    forms.SetField("untitled1", SSNbd[0] + " " + SSNbd[1]);
                    forms.SetFieldProperty("untitled2", "textsize", 15f, null);
                    forms.SetField("untitled2", SSNbd[2] + " " + SSNbd[3] + " " + SSNbd[4] + "  " + SSNbd[5] + " " + SSNbd[6] + " " + SSNbd[7] + " " + SSNbd[8]);
                    forms.SetFieldProperty("untitled3", "textsize", 14f, null);
                    forms.SetField("untitled3", SSNbd[9].ToString());
                }
                catch { }
                //DOB
                try
                {
                    string bdayraw = GovForms.getPagibigMPL(uProfile.NTID).Rows[0][11].ToString();
                    string[] bdayarr = bdayraw.Split('-');
                    string newmonth = bdayarr[1];

                    forms.SetFieldProperty("untitled7", "textsize", 15f, null);
                    forms.SetField("untitled7", newmonth.Substring(0, 1) + " " + newmonth.Substring(1, 1));
                    forms.SetFieldProperty("untitled8", "textsize", 15f, null);
                    forms.SetField("untitled8", bdayarr[2].Substring(0, 1) + " " + bdayarr[2].Substring(1, 1));
                    forms.SetFieldProperty("untitled9", "textsize", 15f, null);
                    forms.SetField("untitled9", bdayarr[0].Substring(0, 1) + " " + bdayarr[0].Substring(1, 1) + " " + bdayarr[0].Substring(2, 1) + " " + bdayarr[0].Substring(3, 1));
                }
                catch { }
                //TIN
                try
                {
                    string fullTIN = GovForms.getPagibigMPL(uProfile.NTID).Rows[0][2].ToString().Replace("-", "");
                    string TIN1 = fullTIN.Substring(0, 1); //First number
                    string TIN2 = fullTIN.Substring(1, 1); //2nd number
                    string TIN3 = fullTIN.Substring(2, 1); //3rd number
                    string TIN1st3 = TIN1 + "  " + TIN2 + " " + TIN3;
                    forms.SetFieldProperty("untitled10", "textsize", 15f, null);
                    string TIN4 = fullTIN.Substring(3, 1);
                    string TIN5 = fullTIN.Substring(4, 1);
                    string TIN6 = fullTIN.Substring(5, 1);
                    string TIN2nd3 = TIN4 + " " + TIN5 + " " + TIN6;
                    forms.SetFieldProperty("untitled11", "textsize", 15f, null);
                    string TIN7 = fullTIN.Substring(6, 1);
                    string TIN8 = fullTIN.Substring(7, 1);
                    string TIN9 = fullTIN.Substring(8, 1);
                    string TIN3rd3 = TIN7 + " " + TIN8 + " " + TIN9;
                    forms.SetFieldProperty("untitled12", "textsize", 15f, null);
                    forms.SetField("untitled10", TIN1st3);
                    forms.SetField("untitled11", TIN2nd3);
                    forms.SetField("untitled12", TIN3rd3);
                }
                catch { }
                string fullname = "";
                try
                {
                    forms.SetField("untitled13", GovForms.getPagibigMPL(uProfile.NTID).Rows[0][5].ToString());
                    forms.SetField("untitled14", GovForms.getPagibigMPL(uProfile.NTID).Rows[0][4].ToString());
                    forms.SetField("untitled15", GovForms.getPagibigMPL(uProfile.NTID).Rows[0][6].ToString());
                }
                catch { }
                fullname = GovForms.getPagibigMPL(uProfile.NTID).Rows[0][4].ToString() + " " + GovForms.getPagibigMPL(uProfile.NTID).Rows[0][6].ToString() + " " + GovForms.getPagibigMPL(uProfile.NTID).Rows[0][5].ToString();
                GovForms.getEmployeePersonalInformation(uProfile.NTID);
                try
                {
                    forms.SetField("untitled18", GovForms.housenuma);//house no.
                    forms.SetField("untitled19", GovForms.streetnamea);//street
                    forms.SetField("untitled21", GovForms.barangaya);//barangay
                    forms.SetField("untitled22", GovForms.citya);//city/municipality
                    forms.SetField("untitled23", GovForms.provincea);//city/municipality                                                             //zip
                    forms.SetFieldProperty("untitled24", "textsize", 15f, null);
                    string fullzip = GovForms.zipcodea;
                    forms.SetField("untitled24", fullzip.Substring(0, 1) + " " + fullzip.Substring(1, 1) + "  " + fullzip.Substring(2, 1) + " " + fullzip.Substring(3, 1));
                }
                catch { }
                try
                {
                    for (int i = 0; i < GovForms.GetPersonalInfo(uProfile.NTID).Rows.Count; i++)
                    {
                        string numtype = GovForms.GetPersonalInfo(uProfile.NTID).Rows[i][1].ToString();
                        if (numtype == "Home")
                        {
                            string num = GovForms.GetPersonalInfo(uProfile.NTID).Rows[i][0].ToString().Replace("-", "");
                            num = getNumeric(num);
                            if (num.Length == 10)
                            {
                                char[] numarr = num.ToCharArray();
                                forms.SetFieldProperty("untitled25", "textsize", 14f, null);
                                forms.SetFieldProperty("untitled26", "textsize", 14f, null);
                                forms.SetField("untitled25", numarr[0] + " " + numarr[1] + "  " + numarr[2]);
                                forms.SetField("untitled26", numarr[3] + " " + numarr[4] + " " + numarr[5] + "  " + numarr[6] + " " + numarr[7] + " " + numarr[8] + "  " + numarr[9]);
                            }
                            else if (num.Length == 7)
                            {
                                char[] numarr = num.ToCharArray();

                                forms.SetFieldProperty("untitled26", "textsize", 14f, null);
                                forms.SetField("untitled26", numarr[0] + " " + numarr[1] + " " + numarr[2] + "  " + numarr[3] + " " + numarr[4] + "  " + numarr[5] + "  " + numarr[6]);

                            }
                        }
                        else if (numtype == "Mobile")
                        {
                            //Mobile Number Breakdown
                            string mobnum = GovForms.GetPersonalInfo(uProfile.NTID).Rows[i][0].ToString().Replace("-", "").Replace("+63", "0").Replace("+", "");
                            bool mobnume = Regex.IsMatch(mobnum, @"^\d+$");// determine if string is pure numbers; return true & null value

                            if (mobnume.ToString() == "True")
                            {
                                char[] mobnumarr = mobnum.ToCharArray();
                                forms.SetFieldProperty("untitled27", "textsize", 14f, null);
                                forms.SetFieldProperty("untitled28", "textsize", 14f, null);
                                forms.SetField("untitled27", mobnumarr[0] + " " + mobnumarr[1] + "  " + mobnumarr[2] + " " + mobnumarr[3]);
                                forms.SetField("untitled28", mobnumarr[4] + " " + mobnumarr[5] + "  " + mobnumarr[6] + " " + mobnumarr[7] + " " + mobnumarr[8] + "  " + mobnumarr[9] + " " + mobnumarr[10]);

                            }
                        }// Mobile Number Breakdown end
                    }
                }
                catch { }
                //Email Address
                try
                {
                    forms.SetField("untitled29", GovForms.GetEmailInfo(uProfile.NTID).Rows[0][0].ToString());
                }
                catch { }
                //SSS
                try
                {
                    string SSNco = GovForms.getCompanyProfile(uProfile.NTID).Rows[0][39].ToString().Replace("-", "");
                    char[] SSNcobd = SSNco.ToCharArray();
                    forms.SetFieldProperty("untitled48", "textsize", 15f, null);
                    forms.SetField("untitled48", SSNcobd[0] + " " + SSNcobd[1]);
                    string SSN2nd = "";
                    for (int i = 2; i < 10; i++)
                    {
                        SSN2nd = SSN2nd + SSNcobd[i] + " ";
                    }
                    forms.SetFieldProperty("untitled49", "textsize", 15f, null);
                    forms.SetField("untitled49", SSN2nd);
                    forms.SetFieldProperty("untitled50", "textsize", 15f, null);
                    forms.SetField("untitled50", SSNcobd[10] + " " + SSNcobd[11] + " " + SSNcobd[12]);
                }
                catch { }
                //Employer TIN
                try
                {
                    string fullTIN2 = GovForms.getPagibigMPL(uProfile.NTID).Rows[0][2].ToString().Replace("-", "");
                    string TIN12 = fullTIN2.Substring(0, 1); //First number
                    string TIN22 = fullTIN2.Substring(1, 1); //2nd number
                    string TIN32 = fullTIN2.Substring(2, 1); //3rd number
                    string TIN1st32 = TIN12 + "  " + TIN22 + " " + TIN32;
                    forms.SetFieldProperty("untitled51", "textsize", 15f, null);
                    string TIN42 = fullTIN2.Substring(3, 1);
                    string TIN52 = fullTIN2.Substring(4, 1);
                    string TIN62 = fullTIN2.Substring(5, 1);
                    string TIN2nd32 = TIN42 + " " + TIN52 + " " + TIN62;
                    forms.SetFieldProperty("untitled52", "textsize", 15f, null);
                    string TIN72 = fullTIN2.Substring(6, 1);
                    string TIN82 = fullTIN2.Substring(7, 1);
                    string TIN92 = fullTIN2.Substring(8, 1);
                    string TIN3rd32 = TIN72 + " " + TIN82 + " " + TIN92;
                    forms.SetFieldProperty("untitled53", "textsize", 15f, null);
                    forms.SetField("untitled51", TIN1st32);
                    forms.SetField("untitled52", TIN2nd32);
                    forms.SetField("untitled53", TIN3rd32);
                }
                catch { }
                //address
                try
                {
                    forms.SetField("untitled55", "Yes");
                    forms.SetField("untitled57", GovForms.getCompanyProfile(uProfile.NTID).Rows[0][3].ToString());
                    string emplradd = GovForms.getPagibigMPL(uProfile.NTID).Rows[0][32].ToString().ToUpper() + " " + GovForms.getPagibigMPL(uProfile.NTID).Rows[0][27].ToString().ToUpper() + " " + GovForms.getPagibigMPL(uProfile.NTID).Rows[0][26].ToString().ToUpper() + " " + GovForms.getPagibigMPL(uProfile.NTID).Rows[0][30].ToString().ToUpper() + " " + GovForms.getPagibigMPL(uProfile.NTID).Rows[0][30].ToString().ToUpper() + " " + GovForms.getPagibigMPL(uProfile.NTID).Rows[0][28].ToString().ToUpper();
                    forms.SetField("untitled58", GovForms.getPagibigMPL(uProfile.NTID).Rows[0][32].ToString().ToUpper() + " " + GovForms.getPagibigMPL(uProfile.NTID).Rows[0][27].ToString().ToUpper());//bldg
                    forms.SetField("untitled60", GovForms.getPagibigMPL(uProfile.NTID).Rows[0][26].ToString().ToUpper());//street
                    forms.SetField("untitled62", GovForms.getPagibigMPL(uProfile.NTID).Rows[0][30].ToString().ToUpper());//barangay
                    forms.SetField("untitled63", GovForms.getPagibigMPL(uProfile.NTID).Rows[0][28].ToString().ToUpper());//city                                                                                                   //zip code
                    string zipcomp = GovForms.getCompanyProfile(uProfile.NTID).Rows[0][14].ToString();
                    forms.SetFieldProperty("untitled65", "textsize", 15f, null);
                    forms.SetField("untitled65", zipcomp.Substring(0, 1) + " " + zipcomp.Substring(1, 1) + "  " + zipcomp.Substring(2, 1) + " " + zipcomp.Substring(3, 1));
                }
                catch { }
                try
                {
                    string[] comptelarr = GovForms.getCompanyProfile(uProfile.NTID).Rows[0][15].ToString().Split('-');
                    forms.SetFieldProperty("untitled66", "textsize", 15f, null);
                    forms.SetFieldProperty("untitled67", "textsize", 15f, null);
                    forms.SetField("untitled66", comptelarr[0].Substring(0, 1) + " " + comptelarr[0].Substring(1, 1) + "  " + comptelarr[0].Substring(2, 1));
                    forms.SetField("untitled67", comptelarr[1].Substring(0, 1) + " " + comptelarr[1].Substring(1, 1) + " " + comptelarr[1].Substring(2, 1) + " " + comptelarr[2].Substring(0, 1) + "  " + comptelarr[2].Substring(1, 1) + " " + comptelarr[2].Substring(2, 1) + " " + comptelarr[2].Substring(3, 1));
                    forms.SetField("untitled68", GovForms.getCompanyProfile(uProfile.NTID).Rows[0][18].ToString());
                }
                catch { }
                //Printed Name
                item = forms.GetFieldItem("untitled37");
                item.GetMerged(0).Put(PdfName.Q, new PdfNumber(PdfFormField.Q_CENTER));
                forms.SetField("untitled37", fullname.ToUpper());
                //Date
                item = forms.GetFieldItem("untitled39");
                item.GetMerged(0).Put(PdfName.Q, new PdfNumber(PdfFormField.Q_CENTER));
                forms.SetField("untitled39", DateTime.Now.ToString("yyyy-MM-dd"));

                GovForms.GetRequestFormSettings();
                GovForms.SignatoryNew(uProfile.NTID, formtype);
                GovForms.GetAppEMP(65, uProfile.NTID);
                string empidApprover = "";
                empidApprover = GovForms.ApproverEMPID;
                if (empidApprover == "" || empidApprover == null || string.IsNullOrWhiteSpace(empidApprover))
                {
                }
                else
                {
                    GovForms.GetApproverNameJobDes(empidApprover);
                    string Approvername = GovForms.ApproverName;
                    string ApproverDesc = GovForms.ApproverJobDesc;
                    GovForms.GetSignatureSignPicture(empidApprover, "sign_1");
                    string Bitsignature = null;
                    Bitsignature = GovForms.BitSign;
                    Byte[] SignatureBit = null;
                    iTextSharp.text.Image signatory = null;
                    iTextSharp.text.Image signatory1 = null;
                    var pdfContentByte = stamper.GetOverContent(1);
                    if (string.IsNullOrWhiteSpace(Bitsignature) == false)
                    {
                        Bitsignature = Bitsignature.Replace(' ', '+');
                        SignatureBit = Convert.FromBase64String(Bitsignature);
                        signatory = iTextSharp.text.Image.GetInstance(SignatureBit);
                        signatory1 = iTextSharp.text.Image.GetInstance(SignatureBit);
                        signatory.SetAbsolutePosition(70, 90);
                        signatory.ScaleToFit(100, 100);
                        signatory.SetAbsolutePosition(200, 120);
                        signatory.ScaleToFit(100, 100);
                        forms.SetField("untitled70", Approvername);
                        forms.SetField("untitled72", ApproverDesc);
                        pdfContentByte.AddImage(signatory);
                    }
                }
                stamper.FormFlattening = true;
                stamper.Close();
                uProfile.Status = "Success";
            }
            catch (Exception ex)
            {
                uProfile.Status = ex.ToString();
            }


            return uProfile.Status;
        }
        #endregion

        #region "SSSForm_SicknessBenefitReimbursement"
        [HttpPost]
        [Route("SSSForm_SicknessBenefitReimbursement")]
        public string SSSForm_SicknessBenefitReimbursement(UserProfile uProfile)
        {

            string path = HttpContext.Current.Server.MapPath("pdf");
            var pdfReader1 = new PdfReader(path + "/GovForms Templates/SSSForm_SicknessBenefitReimbursement.pdf");
            AcroFields af = pdfReader1.AcroFields;

            string formFile = path + "/GovForms Templates/SSSForm_SicknessBenefitReimbursement.pdf";
            string newFile = path + "/GovForms-Employee/SSSForm_SicknessBenefitReimbursement-" + uProfile.NTID + ".pdf";

            PdfReader reader = new PdfReader(formFile);
            PdfStamper stamper = new PdfStamper(reader, new FileStream(newFile, FileMode.Create));

            var fields = stamper.AcroFields;
            try
            {
                GovForms.getEmployeePersonalInformation(uProfile.NTID);
                string fullname = GovForms.getPagibigMPL(uProfile.NTID).Rows[0][4].ToString().ToUpper() + " " + GovForms.getPagibigMPL(uProfile.NTID).Rows[0][5].ToString().ToUpper() + " " + GovForms.getPagibigMPL(uProfile.NTID).Rows[0][6].ToString().ToUpper();
                fields.SetFieldProperty("untitled1", "textsize", 15f, null);
                fields.SetFieldProperty("untitled2", "textsize", 15f, null);
                fields.SetFieldProperty("untitled3", "textsize", 15f, null);
                try
                {
                    string sssraw = GovForms.getPagibigMPL(uProfile.NTID).Rows[0][1].ToString().Replace("-", " ");
                    string[] arrsss = sssraw.Split(new char[0]);
                    fields.SetField("untitled1", arrsss[0].Substring(0, 1) + " " + arrsss[0].Substring(1, 1));
                    fields.SetField("untitled2", arrsss[1].Substring(0, 1) + "  " + arrsss[1].Substring(1, 1) + " " + arrsss[1].Substring(2, 1) + " " + arrsss[1].Substring(3, 1) + " " + arrsss[1].Substring(4, 1) + " " + arrsss[1].Substring(5, 1) + " " + arrsss[1].Substring(6, 1));
                    fields.SetField("untitled3", arrsss[2]);
                }
                catch { }
                try
                {
                    string bdayraw = GovForms.getPagibigMPL(uProfile.NTID).Rows[0][11].ToString();
                    string[] bdayarr = bdayraw.Split(new char[0]);
                    string newmonth = "";
                    switch (bdayarr[0])
                    {
                        case "January":
                            newmonth = "01";
                            break;
                        case "February":
                            newmonth = "02";
                            break;
                        case "March":
                            newmonth = "03";
                            break;
                        case "April":
                            newmonth = "04";
                            break;
                        case "May":
                            newmonth = "05";
                            break;
                        case "June":
                            newmonth = "06";
                            break;
                        case "July":
                            newmonth = "07";
                            break;
                        case "August":
                            newmonth = "08";
                            break;
                        case "September":
                            newmonth = "09";
                            break;
                        case "October":
                            newmonth = "10";
                            break;
                        case "November":
                            newmonth = "11";
                            break;
                        case "December":
                            newmonth = "12";
                            break;
                    }
                    if (bdayarr[1].Count() == 2)
                    {
                        bdayarr[1] = "0" + bdayarr[1];
                    }
                    fields.SetFieldProperty("untitled7", "textsize", 15f, null);
                    fields.SetField("untitled7", newmonth.Substring(0, 1) + " " + newmonth.Substring(1, 1) + " " + bdayarr[1].Substring(0, 1) + " " + bdayarr[1].Substring(1, 1));
                    fields.SetFieldProperty("untitled8", "textsize", 15f, null);
                    fields.SetField("untitled8", bdayarr[2].Substring(0, 1) + " " + bdayarr[2].Substring(1, 1) + " " + bdayarr[2].Substring(2, 1) + " " + bdayarr[2].Substring(3, 1));

                }
                catch { }
                try
                {
                    string fullTIN = GovForms.getPagibigMPL(uProfile.NTID).Rows[0][2].ToString().Replace("-", "");
                    string TIN1 = fullTIN.Substring(0, 1); //First number
                    string TIN2 = fullTIN.Substring(1, 1); //2nd number
                    string TIN3 = fullTIN.Substring(2, 1); //3rd number
                    string TIN1st3 = TIN1 + " " + TIN2 + " " + TIN3;
                    fields.SetFieldProperty("untitled9", "textsize", 15f, null);
                    string TIN4 = fullTIN.Substring(3, 1);
                    string TIN5 = fullTIN.Substring(4, 1);
                    string TIN6 = fullTIN.Substring(5, 1);
                    string TIN2nd3 = TIN4 + " " + TIN5 + " " + TIN6;
                    fields.SetFieldProperty("untitled10", "textsize", 15f, null);
                    string TIN7 = fullTIN.Substring(6, 1);
                    string TIN8 = fullTIN.Substring(7, 1);
                    string TIN9 = fullTIN.Substring(8, 1);
                    string TIN3rd3 = TIN7 + " " + TIN8 + " " + TIN9;
                    fields.SetFieldProperty("untitled11", "textsize", 15f, null);
                    fields.SetField("untitled9", TIN1st3);
                    fields.SetField("untitled10", TIN2nd3);
                    fields.SetField("untitled11", TIN3rd3);
                }
                catch { }


                fields.SetField("untitled12", GovForms.getPagibigMPL(uProfile.NTID).Rows[0][5].ToString().ToUpper()); //last name
                fields.SetField("untitled13", GovForms.getPagibigMPL(uProfile.NTID).Rows[0][4].ToString().ToUpper()); //first name
                fields.SetField("untitled14", GovForms.getPagibigMPL(uProfile.NTID).Rows[0][6].ToString().ToUpper()); //middle name

                GovForms.getEmployeePersonalInformation(uProfile.NTID);
                fields.SetField("untitled17", GovForms.getPagibigMPL(uProfile.NTID).Rows[0][15].ToString().ToUpper()); //house no.
                fields.SetField("untitled18", GovForms.getPagibigMPL(uProfile.NTID).Rows[0][21].ToString().ToUpper()); //street name
                fields.SetField("untitled20", GovForms.getPagibigMPL(uProfile.NTID).Rows[0][20].ToString().ToUpper()); //subdivision
                fields.SetField("untitled19", GovForms.getPagibigMPL(uProfile.NTID).Rows[0][7].ToString().ToUpper()); //barangay
                fields.SetField("untitled21", GovForms.getPagibigMPL(uProfile.NTID).Rows[0][9].ToString().ToUpper()); //city
                fields.SetField("untitled22", GovForms.provincea.ToString().ToUpper()); //Province
                try
                {
                    string zipraw = GovForms.getPagibigMPL(uProfile.NTID).Rows[0][36].ToString();
                    string zip1 = zipraw.Substring(0, 1);
                    string zip2 = zipraw.Substring(1, 1);
                    string zip3 = zipraw.Substring(2, 1);
                    string zip4 = zipraw.Substring(3, 1);
                    string zipfinal = zip1 + "  " + zip2 + "  " + zip3 + "  " + zip4;
                    fields.SetFieldProperty("untitled23", "textsize", 12f, null);
                    fields.SetField("untitled23", zipfinal);
                }
                catch { }

                try
                {

                    string telareacoderaw = GovForms.getPagibigMPL(uProfile.NTID).Rows[0][13].ToString();
                    string telareacode1 = telareacoderaw.Substring(0, 1);
                    string telareacode2 = telareacoderaw.Substring(1, 1);
                    string telareacode3 = telareacoderaw.Substring(2, 1);
                    string telareacodefinal = telareacode1 + " " + telareacode2 + " " + telareacode3;
                    string telnumraw = GovForms.getPagibigMPL(uProfile.NTID).Rows[0][14].ToString().Replace("-", "");
                    string telnum1 = telnumraw.Substring(0, 1);
                    string telnum2 = telnumraw.Substring(1, 1);
                    string telnum3 = telnumraw.Substring(2, 1);
                    string telnum4 = telnumraw.Substring(3, 1);
                    string telnum5 = telnumraw.Substring(4, 1);
                    string telnum6 = telnumraw.Substring(5, 1);
                    string telnum7 = telnumraw.Substring(6, 1);
                    string telnumfinal = telnum1 + " " + telnum2 + " " + telnum3 + " " + telnum4 + "  " + telnum5 + " " + telnum6 + " " + telnum7;
                    fields.SetFieldProperty("untitled24", "textsize", 15f, null);
                    fields.SetField("untitled24", telareacodefinal + "     " + telnumfinal);
                }
                catch { }
                try
                {
                    string cpnumraw = GovForms.getPagibigMPL(uProfile.NTID).Rows[0][23].ToString();
                    char[] cpnum = cpnumraw.ToCharArray();
                    string cpnumareafinal = "";
                    cpnumareafinal = cpnum[0] + " " + cpnum[1] + " " + cpnum[2] + "  " + cpnum[3];
                    string cpnumfinal = cpnum[4] + " " + cpnum[5] + " " + cpnum[6] + " " + cpnum[7] + " " + cpnum[8] + " " + cpnum[9] + " " + cpnum[10];
                    fields.SetFieldProperty("untitled25", "textsize", 15f, null);
                    fields.SetField("untitled25", cpnumareafinal + " " + cpnumfinal);

                }
                catch { }

                fields.SetField("untitled26", GovForms.getPagibigMPL(uProfile.NTID).Rows[0][17].ToString().ToUpper());
                fields.SetField("untitled30", fullname.ToUpper());
                try
                {
                    string emplyrid = GovForms.getCompanyProfile(uProfile.NTID).Rows[0][39].ToString();
                    string[] emplyridnum = emplyrid.Split('-');
                    string emplyrid1 = emplyridnum[0].Substring(0, 1) + " " + emplyridnum[0].Substring(1, 1);
                    fields.SetFieldProperty("untitled40", "textsize", 15f, null);
                    fields.SetField("untitled40", emplyrid1);



                    string emplyrid2 = emplyridnum[1].Substring(0, 1) + " " + emplyridnum[1].Substring(1, 1) + " " + emplyridnum[1].Substring(2, 1) + " " + emplyridnum[1].Substring(3, 1) + " " + emplyridnum[1].Substring(4, 1) + " " + emplyridnum[1].Substring(5, 1) + " " + emplyridnum[1].Substring(6, 1);
                    fields.SetFieldProperty("untitled41", "textsize", 15f, null);
                    fields.SetField("untitled41", emplyrid2);
                    fields.SetField("untitled43", GovForms.getCompanyProfile(uProfile.NTID).Rows[0][38].ToString());
                    fields.SetField("untitled48", GovForms.getCompanyProfile(uProfile.NTID).Rows[0][3].ToString());
                    fields.SetField("untitled49", GovForms.getCompanyProfile(uProfile.NTID).Rows[0][6].ToString() + " " + GovForms.getCompanyProfile(uProfile.NTID).Rows[0][7].ToString());//bldg
                    fields.SetField("untitled51", GovForms.getCompanyProfile(uProfile.NTID).Rows[0][8].ToString());//street
                    fields.SetField("untitled53", GovForms.getCompanyProfile(uProfile.NTID).Rows[0][9].ToString());//barangay
                    fields.SetField("untitled54", GovForms.getCompanyProfile(uProfile.NTID).Rows[0][11].ToString());//barangay
                }
                catch { }
                try
                {
                    string empzip = GovForms.getPagibigMPL(uProfile.NTID).Rows[0][33].ToString();
                    char[] empzipnum = empzip.ToCharArray();
                    fields.SetFieldProperty("untitled56", "textsize", 15f, null);
                    fields.SetField("untitled56", empzipnum[0] + " " + empzipnum[1] + " " + empzipnum[2] + " " + empzipnum[3]);
                    string emplyrtel = GovForms.getCompanyProfile(uProfile.NTID).Rows[0][15].ToString();
                    fields.SetFieldProperty("untitled57", "textsize", 15f, null);
                    fields.SetField("untitled57", emplyrtel.Substring(0, 1) + " " + emplyrtel.Substring(1, 1) + " " + emplyrtel.Substring(2, 1));
                    fields.SetFieldProperty("untitled58", "textsize", 15f, null);
                    fields.SetField("untitled58", emplyrtel.Substring(3, 1) + " " + emplyrtel.Substring(4, 1) + " " + emplyrtel.Substring(5, 1) + "  " + emplyrtel.Substring(6, 1) + " " + emplyrtel.Substring(7, 1) + " " + emplyrtel.Substring(8, 1) + " " + "0");
                    fields.SetField("untitled59", GovForms.getCompanyProfile(uProfile.NTID).Rows[0][18].ToString());

                }
                catch { }

                GovForms.GetRequestFormSettings();
                GovForms.SignatoryNew(uProfile.NTID, formtype);

                GovForms.GetAppEMP(66, uProfile.NTID);
                string empidApprover = "";
                empidApprover = GovForms.ApproverEMPID;
                if (empidApprover == "" || empidApprover == null || string.IsNullOrWhiteSpace(empidApprover))
                {
                }
                else
                {
                    GovForms.GetApproverNameJobDes(empidApprover);
                    string Approvername = GovForms.ApproverName;
                    string ApproverDesc = GovForms.ApproverJobDesc;
                    GovForms.GetSignatureSignPicture(empidApprover, "sign_1");
                    string Bitsignature = null;
                    Bitsignature = GovForms.BitSign;
                    Byte[] SignatureBit = null;
                    iTextSharp.text.Image signatory = null;
                    var pdfContentByte = stamper.GetOverContent(1);
                    if (string.IsNullOrWhiteSpace(Bitsignature) == false)
                    {
                        Bitsignature = Bitsignature.Replace(' ', '+');
                        SignatureBit = Convert.FromBase64String(Bitsignature);
                        signatory = iTextSharp.text.Image.GetInstance(SignatureBit);
                        signatory.SetAbsolutePosition(80, 115);
                        signatory.ScaleToFit(100, 100);
                        fields.SetField("untitled84", Approvername);
                        fields.SetField("untitled86", ApproverDesc);
                        pdfContentByte.AddImage(signatory);
                    }
                }

                stamper.FormFlattening = true;
                stamper.Close();
                uProfile.Status = "Success";

            }

            catch (Exception ex)
            {
                uProfile.Status = ex.ToString();
            }
            return uProfile.Status;
        }

        //public string SSSForms_Maternity_Reimbursement(UserProfile uProfile)
        //{


        //    string path = HttpContext.Current.Server.MapPath("pdf");
        //    var pdfReader1 = new PdfReader(path + "/GovForms Templates/SSSForms_Maternity_Reimbursement.pdf");
        //    AcroFields af = pdfReader1.AcroFields;
        //    AcroFields.Item item;

        //    // get the file paths

        //    // Template file path
        //    string formFile = path + "/GovForms Templates/SSSForms_Maternity_Reimbursement.pdf";

        //    // Output file path
        //    string newFile = path + "/GovForms-Employee/SSSForms_Maternity_Reimbursement-" + uProfile.NTID + ".pdf";

        //    // read the template file
        //    PdfReader reader = new PdfReader(formFile);

        //    // instantiate PDFStamper object
        //    // The Output file will be created from template file and edited by the PDFStamper
        //    PdfStamper stamper = new PdfStamper(reader, new FileStream(newFile, FileMode.Create));
        //    string empbranchid = GovForms.getCompanyProfile(uProfile.NTID).Rows[0][0].ToString();
        //    // Object to deal with the Output file's textfields
        //    AcroFields fields = stamper.AcroFields;

        //    GovForms.getEmployeePersonalInformation(uProfile.NTID);
        //    GovForms.getEmployeePay(uProfile.NTID);
        //    //Name
        //    string lname = GovForms.GetLoyaltyCard(uProfile.NTID).Rows[0][2].ToString();
        //    string fname = GovForms.GetLoyaltyCard(uProfile.NTID).Rows[0][0].ToString();
        //    string mname = GovForms.GetLoyaltyCard(uProfile.NTID).Rows[0][1].ToString();
        //    string suffix = "";
        //    //MAPPING START
        //    try
        //    {
        //        #region Personal Data
        //        //SSS Number
        //        try
        //        {
        //            char[] SSNum = getNumeric(GovForms.getPagibigMPL(uProfile.NTID).Rows[0][1].ToString()).ToCharArray();
        //            for (int i = 0; i < SSNum.Length; i++)
        //            {
        //                fields.SetFieldProperty("untitled" + (i + 1), "textsize", 12f, null);
        //                fields.SetField("untitled" + (i + 1), SSNum[i].ToString());
        //            }
        //        }
        //        catch
        //        {

        //        }
        //        //CRN
        //        try
        //        {
        //            char[] CRN = getNumeric(GovForms.GetSSSSalaryLoan(uProfile.NTID).Rows[0][15].ToString()).ToCharArray();
        //            for (int i = 0; i < CRN.Length; i++)
        //            {
        //                fields.SetFieldProperty("untitled" + (i + 11), "textsize", 12f, null);
        //                fields.SetField("untitled" + (i + 11), CRN[i].ToString());
        //            }
        //        }
        //        catch
        //        {

        //        }
        //        //Date of Birth
        //        try
        //        {
        //            string[] DOB = GovForms.GetMatBen(uProfile.NTID).Rows[0][6].ToString().Split('-');
        //            char[] DOBchar = (DOB[1] + DOB[2] + DOB[0]).ToCharArray();
        //            for (int i = 0; i < DOBchar.Length; i++)
        //            {
        //                fields.SetFieldProperty("untitled" + (i + 23), "textsize", 12f, null);
        //                fields.SetField("untitled" + (i + 23), DOBchar[i].ToString());
        //            }
        //        }
        //        catch
        //        {

        //        }
        //        //TIN
        //        try
        //        {
        //            char[] TIN = getNumeric(GovForms.GetMatBen(uProfile.NTID).Rows[0][13].ToString()).ToCharArray();
        //            for (int i = 0; i < TIN.Length; i++)
        //            {
        //                fields.SetFieldProperty("untitled" + (i + 31), "textsize", 12f, null);
        //                fields.SetField("untitled" + (i + 31), TIN[i].ToString());
        //            }
        //        }
        //        catch
        //        {

        //        }
        //        //Name
        //        try
        //        {
        //            //Last Name
        //            fields.SetField("untitled140", lname);
        //            //First Name
        //            fields.SetField("untitled141", fname);
        //            //Middle Name
        //            fields.SetField("untitled142", mname);
        //            //Suffix
        //            fields.SetField("untitled143", suffix);
        //        }
        //        catch
        //        {

        //        }
        //        //Local Address
        //        try
        //        {
        //            //Rm./Flr./Unit No. & Bldg. Name
        //            fields.SetField("untitled144", "");
        //            //House Number
        //            fields.SetField("untitled145", GovForms.getPagibigMPL(uProfile.NTID).Rows[0][16].ToString());
        //            //Street Name
        //            fields.SetField("untitled146", GovForms.getPagibigMPL(uProfile.NTID).Rows[0][19].ToString());
        //            //Subdivision
        //            fields.SetField("untitled147", "");
        //            //Barangay/District/Locality
        //            fields.SetField("untitled148", GovForms.getPagibigMPL(uProfile.NTID).Rows[0][7].ToString());
        //            //City/Municipality
        //            fields.SetField("untitled149", GovForms.getPagibigMPL(uProfile.NTID).Rows[0][9].ToString());
        //            //Province
        //            fields.SetField("untitled150", GovForms.provincea);
        //        }
        //        catch
        //        {

        //        }
        //        //ZIP code
        //        try
        //        {
        //            string zipcode = GovForms.GetMatBen(uProfile.NTID).Rows[0][12].ToString();
        //            bool zipcodee = Regex.IsMatch(zipcode, @"^\d+$");// determine if string is pure numbers; return true & null value
        //            if (zipcodee.ToString() == "True")
        //            {
        //                for (int i = 0; i < zipcode.Length; i++)
        //                {
        //                    if (zipcode.Length == 4)
        //                    {
        //                        fields.SetFieldProperty("untitled" + (i + 61), "textsize", 14f, null);
        //                        fields.SetField("untitled" + (i + 61), zipcode.Substring(i, 1));
        //                    }
        //                }
        //            }
        //        }
        //        catch
        //        {

        //        }
        //        //Phone numbers
        //        try
        //        {
        //            for (int i = 0; i < GovForms.GetPersonalInfo(uProfile.NTID).Rows.Count; i++)
        //            {
        //                string numtype = GovForms.GetPersonalInfo(uProfile.NTID).Rows[i][1].ToString();
        //                if (numtype == "Home")
        //                {
        //                    string num = GovForms.GetPersonalInfo(uProfile.NTID).Rows[i][0].ToString().Replace("-", "");
        //                    num = getNumeric(num);
        //                    if (num.Length == 10)
        //                    {
        //                        char[] numarr = num.ToCharArray();
        //                        for (int j = 0; j < 10; j++)
        //                        {
        //                            fields.SetFieldProperty("untitled" + (j + 40), "textsize", 14f, null);
        //                            fields.SetField("untitled" + (j + 40), numarr[j].ToString());
        //                        }
        //                    }
        //                    else if (num.Length == 7)
        //                    {
        //                        char[] numarr = num.ToCharArray();
        //                        for (int j = 0; j < 10; j++)
        //                        {
        //                            fields.SetFieldProperty("untitled" + (j + 43), "textsize", 14f, null);
        //                            fields.SetField("untitled" + (j + 43), numarr[j].ToString());
        //                        }

        //                    }
        //                }
        //                else if (numtype == "Mobile")
        //                {
        //                    //Mobile Number Breakdown
        //                    string mobnum = GovForms.GetPersonalInfo(uProfile.NTID).Rows[i][0].ToString().Replace("-", "").Replace("+63", "0").Replace("+", "");
        //                    bool mobnume = Regex.IsMatch(mobnum, @"^\d+$");// determine if string is pure numbers; return true & null value

        //                    if (mobnume.ToString() == "True")
        //                    {
        //                        if (mobnum.Length == 11)
        //                        {
        //                            for (int j = 0; j < mobnum.Length; j++)
        //                            {
        //                                fields.SetFieldProperty("untitled" + (j + 50), "textsize", 13f, null);
        //                                fields.SetField("untitled" + (j + 50), mobnum.Substring(j, 1));
        //                            }
        //                        }

        //                    }
        //                }// Mobile Number Breakdown end
        //            }

        //        }
        //        catch
        //        {

        //        }
        //        //Email Address
        //        try
        //        {
        //            fields.SetField("untitled151", GovForms.GetEmailInfo(uProfile.NTID).Rows[0][0].ToString());
        //        }
        //        catch
        //        {

        //        }
        //        #endregion

        //        #region Employer Data
        //        //Employer ID
        //        try
        //        {
        //            char[] ESSid = getNumeric(GovForms.getPagibigMPL(uProfile.NTID).Rows[0][39].ToString()).ToCharArray();
        //            for (int i = 0; i < ESSid.Length; i++)
        //            {
        //                fields.SetFieldProperty("untitled" + (i + 81), "textsize", 12f, null);
        //                fields.SetField("untitled" + (i + 81), ESSid[i].ToString());
        //            }
        //        }
        //        catch
        //        {

        //        }
        //        //Employer TIN
        //        try
        //        {
        //            char[] ETIN = getNumeric(GovForms.getCompanyProfile(uProfile.NTID).Rows[0][38].ToString()).ToCharArray();
        //            for (int i = 0; i < ETIN.Length; i++)
        //            {
        //                fields.SetFieldProperty("untitled" + (i + 94), "textsize", 12f, null);
        //                fields.SetField("untitled" + (i + 94), ETIN[i].ToString());
        //            }
        //        }
        //        catch
        //        {

        //        }
        //        //Type of Employer
        //        fields.SetField("TypeBusiness", "Yes", true);

        //        //Employer Name
        //        try
        //        {
        //            fields.SetField("untitled166", GovForms.getPagibigMPL(uProfile.NTID).Rows[0][38].ToString());
        //        }
        //        catch
        //        {

        //        }

        //        //Employer Address
        //        try
        //        {
        //            //Rm./Flr./Unit No. & Bldg. Name
        //            fields.SetField("untitled167", GovForms.getCompanyProfile(uProfile.NTID).Rows[0][6].ToString() + " " + GovForms.getCompanyProfile(uProfile.NTID).Rows[0][7].ToString());
        //            //House Number
        //            fields.SetField("untitled168", "");
        //            //Street Name
        //            fields.SetField("untitled169", GovForms.getCompanyProfile(uProfile.NTID).Rows[0][8].ToString());
        //            //Subdivision
        //            fields.SetField("untitled170", "");
        //            //Barangay/District/Locality
        //            fields.SetField("untitled171", GovForms.getCompanyProfile(uProfile.NTID).Rows[0][9].ToString());
        //            //City/Municipality
        //            fields.SetField("untitled172", GovForms.getCompanyProfile(uProfile.NTID).Rows[0][10].ToString());
        //            //Province
        //            fields.SetField("untitled173", GovForms.getCompanyProfile(uProfile.NTID).Rows[0][11].ToString());
        //        }
        //        catch
        //        {

        //        }

        //        //ZIP Code
        //        try
        //        {
        //            char[] zip = getNumeric(GovForms.getPagibigMPL(uProfile.NTID).Rows[0][33].ToString()).ToCharArray();
        //            for (int i = 0; i < zip.Length; i++)
        //            {
        //                fields.SetFieldProperty("untitled" + (i + 116), "textsize", 12f, null);
        //                fields.SetField("untitled" + (i + 116), zip[i].ToString());
        //            }
        //        }
        //        catch
        //        {

        //        }
        //        //Website
        //        fields.SetField("untitled175", GovForms.getCompanyProfile(uProfile.NTID).Rows[0][65].ToString());

        //        //contact number
        //        try
        //        {

        //            for (int i = 0; i < GovForms.GetCompanyContactPhone(empbranchid).Rows.Count; i++)
        //            {
        //                if (GovForms.GetCompanyContactPhone(empbranchid).Rows[i][0].ToString() == "Work" || GovForms.GetCompanyContactPhone(empbranchid).Rows[i][0].ToString() == "Home")
        //                {
        //                    char[] num = getNumeric(GovForms.GetCompanyContactPhone(empbranchid).Rows[i][1].ToString()).ToCharArray();
        //                    for (int j = 0; j < num.Length; j++)
        //                    {
        //                        fields.SetFieldProperty("untitled" + (i + 106), "textsize", 12f, null);
        //                        fields.SetField("untitled" + (i + 106), num[i].ToString());
        //                    }
        //                }
        //            }

        //        }
        //        catch
        //        {

        //        }

        //        //Email Address
        //        try
        //        {
        //            fields.SetField("untitled174", GovForms.GetCompanyEmail(empbranchid).Rows[0][0].ToString());
        //        }
        //        catch
        //        {

        //        }

        //        //Monthly Salary
        //        try
        //        {
        //            fields.SetField("untitled176", GovForms.GetEmployeePayforDocs(uProfile.NTID).Rows[0][29].ToString());
        //        }
        //        catch
        //        {

        //        }
        //        #endregion

        //        #region CERTIFICATION
        //        //Certification
        //        //Printed Name
        //        fields.SetField("untitled155", (fname + " " + mname + " " + lname).ToUpper());
        //        fields.SetField("untitled180", (fname + " " + mname + " " + lname).ToUpper());
        //        //Position
        //        fields.SetField("untitled182", GovForms.getJobDesc(uProfile.NTID).ToUpper());
        //        //Date
        //        fields.SetField("untitled165", DateTime.Now.ToString("yyyy-MM-dd"));
        //        fields.SetField("untitled183", DateTime.Now.ToString("yyyy-MM-dd"));

        //        //Page 3
        //        //SSS Number
        //        try
        //        {
        //            char[] SSNum = getNumeric(GovForms.getPagibigMPL(uProfile.NTID).Rows[0][1].ToString()).ToCharArray();
        //            for (int i = 0; i < SSNum.Length; i++)
        //            {
        //                if (i == 0)
        //                {
        //                    fields.SetFieldProperty("untitled" + (i + 199), "textsize", 12f, null);
        //                    fields.SetField("untitled" + (i + 199), SSNum[i].ToString());
        //                }
        //                else
        //                {
        //                    fields.SetFieldProperty("untitled" + (i + 200), "textsize", 12f, null);
        //                    fields.SetField("untitled" + (i + 200), SSNum[i].ToString());
        //                }
        //            }
        //        }
        //        catch
        //        {

        //        }
        //        //Name
        //        try
        //        {
        //            //Last Name
        //            fields.SetField("untitled210", lname);
        //            //First Name
        //            fields.SetField("untitled211", fname);
        //            //Middle Name
        //            fields.SetField("untitled212", mname);
        //            //Suffix
        //            fields.SetField("untitled213", suffix);
        //        }
        //        catch
        //        {

        //        }
        //        #endregion

        //        #region Employee Signature
        //        //Employee Signature
        //        try
        //        {
        //            string BitEmpsign = null;
        //            Byte[] EmpsignBit = null;
        //            iTextSharp.text.Image Empsign = null;
        //            iTextSharp.text.Image Empsign2 = null;
        //            var EmpsignContentByte = stamper.GetOverContent(1);
        //            GovForms.GetSignatureSignPicture(uProfile.NTID, "sign_0");
        //            BitEmpsign = GovForms.BitSign;

        //            if (!String.IsNullOrWhiteSpace(BitEmpsign))
        //            {
        //                AcroFields.FieldPosition wew = fields.GetFieldPositions("untitled156")[0];
        //                iTextSharp.text.Rectangle rect = wew.position;
        //                string xxxx = rect.Left.ToString();
        //                string yyyy = rect.Bottom.ToString();
        //                BitEmpsign = BitEmpsign.Replace(' ', '+');
        //                EmpsignBit = Convert.FromBase64String(BitEmpsign);
        //                Empsign = iTextSharp.text.Image.GetInstance(EmpsignBit);
        //                Empsign.SetAbsolutePosition(float.Parse(xxxx) + 70, float.Parse(yyyy));
        //                Empsign.ScaleToFit(80, 80);
        //                EmpsignContentByte.AddImage(Empsign);

        //                wew = fields.GetFieldPositions("untitled181")[0];
        //                rect = wew.position;
        //                xxxx = rect.Left.ToString();
        //                yyyy = rect.Bottom.ToString();
        //                BitEmpsign = BitEmpsign.Replace(' ', '+');
        //                EmpsignBit = Convert.FromBase64String(BitEmpsign);
        //                Empsign2 = iTextSharp.text.Image.GetInstance(EmpsignBit);
        //                Empsign2.SetAbsolutePosition(float.Parse(xxxx) + 70, float.Parse(yyyy));
        //                Empsign2.ScaleToFit(80, 80);
        //                EmpsignContentByte.AddImage(Empsign2);
        //            }

        //        }
        //        catch
        //        {

        //        }
        //        #endregion
        //        stamper.FormFlattening = true;

        //        stamper.Close();
        //        uProfile.Status = "Success";

        //    }
        //    catch (Exception ex)
        //    {
        //        uProfile.Status = ex.ToString();
        //        //this.Page.ClientScript.RegisterClientScriptBlock(this.GetType(),
        //        //Guid.NewGuid().ToString("N"), "self.parent.location='login.aspx';", true);
        //    }
        //    return uProfile.Status;
        //}
        #endregion
        #region "SSSForm_SicknessBenefit"
        [HttpPost]
        [Route("SSSForm_SicknessBenefit")]
        public string SSSForm_SicknessBenefit(UserProfile uProfile)
        {
            string path = HttpContext.Current.Server.MapPath("pdf");
            var pdfReader1 = new PdfReader(path + "/GovForms Templates/SSSForm_SicknessBenefitApplication.pdf");
            AcroFields af = pdfReader1.AcroFields;

            string formFile = path + "/GovForms Templates/SSSForm_SicknessBenefitApplication.pdf";
            string newFile = path + "/GovForms-Employee/SSSForm_SicknessBenefit-" + uProfile.NTID + ".pdf";

            PdfReader reader = new PdfReader(formFile);
            PdfStamper stamper = new PdfStamper(reader, new FileStream(newFile, FileMode.Create));
            AcroFields fields = stamper.AcroFields;
            AcroFields.Item item;
            try
            {
                GovForms.getEmployeePersonalInformation(uProfile.NTID);
                string fullname = GovForms.getPagibigMPL(uProfile.NTID).Rows[0][4].ToString().ToUpper() + " " + GovForms.getPagibigMPL(uProfile.NTID).Rows[0][5].ToString().ToUpper() + " " + GovForms.getPagibigMPL(uProfile.NTID).Rows[0][6].ToString().ToUpper();
                //SSS No.
                try
                {
                    fields.SetFieldProperty("untitled1", "textsize", 15f, null);
                    fields.SetFieldProperty("untitled2", "textsize", 15f, null);
                    fields.SetFieldProperty("untitled3", "textsize", 15f, null);
                    string sssraw = GovForms.getPagibigMPL(uProfile.NTID).Rows[0][1].ToString().Replace("-", " ");
                    string[] arrsss = sssraw.Split(new char[0]);
                    fields.SetField("untitled1", arrsss[0].Substring(0, 1) + " " + arrsss[0].Substring(1, 1));
                    fields.SetField("untitled2", arrsss[1].Substring(0, 1) + "  " + arrsss[1].Substring(1, 1) + " " + arrsss[1].Substring(2, 1) + " " + arrsss[1].Substring(3, 1) + " " + arrsss[1].Substring(4, 1) + " " + arrsss[1].Substring(5, 1) + " " + arrsss[1].Substring(6, 1));
                    fields.SetField("untitled3", arrsss[2]);
                }
                catch { }
                //DOB
                try
                {
                    string bdayraw = GovForms.getPagibigMPL(uProfile.NTID).Rows[0][11].ToString();
                    string[] bdayarr = bdayraw.Split('-');
                    string date = bdayarr[1];
                    fields.SetFieldProperty("untitled7", "textsize", 15f, null);
                    fields.SetField("untitled7", bdayarr[1].Substring(0, 1) + " " + bdayarr[1].Substring(1, 1));
                    fields.SetFieldProperty("untitled8", "textsize", 15f, null);
                    fields.SetField("untitled8", bdayarr[2].Substring(0, 1) + " " + bdayarr[2].Substring(1, 1));
                    fields.SetFieldProperty("untitled9", "textsize", 15f, null);
                    fields.SetField("untitled9", bdayarr[0].Substring(0, 1) + "  " + bdayarr[0].Substring(1, 1) + " " + bdayarr[0].Substring(2, 1) + "  " + bdayarr[0].Substring(3, 1));
                }
                catch { }
                //TIN No.
                try
                {
                    string fullTIN = GovForms.getPagibigMPL(uProfile.NTID).Rows[0][2].ToString().Replace("-", "");
                    string TIN1 = fullTIN.Substring(0, 1); //First number
                    string TIN2 = fullTIN.Substring(1, 1); //2nd number
                    string TIN3 = fullTIN.Substring(2, 1); //3rd number
                    string TIN1st3 = TIN1 + "  " + TIN2 + " " + TIN3;
                    fields.SetFieldProperty("untitled10", "textsize", 15f, null);
                    string TIN4 = fullTIN.Substring(3, 1);
                    string TIN5 = fullTIN.Substring(4, 1);
                    string TIN6 = fullTIN.Substring(5, 1);
                    string TIN2nd3 = TIN4 + "  " + TIN5 + " " + TIN6;
                    fields.SetFieldProperty("untitled11", "textsize", 15f, null);
                    string TIN7 = fullTIN.Substring(6, 1);
                    string TIN8 = fullTIN.Substring(7, 1);
                    string TIN9 = fullTIN.Substring(8, 1);
                    string TIN3rd3 = TIN7 + "  " + TIN8 + " " + TIN9;
                    fields.SetFieldProperty("untitled12", "textsize", 15f, null);
                    if (fullTIN.Length == 12)
                    {
                        string TIN10 = fullTIN.Substring(9, 1);
                        string TIN11 = fullTIN.Substring(10, 1);
                        string TIN12 = fullTIN.Substring(11, 1);
                        string TIN4th3 = TIN10 + " " + TIN11 + " " + TIN12;
                        fields.SetFieldProperty("untitled4", "textsize", 15f, null);
                        fields.SetField("untitled4", TIN4th3);
                    }
                    fields.SetField("untitled10", TIN1st3);
                    fields.SetField("untitled11", TIN2nd3);
                    fields.SetField("untitled12", TIN3rd3);
                }
                catch { }
                //address
                try
                {
                    fields.SetField("untitled17", GovForms.getPagibigMPL(uProfile.NTID).Rows[0][5].ToString().ToUpper()); //last name
                    fields.SetField("untitled14", GovForms.getPagibigMPL(uProfile.NTID).Rows[0][4].ToString().ToUpper()); //first name
                    fields.SetField("untitled15", GovForms.getPagibigMPL(uProfile.NTID).Rows[0][6].ToString().ToUpper()); //middle name
                    fields.SetField("untitled18", GovForms.getPagibigMPL(uProfile.NTID).Rows[0][15].ToString().ToUpper()); //house no.
                    fields.SetField("untitled19", GovForms.getPagibigMPL(uProfile.NTID).Rows[0][21].ToString().ToUpper()); //street name
                    fields.SetField("untitled20", GovForms.getPagibigMPL(uProfile.NTID).Rows[0][20].ToString().ToUpper()); //subdivision
                    fields.SetField("untitled21", GovForms.getPagibigMPL(uProfile.NTID).Rows[0][7].ToString().ToUpper()); //barangay
                    fields.SetField("untitled22", GovForms.getPagibigMPL(uProfile.NTID).Rows[0][9].ToString().ToUpper()); //city
                    fields.SetField("untitled23", GovForms.provincea.ToString().ToUpper()); //province
                }
                catch { }
                //zip code
                try
                {
                    string zipraw = GovForms.getPagibigMPL(uProfile.NTID).Rows[0][36].ToString();
                    string zip1 = zipraw.Substring(0, 1);
                    string zip2 = zipraw.Substring(1, 1);
                    string zip3 = zipraw.Substring(2, 1);
                    string zip4 = zipraw.Substring(3, 1);
                    string zipfinal = zip1 + "  " + zip2 + "  " + zip3 + "  " + zip4;
                    fields.SetFieldProperty("untitled24", "textsize", 15f, null);
                    fields.SetField("untitled24", zipfinal);
                }
                catch { }
                //telephone and Mobile No.
                try
                {

                    for (int p = 0; p < GovForms.GetPersonalInfo(uProfile.NTID).Rows.Count; p++)
                    {
                        if (GovForms.GetPersonalInfo(uProfile.NTID).Rows[0][1].ToString() == "Home")
                        {
                            if (GovForms.GetPersonalInfo(uProfile.NTID).Rows[0][0].ToString().Length == 10)
                            {
                                string telareacoderaw = getNumeric(GovForms.GetPersonalInfo(uProfile.NTID).Rows[0][0].ToString());
                                string telareacode1 = telareacoderaw.Substring(0, 1);
                                string telareacode2 = telareacoderaw.Substring(1, 1);
                                string telareacode3 = telareacoderaw.Substring(2, 1);
                                string telareacodefinal = telareacode1 + " " + telareacode2 + " " + telareacode3;
                                fields.SetFieldProperty("untitled25", "textsize", 15f, null);
                                fields.SetField("untitled25", telareacodefinal);
                                string telnum1 = telareacoderaw.Substring(3, 1);
                                string telnum2 = telareacoderaw.Substring(4, 1);
                                string telnum3 = telareacoderaw.Substring(5, 1);
                                string telnum4 = telareacoderaw.Substring(6, 1);
                                string telnum5 = telareacoderaw.Substring(7, 1);
                                string telnum6 = telareacoderaw.Substring(8, 1);
                                string telnum7 = telareacoderaw.Substring(9, 1);
                                string telnumfinal = telnum1 + " " + telnum2 + " " + telnum3 + " " + telnum4 + "  " + telnum5 + " " + telnum6 + " " + telnum7;
                                fields.SetFieldProperty("untitled26", "textsize", 15f, null);
                                fields.SetField("untitled26", telnumfinal);
                            }

                        }
                        else if (GovForms.GetPersonalInfo(uProfile.NTID).Rows[0][1].ToString() == "Mobile")
                        {
                            string cpnumraw = getNumeric(GovForms.GetPersonalInfo(uProfile.NTID).Rows[0][0].ToString());
                            cpnumraw = cpnumraw.Length < 10 ? "0" + cpnumraw : cpnumraw;
                            char[] cpnum = cpnumraw.ToCharArray();
                            string cpnumareafinal = "";
                            cpnumareafinal = cpnum[0] + "  " + cpnum[1] + " " + cpnum[2] + " " + cpnum[3];
                            fields.SetFieldProperty("untitled27", "textsize", 15f, null);
                            fields.SetField("untitled27", cpnumareafinal);
                            string cpnumfinal = cpnum[4] + " " + cpnum[5] + " " + cpnum[6] + "  " + cpnum[7] + " " + cpnum[8] + "  " + cpnum[9] + " " + cpnum[10];
                            fields.SetFieldProperty("untitled28", "textsize", 15f, null);
                            fields.SetField("untitled28", cpnumfinal);
                        }
                    }

                }
                catch { }
                //Email Address
                try
                {
                    fields.SetField("untitled29", GovForms.GetEmailInfo(uProfile.NTID).Rows[0][0].ToString());
                }
                catch { }
                item = fields.GetFieldItem("untitled48");
                item.GetMerged(0).Put(PdfName.Q, new PdfNumber(PdfFormField.Q_CENTER));
                fields.SetField("untitled48", fullname.ToUpper());
                item = fields.GetFieldItem("untitled50");
                item.GetMerged(0).Put(PdfName.Q, new PdfNumber(PdfFormField.Q_CENTER));
                fields.SetField("untitled50", DateTime.Now.ToString("MM/dd/yyyy"));
                stamper.FormFlattening = true;
                stamper.Close();
                uProfile.Status = "Success";
            }
            catch (Exception ex)
            {
                uProfile.Status = ex.ToString();
            }

            return uProfile.Status;
        }
        #endregion
        #region "SSSForm_SicknessNotification"
        [HttpPost]
        [Route("SSSForm_SicknessNotification")]
        public string SSSForm_SicknessNotification(UserProfile uProfile)
        {
            string path = HttpContext.Current.Server.MapPath("pdf");
            var pdfReader1 = new PdfReader(path + "/GovForms Templates/SSSForm_SicknessNotification.pdf");
            AcroFields af = pdfReader1.AcroFields;

            string formFile = path + "/GovForms Templates/SSSForm_SicknessNotification.pdf";
            string newFile = path + "/GovForms-Employee/SSSForm_SicknessNotification-" + uProfile.NTID + ".pdf";

            PdfReader reader = new PdfReader(formFile);
            PdfStamper stamper = new PdfStamper(reader, new FileStream(newFile, FileMode.Create));
            AcroFields fields = stamper.AcroFields;
            try
            {
                string fullname = GovForms.getPagibigMPL(uProfile.NTID).Rows[0][4].ToString().ToUpper() + " " + GovForms.getPagibigMPL(uProfile.NTID).Rows[0][5].ToString().ToUpper() + " " + GovForms.getPagibigMPL(uProfile.NTID).Rows[0][6].ToString().ToUpper();
                //SSS
                try
                {
                    fields.SetFieldProperty("untitled1", "textsize", 15f, null);
                    fields.SetFieldProperty("untitled2", "textsize", 15f, null);
                    fields.SetFieldProperty("untitled3", "textsize", 15f, null);
                    string sssraw = GovForms.getPagibigMPL(uProfile.NTID).Rows[0][1].ToString().Replace("-", " ");
                    string[] arrsss = sssraw.Split(new char[0]);
                    fields.SetField("untitled1", arrsss[0].Substring(0, 1) + " " + arrsss[0].Substring(1, 1));
                    fields.SetField("untitled2", arrsss[1].Substring(0, 1) + " " + arrsss[1].Substring(1, 1) + " " + arrsss[1].Substring(2, 1) + " " + arrsss[1].Substring(3, 1) + " " + arrsss[1].Substring(4, 1) + " " + arrsss[1].Substring(5, 1) + " " + arrsss[1].Substring(6, 1));
                    fields.SetField("untitled3", arrsss[2]);
                }
                catch { }
                //CRN
                try
                {
                    string CRN = GovForms.GetSSSSalaryLoan(uProfile.NTID).Rows[0][15].ToString().Replace("-", "");
                    char[] arrCRN = CRN.ToCharArray();

                    fields.SetFieldProperty("untitled4", "textsize", 15f, null);
                    fields.SetField("untitled4", arrCRN[0] + " " + arrCRN[1] + " " + arrCRN[2]);
                    fields.SetFieldProperty("untitled5", "textsize", 15f, null);
                    fields.SetField("untitled5", arrCRN[3] + " " + arrCRN[4] + " " + arrCRN[5] + " " + arrCRN[6] + " " + arrCRN[7] + " " + arrCRN[8] + " " + arrCRN[9] + " " + arrCRN[10]);
                    fields.SetFieldProperty("untitled7", "textsize", 15f, null);
                    fields.SetField("untitled7", arrCRN[11].ToString());

                }
                catch { }
                //DOB
                try
                {
                    string bdayraw = GovForms.getPagibigMPL(uProfile.NTID).Rows[0][11].ToString();
                    string[] bdayarr = bdayraw.Split('-');
                    string newmonth = bdayarr[1];
                    fields.SetFieldProperty("untitled7", "textsize", 15f, null);
                    fields.SetField("untitled7", newmonth.Substring(0, 1) + " " + newmonth.Substring(1, 1));
                    fields.SetFieldProperty("untitled8", "textsize", 15f, null);
                    fields.SetField("untitled8", bdayarr[2].Substring(0, 1) + " " + bdayarr[2].Substring(1, 1));
                    fields.SetFieldProperty("untitled9", "textsize", 15f, null);
                    fields.SetField("untitled9", bdayarr[0].Substring(0, 1) + " " + bdayarr[0].Substring(1, 1) + " " + bdayarr[0].Substring(2, 1) + " " + bdayarr[0].Substring(3, 1));
                }
                catch { }
                //TIN
                try
                {
                    string fullTIN = GovForms.getPagibigMPL(uProfile.NTID).Rows[0][2].ToString().Replace("-", "");
                    string TIN1 = fullTIN.Substring(0, 1); //First number
                    string TIN2 = fullTIN.Substring(1, 1); //2nd number
                    string TIN3 = fullTIN.Substring(2, 1); //3rd number
                    string TIN1st3 = TIN1 + " " + TIN2 + " " + TIN3;
                    fields.SetFieldProperty("untitled10", "textsize", 15f, null);
                    string TIN4 = fullTIN.Substring(3, 1);
                    string TIN5 = fullTIN.Substring(4, 1);
                    string TIN6 = fullTIN.Substring(5, 1);
                    string TIN2nd3 = TIN4 + " " + TIN5 + " " + TIN6;
                    fields.SetFieldProperty("untitled11", "textsize", 15f, null);
                    string TIN7 = fullTIN.Substring(6, 1);
                    string TIN8 = fullTIN.Substring(7, 1);
                    string TIN9 = fullTIN.Substring(8, 1);
                    string TIN3rd3 = TIN7 + " " + TIN8 + " " + TIN9;
                    fields.SetFieldProperty("untitled12", "textsize", 15f, null);
                    if (fullTIN.Length == 12)
                    {
                        string TIN10 = fullTIN.Substring(9, 1);
                        string TIN11 = fullTIN.Substring(10, 1);
                        string TIN12 = fullTIN.Substring(11, 1);
                        string TIN4th3 = TIN10 + " " + TIN11 + " " + TIN12;
                        fields.SetFieldProperty("untitled4", "textsize", 15f, null);
                        fields.SetField("untitled4", TIN4th3);
                    }
                    fields.SetField("untitled10", TIN1st3);
                    fields.SetField("untitled11", TIN2nd3);
                    fields.SetField("untitled12", TIN3rd3);
                }
                catch { }
                //Name
                try
                {
                    fields.SetField("untitled13", GovForms.getPagibigMPL(uProfile.NTID).Rows[0][5].ToString().ToUpper()); //last name
                    fields.SetField("untitled14", GovForms.getPagibigMPL(uProfile.NTID).Rows[0][4].ToString().ToUpper()); //first name
                    fields.SetField("untitled15", GovForms.getPagibigMPL(uProfile.NTID).Rows[0][6].ToString().ToUpper()); //middle name
                }
                catch { }
                GovForms.getEmployeePersonalInformation(uProfile.NTID);
                //Address
                try
                {
                    fields.SetField("untitled18", GovForms.getPagibigMPL(uProfile.NTID).Rows[0][15].ToString().ToUpper()); //house no.
                    fields.SetField("untitled19", GovForms.getPagibigMPL(uProfile.NTID).Rows[0][21].ToString().ToUpper()); //street name
                    fields.SetField("untitled20", GovForms.getPagibigMPL(uProfile.NTID).Rows[0][20].ToString().ToUpper()); //subdivision
                    fields.SetField("untitled21", GovForms.getPagibigMPL(uProfile.NTID).Rows[0][7].ToString().ToUpper()); //barangay
                    fields.SetField("untitled22", GovForms.getPagibigMPL(uProfile.NTID).Rows[0][9].ToString().ToUpper()); //city
                    fields.SetField("untitled23", GovForms.provincea.ToString().ToUpper()); //city
                    string zipraw = GovForms.getPagibigMPL(uProfile.NTID).Rows[0][36].ToString();
                    string zip1 = zipraw.Substring(0, 1);
                    string zip2 = zipraw.Substring(1, 1);
                    string zip3 = zipraw.Substring(2, 1);
                    string zip4 = zipraw.Substring(3, 1);
                    string zipfinal = zip1 + " " + zip2 + "  " + zip3 + "  " + zip4;
                    fields.SetFieldProperty("untitled24", "textsize", 15f, null);
                    fields.SetField("untitled24", zipfinal);
                }
                catch { }
                try
                {
                    for (int i = 0; i < GovForms.GetPersonalInfo(uProfile.NTID).Rows.Count; i++)
                    {
                        string numtype = GovForms.GetPersonalInfo(uProfile.NTID).Rows[i][1].ToString();
                        if (numtype == "Home")
                        {
                            string num = GovForms.GetPersonalInfo(uProfile.NTID).Rows[i][0].ToString().Replace("-", "");
                            num = getNumeric(num);
                            if (num.Length == 10)
                            {
                                char[] numarr = num.ToCharArray();
                                fields.SetFieldProperty("untitled25", "textsize", 14f, null);
                                fields.SetFieldProperty("untitled26", "textsize", 14f, null);
                                fields.SetField("untitled25", numarr[0] + " " + numarr[1] + "  " + numarr[2]);
                                fields.SetField("untitled26", numarr[3] + " " + numarr[4] + " " + numarr[5] + "  " + numarr[6] + " " + numarr[7] + " " + numarr[8] + "  " + numarr[9]);
                            }
                            else if (num.Length == 7)
                            {
                                char[] numarr = num.ToCharArray();

                                fields.SetFieldProperty("untitled26", "textsize", 14f, null);
                                fields.SetField("untitled26", numarr[0] + " " + numarr[1] + " " + numarr[2] + "  " + numarr[3] + " " + numarr[4] + "  " + numarr[5] + "  " + numarr[6]);

                            }
                        }
                        else if (numtype == "Mobile")
                        {
                            //Mobile Number Breakdown
                            string mobnum = GovForms.GetPersonalInfo(uProfile.NTID).Rows[i][0].ToString().Replace("-", "").Replace("+63", "0").Replace("+", "");
                            bool mobnume = Regex.IsMatch(mobnum, @"^\d+$");// determine if string is pure numbers; return true & null value

                            if (mobnume.ToString() == "True")
                            {
                                char[] mobnumarr = mobnum.ToCharArray();
                                fields.SetFieldProperty("untitled27", "textsize", 14f, null);
                                fields.SetFieldProperty("untitled28", "textsize", 14f, null);
                                fields.SetField("untitled27", mobnumarr[0] + " " + mobnumarr[1] + "  " + mobnumarr[2] + " " + mobnumarr[3]);
                                fields.SetField("untitled28", mobnumarr[4] + " " + mobnumarr[5] + "  " + mobnumarr[6] + " " + mobnumarr[7] + " " + mobnumarr[8] + "  " + mobnumarr[9] + " " + mobnumarr[10]);

                            }
                        }// Mobile Number Breakdown end
                    }
                }
                catch { }
                //Employer Details
                try
                {
                    fields.SetField("untitled29", GovForms.getPagibigMPL(uProfile.NTID).Rows[0][17].ToString().ToUpper());
                    fields.SetField("untitled43", "");
                    string emplyrid = GovForms.getPagibigMPL(uProfile.NTID).Rows[0][39].ToString();
                    char[] emplyridnum = emplyrid.ToCharArray();
                    string emplyrid1 = emplyridnum[0] + " " + emplyridnum[1];
                    fields.SetFieldProperty("untitled42", "textsize", 15f, null);
                    fields.SetField("untitled42", emplyrid1);
                    string emplyrid2 = emplyridnum[2] + " " + emplyridnum[3] + " " + emplyridnum[4] + " " + emplyridnum[5] + " " + emplyridnum[6] + " " + emplyridnum[7] + " " + emplyridnum[8] + " " + emplyridnum[9];
                    fields.SetFieldProperty("untitled43", "textsize", 15f, null);
                    fields.SetField("untitled43", emplyrid2);
                    fields.SetField("untitled47", GovForms.getPagibigMPL(uProfile.NTID).Rows[0][38].ToString().ToUpper());
                    fields.SetField("untitled46", GovForms.getPagibigMPL(uProfile.NTID).Rows[0][40].ToString().ToUpper());
                    fields.SetFieldProperty("untitled45", "textsize", 8f, null);
                    string nobldg = GovForms.getPagibigMPL(uProfile.NTID).Rows[0][32].ToString().ToUpper() + " " + GovForms.getPagibigMPL(uProfile.NTID).Rows[0][27].ToString().ToUpper();
                    fields.SetField("untitled45", nobldg);
                    fields.SetFieldProperty("untitled48", "textsize", 8f, null);
                    fields.SetField("untitled48", GovForms.getPagibigMPL(uProfile.NTID).Rows[0][26].ToString().ToUpper());
                    fields.SetField("untitled49", GovForms.getPagibigMPL(uProfile.NTID).Rows[0][30].ToString().ToUpper());
                    fields.SetField("untitled50", GovForms.getPagibigMPL(uProfile.NTID).Rows[0][28].ToString().ToUpper());
                    string empzip = GovForms.getPagibigMPL(uProfile.NTID).Rows[0][33].ToString();
                    char[] empzipnum = empzip.ToCharArray();
                    fields.SetFieldProperty("untitled51", "textsize", 15f, null);
                    fields.SetField("untitled51", empzipnum[0] + "  " + empzipnum[1] + " " + empzipnum[2] + "  " + empzipnum[3]);
                }
                catch { }

                fields.SetField("untitled33", fullname.ToUpper());
                fields.SetField("untitled73", DateTime.Now.ToString("yyyy-MM-dd"));
                GovForms.GetRequestFormSettings();
                GovForms.SignatoryNew(uProfile.NTID, formtype);
                GovForms.GetAppEMP(68, uProfile.NTID);
                string empidApprover = "";
                empidApprover = GovForms.ApproverEMPID;
                if (empidApprover == "" || empidApprover == null || string.IsNullOrWhiteSpace(empidApprover))
                {
                }
                else
                {
                    GovForms.GetApproverNameJobDes(empidApprover);
                    string Approvername = GovForms.ApproverName;
                    string ApproverDesc = GovForms.ApproverJobDesc;
                    GovForms.GetSignatureSignPicture(empidApprover, "sign_1");
                    string Bitsignature = null;
                    Bitsignature = GovForms.BitSign;
                    Byte[] SignatureBit = null;
                    iTextSharp.text.Image signatory = null;
                    var pdfContentByte = stamper.GetOverContent(1);
                    if (string.IsNullOrWhiteSpace(Bitsignature) == false)
                    {
                        Bitsignature = Bitsignature.Replace(' ', '+');
                        SignatureBit = Convert.FromBase64String(Bitsignature);
                        signatory = iTextSharp.text.Image.GetInstance(SignatureBit);
                        signatory.SetAbsolutePosition(60, 360);
                        signatory.ScaleToFit(100, 100);
                        fields.SetField("untitled69", Approvername);
                        fields.SetField("untitled71", ApproverDesc);
                        pdfContentByte.AddImage(signatory);
                    }
                }

                stamper.FormFlattening = true;
                stamper.Close();
                uProfile.Status = "Success";
            }
            catch (Exception ex)
            {
                uProfile.Status = ex.ToString();

            }

            return uProfile.Status;

        }
        #endregion
        #region "SSSForm_MaternityBenefitApplication"
        [HttpPost]
        [Route("SSSForm_MaternityBenefitApplication")]
        public string SSSForm_MaternityBenefitApplication(UserProfile uProfile)
        {
            string path = HttpContext.Current.Server.MapPath("pdf");
            var pdfReader1 = new PdfReader(path + "/GovForms Templates/SSSForm_MaternityBenefitApplication.pdf");
            AcroFields af = pdfReader1.AcroFields;
            AcroFields.Item item;
            string formFile = path + "/GovForms Templates/SSSForm_MaternityBenefitApplication.pdf";
            string newFile = path + "/GovForms-Employee/SSSForm_MaternityBenefitApplication-" + uProfile.NTID + ".pdf";

            PdfReader reader = new PdfReader(formFile);
            PdfStamper stamper = new PdfStamper(reader, new FileStream(newFile, FileMode.Create));
            AcroFields fields = stamper.AcroFields;
            try
            {
                GovForms.getEmployeePersonalInformation(uProfile.NTID);
                //SSS number
                try
                {
                    string sssraw = GovForms.getPagibigMPL(uProfile.NTID).Rows[0][1].ToString().Replace("-", "");
                    char[] arrsss = sssraw.ToCharArray();
                    for (int i = 1; i <= 10; i++)
                    {
                        fields.SetFieldProperty("untitled" + i, "textsize", 15f, null);
                        fields.SetField("untitled" + i, arrsss[i - 1].ToString());
                    }

                }
                catch
                {

                }


                //CRN
                try
                {
                    string CRN = GovForms.GetSSSSalaryLoan(uProfile.NTID).Rows[0][15].ToString().Replace("-", "");
                    char[] arrCRN = CRN.ToCharArray();
                    for (int i = 0; i < 12; i++)
                    {
                        fields.SetFieldProperty("untitled" + (i + 11), "textsize", 15f, null);
                        fields.SetField("untitled" + (i + 11), arrCRN[i].ToString());
                    }
                }
                catch
                {

                }

                try
                {
                    var s = GovForms.GetMatBen(uProfile.NTID).Rows[0][6].ToString();
                    string[] s2 = s.Split('-');
                    string month;
                    string month2;
                    month = s2[1].Substring(0, 1);
                    month2 = s2[1].Substring(1, 1);
                    fields.SetFieldProperty("untitled23", "textsize", 15f, null);
                    fields.SetField("untitled23", month);
                    fields.SetFieldProperty("untitled24", "textsize", 15f, null);
                    fields.SetField("untitled24", month2);
                    //day
                    string day = s2[2];
                    int day1 = Convert.ToInt32(day.Remove(day.Length - 1));
                    fields.SetFieldProperty("untitled25", "textsize", 15f, null);
                    fields.SetField("untitled25", day.Substring(0, 1));
                    fields.SetFieldProperty("untitled26", "textsize", 15f, null);
                    fields.SetField("untitled26", day.Substring(1, 1));

                    string year = s2[0];
                    fields.SetFieldProperty("untitled27", "textsize", 15f, null);
                    fields.SetField("untitled27", year.Substring(0, 1));
                    fields.SetFieldProperty("untitled28", "textsize", 15f, null);
                    fields.SetField("untitled28", year.Substring(1, 1));
                    fields.SetFieldProperty("untitled29", "textsize", 15f, null);
                    fields.SetField("untitled29", year.Substring(2, 1));
                    fields.SetFieldProperty("untitled30", "textsize", 15f, null);
                    fields.SetField("untitled30", year.Substring(3, 1));
                    //DOB Breakdown end
                }
                catch
                {

                }
                try
                {
                    //TIN Breakdown

                    string fullTIN = GovForms.GetMatBen(uProfile.NTID).Rows[0][13].ToString().Replace("-", "");
                    bool TINe = Regex.IsMatch(fullTIN, @"^\d+$");// determine if string is pure numbers; return true & null value
                    string[] TINfield = new string[9] { "untitled31", "untitled32", "untitled33", "untitled34", "untitled35", "untitled36", "untitled37", "untitled38", "untitled39" };
                    if (TINe.ToString() == "True")
                    {
                        if (fullTIN.Length == 9)
                        {
                            for (int i = 0; i < fullTIN.Length; i++)
                            {
                                fields.SetFieldProperty(TINfield[i], "textsize", 15f, null);
                                fields.SetField(TINfield[i], fullTIN.Substring(i, 1));
                            }
                        }
                    }
                    //TIN Breakdown end
                }
                catch
                {

                }

                //Name
                try
                {
                    fields.SetField("untitled40", GovForms.GetMatBen(uProfile.NTID).Rows[0][2].ToString().First().ToString().ToUpper() + GovForms.GetMatBen(uProfile.NTID).Rows[0][2].ToString().Substring(1));
                    fields.SetField("untitled41", GovForms.GetMatBen(uProfile.NTID).Rows[0][0].ToString().First().ToString().ToUpper() + GovForms.GetMatBen(uProfile.NTID).Rows[0][0].ToString().Substring(1));
                    fields.SetField("untitled42", GovForms.GetMatBen(uProfile.NTID).Rows[0][1].ToString().First().ToString().ToUpper() + GovForms.GetMatBen(uProfile.NTID).Rows[0][1].ToString().Substring(1));
                }
                catch
                {

                }

                //Address
                try
                {
                    fields.SetField("untitled45", GovForms.GetMatBen(uProfile.NTID).Rows[0][7].ToString().First().ToString().ToUpper() + GovForms.GetMatBen(uProfile.NTID).Rows[0][7].ToString().Substring(1));
                    fields.SetField("untitled46", GovForms.GetMatBen(uProfile.NTID).Rows[0][8].ToString().First().ToString().ToUpper() + GovForms.GetMatBen(uProfile.NTID).Rows[0][8].ToString().Substring(1));
                    fields.SetField("untitled48", GovForms.GetMatBen(uProfile.NTID).Rows[0][9].ToString().First().ToString().ToUpper() + GovForms.GetMatBen(uProfile.NTID).Rows[0][9].ToString().Substring(1));
                    fields.SetField("untitled49", GovForms.GetMatBen(uProfile.NTID).Rows[0][10].ToString().First().ToString().ToUpper() + GovForms.GetMatBen(uProfile.NTID).Rows[0][10].ToString().Substring(1));
                    fields.SetField("untitled50", GovForms.provincea.ToString().First().ToString().ToUpper() + GovForms.provincea.ToString().Substring(1));
                }
                catch
                {

                }

                //ZIP Code 
                try
                {
                    string zipcode = GovForms.GetMatBen(uProfile.NTID).Rows[0][12].ToString();
                    bool zipcodee = Regex.IsMatch(zipcode, @"^\d+$");// determine if string is pure numbers; return true & null value
                    string[] zipfield = new string[4] { "untitled51", "untitled52", "untitled53", "untitled54" };
                    if (zipcodee.ToString() == "True")
                    {
                        for (int i = 0; i < zipcode.Length; i++)
                        {
                            if (zipcode.Length == 4)
                            {
                                fields.SetFieldProperty(zipfield[i], "textsize", 14f, null);
                                fields.SetField(zipfield[i], zipcode.Substring(i, 1));
                            }
                        }
                    }
                }
                catch
                {

                }
                //Zip Code End

                //Telephone breakdown 55-64
                try
                {
                    for (int i = 0; i < GovForms.GetPersonalInfo(uProfile.NTID).Rows.Count; i++)
                    {
                        string numtype = GovForms.GetPersonalInfo(uProfile.NTID).Rows[i][1].ToString();
                        if (numtype == "Home")
                        {
                            string num = GovForms.GetPersonalInfo(uProfile.NTID).Rows[i][0].ToString().Replace("-", "");
                            num = getNumeric(num);
                            if (num.Length == 10)
                            {
                                char[] numarr = num.ToCharArray();
                                for (int j = 0; j < 10; j++)
                                {
                                    fields.SetFieldProperty("untitled" + (j + 55), "textsize", 14f, null);
                                    fields.SetField("untitled" + (j + 55), numarr[j].ToString());
                                }
                            }
                            else if (num.Length == 7)
                            {
                                char[] numarr = num.ToCharArray();
                                for (int j = 0; j < 10; j++)
                                {
                                    fields.SetFieldProperty("untitled" + (j + 58), "textsize", 14f, null);
                                    fields.SetField("untitled" + (j + 58), numarr[j].ToString());
                                }

                            }
                        }
                        else if (numtype == "Mobile")
                        {
                            //Mobile Number Breakdown
                            string mobnum = GovForms.GetPersonalInfo(uProfile.NTID).Rows[i][0].ToString().Replace("-", "").Replace("+63", "0").Replace("+", "");
                            bool mobnume = Regex.IsMatch(mobnum, @"^\d+$");// determine if string is pure numbers; return true & null value
                            string[] marks = new string[11] { "untitled65", "untitled66", "untitled67", "untitled68", "untitled69", "untitled70", "untitled71", "untitled72", "untitled73", "untitled74", "untitled75" };

                            if (mobnume.ToString() == "True")
                            {
                                if (mobnum.Length == 11)
                                {
                                    for (int j = 0; j < mobnum.Length; j++)
                                    {
                                        fields.SetFieldProperty(marks[j], "textsize", 13f, null);
                                        fields.SetField(marks[j], mobnum.Substring(j, 1));
                                    }
                                }

                            }
                        }// Mobile Number Breakdown end
                    }

                }
                catch
                {

                }

                //Email Address
                try
                {
                    fields.SetField("untitled76", GovForms.GetEmailInfo(uProfile.NTID).Rows[0][0].ToString());
                }
                catch
                {

                }

                String fname = GovForms.GetLoyaltyCard(uProfile.NTID).Rows[0][0].ToString().First().ToString().ToUpper() + GovForms.GetLoyaltyCard(uProfile.NTID).Rows[0][0].ToString().Substring(1);
                String mname = GovForms.GetLoyaltyCard(uProfile.NTID).Rows[0][1].ToString() == "" ? "" : GovForms.GetLoyaltyCard(uProfile.NTID).Rows[0][1].ToString().First().ToString().ToUpper() + GovForms.GetLoyaltyCard(uProfile.NTID).Rows[0][1].ToString().Substring(1);
                String lname = GovForms.GetLoyaltyCard(uProfile.NTID).Rows[0][2].ToString().First().ToString().ToUpper() + GovForms.GetLoyaltyCard(uProfile.NTID).Rows[0][2].ToString().Substring(1);
                String fullname = lname + ", " + fname + " " + mname;
                item = fields.GetFieldItem("untitled117");
                item.GetMerged(0).Put(PdfName.Q, new PdfNumber(PdfFormField.Q_CENTER));
                fields.SetField("untitled117", fullname.ToUpper());
                item = fields.GetFieldItem("untitled119");
                item.GetMerged(0).Put(PdfName.Q, new PdfNumber(PdfFormField.Q_CENTER));
                fields.SetField("untitled119", DateTime.Now.ToString("yyyy-MM-dd"));

                #region Acknowledgement Stub

                ////CRN
                //try
                //{
                //    string CRN = GovForms.GetSSSSalaryLoan(uProfile.NTID).Rows[0][15].ToString().Replace("-", "");
                //    char[] arrCRN = CRN.ToCharArray();
                //    for (int i = 0; i < 12; i++)
                //    {
                //        fields.SetFieldProperty("untitled" + (i + 128), "textsize", 15f, null);
                //        fields.SetField("untitled" + (i + 128), arrCRN[i].ToString());
                //    }
                //}
                //catch
                //{

                //}

                //fields.SetField("untitled140", lname);
                //fields.SetField("untitled141", fname);
                //fields.SetField("untitled142", lname);
                //item = fields.GetFieldItem("untitled152");
                //item.GetMerged(0).Put(PdfName.Q, new PdfNumber(PdfFormField.Q_CENTER));
                //fields.SetField("untitled152", fullname.ToUpper());
                //item = fields.GetFieldItem("untitled154");
                //item.GetMerged(0).Put(PdfName.Q, new PdfNumber(PdfFormField.Q_CENTER));
                //fields.SetFieldProperty("untitled154", "textsize", 7f, null);
                //fields.SetField("untitled154", DateTime.Now.ToString("MM/dd/yy hh:mm tt"));

                ////3rd Page
                ////SSS number
                //try
                //{
                //    string sssraw = GovForms.getPagibigMPL(uProfile.NTID).Rows[0][1].ToString().Replace("-", "");
                //    char[] arrsss = sssraw.ToCharArray();
                //    for (int i = 1; i <= 10; i++)
                //    {
                //        //fields.SetFieldProperty("untitled" + (i + 186), "textsize", 15f, null);
                //        fields.SetField("untitled" + (i + 185), arrsss[i - 1].ToString());
                //    }
                //    fields.SetField("untitled196", lname);
                //    fields.SetField("untitled197", fname);
                //    fields.SetField("untitled198", lname);
                //}
                //catch
                //{

                //}
                #endregion

                try
                {
                    //Employee Signature
                    string BitEmpsign = null;
                    Byte[] EmpsignBit = null;
                    iTextSharp.text.Image Empsign = null;
                    var EmpsignContentByte = stamper.GetOverContent(1);
                    GovForms.GetSignatureSignPicture(uProfile.NTID, "sign_0");
                    BitEmpsign = GovForms.BitSign;

                    if (!String.IsNullOrWhiteSpace(BitEmpsign))
                    {
                        AcroFields.FieldPosition wew = fields.GetFieldPositions("untitled118")[0];
                        Rectangle rect = wew.position;
                        string xxxx = rect.Left.ToString();
                        string yyyy = rect.Bottom.ToString();
                        BitEmpsign = BitEmpsign.Replace(' ', '+');
                        EmpsignBit = Convert.FromBase64String(BitEmpsign);
                        Empsign = iTextSharp.text.Image.GetInstance(EmpsignBit);
                        Empsign.SetAbsolutePosition(float.Parse(xxxx) + 70, float.Parse(yyyy));
                        Empsign.ScaleToFit(80, 80);
                        EmpsignContentByte.AddImage(Empsign);
                    }
                }
                catch
                {

                }
                stamper.FormFlattening = true;
                stamper.Close();
                uProfile.Status = "Success";
            }
            catch (Exception ex)
            {
                uProfile.Status = ex.ToString();
            }

            return uProfile.Status;
        }
        #endregion
        #region "SSSFORMSPERSONALRECORDE1"
        [HttpPost]
        [Route("SSSFORMSPERSONALRECORDE1")]
        public string SSSFORMSPERSONALRECORDE1(UserProfile uProfile)
        {
            string path = HttpContext.Current.Server.MapPath("pdf");
            var pdfReader1 = new PdfReader(path + "/GovForms Templates/SSSFORMSPERSONALRECORDE1.pdf");
            AcroFields af = pdfReader1.AcroFields;
            string formFile = path + "/GovForms Templates/SSSFORMSPERSONALRECORDE1.pdf";
            string newFile = path + "/GovForms-Employee/SSSFORMSPERSONALRECORDE1-" + uProfile.NTID + ".pdf";

            PdfReader reader = new PdfReader(formFile);
            PdfStamper stamper = new PdfStamper(reader, new FileStream(newFile, FileMode.Create));
            AcroFields fields = stamper.AcroFields;
            AcroFields.Item item;
            try
            {
                DataTable E1dt = new DataTable();
                try
                {
                    con2.myparameters.Clear();
                    con2.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = uProfile.NTID });
                    E1dt = con2.GetDataTable("sp_SearchHRMSEmployeePersonalInformation");
                }
                catch { }
                GovForms.getEmployeePersonalInformation(uProfile.NTID);
                fields.SetField("untitled1", GovForms.GetSSSFORMSE1(uProfile.NTID).Rows[0][2].ToString().ToUpper());
                fields.SetField("untitled2", GovForms.GetSSSFORMSE1(uProfile.NTID).Rows[0][0].ToString().ToUpper());
                fields.SetField("untitled3", GovForms.GetSSSFORMSE1(uProfile.NTID).Rows[0][1].ToString().ToUpper());
                try
                {
                    string TIN = GovForms.GetSSSSalaryLoan(uProfile.NTID).Rows[0][13].ToString().Replace("-", "");
                    string tn1 = TIN.Substring(0, 1);
                    string tn2 = TIN.Substring(1, 1);
                    string tn3 = TIN.Substring(2, 1);
                    string TinNumber = tn1 + "  " + tn2 + "  " + tn3;
                    fields.SetFieldProperty("untitled72", "textsize", 14f, null);
                    fields.SetField("untitled72", TinNumber);
                    string tn4 = TIN.Substring(3, 1);
                    string tn5 = TIN.Substring(4, 1);
                    string tn6 = TIN.Substring(5, 1);
                    string TinNumber1 = tn4 + "  " + tn5 + "  " + tn6;
                    fields.SetFieldProperty("untitled73", "textsize", 14f, null);
                    fields.SetField("untitled73", TinNumber1);
                    string tn7 = TIN.Substring(6, 1);
                    string tn8 = TIN.Substring(7, 1);
                    string tn9 = TIN.Substring(8, 1);
                    string TinNumber2 = tn7 + "  " + tn8 + "  " + tn9;
                    fields.SetFieldProperty("untitled130", "textsize", 14f, null);
                    fields.SetField("untitled130", TinNumber2);
                }
                catch
                {
                }
                try
                {
                    string bdayraw = GovForms.getPagibigMPL(uProfile.NTID).Rows[0][11].ToString();
                    string[] bdayarr = bdayraw.Split('-');
                    fields.SetFieldProperty("untitled5", "textsize", 14f, null);
                    fields.SetFieldProperty("untitled6", "textsize", 14f, null);
                    fields.SetFieldProperty("untitled7", "textsize", 14f, null);
                    fields.SetField("untitled5", bdayarr[1].Substring(0, 1) + "  " + bdayarr[1].Substring(1, 1));
                    fields.SetField("untitled6", bdayarr[2].Substring(0, 1) + "  " + bdayarr[2].Substring(1, 1));
                    fields.SetField("untitled7", bdayarr[0].Substring(0, 1) + "  " + bdayarr[0].Substring(1, 1) + "  " + bdayarr[0].Substring(2, 1) + "  " + bdayarr[0].Substring(3, 1));
                }
                catch { }
                //address
                try
                {
                    fields.SetField("untitled10", GovForms.GetSSSFORMSE1(uProfile.NTID).Rows[0][4].ToString().First().ToString().ToUpper() + GovForms.GetSSSFORMSE1(uProfile.NTID).Rows[0][4].ToString().Substring(1));
                    fields.SetField("untitled16", GovForms.GetSSSFORMSE1(uProfile.NTID).Rows[0][5].ToString().First().ToString().ToUpper() + GovForms.GetSSSFORMSE1(uProfile.NTID).Rows[0][5].ToString().Substring(1));
                    fields.SetField("untitled21", GovForms.GetSSSFORMSE1(uProfile.NTID).Rows[0][6].ToString().First().ToString().ToUpper() + GovForms.GetSSSFORMSE1(uProfile.NTID).Rows[0][6].ToString().Substring(1));
                    fields.SetField("untitled22", GovForms.GetSSSFORMSE1(uProfile.NTID).Rows[0][7].ToString().First().ToString().ToUpper() + GovForms.GetSSSFORMSE1(uProfile.NTID).Rows[0][7].ToString().Substring(1));
                    fields.SetField("untitled24", GovForms.GetSSSFORMSE1(uProfile.NTID).Rows[0][8].ToString().First().ToString().ToUpper() + GovForms.GetSSSFORMSE1(uProfile.NTID).Rows[0][8].ToString().Substring(1));
                    fields.SetField("untitled25", GovForms.GetSSSFORMSE1(uProfile.NTID).Rows[0][10].ToString().First().ToString().ToUpper() + GovForms.GetSSSFORMSE1(uProfile.NTID).Rows[0][10].ToString().Substring(1));
                    fields.SetField("untitled25", GovForms.GetSSSFORMSE1(uProfile.NTID).Rows[0][10].ToString().First().ToString().ToUpper() + GovForms.GetSSSFORMSE1(uProfile.NTID).Rows[0][10].ToString().Substring(1));
                    fields.SetField("untitled26", GovForms.provincea);
                    fields.SetField("untitled28", GovForms.GetSSSFORMSE1(uProfile.NTID).Rows[0][11].ToString().First().ToString().ToUpper() + GovForms.GetSSSFORMSE1(uProfile.NTID).Rows[0][11].ToString().Substring(1));
                }
                catch { }
                try
                {
                    for (int i = 0; i < GovForms.GetPersonalInfo(uProfile.NTID).Rows.Count; i++)
                    {
                        string numtype = GovForms.GetPersonalInfo(uProfile.NTID).Rows[i][1].ToString();
                        if (numtype == "Mobile")
                        {
                            fields.SetField("untitled29", GovForms.GetPersonalInfo(uProfile.NTID).Rows[i][0].ToString().First().ToString().ToUpper() + GovForms.GetPersonalInfo(uProfile.NTID).Rows[i][0].ToString().Substring(1));
                        }
                        if (numtype == "Home")
                        {
                            fields.SetField("untitled31", GovForms.GetPersonalInfo(uProfile.NTID).Rows[i][0].ToString().First().ToString().ToUpper() + GovForms.GetPersonalInfo(uProfile.NTID).Rows[i][0].ToString().Substring(1));
                        }
                    }
                }
                catch
                {
                }
                fields.SetField("untitled30", GovForms.GetEmailInfo(uProfile.NTID).Rows[0][0].ToString() == "" ? "" : GovForms.GetEmailInfo(uProfile.NTID).Rows[0][0].ToString().First().ToString().ToUpper() + GovForms.GetEmailInfo(uProfile.NTID).Rows[0][0].ToString().Substring(1));
                for (int i = 0; i < GovForms.getDependent(uProfile.NTID).Rows.Count; i++)
                {
                    try
                    {
                        if (GovForms.getDependent(uProfile.NTID).Rows[i][2].ToString() == "Mother")
                        {
                            string firstname = GovForms.getDependent(uProfile.NTID).Rows[i][3].ToString();
                            string midname = GovForms.getDependent(uProfile.NTID).Rows[i][4].ToString();
                            string lastname = GovForms.getDependent(uProfile.NTID).Rows[i][5].ToString();
                            fields.SetField("untitled36", lastname);
                            fields.SetField("untitled37", firstname);
                            fields.SetField("untitled38", midname);
                        }
                        else if (GovForms.getDependent(uProfile.NTID).Rows[i][2].ToString() == "Father")
                        {
                            string firstname = GovForms.getDependent(uProfile.NTID).Rows[i][3].ToString();
                            string midname = GovForms.getDependent(uProfile.NTID).Rows[i][4].ToString();
                            string lastname = GovForms.getDependent(uProfile.NTID).Rows[i][5].ToString();
                            fields.SetField("untitled32", lastname);
                            fields.SetField("untitled33", firstname);
                            fields.SetField("untitled34", midname);
                        }
                    }
                    catch
                    {
                    }
                }
                //gender
                try
                {
                    if (GovForms.GetSSSFORMSE1(uProfile.NTID).Rows[0][3].ToString() == "M")
                    {
                        fields.SetField("untitled8", "Yes", true);
                    }
                    else if (GovForms.GetSSSFORMSE1(uProfile.NTID).Rows[0][3].ToString() == "F")
                    {
                        fields.SetField("untitled9", "Yes", true);
                    }
                }
                catch { }
                //civil status
                try
                {
                    if (GovForms.GetSSSFORMSE1(uProfile.NTID).Rows[0][15].ToString() == "Single")
                    {
                        fields.SetField("untitled10", "Yes");
                    }
                    else if (GovForms.GetSSSFORMSE1(uProfile.NTID).Rows[0][15].ToString() == "Married Filing Separately")
                    {
                        fields.SetField("untitled11", "Yes");
                    }
                    else if (GovForms.GetSSSFORMSE1(uProfile.NTID).Rows[0][15].ToString() == "Widowed")
                    {
                        fields.SetField("untitled12", "Yes");
                    }
                    else if (GovForms.GetSSSFORMSE1(uProfile.NTID).Rows[0][15].ToString() == "Legaly Separated")
                    {
                        fields.SetField("untitled13", "Yes");
                    }
                    else if (GovForms.GetSSSFORMSE1(uProfile.NTID).Rows[0][15].ToString() == "")
                    {
                        fields.SetField("untitled14", "Yes");
                    }
                }
                catch { }
                //religion & place of birth
                try
                {
                    fields.SetField("untitled17", E1dt.Rows[0][28].ToString());
                    fields.SetField("untitled18", E1dt.Rows[0][27].ToString());
                }
                catch { }
                //parents' names
                try
                {
                    //father
                    string[] faname = E1dt.Rows[0][30].ToString().Split('/');
                    fields.SetField("untitled32", faname[2].ToString());
                    fields.SetField("untitled33", faname[0].ToString());
                    fields.SetField("untitled34", faname[1].ToString());

                    //mother
                    string[] moname = E1dt.Rows[0][29].ToString().Split('/');
                    fields.SetField("untitled36", moname[2].ToString());
                    fields.SetField("untitled37", moname[0].ToString());
                    fields.SetField("untitled38", moname[1].ToString());
                }
                catch { }

                //Dependents
                string[,] childName = new string[20, 10];
                int j = 0;
                for (int i = 0; i < GovForms.getDependent(uProfile.NTID).Rows.Count; i++)
                {
                    try
                    {

                        if (GovForms.getDependent(uProfile.NTID).Rows[i][2].ToString() == "Spouse")
                        {
                            string firstname = GovForms.getDependent(uProfile.NTID).Rows[i][3].ToString();
                            string midname = GovForms.getDependent(uProfile.NTID).Rows[i][4].ToString();
                            string lastname = GovForms.getDependent(uProfile.NTID).Rows[i][5].ToString();
                            string suffix = (String.IsNullOrEmpty(GovForms.getDependent(uProfile.NTID).Rows[i][5].ToString())) ? GovForms.getDependent(uProfile.NTID).Rows[i][5].ToString() : "";

                            fields.SetField("untitled40", lastname);
                            fields.SetField("untitled41", firstname);
                            fields.SetField("untitled42", midname);
                            fields.SetField("untitled43", suffix);
                            //DOB
                            string spouseDOBraw = GovForms.getDependent(uProfile.NTID).Rows[i][7].ToString();
                            spouseDOBraw = spouseDOBraw.Replace(",", "");
                            spouseDOBraw = spouseDOBraw.Replace(" ", "-");
                            string[] spouseDOB = spouseDOBraw.Split('-');
                            if (spouseDOB[1].Length == 1)
                            {
                                spouseDOB[1] = "0" + spouseDOB[1];
                            }
                            fields.SetFieldProperty("untitled44", "textsize", 14f, null);
                            fields.SetFieldProperty("untitled45", "textsize", 14f, null);
                            fields.SetFieldProperty("untitled46", "textsize", 14f, null);
                            fields.SetField("untitled44", getmonth(spouseDOB[0]).Substring(0, 1) + "  " + getmonth(spouseDOB[0]).Substring(1, 1));
                            fields.SetField("untitled45", spouseDOB[1].Substring(0, 1) + "  " + spouseDOB[1].Substring(1, 1));
                            fields.SetField("untitled46", spouseDOB[2].Substring(0, 1) + "  " + spouseDOB[2].Substring(1, 1) + "  " + spouseDOB[2].Substring(2, 1) + "  " + spouseDOB[2].Substring(3, 1));

                        }
                        else if (GovForms.getDependent(uProfile.NTID).Rows[i][2].ToString() == "Child")
                        {

                            childName[j, 0] = GovForms.getDependent(uProfile.NTID).Rows[i][3].ToString();
                            childName[j, 1] = GovForms.getDependent(uProfile.NTID).Rows[i][4].ToString();
                            childName[j, 2] = GovForms.getDependent(uProfile.NTID).Rows[i][5].ToString();
                            childName[j, 3] = GovForms.getDependent(uProfile.NTID).Rows[i][6].ToString();
                            //DOB
                            string spouseDOBraw = GovForms.getDependent(uProfile.NTID).Rows[i][7].ToString();
                            spouseDOBraw = spouseDOBraw.Replace(",", "");
                            spouseDOBraw = spouseDOBraw.Replace(" ", "-");
                            string[] spouseDOB = spouseDOBraw.Split('-');
                            if (spouseDOB[1].Length == 1)
                            {
                                spouseDOB[1] = "0" + spouseDOB[1];
                            }
                            childName[j, 4] = getmonth(spouseDOB[0]);
                            childName[j, 5] = spouseDOB[1];
                            childName[j, 6] = spouseDOB[2];
                            j++;
                        }
                    }
                    catch (Exception ex)
                    {

                    }
                }
                if (childName != null)
                {

                    for (int i = 0; i < childName.Length - 1; i++)
                    {
                        if (i > 5)
                        {
                            break;
                        }
                        try
                        {
                            //1
                            fields.SetField("LastNameChild" + (i + 1), childName[i, 2]);
                            fields.SetField("FirstNameChild" + (i + 1), childName[i, 0]);
                            fields.SetField("MiddleNameChild" + (i + 1), childName[i, 1]);
                            fields.SetField("SuffixChild" + (i + 1), childName[i, 3]);
                            fields.SetFieldProperty("DOBMonthChild" + (i + 1), "textsize", 14f, null);
                            fields.SetFieldProperty("DOBDateChild" + (i + 1), "textsize", 14f, null);
                            fields.SetFieldProperty("DOBYearChild" + (i + 1), "textsize", 14f, null);
                            fields.SetField("DOBMonthChild" + (i + 1), childName[i, 4].Substring(0, 1) + "  " + childName[i, 4].Substring(1, 1));
                            fields.SetField("DOBDateChild" + (i + 1), childName[i, 5].Substring(0, 1) + "  " + childName[i, 5].Substring(1, 1));
                            fields.SetField("DOBYearChild" + (i + 1), childName[i, 6].Substring(0, 1) + "  " + childName[i, 6].Substring(1, 1) + "  " + childName[i, 6].Substring(2, 1) + "  " + childName[i, 6].Substring(3, 1));
                        }
                        catch
                        {

                        }
                    }

                }
                item = fields.GetFieldItem("untitled112");
                item.GetMerged(0).Put(PdfName.Q, new PdfNumber(PdfFormField.Q_CENTER));

                fields.SetField("untitled112", GovForms.GetCalamityLoan(uProfile.NTID).Rows[0][1].ToString().ToUpper() + " " + GovForms.GetCalamityLoan(uProfile.NTID).Rows[0][2].ToString().ToUpper() + " " + GovForms.GetCalamityLoan(uProfile.NTID).Rows[0][0].ToString().ToUpper());
                fields.SetField("untitled114", DateTime.Now.ToString("MM/dd/yy"));
                stamper.FormFlattening = true;
                stamper.Close();
                uProfile.Status = "Success";
            }
            catch (Exception ex)
            {
                uProfile.Status = ex.ToString();
            }

            return uProfile.Status;


        }
        #endregion
        #region "ML_1"
        [HttpPost]
        [Route("ML_1")]
        public string ML_1(UserProfile uProfile)
        {
            string path = HttpContext.Current.Server.MapPath("pdf");
            var pdfReader1 = new PdfReader(path + "/GovForms Templates/ML-1.pdf");
            AcroFields af = pdfReader1.AcroFields;
            string formFile = path + "/GovForms Templates/ML-1.pdf";
            string newFile = path + "/GovForms-Employee/ML-1-" + uProfile.NTID + ".pdf";

            PdfReader reader = new PdfReader(formFile);
            PdfStamper stamper = new PdfStamper(reader, new FileStream(newFile, FileMode.Create));
            AcroFields fields = stamper.AcroFields;
            try // try catch for uProfile.NTID if session expires: redirects to login page
            {
                try
                {
                    fields.SetField("untitled1", GovForms.getCompanyProfile(uProfile.NTID).Rows[0][39].ToString());
                }
                catch { }
                try
                {
                    fields.SetField("untitled2", GovForms.GetML1FORM(uProfile.NTID).Rows[0][2].ToString());
                }
                catch { }
                try
                {
                    fields.SetField("untitled3", GovForms.getCompanyProfile(uProfile.NTID).Rows[0][3].ToString());
                }
                catch { }
                try
                {
                    fields.SetField("untitled26", GovForms.getCompanyProfile(uProfile.NTID).Rows[0][6].ToString() + " " + GovForms.getCompanyProfile(uProfile.NTID).Rows[0][7].ToString() + " " + GovForms.getCompanyProfile(uProfile.NTID).Rows[0][8].ToString() + " " + GovForms.getCompanyProfile(uProfile.NTID).Rows[0][9].ToString() + " " + GovForms.getCompanyProfile(uProfile.NTID).Rows[0][10].ToString() + " " + GovForms.getCompanyProfile(uProfile.NTID).Rows[0][11].ToString() + " " + GovForms.getCompanyProfile(uProfile.NTID).Rows[0][14].ToString());
                }
                catch { }
                GovForms.GetRequestFormSettings();
                GovForms.Signatory(uProfile.NTID);
                GovForms.GetAppEMP(74, uProfile.NTID);
                string empidApprover = "";
                empidApprover = GovForms.ApproverEMPID;
                if (empidApprover == "" || empidApprover == null || string.IsNullOrWhiteSpace(empidApprover))
                {
                }
                else
                {
                    GovForms.GetApproverNameJobDes(empidApprover);
                    string Approvername = GovForms.ApproverName;
                    string ApproverDesc = GovForms.ApproverJobDesc;
                    GovForms.GetSignatureSignPicture(empidApprover, "sign_1");
                    string Bitsignature = null;
                    Bitsignature = GovForms.BitSign;
                    Byte[] SignatureBit = null;
                    iTextSharp.text.Image signatory = null;
                    var pdfContentByte = stamper.GetOverContent(1);
                    if (string.IsNullOrWhiteSpace(Bitsignature) == false)
                    {
                        Bitsignature = Bitsignature.Replace(' ', '+');
                        SignatureBit = Convert.FromBase64String(Bitsignature);
                        signatory = iTextSharp.text.Image.GetInstance(SignatureBit);
                        signatory.SetAbsolutePosition(430, 225);
                        signatory.ScaleToFit(100, 100);
                        fields.SetField("untitled24", Approvername);
                        fields.SetField("untitled25", ApproverDesc);
                        pdfContentByte.AddImage(signatory);
                    }
                }


                stamper.FormFlattening = true;
                stamper.Close();
                uProfile.Status = "Success";
            }
            catch (Exception ex)
            {
                uProfile.Status = ex.ToString();
            }
            return uProfile.Status;

        }
        #endregion
        #region "SSS_Salary_Loan"
        [HttpPost]
        [Route("SSS_Salary_Loan")]
        public string SSS_Salary_Loan(UserProfile uProfile)
        {
            string path = HttpContext.Current.Server.MapPath("pdf");
            var pdfReader1 = new PdfReader(path + "/GovForms Templates/SSS_Salary_Loan.pdf");

            AcroFields af = pdfReader1.AcroFields;

            string formFile = path + "/GovForms Templates//SSS_Salary_Loan.pdf";
            string newFile = path + "/GovForms-Employee/SSS_Salary_Loan-" + uProfile.NTID + ".pdf";

            PdfReader reader = new PdfReader(formFile);
            PdfStamper stamper = new PdfStamper(reader, new FileStream(newFile, FileMode.Create));
            var forms = stamper.AcroFields;
            try // try catch for uProfile.NTID if session expires: redirects to login page
            {
                GovForms.getEmployeePersonalInformation(uProfile.NTID);
                string empbranchid = GovForms.getCompanyProfile(uProfile.NTID).Rows[0][0].ToString();
                try
                {
                    forms.SetField("untitled13", GovForms.GetSSSSalaryLoan(uProfile.NTID).Rows[0][0].ToString().First().ToString().ToUpper() + GovForms.GetSSSSalaryLoan(uProfile.NTID).Rows[0][0].ToString().Substring(1));
                    forms.SetField("untitled14", GovForms.GetSSSSalaryLoan(uProfile.NTID).Rows[0][1].ToString().First().ToString().ToUpper() + GovForms.GetSSSSalaryLoan(uProfile.NTID).Rows[0][1].ToString().Substring(1));
                    forms.SetField("untitled15", GovForms.GetSSSSalaryLoan(uProfile.NTID).Rows[0][2].ToString().First().ToString().ToUpper() + GovForms.GetSSSSalaryLoan(uProfile.NTID).Rows[0][2].ToString().Substring(1));
                }
                catch
                {
                }
                try
                {
                    forms.SetField("untitled18", GovForms.GetSSSSalaryLoan(uProfile.NTID).Rows[0][3].ToString().First().ToString().ToUpper() + GovForms.GetSSSSalaryLoan(uProfile.NTID).Rows[0][3].ToString().Substring(1));
                    forms.SetField("untitled19", GovForms.GetSSSSalaryLoan(uProfile.NTID).Rows[0][4].ToString().First().ToString().ToUpper() + GovForms.GetRF1(uProfile.NTID).Rows[0][4].ToString().Substring(1));
                    forms.SetField("untitled21", GovForms.GetSSSSalaryLoan(uProfile.NTID).Rows[0][5].ToString().First().ToString().ToUpper() + GovForms.GetRF1(uProfile.NTID).Rows[0][5].ToString().Substring(1));
                    forms.SetField("untitled22", GovForms.getPagibigMPL(uProfile.NTID).Rows[0][9].ToString());
                    forms.SetField("untitled23", GovForms.provincea.ToString());
                }
                catch
                {
                }
                try
                {
                    forms.SetField("untitled26", GovForms.GetSSSSalaryLoan(uProfile.NTID).Rows[0][3].ToString().First().ToString().ToUpper() + GovForms.GetRF1(uProfile.NTID).Rows[0][3].ToString().Substring(1));
                    forms.SetField("untitled27", GovForms.GetSSSSalaryLoan(uProfile.NTID).Rows[0][4].ToString().First().ToString().ToUpper() + GovForms.GetRF1(uProfile.NTID).Rows[0][4].ToString().Substring(1));
                    forms.SetField("untitled29", GovForms.GetSSSSalaryLoan(uProfile.NTID).Rows[0][5].ToString().First().ToString().ToUpper() + GovForms.GetRF1(uProfile.NTID).Rows[0][5].ToString().Substring(1));
                    forms.SetField("untitled30", GovForms.getPagibigMPL(uProfile.NTID).Rows[0][9].ToString());
                    forms.SetField("untitled31", GovForms.provincea.ToString());
                    forms.SetField("untitled37", GovForms.GetEmailInfo(uProfile.NTID).Rows[0][0].ToString());
                }
                catch
                {
                }
                string emplyrid = GovForms.GetSSSSalaryLoan(uProfile.NTID).Rows[0][14].ToString().Replace("-", "");
                emplyrid.Replace("-", "");
                char[] emplyridnum = emplyrid.ToCharArray();
                string emplyrid1 = emplyridnum[0] + " " + emplyridnum[1];
                forms.SetFieldProperty("untitled1", "textsize", 12f, null);
                forms.SetField("untitled1", emplyrid1);
                string emplyrid2 = emplyridnum[2] + " " + emplyridnum[3] + " " + emplyridnum[4] + " " + emplyridnum[5] + "  " + emplyridnum[6] + " " + emplyridnum[7] + "  " + emplyridnum[8] + " " + emplyridnum[9];
                forms.SetFieldProperty("untitled2", "textsize", 12f, null);
                forms.SetField("untitled2", emplyrid2);
                forms.SetFieldProperty("untitled3", "textsize", 12f, null);
                forms.SetField("untitled3", emplyridnum[9].ToString());
                if (GovForms.GetPersonalInfo(uProfile.NTID).Rows.Count != 0)
                {
                    try
                    {
                        for (int i = 0; i < GovForms.GetPersonalInfo(uProfile.NTID).Rows.Count; i++)
                        {
                            string numtype = GovForms.GetPersonalInfo(uProfile.NTID).Rows[i][1].ToString();
                            if (numtype == "Mobile")
                            {
                                string MN = GovForms.GetPersonalInfo(uProfile.NTID).Rows[i][0].ToString().Replace("-", "");
                                string mno = MN.Substring(0, 1);
                                string mno1 = MN.Substring(1, 1);
                                string mno2 = MN.Substring(2, 1);
                                string mno3 = MN.Substring(3, 1);
                                string MNumber = mno + "   " + mno1 + "   " + mno2 + "   " + mno3;
                                forms.SetField("untitled35", MNumber);
                                string mno4 = MN.Substring(4, 1);
                                string mno5 = MN.Substring(5, 1);
                                string mno6 = MN.Substring(6, 1);
                                string mno7 = MN.Substring(7, 1);
                                string mno8 = MN.Substring(8, 1);
                                string mno9 = MN.Substring(9, 1);
                                string mno10 = MN.Substring(10, 1);
                                string mNumber1 = mno4 + "   " + mno5 + "   " + mno6 + "  " + mno7 + "  " + mno8 + "   " + mno9 + "   " + mno10;
                                forms.SetField("untitled36", mNumber1);
                            }
                            if (numtype == "Work")
                            {
                            }
                            if (numtype == "Home")
                            {
                                string PN = GovForms.GetPersonalInfo(uProfile.NTID).Rows[i][0].ToString().Replace("-", "");
                                string pno = PN.Substring(0, 1);
                                string pno1 = PN.Substring(1, 1);
                                string pno2 = PN.Substring(2, 1);
                                string PNumber = pno + "   " + pno1 + "   " + pno2;
                                forms.SetField("untitled33", PNumber);
                                string PN1 = GovForms.GetPersonalInfo(uProfile.NTID).Rows[i][0].ToString().Replace("-", "");
                                string pno3 = PN1.Substring(3, 1);
                                string pno4 = PN1.Substring(4, 1);
                                string pno5 = PN1.Substring(5, 1);
                                string pno6 = PN1.Substring(6, 1);
                                string PNumber1 = pno3 + "  " + pno4 + "   " + pno5 + "   " + pno6;
                                forms.SetField("untitled34", PNumber1);
                            }
                        }
                    }
                    catch
                    {
                    }
                }
                try
                {
                    string ZipC = GovForms.GetSSSSalaryLoan(uProfile.NTID).Rows[0][11].ToString().Replace("-", "");
                    string zc = ZipC.Substring(0, 1);
                    string zc1 = ZipC.Substring(1, 1);
                    string zc2 = ZipC.Substring(2, 1);
                    string zc3 = ZipC.Substring(3, 1);
                    string ZipCode = zc + "   " + zc1 + "   " + zc2 + "   " + zc3;
                    forms.SetField("untitled24", ZipCode);
                    string ZipC1 = GovForms.GetSSSSalaryLoan(uProfile.NTID).Rows[0][11].ToString().Replace("-", "");
                    string zc4 = ZipC1.Substring(0, 1);
                    string zc5 = ZipC1.Substring(1, 1);
                    string zc6 = ZipC1.Substring(2, 1);
                    string zc7 = ZipC1.Substring(3, 1);
                    string ZipCode1 = zc4 + "   " + zc5 + "   " + zc6 + "   " + zc7;
                    forms.SetField("untitled32", ZipCode1);
                }
                catch
                {
                }
                try
                {
                    var s = GovForms.GetSSSSalaryLoan(uProfile.NTID).Rows[0][12].ToString();
                    var firstWord = s.IndexOf(" ") > 0
                                      ? s.Substring(0, s.IndexOf(" "))
                                      : s;
                    String month;
                    if (firstWord == "Jan.")
                    {
                        month = "0 1";
                    }
                    else if (firstWord == "February")
                    {
                        month = "0 2";
                    }
                    else if (firstWord == "March")
                    {
                        month = "0 3";
                    }
                    else if (firstWord == "April")
                    {
                        month = "0 4";
                    }
                    else if (firstWord == "May")
                    {
                        month = "0 5";
                    }
                    else if (firstWord == "June")
                    {
                        month = "0 6";
                    }
                    else if (firstWord == "July")
                    {
                        month = "0 7";
                    }
                    else if (firstWord == "August")
                    {
                        month = "0 8";
                    }
                    else if (firstWord == "September")
                    {
                        month = "0 9";
                    }
                    else if (firstWord == "October")
                    {
                        month = "1 0";
                    }
                    else if (firstWord == "November")
                    {
                        month = "1 1";
                    }
                    else if (firstWord == "December")
                    {
                        month = "1 2";
                    }
                    else
                    {
                        month = " ";
                    }
                    forms.SetField("untitled7", " " + month);
                    string day = s.Split(' ')[1];
                    int day1 = Convert.ToInt32(day.Remove(day.Length - 1));
                    string dayy = day.Substring(0, 1) + "   " + day.Substring(1, 1);
                    forms.SetField("untitled8", dayy);
                    string year = s.Split(' ')[2];
                    string y1 = year.Substring(0, 1) + "  " + year.Substring(1, 1) + "   " + year.Substring(2, 1) + "   " + year.Substring(3, 1);
                    forms.SetField("untitled9", y1);
                }
                catch
                {
                }
                try
                {
                    string TIN = GovForms.GetSSSSalaryLoan(uProfile.NTID).Rows[0][13].ToString().Replace("-", "");
                    string tn1 = TIN.Substring(0, 1);
                    string tn2 = TIN.Substring(1, 1);
                    string tn3 = TIN.Substring(2, 1);
                    string TinNumber = tn1 + "   " + tn2 + "   " + tn3;
                    forms.SetField("untitled10", TinNumber);
                    string tn4 = TIN.Substring(3, 1);
                    string tn5 = TIN.Substring(4, 1);
                    string tn6 = TIN.Substring(5, 1);
                    string TinNumber1 = tn4 + "   " + tn5 + "   " + tn6;
                    forms.SetField("untitled11", TinNumber1);
                    string tn7 = TIN.Substring(6, 1);
                    string tn8 = TIN.Substring(7, 1);
                    string tn9 = TIN.Substring(8, 1);
                    string TinNumber2 = tn7 + "   " + tn8 + "   " + tn9;
                    forms.SetField("untitled12", TinNumber2);
                }
                catch
                {
                }
                try
                {
                    string CRN = GovForms.GetSSSSalaryLoan(uProfile.NTID).Rows[0][15].ToString();
                    CRN = CRN.Remove(CRN.Length - 1);
                    string[] CRN2 = CRN.Split('-');
                    forms.SetFieldProperty("untitled4", "textsize", 12f, null);
                    forms.SetFieldProperty("untitled5", "textsize", 12f, null);
                    forms.SetFieldProperty("untitled6", "textsize", 12f, null);
                    forms.SetField("untitled4", CRN2[0].Substring(0, 1) + " " + CRN2[0].Substring(1, 1) + " " + CRN2[0].Substring(2, 1) + " " + CRN2[0].Substring(3, 1));
                    forms.SetField("untitled5", CRN2[1].Substring(0, 1) + " " + CRN2[1].Substring(1, 1) + " " + CRN2[1].Substring(2, 1) + " " + CRN2[1].Substring(3, 1) + "  " + CRN2[1].Substring(4, 1) + " " + CRN2[1].Substring(5, 1) + " " + CRN2[1].Substring(6, 1));
                    forms.SetField("untitled6", CRN2[2]);
                }
                catch { }
                try
                {
                    string loanamt = "";
                    forms.SetField("untitled44", loanamt);
                }
                catch
                {
                }
                try
                {
                    forms.SetField("untitled63", GovForms.getPagibigMPL(uProfile.NTID).Rows[0][38].ToString());
                }
                catch
                {
                }
                try
                {
                    forms.SetFieldProperty("untitled48", "textsize", 7f, null);
                    string emplradd = GovForms.getPagibigMPL(uProfile.NTID).Rows[0][32].ToString().ToUpper() + " " + GovForms.getPagibigMPL(uProfile.NTID).Rows[0][27].ToString().ToUpper() + " " + GovForms.getPagibigMPL(uProfile.NTID).Rows[0][26].ToString().ToUpper() + " " + GovForms.getPagibigMPL(uProfile.NTID).Rows[0][30].ToString().ToUpper() + " " + GovForms.getPagibigMPL(uProfile.NTID).Rows[0][30].ToString().ToUpper() + " " + GovForms.getPagibigMPL(uProfile.NTID).Rows[0][28].ToString().ToUpper();
                    forms.SetField("untitled65", GovForms.getPagibigMPL(uProfile.NTID).Rows[0][32].ToString().ToUpper());
                    forms.SetField("untitled67", GovForms.getCompanyProfile(uProfile.NTID).Rows[0][8].ToString().ToUpper());
                    forms.SetField("untitled69", GovForms.getPagibigMPL(uProfile.NTID).Rows[0][26].ToString().ToUpper());
                    forms.SetField("untitled70", GovForms.getPagibigMPL(uProfile.NTID).Rows[0][30].ToString().ToUpper());
                    forms.SetField("untitled71", GovForms.getPagibigMPL(uProfile.NTID).Rows[0][28].ToString().ToUpper());
                }
                catch
                {
                }
                try
                {
                    string ZipC = GovForms.getPagibigMPL(uProfile.NTID).Rows[0][33].ToString().Replace("-", "");
                    string zc = ZipC.Substring(0, 1);
                    string zc1 = ZipC.Substring(1, 1);
                    string zc2 = ZipC.Substring(2, 1);
                    string zc3 = ZipC.Substring(3, 1);
                    string ZipCode = zc + "   " + zc1 + "   " + zc2 + "   " + zc3;
                    forms.SetField("untitled72", ZipCode);
                }
                catch
                {
                }
                try
                {
                    forms.SetField("untitled53", "");
                    emplyrid = GovForms.getPagibigMPL(uProfile.NTID).Rows[0][39].ToString();
                    emplyrid.Replace("-", "");
                    emplyridnum = emplyrid.ToCharArray();
                    emplyrid1 = emplyridnum[0] + " " + emplyridnum[1];
                    forms.SetFieldProperty("untitled54", "textsize", 12f, null);
                    forms.SetField("untitled54", emplyrid1);
                    emplyrid2 = emplyridnum[2] + " " + emplyridnum[3] + " " + emplyridnum[4] + " " + emplyridnum[5] + "  " + emplyridnum[6] + " " + emplyridnum[7] + "  " + emplyridnum[8] + " " + emplyridnum[9];
                    forms.SetFieldProperty("untitled55", "textsize", 12f, null);
                    forms.SetField("untitled55", emplyrid2);
                }
                catch
                {
                }
                try
                {
                    string fullTIN = GovForms.getCompanyProfile(uProfile.NTID).Rows[0][38].ToString().Replace("-", "");
                    string TIN1 = fullTIN.Substring(0, 1); //First number
                    string TIN2 = fullTIN.Substring(1, 1); //2nd number
                    string TIN3 = fullTIN.Substring(2, 1); //3rd number
                    string TIN1st3 = TIN1 + " " + TIN2 + " " + TIN3;
                    forms.SetFieldProperty("untitled57", "textsize", 12f, null);
                    string TIN4 = fullTIN.Substring(3, 1);
                    string TIN5 = fullTIN.Substring(4, 1);
                    string TIN6 = fullTIN.Substring(5, 1);
                    string TIN2nd3 = TIN4 + " " + TIN5 + " " + TIN6;
                    forms.SetFieldProperty("untitled58", "textsize", 12f, null);
                    string TIN7 = fullTIN.Substring(6, 1);
                    string TIN8 = fullTIN.Substring(7, 1);
                    string TIN9 = fullTIN.Substring(8, 1);
                    string TIN3rd3 = TIN7 + " " + TIN8 + " " + TIN9;
                    forms.SetFieldProperty("untitled59", "textsize", 12f, null);
                    if (fullTIN.Length == 12)
                    {
                        string TIN10 = fullTIN.Substring(9, 1);
                        string TIN11 = fullTIN.Substring(10, 1);
                        string TIN12 = fullTIN.Substring(11, 1);
                        string TIN4th3 = TIN10 + " " + TIN11 + " " + TIN12;
                        forms.SetFieldProperty("untitled60", "textsize", 12f, null);
                        forms.SetField("untitled60", TIN4th3);
                    }
                    forms.SetField("untitled57", TIN1st3);
                    forms.SetField("untitled58", TIN2nd3);
                    forms.SetField("untitled59", TIN3rd3);
                }
                catch
                {
                }
                try
                {
                    forms.SetField("untitled75", GovForms.GetCompanyEmail(empbranchid).Rows[0][0].ToString());
                    string PN = GovForms.GetCompanyContactPhone(empbranchid).Rows[0][1].ToString().Replace("-", "");
                    string pno = PN.Substring(0, 1);
                    string pno1 = PN.Substring(1, 1);
                    string pno2 = PN.Substring(2, 1);
                    string PNumber = pno + "   " + pno1 + "   " + pno2;
                    forms.SetField("untitled73", PNumber);
                    string PN1 = GovForms.GetCompanyContactPhone(empbranchid).Rows[0][1].ToString().Replace("-", "");
                    string pno3 = PN1.Substring(3, 1);
                    string pno4 = PN1.Substring(4, 1);
                    string pno5 = PN1.Substring(5, 1);
                    string pno6 = PN1.Substring(6, 1);
                    string PNumber1 = pno3 + "  " + pno4 + "   " + pno5 + "   " + pno6 + "  " + PN1.Substring(7, 1) + "  " + PN.Substring(8, 1) + "  " + PN.Substring(9, 1);
                    forms.SetField("untitled74", PNumber1);
                }
                catch
                {
                }
                try
                {
                    forms.SetField("untitled77", GovForms.GetSSSSalaryLoan(uProfile.NTID).Rows[0][1].ToString().ToUpper() + " " + GovForms.GetSSSSalaryLoan(uProfile.NTID).Rows[0][2].ToString().ToUpper() + " " + GovForms.GetSSSSalaryLoan(uProfile.NTID).Rows[0][0].ToString().ToUpper());
                    forms.SetField("untitled83", GovForms.GetSSSSalaryLoan(uProfile.NTID).Rows[0][1].ToString().ToUpper() + " " + GovForms.GetSSSSalaryLoan(uProfile.NTID).Rows[0][2].ToString().ToUpper() + " " + GovForms.GetSSSSalaryLoan(uProfile.NTID).Rows[0][0].ToString().ToUpper());
                    forms.SetField("untitled51", GovForms.GetSSSSalaryLoan(uProfile.NTID).Rows[0][1].ToString().ToUpper() + " " + GovForms.GetSSSSalaryLoan(uProfile.NTID).Rows[0][2].ToString().ToUpper() + " " + GovForms.GetSSSSalaryLoan(uProfile.NTID).Rows[0][0].ToString().ToUpper());
                    forms.SetField("untitled107", GovForms.GetSSSSalaryLoan(uProfile.NTID).Rows[0][1].ToString().ToUpper() + " " + GovForms.GetSSSSalaryLoan(uProfile.NTID).Rows[0][2].ToString().ToUpper() + " " + GovForms.GetSSSSalaryLoan(uProfile.NTID).Rows[0][0].ToString().ToUpper());
                    forms.SetField("untitled53", DateTime.Now.ToString("MM/dd/yyyy"));
                    forms.SetField("untitled81", DateTime.Now.ToString("MM/dd/yyyy"));
                    forms.SetFieldProperty("untitled85", "textsize", 6f, null);
                    forms.SetField("untitled85", DateTime.Now.ToString("MM/dd/yyyy") + " " + DateTime.Now.ToString("hh:mm tt"));
                    forms.SetFieldProperty("untitled88", "textsize", 7f, null);
                    forms.SetField("untitled88", DateTime.Now.ToString("MM/dd/yyyy") + " " + DateTime.Now.ToString("hh:mm tt"));
                    forms.SetFieldProperty("untitled110", "textsize", 7f, null);
                    forms.SetField("untitled110", DateTime.Now.ToString("MM/dd/yyyy") + " " + DateTime.Now.ToString("hh:mm tt"));
                }
                catch
                {
                }
                try
                {
                    GovForms.GetRequestFormSettings();
                    GovForms.Signatory(uProfile.NTID);
                    GovForms.GetAppEMP(75, uProfile.NTID);
                    string empidApprover = "";
                    empidApprover = GovForms.ApproverEMPID;
                    if (empidApprover == "" || empidApprover == null || string.IsNullOrWhiteSpace(empidApprover))
                    {
                    }
                    else
                    {
                        GovForms.GetApproverNameJobDes(empidApprover);
                        string Approvername = GovForms.ApproverName;
                        string ApproverDesc = GovForms.ApproverJobDesc;
                        GovForms.GetSignatureSignPicture(empidApprover, "sign_1");
                        string Bitsignature = null;
                        Bitsignature = GovForms.BitSign;
                        Byte[] SignatureBit = null;
                        iTextSharp.text.Image signatory = null;
                        var pdfContentByte = stamper.GetOverContent(1);
                        if (string.IsNullOrWhiteSpace(Bitsignature) == false)
                        {
                            Bitsignature = Bitsignature.Replace(' ', '+');
                            SignatureBit = Convert.FromBase64String(Bitsignature);
                            signatory = iTextSharp.text.Image.GetInstance(SignatureBit);
                            signatory.SetAbsolutePosition(260, 250);
                            signatory.ScaleToFit(100, 100);
                            forms.SetField("untitled77", Approvername);
                            forms.SetField("untitled80", ApproverDesc);
                            pdfContentByte.AddImage(signatory);
                        }
                    }

                }
                catch { }


                stamper.FormFlattening = true;
                stamper.Close();
                uProfile.Status = "Success";
            }
            catch (Exception ex)
            {
                uProfile.Status = ex.ToString();
            }


            return uProfile.Status;

        }
        #endregion
        #region "SSSForms_Change_Request_E4"
        [HttpPost]
        [Route("SSSForms_Change_Request_E4")]
        public string SSSForms_Change_Request_E4(UserProfile uProfile)
        {
            string path = HttpContext.Current.Server.MapPath("pdf");
            var pdfReader1 = new PdfReader(path + "/GovForms Templates/SSSForms_Change_Request_E4.pdf");
            AcroFields af = pdfReader1.AcroFields;

            string formFile = path + "/GovForms Templates//SSSForms_Change_Request_E4.pdf";
            string newFile = path + "/GovForms-Employee/SSSForms_Change_Request_E4-" + uProfile.NTID + ".pdf";

            PdfReader reader = new PdfReader(formFile);
            PdfStamper stamper = new PdfStamper(reader, new FileStream(newFile, FileMode.Create));
            var forms = stamper.AcroFields;
            try // try catch for uProfile.NTID if session expires: redirects to login page
            {
                //SSS
                try
                {
                    string fullSSN = GovForms.getPagibigMPL(uProfile.NTID).Rows[0][1].ToString().Replace("-", "");
                    char[] SSNbd = fullSSN.ToCharArray();
                    forms.SetFieldProperty("untitled1", "textsize", 14f, null);
                    forms.SetField("untitled1", SSNbd[0] + " " + SSNbd[1]);
                    forms.SetFieldProperty("untitled2", "textsize", 15f, null);
                    forms.SetField("untitled2", SSNbd[2] + " " + SSNbd[3] + " " + SSNbd[4] + "  " + SSNbd[5] + "  " + SSNbd[6] + " " + SSNbd[7] + " " + SSNbd[8]);
                    forms.SetFieldProperty("untitled3", "textsize", 14f, null);
                    forms.SetField("untitled3", SSNbd[9].ToString());
                    forms.SetFieldProperty("untitled129", "textsize", 14f, null);
                    forms.SetField("untitled129", SSNbd[0] + " " + SSNbd[1]);
                    forms.SetFieldProperty("untitled130", "textsize", 15f, null);
                    forms.SetField("untitled130", SSNbd[2] + " " + SSNbd[3] + " " + SSNbd[4] + "  " + SSNbd[5] + "  " + SSNbd[6] + " " + SSNbd[7] + " " + SSNbd[8]);
                    forms.SetFieldProperty("untitled131", "textsize", 14f, null);
                    forms.SetField("untitled131", SSNbd[9].ToString());
                    forms.SetFieldProperty("untitled132", "textsize", 6.5f, null);
                }
                catch { }
                try
                {
                    string CRN = GovForms.GetSSSSalaryLoan(uProfile.NTID).Rows[0][15].ToString();
                    CRN = CRN.Remove(CRN.Length - 1);
                    string[] CRN2 = CRN.Split('-');
                    forms.SetFieldProperty("untitled4", "textsize", 12f, null);
                    forms.SetFieldProperty("untitled5", "textsize", 12f, null);
                    forms.SetFieldProperty("untitled6", "textsize", 12f, null);
                    forms.SetField("untitled4", CRN2[0].Substring(0, 1) + " " + CRN2[0].Substring(1, 1) + " " + CRN2[0].Substring(2, 1) + " " + CRN2[0].Substring(3, 1));
                    forms.SetField("untitled5", CRN2[1].Substring(0, 1) + " " + CRN2[1].Substring(1, 1) + " " + CRN2[1].Substring(2, 1) + " " + CRN2[1].Substring(3, 1) + "  " + CRN2[1].Substring(4, 1) + " " + CRN2[1].Substring(5, 1) + " " + CRN2[1].Substring(6, 1));
                    forms.SetField("untitled6", CRN2[2]);
                }
                catch { }
                //DOB
                try
                {
                    string bdayraw = GovForms.getPagibigMPL(uProfile.NTID).Rows[0][11].ToString();
                    string[] bdayarr = bdayraw.Split('-');
                    string newmonth = bdayarr[1];
                    forms.SetFieldProperty("untitled7", "textsize", 15f, null);
                    forms.SetField("untitled7", newmonth.Substring(0, 1) + " " + newmonth.Substring(1, 1));
                    forms.SetFieldProperty("untitled8", "textsize", 15f, null);
                    forms.SetField("untitled8", bdayarr[2].Substring(0, 1) + " " + bdayarr[2].Substring(1, 1));
                    forms.SetFieldProperty("untitled9", "textsize", 15f, null);
                    forms.SetField("untitled9", bdayarr[0].Substring(0, 1) + " " + bdayarr[0].Substring(1, 1) + " " + bdayarr[0].Substring(2, 1) + " " + bdayarr[0].Substring(3, 1));
                }
                catch { }

                try
                {
                    string fullTIN = GovForms.getPagibigMPL(uProfile.NTID).Rows[0][2].ToString().Replace("-", "");
                    string TIN1 = fullTIN.Substring(0, 1); //First number
                    string TIN2 = fullTIN.Substring(1, 1); //2nd number
                    string TIN3 = fullTIN.Substring(2, 1); //3rd number
                    string TIN1st3 = TIN1 + "  " + TIN2 + " " + TIN3;
                    forms.SetFieldProperty("untitled10", "textsize", 15f, null);
                    string TIN4 = fullTIN.Substring(3, 1);
                    string TIN5 = fullTIN.Substring(4, 1);
                    string TIN6 = fullTIN.Substring(5, 1);
                    string TIN2nd3 = TIN4 + " " + TIN5 + " " + TIN6;
                    forms.SetFieldProperty("untitled11", "textsize", 15f, null);
                    string TIN7 = fullTIN.Substring(6, 1);
                    string TIN8 = fullTIN.Substring(7, 1);
                    string TIN9 = fullTIN.Substring(8, 1);
                    string TIN3rd3 = TIN7 + " " + TIN8 + " " + TIN9;
                    forms.SetFieldProperty("untitled12", "textsize", 15f, null);
                    forms.SetField("untitled10", TIN1st3);
                    forms.SetField("untitled11", TIN2nd3);
                    forms.SetField("untitled12", TIN3rd3);
                }
                catch { }
                try
                {
                    forms.SetField("untitled13", GovForms.getPagibigMPL(uProfile.NTID).Rows[0][5].ToString());
                    forms.SetField("untitled14", GovForms.getPagibigMPL(uProfile.NTID).Rows[0][4].ToString());
                    forms.SetField("untitled15", GovForms.getPagibigMPL(uProfile.NTID).Rows[0][6].ToString());
                    GovForms.getEmployeePersonalInformation(uProfile.NTID);
                    forms.SetField("untitled18", GovForms.housenuma);//house no.
                    forms.SetField("untitled19", GovForms.streetnamea);//street
                    forms.SetField("untitled21", GovForms.barangaya);//barangay
                    forms.SetField("untitled22", GovForms.citya);//city/municipality
                    forms.SetField("untitled23", GovForms.provincea); //province
                    forms.SetFieldProperty("untitled24", "textsize", 15f, null);
                    string fullzip = GovForms.zipcodea;
                    forms.SetField("untitled24", fullzip.Substring(0, 1) + " " + fullzip.Substring(1, 1) + "  " + fullzip.Substring(2, 1) + " " + fullzip.Substring(3, 1));
                }
                catch { }
                //contact numbers
                try
                {
                    for (int i = 0; i < GovForms.GetPersonalInfo(uProfile.NTID).Rows.Count; i++)
                    {
                        string numtype = GovForms.GetPersonalInfo(uProfile.NTID).Rows[i][1].ToString();
                        if (numtype == "Home")
                        {
                            string num = GovForms.GetPersonalInfo(uProfile.NTID).Rows[i][0].ToString().Replace("-", "");
                            num = getNumeric(num);
                            if (num.Length == 10)
                            {
                                char[] numarr = num.ToCharArray();
                                forms.SetFieldProperty("untitled25", "textsize", 14f, null);
                                forms.SetFieldProperty("untitled26", "textsize", 14f, null);
                                forms.SetField("untitled25", numarr[0] + " " + numarr[1] + "  " + numarr[2]);
                                forms.SetField("untitled26", numarr[3] + " " + numarr[4] + "  " + numarr[5] + "  " + numarr[6] + " " + numarr[7] + "  " + numarr[8] + "  " + numarr[9]);
                            }
                            else if (num.Length == 7)
                            {
                                char[] numarr = num.ToCharArray();

                                forms.SetFieldProperty("untitled26", "textsize", 14f, null);
                                forms.SetField("untitled26", numarr[0] + " " + numarr[1] + "  " + numarr[2] + "  " + numarr[3] + " " + numarr[4] + "  " + numarr[5] + "  " + numarr[6]);

                            }
                        }
                        else if (numtype == "Mobile")
                        {
                            //Mobile Number Breakdown
                            string mobnum = GovForms.GetPersonalInfo(uProfile.NTID).Rows[i][0].ToString().Replace("-", "").Replace("+63", "0").Replace("+", "");
                            bool mobnume = Regex.IsMatch(mobnum, @"^\d+$");// determine if string is pure numbers; return true & null value

                            if (mobnume.ToString() == "True")
                            {
                                char[] mobnumarr = mobnum.ToCharArray();
                                forms.SetFieldProperty("untitled27", "textsize", 14f, null);
                                forms.SetFieldProperty("untitled28", "textsize", 14f, null);
                                forms.SetField("untitled27", mobnumarr[0] + "  " + mobnumarr[1] + "  " + mobnumarr[2] + "  " + mobnumarr[3]);
                                forms.SetField("untitled28", mobnumarr[4] + " " + mobnumarr[5] + "  " + mobnumarr[6] + "  " + mobnumarr[7] + "  " + mobnumarr[8] + "  " + mobnumarr[9] + " " + mobnumarr[10]);

                            }
                        }// Mobile Number Breakdown end
                    }
                }
                catch { }
                //Email address
                try
                {
                    forms.SetField("untitled29", GovForms.GetEmailInfo(uProfile.NTID).Rows[0][0].ToString());
                }
                catch
                {

                }
                forms.SetField("untitled132", (GovForms.getPagibigMPL(uProfile.NTID).Rows[0][4].ToString() + " " + GovForms.getPagibigMPL(uProfile.NTID).Rows[0][6].ToString() + " " + GovForms.getPagibigMPL(uProfile.NTID).Rows[0][5].ToString()).ToUpper());//NAME
                forms.SetField("untitled134", DateTime.Now.ToString("MM-dd-yyyy"));
                GovForms.GetRequestFormSettings();
                GovForms.Signatory(uProfile.NTID);
                if (GovForms.active1 == "Active")
                {
                    forms.SetField("untitled315", GovForms.signatory1);
                    forms.SetField("untitled316", GovForms.position1);
                }
                else if (GovForms.active2 == "Active" && GovForms.active1 != "Active")
                {
                    forms.SetField("untitled315", GovForms.signatory2);
                    forms.SetField("untitled316", GovForms.position2);
                }
                stamper.FormFlattening = true;
                stamper.Close();
                uProfile.Status = "Success";
            }
            catch (Exception ex)
            {
                uProfile.Status = ex.ToString();
            }
            return uProfile.Status;

        }
        #endregion
        #region "SSSForms_ER_Contribution_Payment"
        [HttpPost]
        [Route("SSSForms_ER_Contribution_Payment")]
        public string SSSForms_ER_Contribution_Payment(UserProfile uProfile)
        {
            string path = HttpContext.Current.Server.MapPath("pdf");
            var pdfReader1 = new PdfReader(path + "/GovForms Templates/SSSForms_ER_Contribution_Payment.pdf");
            AcroFields af = pdfReader1.AcroFields;
            string formFile = path + "/GovForms Templates/SSSForms_ER_Contribution_Payment.pdf";
            string newFile = path + "/GovForms-Employee/SSSForms_ER_Contribution_Payment-" + uProfile.NTID + ".pdf";

            PdfReader reader = new PdfReader(formFile);
            PdfStamper stamper = new PdfStamper(reader, new FileStream(newFile, FileMode.Create));
            AcroFields fields = stamper.AcroFields;
            try // try catch for uProfile.NTID if session expires: redirects to login page
            {
                //get branchID
                string branchid = GovForms.getCompanyProfile(uProfile.NTID).Rows[0][0].ToString();
                //SSS
                try
                {
                    string SSNco = GovForms.getCompanyProfile(uProfile.NTID).Rows[0][39].ToString().Replace("-", "");
                    char[] SSNcobd = SSNco.ToCharArray();
                    string SSN2nd = "";
                    for (int i = 2; i < 10; i++)
                    {
                        SSN2nd = SSN2nd + SSNcobd[i] + " ";
                    }
                    fields.SetFieldProperty("EmpNum", "textsize", 15f, null);
                    fields.SetField("EmpNum", SSNcobd[0] + " " + SSNcobd[1] + "   " + SSN2nd + " " + SSNcobd[10] + " " + SSNcobd[11] + "  " + SSNcobd[12]);
                }
                catch { }
                //Basic Info
                try
                {
                    fields.SetField("EmpName", GovForms.getCompanyProfile(uProfile.NTID).Rows[0][3].ToString());//"EmpName");
                    fields.SetField("Room", GovForms.getCompanyProfile(uProfile.NTID).Rows[0][6].ToString() + " " + GovForms.getCompanyProfile(uProfile.NTID).Rows[0][7].ToString());
                    fields.SetField("House", GovForms.getCompanyProfile(uProfile.NTID).Rows[0][8].ToString());
                    fields.SetField("Street", GovForms.getCompanyProfile(uProfile.NTID).Rows[0][10].ToString());
                    fields.SetField("Bar", GovForms.getCompanyProfile(uProfile.NTID).Rows[0][9].ToString());
                    fields.SetField("Subdivision", "");
                    fields.SetField("City", GovForms.getCompanyProfile(uProfile.NTID).Rows[0][10].ToString());
                    fields.SetField("Province", GovForms.getCompanyProfile(uProfile.NTID).Rows[0][11].ToString());
                    string FullZip = GovForms.getCompanyProfile(uProfile.NTID).Rows[0][14].ToString();
                    string Zip1 = FullZip.Substring(0, 1); //First number
                    string Zip2 = FullZip.Substring(1, 1); //2nd number
                    string Zip3 = FullZip.Substring(2, 1); //3rd number
                    string Zip4 = FullZip.Substring(3, 1);
                    string CombineZip = Zip1 + " " + " " + Zip2 + " " + " " + Zip3 + " " + " " + Zip4;
                    fields.SetFieldProperty("ZipCode", "textsize", 15f, null);
                    fields.SetField("ZipCode", CombineZip);
                }
                catch { }
                //TIN
                try
                {
                    string fullTIN = GovForms.getCompanyProfile(uProfile.NTID).Rows[0][38].ToString().Replace("-", "");
                    string TIN1 = fullTIN.Substring(0, 1); //First number
                    string TIN2 = fullTIN.Substring(1, 1); //2nd number
                    string TIN3 = fullTIN.Substring(2, 1); //3rd number
                    string TIN4 = fullTIN.Substring(3, 1);
                    string TIN5 = fullTIN.Substring(4, 1);
                    string TIN6 = fullTIN.Substring(5, 1);
                    string TIN7 = fullTIN.Substring(6, 1);
                    string TIN8 = fullTIN.Substring(7, 1);
                    string TIN9 = fullTIN.Substring(8, 1);
                    string TIN10 = fullTIN.Substring(9, 1);
                    string TIN11 = fullTIN.Substring(10, 1);
                    string TIN12 = fullTIN.Substring(11, 1);
                    string CombineTin = TIN1 + " " + " " + TIN2 + " " + " " + TIN3 + " " + TIN4 + " " + " " + TIN5 + " " + TIN6 + " " + TIN7 + " " + " " + TIN8 + " " + " " + TIN9 + "  " + TIN10 + "  " + TIN11 + "  " + TIN12;
                    fields.SetFieldProperty("Tin", "textsize", 15f, null);
                    fields.SetField("Tin", CombineTin);
                }
                catch { }
                //contact numbers
                try
                {
                    for (int i = 0; i < GovForms.GetCompanyContactPhone(branchid).Rows.Count; i++)
                    {
                        string numtype = GovForms.GetCompanyContactPhone(branchid).Rows[i][0].ToString();
                        if (numtype == "Work")
                        {
                            string num = GovForms.GetCompanyContactPhone(branchid).Rows[i][1].ToString().Replace("-", "");
                            num = getNumeric(num);
                            if (num.Length == 10)
                            {
                                char[] numarr = num.ToCharArray();
                                fields.SetFieldProperty("TelNum", "textsize", 14f, null);
                                fields.SetField("TelNum", numarr[0] + " " + numarr[1] + "  " + numarr[2] + "  " + numarr[3] + " " + numarr[4] + "  " + numarr[5] + "  " + numarr[6] + " " + numarr[7] + "  " + numarr[8] + "  " + numarr[9]);
                            }
                            else if (num.Length == 7)
                            {
                                char[] numarr = num.ToCharArray();

                                fields.SetFieldProperty("untitled26", "textsize", 14f, null);
                                fields.SetField("untitled26", numarr[0] + " " + numarr[1] + "  " + numarr[2] + "  " + numarr[3] + " " + numarr[4] + "  " + numarr[5] + "  " + numarr[6]);

                            }
                        }
                        else if (numtype == "Mobile")
                        {
                            //Mobile Number Breakdown
                            string mobnum = GovForms.GetCompanyContactPhone(branchid).Rows[i][1].ToString().Replace("-", "").Replace("+63", "0").Replace("+", "");
                            bool mobnume = Regex.IsMatch(mobnum, @"^\d+$");// determine if string is pure numbers; return true & null value

                            if (mobnume.ToString() == "True")
                            {
                                char[] mobnumarr = mobnum.ToCharArray();
                                fields.SetFieldProperty("CellNum", "textsize", 14f, null);
                                fields.SetField("CellNum", mobnumarr[0] + "  " + mobnumarr[1] + "  " + mobnumarr[2] + "  " + mobnumarr[3] + "  " + mobnumarr[4] + " " + mobnumarr[5] + "  " + mobnumarr[6] + "  " + mobnumarr[7] + "  " + mobnumarr[8] + "  " + mobnumarr[9] + " " + mobnumarr[10]);

                            }
                        }// Mobile Number Breakdown end
                    }
                }
                catch { }
                //Email address
                try
                {
                    fields.SetField("Email", GovForms.GetCompanyEmail(branchid).Rows[0][0].ToString());
                }
                catch
                {

                }
                //CheckBox
                fields.SetField("BusChk", "Yes", true);
                GovForms.GetRequestFormSettings();
                GovForms.SignatoryNew(uProfile.NTID, formtype);
                GovForms.GetAppEMP(77, uProfile.NTID);
                string empidApprover = "";
                empidApprover = GovForms.ApproverEMPID;
                if (empidApprover == "" || empidApprover == null || string.IsNullOrWhiteSpace(empidApprover))
                {
                }
                else
                {
                    GovForms.GetApproverNameJobDes(empidApprover);
                    string Approvername = GovForms.ApproverName;
                    string ApproverDesc = GovForms.ApproverJobDesc;
                    GovForms.GetSignatureSignPicture(empidApprover, "sign_1");
                    string Bitsignature = null;
                    Bitsignature = GovForms.BitSign;
                    Byte[] SignatureBit = null;
                    iTextSharp.text.Image signatory = null;
                    var pdfContentByte = stamper.GetOverContent(1);
                    if (string.IsNullOrWhiteSpace(Bitsignature) == false)
                    {
                        Bitsignature = Bitsignature.Replace(' ', '+');
                        SignatureBit = Convert.FromBase64String(Bitsignature);
                        signatory = iTextSharp.text.Image.GetInstance(SignatureBit);
                        signatory.SetAbsolutePosition(480, 230);
                        signatory.ScaleToFit(100, 100);
                        fields.SetField("PrintName", Approvername);
                        fields.SetField("PositionTitle", ApproverDesc);
                        pdfContentByte.AddImage(signatory);
                    }
                }

                stamper.FormFlattening = true;
                stamper.Close();
                uProfile.Status = "Success";
            }
            catch (Exception ex)
            {
                uProfile.Status = ex.ToString();
            }

            return uProfile.Status;

        }
        #endregion
        //#region "R1_A dipa tapos"
        //[HttpPost]
        //[Route("R1_A")]
        //public string R1_A(UserProfile uProfile)
        //{
        //    try
        //    {


        //        int numOfPage = (GovForms.getNamesandGovNumbers().Rows.Count) / 10;
        //        if ((GovForms.getNamesandGovNumbers().Rows.Count) % 10 != 0)
        //        {
        //            numOfPage += 1;
        //        }
        //        string path = HttpContext.Current.Server.MapPath("pdf");
        //        string formFile = path + "\\GovForms-Employee\\R1-Aset1.pdf";//should always exist
        //        string formFile2 = path + "\\GovForms Templates\\R1-Ab.pdf";//should always exist
        //        string newFile = path + "\\GovForms-Employee\\R1-A-temp.pdf";//should always exist
        //        string newerfile = path + "\\GovForms-Employee\\R1-A-junk" + ext + ".pdf";
        //        string newestfile = path + "\\GovForms-Employee\\R1-A-placeholder.pdf";//should always exist
        //        string finalfile = path + "\\GovForms-Employee\\R1-A-" + uProfile.NTID + "-" + ext + ".pdf";

        //        File.Delete(newerfile);
        //        File.Copy(newestfile, newerfile);
        //        createPageR1(numOfPage, uProfile.NTID);
        //        connectPageR1(numOfPage);
        //        var pdfReader1 = new PdfReader(newFile);
        //        AcroFields af = pdfReader1.AcroFields;
        //        PdfReader reader = new PdfReader(newFile);
        //        PdfStamper stamper = new PdfStamper(reader, new FileStream(finalfile, FileMode.Create));
        //        var forms = stamper.AcroFields;

        //        stamper.Close();
        //        pdfReader1.Close();
        //        reader.Close();
        //        uProfile.Status = "Success";
        //    }
        //    catch (Exception ex)
        //    {
        //        uProfile.Status = ex.ToString();
        //    }
        //    return uProfile.Status;

        //}
        //private void createPageR1(int iter, string empid)
        //{
        //    for (int i = 1; i <= iter; i++)
        //    {

        //        string path = HttpContext.Current.Server.MapPath("pdf");
        //        string formFile = path + "\\GovForms Templates\\R1-A.pdf";
        //        string formFile2 = path + "\\GovForms Templates\\R1-Ab.pdf";
        //        string newFile = path + "\\GovForms-Employee\\R1-A-temp.pdf";
        //        string newerfile = path + "\\GovForms-Employee\\R1-A-junk" + ext + ".pdf";
        //        string newestfile = path + "\\GovForms-Employee\\R1-A-placeholder.pdf";
        //        string newestfile2 = path + "\\GovForms-Employee\\R1-A-set" + i + ext + ".pdf";

        //        var pdfReader1 = new PdfReader(path + "/GovForms Templates/R1-A.pdf");

        //        AcroFields af = pdfReader1.AcroFields;


        //        PdfReader reader = new PdfReader(formFile);

        //        PdfStamper stamper = new PdfStamper(reader, new FileStream(newestfile2, FileMode.Create));

        //        var form = stamper.AcroFields;

        //        try // try catch for empid if session expires: redirects to login page
        //        {

        //            //Employer ID 50-62
        //            try
        //            {
        //                string EmplyrID = getNumeric(GovForms.getCompanyProfile(empid).Rows[0][39].ToString());
        //                char[] EmplyrIDarr = EmplyrID.ToCharArray();
        //                for (int a = 0; a < 12; a++)
        //                {
        //                    form.SetField("Text" + (a + 50), EmplyrIDarr[a].ToString());
        //                }
        //            }
        //            catch
        //            {

        //            }

        //            //Employer Name
        //            try
        //            {
        //                form.SetField("Text1", GovForms.getCompanyProfile(empid).Rows[0][3].ToString());
        //            }
        //            catch
        //            {

        //            }

        //            //Employer TIN
        //            try
        //            {
        //                form.SetField("Text3", GovForms.getCompanyProfile(empid).Rows[0][38].ToString());
        //            }
        //            catch
        //            {

        //            }

        //            //Employer Address 6-13
        //            try
        //            {
        //                string[] CompAdd =
        //            {
        //            GovForms.getCompanyProfile(empid).Rows[0][6].ToString(),
        //            GovForms.getCompanyProfile(empid).Rows[0][7].ToString(),
        //            GovForms.getCompanyProfile(empid).Rows[0][8].ToString(),
        //            GovForms.getCompanyProfile(empid).Rows[0][9].ToString(),
        //            GovForms.getCompanyProfile(empid).Rows[0][10].ToString(),
        //            GovForms.getCompanyProfile(empid).Rows[0][11].ToString(),
        //            GovForms.getCompanyProfile(empid).Rows[0][12].ToString(),
        //            GovForms.getCompanyProfile(empid).Rows[0][13].ToString()
        //        };
        //                string fullCompAdd = "";
        //                for (int j = 0; j <= 7; j++)
        //                {
        //                    fullCompAdd = fullCompAdd + "" + CompAdd[j] + " ";
        //                }
        //                form.SetField("Text2", fullCompAdd);
        //            }
        //            catch
        //            {

        //            }


        //            // Zip Code
        //            try
        //            {
        //                string ZC = GovForms.getCompanyProfile(empid).Rows[0][14].ToString();
        //                string zc1 = ZC.Substring(0, 1);
        //                string zipCode = zc1;
        //                form.SetField("Text63", zipCode);

        //                string zc2 = ZC.Substring(1, 1);
        //                string zipCode1 = zc2;
        //                form.SetField("Text64", zipCode1);

        //                string zc3 = ZC.Substring(2, 1);
        //                string zipCode2 = zc3;
        //                form.SetField("Text65", zipCode2);

        //                string zc4 = ZC.Substring(3, 1);
        //                string zipCode3 = zc4;
        //                form.SetField("Text66", zipCode3);
        //            }
        //            catch
        //            {

        //            }

        //            //Alphalist

        //            for (int k = 0; k < 20; k++)
        //            {
        //                //variable
        //                int ctr = ((i - 1) * 20) + k;
        //                string fullname = GovForms.getNamesandGovNumbers().Rows[ctr][1].ToString(); //First, Middle, Last

        //                string empid2 = GovForms.getEmployeeIDbyFullName(fullname).Rows[0][0].ToString();
        //                string fullname2 = GovForms.getNamesandGovNumbers().Rows[ctr][3].ToString() + ", " + GovForms.getNamesandGovNumbers().Rows[ctr][2].ToString() + " " + GovForms.getNamesandGovNumbers().Rows[ctr][4].ToString(); //Last, First, Middle
        //                                                                                                                                                                                                                                //Full Name
        //                try
        //                {
        //                    if (k < 9)
        //                    {
        //                        form.SetField("Text" + (k + 4), fullname2);
        //                    }
        //                    else
        //                    {
        //                        form.SetField("Text" + (k + 5), fullname2);
        //                    }
        //                }
        //                catch
        //                {

        //                }
        //                //SSS Number
        //                try
        //                {
        //                    string SSSnum = getNumeric(GovForms.getNamesandGovNumbers().Rows[ctr][9].ToString());
        //                    char[] SSSnumArr = SSSnum.ToCharArray();
        //                    int ctr2 = (k * 10) + 67;
        //                    for (int l = 0; l < 10; l++)
        //                    {
        //                        form.SetField("Text" + (ctr2 + l), SSSnumArr[l].ToString());
        //                    }

        //                }
        //                catch
        //                {

        //                }
        //                //DOB
        //                try
        //                {
        //                    GovForms.getEmployeePersonalInformation(empid2);
        //                    string bdayraw = GovForms.doba;
        //                    if (bdayraw != "")
        //                    {
        //                        string[] bdayarr = bdayraw.Split(new char[0]);
        //                        //month convert from word to number
        //                        string newmonth = "";
        //                        switch (bdayarr[0])
        //                        {
        //                            case "January":
        //                                newmonth = "01";
        //                                break;
        //                            case "February":
        //                                newmonth = "02";
        //                                break;
        //                            case "March":
        //                                newmonth = "03";
        //                                break;
        //                            case "April":
        //                                newmonth = "04";
        //                                break;
        //                            case "May":
        //                                newmonth = "05";
        //                                break;
        //                            case "June":
        //                                newmonth = "06";
        //                                break;
        //                            case "July":
        //                                newmonth = "07";
        //                                break;
        //                            case "August":
        //                                newmonth = "08";
        //                                break;
        //                            case "September":
        //                                newmonth = "09";
        //                                break;
        //                            case "October":
        //                                newmonth = "10";
        //                                break;
        //                            case "November":
        //                                newmonth = "11";
        //                                break;
        //                            case "December":
        //                                newmonth = "12";
        //                                break;
        //                        }
        //                        bdayarr[1] = getNumeric(bdayarr[1]);
        //                        if (bdayarr[1].Length == 1)
        //                        {
        //                            bdayarr[1] = "0" + bdayarr[1];
        //                        }
        //                        bdayraw = getNumeric(newmonth + "" + bdayarr[1] + "" + bdayarr[2]);
        //                        char[] DOBarr = bdayraw.ToCharArray();
        //                        int ctr2 = (k * 8) + 268;
        //                        for (int l = 0; l < 8; l++)
        //                        {
        //                            form.SetField("Text" + (ctr2 + l), DOBarr[l].ToString());
        //                        }
        //                    }

        //                }
        //                catch
        //                {

        //                }
        //                //Position
        //                try
        //                {
        //                    form.SetField("Text" + (k + 25), GovForms.getNamesandGovNumbers().Rows[ctr][11].ToString());
        //                }
        //                catch
        //                {

        //                }
        //                //Monthly Salary
        //                try
        //                {
        //                    string empsal = GovForms.getEmpPay(empid2);
        //                    char[] empsalArr = new char[6] { ' ', ' ', ' ', ' ', ' ', ' ' };
        //                    //
        //                    char[] empsalArr2 = empsal.ToCharArray();
        //                    if (empsalArr2.Length < 6)
        //                    {
        //                        int d = 0;
        //                        for (int c = 6 - empsal.Length; c <= 5; c++)
        //                        {


        //                            try
        //                            {

        //                                if (d < empsalArr2.Length)
        //                                {
        //                                    empsalArr[c] = empsalArr2[d];
        //                                }
        //                                d++;
        //                            }
        //                            catch
        //                            {

        //                            }
        //                        }
        //                    }
        //                    else
        //                    {
        //                        empsalArr = empsal.ToCharArray();
        //                    }
        //                    int iswhite = 0;
        //                    for (int c = 0; c < 6; c++)
        //                    {
        //                        if (Char.IsWhiteSpace(empsalArr[c]))
        //                        {
        //                            iswhite++;
        //                        }
        //                    }
        //                    if (iswhite == 6)
        //                    {
        //                        empsalArr[5] = '0';
        //                    }
        //                    int ctr2 = 0;

        //                    ctr2 = (k * 14) + 431;

        //                    for (int l = 0; l < 6; l++)
        //                    {
        //                        form.SetField("Text" + (ctr2 + l), empsalArr[l].ToString());
        //                    }
        //                }
        //                catch
        //                {

        //                }
        //                //Date of Employment
        //                try
        //                {
        //                    string JoinDate = GovForms.getJoinDate(empid2);
        //                    char[] JoinDateArr = JoinDate.ToCharArray();
        //                    int ctr2 = 0;
        //                    ctr2 = (k * 14) + 437;
        //                    for (int l = 0; l < 8; l++)
        //                    {
        //                        form.SetField("Text" + (ctr2 + l), JoinDateArr[l].ToString());
        //                    }
        //                }
        //                catch
        //                {

        //                }
        //            }

        //            //No. of Pages
        //            form.SetField("Text48", i.ToString());
        //            form.SetField("Text49", iter.ToString());

        //            //Date
        //            form.SetFieldProperty("Text267", "MAXLEN", 0, null);
        //            form.SetField("Text267", DateTime.Now.ToString("MM/dd/yyyy"));

        //            //Signatory
        //            GovForms.Signatory(empid);
        //            form.SetField("Text45", GovForms.signatory1);
        //            form.SetField("Text46", GovForms.position1);
        //            GovForms.GetRequestFormSettings();
        //            GovForms.SignatoryNew(empid, formtype);

        //            //retrieve approver  empid
        //            GovForms.GetAppEMP(79, empid);
        //            string empidApprover = "";
        //            empidApprover = GovForms.ApproverEMPID;
        //            if (empidApprover == "" || empidApprover == null || string.IsNullOrWhiteSpace(empidApprover))
        //            {
        //                //no display signiture ,empname and job desc
        //            }
        //            else
        //            {
        //                //get  approver JObdescription and  empname
        //                GovForms.GetApproverNameJobDes(empidApprover);
        //                string Approvername = GovForms.ApproverName;
        //                string ApproverDesc = GovForms.ApproverJobDesc;
        //                //get signatory sign in based64
        //                GovForms.GetSignatureSignPicture(empidApprover, "sign_1");
        //                string Bitsignature = null;
        //                Bitsignature = GovForms.BitSign;
        //                Byte[] SignatureBit = null;
        //                iTextSharp.text.Image signatory = null;
        //                var pdfContentByte = stamper.GetOverContent(1);
        //                if (string.IsNullOrWhiteSpace(Bitsignature) == false)
        //                {
        //                    Bitsignature = Bitsignature.Replace(' ', '+');
        //                    SignatureBit = Convert.FromBase64String(Bitsignature);
        //                    signatory = iTextSharp.text.Image.GetInstance(SignatureBit);
        //                    signatory.SetAbsolutePosition(60, 178);
        //                    signatory.ScaleToFit(100, 100);
        //                    form.SetField("Text45", Approvername);
        //                    form.SetField("Text46", ApproverDesc);
        //                    pdfContentByte.AddImage(signatory);
        //                }
        //            }



        //            if (GovForms.Permission > 0)
        //            {

        //            }
        //            if (GovForms.PrintSign > 0)
        //            {

        //            }
        //            if (GovForms.Default1 == "True")
        //            {

        //            }
        //            if (GovForms.Default2 == "True")
        //            {

        //            }


        //            form.SetField("text67", i.ToString());

        //            stamper.FormFlattening = true;

        //            stamper.Close();
        //        }
        //        catch (Exception ex)
        //        {
        //            // this.Page.ClientScript.RegisterClientScriptBlock(this.GetType(),
        //            //Guid.NewGuid().ToString("N"), "self.parent.location='login.aspx';", true);
        //        }
        //        //Signatory
        //    }
        //}
        //private void connectPageR1(int pages)
        //{
        //    string path = HttpContext.Current.Server.MapPath("pdf");
        //    string formFile = path + "\\GovForms-Employee\\R1-A-set1" + ext + ".pdf";
        //    string formFile2 = path + "\\GovForms Templates\\R1-Ab.pdf";
        //    string newFile = path + "\\GovForms-Employee\\R1-A-temp.pdf";
        //    string newerfile = path + "\\GovForms-Employee\\R1-A-junk" + ext + ".pdf";
        //    string newestfile = path + "\\GovForms-Employee\\R1-A-placeholder.pdf";

        //    for (int i = 1; i <= pages; i++)
        //    {
        //        string newestfile2 = path + "\\GovForms-Employee\\R1-A-set" + i + ext + ".pdf";
        //        string secondfile = path + "\\GovForms-Employee\\R1-A-set2" + ext + ".pdf";

        //        try // try catch for empid if session expires: redirects to login page
        //        {

        //            if (i == 1)
        //            {
        //                AppendToDocumentR1(formFile, newFile, secondfile, pages);
        //            }
        //            else if (i > 2)
        //            {
        //                File.Delete(formFile2);
        //                File.Copy(newFile, formFile2);
        //                AppendToDocumentR1(formFile2, newFile, newestfile2, pages);
        //            }

        //            var pdfReader1 = new PdfReader(newFile);

        //            AcroFields af = pdfReader1.AcroFields;
        //            PdfReader reader = new PdfReader(newFile);

        //            PdfStamper stamper = new PdfStamper(reader, new FileStream(newestfile2, FileMode.Create));

        //            var form = stamper.AcroFields;

        //            stamper.Close();
        //            reader.Close();
        //            pdfReader1.Close();

        //        }
        //        catch (Exception ex)
        //        {
        //            return;
        //        }
        //    }
        //}
        //private static void AppendToDocumentR1(/* first page of alphalist */string sourcepath, /*temp file */string destpath, /*should be looped*/string extra, int pages)
        //{
        //    for (int i = 1; i <= pages; i++)
        //    {
        //        PdfReader cover = new PdfReader(extra);
        //        PdfReader reader = new PdfReader(sourcepath);
        //        Document document = new Document();
        //        PdfCopy copy = new PdfCopy(document, new FileStream(destpath, FileMode.Create));
        //        document.Open();
        //        copy.AddDocument(reader);
        //        copy.AddDocument(cover);
        //        document.Close();
        //        reader.Close();
        //        copy.Close();
        //        cover.Close();
        //    }
        //}
        //#endregion
        //#region "R_3 dipa tapos"
        //[HttpPost]
        //[Route("R_3")]
        //public string R_3(UserProfile uProfile)
        //{

        //    int numOfPage = (GovForms.getNamesandGovNumbers().Rows.Count) / 15;
        //    if ((GovForms.getNamesandGovNumbers().Rows.Count) % 15 != 0)
        //    {
        //        numOfPage += 1;
        //    }
        //    try
        //    {
        //        string path = HttpContext.Current.Server.MapPath("pdf");
        //        string formFile = path + "\\GovForms-Employee\\R-3-set1.pdf";//should always exist
        //        string formFile2 = path + "\\GovForms Templates\\R-3b.pdf";//should always exist
        //        string newFile = path + "\\GovForms-Employee\\R-3-temp.pdf";//should always exist
        //        string newerfile = path + "\\GovForms-Employee\\R-3-junk" + ext + ".pdf";
        //        string newestfile = path + "\\GovForms-Employee\\R-3-placeholder.pdf";//should always exist
        //        string finalfile = path + "\\GovForms-Employee\\R-3-final(" + ext + ").pdf";

        //        File.Delete(newerfile);
        //        File.Copy(newestfile, newerfile);
        //        createPageR3(numOfPage, uProfile.NTID);
        //        connectPageR3(numOfPage);

        //        var pdfReader1 = new PdfReader(newFile);
        //        AcroFields af = pdfReader1.AcroFields;
        //        PdfReader reader = new PdfReader(newFile);
        //        PdfStamper stamper = new PdfStamper(reader, new FileStream(finalfile, FileMode.Create));
        //        var forms = stamper.AcroFields;


        //        stamper.Close();
        //        pdfReader1.Close();
        //        reader.Close();
        //        uProfile.Status = "Success";
        //    }
        //    catch (Exception ex)
        //    {
        //        uProfile.Status = ex.ToString();
        //    }



        //    return uProfile.Status;
        //}
        //private void createPageR3(int iter, string empid)
        //{
        //    for (int i = 1; i <= iter; i++)
        //    {
        //        int totalval1 = 0;
        //        int totalval2 = 0;

        //        string path = HttpContext.Current.Server.MapPath("pdf");
        //        var pdfReader1 = new PdfReader(path + "/GovForms Templates/R-3.pdf");

        //        AcroFields af = pdfReader1.AcroFields;
        //        string formFile = path + "\\GovForms Templates\\R-3.pdf";
        //        string formFile2 = path + "\\GovForms Templates\\R-3b.pdf";
        //        string newFile = path + "\\GovForms-Employee\\R-3temp.pdf";
        //        string newerfile = path + "\\GovForms-Employee\\R-3-junk" + ext + ".pdf";
        //        string newestfile = path + "\\GovForms-Employee\\R-3-placeholder.pdf";
        //        string newestfile2 = path + "\\GovForms-Employee\\R-3-set" + i + ext + ".pdf";

        //        PdfReader reader = new PdfReader(formFile);
        //        PdfStamper stamper = new PdfStamper(reader, new FileStream(newestfile2, FileMode.Create));

        //        var form = stamper.AcroFields;

        //        try // try catch for empid if session expires: redirects to login page
        //        {

        //            for (int j = 0; j < 15; j++)
        //            {
        //                int ctr = ((i - 1) * 15) + j;
        //                string fullname = GovForms.getNamesandGovNumbers().Rows[ctr][1].ToString();
        //                string empid2 = GovForms.getEmployeeIDbyFullName(fullname).Rows[0][0].ToString();
        //                GovForms.getEmployeePersonalInformation(empid2);
        //                string[] phArr = GovForms.getNamesandGovNumbers().Rows[ctr][9].ToString().Split('-');
        //                try
        //                {

        //                    //Name Field
        //                    if (GovForms.getNamesandGovNumbers().Rows[ctr][4].ToString() != "")
        //                    {
        //                        form.SetField("name" + (j + 1), GovForms.getNamesandGovNumbers().Rows[ctr][3].ToString().ToUpper() + " " + GovForms.getNamesandGovNumbers().Rows[ctr][2].ToString().ToUpper() + " " + GovForms.getNamesandGovNumbers().Rows[ctr][4].ToString().Substring(0, 1).ToUpper());
        //                    }
        //                    else
        //                    {
        //                        form.SetField("name" + (j + 1), GovForms.getNamesandGovNumbers().Rows[ctr][3].ToString().ToUpper() + " " + GovForms.getNamesandGovNumbers().Rows[ctr][2].ToString().ToUpper());
        //                    }
        //                    //end of Name Field
        //                }
        //                catch (Exception ex3)
        //                {

        //                }
        //                //SSS employee
        //                try
        //                {
        //                    form.SetField("sssNum" + (j + 1) + "_1", phArr[0].Substring(0, 1));
        //                    form.SetField("sssNum" + (j + 1) + "_2", phArr[0].Substring(1, 1));
        //                    form.SetField("sssNum" + (j + 1) + "_3", phArr[1].Substring(0, 1));
        //                    form.SetField("sssNum" + (j + 1) + "_4", phArr[1].Substring(1, 1));
        //                    form.SetField("sssNum" + (j + 1) + "_5", phArr[1].Substring(2, 1));
        //                    form.SetField("sssNum" + (j + 1) + "_6", phArr[1].Substring(3, 1));
        //                    form.SetField("sssNum" + (j + 1) + "_7", phArr[1].Substring(4, 1));
        //                    form.SetField("sssNum" + (j + 1) + "_8", phArr[1].Substring(5, 1));
        //                    form.SetField("sssNum" + (j + 1) + "_9", phArr[1].Substring(6, 1));
        //                    form.SetField("sssNum" + (j + 1) + "_10", phArr[2].Substring(0, 1));
        //                }
        //                catch (Exception ex3)
        //                {

        //                }
        //                //end SSS employee
        //                // start monthly contribution
        //                try
        //                {
        //                    string thisyear = DateTime.Now.ToString("yyyy");
        //                    string thismonth = DateTime.Now.ToString("MM");
        //                    int thismonthnum = Convert.ToInt32(thismonth);
        //                    DataTable ds = new DataTable();


        //                    if (thismonthnum <= 3 && thismonthnum > 0)
        //                    {
        //                        thismonthnum = 1;
        //                    }
        //                    else if (thismonthnum <= 6 && thismonthnum > 3)
        //                    {
        //                        thismonthnum = 4;
        //                    }
        //                    else if (thismonthnum <= 9 && thismonthnum > 6)
        //                    {
        //                        thismonthnum = 7;
        //                    }
        //                    else
        //                    {
        //                        thismonthnum = 10;
        //                    }
        //                    string sssmonthly = "";
        //                    try
        //                    {
        //                        //1st month
        //                        ds = GovForms.getGovContrib(empid2, thisyear, "0" + thismonthnum.ToString());
        //                        sssmonthly = ds.Rows[0][2].ToString();
        //                        form.SetField("sssMonth1" + (j + 1) + "5", sssmonthly.Substring(2, 1));
        //                        form.SetField("sssMonth1" + (j + 1) + "4", sssmonthly.Substring(1, 1));
        //                        form.SetField("sssMonth1" + (j + 1) + "3", sssmonthly.Substring(0, 1));
        //                    }
        //                    catch
        //                    {
        //                        sssmonthly = "0";
        //                    }
        //                    try
        //                    {
        //                        //2nd month
        //                        thismonthnum += 1;
        //                        ds = GovForms.getGovContrib(empid2, thisyear, "0" + thismonthnum.ToString());
        //                        sssmonthly = ds.Rows[0][2].ToString();
        //                        form.SetField("sssMonth2" + (j + 1) + "5", sssmonthly.Substring(2, 1));
        //                        form.SetField("sssMonth2" + (j + 1) + "4", sssmonthly.Substring(2, 1));
        //                        form.SetField("sssMonth2" + (j + 1) + "3", sssmonthly.Substring(2, 1));
        //                    }
        //                    catch
        //                    {
        //                        //sssmonthly = "0";
        //                    }
        //                    try
        //                    {
        //                        //3rd month
        //                        thismonthnum += 1;
        //                        ds = GovForms.getGovContrib(empid2, thisyear, "0" + thismonthnum.ToString());
        //                        sssmonthly = ds.Rows[0][2].ToString();
        //                        form.SetField("sssMonth3" + (j + 1) + "5", sssmonthly.Substring(2, 1));
        //                        form.SetField("sssMonth3" + (j + 1) + "4", sssmonthly.Substring(2, 1));
        //                        form.SetField("sssMonth3" + (j + 1) + "3", sssmonthly.Substring(2, 1));
        //                    }
        //                    catch
        //                    {
        //                        // sssmonthly = "0";
        //                    }
        //                    //EC month
        //                    form.SetField("ECmonth1" + (j + 1) + "3", "0");
        //                    form.SetField("ECmonth1" + (j + 1) + "2", "1");

        //                    form.SetField("ECmonth2" + (j + 1) + "3", "0");
        //                    form.SetField("ECmonth2" + (j + 1) + "2", "1");

        //                    form.SetField("ECmonth3" + (j + 1) + "3", "0");
        //                    form.SetField("ECmonth3" + (j + 1) + "2", "1");
        //                    int tempnum = Convert.ToInt32(sssmonthly);
        //                    totalval1 += tempnum;
        //                    totalval2 += 10;

        //                    double empsal = Convert.ToDouble(GovForms.PaySlipRecordsEmployeePay(empid2).Rows[ctr][2].ToString());
        //                    if (empsal != 0)
        //                    {
        //                        double ec = empsal * 0.01;
        //                    }
        //                }
        //                catch (Exception ex3)
        //                {

        //                }
        //                //end monthly contribution
        //                //Headers
        //                string fullSSN = GovForms.getCompanyProfile(empid2).Rows[0][39].ToString().Replace("-", "");
        //                char[] SSNbd = fullSSN.ToCharArray();

        //                for (int m = 0; m < SSNbd.Length; m++)
        //                {
        //                    form.SetField("empNum" + (m + 1), SSNbd[m].ToString());
        //                }

        //                //employer name
        //                form.SetField("employerName", GovForms.getCompanyProfile(empid2).Rows[0][3].ToString());

        //                //tel no.
        //                form.SetField("telNo", GovForms.getCompanyProfile(empid2).Rows[0][15].ToString());

        //                //address
        //                string compadd = "";
        //                for (int m = 6; m <= 14; m++)
        //                {
        //                    compadd = compadd + " " + GovForms.getCompanyProfile(empid2).Rows[0][m].ToString();
        //                }

        //                form.SetField("address", compadd);
        //                //end Headers
        //            }
        //            try
        //            {
        //                string stringval = totalval1.ToString(), stringval2 = totalval2.ToString();

        //                form.SetField("ssstotal15", stringval.Substring((stringval.Length) - 1, 1));
        //                form.SetField("ssstotal14", stringval.Substring((stringval.Length) - 2, 1));
        //                form.SetField("ssstotal13", stringval.Substring((stringval.Length) - 3, 1));
        //                form.SetField("ssstotal12", stringval.Substring((stringval.Length) - 4, 1));
        //                //form.SetField("ssstotal11", stringval.Substring((stringval.Length) - 5, 1));

        //                form.SetField("ssstotal25", stringval.Substring((stringval.Length) - 1, 1));
        //                form.SetField("ssstotal24", stringval.Substring((stringval.Length) - 2, 1));
        //                form.SetField("ssstotal23", stringval.Substring((stringval.Length) - 3, 1));
        //                form.SetField("ssstotal22", stringval.Substring((stringval.Length) - 4, 1));
        //                // form.SetField("ssstotal21", stringval.Substring((stringval.Length) - 5, 1));

        //                form.SetField("ssstotal35", stringval.Substring((stringval.Length) - 1, 1));
        //                form.SetField("ssstotal34", stringval.Substring((stringval.Length) - 2, 1));
        //                form.SetField("ssstotal33", stringval.Substring((stringval.Length) - 3, 1));
        //                form.SetField("ssstotal32", stringval.Substring((stringval.Length) - 4, 1));
        //                // form.SetField("ssstotal31", stringval.Substring((stringval.Length) - 5, 1));

        //                form.SetField("ECmonth13", stringval2.Substring((stringval2.Length) - 1, 1));
        //                form.SetField("ECmonth12", stringval2.Substring((stringval2.Length) - 2, 1));
        //                form.SetField("ECmonth11", stringval2.Substring((stringval2.Length) - 3, 1));

        //                form.SetField("ECmonth23", stringval2.Substring((stringval2.Length) - 1, 1));
        //                form.SetField("ECmonth22", stringval2.Substring((stringval2.Length) - 2, 1));
        //                form.SetField("ECmonth21", stringval2.Substring((stringval2.Length) - 3, 1));

        //                form.SetField("ECmonth33", stringval2.Substring((stringval2.Length) - 1, 1));
        //                form.SetField("ECmonth32", stringval2.Substring((stringval2.Length) - 2, 1));
        //                form.SetField("ECmonth31", stringval2.Substring((stringval2.Length) - 3, 1));

        //            }
        //            catch (Exception ex3)
        //            {

        //            }
        //            //Signatory

        //            GovForms.GetRequestFormSettings();
        //            GovForms.Signatory(empid);

        //            //retrieve approver  empid
        //            GovForms.GetAppEMP(80, empid);
        //            string empidApprover = "";
        //            empidApprover = GovForms.ApproverEMPID;
        //            if (empidApprover == "" || empidApprover == null || string.IsNullOrWhiteSpace(empidApprover))
        //            {
        //                //no display signiture ,empname and job desc
        //            }
        //            else
        //            {
        //                //get  approver JObdescription and  empname
        //                GovForms.GetApproverNameJobDes(empidApprover);
        //                string Approvername = GovForms.ApproverName;
        //                string ApproverDesc = GovForms.ApproverJobDesc;
        //                //get signatory sign in based64
        //                GovForms.GetSignatureSignPicture(empidApprover, "sign_1");
        //                string Bitsignature = null;
        //                Bitsignature = GovForms.BitSign;
        //                Byte[] SignatureBit = null;
        //                iTextSharp.text.Image signatory = null;
        //                var pdfContentByte = stamper.GetOverContent(1);
        //                if (string.IsNullOrWhiteSpace(Bitsignature) == false)
        //                {
        //                    Bitsignature = Bitsignature.Replace(' ', '+');
        //                    SignatureBit = Convert.FromBase64String(Bitsignature);
        //                    signatory = iTextSharp.text.Image.GetInstance(SignatureBit);
        //                    signatory.SetAbsolutePosition(550, 135);
        //                    signatory.ScaleToFit(100, 100);
        //                    form.SetField("untitled315", Approvername);
        //                    form.SetField("untitled316", ApproverDesc);
        //                    pdfContentByte.AddImage(signatory);
        //                }
        //            }

        //            stamper.FormFlattening = true;
        //            stamper.Close();
        //            reader.Close();
        //            pdfReader1.Close();
        //        }
        //        catch (Exception ex)
        //        {

        //        }

        //    }
        //}
        //private void connectPageR3(int pages)
        //{
        //    string path = HttpContext.Current.Server.MapPath("pdf");
        //    string formFile = path + "\\GovForms-Employee\\R-3-set1" + ext + ".pdf";
        //    string formFile2 = path + "\\GovForms Templates\\R-3b.pdf";
        //    string newFile = path + "\\GovForms-Employee\\R-3-temp.pdf";
        //    string newerfile = path + "\\GovForms-Employee\\R-3-junk" + ext + ".pdf";
        //    string newestfile = path + "\\GovForms-Employee\\R-3-placeholder.pdf";

        //    for (int i = 1; i <= pages; i++)
        //    {
        //        string newestfile2 = path + "\\GovForms-Employee\\R-3-set" + i + ext + ".pdf";
        //        string secondfile = path + "\\GovForms-Employee\\R-3-set2" + ext + ".pdf";

        //        if (i == 1)
        //        {
        //            AppendToDocumentR3(formFile, newFile, secondfile, pages);
        //        }
        //        else if (i > 2)
        //        {
        //            File.Delete(formFile2);
        //            File.Copy(newFile, formFile2);
        //            AppendToDocumentR3(formFile2, newFile, newestfile2, pages);
        //        }

        //        var pdfReader1 = new PdfReader(newFile);

        //        AcroFields af = pdfReader1.AcroFields;
        //        PdfReader reader = new PdfReader(newFile);

        //        PdfStamper stamper = new PdfStamper(reader, new FileStream(newestfile2, FileMode.Create));

        //        var forms = stamper.AcroFields;
        //        //to read only
        //        stamper.FormFlattening = true;
        //        stamper.Close();
        //        reader.Close();
        //        pdfReader1.Close();
        //    }
        //}
        //private static void AppendToDocumentR3(/* first page of alphalist */string sourcepath, /*temp file */string destpath, /*should be looped*/string extra, int pages)
        //{
        //    for (int i = 1; i <= pages; i++)
        //    {
        //        PdfReader cover = new PdfReader(extra);
        //        PdfReader reader = new PdfReader(sourcepath);
        //        Document document = new Document();
        //        PdfCopy copy = new PdfCopy(document, new FileStream(destpath, FileMode.Create));
        //        document.Open();
        //        copy.AddDocument(reader);
        //        copy.AddDocument(cover);
        //        document.Close();
        //        reader.Close();
        //        copy.Close();
        //        cover.Close();
        //    }
        //}
        //#endregion
        #region "SSSForms_Employer_Change_Request"
        [HttpPost]
        [Route("SSSForms_Employer_Change_Request")]
        public string SSSForms_Employer_Change_Request(UserProfile uProfile)
        {
            string formname = "Change Request";
            string path = HttpContext.Current.Server.MapPath("pdf");

            var pdfReader1 = new PdfReader(path + "/GovForms Templates/SSSForms_Employer_Change_Request.pdf");
            AcroFields af = pdfReader1.AcroFields;

            string grr = "";
            foreach (var field in af.Fields)
            {
                grr = grr + " " + field.Key; // names of textfields
            }
            string formFile = path + "/GovForms Templates//SSSForms_Employer_Change_Request.pdf";
            string newFile = path + "/GovForms-Employee/SSSForms_Employer_Change_Request-" + uProfile.NTID + ".pdf";

            PdfReader reader = new PdfReader(formFile);
            PdfStamper stamper = new PdfStamper(reader, new FileStream(newFile, FileMode.Create));
            string empbranchid = GovForms.getCompanyProfile(uProfile.NTID).Rows[0][0].ToString();
            var forms = stamper.AcroFields;
            try // try catch for uProfile.NTID if session expires: redirects to login page
            {
                //EMPLOYER ID
                try
                {
                    string SSNco = GovForms.getCompanyProfile(uProfile.NTID).Rows[0][39].ToString().Replace("-", "");
                    char[] SSNcobd = SSNco.ToCharArray();
                    //1st part
                    forms.SetFieldProperty("untitled1", "textsize", 14f, null);
                    forms.SetField("untitled1", SSNcobd[0] + "  " + SSNcobd[1]);
                    //2nd part
                    string SSN2nd = "";
                    for (int i = 2; i < 10; i++)
                    {
                        SSN2nd = SSN2nd + SSNcobd[i] + "  ";
                    }
                    forms.SetFieldProperty("untitled2", "textsize", 14f, null);
                    forms.SetField("untitled2", SSN2nd);
                    //3rd part
                    forms.SetFieldProperty("untitled3", "textsize", 14f, null);
                    forms.SetField("untitled3", SSNcobd[10] + " " + SSNcobd[11] + " " + SSNcobd[12]);
                }
                catch
                {

                }


                try
                {
                    forms.SetField("untitled10", GovForms.GetSSSempchangereq(uProfile.NTID).Rows[0][1].ToString() == "" ? "" : GovForms.GetSSSempchangereq(uProfile.NTID).Rows[0][1].ToString().First().ToString().ToUpper() + GovForms.GetSSSempchangereq(uProfile.NTID).Rows[0][1].ToString().Substring(1));

                    forms.SetField("untitled11", "                                                " + GovForms.GetSSSempchangereq(uProfile.NTID).Rows[0][2].ToString() == "" ? "" : GovForms.GetSSSempchangereq(uProfile.NTID).Rows[0][2].ToString().First().ToString().ToUpper() + GovForms.GetSSSempchangereq(uProfile.NTID).Rows[0][2].ToString().Substring(1) + " " +
                                               GovForms.GetSSSempchangereq(uProfile.NTID).Rows[0][3].ToString() == "" ? "" : GovForms.GetSSSempchangereq(uProfile.NTID).Rows[0][3].ToString().First().ToString().ToUpper() + GovForms.GetSSSempchangereq(uProfile.NTID).Rows[0][3].ToString().Substring(1));

                    forms.SetField("untitled13", GovForms.GetSSSempchangereq(uProfile.NTID).Rows[0][4].ToString() == "" ? "" : GovForms.GetSSSempchangereq(uProfile.NTID).Rows[0][4].ToString().First().ToString().ToUpper() + GovForms.GetSSSempchangereq(uProfile.NTID).Rows[0][4].ToString().Substring(1));
                    forms.SetField("untitled15", "     " + GovForms.GetSSSempchangereq(uProfile.NTID).Rows[0][5].ToString() == "" ? "" : GovForms.GetSSSempchangereq(uProfile.NTID).Rows[0][5].ToString().First().ToString().ToUpper() + GovForms.GetSSSempchangereq(uProfile.NTID).Rows[0][5].ToString().Substring(1));
                    forms.SetField("untitled16", "              " + GovForms.GetSSSempchangereq(uProfile.NTID).Rows[0][6].ToString() == "" ? "" : GovForms.GetSSSempchangereq(uProfile.NTID).Rows[0][6].ToString().First().ToString().ToUpper() + GovForms.GetSSSempchangereq(uProfile.NTID).Rows[0][6].ToString().Substring(1));
                    forms.SetField("untitled17", "                  " + GovForms.GetSSSempchangereq(uProfile.NTID).Rows[0][7].ToString() == "" ? "" : GovForms.GetSSSempchangereq(uProfile.NTID).Rows[0][7].ToString().First().ToString().ToUpper() + GovForms.GetSSSempchangereq(uProfile.NTID).Rows[0][7].ToString().Substring(1));

                    forms.SetField("untitled23", GovForms.GetCompanyEmail(empbranchid).Rows[0][0].ToString());
                }
                catch
                {

                }


                //forms.SetField("untitled3", GovForms.GetSSSempchangereq(uProfile.NTID).Rows[0][4].ToString().First().ToString().ToUpper() + GovForms.GetSSSempchangereq(uProfile.NTID).Rows[0][4].ToString().Substring(1) + " " +
                //                            GovForms.GetSSSempchangereq(uProfile.NTID).Rows[0][5].ToString().First().ToString().ToUpper() + GovForms.GetSSSempchangereq(uProfile.NTID).Rows[0][5].ToString().Substring(1));
                //forms.SetField("untitled8", GovForms.GetSSSempchangereq(uProfile.NTID).Rows[0][6].ToString().First().ToString().ToUpper() + GovForms.GetSSSempchangereq(uProfile.NTID).Rows[0][6].ToString().Substring(1));
                //forms.SetField("untitled9", GovForms.GetSSSempchangereq(uProfile.NTID).Rows[0][7].ToString().First().ToString().ToUpper() + GovForms.GetSSSempchangereq(uProfile.NTID).Rows[0][7].ToString().Substring(1));
                //forms.SetField("untitled23", GovForms.GetSSSempchangereq(uProfile.NTID).Rows[0][8].ToString().First().ToString().ToUpper() + GovForms.GetSSSempchangereq(uProfile.NTID).Rows[0][8].ToString().Substring(1));
                //forms.SetField("untitled24", GovForms.GetSSSempchangereq(uProfile.NTID).Rows[0][9].ToString().First().ToString().ToUpper() + GovForms.GetSSSempchangereq(uProfile.NTID).Rows[0][9].ToString().Substring(1));
                //forms.SetField("untitled25", GovForms.GetSSSempchangereq(uProfile.NTID).Rows[0][10].ToString().First().ToString().ToUpper() + GovForms.GetSSSempchangereq(uProfile.NTID).Rows[0][10].ToString().Substring(1));
                //forms.SetField("untitled26", GovForms.GetSSSempchangereq(uProfile.NTID).Rows[0][11].ToString().First().ToString().ToUpper() + GovForms.GetSSSempchangereq(uProfile.NTID).Rows[0][11].ToString().Substring(1));
                //forms.SetField("untitled27", GovForms.GetSSSempchangereq(uProfile.NTID).Rows[0][12].ToString().First().ToString().ToUpper() + GovForms.GetSSSempchangereq(uProfile.NTID).Rows[0][12].ToString().Substring(1));
                //forms.SetField("untitled28", GovForms.GetSSSempchangereq(uProfile.NTID).Rows[0][13].ToString().First().ToString().ToUpper() + GovForms.GetSSSempchangereq(uProfile.NTID).Rows[0][13].ToString().Substring(1));
                //forms.SetField("untitled29", GovForms.GetSSSempchangereq(uProfile.NTID).Rows[0][14].ToString().First().ToString().ToUpper() + GovForms.GetSSSempchangereq(uProfile.NTID).Rows[0][14].ToString().Substring(1));

                //SEPERATING NUMBER BY BOX (TIN)

                try
                {
                    string TIN = GovForms.GetSSSempchangereq(uProfile.NTID).Rows[0][0].ToString().Replace("-", "");
                    string tn = TIN.Substring(0, 1);
                    string tn1 = TIN.Substring(1, 1);
                    string tn2 = TIN.Substring(2, 1);
                    string tntl = tn + "  " + tn1 + "  " + tn2;
                    forms.SetFieldProperty("untitled6", "textsize", 14f, null);
                    forms.SetField("untitled6", tntl);


                    string tn3 = TIN.Substring(3, 1);
                    string tn4 = TIN.Substring(4, 1);
                    string tn5 = TIN.Substring(5, 1);
                    string tntl1 = tn3 + "  " + tn4 + "  " + tn5;
                    forms.SetFieldProperty("untitled7", "textsize", 14f, null);
                    forms.SetField("untitled7", tntl1);


                    string tn6 = TIN.Substring(6, 1);
                    string tn7 = TIN.Substring(7, 1);
                    string tn8 = TIN.Substring(8, 1);
                    string tntl2 = tn6 + "  " + tn7 + "  " + tn8;
                    forms.SetFieldProperty("untitled8", "textsize", 14f, null);
                    forms.SetField("untitled8", tntl2);

                    //TIN
                }
                catch
                {

                }

                //ZIPCODE
                try
                {
                    string zipcode = GovForms.GetSSSempchangereq(uProfile.NTID).Rows[0][8].ToString().Replace("-", "");
                    string zc = zipcode.Substring(0, 1);
                    string zc1 = zipcode.Substring(1, 1);
                    string zc2 = zipcode.Substring(2, 1);
                    string zc3 = zipcode.Substring(3, 1);
                    string zcode = zc + "  " + zc1 + "  " + zc2 + "  " + zc3;
                    forms.SetFieldProperty("untitled18", "textsize", 14f, null);
                    forms.SetField("untitled18", zcode);
                }
                catch
                {

                }
                //ZIPCODE



                try
                {
                    //contact number

                    forms.SetField("untitled23", GovForms.GetCompanyEmail(empbranchid).Rows[0][0].ToString());
                    for (int i = 0; i < GovForms.GetCompanyContactPhone(empbranchid).Rows.Count; i++)
                    {
                        if (GovForms.GetCompanyContactPhone(empbranchid).Rows[i][0].ToString() == "Work")
                        {
                            //PHONE NUMBER
                            string PN = GovForms.GetCompanyContactPhone(empbranchid).Rows[i][1].ToString().Replace("-", "");
                            string pno = PN.Substring(0, 1);
                            string pno1 = PN.Substring(1, 1);
                            string pno2 = PN.Substring(2, 1);
                            string PNumber = pno + "  " + pno1 + "  " + pno2;
                            forms.SetFieldProperty("untitled19", "textsize", 14f, null);
                            forms.SetField("untitled19", PNumber);

                            string PN1 = GovForms.GetCompanyContactPhone(empbranchid).Rows[i][1].ToString().Replace("-", "");
                            string pno3 = PN1.Substring(3, 1);
                            string pno4 = PN1.Substring(4, 1);
                            string pno5 = PN1.Substring(5, 1);
                            string pno6 = PN1.Substring(6, 1);
                            string PNumber1 = pno3 + "  " + pno4 + "  " + pno5 + "  " + pno6 + "  " + PN1.Substring(7, 1) + "  " + PN.Substring(8, 1) + "  " + PN.Substring(9, 1);
                            forms.SetFieldProperty("untitled20", "textsize", 14f, null);
                            forms.SetField("untitled20", PNumber1);
                        }
                        else if (GovForms.GetCompanyContactPhone(empbranchid).Rows[i][0].ToString() == "Mobile")
                        {
                            string PN = GovForms.GetCompanyContactPhone(empbranchid).Rows[i][1].ToString().Replace("+63", "").Replace("+", "").Replace("-", "");
                            string pno = PN.Substring(0, 1);
                            string pno1 = PN.Substring(1, 1);
                            string pno2 = PN.Substring(2, 1);
                            string pno3 = PN.Substring(3, 1);
                            string PNumber = pno + "  " + pno1 + "  " + pno2 + "  " + pno3;
                            forms.SetFieldProperty("untitled21", "textsize", 14f, null);
                            forms.SetField("untitled21", PNumber);

                            string PN1 = GovForms.GetCompanyContactPhone(empbranchid).Rows[i][1].ToString().Replace("-", "");
                            string pno4 = PN1.Substring(4, 1);
                            string pno5 = PN1.Substring(5, 1);
                            string pno6 = PN1.Substring(6, 1);
                            string pno7 = PN1.Substring(7, 1);
                            string PNumber1 = pno4 + "  " + pno5 + "  " + pno6 + "  " + pno7 + "  " + PN1.Substring(8, 1) + "  " + PN.Substring(9, 1) + "  " + PN.Substring(10, 1);
                            forms.SetFieldProperty("untitled22", "textsize", 14f, null);
                            forms.SetField("untitled22", PNumber1);

                        }
                    }

                }
                catch
                {

                }

                GovForms.GetRequestFormSettings();
                GovForms.SignatoryNew(uProfile.NTID, formname);
                GovForms.GetAppEMP(61, uProfile.NTID);
                string empidApprover = "";
                empidApprover = GovForms.ApproverEMPID;
                if (empidApprover == "" || empidApprover == null || string.IsNullOrWhiteSpace(empidApprover))
                {
                }
                else
                {
                    GovForms.GetApproverNameJobDes(empidApprover);
                    string Approvername = GovForms.ApproverName;
                    string ApproverDesc = GovForms.ApproverJobDesc;
                    GovForms.GetSignatureSignPicture(empidApprover, "sign_1");
                    string Bitsignature = null;
                    Bitsignature = GovForms.BitSign;
                    Byte[] SignatureBit = null;
                    iTextSharp.text.Image signatory = null;
                    var pdfContentByte = stamper.GetOverContent(1);
                    AcroFields.Item item;
                    item = forms.GetFieldItem("untitled116");
                    item.GetMerged(0).Put(PdfName.Q, new PdfNumber(PdfFormField.Q_CENTER));
                    item = forms.GetFieldItem("untitled118");
                    item.GetMerged(0).Put(PdfName.Q, new PdfNumber(PdfFormField.Q_CENTER));
                    item = forms.GetFieldItem("untitled119");
                    item.GetMerged(0).Put(PdfName.Q, new PdfNumber(PdfFormField.Q_CENTER));
                    //form.SetFieldProperty("untitled116", "fflags", PdfFormField.Q_CENTER, null);
                    forms.SetField("untitled116", Approvername.ToUpper());
                    forms.SetField("untitled118", ApproverDesc);
                    forms.SetField("untitled119", DateTime.Now.ToString("MM/dd/yyyy"));
                    if (string.IsNullOrWhiteSpace(Bitsignature) == false)
                    {
                        Bitsignature = Bitsignature.Replace(' ', '+');
                        SignatureBit = Convert.FromBase64String(Bitsignature);
                        signatory = iTextSharp.text.Image.GetInstance(SignatureBit);
                        signatory.SetAbsolutePosition(200, 200);
                        signatory.ScaleToFit(100, 100);
                        pdfContentByte.AddImage(signatory);
                    }
                }
                stamper.FormFlattening = true;
                stamper.Close();
                uProfile.Status = "Success";

            }
            catch (Exception ex)
            {
                uProfile.Status = ex.ToString();
            }

            return uProfile.Status;

        }
        #endregion
        #region "M1_1 dipa tapos"

        #endregion
        #region "FPF"
        [HttpPost]
        [Route("FPF")]
        public string FPF(UserProfile uProfile)
        {
            string path = HttpContext.Current.Server.MapPath("pdf");
            var pdfReader1 = new PdfReader(path + "/GovForms Templates/FPF.pdf");
            AcroFields af = pdfReader1.AcroFields;
            AcroFields.Item item;
            string formFile = path + "/GovForms Templates/FPF.pdf";
            string newFile = path + "/GovForms-Employee/FPF-" + uProfile.NTID + ".pdf";

            PdfReader reader = new PdfReader(formFile);
            PdfStamper stamper = new PdfStamper(reader, new FileStream(newFile, FileMode.Create));

            AcroFields fields = stamper.AcroFields;
            GovForms.getEmployeePersonalInformation(uProfile.NTID);
            DataTable FPFdt = new DataTable();
            try // try catch for uProfile.NTID if session expires: redirects to login page
            {
                con2.myparameters.Clear();
                con2.myparameters.Add(new myParameters { ParameterName = "@uProfile.NTID", mytype = SqlDbType.NVarChar, Value = uProfile.NTID });
                FPFdt = con2.GetDataTable("sp_SearchHRMSEmployeePersonalInformation");

                //Date
                item = fields.GetFieldItem("untitled1");
                item.GetMerged(0).Put(PdfName.Q, new PdfNumber(PdfFormField.Q_CENTER));
                fields.SetField("untitled1", DateTime.Now.ToString("MM-dd-yyyy"));

                //Name
                try
                {
                    fields.SetField("untitled2", GovForms.GetFPF400FORM(uProfile.NTID).Rows[0][2].ToString().First().ToString().ToUpper() + GovForms.GetFPF400FORM(uProfile.NTID).Rows[0][2].ToString().Substring(1));//"Last Name");
                    fields.SetField("untitled3", GovForms.GetFPF400FORM(uProfile.NTID).Rows[0][0].ToString().First().ToString().ToUpper() + GovForms.GetFPF400FORM(uProfile.NTID).Rows[0][0].ToString().Substring(1));//"First Name");
                                                                                                                                                                                                                      //fields.SetField("untitled4", "Name Extensions (Jr., Sr., II, etc.)");
                    fields.SetField("untitled5", GovForms.GetFPF400FORM(uProfile.NTID).Rows[0][1].ToString().First().ToString().ToUpper() + GovForms.GetFPF400FORM(uProfile.NTID).Rows[0][1].ToString().Substring(1)); //"Middle Name (Maiden)");

                }
                catch
                { }
                //Civil Status
                try
                {
                    string cvstat = FPFdt.Rows[0][9].ToString();
                    switch (cvstat)
                    {
                        case "Single":
                            fields.SetField("SignleChk", "Yes", true);
                            break;
                        case "Married":
                            fields.SetField("MarriedChk", "Yes", true);
                            break;
                        case "Widowed":
                            fields.SetField("WidowChk", "Yes", true);
                            break;
                        case "Annulled":
                            fields.SetField("AnnulledChk", "Yes", true);
                            break;
                        case "Legally Separated":
                            fields.SetField("SeparatedChk", "Yes", true);
                            break;
                    }
                }
                catch { }
                //Home Address
                try
                {
                    fields.SetField("untitled6", GovForms.GetFPF400FORM(uProfile.NTID).Rows[0][3].ToString().First().ToString().ToUpper() + GovForms.GetFPF400FORM(uProfile.NTID).Rows[0][3].ToString().Substring(1) + " " + GovForms.provincea);
                }
                catch { }
                //Telephone
                try
                {
                    string telnum = "";
                    for (int g = 0; g < GovForms.GetPersonalInfo(uProfile.NTID).Rows.Count; g++)
                    {
                        telnum = GovForms.GetPersonalInfo(uProfile.NTID).Rows[g][1].ToString() == "Home" ? GovForms.GetPersonalInfo(uProfile.NTID).Rows[g][0].ToString() : "";

                    }
                    fields.SetField("untitled8", telnum);
                }
                catch { }
                //Employer
                try
                {
                    fields.SetField("untitled9", GovForms.getCompanyProfile(uProfile.NTID).Rows[0][3].ToString());
                }
                catch { }
                //Company Address
                try
                {
                    string compaddr = (GovForms.getCompanyProfile(uProfile.NTID).Rows[0][6].ToString() + " " + GovForms.getCompanyProfile(uProfile.NTID).Rows[0][7].ToString() + " " + GovForms.getCompanyProfile(uProfile.NTID).Rows[0][8].ToString() + " " + GovForms.getCompanyProfile(uProfile.NTID).Rows[0][9].ToString() + " " + GovForms.getCompanyProfile(uProfile.NTID).Rows[0][10].ToString() + " " + GovForms.getCompanyProfile(uProfile.NTID).Rows[0][11].ToString() + " " + GovForms.getCompanyProfile(uProfile.NTID).Rows[0][14].ToString()).ToUpper();
                    fields.SetField("untitled10", compaddr);
                }
                catch { }
                //Company Telephone
                try
                {
                    string comptelnum = GovForms.GetCompanyContactPhone(GovForms.getCompanyProfile(uProfile.NTID).Rows[0][0].ToString()).Rows[0][1].ToString();
                    fields.SetField("untitled12", comptelnum);
                }
                catch { }
                //Company ID
                try
                {
                    fields.SetField("untitled13", GovForms.getCompanyProfile(uProfile.NTID).Rows[0][40].ToString());
                }
                catch { }
                //fields.SetField("untitled11", "Company/Employer Address:");
                //fields.SetField("untitled12", GovForms.GetFPF400FORM(uProfile.NTID).Rows[0][8].ToString().First().ToString().ToUpper() + GovForms.GetFPF400FORM(uProfile.NTID).Rows[0][8].ToString().Substring(1)); //"Telephone No.:");
                //fields.SetField("untitled13", GovForms.getCompanyProfile(uProfile.NTID).Rows[0][40].ToString());
                //fields.SetField("untitled34", GovForms.getEmployeeMaster(uProfile.NTID).Rows[0][5].ToString().ToUpper());
                //fields.SetField("ClaimsChk", "No");
                //fields.SetField("STLChk", "No");
                //fields.SetField("OthersChk", "No");
                //fields.SetField("ConsolationChkChk", "No");
                //fields.SetField("Loan", "No");
                //fields.SetField("STLChk", "No");
                //fields.SetField("IntraRad", "Yes");

                //Signature
                try
                {
                    item = fields.GetFieldItem("untitled34");
                    item.GetMerged(0).Put(PdfName.Q, new PdfNumber(PdfFormField.Q_CENTER));

                    fields.SetField("untitled34", (GovForms.GetFPF400FORM(uProfile.NTID).Rows[0][2].ToString() + " " + GovForms.GetFPF400FORM(uProfile.NTID).Rows[0][0].ToString() + " " + GovForms.GetFPF400FORM(uProfile.NTID).Rows[0][1].ToString()).ToUpper());
                }
                catch { }
                stamper.FormFlattening = true;
                stamper.Close();
                uProfile.Status = "Success";
            }
            catch
            {
                uProfile.Status = ext.ToString();
            }
            return uProfile.Status;

        }
        #endregion
        #region "Pagibig_Revised_Loyalty_Card_Application_Form"
        [HttpPost]
        [Route("Pagibig_Revised_Loyalty_Card_Application_Form")]
        public string Pagibig_Revised_Loyalty_Card_Application_Form(UserProfile uProfile)
        {

            string path = HttpContext.Current.Server.MapPath("pdf");
            var pdfReader1 = new PdfReader(path + "/GovForms Templates/Pagibig_Revised_Loyalty_Card_Application_Form.pdf");
            AcroFields af = pdfReader1.AcroFields;
            AcroFields.Item item;
            string formFile = path + "/GovForms Templates/Pagibig_Revised_Loyalty_Card_Application_Form.pdf";
            string newFile = path + "/GovForms-Employee/Pagibig_Revised_Loyalty_Card_Application_Form-" + uProfile.NTID + ".pdf";

            PdfReader reader = new PdfReader(formFile);
            PdfStamper stamper = new PdfStamper(reader, new FileStream(newFile, FileMode.Create));

            AcroFields fields = stamper.AcroFields;
            try
            {
                GovForms.getEmployeePersonalInformation(uProfile.NTID);
                //Datatable
                DataTable MPLdt = new DataTable();
                try
                {
                    con2.myparameters.Add(new myParameters { ParameterName = "@uProfile.NTID", mytype = SqlDbType.NVarChar, Value = uProfile.NTID });
                    MPLdt = con2.GetDataTable("sp_SearchHRMSEmployeePersonalInformation");
                }
                catch
                {

                }
                //TIN
                try
                {
                    string fullTIN = GovForms.GetLoyaltyCard(uProfile.NTID).Rows[0][13].ToString().Replace("-", "");
                    string TIN1 = fullTIN.Substring(0, 1); //First number
                    string TIN2 = fullTIN.Substring(1, 1); //2nd number
                    string TIN3 = fullTIN.Substring(2, 1); //3rd number
                    string TIN1st3 = TIN1 + "  " + TIN2 + "  " + TIN3;
                    fields.SetFieldProperty("untitled33", "textsize", 15f, null);
                    string TIN4 = fullTIN.Substring(3, 1);
                    string TIN5 = fullTIN.Substring(4, 1);
                    string TIN6 = fullTIN.Substring(5, 1);
                    string TIN2nd3 = TIN4 + "  " + TIN5 + "  " + TIN6;
                    fields.SetFieldProperty("untitled34", "textsize", 15f, null);
                    string TIN7 = fullTIN.Substring(6, 1);
                    string TIN8 = fullTIN.Substring(7, 1);
                    string TIN9 = fullTIN.Substring(8, 1);
                    string TIN3rd3 = TIN7 + "  " + TIN8 + "  " + TIN9;
                    fields.SetFieldProperty("untitled35", "textsize", 15f, null);
                    fields.SetField("untitled33", TIN1st3);
                    fields.SetField("untitled34", TIN2nd3);
                    fields.SetField("untitled35", TIN3rd3);
                }
                catch
                {

                }

                //SSS Number
                try
                {
                    string fullSSS = GovForms.GetLoyaltyCard(uProfile.NTID).Rows[0][15].ToString().Replace("-", "");
                    string SSS1 = fullSSS.Substring(0, 1); //First number
                    string SSS2 = fullSSS.Substring(1, 1); //2nd number
                    string SSS1st3 = SSS1 + "  " + SSS2;
                    fields.SetFieldProperty("untitled36", "textsize", 15f, null);
                    fields.SetField("untitled36", SSS1st3);
                    string SSS3 = fullSSS.Substring(2, 1);
                    string SSS4 = fullSSS.Substring(3, 1);
                    string SSS5 = fullSSS.Substring(4, 1);
                    string SSS6 = fullSSS.Substring(5, 1);
                    string SSS7 = fullSSS.Substring(6, 1);
                    string SSS8 = fullSSS.Substring(7, 1);
                    string SSS9 = fullSSS.Substring(8, 1);
                    string SSS2nd3 = SSS3 + "  " + SSS4 + " " + SSS5 + " " + SSS6 + " " + SSS7 + "  " + SSS8 + "  " + SSS9;
                    fields.SetFieldProperty("untitled37", "textsize", 15f, null);
                    fields.SetField("untitled37", SSS2nd3);
                    string SSS10 = fullSSS.Substring(2, 1);
                    string SSS11 = fullSSS.Substring(3, 1);
                    string SSS3rd3 = SSS10 + "  " + SSS11;
                    fields.SetFieldProperty("untitled38", "textsize", 15f, null);
                    fields.SetField("untitled38", SSS2nd3);
                }
                catch
                {

                }
                //Citizenship
                try
                {
                    fields.SetFieldProperty("untitled24", "textsize", 12f, null);
                    fields.SetField("untitled24", GovForms.GetLoyaltyCard(uProfile.NTID).Rows[0][5].ToString().First().ToString().ToUpper() + GovForms.GetLoyaltyCard(uProfile.NTID).Rows[0][5].ToString().Substring(1));
                }
                catch
                {

                }
                //Name
                try
                {
                    fields.SetFieldProperty("untitled16", "textsize", 12f, null);
                    fields.SetField("untitled16", GovForms.GetLoyaltyCard(uProfile.NTID).Rows[0][2].ToString() == "" ? "" : GovForms.GetLoyaltyCard(uProfile.NTID).Rows[0][2].ToString().First().ToString().ToUpper() + GovForms.GetLoyaltyCard(uProfile.NTID).Rows[0][2].ToString().Substring(1));
                    fields.SetFieldProperty("untitled17", "textsize", 12f, null);
                    fields.SetField("untitled17", GovForms.GetLoyaltyCard(uProfile.NTID).Rows[0][0].ToString() == "" ? "" : GovForms.GetLoyaltyCard(uProfile.NTID).Rows[0][0].ToString().First().ToString().ToUpper() + GovForms.GetLoyaltyCard(uProfile.NTID).Rows[0][0].ToString().Substring(1));
                    fields.SetFieldProperty("untitled19", "textsize", 12f, null);
                    fields.SetField("untitled19", GovForms.GetLoyaltyCard(uProfile.NTID).Rows[0][1].ToString() == "" ? "" : GovForms.GetLoyaltyCard(uProfile.NTID).Rows[0][1].ToString().First().ToString().ToUpper() + GovForms.GetLoyaltyCard(uProfile.NTID).Rows[0][1].ToString().Substring(1));
                }
                catch
                {

                }
                try
                {
                    fields.SetField("untitled46", "");//Unit/Room No., Floor
                    fields.SetField("untitled47", "");//Building Name
                    fields.SetField("untitled48", MPLdt.Rows[0][0].ToString().ToUpper());//Lot No., Block No., Phase No. House No. 
                    fields.SetField("untitled49", MPLdt.Rows[0][1].ToString().ToUpper());//Street Name 
                    fields.SetField("untitled50", MPLdt.Rows[0][3].ToString().ToUpper());//Subdivision
                    fields.SetField("untitled51", MPLdt.Rows[0][2].ToString().ToUpper());//Barangay
                    fields.SetField("untitled52", MPLdt.Rows[0][4].ToString().ToUpper());//Municipality/City 
                    fields.SetField("untitled53", MPLdt.Rows[0][23].ToString().ToUpper());//Province
                }
                catch
                {

                }
                //Zip Code
                try
                {
                    string zipcode = MPLdt.Rows[0][6].ToString();
                    string Zip1 = zipcode.Substring(0, 1);
                    string Zip2 = zipcode.Substring(1, 1);
                    string Zip3 = zipcode.Substring(2, 1);
                    string Zip4 = zipcode.Substring(3, 1);
                    string zip = Zip1 + " " + Zip2 + " " + Zip3 + " " + Zip4;
                    fields.SetField("untitled54", zip);
                }
                catch
                {

                }
                //Gender
                try
                {
                    if (GovForms.GetLoyaltyCard(uProfile.NTID).Rows[0][4].ToString() == "M")
                    {
                        fields.SetField("untitled26", "x");
                    }
                    else if (GovForms.GetLoyaltyCard(uProfile.NTID).Rows[0][4].ToString() == "F")
                    {
                        fields.SetField("untitled27", "x");
                    }
                }
                catch
                {

                }
                //Marital Status
                try
                {
                    if (GovForms.GetLoyaltyCard(uProfile.NTID).Rows[0][17].ToString() == "Single")
                    {
                        fields.SetField("untitled28", "x");
                    }
                    else if (GovForms.GetLoyaltyCard(uProfile.NTID).Rows[0][17].ToString() == "Married")
                    {
                        fields.SetField("untitled29", "x");
                    }
                    else if (GovForms.GetLoyaltyCard(uProfile.NTID).Rows[0][17].ToString() == "Widow")
                    {
                        fields.SetField("untitled30", "x");
                    }
                    else if (GovForms.GetLoyaltyCard(uProfile.NTID).Rows[0][17].ToString() == "Annulled")
                    {
                        fields.SetField("untitled31", "x");
                    }
                    else if (GovForms.GetLoyaltyCard(uProfile.NTID).Rows[0][17].ToString() == "LEgaly Separated")
                    {
                        fields.SetField("untitled32", "x");
                    }
                }
                catch
                {

                }
                //Present Home Address
                try
                {
                    fields.SetField("untitled55", "");//Unit/Room No., Floor
                    fields.SetField("untitled56", "");//Building Name
                    fields.SetField("untitled57", MPLdt.Rows[0][0].ToString().ToUpper());//Lot No., Block No., Phase No. House No. 
                    fields.SetField("untitled58", MPLdt.Rows[0][1].ToString().ToUpper());//Street Name 
                    fields.SetField("untitled59", MPLdt.Rows[0][3].ToString().ToUpper());//Subdivision
                    fields.SetField("untitled60", MPLdt.Rows[0][2].ToString().ToUpper());//Barangay
                    fields.SetField("untitled61", MPLdt.Rows[0][4].ToString().ToUpper());//Municipality/City 
                    fields.SetField("untitled62", MPLdt.Rows[0][23].ToString().ToUpper());//Province
                }
                catch
                {

                }
                //Zip
                try
                {
                    string zzipcode = GovForms.GetLoyaltyCard(uProfile.NTID).Rows[0][12].ToString();
                    string zZip1 = zzipcode.Substring(0, 1);
                    string zZip2 = zzipcode.Substring(1, 1);
                    string zZip3 = zzipcode.Substring(2, 1);
                    string zZip4 = zzipcode.Substring(3, 1);
                    string zzip = zZip1 + " " + zZip2 + " " + zZip3 + " " + zZip4;
                    fields.SetField("untitled63", zzip);
                }
                catch
                {

                }
                string fname = GovForms.GetLoyaltyCard(uProfile.NTID).Rows[0][0].ToString() == "" ? "" : GovForms.GetLoyaltyCard(uProfile.NTID).Rows[0][0].ToString().First().ToString().ToUpper() + GovForms.GetLoyaltyCard(uProfile.NTID).Rows[0][0].ToString().Substring(1);
                string mname = GovForms.GetLoyaltyCard(uProfile.NTID).Rows[0][1].ToString() == "" ? "" : GovForms.GetLoyaltyCard(uProfile.NTID).Rows[0][1].ToString().First().ToString().ToUpper() + GovForms.GetLoyaltyCard(uProfile.NTID).Rows[0][1].ToString().Substring(1);
                string lname = GovForms.GetLoyaltyCard(uProfile.NTID).Rows[0][2].ToString() == "" ? "" : GovForms.GetLoyaltyCard(uProfile.NTID).Rows[0][2].ToString().First().ToString().ToUpper() + GovForms.GetLoyaltyCard(uProfile.NTID).Rows[0][2].ToString().Substring(1);
                string fullname = lname + ", " + fname + " " + mname;

                item = fields.GetFieldItem("untitled169");
                item.GetMerged(0).Put(PdfName.Q, new PdfNumber(PdfFormField.Q_CENTER));

                fields.SetField("untitled169", (fname + " " + mname + " " + lname).ToUpper());
                item = fields.GetFieldItem("untitled171");
                item.GetMerged(0).Put(PdfName.Q, new PdfNumber(PdfFormField.Q_CENTER));

                fields.SetField("untitled171", DateTime.Now.ToString("MM/dd/yyyy"));
                //Date of Birth
                try
                {
                    string[] s = MPLdt.Rows[0][10].ToString().Split('-');

                    fields.SetFieldProperty("untitled21", "textsize", 15f, null);
                    fields.SetField("untitled21", s[2].Substring(0, 1) + "  " + s[2].Substring(1, 1));

                    fields.SetFieldProperty("untitled22", "textsize", 15f, null);
                    fields.SetField("untitled22", s[1].Substring(0, 1) + "  " + s[1].Substring(1, 1));

                    fields.SetFieldProperty("untitled23", "textsize", 15f, null);
                    fields.SetField("untitled23", s[0].Substring(0, 1) + "  " + s[0].Substring(1, 1) + " " + s[0].Substring(2, 1) + " " + s[0].Substring(3, 1));
                }
                catch
                {

                }
                string empidd = " ";
                for (int ei = 0; ei < uProfile.NTID.Length; ei++)
                {
                    empidd = empidd + uProfile.NTID.Substring(ei, 1) + "  ";
                }
                fields.SetFieldProperty("untitled39", "textsize", 15f, null);
                fields.SetField("untitled39", empidd);
                //contact details
                try
                {
                    for (int i = 0; i < GovForms.GetPersonalInfo(uProfile.NTID).Rows.Count; i++)
                    {
                        string numtype = GovForms.GetPersonalInfo(uProfile.NTID).Rows[i][1].ToString();
                        if (numtype == "Mobile")
                        {
                            fields.SetField("untitled71", GovForms.GetPersonalInfo(uProfile.NTID).Rows[i][0].ToString());
                        }
                        if (numtype == "Home")
                        {
                            fields.SetField("untitled69", GovForms.GetPersonalInfo(uProfile.NTID).Rows[i][0].ToString());
                        }
                    }
                }
                catch
                {

                }
                //Email address
                try
                {
                    fields.SetField("untitled77", GovForms.GetEmailInfo(uProfile.NTID).Rows[0][0].ToString());
                }
                catch
                {

                }
                //Employer Information
                try
                {
                    fields.SetField("untitled78", GovForms.getPagibigMPL(uProfile.NTID).Rows[0][38].ToString());
                    fields.SetField("untitled79", GovForms.getPagibigMPL(uProfile.NTID).Rows[0][32].ToString());
                    fields.SetField("untitled80", GovForms.getPagibigMPL(uProfile.NTID).Rows[0][27].ToString());
                    fields.SetField("untitled82", GovForms.getCompanyProfile(uProfile.NTID).Rows[0][8].ToString());
                    fields.SetField("untitled84", GovForms.getCompanyProfile(uProfile.NTID).Rows[0][9].ToString());
                    fields.SetField("untitled85", GovForms.getPagibigMPL(uProfile.NTID).Rows[0][28].ToString());
                    fields.SetField("untitled88", GovForms.getPagibigMPL(uProfile.NTID).Rows[0][33].ToString());
                    fields.SetField("untitled89", GovForms.getEmployeeMaster(uProfile.NTID).Rows[0][24].ToString());//Occupation
                    fields.SetField("untitled90", GovForms.getCompanyProfile(uProfile.NTID).Rows[0][67].ToString());
                }
                catch
                {

                }
                //Office Assignment
                try
                {
                    string officetype = GovForms.getCompanyProfile(uProfile.NTID).Rows[0][1].ToString();
                    if (officetype == "Head Quarters")
                    {
                        fields.SetField("untitled106", "X");

                    }
                    else if (officetype == "Branch")
                    {
                        fields.SetField("untitled107", "X");
                    }
                }
                catch
                {

                }
                //Date joined
                try
                {
                    string[] JoinDate = MPLdt.Rows[0][38].ToString().Split('-');
                    fields.SetField("untitled109", JoinDate[1].Substring(0, 1) + "  " + JoinDate[1].Substring(1, 1));
                    fields.SetField("untitled110", JoinDate[0].Substring(0, 1) + "   " + JoinDate[0].Substring(1, 1) + "   " + JoinDate[0].Substring(2, 1) + "   " + JoinDate[0].Substring(3, 1));
                }
                catch
                {

                }
                stamper.FormFlattening = true;
                stamper.Close();
                uProfile.Status = "Success";
            }
            catch (Exception ex)
            {
                uProfile.Status = ex.ToString();
            }
            return uProfile.Status;

        }
        #endregion
        #region "STLRF dipa tapos"

        #endregion
        #region "MCRF dipa tapos"

        #endregion
        #region "PFF039_MembersDataForm_V06"
        [HttpPost]
        [Route("PFF039_MembersDataForm_V06")]
        public string PFF039_MembersDataForm_V06(UserProfile uProfile)
        {
            string path = HttpContext.Current.Server.MapPath("pdf");
            var pdfReader1 = new PdfReader(path + "/GovForms Templates/PFF039_MembersDataForm_V06.pdf");
            AcroFields af = pdfReader1.AcroFields;

            string formFile = path + "/GovForms Templates/PFF039_MembersDataForm_V06.pdf";
            string newFile = path + "/GovForms-Employee/PFF039_MembersDataForm_V06-" + uProfile.NTID + ".pdf";

            PdfReader reader = new PdfReader(formFile);
            PdfStamper stamper = new PdfStamper(reader, new FileStream(newFile, FileMode.Create));

            AcroFields fields = stamper.AcroFields;
            AcroFields.Item item;
            DataTable MDFdt = new DataTable();
            DataTable MDFemp = new DataTable();
            string fullname = GovForms.GetSSSSalaryLoan(uProfile.NTID).Rows[0][1].ToString() + " " + GovForms.GetSSSSalaryLoan(uProfile.NTID).Rows[0][2].ToString() + " " + GovForms.GetSSSSalaryLoan(uProfile.NTID).Rows[0][0].ToString();
            try
            {
                con2.myparameters.Add(new myParameters { ParameterName = "@uProfile.NTID", mytype = SqlDbType.NVarChar, Value = uProfile.NTID });
                MDFdt = con2.GetDataTable("sp_SearchHRMSEmployeePersonalInformation");
                con2.myparameters.Clear();
                con2.myparameters.Add(new myParameters { ParameterName = "@uProfile.NTID", mytype = SqlDbType.NVarChar, Value = uProfile.NTID });
                MDFemp = con2.GetDataTable("sp_ShowCompanyDetails");
            }
            catch
            {

            }
            try
            {
                //Personal Details
                //Member
                try
                {
                    //last name
                    fields.SetField("untitled13", GovForms.GetSSSSalaryLoan(uProfile.NTID).Rows[0][0].ToString());
                    //first name
                    fields.SetField("untitled14", GovForms.GetSSSSalaryLoan(uProfile.NTID).Rows[0][1].ToString());
                    //middle name
                    fields.SetField("untitled16", GovForms.GetSSSSalaryLoan(uProfile.NTID).Rows[0][2].ToString());
                }
                catch { }
                //Father
                try
                {
                    string[] father = MDFdt.Rows[0][30].ToString().Split('/');
                    //last name
                    fields.SetField("untitled17", father[2]);
                    //first name
                    fields.SetField("untitled18", father[0]);
                    //middle name
                    fields.SetField("untitled20", father[1]);
                }
                catch { }
                //Mother
                try
                {
                    string[] mother = MDFdt.Rows[0][29].ToString().Split('/');
                    //last name
                    fields.SetField("untitled21", mother[2]);
                    //first name
                    fields.SetField("untitled22", mother[0]);
                    //middle name
                    fields.SetField("untitled24", mother[1]);
                }
                catch { }
                ////spouse
                //try
                //{
                //    string[] mother = MDFdt.Rows[0][29].ToString().Split('/');
                //    //last name
                //    fields.SetField("untitled21", mother[2]);
                //    //first name
                //    fields.SetField("untitled22", mother[0]);
                //    //middle name
                //    fields.SetField("untitled24", mother[1]);
                //}
                //catch { }
                //Member's Name
                try
                {
                    fields.SetField("untitled29", fullname);
                }

                catch { }

                //TIN 38-46
                try
                {
                    char[] TIN = getNumeric(GovForms.GetSSSSalaryLoan(uProfile.NTID).Rows[0][13].ToString()).ToCharArray();
                    for (int l = 0; l < TIN.Length; l++)
                    {
                        fields.SetField("untitled" + (l + 38), TIN[l].ToString());
                    }
                }
                catch { }
                //SSS
                try
                {
                    char[] SSS = getNumeric(GovForms.GetSSSSalaryLoan(uProfile.NTID).Rows[0][14].ToString()).ToCharArray();
                    for (int l = 0; l < SSS.Length; l++)
                    {
                        fields.SetField("untitled" + (l + 47), SSS[l].ToString());
                    }
                }
                catch { }
                //Employee ID
                try
                {
                    char[] TIN = uProfile.NTID.ToCharArray();
                    for (int l = 0; l < TIN.Length; l++)
                    {
                        fields.SetField("untitled" + (l + 58), TIN[l].ToString());
                    }
                }
                catch { }
                //Date  of Birth
                try
                {
                    string[] DOB = MDFdt.Rows[0][10].ToString().Split('-');
                    fields.SetField("untitled30", DOB[1].Substring(0, 1));
                    fields.SetField("untitled31", DOB[1].Substring(1, 1));

                    fields.SetField("untitled32", DOB[2].Substring(0, 1));
                    fields.SetField("untitled33", DOB[2].Substring(1, 1));

                    fields.SetField("untitled34", DOB[0].Substring(0, 1));
                    fields.SetField("untitled35", DOB[0].Substring(1, 1));
                    fields.SetField("untitled36", DOB[0].Substring(2, 1));
                    fields.SetField("untitled37", DOB[0].Substring(3, 1));
                }
                catch { }
                //Marital Status
                try
                {
                    string MStatus = MDFdt.Rows[0][9].ToString();
                    switch (MStatus)
                    {
                        case "Single":
                            fields.SetField("untitled252", "Yes", true);
                            break;
                        case "Married":
                            fields.SetField("untitled253", "Yes", true);
                            break;
                        case "Widowed":
                            fields.SetField("untitled254", "Yes", true);
                            break;
                        case "Legally Separated":
                            fields.SetField("untitled255", "Yes", true);
                            break;
                        case "Annulled":
                            fields.SetField("untitled256", "Yes", true);
                            break;
                        default:
                            break;
                    }
                }
                catch { }
                //Place of Birth
                try
                {
                    fields.SetField("untitled70", MDFdt.Rows[0][27].ToString());
                }
                catch { }
                //Citizenship
                try
                {
                    fields.SetField("untitled73", MDFdt.Rows[0][11].ToString());
                }
                catch { }
                //Sex
                try
                {
                    string sex = MDFdt.Rows[0][8].ToString();
                    switch (sex)
                    {
                        case "M":
                            fields.SetField("untitled257", "Yes", true);
                            break;
                        case "F":
                            fields.SetField("untitled258", "Yes", true);
                            break;
                        default:
                            break;
                    }
                }
                catch { }
                //height and weight
                try
                {
                    string ht = MDFdt.Rows[0][50].ToString();
                    string wt = MDFdt.Rows[0][51].ToString();

                    fields.SetField("untitled71", ht);
                    fields.SetField("untitled72", wt);
                }
                catch { }
                //CRN
                try
                {
                    char[] CRN = getNumeric(GovForms.GetSSSSalaryLoan(uProfile.NTID).Rows[0][14].ToString()).ToCharArray();
                    for (int l = 0; l < CRN.Length; l++)
                    {
                        fields.SetField("untitled" + (l + 75), CRN[l].ToString());
                    }
                }
                catch { }

                //Address and Contact Details
                //Permanent Home Address
                try
                {
                    //Lot No., Block No., House No., Phase No.
                    fields.SetField("untitled89", MDFdt.Rows[0][0].ToString());
                    //Street Name
                    fields.SetField("untitled90", MDFdt.Rows[0][1].ToString());
                    //Subdivision
                    fields.SetField("untitled91", MDFdt.Rows[0][3].ToString());
                    //Barangay
                    fields.SetField("untitled92", MDFdt.Rows[0][2].ToString());
                    //Municipality/City
                    fields.SetField("untitled93", MDFdt.Rows[0][4].ToString());
                    //Province
                    fields.SetField("untitled94", MDFdt.Rows[0][23].ToString());
                    //Zip Code
                    fields.SetField("untitled95", MDFdt.Rows[0][6].ToString());
                }
                catch { }

                //Present Home Address
                try
                {
                    //Lot No., Block No., House No., Phase No.
                    fields.SetField("untitled98", MDFdt.Rows[0][0].ToString());
                    //Street Name
                    fields.SetField("untitled99", MDFdt.Rows[0][1].ToString());
                    //Subdivision
                    fields.SetField("untitled100", MDFdt.Rows[0][3].ToString());
                    //Barangay
                    fields.SetField("untitled101", MDFdt.Rows[0][2].ToString());
                    //Municipality/City
                    fields.SetField("untitled102", MDFdt.Rows[0][4].ToString());
                    //Province
                    fields.SetField("untitled103", MDFdt.Rows[0][23].ToString());
                    //Zip Code
                    fields.SetField("untitled104", MDFdt.Rows[0][6].ToString());
                }
                catch { }

                //Contact Details
                try
                {
                    for (int p = 0; p < GovForms.GetPersonalInfo(uProfile.NTID).Rows.Count; p++)
                    {
                        string type = GovForms.GetPersonalInfo(uProfile.NTID).Rows[p][1].ToString();
                        string num = GovForms.GetPersonalInfo(uProfile.NTID).Rows[p][0].ToString();

                        if (type == "Home")
                        {
                            num = getNumeric(num);
                            if (num.Length == 10)
                            {
                                fields.SetField("untitled105", num.Substring(0, 3));
                                fields.SetField("untitled109", num.Substring(3, 7));
                            }
                            else
                            {
                                fields.SetField("untitled109", num);
                            }
                        }
                        else if (type == "Work")
                        {
                            num = getNumeric(num);
                            if (num.Length == 10)
                            {
                                fields.SetField("untitled107", num.Substring(0, 3));
                                fields.SetField("untitled111", num.Substring(3, 7));
                            }
                            else
                            {
                                fields.SetField("untitled110", num);
                            }
                        }
                        else if (type == "Mobile")
                        {
                            num = getNumeric(num);
                            if (num.Length == 11)
                            {
                                fields.SetField("untitled106", num.Substring(0, 4));
                                fields.SetField("untitled110", num.Substring(4, 7));
                            }
                            else
                            {
                                fields.SetField("untitled110", num);
                            }
                        }
                    }
                }
                catch { }
                //Present Employer Details
                //Occupation
                try
                {
                    con2.myparameters.Clear();
                    con2.myparameters.Add(new myParameters { ParameterName = "@uProfile.NTID", mytype = SqlDbType.NVarChar, Value = uProfile.NTID });
                    string occ = con2.GetDataTable("sp_GetEmployeeMaster").Rows[0][25].ToString();
                    fields.SetField("untitled115", occ);
                }
                catch { }
                //Employer Name
                try
                {
                    fields.SetField("untitled116", MDFemp.Rows[0][3].ToString());
                }
                catch { }
                //Employer Address
                try
                {
                    //Unit/Room No., Floor No. 117
                    fields.SetField("untitled117", MDFemp.Rows[0][6].ToString());
                    //Building Name 118
                    fields.SetField("untitled118", MDFemp.Rows[0][7].ToString());
                    //Lot No., Block No., Phase No. 119
                    //Street Name 120
                    fields.SetField("untitled120", MDFemp.Rows[0][8].ToString());
                    //Subdivision 121
                    //Barangay 122
                    fields.SetField("untitled122", MDFemp.Rows[0][9].ToString());
                    //Municipality/City 123
                    fields.SetField("untitled123", MDFemp.Rows[0][10].ToString());
                    //Province 124
                    fields.SetField("untitled124", MDFemp.Rows[0][11].ToString());
                    //State/Country 125
                    //Zip Code 126
                    fields.SetField("untitled126", MDFemp.Rows[0][14].ToString());
                }
                catch { }
                //Monthly Income
                try
                {
                    string basic = GovForms.PaySlipRecordsEmployeePay(uProfile.NTID).Rows[0][2].ToString();
                    string allowance = GovForms.DeminimisAmount(uProfile.NTID, "2017-09-01", "2017-09-15").ToString();
                    //Basic Salary
                    fields.SetField("untitled127", basic);
                    //Allowances
                    fields.SetField("untitled128", allowance);
                    //total
                    fields.SetField("untitled129", (Math.Round((float.Parse(basic) + float.Parse(allowance)), 2)).ToString());
                }
                catch { }
                //Office Assignment
                try
                {
                    if (MDFemp.Rows[0][1].ToString() == "Head Quarters")
                    {
                        fields.SetField("untitled276", "Yes", true);
                    }
                    else
                    {
                        fields.SetField("untitled277", "Yes", true);
                        fields.SetField("untitled230", MDFemp.Rows[0][2].ToString());
                    }
                }
                catch { }
                //Date Employed
                try
                {
                    DateTime datejoined = Convert.ToDateTime(MDFdt.Rows[0][38].ToString());
                    fields.SetField("untitled130", datejoined.ToString("MMMM, yyyy"));
                }
                catch { }

                //Signature
                try
                {
                    AcroFields.FieldPosition wew = fields.GetFieldPositions("untitled228")[0];
                    iTextSharp.text.Rectangle rect = wew.position;
                    string xxxx = rect.Left.ToString();
                    string yyyy = rect.Bottom.ToString();

                    string BitEmpsign = null;
                    Byte[] EmpsignBit = null;
                    iTextSharp.text.Image Empsign = null;
                    //iTextSharp.text.Image Empsign4 = null;
                    var EmpsignContentByte = stamper.GetOverContent(2);
                    GovForms.GetSignatureSignPicture(uProfile.NTID, "sign_0");
                    BitEmpsign = GovForms.BitSign;

                    if (!String.IsNullOrWhiteSpace(BitEmpsign))
                    {
                        BitEmpsign = BitEmpsign.Replace(' ', '+');
                        EmpsignBit = Convert.FromBase64String(BitEmpsign);
                        Empsign = iTextSharp.text.Image.GetInstance(EmpsignBit);
                        Empsign.SetAbsolutePosition(float.Parse(xxxx), float.Parse(yyyy));
                        Empsign.ScaleToFit(50, 50);
                        EmpsignContentByte.AddImage(Empsign);
                    }

                    item = fields.GetFieldItem("untitled229");
                    item.GetMerged(0).Put(PdfName.Q, new PdfNumber(PdfFormField.Q_CENTER));

                    fields.SetField("untitled229", DateTime.Now.ToString("MM/dd/yyyy"));
                }
                catch { }
                stamper.FormFlattening = true;
                stamper.Close();
                uProfile.Status = "Success";
            }
            catch (Exception ex)
            {
                uProfile.Status = ex.ToString();
            }

            return uProfile.Status;

        }
        #endregion
        #region "Pagibig_MultiPurposeLoanApplication_V03"
        [HttpPost]
        [Route("Pagibig_MultiPurposeLoanApplication_V03")]
        public string Pagibig_MultiPurposeLoanApplication_V03(UserProfile uProfile)
        {
            string path = HttpContext.Current.Server.MapPath("pdf");
            var pdfReader1 = new PdfReader(path + "/GovForms Templates/Pagibig_MultiPurposeLoanApplication_V03.pdf");
            AcroFields af = pdfReader1.AcroFields;
            string formFile = path + "/GovForms Templates/Pagibig_MultiPurposeLoanApplication_V03.pdf";
            string newFile = path + "/GovForms-Employee/Pagibig_MultiPurposeLoanApplication_V03-" + uProfile.NTID + ".pdf";

            PdfReader reader = new PdfReader(formFile);
            PdfStamper stamper = new PdfStamper(reader, new FileStream(newFile, FileMode.Create));

            AcroFields fields = stamper.AcroFields;
            try
            {
                string loanamt = "";
                GovForms.getEmployeePersonalInformation(uProfile.NTID);
                string fullname = GovForms.getPagibigMPL(uProfile.NTID).Rows[0][4].ToString().ToUpper() + " " + GovForms.getPagibigMPL(uProfile.NTID).Rows[0][6].ToString().ToUpper() + " " + GovForms.getPagibigMPL(uProfile.NTID).Rows[0][5].ToString().ToUpper();
                fields.SetField("untitled1", GovForms.getPagibigMPL(uProfile.NTID).Rows[0][0].ToString());
                fields.SetField("untitled3", GovForms.getPagibigMPL(uProfile.NTID).Rows[0][5].ToString());
                fields.SetField("untitled4", GovForms.getPagibigMPL(uProfile.NTID).Rows[0][4].ToString());
                fields.SetField("untitled6", GovForms.getPagibigMPL(uProfile.NTID).Rows[0][6].ToString());
                fields.SetField("untitled8", GovForms.getPagibigMPL(uProfile.NTID).Rows[0][11].ToString());
                try
                {
                    for (int i = 0; i < GovForms.getDependent(uProfile.NTID).Rows.Count; i++)
                    {

                        if (GovForms.getDependent(uProfile.NTID).Rows[i][2].ToString() == "Mother")
                        {
                            string firstname = GovForms.getDependent(uProfile.NTID).Rows[i][3].ToString();
                            string midname = GovForms.getDependent(uProfile.NTID).Rows[i][4].ToString();
                            string lastname = GovForms.getDependent(uProfile.NTID).Rows[i][5].ToString();
                            fields.SetField("untitled10", firstname + " " + midname + " " + lastname);
                        }
                    }
                }
                catch
                {
                }
                fields.SetField("untitled11", GovForms.getPagibigMPL(uProfile.NTID).Rows[0][8].ToString());
                string gen;
                gen = GovForms.getPagibigMPL(uProfile.NTID).Rows[0][12].ToString();
                if ((gen == "M") || (gen == "Male"))
                {
                    fields.SetField("SexMale", "Yes", true);
                }
                else
                {
                    fields.SetField("SexFemale", "Yes", true);
                }
                try
                {
                    fields.SetField("untitled19", GovForms.getPagibigMPL(uProfile.NTID).Rows[0][8].ToString());
                    fields.SetField("untitled20", GovForms.GetEmailInfo(uProfile.NTID).Rows[0][0].ToString());
                    fields.SetField("untitled26", GovForms.getPagibigMPL(uProfile.NTID).Rows[0][15].ToString());
                    fields.SetField("untitled27", GovForms.getPagibigMPL(uProfile.NTID).Rows[0][21].ToString());
                    fields.SetField("untitled31", GovForms.getPagibigMPL(uProfile.NTID).Rows[0][7].ToString());
                    fields.SetField("untitled32", GovForms.getPagibigMPL(uProfile.NTID).Rows[0][9].ToString());
                    fields.SetField("untitled34", GovForms.getPagibigMPL(uProfile.NTID).Rows[0][36].ToString());
                    fields.SetField("untitled33", GovForms.provincea.ToString());
                    fields.SetField("untitled42", GovForms.getPagibigMPL(uProfile.NTID).Rows[0][15].ToString());
                    fields.SetField("untitled43", GovForms.getPagibigMPL(uProfile.NTID).Rows[0][21].ToString());
                    fields.SetField("untitled139", GovForms.getPagibigMPL(uProfile.NTID).Rows[0][7].ToString());
                    fields.SetField("untitled47", GovForms.getPagibigMPL(uProfile.NTID).Rows[0][9].ToString());
                    fields.SetField("untitled49", GovForms.getPagibigMPL(uProfile.NTID).Rows[0][36].ToString());
                    fields.SetField("untitled48", GovForms.provincea.ToString());
                }
                catch { }
                try
                {
                    for (int i = 0; i < GovForms.GetPersonalInfo(uProfile.NTID).Rows.Count; i++)
                    {
                        string numtype = GovForms.GetPersonalInfo(uProfile.NTID).Rows[i][1].ToString();
                        if (numtype == "Mobile")
                        {
                            fields.SetField("untitled28", GovForms.GetPersonalInfo(uProfile.NTID).Rows[i][0].ToString());
                        }
                        if (numtype == "Home")
                        {
                            fields.SetField("untitled29", GovForms.GetPersonalInfo(uProfile.NTID).Rows[i][0].ToString());
                        }
                    }
                }
                catch
                {
                }
                fields.SetField("untitled35", GovForms.getPagibigMPL(uProfile.NTID).Rows[0][2].ToString());
                fields.SetField("untitled36", GovForms.getPagibigMPL(uProfile.NTID).Rows[0][1].ToString());
                string empbranchid = GovForms.getCompanyProfile(uProfile.NTID).Rows[0][0].ToString();
                string compphone = GovForms.GetCompanyContactPhone(empbranchid).Rows[0][1].ToString();
                if (compphone.Length == 10)
                {
                    compphone = compphone.Insert(0, "(");
                    compphone = compphone.Insert(4, ")");
                    compphone = compphone.Insert(5, " ");
                    compphone = compphone.Insert(9, "-");
                }
                try
                {
                    fields.SetField("untitled44", compphone);
                    fields.SetField("untitled45", GovForms.getPagibigMPL(uProfile.NTID).Rows[0][24].ToString());
                    fields.SetField("untitled50", GovForms.getPagibigMPL(uProfile.NTID).Rows[0][25].ToString());
                    fields.SetField("untitled51", GovForms.getPagibigMPL(uProfile.NTID).Rows[0][3].ToString());
                    fields.SetField("untitled52", GovForms.getPagibigMPL(uProfile.NTID).Rows[0][38].ToString());
                    fields.SetField("untitled58", GovForms.getPagibigMPL(uProfile.NTID).Rows[0][32].ToString());
                    fields.SetField("untitled60", GovForms.getPagibigMPL(uProfile.NTID).Rows[0][27].ToString());
                    fields.SetField("untitled63", GovForms.getCompanyProfile(uProfile.NTID).Rows[0][8].ToString());
                    fields.SetField("untitled65", GovForms.getPagibigMPL(uProfile.NTID).Rows[0][30].ToString());
                    fields.SetField("untitled66", GovForms.getCompanyProfile(uProfile.NTID).Rows[0][11].ToString());
                    fields.SetField("untitled67", GovForms.getPagibigMPL(uProfile.NTID).Rows[0][33].ToString());
                    fields.SetField("untitled93", GovForms.getPagibigMPL(uProfile.NTID).Rows[0][35].ToString());
                }
                catch { }
                try
                {
                    if (loanamt == "60%")
                    {
                        fields.SetField("amt60", "Yes", true);
                    }
                    else if (loanamt == "80%")
                    {
                        fields.SetField("amt80", "Yes", true);
                    }
                    else if (loanamt == "70%")
                    {
                        fields.SetField("amt70", "Yes", true);
                    }
                }
                catch { }
                try
                {
                    fields.SetField("untitled95", GovForms.getPagibigMPL(uProfile.NTID).Rows[0][46].ToString());
                    fields.SetField("untitled91", fullname);
                    fields.SetField("untitled102", fullname);
                    fields.SetField("untitled137", fullname);
                    fields.SetField("untitled105", fullname);
                    fields.SetField("untitled106", (DateTime.Now.AddMonths(-1)).ToString("MMMM"));
                }
                catch { }
                string bscsal = "0";


                double hdmf = 0;
                double phic = 0;
                double sss = 0;
                double tax = 0;
                if (GovForms.getPaySlipDetailsAllbyMonth(uProfile.NTID, (DateTime.Now.AddMonths(-1)).Month.ToString(), DateTime.Now.Year.ToString()).Rows.Count > 0)
                {
                    bscsal = GovForms.getPaySlipDetailsAllbyMonth(uProfile.NTID, (DateTime.Now.AddMonths(-1)).Month.ToString(), DateTime.Now.Year.ToString()).Rows[0][20].ToString();

                    hdmf = Convert.ToDouble(GovForms.getPaySlipDetailsAllbyMonth(uProfile.NTID, (DateTime.Now.AddMonths(-1)).Month.ToString(), DateTime.Now.Year.ToString()).Rows[0][10].ToString());
                    phic = Convert.ToDouble(GovForms.getPaySlipDetailsAllbyMonth(uProfile.NTID, (DateTime.Now.AddMonths(-1)).Month.ToString(), DateTime.Now.Year.ToString()).Rows[0][11].ToString());
                    sss = Convert.ToDouble(GovForms.getPaySlipDetailsAllbyMonth(uProfile.NTID, (DateTime.Now.AddMonths(-1)).Month.ToString(), DateTime.Now.Year.ToString()).Rows[0][12].ToString());
                    tax = Convert.ToDouble(GovForms.getPaySlipDetailsAllbyMonth(uProfile.NTID, (DateTime.Now.AddMonths(-1)).Month.ToString(), DateTime.Now.Year.ToString()).Rows[0][13].ToString());
                }
                fields.SetField("untitled107", bscsal);
                fields.SetField("untitled118", bscsal);
                fields.SetField("untitled119", "HDMF");
                fields.SetField("untitled120", hdmf.ToString());
                fields.SetField("untitled121", "Philhealth");
                fields.SetField("untitled122", phic.ToString());
                fields.SetField("untitled123", "SSS");
                fields.SetField("untitled124", sss.ToString());
                fields.SetField("untitled125", "Tax");
                fields.SetField("untitled126", tax.ToString());
                double Grded = hdmf + phic + sss + tax;
                fields.SetField("untitled129", Grded.ToString());
                double netic = Convert.ToDouble(bscsal) - Grded;
                fields.SetField("untitled130", netic.ToString());
                fields.SetField("untitled131", DateTime.Now.ToString("dd"));
                fields.SetField("untitled132", DateTime.Now.ToString("MMMM"));
                fields.SetField("untitled133", DateTime.Now.ToString("yy"));
                GovForms.GetRequestFormSettings();
                GovForms.SignatoryNew(uProfile.NTID, formtype);
                GovForms.GetAuthorizedRepresentative(uProfile.NTID);
                GovForms.GetAppEMP(94, uProfile.NTID);
                string approver = "";
                approver = GovForms.ARName;
                if (string.IsNullOrWhiteSpace(approver))
                {
                }
                else
                {
                    string Approvername = GovForms.ApproverName;
                    string ApproverDesc = GovForms.ApproverJobDesc;
                    string Bitsignature = null;
                    Bitsignature = GovForms.ARSignature;
                    Byte[] SignatureBit = null;
                    iTextSharp.text.Image signatory = null;
                    iTextSharp.text.Image signatory1 = null;
                    var pdfContentByte = stamper.GetOverContent(1);
                    if (string.IsNullOrWhiteSpace(Bitsignature) == false)
                    {
                        Bitsignature = Bitsignature.Replace(' ', '+');
                        SignatureBit = Convert.FromBase64String(Bitsignature);
                        signatory = iTextSharp.text.Image.GetInstance(SignatureBit);
                        signatory1 = iTextSharp.text.Image.GetInstance(SignatureBit);
                        signatory.SetAbsolutePosition(430, 465);
                        signatory.ScaleToFit(100, 100);
                        signatory1.SetAbsolutePosition(90, 215);
                        signatory1.ScaleToFit(100, 100);
                        fields.SetField("untitled88", GovForms.ARName);
                        fields.SetField("untitled90", GovForms.ARDescription);
                        fields.SetField("untitled134", GovForms.ARName);
                        pdfContentByte.AddImage(signatory);
                        pdfContentByte = stamper.GetOverContent(2);
                        pdfContentByte.AddImage(signatory1);
                    }
                }

                stamper.FormFlattening = true;
                stamper.Close();
                uProfile.Status = "Success";
            }
            catch (Exception ex)
            {
                uProfile.Status = ex.ToString();
            }

            return uProfile.Status;

        }
        #endregion
        #region "Pagibig_SLF002_CalamityLoanApplication_V05"
        [HttpPost]
        [Route("Pagibig_SLF002_CalamityLoanApplication_V05")]
        public string Pagibig_SLF002_CalamityLoanApplication_V05(UserProfile uProfile)
        {
            string path = HttpContext.Current.Server.MapPath("pdf");
            var pdfReader1 = new PdfReader(path + "/GovForms Templates/Pagibig_SLF002_CalamityLoanApplication_V05.pdf");
            AcroFields af = pdfReader1.AcroFields;
            AcroFields.Item item;
            string formFile = path + "/GovForms Templates/Pagibig_SLF002_CalamityLoanApplication_V05.pdf";
            string newFile = path + "/GovForms-Employee/Pagibig_SLF002_CalamityLoanApplication_V05-" + uProfile.NTID + ".pdf";

            PdfReader reader = new PdfReader(formFile);

            PdfStamper stamper = new PdfStamper(reader, new FileStream(newFile, FileMode.Create));
            AcroFields fields = stamper.AcroFields;
            try
            {
                DataTable CLAdt = new DataTable();
                try
                {
                    con2.myparameters.Clear();
                    con2.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = uProfile.NTID });
                    con2.GetDataTable("sp_SearchHRMSEmployeePersonalInformation");
                }
                catch { }
                GovForms.getEmployeePersonalInformation(uProfile.NTID);
                //Name
                try
                {
                    string lname = GovForms.GetCalamityLoan(uProfile.NTID).Rows[0][0].ToString() == "" ? "" : GovForms.GetCalamityLoan(uProfile.NTID).Rows[0][0].ToString().First().ToString().ToUpper() + GovForms.GetCalamityLoan(uProfile.NTID).Rows[0][0].ToString().Substring(1);
                    string fname = GovForms.GetCalamityLoan(uProfile.NTID).Rows[0][1].ToString() == "" ? "" : GovForms.GetCalamityLoan(uProfile.NTID).Rows[0][1].ToString().First().ToString().ToUpper() + GovForms.GetCalamityLoan(uProfile.NTID).Rows[0][1].ToString().Substring(1);
                    string mname = GovForms.GetCalamityLoan(uProfile.NTID).Rows[0][2].ToString() == "" ? "" : GovForms.GetCalamityLoan(uProfile.NTID).Rows[0][2].ToString().First().ToString().ToUpper() + GovForms.GetCalamityLoan(uProfile.NTID).Rows[0][2].ToString().Substring(1);
                    fields.SetField("untitled3", lname);
                    fields.SetField("untitled4", fname);
                    fields.SetField("untitled7", mname);

                    item = fields.GetFieldItem("untitled84");
                    item.GetMerged(0).Put(PdfName.Q, new PdfNumber(PdfFormField.Q_CENTER));
                    fields.SetField("untitled84", lname.ToUpper() + ", " + fname.ToUpper() + " " + mname.ToUpper());
                    item = fields.GetFieldItem("untitled97");
                    item.GetMerged(0).Put(PdfName.Q, new PdfNumber(PdfFormField.Q_CENTER));
                    fields.SetField("untitled97", lname.ToUpper() + ", " + fname.ToUpper() + " " + mname.ToUpper());
                    item = fields.GetFieldItem("untitled102");
                    item.GetMerged(0).Put(PdfName.Q, new PdfNumber(PdfFormField.Q_CENTER));
                    fields.SetField("untitled102", lname.ToUpper() + ", " + fname.ToUpper() + " " + mname.ToUpper());
                }
                catch { }
                fields.SetField("untitled21", GovForms.GetCalamityLoan(uProfile.NTID).Rows[0][3].ToString() == "" ? "" : GovForms.GetCalamityLoan(uProfile.NTID).Rows[0][3].ToString().First().ToString().ToUpper() + GovForms.GetCalamityLoan(uProfile.NTID).Rows[0][3].ToString().Substring(1));
                //Mobile Number
                try
                {
                    string mobnum = "";
                    for (int v = 0; v < GovForms.GetPersonalInfo(uProfile.NTID).Rows.Count; v++)
                    {
                        mobnum = GovForms.GetPersonalInfo(uProfile.NTID).Rows[0][1].ToString() == "Mobile" ? GovForms.GetPersonalInfo(uProfile.NTID).Rows[0][0].ToString() : "";
                    }

                    mobnum = mobnum.Replace("+63", "").Replace("+", "").Replace("-", "");
                    if (mobnum.Length == 11)
                    {
                        bool mobnume = Regex.IsMatch(mobnum, @"^\d+$");// determine if string is pure numbers; return true & null value
                        if (mobnume.ToString() == "True")
                        {
                            fields.SetField("untitled27", GovForms.GetCalamityLoan(uProfile.NTID).Rows[0][4].ToString());
                        }

                    }
                }
                catch { }
                //Gender
                try
                {
                    if (GovForms.GetCalamityLoan(uProfile.NTID).Rows[0][5].ToString() == "M")
                    {
                        fields.SetField("untitled13", "x");
                    }
                    else if (GovForms.GetCalamityLoan(uProfile.NTID).Rows[0][5].ToString() == "F")
                    {
                        fields.SetField("untitled14", "x");
                    }
                }
                catch { }
                //Address
                try
                {
                    fields.SetField("untitled12", GovForms.GetCalamityLoan(uProfile.NTID).Rows[0][6].ToString() == "" ? "" : GovForms.GetCalamityLoan(uProfile.NTID).Rows[0][6].ToString().First().ToString().ToUpper() + GovForms.GetCalamityLoan(uProfile.NTID).Rows[0][6].ToString().Substring(1));
                    fields.SetField("untitled20", GovForms.GetCalamityLoan(uProfile.NTID).Rows[0][6].ToString() == "" ? "" : GovForms.GetCalamityLoan(uProfile.NTID).Rows[0][6].ToString().First().ToString().ToUpper() + GovForms.GetCalamityLoan(uProfile.NTID).Rows[0][6].ToString().Substring(1));
                    fields.SetField("untitled9", GovForms.GetCalamityLoan(uProfile.NTID).Rows[0][7].ToString() == "" ? "" : GovForms.GetCalamityLoan(uProfile.NTID).Rows[0][7].ToString().First().ToString().ToUpper() + GovForms.GetCalamityLoan(uProfile.NTID).Rows[0][7].ToString().Substring(1));
                    fields.SetField("untitled25", GovForms.GetCalamityLoan(uProfile.NTID).Rows[0][8].ToString() == "" ? "" : GovForms.GetCalamityLoan(uProfile.NTID).Rows[0][8].ToString().First().ToString().ToUpper() + GovForms.GetCalamityLoan(uProfile.NTID).Rows[0][8].ToString().Substring(1));
                    fields.SetField("untitled33", GovForms.GetCalamityLoan(uProfile.NTID).Rows[0][9].ToString() == "" ? "" : GovForms.GetCalamityLoan(uProfile.NTID).Rows[0][9].ToString().First().ToString().ToUpper() + GovForms.GetCalamityLoan(uProfile.NTID).Rows[0][9].ToString().Substring(1));
                    fields.SetField("untitled26", GovForms.GetCalamityLoan(uProfile.NTID).Rows[0][10].ToString() == "" ? "" : GovForms.GetCalamityLoan(uProfile.NTID).Rows[0][10].ToString().First().ToString().ToUpper() + GovForms.GetCalamityLoan(uProfile.NTID).Rows[0][10].ToString().Substring(1));
                    fields.SetField("untitled30", GovForms.GetCalamityLoan(uProfile.NTID).Rows[0][11].ToString() == "" ? "" : GovForms.GetCalamityLoan(uProfile.NTID).Rows[0][11].ToString().First().ToString().ToUpper() + GovForms.GetCalamityLoan(uProfile.NTID).Rows[0][11].ToString().Substring(1));
                    fields.SetField("untitled31", GovForms.GetCalamityLoan(uProfile.NTID).Rows[0][12].ToString() == "" ? "" : GovForms.GetCalamityLoan(uProfile.NTID).Rows[0][12].ToString().First().ToString().ToUpper() + GovForms.GetCalamityLoan(uProfile.NTID).Rows[0][12].ToString().Substring(1));
                    fields.SetField("untitled32", GovForms.provincea.ToString() == "" ? "" : GovForms.provincea.ToString().First().ToString().ToUpper() + GovForms.provincea.ToString().Substring(1));

                    //Permanent
                    fields.SetField("untitled39", GovForms.GetCalamityLoan(uProfile.NTID).Rows[0][8].ToString() == "" ? "" : GovForms.GetCalamityLoan(uProfile.NTID).Rows[0][8].ToString().First().ToString().ToUpper() + GovForms.GetCalamityLoan(uProfile.NTID).Rows[0][8].ToString().Substring(1));
                    fields.SetField("untitled47", GovForms.GetCalamityLoan(uProfile.NTID).Rows[0][9].ToString() == "" ? "" : GovForms.GetCalamityLoan(uProfile.NTID).Rows[0][9].ToString().First().ToString().ToUpper() + GovForms.GetCalamityLoan(uProfile.NTID).Rows[0][9].ToString().Substring(1));
                    fields.SetField("untitled40", GovForms.GetCalamityLoan(uProfile.NTID).Rows[0][10].ToString() == "" ? "" : GovForms.GetCalamityLoan(uProfile.NTID).Rows[0][10].ToString().First().ToString().ToUpper() + GovForms.GetCalamityLoan(uProfile.NTID).Rows[0][10].ToString().Substring(1));
                    fields.SetField("untitled44", GovForms.GetCalamityLoan(uProfile.NTID).Rows[0][11].ToString() == "" ? "" : GovForms.GetCalamityLoan(uProfile.NTID).Rows[0][11].ToString().First().ToString().ToUpper() + GovForms.GetCalamityLoan(uProfile.NTID).Rows[0][11].ToString().Substring(1));
                    fields.SetField("untitled45", GovForms.GetCalamityLoan(uProfile.NTID).Rows[0][12].ToString() == "" ? "" : GovForms.GetCalamityLoan(uProfile.NTID).Rows[0][12].ToString().First().ToString().ToUpper() + GovForms.GetCalamityLoan(uProfile.NTID).Rows[0][12].ToString().Substring(1));
                    fields.SetField("untitled46", GovForms.provincea.ToString() == "" ? "" : GovForms.provincea.ToString().First().ToString().ToUpper() + GovForms.provincea.ToString().Substring(1));
                }
                catch { }

                //Zip code
                try
                {
                    string zipcode = GovForms.GetCalamityLoan(uProfile.NTID).Rows[0][13].ToString();
                    if (zipcode.Length == 4)
                    {
                        bool zipcodee = Regex.IsMatch(zipcode, @"^\d+$");// determine if string is pure numbers; return true & null value
                        if (zipcodee.ToString() == "True")
                        {
                            fields.SetField("untitle47", GovForms.GetCalamityLoan(uProfile.NTID).Rows[0][13].ToString());
                            fields.SetField("untitle33", GovForms.GetCalamityLoan(uProfile.NTID).Rows[0][13].ToString());
                        }
                    }
                }
                catch { }
                //TIN
                try
                {
                    string tin = GovForms.GetCalamityLoan(uProfile.NTID).Rows[0][15].ToString().Replace("-", "");
                    if (tin.Length == 9)
                    {
                        bool tine = Regex.IsMatch(tin, @"^\d+$");// determine if string is pure numbers; return true & null value
                        if (tine.ToString() == "True")
                        {
                            fields.SetField("untitled34", GovForms.GetCalamityLoan(uProfile.NTID).Rows[0][15].ToString());
                        }
                    }
                }
                catch { }
                //Pagibig
                try
                {
                    string pagibig = GovForms.GetCalamityLoan(uProfile.NTID).Rows[0][16].ToString().Replace("-", "");

                    bool pagibige = Regex.IsMatch(pagibig, @"^\d+$");// determine if string is pure numbers; return true & null value
                    if (pagibige.ToString() == "True")
                    {
                        fields.SetField("untitled1", GovForms.GetCalamityLoan(uProfile.NTID).Rows[0][16].ToString());
                    }

                }
                catch { }
                //SSS
                try
                {
                    string SSS = GovForms.GetCalamityLoan(uProfile.NTID).Rows[0][17].ToString().Replace("-", "");
                    if (SSS.Length == 10)
                    {
                        bool SSSe = Regex.IsMatch(SSS, @"^\d+$");// determine if string is pure numbers; return true & null value
                        if (SSSe.ToString() == "True")
                        {
                            fields.SetField("untitled35", GovForms.GetCalamityLoan(uProfile.NTID).Rows[0][17].ToString());
                        }
                    }
                }
                catch { }
                //Employer Information
                try
                {
                    fields.SetField("untitled49", GovForms.GetCalamityLoan(uProfile.NTID).Rows[0][18].ToString());
                    fields.SetField("untitled48", GovForms.GetCalamityLoan(uProfile.NTID).Rows[0][19].ToString()); // Employee ID
                    fields.SetField("untitled50", GovForms.getPagibigMPL(uProfile.NTID).Rows[0][38].ToString());
                }
                catch { }
                //Employer Address
                try
                {
                    fields.SetField("untitled52", GovForms.getPagibigMPL(uProfile.NTID).Rows[0][33].ToString());//unit/floor
                    fields.SetField("untitled53", GovForms.getPagibigMPL(uProfile.NTID).Rows[0][27].ToString());//bldg name
                    fields.SetField("untitled56", GovForms.getPagibigMPL(uProfile.NTID).Rows[0][44].ToString());//street
                    fields.SetField("untitled58", GovForms.getPagibigMPL(uProfile.NTID).Rows[0][26].ToString());//barangay
                    fields.SetField("untitled59", GovForms.getPagibigMPL(uProfile.NTID).Rows[0][30].ToString());//municipality
                    fields.SetField("untitled61", GovForms.getPagibigMPL(uProfile.NTID).Rows[0][33].ToString());//zip
                }
                catch { }
                //Other Employer Details
                try
                {
                    fields.SetField("untitled41", GovForms.GetCompanyContactPhone(GovForms.getCompanyProfile(uProfile.NTID).Rows[0][0].ToString()).Rows[1].ToString());//tel num
                    fields.SetField("untitled42", GovForms.getEmployeeMaster(uProfile.NTID).Rows[0][24].ToString());//Nature of Work
                    fields.SetField("untitled86", GovForms.getPagibigMPL(uProfile.NTID).Rows[0][35].ToString());//pagibig id employer
                }
                catch { }

                GovForms.GetRequestFormSettings();
                GovForms.Signatory(uProfile.NTID);
                GovForms.GetAppEMP(95, uProfile.NTID);
                string empidApprover = "";
                empidApprover = GovForms.ApproverEMPID;
                if (empidApprover == "" || empidApprover == null || string.IsNullOrWhiteSpace(empidApprover))
                {
                }
                else
                {
                    GovForms.GetApproverNameJobDes(empidApprover);
                    string Approvername = GovForms.ApproverName;
                    string ApproverDesc = GovForms.ApproverJobDesc;
                    GovForms.GetSignatureSignPicture(empidApprover, "sign_1");
                    string Bitsignature = null;
                    Bitsignature = GovForms.BitSign;
                    Byte[] SignatureBit = null;
                    iTextSharp.text.Image signatory = null;
                    var pdfContentByte = stamper.GetOverContent(1);
                    if (string.IsNullOrWhiteSpace(Bitsignature) == false)
                    {
                        Bitsignature = Bitsignature.Replace(' ', '+');
                        SignatureBit = Convert.FromBase64String(Bitsignature);
                        signatory = iTextSharp.text.Image.GetInstance(SignatureBit);
                        signatory.SetAbsolutePosition(430, 455);
                        signatory.ScaleToFit(100, 100);
                        fields.SetField("untitled82", Approvername);
                        fields.SetField("untitled83", ApproverDesc);
                        pdfContentByte.AddImage(signatory);
                    }
                }

                stamper.FormFlattening = true;
                stamper.Close();
                uProfile.Status = "Success";

            }
            catch (Exception ex)
            {
                uProfile.Status = ex.ToString();
            }
            return uProfile.Status;

        }
        #endregion 
        #region "MSRF dipa tapos"

        #endregion
        #region "1601C dipa tapos"
        [HttpPost]
        [Route("1601C")]
        public string generate1601C(form1601C prof)
        {
            try
            {
                string path = System.Web.Hosting.HostingEnvironment.MapPath("~/pdf");
                //Response.Redirect("~/pdf/BIR19021.pdf"); // view pdf
                var pdfReader1 = new PdfReader(path + "/GovForms Templates/1601C.pdf");
                AcroFields af = pdfReader1.AcroFields;


                // Template file path
                string formFile = path + "/GovForms Templates/1601C.pdf";

                // Output file path
                string newFile = path + "/1601C-" + prof.empid + ".pdf";


                // read the template file
                PdfReader reader = new PdfReader(formFile);


                // instantiate PDFStamper object
                // The Output file will be created from template file and edited by the PDFStamper
                PdfStamper stamper = new PdfStamper(reader, new FileStream(
                            newFile, FileMode.Create));


                // Object to deal with the Output file's textfields
                AcroFields fields = stamper.AcroFields;



                //
                string housenuma, maritala, streetnamea, barangaya, towna, citya, provincea = "", religiona, zipcodea, bloodtypea, gendera, maritalstata,
                doba, citizenshipa, nameofa, mobileareacodea, mobilenumbera, homeeracodea, homenumbera, personalemaila, emrgnamea, emrgnumbera, emrgrelationshipa,
                optionaemaila, contacta, pob, religion, mothername, fathername, payrollinfo, bankname, branchname, accountnumber, height, weight, FNAMEEMRG, MNAMEEMRG, LNAMEEMRG, MDATE,
                wemail, DateJoined, ProductionStartDate, Wallet, Tenure, TenureMonths, Batch, Class, Skill, JobDesc, Promotion, NameOfOrganization,
                extension;
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = prof.empid });
                DataTable dt = con.GetDataTable("sp_SearchHRMSEmployeePersonalInformation");
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    housenuma = dt.Rows[i]["HouseNumber"].ToString();
                    streetnamea = dt.Rows[i]["StreetName"].ToString();
                    barangaya = dt.Rows[i]["Barangay"].ToString();
                    towna = dt.Rows[i]["Town"].ToString();
                    citya = dt.Rows[i]["City"].ToString();
                    religiona = dt.Rows[i]["Region"].ToString();
                    zipcodea = dt.Rows[i]["ZipCode"].ToString();
                    bloodtypea = dt.Rows[i]["BloodType"].ToString();
                    gendera = dt.Rows[i]["Gender"].ToString();
                    maritalstata = dt.Rows[i]["MaritalStatus"].ToString();
                    if (string.IsNullOrWhiteSpace(dt.Rows[i]["DOB"].ToString()) == true)
                    {
                        doba = "";
                    }
                    else
                    {
                        doba = Convert.ToDateTime(dt.Rows[i]["DOB"]).ToString("yyyy-MM-dd");
                    }

                    citizenshipa = dt.Rows[i]["Citizenship"].ToString();
                    nameofa = dt.Rows[i]["NameOfOrganization"].ToString();
                    mobileareacodea = dt.Rows[i]["MobileAreaCode"].ToString();
                    mobilenumbera = dt.Rows[i]["MobileNumber"].ToString();
                    homeeracodea = dt.Rows[i]["HomeAreaCode"].ToString();
                    homenumbera = dt.Rows[i]["HomeNumber"].ToString();
                    personalemaila = dt.Rows[i]["Email"].ToString();
                    wemail = dt.Rows[i]["WorkEmail"].ToString();
                    extension = dt.Rows[i]["Extension"].ToString();
                    emrgnamea = dt.Rows[i]["EmrgName"].ToString();
                    emrgnumbera = dt.Rows[i]["EmrgNumber"].ToString();
                    emrgrelationshipa = dt.Rows[i]["EmrgRelationship"].ToString();
                    optionaemaila = dt.Rows[i]["OptionalEmail"].ToString();
                    contacta = dt.Rows[i]["ContactNo"].ToString();
                    provincea = dt.Rows[i]["Province"].ToString();
                    FNAMEEMRG = dt.Rows[i]["EmrgFname"].ToString();
                    MNAMEEMRG = dt.Rows[i]["EmrgMname"].ToString();
                    LNAMEEMRG = dt.Rows[i]["EmrgLname"].ToString();
                    DateJoined = dt.Rows[i]["DateJoined"].ToString();
                    ProductionStartDate = dt.Rows[i]["ProductionStartDate"].ToString();
                    Wallet = dt.Rows[i]["Wallet"].ToString();
                    Tenure = dt.Rows[i]["Tenure"].ToString();
                    TenureMonths = dt.Rows[i]["TenureMonths"].ToString();
                    Batch = dt.Rows[i]["Batch"].ToString();
                    Class = dt.Rows[i]["Class"].ToString();
                    Skill = dt.Rows[i]["Skill"].ToString();
                    JobDesc = dt.Rows[i]["JobDesc"].ToString();
                    Promotion = dt.Rows[i]["Promotion"].ToString();
                    NameOfOrganization = dt.Rows[i]["NameOfOrganization"].ToString();
                    maritala = dt.Rows[i]["MaritalStatus"].ToString();
                    pob = dt.Rows[i]["POB"].ToString();
                    religion = dt.Rows[i]["religion"].ToString();
                    mothername = dt.Rows[i]["MotherName"].ToString();
                    fathername = dt.Rows[i]["FatherName"].ToString();
                    payrollinfo = dt.Rows[i]["Payrollinfo"].ToString();
                    bankname = dt.Rows[i]["BankName"].ToString();
                    branchname = (dt.Rows[i]["BranchName"].ToString());
                    accountnumber = dt.Rows[i]["AccountNumber"].ToString();
                    height = dt.Rows[i]["Height"].ToString();
                    weight = dt.Rows[i]["Weight"].ToString();
                    MDATE = dt.Rows[i]["MarriedDate"].ToString();
                }
                //
                fields.SetField("untitled47", "X");
                fields.SetField("untitled48", "X");
                prof.mon = getmonth(prof.mon);
                //d
                try
                {

                    con.myparameters.Clear();
                    con.myparameters.Add(new myParameters { ParameterName = "@empid", mytype = SqlDbType.NVarChar, Value = prof.empid });
                    DataTable GFdt = con.GetDataTable("sp_getBIR1601C");
                    string fullTIN = GFdt.Rows[0][14].ToString().Replace("-", "");
                    string TIN1 = fullTIN.Substring(0, 1); //First number
                    string TIN2 = fullTIN.Substring(1, 1); //2nd number
                    string TIN3 = fullTIN.Substring(2, 1); //3rd number
                    string TIN1st3 = TIN1 + " " + TIN2 + " " + TIN3;
                    fields.SetFieldProperty("untitled52", "textsize", 15f, null);
                    string TIN4 = fullTIN.Substring(3, 1);
                    string TIN5 = fullTIN.Substring(4, 1);
                    string TIN6 = fullTIN.Substring(5, 1);
                    string TIN2nd3 = TIN4 + " " + TIN5 + " " + TIN6;
                    fields.SetFieldProperty("untitled53", "textsize", 15f, null);
                    string TIN7 = fullTIN.Substring(6, 1);
                    string TIN8 = fullTIN.Substring(7, 1);
                    string TIN9 = fullTIN.Substring(8, 1);
                    string TIN3rd3 = TIN7 + " " + TIN8 + " " + TIN9;
                    fields.SetFieldProperty("untitled54", "textsize", 15f, null);
                    if (fullTIN.Length == 12)
                    {
                        string TIN10 = fullTIN.Substring(9, 1);
                        string TIN11 = fullTIN.Substring(10, 1);
                        string TIN12 = fullTIN.Substring(11, 1);
                        string TIN4th3 = TIN10 + " " + TIN11 + " " + TIN12;
                        fields.SetFieldProperty("untitled55", "textsize", 15f, null);
                        fields.SetField("untitled52", TIN4th3);
                    }
                    fields.SetField("untitled52", TIN1st3);
                    fields.SetField("untitled53", TIN2nd3);
                    fields.SetField("untitled54", TIN3rd3);
                }
                catch
                {
                }
                try
                {
                    con.myparameters.Clear();
                    con.myparameters.Add(new myParameters { ParameterName = "@empid", mytype = SqlDbType.NVarChar, Value = prof.empid });
                    DataTable GFdt2 = con.GetDataTable("sp_getBIR1601C");
                    fields.SetField("untitled1", GFdt2.Rows[0][2].ToString().First().ToString().ToUpper() + GFdt2.Rows[0][2].ToString().Substring(1) + " " + GFdt2.Rows[0][0].ToString().First().ToString().ToUpper() + GFdt2.Rows[0][0].ToString().Substring(1) + " " + GFdt2.Rows[0][1].ToString().First().ToString().ToUpper() + GFdt2.Rows[0][1].ToString().Substring(1));
                }
                catch
                {
                }
                //5.RDO Code
                try
                {
                    con.myparameters.Clear();
                    con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = prof.empid });
                    DataTable dt2 = con.GetDataTable("sp_ShowCompanyDetails");
                    string rdo = dt2.Rows[0][63].ToString();
                    switch (rdo.Length)
                    {
                        case 1:
                            {
                                rdo = "      " + rdo;
                                break;
                            }
                        case 2:
                            {
                                rdo = "    " + rdo;
                                break;
                            }
                        default:
                            {
                                break;
                            }
                    }
                    fields.SetFieldProperty("untitled56", "textsize", 10f, null);
                    fields.SetField("untitled56", rdo);
                }
                catch
                {

                }
                try
                {
                    string telnum = "";
                    // SEPERATING NUMBER BY BOX
                    con.myparameters.Clear();
                    con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = prof.empid });
                    DataTable dt3 = con.GetDataTable("sp_getPersonalContactInfo");
                    if (dt3.Rows.Count != 0)
                    {
                        try
                        {

                            for (int i = 0; i < dt3.Rows.Count; i++)
                            {
                                string numtype = dt3.Rows[i][1].ToString();
                                //if (numtype == "Mobile")
                                //{
                                //    //MOBILE NUMBER
                                //    string MN = EC.GetPersonalInfo(empid).Rows[i][0].ToString().Replace("-", "");
                                //    string mno = MN.Substring(0, 1);
                                //    string mno1 = MN.Substring(1, 1);
                                //    string mno2 = MN.Substring(2, 1);
                                //    string mno3 = MN.Substring(3, 1);
                                //    string MNumber = mno + "   " + mno1 + "   " + mno2 + "   " + mno3;
                                //    form.SetField("untitled35", MNumber);


                                //    string mno4 = MN.Substring(4, 1);
                                //    string mno5 = MN.Substring(5, 1);
                                //    string mno6 = MN.Substring(6, 1);
                                //    string mno7 = MN.Substring(7, 1);
                                //    string mno8 = MN.Substring(8, 1);
                                //    string mno9 = MN.Substring(9, 1);
                                //    string mno10 = MN.Substring(10, 1);
                                //    string mNumber1 = mno4 + "   " + mno5 + "   " + mno6 + "  " + mno7 + "  " + mno8 + "   " + mno9 + "   " + mno10;
                                //    form.SetField("untitled36", mNumber1);
                                //}
                                //if (numtype == "Work")
                                //{

                                //}
                                if (numtype == "Home")
                                {
                                    //PHONE NUMBER
                                    telnum = dt3.Rows[i][0].ToString().Replace("-", "");
                                }
                            }
                        }
                        catch
                        {

                        }
                    }
                    string teln1 = telnum.Substring(0, 1);
                    string teln2 = telnum.Substring(1, 1);
                    string teln3 = telnum.Substring(2, 1);
                    string teln4 = telnum.Substring(3, 1);
                    string teln5 = telnum.Substring(4, 1);
                    string teln6 = telnum.Substring(5, 1);
                    string teln7 = telnum.Substring(6, 1);
                    string telnumf = teln1 + " " + teln2 + " " + teln3 + " " + teln4 + " " + teln5 + " " + teln6 + " " + teln7;
                    fields.SetFieldProperty("untitled57", "textsize", 14f, null);
                    fields.SetField("untitled57", telnumf);
                }
                catch
                {
                }
                try
                {
                    con.myparameters.Clear();
                    con.myparameters.Add(new myParameters { ParameterName = "@empid", mytype = SqlDbType.NVarChar, Value = prof.empid });
                    DataTable dt4 = con.GetDataTable("sp_getBIR1601C");
                    fields.SetField("untitled2", dt4.Rows[0][6].ToString() + " " + dt4.Rows[0][7].ToString() + ", " + dt4.Rows[0][8].ToString() + ", " + dt4.Rows[0][9].ToString() + ", " + dt4.Rows[0][10].ToString() + ", " + provincea);
                }
                catch
                {
                }
                try
                {
                    con.myparameters.Clear();
                    con.myparameters.Add(new myParameters { ParameterName = "@empid", mytype = SqlDbType.NVarChar, Value = prof.empid });
                    DataTable dt5 = con.GetDataTable("sp_getBIR1601C");
                    string zipraw = dt5.Rows[0][11].ToString();
                    string zipb1 = zipraw.Substring(0, 1);
                    string zipb2 = zipraw.Substring(1, 1);
                    string zipb3 = zipraw.Substring(2, 1);
                    string zipb4 = zipraw.Substring(3, 1);
                    string zipf = zipb1 + "  " + zipb2 + "  " + zipb3 + "  " + zipb4;
                    fields.SetFieldProperty("untitled59", "textsize", 14f, null);
                    fields.SetField("untitled59", zipf);
                }
                catch
                {
                }
                try
                {
                    con.myparameters.Clear();
                    con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = prof.empid });
                    DataTable dtgtcompany = con.GetDataTable("sp_ShowCompanyDetails");
                    fields.SetFieldProperty("untitled51", "textsize", 5.5f, null);
                    fields.SetField("untitled51", dtgtcompany.Rows[0]["Industry"].ToString());
                    fields.SetField("untitled58", "X");
                }
                catch
                {
                }

                //string yr = "2017";
                DataTable dt1601 = new DataTable();

                con.myparameters.Clear();
                con.myparameters.Add(new myParameters { ParameterName = "@empid", mytype = SqlDbType.NVarChar, Value = prof.empid });
                con.myparameters.Add(new myParameters { ParameterName = "@month", mytype = SqlDbType.NVarChar, Value = prof.mon });
                con.myparameters.Add(new myParameters { ParameterName = "@year", mytype = SqlDbType.NVarChar, Value = prof.yr });
                dt1601 = con.GetDataTable("sp_GetPaySlipRecordsPaySlipDetailsMonthly");
                string hourlys,
                dailys,
                monthlys,
                complexity_premiums,
                foods,
                travels,
                clothings,
            night_differentials,
            regular_overtimes,
            regular_holidays,
            regular_holiday_ots,
            regular_holiday_nds,
            special_holidays,
            special_holiday_ots,
            special_holiday_nds,
                bank_accounts,
                tax_statuss,
                employee_salarys;
                DataTable dr1601m1 = new DataTable();
                con.myparameters.Clear();
                con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = prof.empid });
                dr1601m1 = con.GetDataTable("sp_SearchHRMSEmployeePay");
                for (int i = 0; i < dr1601m1.Rows.Count; i++)
                {
                    hourlys = (dr1601m1.Rows[i]["hourly"].ToString());
                    dailys = (dr1601m1.Rows[i]["daily"].ToString());
                    monthlys = (dr1601m1.Rows[i]["monthly"].ToString());
                    complexity_premiums = (dr1601m1.Rows[i]["complexity_premium"].ToString());
                    foods = (dr1601m1.Rows[i]["food"].ToString());
                    travels = (dr1601m1.Rows[i]["travel"].ToString());
                    clothings = (dr1601m1.Rows[i]["clothing"].ToString());
                    night_differentials = (dr1601m1.Rows[i]["night_differential"].ToString());
                    regular_overtimes = (dr1601m1.Rows[i]["regular_overtime"].ToString());
                    regular_holidays = (dr1601m1.Rows[i]["regular_holiday"].ToString());
                    regular_holiday_ots = (dr1601m1.Rows[i]["regular_holiday_ot"].ToString());
                    regular_holiday_nds = (dr1601m1.Rows[i]["regular_holiday_nd"].ToString());
                    special_holidays = (dr1601m1.Rows[i]["special_holiday"].ToString());
                    special_holiday_ots = (dr1601m1.Rows[i]["special_holiday_ot"].ToString());
                    special_holiday_nds = (dr1601m1.Rows[i]["special_holiday_nd"].ToString());
                    bank_accounts = (dr1601m1.Rows[i]["bank_account"].ToString());
                    tax_statuss = (dr1601m1.Rows[i]["tax_status"].ToString());

                }
                //regular Hours
                string[] Regular = new string[8];
                Regular[0] = String.IsNullOrEmpty(dt1601.Rows[0][0].ToString()) ? "0" : dt1601.Rows[0][0].ToString();
                Regular[1] = String.IsNullOrEmpty(dt1601.Rows[0][3].ToString()) ? "0" : dt1601.Rows[0][3].ToString();
                Regular[2] = String.IsNullOrEmpty(dt1601.Rows[0][6].ToString()) ? "0" : dt1601.Rows[0][6].ToString();
                Regular[3] = String.IsNullOrEmpty(dt1601.Rows[0][9].ToString()) ? "0" : dt1601.Rows[0][9].ToString();
                Regular[4] = String.IsNullOrEmpty(dt1601.Rows[0][12].ToString()) ? "0" : dt1601.Rows[0][12].ToString();
                Regular[5] = String.IsNullOrEmpty(dt1601.Rows[0][15].ToString()) ? "0" : dt1601.Rows[0][15].ToString();
                Regular[6] = String.IsNullOrEmpty(dt1601.Rows[0][18].ToString()) ? "0" : dt1601.Rows[0][18].ToString();
                Regular[7] = String.IsNullOrEmpty(dt1601.Rows[0][21].ToString()) ? "0" : dt1601.Rows[0][21].ToString();

                double totalRegHrs = 0;
                for (int i = 0; i < 8; i++)
                {
                    try
                    {
                        totalRegHrs += Convert.ToDouble(Regular[i]);
                    }
                    catch
                    {
                        totalRegHrs += 0;
                    }
                }

                //Overtime
                string[] Overtime = new string[8];
                Overtime[0] = String.IsNullOrEmpty(dt1601.Rows[0][1].ToString()) ? "0" : dt1601.Rows[0][1].ToString();
                Overtime[1] = String.IsNullOrEmpty(dt1601.Rows[0][4].ToString()) ? "0" : dt1601.Rows[0][4].ToString();
                Overtime[2] = String.IsNullOrEmpty(dt1601.Rows[0][7].ToString()) ? "0" : dt1601.Rows[0][7].ToString();
                Overtime[3] = String.IsNullOrEmpty(dt1601.Rows[0][10].ToString()) ? "0" : dt1601.Rows[0][10].ToString();
                Overtime[4] = String.IsNullOrEmpty(dt1601.Rows[0][13].ToString()) ? "0" : dt1601.Rows[0][13].ToString();
                Overtime[5] = String.IsNullOrEmpty(dt1601.Rows[0][16].ToString()) ? "0" : dt1601.Rows[0][16].ToString();
                Overtime[6] = String.IsNullOrEmpty(dt1601.Rows[0][19].ToString()) ? "0" : dt1601.Rows[0][19].ToString();
                Overtime[7] = String.IsNullOrEmpty(dt1601.Rows[0][22].ToString()) ? "0" : dt1601.Rows[0][22].ToString();

                double totalOTHrs = 0;
                for (int i = 0; i < 8; i++)
                {
                    try
                    {
                        totalOTHrs += Convert.ToDouble(Overtime[i]);
                    }
                    catch
                    {
                        totalOTHrs += 0;
                    }
                }
                //Night Differential
                string[] Nightdiff = new string[8];
                Nightdiff[0] = String.IsNullOrEmpty(dt1601.Rows[0][2].ToString()) ? "0" : dt1601.Rows[0][2].ToString();
                Nightdiff[1] = String.IsNullOrEmpty(dt1601.Rows[0][5].ToString()) ? "0" : dt1601.Rows[0][5].ToString();
                Nightdiff[2] = String.IsNullOrEmpty(dt1601.Rows[0][8].ToString()) ? "0" : dt1601.Rows[0][8].ToString();
                Nightdiff[3] = String.IsNullOrEmpty(dt1601.Rows[0][11].ToString()) ? "0" : dt1601.Rows[0][11].ToString();
                Nightdiff[4] = String.IsNullOrEmpty(dt1601.Rows[0][14].ToString()) ? "0" : dt1601.Rows[0][14].ToString();
                Nightdiff[5] = String.IsNullOrEmpty(dt1601.Rows[0][17].ToString()) ? "0" : dt1601.Rows[0][17].ToString();
                Nightdiff[6] = String.IsNullOrEmpty(dt1601.Rows[0][20].ToString()) ? "0" : dt1601.Rows[0][20].ToString();
                Nightdiff[7] = String.IsNullOrEmpty(dt1601.Rows[0][23].ToString()) ? "0" : dt1601.Rows[0][23].ToString();

                double totalNDHrs = 0;
                for (int i = 0; i < 8; i++)
                {
                    try
                    {
                        totalNDHrs += Convert.ToDouble(Nightdiff[i]);
                    }
                    catch
                    {
                        totalNDHrs += 0;
                    }
                }
                //Leave
                string leaveamt = String.IsNullOrEmpty(dt1601.Rows[0][24].ToString()) ? "0" : dt1601.Rows[0][24].ToString();


                //De Minimis
                double deminitax = String.IsNullOrEmpty(dt1601.Rows[0][26].ToString()) ? 0 : Convert.ToDouble(dt1601.Rows[0][26].ToString());
                double demininontax = String.IsNullOrEmpty(dt1601.Rows[0][27].ToString()) ? 0 : Convert.ToDouble(dt1601.Rows[0][27].ToString());
                double demini = deminitax + demininontax;
                //Fringe
                double fringetax = String.IsNullOrEmpty(dt1601.Rows[0][28].ToString()) ? 0 : Convert.ToDouble(dt1601.Rows[0][28].ToString());
                double fringenontax = String.IsNullOrEmpty(dt1601.Rows[0][29].ToString()) ? 0 : Convert.ToDouble(dt1601.Rows[0][29].ToString());
                //Other Benefit
                double othertax = String.IsNullOrEmpty(dt1601.Rows[0][30].ToString()) ? 0 : Convert.ToDouble(dt1601.Rows[0][30].ToString());
                double othernontax = String.IsNullOrEmpty(dt1601.Rows[0][31].ToString()) ? 0 : Convert.ToDouble(dt1601.Rows[0][31].ToString());
                //SSS, Pagibig, Philhealth
                string HDMFtotal = String.IsNullOrEmpty(dt1601.Rows[0][32].ToString()) ? "0" : dt1601.Rows[0][32].ToString();
                string PHICtotal = String.IsNullOrEmpty(dt1601.Rows[0][33].ToString()) ? "0" : dt1601.Rows[0][33].ToString();
                string SSStotal = String.IsNullOrEmpty(dt1601.Rows[0][34].ToString()) ? "0" : dt1601.Rows[0][34].ToString();
                double deducttotal = Convert.ToDouble(HDMFtotal) + Convert.ToDouble(PHICtotal) + Convert.ToDouble(SSStotal);

                double tottaxble = totalRegHrs + totalOTHrs + totalNDHrs + Convert.ToDouble(leaveamt) + deminitax + fringetax + othertax;
                double totalamount = tottaxble + demininontax + fringenontax + othernontax;
                string month1tax = "";
                string month2tax = "";
                string month3tax = "";
                month1tax = "0";// String.IsNullOrEmpty(dt1601.Rows[0][36].ToString()) ? "0" : dt1601.Rows[0][36].ToString();
                month2tax = "0";// String.IsNullOrEmpty(dt1601.Rows[0][37].ToString()) ? "0" : dt1601.Rows[0][37].ToString();
                month3tax = "0";// String.IsNullOrEmpty(dt1601.Rows[0][38].ToString()) ? "0" : dt1601.Rows[0][38].ToString();

                //Tax Formula(Monthly)
                double taxdue = 0;

                if (tottaxble <= 33333)
                {
                    taxdue = 0;
                }
                else if (tottaxble <= 33333 && tottaxble > 20833)
                {
                    taxdue = (tottaxble - 20833) * .2;
                }
                else if (tottaxble <= 66667)
                {
                    taxdue = ((tottaxble - 33333) * .25) + 2500;
                }
                else if (tottaxble <= 166667)
                {
                    taxdue = ((tottaxble - 66667) * .30) + 10833.33;
                }
                else if (tottaxble <= 666667)
                {
                    taxdue = ((tottaxble - 166667) * .32) + 40833.33;
                }
                else
                {
                    taxdue = ((tottaxble - 666667) * .35) + 200833.33;
                }

                char[] mon2 = prof.mon.ToCharArray();
                char[] yr2 = prof.yr.ToCharArray();
                fields.SetFieldProperty("untitled44", "textsize", 15f, null);
                fields.SetFieldProperty("untitled45", "textsize", 15f, null);
                fields.SetField("untitled44", mon2[0] + " " + mon2[1]);
                fields.SetField("untitled45", yr2[0] + " " + prof.yr[1] + " " + yr2[2] + " " + yr2[3]);
                fields.SetField("untitled4", commafy(totalamount.ToString()));
                fields.SetField("untitled5", "0");
                fields.SetField("untitled6", "0");
                fields.SetField("untitled7", commafy(demini));
                fields.SetField("untitled8", commafy(tottaxble.ToString()));
                fields.SetField("untitled9", commafy(taxdue));
                fields.SetField("untitled10", "0");
                fields.SetField("untitled11", "0");
                fields.SetField("untitled12", "0");
                fields.SetField("untitled13", "0");
                fields.SetField("untitled14", "0");
                fields.SetField("untitled15", commafy(0));
                fields.SetField("untitled18", "0");
                fields.SetField("untitled19", "0");
                fields.SetField("untitled20", "0");
                fields.SetField("untitled16", "0");
                fields.SetField("untitled17", commafy(0));
                fields.SetField("untitled21", submonth(prof.mon, 1, Convert.ToInt32(DateTime.Now.Year.ToString()))[0]);
                fields.SetField("untitled24", submonth(prof.mon, 1, Convert.ToInt32(DateTime.Now.Year.ToString()))[1]);
                fields.SetField("untitled22", submonth(prof.mon, 2, Convert.ToInt32(DateTime.Now.Year.ToString()))[0]);
                fields.SetField("untitled25", submonth(prof.mon, 2, Convert.ToInt32(DateTime.Now.Year.ToString()))[1]);
                fields.SetField("untitled23", submonth(prof.mon, 3, Convert.ToInt32(DateTime.Now.Year.ToString()))[0]);
                fields.SetField("untitled26", submonth(prof.mon, 3, Convert.ToInt32(DateTime.Now.Year.ToString()))[1]);
                fields.SetField("untitled42", commafy(month1tax));
                fields.SetField("untitled43", commafy(month2tax));
                fields.SetField("untitled63", commafy(month3tax));
                if (!string.IsNullOrEmpty(dt1601.Rows[0][25].ToString()))
                {
                    fields.SetField("untitled64", commafy(0));
                }
                else
                {
                    fields.SetField("untitled64", "0");
                }
                if (!string.IsNullOrEmpty(dt1601.Rows[0][26].ToString()))
                {
                    fields.SetField("untitled65", commafy(0));
                }
                else
                {
                    fields.SetField("untitled65", "0");
                }
                if (!string.IsNullOrEmpty(dt1601.Rows[0][27].ToString()))
                {
                    fields.SetField("untitled66", commafy(0));
                }
                else
                {
                    fields.SetField("untitled66", "0");
                }




                // form flattening rids the form of editable text fields so
                // the final output can't be edited
                // stamper.FormFlattening = true;

                //Signatory

                DataTable dtsign = new DataTable();

                con.myparameters.Clear();
                con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = prof.empid });
                dtsign = con.GetDataTable("sp_SearchHRMSDocumentsRequestForm");
                //sign
                string Form, Pattern, ReqStart, ReqEnd, CoveredStart, CoveredEnd, NotifMsg, President, PresBck, HR, HRBck, Savedloc, SharedEmail, EmailTo, EmailCc, EmailBcc, EmailSubject, EmailMessege, SignMessage, Default1, Default2;
                int Function, DailyDay, DailyWeek, RecurWeek, Monday, Tuesday, Wednesday, Thursday, Friday, Saturday, Sunday, DayMonth, Month, Quarter, Day, Months, EndDay, AutoSign, Permission, PrintSign, FormRequested, SignedDays, SaveLoc, SharEmail, Email, Number;

                for (int i = 0; i < dtsign.Rows.Count; i++)
                {
                    Form = dtsign.Rows[i]["Form"].ToString();
                    Function = Convert.ToInt32(dtsign.Rows[i]["Functions"].ToString());
                    Pattern = dtsign.Rows[i]["Pattern"].ToString();
                    DailyDay = Convert.ToInt32(dtsign.Rows[i]["EveryNday"].ToString());
                    DailyWeek = Convert.ToInt32(dtsign.Rows[i]["EveryWeekday"].ToString());
                    RecurWeek = Convert.ToInt32(dtsign.Rows[i]["RecurWeek"].ToString());
                    Monday = Convert.ToInt32(dtsign.Rows[i]["RecurMon"].ToString());
                    Tuesday = Convert.ToInt32(dtsign.Rows[i]["RecurTue"].ToString());
                    Wednesday = Convert.ToInt32(dtsign.Rows[i]["RecurWed"].ToString());
                    Thursday = Convert.ToInt32(dtsign.Rows[i]["RecurThu"].ToString());
                    Friday = Convert.ToInt32(dtsign.Rows[i]["RecurFri"].ToString());
                    Saturday = Convert.ToInt32(dtsign.Rows[i]["RecurSat"].ToString());
                    Sunday = Convert.ToInt32(dtsign.Rows[i]["RecurSun"].ToString());
                    DayMonth = Convert.ToInt32(dtsign.Rows[i]["DayNMonth"].ToString());
                    Month = Convert.ToInt32(dtsign.Rows[i]["NMonth"].ToString());
                    Quarter = Convert.ToInt32(dtsign.Rows[i]["NQuarter"].ToString());
                    Day = Convert.ToInt32(dtsign.Rows[i]["NDay"].ToString());
                    Months = Convert.ToInt32(dtsign.Rows[i]["EveryNMonth"].ToString());
                    ReqStart = dtsign.Rows[i]["RequestStart"].ToString();
                    ReqEnd = dtsign.Rows[i]["RequestEnd"].ToString();
                    EndDay = Convert.ToInt32(dtsign.Rows[i]["EndDay"].ToString());
                    CoveredStart = dtsign.Rows[i]["CoveredStart"].ToString();
                    CoveredEnd = dtsign.Rows[i]["CoveredEnd"].ToString();
                    NotifMsg = dtsign.Rows[i]["NotificationMessege"].ToString();
                    President = dtsign.Rows[i]["President"].ToString();
                    PresBck = dtsign.Rows[i]["PresBackup"].ToString();
                    HR = dtsign.Rows[i]["HR"].ToString();
                    HRBck = dtsign.Rows[i]["HRBackup"].ToString();
                    AutoSign = Convert.ToInt32(dtsign.Rows[i]["AutoSign"].ToString());
                    Permission = Convert.ToInt32(dtsign.Rows[i]["SignPermission"].ToString());
                    PrintSign = Convert.ToInt32(dtsign.Rows[i]["PrintSign"].ToString());
                    if (string.IsNullOrWhiteSpace(dtsign.Rows[i]["Default1"].ToString()) == false)
                    {
                        Default1 = dtsign.Rows[i]["Default1"].ToString();
                    }
                    else
                    {
                        Default1 = "False";
                    }
                    if (string.IsNullOrWhiteSpace(dtsign.Rows[i]["Default2"].ToString()) == false)
                    {
                        Default2 = dtsign.Rows[i]["Default2"].ToString();
                    }
                    else
                    {
                        Default2 = "False";
                    }
                    FormRequested = Convert.ToInt32(dtsign.Rows[i]["FormIsRequested"].ToString());
                    SignedDays = Convert.ToInt32(dtsign.Rows[i]["SignedForNDays"].ToString());
                    SaveLoc = Convert.ToInt32(dtsign.Rows[i]["SaveLocation"].ToString());
                    Savedloc = dtsign.Rows[i]["Location"].ToString();
                    SharEmail = Convert.ToInt32(dtsign.Rows[i]["ShareEmail"].ToString());
                    SharedEmail = dtsign.Rows[i]["Email"].ToString();
                    Email = Convert.ToInt32(dtsign.Rows[i]["SendToEmail"].ToString());
                    Number = Convert.ToInt32(dtsign.Rows[i]["SendToNumber"].ToString());
                    EmailTo = dtsign.Rows[i]["EmailTo"].ToString();
                    EmailCc = dtsign.Rows[i]["EmailCc"].ToString();
                    EmailBcc = dtsign.Rows[i]["EmailBcc"].ToString();
                    EmailSubject = dtsign.Rows[i]["EmailSubject"].ToString();
                    EmailMessege = dtsign.Rows[i]["EmailMessege"].ToString();
                    SignMessage = dtsign.Rows[i]["SignMessage"].ToString();
                }
                //sign

                DataTable dtsign1 = new DataTable();
                DataTable dtsign2 = new DataTable();
                string signatory1, signatory2, position1, position2, active1, active2, empid1, empid2;

                con.myparameters.Clear();
                con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = prof.empid });
                dtsign1 = con.GetDataTable("sp_GetSignatory1");
                dtsign2 = con.GetDataTable("sp_GetSignatory2");
                for (int i = 0; i < dtsign1.Rows.Count; i++)
                {
                    empid1 = dtsign1.Rows[i]["EmpID"].ToString();
                    active1 = dtsign1.Rows[i]["SIgnator1Status"].ToString();
                    signatory1 = dtsign1.Rows[i]["EmpName"].ToString();
                    position1 = dtsign1.Rows[i]["JobDesc"].ToString();
                }
                for (int i = 0; i < dtsign2.Rows.Count; i++)
                {
                    empid2 = dtsign2.Rows[i]["EmpID"].ToString();
                    active2 = dtsign2.Rows[i]["SIgnator2Status"].ToString();
                    signatory2 = dtsign2.Rows[i]["EmpName"].ToString();
                    position2 = dtsign2.Rows[i]["JobDesc"].ToString();
                }

                //retrieve approver  empid

                string ApproverEMPID = "", ApproverAUDITTrail, DateAuditTrail, AuditRemarks;
                DataTable dtsign3 = new DataTable();
                con.myparameters.Clear();
                con.myparameters.Add(new myParameters { ParameterName = "@FORMID", mytype = SqlDbType.NVarChar, Value = 83 });
                con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = prof.empid });
                dtsign3 = con.GetDataTable("sp_GetApproverEmpid");
                for (int i = 0; i < dtsign3.Rows.Count; i++)
                {
                    ApproverEMPID = dtsign3.Rows[i][0].ToString();
                }
                string empidApprover = "";
                empidApprover = ApproverEMPID;
                if (empidApprover == "" || empidApprover == null || string.IsNullOrWhiteSpace(empidApprover))
                {
                    //no display signiture ,empname and job desc
                }
                else
                {
                    //get  approver JObdescription and  empname
                    string BitSign = "", BitSignPres, BitSignHR, ApproverName = "", ApproverJobDesc = "", ReqName, ReqDesc;
                    DataTable dtsign4 = new DataTable();
                    con.myparameters.Clear();
                    con.myparameters.Add(new myParameters { ParameterName = "@EMPIDAPPROVER", mytype = SqlDbType.NVarChar, Value = empidApprover });
                    dtsign4 = con.GetDataTable("sp_SearchEmpaneJobSinatory");
                    for (int i = 0; i < dtsign4.Rows.Count; i++)
                    {
                        ApproverName = dtsign4.Rows[i][0].ToString();
                        ApproverJobDesc = dtsign4.Rows[i][1].ToString();
                    }
                    string Approvername = ApproverName;
                    string ApproverDesc = ApproverJobDesc;
                    //get signatory sign in based64
                    DataTable dtsign5 = new DataTable();
                    con.myparameters.Clear();
                    con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = empidApprover });
                    con.myparameters.Add(new myParameters { ParameterName = "@TYPES", mytype = SqlDbType.NVarChar, Value = "sign_1" });
                    dtsign5 = con.GetDataTable("sp_SearchBased64");
                    for (int i = 0; i < dtsign5.Rows.Count; i++)
                    {
                        BitSign = dtsign5.Rows[i][0].ToString();
                    }
                    string Bitsignature = null;
                    Bitsignature = BitSign;
                    Byte[] SignatureBit = null;
                    iTextSharp.text.Image signatory = null;
                    var pdfContentByte = stamper.GetOverContent(1);
                    if (string.IsNullOrWhiteSpace(Bitsignature) == false)
                    {
                        Bitsignature = Bitsignature.Replace(' ', '+');
                        SignatureBit = Convert.FromBase64String(Bitsignature);
                        signatory = iTextSharp.text.Image.GetInstance(SignatureBit);
                        signatory.SetAbsolutePosition(120, 240);
                        signatory.ScaleToFit(100, 100);
                        fields.SetField("untitled73", Approvername);
                        fields.SetField("untitled80", ApproverDesc);
                        DataTable dtsign6 = new DataTable();
                        con.myparameters.Clear();
                        con.myparameters.Add(new myParameters { ParameterName = "@empid", mytype = SqlDbType.NVarChar, Value = prof.empid });
                        dtsign6 = con.GetDataTable("sp_getBIR1601C");
                        fields.SetField("untitled77", dtsign6.Rows[0][14].ToString());
                        pdfContentByte.AddImage(signatory);
                    }
                }

                //    PR.GetBased64(GF.empid1, "sign_0");

                //}
                //string Sign1 = null;
                //Sign1 = PR.Based64D;
                //Byte[] signa1 = null;
                //iTextSharp.text.Image signature1 = null;
                //iTextSharp.text.Image signature2 = null;

                //if (string.IsNullOrWhiteSpace(Sign1) == false)
                //{
                //    Sign1 = Sign1.Replace(' ', '+');
                //    signa1 = Convert.FromBase64String(Sign1);
                //    signature1 = iTextSharp.text.Image.GetInstance(signa1);
                //    signature1.SetAbsolutePosition(60, 90);
                //    signature1.ScaleToFit(100, 100);
                //    signature2 = iTextSharp.text.Image.GetInstance(signa1);
                //    signature2.SetAbsolutePosition(60, 240);
                //    signature2.ScaleToFit(100, 100);
                //}

                //var pdfContentByte = stamper.GetOverContent(1);


                //    fields.SetField("untitled73", GF.signatory1);
                //    fields.SetField("untitled80", GF.position1);

                //        pdfContentByte.AddImage(signature2);




                //        pdfContentByte.AddImage(signature2);


                //if(GF.active1=="Active" && GF.active2 == "Active")
                //{
                //    fields.SetField("untitled73", GF.signatory1);
                //    fields.SetField("untitled80", GF.position1);
                //    fields.SetField("untitled74", GF.signatory2);
                //    fields.SetField("untitled75", GF.position2);
                //}

                //if (AutoSign > 0)
                //{

                //}
                //if (Permission > 0)
                //{

                //}
                //if (PrintSign > 0)
                //{

                //}
                //if (Default1 == "True")
                //{

                //}
                //if (Default2 == "True")
                //{

                //}
                //foreach (var field in af.Fields)
                //{
                //    Console.WriteLine("{0}, {1}", field.Key, field.Value);

                //    string sample2 = field.Key.ToString();
                //    fields.SetField(sample2, sample2);
                //}
                //to read only
                stamper.FormFlattening = true;
                // closing the stamper
                stamper.Close();
                pdfReader1.Dispose();
                prof.empid = "success";
            }
            catch (Exception ex)
            {
                prof.empid = ex.ToString();
            }
            return prof.empid;
        }
        private string commafy(object num)
        {
            string commafied = String.Format("{0:n}", Math.Round(Convert.ToDouble(num), 2));
            return commafied;
        }
        //public string _1601C(UserProfile uProfile)
        //{
        //    string path = HttpContext.Current.Server.MapPath("pdf");
        //    var pdfReader1 = new PdfReader(path + "/GovForms Templates/1601C.pdf");
        //    AcroFields af = pdfReader1.AcroFields;
        //    string formFile = path + "/GovForms Templates/1601C.pdf";
        //    string newFile = path + "/GovForms-Employee/1601C-" + uProfile.NTID + ".pdf";

        //    PdfReader reader = new PdfReader(formFile);
        //    PdfStamper stamper = new PdfStamper(reader, new FileStream(newFile, FileMode.Create));

        //    AcroFields fields = stamper.AcroFields;

        //    try
        //    {
        //        GovForms.getEmployeePersonalInformation(uProfile.NTID);
        //        fields.SetField("untitled47", "X");
        //        fields.SetField("untitled48", "X");
        //        try
        //        {
        //            string fullTIN = GovForms.getBIR1601C(uProfile.NTID).Rows[0][14].ToString().Replace("-", "");
        //            string TIN1 = fullTIN.Substring(0, 1); //First number
        //            string TIN2 = fullTIN.Substring(1, 1); //2nd number
        //            string TIN3 = fullTIN.Substring(2, 1); //3rd number
        //            string TIN1st3 = TIN1 + " " + TIN2 + " " + TIN3;
        //            fields.SetFieldProperty("untitled52", "textsize", 15f, null);
        //            string TIN4 = fullTIN.Substring(3, 1);
        //            string TIN5 = fullTIN.Substring(4, 1);
        //            string TIN6 = fullTIN.Substring(5, 1);
        //            string TIN2nd3 = TIN4 + " " + TIN5 + " " + TIN6;
        //            fields.SetFieldProperty("untitled53", "textsize", 15f, null);
        //            string TIN7 = fullTIN.Substring(6, 1);
        //            string TIN8 = fullTIN.Substring(7, 1);
        //            string TIN9 = fullTIN.Substring(8, 1);
        //            string TIN3rd3 = TIN7 + " " + TIN8 + " " + TIN9;
        //            fields.SetFieldProperty("untitled54", "textsize", 15f, null);
        //            if (fullTIN.Length == 12)
        //            {
        //                string TIN10 = fullTIN.Substring(9, 1);
        //                string TIN11 = fullTIN.Substring(10, 1);
        //                string TIN12 = fullTIN.Substring(11, 1);
        //                string TIN4th3 = TIN10 + " " + TIN11 + " " + TIN12;
        //                fields.SetFieldProperty("untitled55", "textsize", 15f, null);
        //                fields.SetField("untitled52", TIN4th3);
        //            }
        //            fields.SetField("untitled52", TIN1st3);
        //            fields.SetField("untitled53", TIN2nd3);
        //            fields.SetField("untitled54", TIN3rd3);
        //        }
        //        catch
        //        {
        //        }
        //        try
        //        {
        //            fields.SetField("untitled1", GovForms.getBIR1601C(uProfile.NTID).Rows[0][2].ToString().First().ToString().ToUpper() + GovForms.getBIR1601C(uProfile.NTID).Rows[0][2].ToString().Substring(1) + " " + GovForms.getBIR1601C(uProfile.NTID).Rows[0][0].ToString().First().ToString().ToUpper() + GovForms.getBIR1601C(uProfile.NTID).Rows[0][0].ToString().Substring(1) + " " + GovForms.getBIR1601C(uProfile.NTID).Rows[0][1].ToString().First().ToString().ToUpper() + GovForms.getBIR1601C(uProfile.NTID).Rows[0][1].ToString().Substring(1));
        //        }
        //        catch
        //        {
        //        }
        //        //5.RDO Code
        //        try
        //        {
        //            string rdo = GovForms.getCompanyProfile(uProfile.NTID).Rows[0][63].ToString();
        //            switch (rdo.Length)
        //            {
        //                case 1:
        //                    {
        //                        rdo = "      " + rdo;
        //                        break;
        //                    }
        //                case 2:
        //                    {
        //                        rdo = "    " + rdo;
        //                        break;
        //                    }
        //                default:
        //                    {
        //                        break;
        //                    }
        //            }
        //            fields.SetFieldProperty("untitled56", "textsize", 10f, null);
        //            fields.SetField("untitled56", rdo);
        //        }
        //        catch
        //        {

        //        }
        //        try
        //        {
        //            string telnum = "";
        //            // SEPERATING NUMBER BY BOX
        //            if (GovForms.GetPersonalInfo(uProfile.NTID).Rows.Count != 0)
        //            {
        //                try
        //                {

        //                    for (int i = 0; i < GovForms.GetPersonalInfo(uProfile.NTID).Rows.Count; i++)
        //                    {
        //                        string numtype = GovForms.GetPersonalInfo(uProfile.NTID).Rows[i][1].ToString();
        //                        //if (numtype == "Mobile")
        //                        //{
        //                        //    //MOBILE NUMBER
        //                        //    string MN = GovForms.GetPersonalInfo(uProfile.NTID).Rows[i][0].ToString().Replace("-", "");
        //                        //    string mno = MN.Substring(0, 1);
        //                        //    string mno1 = MN.Substring(1, 1);
        //                        //    string mno2 = MN.Substring(2, 1);
        //                        //    string mno3 = MN.Substring(3, 1);
        //                        //    string MNumber = mno + "   " + mno1 + "   " + mno2 + "   " + mno3;
        //                        //    form.SetField("untitled35", MNumber);


        //                        //    string mno4 = MN.Substring(4, 1);
        //                        //    string mno5 = MN.Substring(5, 1);
        //                        //    string mno6 = MN.Substring(6, 1);
        //                        //    string mno7 = MN.Substring(7, 1);
        //                        //    string mno8 = MN.Substring(8, 1);
        //                        //    string mno9 = MN.Substring(9, 1);
        //                        //    string mno10 = MN.Substring(10, 1);
        //                        //    string mNumber1 = mno4 + "   " + mno5 + "   " + mno6 + "  " + mno7 + "  " + mno8 + "   " + mno9 + "   " + mno10;
        //                        //    form.SetField("untitled36", mNumber1);
        //                        //}
        //                        //if (numtype == "Work")
        //                        //{

        //                        //}
        //                        if (numtype == "Home")
        //                        {
        //                            //PHONE NUMBER
        //                            telnum = GovForms.GetPersonalInfo(uProfile.NTID).Rows[i][0].ToString().Replace("-", "");
        //                        }
        //                    }
        //                }
        //                catch
        //                {

        //                }
        //            }
        //            string teln1 = telnum.Substring(0, 1);
        //            string teln2 = telnum.Substring(1, 1);
        //            string teln3 = telnum.Substring(2, 1);
        //            string teln4 = telnum.Substring(3, 1);
        //            string teln5 = telnum.Substring(4, 1);
        //            string teln6 = telnum.Substring(5, 1);
        //            string teln7 = telnum.Substring(6, 1);
        //            string telnumf = teln1 + " " + teln2 + " " + teln3 + " " + teln4 + " " + teln5 + " " + teln6 + " " + teln7;
        //            fields.SetFieldProperty("untitled57", "textsize", 14f, null);
        //            fields.SetField("untitled57", telnumf);
        //        }
        //        catch
        //        {
        //        }
        //        try
        //        {
        //            fields.SetField("untitled2", GovForms.getBIR1601C(uProfile.NTID).Rows[0][6].ToString() + " " + GovForms.getBIR1601C(uProfile.NTID).Rows[0][7].ToString() + ", " + GovForms.getBIR1601C(uProfile.NTID).Rows[0][8].ToString() + ", " + GovForms.getBIR1601C(uProfile.NTID).Rows[0][9].ToString() + ", " + GovForms.getBIR1601C(uProfile.NTID).Rows[0][10].ToString() + ", " + GovForms.provincea);
        //        }
        //        catch
        //        {
        //        }
        //        try
        //        {
        //            string zipraw = GovForms.getBIR1601C(uProfile.NTID).Rows[0][11].ToString();
        //            string zipb1 = zipraw.Substring(0, 1);
        //            string zipb2 = zipraw.Substring(1, 1);
        //            string zipb3 = zipraw.Substring(2, 1);
        //            string zipb4 = zipraw.Substring(3, 1);
        //            string zipf = zipb1 + "  " + zipb2 + "  " + zipb3 + "  " + zipb4;
        //            fields.SetFieldProperty("untitled59", "textsize", 14f, null);
        //            fields.SetField("untitled59", zipf);
        //        }
        //        catch
        //        {
        //        }
        //        try
        //        {
        //            fields.SetField("untitled51", GovForms.getBIR1601C(uProfile.NTID).Rows[0][15].ToString());
        //            fields.SetField("untitled58", "X");
        //        }
        //        catch
        //        {
        //        }
        //        string mon = "";
        //        try
        //        {
        //            mon = getmonth("January");//getmonth(PeriodFrom.Value.ToString());
        //        }
        //        catch
        //        {
        //            mon = "10";
        //        }
        //        string yr = ""; //yearOnly.Value.ToString();
        //        DataTable dt1601 = new DataTable();
        //        dt1601 = GovForms.getPaySlipDetailsAllbyMonth(uProfile.NTID, mon, yr);
        //        DataTable dr1601m1 = new DataTable();
        //        double tottaxble = 0;
        //        try
        //        {
        //            tottaxble = Convert.ToInt32(dt1601.Rows[0][23]) - Convert.ToInt32(dt1601.Rows[0][19]);
        //        }
        //        catch { }
        //        string month1tax;
        //        string month2tax;
        //        string month3tax;
        //        string TotRgHrs = "";
        //        string TotOTHrs = "";
        //        string emprate = "";
        //        try
        //        {
        //            TotRgHrs = GovForms.PaySlipTotalHours(uProfile.NTID).Rows[0][1].ToString();
        //            TotOTHrs = GovForms.PaySlipTotalHours(uProfile.NTID).Rows[0][2].ToString();
        //            emprate = GovForms.PaySlipRecordsEmployeePay(uProfile.NTID).Rows[0][1].ToString();
        //        }
        //        catch { }
        //        double gpRg = 0;
        //        try
        //        {
        //            gpRg = Convert.ToDouble(TotRgHrs) * Convert.ToDouble(emprate);//regular hours
        //        }
        //        catch { }
        //        double gpOT = 0;
        //        try
        //        {
        //            gpOT = Convert.ToDouble(TotOTHrs) * (Convert.ToDouble(emprate) * 1.25);
        //        }
        //        catch { }
        //        string[] datess = new string[2];
        //        try
        //        {
        //            datess[0] = yr + "-" + mon + "-01";
        //            datess[1] = Convert.ToDateTime(yr + "-" + mon + "-01").AddMonths(1).AddDays(-1).ToString("yyyy-MM-dd");
        //        }
        //        catch { }
        //        string demini = "";
        //        string HDMFtotal = "";
        //        string PHICtotal = "";
        //        string SSStotal = "";
        //        try
        //        {
        //            demini = GovForms.DeminimisAmount(uProfile.NTID, datess[0], datess[1]).ToString();
        //            HDMFtotal = GovForms.PaySlipTotalGovDeductions(uProfile.NTID).Rows[0][1].ToString();
        //            PHICtotal = GovForms.PaySlipTotalGovDeductions(uProfile.NTID).Rows[0][2].ToString();
        //            SSStotal = GovForms.PaySlipTotalGovDeductions(uProfile.NTID).Rows[0][3].ToString();
        //        }
        //        catch { }
        //        double deducttotal = 0;
        //        try
        //        {
        //            deducttotal = Convert.ToDouble(HDMFtotal) + Convert.ToDouble(PHICtotal) + Convert.ToDouble(SSStotal);
        //        }
        //        catch { }
        //        double totalnontaxbl = 0;
        //        try
        //        {
        //            totalnontaxbl = Convert.ToDouble(demini) + Convert.ToDouble(deducttotal);
        //        }
        //        catch { }
        //        double totaltaxbl = 0;
        //        try
        //        {
        //            totaltaxbl = gpRg + gpOT;
        //        }
        //        catch { }
        //        double total = 0;
        //        try
        //        {
        //            total = totalnontaxbl + totaltaxbl;
        //        }
        //        catch { }
        //        double bscPerEx = 50000;
        //        double depcount = GovForms.SearchDependentv2(uProfile.NTID).Rows.Count;
        //        double addEx = 0;
        //        try
        //        {
        //            addEx = 25000 * depcount;
        //        }
        //        catch { }
        //        double totalEx = 0;
        //        try
        //        {
        //            totalEx = bscPerEx + addEx;
        //        }
        //        catch { }
        //        double nettxbl = 0;
        //        try
        //        {
        //            nettxbl = totaltaxbl - totalEx;
        //        }
        //        catch { }
        //        double taxdue = Convert.ToDouble(GovForms.PaySlipTotalGovDeductions(uProfile.NTID).Rows[0][3].ToString());
        //        try
        //        {
        //            month1tax = GovForms.getPaySlipDetailsAllbyMonth(uProfile.NTID, submonth(mon, 1, Convert.ToInt32(DateTime.Now.Year.ToString()))[0], submonth(mon, 1, Convert.ToInt32(DateTime.Now.Year.ToString()))[1]).Rows[0][13].ToString();
        //        }
        //        catch
        //        {
        //            month1tax = "0";
        //        }
        //        try
        //        {
        //            month2tax = GovForms.getPaySlipDetailsAllbyMonth(uProfile.NTID, submonth(mon, 2, Convert.ToInt32(DateTime.Now.Year.ToString()))[0], submonth(mon, 1, Convert.ToInt32(DateTime.Now.Year.ToString()))[1]).Rows[0][13].ToString();
        //        }
        //        catch
        //        {
        //            month2tax = "0";
        //        }
        //        try
        //        {
        //            month3tax = GovForms.getPaySlipDetailsAllbyMonth(uProfile.NTID, submonth(mon, 3, Convert.ToInt32(DateTime.Now.Year.ToString()))[0], submonth(mon, 1, Convert.ToInt32(DateTime.Now.Year.ToString()))[1]).Rows[0][13].ToString();
        //        }
        //        catch
        //        {
        //            month3tax = "0";
        //        }
        //        char[] mon2 = mon.ToCharArray();
        //        char[] yr2 = yr.ToCharArray();
        //        fields.SetFieldProperty("untitled44", "textsize", 15f, null);
        //        fields.SetFieldProperty("untitled45", "textsize", 15f, null);
        //        fields.SetField("untitled44", mon2[0] + " " + mon2[1]);
        //        fields.SetField("untitled45", yr2[0] + " " + yr[1] + " " + yr2[2] + " " + yr2[3]);
        //        fields.SetField("untitled4", (Convert.ToDouble(dt1601.Rows[0][23]) + Convert.ToDouble(demini)).ToString());
        //        fields.SetField("untitled5", "0");
        //        fields.SetField("untitled6", "0");
        //        fields.SetField("untitled7", demini);
        //        fields.SetField("untitled8", tottaxble.ToString());
        //        fields.SetField("untitled9", dt1601.Rows[0][13].ToString());
        //        fields.SetField("untitled10", "0");
        //        fields.SetField("untitled11", "0");
        //        fields.SetField("untitled12", "0");
        //        fields.SetField("untitled13", "0");
        //        fields.SetField("untitled14", "0");
        //        fields.SetField("untitled15", dt1601.Rows[0][13].ToString());
        //        fields.SetField("untitled18", "0");
        //        fields.SetField("untitled19", "0");
        //        fields.SetField("untitled20", "0");
        //        fields.SetField("untitled16", "0");
        //        fields.SetField("untitled17", dt1601.Rows[0][13].ToString());
        //        fields.SetField("untitled21", submonth(mon, 1, Convert.ToInt32(DateTime.Now.Year.ToString()))[0]);
        //        fields.SetField("untitled24", submonth(mon, 1, Convert.ToInt32(DateTime.Now.Year.ToString()))[1]);
        //        fields.SetField("untitled22", submonth(mon, 2, Convert.ToInt32(DateTime.Now.Year.ToString()))[0]);
        //        fields.SetField("untitled25", submonth(mon, 2, Convert.ToInt32(DateTime.Now.Year.ToString()))[1]);
        //        fields.SetField("untitled23", submonth(mon, 3, Convert.ToInt32(DateTime.Now.Year.ToString()))[0]);
        //        fields.SetField("untitled26", submonth(mon, 3, Convert.ToInt32(DateTime.Now.Year.ToString()))[1]);
        //        fields.SetField("untitled42", month1tax);
        //        fields.SetField("untitled43", month2tax);
        //        fields.SetField("untitled63", month3tax);
        //        if (!string.IsNullOrEmpty(dt1601.Rows[0][25].ToString()))
        //        {
        //            fields.SetField("untitled64", (Convert.ToDouble(month1tax) - Convert.ToDouble(dt1601.Rows[0][25].ToString())).ToString());
        //        }
        //        else
        //        {
        //            fields.SetField("untitled64", "0");
        //        }
        //        if (!string.IsNullOrEmpty(dt1601.Rows[0][26].ToString()))
        //        {
        //            fields.SetField("untitled65", (Convert.ToDouble(month2tax) - Convert.ToDouble(dt1601.Rows[0][26].ToString())).ToString());
        //        }
        //        else
        //        {
        //            fields.SetField("untitled65", "0");
        //        }
        //        if (!string.IsNullOrEmpty(dt1601.Rows[0][27].ToString()))
        //        {
        //            fields.SetField("untitled66", (Convert.ToDouble(month3tax) - Convert.ToDouble(dt1601.Rows[0][27].ToString())).ToString());
        //        }
        //        else
        //        {
        //            fields.SetField("untitled66", "0");
        //        }
        //        GovForms.GetRequestFormSettings();
        //        GovForms.Signatory(uProfile.NTID);
        //        GovForms.GetAppEMP(83, uProfile.NTID);
        //        string empidApprover = "";
        //        empidApprover = GovForms.ApproverEMPID;
        //        if (empidApprover == "" || empidApprover == null || string.IsNullOrWhiteSpace(empidApprover))
        //        {
        //        }
        //        else
        //        {
        //            GovForms.GetApproverNameJobDes(empidApprover);
        //            string Approvername = GovForms.ApproverName;
        //            string ApproverDesc = GovForms.ApproverJobDesc;
        //            GovForms.GetSignatureSignPicture(empidApprover, "sign_1");
        //            string Bitsignature = null;
        //            Bitsignature = GovForms.BitSign;
        //            Byte[] SignatureBit = null;
        //            iTextSharp.text.Image signatory = null;
        //            var pdfContentByte = stamper.GetOverContent(1);
        //            if (string.IsNullOrWhiteSpace(Bitsignature) == false)
        //            {
        //                Bitsignature = Bitsignature.Replace(' ', '+');
        //                SignatureBit = Convert.FromBase64String(Bitsignature);
        //                signatory = iTextSharp.text.Image.GetInstance(SignatureBit);
        //                signatory.SetAbsolutePosition(120, 240);
        //                signatory.ScaleToFit(50, 50);
        //                fields.SetField("untitled73", Approvername);
        //                fields.SetField("untitled80", ApproverDesc);
        //                fields.SetField("untitled77", GovForms.getBIR1601C(empidApprover).Rows[0][14].ToString());
        //                pdfContentByte.AddImage(signatory);
        //            }
        //        }

        //        stamper.FormFlattening = true;
        //        stamper.Close();
        //        uProfile.Status = "Success";

        //    }
        //    catch (Exception ex)
        //    {
        //        uProfile.Status = ex.ToString();
        //    }

        //    return uProfile.Status;

        //}
        #endregion
        #region "BIR1902"
        [HttpPost]
        [Route("BIR1902")]
        public string BIR1902(UserProfile uProfile)
        {
            string formtypes = "1902";
            string path = HttpContext.Current.Server.MapPath("pdf");
            var pdfReader1 = new PdfReader(path + "/GovForms Templates/BIR_1902.pdf");
            AcroFields af = pdfReader1.AcroFields;
            string formFile = path + "/GovForms Templates/BIR_1902.pdf";
            string newFile = path + "/GovForms-Employee/BIR1902-" + uProfile.NTID + ".pdf";

            PdfReader reader = new PdfReader(formFile);
            PdfStamper stamper = new PdfStamper(reader, new FileStream(newFile, FileMode.Create));

            AcroFields fields = stamper.AcroFields;
            try // try catch for uProfile.NTID if session expires: redirects to login page
            {
                GovForms.getEmployeePersonalInformation(uProfile.NTID);
                DataTable b1902dt = new DataTable();
                try
                {
                    con2.myparameters.Clear();
                    con2.myparameters.Add(new myParameters { ParameterName = "@uProfile.NTID", mytype = SqlDbType.NVarChar, Value = uProfile.NTID });
                    b1902dt = con2.GetDataTable("sp_SearchHRMSEmployeePersonalInformation");
                }
                catch { }
                //Name
                try
                {
                    fields.SetField("untitled21", GovForms.GetBIRFORM1902(uProfile.NTID).Rows[0][0].ToString() == "" ? "" : GovForms.GetBIRFORM1902(uProfile.NTID).Rows[0][0].ToString().First().ToString().ToUpper() + GovForms.GetBIRFORM1902(uProfile.NTID).Rows[0][0].ToString().Substring(1));
                    fields.SetField("untitled22", GovForms.GetBIRFORM1902(uProfile.NTID).Rows[0][1].ToString() == "" ? "" : GovForms.GetBIRFORM1902(uProfile.NTID).Rows[0][1].ToString().First().ToString().ToUpper() + GovForms.GetBIRFORM1902(uProfile.NTID).Rows[0][1].ToString().Substring(1));
                    fields.SetField("untitled20", GovForms.GetBIRFORM1902(uProfile.NTID).Rows[0][2].ToString() == "" ? "" : GovForms.GetBIRFORM1902(uProfile.NTID).Rows[0][2].ToString().First().ToString().ToUpper() + GovForms.GetBIRFORM1902(uProfile.NTID).Rows[0][2].ToString().Substring(1));
                }
                catch { }

                //Gender
                try
                {
                    if (GovForms.GetBIRFORM1902(uProfile.NTID).Rows[0][3].ToString() == "M")
                    {
                        fields.SetField("untitled17", "X");
                    }
                    else if (GovForms.GetBIRFORM1902(uProfile.NTID).Rows[0][3].ToString() == "F")
                    {
                        fields.SetField("untitled18", "X");
                    }
                }
                catch { }
                //Address
                try
                {
                    fields.SetField("untitled19", GovForms.GetBIRFORM1902(uProfile.NTID).Rows[0][5].ToString() == "" ? "" : GovForms.GetBIRFORM1902(uProfile.NTID).Rows[0][4].ToString().First().ToString().ToUpper() + GovForms.GetBIRFORM1902(uProfile.NTID).Rows[0][4].ToString().Substring(1));
                    fields.SetField("untitled26", GovForms.GetBIRFORM1902(uProfile.NTID).Rows[0][6].ToString() == "" ? "" : GovForms.GetBIRFORM1902(uProfile.NTID).Rows[0][6].ToString().First().ToString().ToUpper() + GovForms.GetBIRFORM1902(uProfile.NTID).Rows[0][6].ToString().Substring(1));
                    fields.SetField("untitled27", GovForms.GetBIRFORM1902(uProfile.NTID).Rows[0][7].ToString() == "" ? "" : GovForms.GetBIRFORM1902(uProfile.NTID).Rows[0][7].ToString().First().ToString().ToUpper() + GovForms.GetBIRFORM1902(uProfile.NTID).Rows[0][7].ToString().Substring(1));
                    fields.SetField("untitled28", GovForms.GetBIRFORM1902(uProfile.NTID).Rows[0][8].ToString() == "" ? "" : GovForms.GetBIRFORM1902(uProfile.NTID).Rows[0][8].ToString().First().ToString().ToUpper() + GovForms.GetBIRFORM1902(uProfile.NTID).Rows[0][8].ToString().Substring(1));
                    fields.SetField("untitled29", GovForms.GetBIRFORM1902(uProfile.NTID).Rows[0][10].ToString() == "" ? "" : GovForms.GetBIRFORM1902(uProfile.NTID).Rows[0][10].ToString().First().ToString().ToUpper() + GovForms.GetBIRFORM1902(uProfile.NTID).Rows[0][10].ToString().Substring(1));
                    fields.SetField("untitled30", GovForms.provincea.ToString().First().ToString().ToUpper() + GovForms.provincea.ToString().Substring(1));
                }
                catch { }
                //Zip Code
                try
                {
                    string zipcode = GovForms.GetBIRFORM1902(uProfile.NTID).Rows[0][11].ToString();
                    string zip = "";
                    bool zipcodee = Regex.IsMatch(zipcode, @"^\d+$");// determine if string is pure numbers; return true & null value
                    if (zipcodee.ToString() == "True")
                    {
                        for (int i = 0; i < zipcode.Length; i++)
                        {
                            if (zipcode.Length == 4)
                            {
                                zip = zip + zipcode.Substring(i, 1) + " ";
                            }
                        }
                        fields.SetFieldProperty("untitled32", "textsize", 14f, null);
                        fields.SetField("untitled32", zip);
                    }
                }
                catch { }
                //Telephone
                try
                {
                    string fulltel = "";
                    for (int h = 0; h < GovForms.GetPersonalInfo(uProfile.NTID).Rows.Count; h++)
                    {
                        fulltel = GovForms.GetPersonalInfo(uProfile.NTID).Rows[h][1].ToString() == "Home" ? getNumeric(GovForms.GetPersonalInfo(uProfile.NTID).Rows[h][0].ToString()) : "";
                    }
                    bool TELe = Regex.IsMatch(fulltel, @"^\d+$");// determine if string is pure numbers; return true & null value
                    if (TELe.ToString() == "True")
                    {
                        fields.SetFieldProperty("untitled31", "textsize", 14f, null);
                        fields.SetField("untitled31", fulltel.Substring(0, 1) + " " + fulltel.Substring(1, 1) + "  " + fulltel.Substring(2, 1) + "  " + fulltel.Substring(3, 1) + " " + fulltel.Substring(4, 1) + " " + fulltel.Substring(5, 1) + " " + fulltel.Substring(6, 1));
                    }
                }
                catch { }
                //TIN
                try
                {
                    string fullTIN = GovForms.GetBIRFORM1902(uProfile.NTID).Rows[0][12].ToString().Replace("-", "");
                    bool TINe = Regex.IsMatch(fullTIN, @"^\d+$");// determine if string is pure numbers; return true & null value
                    if (TINe.ToString() == "True")
                    {
                        if (fullTIN.Length == 9)
                        {
                            string TIN1 = fullTIN.Substring(0, 1); //First number
                            string TIN2 = fullTIN.Substring(1, 1); //2nd number
                            string TIN3 = fullTIN.Substring(2, 1); //3rd number
                            string TIN1st3 = TIN1 + " " + TIN2 + " " + TIN3;
                            fields.SetFieldProperty("untitled12", "textsize", 15f, null);
                            fields.SetField("untitled12", TIN1st3);
                            string TIN4 = fullTIN.Substring(3, 1);
                            string TIN5 = fullTIN.Substring(4, 1);
                            string TIN6 = fullTIN.Substring(5, 1);
                            string TIN2nd3 = TIN4 + " " + TIN5 + " " + TIN6;
                            fields.SetFieldProperty("untitled13", "textsize", 15f, null);
                            fields.SetField("untitled13", TIN2nd3);
                            string TIN7 = fullTIN.Substring(6, 1);
                            string TIN8 = fullTIN.Substring(7, 1);
                            string TIN9 = fullTIN.Substring(8, 1);
                            string TIN3rd3 = TIN7 + " " + TIN8 + " " + TIN9;
                            fields.SetFieldProperty("untitled14", "textsize", 15f, null);
                            fields.SetField("untitled14", TIN3rd3);
                        }
                    }
                }
                catch { }
                //Tax Status
                try
                {
                    string taxstat = GovForms.GetTaxStatus(uProfile.NTID).Rows[0][0].ToString();
                    if (taxstat == "Single")
                    {
                        fields.SetField("untitled36", "X");
                    }
                    else if (taxstat == "Married")
                    {
                        fields.SetField("untitled40", "X");
                    }
                }
                catch { }
                //Employer TIN
                try
                {
                    string fullBTIN = GovForms.getPagibigMPL(uProfile.NTID).Rows[0][41].ToString().Replace("-", "");
                    var pBTIN = fullBTIN.ToCharArray();
                    fields.SetFieldProperty("untitled111", "textsize", 13f, null);
                    fields.SetField("untitled111", pBTIN[0] + " " + pBTIN[1] + " " + pBTIN[2]);
                    fields.SetFieldProperty("untitled112", "textsize", 13f, null);
                    fields.SetField("untitled112", pBTIN[3] + " " + pBTIN[4] + " " + pBTIN[5]);
                    fields.SetFieldProperty("untitled113", "textsize", 13f, null);
                    fields.SetField("untitled113", pBTIN[6] + " " + pBTIN[7] + " " + pBTIN[8]);
                }
                catch { }
                //Employer Address
                try
                {
                    fields.SetField("untitled116", GovForms.getPagibigMPL(uProfile.NTID).Rows[0][38].ToString().ToUpper());
                    string regadd = GovForms.getPagibigMPL(uProfile.NTID).Rows[0][32].ToString().ToUpper() + " " + GovForms.getPagibigMPL(uProfile.NTID).Rows[0][27].ToString().ToUpper() + " " + GovForms.getPagibigMPL(uProfile.NTID).Rows[0][26].ToString().ToUpper() + " " + GovForms.getPagibigMPL(uProfile.NTID).Rows[0][30].ToString().ToUpper() + " " + GovForms.getPagibigMPL(uProfile.NTID).Rows[0][28].ToString().ToUpper();
                    fields.SetField("untitled119", regadd);
                    string regzip = GovForms.getPagibigMPL(uProfile.NTID).Rows[0][33].ToString();
                    var bdzip = regzip.ToCharArray();
                    fields.SetFieldProperty("untitled120", "textsize", 13f, null);
                    fields.SetField("untitled120", bdzip[0] + " " + bdzip[1] + " " + bdzip[2] + " " + bdzip[3]);
                }
                catch { }
                //branch
                try
                {

                    string branchname = GovForms.getCompanyProfile(uProfile.NTID).Rows[0][1].ToString();
                    if (branchname == "Head Quarters")
                    {
                        fields.SetField("untitled109", "X");
                    }
                    else if (branchname == "Branch")
                    {
                        fields.SetField("untitled110", "X");
                    }
                    fields.SetField("untitled128", getNumeric(GovForms.GetCompanyContactPhone(GovForms.getCompanyProfile(uProfile.NTID).Rows[0][0].ToString()).Rows[0][1].ToString()));
                }
                catch { }
                //Civil Status
                string cvstat = b1902dt.Rows[0][9].ToString();
                switch (cvstat)
                {
                    case "Single":
                        fields.SetField("untitled36", "X");
                        break;
                    case "Married":
                        fields.SetField("untitled40", "X");
                        break;
                    case "Widowed":
                        fields.SetField("untitled37", "X");
                        break;
                    case "Legally Separated":
                        fields.SetField("untitled39", "X");
                        break;
                }

            }
            //
            catch (Exception ex)
            {
                //this.Page.ClientScript.RegisterClientScriptBlock(this.GetType(),
                //Guid.NewGuid().ToString("N"), "self.parent.location='login.aspx';", true);
            }
            try
            {
                string s = "";
                string month = "";
                string day = "";
                string dayy = "";
                string year = "";
                string y1 = "";
                if (GovForms.GetLoyaltyCard(uProfile.NTID).Rows[0][6].ToString() != "")
                {
                    s = Convert.ToDateTime(GovForms.GetLoyaltyCard(uProfile.NTID).Rows[0][6]).ToString("yyyy-MM-dd");
                    month = s.Split('-')[1];

                    day = s.Split('-')[2];
                    int day1 = Convert.ToInt32(day.Remove(day.Length - 1));
                    dayy = day.Substring(0, 1) + " " + day.Substring(1, 1);

                    year = s.Split(' ')[0];
                    y1 = year.Substring(0, 1) + " " + year.Substring(1, 1) + " " + year.Substring(2, 1) + " " + year.Substring(3, 1);

                }

                fields.SetFieldProperty("untitled23", "textsize", 13f, null);
                fields.SetField("untitled23", month.ToString() == "" ? "" : month.Substring(0, 1) + " " + month.Substring(1, 1));


                fields.SetFieldProperty("untitled24", "textsize", 13f, null);
                fields.SetField("untitled24", dayy.ToString() == "" ? "" : dayy);

                fields.SetFieldProperty("untitled25", "textsize", 13f, null);
                fields.SetField("untitled25", y1.ToString() == "" ? "" : y1);

                GovForms.GetRequestFormSettings();
                GovForms.SignatoryNew(uProfile.NTID, formtypes);
                GovForms.GetAppEMP(84, uProfile.NTID);
                string empidApprover = "";
                empidApprover = GovForms.ApproverEMPID;
                if (empidApprover == "" || empidApprover == null || string.IsNullOrWhiteSpace(empidApprover))
                {
                }
                else
                {
                    GovForms.GetApproverNameJobDes(empidApprover);
                    string Approvername = GovForms.ApproverName;
                    string ApproverDesc = GovForms.ApproverJobDesc;
                    GovForms.GetSignatureSignPicture(empidApprover, "sign_1");
                    string Bitsignature = null;
                    Bitsignature = GovForms.BitSign;
                    Byte[] SignatureBit = null;
                    iTextSharp.text.Image signatory = null;
                    iTextSharp.text.Image signatory1 = null;
                    var pdfContentByte = stamper.GetOverContent(1);
                    if (string.IsNullOrWhiteSpace(Bitsignature) == false)
                    {
                        Bitsignature = Bitsignature.Replace(' ', '+');
                        SignatureBit = Convert.FromBase64String(Bitsignature);
                        signatory = iTextSharp.text.Image.GetInstance(SignatureBit);
                        signatory1 = iTextSharp.text.Image.GetInstance(SignatureBit);
                        signatory.SetAbsolutePosition(80, 100);
                        signatory.ScaleToFit(100, 100);
                        signatory1.SetAbsolutePosition(120, 280);
                        signatory1.ScaleToFit(100, 100);
                        fields.SetField("untitled107", Approvername);
                        fields.SetField("untitled129", Approvername);
                        fields.SetField("untitled131", ApproverDesc);
                        pdfContentByte.AddImage(signatory);
                        pdfContentByte.AddImage(signatory1);
                    }
                }


                stamper.FormFlattening = true;
                stamper.Close();
                uProfile.Status = "Success";
            }
            catch (Exception ex)
            {
                uProfile.Status = ex.ToString();
            }
            return uProfile.Status;

        }
        #endregion
        #region "BIR_1905"
        [HttpPost]
        [Route("BIR_1905")]
        public string BIR_1905(UserProfile uProfile)
        {
            string formtype = "1905";
            string path = HttpContext.Current.Server.MapPath("pdf");
            var pdfReader1 = new PdfReader(path + "/GovForms Templates/BIR_1905.pdf");
            AcroFields af = pdfReader1.AcroFields;
            AcroFields.Item item;
            string formFile = path + "/GovForms Templates/BIR_1905.pdf";
            string newFile = path + "/GovForms-Employee/BIR_1905-" + uProfile.NTID + ".pdf";

            PdfReader reader = new PdfReader(formFile);
            PdfStamper stamper = new PdfStamper(reader, new FileStream(newFile, FileMode.Create));
            AcroFields fields = stamper.AcroFields;
            try // try catch for uProfile.NTID if session expires: redirects to login page
            {
                try
                {
                    string fullTIN = GovForms.GetBIRFORM1905(uProfile.NTID).Rows[0][3].ToString().Replace("-", "");
                    bool TINe = Regex.IsMatch(fullTIN, @"^\d+$");// determine if string is pure numbers; return true & null value
                    if (TINe.ToString() == "True")
                    {
                        if (fullTIN.Length == 9)
                        {
                            string TIN1 = fullTIN.Substring(0, 1); //First number
                            string TIN2 = fullTIN.Substring(1, 1); //2nd number
                            string TIN3 = fullTIN.Substring(2, 1); //3rd number
                            string TIN1st3 = TIN1 + " " + TIN2 + " " + TIN3;
                            fields.SetFieldProperty("untitled1", "textsize", 15f, null);
                            string TIN4 = fullTIN.Substring(3, 1);
                            string TIN5 = fullTIN.Substring(4, 1);
                            string TIN6 = fullTIN.Substring(5, 1);
                            string TIN2nd3 = TIN4 + " " + TIN5 + " " + TIN6;
                            fields.SetFieldProperty("untitled2", "textsize", 15f, null);
                            string TIN7 = fullTIN.Substring(6, 1);
                            string TIN8 = fullTIN.Substring(7, 1);
                            string TIN9 = fullTIN.Substring(8, 1);
                            string TIN3rd3 = TIN7 + " " + TIN8 + " " + TIN9;
                            fields.SetFieldProperty("untitled3", "textsize", 15f, null);
                            fields.SetField("untitled1", TIN1st3);
                            fields.SetField("untitled2", TIN2nd3);
                            fields.SetField("untitled3", TIN3rd3);
                        }
                    }
                }
                catch { }
                //Name
                try
                {
                    fields.SetFieldProperty("untitled6", "textsize", 12f, null);
                    fields.SetField("untitled6", GovForms.GetBIRFORM1905(uProfile.NTID).Rows[0][0].ToString() == "" ? "" : GovForms.GetBIRFORM1905(uProfile.NTID).Rows[0][0].ToString().First().ToString().ToUpper() + GovForms.GetBIRFORM1905(uProfile.NTID).Rows[0][0].ToString().Substring(1));
                    fields.SetFieldProperty("untitled7", "textsize", 12f, null);
                    fields.SetField("untitled7", GovForms.GetBIRFORM1905(uProfile.NTID).Rows[0][1].ToString() == "" ? "" : GovForms.GetBIRFORM1905(uProfile.NTID).Rows[0][1].ToString().First().ToString().ToUpper() + GovForms.GetBIRFORM1905(uProfile.NTID).Rows[0][1].ToString().Substring(1));
                    fields.SetFieldProperty("untitled8", "textsize", 12f, null);
                    fields.SetField("untitled8", GovForms.GetBIRFORM1905(uProfile.NTID).Rows[0][2].ToString() == "" ? "" : GovForms.GetBIRFORM1905(uProfile.NTID).Rows[0][2].ToString().First().ToString().ToUpper() + GovForms.GetBIRFORM1905(uProfile.NTID).Rows[0][2].ToString().Substring(1));
                }
                catch { }
                //RDO
                try
                {
                    item = fields.GetFieldItem("untitled5");
                    item.GetMerged(0).Put(PdfName.Q, new PdfNumber(PdfFormField.Q_RIGHT));

                    fields.SetField("untitled5", GovForms.getCompanyProfile(uProfile.NTID).Rows[0][63].ToString());
                }
                catch { }

                GovForms.GetRequestFormSettings();
                GovForms.SignatoryNew(uProfile.NTID, formtype);
                GovForms.GetAppEMP(85, uProfile.NTID);
                string empidApprover = "";
                empidApprover = GovForms.ApproverEMPID;
                if (empidApprover == "" || empidApprover == null || string.IsNullOrWhiteSpace(empidApprover))
                {
                }
                else
                {
                    GovForms.GetApproverNameJobDes(empidApprover);
                    string Approvername = GovForms.ApproverName;
                    string ApproverDesc = GovForms.ApproverJobDesc;
                    GovForms.GetSignatureSignPicture(empidApprover, "sign_1");
                    string Bitsignature = null;
                    Bitsignature = GovForms.BitSign;
                    Byte[] SignatureBit = null;
                    iTextSharp.text.Image signatory = null;
                    var pdfContentByte = stamper.GetOverContent(2);
                    if (string.IsNullOrWhiteSpace(Bitsignature) == false)
                    {
                        Bitsignature = Bitsignature.Replace(' ', '+');
                        SignatureBit = Convert.FromBase64String(Bitsignature);
                        signatory = iTextSharp.text.Image.GetInstance(SignatureBit);
                        signatory.SetAbsolutePosition(50, 420);
                        signatory.ScaleToFit(100, 100);
                        fields.SetField("untitled163", Approvername);
                        fields.SetField("untitled165", ApproverDesc);
                        pdfContentByte.AddImage(signatory);
                    }
                }
                stamper.FormFlattening = true;
                stamper.Close();
                uProfile.Status = "Success";
            }
            catch (Exception ex)
            {
                uProfile.Status = ex.ToString();
            }


            return uProfile.Status;
        }
        #endregion

        #region "BIR_2305"
        [HttpPost]
        [Route("BIR_2305")]
        public string BIR_2305(UserProfile uProfile)
        {
            string formtype = "2305";
            string path = HttpContext.Current.Server.MapPath("pdf");
            var pdfReader1 = new PdfReader(path + "/GovForms Templates/BIR_2305.pdf");
            AcroFields af = pdfReader1.AcroFields;
            AcroFields.Item item;
            string formFile = path + "/GovForms Templates/BIR_2305.pdf";
            string newFile = path + "/GovForms-Employee/BIR_2305-" + uProfile.NTID + ".pdf";

            PdfReader reader = new PdfReader(formFile);
            PdfStamper stamper = new PdfStamper(reader, new FileStream(newFile, FileMode.Create));
            AcroFields fields = stamper.AcroFields;
            try
            {
                GovForms.getEmployeePersonalInformation(uProfile.NTID);
                string fullname = GovForms.getPagibigMPL(uProfile.NTID).Rows[0][5].ToString().ToUpper() + " " + GovForms.getPagibigMPL(uProfile.NTID).Rows[0][4].ToString().ToUpper() + " " + GovForms.getPagibigMPL(uProfile.NTID).Rows[0][6].ToString().ToUpper();
                fields.SetField("untitled1", "X");
                try
                {
                    string fullTIN = GovForms.getPagibigMPL(uProfile.NTID).Rows[0][2].ToString().Replace("-", "");
                    string TIN1 = fullTIN.Substring(0, 1); //First number
                    string TIN2 = fullTIN.Substring(1, 1); //2nd number
                    string TIN3 = fullTIN.Substring(2, 1); //3rd number
                    string TIN1st3 = TIN1 + " " + TIN2 + " " + TIN3;
                    fields.SetFieldProperty("untitled6", "textsize", 15f, null);
                    string TIN4 = fullTIN.Substring(3, 1);
                    string TIN5 = fullTIN.Substring(4, 1);
                    string TIN6 = fullTIN.Substring(5, 1);
                    string TIN2nd3 = TIN4 + " " + TIN5 + " " + TIN6;
                    fields.SetFieldProperty("untitled7", "textsize", 15f, null);
                    string TIN7 = fullTIN.Substring(6, 1);
                    string TIN8 = fullTIN.Substring(7, 1);
                    string TIN9 = fullTIN.Substring(8, 1);
                    string TIN3rd3 = TIN7 + " " + TIN8 + " " + TIN9;
                    fields.SetFieldProperty("untitled8", "textsize", 15f, null);
                    fields.SetField("untitled6", TIN1st3);
                    fields.SetField("untitled7", TIN2nd3);
                    fields.SetField("untitled8", TIN3rd3);
                    string msex = GovForms.getPagibigMPL(uProfile.NTID).Rows[0][12].ToString();
                    if (msex == "Male" || msex == "M")
                    {
                        fields.SetField("untitled11", "X");
                    }
                    else
                    {
                        fields.SetField("untitled12", "X");
                    }
                }
                catch { }
                //RDO Code
                try
                {
                    string rdo = GovForms.getCompanyProfile(uProfile.NTID).Rows[0][63].ToString();
                    item = fields.GetFieldItem("untitled10");
                    item.GetMerged(0).Put(PdfName.Q, new PdfNumber(PdfFormField.Q_RIGHT));
                    fields.SetField("untitled10", rdo);
                }
                catch { }
                //DOB
                try
                {
                    fields.SetField("untitled13", fullname);
                    string bdayraw = GovForms.getPagibigMPL(uProfile.NTID).Rows[0][11].ToString();
                    string[] bdayarr = bdayraw.Split('-');

                    char[] dob1 = bdayarr[1].ToCharArray();
                    char[] dob2 = bdayarr[2].ToCharArray();
                    char[] dob3 = bdayarr[0].ToCharArray();
                    fields.SetFieldProperty("untitled14", "textsize", 10f, null);
                    fields.SetFieldProperty("untitled15", "textsize", 10f, null);
                    fields.SetFieldProperty("untitled16", "textsize", 10f, null);
                    fields.SetField("untitled14", dob1[0] + "  " + dob1[1]);
                    fields.SetField("untitled15", dob2[0] + "  " + dob2[1]);
                    fields.SetField("untitled16", dob3[0] + "  " + dob3[1] + "  " + dob3[2] + "  " + dob3[3]);
                }
                catch { }
                //ZIP
                try
                {
                    fields.SetField("untitled17", GovForms.getPagibigMPL(uProfile.NTID).Rows[0][15].ToString().ToUpper() + " " + GovForms.getPagibigMPL(uProfile.NTID).Rows[0][21].ToString().ToUpper() + " " + GovForms.getPagibigMPL(uProfile.NTID).Rows[0][20].ToString().ToUpper() + " " + GovForms.getPagibigMPL(uProfile.NTID).Rows[0][7].ToString().ToUpper() + " " + GovForms.getPagibigMPL(uProfile.NTID).Rows[0][9].ToString().ToUpper() + " " + GovForms.provincea.ToString().ToUpper());
                    string zipraw = GovForms.getPagibigMPL(uProfile.NTID).Rows[0][36].ToString();
                    string zip1 = zipraw.Substring(0, 1);
                    string zip2 = zipraw.Substring(1, 1);
                    string zip3 = zipraw.Substring(2, 1);
                    string zip4 = zipraw.Substring(3, 1);
                    string zipfinal = zip1 + "  " + zip2 + "  " + zip3 + "  " + zip4;
                    fields.SetFieldProperty("untitled18", "textsize", 12f, null);
                    fields.SetField("untitled18", zipfinal);
                }
                catch { }
                string fullname1 = GovForms.getPagibigMPL(uProfile.NTID).Rows[0][4].ToString().ToUpper() + " " + GovForms.getPagibigMPL(uProfile.NTID).Rows[0][5].ToString().ToUpper() + " " + GovForms.getPagibigMPL(uProfile.NTID).Rows[0][6].ToString().ToUpper();
                item = fields.GetFieldItem("untitled21");
                item.GetMerged(0).Put(PdfName.Q, new PdfNumber(PdfFormField.Q_CENTER));
                fields.SetField("untitled21", fullname1);
                try
                {
                    string fullTIN = GovForms.getPagibigMPL(uProfile.NTID).Rows[0][41].ToString().Replace("-", "");
                    string TIN1 = fullTIN.Substring(0, 1); //First number
                    string TIN2 = fullTIN.Substring(1, 1); //2nd number
                    string TIN3 = fullTIN.Substring(2, 1); //3rd number
                    string TIN1st3 = TIN1 + " " + TIN2 + " " + TIN3;
                    fields.SetFieldProperty("untitled90", "textsize", 15f, null);
                    string TIN4 = fullTIN.Substring(3, 1);
                    string TIN5 = fullTIN.Substring(4, 1);
                    string TIN6 = fullTIN.Substring(5, 1);
                    string TIN2nd3 = TIN4 + " " + TIN5 + " " + TIN6;
                    fields.SetFieldProperty("untitled91", "textsize", 15f, null);
                    string TIN7 = fullTIN.Substring(6, 1);
                    string TIN8 = fullTIN.Substring(7, 1);
                    string TIN9 = fullTIN.Substring(8, 1);
                    string TIN3rd3 = TIN7 + " " + TIN8 + " " + TIN9;
                    fields.SetFieldProperty("untitled92", "textsize", 15f, null);
                    if (fullTIN.Length == 12)
                    {
                        string TIN10 = fullTIN.Substring(9, 1);
                        string TIN11 = fullTIN.Substring(10, 1);
                        string TIN12 = fullTIN.Substring(11, 1);
                        string TIN4th3 = TIN10 + " " + TIN11 + " " + TIN12;
                        fields.SetFieldProperty("untitled93", "textsize", 15f, null);
                        fields.SetField("untitled46", TIN4th3);
                    }
                    else
                    {
                        fields.SetFieldProperty("untitled93", "textsize", 15f, null);
                        fields.SetField("untitled93", "0 0 0 0");
                    }
                    fields.SetField("untitled90", TIN1st3);
                    fields.SetField("untitled91", TIN2nd3);
                    fields.SetField("untitled92", TIN3rd3);
                }
                catch { }
                //address
                try
                {
                    fields.SetField("untitled95", GovForms.getPagibigMPL(uProfile.NTID).Rows[0][38].ToString().ToUpper());
                    string nobldg = GovForms.getPagibigMPL(uProfile.NTID).Rows[0][32].ToString().ToUpper() + " " + GovForms.getPagibigMPL(uProfile.NTID).Rows[0][27].ToString().ToUpper();
                    fields.SetField("untitled97", nobldg);
                    fields.SetField("untitled100", GovForms.getPagibigMPL(uProfile.NTID).Rows[0][26].ToString().ToUpper());
                    fields.SetField("untitled101", GovForms.getPagibigMPL(uProfile.NTID).Rows[0][30].ToString().ToUpper());
                    fields.SetField("untitled102", GovForms.getPagibigMPL(uProfile.NTID).Rows[0][28].ToString().ToUpper());
                    fields.SetField("untitled103", GovForms.getPagibigMPL(uProfile.NTID).Rows[0][33].ToString());
                }
                catch { }

                GovForms.GetRequestFormSettings();
                GovForms.SignatoryNew(uProfile.NTID, formtype);
                GovForms.GetAppEMP(86, uProfile.NTID);
                string empidApprover = "";
                empidApprover = GovForms.ApproverEMPID;
                if (empidApprover == "" || empidApprover == null || string.IsNullOrWhiteSpace(empidApprover))
                {
                }
                else
                {
                    GovForms.GetApproverNameJobDes(empidApprover);
                    string Approvername = GovForms.ApproverName;
                    string ApproverDesc = GovForms.ApproverJobDesc;
                    GovForms.GetSignatureSignPicture(empidApprover, "sign_1");
                    string Bitsignature = null;
                    Bitsignature = GovForms.BitSign;
                    Byte[] SignatureBit = null;
                    iTextSharp.text.Image signatory = null;
                    var pdfContentByte = stamper.GetOverContent(1);
                    if (string.IsNullOrWhiteSpace(Bitsignature) == false)
                    {
                        Bitsignature = Bitsignature.Replace(' ', '+');
                        SignatureBit = Convert.FromBase64String(Bitsignature);
                        signatory = iTextSharp.text.Image.GetInstance(SignatureBit);
                        signatory.SetAbsolutePosition(50, 100);
                        signatory.ScaleToFit(50, 50);
                        fields.SetField("untitled107", Approvername);
                        fields.SetField("untitled21", Approvername);
                        fields.SetField("untitled109", ApproverDesc);
                        pdfContentByte.AddImage(signatory);
                    }
                }


                stamper.FormFlattening = true;
                stamper.Close();
                uProfile.Status = "Success";
            }
            catch (Exception ex)
            {
                uProfile.Status = ex.ToString();
            }

            return uProfile.Status;

        }
        #endregion
        #region "2316"
        [HttpPost]
        [Route("2316v3")]
        public string _2316(UserProfilev2 uProfile)
        {
            string formtype = "2316";
            string yr = DateTime.Now.ToString("yyyy");
            string month1 = getmonth("January");
            string month2 = getmonth(DateTime.Now.ToString("MMMM"));
            //Response.Redirect("~/pdf/BIR19021.pdf"); // view pdf
            string path = HttpContext.Current.Server.MapPath("pdf");
            var pdfReader1 = new PdfReader(path + "/GovForms Templates/2316.pdf");
            AcroFields af = pdfReader1.AcroFields;
            string empids = uProfile.NTID;

            // Template file path
            string formFile = path + "/GovForms Templates/2316.pdf";

            // Output file path
            string newFile = path + "/GovForms-Employee/2316-" + uProfile.NTID + ".pdf";


            // read the template file
            PdfReader reader = new PdfReader(formFile);


            // instantiate PDFStamper object
            // The Output file will be created from template file and edited by the PDFStamper
            PdfStamper stamper = new PdfStamper(reader, new FileStream(
                        newFile, FileMode.Create));


            // Object to deal with the Output file's textfields
            AcroFields fields = stamper.AcroFields;



            // set form fields("Field Name in PDF", "String to be placed in this PDF text field");
            // for (int i = 0; i <= 200; i++)
            //  {
            //string ii = i.ToString();
            //fields.SetField("untitled" + i, ii); //temporary values
            string housenuma, maritala, streetnamea, barangaya, towna, citya, provincea = "", religiona, zipcodea, bloodtypea, gendera, maritalstata, doba, citizenshipa, nameofa, mobileareacodea,
                mobilenumbera, homeeracodea, homenumbera, personalemaila, emrgnamea, emrgnumbera, emrgrelationshipa, optionaemaila, contacta, pob, religion, mothername, fathername,
                payrollinfo, bankname, branchname, accountnumber, height, weight, wemail, extension, FNAMEEMRG, MNAMEEMRG, LNAMEEMRG, DateJoined, ProductionStartDate, Wallet, Tenure,
                TenureMonths, Batch, Class, Skill, JobDesc, Promotion, NameOfOrganization, MDATE, gPreffix, gSuffix, UserCountry;

            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = uProfile.CN;
            SelfieRegistration2Controller.GetDB2(GDB);

            Connection con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = empids });
            DataTable getEmployeePersonalInformation = con.GetDataTable("sp_SearchHRMSEmployeePersonalInformation");
            foreach (DataRow row in getEmployeePersonalInformation.Rows)
            {
                housenuma = row["HouseNumber"].ToString();
                streetnamea = row["StreetName"].ToString();
                barangaya = row["Barangay"].ToString();
                towna = row["Town"].ToString();
                citya = row["City"].ToString();
                religiona = row["Region"].ToString();
                zipcodea = row["ZipCode"].ToString();
                bloodtypea = row["BloodType"].ToString();
                gendera = row["Gender"].ToString();
                maritalstata = row["MaritalStatus"].ToString();
                if (string.IsNullOrWhiteSpace(row["DOB"].ToString()) == true)
                {
                    doba = "";
                }
                else
                {
                    doba = Convert.ToDateTime(row["DOB"]).ToString("yyyy-MM-dd");
                }

                citizenshipa = row["Citizenship"].ToString();
                nameofa = row["NameOfOrganization"].ToString();
                mobileareacodea = row["MobileAreaCode"].ToString();
                mobilenumbera = row["MobileNumber"].ToString();
                homeeracodea = row["HomeAreaCode"].ToString();
                homenumbera = row["HomeNumber"].ToString();
                personalemaila = row["Email"].ToString();
                wemail = row["WorkEmail"].ToString();
                extension = row["Extension"].ToString();
                emrgnamea = row["EmrgName"].ToString();
                emrgnumbera = row["EmrgNumber"].ToString();
                emrgrelationshipa = row["EmrgRelationship"].ToString();
                optionaemaila = row["OptionalEmail"].ToString();
                contacta = row["ContactNo"].ToString();
                provincea = row["Province"].ToString();
                FNAMEEMRG = row["EmrgFname"].ToString();
                MNAMEEMRG = row["EmrgMname"].ToString();
                LNAMEEMRG = row["EmrgLname"].ToString();
                DateJoined = row["DateJoined"].ToString();
                ProductionStartDate = row["ProductionStartDate"].ToString();
                Wallet = row["Wallet"].ToString();
                Tenure = row["Tenure"].ToString();
                TenureMonths = row["TenureMonths"].ToString();
                Batch = row["Batch"].ToString();
                Class = row["Class"].ToString();
                Skill = row["Skill"].ToString();
                JobDesc = row["JobDesc"].ToString();
                Promotion = row["Promotion"].ToString();
                NameOfOrganization = row["NameOfOrganization"].ToString();
                maritala = row["MaritalStatus"].ToString();
                pob = row["POB"].ToString();
                religion = row["religion"].ToString();
                mothername = row["MotherName"].ToString();
                fathername = row["FatherName"].ToString();
                payrollinfo = row["Payrollinfo"].ToString();
                bankname = row["BankName"].ToString();
                branchname = (row["BranchName"].ToString());
                accountnumber = row["AccountNumber"].ToString();
                height = row["Height"].ToString();
                weight = row["Weight"].ToString();
                MDATE = row["MarriedDate"].ToString();
                gPreffix = row["Preffix"].ToString();
                gSuffix = row["Suffix"].ToString();
                UserCountry = row["Country"].ToString();
            }

            try
            {
                //sign.getEmployeePersonalInformation(empids);
                //Period Covered

                try
                {
                    GDB = new GetDatabase();
                    GDB.ClientName = uProfile.CN;
                    SelfieRegistration2Controller.GetDB2(GDB);

                    con = new Connection();
                    con.myparameters.Add(new myParameters { ParameterName = "@empid", mytype = SqlDbType.NVarChar, Value = empids });
                    DataTable DT = con.GetDataTable("sp_BIRFORM2316");
                    if (DT.Rows.Count > 0)
                    {
                        #region TODO 2316FIELDS
                        try
                        {
                            string yrchar = DT.Rows[0][0].ToString();
                            fields.SetFieldProperty("untitled1", "textsize", 12f, null);
                            fields.SetField("untitled1", yrchar.Substring(0, 1) + " " + yrchar.Substring(1, 1) + " " + yrchar.Substring(2, 1) + " " + yrchar.Substring(3, 1));

                            string period1 = DT.Rows[0][1].ToString().Replace(" ", "");
                            fields.SetField("untitled2", period1.Substring(0, 1) + "  " + period1.Substring(1, 1) + "   " + period1.Substring(2, 1) + "   " + period1.Substring(3, 1));
                            string period2 = DT.Rows[0][2].ToString().Replace(" ", "");
                            fields.SetField("untitled3", period2.Substring(0, 1) + "  " + period2.Substring(1, 1) + "   " + period2.Substring(2, 1) + "   " + period2.Substring(3, 1));

                            string fullTIN = DT.Rows[0][3].ToString().Replace(" ", "");
                            string TIN1 = fullTIN.Substring(0, 1); //First number
                            string TIN2 = fullTIN.Substring(1, 1); //2nd number
                            string TIN3 = fullTIN.Substring(2, 1); //3rd number
                            string TIN1st3 = TIN1 + " " + TIN2 + " " + TIN3;
                            fields.SetFieldProperty("untitled4", "textsize", 12f, null);

                            string TIN4 = fullTIN.Substring(3, 1);
                            string TIN5 = fullTIN.Substring(4, 1);
                            string TIN6 = fullTIN.Substring(5, 1);
                            string TIN2nd3 = TIN4 + " " + TIN5 + " " + TIN6;
                            fields.SetFieldProperty("untitled5", "textsize", 12f, null);

                            string TIN7 = fullTIN.Substring(6, 1);
                            string TIN8 = fullTIN.Substring(7, 1);
                            string TIN9 = fullTIN.Substring(8, 1);
                            string TIN3rd3 = TIN7 + " " + TIN8 + " " + TIN9;
                            fields.SetFieldProperty("untitled6", "textsize", 12f, null);

                            if (fullTIN.Length >= 12)
                            {
                                string TIN10 = fullTIN.Substring(9, 1);
                                string TIN11 = fullTIN.Substring(10, 1);
                                string TIN12 = fullTIN.Substring(11, 1);
                                string TIN13 = fullTIN.Substring(12, 1);
                                string TIN4th3 = TIN10 + " " + TIN11 + " " + TIN12 + " " + TIN13;
                                fields.SetFieldProperty("untitled7", "textsize", 12f, null);
                                fields.SetField("untitled7", TIN4th3);
                            }
                            fields.SetField("untitled4", TIN1st3);
                            fields.SetField("untitled5", TIN2nd3);
                            fields.SetField("untitled6", TIN3rd3);
                        }
                        catch { }

                        string fullname = DT.Rows[0][5].ToString().ToUpper() + " " + DT.Rows[0][6].ToString().ToUpper() + " " + DT.Rows[0][7].ToString().ToUpper();
                        string signatoryname = DT.Rows[0][6].ToString().ToUpper() + " " + DT.Rows[0][7].ToString().ToUpper() + " " + DT.Rows[0][5].ToString().ToUpper();
                        try
                        {

                            fields.SetFieldProperty("untitled8", "setfflags", PdfFormField.FF_READ_ONLY, null);
                            fields.SetField("untitled8", fullname);
                            fields.SetField("untitled100", signatoryname);
                            fields.SetField("untitled104", signatoryname);
                        }
                        catch
                        {

                        }

                        try
                        {
                            string rdo = DT.Rows[0][8].ToString();
                            switch (rdo.Length)
                            {
                                case 1:
                                    {
                                        rdo = "             " + rdo;
                                        break;
                                    }
                                case 2:
                                    {
                                        rdo = "         " + rdo;
                                        break;
                                    }
                                default:
                                    {
                                        break;
                                    }
                            }

                            fields.SetFieldProperty("untitled12", "textsize", 10f, null);
                            fields.SetField("untitled12", rdo);

                            string regadd = DT.Rows[0][9].ToString().ToUpper();
                            fields.SetFieldProperty("untitled9", "textsize", 4.6f, null);
                            fields.SetField("untitled9", regadd);
                        }
                        catch { }

                        try
                        {
                            string locadd = DT.Rows[0][11].ToString().ToUpper();
                            fields.SetFieldProperty("untitled10", "textsize", 4.6f, null);
                            fields.SetField("untitled10", locadd);
                            string zipraw = DT.Rows[0][12].ToString();
                            string zip1 = zipraw.Substring(0, 1);
                            string zip2 = zipraw.Substring(1, 1);
                            string zip3 = zipraw.Substring(2, 1);
                            string zip4 = zipraw.Substring(3, 1);
                            string zipfinal = zip1 + "  " + zip2 + "  " + zip3 + "  " + zip4;
                            fields.SetFieldProperty("untitled14", "textsize", 10f, null);
                            fields.SetField("untitled14", zipfinal);
                        }
                        catch { }

                        try
                        {
                            string zipraw = DT.Rows[0][10].ToString();
                            string zip1 = zipraw.Substring(0, 1);
                            string zip2 = zipraw.Substring(1, 1);
                            string zip3 = zipraw.Substring(2, 1);
                            string zip4 = zipraw.Substring(3, 1);
                            string zipfinal = zip1 + "  " + zip2 + "  " + zip3 + "  " + zip4;
                            fields.SetFieldProperty("untitled13", "textsize", 10f, null);
                            fields.SetField("untitled13", zipfinal);
                        }
                        catch { }


                        try
                        {
                            string foreignadd = DT.Rows[0][13].ToString();
                            fields.SetFieldProperty("untitled11", "textsize", 4.6f, null);
                            fields.SetField("untitled11", foreignadd);
                            string zipraw = DT.Rows[0][14].ToString();
                            string zip1 = zipraw.Substring(0, 1);
                            string zip2 = zipraw.Substring(1, 1);
                            string zip3 = zipraw.Substring(2, 1);
                            string zip4 = zipraw.Substring(3, 1);
                            string zipfinal = zip1 + "  " + zip2 + "  " + zip3 + "  " + zip4;
                            fields.SetFieldProperty("untitled15", "textsize", 10f, null);
                            fields.SetField("untitled15", zipfinal);
                        }
                        catch { }

                        try
                        {
                            string bdayraw = DT.Rows[0][15].ToString();
                            string[] bdayarr = bdayraw.Split('-');

                            //add 0 if date is single digit
                            string date = bdayarr[1];
                            fields.SetFieldProperty("untitled21", "textsize", 10f, null);
                            fields.SetFieldProperty("untitled22", "textsize", 10f, null);
                            fields.SetFieldProperty("untitled23", "textsize", 10f, null);
                            fields.SetField("untitled21", bdayarr[2].Substring(0, 1) + " " + bdayarr[2].Substring(1, 1));
                            fields.SetField("untitled22", date.Substring(0, 1) + " " + date.Substring(1, 1));
                            fields.SetField("untitled23", bdayarr[0].Substring(0, 1) + "   " + bdayarr[0].Substring(1, 1) + "   " + bdayarr[0].Substring(2, 1) + "   " + bdayarr[0].Substring(3, 1));

                            string telnumraw = DT.Rows[0][16].ToString();
                            fields.SetField("untitled16", telnumraw);

                            string taxstat = DT.Rows[0][17].ToString();
                            //Single
                            if (taxstat == "single")
                            {
                                fields.SetField("untitled24", "X");
                            }

                            //Married
                            else if (taxstat == "married")
                            {
                                fields.SetField("untitled26", "X");
                            }

                            string claimexemp = DT.Rows[0][18].ToString();
                            //Single
                            if (claimexemp == "yes")
                            {
                                fields.SetField("untitled25", "X");
                            }

                            //Married
                            else if (claimexemp == "no")
                            {
                                fields.SetField("untitled27", "X");
                            }
                        }
                        catch { }
                        try
                        {
                            string child1 = DT.Rows[0][19].ToString();
                            fields.SetField("untitled17", child1);
                            string child1bdayraw = DT.Rows[0][20].ToString();
                            string[] child1bdayarr = child1bdayraw.Split('-');

                            //add 0 if date is single digit
                            string c1bdate = child1bdayarr[1];
                            fields.SetFieldProperty("untitled29", "textsize", 10f, null);
                            fields.SetFieldProperty("untitled33", "textsize", 10f, null);
                            fields.SetFieldProperty("untitled37", "textsize", 10f, null);
                            fields.SetField("untitled29", child1bdayarr[2].Substring(0, 1) + "  " + child1bdayarr[2].Substring(1, 1));
                            fields.SetField("untitled33", c1bdate.Substring(0, 1) + "  " + c1bdate.Substring(1, 1));
                            fields.SetField("untitled37", child1bdayarr[0].Substring(0, 1) + "   " + child1bdayarr[0].Substring(1, 1) + "   " + child1bdayarr[0].Substring(2, 1) + "  " + child1bdayarr[0].Substring(3, 1));
                        }
                        catch { }

                        try
                        {
                            string child1 = DT.Rows[0][21].ToString();
                            fields.SetField("untitled18", child1);
                            string child1bdayraw = DT.Rows[0][22].ToString();
                            string[] child1bdayarr = child1bdayraw.Split('-');

                            //add 0 if date is single digit
                            string c1bdate = child1bdayarr[1];
                            fields.SetFieldProperty("untitled30", "textsize", 10f, null);
                            fields.SetFieldProperty("untitled34", "textsize", 10f, null);
                            fields.SetFieldProperty("untitled38", "textsize", 10f, null);
                            fields.SetField("untitled30", child1bdayarr[2].Substring(0, 1) + "  " + child1bdayarr[2].Substring(1, 1));
                            fields.SetField("untitled34", c1bdate.Substring(0, 1) + "  " + c1bdate.Substring(1, 1));
                            fields.SetField("untitled38", child1bdayarr[0].Substring(0, 1) + "   " + child1bdayarr[0].Substring(1, 1) + "   " + child1bdayarr[0].Substring(2, 1) + "  " + child1bdayarr[0].Substring(3, 1));
                        }
                        catch { }

                        try
                        {
                            string child1 = DT.Rows[0][23].ToString();
                            fields.SetField("untitled19", child1);
                            string child1bdayraw = DT.Rows[0][24].ToString();
                            string[] child1bdayarr = child1bdayraw.Split('-');

                            //add 0 if date is single digit
                            string c1bdate = child1bdayarr[1];
                            fields.SetFieldProperty("untitled31", "textsize", 10f, null);
                            fields.SetFieldProperty("untitled35", "textsize", 10f, null);
                            fields.SetFieldProperty("untitled39", "textsize", 10f, null);
                            fields.SetField("untitled31", child1bdayarr[2].Substring(0, 1) + "  " + child1bdayarr[2].Substring(1, 1));
                            fields.SetField("untitled35", c1bdate.Substring(0, 1) + "  " + c1bdate.Substring(1, 1));
                            fields.SetField("untitled39", child1bdayarr[0].Substring(0, 1) + "   " + child1bdayarr[0].Substring(1, 1) + "   " + child1bdayarr[0].Substring(2, 1) + "  " + child1bdayarr[0].Substring(3, 1));
                        }
                        catch { }

                        try
                        {
                            string child1 = DT.Rows[0][25].ToString();
                            fields.SetField("untitled20", child1);
                            string child1bdayraw = DT.Rows[0][26].ToString();
                            string[] child1bdayarr = child1bdayraw.Split('-');

                            //add 0 if date is single digit
                            string c1bdate = child1bdayarr[1];
                            fields.SetFieldProperty("untitled32", "textsize", 10f, null);
                            fields.SetFieldProperty("untitled36", "textsize", 10f, null);
                            fields.SetFieldProperty("untitled40", "textsize", 10f, null);
                            fields.SetField("untitled32", child1bdayarr[2].Substring(0, 1) + "  " + child1bdayarr[2].Substring(1, 1));
                            fields.SetField("untitled36", c1bdate.Substring(0, 1) + "  " + c1bdate.Substring(1, 1));
                            fields.SetField("untitled40", child1bdayarr[0].Substring(0, 1) + "   " + child1bdayarr[0].Substring(1, 1) + "   " + child1bdayarr[0].Substring(2, 1) + "  " + child1bdayarr[0].Substring(3, 1));
                        }
                        catch { }
                        try
                        {
                            string wageperday = DT.Rows[0][27].ToString();
                            fields.SetField("untitled41", wageperday);

                            string wagepermonth = DT.Rows[0][28].ToString();
                            fields.SetField("untitled42", wagepermonth);

                            string isminwage = DT.Rows[0][29].ToString();
                            fields.SetField("untitled28", isminwage);
                        }
                        catch { }



                        try
                        {
                            string EMPfullTIN = DT.Rows[0][30].ToString().Replace("-", "");
                            string EMPTIN1 = EMPfullTIN.Substring(0, 1); //First number
                            string EMPTIN2 = EMPfullTIN.Substring(1, 1); //2nd number
                            string EMPTIN3 = EMPfullTIN.Substring(2, 1); //3rd number
                            string EMPTIN1st3 = EMPTIN1 + " " + EMPTIN2 + " " + EMPTIN3;
                            fields.SetFieldProperty("untitled43", "textsize", 12f, null);
                            string EMPTIN4 = EMPfullTIN.Substring(3, 1);
                            string EMPTIN5 = EMPfullTIN.Substring(4, 1);
                            string EMPTIN6 = EMPfullTIN.Substring(5, 1);
                            string EMPTIN2nd3 = EMPTIN4 + " " + EMPTIN5 + " " + EMPTIN6;
                            fields.SetFieldProperty("untitled44", "textsize", 12f, null);
                            string EMPTIN7 = EMPfullTIN.Substring(6, 1);
                            string EMPTIN8 = EMPfullTIN.Substring(7, 1);
                            string EMPTIN9 = EMPfullTIN.Substring(8, 1);
                            string EMPTIN3rd3 = EMPTIN7 + " " + EMPTIN8 + " " + EMPTIN9;
                            fields.SetFieldProperty("untitled45", "textsize", 12f, null);
                            if (EMPfullTIN.Length >= 12)
                            {
                                string EMPTIN10 = EMPfullTIN.Substring(9, 1);
                                string EMPTIN11 = EMPfullTIN.Substring(10, 1);
                                string EMPTIN12 = EMPfullTIN.Substring(11, 1);
                                string EMPTIN13 = EMPfullTIN.Substring(12, 1);
                                string EMPTIN4th3 = EMPTIN10 + " " + EMPTIN11 + " " + EMPTIN12 + " " + EMPTIN13;
                                fields.SetFieldProperty("untitled46", "textsize", 12f, null);
                                fields.SetField("untitled46", EMPTIN4th3);
                            }
                            fields.SetField("untitled43", EMPTIN1st3);
                            fields.SetField("untitled44", EMPTIN2nd3);
                            fields.SetField("untitled45", EMPTIN3rd3);
                        }
                        catch
                        {

                        }

                        try
                        {
                            string employersname = DT.Rows[0][31].ToString();
                            fields.SetField("untitled47", employersname);

                            string employersadd = DT.Rows[0][32].ToString();
                            fields.SetField("untitled48", employersadd);

                            string empzipraw = DT.Rows[0][33].ToString();
                            string empzip1 = empzipraw.Substring(0, 1);
                            string empzip2 = empzipraw.Substring(1, 1);
                            string empzip3 = empzipraw.Substring(2, 1);
                            string empzip4 = empzipraw.Substring(3, 1);
                            string empzipfinal = empzip1 + "  " + empzip2 + "  " + empzip3 + "  " + empzip4;
                            fields.SetFieldProperty("untitled49", "textsize", 10f, null);
                            fields.SetField("untitled49", empzipfinal);
                        }
                        catch { }

                        try
                        {
                            string PREVEMPfullTIN = DT.Rows[0][34].ToString().Replace("-", "");
                            string PREVEMPTIN1 = PREVEMPfullTIN.Substring(0, 1); //First number
                            string PREVEMPTIN2 = PREVEMPfullTIN.Substring(1, 1); //2nd number
                            string PREVEMPTIN3 = PREVEMPfullTIN.Substring(2, 1); //3rd number
                            string PREVEMPTIN1st3 = PREVEMPTIN1 + " " + PREVEMPTIN2 + " " + PREVEMPTIN3;
                            fields.SetFieldProperty("untitled50", "textsize", 12f, null);
                            string PREVEMPTIN4 = PREVEMPfullTIN.Substring(3, 1);
                            string PREVEMPTIN5 = PREVEMPfullTIN.Substring(4, 1);
                            string PREVEMPTIN6 = PREVEMPfullTIN.Substring(5, 1);
                            string PREVEMPTIN2nd3 = PREVEMPTIN4 + " " + PREVEMPTIN5 + " " + PREVEMPTIN6;
                            fields.SetFieldProperty("untitled51", "textsize", 12f, null);
                            string PREVEMPTIN7 = PREVEMPfullTIN.Substring(6, 1);
                            string PREVEMPTIN8 = PREVEMPfullTIN.Substring(7, 1);
                            string PREVEMPTIN9 = PREVEMPfullTIN.Substring(8, 1);
                            string PREVEMPTIN3rd3 = PREVEMPTIN7 + " " + PREVEMPTIN8 + " " + PREVEMPTIN9;
                            fields.SetFieldProperty("untitled52", "textsize", 12f, null);
                            if (PREVEMPfullTIN.Length >= 12)
                            {
                                string PREVEMPTIN10 = PREVEMPfullTIN.Substring(9, 1);
                                string PREVEMPTIN11 = PREVEMPfullTIN.Substring(10, 1);
                                string PREVEMPTIN12 = PREVEMPfullTIN.Substring(11, 1);
                                string PREVEMPTIN13 = PREVEMPfullTIN.Substring(12, 1);
                                string PREVEMPTIN4th3 = PREVEMPTIN10 + " " + PREVEMPTIN11 + " " + PREVEMPTIN12 + " " + PREVEMPTIN13;
                                fields.SetFieldProperty("untitled53", "textsize", 12f, null);
                                fields.SetField("untitled53", PREVEMPTIN4th3);
                            }
                            fields.SetField("untitled50", PREVEMPTIN1st3);
                            fields.SetField("untitled51", PREVEMPTIN2nd3);
                            fields.SetField("untitled52", PREVEMPTIN3rd3);
                        }
                        catch
                        {

                        }
                        try
                        {
                            string prevemployersname = DT.Rows[0][35].ToString();
                            fields.SetField("untitled54", prevemployersname);

                            string prevemployersadd = DT.Rows[0][36].ToString();
                            fields.SetField("untitled55", prevemployersadd);

                            string prevempzipraw = DT.Rows[0][37].ToString();
                            string prevempzip1 = prevempzipraw.Substring(0, 1);
                            string prevempzip2 = prevempzipraw.Substring(1, 1);
                            string prevempzip3 = prevempzipraw.Substring(2, 1);
                            string prevempzip4 = prevempzipraw.Substring(3, 1);
                            string prevempzipfinal = prevempzip1 + "  " + prevempzip2 + "  " + prevempzip3 + "  " + prevempzip4;
                            fields.SetFieldProperty("untitled56", "textsize", 10f, null);
                            fields.SetField("untitled56", prevempzipfinal);
                        }
                        catch { }

                        try
                        {
                            string presempgrossincome = DT.Rows[0][38].ToString();
                            fields.SetField("untitled57", presempgrossincome);
                            string presempttlnontax = DT.Rows[0][39].ToString();
                            fields.SetField("untitled58", presempttlnontax);
                            string presemptaxcompincome = DT.Rows[0][40].ToString();
                            fields.SetField("untitled59", presemptaxcompincome);
                            string prevemptaxcompincome1 = DT.Rows[0][41].ToString();
                            fields.SetField("untitled60", prevemptaxcompincome1);
                            string grosstaxincome = DT.Rows[0][42].ToString();
                            fields.SetField("untitled61", grosstaxincome);
                            string tax_exemp = DT.Rows[0][43].ToString();
                            fields.SetField("untitled62", tax_exemp);
                            string healthpaid = DT.Rows[0][44].ToString();
                            fields.SetField("untitled63", healthpaid);
                            string nettax1 = DT.Rows[0][45].ToString();
                            fields.SetField("untitled64", nettax1);
                            string taxdue = DT.Rows[0][46].ToString();
                            fields.SetField("untitled65", taxdue);
                            string presemptaxwithheld = DT.Rows[0][47].ToString();
                            fields.SetField("untitled66", presemptaxwithheld);
                            string prevemptaxwithheld = DT.Rows[0][48].ToString();
                            fields.SetField("untitled67", prevemptaxwithheld);
                            string TotalTaxWithHeld_Adjusted = DT.Rows[0][49].ToString();
                            fields.SetField("untitled68", TotalTaxWithHeld_Adjusted);
                            string nontaxholidaypay = DT.Rows[0][51].ToString();
                            fields.SetField("untitled70", nontaxholidaypay);
                            string nontaxotpay = DT.Rows[0][52].ToString();
                            fields.SetField("untitled71", nontaxotpay);
                            string nontaxnightdiffpay = DT.Rows[0][53].ToString();
                            fields.SetField("untitled72", nontaxnightdiffpay);
                            string nontaxhazardpay = DT.Rows[0][54].ToString();
                            fields.SetField("untitled73", nontaxhazardpay);
                            string nontaxotherbenand13thmonth = DT.Rows[0][55].ToString();
                            fields.SetField("untitled74", nontaxotherbenand13thmonth);
                            string nontaxdemben = DT.Rows[0][56].ToString();
                            fields.SetField("untitled75", nontaxdemben);
                            string nontaxgovcon = DT.Rows[0][57].ToString();
                            fields.SetField("untitled76", nontaxgovcon);
                            string nontaxsalariesandcompensation = DT.Rows[0][58].ToString();
                            fields.SetField("untitled77", nontaxsalariesandcompensation);
                            string nontaxexemptcompincome = DT.Rows[0][59].ToString();
                            fields.SetField("untitled78", nontaxexemptcompincome);
                            string nontaxbasicsalary1 = DT.Rows[0][50].ToString();
                            int bsal = Convert.ToInt32(DT.Rows[0][50].ToString().Replace(",", ""));
                            if (bsal <= 250000)
                            {
                                fields.SetField("untitled69", nontaxbasicsalary1);
                                fields.SetField("untitled79", "");
                            }
                            else
                            {
                                fields.SetField("untitled79", nontaxbasicsalary1);
                                fields.SetField("untitled69", "");
                            }

                        }
                        catch
                        {

                        }

                        try
                        {
                            string taxbasicsalary1 = DT.Rows[0][60].ToString();
                            int bsalary = Convert.ToInt32(DT.Rows[0][60].ToString().Replace(",", ""));
                            if (bsalary <= 250000)
                            {
                                fields.SetField("untitled69", taxbasicsalary1);
                                fields.SetField("untitled79", "");
                            }
                            else
                            {
                                fields.SetField("untitled79", taxbasicsalary1);
                                fields.SetField("untitled69", "");
                            }

                            string taxrepresentation = DT.Rows[0][61].ToString();
                            fields.SetField("untitled80", taxrepresentation);
                            string taxtranspo = DT.Rows[0][62].ToString();
                            fields.SetField("untitled81", taxtranspo);
                            string taxcostoflivingallowance = DT.Rows[0][63].ToString();
                            fields.SetField("untitled82", taxcostoflivingallowance);
                            string taxfixhouseallowance = DT.Rows[0][64].ToString();
                            fields.SetField("untitled83", taxfixhouseallowance);

                            string taxothers47A1 = DT.Rows[0][65].ToString();
                            fields.SetField("untitled97", taxothers47A1);
                            string taxothers47A2 = DT.Rows[0][66].ToString();
                            fields.SetField("untitled84", taxothers47A2);
                            string taxothers47B1 = DT.Rows[0][67].ToString();
                            fields.SetField("untitled98", taxothers47B1);
                            string taxothers47B2 = DT.Rows[0][68].ToString();
                            fields.SetField("untitled85", taxothers47B2);

                            string commission = DT.Rows[0][69].ToString();
                            fields.SetField("untitled86", commission);
                            string profitsharing = DT.Rows[0][70].ToString();
                            fields.SetField("untitled87", profitsharing);
                            string directorsfee = DT.Rows[0][71].ToString();
                            fields.SetField("untitled88", directorsfee);
                            string tax13thmonth = DT.Rows[0][72].ToString();
                            fields.SetField("untitled89", tax13thmonth);
                            string supphazardpay = DT.Rows[0][73].ToString();
                            fields.SetField("untitled90", supphazardpay);
                            string supplementaryOTpay = DT.Rows[0][74].ToString();
                            fields.SetField("untitled91", supplementaryOTpay);
                            string supplementothers1 = DT.Rows[0][75].ToString();
                            fields.SetField("untitled95", supplementothers1);
                            string supplementothers2 = DT.Rows[0][76].ToString();
                            fields.SetField("untitled92", supplementothers2);
                            string supplementothers3 = DT.Rows[0][77].ToString();
                            fields.SetField("untitled96", supplementothers3);
                            string supplementothers4 = DT.Rows[0][78].ToString();
                            fields.SetField("untitled93", supplementothers4);
                            string totaltaxcompincome = DT.Rows[0][79].ToString();
                            fields.SetField("untitled94", totaltaxcompincome);
                        }
                        catch { }
                        #endregion

                    }
                    else
                    {
                        #region NORMALMAPPING
                        try
                        {

                            char[] yrchar = yr.ToCharArray();
                            fields.SetFieldProperty("untitled1", "textsize", 12f, null);
                            fields.SetField("untitled1", yrchar[0].ToString() + " " + yrchar[1].ToString() + " " + yrchar[2].ToString() + " " + yrchar[3].ToString());
                            fields.SetField("untitled2", month1.Substring(0, 1) + "  " + month1.Substring(1, 1) + "   0  1");
                            fields.SetField("untitled3", month2.Substring(0, 1) + "  " + month2.Substring(1, 1) + "   0  1");

                        }
                        catch
                        {

                        }

                        GDB = new GetDatabase();
                        GDB.ClientName = uProfile.CN;
                        SelfieRegistration2Controller.GetDB2(GDB);

                        con = new Connection();
                        con.myparameters.Add(new myParameters { ParameterName = "@empid", mytype = SqlDbType.NVarChar, Value = empids });
                        DataTable getPagibigMPL = con.GetDataTable("sp_PagibigMPL");

                        #region Personal Information
                        //TIN Breakdown
                        try
                        {


                            string fullTIN = getPagibigMPL.Rows[0][2].ToString().Replace("-", "");
                            string TIN1 = fullTIN.Substring(0, 1); //First number
                            string TIN2 = fullTIN.Substring(1, 1); //2nd number
                            string TIN3 = fullTIN.Substring(2, 1); //3rd number
                            string TIN1st3 = TIN1 + " " + TIN2 + " " + TIN3;
                            fields.SetFieldProperty("untitled4", "textsize", 12f, null);

                            string TIN4 = fullTIN.Substring(3, 1);
                            string TIN5 = fullTIN.Substring(4, 1);
                            string TIN6 = fullTIN.Substring(5, 1);
                            string TIN2nd3 = TIN4 + " " + TIN5 + " " + TIN6;
                            fields.SetFieldProperty("untitled5", "textsize", 12f, null);

                            string TIN7 = fullTIN.Substring(6, 1);
                            string TIN8 = fullTIN.Substring(7, 1);
                            string TIN9 = fullTIN.Substring(8, 1);
                            string TIN3rd3 = TIN7 + " " + TIN8 + " " + TIN9;
                            fields.SetFieldProperty("untitled6", "textsize", 12f, null);

                            if (fullTIN.Length == 12)
                            {
                                string TIN10 = fullTIN.Substring(9, 1);
                                string TIN11 = fullTIN.Substring(10, 1);
                                string TIN12 = fullTIN.Substring(11, 1);
                                string TIN4th3 = TIN10 + " " + TIN11 + " " + TIN12;
                                fields.SetFieldProperty("untitled7", "textsize", 12f, null);
                                fields.SetField("untitled7", TIN4th3);
                            }
                            fields.SetField("untitled4", TIN1st3);
                            fields.SetField("untitled5", TIN2nd3);
                            fields.SetField("untitled6", TIN3rd3);

                        }
                        catch
                        {

                        }
                        string fullname = getPagibigMPL.Rows[0][6].ToString().ToUpper() + " " + getPagibigMPL.Rows[0][5].ToString().ToUpper() + " " + getPagibigMPL.Rows[0][7].ToString().ToUpper();
                        string signatoryname = DT.Rows[0][6].ToString().ToUpper() + " " + DT.Rows[0][7].ToString().ToUpper() + " " + DT.Rows[0][5].ToString().ToUpper();
                        try
                        {

                            fields.SetFieldProperty("untitled8", "setfflags", PdfFormField.FF_READ_ONLY, null);
                            fields.SetField("untitled8", fullname);
                        }
                        catch
                        {

                        }
                        GDB = new GetDatabase();
                        GDB.ClientName = uProfile.CN;
                        SelfieRegistration2Controller.GetDB2(GDB);

                        con = new Connection();
                        con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = empids });
                        DataTable getCompanyProfile = con.GetDataTable("sp_ShowCompanyDetails");
                        //5.RDO Code
                        try
                        {



                            string rdo = getCompanyProfile.Rows[0][63].ToString();
                            switch (rdo.Length)
                            {
                                case 1:
                                    {
                                        rdo = "             " + rdo;
                                        break;
                                    }
                                case 2:
                                    {
                                        rdo = "         " + rdo;
                                        break;
                                    }
                                default:
                                    {
                                        break;
                                    }
                            }
                            fields.SetFieldProperty("untitled12", "textsize", 10f, null);
                            fields.SetField("untitled12", rdo);

                        }
                        catch
                        {

                        }
                        //6.Registered Address
                        try
                        {
                            string regadd = getPagibigMPL.Rows[0][15].ToString().ToUpper() + " " + getPagibigMPL.Rows[0][21].ToString().ToUpper() + " " + getPagibigMPL.Rows[0][20].ToString().ToUpper() + " " + getPagibigMPL.Rows[0][7].ToString().ToUpper() + " " + getPagibigMPL.Rows[0][9].ToString().ToUpper() + " " + provincea.ToUpper();
                            fields.SetFieldProperty("untitled9", "textsize", 4.6f, null);
                            fields.SetField("untitled9", regadd);
                            fields.SetFieldProperty("untitled10", "textsize", 4.6f, null);
                            fields.SetField("untitled10", regadd);

                        }
                        catch
                        {

                        }
                        //6A.Zip Code
                        try
                        {
                            string zipraw = getPagibigMPL.Rows[0][36].ToString();
                            string zip1 = zipraw.Substring(0, 1);
                            string zip2 = zipraw.Substring(1, 1);
                            string zip3 = zipraw.Substring(2, 1);
                            string zip4 = zipraw.Substring(3, 1);
                            string zipfinal = zip1 + "  " + zip2 + "  " + zip3 + "  " + zip4;
                            fields.SetFieldProperty("untitled13", "textsize", 10f, null);
                            fields.SetField("untitled13", zipfinal);
                        }
                        catch
                        {

                        }

                        //7. Date of Birth
                        try
                        {
                            string bdayraw = Convert.ToDateTime(getPagibigMPL.Rows[0][11].ToString()).ToString("yyyy-MM-dd");
                            string[] bdayarr = bdayraw.Split('-');

                            //add 0 if date is single digit
                            string date = bdayarr[2];
                            fields.SetFieldProperty("untitled21", "textsize", 10f, null);
                            fields.SetFieldProperty("untitled22", "textsize", 10f, null);
                            fields.SetFieldProperty("untitled23", "textsize", 10f, null);
                            fields.SetField("untitled21", bdayarr[1].Substring(0, 1) + " " + bdayarr[1].Substring(1, 1));
                            fields.SetField("untitled22", date.Substring(0, 1) + " " + date.Substring(1, 1));
                            fields.SetField("untitled23", bdayarr[0].Substring(0, 1) + "   " + bdayarr[0].Substring(1, 1) + "   " + bdayarr[0].Substring(2, 1) + "   " + bdayarr[0].Substring(3, 1));
                        }
                        catch
                        {

                        }

                        //8. Telephone Number
                        try
                        {
                            string telareacoderaw = getPagibigMPL.Rows[0][13].ToString();
                            string telnumraw = getPagibigMPL.Rows[0][14].ToString().Replace("-", "");
                            fields.SetField("untitled16", "(" + telareacoderaw + ")" + " " + telnumraw);
                        }
                        catch
                        {

                        }


                        //9.Excemption Status
                        GDB = new GetDatabase();
                        GDB.ClientName = uProfile.CN;
                        SelfieRegistration2Controller.GetDB2(GDB);

                        con = new Connection();
                        con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = empids });
                        DataTable GetTaxStatus = con.GetDataTable("sp_GettaxStatus");

                        string taxstat = GetTaxStatus.Rows[0][0].ToString();
                        //Single
                        if (taxstat == "Single")
                        {
                            fields.SetField("untitled24", "X");
                        }

                        //Married
                        else if (taxstat == "Married")
                        {
                            fields.SetField("untitled26", "X");
                        }

                        //claim add exemp


                        #endregion

                        #region Employer Data
                        //employer TIN
                        try
                        {
                            string fullTIN = getPagibigMPL.Rows[0][2].ToString().Replace("-", "");
                            string TIN1 = fullTIN.Substring(0, 1); //First number
                            string TIN2 = fullTIN.Substring(1, 1); //2nd number
                            string TIN3 = fullTIN.Substring(2, 1); //3rd number
                            string TIN1st3 = TIN1 + " " + TIN2 + " " + TIN3;
                            fields.SetFieldProperty("untitled43", "textsize", 12f, null);
                            string TIN4 = fullTIN.Substring(3, 1);
                            string TIN5 = fullTIN.Substring(4, 1);
                            string TIN6 = fullTIN.Substring(5, 1);
                            string TIN2nd3 = TIN4 + " " + TIN5 + " " + TIN6;
                            fields.SetFieldProperty("untitled44", "textsize", 12f, null);
                            string TIN7 = fullTIN.Substring(6, 1);
                            string TIN8 = fullTIN.Substring(7, 1);
                            string TIN9 = fullTIN.Substring(8, 1);
                            string TIN3rd3 = TIN7 + " " + TIN8 + " " + TIN9;
                            fields.SetFieldProperty("untitled45", "textsize", 12f, null);
                            if (fullTIN.Length == 12)
                            {
                                string TIN10 = fullTIN.Substring(9, 1);
                                string TIN11 = fullTIN.Substring(10, 1);
                                string TIN12 = fullTIN.Substring(11, 1);
                                string TIN4th3 = TIN10 + " " + TIN11 + " " + TIN12;
                                fields.SetFieldProperty("untitled46", "textsize", 12f, null);
                                fields.SetField("untitled46", TIN4th3);
                            }
                            fields.SetField("untitled43", TIN1st3);
                            fields.SetField("untitled44", TIN2nd3);
                            fields.SetField("untitled45", TIN3rd3);
                        }
                        catch
                        {

                        }
                        fields.SetField("untitled47", getPagibigMPL.Rows[0][38].ToString());
                        fields.SetFieldProperty("untitled48", "textsize", 4.6f, null);
                        //Employer Address
                        try
                        {
                            string emplradd = getCompanyProfile.Rows[0][6].ToString() + " " + getCompanyProfile.Rows[0][7].ToString() + " " + getCompanyProfile.Rows[0][8].ToString() + ", " + getCompanyProfile.Rows[0][9].ToString() + ", " + getCompanyProfile.Rows[0][10].ToString() + ", " + getCompanyProfile.Rows[0][11].ToString();
                            fields.SetField("untitled48", emplradd.ToUpper());
                        }
                        catch
                        {

                        }
                        // Employer Zip
                        try
                        {
                            string empzip = getPagibigMPL.Rows[0][33].ToString();
                            char[] empzipnum = empzip.ToCharArray();
                            fields.SetFieldProperty("untitled49", "textsize", 10f, null);
                            fields.SetField("untitled49", empzipnum[0] + "  " + empzipnum[1] + "  " + empzipnum[2] + "  " + empzipnum[3]);
                            fields.SetField("untitled115", "");
                            fields.SetField("untitled116", "");
                            fields.SetField("untitled100", signatoryname);
                            fields.SetField("untitled104", signatoryname);
                        }
                        catch
                        {

                        }

                        #endregion



                    }
                }
                catch (Exception ex)
                {
                    //this.Page.ClientScript.RegisterClientScriptBlock(this.GetType(),
                    //    Guid.NewGuid().ToString("N"), "self.parent.location='login.aspx';", true);
                }
            }
            catch { }


            #region Calculations
            //Tax calculations
            try
            {
                DataTable BIR2316 = new DataTable();
                DataTable dtDispute = new DataTable();
                try
                {
                    GDB = new GetDatabase();
                    GDB.ClientName = uProfile.CN;
                    SelfieRegistration2Controller.GetDB2(GDB);

                    con = new Connection();
                    con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = empids });
                    con.myparameters.Add(new myParameters { ParameterName = "@YEAR", mytype = SqlDbType.NVarChar, Value = yr });
                    BIR2316 = con.GetDataTable("sp_GetPaySlipRecordsPaySlipDetailsbyYear");
                }
                catch
                {

                }
                try
                {
                    GDB = new GetDatabase();
                    GDB.ClientName = uProfile.CN;
                    SelfieRegistration2Controller.GetDB2(GDB);

                    con = new Connection();
                    con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = empids });
                    con.myparameters.Add(new myParameters { ParameterName = "@YEAR", mytype = SqlDbType.NVarChar, Value = yr });
                    dtDispute = con.GetDataTable("sp_getAllDisputeAmount");
                }
                catch { }
                GDB = new GetDatabase();
                GDB.ClientName = uProfile.CN;
                SelfieRegistration2Controller.GetDB2(GDB);

                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = empids });
                DataTable getEmployeePay = con.GetDataTable("sp_getAllDisputeAmount");
                string hourlys, dailys, monthlys, complexity_premiums, foods, travels, clothings, night_differentials, regular_overtimes, regular_holidays, regular_holiday_ots,
                    regular_holiday_nds, special_holidays, special_holiday_ots, special_holiday_nds, bank_accounts, tax_statuss;
                foreach (DataRow row in getEmployeePay.Rows)
                {
                    hourlys = (row["hourly"].ToString());
                    dailys = (row["daily"].ToString());
                    monthlys = (row["monthly"].ToString());
                    complexity_premiums = (row["complexity_premium"].ToString());
                    foods = (row["food"].ToString());
                    travels = (row["travel"].ToString());
                    clothings = (row["clothing"].ToString());
                    night_differentials = (row["night_differential"].ToString());
                    regular_overtimes = (row["regular_overtime"].ToString());
                    regular_holidays = (row["regular_holiday"].ToString());
                    regular_holiday_ots = (row["regular_holiday_ot"].ToString());
                    regular_holiday_nds = (row["regular_holiday_nd"].ToString());
                    special_holidays = (row["special_holiday"].ToString());
                    special_holiday_ots = (row["special_holiday_ot"].ToString());
                    special_holiday_nds = (row["special_holiday_nd"].ToString());
                    bank_accounts = (row["bank_account"].ToString());
                    tax_statuss = (row["tax_status"].ToString());
                }

                if (BIR2316.Rows.Count > 0)
                {
                    fields.SetField("untitled78", BIR2316.Rows[0]["GROSSPAY"].ToString());//Total Non-Taxable/Exempt Compensation Income
                    //sign.getEmployeePay(empids);
                    //regular Hours
                    string[] Regular = new string[8];
                    Regular[0] = BIR2316.Rows[0][0].ToString();
                    Regular[1] = BIR2316.Rows[0][3].ToString();
                    Regular[2] = BIR2316.Rows[0][6].ToString();
                    Regular[3] = BIR2316.Rows[0][9].ToString();
                    Regular[4] = BIR2316.Rows[0][12].ToString();
                    Regular[5] = BIR2316.Rows[0][15].ToString();
                    Regular[6] = BIR2316.Rows[0][18].ToString();
                    Regular[7] = BIR2316.Rows[0][21].ToString();
                    //Overtime
                    string[] Overtime = new string[8];
                    Overtime[0] = BIR2316.Rows[0][1].ToString();
                    Overtime[1] = BIR2316.Rows[0][4].ToString();
                    Overtime[2] = BIR2316.Rows[0][7].ToString();
                    Overtime[3] = BIR2316.Rows[0][10].ToString();
                    Overtime[4] = BIR2316.Rows[0][13].ToString();
                    Overtime[5] = BIR2316.Rows[0][16].ToString();
                    Overtime[6] = BIR2316.Rows[0][19].ToString();
                    Overtime[7] = BIR2316.Rows[0][22].ToString();
                    //Night Differential
                    string[] Nightdiff = new string[8];
                    Nightdiff[0] = BIR2316.Rows[0][2].ToString();
                    Nightdiff[1] = BIR2316.Rows[0][5].ToString();
                    Nightdiff[2] = BIR2316.Rows[0][8].ToString();
                    Nightdiff[3] = BIR2316.Rows[0][11].ToString();
                    Nightdiff[4] = BIR2316.Rows[0][14].ToString();
                    Nightdiff[5] = BIR2316.Rows[0][17].ToString();
                    Nightdiff[6] = BIR2316.Rows[0][20].ToString();
                    Nightdiff[7] = BIR2316.Rows[0][23].ToString();
                    //Leave
                    string leaveamt = String.IsNullOrEmpty(BIR2316.Rows[0][24].ToString()) ? "0" : BIR2316.Rows[0][24].ToString();


                    double BasicSal = 0;
                    for (int i = 0; i < 8; i++)
                    {
                        if (String.IsNullOrWhiteSpace(Regular[i]))
                        {
                            Regular[i] = "0";
                        }
                        if (String.IsNullOrWhiteSpace(Nightdiff[i]))
                        {
                            Nightdiff[i] = "0";
                        }
                        BasicSal += Convert.ToDouble(Regular[i]);
                        BasicSal += Convert.ToDouble(Nightdiff[i]);
                    }
                    BasicSal += Convert.ToDouble(leaveamt);
                    double OTPay = 0;
                    for (int i = 0; i < 8; i++)
                    {
                        if (String.IsNullOrWhiteSpace(Overtime[i]))
                        {
                            Overtime[i] = "0";
                        }
                        OTPay += Convert.ToDouble(Overtime[i]);
                    }
                    //Adjustments
                    for (int i = 0; i < dtDispute.Rows.Count; i++)
                    {
                        if (dtDispute.Rows[i][0].ToString() == "Regular" || dtDispute.Rows[i][0].ToString() == "Double Holiday" || dtDispute.Rows[i][0].ToString() == "Regular Holiday" || dtDispute.Rows[i][0].ToString() == "Special Holiday" || dtDispute.Rows[i][0].ToString() == "Double Holiday Night Diff" || dtDispute.Rows[i][0].ToString() == "Regular Holiday Night Diff" || dtDispute.Rows[i][0].ToString() == "Regular Night Diff" || dtDispute.Rows[i][0].ToString() == "Special Holiday Night Diff" || dtDispute.Rows[i][0].ToString() == "Leave" || dtDispute.Rows[i][0].ToString() == "Paternity" || dtDispute.Rows[i][0].ToString() == "Solo Parent")
                        {
                            string disp = dtDispute.Rows[i][2].ToString();
                            if (String.IsNullOrWhiteSpace(disp))
                            {
                                disp = "0";
                            }
                            BasicSal += Convert.ToDouble(disp);
                        }
                        else if (dtDispute.Rows[i][0].ToString().Contains("Overtime"))
                        {
                            string disp = dtDispute.Rows[i][2].ToString();
                            if (String.IsNullOrWhiteSpace(disp))
                            {
                                disp = "0";
                            }
                            OTPay += Convert.ToDouble(disp);
                        }
                    }
                    OTPay = Convert.ToDouble(BIR2316.Rows[0][1].ToString());
                    //13th month pay
                    double running13th = String.IsNullOrEmpty(BIR2316.Rows[0][25].ToString()) ? 0 : Convert.ToDouble(BIR2316.Rows[0][25].ToString());
                    //De Minimis
                    double demini = String.IsNullOrEmpty(BIR2316.Rows[0][27].ToString()) ? 0 : Convert.ToDouble(BIR2316.Rows[0][27].ToString());
                    //SSS, Pagibig, Philhealth
                    string HDMFtotal = BIR2316.Rows[0][32].ToString();
                    string PHICtotal = BIR2316.Rows[0][33].ToString();
                    string SSStotal = BIR2316.Rows[0][34].ToString();
                    double deducttotal = Convert.ToDouble(HDMFtotal) + Convert.ToDouble(PHICtotal) + Convert.ToDouble(SSStotal);
                    double grosspay = Convert.ToDouble(BIR2316.Rows[0]["GROSSPAY"].ToString());
                    //total non-Taxables
                    double totalnontaxbl = running13th + Convert.ToDouble(demini) + Convert.ToDouble(deducttotal);
                    totalnontaxbl = grosspay;
                    //total Taxable
                    //double totaltaxbl = BasicSal + OTPay;
                    double totaltaxbl = 0;
                    //Gross Compensation
                    double total = grosspay + totaltaxbl;
                    //Tax Exemption
                    //double bscPerEx = 50000;
                    //double depcount = sign.SearchDependentv2(empids).Rows.Count;
                    //double addEx = 25000 * depcount;
                    double totalEx = totaltaxbl - 0;
                    //NET taxable income
                    double nettxbl = totaltaxbl;
                    //Tax Due
                    double taxdue2 = Convert.ToDouble(BIR2316.Rows[0]["TAX"].ToString());
                    double taxdue = ComputeTax(totaltaxbl);
                    BasicSal = grosspay - OTPay - running13th - deducttotal;


                    //NON-TAXABLE/EXEMPT COMPENSATION INCOME
                    double basicsalary = Convert.ToDouble(String.Format("{0:n}", Math.Round(BasicSal, 2)));
                    string bsalary = String.Format("{0:n}", Math.Round(BasicSal, 2));
                    if (basicsalary <= 250000)
                    {
                        fields.SetField("untitled69", bsalary);
                        fields.SetField("untitled79", "0");
                    }
                    else
                    {
                        fields.SetField("untitled79", bsalary);
                        fields.SetField("untitled69", "0");
                    }
                    //Basic Salary/Statutory Minimum Wage
                    fields.SetField("untitled70", "0");//Holiday Pay
                    fields.SetField("untitled71", String.Format("{0:n}", Math.Round(OTPay, 2)));//Overtime Pay
                    fields.SetField("untitled72", "0");//Night Shift Differential
                    fields.SetField("untitled73", "0");//Hazard Pay
                    fields.SetField("untitled74", String.Format("{0:n}", Math.Round(running13th, 2)));//13th Month Pay and Other Benefits
                    fields.SetField("untitled75", "0");//De Minimis Benefits
                    fields.SetField("untitled76", String.Format("{0:n}", Math.Round(deducttotal, 2)));//SSS, GSIS, PHIC & Pag-ibig Contributions, & Union Dues
                    fields.SetField("untitled77", "0");//Salaries & Other Forms of Compensation


                    //TAXABLE
                    //fields.SetField("untitled79", String.Format("{0:n}", Math.Round(BasicSal, 2)));//Basic Salary
                    fields.SetField("untitled80", "0");//Representation
                    fields.SetField("untitled81", "0");//Transportation
                    fields.SetField("untitled82", "0");//Cost of Living Allowance
                    fields.SetField("untitled83", "0");//Fixed Housing Allowance
                                                       //Others start
                    fields.SetField("untitled84", "0");
                    fields.SetField("untitled85", "0");
                    fields.SetField("untitled97", " n/a");
                    fields.SetField("untitled98", "n/a");
                    //Others end
                    //Supplementary start
                    fields.SetField("untitled86", "0");//Commission
                    fields.SetField("untitled87", "0");//Profit Sharing
                    fields.SetField("untitled88", "0");//Fees including Director's Fees
                    fields.SetField("untitled89", "0");//Taxable 13th Month Pay and Other Benefits
                    fields.SetField("untitled90", "0");//Hazard Pay
                    fields.SetField("untitled91", "0");//Overtime Pay
                                                       //Others start
                    fields.SetField("untitled92", "0");
                    fields.SetField("untitled93", "0");
                    fields.SetField("untitled95", "0");
                    fields.SetField("untitled96", "0");
                    //Others end
                    //Supplementary end
                    fields.SetField("untitled94", "0");//Total Taxable Compensation Income
                    #endregion

                    #region SUMMARY
                    fields.SetField("untitled57", String.Format("{0:n}", Math.Round(total, 2)));//Gross Compensation Income from Present Employer
                    fields.SetField("untitled58", String.Format("{0:n}", Math.Round(totalnontaxbl, 2)));//Less: Total Non-Taxable/Exempt
                    fields.SetField("untitled59", String.Format("{0:n}", Math.Round(totaltaxbl, 2)));//Taxable Compensation Income from Present Employer
                    fields.SetField("untitled60", "0");//Add: Taxable Compensation Income from Previous Employer
                    fields.SetField("untitled61", String.Format("{0:n}", Math.Round(totaltaxbl, 2)));//Gross Taxable Compensation Income
                    fields.SetField("untitled62", String.Format("{0:n}", 0));//Less: Total Exemptions
                    fields.SetField("untitled63", "0");//Less: Premium Paid on Health
                    fields.SetField("untitled64", String.Format("{0:n}", Math.Round(totalEx, 2)));//Net Taxable Compensation Income
                    fields.SetField("untitled65", String.Format("{0:n}", Math.Round(taxdue, 2)));//Tax Due
                    fields.SetField("untitled66", String.Format("{0:n}", Math.Round(taxdue2, 2)));// Amount of Taxes Withheld(Present Employer)
                    fields.SetField("untitled67", "0");// Amount of Taxes Withheld(Previous Employer)
                    fields.SetField("untitled68", String.Format("{0:n}", Math.Round(taxdue - taxdue2, 2)));//Total Amount of Taxes Withheld As Adjusted
                    #endregion
                }
            }
            catch
            {

            }
            #endregion


            #region Employee Signature
            //Employee Signature
            try
            {
                string BitEmpsign = null;
                Byte[] EmpsignBit = null;
                iTextSharp.text.Image Empsign = null;
                iTextSharp.text.Image Empsign2 = null;
                var EmpsignContentByte = stamper.GetOverContent(1);
                //PR.GetSignatureSignPicture(empids, "sign_0");
                //BitEmpsign = PR.BitSign;

                GDB = new GetDatabase();
                GDB.ClientName = uProfile.CN;
                SelfieRegistration2Controller.GetDB2(GDB);

                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = empids });
                con.myparameters.Add(new myParameters { ParameterName = "@TYPES", mytype = SqlDbType.NVarChar, Value = "sign_0" });
                BitEmpsign = con.ExecuteScalar("sp_SearchBased64");

                if (!String.IsNullOrWhiteSpace(BitEmpsign))
                {
                    AcroFields.FieldPosition wew = fields.GetFieldPositions("untitled100")[0];
                    iTextSharp.text.Rectangle rect = wew.position;
                    string xxxx = rect.Left.ToString();
                    string yyyy = rect.Bottom.ToString();
                    BitEmpsign = BitEmpsign.Replace(' ', '+');
                    EmpsignBit = Convert.FromBase64String(BitEmpsign);
                    Empsign = iTextSharp.text.Image.GetInstance(EmpsignBit);
                    Empsign.SetAbsolutePosition(420, 80);
                    Empsign.ScaleToFit(50, 50);
                    EmpsignContentByte.AddImage(Empsign);

                    wew = fields.GetFieldPositions("untitled104")[0];
                    rect = wew.position;
                    xxxx = rect.Left.ToString();
                    yyyy = rect.Bottom.ToString();
                    BitEmpsign = BitEmpsign.Replace(' ', '+');
                    EmpsignBit = Convert.FromBase64String(BitEmpsign);
                    Empsign2 = iTextSharp.text.Image.GetInstance(EmpsignBit);
                    Empsign2.SetAbsolutePosition(160, 170);
                    Empsign2.ScaleToFit(50, 50);
                    EmpsignContentByte.AddImage(Empsign2);
                }

            }
            catch
            {

            }
            #endregion

            string Form, Pattern, ReqStart, ReqEnd, CoveredStart, CoveredEnd, NotifMsg, President, PresBck, HR, HRBck, Savedloc, SharedEmail, EmailTo, EmailCc, EmailBcc, EmailSubject, EmailMessege, SignMessage, Default1, Default2;
            int Function, DailyDay, DailyWeek, RecurWeek, Monday, Tuesday, Wednesday, Thursday, Friday, Saturday, Sunday, DayMonth, Month, Quarter, Day, Months, EndDay, AutoSign, Permission, PrintSign, FormRequested, SignedDays, SaveLoc, SharEmail, Email, Number;

            GDB = new GetDatabase();
            GDB.ClientName = uProfile.CN;
            SelfieRegistration2Controller.GetDB2(GDB);

            con = new Connection();
            DataTable GetRequestFormSettings = con.GetDataTable("sp_SearchHRMSDocumentsRequestForm");

            foreach (DataRow row in GetRequestFormSettings.Rows)
            {
                Form = row["Form"].ToString();
                Function = Convert.ToInt32(row["Functions"].ToString());
                Pattern = row["Pattern"].ToString();
                DailyDay = Convert.ToInt32(row["EveryNday"].ToString());
                DailyWeek = Convert.ToInt32(row["EveryWeekday"].ToString());
                RecurWeek = Convert.ToInt32(row["RecurWeek"].ToString());
                Monday = Convert.ToInt32(row["RecurMon"].ToString());
                Tuesday = Convert.ToInt32(row["RecurTue"].ToString());
                Wednesday = Convert.ToInt32(row["RecurWed"].ToString());
                Thursday = Convert.ToInt32(row["RecurThu"].ToString());
                Friday = Convert.ToInt32(row["RecurFri"].ToString());
                Saturday = Convert.ToInt32(row["RecurSat"].ToString());
                Sunday = Convert.ToInt32(row["RecurSun"].ToString());
                DayMonth = Convert.ToInt32(row["DayNMonth"].ToString());
                Month = Convert.ToInt32(row["NMonth"].ToString());
                Quarter = Convert.ToInt32(row["NQuarter"].ToString());
                Day = Convert.ToInt32(row["NDay"].ToString());
                Months = Convert.ToInt32(row["EveryNMonth"].ToString());
                ReqStart = row["RequestStart"].ToString();
                ReqEnd = row["RequestEnd"].ToString();
                EndDay = Convert.ToInt32(row["EndDay"].ToString());
                CoveredStart = row["CoveredStart"].ToString();
                CoveredEnd = row["CoveredEnd"].ToString();
                NotifMsg = row["NotificationMessege"].ToString();
                President = row["President"].ToString();
                PresBck = row["PresBackup"].ToString();
                HR = row["HR"].ToString();
                HRBck = row["HRBackup"].ToString();
                AutoSign = Convert.ToInt32(row["AutoSign"].ToString());
                Permission = Convert.ToInt32(row["SignPermission"].ToString());
                PrintSign = Convert.ToInt32(row["PrintSign"].ToString());
                if (string.IsNullOrWhiteSpace(row["Default1"].ToString()) == false)
                {
                    Default1 = row["Default1"].ToString();
                }
                else
                {
                    Default1 = "False";
                }
                if (string.IsNullOrWhiteSpace(row["Default2"].ToString()) == false)
                {
                    Default2 = row["Default2"].ToString();
                }
                else
                {
                    Default2 = "False";
                }
                FormRequested = Convert.ToInt32(row["FormIsRequested"].ToString());
                SignedDays = Convert.ToInt32(row["SignedForNDays"].ToString());
                SaveLoc = Convert.ToInt32(row["SaveLocation"].ToString());
                Savedloc = row["Location"].ToString();
                SharEmail = Convert.ToInt32(row["ShareEmail"].ToString());
                SharedEmail = row["Email"].ToString();
                Email = Convert.ToInt32(row["SendToEmail"].ToString());
                Number = Convert.ToInt32(row["SendToNumber"].ToString());
                EmailTo = row["EmailTo"].ToString();
                EmailCc = row["EmailCc"].ToString();
                EmailBcc = row["EmailBcc"].ToString();
                EmailSubject = row["EmailSubject"].ToString();
                EmailMessege = row["EmailMessege"].ToString();
                SignMessage = row["SignMessage"].ToString();
            }

            string signatory1, position1, empid1, empid2, active2, signatory2, position2;

            GDB = new GetDatabase();
            GDB.ClientName = uProfile.CN;
            SelfieRegistration2Controller.GetDB2(GDB);

            con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.NVarChar, Value = empids });
            con.myparameters.Add(new myParameters { ParameterName = "@FormName", mytype = SqlDbType.NVarChar, Value = formtype });
            DataTable SignatoryNew = con.GetDataTable("sp_GetSignatoryNew");

            foreach (DataRow row in SignatoryNew.Rows)
            {
                signatory1 = row["EmployeeName"].ToString();
                position1 = row["JobDesc"].ToString();
                empid1 = row["EmpID"].ToString();
            }

            GDB = new GetDatabase();
            GDB.ClientName = uProfile.CN;
            SelfieRegistration2Controller.GetDB2(GDB);

            con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = empids });
            DataTable Signatory2 = con.GetDataTable("sp_GetSignatory2");

            foreach (DataRow row in Signatory2.Rows)
            {
                empid2 = row["EmpID"].ToString();
                active2 = row["SIgnator2Status"].ToString();
                signatory2 = row["EmpName"].ToString();
                position2 = row["JobDesc"].ToString();
            }

            #region Approver Signatory
            try
            {

                //sign.GetRequestFormSettings();
                //GF.SignatoryNew(empids, formtype);
                //RF.GetAppEMP(87, empids);
                string empidApprover = "";
                //empidApprover = RF.ApproverEMPID;
                GDB = new GetDatabase();
                GDB.ClientName = uProfile.CN;
                SelfieRegistration2Controller.GetDB2(GDB);

                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = empids });
                con.myparameters.Add(new myParameters { ParameterName = "@FORMID", mytype = SqlDbType.NVarChar, Value = 87 });
                empidApprover = con.ExecuteScalar("sp_GetApproverEmpid");

                if (empidApprover == "" || empidApprover == null || string.IsNullOrWhiteSpace(empidApprover))
                {
                }
                else
                {
                    //PR.GetApproverNameJobDes(empidApprover);
                    //string Approvername = PR.ApproverName;
                    //string ApproverDesc = PR.ApproverJobDesc;

                    GDB = new GetDatabase();
                    GDB.ClientName = uProfile.CN;
                    SelfieRegistration2Controller.GetDB2(GDB);

                    con = new Connection();
                    con.myparameters.Add(new myParameters { ParameterName = "@EMPIDAPPROVER", mytype = SqlDbType.NVarChar, Value = empidApprover });
                    DataTable GetApproverNameJobDes = con.GetDataTable("sp_SearchEmpaneJobSinatory");

                    //PR.GetSignatureSignPicture(empidApprover, "sign_1");
                    string Bitsignature = "";

                    GDB = new GetDatabase();
                    GDB.ClientName = uProfile.CN;
                    SelfieRegistration2Controller.GetDB2(GDB);

                    con = new Connection();
                    con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = empidApprover });
                    con.myparameters.Add(new myParameters { ParameterName = "@TYPES", mytype = SqlDbType.NVarChar, Value = "sign_1" });



                    //Bitsignature = PR.BitSign;
                    Bitsignature = con.ExecuteScalar("sp_SearchBased64");
                    Byte[] SignatureBit = null;
                    iTextSharp.text.Image signatory = null;
                    iTextSharp.text.Image signatoryy1 = null;
                    var pdfContentByte = stamper.GetOverContent(1);
                    if (string.IsNullOrWhiteSpace(Bitsignature) == false)
                    {


                        //Byte[] signa1 = null;
                        //iTextSharp.text.Image sign1 = null;
                        if (Bitsignature.Contains("ws.durusthr.com"))
                        {
                            StringBuilder _sb = new StringBuilder();

                            Byte[] _byte = GetImage(Bitsignature);

                            _sb.Append(Convert.ToBase64String(_byte, 0, _byte.Length));

                            string ssign = _sb.ToString().Replace(' ', '+');
                            SignatureBit = Convert.FromBase64String(ssign);
                        }
                        else
                        {
                            Bitsignature = Bitsignature.Replace(' ', '+');
                            SignatureBit = Convert.FromBase64String(Bitsignature);
                        }





                        //Bitsignature = Bitsignature.Replace(' ', '+');
                        //SignatureBit = Convert.FromBase64String(Bitsignature);
                        signatory = iTextSharp.text.Image.GetInstance(SignatureBit);
                        signatoryy1 = iTextSharp.text.Image.GetInstance(SignatureBit);

                        AcroFields.FieldPosition wew = fields.GetFieldPositions("untitled99")[0];
                        iTextSharp.text.Rectangle rect = wew.position;
                        string xxxx = rect.Left.ToString();
                        string yyyy = rect.Bottom.ToString();

                        signatory.SetAbsolutePosition(150, 100);
                        signatory.ScaleToFit(50, 50);

                        wew = fields.GetFieldPositions("untitled103")[0];
                        rect = wew.position;
                        xxxx = rect.Left.ToString();
                        yyyy = rect.Bottom.ToString();

                        signatoryy1.SetAbsolutePosition(150, 190);
                        signatoryy1.ScaleToFit(50, 50);





                        string ApproverName = "";
                        string ApproverJobDesc = "";
                        if (GetApproverNameJobDes.Rows.Count > 0)
                        {

                        }
                        else
                        {
                            fields.SetField("untitled99", "");
                            fields.SetField("untitled103", "");
                        }
                        //fields.SetField("untitled99", Approvername);
                        //fields.SetField("untitled103", Approvername);
                        pdfContentByte.AddImage(signatory);
                        pdfContentByte.AddImage(signatoryy1);


                        string thismonth = DateTime.Now.ToString("MM");
                        string thisyear = DateTime.Now.ToString("yyyy");
                        string thisdate = DateTime.Now.ToString("dd");

                        fields.SetField("untitled112", thismonth.Substring(0, 1) + " " + thismonth.Substring(1, 1));
                        fields.SetField("untitled107", thisdate.Substring(0, 1) + " " + thisdate.Substring(1, 1));
                        fields.SetField("untitled106", thisyear.Substring(0, 1) + "   " + thisyear.Substring(1, 1) + "    " + thisyear.Substring(2, 1) + "  " + thisyear.Substring(3, 1));

                        fields.SetField("untitled113", thismonth.Substring(0, 1) + " " + thismonth.Substring(1, 1));
                        fields.SetField("untitled110", thisdate.Substring(0, 1) + " " + thisdate.Substring(1, 1));
                        fields.SetField("untitled109", thisyear.Substring(0, 1) + "   " + thisyear.Substring(1, 1) + "    " + thisyear.Substring(2, 1) + "  " + thisyear.Substring(3, 1));
                    }

                    fields.SetField("untitled99", GetApproverNameJobDes.Rows[0][0].ToString());
                    fields.SetField("untitled103", GetApproverNameJobDes.Rows[0][0].ToString());
                }

            }
            catch (Exception ex)
            {
                uProfile.Status = ex.ToString();
            }
            #endregion

            //foreach (var field in af.Fields)
            //{
            //    Console.WriteLine("{0}, {1}", field.Key, field.Value);

            //    string sample2 = field.Key.ToString();
            //    //TextBox2.Text = sample + field.Key.ToString() + ',';
            //    fields.SetField(sample2, sample2);
            //}

            stamper.FormFlattening = true;
            stamper.Close();
            // "Flatten" the form so it wont be editable/usable anymor

            return uProfile.Status;



        }

        private double ComputeTax(double totalincome)
        {
            double taxdue = 0;
            if (totalincome <= 250000)
            {
                taxdue = 0;
            }
            else if (totalincome <= 400000 && totalincome > 250000)
            {
                taxdue = (totalincome - 250000) * .2;
            }
            else if (totalincome <= 800000 && totalincome > 400000)
            {
                taxdue = ((totalincome - 400000) * .25) + 30000;
            }
            else if (totalincome <= 2000000 && totalincome > 800000)
            {
                taxdue = ((totalincome - 800000) * .3) + 130000;
            }
            else if (totalincome <= 8000000 && totalincome > 2000000)
            {
                taxdue = ((totalincome - 2000000) * .32) + 490000;
            }
            else if (totalincome > 8000000)
            {
                taxdue = ((totalincome - 8000000) * .35) + 2410000;
            }
            return taxdue;
        }

        [HttpPost]
        [Route("2316")]
        public string __2316(UserProfile uProfile)
        {
           string formtype = "2316";
            string yr = DateTime.Now.ToString("yyyy");
            string month1 = getmonth("January");
            string month2 = getmonth(DateTime.Now.ToString("MMMM"));
            //Response.Redirect("~/pdf/BIR19021.pdf"); // view pdf
            string path = HttpContext.Current.Server.MapPath("pdf");
            var pdfReader1 = new PdfReader(path + "/GovForms Templates/2316.pdf");
            AcroFields af = pdfReader1.AcroFields;
            string empids = uProfile.NTID;

            // Template file path
            string formFile = path + "/GovForms Templates/2316.pdf";

            // Output file path
            string newFile = path + "/GovForms-Employee/2316-" + uProfile.NTID + ".pdf";


            // read the template file
            PdfReader reader = new PdfReader(formFile);


            // instantiate PDFStamper object
            // The Output file will be created from template file and edited by the PDFStamper
            PdfStamper stamper = new PdfStamper(reader, new FileStream(
                        newFile, FileMode.Create));


            // Object to deal with the Output file's textfields
            AcroFields fields = stamper.AcroFields;



            // set form fields("Field Name in PDF", "String to be placed in this PDF text field");
            // for (int i = 0; i <= 200; i++)
            //  {
            //string ii = i.ToString();
            //fields.SetField("untitled" + i, ii); //temporary values
            string housenuma, maritala, streetnamea, barangaya, towna, citya, provincea = "", religiona, zipcodea, bloodtypea, gendera, maritalstata, doba, citizenshipa, nameofa, mobileareacodea,
                mobilenumbera, homeeracodea, homenumbera, personalemaila, emrgnamea, emrgnumbera, emrgrelationshipa, optionaemaila, contacta, pob, religion, mothername, fathername,
                payrollinfo, bankname, branchname, accountnumber, height, weight, wemail, extension, FNAMEEMRG, MNAMEEMRG, LNAMEEMRG, DateJoined, ProductionStartDate, Wallet, Tenure,
                TenureMonths, Batch, Class, Skill, JobDesc, Promotion, NameOfOrganization, MDATE, gPreffix, gSuffix, UserCountry;

            
            
            

            Connection con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = empids });
            DataTable getEmployeePersonalInformation = con.GetDataTable("sp_SearchHRMSEmployeePersonalInformation");
            foreach (DataRow row in getEmployeePersonalInformation.Rows)
            {
                housenuma = row["HouseNumber"].ToString();
                streetnamea = row["StreetName"].ToString();
                barangaya = row["Barangay"].ToString();
                towna = row["Town"].ToString();
                citya = row["City"].ToString();
                religiona = row["Region"].ToString();
                zipcodea = row["ZipCode"].ToString();
                bloodtypea = row["BloodType"].ToString();
                gendera = row["Gender"].ToString();
                maritalstata = row["MaritalStatus"].ToString();
                if (string.IsNullOrWhiteSpace(row["DOB"].ToString()) == true)
                {
                    doba = "";
                }
                else
                {
                    doba = Convert.ToDateTime(row["DOB"]).ToString("yyyy-MM-dd");
                }

                citizenshipa = row["Citizenship"].ToString();
                nameofa = row["NameOfOrganization"].ToString();
                mobileareacodea = row["MobileAreaCode"].ToString();
                mobilenumbera = row["MobileNumber"].ToString();
                homeeracodea = row["HomeAreaCode"].ToString();
                homenumbera = row["HomeNumber"].ToString();
                personalemaila = row["Email"].ToString();
                wemail = row["WorkEmail"].ToString();
                extension = row["Extension"].ToString();
                emrgnamea = row["EmrgName"].ToString();
                emrgnumbera = row["EmrgNumber"].ToString();
                emrgrelationshipa = row["EmrgRelationship"].ToString();
                optionaemaila = row["OptionalEmail"].ToString();
                contacta = row["ContactNo"].ToString();
                provincea = row["Province"].ToString();
                FNAMEEMRG = row["EmrgFname"].ToString();
                MNAMEEMRG = row["EmrgMname"].ToString();
                LNAMEEMRG = row["EmrgLname"].ToString();
                DateJoined = row["DateJoined"].ToString();
                ProductionStartDate = row["ProductionStartDate"].ToString();
                Wallet = row["Wallet"].ToString();
                Tenure = row["Tenure"].ToString();
                TenureMonths = row["TenureMonths"].ToString();
                Batch = row["Batch"].ToString();
                Class = row["Class"].ToString();
                Skill = row["Skill"].ToString();
                JobDesc = row["JobDesc"].ToString();
                Promotion = row["Promotion"].ToString();
                NameOfOrganization = row["NameOfOrganization"].ToString();
                maritala = row["MaritalStatus"].ToString();
                pob = row["POB"].ToString();
                religion = row["religion"].ToString();
                mothername = row["MotherName"].ToString();
                fathername = row["FatherName"].ToString();
                payrollinfo = row["Payrollinfo"].ToString();
                bankname = row["BankName"].ToString();
                branchname = (row["BranchName"].ToString());
                accountnumber = row["AccountNumber"].ToString();
                height = row["Height"].ToString();
                weight = row["Weight"].ToString();
                MDATE = row["MarriedDate"].ToString();
                gPreffix = row["Preffix"].ToString();
                gSuffix = row["Suffix"].ToString();
                UserCountry = row["Country"].ToString();
            }

            try
            {
                //sign.getEmployeePersonalInformation(empids);
                //Period Covered

                try
                {
                    
                    
                    

                    con = new Connection();
                    con.myparameters.Add(new myParameters { ParameterName = "@empid", mytype = SqlDbType.NVarChar, Value = empids });
                    DataTable DT = con.GetDataTable("sp_BIRFORM2316");
                    if (DT.Rows.Count > 0)
                    {
                        #region TODO 2316FIELDS
                        try
                        {
                            string yrchar = DT.Rows[0][0].ToString();
                            fields.SetFieldProperty("untitled1", "textsize", 12f, null);
                            fields.SetField("untitled1", yrchar.Substring(0, 1) + " " + yrchar.Substring(1, 1) + " " + yrchar.Substring(2, 1) + " " + yrchar.Substring(3, 1));

                            string period1 = DT.Rows[0][1].ToString().Replace(" ", "");
                            fields.SetField("untitled2", period1.Substring(0, 1) + "  " + period1.Substring(1, 1) + "   " + period1.Substring(2, 1) + "   " + period1.Substring(3, 1));
                            string period2 = DT.Rows[0][2].ToString().Replace(" ", "");
                            fields.SetField("untitled3", period2.Substring(0, 1) + "  " + period2.Substring(1, 1) + "   " + period2.Substring(2, 1) + "   " + period2.Substring(3, 1));

                            string fullTIN = DT.Rows[0][3].ToString().Replace(" ", "");
                            string TIN1 = fullTIN.Substring(0, 1); //First number
                            string TIN2 = fullTIN.Substring(1, 1); //2nd number
                            string TIN3 = fullTIN.Substring(2, 1); //3rd number
                            string TIN1st3 = TIN1 + " " + TIN2 + " " + TIN3;
                            fields.SetFieldProperty("untitled4", "textsize", 12f, null);

                            string TIN4 = fullTIN.Substring(3, 1);
                            string TIN5 = fullTIN.Substring(4, 1);
                            string TIN6 = fullTIN.Substring(5, 1);
                            string TIN2nd3 = TIN4 + " " + TIN5 + " " + TIN6;
                            fields.SetFieldProperty("untitled5", "textsize", 12f, null);

                            string TIN7 = fullTIN.Substring(6, 1);
                            string TIN8 = fullTIN.Substring(7, 1);
                            string TIN9 = fullTIN.Substring(8, 1);
                            string TIN3rd3 = TIN7 + " " + TIN8 + " " + TIN9;
                            fields.SetFieldProperty("untitled6", "textsize", 12f, null);

                            if (fullTIN.Length >= 12)
                            {
                                string TIN10 = fullTIN.Substring(9, 1);
                                string TIN11 = fullTIN.Substring(10, 1);
                                string TIN12 = fullTIN.Substring(11, 1);
                                string TIN13 = fullTIN.Substring(12, 1);
                                string TIN4th3 = TIN10 + " " + TIN11 + " " + TIN12 + " " + TIN13;
                                fields.SetFieldProperty("untitled7", "textsize", 12f, null);
                                fields.SetField("untitled7", TIN4th3);
                            }
                            fields.SetField("untitled4", TIN1st3);
                            fields.SetField("untitled5", TIN2nd3);
                            fields.SetField("untitled6", TIN3rd3);
                        }
                        catch { }

                        string fullname = DT.Rows[0][5].ToString().ToUpper() + " " + DT.Rows[0][6].ToString().ToUpper() + " " + DT.Rows[0][7].ToString().ToUpper();
                        string signatoryname = DT.Rows[0][6].ToString().ToUpper() + " " + DT.Rows[0][7].ToString().ToUpper() + " " + DT.Rows[0][5].ToString().ToUpper();
                        try
                        {

                            fields.SetFieldProperty("untitled8", "setfflags", PdfFormField.FF_READ_ONLY, null);
                            fields.SetField("untitled8", fullname);
                            fields.SetField("untitled100", signatoryname);
                            fields.SetField("untitled104", signatoryname);
                        }
                        catch
                        {

                        }

                        try
                        {
                            string rdo = DT.Rows[0][8].ToString();
                            switch (rdo.Length)
                            {
                                case 1:
                                    {
                                        rdo = "             " + rdo;
                                        break;
                                    }
                                case 2:
                                    {
                                        rdo = "         " + rdo;
                                        break;
                                    }
                                default:
                                    {
                                        break;
                                    }
                            }

                            fields.SetFieldProperty("untitled12", "textsize", 10f, null);
                            fields.SetField("untitled12", rdo);

                            string regadd = DT.Rows[0][9].ToString().ToUpper();
                            fields.SetFieldProperty("untitled9", "textsize", 4.6f, null);
                            fields.SetField("untitled9", regadd);
                        }
                        catch { }

                        try
                        {
                            string locadd = DT.Rows[0][11].ToString().ToUpper();
                            fields.SetFieldProperty("untitled10", "textsize", 4.6f, null);
                            fields.SetField("untitled10", locadd);
                            string zipraw = DT.Rows[0][12].ToString();
                            string zip1 = zipraw.Substring(0, 1);
                            string zip2 = zipraw.Substring(1, 1);
                            string zip3 = zipraw.Substring(2, 1);
                            string zip4 = zipraw.Substring(3, 1);
                            string zipfinal = zip1 + "  " + zip2 + "  " + zip3 + "  " + zip4;
                            fields.SetFieldProperty("untitled14", "textsize", 10f, null);
                            fields.SetField("untitled14", zipfinal);
                        }
                        catch { }

                        try
                        {
                            string zipraw = DT.Rows[0][10].ToString();
                            string zip1 = zipraw.Substring(0, 1);
                            string zip2 = zipraw.Substring(1, 1);
                            string zip3 = zipraw.Substring(2, 1);
                            string zip4 = zipraw.Substring(3, 1);
                            string zipfinal = zip1 + "  " + zip2 + "  " + zip3 + "  " + zip4;
                            fields.SetFieldProperty("untitled13", "textsize", 10f, null);
                            fields.SetField("untitled13", zipfinal);
                        }
                        catch { }


                        try
                        {
                            string foreignadd = DT.Rows[0][13].ToString();
                            fields.SetFieldProperty("untitled11", "textsize", 4.6f, null);
                            fields.SetField("untitled11", foreignadd);
                            string zipraw = DT.Rows[0][14].ToString();
                            string zip1 = zipraw.Substring(0, 1);
                            string zip2 = zipraw.Substring(1, 1);
                            string zip3 = zipraw.Substring(2, 1);
                            string zip4 = zipraw.Substring(3, 1);
                            string zipfinal = zip1 + "  " + zip2 + "  " + zip3 + "  " + zip4;
                            fields.SetFieldProperty("untitled15", "textsize", 10f, null);
                            fields.SetField("untitled15", zipfinal);
                        }
                        catch { }

                        try
                        {
                            string bdayraw = DT.Rows[0][15].ToString();
                            string[] bdayarr = bdayraw.Split('-');

                            //add 0 if date is single digit
                            string date = bdayarr[1];
                            fields.SetFieldProperty("untitled21", "textsize", 10f, null);
                            fields.SetFieldProperty("untitled22", "textsize", 10f, null);
                            fields.SetFieldProperty("untitled23", "textsize", 10f, null);
                            fields.SetField("untitled21", bdayarr[2].Substring(0, 1) + " " + bdayarr[2].Substring(1, 1));
                            fields.SetField("untitled22", date.Substring(0, 1) + " " + date.Substring(1, 1));
                            fields.SetField("untitled23", bdayarr[0].Substring(0, 1) + "   " + bdayarr[0].Substring(1, 1) + "   " + bdayarr[0].Substring(2, 1) + "   " + bdayarr[0].Substring(3, 1));

                            string telnumraw = DT.Rows[0][16].ToString();
                            fields.SetField("untitled16", telnumraw);

                            string taxstat = DT.Rows[0][17].ToString();
                            //Single
                            if (taxstat == "single")
                            {
                                fields.SetField("untitled24", "X");
                            }

                            //Married
                            else if (taxstat == "married")
                            {
                                fields.SetField("untitled26", "X");
                            }

                            string claimexemp = DT.Rows[0][18].ToString();
                            //Single
                            if (claimexemp == "yes")
                            {
                                fields.SetField("untitled25", "X");
                            }

                            //Married
                            else if (claimexemp == "no")
                            {
                                fields.SetField("untitled27", "X");
                            }
                        }
                        catch { }
                        try
                        {
                            string child1 = DT.Rows[0][19].ToString();
                            fields.SetField("untitled17", child1);
                            string child1bdayraw = DT.Rows[0][20].ToString();
                            string[] child1bdayarr = child1bdayraw.Split('-');

                            //add 0 if date is single digit
                            string c1bdate = child1bdayarr[1];
                            fields.SetFieldProperty("untitled29", "textsize", 10f, null);
                            fields.SetFieldProperty("untitled33", "textsize", 10f, null);
                            fields.SetFieldProperty("untitled37", "textsize", 10f, null);
                            fields.SetField("untitled29", child1bdayarr[2].Substring(0, 1) + "  " + child1bdayarr[2].Substring(1, 1));
                            fields.SetField("untitled33", c1bdate.Substring(0, 1) + "  " + c1bdate.Substring(1, 1));
                            fields.SetField("untitled37", child1bdayarr[0].Substring(0, 1) + "   " + child1bdayarr[0].Substring(1, 1) + "   " + child1bdayarr[0].Substring(2, 1) + "  " + child1bdayarr[0].Substring(3, 1));
                        }
                        catch { }

                        try
                        {
                            string child1 = DT.Rows[0][21].ToString();
                            fields.SetField("untitled18", child1);
                            string child1bdayraw = DT.Rows[0][22].ToString();
                            string[] child1bdayarr = child1bdayraw.Split('-');

                            //add 0 if date is single digit
                            string c1bdate = child1bdayarr[1];
                            fields.SetFieldProperty("untitled30", "textsize", 10f, null);
                            fields.SetFieldProperty("untitled34", "textsize", 10f, null);
                            fields.SetFieldProperty("untitled38", "textsize", 10f, null);
                            fields.SetField("untitled30", child1bdayarr[2].Substring(0, 1) + "  " + child1bdayarr[2].Substring(1, 1));
                            fields.SetField("untitled34", c1bdate.Substring(0, 1) + "  " + c1bdate.Substring(1, 1));
                            fields.SetField("untitled38", child1bdayarr[0].Substring(0, 1) + "   " + child1bdayarr[0].Substring(1, 1) + "   " + child1bdayarr[0].Substring(2, 1) + "  " + child1bdayarr[0].Substring(3, 1));
                        }
                        catch { }

                        try
                        {
                            string child1 = DT.Rows[0][23].ToString();
                            fields.SetField("untitled19", child1);
                            string child1bdayraw = DT.Rows[0][24].ToString();
                            string[] child1bdayarr = child1bdayraw.Split('-');

                            //add 0 if date is single digit
                            string c1bdate = child1bdayarr[1];
                            fields.SetFieldProperty("untitled31", "textsize", 10f, null);
                            fields.SetFieldProperty("untitled35", "textsize", 10f, null);
                            fields.SetFieldProperty("untitled39", "textsize", 10f, null);
                            fields.SetField("untitled31", child1bdayarr[2].Substring(0, 1) + "  " + child1bdayarr[2].Substring(1, 1));
                            fields.SetField("untitled35", c1bdate.Substring(0, 1) + "  " + c1bdate.Substring(1, 1));
                            fields.SetField("untitled39", child1bdayarr[0].Substring(0, 1) + "   " + child1bdayarr[0].Substring(1, 1) + "   " + child1bdayarr[0].Substring(2, 1) + "  " + child1bdayarr[0].Substring(3, 1));
                        }
                        catch { }

                        try
                        {
                            string child1 = DT.Rows[0][25].ToString();
                            fields.SetField("untitled20", child1);
                            string child1bdayraw = DT.Rows[0][26].ToString();
                            string[] child1bdayarr = child1bdayraw.Split('-');

                            //add 0 if date is single digit
                            string c1bdate = child1bdayarr[1];
                            fields.SetFieldProperty("untitled32", "textsize", 10f, null);
                            fields.SetFieldProperty("untitled36", "textsize", 10f, null);
                            fields.SetFieldProperty("untitled40", "textsize", 10f, null);
                            fields.SetField("untitled32", child1bdayarr[2].Substring(0, 1) + "  " + child1bdayarr[2].Substring(1, 1));
                            fields.SetField("untitled36", c1bdate.Substring(0, 1) + "  " + c1bdate.Substring(1, 1));
                            fields.SetField("untitled40", child1bdayarr[0].Substring(0, 1) + "   " + child1bdayarr[0].Substring(1, 1) + "   " + child1bdayarr[0].Substring(2, 1) + "  " + child1bdayarr[0].Substring(3, 1));
                        }
                        catch { }
                        try
                        {
                            string wageperday = DT.Rows[0][27].ToString();
                            fields.SetField("untitled41", wageperday);

                            string wagepermonth = DT.Rows[0][28].ToString();
                            fields.SetField("untitled42", wagepermonth);

                            string isminwage = DT.Rows[0][29].ToString();
                            fields.SetField("untitled28", isminwage);
                        }
                        catch { }



                        try
                        {
                            string EMPfullTIN = DT.Rows[0][30].ToString().Replace("-", "");
                            string EMPTIN1 = EMPfullTIN.Substring(0, 1); //First number
                            string EMPTIN2 = EMPfullTIN.Substring(1, 1); //2nd number
                            string EMPTIN3 = EMPfullTIN.Substring(2, 1); //3rd number
                            string EMPTIN1st3 = EMPTIN1 + " " + EMPTIN2 + " " + EMPTIN3;
                            fields.SetFieldProperty("untitled43", "textsize", 12f, null);
                            string EMPTIN4 = EMPfullTIN.Substring(3, 1);
                            string EMPTIN5 = EMPfullTIN.Substring(4, 1);
                            string EMPTIN6 = EMPfullTIN.Substring(5, 1);
                            string EMPTIN2nd3 = EMPTIN4 + " " + EMPTIN5 + " " + EMPTIN6;
                            fields.SetFieldProperty("untitled44", "textsize", 12f, null);
                            string EMPTIN7 = EMPfullTIN.Substring(6, 1);
                            string EMPTIN8 = EMPfullTIN.Substring(7, 1);
                            string EMPTIN9 = EMPfullTIN.Substring(8, 1);
                            string EMPTIN3rd3 = EMPTIN7 + " " + EMPTIN8 + " " + EMPTIN9;
                            fields.SetFieldProperty("untitled45", "textsize", 12f, null);
                            if (EMPfullTIN.Length >= 12)
                            {
                                string EMPTIN10 = EMPfullTIN.Substring(9, 1);
                                string EMPTIN11 = EMPfullTIN.Substring(10, 1);
                                string EMPTIN12 = EMPfullTIN.Substring(11, 1);
                                string EMPTIN13 = EMPfullTIN.Substring(12, 1);
                                string EMPTIN4th3 = EMPTIN10 + " " + EMPTIN11 + " " + EMPTIN12 + " " + EMPTIN13;
                                fields.SetFieldProperty("untitled46", "textsize", 12f, null);
                                fields.SetField("untitled46", EMPTIN4th3);
                            }
                            fields.SetField("untitled43", EMPTIN1st3);
                            fields.SetField("untitled44", EMPTIN2nd3);
                            fields.SetField("untitled45", EMPTIN3rd3);
                        }
                        catch
                        {

                        }

                        try
                        {
                            string employersname = DT.Rows[0][31].ToString();
                            fields.SetField("untitled47", employersname);

                            string employersadd = DT.Rows[0][32].ToString();
                            fields.SetField("untitled48", employersadd);

                            string empzipraw = DT.Rows[0][33].ToString();
                            string empzip1 = empzipraw.Substring(0, 1);
                            string empzip2 = empzipraw.Substring(1, 1);
                            string empzip3 = empzipraw.Substring(2, 1);
                            string empzip4 = empzipraw.Substring(3, 1);
                            string empzipfinal = empzip1 + "  " + empzip2 + "  " + empzip3 + "  " + empzip4;
                            fields.SetFieldProperty("untitled49", "textsize", 10f, null);
                            fields.SetField("untitled49", empzipfinal);
                        }
                        catch { }

                        try
                        {
                            string PREVEMPfullTIN = DT.Rows[0][34].ToString().Replace("-", "");
                            string PREVEMPTIN1 = PREVEMPfullTIN.Substring(0, 1); //First number
                            string PREVEMPTIN2 = PREVEMPfullTIN.Substring(1, 1); //2nd number
                            string PREVEMPTIN3 = PREVEMPfullTIN.Substring(2, 1); //3rd number
                            string PREVEMPTIN1st3 = PREVEMPTIN1 + " " + PREVEMPTIN2 + " " + PREVEMPTIN3;
                            fields.SetFieldProperty("untitled50", "textsize", 12f, null);
                            string PREVEMPTIN4 = PREVEMPfullTIN.Substring(3, 1);
                            string PREVEMPTIN5 = PREVEMPfullTIN.Substring(4, 1);
                            string PREVEMPTIN6 = PREVEMPfullTIN.Substring(5, 1);
                            string PREVEMPTIN2nd3 = PREVEMPTIN4 + " " + PREVEMPTIN5 + " " + PREVEMPTIN6;
                            fields.SetFieldProperty("untitled51", "textsize", 12f, null);
                            string PREVEMPTIN7 = PREVEMPfullTIN.Substring(6, 1);
                            string PREVEMPTIN8 = PREVEMPfullTIN.Substring(7, 1);
                            string PREVEMPTIN9 = PREVEMPfullTIN.Substring(8, 1);
                            string PREVEMPTIN3rd3 = PREVEMPTIN7 + " " + PREVEMPTIN8 + " " + PREVEMPTIN9;
                            fields.SetFieldProperty("untitled52", "textsize", 12f, null);
                            if (PREVEMPfullTIN.Length >= 12)
                            {
                                string PREVEMPTIN10 = PREVEMPfullTIN.Substring(9, 1);
                                string PREVEMPTIN11 = PREVEMPfullTIN.Substring(10, 1);
                                string PREVEMPTIN12 = PREVEMPfullTIN.Substring(11, 1);
                                string PREVEMPTIN13 = PREVEMPfullTIN.Substring(12, 1);
                                string PREVEMPTIN4th3 = PREVEMPTIN10 + " " + PREVEMPTIN11 + " " + PREVEMPTIN12 + " " + PREVEMPTIN13;
                                fields.SetFieldProperty("untitled53", "textsize", 12f, null);
                                fields.SetField("untitled53", PREVEMPTIN4th3);
                            }
                            fields.SetField("untitled50", PREVEMPTIN1st3);
                            fields.SetField("untitled51", PREVEMPTIN2nd3);
                            fields.SetField("untitled52", PREVEMPTIN3rd3);
                        }
                        catch
                        {

                        }
                        try
                        {
                            string prevemployersname = DT.Rows[0][35].ToString();
                            fields.SetField("untitled54", prevemployersname);

                            string prevemployersadd = DT.Rows[0][36].ToString();
                            fields.SetField("untitled55", prevemployersadd);

                            string prevempzipraw = DT.Rows[0][37].ToString();
                            string prevempzip1 = prevempzipraw.Substring(0, 1);
                            string prevempzip2 = prevempzipraw.Substring(1, 1);
                            string prevempzip3 = prevempzipraw.Substring(2, 1);
                            string prevempzip4 = prevempzipraw.Substring(3, 1);
                            string prevempzipfinal = prevempzip1 + "  " + prevempzip2 + "  " + prevempzip3 + "  " + prevempzip4;
                            fields.SetFieldProperty("untitled56", "textsize", 10f, null);
                            fields.SetField("untitled56", prevempzipfinal);
                        }
                        catch { }

                        try
                        {
                            string presempgrossincome = DT.Rows[0][38].ToString();
                            fields.SetField("untitled57", presempgrossincome);
                            string presempttlnontax = DT.Rows[0][39].ToString();
                            fields.SetField("untitled58", presempttlnontax);
                            string presemptaxcompincome = DT.Rows[0][40].ToString();
                            fields.SetField("untitled59", presemptaxcompincome);
                            string prevemptaxcompincome1 = DT.Rows[0][41].ToString();
                            fields.SetField("untitled60", prevemptaxcompincome1);
                            string grosstaxincome = DT.Rows[0][42].ToString();
                            fields.SetField("untitled61", grosstaxincome);
                            string tax_exemp = DT.Rows[0][43].ToString();
                            fields.SetField("untitled62", tax_exemp);
                            string healthpaid = DT.Rows[0][44].ToString();
                            fields.SetField("untitled63", healthpaid);
                            string nettax1 = DT.Rows[0][45].ToString();
                            fields.SetField("untitled64", nettax1);
                            string taxdue = DT.Rows[0][46].ToString();
                            fields.SetField("untitled65", taxdue);
                            string presemptaxwithheld = DT.Rows[0][47].ToString();
                            fields.SetField("untitled66", presemptaxwithheld);
                            string prevemptaxwithheld = DT.Rows[0][48].ToString();
                            fields.SetField("untitled67", prevemptaxwithheld);
                            string TotalTaxWithHeld_Adjusted = DT.Rows[0][49].ToString();
                            fields.SetField("untitled68", TotalTaxWithHeld_Adjusted);
                            string nontaxholidaypay = DT.Rows[0][51].ToString();
                            fields.SetField("untitled70", nontaxholidaypay);
                            string nontaxotpay = DT.Rows[0][52].ToString();
                            fields.SetField("untitled71", nontaxotpay);
                            string nontaxnightdiffpay = DT.Rows[0][53].ToString();
                            fields.SetField("untitled72", nontaxnightdiffpay);
                            string nontaxhazardpay = DT.Rows[0][54].ToString();
                            fields.SetField("untitled73", nontaxhazardpay);
                            string nontaxotherbenand13thmonth = DT.Rows[0][55].ToString();
                            fields.SetField("untitled74", nontaxotherbenand13thmonth);
                            string nontaxdemben = DT.Rows[0][56].ToString();
                            fields.SetField("untitled75", nontaxdemben);
                            string nontaxgovcon = DT.Rows[0][57].ToString();
                            fields.SetField("untitled76", nontaxgovcon);
                            string nontaxsalariesandcompensation = DT.Rows[0][58].ToString();
                            fields.SetField("untitled77", nontaxsalariesandcompensation);
                            string nontaxexemptcompincome = DT.Rows[0][59].ToString();
                            fields.SetField("untitled78", nontaxexemptcompincome);
                            string nontaxbasicsalary1 = DT.Rows[0][50].ToString();
                            int bsal = Convert.ToInt32(DT.Rows[0][50].ToString().Replace(",", ""));
                            if (bsal <= 250000)
                            {
                                fields.SetField("untitled69", nontaxbasicsalary1);
                                fields.SetField("untitled79", "");
                            }
                            else
                            {
                                fields.SetField("untitled79", nontaxbasicsalary1);
                                fields.SetField("untitled69", "");
                            }

                        }
                        catch
                        {

                        }

                        try
                        {
                            string taxbasicsalary1 = DT.Rows[0][60].ToString();
                            int bsalary = Convert.ToInt32(DT.Rows[0][60].ToString().Replace(",", ""));
                            if (bsalary <= 250000)
                            {
                                fields.SetField("untitled69", taxbasicsalary1);
                                fields.SetField("untitled79", "");
                            }
                            else
                            {
                                fields.SetField("untitled79", taxbasicsalary1);
                                fields.SetField("untitled69", "");
                            }

                            string taxrepresentation = DT.Rows[0][61].ToString();
                            fields.SetField("untitled80", taxrepresentation);
                            string taxtranspo = DT.Rows[0][62].ToString();
                            fields.SetField("untitled81", taxtranspo);
                            string taxcostoflivingallowance = DT.Rows[0][63].ToString();
                            fields.SetField("untitled82", taxcostoflivingallowance);
                            string taxfixhouseallowance = DT.Rows[0][64].ToString();
                            fields.SetField("untitled83", taxfixhouseallowance);

                            string taxothers47A1 = DT.Rows[0][65].ToString();
                            fields.SetField("untitled97", taxothers47A1);
                            string taxothers47A2 = DT.Rows[0][66].ToString();
                            fields.SetField("untitled84", taxothers47A2);
                            string taxothers47B1 = DT.Rows[0][67].ToString();
                            fields.SetField("untitled98", taxothers47B1);
                            string taxothers47B2 = DT.Rows[0][68].ToString();
                            fields.SetField("untitled85", taxothers47B2);

                            string commission = DT.Rows[0][69].ToString();
                            fields.SetField("untitled86", commission);
                            string profitsharing = DT.Rows[0][70].ToString();
                            fields.SetField("untitled87", profitsharing);
                            string directorsfee = DT.Rows[0][71].ToString();
                            fields.SetField("untitled88", directorsfee);
                            string tax13thmonth = DT.Rows[0][72].ToString();
                            fields.SetField("untitled89", tax13thmonth);
                            string supphazardpay = DT.Rows[0][73].ToString();
                            fields.SetField("untitled90", supphazardpay);
                            string supplementaryOTpay = DT.Rows[0][74].ToString();
                            fields.SetField("untitled91", supplementaryOTpay);
                            string supplementothers1 = DT.Rows[0][75].ToString();
                            fields.SetField("untitled95", supplementothers1);
                            string supplementothers2 = DT.Rows[0][76].ToString();
                            fields.SetField("untitled92", supplementothers2);
                            string supplementothers3 = DT.Rows[0][77].ToString();
                            fields.SetField("untitled96", supplementothers3);
                            string supplementothers4 = DT.Rows[0][78].ToString();
                            fields.SetField("untitled93", supplementothers4);
                            string totaltaxcompincome = DT.Rows[0][79].ToString();
                            fields.SetField("untitled94", totaltaxcompincome);
                        }
                        catch { }
                        #endregion

                    }
                    else
                    {
                        #region NORMALMAPPING
                        try
                        {

                            char[] yrchar = yr.ToCharArray();
                            fields.SetFieldProperty("untitled1", "textsize", 12f, null);
                            fields.SetField("untitled1", yrchar[0].ToString() + " " + yrchar[1].ToString() + " " + yrchar[2].ToString() + " " + yrchar[3].ToString());
                            fields.SetField("untitled2", month1.Substring(0, 1) + "  " + month1.Substring(1, 1) + "   0  1");
                            fields.SetField("untitled3", month2.Substring(0, 1) + "  " + month2.Substring(1, 1) + "   0  1");

                        }
                        catch
                        {

                        }

                        
                        
                        

                        con = new Connection();
                        con.myparameters.Add(new myParameters { ParameterName = "@empid", mytype = SqlDbType.NVarChar, Value = empids });
                        DataTable getPagibigMPL = con.GetDataTable("sp_PagibigMPL");

                        #region Personal Information
                        //TIN Breakdown
                        try
                        {


                            string fullTIN = getPagibigMPL.Rows[0][2].ToString().Replace("-", "");
                            string TIN1 = fullTIN.Substring(0, 1); //First number
                            string TIN2 = fullTIN.Substring(1, 1); //2nd number
                            string TIN3 = fullTIN.Substring(2, 1); //3rd number
                            string TIN1st3 = TIN1 + " " + TIN2 + " " + TIN3;
                            fields.SetFieldProperty("untitled4", "textsize", 12f, null);

                            string TIN4 = fullTIN.Substring(3, 1);
                            string TIN5 = fullTIN.Substring(4, 1);
                            string TIN6 = fullTIN.Substring(5, 1);
                            string TIN2nd3 = TIN4 + " " + TIN5 + " " + TIN6;
                            fields.SetFieldProperty("untitled5", "textsize", 12f, null);

                            string TIN7 = fullTIN.Substring(6, 1);
                            string TIN8 = fullTIN.Substring(7, 1);
                            string TIN9 = fullTIN.Substring(8, 1);
                            string TIN3rd3 = TIN7 + " " + TIN8 + " " + TIN9;
                            fields.SetFieldProperty("untitled6", "textsize", 12f, null);

                            if (fullTIN.Length == 12)
                            {
                                string TIN10 = fullTIN.Substring(9, 1);
                                string TIN11 = fullTIN.Substring(10, 1);
                                string TIN12 = fullTIN.Substring(11, 1);
                                string TIN4th3 = TIN10 + " " + TIN11 + " " + TIN12;
                                fields.SetFieldProperty("untitled7", "textsize", 12f, null);
                                fields.SetField("untitled7", TIN4th3);
                            }
                            fields.SetField("untitled4", TIN1st3);
                            fields.SetField("untitled5", TIN2nd3);
                            fields.SetField("untitled6", TIN3rd3);

                        }
                        catch
                        {

                        }
                        string fullname = getPagibigMPL.Rows[0][6].ToString().ToUpper() + " " + getPagibigMPL.Rows[0][5].ToString().ToUpper() + " " + getPagibigMPL.Rows[0][7].ToString().ToUpper();
                        string signatoryname = DT.Rows[0][6].ToString().ToUpper() + " " + DT.Rows[0][7].ToString().ToUpper() + " " + DT.Rows[0][5].ToString().ToUpper();
                        try
                        {

                            fields.SetFieldProperty("untitled8", "setfflags", PdfFormField.FF_READ_ONLY, null);
                            fields.SetField("untitled8", fullname);
                        }
                        catch
                        {

                        }
                        
                        
                        

                        con = new Connection();
                        con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = empids });
                        DataTable getCompanyProfile = con.GetDataTable("sp_ShowCompanyDetails");
                        //5.RDO Code
                        try
                        {



                            string rdo = getCompanyProfile.Rows[0][63].ToString();
                            switch (rdo.Length)
                            {
                                case 1:
                                    {
                                        rdo = "             " + rdo;
                                        break;
                                    }
                                case 2:
                                    {
                                        rdo = "         " + rdo;
                                        break;
                                    }
                                default:
                                    {
                                        break;
                                    }
                            }
                            fields.SetFieldProperty("untitled12", "textsize", 10f, null);
                            fields.SetField("untitled12", rdo);

                        }
                        catch
                        {

                        }
                        //6.Registered Address
                        try
                        {
                            string regadd = getPagibigMPL.Rows[0][15].ToString().ToUpper() + " " + getPagibigMPL.Rows[0][21].ToString().ToUpper() + " " + getPagibigMPL.Rows[0][20].ToString().ToUpper() + " " + getPagibigMPL.Rows[0][7].ToString().ToUpper() + " " + getPagibigMPL.Rows[0][9].ToString().ToUpper() + " " + provincea.ToUpper();
                            fields.SetFieldProperty("untitled9", "textsize", 4.6f, null);
                            fields.SetField("untitled9", regadd);
                            fields.SetFieldProperty("untitled10", "textsize", 4.6f, null);
                            fields.SetField("untitled10", regadd);

                        }
                        catch
                        {

                        }
                        //6A.Zip Code
                        try
                        {
                            string zipraw = getPagibigMPL.Rows[0][36].ToString();
                            string zip1 = zipraw.Substring(0, 1);
                            string zip2 = zipraw.Substring(1, 1);
                            string zip3 = zipraw.Substring(2, 1);
                            string zip4 = zipraw.Substring(3, 1);
                            string zipfinal = zip1 + "  " + zip2 + "  " + zip3 + "  " + zip4;
                            fields.SetFieldProperty("untitled13", "textsize", 10f, null);
                            fields.SetField("untitled13", zipfinal);
                        }
                        catch
                        {

                        }

                        //7. Date of Birth
                        try
                        {
                            string bdayraw = Convert.ToDateTime(getPagibigMPL.Rows[0][11].ToString()).ToString("yyyy-MM-dd");
                            string[] bdayarr = bdayraw.Split('-');

                            //add 0 if date is single digit
                            string date = bdayarr[2];
                            fields.SetFieldProperty("untitled21", "textsize", 10f, null);
                            fields.SetFieldProperty("untitled22", "textsize", 10f, null);
                            fields.SetFieldProperty("untitled23", "textsize", 10f, null);
                            fields.SetField("untitled21", bdayarr[1].Substring(0, 1) + " " + bdayarr[1].Substring(1, 1));
                            fields.SetField("untitled22", date.Substring(0, 1) + " " + date.Substring(1, 1));
                            fields.SetField("untitled23", bdayarr[0].Substring(0, 1) + "   " + bdayarr[0].Substring(1, 1) + "   " + bdayarr[0].Substring(2, 1) + "   " + bdayarr[0].Substring(3, 1));
                        }
                        catch
                        {

                        }

                        //8. Telephone Number
                        try
                        {
                            string telareacoderaw = getPagibigMPL.Rows[0][13].ToString();
                            string telnumraw = getPagibigMPL.Rows[0][14].ToString().Replace("-", "");
                            fields.SetField("untitled16", "(" + telareacoderaw + ")" + " " + telnumraw);
                        }
                        catch
                        {

                        }


                        //9.Excemption Status
                        
                        
                        

                        con = new Connection();
                        con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = empids });
                        DataTable GetTaxStatus = con.GetDataTable("sp_GettaxStatus");

                        string taxstat = GetTaxStatus.Rows[0][0].ToString();
                        //Single
                        if (taxstat == "Single")
                        {
                            fields.SetField("untitled24", "X");
                        }

                        //Married
                        else if (taxstat == "Married")
                        {
                            fields.SetField("untitled26", "X");
                        }

                        //claim add exemp


                        #endregion

                        #region Employer Data
                        //employer TIN
                        try
                        {
                            string fullTIN = getPagibigMPL.Rows[0][2].ToString().Replace("-", "");
                            string TIN1 = fullTIN.Substring(0, 1); //First number
                            string TIN2 = fullTIN.Substring(1, 1); //2nd number
                            string TIN3 = fullTIN.Substring(2, 1); //3rd number
                            string TIN1st3 = TIN1 + " " + TIN2 + " " + TIN3;
                            fields.SetFieldProperty("untitled43", "textsize", 12f, null);
                            string TIN4 = fullTIN.Substring(3, 1);
                            string TIN5 = fullTIN.Substring(4, 1);
                            string TIN6 = fullTIN.Substring(5, 1);
                            string TIN2nd3 = TIN4 + " " + TIN5 + " " + TIN6;
                            fields.SetFieldProperty("untitled44", "textsize", 12f, null);
                            string TIN7 = fullTIN.Substring(6, 1);
                            string TIN8 = fullTIN.Substring(7, 1);
                            string TIN9 = fullTIN.Substring(8, 1);
                            string TIN3rd3 = TIN7 + " " + TIN8 + " " + TIN9;
                            fields.SetFieldProperty("untitled45", "textsize", 12f, null);
                            if (fullTIN.Length == 12)
                            {
                                string TIN10 = fullTIN.Substring(9, 1);
                                string TIN11 = fullTIN.Substring(10, 1);
                                string TIN12 = fullTIN.Substring(11, 1);
                                string TIN4th3 = TIN10 + " " + TIN11 + " " + TIN12;
                                fields.SetFieldProperty("untitled46", "textsize", 12f, null);
                                fields.SetField("untitled46", TIN4th3);
                            }
                            fields.SetField("untitled43", TIN1st3);
                            fields.SetField("untitled44", TIN2nd3);
                            fields.SetField("untitled45", TIN3rd3);
                        }
                        catch
                        {

                        }
                        fields.SetField("untitled47", getPagibigMPL.Rows[0][38].ToString());
                        fields.SetFieldProperty("untitled48", "textsize", 4.6f, null);
                        //Employer Address
                        try
                        {
                            string emplradd = getCompanyProfile.Rows[0][6].ToString() + " " + getCompanyProfile.Rows[0][7].ToString() + " " + getCompanyProfile.Rows[0][8].ToString() + ", " + getCompanyProfile.Rows[0][9].ToString() + ", " + getCompanyProfile.Rows[0][10].ToString() + ", " + getCompanyProfile.Rows[0][11].ToString();
                            fields.SetField("untitled48", emplradd.ToUpper());
                        }
                        catch
                        {

                        }
                        // Employer Zip
                        try
                        {
                            string empzip = getPagibigMPL.Rows[0][33].ToString();
                            char[] empzipnum = empzip.ToCharArray();
                            fields.SetFieldProperty("untitled49", "textsize", 10f, null);
                            fields.SetField("untitled49", empzipnum[0] + "  " + empzipnum[1] + "  " + empzipnum[2] + "  " + empzipnum[3]);
                            fields.SetField("untitled115", "");
                            fields.SetField("untitled116", "");
                            fields.SetField("untitled100", signatoryname);
                            fields.SetField("untitled104", signatoryname);
                        }
                        catch
                        {

                        }

                        #endregion



                    }
                }
                catch (Exception ex)
                {
                    //this.Page.ClientScript.RegisterClientScriptBlock(this.GetType(),
                    //    Guid.NewGuid().ToString("N"), "self.parent.location='login.aspx';", true);
                }
            }
            catch { }


            #region Calculations
            //Tax calculations
            try
            {
                DataTable BIR2316 = new DataTable();
                DataTable dtDispute = new DataTable();
                try
                {
                    
                    
                    

                    con = new Connection();
                    con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = empids });
                    con.myparameters.Add(new myParameters { ParameterName = "@YEAR", mytype = SqlDbType.NVarChar, Value = yr });
                    BIR2316 = con.GetDataTable("sp_GetPaySlipRecordsPaySlipDetailsbyYear");
                }
                catch
                {

                }
                try
                {
                    
                    
                    

                    con = new Connection();
                    con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = empids });
                    con.myparameters.Add(new myParameters { ParameterName = "@YEAR", mytype = SqlDbType.NVarChar, Value = yr });
                    dtDispute = con.GetDataTable("sp_getAllDisputeAmount");
                }
                catch { }
                
                
                

                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = empids });
                DataTable getEmployeePay = con.GetDataTable("sp_getAllDisputeAmount");
                string hourlys, dailys, monthlys, complexity_premiums, foods, travels, clothings, night_differentials, regular_overtimes, regular_holidays, regular_holiday_ots,
                    regular_holiday_nds, special_holidays, special_holiday_ots, special_holiday_nds, bank_accounts, tax_statuss;
                foreach (DataRow row in getEmployeePay.Rows)
                {
                    hourlys = (row["hourly"].ToString());
                    dailys = (row["daily"].ToString());
                    monthlys = (row["monthly"].ToString());
                    complexity_premiums = (row["complexity_premium"].ToString());
                    foods = (row["food"].ToString());
                    travels = (row["travel"].ToString());
                    clothings = (row["clothing"].ToString());
                    night_differentials = (row["night_differential"].ToString());
                    regular_overtimes = (row["regular_overtime"].ToString());
                    regular_holidays = (row["regular_holiday"].ToString());
                    regular_holiday_ots = (row["regular_holiday_ot"].ToString());
                    regular_holiday_nds = (row["regular_holiday_nd"].ToString());
                    special_holidays = (row["special_holiday"].ToString());
                    special_holiday_ots = (row["special_holiday_ot"].ToString());
                    special_holiday_nds = (row["special_holiday_nd"].ToString());
                    bank_accounts = (row["bank_account"].ToString());
                    tax_statuss = (row["tax_status"].ToString());
                }

                if (BIR2316.Rows.Count > 0)
                {
                    fields.SetField("untitled78", BIR2316.Rows[0]["GROSSPAY"].ToString());//Total Non-Taxable/Exempt Compensation Income
                    //sign.getEmployeePay(empids);
                    //regular Hours
                    string[] Regular = new string[8];
                    Regular[0] = BIR2316.Rows[0][0].ToString();
                    Regular[1] = BIR2316.Rows[0][3].ToString();
                    Regular[2] = BIR2316.Rows[0][6].ToString();
                    Regular[3] = BIR2316.Rows[0][9].ToString();
                    Regular[4] = BIR2316.Rows[0][12].ToString();
                    Regular[5] = BIR2316.Rows[0][15].ToString();
                    Regular[6] = BIR2316.Rows[0][18].ToString();
                    Regular[7] = BIR2316.Rows[0][21].ToString();
                    //Overtime
                    string[] Overtime = new string[8];
                    Overtime[0] = BIR2316.Rows[0][1].ToString();
                    Overtime[1] = BIR2316.Rows[0][4].ToString();
                    Overtime[2] = BIR2316.Rows[0][7].ToString();
                    Overtime[3] = BIR2316.Rows[0][10].ToString();
                    Overtime[4] = BIR2316.Rows[0][13].ToString();
                    Overtime[5] = BIR2316.Rows[0][16].ToString();
                    Overtime[6] = BIR2316.Rows[0][19].ToString();
                    Overtime[7] = BIR2316.Rows[0][22].ToString();
                    //Night Differential
                    string[] Nightdiff = new string[8];
                    Nightdiff[0] = BIR2316.Rows[0][2].ToString();
                    Nightdiff[1] = BIR2316.Rows[0][5].ToString();
                    Nightdiff[2] = BIR2316.Rows[0][8].ToString();
                    Nightdiff[3] = BIR2316.Rows[0][11].ToString();
                    Nightdiff[4] = BIR2316.Rows[0][14].ToString();
                    Nightdiff[5] = BIR2316.Rows[0][17].ToString();
                    Nightdiff[6] = BIR2316.Rows[0][20].ToString();
                    Nightdiff[7] = BIR2316.Rows[0][23].ToString();
                    //Leave
                    string leaveamt = String.IsNullOrEmpty(BIR2316.Rows[0][24].ToString()) ? "0" : BIR2316.Rows[0][24].ToString();


                    double BasicSal = 0;
                    for (int i = 0; i < 8; i++)
                    {
                        if (String.IsNullOrWhiteSpace(Regular[i]))
                        {
                            Regular[i] = "0";
                        }
                        if (String.IsNullOrWhiteSpace(Nightdiff[i]))
                        {
                            Nightdiff[i] = "0";
                        }
                        BasicSal += Convert.ToDouble(Regular[i]);
                        BasicSal += Convert.ToDouble(Nightdiff[i]);
                    }
                    BasicSal += Convert.ToDouble(leaveamt);
                    double OTPay = 0;
                    for (int i = 0; i < 8; i++)
                    {
                        if (String.IsNullOrWhiteSpace(Overtime[i]))
                        {
                            Overtime[i] = "0";
                        }
                        OTPay += Convert.ToDouble(Overtime[i]);
                    }
                    //Adjustments
                    for (int i = 0; i < dtDispute.Rows.Count; i++)
                    {
                        if (dtDispute.Rows[i][0].ToString() == "Regular" || dtDispute.Rows[i][0].ToString() == "Double Holiday" || dtDispute.Rows[i][0].ToString() == "Regular Holiday" || dtDispute.Rows[i][0].ToString() == "Special Holiday" || dtDispute.Rows[i][0].ToString() == "Double Holiday Night Diff" || dtDispute.Rows[i][0].ToString() == "Regular Holiday Night Diff" || dtDispute.Rows[i][0].ToString() == "Regular Night Diff" || dtDispute.Rows[i][0].ToString() == "Special Holiday Night Diff" || dtDispute.Rows[i][0].ToString() == "Leave" || dtDispute.Rows[i][0].ToString() == "Paternity" || dtDispute.Rows[i][0].ToString() == "Solo Parent")
                        {
                            string disp = dtDispute.Rows[i][2].ToString();
                            if (String.IsNullOrWhiteSpace(disp))
                            {
                                disp = "0";
                            }
                            BasicSal += Convert.ToDouble(disp);
                        }
                        else if (dtDispute.Rows[i][0].ToString().Contains("Overtime"))
                        {
                            string disp = dtDispute.Rows[i][2].ToString();
                            if (String.IsNullOrWhiteSpace(disp))
                            {
                                disp = "0";
                            }
                            OTPay += Convert.ToDouble(disp);
                        }
                    }
                    OTPay = Convert.ToDouble(BIR2316.Rows[0][1].ToString());
                    //13th month pay
                    double running13th = String.IsNullOrEmpty(BIR2316.Rows[0][25].ToString()) ? 0 : Convert.ToDouble(BIR2316.Rows[0][25].ToString());
                    //De Minimis
                    double demini = String.IsNullOrEmpty(BIR2316.Rows[0][27].ToString()) ? 0 : Convert.ToDouble(BIR2316.Rows[0][27].ToString());
                    //SSS, Pagibig, Philhealth
                    string HDMFtotal = BIR2316.Rows[0][32].ToString();
                    string PHICtotal = BIR2316.Rows[0][33].ToString();
                    string SSStotal = BIR2316.Rows[0][34].ToString();
                    double deducttotal = Convert.ToDouble(HDMFtotal) + Convert.ToDouble(PHICtotal) + Convert.ToDouble(SSStotal);
                    double grosspay = Convert.ToDouble(BIR2316.Rows[0]["GROSSPAY"].ToString());
                    //total non-Taxables
                    double totalnontaxbl = running13th + Convert.ToDouble(demini) + Convert.ToDouble(deducttotal);
                    totalnontaxbl = grosspay;
                    //total Taxable
                    //double totaltaxbl = BasicSal + OTPay;
                    double totaltaxbl = 0;
                    //Gross Compensation
                    double total = grosspay + totaltaxbl;
                    //Tax Exemption
                    //double bscPerEx = 50000;
                    //double depcount = sign.SearchDependentv2(empids).Rows.Count;
                    //double addEx = 25000 * depcount;
                    double totalEx = totaltaxbl - 0;
                    //NET taxable income
                    double nettxbl = totaltaxbl;
                    //Tax Due
                    double taxdue2 = Convert.ToDouble(BIR2316.Rows[0]["TAX"].ToString());
                    double taxdue = ComputeTax(totaltaxbl);
                    BasicSal = grosspay - OTPay - running13th - deducttotal;


                    //NON-TAXABLE/EXEMPT COMPENSATION INCOME
                    double basicsalary = Convert.ToDouble(String.Format("{0:n}", Math.Round(BasicSal, 2)));
                    string bsalary = String.Format("{0:n}", Math.Round(BasicSal, 2));
                    if (basicsalary <= 250000)
                    {
                        fields.SetField("untitled69", bsalary);
                        fields.SetField("untitled79", "0");
                    }
                    else
                    {
                        fields.SetField("untitled79", bsalary);
                        fields.SetField("untitled69", "0");
                    }
                    //Basic Salary/Statutory Minimum Wage
                    fields.SetField("untitled70", "0");//Holiday Pay
                    fields.SetField("untitled71", String.Format("{0:n}", Math.Round(OTPay, 2)));//Overtime Pay
                    fields.SetField("untitled72", "0");//Night Shift Differential
                    fields.SetField("untitled73", "0");//Hazard Pay
                    fields.SetField("untitled74", String.Format("{0:n}", Math.Round(running13th, 2)));//13th Month Pay and Other Benefits
                    fields.SetField("untitled75", "0");//De Minimis Benefits
                    fields.SetField("untitled76", String.Format("{0:n}", Math.Round(deducttotal, 2)));//SSS, GSIS, PHIC & Pag-ibig Contributions, & Union Dues
                    fields.SetField("untitled77", "0");//Salaries & Other Forms of Compensation


                    //TAXABLE
                    //fields.SetField("untitled79", String.Format("{0:n}", Math.Round(BasicSal, 2)));//Basic Salary
                    fields.SetField("untitled80", "0");//Representation
                    fields.SetField("untitled81", "0");//Transportation
                    fields.SetField("untitled82", "0");//Cost of Living Allowance
                    fields.SetField("untitled83", "0");//Fixed Housing Allowance
                                                       //Others start
                    fields.SetField("untitled84", "0");
                    fields.SetField("untitled85", "0");
                    fields.SetField("untitled97", " n/a");
                    fields.SetField("untitled98", "n/a");
                    //Others end
                    //Supplementary start
                    fields.SetField("untitled86", "0");//Commission
                    fields.SetField("untitled87", "0");//Profit Sharing
                    fields.SetField("untitled88", "0");//Fees including Director's Fees
                    fields.SetField("untitled89", "0");//Taxable 13th Month Pay and Other Benefits
                    fields.SetField("untitled90", "0");//Hazard Pay
                    fields.SetField("untitled91", "0");//Overtime Pay
                                                       //Others start
                    fields.SetField("untitled92", "0");
                    fields.SetField("untitled93", "0");
                    fields.SetField("untitled95", "0");
                    fields.SetField("untitled96", "0");
                    //Others end
                    //Supplementary end
                    fields.SetField("untitled94", "0");//Total Taxable Compensation Income
                    #endregion

                    #region SUMMARY
                    fields.SetField("untitled57", String.Format("{0:n}", Math.Round(total, 2)));//Gross Compensation Income from Present Employer
                    fields.SetField("untitled58", String.Format("{0:n}", Math.Round(totalnontaxbl, 2)));//Less: Total Non-Taxable/Exempt
                    fields.SetField("untitled59", String.Format("{0:n}", Math.Round(totaltaxbl, 2)));//Taxable Compensation Income from Present Employer
                    fields.SetField("untitled60", "0");//Add: Taxable Compensation Income from Previous Employer
                    fields.SetField("untitled61", String.Format("{0:n}", Math.Round(totaltaxbl, 2)));//Gross Taxable Compensation Income
                    fields.SetField("untitled62", String.Format("{0:n}", 0));//Less: Total Exemptions
                    fields.SetField("untitled63", "0");//Less: Premium Paid on Health
                    fields.SetField("untitled64", String.Format("{0:n}", Math.Round(totalEx, 2)));//Net Taxable Compensation Income
                    fields.SetField("untitled65", String.Format("{0:n}", Math.Round(taxdue, 2)));//Tax Due
                    fields.SetField("untitled66", String.Format("{0:n}", Math.Round(taxdue2, 2)));// Amount of Taxes Withheld(Present Employer)
                    fields.SetField("untitled67", "0");// Amount of Taxes Withheld(Previous Employer)
                    fields.SetField("untitled68", String.Format("{0:n}", Math.Round(taxdue - taxdue2, 2)));//Total Amount of Taxes Withheld As Adjusted
                    #endregion
                }
            }
            catch
            {

            }
            #endregion


            #region Employee Signature
            //Employee Signature
            try
            {
                string BitEmpsign = null;
                Byte[] EmpsignBit = null;
                iTextSharp.text.Image Empsign = null;
                iTextSharp.text.Image Empsign2 = null;
                var EmpsignContentByte = stamper.GetOverContent(1);
                //PR.GetSignatureSignPicture(empids, "sign_0");
                //BitEmpsign = PR.BitSign;

                
                
                

                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = empids });
                con.myparameters.Add(new myParameters { ParameterName = "@TYPES", mytype = SqlDbType.NVarChar, Value = "sign_0" });
                BitEmpsign = con.ExecuteScalar("sp_SearchBased64");

                if (!String.IsNullOrWhiteSpace(BitEmpsign))
                {
                    AcroFields.FieldPosition wew = fields.GetFieldPositions("untitled100")[0];
                    iTextSharp.text.Rectangle rect = wew.position;
                    string xxxx = rect.Left.ToString();
                    string yyyy = rect.Bottom.ToString();
                    BitEmpsign = BitEmpsign.Replace(' ', '+');
                    EmpsignBit = Convert.FromBase64String(BitEmpsign);
                    Empsign = iTextSharp.text.Image.GetInstance(EmpsignBit);
                    Empsign.SetAbsolutePosition(420, 80);
                    Empsign.ScaleToFit(50, 50);
                    EmpsignContentByte.AddImage(Empsign);

                    wew = fields.GetFieldPositions("untitled104")[0];
                    rect = wew.position;
                    xxxx = rect.Left.ToString();
                    yyyy = rect.Bottom.ToString();
                    BitEmpsign = BitEmpsign.Replace(' ', '+');
                    EmpsignBit = Convert.FromBase64String(BitEmpsign);
                    Empsign2 = iTextSharp.text.Image.GetInstance(EmpsignBit);
                    Empsign2.SetAbsolutePosition(160, 170);
                    Empsign2.ScaleToFit(50, 50);
                    EmpsignContentByte.AddImage(Empsign2);
                }

            }
            catch
            {

            }
            #endregion

            string Form, Pattern, ReqStart, ReqEnd, CoveredStart, CoveredEnd, NotifMsg, President, PresBck, HR, HRBck, Savedloc, SharedEmail, EmailTo, EmailCc, EmailBcc, EmailSubject, EmailMessege, SignMessage, Default1, Default2;
            int Function, DailyDay, DailyWeek, RecurWeek, Monday, Tuesday, Wednesday, Thursday, Friday, Saturday, Sunday, DayMonth, Month, Quarter, Day, Months, EndDay, AutoSign, Permission, PrintSign, FormRequested, SignedDays, SaveLoc, SharEmail, Email, Number;

            
            
            

            con = new Connection();
            DataTable GetRequestFormSettings = con.GetDataTable("sp_SearchHRMSDocumentsRequestForm");

            foreach (DataRow row in GetRequestFormSettings.Rows)
            {
                Form = row["Form"].ToString();
                Function = Convert.ToInt32(row["Functions"].ToString());
                Pattern = row["Pattern"].ToString();
                DailyDay = Convert.ToInt32(row["EveryNday"].ToString());
                DailyWeek = Convert.ToInt32(row["EveryWeekday"].ToString());
                RecurWeek = Convert.ToInt32(row["RecurWeek"].ToString());
                Monday = Convert.ToInt32(row["RecurMon"].ToString());
                Tuesday = Convert.ToInt32(row["RecurTue"].ToString());
                Wednesday = Convert.ToInt32(row["RecurWed"].ToString());
                Thursday = Convert.ToInt32(row["RecurThu"].ToString());
                Friday = Convert.ToInt32(row["RecurFri"].ToString());
                Saturday = Convert.ToInt32(row["RecurSat"].ToString());
                Sunday = Convert.ToInt32(row["RecurSun"].ToString());
                DayMonth = Convert.ToInt32(row["DayNMonth"].ToString());
                Month = Convert.ToInt32(row["NMonth"].ToString());
                Quarter = Convert.ToInt32(row["NQuarter"].ToString());
                Day = Convert.ToInt32(row["NDay"].ToString());
                Months = Convert.ToInt32(row["EveryNMonth"].ToString());
                ReqStart = row["RequestStart"].ToString();
                ReqEnd = row["RequestEnd"].ToString();
                EndDay = Convert.ToInt32(row["EndDay"].ToString());
                CoveredStart = row["CoveredStart"].ToString();
                CoveredEnd = row["CoveredEnd"].ToString();
                NotifMsg = row["NotificationMessege"].ToString();
                President = row["President"].ToString();
                PresBck = row["PresBackup"].ToString();
                HR = row["HR"].ToString();
                HRBck = row["HRBackup"].ToString();
                AutoSign = Convert.ToInt32(row["AutoSign"].ToString());
                Permission = Convert.ToInt32(row["SignPermission"].ToString());
                PrintSign = Convert.ToInt32(row["PrintSign"].ToString());
                if (string.IsNullOrWhiteSpace(row["Default1"].ToString()) == false)
                {
                    Default1 = row["Default1"].ToString();
                }
                else
                {
                    Default1 = "False";
                }
                if (string.IsNullOrWhiteSpace(row["Default2"].ToString()) == false)
                {
                    Default2 = row["Default2"].ToString();
                }
                else
                {
                    Default2 = "False";
                }
                FormRequested = Convert.ToInt32(row["FormIsRequested"].ToString());
                SignedDays = Convert.ToInt32(row["SignedForNDays"].ToString());
                SaveLoc = Convert.ToInt32(row["SaveLocation"].ToString());
                Savedloc = row["Location"].ToString();
                SharEmail = Convert.ToInt32(row["ShareEmail"].ToString());
                SharedEmail = row["Email"].ToString();
                Email = Convert.ToInt32(row["SendToEmail"].ToString());
                Number = Convert.ToInt32(row["SendToNumber"].ToString());
                EmailTo = row["EmailTo"].ToString();
                EmailCc = row["EmailCc"].ToString();
                EmailBcc = row["EmailBcc"].ToString();
                EmailSubject = row["EmailSubject"].ToString();
                EmailMessege = row["EmailMessege"].ToString();
                SignMessage = row["SignMessage"].ToString();
            }

            string signatory1, position1, empid1, empid2, active2, signatory2, position2;

            
            
            

            con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.NVarChar, Value = empids });
            con.myparameters.Add(new myParameters { ParameterName = "@FormName", mytype = SqlDbType.NVarChar, Value = formtype });
            DataTable SignatoryNew = con.GetDataTable("sp_GetSignatoryNew");

            foreach (DataRow row in SignatoryNew.Rows)
            {
                signatory1 = row["EmployeeName"].ToString();
                position1 = row["JobDesc"].ToString();
                empid1 = row["EmpID"].ToString();
            }

            
            
            

            con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = empids });
            DataTable Signatory2 = con.GetDataTable("sp_GetSignatory2");

            foreach (DataRow row in Signatory2.Rows)
            {
                empid2 = row["EmpID"].ToString();
                active2 = row["SIgnator2Status"].ToString();
                signatory2 = row["EmpName"].ToString();
                position2 = row["JobDesc"].ToString();
            }

            #region Approver Signatory
            try
            {

                //sign.GetRequestFormSettings();
                //GF.SignatoryNew(empids, formtype);
                //RF.GetAppEMP(87, empids);
                string empidApprover = "";
                //empidApprover = RF.ApproverEMPID;
                
                
                

                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = empids });
                con.myparameters.Add(new myParameters { ParameterName = "@FORMID", mytype = SqlDbType.NVarChar, Value = 87 });
                empidApprover = con.ExecuteScalar("sp_GetApproverEmpid");

                if (empidApprover == "" || empidApprover == null || string.IsNullOrWhiteSpace(empidApprover))
                {
                }
                else
                {
                    //PR.GetApproverNameJobDes(empidApprover);
                    //string Approvername = PR.ApproverName;
                    //string ApproverDesc = PR.ApproverJobDesc;

                    
                    
                    

                    con = new Connection();
                    con.myparameters.Add(new myParameters { ParameterName = "@EMPIDAPPROVER", mytype = SqlDbType.NVarChar, Value = empidApprover });
                    DataTable GetApproverNameJobDes = con.GetDataTable("sp_SearchEmpaneJobSinatory");

                    //PR.GetSignatureSignPicture(empidApprover, "sign_1");
                    string Bitsignature = "";

                    
                    
                    

                    con = new Connection();
                    con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = empidApprover });
                    con.myparameters.Add(new myParameters { ParameterName = "@TYPES", mytype = SqlDbType.NVarChar, Value = "sign_1" });



                    //Bitsignature = PR.BitSign;
                    Bitsignature = con.ExecuteScalar("sp_SearchBased64");
                    Byte[] SignatureBit = null;
                    iTextSharp.text.Image signatory = null;
                    iTextSharp.text.Image signatoryy1 = null;
                    var pdfContentByte = stamper.GetOverContent(1);
                    if (string.IsNullOrWhiteSpace(Bitsignature) == false)
                    {


                        //Byte[] signa1 = null;
                        //iTextSharp.text.Image sign1 = null;
                            if (Bitsignature.Contains("ws.durusthr.com"))
                            {
                                StringBuilder _sb = new StringBuilder();

                                Byte[] _byte = GetImage(Bitsignature);

                                _sb.Append(Convert.ToBase64String(_byte, 0, _byte.Length));

                                string ssign = _sb.ToString().Replace(' ', '+');
                                SignatureBit = Convert.FromBase64String(ssign);
                            }
                            else
                            {
                                Bitsignature = Bitsignature.Replace(' ', '+');
                                SignatureBit = Convert.FromBase64String(Bitsignature);
                            }





                            //Bitsignature = Bitsignature.Replace(' ', '+');
                            //SignatureBit = Convert.FromBase64String(Bitsignature);
                            signatory = iTextSharp.text.Image.GetInstance(SignatureBit);
                            signatoryy1 = iTextSharp.text.Image.GetInstance(SignatureBit);

                            AcroFields.FieldPosition wew = fields.GetFieldPositions("untitled99")[0];
                            iTextSharp.text.Rectangle rect = wew.position;
                            string xxxx = rect.Left.ToString();
                            string yyyy = rect.Bottom.ToString();

                            signatory.SetAbsolutePosition(150, 100);
                            signatory.ScaleToFit(50, 50);

                            wew = fields.GetFieldPositions("untitled103")[0];
                            rect = wew.position;
                            xxxx = rect.Left.ToString();
                            yyyy = rect.Bottom.ToString();

                            signatoryy1.SetAbsolutePosition(150, 190);
                            signatoryy1.ScaleToFit(50, 50);





                            string ApproverName = "";
                            string ApproverJobDesc = "";
                            if (GetApproverNameJobDes.Rows.Count > 0)
                            {

                            }
                            else
                            {
                                fields.SetField("untitled99", "");
                                fields.SetField("untitled103", "");
                            }
                            //fields.SetField("untitled99", Approvername);
                            //fields.SetField("untitled103", Approvername);
                            pdfContentByte.AddImage(signatory);
                            pdfContentByte.AddImage(signatoryy1);
                        

                        string thismonth = DateTime.Now.ToString("MM");
                        string thisyear = DateTime.Now.ToString("yyyy");
                        string thisdate = DateTime.Now.ToString("dd");

                        fields.SetField("untitled112", thismonth.Substring(0, 1) + " " + thismonth.Substring(1, 1));
                        fields.SetField("untitled107", thisdate.Substring(0, 1) + " " + thisdate.Substring(1, 1));
                        fields.SetField("untitled106", thisyear.Substring(0, 1) + "   " + thisyear.Substring(1, 1) + "    " + thisyear.Substring(2, 1) + "  " + thisyear.Substring(3, 1));

                        fields.SetField("untitled113", thismonth.Substring(0, 1) + " " + thismonth.Substring(1, 1));
                        fields.SetField("untitled110", thisdate.Substring(0, 1) + " " + thisdate.Substring(1, 1));
                        fields.SetField("untitled109", thisyear.Substring(0, 1) + "   " + thisyear.Substring(1, 1) + "    " + thisyear.Substring(2, 1) + "  " + thisyear.Substring(3, 1));
                    }

                    fields.SetField("untitled99", GetApproverNameJobDes.Rows[0][0].ToString());
                    fields.SetField("untitled103", GetApproverNameJobDes.Rows[0][0].ToString());
                }

            }
            catch(Exception ex)
            {
                uProfile.Status = ex.ToString();
            }
            #endregion

            //foreach (var field in af.Fields)
            //{
            //    Console.WriteLine("{0}, {1}", field.Key, field.Value);

            //    string sample2 = field.Key.ToString();
            //    //TextBox2.Text = sample + field.Key.ToString() + ',';
            //    fields.SetField(sample2, sample2);
            //}

            stamper.FormFlattening = true;
            stamper.Close();
            // "Flatten" the form so it wont be editable/usable anymor

            return uProfile.Status;



        }

        public struct DateTimeSpan
        {
            private readonly int years;
            private readonly int months;
            private readonly int days;
            private readonly int hours;
            private readonly int minutes;
            private readonly int seconds;
            private readonly int milliseconds;

            public DateTimeSpan(int years, int months, int days, int hours, int minutes, int seconds, int milliseconds)
            {
                this.years = years;
                this.months = months;
                this.days = days;
                this.hours = hours;
                this.minutes = minutes;
                this.seconds = seconds;
                this.milliseconds = milliseconds;
            }

            public int Years { get { return years; } }
            public int Months { get { return months; } }
            public int Days { get { return days; } }
            public int Hours { get { return hours; } }
            public int Minutes { get { return minutes; } }
            public int Seconds { get { return seconds; } }
            public int Milliseconds { get { return milliseconds; } }

            enum Phase { Years, Months, Days, Done }

            public static DateTimeSpan CompareDates(DateTime date1, DateTime date2)
            {
                if (date2 < date1)
                {
                    var sub = date1;
                    date1 = date2;
                    date2 = sub;
                }

                DateTime current = date1;
                int years = 0;
                int months = 0;
                int days = 0;

                Phase phase = Phase.Years;
                DateTimeSpan span = new DateTimeSpan();
                int officialDay = current.Day;

                while (phase != Phase.Done)
                {
                    switch (phase)
                    {
                        case Phase.Years:
                            if (current.AddYears(years + 1) > date2)
                            {
                                phase = Phase.Months;
                                current = current.AddYears(years);
                            }
                            else
                            {
                                years++;
                            }
                            break;
                        case Phase.Months:
                            if (current.AddMonths(months + 1) > date2)
                            {
                                phase = Phase.Days;
                                current = current.AddMonths(months);
                                if (current.Day < officialDay && officialDay <= DateTime.DaysInMonth(current.Year, current.Month))
                                    current = current.AddDays(officialDay - current.Day);
                            }
                            else
                            {
                                months++;
                            }
                            break;
                        case Phase.Days:
                            if (current.AddDays(days + 1) > date2)
                            {
                                current = current.AddDays(days);
                                var timespan = date2 - current;
                                span = new DateTimeSpan(years, months, days, timespan.Hours, timespan.Minutes, timespan.Seconds, timespan.Milliseconds);
                                phase = Phase.Done;
                            }
                            else
                            {
                                days++;
                            }
                            break;
                    }
                }
                return span;
            }
        }
        #endregion
        #region "Custom"
        [HttpPost]
        [Route("GetCustomForms")]
        public GetCustomFormsResult CustomFormsGet(SelCustomForms uProfile)
        {
            GetCustomFormsResult GetCustomFormsResult = new GetCustomFormsResult();
            //try
            //{
            Connection Connection = new Connection();
            List<CustomForms> CustomFormList = new List<CustomForms>();
            DataTable dtCUSTOM = new DataTable();
            //BasicFunction BasicFunction = new BasicFunction();
            Connection.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = uProfile.EmpID });
            dtCUSTOM = Connection.GetDataTable("sp_SearchReqAccess4");
            //CultureInfo provider = CultureInfo.InvariantCulture;
            if (dtCUSTOM.Rows.Count > 0)
            {
                foreach (DataRow row in dtCUSTOM.Rows)
                {
                    CustomForms Custom = new CustomForms
                    {
                        ID = row["ID"].ToString(),
                        Forms = row["Forms"].ToString(),
                        CodeName = row["CodeName"].ToString(),

                    };
                    CustomFormList.Add(Custom);
                }
                GetCustomFormsResult.CustomForms = CustomFormList;
                GetCustomFormsResult.myreturn = "Success";
            }
            else
            {
                GetCustomFormsResult.myreturn = "No Data Available";
            }
            return GetCustomFormsResult;
        }

        [HttpPost]
        [Route("SelectCustomForms")]
        public string CustomFormsSelect(SelCustomForms uProfile)
        {

            // GetPayslipResult.paysliplink = "http://illimitadodevs.azurewebsites.net/pdf/" + row["PDF"].ToString() + "-";
            DataTable getCOE = PayMtd.getdataforCOE(uProfile.EmpID);

            try
            {
                //PdfReader reader1 = new PdfReader("http://192.168.1.250:81/pdf/" + row["PDF"].ToString() + ".pdf");
                PdfReader reader1 = new PdfReader("http://ilm.illimitado.com/ilm/pdf/" + uProfile.Forms + ".pdf");
                // PdfReader reader1 = new PdfReader(HttpContext.Current.Server.MapPath("~/pdf/") + row["PDF"].ToString() + ".pdf");
                LocationTextExtractionStrategy lteStrategy = new LocationTextExtractionStrategy();
                string strExact = PdfTextExtractor.GetTextFromPage(reader1, 1, lteStrategy);
                StringBuilder sb = new StringBuilder();
                sb.Append(PdfTextExtractor.GetTextFromPage(reader1, 1));

                sb.Replace("[Employee ID]", uProfile.EmpID);
                sb.Replace("[Employee\nID]", uProfile.EmpID);
                if (getCOE.Rows.Count > 0)
                {
                    sb.Replace("[Employee Name]", getCOE.Rows[0][0].ToString() == "" ? "N/A" : getCOE.Rows[0][0].ToString());
                    sb.Replace("[Date Joined]", getCOE.Rows[0][1].ToString() == "" ? "N/A" : getCOE.Rows[0][1].ToString());
                    sb.Replace("[Employee\nName]", getCOE.Rows[0][0].ToString() == "" ? "N/A" : getCOE.Rows[0][0].ToString());
                    sb.Replace("[Date\nJoined]", getCOE.Rows[0][1].ToString() == "" ? "N/A" : getCOE.Rows[0][1].ToString());

                    sb.Replace("[Job Description]", getCOE.Rows[0][2].ToString() == "" ? "N/A" : getCOE.Rows[0][2].ToString());
                    sb.Replace("[Department Code]", getCOE.Rows[0][4].ToString() == "" ? "N/A" : getCOE.Rows[0][4].ToString());
                    sb.Replace("[Job\nDescription]", getCOE.Rows[0][2].ToString() == "" ? "N/A" : getCOE.Rows[0][2].ToString());
                    sb.Replace("[Department\nCode]", getCOE.Rows[0][4].ToString() == "" ? "N/A" : getCOE.Rows[0][4].ToString());

                    //Employer Details Start
                    #region Employer Details
                    sb.Replace("[Company Name]", getCOE.Rows[0]["TradingName"].ToString() == "" ? "N/A" : getCOE.Rows[0]["TradingName"].ToString());
                    sb.Replace("[Address]", getCOE.Rows[0]["Address"].ToString() == "" ? "N/A" : getCOE.Rows[0]["Address"].ToString());
                    sb.Replace("[ZIP]", getCOE.Rows[0]["AddZipCode"].ToString() == "" ? "N/A" : getCOE.Rows[0]["AddZipCode"].ToString());
                    sb.Replace("[Telephone]", getCOE.Rows[0]["ContactNumber"].ToString() == "" ? "N/A" : getCOE.Rows[0]["ContactNumber"].ToString());
                    sb.Replace("[Fax]", "N/A");
                    sb.Replace("[General Email]", getCOE.Rows[0]["Emailtxt"].ToString() == "" ? "N/A" : getCOE.Rows[0]["Emailtxt"].ToString());
                    sb.Replace("[TIN]", getCOE.Rows[0]["Tin"].ToString() == "" ? "N/A" : getCOE.Rows[0]["Tin"].ToString());
                    sb.Replace("[SSS]", getCOE.Rows[0]["Sss"].ToString() == "" ? "N/A" : getCOE.Rows[0]["Sss"].ToString());
                    sb.Replace("[Philhealth]", getCOE.Rows[0]["Philhealth"].ToString() == "" ? "N/A" : getCOE.Rows[0]["Philhealth"].ToString());
                    sb.Replace("[HDMF]", getCOE.Rows[0]["Pagibig"].ToString() == "" ? "N/A" : getCOE.Rows[0]["Pagibig"].ToString());
                    sb.Replace("[Web Page]", "N/A");
                    sb.Replace("[Company Registration]", getCOE.Rows[0]["CompanyRegistrationNo"].ToString() == "" ? "N/A" : getCOE.Rows[0]["CompanyRegistrationNo"].ToString());
                    sb.Replace("[Date of Registration]", getCOE.Rows[0]["DateOfRegistration"].ToString() == "" ? "N/A" : getCOE.Rows[0]["DateOfRegistration"].ToString());
                    sb.Replace("[Date of Registration]", getCOE.Rows[0]["DateOfRegistration"].ToString() == "" ? "N/A" : getCOE.Rows[0]["DateOfRegistration"].ToString());
                    sb.Replace("[Resigned]", "N/A");
                    #endregion
                    //Employer Details End

                    //Employee Profile Start
                    #region Employee Profile

                    sb.Replace("[Contract End Date]", "N/A");
                    sb.Replace("[Hourly Rate]", getCOE.Rows[0]["hourly"].ToString() == "" ? "N/A" : getCOE.Rows[0]["hourly"].ToString());
                    sb.Replace("[Daily Rate]", getCOE.Rows[0]["daily"].ToString() == "" ? "N/A" : getCOE.Rows[0]["daily"].ToString());
                    sb.Replace("[Monthly Rate]", getCOE.Rows[0]["monthly"].ToString() == "" ? "N/A" : getCOE.Rows[0]["monthly"].ToString());
                    sb.Replace("[Tax Status]", getCOE.Rows[0]["tax_status"].ToString() == "" ? "N/A" : getCOE.Rows[0]["tax_status"].ToString());
                    sb.Replace("[Tax Category]", "N/A");
                    #endregion
                    //Employee Profile End

                }


                string url = "http://ilm.illimitado.com/ilm/pdf/" + uProfile.Forms + ".pdf";
                //string url = "http://192.168.1.250:81/pdf/" + row["PDF"].ToString() + ".pdf";
                // The easiest way to load our document from the internet is make use of the 
                // System.Net.WebClient class. Create an instance of it and pass the URL
                // to download from.
                WebClient webClient = new WebClient();

                // Download the bytes from the location referenced by the URL.
                byte[] dataBytes = webClient.DownloadData(url);

                // Wrap the bytes representing the document in memory into a MemoryStream object.
                MemoryStream byteStream = new MemoryStream(dataBytes);


                // Stream stream = File.OpenRead(path );
                Aspose.Pdf.Document doc = new Aspose.Pdf.Document(byteStream);

                sb.Replace("[Signature 1]", "__________");
                sb.Replace("[Signature 2]", "__________");
                sb.Replace("[Signature 3]", "__________");
                sb.Replace("[Employee Signature]", "__________");
                sb.Replace("[Signature\n1]", "__________");
                sb.Replace("[Signature\n2]", "__________");
                sb.Replace("[Signature\n2]", "__________");
                sb.Replace("[Employee\nSignature]", "__________");


                FileStream Newstream = new FileStream(string.Concat(HttpContext.Current.Server.MapPath("~/pdf/"), uProfile.Forms + "-" + uProfile.EmpID + ".pdf"), FileMode.Create);


                MemoryStream ms = new MemoryStream();
                Document document = new Document(PageSize.A4, 25, 25, 30, 30);
                //Code for adding the string builder sb to the document
                PdfWriter writer = PdfWriter.GetInstance(document, Newstream);

                document.Open();
                document.Add(new Paragraph(sb.ToString()));

                PayMtd.GetSignatureSignPicture(uProfile.EmpID, "sign_1");
                //if (PayMtd.BitSign != null)
                //{


                //    List<Phrase> PhraseList = new List<Phrase>();
                //    for (int page = 1; page <= reader1.NumberOfPages; page++)
                //    {
                //        ITextExtractionStrategy strategy = new SimpleTextExtractionStrategy();
                //        string currentPageText = PdfTextExtractor.GetTextFromPage(reader1, page, strategy);
                //        if (currentPageText.Contains("[Employee Signature]") || currentPageText.Contains("[Employee\nSignature]"))
                //        {
                //            TextFragmentAbsorber absorber = new TextFragmentAbsorber("[Employee Signature]");


                //            // Accept the absorber for first page
                //            doc.Pages[1].Accept(absorber);

                //            // View text and placement info of first text occurrence
                //            TextFragment firstOccurrence = absorber.TextFragments[1];
                //            // document.Add(new Paragraph(Convert.ToDouble(firstOccurrence.Position.XIndent).ToString()));
                //            float xx = (float)firstOccurrence.Position.XIndent + 20;
                //            float yy = (float)firstOccurrence.Position.YIndent;


                //            string Bitsignature = null;
                //            Bitsignature = PayMtd.BitSign;
                //            Byte[] SignatureBit = null;
                //            iTextSharp.text.Image signatory = null;

                //            Bitsignature = Bitsignature.Replace(' ', '+');
                //            SignatureBit = Convert.FromBase64String(Bitsignature);
                //            signatory = iTextSharp.text.Image.GetInstance(SignatureBit);
                //            signatory.SetAbsolutePosition(xx, yy);
                //            signatory.ScaleToFit(40, 40);

                //            document.Add(signatory);
                //        }
                //        if (currentPageText.Contains("[President Signature]") || currentPageText.Contains("[President\nSignature]"))
                //        {
                //            TextFragmentAbsorber absorber = new TextFragmentAbsorber("[President Signature]");


                //            // Accept the absorber for first page
                //            doc.Pages[1].Accept(absorber);

                //            // View text and placement info of first text occurrence
                //            TextFragment firstOccurrence = absorber.TextFragments[1];
                //            // document.Add(new Paragraph(Convert.ToDouble(firstOccurrence.Position.XIndent).ToString()));
                //            float xx = (float)firstOccurrence.Position.XIndent + 20;
                //            float yy = (float)firstOccurrence.Position.YIndent;


                //            string Bitsignature = null;
                //            Bitsignature = PayMtd.BitSign;
                //            Byte[] SignatureBit = null;
                //            iTextSharp.text.Image signatory = null;

                //            Bitsignature = Bitsignature.Replace(' ', '+');
                //            SignatureBit = Convert.FromBase64String(Bitsignature);
                //            signatory = iTextSharp.text.Image.GetInstance(SignatureBit);
                //            signatory.SetAbsolutePosition(xx, yy);

                //            signatory.ScaleToFit(40, 40);

                //            document.Add(signatory);
                //        }

                //        if (currentPageText.Contains("[HR Signature]") || currentPageText.Contains("[HR\nSignature]"))
                //        {
                //            TextFragmentAbsorber absorber = new TextFragmentAbsorber("[HR Signature]");


                //            // Accept the absorber for first page
                //            doc.Pages[1].Accept(absorber);

                //            // View text and placement info of first text occurrence
                //            TextFragment firstOccurrence = absorber.TextFragments[1];
                //            // document.Add(new Paragraph(Convert.ToDouble(firstOccurrence.Position.XIndent).ToString()));
                //            float xx = (float)firstOccurrence.Position.XIndent + 20;
                //            float yy = (float)firstOccurrence.Position.YIndent;


                //            string Bitsignature = null;
                //            Bitsignature = PayMtd.BitSign;
                //            Byte[] SignatureBit = null;
                //            iTextSharp.text.Image signatory = null;

                //            Bitsignature = Bitsignature.Replace(' ', '+');
                //            SignatureBit = Convert.FromBase64String(Bitsignature);
                //            signatory = iTextSharp.text.Image.GetInstance(SignatureBit);
                //            signatory.SetAbsolutePosition(xx, yy);

                //            signatory.ScaleToFit(40, 40);

                //            document.Add(signatory);
                //        }

                //    }
                //}

                //PayMtd.GetSignatureSignPicturePres(empid);
                PayMtd.GetSignatureALL("Signature 1");
                PayMtd.GetSignatureALL("Signature 2");
                PayMtd.GetSignatureALL("Signature 3");
                if (PayMtd.BitSign != null)
                {


                    List<Phrase> PhraseList = new List<Phrase>();
                    for (int page = 1; page <= reader1.NumberOfPages; page++)
                    {
                        ITextExtractionStrategy strategy = new SimpleTextExtractionStrategy();
                        string currentPageText = PdfTextExtractor.GetTextFromPage(reader1, page, strategy);
                        if (currentPageText.Contains("[Employee Signature]") || currentPageText.Contains("[Employee\nSignature]"))
                        {
                            TextFragmentAbsorber absorber = new TextFragmentAbsorber("[Employee Signature]");


                            // Accept the absorber for first page
                            doc.Pages[1].Accept(absorber);

                            // View text and placement info of first text occurrence
                            TextFragment firstOccurrence = absorber.TextFragments[1];
                            // document.Add(new Paragraph(Convert.ToDouble(firstOccurrence.Position.XIndent).ToString()));
                            float xx = (float)firstOccurrence.Position.XIndent + 20;
                            float yy = (float)firstOccurrence.Position.YIndent;


                            string Bitsignature = null;
                            Bitsignature = PayMtd.BitSign;
                            Byte[] SignatureBit = null;
                            iTextSharp.text.Image signatory = null;

                            Bitsignature = Bitsignature.Replace(' ', '+');
                            SignatureBit = Convert.FromBase64String(Bitsignature);
                            signatory = iTextSharp.text.Image.GetInstance(SignatureBit);
                            signatory.SetAbsolutePosition(xx, yy);
                            signatory.ScaleToFit(50, 50);

                            document.Add(signatory);
                        }

                    }
                }

                if (PayMtd.sig1 != null)
                {


                    List<Phrase> PhraseList = new List<Phrase>();
                    for (int page = 1; page <= reader1.NumberOfPages; page++)
                    {
                        ITextExtractionStrategy strategy = new SimpleTextExtractionStrategy();
                        string currentPageText = PdfTextExtractor.GetTextFromPage(reader1, page, strategy);
                        if (currentPageText.Contains("[Signature 1]") || currentPageText.Contains("[Signature\n1]"))
                        {
                            TextFragmentAbsorber absorber = new TextFragmentAbsorber("[Signature 1]");


                            // Accept the absorber for first page
                            doc.Pages[1].Accept(absorber);

                            // View text and placement info of first text occurrence
                            TextFragment firstOccurrence = absorber.TextFragments[1];
                            // document.Add(new Paragraph(Convert.ToDouble(firstOccurrence.Position.XIndent).ToString()));
                            float xx = (float)firstOccurrence.Position.XIndent + 20;
                            float yy = (float)firstOccurrence.Position.YIndent;


                            string Bitsignature = null;
                            Bitsignature = PayMtd.sig1;
                            Byte[] SignatureBit = null;
                            iTextSharp.text.Image signatory = null;

                            Bitsignature = Bitsignature.Replace(' ', '+');
                            SignatureBit = Convert.FromBase64String(Bitsignature);
                            signatory = iTextSharp.text.Image.GetInstance(SignatureBit);
                            signatory.SetAbsolutePosition(xx, yy);

                            signatory.ScaleToFit(50, 50);

                            document.Add(signatory);
                        }


                    }
                }

                if (PayMtd.sig2 != null)
                {


                    List<Phrase> PhraseList = new List<Phrase>();
                    for (int page = 1; page <= reader1.NumberOfPages; page++)
                    {
                        ITextExtractionStrategy strategy = new SimpleTextExtractionStrategy();
                        string currentPageText = PdfTextExtractor.GetTextFromPage(reader1, page, strategy);
                        if (currentPageText.Contains("[Signature 2]") || currentPageText.Contains("[Signature\n2]"))
                        {
                            TextFragmentAbsorber absorber = new TextFragmentAbsorber("[Signature 2]");


                            // Accept the absorber for first page
                            doc.Pages[1].Accept(absorber);

                            // View text and placement info of first text occurrence
                            TextFragment firstOccurrence = absorber.TextFragments[1];
                            // document.Add(new Paragraph(Convert.ToDouble(firstOccurrence.Position.XIndent).ToString()));
                            float xx = (float)firstOccurrence.Position.XIndent + 20;
                            float yy = (float)firstOccurrence.Position.YIndent;


                            string Bitsignature = null;
                            Bitsignature = PayMtd.sig2;
                            Byte[] SignatureBit = null;
                            iTextSharp.text.Image signatory = null;

                            Bitsignature = Bitsignature.Replace(' ', '+');
                            SignatureBit = Convert.FromBase64String(Bitsignature);
                            signatory = iTextSharp.text.Image.GetInstance(SignatureBit);
                            signatory.SetAbsolutePosition(xx, yy);

                            signatory.ScaleToFit(50, 50);

                            document.Add(signatory);
                        }


                    }
                }

                if (PayMtd.sig3 != null)
                {


                    List<Phrase> PhraseList = new List<Phrase>();
                    for (int page = 1; page <= reader1.NumberOfPages; page++)
                    {
                        ITextExtractionStrategy strategy = new SimpleTextExtractionStrategy();
                        string currentPageText = PdfTextExtractor.GetTextFromPage(reader1, page, strategy);
                        if (currentPageText.Contains("[Signature 3]") || currentPageText.Contains("[Signature\n3]"))
                        {
                            TextFragmentAbsorber absorber = new TextFragmentAbsorber("[Signature 3]");


                            // Accept the absorber for first page
                            doc.Pages[1].Accept(absorber);

                            // View text and placement info of first text occurrence
                            TextFragment firstOccurrence = absorber.TextFragments[1];
                            // document.Add(new Paragraph(Convert.ToDouble(firstOccurrence.Position.XIndent).ToString()));
                            float xx = (float)firstOccurrence.Position.XIndent + 20;
                            float yy = (float)firstOccurrence.Position.YIndent;


                            string Bitsignature = null;
                            Bitsignature = PayMtd.sig3;
                            Byte[] SignatureBit = null;
                            iTextSharp.text.Image signatory = null;

                            Bitsignature = Bitsignature.Replace(' ', '+');
                            SignatureBit = Convert.FromBase64String(Bitsignature);
                            signatory = iTextSharp.text.Image.GetInstance(SignatureBit);
                            signatory.SetAbsolutePosition(xx, yy);

                            signatory.ScaleToFit(50, 50);

                            document.Add(signatory);
                        }


                    }
                }






                document.Close();
                writer.Close();
                // closing the stamper
                uProfile.Status = "Success";

            }
            catch (Exception ex)
            {
                uProfile.Status = ex.ToString();
                //this.Page.ClientScript.RegisterClientScriptBlock(this.GetType(),
                //Guid.NewGuid().ToString("N"), "self.parent.location='login.aspx';", true);
            }

            return uProfile.Status;
        }



        #endregion

        [HttpPost]
        [Route("GetCustomFormsv2")]
        public GetCustomFormsResult CustomFormsGetv2(SelCustomFormsv2 uProfile)
        {
            GetCustomFormsResult GetCustomFormsResult = new GetCustomFormsResult();
            //try
            //{
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = uProfile.CN;
            SelfieRegistration2Controller.GetDB2(GDB);


            Connection Connection = new Connection();
            List<CustomForms> CustomFormList = new List<CustomForms>();
            DataTable dtCUSTOM = new DataTable();
            //BasicFunction BasicFunction = new BasicFunction();
            Connection.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = uProfile.EmpID });
            dtCUSTOM = Connection.GetDataTable("sp_SearchReqAccess4");
            //CultureInfo provider = CultureInfo.InvariantCulture;
            if (dtCUSTOM.Rows.Count > 0)
            {
                foreach (DataRow row in dtCUSTOM.Rows)
                {
                    CustomForms Custom = new CustomForms
                    {
                        ID = row["ID"].ToString(),
                        Forms = row["Forms"].ToString(),
                        CodeName = row["CodeName"].ToString(),

                    };
                    CustomFormList.Add(Custom);
                }
                GetCustomFormsResult.CustomForms = CustomFormList;
                GetCustomFormsResult.myreturn = "Success";
            }
            else
            {
                GetCustomFormsResult.myreturn = "No Data Available";
            }
            return GetCustomFormsResult;
        }

        [HttpPost]
        [Route("SelectCustomFormsv2")]
        public string CustomFormsSelectv2(SelCustomFormsv2 uProfile)
        {
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = uProfile.CN;
            SelfieRegistration2Controller.GetDB2(GDB);

            // GetPayslipResult.paysliplink = "http://illimitadodevs.azurewebsites.net/pdf/" + row["PDF"].ToString() + "-";
            DataTable getCOE = PayMtd.getdataforCOE(uProfile.EmpID);

            try
            {
                //PdfReader reader1 = new PdfReader("http://192.168.1.250:81/pdf/" + row["PDF"].ToString() + ".pdf");
                PdfReader reader1 = new PdfReader("http://ilm.illimitado.com/ilm/pdf/" + uProfile.Forms + ".pdf");
                // PdfReader reader1 = new PdfReader(HttpContext.Current.Server.MapPath("~/pdf/") + row["PDF"].ToString() + ".pdf");
                LocationTextExtractionStrategy lteStrategy = new LocationTextExtractionStrategy();
                string strExact = PdfTextExtractor.GetTextFromPage(reader1, 1, lteStrategy);
                StringBuilder sb = new StringBuilder();
                sb.Append(PdfTextExtractor.GetTextFromPage(reader1, 1));

                sb.Replace("[Employee ID]", uProfile.EmpID);
                sb.Replace("[Employee\nID]", uProfile.EmpID);
                if (getCOE.Rows.Count > 0)
                {
                    sb.Replace("[Employee Name]", getCOE.Rows[0][0].ToString() == "" ? "N/A" : getCOE.Rows[0][0].ToString());
                    sb.Replace("[Date Joined]", getCOE.Rows[0][1].ToString() == "" ? "N/A" : getCOE.Rows[0][1].ToString());
                    sb.Replace("[Employee\nName]", getCOE.Rows[0][0].ToString() == "" ? "N/A" : getCOE.Rows[0][0].ToString());
                    sb.Replace("[Date\nJoined]", getCOE.Rows[0][1].ToString() == "" ? "N/A" : getCOE.Rows[0][1].ToString());

                    sb.Replace("[Job Description]", getCOE.Rows[0][2].ToString() == "" ? "N/A" : getCOE.Rows[0][2].ToString());
                    sb.Replace("[Department Code]", getCOE.Rows[0][4].ToString() == "" ? "N/A" : getCOE.Rows[0][4].ToString());
                    sb.Replace("[Job\nDescription]", getCOE.Rows[0][2].ToString() == "" ? "N/A" : getCOE.Rows[0][2].ToString());
                    sb.Replace("[Department\nCode]", getCOE.Rows[0][4].ToString() == "" ? "N/A" : getCOE.Rows[0][4].ToString());

                    //Employer Details Start
                    #region Employer Details
                    sb.Replace("[Company Name]", getCOE.Rows[0]["TradingName"].ToString() == "" ? "N/A" : getCOE.Rows[0]["TradingName"].ToString());
                    sb.Replace("[Address]", getCOE.Rows[0]["Address"].ToString() == "" ? "N/A" : getCOE.Rows[0]["Address"].ToString());
                    sb.Replace("[ZIP]", getCOE.Rows[0]["AddZipCode"].ToString() == "" ? "N/A" : getCOE.Rows[0]["AddZipCode"].ToString());
                    sb.Replace("[Telephone]", getCOE.Rows[0]["ContactNumber"].ToString() == "" ? "N/A" : getCOE.Rows[0]["ContactNumber"].ToString());
                    sb.Replace("[Fax]", "N/A");
                    sb.Replace("[General Email]", getCOE.Rows[0]["Emailtxt"].ToString() == "" ? "N/A" : getCOE.Rows[0]["Email   txt"].ToString());
                    sb.Replace("[TIN]", getCOE.Rows[0]["Tin"].ToString() == "" ? "N/A" : getCOE.Rows[0]["Tin"].ToString());
                    sb.Replace("[SSS]", getCOE.Rows[0]["Sss"].ToString() == "" ? "N/A" : getCOE.Rows[0]["Sss"].ToString());
                    sb.Replace("[Philhealth]", getCOE.Rows[0]["Philhealth"].ToString() == "" ? "N/A" : getCOE.Rows[0]["Philhealth"].ToString());
                    sb.Replace("[HDMF]", getCOE.Rows[0]["Pagibig"].ToString() == "" ? "N/A" : getCOE.Rows[0]["Pagibig"].ToString());
                    sb.Replace("[Web Page]", "N/A");
                    sb.Replace("[Company Registration]", getCOE.Rows[0]["CompanyRegistrationNo"].ToString() == "" ? "N/A" : getCOE.Rows[0]["CompanyRegistrationNo"].ToString());
                    sb.Replace("[Date of Registration]", getCOE.Rows[0]["DateOfRegistration"].ToString() == "" ? "N/A" : getCOE.Rows[0]["DateOfRegistration"].ToString());
                    sb.Replace("[Date of Registration]", getCOE.Rows[0]["DateOfRegistration"].ToString() == "" ? "N/A" : getCOE.Rows[0]["DateOfRegistration"].ToString());
                    sb.Replace("[Resigned]", "N/A");
                    #endregion
                    //Employer Details End

                    //Employee Profile Start
                    #region Employee Profile

                    sb.Replace("[Contract End Date]", "N/A");
                    sb.Replace("[Hourly Rate]", getCOE.Rows[0]["hourly"].ToString() == "" ? "N/A" : getCOE.Rows[0]["hourly"].ToString());
                    sb.Replace("[Daily Rate]", getCOE.Rows[0]["daily"].ToString() == "" ? "N/A" : getCOE.Rows[0]["daily"].ToString());
                    sb.Replace("[Monthly Rate]", getCOE.Rows[0]["monthly"].ToString() == "" ? "N/A" : getCOE.Rows[0]["monthly"].ToString());
                    sb.Replace("[Tax Status]", getCOE.Rows[0]["tax_status"].ToString() == "" ? "N/A" : getCOE.Rows[0]["tax_status"].ToString());
                    sb.Replace("[Tax Category]", "N/A");
                    #endregion
                    //Employee Profile End

                }


                string url = "http://ilm.illimitado.com/ilm/pdf/" + uProfile.Forms + ".pdf";
                //string url = "http://192.168.1.250:81/pdf/" + row["PDF"].ToString() + ".pdf";
                // The easiest way to load our document from the internet is make use of the 
                // System.Net.WebClient class. Create an instance of it and pass the URL
                // to download from.
                WebClient webClient = new WebClient();

                // Download the bytes from the location referenced by the URL.
                byte[] dataBytes = webClient.DownloadData(url);

                // Wrap the bytes representing the document in memory into a MemoryStream object.
                MemoryStream byteStream = new MemoryStream(dataBytes);


                // Stream stream = File.OpenRead(path );
                Aspose.Pdf.Document doc = new Aspose.Pdf.Document(byteStream);

                sb.Replace("[Signature 1]", "__________");
                sb.Replace("[Signature 2]", "__________");
                sb.Replace("[Signature 3]", "__________");
                sb.Replace("[Employee Signature]", "__________");
                sb.Replace("[Signature\n1]", "__________");
                sb.Replace("[Signature\n2]", "__________");
                sb.Replace("[Signature\n2]", "__________");
                sb.Replace("[Employee\nSignature]", "__________");


                FileStream Newstream = new FileStream(string.Concat(HttpContext.Current.Server.MapPath("~/pdf/"), uProfile.Forms + "-" + uProfile.EmpID + ".pdf"), FileMode.Create);


                MemoryStream ms = new MemoryStream();
                Document document = new Document(PageSize.A4, 25, 25, 30, 30);
                //Code for adding the string builder sb to the document
                PdfWriter writer = PdfWriter.GetInstance(document, Newstream);

                document.Open();
                document.Add(new Paragraph(sb.ToString()));

                PayMtd.GetSignatureSignPicture(uProfile.EmpID, "sign_1");
                //if (PayMtd.BitSign != null)
                //{


                //    List<Phrase> PhraseList = new List<Phrase>();
                //    for (int page = 1; page <= reader1.NumberOfPages; page++)
                //    {
                //        ITextExtractionStrategy strategy = new SimpleTextExtractionStrategy();
                //        string currentPageText = PdfTextExtractor.GetTextFromPage(reader1, page, strategy);
                //        if (currentPageText.Contains("[Employee Signature]") || currentPageText.Contains("[Employee\nSignature]"))
                //        {
                //            TextFragmentAbsorber absorber = new TextFragmentAbsorber("[Employee Signature]");


                //            // Accept the absorber for first page
                //            doc.Pages[1].Accept(absorber);

                //            // View text and placement info of first text occurrence
                //            TextFragment firstOccurrence = absorber.TextFragments[1];
                //            // document.Add(new Paragraph(Convert.ToDouble(firstOccurrence.Position.XIndent).ToString()));
                //            float xx = (float)firstOccurrence.Position.XIndent + 20;
                //            float yy = (float)firstOccurrence.Position.YIndent;


                //            string Bitsignature = null;
                //            Bitsignature = PayMtd.BitSign;
                //            Byte[] SignatureBit = null;
                //            iTextSharp.text.Image signatory = null;

                //            Bitsignature = Bitsignature.Replace(' ', '+');
                //            SignatureBit = Convert.FromBase64String(Bitsignature);
                //            signatory = iTextSharp.text.Image.GetInstance(SignatureBit);
                //            signatory.SetAbsolutePosition(xx, yy);
                //            signatory.ScaleToFit(40, 40);

                //            document.Add(signatory);
                //        }
                //        if (currentPageText.Contains("[President Signature]") || currentPageText.Contains("[President\nSignature]"))
                //        {
                //            TextFragmentAbsorber absorber = new TextFragmentAbsorber("[President Signature]");


                //            // Accept the absorber for first page
                //            doc.Pages[1].Accept(absorber);

                //            // View text and placement info of first text occurrence
                //            TextFragment firstOccurrence = absorber.TextFragments[1];
                //            // document.Add(new Paragraph(Convert.ToDouble(firstOccurrence.Position.XIndent).ToString()));
                //            float xx = (float)firstOccurrence.Position.XIndent + 20;
                //            float yy = (float)firstOccurrence.Position.YIndent;


                //            string Bitsignature = null;
                //            Bitsignature = PayMtd.BitSign;
                //            Byte[] SignatureBit = null;
                //            iTextSharp.text.Image signatory = null;

                //            Bitsignature = Bitsignature.Replace(' ', '+');
                //            SignatureBit = Convert.FromBase64String(Bitsignature);
                //            signatory = iTextSharp.text.Image.GetInstance(SignatureBit);
                //            signatory.SetAbsolutePosition(xx, yy);

                //            signatory.ScaleToFit(40, 40);

                //            document.Add(signatory);
                //        }

                //        if (currentPageText.Contains("[HR Signature]") || currentPageText.Contains("[HR\nSignature]"))
                //        {
                //            TextFragmentAbsorber absorber = new TextFragmentAbsorber("[HR Signature]");


                //            // Accept the absorber for first page
                //            doc.Pages[1].Accept(absorber);

                //            // View text and placement info of first text occurrence
                //            TextFragment firstOccurrence = absorber.TextFragments[1];
                //            // document.Add(new Paragraph(Convert.ToDouble(firstOccurrence.Position.XIndent).ToString()));
                //            float xx = (float)firstOccurrence.Position.XIndent + 20;
                //            float yy = (float)firstOccurrence.Position.YIndent;


                //            string Bitsignature = null;
                //            Bitsignature = PayMtd.BitSign;
                //            Byte[] SignatureBit = null;
                //            iTextSharp.text.Image signatory = null;

                //            Bitsignature = Bitsignature.Replace(' ', '+');
                //            SignatureBit = Convert.FromBase64String(Bitsignature);
                //            signatory = iTextSharp.text.Image.GetInstance(SignatureBit);
                //            signatory.SetAbsolutePosition(xx, yy);

                //            signatory.ScaleToFit(40, 40);

                //            document.Add(signatory);
                //        }

                //    }
                //}

                //PayMtd.GetSignatureSignPicturePres(empid);
                PayMtd.GetSignatureALL("Signature 1");
                PayMtd.GetSignatureALL("Signature 2");
                PayMtd.GetSignatureALL("Signature 3");
                if (PayMtd.BitSign != null)
                {


                    List<Phrase> PhraseList = new List<Phrase>();
                    for (int page = 1; page <= reader1.NumberOfPages; page++)
                    {
                        ITextExtractionStrategy strategy = new SimpleTextExtractionStrategy();
                        string currentPageText = PdfTextExtractor.GetTextFromPage(reader1, page, strategy);
                        if (currentPageText.Contains("[Employee Signature]") || currentPageText.Contains("[Employee\nSignature]"))
                        {
                            TextFragmentAbsorber absorber = new TextFragmentAbsorber("[Employee Signature]");


                            // Accept the absorber for first page
                            doc.Pages[1].Accept(absorber);

                            // View text and placement info of first text occurrence
                            TextFragment firstOccurrence = absorber.TextFragments[1];
                            // document.Add(new Paragraph(Convert.ToDouble(firstOccurrence.Position.XIndent).ToString()));
                            float xx = (float)firstOccurrence.Position.XIndent + 20;
                            float yy = (float)firstOccurrence.Position.YIndent;


                            string Bitsignature = null;
                            Bitsignature = PayMtd.BitSign;
                            Byte[] SignatureBit = null;
                            iTextSharp.text.Image signatory = null;

                            Bitsignature = Bitsignature.Replace(' ', '+');
                            SignatureBit = Convert.FromBase64String(Bitsignature);
                            signatory = iTextSharp.text.Image.GetInstance(SignatureBit);
                            signatory.SetAbsolutePosition(xx, yy);
                            signatory.ScaleToFit(50, 50);

                            document.Add(signatory);
                        }

                    }
                }

                if (PayMtd.sig1 != null)
                {


                    List<Phrase> PhraseList = new List<Phrase>();
                    for (int page = 1; page <= reader1.NumberOfPages; page++)
                    {
                        ITextExtractionStrategy strategy = new SimpleTextExtractionStrategy();
                        string currentPageText = PdfTextExtractor.GetTextFromPage(reader1, page, strategy);
                        if (currentPageText.Contains("[Signature 1]") || currentPageText.Contains("[Signature\n1]"))
                        {
                            TextFragmentAbsorber absorber = new TextFragmentAbsorber("[Signature 1]");


                            // Accept the absorber for first page
                            doc.Pages[1].Accept(absorber);

                            // View text and placement info of first text occurrence
                            TextFragment firstOccurrence = absorber.TextFragments[1];
                            // document.Add(new Paragraph(Convert.ToDouble(firstOccurrence.Position.XIndent).ToString()));
                            float xx = (float)firstOccurrence.Position.XIndent + 20;
                            float yy = (float)firstOccurrence.Position.YIndent;


                            string Bitsignature = null;
                            Bitsignature = PayMtd.sig1;
                            Byte[] SignatureBit = null;
                            iTextSharp.text.Image signatory = null;

                            Bitsignature = Bitsignature.Replace(' ', '+');
                            SignatureBit = Convert.FromBase64String(Bitsignature);
                            signatory = iTextSharp.text.Image.GetInstance(SignatureBit);
                            signatory.SetAbsolutePosition(xx, yy);

                            signatory.ScaleToFit(50, 50);

                            document.Add(signatory);
                        }


                    }
                }

                if (PayMtd.sig2 != null)
                {


                    List<Phrase> PhraseList = new List<Phrase>();
                    for (int page = 1; page <= reader1.NumberOfPages; page++)
                    {
                        ITextExtractionStrategy strategy = new SimpleTextExtractionStrategy();
                        string currentPageText = PdfTextExtractor.GetTextFromPage(reader1, page, strategy);
                        if (currentPageText.Contains("[Signature 2]") || currentPageText.Contains("[Signature\n2]"))
                        {
                            TextFragmentAbsorber absorber = new TextFragmentAbsorber("[Signature 2]");


                            // Accept the absorber for first page
                            doc.Pages[1].Accept(absorber);

                            // View text and placement info of first text occurrence
                            TextFragment firstOccurrence = absorber.TextFragments[1];
                            // document.Add(new Paragraph(Convert.ToDouble(firstOccurrence.Position.XIndent).ToString()));
                            float xx = (float)firstOccurrence.Position.XIndent + 20;
                            float yy = (float)firstOccurrence.Position.YIndent;


                            string Bitsignature = null;
                            Bitsignature = PayMtd.sig2;
                            Byte[] SignatureBit = null;
                            iTextSharp.text.Image signatory = null;

                            Bitsignature = Bitsignature.Replace(' ', '+');
                            SignatureBit = Convert.FromBase64String(Bitsignature);
                            signatory = iTextSharp.text.Image.GetInstance(SignatureBit);
                            signatory.SetAbsolutePosition(xx, yy);

                            signatory.ScaleToFit(50, 50);

                            document.Add(signatory);
                        }


                    }
                }

                if (PayMtd.sig3 != null)
                {


                    List<Phrase> PhraseList = new List<Phrase>();
                    for (int page = 1; page <= reader1.NumberOfPages; page++)
                    {
                        ITextExtractionStrategy strategy = new SimpleTextExtractionStrategy();
                        string currentPageText = PdfTextExtractor.GetTextFromPage(reader1, page, strategy);
                        if (currentPageText.Contains("[Signature 3]") || currentPageText.Contains("[Signature\n3]"))
                        {
                            TextFragmentAbsorber absorber = new TextFragmentAbsorber("[Signature 3]");


                            // Accept the absorber for first page
                            doc.Pages[1].Accept(absorber);

                            // View text and placement info of first text occurrence
                            TextFragment firstOccurrence = absorber.TextFragments[1];
                            // document.Add(new Paragraph(Convert.ToDouble(firstOccurrence.Position.XIndent).ToString()));
                            float xx = (float)firstOccurrence.Position.XIndent + 20;
                            float yy = (float)firstOccurrence.Position.YIndent;


                            string Bitsignature = null;
                            Bitsignature = PayMtd.sig3;
                            Byte[] SignatureBit = null;
                            iTextSharp.text.Image signatory = null;

                            Bitsignature = Bitsignature.Replace(' ', '+');
                            SignatureBit = Convert.FromBase64String(Bitsignature);
                            signatory = iTextSharp.text.Image.GetInstance(SignatureBit);
                            signatory.SetAbsolutePosition(xx, yy);

                            signatory.ScaleToFit(50, 50);

                            document.Add(signatory);
                        }


                    }
                }






                document.Close();
                writer.Close();
                // closing the stamper
                uProfile.Status = "Success";

            }
            catch (Exception ex)
            {
                uProfile.Status = ex.ToString();
                //this.Page.ClientScript.RegisterClientScriptBlock(this.GetType(),
                //Guid.NewGuid().ToString("N"), "self.parent.location='login.aspx';", true);
            }

            return uProfile.Status;
        }
        [HttpPost]
        [Route("SelectCustomFormsv3")]
        public string CustomFormsSelectv3(SelCustomFormsv2 uProfile)
        {
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = uProfile.CN;
            string clientLink = SelfieRegistration2Controller.GetDB3(GDB);
            // GetPayslipResult.paysliplink = "http://illimitadodevs.azurewebsites.net/pdf/" + row["PDF"].ToString() + "-";
            DataTable getCOE = PayMtd.getdataforCOE(uProfile.EmpID);

            try
            {
                //PdfReader reader1 = new PdfReader("http://192.168.1.250:81/pdf/" + row["PDF"].ToString() + ".pdf");
                PdfReader reader1 = new PdfReader(clientLink + "pdf/" + uProfile.Forms + ".pdf");
                // PdfReader reader1 = new PdfReader(HttpContext.Current.Server.MapPath("~/pdf/") + row["PDF"].ToString() + ".pdf");
                LocationTextExtractionStrategy lteStrategy = new LocationTextExtractionStrategy();
                string strExact = PdfTextExtractor.GetTextFromPage(reader1, 1, lteStrategy);
                StringBuilder sb = new StringBuilder();
                sb.Append(PdfTextExtractor.GetTextFromPage(reader1, 1));

                sb.Replace("[Employee ID]", uProfile.EmpID);
                sb.Replace("[Employee\nID]", uProfile.EmpID);
                if (getCOE.Rows.Count > 0)
                {
                    sb.Replace("[Employee Name]", getCOE.Rows[0][0].ToString() == "" ? "N/A" : getCOE.Rows[0][0].ToString());
                    sb.Replace("[Date Joined]", getCOE.Rows[0][1].ToString() == "" ? "N/A" : getCOE.Rows[0][1].ToString());
                    sb.Replace("[Employee\nName]", getCOE.Rows[0][0].ToString() == "" ? "N/A" : getCOE.Rows[0][0].ToString());
                    sb.Replace("[Date\nJoined]", getCOE.Rows[0][1].ToString() == "" ? "N/A" : getCOE.Rows[0][1].ToString());

                    sb.Replace("[Job Description]", getCOE.Rows[0][2].ToString() == "" ? "N/A" : getCOE.Rows[0][2].ToString());
                    sb.Replace("[Department Code]", getCOE.Rows[0][4].ToString() == "" ? "N/A" : getCOE.Rows[0][4].ToString());
                    sb.Replace("[Job\nDescription]", getCOE.Rows[0][2].ToString() == "" ? "N/A" : getCOE.Rows[0][2].ToString());
                    sb.Replace("[Department\nCode]", getCOE.Rows[0][4].ToString() == "" ? "N/A" : getCOE.Rows[0][4].ToString());

                    //Employer Details Start
                    #region Employer Details
                    sb.Replace("[Company Name]", getCOE.Rows[0]["TradingName"].ToString() == "" ? "N/A" : getCOE.Rows[0]["TradingName"].ToString());
                    sb.Replace("[Address]", getCOE.Rows[0]["Address"].ToString() == "" ? "N/A" : getCOE.Rows[0]["Address"].ToString());
                    sb.Replace("[ZIP]", getCOE.Rows[0]["AddZipCode"].ToString() == "" ? "N/A" : getCOE.Rows[0]["AddZipCode"].ToString());
                    sb.Replace("[Telephone]", getCOE.Rows[0]["ContactNumber"].ToString() == "" ? "N/A" : getCOE.Rows[0]["ContactNumber"].ToString());
                    sb.Replace("[Fax]", "N/A");
                    sb.Replace("[General Email]", getCOE.Rows[0]["Emailtxt"].ToString() == "" ? "N/A" : getCOE.Rows[0]["Emailtxt"].ToString());
                    sb.Replace("[TIN]", getCOE.Rows[0]["Tin"].ToString() == "" ? "N/A" : getCOE.Rows[0]["Tin"].ToString());
                    sb.Replace("[SSS]", getCOE.Rows[0]["Sss"].ToString() == "" ? "N/A" : getCOE.Rows[0]["Sss"].ToString());
                    sb.Replace("[Philhealth]", getCOE.Rows[0]["Philhealth"].ToString() == "" ? "N/A" : getCOE.Rows[0]["Philhealth"].ToString());
                    sb.Replace("[HDMF]", getCOE.Rows[0]["Pagibig"].ToString() == "" ? "N/A" : getCOE.Rows[0]["Pagibig"].ToString());
                    sb.Replace("[Web Page]", "N/A");
                    sb.Replace("[Company Registration]", getCOE.Rows[0]["CompanyRegistrationNo"].ToString() == "" ? "N/A" : getCOE.Rows[0]["CompanyRegistrationNo"].ToString());
                    sb.Replace("[Date of Registration]", getCOE.Rows[0]["DateOfRegistration"].ToString() == "" ? "N/A" : getCOE.Rows[0]["DateOfRegistration"].ToString());
                    sb.Replace("[Date of Registration]", getCOE.Rows[0]["DateOfRegistration"].ToString() == "" ? "N/A" : getCOE.Rows[0]["DateOfRegistration"].ToString());
                    sb.Replace("[Resigned]", "N/A");
                    #endregion
                    //Employer Details End

                    //Employee Profile Start
                    #region Employee Profile

                    sb.Replace("[Contract End Date]", "N/A");
                    sb.Replace("[Hourly Rate]", getCOE.Rows[0]["hourly"].ToString() == "" ? "N/A" : getCOE.Rows[0]["hourly"].ToString());
                    sb.Replace("[Daily Rate]", getCOE.Rows[0]["daily"].ToString() == "" ? "N/A" : getCOE.Rows[0]["daily"].ToString());
                    sb.Replace("[Monthly Rate]", getCOE.Rows[0]["monthly"].ToString() == "" ? "N/A" : getCOE.Rows[0]["monthly"].ToString());
                    sb.Replace("[Tax Status]", getCOE.Rows[0]["tax_status"].ToString() == "" ? "N/A" : getCOE.Rows[0]["tax_status"].ToString());
                    sb.Replace("[Tax Category]", "N/A");
                    #endregion
                    //Employee Profile End

                }


                string url = clientLink + "pdf/" + uProfile.Forms + ".pdf";
                //string url = "http://192.168.1.250:81/pdf/" + row["PDF"].ToString() + ".pdf";
                // The easiest way to load our document from the internet is make use of the 
                // System.Net.WebClient class. Create an instance of it and pass the URL
                // to download from.
                WebClient webClient = new WebClient();

                // Download the bytes from the location referenced by the URL.
                byte[] dataBytes = webClient.DownloadData(url);

                // Wrap the bytes representing the document in memory into a MemoryStream object.
                MemoryStream byteStream = new MemoryStream(dataBytes);


                // Stream stream = File.OpenRead(path );
                Aspose.Pdf.Document doc = new Aspose.Pdf.Document(byteStream);

                sb.Replace("[Signature 1]", "__________");
                sb.Replace("[Signature 2]", "__________");
                sb.Replace("[Signature 3]", "__________");
                sb.Replace("[Employee Signature]", "__________");
                sb.Replace("[Signature\n1]", "__________");
                sb.Replace("[Signature\n2]", "__________");
                sb.Replace("[Signature\n2]", "__________");
                sb.Replace("[Employee\nSignature]", "__________");


                FileStream Newstream = new FileStream(string.Concat(HttpContext.Current.Server.MapPath(uProfile.CN + "/pdf/"), uProfile.Forms + "-" + uProfile.EmpID + ".pdf"), FileMode.Create);


                MemoryStream ms = new MemoryStream();
                Document document = new Document(PageSize.A4, 25, 25, 30, 30);
                //Code for adding the string builder sb to the document
                PdfWriter writer = PdfWriter.GetInstance(document, Newstream);

                document.Open();
                document.Add(new Paragraph(sb.ToString()));

                PayMtd.GetSignatureSignPicture(uProfile.EmpID, "sign_1");
                //if (PayMtd.BitSign != null)
                //{


                //    List<Phrase> PhraseList = new List<Phrase>();
                //    for (int page = 1; page <= reader1.NumberOfPages; page++)
                //    {
                //        ITextExtractionStrategy strategy = new SimpleTextExtractionStrategy();
                //        string currentPageText = PdfTextExtractor.GetTextFromPage(reader1, page, strategy);
                //        if (currentPageText.Contains("[Employee Signature]") || currentPageText.Contains("[Employee\nSignature]"))
                //        {
                //            TextFragmentAbsorber absorber = new TextFragmentAbsorber("[Employee Signature]");


                //            // Accept the absorber for first page
                //            doc.Pages[1].Accept(absorber);

                //            // View text and placement info of first text occurrence
                //            TextFragment firstOccurrence = absorber.TextFragments[1];
                //            // document.Add(new Paragraph(Convert.ToDouble(firstOccurrence.Position.XIndent).ToString()));
                //            float xx = (float)firstOccurrence.Position.XIndent + 20;
                //            float yy = (float)firstOccurrence.Position.YIndent;


                //            string Bitsignature = null;
                //            Bitsignature = PayMtd.BitSign;
                //            Byte[] SignatureBit = null;
                //            iTextSharp.text.Image signatory = null;

                //            Bitsignature = Bitsignature.Replace(' ', '+');
                //            SignatureBit = Convert.FromBase64String(Bitsignature);
                //            signatory = iTextSharp.text.Image.GetInstance(SignatureBit);
                //            signatory.SetAbsolutePosition(xx, yy);
                //            signatory.ScaleToFit(40, 40);

                //            document.Add(signatory);
                //        }
                //        if (currentPageText.Contains("[President Signature]") || currentPageText.Contains("[President\nSignature]"))
                //        {
                //            TextFragmentAbsorber absorber = new TextFragmentAbsorber("[President Signature]");


                //            // Accept the absorber for first page
                //            doc.Pages[1].Accept(absorber);

                //            // View text and placement info of first text occurrence
                //            TextFragment firstOccurrence = absorber.TextFragments[1];
                //            // document.Add(new Paragraph(Convert.ToDouble(firstOccurrence.Position.XIndent).ToString()));
                //            float xx = (float)firstOccurrence.Position.XIndent + 20;
                //            float yy = (float)firstOccurrence.Position.YIndent;


                //            string Bitsignature = null;
                //            Bitsignature = PayMtd.BitSign;
                //            Byte[] SignatureBit = null;
                //            iTextSharp.text.Image signatory = null;

                //            Bitsignature = Bitsignature.Replace(' ', '+');
                //            SignatureBit = Convert.FromBase64String(Bitsignature);
                //            signatory = iTextSharp.text.Image.GetInstance(SignatureBit);
                //            signatory.SetAbsolutePosition(xx, yy);

                //            signatory.ScaleToFit(40, 40);

                //            document.Add(signatory);
                //        }

                //        if (currentPageText.Contains("[HR Signature]") || currentPageText.Contains("[HR\nSignature]"))
                //        {
                //            TextFragmentAbsorber absorber = new TextFragmentAbsorber("[HR Signature]");


                //            // Accept the absorber for first page
                //            doc.Pages[1].Accept(absorber);

                //            // View text and placement info of first text occurrence
                //            TextFragment firstOccurrence = absorber.TextFragments[1];
                //            // document.Add(new Paragraph(Convert.ToDouble(firstOccurrence.Position.XIndent).ToString()));
                //            float xx = (float)firstOccurrence.Position.XIndent + 20;
                //            float yy = (float)firstOccurrence.Position.YIndent;


                //            string Bitsignature = null;
                //            Bitsignature = PayMtd.BitSign;
                //            Byte[] SignatureBit = null;
                //            iTextSharp.text.Image signatory = null;

                //            Bitsignature = Bitsignature.Replace(' ', '+');
                //            SignatureBit = Convert.FromBase64String(Bitsignature);
                //            signatory = iTextSharp.text.Image.GetInstance(SignatureBit);
                //            signatory.SetAbsolutePosition(xx, yy);

                //            signatory.ScaleToFit(40, 40);

                //            document.Add(signatory);
                //        }

                //    }
                //}

                //PayMtd.GetSignatureSignPicturePres(empid);
                PayMtd.GetSignatureALL("Signature 1");
                PayMtd.GetSignatureALL("Signature 2");
                PayMtd.GetSignatureALL("Signature 3");
                if (PayMtd.BitSign != null)
                {


                    List<Phrase> PhraseList = new List<Phrase>();
                    for (int page = 1; page <= reader1.NumberOfPages; page++)
                    {
                        ITextExtractionStrategy strategy = new SimpleTextExtractionStrategy();
                        string currentPageText = PdfTextExtractor.GetTextFromPage(reader1, page, strategy);
                        if (currentPageText.Contains("[Employee Signature]") || currentPageText.Contains("[Employee\nSignature]"))
                        {
                            TextFragmentAbsorber absorber = new TextFragmentAbsorber("[Employee Signature]");


                            // Accept the absorber for first page
                            doc.Pages[1].Accept(absorber);

                            // View text and placement info of first text occurrence
                            TextFragment firstOccurrence = absorber.TextFragments[1];
                            // document.Add(new Paragraph(Convert.ToDouble(firstOccurrence.Position.XIndent).ToString()));
                            float xx = (float)firstOccurrence.Position.XIndent + 20;
                            float yy = (float)firstOccurrence.Position.YIndent;


                            string Bitsignature = null;
                            Bitsignature = PayMtd.BitSign;
                            Byte[] SignatureBit = null;
                            iTextSharp.text.Image signatory = null;

                            Bitsignature = Bitsignature.Replace(' ', '+');
                            SignatureBit = Convert.FromBase64String(Bitsignature);
                            signatory = iTextSharp.text.Image.GetInstance(SignatureBit);
                            signatory.SetAbsolutePosition(xx, yy);
                            signatory.ScaleToFit(50, 50);

                            document.Add(signatory);
                        }

                    }
                }

                if (PayMtd.sig1 != null)
                {


                    List<Phrase> PhraseList = new List<Phrase>();
                    for (int page = 1; page <= reader1.NumberOfPages; page++)
                    {
                        ITextExtractionStrategy strategy = new SimpleTextExtractionStrategy();
                        string currentPageText = PdfTextExtractor.GetTextFromPage(reader1, page, strategy);
                        if (currentPageText.Contains("[Signature 1]") || currentPageText.Contains("[Signature\n1]"))
                        {
                            TextFragmentAbsorber absorber = new TextFragmentAbsorber("[Signature 1]");


                            // Accept the absorber for first page
                            doc.Pages[1].Accept(absorber);

                            // View text and placement info of first text occurrence
                            TextFragment firstOccurrence = absorber.TextFragments[1];
                            // document.Add(new Paragraph(Convert.ToDouble(firstOccurrence.Position.XIndent).ToString()));
                            float xx = (float)firstOccurrence.Position.XIndent + 20;
                            float yy = (float)firstOccurrence.Position.YIndent;


                            string Bitsignature = null;
                            Bitsignature = PayMtd.sig1;
                            Byte[] SignatureBit = null;
                            iTextSharp.text.Image signatory = null;

                            Bitsignature = Bitsignature.Replace(' ', '+');
                            SignatureBit = Convert.FromBase64String(Bitsignature);
                            signatory = iTextSharp.text.Image.GetInstance(SignatureBit);
                            signatory.SetAbsolutePosition(xx, yy);

                            signatory.ScaleToFit(50, 50);

                            document.Add(signatory);
                        }


                    }
                }

                if (PayMtd.sig2 != null)
                {


                    List<Phrase> PhraseList = new List<Phrase>();
                    for (int page = 1; page <= reader1.NumberOfPages; page++)
                    {
                        ITextExtractionStrategy strategy = new SimpleTextExtractionStrategy();
                        string currentPageText = PdfTextExtractor.GetTextFromPage(reader1, page, strategy);
                        if (currentPageText.Contains("[Signature 2]") || currentPageText.Contains("[Signature\n2]"))
                        {
                            TextFragmentAbsorber absorber = new TextFragmentAbsorber("[Signature 2]");


                            // Accept the absorber for first page
                            doc.Pages[1].Accept(absorber);

                            // View text and placement info of first text occurrence
                            TextFragment firstOccurrence = absorber.TextFragments[1];
                            // document.Add(new Paragraph(Convert.ToDouble(firstOccurrence.Position.XIndent).ToString()));
                            float xx = (float)firstOccurrence.Position.XIndent + 20;
                            float yy = (float)firstOccurrence.Position.YIndent;


                            string Bitsignature = null;
                            Bitsignature = PayMtd.sig2;
                            Byte[] SignatureBit = null;
                            iTextSharp.text.Image signatory = null;

                            Bitsignature = Bitsignature.Replace(' ', '+');
                            SignatureBit = Convert.FromBase64String(Bitsignature);
                            signatory = iTextSharp.text.Image.GetInstance(SignatureBit);
                            signatory.SetAbsolutePosition(xx, yy);

                            signatory.ScaleToFit(50, 50);

                            document.Add(signatory);
                        }


                    }
                }

                if (PayMtd.sig3 != null)
                {


                    List<Phrase> PhraseList = new List<Phrase>();
                    for (int page = 1; page <= reader1.NumberOfPages; page++)
                    {
                        ITextExtractionStrategy strategy = new SimpleTextExtractionStrategy();
                        string currentPageText = PdfTextExtractor.GetTextFromPage(reader1, page, strategy);
                        if (currentPageText.Contains("[Signature 3]") || currentPageText.Contains("[Signature\n3]"))
                        {
                            TextFragmentAbsorber absorber = new TextFragmentAbsorber("[Signature 3]");


                            // Accept the absorber for first page
                            doc.Pages[1].Accept(absorber);

                            // View text and placement info of first text occurrence
                            TextFragment firstOccurrence = absorber.TextFragments[1];
                            // document.Add(new Paragraph(Convert.ToDouble(firstOccurrence.Position.XIndent).ToString()));
                            float xx = (float)firstOccurrence.Position.XIndent + 20;
                            float yy = (float)firstOccurrence.Position.YIndent;


                            string Bitsignature = null;
                            Bitsignature = PayMtd.sig3;
                            Byte[] SignatureBit = null;
                            iTextSharp.text.Image signatory = null;

                            Bitsignature = Bitsignature.Replace(' ', '+');
                            SignatureBit = Convert.FromBase64String(Bitsignature);
                            signatory = iTextSharp.text.Image.GetInstance(SignatureBit);
                            signatory.SetAbsolutePosition(xx, yy);

                            signatory.ScaleToFit(50, 50);

                            document.Add(signatory);
                        }


                    }
                }






                document.Close();
                writer.Close();
                // closing the stamper
                uProfile.Status = "Success";

            }
            catch (Exception ex)
            {
                uProfile.Status = ex.ToString();
                //this.Page.ClientScript.RegisterClientScriptBlock(this.GetType(),
                //Guid.NewGuid().ToString("N"), "self.parent.location='login.aspx';", true);
            }

            return uProfile.Status;
        }

      

        [HttpPost]
        [Route("SelectCustomFormsv4")]
        public string CustomFormsSelectv4(SelCustomFormsv2 uProfile)
        {

            var settings = ConfigurationManager.ConnectionStrings["connectionstring"];
            var fi = typeof(ConfigurationElement).GetField(
                          "_bReadOnly",
                          BindingFlags.Instance | BindingFlags.NonPublic);
            fi.SetValue(settings, false);
            // settings.ConnectionString = "Data Source=tcp:ilmuat.c3srcolvclfy.ap-southeast-1.rds.amazonaws.com,1400;Database=ILM_Main;User Id=devuatuser;Password=ILMV3ry53cur3dMGTpa55w0rd2020;";
            settings.ConnectionString = "Data Source=livedb-ilm.c3srcolvclfy.ap-southeast-1.rds.amazonaws.com,1412;Database=ILM_Main;User Id=ILM_LiveDBUser;Password=ILMV3ry53cur3dMGTpa55w0rd2020;";
            //settings.ConnectionString = "Data Source=192.168.2.4,1444;Database=ILM_Local;User Id=DevUser;Password=securedmgt@1230;";



            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@ClientName", mytype = SqlDbType.NVarChar, Value = uProfile.CN });
            // DataTable EmployeeDt = new DataTable();
            DataRow dr;
            dr = Connection.GetSingleRow("sp_CheckClientDB");

            GetDatabase dab = new GetDatabase
            {
                ClientName = dr["DB_Settings"].ToString()
            };

            if (dab.ClientName == "ILM_Live")
            // if (dab.ClientName == "ILM_DevSvr")
            {
                // settings.ConnectionString = "Data Source=tcp:ilm-dev-uat.c3srcolvclfy.ap-southeast-1.rds.amazonaws.com,1400;Database=ILM_DevSvr;User Id=devuatuser;Password=ILMV3ry53cur3dMGTpa55w0rd2020;";
                settings.ConnectionString = "Data Source=livedb-ilm.c3srcolvclfy.ap-southeast-1.rds.amazonaws.com,1412;Database=ILM_Live;User Id=ILM_LiveDBUser;Password=ILMV3ry53cur3dMGTpa55w0rd2020;";

            }
            //else if (dab.ClientName == "ILM_DevSvr")
            //{
            //    settings.ConnectionString = "Data Source=tcp:ilm-dev-uat.c3srcolvclfy.ap-southeast-1.rds.amazonaws.com,1400;Database=ILM_DevSvr;User Id=devuatuser;Password=ILMV3ry53cur3dMGTpa55w0rd2020;";
            //    // settings.ConnectionString = "Data Source=livedb-ilm.c3srcolvclfy.ap-southeast-1.rds.amazonaws.com,1412;Database=ILM_DemoA;User Id=ILM_LiveDBUser;Password=ILMV3ry53cur3dMGTpa55w0rd2020;";

            //}
            else if (dab.ClientName == "StarDB")
            {
                settings.ConnectionString = "Data Source=livedb-ilm.c3srcolvclfy.ap-southeast-1.rds.amazonaws.com,1412;Database=StarDB;User Id=ILM_LiveDBUser;Password=ILMV3ry53cur3dMGTpa55w0rd2020;";
                //settings.ConnectionString = "Data Source=tcp:ilmuat.c3srcolvclfy.ap-southeast-1.rds.amazonaws.com,1433;Database=StarDB;User Id=ILMUATSA;Password=ILMV3ry53cur3dMGTpa55w0rd2020;";

            }
            else if (dab.ClientName == "PetBoweDB")
            {
                settings.ConnectionString = "Data Source=tcp:ilmuat.c3srcolvclfy.ap-southeast-1.rds.amazonaws.com,1433;Database=PetBoweDB;User Id=ILMUATSA;Password=ILMV3ry53cur3dMGTpa55w0rd2020;";

            }
            else if (dab.ClientName == "UnionBankDB")
            {
                settings.ConnectionString = "Data Source=tcp:ilmuat.c3srcolvclfy.ap-southeast-1.rds.amazonaws.com,1433;Database=UnionBankDB;User Id=ILMUATSA;Password=ILMV3ry53cur3dMGTpa55w0rd2020;";

            }
            else if (dab.ClientName == "ILM_UAT")
            {
                settings.ConnectionString = "Data Source=tcp:ilmuat.c3srcolvclfy.ap-southeast-1.rds.amazonaws.com,1433;Database=ILM_UAT;User Id=ILMUATSA;Password=ILMV3ry53cur3dMGTpa55w0rd2020;";

            }
            //else if (dab.ClientName == "ILM_Local")
            //{
            //    settings.ConnectionString = "Data Source=192.168.2.4,1444;Database=ILM_Local;User Id=DevUser;Password=securedmgt@1230;";
            //}
            else
            {
                settings.ConnectionString = "Data Source=livedb-ilm.c3srcolvclfy.ap-southeast-1.rds.amazonaws.com,1412;Database=" + dab.ClientName + ";User Id=ILM_LiveDBUser;Password=ILMV3ry53cur3dMGTpa55w0rd2020;";

            }



            // GetPayslipResult.paysliplink = "http://illimitadodevs.azurewebsites.net/pdf/" + row["PDF"].ToString() + "-";
            Connection con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = uProfile.EmpID });
            DataTable getCOE = con.GetDataTable("sp_get_dataforCOE");

            con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.NVarChar, Value = uProfile.EmpID });
            DataTable customCOE = con.GetDataTable("sp_CustomCOE");

            var datenow = DateTime.Now.ToString("yyyy-MM-dd");


            try
            {
                if (uProfile.Forms.ToLower() == "coe separation application")
                {
                    string path = HttpContext.Current.Server.MapPath("pdf");
                    string path2 = HttpContext.Current.Server.MapPath("pdf");
                    var pdfReader1 = new PdfReader(path + "/COE Separation Application.pdf");

                    AcroFields af = pdfReader1.AcroFields;
                    string sam = "";
                    foreach (var field in af.Fields)
                    {
                        sam = sam + field.Key + "-"; // names of textfields
                    }
                    string formFile = "/COE Separation Application.pdf";
                    // Output file path
                    string newFile = path2 + "/COE Separation Application-" + uProfile.CN + "-" + uProfile.EmpID + ".pdf";

                    PdfReader reader;
                    reader = new PdfReader(path2 + "/COE Separation Application.pdf");

                    PdfStamper stamper = new PdfStamper(reader, new FileStream(newFile, FileMode.Create));
                    AcroFields fields = stamper.AcroFields;
                    //make singit here

                    string EmpN = "", MSal = "", JObDesc = "", gender = "", AssD = "",
                        position = "", datejoined = "", ADDS = "",
                        Effectivedate = "", day = "", month = "", year = ""; var DateJ = "" ;

                    //if (getCOE.Rows.Count > 1)
                    //{
                        EmpN = getCOE.Rows[0][0].ToString() == "" ? "N/A" : getCOE.Rows[0][0].ToString();
                        gender = getCOE.Rows[0][3].ToString() == "" ? "N/A" : getCOE.Rows[0][3].ToString();
                        AssD = getCOE.Rows[0][3].ToString() == "M" ? "He" : "She";
                        ADDS = getCOE.Rows[0][3].ToString() == "M" ? "him" : "her";
                        position = getCOE.Rows[0][2].ToString() == "" ? "N/A" : getCOE.Rows[0][2].ToString();
                        datejoined = getCOE.Rows[0][1].ToString() == "" ? "N/A" : getCOE.Rows[0][1].ToString();
                        Effectivedate = getCOE.Rows[0][32].ToString() == "" ? "N/A" : getCOE.Rows[0][32].ToString();
                        DateTime today = DateTime.Today;
                        day = today.Day.ToString() == "1" || today.Day.ToString() == "21" ||  today.Day.ToString() == "31" ? today.Day.ToString() + "st" :
                              today.Day.ToString() == "2" || today.Day.ToString() == "22" || today.Day.ToString() == "32" ? today.Day.ToString() + "nd" :
                              today.Day.ToString() == "3" || today.Day.ToString() == "23" || today.Day.ToString() == "33" ? today.Day.ToString() + "rd" :
                              today.Day.ToString() + "th";
                        month = Convert.ToDateTime(today).ToString("MMMM");
                        year = today.Year.ToString();
                    fields.SetField("MsgBody", "This is to certify that " + EmpN + "  was an employee of Ramcar Group of Companies. " + AssD + " was holding the position of " + position + " from " + datejoined + " until " + ADDS + " separation effective " + Effectivedate + "."
                            + Environment.NewLine
                            + Environment.NewLine
                            + Environment.NewLine
                            + "This certifies that " + EmpN + "  has been cleared from any liability to the company arising from, relating to or in connection with her employment with the company.This certification is being issued upon " + ADDS + " request for whatever legal purpose this may serve."
                            + Environment.NewLine
                            + Environment.NewLine
                            + Environment.NewLine
                            + "Issued this " + day + " day of " + month + " "  + year + " in Makati City, Philippines.");
                   // }


                    //if (getCOE.Rows.Count > 0)
                    //{
                    //    //fields.SetField("EmployeeLevel", customCOE.Rows[0][1].ToString() == "" ? "N/A" : customCOE.Rows[0][1].ToString());
                    //    fields.SetField("untitled1", datenow);
                    //    fields.SetField("untitled2", "");
                    //    fields.SetField("JobTxt", getCOE.Rows[0][4].ToString() == "" ? "N/A" : getCOE.Rows[0][4].ToString());
                    //    fields.SetField("untitled3", getCOE.Rows[0][0].ToString() == "" ? "N/A" : getCOE.Rows[0][0].ToString());
                    //    fields.SetField("DateJoinedTxt", getCOE.Rows[0][1].ToString() == "" ? "N/A" : getCOE.Rows[0][1].ToString());
                    //    fields.SetField("EmpNameTXt", getCOE.Rows[0][0].ToString() == "" ? "N/A" : getCOE.Rows[0][0].ToString());
                    //    fields.SetField("MonthTxt", getCOE.Rows[0]["monthly"].ToString() == "" ? "N/A" : getCOE.Rows[0]["monthly"].ToString());
                    //}



                    ////generated signature postion 1  
                    var pdfpage = stamper.GetOverContent(1);
                    string signature1 = "";

                    Connection Con = new Connection();
                    Con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = "i143" });
                    Con.myparameters.Add(new myParameters { ParameterName = "@TYPES", mytype = SqlDbType.NVarChar, Value = "sign_0" });
                    signature1 = Con.ExecuteScalar("sp_SearchBased64");


                    if (signature1 != "")
                    {
                        Byte[] signa1 = null;
                        iTextSharp.text.Image sign1 = null;
                        if (string.IsNullOrWhiteSpace(signature1) == false)
                        {
                            if(signature1.Contains("ws.durusthr.com"))
                            {
                                StringBuilder _sb = new StringBuilder();

                                Byte[] _byte = GetImage(signature1);

                                _sb.Append(Convert.ToBase64String(_byte, 0, _byte.Length));

                                string ssign = _sb.ToString().Replace(' ', '+');
                                signa1 = Convert.FromBase64String(ssign);
                            }
                            else
                            {
                                signature1 = signature1.Replace(' ', '+');
                                signa1 = Convert.FromBase64String(signature1);
                            }
                            
                            
                            sign1 = iTextSharp.text.Image.GetInstance(signa1);
                            sign1.SetAbsolutePosition(80, 290);
                            sign1.ScaleToFit(100, 50);
                            pdfpage.AddImage(sign1);
                        }
                    }


                    stamper.FormFlattening = true;
                    stamper.Close();
                    uProfile.Status = "Success";

                }
                else if (uProfile.Forms.ToLower() == "coe visa application")
                {
                    string path = HttpContext.Current.Server.MapPath("pdf");
                    string path2 = HttpContext.Current.Server.MapPath("pdf");
                    var pdfReader1 = new PdfReader(path + "/COE Visa Application.pdf");

                    AcroFields af = pdfReader1.AcroFields;
                    string sam = "";
                    foreach (var field in af.Fields)
                    {
                        sam = sam + field.Key + "-"; // names of textfields
                    }
                    string formFile = "/COE Visa Application.pdf";
                    // Output file path
                    string newFile = path2 + "/COE Visa Application-" + uProfile.CN + "-" + uProfile.EmpID + ".pdf";

                    PdfReader reader;
                    reader = new PdfReader(path2 + "/COE Visa Application.pdf");

                    PdfStamper stamper = new PdfStamper(reader, new FileStream(newFile, FileMode.Create));
                    AcroFields fields = stamper.AcroFields;
                    //make singit here

                    string EmpN = "", MSal = "", JObDesc = "", gender = "", AssD = "",
                    position = "", datejoined = "", ADDS = "",
                    Effectivedate = "", day = "", month = "", year = "", grosspay = ""; var DateJ = "";

                    string dnow = DateTime.Now.ToString("MMMM dd, yyyy", CultureInfo.InvariantCulture);
                    fields.SetField("DateTimeNowTxt", dnow);


                    //if (getCOE.Rows.Count > 1)
                    //{


                        //if (getCOE.Rows.Count > 1)
                        //{
                        EmpN = getCOE.Rows[0][0].ToString() == "" ? "N/A" : getCOE.Rows[0][0].ToString();
                        gender = getCOE.Rows[0][3].ToString() == "M" ? "Mr. " : "Ms. "; ;
                        AssD = getCOE.Rows[0][3].ToString() == "M" ? "He" : "She";
                        ADDS = getCOE.Rows[0][3].ToString() == "M" ? "his" : "her";
                        position = getCOE.Rows[0][2].ToString() == "" ? "N/A" : getCOE.Rows[0][2].ToString();
                        datejoined = getCOE.Rows[0][1].ToString() == "" ? "N/A" : getCOE.Rows[0][1].ToString();
                        Effectivedate = getCOE.Rows[0][32].ToString() == "" ? "N/A" : getCOE.Rows[0][32].ToString();
                    grosspay = String.Format("{0:n}", Convert.ToDouble(getCOE.Rows[0]["GrossPay"].ToString()) * 14); //Convert.ToDouble(getCOE.Rows[0]["GrossPay"].ToString()).ToString("#,##0.00");
                        DateTime today = DateTime.Today;
                        day = today.Day.ToString() == "1" || today.Day.ToString() == "21" || today.Day.ToString() == "31" ? today.Day.ToString() + "st" :
                              today.Day.ToString() == "2" || today.Day.ToString() == "22" || today.Day.ToString() == "32" ? today.Day.ToString() + "nd" :
                              today.Day.ToString() == "3" || today.Day.ToString() == "23" || today.Day.ToString() == "33" ? today.Day.ToString() + "rd" :
                              today.Day.ToString() + "th";
                        month = Convert.ToDateTime(today).ToString("MMMM");
                        year = today.Year.ToString();

                        fields.SetField("BodyTxt", "This is to certify that " + gender + EmpN + " is an employee of Generali Life Assurance Philippines, Inc. since " + datejoined + ". " + AssD + " is holding the position of " + position + " and has a gross annual salary of " + grosspay + "."
                           + Environment.NewLine
                            + Environment.NewLine
                           + "This certification is being issued upon the request of " + gender + EmpN + " for " + ADDS + " Visa application."
                           + Environment.NewLine
                            + Environment.NewLine
                           + "For any clarifications with regard to " + ADDS + " employment, you may call our Human Capital Department at 02 580 - 6685. ");


                        //EmpN = getCOE.Rows[0][0].ToString() == "" ? "N/A" : getCOE.Rows[0][0].ToString();

                        //JObDesc = getCOE.Rows[0][4].ToString() == "" ? "N/A" : getCOE.Rows[0][4].ToString();


                        //if (getCOE.Rows[0]["monthly"].ToString() == "" || getCOE.Rows[0]["monthly"].ToString() == null)
                        //{

                        //}
                        //else
                        //{
                        //    MSal = Convert.ToDouble(getCOE.Rows[0]["monthly"].ToString()).ToString("#,##0.00");
                        //}
                        //if (getCOE.Rows[0][1].ToString() == "" || getCOE.Rows[0][1].ToString() == null)
                        //{
                        //    DateJ = "";
                        //}
                        //else
                        //{
                        //    DateJ = Convert.ToDateTime(getCOE.Rows[0][1].ToString()).ToString("MMMM dd, yyyy");

                        //}



                        //fields.SetField("DateNowTxt", datenow);
                        //fields.SetField("EmpNameTxt1", EmpN);
                        //fields.SetField("JobTxt", JObDesc);
                        //fields.SetField("JoinedDateTxt", DateJ);
                        //fields.SetField("MonthlySalaryTxt", MSal);
                        //fields.SetField("EmpNameTxt2", EmpN);
                   // }


                    //if (getCOE.Rows.Count > 0)
                    //{
                    //    //fields.SetField("EmployeeLevel", customCOE.Rows[0][1].ToString() == "" ? "N/A" : customCOE.Rows[0][1].ToString());
                    //    fields.SetField("untitled1", datenow);
                    //    fields.SetField("untitled2", "");
                    //    fields.SetField("JobTxt", getCOE.Rows[0][4].ToString() == "" ? "N/A" : getCOE.Rows[0][4].ToString());
                    //    fields.SetField("untitled3", getCOE.Rows[0][0].ToString() == "" ? "N/A" : getCOE.Rows[0][0].ToString());
                    //    fields.SetField("DateJoinedTxt", getCOE.Rows[0][1].ToString() == "" ? "N/A" : getCOE.Rows[0][1].ToString());
                    //    fields.SetField("EmpNameTXt", getCOE.Rows[0][0].ToString() == "" ? "N/A" : getCOE.Rows[0][0].ToString());
                    //    fields.SetField("MonthTxt", getCOE.Rows[0]["monthly"].ToString() == "" ? "N/A" : getCOE.Rows[0]["monthly"].ToString());
                    //}



                    ////generated signature postion 1  
                    var pdfpage = stamper.GetOverContent(1);
                    string signature1 = "";

                    Connection Con = new Connection();
                    Con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = "170017" });
                    Con.myparameters.Add(new myParameters { ParameterName = "@TYPES", mytype = SqlDbType.NVarChar, Value = "sign_0" });
                    signature1 = Con.ExecuteScalar("sp_SearchBased64");


                    //if (signature1 != "")
                    //{
                    //    Byte[] signa1 = null;
                    //    iTextSharp.text.Image sign1 = null;
                    //    if (string.IsNullOrWhiteSpace(signature1) == false)
                    //    {
                    //        signature1 = signature1.Replace(' ', '+');
                    //        signa1 = Convert.FromBase64String(signature1);
                    //        sign1 = iTextSharp.text.Image.GetInstance(signa1);
                    //        sign1.SetAbsolutePosition(65, 360);
                    //        sign1.ScaleToFit(100, 50);
                    //        pdfpage.AddImage(sign1);
                    //    }
                    //}


                    stamper.FormFlattening = true;
                    stamper.Close();
                    uProfile.Status = "Success";

                }
                else if (uProfile.Forms.ToLower() == "coe legal purpose")
                {
                    string path = HttpContext.Current.Server.MapPath("pdf");
                    string path2 = HttpContext.Current.Server.MapPath("pdf");
                    var pdfReader1 = new PdfReader(path + "/COE Legal Purpose.pdf");

                    AcroFields af = pdfReader1.AcroFields;
                    string sam = "";
                    foreach (var field in af.Fields)
                    {
                        sam = sam + field.Key + "-"; // names of textfields
                    }
                    string formFile = "/COE Legal Purpose.pdf";
                    // Output file path
                    string newFile = path2 + "/COE Legal Purpose-" + uProfile.CN + "-" + uProfile.EmpID + ".pdf";

                    PdfReader reader;
                    reader = new PdfReader(path2 + "/COE Legal Purpose.pdf");

                    PdfStamper stamper = new PdfStamper(reader, new FileStream(newFile, FileMode.Create));
                    AcroFields fields = stamper.AcroFields;
                    //make singit here

                    string EmpN = "", MSal = "", JObDesc = "", gender = "", AssD = "",
                    position = "", datejoined = "", ADDS = "",
                    Effectivedate = "", day = "", month = "", year = "", grosspay = ""; var DateJ = "";

                    string dnow = DateTime.Now.ToString("MMMM dd, yyyy", CultureInfo.InvariantCulture);
                    fields.SetField("DateTimeNowTxt", dnow);


                    //if (getCOE.Rows.Count > 1)
                    //{


                    //if (getCOE.Rows.Count > 1)
                    //{
                    EmpN = getCOE.Rows[0][0].ToString() == "" ? "N/A" : getCOE.Rows[0][0].ToString();
                    gender = getCOE.Rows[0][3].ToString() == "M" ? "Mr. " : "Ms. "; ;
                    AssD = getCOE.Rows[0][3].ToString() == "M" ? "He" : "She";
                    ADDS = getCOE.Rows[0][3].ToString() == "M" ? "his" : "her";
                    position = getCOE.Rows[0][2].ToString() == "" ? "N/A" : getCOE.Rows[0][2].ToString();
                    datejoined = getCOE.Rows[0][1].ToString() == "" ? "N/A" : getCOE.Rows[0][1].ToString();
                    Effectivedate = getCOE.Rows[0][32].ToString() == "" ? "N/A" : getCOE.Rows[0][32].ToString();
                    grosspay = String.Format("{0:n}", Convert.ToDouble(getCOE.Rows[0]["GrossPay"].ToString()) * 14);
                    DateTime today = DateTime.Today;
                    day = today.Day.ToString() == "1" || today.Day.ToString() == "21" || today.Day.ToString() == "31" ? today.Day.ToString() + "st" :
                          today.Day.ToString() == "2" || today.Day.ToString() == "22" || today.Day.ToString() == "32" ? today.Day.ToString() + "nd" :
                          today.Day.ToString() == "3" || today.Day.ToString() == "23" || today.Day.ToString() == "33" ? today.Day.ToString() + "rd" :
                          today.Day.ToString() + "th";
                    month = Convert.ToDateTime(today).ToString("MMMM");
                    year = today.Year.ToString();

                    fields.SetField("BodyTxt", "This is to certify that " + gender + EmpN + " is an employee of Generali Life Assurance Philippines, Inc. since " + datejoined + ". " + AssD + " is holding the position of " + position + " and has an annual compensation of " + grosspay + "."
                       + Environment.NewLine
                        + Environment.NewLine
                       + "This certification is being issued upon the request of " + gender + EmpN + "  for whatever legal purpose it may serve."
                       + Environment.NewLine
                        + Environment.NewLine
                       + "For any clarifications with regard to " + ADDS + " employment, you may call our Human Capital Department at 02 580 - 6685. ");

                    var pdfpage = stamper.GetOverContent(1);
                    string signature1 = "";

                    Connection Con = new Connection();
                    Con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = "170017" });
                    Con.myparameters.Add(new myParameters { ParameterName = "@TYPES", mytype = SqlDbType.NVarChar, Value = "sign_0" });
                    signature1 = Con.ExecuteScalar("sp_SearchBased64");

                    stamper.FormFlattening = true;
                    stamper.Close();
                    uProfile.Status = "Success";

                }
                else if (uProfile.Forms.ToLower() == "coe loan purpose")
                {
                    string path = HttpContext.Current.Server.MapPath("pdf");
                    string path2 = HttpContext.Current.Server.MapPath("pdf");
                    var pdfReader1 = new PdfReader(path + "/COE Loan Purpose.pdf");

                    AcroFields af = pdfReader1.AcroFields;
                    string sam = "";
                    foreach (var field in af.Fields)
                    {
                        sam = sam + field.Key + "-"; // names of textfields
                    }
                    string formFile = "/COE Loan Purpose.pdf";
                    // Output file path
                    string newFile = path2 + "/COE Loan Purpose-" + uProfile.CN + "-" + uProfile.EmpID + ".pdf";

                    PdfReader reader;
                    reader = new PdfReader(path2 + "/COE Loan Purpose.pdf");

                    PdfStamper stamper = new PdfStamper(reader, new FileStream(newFile, FileMode.Create));
                    AcroFields fields = stamper.AcroFields;
                    //make singit here

                    string EmpN = "", MSal = "", JObDesc = "", gender = "", AssD = "",
                    position = "", datejoined = "", ADDS = "",
                    Effectivedate = "", day = "", month = "", year = "", grosspay = ""; var DateJ = "";

                    string dnow = DateTime.Now.ToString("MMMM dd, yyyy", CultureInfo.InvariantCulture);
                    fields.SetField("DateTimeNowTxt", dnow);


                    //if (getCOE.Rows.Count > 1)
                    //{


                    //if (getCOE.Rows.Count > 1)
                    //{
                    EmpN = getCOE.Rows[0][0].ToString() == "" ? "N/A" : getCOE.Rows[0][0].ToString();
                    gender = getCOE.Rows[0][3].ToString() == "M" ? "Mr. " : "Ms. "; ;
                    AssD = getCOE.Rows[0][3].ToString() == "M" ? "He" : "She";
                    ADDS = getCOE.Rows[0][3].ToString() == "M" ? "his" : "her";
                    position = getCOE.Rows[0][2].ToString() == "" ? "N/A" : getCOE.Rows[0][2].ToString();
                    datejoined = getCOE.Rows[0][1].ToString() == "" ? "N/A" : getCOE.Rows[0][1].ToString();
                    Effectivedate = getCOE.Rows[0][32].ToString() == "" ? "N/A" : getCOE.Rows[0][32].ToString();
                    grosspay = String.Format("{0:n}", Convert.ToDouble(getCOE.Rows[0]["GrossPay"].ToString()) * 14);
                    DateTime today = DateTime.Today;
                    day = today.Day.ToString() == "1" || today.Day.ToString() == "21" || today.Day.ToString() == "31" ? today.Day.ToString() + "st" :
                          today.Day.ToString() == "2" || today.Day.ToString() == "22" || today.Day.ToString() == "32" ? today.Day.ToString() + "nd" :
                          today.Day.ToString() == "3" || today.Day.ToString() == "23" || today.Day.ToString() == "33" ? today.Day.ToString() + "rd" :
                          today.Day.ToString() + "th";
                    month = Convert.ToDateTime(today).ToString("MMMM");
                    year = today.Year.ToString();

                    fields.SetField("BodyTxt", "This is to certify that " + gender + EmpN + " is an employee of Generali Life Assurance Philippines, Inc. since " + datejoined + ". " + AssD + " works in the Human Capital as " + position + " and is receiving an annual basic salary of " + grosspay + "."
                       + Environment.NewLine
                        + Environment.NewLine
                       + "This certification is being issued upon the request of " + gender + EmpN + "  for " + ADDS + " loan application."
                       + Environment.NewLine
                        + Environment.NewLine
                       + "For any clarifications with regard to " + ADDS + " employment, you may call our Human Capital Department at 02 580 - 6685. ");

                    var pdfpage = stamper.GetOverContent(1);
                    string signature1 = "";

                    Connection Con = new Connection();
                    Con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = "170017" });
                    Con.myparameters.Add(new myParameters { ParameterName = "@TYPES", mytype = SqlDbType.NVarChar, Value = "sign_0" });
                    signature1 = Con.ExecuteScalar("sp_SearchBased64");

                    stamper.FormFlattening = true;
                    stamper.Close();
                    uProfile.Status = "Success";

                }
                else if (uProfile.Forms.ToLower() == "coe legal purpose without salary")
                {
                    string path = HttpContext.Current.Server.MapPath("pdf");
                    string path2 = HttpContext.Current.Server.MapPath("pdf");
                    var pdfReader1 = new PdfReader(path + "/COE Legal Purpose without Salary.pdf");

                    AcroFields af = pdfReader1.AcroFields;
                    string sam = "";
                    foreach (var field in af.Fields)
                    {
                        sam = sam + field.Key + "-"; // names of textfields
                    }
                    string formFile = "/COE Legal Purpose without Salary.pdf";
                    // Output file path
                    string newFile = path2 + "/COE Legal Purpose without Salary-" + uProfile.CN + "-" + uProfile.EmpID + ".pdf";

                    PdfReader reader;
                    reader = new PdfReader(path2 + "/COE Legal Purpose without Salary.pdf");

                    PdfStamper stamper = new PdfStamper(reader, new FileStream(newFile, FileMode.Create));
                    AcroFields fields = stamper.AcroFields;
                    //make singit here

                    string EmpN = "", MSal = "", JObDesc = "", gender = "", AssD = "",
                    position = "", datejoined = "", ADDS = "",
                    Effectivedate = "", day = "", month = "", year = "", grosspay = ""; var DateJ = "";

                    string dnow = DateTime.Now.ToString("MMMM dd, yyyy", CultureInfo.InvariantCulture);
                    fields.SetField("DateTimeNowTxt", dnow);


                    //if (getCOE.Rows.Count > 1)
                    //{


                    //if (getCOE.Rows.Count > 1)
                    //{
                    EmpN = getCOE.Rows[0][0].ToString() == "" ? "N/A" : getCOE.Rows[0][0].ToString();
                    gender = getCOE.Rows[0][3].ToString() == "M" ? "Mr. " : "Ms. "; ;
                    AssD = getCOE.Rows[0][3].ToString() == "M" ? "He" : "She";
                    ADDS = getCOE.Rows[0][3].ToString() == "M" ? "his" : "her";
                    position = getCOE.Rows[0][2].ToString() == "" ? "N/A" : getCOE.Rows[0][2].ToString();
                    datejoined = getCOE.Rows[0][1].ToString() == "" ? "N/A" : getCOE.Rows[0][1].ToString();
                    Effectivedate = getCOE.Rows[0][32].ToString() == "" ? "N/A" : getCOE.Rows[0][32].ToString();
                    grosspay = String.Format("{0:n}", Convert.ToDouble(getCOE.Rows[0]["GrossPay"].ToString()) * 14);
                    DateTime today = DateTime.Today;
                    day = today.Day.ToString() == "1" || today.Day.ToString() == "21" || today.Day.ToString() == "31" ? today.Day.ToString() + "st" :
                          today.Day.ToString() == "2" || today.Day.ToString() == "22" || today.Day.ToString() == "32" ? today.Day.ToString() + "nd" :
                          today.Day.ToString() == "3" || today.Day.ToString() == "23" || today.Day.ToString() == "33" ? today.Day.ToString() + "rd" :
                          today.Day.ToString() + "th";
                    month = Convert.ToDateTime(today).ToString("MMMM");
                    year = today.Year.ToString();

                    fields.SetField("BodyTxt", "This is to certify that " + gender + EmpN + " is an employee of Generali Life Assurance Philippines, Inc. since " + datejoined + ". " + AssD + " is holding the position of " + position + "."
                       + Environment.NewLine
                        + Environment.NewLine
                       + "This certification is being issued upon the request of " + gender + EmpN + " for whatever legal purpose it may serve."
                       + Environment.NewLine
                        + Environment.NewLine
                       + "For any clarifications with regard to " + ADDS + " employment, you may call our Human Capital Department at 02 580 - 6685. ");

                    var pdfpage = stamper.GetOverContent(1);
                    string signature1 = "";

                    Connection Con = new Connection();
                    Con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = "170017" });
                    Con.myparameters.Add(new myParameters { ParameterName = "@TYPES", mytype = SqlDbType.NVarChar, Value = "sign_0" });
                    signature1 = Con.ExecuteScalar("sp_SearchBased64");

                    stamper.FormFlattening = true;
                    stamper.Close();
                    uProfile.Status = "Success";

                }
                else if (uProfile.Forms.ToLower() == "coe mobile plan purpose")
                {
                    string path = HttpContext.Current.Server.MapPath("pdf");
                    string path2 = HttpContext.Current.Server.MapPath("pdf");
                    var pdfReader1 = new PdfReader(path + "/COE Mobile Plan Purpose.pdf");

                    AcroFields af = pdfReader1.AcroFields;
                    string sam = "";
                    foreach (var field in af.Fields)
                    {
                        sam = sam + field.Key + "-"; // names of textfields
                    }
                    string formFile = "/COE Mobile Plan Purpose.pdf";
                    // Output file Mobile Plan
                    string newFile = path2 + "/COE Mobile Plan Purpose-" + uProfile.CN + "-" + uProfile.EmpID + ".pdf";

                    PdfReader reader;
                    reader = new PdfReader(path2 + "/COE Mobile Plan Purpose.pdf");

                    PdfStamper stamper = new PdfStamper(reader, new FileStream(newFile, FileMode.Create));
                    AcroFields fields = stamper.AcroFields;
                    //make singit here

                    string EmpN = "", MSal = "", JObDesc = "", gender = "", AssD = "",
                    position = "", datejoined = "", ADDS = "",
                    Effectivedate = "", day = "", month = "", year = "", grosspay = ""; var DateJ = "";

                    string dnow = DateTime.Now.ToString("MMMM dd, yyyy", CultureInfo.InvariantCulture);
                    fields.SetField("DateTimeNowTxt", dnow);


                    //if (getCOE.Rows.Count > 1)
                    //{


                    //if (getCOE.Rows.Count > 1)
                    //{
                    EmpN = getCOE.Rows[0][0].ToString() == "" ? "N/A" : getCOE.Rows[0][0].ToString();
                    gender = getCOE.Rows[0][3].ToString() == "M" ? "Mr. " : "Ms. "; ;
                    AssD = getCOE.Rows[0][3].ToString() == "M" ? "He" : "She";
                    ADDS = getCOE.Rows[0][3].ToString() == "M" ? "his" : "her";
                    position = getCOE.Rows[0][2].ToString() == "" ? "N/A" : getCOE.Rows[0][2].ToString();
                    datejoined = getCOE.Rows[0][1].ToString() == "" ? "N/A" : getCOE.Rows[0][1].ToString();
                    Effectivedate = getCOE.Rows[0][32].ToString() == "" ? "N/A" : getCOE.Rows[0][32].ToString();
                    grosspay = String.Format("{0:n}", Convert.ToDouble(getCOE.Rows[0]["GrossPay"].ToString()) * 14);
                    DateTime today = DateTime.Today;
                    day = today.Day.ToString() == "1" || today.Day.ToString() == "21" || today.Day.ToString() == "31" ? today.Day.ToString() + "st" :
                          today.Day.ToString() == "2" || today.Day.ToString() == "22" || today.Day.ToString() == "32" ? today.Day.ToString() + "nd" :
                          today.Day.ToString() == "3" || today.Day.ToString() == "23" || today.Day.ToString() == "33" ? today.Day.ToString() + "rd" :
                          today.Day.ToString() + "th";
                    month = Convert.ToDateTime(today).ToString("MMMM");
                    year = today.Year.ToString();

                    fields.SetField("BodyTxt", "This is to certify that " + gender + EmpN + " is an employee of Generali Life Assurance Philippines, Inc. since " + datejoined + ". " + AssD + " is currently holding the position of " + position + " and has an annual salary of " + grosspay + "."
                       + Environment.NewLine
                        + Environment.NewLine
                       + "This certification is being issued upon the request of " + gender + EmpN + "  for " + ADDS + " postpaid application."
                       + Environment.NewLine
                        + Environment.NewLine
                       + "For any clarifications with regard to " + ADDS + " employment, you may call our Human Capital Department at 02 580 - 6685. ");

                    var pdfpage = stamper.GetOverContent(1);
                    string signature1 = "";

                    Connection Con = new Connection();
                    Con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = "170017" });
                    Con.myparameters.Add(new myParameters { ParameterName = "@TYPES", mytype = SqlDbType.NVarChar, Value = "sign_0" });
                    signature1 = Con.ExecuteScalar("sp_SearchBased64");

                    stamper.FormFlattening = true;
                    stamper.Close();
                    uProfile.Status = "Success";

                }
                else if (uProfile.Forms.ToLower() == "philhealth certification")
                {

                    string path = HttpContext.Current.Server.MapPath("pdf");
                    string path2 = HttpContext.Current.Server.MapPath("pdf");
                    var pdfReader1 = new PdfReader(path + "/Philhealth Certification.pdf");

                    AcroFields af = pdfReader1.AcroFields;
                    string sam = "";
                    foreach (var field in af.Fields)
                    {
                        sam = sam + field.Key + "-"; // names of textfields
                    }
                    string formFile = "/Philhealth Certification.pdf";
                    // Output file path
                    string newFile = path2 + "/Philhealth Certification-" + uProfile.CN + "-" + uProfile.EmpID + ".pdf";

                    PdfReader reader;
                    reader = new PdfReader(path2 + "/Philhealth Certification.pdf");

                    PdfStamper stamper = new PdfStamper(reader, new FileStream(newFile, FileMode.Create));
                    AcroFields fields = stamper.AcroFields;

                    string Gender = "", AssD = "", ADDS = "", EMPNAME = "", PHILID = "";

                    con = new Connection();
                    con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.VarChar, Value = uProfile.EmpID });
                    DataTable dtD = con.GetDataTable("sp_get_dataforCOE");
                    if (dtD.Rows.Count > 0)
                    {
                        Gender = dtD.Rows[0][3].ToString();
                        EMPNAME = dtD.Rows[0][0].ToString();
                        PHILID = dtD.Rows[0][15].ToString();
                    }




                    switch (Gender)
                    {
                        case "F":
                            Gender = "Ms. ";
                            AssD = "She ";
                            ADDS = "her ";
                            break;
                        case "M":
                            Gender = "Mr. ";
                            AssD = "He ";
                            ADDS = "his ";
                            break;
                        default:
                            Gender = "Mr./Ms. ";
                            AssD = "He/She ";
                            ADDS = "his/her ";
                            break;
                    }
                    var DateNow = DateTime.Now.ToString("yyyy-MMMM-dd");
                    string[] dayarr = DateNow.Replace("-", " ").Split(' ');
                    string month = "", year = "", day = "";
                    month = dayarr[1];
                    year = dayarr[0];
                    day = dayarr[2];


                    //call web method




                    #region Field Property
                    fields.SetFieldProperty("FirstPagraph", "textsize", 11f, null);
                    fields.SetFieldProperty("SecondParagraph", "textsize", 11f, null);
                    fields.SetFieldProperty("ThirdParagraph", "textsize", 11f, null);
                    fields.SetFieldProperty("MonthTxt1", "textsize", 11f, null);
                    fields.SetFieldProperty("MonthTxt2", "textsize", 11f, null);
                    fields.SetFieldProperty("MonthTxt3", "textsize", 11f, null);
                    fields.SetFieldProperty("MonthTxt4", "textsize", 11f, null);
                    fields.SetFieldProperty("MonthTxt5", "textsize", 11f, null);
                    fields.SetFieldProperty("MonthTxt6", "textsize", 11f, null);
                    fields.SetFieldProperty("MonthTxt7", "textsize", 11f, null);
                    fields.SetFieldProperty("MonthTxt8", "textsize", 11f, null);
                    fields.SetFieldProperty("MonthTxt9", "textsize", 11f, null);
                    fields.SetFieldProperty("EmployeeTxt1", "textsize", 11f, null);
                    fields.SetFieldProperty("EmployeeTxt2", "textsize", 11f, null);
                    fields.SetFieldProperty("EmployeeTxt3", "textsize", 11f, null);
                    fields.SetFieldProperty("EmployeeTxt4", "textsize", 11f, null);
                    fields.SetFieldProperty("EmployeeTxt5", "textsize", 11f, null);
                    fields.SetFieldProperty("EmployeeTxt6", "textsize", 11f, null);
                    fields.SetFieldProperty("EmployeeTxt7", "textsize", 11f, null);
                    fields.SetFieldProperty("EmployeeTxt8", "textsize", 11f, null);
                    fields.SetFieldProperty("EmployeeTxt9", "textsize", 11f, null);
                    fields.SetFieldProperty("EmployerTxt1", "textsize", 11f, null);
                    fields.SetFieldProperty("EmployerTxt2", "textsize", 11f, null);
                    fields.SetFieldProperty("EmployerTxt3", "textsize", 11f, null);
                    fields.SetFieldProperty("EmployerTxt4", "textsize", 11f, null);
                    fields.SetFieldProperty("EmployerTxt5", "textsize", 11f, null);
                    fields.SetFieldProperty("EmployerTxt6", "textsize", 11f, null);
                    fields.SetFieldProperty("EmployerTxt7", "textsize", 11f, null);
                    fields.SetFieldProperty("EmployerTxt8", "textsize", 11f, null);
                    fields.SetFieldProperty("EmployerTxt9", "textsize", 11f, null);
                    fields.SetFieldProperty("Totaltxt1", "textsize", 11f, null);
                    fields.SetFieldProperty("Totaltxt2", "textsize", 11f, null);
                    fields.SetFieldProperty("Totaltxt3", "textsize", 11f, null);
                    fields.SetFieldProperty("Totaltxt4", "textsize", 11f, null);
                    fields.SetFieldProperty("Totaltxt5", "textsize", 11f, null);
                    fields.SetFieldProperty("Totaltxt6", "textsize", 11f, null);
                    fields.SetFieldProperty("Totaltxt7", "textsize", 11f, null);
                    fields.SetFieldProperty("Totaltxt8", "textsize", 11f, null);
                    fields.SetFieldProperty("Totaltxt9", "textsize", 11f, null);


                    fields.SetFieldProperty("MonthTxt", "textsize", 12f, null);
                    fields.SetFieldProperty("EmployeeTxt", "textsize", 12f, null);
                    fields.SetFieldProperty("EmployerTxt", "textsize", 12f, null);
                    fields.SetFieldProperty("TotalTxt", "textsize", 12f, null);


                    fields.SetFieldProperty("SignatoryNameTxt", "textsize", 12f, null);
                    fields.SetFieldProperty("SignatoryJobDesctxt", "textsize", 12f, null);

                    fields.SetField("MonthTxt", "Month");
                    fields.SetField("EmployeeTxt", "Employee");
                    fields.SetField("EmployerTxt", "Employer");
                    fields.SetField("TotalTxt", "Total");

                    fields.SetField("SignatoryNameTxt", "Lorna T. Pabelico");
                    fields.SetField("SignatoryJobDesctxt", "Head, Human Capital");


                    #endregion
                    //Fill first paragraph
                    fields.SetField("FirstPagraph", "     This is to certify that GENERALI LIFE ASSURANCE PHILIPPINES INC.  located at the 10th Floor Petron Mega Plaza Building, Sen. Gil J. Puyat Avenue, Makati City with Corporate Philhealth ID Number 001030004862 had remitted the following Philhealth contribution payment(s) of " +
                        Gender + " " +
                        EMPNAME +
                        " with PhilHealth ID Number " +
                        PHILID +
                        ".");
                    fields.SetField("SecondParagraph", "This certification is being issued this " +
                        month + " " + day + ", " + year +
                        " as requested by the employee for " +
                        ADDS +
                        " PhilHealth claim requirements.");
                    fields.SetField("ThirdParagraph", "Generali Life Assurance Philippines Inc");


                    //string MonthTxt1 = "", MonthTxt2 = "", MonthTxt3 = "", MonthTxt4 = "", MonthTxt5 = "", MonthTxt6 = "", MonthTxt7 = "", MonthTxt8 = "", MonthTxt9 = "";
                    //string EmployeeTxt1 = "0.00", EmployeeTxt2 = "0.00", EmployeeTxt3 = "0.00", EmployeeTxt4 = "0.00", EmployeeTxt5 = "0.00", EmployeeTxt6 = "0.00", EmployeeTxt7 = "0.00", EmployeeTxt8 = "0.00", EmployeeTxt9 = "0.00";
                    //string EmployerTxt1 = "0.00", EmployerTxt2 = "0.00", EmployerTxt3 = "0.00", EmployerTxt4 = "0.00", EmployerTxt5 = "0.00", EmployerTxt6 = "0.00", EmployerTxt7 = "0.00", EmployerTxt8 = "0.00", EmployerTxt9 = "0.00";
                    //string Totaltxt1 = "0.00", Totaltxt2 = "0.00", Totaltxt3 = "0.00", Totaltxt4 = "0.00", Totaltxt5 = "0.00", Totaltxt6 = "0.00", Totaltxt7 = "0.00", Totaltxt8 = "0.00", Totaltxt9 = "0.00
                    string MonthTxt1 = "", MonthTxt2 = "", MonthTxt3 = "", MonthTxt4 = "", MonthTxt5 = "", MonthTxt6 = "", MonthTxt7 = "", MonthTxt8 = "", MonthTxt9 = "";
                    string EmployeeTxt1 = "", EmployeeTxt2 = "", EmployeeTxt3 = "", EmployeeTxt4 = "", EmployeeTxt5 = "", EmployeeTxt6 = "", EmployeeTxt7 = "", EmployeeTxt8 = "", EmployeeTxt9 = "";
                    string EmployerTxt1 = "", EmployerTxt2 = "", EmployerTxt3 = "", EmployerTxt4 = "", EmployerTxt5 = "", EmployerTxt6 = "", EmployerTxt7 = "", EmployerTxt8 = "", EmployerTxt9 = "";
                    string Totaltxt1 = "", Totaltxt2 = "", Totaltxt3 = "", Totaltxt4 = "", Totaltxt5 = "", Totaltxt6 = "", Totaltxt7 = "", Totaltxt8 = "", Totaltxt9 = "";


                    con = new Connection();
                    con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.VarChar, Value = uProfile.EmpID });
                    DataTable dt = con.GetDataTable("sp_GetReqPhilCert");
                    if (dt.Rows.Count > 0)
                    {
                        try
                        {
                            MonthTxt1 = dt.Rows[0][0].ToString();
                            EmployeeTxt1 = dt.Rows[0][1].ToString();
                            EmployerTxt1 = dt.Rows[0][2].ToString();
                            Totaltxt1 = dt.Rows[0][3].ToString();
                            MonthTxt2 = dt.Rows[1][0].ToString();
                            EmployeeTxt2 = dt.Rows[1][1].ToString();
                            EmployerTxt2 = dt.Rows[1][2].ToString();
                            Totaltxt2 = dt.Rows[1][3].ToString();
                            MonthTxt3 = dt.Rows[2][0].ToString();
                            EmployeeTxt3 = dt.Rows[2][1].ToString();
                            EmployerTxt3 = dt.Rows[2][2].ToString();
                            Totaltxt3 = dt.Rows[2][3].ToString();
                            MonthTxt4 = dt.Rows[3][0].ToString();
                            EmployeeTxt4 = dt.Rows[3][1].ToString();
                            EmployerTxt4 = dt.Rows[3][2].ToString();
                            Totaltxt4 = dt.Rows[3][3].ToString();
                            MonthTxt5 = dt.Rows[4][0].ToString();
                            EmployeeTxt5 = dt.Rows[4][1].ToString();
                            EmployerTxt5 = dt.Rows[4][2].ToString();
                            Totaltxt5 = dt.Rows[4][3].ToString();
                            MonthTxt6 = dt.Rows[5][0].ToString();
                            EmployeeTxt6 = dt.Rows[5][1].ToString();
                            EmployerTxt6 = dt.Rows[5][2].ToString();
                            Totaltxt6 = dt.Rows[5][3].ToString();
                            MonthTxt7 = dt.Rows[6][0].ToString();
                            EmployeeTxt7 = dt.Rows[6][1].ToString();
                            EmployerTxt7 = dt.Rows[6][2].ToString();
                            Totaltxt7 = dt.Rows[6][3].ToString();
                            MonthTxt8 = dt.Rows[7][0].ToString();
                            EmployeeTxt8 = dt.Rows[7][1].ToString();
                            EmployerTxt8 = dt.Rows[7][2].ToString();
                            Totaltxt8 = dt.Rows[7][3].ToString();
                            MonthTxt9 = dt.Rows[8][0].ToString();
                            EmployeeTxt9 = dt.Rows[8][1].ToString();
                            EmployerTxt9 = dt.Rows[8][2].ToString();
                            Totaltxt9 = dt.Rows[8][3].ToString();

                        }
                        catch (Exception)
                        {


                        }
                    }

                    fields.SetField("MonthTxt1", MonthTxt1);
                    fields.SetField("MonthTxt2", MonthTxt2);
                    fields.SetField("MonthTxt3", MonthTxt3);
                    fields.SetField("MonthTxt4", MonthTxt4);
                    fields.SetField("MonthTxt5", MonthTxt5);
                    fields.SetField("MonthTxt6", MonthTxt6);
                    fields.SetField("MonthTxt7", MonthTxt7);
                    fields.SetField("MonthTxt8", MonthTxt8);
                    fields.SetField("MonthTxt9", MonthTxt9);

                    fields.SetField("EmployeeTxt1", EmployeeTxt1);
                    fields.SetField("EmployeeTxt2", EmployeeTxt2);
                    fields.SetField("EmployeeTxt3", EmployeeTxt3);
                    fields.SetField("EmployeeTxt4", EmployeeTxt4);
                    fields.SetField("EmployeeTxt5", EmployeeTxt5);
                    fields.SetField("EmployeeTxt6", EmployeeTxt6);
                    fields.SetField("EmployeeTxt7", EmployeeTxt7);
                    fields.SetField("EmployeeTxt8", EmployeeTxt8);
                    fields.SetField("EmployeeTxt9", EmployeeTxt9);


                    fields.SetField("EmployerTxt1", EmployerTxt1);
                    fields.SetField("EmployerTxt2", EmployerTxt2);
                    fields.SetField("EmployerTxt3", EmployerTxt3);
                    fields.SetField("EmployerTxt4", EmployerTxt4);
                    fields.SetField("EmployerTxt5", EmployerTxt5);
                    fields.SetField("EmployerTxt6", EmployerTxt6);
                    fields.SetField("EmployerTxt7", EmployerTxt7);
                    fields.SetField("EmployerTxt8", EmployerTxt8);
                    fields.SetField("EmployerTxt9", EmployerTxt9);

                    fields.SetField("Totaltxt1", Totaltxt1);
                    fields.SetField("Totaltxt2", Totaltxt2);
                    fields.SetField("Totaltxt3", Totaltxt3);
                    fields.SetField("Totaltxt4", Totaltxt4);
                    fields.SetField("Totaltxt5", Totaltxt5);
                    fields.SetField("Totaltxt6", Totaltxt6);
                    fields.SetField("Totaltxt7", Totaltxt7);
                    fields.SetField("Totaltxt8", Totaltxt8);
                    fields.SetField("Totaltxt9", Totaltxt9);

                    #region Employee Signature
                    //Employee Signature
                    try
                    {
                        string BitEmpsign = null;
                        Byte[] EmpsignBit = null;
                        iTextSharp.text.Image Empsign = null;
                        var EmpsignContentByte = stamper.GetOverContent(1);
                        string signature1 = "";

                        Connection Con = new Connection();
                        Con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = "160040" });
                        Con.myparameters.Add(new myParameters { ParameterName = "@TYPES", mytype = SqlDbType.NVarChar, Value = "sign_0" });
                        signature1 = Con.ExecuteScalar("sp_SearchBased64");


                        if (signature1.Contains("ws.durusthr.com"))
                        {
                            try
                            {
                                ConvertImageURLToBase64(signature1);
                                signature1 = globalB64;
                            }
                            catch (Exception)
                            {


                            }

                        }


                        if (!String.IsNullOrWhiteSpace(signature1))
                        {
                            AcroFields.FieldPosition wew = fields.GetFieldPositions("SignatureTxt")[0];
                            iTextSharp.text.Rectangle rect = wew.position;
                            string xxxx = rect.Left.ToString();
                            string yyyy = rect.Bottom.ToString();
                            BitEmpsign = signature1.Replace(' ', '+');
                            EmpsignBit = Convert.FromBase64String(BitEmpsign);
                            Empsign = iTextSharp.text.Image.GetInstance(EmpsignBit);
                            Empsign.SetAbsolutePosition(float.Parse(xxxx), float.Parse(yyyy) - 3);
                            Empsign.ScaleToFit(100, 100);
                            EmpsignContentByte.AddImage(Empsign);


                        }

                    }
                    catch
                    {

                    }
                    #endregion

                    // "Flatten" the form so it wont be editable/usable anymore
                    stamper.FormFlattening = true;
                    // closing the stamper
                    stamper.Close();
                    uProfile.Status = "Success";

                }
                else if ((dab.ClientName.ToLower() == "generali_dev" || dab.ClientName.ToLower() == "generali") && uProfile.Forms == "generali_coe")
                {
                    string path = "https://ws.durusthr.com/ILM_WS_Live/pdf";
                    string path2 = HttpContext.Current.Server.MapPath("pdf");
                    var pdfReader1 = new PdfReader(path + "/generali_coe.pdf");

                    AcroFields af = pdfReader1.AcroFields;
                    string sam = "";
                    foreach (var field in af.Fields)
                    {
                        sam = sam + field.Key + "-"; // names of textfields
                    }
                    string formFile = "/generali_coe.pdf";
                    // Output file path
                    string newFile = path2 + "/generali_coe-" + uProfile.CN + "-" + uProfile.EmpID + ".pdf";

                    PdfReader reader;
                    reader = new PdfReader(path2 + "/generali_coe.pdf");

                    PdfStamper stamper = new PdfStamper(reader, new FileStream(newFile, FileMode.Create));
                    AcroFields fields = stamper.AcroFields;
                    //make singit here

                    fields.SetField("EmployeeID", uProfile.EmpID);
                    if (customCOE.Rows.Count > 0)
                    {
                        fields.SetField("EmployeeLevel", customCOE.Rows[0][1].ToString() == "" ? "N/A" : customCOE.Rows[0][1].ToString());
                        fields.SetField("ExitDate", customCOE.Rows[0][2].ToString() == "" ? "N/A" : customCOE.Rows[0][2].ToString());
                        fields.SetField("ImmediateSupervisor", customCOE.Rows[0][3].ToString() == "" ? "N/A" : customCOE.Rows[0][3].ToString());
                        fields.SetField("Day", customCOE.Rows[0][4].ToString() == "" ? "N/A" : customCOE.Rows[0][4].ToString());
                        fields.SetField("Month", customCOE.Rows[0][5].ToString() == "" ? "N/A" : customCOE.Rows[0][5].ToString());
                        fields.SetField("Year", customCOE.Rows[0][6].ToString() == "" ? "N/A" : customCOE.Rows[0][6].ToString());
                    }
                    else
                    {
                        fields.SetField("EmployeeLevel", "N/A");
                        fields.SetField("ExitDate", "N/A");
                        fields.SetField("ImmediateSupervisor", "N/A");
                        fields.SetField("Date", "N/A");
                        fields.SetField("Month", "N/A");
                        fields.SetField("Year", "N/A");
                    }

                    if (getCOE.Rows.Count > 0)
                    {
                        fields.SetField("EmployeeName", getCOE.Rows[0][0].ToString() == "" ? "N/A" : getCOE.Rows[0][0].ToString());
                        fields.SetField("DateJoined]", getCOE.Rows[0][1].ToString() == "" ? "N/A" : getCOE.Rows[0][1].ToString());
                        fields.SetField("JobDescription", getCOE.Rows[0][2].ToString() == "" ? "N/A" : getCOE.Rows[0][2].ToString());

                        //Employer Details Start
                        #region Employer Details
                        fields.SetField("CompanyName", getCOE.Rows[0]["TradingName"].ToString() == "" ? "N/A" : getCOE.Rows[0]["TradingName"].ToString());
                        fields.SetField("Address", getCOE.Rows[0]["Address"].ToString() == "" ? "N/A" : getCOE.Rows[0]["Address"].ToString());
                        #endregion
                        //Employer Details End

                    }
                    stamper.FormFlattening = true;
                    stamper.Close();
                    uProfile.Status = "Success";
                }

                else if (uProfile.Forms.ToLower() == "ilm coe")
                {
                    string path = "https://ws.durusthr.com/ILM_WS_Live/pdf";
                    string path2 = HttpContext.Current.Server.MapPath("pdf");
                    var pdfReader1 = new PdfReader(path + "/ILM COE.pdf");

                    AcroFields af = pdfReader1.AcroFields;
                    string sam = "";
                    foreach (var field in af.Fields)
                    {
                        sam = sam + field.Key + "-"; // names of textfields
                    }
                    string formFile = "/ILM COE.pdf";
                    // Output file path
                    string newFile = path2 + "/ILM COE-" + uProfile.CN + "-" + uProfile.EmpID + ".pdf";

                    PdfReader reader;
                    reader = new PdfReader(path2 + "/ILM COE.pdf");

                    PdfStamper stamper = new PdfStamper(reader, new FileStream(newFile, FileMode.Create));
                    AcroFields fields = stamper.AcroFields;
                    //make singit here

                    string Gender = "",
                            AssD2 = "",
                            ADDS2 = "",
                            EMPNAME = "",
                            PHILID = "",
                            LASTNAME = "N/A",
                            DateJoined = "N/A",
                            JobDesc = "N/A",
                            EndDate = "present",
                            MonthlySalary = "N/A",
                            Mngrid = "",
                            MANAGERNAME = "N/A",
                            ManagerJob = "N/A";

                        con = new Connection();
                        con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.VarChar, Value = uProfile.EmpID });
                        DataTable dtD = con.GetDataTable("sp_get_dataforCOE");
                        if (dtD.Rows.Count > 0)
                        {
                            try
                            {
                                Gender = dtD.Rows[0][3].ToString();
                                EMPNAME = dtD.Rows[0][0].ToString();
                                LASTNAME = dtD.Rows[0]["Last_Name"].ToString();
                                DateJoined = dtD.Rows[0]["ProductionStartDate"].ToString();
                                PHILID = dtD.Rows[0][15].ToString();
                                JobDesc = dtD.Rows[0]["JobDesc"].ToString();

                                MonthlySalary = dtD.Rows[0]["monthly"].ToString();
                                MANAGERNAME = dtD.Rows[0]["MngrName"].ToString();
                                Mngrid = dtD.Rows[0]["ManagerID"].ToString();
                                ManagerJob = dtD.Rows[0]["ManagerJobdesc"].ToString();
                                EndDate = Convert.ToDateTime(dtD.Rows[0]["EffectiveDate"].ToString()).ToString("yyyy-MM-dd");
                            }
                            catch (Exception)
                            {

                            }

                        }

                        switch (Gender)
                        {
                            case "F":
                                Gender = "Ms. ";
                                AssD2 = "She ";
                                ADDS2 = "her ";
                                break;
                            case "M":
                                Gender = "Mr. ";
                                AssD2 = "He ";
                                ADDS2 = "his ";
                                break;
                            default:
                                Gender = "Mr./Ms. ";
                                AssD2 = "He/She ";
                                ADDS2 = "his/her ";
                                break;
                        }
                        var DateNow2 = DateTime.Now.ToString("yyyy-MMMM-dd");
                        string[] dayarr = DateNow2.Replace("-", " ").Split(' ');
                        string month = "", year = "", day = "";
                        month = dayarr[1];
                        year = dayarr[0];
                        day = dayarr[2];

                        fields.SetFieldProperty("BodyTxt", "textsize", 12f, null);
                        fields.SetFieldProperty("SignatoryNameTxt", "textsize", 13f, null);
                        fields.SetFieldProperty("JobdescTxt", "textsize", 12f, null);
                        fields.SetFieldProperty("ParagraphTxt", "textsize", 12f, null);




                        fields.SetField("BodyTxt", "To Whom It May Concern:" +
                              Environment.NewLine +
                          Environment.NewLine +
                          Environment.NewLine +
                            "This is to certify that, " +
                            EMPNAME +
                            " has been an employee of " +
                            "ILLIMITADO INC as " +
                            JobDesc +
                            " from " +
                            DateJoined +
                            " to " +
                            EndDate + "." +
                             Environment.NewLine +
                          Environment.NewLine +
                          Environment.NewLine +
                            "This further certifies that " +
                            AssD2 +
                            " is a regular employee of the company and is receiving a monthly salary of " +
                            MonthlySalary +
                            "." +
                             Environment.NewLine +
                          Environment.NewLine +
                          Environment.NewLine +
                            "This certification is being issued upon the request of " +
                            LASTNAME +
                            " as a requirement for " + ADDS2 + " new employment." +
                             Environment.NewLine +
                          Environment.NewLine +
                          Environment.NewLine +
                            "This information is furnished without any risk and responsibilities on our part in any respect whatsoever." +
                            "");



                        #region Employee Signature
                        //Employee Signature
                        try
                        {



                            string BitEmpsign = null;
                            Byte[] EmpsignBit = null;
                            iTextSharp.text.Image Empsign = null;
                            var EmpsignContentByte = stamper.GetOverContent(1);
                            string signature1 = "";

                            Connection Con = new Connection();
                            Con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = Mngrid });
                            Con.myparameters.Add(new myParameters { ParameterName = "@TYPES", mytype = SqlDbType.NVarChar, Value = "sign_0" });
                            signature1 = Con.ExecuteScalar("sp_SearchBased64");
                            //globalB64 = signature1;


                            if (signature1.Contains("ws.durusthr.com"))
                            {
                                try
                                {
                                    ConvertImageURLToBase64(signature1);
                                    signature1 = globalB64;
                                }
                                catch (Exception)
                                {


                                }

                            }

                            if (!String.IsNullOrWhiteSpace(signature1))
                            {
                                AcroFields.FieldPosition wew = fields.GetFieldPositions("SignatureTxt")[0];
                                iTextSharp.text.Rectangle rect = wew.position;
                                string xxxx = rect.Left.ToString();
                                string yyyy = rect.Bottom.ToString();
                                BitEmpsign = signature1.Replace(' ', '+');
                                EmpsignBit = Convert.FromBase64String(BitEmpsign);
                                Empsign = iTextSharp.text.Image.GetInstance(EmpsignBit);
                                Empsign.SetAbsolutePosition(float.Parse(xxxx), float.Parse(yyyy) - 3);
                                Empsign.ScaleToFit(100, 100);
                                EmpsignContentByte.AddImage(Empsign);
                            }
                        }
                        catch
                        {

                        }
                        #endregion

                        fields.SetField("ParagraphTxt", "For iLLimitado Inc ");
                        fields.SetField("SignatoryNameTxt", MANAGERNAME);
                        fields.SetField("JobdescTxt", (ManagerJob));

                    // "Flatten" the form so it wont be editable/usable anymore
                    stamper.FormFlattening = true;
                    // closing the stamper
                    stamper.Close();
                    uProfile.Status = "Success";

                }

                else
                {

                    //PdfReader reader1 = new PdfReader("http://192.168.1.250:81/pdf/" + row["PDF"].ToString() + ".pdf");
                    PdfReader reader1 = new PdfReader(dr["WebLink"].ToString() + "pdf/" + uProfile.Forms + ".pdf");
                    // PdfReader reader1 = new PdfReader(HttpContext.Current.Server.MapPath("~/pdf/") + row["PDF"].ToString() + ".pdf");
                    LocationTextExtractionStrategy lteStrategy = new LocationTextExtractionStrategy();
                    string strExact = PdfTextExtractor.GetTextFromPage(reader1, 1, lteStrategy);
                    StringBuilder sb = new StringBuilder();
                    sb.Append(PdfTextExtractor.GetTextFromPage(reader1, 1));

                    sb.Replace("[Employee ID]", uProfile.EmpID);
                    sb.Replace("[Employee\nID]", uProfile.EmpID);
                    if (getCOE.Rows.Count > 0)
                    {
                        sb.Replace("[Employee Name]", getCOE.Rows[0][0].ToString() == "" ? "N/A" : getCOE.Rows[0][0].ToString());
                        sb.Replace("[Date Joined]", getCOE.Rows[0][1].ToString() == "" ? "N/A" : getCOE.Rows[0][1].ToString());
                        sb.Replace("[Employee\nName]", getCOE.Rows[0][0].ToString() == "" ? "N/A" : getCOE.Rows[0][0].ToString());
                        sb.Replace("[Date\nJoined]", getCOE.Rows[0][1].ToString() == "" ? "N/A" : getCOE.Rows[0][1].ToString());

                        sb.Replace("[Job Description]", getCOE.Rows[0][2].ToString() == "" ? "N/A" : getCOE.Rows[0][2].ToString());
                        sb.Replace("[Department Code]", getCOE.Rows[0][4].ToString() == "" ? "N/A" : getCOE.Rows[0][4].ToString());
                        sb.Replace("[Job\nDescription]", getCOE.Rows[0][2].ToString() == "" ? "N/A" : getCOE.Rows[0][2].ToString());
                        sb.Replace("[Department\nCode]", getCOE.Rows[0][4].ToString() == "" ? "N/A" : getCOE.Rows[0][4].ToString());

                        //Employer Details Start
                        #region Employer Details
                        sb.Replace("[Company Name]", getCOE.Rows[0]["TradingName"].ToString() == "" ? "N/A" : getCOE.Rows[0]["TradingName"].ToString());
                        sb.Replace("[Address]", getCOE.Rows[0]["Address"].ToString() == "" ? "N/A" : getCOE.Rows[0]["Address"].ToString());
                        sb.Replace("[ZIP]", getCOE.Rows[0]["AddZipCode"].ToString() == "" ? "N/A" : getCOE.Rows[0]["AddZipCode"].ToString());
                        sb.Replace("[Telephone]", getCOE.Rows[0]["ContactNumber"].ToString() == "" ? "N/A" : getCOE.Rows[0]["ContactNumber"].ToString());
                        sb.Replace("[Fax]", "N/A");
                        sb.Replace("[General Email]", getCOE.Rows[0]["Emailtxt"].ToString() == "" ? "N/A" : getCOE.Rows[0]["Emailtxt"].ToString());
                        sb.Replace("[TIN]", getCOE.Rows[0]["Tin"].ToString() == "" ? "N/A" : getCOE.Rows[0]["Tin"].ToString());
                        sb.Replace("[SSS]", getCOE.Rows[0]["Sss"].ToString() == "" ? "N/A" : getCOE.Rows[0]["Sss"].ToString());
                        sb.Replace("[Philhealth]", getCOE.Rows[0]["Philhealth"].ToString() == "" ? "N/A" : getCOE.Rows[0]["Philhealth"].ToString());
                        sb.Replace("[HDMF]", getCOE.Rows[0]["Pagibig"].ToString() == "" ? "N/A" : getCOE.Rows[0]["Pagibig"].ToString());
                        sb.Replace("[Web Page]", "N/A");
                        sb.Replace("[Company Registration]", getCOE.Rows[0]["CompanyRegistrationNo"].ToString() == "" ? "N/A" : getCOE.Rows[0]["CompanyRegistrationNo"].ToString());
                        sb.Replace("[Date of Registration]", getCOE.Rows[0]["DateOfRegistration"].ToString() == "" ? "N/A" : getCOE.Rows[0]["DateOfRegistration"].ToString());
                        sb.Replace("[Date of Registration]", getCOE.Rows[0]["DateOfRegistration"].ToString() == "" ? "N/A" : getCOE.Rows[0]["DateOfRegistration"].ToString());
                        sb.Replace("[Resigned]", "N/A");
                        #endregion
                        //Employer Details End

                        //Employee Profile Start
                        #region Employee Profile

                        sb.Replace("[Contract End Date]", "N/A");
                        sb.Replace("[Hourly Rate]", getCOE.Rows[0]["hourly"].ToString() == "" ? "N/A" : getCOE.Rows[0]["hourly"].ToString());
                        sb.Replace("[Daily Rate]", getCOE.Rows[0]["daily"].ToString() == "" ? "N/A" : getCOE.Rows[0]["daily"].ToString());
                        sb.Replace("[Monthly Rate]", getCOE.Rows[0]["monthly"].ToString() == "" ? "N/A" : getCOE.Rows[0]["monthly"].ToString());
                        sb.Replace("[Tax Status]", getCOE.Rows[0]["tax_status"].ToString() == "" ? "N/A" : getCOE.Rows[0]["tax_status"].ToString());
                        sb.Replace("[Tax Category]", "N/A");
                        #endregion
                        //Employee Profile End

                    }


                    string url = dr["WebLink"].ToString() + "pdf/" + uProfile.Forms + ".pdf";
                    //string url = "http://192.168.1.250:81/pdf/" + row["PDF"].ToString() + ".pdf";
                    // The easiest way to load our document from the internet is make use of the 
                    // System.Net.WebClient class. Create an instance of it and pass the URL
                    // to download from.
                    WebClient webClient = new WebClient();

                    // Download the bytes from the location referenced by the URL.
                    byte[] dataBytes = webClient.DownloadData(url);

                    // Wrap the bytes representing the document in memory into a MemoryStream object.
                    MemoryStream byteStream = new MemoryStream(dataBytes);


                    // Stream stream = File.OpenRead(path );
                    Aspose.Pdf.Document doc = new Aspose.Pdf.Document(byteStream);

                    sb.Replace("[Signature 1]", "__________");
                    sb.Replace("[Signature 2]", "__________");
                    sb.Replace("[Signature 3]", "__________");
                    sb.Replace("[Employee Signature]", "__________");
                    sb.Replace("[Signature\n1]", "__________");
                    sb.Replace("[Signature\n2]", "__________");
                    sb.Replace("[Signature\n2]", "__________");
                    sb.Replace("[Employee\nSignature]", "__________");


                    FileStream Newstream = new FileStream(string.Concat(HttpContext.Current.Server.MapPath("~/pdf/"), uProfile.Forms + "-" + uProfile.CN + "-" + uProfile.EmpID + ".pdf"), FileMode.Create);


                    MemoryStream ms = new MemoryStream();
                    Document document = new Document(PageSize.A4, 25, 25, 30, 30);
                    //Code for adding the string builder sb to the document
                    PdfWriter writer = PdfWriter.GetInstance(document, Newstream);

                    document.Open();
                    document.Add(new Paragraph(sb.ToString()));

                    PayMtd.GetSignatureSignPicture(uProfile.EmpID, "sign_1");
                    //if (PayMtd.BitSign != null)
                    //{


                    //    List<Phrase> PhraseList = new List<Phrase>();
                    //    for (int page = 1; page <= reader1.NumberOfPages; page++)
                    //    {
                    //        ITextExtractionStrategy strategy = new SimpleTextExtractionStrategy();
                    //        string currentPageText = PdfTextExtractor.GetTextFromPage(reader1, page, strategy);
                    //        if (currentPageText.Contains("[Employee Signature]") || currentPageText.Contains("[Employee\nSignature]"))
                    //        {
                    //            TextFragmentAbsorber absorber = new TextFragmentAbsorber("[Employee Signature]");


                    //            // Accept the absorber for first page
                    //            doc.Pages[1].Accept(absorber);

                    //            // View text and placement info of first text occurrence
                    //            TextFragment firstOccurrence = absorber.TextFragments[1];
                    //            // document.Add(new Paragraph(Convert.ToDouble(firstOccurrence.Position.XIndent).ToString()));
                    //            float xx = (float)firstOccurrence.Position.XIndent + 20;
                    //            float yy = (float)firstOccurrence.Position.YIndent;


                    //            string Bitsignature = null;
                    //            Bitsignature = PayMtd.BitSign;
                    //            Byte[] SignatureBit = null;
                    //            iTextSharp.text.Image signatory = null;

                    //            Bitsignature = Bitsignature.Replace(' ', '+');
                    //            SignatureBit = Convert.FromBase64String(Bitsignature);
                    //            signatory = iTextSharp.text.Image.GetInstance(SignatureBit);
                    //            signatory.SetAbsolutePosition(xx, yy);
                    //            signatory.ScaleToFit(40, 40);

                    //            document.Add(signatory);
                    //        }
                    //        if (currentPageText.Contains("[President Signature]") || currentPageText.Contains("[President\nSignature]"))
                    //        {
                    //            TextFragmentAbsorber absorber = new TextFragmentAbsorber("[President Signature]");


                    //            // Accept the absorber for first page
                    //            doc.Pages[1].Accept(absorber);

                    //            // View text and placement info of first text occurrence
                    //            TextFragment firstOccurrence = absorber.TextFragments[1];
                    //            // document.Add(new Paragraph(Convert.ToDouble(firstOccurrence.Position.XIndent).ToString()));
                    //            float xx = (float)firstOccurrence.Position.XIndent + 20;
                    //            float yy = (float)firstOccurrence.Position.YIndent;


                    //            string Bitsignature = null;
                    //            Bitsignature = PayMtd.BitSign;
                    //            Byte[] SignatureBit = null;
                    //            iTextSharp.text.Image signatory = null;

                    //            Bitsignature = Bitsignature.Replace(' ', '+');
                    //            SignatureBit = Convert.FromBase64String(Bitsignature);
                    //            signatory = iTextSharp.text.Image.GetInstance(SignatureBit);
                    //            signatory.SetAbsolutePosition(xx, yy);

                    //            signatory.ScaleToFit(40, 40);

                    //            document.Add(signatory);
                    //        }

                    //        if (currentPageText.Contains("[HR Signature]") || currentPageText.Contains("[HR\nSignature]"))
                    //        {
                    //            TextFragmentAbsorber absorber = new TextFragmentAbsorber("[HR Signature]");


                    //            // Accept the absorber for first page
                    //            doc.Pages[1].Accept(absorber);

                    //            // View text and placement info of first text occurrence
                    //            TextFragment firstOccurrence = absorber.TextFragments[1];
                    //            // document.Add(new Paragraph(Convert.ToDouble(firstOccurrence.Position.XIndent).ToString()));
                    //            float xx = (float)firstOccurrence.Position.XIndent + 20;
                    //            float yy = (float)firstOccurrence.Position.YIndent;


                    //            string Bitsignature = null;
                    //            Bitsignature = PayMtd.BitSign;
                    //            Byte[] SignatureBit = null;
                    //            iTextSharp.text.Image signatory = null;

                    //            Bitsignature = Bitsignature.Replace(' ', '+');
                    //            SignatureBit = Convert.FromBase64String(Bitsignature);
                    //            signatory = iTextSharp.text.Image.GetInstance(SignatureBit);
                    //            signatory.SetAbsolutePosition(xx, yy);

                    //            signatory.ScaleToFit(40, 40);

                    //            document.Add(signatory);
                    //        }

                    //    }
                    //}

                    //PayMtd.GetSignatureSignPicturePres(empid);
                    PayMtd.GetSignatureALL("Signature 1");
                    PayMtd.GetSignatureALL("Signature 2");
                    PayMtd.GetSignatureALL("Signature 3");
                    if (PayMtd.BitSign != null)
                    {


                        List<Phrase> PhraseList = new List<Phrase>();
                        for (int page = 1; page <= reader1.NumberOfPages; page++)
                        {
                            ITextExtractionStrategy strategy = new SimpleTextExtractionStrategy();
                            string currentPageText = PdfTextExtractor.GetTextFromPage(reader1, page, strategy);
                            if (currentPageText.Contains("[Employee Signature]") || currentPageText.Contains("[Employee\nSignature]"))
                            {
                                TextFragmentAbsorber absorber = new TextFragmentAbsorber("[Employee Signature]");


                                // Accept the absorber for first page
                                doc.Pages[1].Accept(absorber);

                                // View text and placement info of first text occurrence
                                TextFragment firstOccurrence = absorber.TextFragments[1];
                                // document.Add(new Paragraph(Convert.ToDouble(firstOccurrence.Position.XIndent).ToString()));
                                float xx = (float)firstOccurrence.Position.XIndent + 20;
                                float yy = (float)firstOccurrence.Position.YIndent;

                                if (PayMtd.BitSign.Contains("ws.durusthr.com"))
                                {
                                    try
                                    {

                                        ConvertImageURLToBase64(PayMtd.BitSign);
                                        PayMtd.BitSign = globalB64;

                                        string Bitsignature = null;
                                        Bitsignature = PayMtd.BitSign;
                                        Byte[] SignatureBit = null;
                                        iTextSharp.text.Image signatory = null;

                                        Bitsignature = Bitsignature.Replace(' ', '+');
                                        SignatureBit = Convert.FromBase64String(Bitsignature);
                                        signatory = iTextSharp.text.Image.GetInstance(SignatureBit);
                                        signatory.SetAbsolutePosition(xx, yy);

                                        signatory.ScaleToFit(50, 50);

                                        document.Add(signatory);
                                    }
                                    catch (Exception)
                                    {
                                        //ConvertImageURLToBase64(B64.signature);
                                        //B64.signature = defprof;


                                    }

                                }
                                else
                                {
                                    string Bitsignature = null;
                                    Bitsignature = PayMtd.BitSign;
                                    Byte[] SignatureBit = null;
                                    iTextSharp.text.Image signatory = null;

                                    Bitsignature = Bitsignature.Replace(' ', '+');
                                    SignatureBit = Convert.FromBase64String(Bitsignature);
                                    signatory = iTextSharp.text.Image.GetInstance(SignatureBit);
                                    signatory.SetAbsolutePosition(xx, yy);
                                    signatory.ScaleToFit(50, 50);

                                    document.Add(signatory);
                                }
                                
                            }

                        }
                    }

                    if (PayMtd.sig1 != null)
                    {


                        List<Phrase> PhraseList = new List<Phrase>();
                        for (int page = 1; page <= reader1.NumberOfPages; page++)
                        {
                            ITextExtractionStrategy strategy = new SimpleTextExtractionStrategy();
                            string currentPageText = PdfTextExtractor.GetTextFromPage(reader1, page, strategy);
                            if (currentPageText.Contains("[Signature 1]") || currentPageText.Contains("[Signature\n1]"))
                            {
                                TextFragmentAbsorber absorber = new TextFragmentAbsorber("[Signature 1]");


                                // Accept the absorber for first page
                                doc.Pages[1].Accept(absorber);

                                // View text and placement info of first text occurrence
                                TextFragment firstOccurrence = absorber.TextFragments[1];
                                // document.Add(new Paragraph(Convert.ToDouble(firstOccurrence.Position.XIndent).ToString()));
                                float xx = (float)firstOccurrence.Position.XIndent + 20;
                                float yy = (float)firstOccurrence.Position.YIndent;
                                if (PayMtd.sig1.Contains("ws.durusthr.com"))
                                {
                                    try
                                    {

                                        ConvertImageURLToBase64(PayMtd.sig1);
                                        PayMtd.sig1 = globalB64;

                                        string Bitsignature = null;
                                        Bitsignature = PayMtd.sig1;
                                        Byte[] SignatureBit = null;
                                        iTextSharp.text.Image signatory = null;

                                        Bitsignature = Bitsignature.Replace(' ', '+');
                                        SignatureBit = Convert.FromBase64String(Bitsignature);
                                        signatory = iTextSharp.text.Image.GetInstance(SignatureBit);
                                        signatory.SetAbsolutePosition(xx, yy);

                                        signatory.ScaleToFit(50, 50);

                                        document.Add(signatory);
                                    }
                                    catch (Exception)
                                    {
                                        //ConvertImageURLToBase64(B64.signature);
                                        //B64.signature = defprof;


                                    }

                                }
                                else
                                {
                                    string Bitsignature = null;
                                    Bitsignature = PayMtd.sig1;
                                    Byte[] SignatureBit = null;
                                    iTextSharp.text.Image signatory = null;

                                    Bitsignature = Bitsignature.Replace(' ', '+');
                                    SignatureBit = Convert.FromBase64String(Bitsignature);
                                    signatory = iTextSharp.text.Image.GetInstance(SignatureBit);
                                    signatory.SetAbsolutePosition(xx, yy);

                                    signatory.ScaleToFit(50, 50);

                                    document.Add(signatory);
                                }

                                
                            }


                        }
                    }

                    if (PayMtd.sig2 != null)
                    {


                        List<Phrase> PhraseList = new List<Phrase>();
                        for (int page = 1; page <= reader1.NumberOfPages; page++)
                        {
                            ITextExtractionStrategy strategy = new SimpleTextExtractionStrategy();
                            string currentPageText = PdfTextExtractor.GetTextFromPage(reader1, page, strategy);
                            if (currentPageText.Contains("[Signature 2]") || currentPageText.Contains("[Signature\n2]"))
                            {
                                TextFragmentAbsorber absorber = new TextFragmentAbsorber("[Signature 2]");


                                // Accept the absorber for first page
                                doc.Pages[1].Accept(absorber);

                                // View text and placement info of first text occurrence
                                TextFragment firstOccurrence = absorber.TextFragments[1];
                                // document.Add(new Paragraph(Convert.ToDouble(firstOccurrence.Position.XIndent).ToString()));
                                float xx = (float)firstOccurrence.Position.XIndent + 20;
                                float yy = (float)firstOccurrence.Position.YIndent;
                                if (PayMtd.sig2.Contains("ws.durusthr.com"))
                                {
                                    try
                                    {

                                        ConvertImageURLToBase64(PayMtd.sig2);
                                        PayMtd.sig2 = globalB64;

                                        string Bitsignature = null;
                                        Bitsignature = PayMtd.sig2;
                                        Byte[] SignatureBit = null;
                                        iTextSharp.text.Image signatory = null;

                                        Bitsignature = Bitsignature.Replace(' ', '+');
                                        SignatureBit = Convert.FromBase64String(Bitsignature);
                                        signatory = iTextSharp.text.Image.GetInstance(SignatureBit);
                                        signatory.SetAbsolutePosition(xx, yy);

                                        signatory.ScaleToFit(50, 50);

                                        document.Add(signatory);
                                    }
                                    catch (Exception)
                                    {
                                        //ConvertImageURLToBase64(B64.signature);
                                        //B64.signature = defprof;


                                    }

                                }
                                else
                                {
                                    string Bitsignature = null;
                                    Bitsignature = PayMtd.sig2;
                                    Byte[] SignatureBit = null;
                                    iTextSharp.text.Image signatory = null;

                                    Bitsignature = Bitsignature.Replace(' ', '+');
                                    SignatureBit = Convert.FromBase64String(Bitsignature);
                                    signatory = iTextSharp.text.Image.GetInstance(SignatureBit);
                                    signatory.SetAbsolutePosition(xx, yy);

                                    signatory.ScaleToFit(50, 50);

                                    document.Add(signatory);
                                }


                            }


                        }
                    }

                    if (PayMtd.sig3 != null)
                    {


                        List<Phrase> PhraseList = new List<Phrase>();
                        for (int page = 1; page <= reader1.NumberOfPages; page++)
                        {
                            ITextExtractionStrategy strategy = new SimpleTextExtractionStrategy();
                            string currentPageText = PdfTextExtractor.GetTextFromPage(reader1, page, strategy);
                            if (currentPageText.Contains("[Signature 3]") || currentPageText.Contains("[Signature\n3]"))
                            {
                                TextFragmentAbsorber absorber = new TextFragmentAbsorber("[Signature 3]");


                                // Accept the absorber for first page
                                doc.Pages[1].Accept(absorber);

                                // View text and placement info of first text occurrence
                                TextFragment firstOccurrence = absorber.TextFragments[1];
                                // document.Add(new Paragraph(Convert.ToDouble(firstOccurrence.Position.XIndent).ToString()));
                                float xx = (float)firstOccurrence.Position.XIndent + 20;
                                float yy = (float)firstOccurrence.Position.YIndent;
                                if (PayMtd.sig3.Contains("ws.durusthr.com"))
                                {
                                    try
                                    {

                                        ConvertImageURLToBase64(PayMtd.sig3);
                                        PayMtd.sig3 = globalB64;

                                        string Bitsignature = null;
                                        Bitsignature = PayMtd.sig3;
                                        Byte[] SignatureBit = null;
                                        iTextSharp.text.Image signatory = null;

                                        Bitsignature = Bitsignature.Replace(' ', '+');
                                        SignatureBit = Convert.FromBase64String(Bitsignature);
                                        signatory = iTextSharp.text.Image.GetInstance(SignatureBit);
                                        signatory.SetAbsolutePosition(xx, yy);

                                        signatory.ScaleToFit(50, 50);

                                        document.Add(signatory);
                                    }
                                    catch (Exception)
                                    {
                                        //ConvertImageURLToBase64(B64.signature);
                                        //B64.signature = defprof;


                                    }

                                }
                                else
                                {
                                    string Bitsignature = null;
                                    Bitsignature = PayMtd.sig3;
                                    Byte[] SignatureBit = null;
                                    iTextSharp.text.Image signatory = null;

                                    Bitsignature = Bitsignature.Replace(' ', '+');
                                    SignatureBit = Convert.FromBase64String(Bitsignature);
                                    signatory = iTextSharp.text.Image.GetInstance(SignatureBit);
                                    signatory.SetAbsolutePosition(xx, yy);

                                    signatory.ScaleToFit(50, 50);

                                    document.Add(signatory);
                                }

 
                            }


                        }
                    }






                    document.Close();
                    writer.Close();
                    // closing the stamper
                    uProfile.Status = "Success";
                }
            }
            catch (Exception ex)
            {
                uProfile.Status = ex.ToString();
                //this.Page.ClientScript.RegisterClientScriptBlock(this.GetType(),
                //Guid.NewGuid().ToString("N"), "self.parent.location='login.aspx';", true);
            }

            return uProfile.Status;
        }

        public static String ConvertImageURLToBase64(String url)
        {
            StringBuilder _sb = new StringBuilder();

            Byte[] _byte = GetImage(url);

            _sb.Append(Convert.ToBase64String(_byte, 0, _byte.Length));
            globalB64 = _sb.ToString();
            return globalB64;
        }

        public static byte[] GetImage(string url)
        {
            Stream stream = null;
            byte[] buf;

            try
            {
                WebProxy myProxy = new WebProxy();
                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(url);

                HttpWebResponse response = (HttpWebResponse)req.GetResponse();
                stream = response.GetResponseStream();

                using (BinaryReader br = new BinaryReader(stream))
                {
                    int len = (int)(response.ContentLength);
                    buf = br.ReadBytes(len);
                    br.Close();
                }

                stream.Close();
                response.Close();
            }
            catch (Exception exp)
            {
                buf = null;
            }

            return (buf);
        }






    }
}
