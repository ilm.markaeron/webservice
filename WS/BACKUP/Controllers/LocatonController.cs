﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using illimitadoWepAPI.Models;
using System.Xml.Serialization;
using System.IO;
using System.Data;
using System.Data.SqlClient;
using illimitadoWepAPI.MyClass;
using static illimitadoWepAPI.Models.TimeKeeping;
using static illimitadoWepAPI.Models.AvailableBenefits;
using static illimitadoWepAPI.Models.ReimbursableReceiptBenefits;

namespace illimitadoWepAPI.Controllers
{
    public class LocatonController : ApiController
    {
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/TimeKeeping/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/TimeKeeping
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/TimeKeeping/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/TimeKeeping/5
        public void Delete(int id)
        {
        }


        //[HttpPost]
        //[Route("GetRegion")]
        ////{
        ////"NTID": "i101",
        ////"Start_Date": "2017-04-01",
        ////"End_Date": "2017-04-30"
        ////}

        //public ListAllLocationResult GetRegion()
        //{
        //    ListAllLocationResult GetAllLocation = new ListAllLocationResult();
        //    try
        //    {
        //        Connection Connection = new Connection();
        //        List<Location> ListAllLocation = new List<Location>();
        //        DataSet ds = new DataSet();
        //        DataTable dtLoc = new DataTable();

        //        //Connection.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = userToSave.LoginID });

        //        ds = Connection.GetDataset("sp_Vue_GetRegion");
        //        dtLoc = ds.Tables[0];
        //        if (dtLoc.Rows.Count > 0)
        //        {
        //            foreach (DataRow row in dtLoc.Rows)
        //            {
        //                Location LocRequest = new Location
        //                {


        //                    Loc = row["regDesc"].ToString()



        //                };
        //                ListAllLocation.Add(LocRequest);
        //            }
        //            GetAllLocation.Locations = ListAllLocation;
        //            GetAllLocation.myreturn = "Success";
        //        }
        //        else
        //        {
        //            GetAllLocation.myreturn = "No Data Available";
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        GetAllLocation.myreturn = ex.Message;
        //    }
        //    return GetAllLocation;
        //}

        //[HttpPost]
        //[Route("GetCityMun")]
        ////{
        ////"NTID": "i101",
        ////"Start_Date": "2017-04-01",
        ////"End_Date": "2017-04-30"
        ////}

        //public ListAllLocationResult GetCityMun(Location Loc)
        //{
        //    ListAllLocationResult GetAllLocation = new ListAllLocationResult();
        //    try
        //    {
        //        Connection Connection = new Connection();
        //        List<Location> ListAllLocation = new List<Location>();
        //        DataSet ds = new DataSet();
        //        DataTable dtLoc = new DataTable();

        //        Connection.myparameters.Add(new myParameters { ParameterName = "@REGION", mytype = SqlDbType.NVarChar, Value = Loc.Loc });

        //        ds = Connection.GetDataset("sp_Vue_GetCityMun");
        //        dtLoc = ds.Tables[0];
        //        if (dtLoc.Rows.Count > 0)
        //        {
        //            foreach (DataRow row in dtLoc.Rows)
        //            {
        //                Location LocRequest = new Location
        //                {


        //                    Loc = row["citymunDesc"].ToString()



        //                };
        //                ListAllLocation.Add(LocRequest);
        //            }
        //            GetAllLocation.Locations = ListAllLocation;
        //            GetAllLocation.myreturn = "Success";
        //        }
        //        else
        //        {
        //            GetAllLocation.myreturn = "No Data Available";
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        GetAllLocation.myreturn = ex.Message;
        //    }
        //    return GetAllLocation;
        //}
        [HttpPost]
        [Route("GetSakaMsg")]
        public ListTimesheetView_Result GetTimesheetView()
        {
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = "XDB_ImpactHack";
            SelfieRegistration2Controller.GetDB2(GDB);
            ListTimesheetView_Result GetActivityResult = new ListTimesheetView_Result();
            //try
            //{
            Connection Connection = new Connection();
            List<ListTimesheetView> AllActivity = new List<ListTimesheetView>();
            DataTable dtDAILY = new DataTable();
            dtDAILY = Connection.GetDataTable("sp_GetMsg");
            //CultureInfo provider = CultureInfo.InvariantCulture;
            if (dtDAILY.Rows.Count > 0)
            {

                for (int I = 0; I < dtDAILY.Rows.Count; I++)
                {
                    ListTimesheetView ListAllActivity = new ListTimesheetView();

                    ListAllActivity.EmpID = dtDAILY.Rows[I]["TxtMsg"].ToString();
                    ListAllActivity.SeriesID = dtDAILY.Rows[I]["Number"].ToString();


                    AllActivity.Add(ListAllActivity);
                }
                GetActivityResult.ListTimesheetView = AllActivity;
                GetActivityResult.myreturn = "Success";
            }
            else
            {
                GetActivityResult.myreturn = "No Data Available";
            }

            return GetActivityResult;
        }
        [HttpPost]
        [Route("InsertGeofence")]
        public string InsertGeofence(LocationNotifv2 LocationNotif)
        {
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = LocationNotif.CN;
            SelfieRegistration2Controller.GetDB2(GDB);

            Connection Connection = new Connection();
            Connection.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = LocationNotif.EmpID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@DATETIME", mytype = SqlDbType.NVarChar, Value = LocationNotif.DateTime });
            Connection.myparameters.Add(new myParameters { ParameterName = "@NotifTitle", mytype = SqlDbType.NVarChar, Value = LocationNotif.NotifTitle });
            Connection.myparameters.Add(new myParameters { ParameterName = "@NotifDescription", mytype = SqlDbType.NVarChar, Value = LocationNotif.NotifDescription });
            // DataTable EmployeeDt = new DataTable();
            String ID = Connection.ExecuteScalar("sp_LocationNotif_Mobile");

            return "Success";

        }


        [HttpPost]
        [Route("Notification")]
        public string LocationNotification(LocationNotif LocationNotif)
        {
            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = LocationNotif.EmpID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@DATETIME", mytype = SqlDbType.NVarChar, Value = LocationNotif.DateTime });
            Connection.myparameters.Add(new myParameters { ParameterName = "@NotifTitle", mytype = SqlDbType.NVarChar, Value = LocationNotif.NotifTitle });
            Connection.myparameters.Add(new myParameters { ParameterName = "@NotifDescription", mytype = SqlDbType.NVarChar, Value = LocationNotif.NotifDescription });
            // DataTable EmployeeDt = new DataTable();
            String ID = Connection.ExecuteScalar("sp_LocationNotif_Mobile");

            return "Success";

        }

        [HttpPost]
        [Route("GetNotification")]
        //{
        //"NTID": "i101",
        //"Start_Date": "2017-04-01",
        //"End_Date": "2017-04-30"
        //}

        public ListAllLocationNotifResult GetLocationNotification(LocationNotif LocationNotif)
        {
            ListAllLocationNotifResult GetAllLocationNotif = new ListAllLocationNotifResult();
            try
            {
                Connection Connection = new Connection();
                List<LocationNotif> ListAllLocationNotif = new List<LocationNotif>();
                DataSet ds = new DataSet();
                DataTable dtLoc = new DataTable();

                Connection.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = LocationNotif.EmpID });

                ds = Connection.GetDataset("sp_Get_LocationNotif_Mobile");
                dtLoc = ds.Tables[0];
                if (dtLoc.Rows.Count > 0)
                {
                    foreach (DataRow row in dtLoc.Rows)
                    {
                        LocationNotif LocNotifRequest = new LocationNotif
                        {   ID = row["ID"].ToString(),
                            EmpID = row["EmpID"].ToString(),
                            IS_EmpID = row["IS_EmpID"].ToString(),
                            DateTime = row["DateTime"].ToString(),
                            NotifTitle = row["NotifTitle"].ToString(),
                            NotifDescription = row["NotifDescription"].ToString()

                        };
                        ListAllLocationNotif.Add(LocNotifRequest);
                    }
                    GetAllLocationNotif.LocationNotif = ListAllLocationNotif;
                    GetAllLocationNotif.myreturn = "Success";
                }
                else
                {
                    GetAllLocationNotif.myreturn = "No Data Available";
                }
            }
            catch (Exception ex)
            {
                GetAllLocationNotif.myreturn = ex.Message;
            }
            return GetAllLocationNotif;
        }


        [HttpPost]
        [Route("DeleteLocationNotif")]
        public string DeleteLocationNotif(LocationNotif Notif)
        {
            try
            {
                Connection Con = new Connection();
                Con.myparameters.Add(new myParameters { ParameterName = "@ID", mytype = SqlDbType.NVarChar, Value = Notif.ID });               
                Con.ExecuteScalar("sp_Delete_LocationNotif_Mobile");
                return "success";
            }
            catch (Exception e)
            {
                return "failed" + e;
            }
        }

        [HttpPost]
        [Route("GetAvailableBenefits")]
        //{
        //"NTID": "i101",
        //"Start_Date": "2017-04-01",
        //"End_Date": "2017-04-30"
        //}

        public ListAllAvailableBenefits AvailableBenefitsGet(LocationNotif LocationNotif)
        {
            ListAllAvailableBenefits GetAllAvailBenefits = new ListAllAvailableBenefits();
            try
            {
                Connection Connection = new Connection();
                List<AvailableBenefits> ListAllAvailBenefits = new List<AvailableBenefits>();
                DataSet ds = new DataSet();
                DataTable dtLoc = new DataTable();

                Connection.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = LocationNotif.EmpID });

                ds = Connection.GetDataset("sp_GetAvailableBenefits_Mobile");
                dtLoc = ds.Tables[0];
                if (dtLoc.Rows.Count > 0)
                {
                    foreach (DataRow row in dtLoc.Rows)
                    {
                        AvailableBenefits LocNotifRequest = new AvailableBenefits
                        {
                            BenefitType = row["BENEFITSTYPE"].ToString(),
                            BenefitID = row["BENEFITSID"].ToString(),
                            BenefitName= row["BENEFITSNAME"].ToString(),
                            BenefitsAmount= row["BENEFITSAMMOUNT"].ToString()

                        };
                        ListAllAvailBenefits.Add(LocNotifRequest);
                    }
                    GetAllAvailBenefits.Benefits = ListAllAvailBenefits;
                    GetAllAvailBenefits.myreturn = "Success";
                }
                else
                {
                    GetAllAvailBenefits.myreturn = "No Data Available";
                }
            }
            catch (Exception ex)
            {
                GetAllAvailBenefits.myreturn = ex.Message;
            }
            return GetAllAvailBenefits;
        }


        [HttpPost]
        [Route("GetZipMunRegCountrys")]
        public ZipMunRegCountrys getZipMunRegCountrys()
        {
            ZipMunRegCountrys getZMRG = new ZipMunRegCountrys();
            Connection con = new Connection();
            //GetDatabase GDB = new GetDatabase();
            //GDB.ClientName = "ilm";
            //SelfieRegistration2Controller.GetDB2(GDB);
            List<ZipMunRegCountryParams> ListAllSwap = new List<ZipMunRegCountryParams>();
            //DataSet ds = new DataSet();
            DataTable dt = con.GetDataTable("sp_ZipMunRegCountry");

            foreach (DataRow row in dt.Rows)
            {
                ZipMunRegCountryParams param = new ZipMunRegCountryParams()
                {
                    ID = row[0].ToString(),
                    Zipcode = row[1].ToString(),
                    Country = row[2].ToString(),
                    Region = row[3].ToString(),
                    Province = row[4].ToString(),
                    Municipal = row[5].ToString() 
                };
                ListAllSwap.Add(param);
            }
            getZMRG.ZipMunRegCountryParams = ListAllSwap;
            getZMRG.myreturn = "Success";
            return getZMRG;
        }

        [HttpPost]
        [Route("GetBrgy")]

        public List<BrgyList> GetBrgy(BrgyList GetZip)
        {
            Connection con = new Connection();
            DataTable DT = new DataTable();
            con.myparameters.Add(new myParameters { ParameterName = "@ZIPCODE", mytype = SqlDbType.NVarChar, Value = GetZip.ZIPCODE });

            DT = con.GetDataTable("sp_GetBrgyCode");
            List<BrgyList> ListReturned = new List<BrgyList>();
            for (int i = 0; i < DT.Rows.Count; i++)
            {
                BrgyList GetZ = new BrgyList();
                GetZ.BRGYID = DT.Rows[i]["brgyCode"].ToString();
                GetZ.BRGYNAME = DT.Rows[i]["brgyDesc"].ToString();

                ListReturned.Add(GetZ);
            }
            return ListReturned;
        }

        //---------------------------------------------------------------Revised Change DB Start------------------------------------------------------------------------

        [HttpPost]
        [Route("Notificationv2")]
        public string LocationNotificationv2(LocationNotifv2 LocationNotif)
        {
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = LocationNotif.CN;
            SelfieRegistration2Controller.GetDB2(GDB);

            Connection Connection = new Connection();
            Connection.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = LocationNotif.EmpID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@DATETIME", mytype = SqlDbType.NVarChar, Value = LocationNotif.DateTime });
            Connection.myparameters.Add(new myParameters { ParameterName = "@NotifTitle", mytype = SqlDbType.NVarChar, Value = LocationNotif.NotifTitle });
            Connection.myparameters.Add(new myParameters { ParameterName = "@NotifDescription", mytype = SqlDbType.NVarChar, Value = LocationNotif.NotifDescription });
            // DataTable EmployeeDt = new DataTable();
            String ID = Connection.ExecuteScalar("sp_LocationNotif_Mobile");

            return "Success";

        }



        [HttpPost]
        [Route("GetAvailableBenefitsv2")]
        public ListAllAvailableBenefits AvailableBenefitsGetv2(LocationNotifv2 LocationNotif)
        {
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = LocationNotif.CN;
            SelfieRegistration2Controller.GetDB2(GDB);

            ListAllAvailableBenefits GetAllAvailBenefits = new ListAllAvailableBenefits();
            try
            {
                Connection Connection = new Connection();
                List<AvailableBenefits> ListAllAvailBenefits = new List<AvailableBenefits>();
                DataSet ds = new DataSet();
                DataTable dtLoc = new DataTable();

                Connection.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = LocationNotif.EmpID });

                ds = Connection.GetDataset("sp_GetAvailableBenefits_Mobile");
                dtLoc = ds.Tables[0];
                if (dtLoc.Rows.Count > 0)
                {
                    foreach (DataRow row in dtLoc.Rows)
                    {
                        AvailableBenefits LocNotifRequest = new AvailableBenefits
                        {
                            BenefitType = row["BENEFITSTYPE"].ToString(),
                            BenefitID = row["BENEFITSID"].ToString(),
                            BenefitName = row["BENEFITSNAME"].ToString(),
                            BenefitsAmount = row["BENEFITSAMMOUNT"].ToString(),
                            MOP = row["MOP"].ToString()
                        };
                        ListAllAvailBenefits.Add(LocNotifRequest);
                    }
                    GetAllAvailBenefits.Benefits = ListAllAvailBenefits;
                    GetAllAvailBenefits.myreturn = "Success";
                }
                else
                {
                    GetAllAvailBenefits.myreturn = "No Data Available";
                }
            }
            catch (Exception ex)
            {
                GetAllAvailBenefits.myreturn = ex.Message;
            }
            return GetAllAvailBenefits;
        }
        [HttpPost]
        [Route("GetReimbursableReceiptBenefit")]
        public ListReimbursableReceiptBenefits GetReimbursableReceiptBenefit(LocationNotifv2 LocationNotif)
        {
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = LocationNotif.CN;
            SelfieRegistration2Controller.GetDB2(GDB);

            ListReimbursableReceiptBenefits GetAllReimbursable = new ListReimbursableReceiptBenefits();
            try
            {
                Connection Connection = new Connection();
                List<ReimbursableReceiptBenefits> ListAllReimbursable = new List<ReimbursableReceiptBenefits>();
                DataSet ds = new DataSet();
                DataTable dtLoc = new DataTable();

                ds = Connection.GetDataset("sp_GetReimbursableReciept_Benefit");
                dtLoc = ds.Tables[0];
                if (dtLoc.Rows.Count > 0)
                {
                    foreach (DataRow row in dtLoc.Rows)
                    {
                        ReimbursableReceiptBenefits LocNotifRequest = new ReimbursableReceiptBenefits
                        {
                            BenefitID = row["BenefitID"].ToString(),
                            BenefitName = row["BenefitName"].ToString()
                        };
                        ListAllReimbursable.Add(LocNotifRequest);
                    }
                    GetAllReimbursable.Reimbursable = ListAllReimbursable;
                    GetAllReimbursable.myreturn = "Success";
                }
                else
                {
                    GetAllReimbursable.myreturn = "No Data Available";
                }
            }
            catch (Exception ex)
            {
                GetAllReimbursable.myreturn = ex.Message;
            }
            return GetAllReimbursable;
        }
        [HttpPost]
        [Route("GetNotificationv2")]
        public ListAllLocationNotifResult GetLocationNotificationv2(LocationNotifv2 LocationNotif)
        {
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = LocationNotif.CN;
            SelfieRegistration2Controller.GetDB2(GDB);

            ListAllLocationNotifResult GetAllLocationNotif = new ListAllLocationNotifResult();
            try
            {
                Connection Connection = new Connection();
                List<LocationNotif> ListAllLocationNotif = new List<LocationNotif>();
                DataSet ds = new DataSet();
                DataTable dtLoc = new DataTable();

                Connection.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = LocationNotif.EmpID });

                ds = Connection.GetDataset("sp_Get_LocationNotif_Mobile");
                dtLoc = ds.Tables[0];
                if (dtLoc.Rows.Count > 0)
                {
                    foreach (DataRow row in dtLoc.Rows)
                    {
                        LocationNotif LocNotifRequest = new LocationNotif
                        {
                            ID = row["ID"].ToString(),
                            EmpID = row["EmpID"].ToString(),
                            IS_EmpID = row["IS_EmpID"].ToString(),
                            DateTime = row["DateTime"].ToString(),
                            NotifTitle = row["NotifTitle"].ToString(),
                            NotifDescription = row["NotifDescription"].ToString()

                        };
                        ListAllLocationNotif.Add(LocNotifRequest);
                    }
                    GetAllLocationNotif.LocationNotif = ListAllLocationNotif;
                    GetAllLocationNotif.myreturn = "Success";
                }
                else
                {
                    GetAllLocationNotif.myreturn = "No Data Available";
                }
            }
            catch (Exception ex)
            {
                GetAllLocationNotif.myreturn = ex.Message;
            }
            return GetAllLocationNotif;
        }

        [HttpPost]
        [Route("DeleteLocationNotifv2")]
        public string DeleteLocationNotifv2(LocationNotifv2 Notif)
        {
            try
            {
                GetDatabase GDB = new GetDatabase();
                GDB.ClientName = Notif.CN;
                SelfieRegistration2Controller.GetDB2(GDB);

                Connection Con = new Connection();
                Con.myparameters.Add(new myParameters { ParameterName = "@ID", mytype = SqlDbType.NVarChar, Value = Notif.ID });
                Con.ExecuteScalar("sp_Delete_LocationNotif_Mobile");
                return "success";
            }
            catch (Exception e)
            {
                return "failed" + e;
            }
        }

        [HttpPost]
        [Route("GetSafetyCheck")]
        public ListSafetyCheck listsafetycheck(SafetyCheck safetycheck)
        {
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = safetycheck.CN;
            SelfieRegistration2Controller.GetDB2(GDB);

            ListSafetyCheck listsafetycheck = new ListSafetyCheck();
            try
            {
                Connection Connection = new Connection();
                List<GetSafetyCheck> getsafetycheck = new List<GetSafetyCheck>();
                DataSet ds = new DataSet();
                DataTable dtsc = new DataTable();

                Connection.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.VarChar, Value = safetycheck.EmpID });

                ds = Connection.GetDataset("sp_GET_SafetyCheck");
                dtsc = ds.Tables[0];
                if (dtsc.Rows.Count > 0)
                {
                    foreach (DataRow row in dtsc.Rows)
                    {
                        GetSafetyCheck SafetyCheck = new GetSafetyCheck
                        {
                            scDID = row["scDID"].ToString(),
                            scName = row["scName"].ToString(),
                            scQMID = row["scQMID"].ToString(),
                            QuestionTitle = row["QuestionTitle"].ToString(),
                            scQDID = row["scQDID"].ToString(),
                            QuestionDescription = row["QuestionDescription"].ToString(),
                            sDate = row["StartDate"].ToString(),
                            eDate = row["EndDate"].ToString(),
                            sTime = row["StartTime"].ToString(),
                            eTime = row["EndTime"].ToString()

                        };
                        getsafetycheck.Add(SafetyCheck);
                    }
                    listsafetycheck.getSafetyCheck = getsafetycheck;
                    listsafetycheck.myreturn = "Success";
                }
                else
                {
                    listsafetycheck.myreturn = "No Data Available";
                }
            }
            catch (Exception ex)
            {
                listsafetycheck.myreturn = ex.Message;
            }
            return listsafetycheck;
        }

        [HttpPost]
        [Route("InsertSafetyCheckAnswer")]
        public string insertSafetyCheckAnswer(insertSafetyCheckAnswer ins)
        {
            try
            {
                GetDatabase GDB = new GetDatabase();
                GDB.ClientName = ins.CN;
                SelfieRegistration2Controller.GetDB2(GDB);

                Connection Con = new Connection();
                Con.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.NVarChar, Value = ins.EmpID });
                Con.myparameters.Add(new myParameters { ParameterName = "@scQMID", mytype = SqlDbType.NVarChar, Value = ins.scQMID });
                Con.myparameters.Add(new myParameters { ParameterName = "@scQDID", mytype = SqlDbType.NVarChar, Value = ins.scQDID });
                Con.ExecuteScalar("sp_INSERT_SafetyCheckAnswer");
                return "success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }

        //---------------------------------------------------------------Revised Change DB Start------------------------------------------------------------------------

    }
}
