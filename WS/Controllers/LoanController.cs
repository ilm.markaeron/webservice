﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using illimitadoWepAPI.Models;
using System.Xml.Serialization;
using System.IO;
using System.IO.Compression;
using System.Data;
using System.Data.SqlClient;
using illimitadoWepAPI.MyClass;
using static illimitadoWepAPI.Models.Loan;
using iTextSharp.text.pdf;
using System.Web;
using illimitadoWepAPI.App_Start;
using System.Globalization;
using iTextSharp.text.pdf.parser;
using System.Text;
using iTextSharp.text;
using Aspose.Pdf.Text;
using System.Net.Mail;
using System.Net.Mime;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace illimitadoWepAPI.Controllers
{
    public class LoanController : ApiController
    {

        // MethodAndVariables MAV = new MethodAndVariables();
        // static clsUserType CREDITS = new clsUserType();
        //  static DisputeUser DSS = new DisputeUser();
        //  MethodAndVariables mav = new MethodAndVariables();
        //  ProfileModule PR = new ProfileModule();
        PayslipMethod PayMtd = new PayslipMethod();
        DataTable RetriveEmployeeMaster;
        // DisputeUser DS = new DisputeUser();
        public DataTable GetEmpLevelandCategory;
        public string manageremail;
        DataTable YearToDate;
        DataTable LeaveHoursAmount;
        DataTable LeaveHoursAmountYearToDate;
        DataTable RetrieveReason;
        public DataTable paysliprecordsempmaster;
        public DataTable paysliprecordsconfidentialinfo;
        public DataTable PayslipItems;
        public DataTable paysliprecordsemployeepay;
        public DataTable paysliprecordspayslipdetails;
        public DataTable payslipworkedhours;
        public DataTable paysliprecordspayperiod;
        public DataTable paysliprecordsallowanceamount;
        public DataTable paysliprecordsbonuses;
        public DataTable paysliprecordscompbenempcatbonuses;
        public DataTable LatestPayslipForm;
        public DataTable paysliprecordsdisputes;
        public string Text { get; private set; }
        string ReqSent = "alertify.success(\"Successful Request Sent\");";
        string ReqEmail = "alertify.error(\"Required Email\");";
        public int indx = 0;
        string hoursdatefrom;
        string hoursdateto;
        string finalsalary = "0.00";
        public double totalbonus = 0.00;
        double totalsalary = 0.00;
        double DeminimisAmount = 0.00;
        double FringeAmount = 0.00;
        double SSSLoanDeduction = 0.00;
        double HDMFLoanDeduction = 0.00;
        double OtherBenefits = 0.00;
        double PayoutMonths = 0.00;
        double Disputes = 0.00;
        string payperiod;
        static string empid;
        static string empidprof;
        static string empname;
        static int TotalValue = 0;

        [HttpPost]
        [Route("ShowLoans")]
        public GetLoanResult ShowLoans()
        {
            GetLoanResult GetLoanResult = new GetLoanResult();
            try
            {
                Connection Connection = new Connection();
                List<Loan> ListLoan = new List<Loan>();
                DataSet ds = new DataSet();
                DataTable dtLEAVES = new DataTable();
                ds = Connection.GetDataset("sp_ShowLoans_Mobile");
                dtLEAVES = ds.Tables[0];
                if (dtLEAVES.Rows.Count > 0)
                {
                    foreach (DataRow row in dtLEAVES.Rows)
                    {
                        Loan Loan = new Loan
                        {
                            LoanAmount = row["Loan_Amount"].ToString(),
                            LoanTerm = row["Loan_Term"].ToString(),

                        };
                        ListLoan.Add(Loan);
                    }
                    GetLoanResult.Loan = ListLoan;
                    GetLoanResult.myreturn = "Success";
                }
                else
                {
                    GetLoanResult.myreturn = "No Data Available";
                }
            }
            catch (Exception ex)
            {
                GetLoanResult.myreturn = ex.Message;
            }
            return GetLoanResult;
        }

        void BonusPayoutMonth(string empid, string startdate, string enddate)
        {
            double BonusMonth = 0.00;
            BonusMonth = PayMtd.BonusPayoutMonth(empid, startdate, enddate);
            totalbonus = totalbonus + BonusMonth;
            totalsalary = totalsalary + BonusMonth;
            finalsalary = (Convert.ToDouble(finalsalary) + BonusMonth).ToString();
            PayoutMonths = PayoutMonths + BonusMonth;
        }

        void CompBenDeminimis(string empid, string empcatid, string startdate, string enddate)
        {
            //DataTable Deminimis = new DataTable();
            //Deminimis = PayMtd.getPayslipCompBenDeminimis(empcatid, empid);
            //for (int i = 0; i < Deminimis.Rows.Count; i++)
            //{
            //    totalbonus = totalbonus + Convert.ToDouble(Deminimis.Rows[i][0].ToString());
            //    totalsalary = totalsalary + Convert.ToDouble(Deminimis.Rows[i][0].ToString());
            //}
            DeminimisAmount = PayMtd.DeminimisAmount(empid, startdate, enddate);
            //totalbonus = totalbonus + DeminimisAmount;
            totalsalary = totalsalary + DeminimisAmount;
            finalsalary = (Convert.ToDouble(finalsalary) + DeminimisAmount).ToString();
        }

        void PayrollDisputes(string empid, string datefrom, string dateto)
        {
            paysliprecordsdisputes = PayMtd.DisputesPayrollAdjustments(empid, datefrom, dateto);
            for (int p = 0; p < paysliprecordsdisputes.Rows.Count; p++)
            {
                if (paysliprecordsdisputes.Rows[p][1] != DBNull.Value || paysliprecordsdisputes.Rows[p][2] != DBNull.Value)
                {
                    if (paysliprecordsdisputes.Rows[p][1].ToString() != "" && paysliprecordsdisputes.Rows[p][2].ToString() == "")
                    {
                        totalsalary = totalsalary + Convert.ToDouble(paysliprecordsdisputes.Rows[p][1].ToString());
                        finalsalary = (Convert.ToDouble(finalsalary) + Convert.ToDouble(paysliprecordsdisputes.Rows[p][1].ToString())).ToString();
                        Disputes = Disputes + Convert.ToDouble(paysliprecordsdisputes.Rows[p][1].ToString());
                    }
                    else if (paysliprecordsdisputes.Rows[p][1].ToString() == "" && paysliprecordsdisputes.Rows[p][2].ToString() != "")
                    {
                        totalsalary = totalsalary - Convert.ToDouble(paysliprecordsdisputes.Rows[p][2].ToString());
                        finalsalary = (Convert.ToDouble(finalsalary) - Convert.ToDouble(paysliprecordsdisputes.Rows[p][2].ToString())).ToString();
                        Disputes = Disputes - Convert.ToDouble(paysliprecordsdisputes.Rows[p][2].ToString());
                    }
                    else if (paysliprecordsdisputes.Rows[p][1].ToString() != "" && paysliprecordsdisputes.Rows[p][2].ToString() != "")
                    {
                        totalsalary = totalsalary + Convert.ToDouble(paysliprecordsdisputes.Rows[p][1].ToString());
                        finalsalary = (Convert.ToDouble(finalsalary) + Convert.ToDouble(paysliprecordsdisputes.Rows[p][1].ToString())).ToString();
                        Disputes = Disputes + Convert.ToDouble(paysliprecordsdisputes.Rows[p][1].ToString());
                        totalsalary = totalsalary - Convert.ToDouble(paysliprecordsdisputes.Rows[p][2].ToString());
                        finalsalary = (Convert.ToDouble(finalsalary) - Convert.ToDouble(paysliprecordsdisputes.Rows[p][2].ToString())).ToString();
                        Disputes = Disputes - Convert.ToDouble(paysliprecordsdisputes.Rows[p][2].ToString());
                    }
                }
                else
                {

                }
            }
        }

        void SSSLoans(string empid, string datefrom, string dateto)
        {
            SSSLoanDeduction = PayMtd.SSSLoanDeduction(empid, datefrom, dateto);
            totalsalary = totalsalary - SSSLoanDeduction;
            finalsalary = (Convert.ToDouble(finalsalary) - SSSLoanDeduction).ToString();
        }

        void PayrollAdjustments(string empid, string payperiod)
        {
            //List<string> Adjust = new List<string>();
            //Adjust.Add("Basic Salary");
            //Adjust.Add("Regular Overtime");
            //Adjust.Add("Regular Night Diff");
            //foreach (string item in Adjust)
            //{
            //    double adjustmentamount = 0.00;
            //    adjustmentamount = PayMtd.PayrollAdjustments(empid, payperiod, item);
            //    totalsalary = totalsalary + adjustmentamount;
            //    finalsalary = (Convert.ToDouble(finalsalary) + adjustmentamount).ToString();
            //}
            double adjustmentamount = 0.00;
            adjustmentamount = PayMtd.PayrollAdjustments(empid, payperiod, "");
            Disputes = Disputes + adjustmentamount;
            totalsalary = totalsalary + adjustmentamount;
            finalsalary = (Convert.ToDouble(finalsalary) + adjustmentamount).ToString();
        }



        [HttpPost]
        [Route("GetPayslips")]
        //{
        //"NTID": "i101",
        //"Start_Date": "2017-04-01",
        //"End_Date": "2017-04-30"
        //}
        public payslip GetPayslip()
        {
            payslip GetPayslipResult = new payslip();
            //try
            //{
            Connection Connection = new Connection();
            DataTable dtPAYSLIP = new DataTable();
            dtPAYSLIP = Connection.GetDataTable("sp_GetActivePayslip_Mobile");
            if (dtPAYSLIP.Rows.Count > 0)
            {

                foreach (DataRow row in dtPAYSLIP.Rows)
                {
                    GetPayslipResult.paysliplink = "https://ws.durusthr.com/ILM_WS/pdf" + row["PDF"].ToString() + "-";
                    // GetPayslipResult.paysliplink = "http://192.168.1.250:81/pdf/" + row["PDF"].ToString() + "-";
                }
            }
            else
            {
                GetPayslipResult.paysliplink = "https://ws.durusthr.com/ILM_WS/pdf/PaySlip-";
                // GetPayslipResult.paysliplink = "http://192.168.1.250:81/pdf/PaySlip-";
            }

            return GetPayslipResult;
        }




        [HttpPost]
        [Route("GetPayslip")]
        public string Payslip(UserProfile uProfile)
        {
            #region delete

            //empname = PayMtd.searchempname(uProfile.NTID);


            //empidprof = uProfile.NTID;
            //GetEmpLevelandCategory = PayMtd.GetEmpLevelandCategory(uProfile.NTID);
            //string EmpLevel = GetEmpLevelandCategory.Rows[0][0].ToString();
            //string EmpCategory = GetEmpLevelandCategory.Rows[0][1].ToString();
            //paysliprecordsempmaster = PayMtd.PaySlipRecordsEmpMaster(uProfile.NTID);
            //paysliprecordsconfidentialinfo = PayMtd.PaySlipRecordsConfidentialInfo(uProfile.NTID);
            //paysliprecordsemployeepay = PayMtd.PaySlipRecordsEmployeePay(uProfile.NTID);

            //empid = uProfile.NTID;
            //int latestpayslipcounter = 0;
            //LatestPayslipForm = PayMtd.LatestPayslipForm();
            //latestpayslipcounter = LatestPayslipForm.Rows.Count;
            //if (latestpayslipcounter == 1)
            //{
            //    #region Dynamic payslip
            //    #region Retrieve Neccessary Data
            //    // indx = DDLPayPeriod.SelectedIndex;
            //    GetEmpLevelandCategory = PayMtd.GetEmpLevelandCategory(empid);
            //    EmpLevel = GetEmpLevelandCategory.Rows[0][0].ToString();
            //    EmpCategory = GetEmpLevelandCategory.Rows[0][1].ToString();
            //    int psrpsd = 0;
            //    int pswh = 0;
            //    paysliprecordspayperiod = PayMtd.PaySlipRecordsPayPeriod(EmpLevel, EmpCategory);
            //    #region MyRegion
            //    if (paysliprecordspayperiod.Rows.Count > 0)
            //    {
            //        string ddlmonth = uProfile.PayMonth.ToString();
            //        string ddlyear = uProfile.PayYear.ToString();
            //        string stringtobesplitted = uProfile.PayDay.ToString();
            //        string[] words = stringtobesplitted.Split('-');
            //        string firstpayperiodstart = words[0].ToString();
            //        string firstpayperiodend;
            //        if (words[1].ToString() == "End of month")
            //        {
            //            int a, b;
            //            a = Convert.ToInt32(ddlmonth);
            //            b = Convert.ToInt32(ddlyear);
            //            int x = DateTime.DaysInMonth(b, a);
            //            firstpayperiodend = x.ToString();
            //        }
            //        else
            //        {
            //            firstpayperiodend = words[1].ToString();
            //        }

            //        //string firstpayperiodstart = paysliprecordspayperiod.Rows[indx][0].ToString();
            //        //string firstpayperiodend = paysliprecordspayperiod.Rows[indx][1].ToString();
            //        string bonusdayfrom = firstpayperiodstart;
            //        string bonusdayto = firstpayperiodend;

            //        string datefrom = ddlyear + '-' + ddlmonth + '-' + firstpayperiodstart;
            //        string dateto = ddlyear + '-' + ddlmonth + '-' + firstpayperiodend;

            //        if (firstpayperiodend == "End of month")
            //        {
            //            int x = DateTime.DaysInMonth(Convert.ToInt32(ddlyear), Convert.ToInt32(ddlmonth));
            //            firstpayperiodend = x.ToString();
            //        }

            //        if (firstpayperiodstart == "1" || firstpayperiodstart == "2" || firstpayperiodstart == "3" || firstpayperiodstart == "4" || firstpayperiodstart == "5"
            //            || firstpayperiodstart == "6" || firstpayperiodstart == "7" || firstpayperiodstart == "8" || firstpayperiodstart == "9")
            //        {
            //            hoursdatefrom = ddlyear + '-' + ddlmonth + '-' + ("0" + firstpayperiodstart);
            //        }
            //        else
            //        {
            //            hoursdatefrom = ddlyear + '-' + ddlmonth + '-' + firstpayperiodstart;
            //        }
            //        if (firstpayperiodend == "1" || firstpayperiodend == "2" || firstpayperiodend == "3" || firstpayperiodend == "4" || firstpayperiodend == "5"
            //            || firstpayperiodend == "6" || firstpayperiodend == "7" || firstpayperiodend == "8" || firstpayperiodend == "9")
            //        {
            //            hoursdateto = ddlyear + '-' + ddlmonth + '-' + ("0" + firstpayperiodend);
            //        }
            //        else if (firstpayperiodend == "31")
            //        {
            //            if (ddlmonth == "02" || ddlmonth == "04" || ddlmonth == "06" || ddlmonth == "09" || ddlmonth == "11")
            //            {
            //                int x = DateTime.DaysInMonth(Convert.ToInt32(ddlyear), Convert.ToInt32(ddlmonth));
            //                hoursdateto = ddlyear + '-' + ddlmonth + '-' + x;
            //            }
            //            else
            //            {
            //                hoursdateto = ddlyear + '-' + ddlmonth + '-' + firstpayperiodend;
            //            }
            //        }
            //        else
            //        {
            //            hoursdateto = ddlyear + '-' + ddlmonth + '-' + firstpayperiodend;
            //        }

            //        //HelloWorld
            //        //PayMtd.SecretMethodForPayslip("2017-08-22");
            //        //PayMtd.RunPayslipScript(hoursdatefrom, hoursdateto,"2017-08-22");

            //        payslipworkedhours = PayMtd.PaySlipWorkedHours(empid, hoursdatefrom, hoursdateto);
            //        pswh = payslipworkedhours.Rows.Count;
            //        //payperiod = datefrom + '/' + dateto;
            //        payperiod = hoursdatefrom + '-' + hoursdateto;
            //        paysliprecordspayslipdetails = PayMtd.PaySlipRecordsPaySlipDetails(empid, payperiod);
            //        psrpsd = paysliprecordspayslipdetails.Rows.Count;
            //        paysliprecordsallowanceamount = PayMtd.getPayslipRecordsAllowanceAmount(empid);
            //        paysliprecordsbonuses = PayMtd.getPayslipRecordsBonuses(empid, datefrom + "/" + dateto);
            //        paysliprecordscompbenempcatbonuses = PayMtd.getPayslipCompBenEmpCatBonuses(EmpLevel, EmpCategory, hoursdatefrom, hoursdateto, bonusdayfrom, bonusdayto);
            //        //BonusPayoutCompute(EmpLevel, EmpCategory, empid,hoursdatefrom,hoursdateto);
            //        BonusPayoutMonth(empid, hoursdatefrom, hoursdateto);
            //        CompBenDeminimis(empid, EmpCategory, hoursdatefrom, hoursdateto);
            //        //PayrollDisputes(empid, hoursdatefrom, hoursdateto);
            //        SSSLoans(empid, hoursdatefrom, hoursdateto);
            //        PayrollAdjustments(empid, hoursdatefrom + '-' + hoursdateto);
            //    }
            //    #endregion
            //    #region Comments
            //    //if (paysliprecordspayperiod.Rows.Count > 0)
            //    //{


            //    //    string ddlmonth = DDLMonths.SelectedValue.ToString();
            //    //    string ddlyear = DDLYear.SelectedValue.ToString();
            //    //    string firstpayperiodstart = paysliprecordspayperiod.Rows[indx][0].ToString();
            //    //    string firstpayperiodend = paysliprecordspayperiod.Rows[indx][1].ToString();
            //    //    if (firstpayperiodend == "End of month")
            //    //    {
            //    //        int x = DateTime.DaysInMonth(Convert.ToInt32(ddlyear), Convert.ToInt32(ddlmonth));
            //    //        firstpayperiodend = x.ToString();
            //    //    }
            //    //    string bonusdayfrom = firstpayperiodstart;
            //    //    string bonusdayto = firstpayperiodend;
            //    //    string datefrom = ddlyear + '-' + ddlmonth + '-' + firstpayperiodstart;
            //    //    string dateto = ddlyear + '-' + ddlmonth + '-' + firstpayperiodend;
            //    //    string hoursdatefrom;
            //    //    string hoursdateto;
            //    //    if (firstpayperiodstart == "1" || firstpayperiodstart == "2" || firstpayperiodstart == "3" || firstpayperiodstart == "4" || firstpayperiodstart == "5"
            //    //        || firstpayperiodstart == "6" || firstpayperiodstart == "7" || firstpayperiodstart == "8" || firstpayperiodstart == "9")
            //    //    {
            //    //        hoursdatefrom = ddlyear + '-' + ddlmonth + '-' + ("0" + firstpayperiodstart);
            //    //    }
            //    //    else
            //    //    {
            //    //        hoursdatefrom = ddlyear + '-' + ddlmonth + '-' + firstpayperiodstart;
            //    //    }
            //    //    if (firstpayperiodend == "1" || firstpayperiodend == "2" || firstpayperiodend == "3" || firstpayperiodend == "4" || firstpayperiodend == "5"
            //    //        || firstpayperiodend == "6" || firstpayperiodend == "7" || firstpayperiodend == "8" || firstpayperiodend == "9")
            //    //    {
            //    //        hoursdateto = ddlyear + '-' + ddlmonth + '-' + ("0" + firstpayperiodend);
            //    //    }
            //    //    else if (firstpayperiodend == "31")
            //    //    {
            //    //        if (ddlmonth == "02" || ddlmonth == "04" || ddlmonth == "06" || ddlmonth == "09" || ddlmonth == "11")
            //    //        {
            //    //            int x = DateTime.DaysInMonth(Convert.ToInt32(ddlyear), Convert.ToInt32(ddlmonth));
            //    //            hoursdateto = ddlyear + '-' + ddlmonth + '-' + x;
            //    //        }
            //    //        else
            //    //        {
            //    //            hoursdateto = ddlyear + '-' + ddlmonth + '-' + firstpayperiodend;
            //    //        }
            //    //    }
            //    //    else
            //    //    {
            //    //        hoursdateto = ddlyear + '-' + ddlmonth + '-' + firstpayperiodend;
            //    //    }
            //    //    payslipworkedhours = PayMtd.PaySlipWorkedHours(empid, hoursdatefrom, hoursdateto);
            //    //    pswh = payslipworkedhours.Rows.Count;
            //    //     payperiod = datefrom + '-' + dateto;
            //    //    paysliprecordspayslipdetails = PayMtd.PaySlipRecordsPaySlipDetails(empid, payperiod);
            //    //    psrpsd = paysliprecordspayslipdetails.Rows.Count;
            //    //    paysliprecordsallowanceamount = PayMtd.getPayslipRecordsAllowanceAmount(empid);
            //    //    paysliprecordsbonuses = PayMtd.getPayslipRecordsBonuses(empid, datefrom + "/" + dateto);
            //    //    paysliprecordscompbenempcatbonuses = PayMtd.getPayslipCompBenEmpCatBonuses(EmpLevel, EmpCategory, hoursdatefrom, hoursdateto, bonusdayfrom, bonusdayto);
            //    //    BonusPayoutCompute(EmpLevel, EmpCategory, empid, hoursdatefrom, hoursdateto);
            //    //    CompBenDeminimis(empid, EmpCategory);
            //    //}
            //    #endregion
            //    #endregion




            //    string pathmoto = LatestPayslipForm.Rows[0][0].ToString();
            //    // PdfReader reader1 = new PdfReader("http://192.168.1.250:81/pdf/" + LatestPayslipForm.Rows[0][0].ToString() + ".pdf");
            //    PdfReader reader1 = new PdfReader("http://illimitadodevs.azurewebsites.net/pdf/" + LatestPayslipForm.Rows[0][0].ToString() + ".pdf");
            //    LocationTextExtractionStrategy lteStrategy = new LocationTextExtractionStrategy();
            //    string strExact = PdfTextExtractor.GetTextFromPage(reader1, 1, lteStrategy);
            //    StringBuilder sb = new StringBuilder();
            //    //this code used for replacing the found text
            //    sb.Append(PdfTextExtractor.GetTextFromPage(reader1, 1));
            //    #region Passing Retrieved Datas
            //    sb.Replace("[Employee ID]", empid);
            //    if (paysliprecordsempmaster.Rows.Count > 0)
            //    {
            //        if (paysliprecordsempmaster.Rows[0][0] != DBNull.Value)
            //        {
            //            sb.Replace("[Employee Name]", paysliprecordsempmaster.Rows[0][0].ToString());
            //        }
            //        else
            //        {
            //            sb.Replace("[Employee Name]", "N/A");
            //        }
            //        if (paysliprecordsempmaster.Rows[0][2] != DBNull.Value)
            //        {
            //            sb.Replace("[Date Joined]", paysliprecordsempmaster.Rows[0][2].ToString());
            //        }
            //        else
            //        {
            //            sb.Replace("[Date Joined]", "N/A");
            //        }
            //        if (paysliprecordsempmaster.Rows[0][3] != DBNull.Value)
            //        {
            //            sb.Replace("[Resigned]", paysliprecordsempmaster.Rows[0][3].ToString());
            //        }
            //        else
            //        {
            //            sb.Replace("[Resigned]", "N/A");
            //        }

            //    }
            //    else
            //    {
            //        sb.Replace("[Employee Name]", "N/A");
            //        sb.Replace("[Date Joined]", "N/A");
            //        sb.Replace("[Resigned]", "N/A");
            //    }
            //    if (paysliprecordsconfidentialinfo.Rows.Count > 0)
            //    {
            //        if (paysliprecordsconfidentialinfo.Rows[0][0] != DBNull.Value)
            //        {
            //            sb.Replace("[HDMF]", paysliprecordsconfidentialinfo.Rows[0][0].ToString());
            //        }
            //        else
            //        {
            //            sb.Replace("[HDMF]", "N/A");
            //        }
            //        if (paysliprecordsconfidentialinfo.Rows[0][1] != DBNull.Value)
            //        {
            //            sb.Replace("[PhilHealth No]", paysliprecordsconfidentialinfo.Rows[0][1].ToString());
            //        }
            //        else
            //        {
            //            sb.Replace("[PhilHealth No]", "N/A");
            //        }
            //        if (paysliprecordsconfidentialinfo.Rows[0][2] != DBNull.Value)
            //        {
            //            sb.Replace("[SSS No]", paysliprecordsconfidentialinfo.Rows[0][2].ToString());
            //        }
            //        else
            //        {
            //            sb.Replace("[SSS No]", "N/A");
            //        }
            //        if (paysliprecordsconfidentialinfo.Rows[0][3] != DBNull.Value)
            //        {
            //            sb.Replace("[TIN]", paysliprecordsconfidentialinfo.Rows[0][3].ToString());
            //        }
            //        else
            //        {
            //            sb.Replace("[TIN]", "N/A");
            //        }
            //        if (paysliprecordsconfidentialinfo.Rows[0][4] != DBNull.Value)
            //        {
            //            sb.Replace("[Tax Category]", paysliprecordsconfidentialinfo.Rows[0][4].ToString());
            //        }
            //        else
            //        {
            //            sb.Replace("[Tax Category]", "N/A");
            //        }
            //    }
            //    else
            //    {
            //        sb.Replace("[HDMF]", "N/A");
            //        sb.Replace("[PhilHealth No]", "N/A");
            //        sb.Replace("[SSS No]", "N/A");
            //        sb.Replace("[TIN]", "N/A");
            //        sb.Replace("[Tax Category]", "N/A");
            //    }
            //    if (paysliprecordsemployeepay.Rows.Count > 0)
            //    {
            //        if (paysliprecordsemployeepay.Rows[0][0] != DBNull.Value)
            //        {
            //            sb.Replace("[Bank Account]", paysliprecordsemployeepay.Rows[0][0].ToString());
            //        }
            //        else
            //        {
            //            sb.Replace("[Bank Account]", "N/A");
            //        }
            //        if (paysliprecordsemployeepay.Rows[0][1] != DBNull.Value)
            //        {
            //            sb.Replace("[Hourly Rate]", paysliprecordsemployeepay.Rows[0][1].ToString());
            //        }
            //        else
            //        {
            //            sb.Replace("[Hourly Rate]", "N/A");
            //        }
            //    }
            //    else
            //    {
            //        sb.Replace("[Bank Account]", "N/A");
            //        sb.Replace("[Hourly Rate]", "N/A");
            //    }
            //    sb.Replace("[Pay Period]", payperiod);
            //    if (psrpsd > 0)
            //    {
            //        //double totalsalary = 0.00;
            //        if (paysliprecordspayslipdetails.Rows[0][1] != DBNull.Value)
            //        {
            //            sb.Replace("[Pay Day]", paysliprecordspayslipdetails.Rows[0][1].ToString());
            //        }
            //        else
            //        {
            //            sb.Replace("[Pay Day]", "N/A");
            //        }
            //        if (paysliprecordspayslipdetails.Rows[0][2] != DBNull.Value)
            //        {
            //            sb.Replace("[Regular Night Differential]", paysliprecordspayslipdetails.Rows[0][2].ToString());
            //            totalsalary = totalsalary + Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][2].ToString());
            //        }
            //        else
            //        {
            //            sb.Replace("[Regular Night Differential]", "N/A");
            //        }
            //        if (paysliprecordspayslipdetails.Rows[0][3] != DBNull.Value)
            //        {
            //            sb.Replace("[Regular Holiday]", paysliprecordspayslipdetails.Rows[0][3].ToString());
            //            totalsalary = totalsalary + Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][3].ToString());
            //        }
            //        else
            //        {
            //            sb.Replace("[Regular Holiday]", "N/A");
            //        }
            //        if (paysliprecordspayslipdetails.Rows[0][4] != DBNull.Value)
            //        {
            //            sb.Replace("[Regular Holiday Night Differential]", paysliprecordspayslipdetails.Rows[0][4].ToString());
            //            totalsalary = totalsalary + Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][4].ToString());
            //        }
            //        else
            //        {
            //            sb.Replace("[Regular Holiday Night Differential]", "N/A");
            //        }
            //        //--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--
            //        //--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--
            //        //--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--
            //        sb.Replace("[Paid Time Off]", paysliprecordspayslipdetails.Rows[0][5].ToString());
            //        //--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--
            //        //--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--
            //        //--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--
            //        if (paysliprecordspayslipdetails.Rows[0][6] != DBNull.Value)
            //        {
            //            sb.Replace("[Regular Over Time]", paysliprecordspayslipdetails.Rows[0][6].ToString());
            //            totalsalary = totalsalary + Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][6].ToString());
            //        }
            //        else
            //        {
            //            sb.Replace("[Regular Over Time]", "N/A");
            //        }
            //        if (paysliprecordspayslipdetails.Rows[0][7] != DBNull.Value)
            //        {
            //            sb.Replace("[Regular Holiday Over Time]", paysliprecordspayslipdetails.Rows[0][7].ToString());
            //            totalsalary = totalsalary + Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][7].ToString());
            //        }
            //        else
            //        {
            //            sb.Replace("[Regular Holiday Over Time]", "N/A");
            //        }
            //        if (paysliprecordspayslipdetails.Rows[0][8] != DBNull.Value)
            //        {
            //            sb.Replace("[Special Holiday]", paysliprecordspayslipdetails.Rows[0][8].ToString());
            //            totalsalary = totalsalary + Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][8].ToString());
            //        }
            //        else
            //        {
            //            sb.Replace("[Special Holiday]", "N/A");
            //        }
            //        if (paysliprecordspayslipdetails.Rows[0][9] != DBNull.Value)
            //        {
            //            sb.Replace("[Special Holiday Over Time]", paysliprecordspayslipdetails.Rows[0][9].ToString());
            //            totalsalary = totalsalary + Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][9].ToString());
            //        }
            //        else
            //        {
            //            sb.Replace("[Special Holiday Over Time]", "N/A");
            //        }
            //        if (paysliprecordspayslipdetails.Rows[0][10] != DBNull.Value)
            //        {
            //            sb.Replace("[Special Holiday Night Differential]", paysliprecordspayslipdetails.Rows[0][10].ToString());
            //            totalsalary = totalsalary + Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][10].ToString());
            //        }
            //        else
            //        {
            //            sb.Replace("[Special Holiday Night Differential]", "N/A");
            //        }
            //        if (paysliprecordspayslipdetails.Rows[0][11] != DBNull.Value)
            //        {
            //            sb.Replace("[Employee HDMF]", paysliprecordspayslipdetails.Rows[0][11].ToString());
            //        }
            //        else
            //        {
            //            sb.Replace("[Employee HDMF]", "N/A");
            //        }
            //        if (paysliprecordspayslipdetails.Rows[0][12] != DBNull.Value)
            //        {
            //            sb.Replace("[Employee Philhealth]", paysliprecordspayslipdetails.Rows[0][12].ToString());
            //        }
            //        else
            //        {
            //            sb.Replace("[Employee Philhealth]", "N/A");
            //        }
            //        if (paysliprecordspayslipdetails.Rows[0][13] != DBNull.Value)
            //        {
            //            sb.Replace("[Employee SSS]", paysliprecordspayslipdetails.Rows[0][13].ToString());
            //        }
            //        else
            //        {
            //            sb.Replace("[Employee SSS]", "N/A");
            //        }
            //        if (paysliprecordspayslipdetails.Rows[0][14] != DBNull.Value)
            //        {
            //            sb.Replace("[Employee WithHoldingTax]", paysliprecordspayslipdetails.Rows[0][14].ToString());
            //        }
            //        else
            //        {
            //            sb.Replace("[Employee WithHoldingTax]", "N/A");
            //        }
            //        if (paysliprecordspayslipdetails.Rows[0][15] != DBNull.Value)
            //        {
            //            sb.Replace("[Employer HDMF]", paysliprecordspayslipdetails.Rows[0][15].ToString());
            //        }
            //        else
            //        {
            //            sb.Replace("[Employer HDMF]", "N/A");
            //        }
            //        if (paysliprecordspayslipdetails.Rows[0][16] != DBNull.Value)
            //        {
            //            sb.Replace("[Employer Philhealth]", paysliprecordspayslipdetails.Rows[0][16].ToString());
            //        }
            //        else
            //        {
            //            sb.Replace("[Employer Philhealth]", "N/A");
            //        }
            //        if (paysliprecordspayslipdetails.Rows[0][17] != DBNull.Value)
            //        {
            //            sb.Replace("[Employer SSS]", paysliprecordspayslipdetails.Rows[0][17].ToString());
            //        }
            //        else
            //        {
            //            sb.Replace("[Employer SSS]", "N/A");
            //        }
            //        if (paysliprecordspayslipdetails.Rows[0][18] != DBNull.Value)
            //        {
            //            sb.Replace("[Employer SSS EC]", paysliprecordspayslipdetails.Rows[0][18].ToString());
            //        }
            //        else
            //        {
            //            sb.Replace("[Employer SSS EC]", "N/A");
            //        }
            //        //--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--
            //        //--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--
            //        //--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--
            //        sb.Replace("[Net Pay]", paysliprecordspayslipdetails.Rows[0][19].ToString());
            //        //--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--
            //        //--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--
            //        //--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--
            //        if (paysliprecordspayslipdetails.Rows[0][21] != DBNull.Value)
            //        {
            //            sb.Replace("[Non Taxable Allowance]", (Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][21].ToString()) + DeminimisAmount).ToString());
            //        }
            //        else
            //        {
            //            sb.Replace("[Non Taxable Allowance]", "N/A");
            //        }
            //        if (paysliprecordspayslipdetails.Rows[0][22] != DBNull.Value)
            //        {
            //            sb.Replace("[Basic Salary]", paysliprecordspayslipdetails.Rows[0][22].ToString());
            //            totalsalary = totalsalary + Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][22].ToString());
            //        }
            //        else
            //        {
            //            sb.Replace("[Basic Salary]", "N/A");
            //        }
            //    }
            //    else
            //    {
            //        sb.Replace("[Basic Salary]", "N/A");
            //        sb.Replace("[Pay Day]", "N/A");
            //        sb.Replace("[Regular Night Differential]", "N/A");
            //        sb.Replace("[Regular Holiday]", "N/A");
            //        sb.Replace("[Regular Holiday Night Differential]", "N/A");
            //        sb.Replace("[Regular Over Time]", "N/A");
            //        sb.Replace("[Regular Holiday Over Time]", "N/A");
            //        sb.Replace("[Special Holiday]", "N/A");
            //        sb.Replace("[Special Holiday Over Time]", "N/A");
            //        sb.Replace("[Special Holiday Night Differential]", "N/A");
            //        sb.Replace("[Employee HDMF]", "N/A");
            //        sb.Replace("[Employee Philhealth]", "N/A");
            //        sb.Replace("[Employee SSS]", "N/A");
            //        sb.Replace("[Employee WithHoldingTax]", "N/A");
            //        sb.Replace("[Employer HDMF]", "N/A");
            //        sb.Replace("[Employer Philhealth]", "N/A");
            //        sb.Replace("[Employer SSS]", "N/A");
            //        sb.Replace("[Employer SSS EC]", "N/A");
            //        sb.Replace("[Non Taxable Allowance]", "N/A");
            //        sb.Replace("[Net Pay]", "N/A");
            //    }


            //    #region Disputes
            //    //for (int p = 0; p < paysliprecordsdisputes.Rows.Count; p++)
            //    //{
            //    //    if (paysliprecordsdisputes.Rows[p][1] != DBNull.Value || paysliprecordsdisputes.Rows[p][2] != DBNull.Value)
            //    //    {
            //    //        if (paysliprecordsdisputes.Rows[p][1].ToString() != "" && paysliprecordsdisputes.Rows[p][2].ToString() == "")
            //    //        {
            //    //            totalsalary = totalsalary + Convert.ToDouble(paysliprecordsdisputes.Rows[p][1].ToString());
            //    //        }
            //    //        else if (paysliprecordsdisputes.Rows[p][1].ToString() == "" && paysliprecordsdisputes.Rows[p][2].ToString() != "")
            //    //        {
            //    //            totalsalary = totalsalary - Convert.ToDouble(paysliprecordsdisputes.Rows[p][2].ToString());
            //    //        }
            //    //    }
            //    //    else
            //    //    {

            //    //    }
            //    //}
            //    #endregion


            //    sb.Replace("[Total Salary]", totalsalary.ToString());

            //    if (pswh > 0)
            //    {
            //        double hourandamount;

            //        if (payslipworkedhours.Rows[0][1] != DBNull.Value)
            //        {
            //            sb.Replace("[Basic Salary Hours]", payslipworkedhours.Rows[0][1].ToString());
            //        }
            //        else
            //        {
            //            sb.Replace("[Basic Salary Hours]", "N/A");
            //        }
            //        if (paysliprecordsemployeepay.Rows.Count > 1)
            //        {
            //            hourandamount = (Convert.ToDouble(payslipworkedhours.Rows[0][2].ToString()) * Convert.ToDouble(paysliprecordsemployeepay.Rows[0][1].ToString()));
            //            //fields.SetField("txtbasic", hourandamount.ToString());//Basic Salary

            //            //if (psrpsd > 0)
            //            //{
            //            //    totalsalary = hourandamount + Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][2].ToString()) +
            //            //    Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][3].ToString()) + Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][4].ToString()) +
            //            //    Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][5].ToString()) + Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][6].ToString()) +
            //            //    Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][7].ToString()) + Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][8].ToString()) +
            //            //    Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][9].ToString()) + Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][10].ToString());
            //            //    sb.Replace("[Total Salary]", totalsalary.ToString());
            //            //}
            //        }
            //        if (payslipworkedhours.Rows[0][2] != DBNull.Value)
            //        {
            //            sb.Replace("[Regular Night Differential Hours]", payslipworkedhours.Rows[0][2].ToString());
            //        }
            //        else
            //        {
            //            sb.Replace("[Regular Night Differential Hours]", "N/A");
            //        }
            //        if (payslipworkedhours.Rows[0][3] != DBNull.Value)
            //        {
            //            sb.Replace("[Regular Holiday Hours]", payslipworkedhours.Rows[0][3].ToString());
            //        }
            //        else
            //        {
            //            sb.Replace("[Regular Holiday Hours]", "N/A");
            //        }
            //        if (payslipworkedhours.Rows[0][4] != DBNull.Value)
            //        {
            //            sb.Replace("[Regular Holiday Night Differential Hours]", payslipworkedhours.Rows[0][4].ToString());
            //        }
            //        else
            //        {
            //            sb.Replace("[Regular Holiday Night Differential Hours]", "N/A");
            //        }
            //        if (payslipworkedhours.Rows[0][5] != DBNull.Value)
            //        {
            //            sb.Replace("[Paid Time Off Hours]", payslipworkedhours.Rows[0][5].ToString());
            //        }
            //        else
            //        {
            //            sb.Replace("[Paid Time Off Hours]", "N/A");
            //        }
            //        if (payslipworkedhours.Rows[0][6] != DBNull.Value)
            //        {
            //            sb.Replace("[Regular Overtime Hours]", payslipworkedhours.Rows[0][6].ToString());
            //        }
            //        else
            //        {
            //            sb.Replace("[Regular Overtime Hours]", "N/A");
            //        }
            //        if (payslipworkedhours.Rows[0][7] != DBNull.Value)
            //        {
            //            sb.Replace("[Regular Holiday Overtime Hours]", payslipworkedhours.Rows[0][7].ToString());
            //        }
            //        else
            //        {
            //            sb.Replace("[Regular Holiday Overtime Hours]", "N/A");
            //        }
            //        if (payslipworkedhours.Rows[0][8] != DBNull.Value)
            //        {
            //            sb.Replace("[Special Holiday Hours]", payslipworkedhours.Rows[0][8].ToString());
            //        }
            //        else
            //        {
            //            sb.Replace("[Special Holiday Hours]", "N/A");
            //        }
            //        if (payslipworkedhours.Rows[0][9] != DBNull.Value)
            //        {
            //            sb.Replace("[Special Holiday Overtime Hours]", payslipworkedhours.Rows[0][9].ToString());
            //        }
            //        else
            //        {
            //            sb.Replace("[Special Holiday Overtime Hours]", "N/A");
            //        }
            //        if (payslipworkedhours.Rows[0][10] != DBNull.Value)
            //        {
            //            sb.Replace("[Special Holiday Night Differential Hours]", payslipworkedhours.Rows[0][10].ToString());
            //        }
            //        else
            //        {
            //            sb.Replace("[Special Holiday Night Differential Hours]", "N/A");
            //        }
            //    }
            //    else
            //    {
            //        sb.Replace("[Basic Salary Hours]", "N/A");
            //        sb.Replace("[Basic Salary]", "N/A");
            //        sb.Replace("[Total Salary]", "N/A");
            //        sb.Replace("[Regular Night Differential Hours]", "N/A");
            //        sb.Replace("[Regular Holiday Hours]", "N/A");
            //        sb.Replace("[Regular Holiday Night Differential Hours]", "N/A");
            //        sb.Replace("[Paid Time Off Hours]", "N/A");
            //        sb.Replace("[Regular Overtime Hours]", "N/A");
            //        sb.Replace("[Regular Holiday Overtime Hours]", "N/A");
            //        sb.Replace("[Special Holiday Hours]", "N/A");
            //        sb.Replace("[Special Holiday Overtime Hours]", "N/A");
            //        sb.Replace("[Special Holiday Night Differential Hours]", "N/A");
            //    }
            //    if (paysliprecordsallowanceamount.Rows.Count > 0)
            //    {
            //        //fields.SetField("txtallowancecurrent", paysliprecordsallowanceamount.Rows[0][1].ToString());//allowance
            //        //sb.Replace("[Allowance]", paysliprecordsallowanceamount.Rows[0][1].ToString());
            //    }
            //    else
            //    {
            //        sb.Replace("[Allowance]", "N/A");
            //    }
            //    if (paysliprecordsbonuses.Rows.Count > 0)
            //    {
            //        if (paysliprecordsallowanceamount.Rows.Count > 0)
            //        {
            //            //sb.Replace("[Bonuses]", paysliprecordsallowanceamount.Rows[0][1].ToString());
            //            //totalbonus = totalbonus + Convert.ToDouble(paysliprecordsallowanceamount.Rows[0][1].ToString());
            //        }
            //        else
            //        {

            //        }
            //        //fields.SetField("txtbonuscurrent", paysliprecordsallowanceamount.Rows[0][1].ToString());//bonuses                
            //    }
            //    else
            //    {
            //        sb.Replace("[Bonuses]", "N/A");
            //    }
            //    if (paysliprecordscompbenempcatbonuses.Rows.Count > 0)
            //    {
            //        for (int i = 0; i < paysliprecordscompbenempcatbonuses.Rows.Count; i++)
            //        {
            //            if (paysliprecordscompbenempcatbonuses.Rows[i][0] == DBNull.Value)
            //            {

            //            }
            //            else
            //            {
            //                totalbonus = totalbonus + Convert.ToDouble(paysliprecordscompbenempcatbonuses.Rows[i][0].ToString());
            //            }
            //        }
            //    }
            //    //sb.Replace("[Bonuses]", totalbonus.ToString());

            //    sb.Replace("[Bonus Payout Month]", PayoutMonths.ToString());
            //    sb.Replace("[Deminimis]", DeminimisAmount.ToString());
            //    sb.Replace("[SSS Loan Deduction]", SSSLoanDeduction.ToString());

            //    #endregion

            //    FileStream Newstream = new FileStream(string.Concat(HttpContext.Current.Server.MapPath("~/pdf/"), "Payslip-" + uProfile.NTID + ".pdf"), FileMode.Create);
            //    MemoryStream ms = new MemoryStream();
            //    Document document = new Document(PageSize.A4, 25, 25, 30, 30);
            //    //Code for adding the string builder sb to the document
            //    PdfWriter writer = PdfWriter.GetInstance(document, Newstream);
            //    document.Open();
            //    document.Add(new Paragraph(sb.ToString()));
            //    document.Close();
            //    writer.Close();
            //    //HttpContext.Current.Response.ContentType = "pdf/application";
            //    //HttpContext.Current.Response.AddHeader("content-disposition", ("attachment;filename=" + empid + "newsamplepayslip.pdf"));
            //    //HttpContext.Current.Response.OutputStream.Write(ms.GetBuffer(), 0, ms.GetBuffer().Length);

            //    //        viewpdf.Attributes.Add("src", "pdf\\" + empid + pathmoto + ".pdf");
            //    #endregion
            //}
            //else
            //{

            //    #region Fixed payslip
            //    //txtallowancecurrent,txtbonuscurrent,txtallowanceyrtd,txtbonusyrtd

            //    //                indx = DDLPayPeriod.SelectedIndex;
            //    GetEmpLevelandCategory = PayMtd.GetEmpLevelandCategory(empid);
            //    EmpLevel = GetEmpLevelandCategory.Rows[0][0].ToString();
            //    EmpCategory = GetEmpLevelandCategory.Rows[0][1].ToString();
            //    int psrpsd = 0;
            //    int pswh = 0;
            //    //paysliprecordspayperiod = PayMtd.PaySlipRecordsPayPeriod(EmpLevel, EmpCategory);
            //    paysliprecordspayperiod = PayMtd.PaySlipRecordsPayPeriod(EmpLevel, EmpCategory);


            //    if (paysliprecordspayperiod.Rows.Count > 0)
            //    {
            //        string ddlmonth = uProfile.PayMonth.ToString();
            //        string ddlyear = uProfile.PayYear.ToString();
            //        string stringtobesplitted = uProfile.PayDay.ToString();
            //        string[] words = stringtobesplitted.Split('-');
            //        string firstpayperiodstart = words[0].ToString();
            //        string firstpayperiodend;
            //        if (words[1].ToString() == "End of month")
            //        {
            //            int a, b;
            //            a = Convert.ToInt32(ddlmonth);
            //            b = Convert.ToInt32(ddlyear);
            //            int x = DateTime.DaysInMonth(b, a);
            //            firstpayperiodend = x.ToString();
            //        }
            //        else
            //        {
            //            firstpayperiodend = words[1].ToString();
            //        }

            //        //string firstpayperiodstart = paysliprecordspayperiod.Rows[indx][0].ToString();
            //        //string firstpayperiodend = paysliprecordspayperiod.Rows[indx][1].ToString();
            //        string bonusdayfrom = firstpayperiodstart;
            //        string bonusdayto = firstpayperiodend;

            //        string datefrom = ddlyear + '-' + ddlmonth + '-' + firstpayperiodstart;
            //        string dateto = ddlyear + '-' + ddlmonth + '-' + firstpayperiodend;

            //        if (firstpayperiodend == "End of month")
            //        {
            //            int x = DateTime.DaysInMonth(Convert.ToInt32(ddlyear), Convert.ToInt32(ddlmonth));
            //            firstpayperiodend = x.ToString();
            //        }

            //        if (firstpayperiodstart == "1" || firstpayperiodstart == "2" || firstpayperiodstart == "3" || firstpayperiodstart == "4" || firstpayperiodstart == "5"
            //            || firstpayperiodstart == "6" || firstpayperiodstart == "7" || firstpayperiodstart == "8" || firstpayperiodstart == "9")
            //        {
            //            hoursdatefrom = ddlyear + '-' + ddlmonth + '-' + ("0" + firstpayperiodstart);
            //        }
            //        else
            //        {
            //            hoursdatefrom = ddlyear + '-' + ddlmonth + '-' + firstpayperiodstart;
            //        }
            //        if (firstpayperiodend == "1" || firstpayperiodend == "2" || firstpayperiodend == "3" || firstpayperiodend == "4" || firstpayperiodend == "5"
            //            || firstpayperiodend == "6" || firstpayperiodend == "7" || firstpayperiodend == "8" || firstpayperiodend == "9")
            //        {
            //            hoursdateto = ddlyear + '-' + ddlmonth + '-' + ("0" + firstpayperiodend);
            //        }
            //        else if (firstpayperiodend == "31")
            //        {
            //            if (ddlmonth == "02" || ddlmonth == "04" || ddlmonth == "06" || ddlmonth == "09" || ddlmonth == "11")
            //            {
            //                int x = DateTime.DaysInMonth(Convert.ToInt32(ddlyear), Convert.ToInt32(ddlmonth));
            //                hoursdateto = ddlyear + '-' + ddlmonth + '-' + x;
            //            }
            //            else
            //            {
            //                hoursdateto = ddlyear + '-' + ddlmonth + '-' + firstpayperiodend;
            //            }
            //        }
            //        else
            //        {
            //            hoursdateto = ddlyear + '-' + ddlmonth + '-' + firstpayperiodend;
            //        }

            //        //HelloWorld
            //        //PayMtd.SecretMethodForPayslip("2017-08-22");
            //        //PayMtd.RunPayslipScript(hoursdatefrom, hoursdateto,"2017-08-22");

            //        payslipworkedhours = PayMtd.PaySlipWorkedHours(empid, hoursdatefrom, hoursdateto);
            //        pswh = payslipworkedhours.Rows.Count;
            //        //payperiod = datefrom + '/' + dateto;
            //        payperiod = hoursdatefrom + '-' + hoursdateto;
            //        paysliprecordspayslipdetails = PayMtd.PaySlipRecordsPaySlipDetails(empid, payperiod);
            //        psrpsd = paysliprecordspayslipdetails.Rows.Count;
            //        paysliprecordsallowanceamount = PayMtd.getPayslipRecordsAllowanceAmount(empid);
            //        paysliprecordsbonuses = PayMtd.getPayslipRecordsBonuses(empid, datefrom + "/" + dateto);
            //        paysliprecordscompbenempcatbonuses = PayMtd.getPayslipCompBenEmpCatBonuses(EmpLevel, EmpCategory, hoursdatefrom, hoursdateto, bonusdayfrom, bonusdayto);
            //        //BonusPayoutCompute(EmpLevel, EmpCategory, empid,hoursdatefrom,hoursdateto);
            //        BonusPayoutMonth(empid, hoursdatefrom, hoursdateto);
            //        CompBenDeminimis(empid, EmpCategory, hoursdatefrom, hoursdateto);
            //        //PayrollDisputes(empid, hoursdatefrom, hoursdateto);
            //        SSSLoans(empid, hoursdatefrom, hoursdateto);
            //        PayrollAdjustments(empid, hoursdatefrom + '-' + hoursdateto);
            //    }


            //    //Response.Redirect("~/pdf/BIR19021.pdf"); // view pdf
            //    // PdfReader reader = new PdfReader("http://192.168.1.250:81/pdf/PaySlipDefault.pdf");
            //    PdfReader reader = new PdfReader("http://illimitadodevs.azurewebsites.net/pdf/PaySlipDefault.pdf");
            //    PdfStamper stamper = new PdfStamper(reader, new FileStream(string.Concat(HttpContext.Current.Server.MapPath("~/pdf/"), "Payslip-" + uProfile.NTID + ".pdf"), FileMode.Create));


            //    // Object to deal with the Output file's textfields
            //    AcroFields fields = stamper.AcroFields;

            //    // set form fields("Field Name in PDF", "String to be placed in this PDF text field");
            //    //for (int i = 0; i <= 1000; i++)
            //    //{
            //    //    string ii = i.ToString();
            //    //    
            //    //    fields.SetField("untitled" + i, "vt" + ii); //temporary values
            //    //}

            //    //------------------------------------------------
            //    //loop to display all field names
            //    if (paysliprecordsempmaster.Rows.Count > 0)
            //    {
            //        //fields.SetFieldProperty("txtempid", "setflags", PdfAnnotation.FLAGS_HIDDEN,null);
            //        fields.SetField("untitled2", paysliprecordsempmaster.Rows[0][0].ToString());//Employee Name
            //        fields.SetField("txtempid", empid);//Employee No
            //        fields.SetField("txtjoined", paysliprecordsempmaster.Rows[0][2].ToString());//Joined
            //        fields.SetField("txtresigned", paysliprecordsempmaster.Rows[0][3].ToString());//Resigned
            //    }
            //    else
            //    {

            //    }
            //    if (paysliprecordsconfidentialinfo.Rows.Count > 0)
            //    {
            //        fields.SetField("txtpagibig", paysliprecordsconfidentialinfo.Rows[0][0].ToString());//Pag-IBIG No
            //        fields.SetField("txtph", paysliprecordsconfidentialinfo.Rows[0][1].ToString());//PhilHealth No
            //        fields.SetField("txtsss", paysliprecordsconfidentialinfo.Rows[0][2].ToString());//SSS No
            //        fields.SetField("txttax", paysliprecordsconfidentialinfo.Rows[0][3].ToString());//Tax No
            //        fields.SetField("txtcat", paysliprecordsconfidentialinfo.Rows[0][4].ToString());//Tax Category
            //    }
            //    else
            //    {

            //    }
            //    if (paysliprecordsemployeepay.Rows.Count > 0)
            //    {
            //        fields.SetField("untitled32", paysliprecordsempmaster.Rows[0][0].ToString());//Employee Name
            //        fields.SetField("txtempid", empid);//Employee No
            //        fields.SetField("txtbank", paysliprecordsemployeepay.Rows[0][0].ToString());//Bank Account
            //        fields.SetField("txthourly", paysliprecordsemployeepay.Rows[0][1].ToString());//Hourly Rate
            //                                                                                      //fields.SetField("txtbasic", paysliprecordsemployeepay.Rows[0][2].ToString());//Basic Salary

            //    }
            //    else
            //    {

            //    }
            //    fields.SetField("txtPayPeriod", payperiod);//Pay Period
            //    if (psrpsd > 0)
            //    {
            //        //string a, b, c, d, ea, f, g, h, i, j, k, l, m, n, o, p, q, r, s;
            //        //fields.SetField("txtPayPeriod", paysliprecordspayslipdetails.Rows[0][0].ToString());//Pay Period

            //        fields.SetField("txtpayday", paysliprecordspayslipdetails.Rows[0][1].ToString());//Pay Day
            //                                                                                         //a = paysliprecordspayslipdetails.Rows[0][1].ToString();
            //        fields.SetField("txtrdnd", paysliprecordspayslipdetails.Rows[0][2].ToString());//Regular Night Differential
            //                                                                                       //b = paysliprecordspayslipdetails.Rows[0][2].ToString();
            //        if (paysliprecordspayslipdetails.Rows[0][2] != DBNull.Value)
            //        {
            //            finalsalary = (Convert.ToDouble(finalsalary) + Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][2].ToString())).ToString();
            //        }
            //        fields.SetField("txtrh", paysliprecordspayslipdetails.Rows[0][3].ToString());//Regular Holiday
            //                                                                                     //c = paysliprecordspayslipdetails.Rows[0][3].ToString();
            //        if (paysliprecordspayslipdetails.Rows[0][3] != DBNull.Value)
            //        {
            //            finalsalary = (Convert.ToDouble(finalsalary) + Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][3].ToString())).ToString();
            //        }
            //        fields.SetField("txtrhnd", paysliprecordspayslipdetails.Rows[0][4].ToString());//Regular Holiday Night Differential
            //                                                                                       //d = paysliprecordspayslipdetails.Rows[0][4].ToString();
            //        if (paysliprecordspayslipdetails.Rows[0][4] != DBNull.Value)
            //        {
            //            finalsalary = (Convert.ToDouble(finalsalary) + Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][4].ToString())).ToString();
            //        }
            //        fields.SetField("txtpto", paysliprecordspayslipdetails.Rows[0][5].ToString());//Paid Time Off
            //                                                                                      //ea = paysliprecordspayslipdetails.Rows[0][5].ToString();
            //        if (paysliprecordspayslipdetails.Rows[0][5] != DBNull.Value)
            //        {
            //            finalsalary = (Convert.ToDouble(finalsalary) + Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][5].ToString())).ToString();
            //        }
            //        fields.SetField("txtreghld", paysliprecordspayslipdetails.Rows[0][6].ToString());//Regular Over Time
            //                                                                                         //f = paysliprecordspayslipdetails.Rows[0][6].ToString();
            //        if (paysliprecordspayslipdetails.Rows[0][6] != DBNull.Value)
            //        {
            //            finalsalary = (Convert.ToDouble(finalsalary) + Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][6].ToString())).ToString();
            //        }
            //        fields.SetField("txtreghldOT", paysliprecordspayslipdetails.Rows[0][7].ToString());//Regular Holiday Over Time
            //                                                                                           //g = paysliprecordspayslipdetails.Rows[0][7].ToString();
            //        if (paysliprecordspayslipdetails.Rows[0][7] != DBNull.Value)
            //        {
            //            finalsalary = (Convert.ToDouble(finalsalary) + Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][7].ToString())).ToString();
            //        }
            //        fields.SetField("txtspcl", paysliprecordspayslipdetails.Rows[0][8].ToString());//Special Holiday
            //                                                                                       //h = paysliprecordspayslipdetails.Rows[0][8].ToString();
            //        if (paysliprecordspayslipdetails.Rows[0][8] != DBNull.Value)
            //        {
            //            finalsalary = (Convert.ToDouble(finalsalary) + Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][8].ToString())).ToString();
            //        }
            //        fields.SetField("txtspclOT", paysliprecordspayslipdetails.Rows[0][9].ToString());//Special Holiday Over Time
            //                                                                                         //i = paysliprecordspayslipdetails.Rows[0][9].ToString();
            //        if (paysliprecordspayslipdetails.Rows[0][9] != DBNull.Value)
            //        {
            //            finalsalary = (Convert.ToDouble(finalsalary) + Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][9].ToString())).ToString();
            //        }
            //        fields.SetField("txtspclnd", paysliprecordspayslipdetails.Rows[0][10].ToString());//Special Holiday Night Differential
            //                                                                                          //j = paysliprecordspayslipdetails.Rows[0][10].ToString();
            //        if (paysliprecordspayslipdetails.Rows[0][10] != DBNull.Value)
            //        {
            //            finalsalary = (Convert.ToDouble(finalsalary) + Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][10].ToString())).ToString();
            //        }
            //        fields.SetField("txthdmf", paysliprecordspayslipdetails.Rows[0][11].ToString());//Employee HDMF
            //                                                                                        //k = paysliprecordspayslipdetails.Rows[0][11].ToString();
            //        fields.SetField("txteph", paysliprecordspayslipdetails.Rows[0][12].ToString());//Employee PhilHealth
            //                                                                                       //l = paysliprecordspayslipdetails.Rows[0][12].ToString();
            //        fields.SetField("txtesss", paysliprecordspayslipdetails.Rows[0][13].ToString());//Employee SSS
            //                                                                                        //m = paysliprecordspayslipdetails.Rows[0][13].ToString();
            //        fields.SetField("txtwighTax", paysliprecordspayslipdetails.Rows[0][14].ToString());//Employee WithHoldingTax
            //                                                                                           //n = paysliprecordspayslipdetails.Rows[0][14].ToString();
            //        fields.SetField("txtemplyrHDMF", paysliprecordspayslipdetails.Rows[0][15].ToString());//Employer HDMF
            //                                                                                              //o = paysliprecordspayslipdetails.Rows[0][15].ToString();
            //        fields.SetField("txtemplyrPH", paysliprecordspayslipdetails.Rows[0][16].ToString());//Employer PhilHealth
            //                                                                                            //p = paysliprecordspayslipdetails.Rows[0][16].ToString();
            //        fields.SetField("txtemplyrSSS", paysliprecordspayslipdetails.Rows[0][17].ToString());//Employer SSS
            //                                                                                             //q = paysliprecordspayslipdetails.Rows[0][17].ToString();
            //        fields.SetField("txtemplyrSSSEC", paysliprecordspayslipdetails.Rows[0][18].ToString());//Employer SSS EC
            //                                                                                               //r = paysliprecordspayslipdetails.Rows[0][18].ToString();
            //        fields.SetField("txtnetpay", paysliprecordspayslipdetails.Rows[0][19].ToString());//Net Pay
            //                                                                                          //s = paysliprecordspayslipdetails.Rows[0][19].ToString();
            //        fields.SetField("txthourly", paysliprecordspayslipdetails.Rows[0][20].ToString());//Hourly Rate
            //        if (paysliprecordspayslipdetails.Rows[0][21] != DBNull.Value)
            //        {
            //            double payslipnontax = 0.00;
            //            payslipnontax = Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][21].ToString());
            //            fields.SetField("txtnontax", (payslipnontax + DeminimisAmount).ToString());//Non Taxable Allowance
            //            finalsalary = (Convert.ToDouble(finalsalary) + DeminimisAmount).ToString();
            //            finalsalary = (Convert.ToDouble(finalsalary) + Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][21].ToString())).ToString();
            //        }
            //        else
            //        {
            //            fields.SetField("txtnontax", DeminimisAmount.ToString());//Non Taxable Allowance
            //            finalsalary = (Convert.ToDouble(finalsalary) + DeminimisAmount).ToString();
            //        }
            //        fields.SetField("txtbasic", paysliprecordspayslipdetails.Rows[0][22].ToString());//Basic Salary
            //        if (paysliprecordspayslipdetails.Rows[0][22] != DBNull.Value)
            //        {
            //            finalsalary = (Convert.ToDouble(finalsalary) + Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][22].ToString())).ToString();
            //        }
            //        //fields.SetField("txtbasichr", paysliprecordspayslipdetails.Rows[0][23].ToString());//Basic Salary Hours

            //        fields.SetField("txtRegOverT", paysliprecordspayslipdetails.Rows[0][24].ToString());//Regular Overtime
            //    }
            //    else if (paysliprecordspayslipdetails == null)
            //    {

            //    }
            //    else
            //    {

            //    }


            //    #region Disputes
            //    //for (int p = 0; p < paysliprecordsdisputes.Rows.Count; p++)
            //    //{
            //    //    if (paysliprecordsdisputes.Rows[p][1] != DBNull.Value || paysliprecordsdisputes.Rows[p][2] != DBNull.Value)
            //    //    {
            //    //        if (paysliprecordsdisputes.Rows[p][1].ToString() != "" && paysliprecordsdisputes.Rows[p][2].ToString() == "")
            //    //        {
            //    //            totalsalary = totalsalary + Convert.ToDouble(paysliprecordsdisputes.Rows[p][1].ToString());
            //    //        }
            //    //        else if (paysliprecordsdisputes.Rows[p][1].ToString() == "" && paysliprecordsdisputes.Rows[p][2].ToString() != "")
            //    //        {
            //    //            totalsalary = totalsalary - Convert.ToDouble(paysliprecordsdisputes.Rows[p][2].ToString());
            //    //        }
            //    //    }
            //    //    else
            //    //    {

            //    //    }
            //    //}
            //    #endregion


            //    if (pswh > 0)
            //    {
            //        double hourandamount;
            //        //double totalsalary;
            //        //fields.SetField("txtbasichr", payslipworkedhours.Rows[0][2].ToString());//Basic Salary Hours
            //        if (paysliprecordsemployeepay.Rows.Count > 1)
            //        {
            //            hourandamount = (Convert.ToDouble(payslipworkedhours.Rows[0][2].ToString()) * Convert.ToDouble(paysliprecordsemployeepay.Rows[0][1].ToString()));
            //            fields.SetField("txtbasic", hourandamount.ToString());//Basic Salary
            //            if (psrpsd > 0)
            //            {
            //                totalsalary = hourandamount + Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][2].ToString()) +
            //                Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][3].ToString()) + Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][4].ToString()) +
            //                Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][5].ToString()) + Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][6].ToString()) +
            //                Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][7].ToString()) + Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][8].ToString()) +
            //                Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][9].ToString()) + Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][10].ToString());
            //                //finalsalary = totalsalary.ToString();
            //                //fields.SetField("txtTotalSalary", totalsalary.ToString());//Total Salary
            //            }
            //        }
            //        fields.SetFieldProperty("txtnontaxhrs", "setflags", PdfAnnotation.FLAGS_HIDDEN, null);//Non Taxable Allowance Hours
            //                                                                                              //fields.SetField("txtregNDhrs", payslipworkedhours.Rows[0][2].ToString());//Regular Night Differential Hours
            //                                                                                              //fields.SetField("txtregHhrs", payslipworkedhours.Rows[0][3].ToString());//Regular Holiday
            //                                                                                              //fields.SetField("txtregHNdHrs", payslipworkedhours.Rows[0][4].ToString());//Regular Holiday Night Differential
            //                                                                                              //fields.SetField("txtPTOHrs", payslipworkedhours.Rows[0][5].ToString());//Paid Time Off
            //                                                                                              //fields.SetField("txtRegOverT", payslipworkedhours.Rows[0][6].ToString());//Regular Overtime
            //                                                                                              //fields.SetField("txtregHOHrs", payslipworkedhours.Rows[0][7].ToString());//Regular Holiday Overtime
            //                                                                                              //fields.SetField("txtspclHolHrs", payslipworkedhours.Rows[0][8].ToString());//Special Holiday
            //                                                                                              //fields.SetField("txtspclHolOOTHrs", payslipworkedhours.Rows[0][9].ToString());//Special Holiday Overtime
            //                                                                                              //fields.SetField("txtspclHolNDHrs", payslipworkedhours.Rows[0][10].ToString());//Special Holiday Night Differential


            //    }
            //    else
            //    {

            //    }
            //    if (paysliprecordsallowanceamount.Rows.Count > 0)
            //    {
            //        //fields.SetField("txtallowancecurrent", paysliprecordsallowanceamount.Rows[0][1].ToString());//allowance
            //        //finalsalary = (Convert.ToDouble(finalsalary) + Convert.ToDouble(paysliprecordsallowanceamount.Rows[0][1].ToString())).ToString();
            //    }
            //    else
            //    {

            //    }
            //    if (paysliprecordsbonuses.Rows.Count > 0)
            //    {
            //        //fields.SetField("txtbonuscurrent", paysliprecordsbonuses.Rows[0][1].ToString());//bonuses
            //        if (paysliprecordsbonuses.Rows[0][1] == DBNull.Value || paysliprecordsbonuses.Rows[0][1].ToString() == "")
            //        {

            //        }
            //        else
            //        {
            //            totalbonus = totalbonus + Convert.ToDouble(paysliprecordsbonuses.Rows[0][1].ToString());
            //        }
            //    }
            //    else
            //    {

            //    }
            //    if (paysliprecordscompbenempcatbonuses.Rows.Count > 0)
            //    {
            //        for (int i = 0; i < paysliprecordscompbenempcatbonuses.Rows.Count; i++)
            //        {
            //            totalbonus = totalbonus + Convert.ToDouble(paysliprecordscompbenempcatbonuses.Rows[i][0].ToString());
            //        }
            //    }
            //    //finalsalary = (Convert.ToDouble(finalsalary) + Convert.ToDouble(totalbonus)).ToString();
            //    fields.SetField("txtTotalSalary", finalsalary);//Total Salary
            //                                                   //fields.SetField("txtbonuscurrent", totalbonus.ToString());
            //    string bs = "0.00", nta = "0.00", rnd = "0.00", rh = "0.00", rhnd = "0.00", pto = "0.00", ro = "0.00", rhot = "0.00", sh = "0.00"
            //        , shot = "0.00", shnd = "0.00", allowance = "0.00", bonus = "0.00", salary = "0.00", hdmf = "0.00", ph = "0.00", sss = "0.00"
            //        , tax = "0.00", netpay = "0.00", ehdmf = "0.00", eph = "0.00", esss = "0.00", esssec = "0.00";

            //    YearToDate = PayMtd.PaySlipRecordsPaySlipDetailsYearToDate(empid, hoursdatefrom, hoursdateto);

            //    for (int i = 0; i < YearToDate.Rows.Count; i++)
            //    {
            //        if (YearToDate.Rows[i][0] == DBNull.Value)
            //        {

            //        }
            //        else
            //        {
            //            bs = YearToDate.Rows[i][0].ToString();
            //        }
            //        if (YearToDate.Rows[i][1] == DBNull.Value)
            //        {

            //        }
            //        else
            //        {
            //            nta = YearToDate.Rows[i][1].ToString();
            //        }
            //        if (YearToDate.Rows[i][2] == DBNull.Value)
            //        {

            //        }
            //        else
            //        {
            //            rnd = YearToDate.Rows[i][2].ToString();
            //        }
            //        if (YearToDate.Rows[i][3] == DBNull.Value)
            //        {

            //        }
            //        else
            //        {
            //            rh = YearToDate.Rows[i][3].ToString();
            //        }
            //        if (YearToDate.Rows[i][4] == DBNull.Value)
            //        {

            //        }
            //        else
            //        {
            //            rhnd = YearToDate.Rows[i][4].ToString();
            //        }
            //        if (YearToDate.Rows[i][5] == DBNull.Value)
            //        {

            //        }
            //        else
            //        {
            //            //pto = YearToDate.Rows[i][5].ToString();
            //        }
            //        if (YearToDate.Rows[i][6] == DBNull.Value)
            //        {

            //        }
            //        else
            //        {
            //            ro = YearToDate.Rows[i][6].ToString();
            //        }
            //        if (YearToDate.Rows[i][7] == DBNull.Value)
            //        {

            //        }
            //        else
            //        {
            //            rhot = YearToDate.Rows[i][7].ToString();
            //        }
            //        if (YearToDate.Rows[i][8] == DBNull.Value)
            //        {

            //        }
            //        else
            //        {
            //            sh = YearToDate.Rows[i][8].ToString();
            //        }
            //        if (YearToDate.Rows[i][9] == DBNull.Value)
            //        {

            //        }
            //        else
            //        {
            //            shot = YearToDate.Rows[i][9].ToString();
            //        }
            //        if (YearToDate.Rows[i][10] == DBNull.Value)
            //        {

            //        }
            //        else
            //        {
            //            shnd = YearToDate.Rows[i][10].ToString();
            //        }
            //        if (YearToDate.Rows[i][11] == DBNull.Value)
            //        {

            //        }
            //        else
            //        {
            //            netpay = YearToDate.Rows[i][11].ToString();

            //        }
            //        if (YearToDate.Rows[i][12] == DBNull.Value)
            //        {

            //        }
            //        else
            //        {
            //            hdmf = YearToDate.Rows[i][12].ToString();

            //        }
            //        if (YearToDate.Rows[i][13] == DBNull.Value)
            //        {

            //        }
            //        else
            //        {
            //            ph = YearToDate.Rows[i][13].ToString();

            //        }
            //        if (YearToDate.Rows[i][14] == DBNull.Value)
            //        {

            //        }
            //        else
            //        {
            //            sss = YearToDate.Rows[i][14].ToString();

            //        }
            //        if (YearToDate.Rows[i][15] == DBNull.Value)
            //        {

            //        }
            //        else
            //        {
            //            tax = YearToDate.Rows[i][15].ToString();
            //        }
            //        if (YearToDate.Rows[i][16] == DBNull.Value)
            //        {

            //        }
            //        else
            //        {
            //            ehdmf = YearToDate.Rows[i][16].ToString();
            //        }
            //        if (YearToDate.Rows[i][17] == DBNull.Value)
            //        {

            //        }
            //        else
            //        {
            //            eph = YearToDate.Rows[i][17].ToString();
            //        }
            //        if (YearToDate.Rows[i][18] == DBNull.Value)
            //        {

            //        }
            //        else
            //        {
            //            esss = YearToDate.Rows[i][18].ToString();
            //        }
            //        if (YearToDate.Rows[i][19] == DBNull.Value)
            //        {

            //        }
            //        else
            //        {
            //            esssec = YearToDate.Rows[i][19].ToString();
            //        }
            //    }
            //    fields.SetField("txttbs", bs.ToString());
            //    //fields.SetField("txttnta", nta.ToString());
            //    fields.SetField("txttrnd", rnd.ToString());
            //    fields.SetField("txttrh", rh.ToString());
            //    fields.SetField("txttrhnd", rhnd.ToString());

            //    LeaveHoursAmount = PayMtd.PaySlipRecordsLeaveHoursAmount(empid, hoursdatefrom, hoursdateto);
            //    for (int o = 0; o < LeaveHoursAmount.Rows.Count; o++)
            //    {
            //        if (LeaveHoursAmount.Rows[o][0] != DBNull.Value)
            //        {
            //            fields.SetField("txtPTOHrs", LeaveHoursAmount.Rows[o][0].ToString());//Paid Time Off
            //        }
            //        if (LeaveHoursAmount.Rows[o][1] != DBNull.Value)
            //        {
            //            //pto = LeaveHoursAmount.Rows[o][1].ToString();
            //            fields.SetField("txtpto", LeaveHoursAmount.Rows[o][1].ToString());//Paid Time Off
            //        }
            //    }

            //    LeaveHoursAmountYearToDate = PayMtd.PaySlipRecordsLeaveHoursAmountYearToDate(empid, hoursdatefrom, hoursdateto);
            //    for (int u = 0; u < LeaveHoursAmountYearToDate.Rows.Count; u++)
            //    {
            //        if (LeaveHoursAmountYearToDate.Rows[u][0] != DBNull.Value)
            //        {
            //            fields.SetField("txtPTOHrs", LeaveHoursAmount.Rows[u][0].ToString());//Paid Time Off
            //        }
            //        if (LeaveHoursAmountYearToDate.Rows[u][1] != DBNull.Value)
            //        {
            //            pto = LeaveHoursAmount.Rows[u][1].ToString();
            //            //fields.SetField("txtpto", LeaveHoursAmount.Rows[u][1].ToString());//Paid Time Off
            //        }
            //        else
            //        {
            //            pto = "0.00";
            //        }
            //    }

            //    fields.SetField("txttpto", pto);

            //    fields.SetField("txttro", ro.ToString());
            //    fields.SetField("txttrhot", rhot.ToString());
            //    fields.SetField("txttsh", sh.ToString());
            //    fields.SetField("txttshot", shot.ToString());
            //    fields.SetField("txttshnd", shnd.ToString());

            //    salary = (Convert.ToDouble(salary) + (Convert.ToDouble(bs))).ToString();
            //    salary = (Convert.ToDouble(salary) + (Convert.ToDouble(rnd))).ToString();
            //    salary = (Convert.ToDouble(salary) + (Convert.ToDouble(rh))).ToString();
            //    salary = (Convert.ToDouble(salary) + (Convert.ToDouble(rhnd))).ToString();
            //    salary = (Convert.ToDouble(salary) + (Convert.ToDouble(pto))).ToString();
            //    salary = (Convert.ToDouble(salary) + (Convert.ToDouble(ro))).ToString();
            //    salary = (Convert.ToDouble(salary) + (Convert.ToDouble(rhot))).ToString();
            //    salary = (Convert.ToDouble(salary) + (Convert.ToDouble(sh))).ToString();
            //    salary = (Convert.ToDouble(salary) + (Convert.ToDouble(shot))).ToString();
            //    //salary = (Convert.ToDouble(salary) + (Convert.ToDouble(shnd))).ToString();
            //    //salary = (Convert.ToDouble(bs) + Convert.ToDouble(rnd) + Convert.ToDouble(rh) + Convert.ToDouble(rhnd) + Convert.ToDouble(pto) 
            //    //    + Convert.ToDouble(ro) + Convert.ToDouble(rhot) + Convert.ToDouble(sh) + Convert.ToDouble(shot) + Convert.ToDouble(shnd)).ToString();
            //    #region Disputes
            //    //for (int p = 0; p < paysliprecordsdisputes.Rows.Count; p++)
            //    //{
            //    //    if (paysliprecordsdisputes.Rows[p][1] != DBNull.Value || paysliprecordsdisputes.Rows[p][2] != DBNull.Value)
            //    //    {
            //    //        if (paysliprecordsdisputes.Rows[p][1].ToString() != "" && paysliprecordsdisputes.Rows[p][2].ToString() == "")
            //    //        {
            //    //            totalsalary = totalsalary + Convert.ToDouble(paysliprecordsdisputes.Rows[p][1].ToString());
            //    //        }
            //    //        else if (paysliprecordsdisputes.Rows[p][1].ToString() == "" && paysliprecordsdisputes.Rows[p][2].ToString() != "")
            //    //        {
            //    //            totalsalary = totalsalary - Convert.ToDouble(paysliprecordsdisputes.Rows[p][2].ToString());
            //    //        }
            //    //    }
            //    //    else
            //    //    {

            //    //    }
            //    //}
            //    #endregion
            //    fields.SetField("txtttotalsal", salary);

            //    fields.SetField("txttehdmf", hdmf.ToString());
            //    fields.SetField("txtteph", ph.ToString());
            //    fields.SetField("txttsss", sss.ToString());
            //    fields.SetField("txttwtax", tax.ToString());

            //    fields.SetField("txttenetpay", netpay.ToString());

            //    fields.SetField("txtemplyrtHDMF", ehdmf.ToString());
            //    fields.SetField("txtemplyrtPH", eph.ToString());
            //    fields.SetField("txtemplyrtSSS", esss.ToString());
            //    fields.SetField("txtemplyrtSSSEC", esssec.ToString());


            //    fields.SetField("txtsssloandeduct", SSSLoanDeduction.ToString());//SSS LOAN DEDUCTION
            //    fields.SetField("pagibigloandeduct", HDMFLoanDeduction.ToString());//PAGIBIG LOAN
            //    fields.SetField("txtfringe", FringeAmount.ToString());//FRINGE
            //    fields.SetField("txtdeminimis", DeminimisAmount.ToString());//DEMINIMIS
            //    fields.SetField("txtotherbenefits", OtherBenefits.ToString());
            //    fields.SetField("txtbonuspayoutmonth", PayoutMonths.ToString());//BONUS PAYOUT MONTH
            //    fields.SetField("adjustmentscurrent", Disputes.ToString());// DISPUTES
            //                                                               //txtsssloandeduct txtsssld
            //                                                               //pagibigloandeduct pagibigld
            //                                                               //txtfringe txtfrng
            //                                                               //txtdeminimis txtdmnms
            //                                                               //txtotherbenefits txtothrbnfts
            //                                                               //txtbonuspayoutmonth txtbnspytmnth
            //                                                               //fields.SetField("txtallowanceyrtd", allowance.ToString());
            //                                                               //fields.SetField("txtbonusyrtd", bonus.ToString());



            //    //foreach (var field in af.Fields)
            //    //{
            //    //    Console.WriteLine("{0}, {1}", field.Key, field.Value);
            //    //    //string sample = TextBox2.Text;
            //    //    string sample2 = field.Key.ToString();
            //    //    //TextBox2.Text = sample + field.Key.ToString() + ',';
            //    //    fields.SetField(field.Key.ToString(), field.ToString());
            //    //}


            //    //------------------------------------------------

            //    // form flattening rids the form of editable text fields so
            //    // the final output can't be edited
            //    stamper.FormFlattening = true;
            //    // closing the stamper
            //    stamper.Close();

            //    //Response.Redirect("~/pdf/PaySlip-I101.pdf");
            //    #endregion
            //}

            #endregion

            string DDLMonths = uProfile.PayMonth;
            string DDLPayPeriod = uProfile.PayDay;
            string DDLYear = uProfile.PayYear;
            empid = uProfile.NTID;

            empname = PayMtd.searchempname(empid);

            empidprof = empid;
            GetEmpLevelandCategory = PayMtd.GetEmpLevelandCategory(empid);
            string EmpLevel = GetEmpLevelandCategory.Rows[0][0].ToString();
            string EmpCategory = GetEmpLevelandCategory.Rows[0][1].ToString();
            paysliprecordsempmaster = PayMtd.PaySlipRecordsEmpMaster(empid);
            paysliprecordsconfidentialinfo = PayMtd.PaySlipRecordsConfidentialInfo(empid);
            paysliprecordsemployeepay = PayMtd.PaySlipRecordsEmployeePay(empid);
            int schmct = PayMtd.GetSalarySchemes(EmpLevel, EmpCategory).Rows.Count;

            #region "New Payslip"
            TotalValue = 0;
            //viewingBuble();
            //countingInbox();
            int latestpayslipcounter = 0;
            LatestPayslipForm = PayMtd.LatestPayslipForm();
            latestpayslipcounter = LatestPayslipForm.Rows.Count;
            DataTable BonusPayoutMonths = PayMtd.BonusPayoutMonths(empid, DDLMonths.ToString());
            // if (DDLPayPeriod.SelectedIndex > ((DDLPayPeriod.Items.Count - BonusPayoutMonths.Rows.Count) - 1))
            // {
            //   BonusPayoutPDF();
            // }
            // else
            // {
            if (latestpayslipcounter == 1)
            {
                #region Dynamic payslip
                #region Retrieve Neccessary Data
                //indx = DDLPayPeriod.SelectedIndex;
                GetEmpLevelandCategory = PayMtd.GetEmpLevelandCategory(empid);
                int psrpsd = 0;
                int pswh = 0;
                paysliprecordspayperiod = PayMtd.PaySlipRecordsPayPeriod(EmpLevel, EmpCategory);
                #region MyRegion
                if (paysliprecordspayperiod.Rows.Count > 0)
                {
                    string ddlmonth = DDLMonths.ToString();
                    string ddlyear = DDLYear.ToString();
                    string stringtobesplitted = DDLPayPeriod.ToString();
                    string[] words = stringtobesplitted.Split('-');
                    string firstpayperiodstart = words[0].ToString();
                    string firstpayperiodend;
                    if (words[1].ToString() == "End of month")
                    {
                        int a, b;
                        a = Convert.ToInt32(ddlmonth);
                        b = Convert.ToInt32(ddlyear);
                        int x = DateTime.DaysInMonth(b, a);
                        firstpayperiodend = x.ToString();
                    }
                    else
                    {
                        firstpayperiodend = words[1].ToString();
                    }

                    string bonusdayfrom = firstpayperiodstart;
                    string bonusdayto = firstpayperiodend;

                    string datefrom = ddlyear + '-' + ddlmonth + '-' + firstpayperiodstart;
                    string dateto = ddlyear + '-' + ddlmonth + '-' + firstpayperiodend;

                    if (firstpayperiodend == "End of month")
                    {
                        int x = DateTime.DaysInMonth(Convert.ToInt32(ddlyear), Convert.ToInt32(ddlmonth));
                        firstpayperiodend = x.ToString();
                    }

                    if (firstpayperiodstart == "1" || firstpayperiodstart == "2" || firstpayperiodstart == "3" || firstpayperiodstart == "4" || firstpayperiodstart == "5"
                        || firstpayperiodstart == "6" || firstpayperiodstart == "7" || firstpayperiodstart == "8" || firstpayperiodstart == "9")
                    {
                        hoursdatefrom = ddlyear + '-' + ddlmonth + '-' + ("0" + firstpayperiodstart);
                    }
                    else
                    {
                        hoursdatefrom = ddlyear + '-' + ddlmonth + '-' + firstpayperiodstart;
                    }
                    if (firstpayperiodend == "1" || firstpayperiodend == "2" || firstpayperiodend == "3" || firstpayperiodend == "4" || firstpayperiodend == "5"
                        || firstpayperiodend == "6" || firstpayperiodend == "7" || firstpayperiodend == "8" || firstpayperiodend == "9")
                    {
                        hoursdateto = ddlyear + '-' + ddlmonth + '-' + ("0" + firstpayperiodend);
                    }
                    else if (firstpayperiodend == "31")
                    {
                        if (ddlmonth == "02" || ddlmonth == "04" || ddlmonth == "06" || ddlmonth == "09" || ddlmonth == "11")
                        {
                            int x = DateTime.DaysInMonth(Convert.ToInt32(ddlyear), Convert.ToInt32(ddlmonth));
                            hoursdateto = ddlyear + '-' + ddlmonth + '-' + x;
                        }
                        else
                        {
                            hoursdateto = ddlyear + '-' + ddlmonth + '-' + firstpayperiodend;
                        }
                    }
                    else
                    {
                        hoursdateto = ddlyear + '-' + ddlmonth + '-' + firstpayperiodend;
                    }


                    //payslipworkedhours = PayMtd.PaySlipWorkedHours(empid, hoursdatefrom, hoursdateto);
                    //pswh = payslipworkedhours.Rows.Count;
                    //payperiod = datefrom + '/' + dateto;
                    payperiod = hoursdatefrom + '-' + hoursdateto;
                    paysliprecordspayslipdetails = PayMtd.PaySlipRecordsPaySlipDetails(empid, payperiod);
                    //psrpsd = paysliprecordspayslipdetails.Rows.Count;
                    //paysliprecordsallowanceamount = PayMtd.getPayslipRecordsAllowanceAmount(empid);
                    //paysliprecordsbonuses = PayMtd.getPayslipRecordsBonuses(empid, datefrom + "/" + dateto);
                    //paysliprecordscompbenempcatbonuses = PayMtd.getPayslipCompBenEmpCatBonuses(EmpLevel, EmpCategory, hoursdatefrom, hoursdateto, bonusdayfrom, bonusdayto);
                    //BonusPayoutCompute(EmpLevel, EmpCategory, empid,hoursdatefrom,hoursdateto);
                    //BonusPayoutMonth(empid, hoursdatefrom, hoursdateto);
                    //CompBenDeminimis(empid, EmpCategory, hoursdatefrom, hoursdateto);
                    //PayrollDisputes(empid, hoursdatefrom, hoursdateto);
                    //SSSLoans(empid, hoursdatefrom, hoursdateto);
                    //PayrollAdjustments(empid, hoursdatefrom + '-' + hoursdateto);
                }
                #endregion

                #endregion


                string pathmoto = "PaySlipDefault"; //LatestPayslipForm.Rows[0][0].ToString();
                PdfReader reader1 = new PdfReader(HttpContext.Current.Server.MapPath("~/pdf/") + "PaySlipDefault.pdf");
                LocationTextExtractionStrategy lteStrategy = new LocationTextExtractionStrategy();
                string strExact = PdfTextExtractor.GetTextFromPage(reader1, 1, lteStrategy);
                StringBuilder sb = new StringBuilder();
                //this code used for replacing the found text
                sb.Append(PdfTextExtractor.GetTextFromPage(reader1, 1));
                #region Passing Retrieved Datas
                sb.Replace("[Employee ID]", empid);
                if (paysliprecordsempmaster.Rows.Count > 0)
                {
                    sb.Replace("[Employee Name]", paysliprecordsempmaster.Rows[0][0].ToString() == "" ? "N/A" : paysliprecordsempmaster.Rows[0][0].ToString());
                    sb.Replace("[Date Joined]", paysliprecordsempmaster.Rows[0][2].ToString() == "" ? "N/A" : paysliprecordsempmaster.Rows[0][2].ToString());
                    sb.Replace("[Resigned]", paysliprecordsempmaster.Rows[0][3].ToString() == "" ? "N/A" : paysliprecordsempmaster.Rows[0][3].ToString());
                }
                else
                {
                    sb.Replace("[Employee Name]", "N/A");
                    sb.Replace("[Date Joined]", "N/A");
                    sb.Replace("[Resigned]", "N/A");
                }
                if (paysliprecordsconfidentialinfo.Rows.Count > 0)
                {
                    sb.Replace("[HDMF]", paysliprecordsconfidentialinfo.Rows[0][0].ToString() == "" ? "N/A" : paysliprecordsconfidentialinfo.Rows[0][0].ToString());
                    sb.Replace("[PhilHealth No]", paysliprecordsconfidentialinfo.Rows[0][1].ToString() == "" ? "N/A" : paysliprecordsconfidentialinfo.Rows[0][1].ToString());
                    sb.Replace("[SSS No]", paysliprecordsconfidentialinfo.Rows[0][2].ToString() == "" ? "N/A" : paysliprecordsconfidentialinfo.Rows[0][2].ToString());
                    sb.Replace("[TIN]", paysliprecordsconfidentialinfo.Rows[0][3].ToString() == "" ? "N/A" : paysliprecordsconfidentialinfo.Rows[0][3].ToString());
                    sb.Replace("[Tax Category]", paysliprecordsconfidentialinfo.Rows[0][4].ToString() == "" ? "N/A" : paysliprecordsconfidentialinfo.Rows[0][4].ToString());
                }
                else
                {
                    sb.Replace("[HDMF]", "N/A");
                    sb.Replace("[PhilHealth No]", "N/A");
                    sb.Replace("[SSS No]", "N/A");
                    sb.Replace("[TIN]", "N/A");
                    sb.Replace("[Tax Category]", "N/A");
                }
                if (paysliprecordsemployeepay.Rows.Count > 0)
                {

                    sb.Replace("[Bank Account]", paysliprecordsemployeepay.Rows[0][0].ToString() == "" ? "N/A" : paysliprecordsemployeepay.Rows[0][0].ToString());
                    sb.Replace("[Hourly Rate]", paysliprecordsemployeepay.Rows[0][1].ToString() == "" ? "0.00" : paysliprecordsemployeepay.Rows[0][1].ToString());
                }
                else
                {
                    sb.Replace("[Bank Account]", "N/A");
                    sb.Replace("[Hourly Rate]", "N/A");
                }
                sb.Replace("[Pay Period]", payperiod == "" ? "N/A" : payperiod);
                if (paysliprecordspayslipdetails.Rows.Count > 0)
                {
                    sb.Replace("[Pay Day]", paysliprecordspayslipdetails.Rows[0][1].ToString() == "" ? "N/A" : paysliprecordspayslipdetails.Rows[0][1].ToString());
                    sb.Replace("[Basic Salary]", paysliprecordspayslipdetails.Rows[0][2].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][2].ToString())));
                    sb.Replace("[Non Taxable Allowance]", paysliprecordspayslipdetails.Rows[0][3].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][3].ToString())));
                    sb.Replace("[Regular Night Differential]", paysliprecordspayslipdetails.Rows[0][4].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][4].ToString())));
                    sb.Replace("[Regular Over Time]", paysliprecordspayslipdetails.Rows[0][5].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][5].ToString())));
                    sb.Replace("[Regular Holiday]", paysliprecordspayslipdetails.Rows[0][6].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][6].ToString())));
                    sb.Replace("[Regular Holiday Night Differential]", paysliprecordspayslipdetails.Rows[0][7].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][7].ToString())));
                    sb.Replace("[Regular Holiday Over Time]", paysliprecordspayslipdetails.Rows[0][8].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][8].ToString())));
                    sb.Replace("[Paid Time Off]", paysliprecordspayslipdetails.Rows[0][9].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][9].ToString())));
                    sb.Replace("[Special Holiday]", paysliprecordspayslipdetails.Rows[0][10].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][10].ToString())));
                    sb.Replace("[Special Holiday Night Differential]", paysliprecordspayslipdetails.Rows[0][11].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][11].ToString())));
                    sb.Replace("[Special Holiday Over Time]", paysliprecordspayslipdetails.Rows[0][12].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][12].ToString())));
                    sb.Replace("[Allowance]", paysliprecordspayslipdetails.Rows[0][13].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][13].ToString())));
                    sb.Replace("[Bonuses]", paysliprecordspayslipdetails.Rows[0][14].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][14].ToString())));
                    sb.Replace("[Adjustments]", paysliprecordspayslipdetails.Rows[0][15].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][15].ToString())));
                    sb.Replace("[Total Salary]", paysliprecordspayslipdetails.Rows[0][16].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][16].ToString())));
                    sb.Replace("[Employee HDMF]", paysliprecordspayslipdetails.Rows[0][17].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][17].ToString())));
                    sb.Replace("[Employee Philhealth]", paysliprecordspayslipdetails.Rows[0][18].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][18].ToString())));
                    sb.Replace("[Employee SSS]", paysliprecordspayslipdetails.Rows[0][19].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][19].ToString())));
                    sb.Replace("[Employee WithHoldingTax]", paysliprecordspayslipdetails.Rows[0][20].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][20].ToString())));
                    sb.Replace("[SSS Loan Deduction]", paysliprecordspayslipdetails.Rows[0][21].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][21].ToString())));
                    sb.Replace("[Pag-ibig Loan Deduction]", paysliprecordspayslipdetails.Rows[0][22].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][22].ToString())));
                    sb.Replace("[Net Pay]", paysliprecordspayslipdetails.Rows[0][23].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][23].ToString())));
                    sb.Replace("[Employer HDMF]", paysliprecordspayslipdetails.Rows[0][24].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][24].ToString())));
                    sb.Replace("[Employer Philhealth]", paysliprecordspayslipdetails.Rows[0][25].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][25].ToString())));
                    sb.Replace("[Employer SSS]", paysliprecordspayslipdetails.Rows[0][26].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][26].ToString())));
                    sb.Replace("[Employer SSS EC]", paysliprecordspayslipdetails.Rows[0][27].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][27].ToString())));
                }
                else
                {
                    sb.Replace("[Pay Day]", "N/A");
                    sb.Replace("[Basic Salary]", "N/A");
                    sb.Replace("[Non Taxable Allowance]", "N/A");
                    sb.Replace("[Regular Night Differential]", "N/A");
                    sb.Replace("[Regular Over Time]", "N/A");
                    sb.Replace("[Regular Holiday]", "N/A");
                    sb.Replace("[Regular Holiday Night Differential]", "N/A");
                    sb.Replace("[Regular Holiday Over Time]", "N/A");
                    sb.Replace("[Paid Time Off]", "N/A");
                    sb.Replace("[Special Holiday]", "N/A");
                    sb.Replace("[Special Holiday Night Differential]", "N/A");
                    sb.Replace("[Special Holiday Over Time]", "N/A");
                    sb.Replace("[Allowance]", "N/A");
                    sb.Replace("[Bonuses]", "N/A");
                    sb.Replace("[Adjustments]", "N/A");
                    sb.Replace("[Total Salary]", "N/A");
                    sb.Replace("[Employee HDMF]", "N/A");
                    sb.Replace("[Employee Philhealth]", "N/A");
                    sb.Replace("[Employee SSS]", "N/A");
                    sb.Replace("[Employee WithHoldingTax]", "N/A");
                    sb.Replace("[SSS Loan Deduction]", "N/A");
                    sb.Replace("[Pag-ibig Loan Deduction]", "N/A");
                    sb.Replace("[Net Pay]", "N/A");
                    sb.Replace("[Employer HDMF]", "N/A");
                    sb.Replace("[Employer Philhealth]", "N/A");
                    sb.Replace("[Employer SSS]", "N/A");
                    sb.Replace("[Employer SSS EC]", "N/A");
                }

                #endregion

                FileStream Newstream = new FileStream(string.Concat(HttpContext.Current.Server.MapPath("~/pdf/"), empid + pathmoto + ".pdf"), FileMode.Create);
                MemoryStream ms = new MemoryStream();
                Document document = new Document(PageSize.A4, 25, 25, 30, 30);
                //Code for adding the string builder sb to the document
                PdfWriter writer = PdfWriter.GetInstance(document, Newstream);
                document.Open();
                document.Add(new Paragraph(sb.ToString()));
                document.Close();
                writer.Close();
                //HttpContext.Current.Response.ContentType = "pdf/application";
                //HttpContext.Current.Response.AddHeader("content-disposition", ("attachment;filename=" + empid + "newsamplepayslip.pdf"));
                //HttpContext.Current.Response.OutputStream.Write(ms.GetBuffer(), 0, ms.GetBuffer().Length);

                //viewpdf.Attributes.Add("src", "pdf\\" + empid + pathmoto + ".pdf");
                #endregion
            }
            else
            {

                #region Fixed payslip

                //indx = DDLPayPeriod;
                GetEmpLevelandCategory = PayMtd.GetEmpLevelandCategory(empid);
                int psrpsd = 0;
                int pswh = 0;

                paysliprecordspayperiod = PayMtd.PaySlipRecordsPayPeriod(EmpLevel, EmpCategory);


                if (paysliprecordspayperiod.Rows.Count > 0)
                {
                    string ddlmonth = DDLMonths.ToString();
                    string ddlyear = DDLYear.ToString();
                    string stringtobesplitted = DDLPayPeriod.ToString();
                    string[] words = stringtobesplitted.Split('-');
                    string firstpayperiodstart = words[0].ToString();
                    string firstpayperiodend;
                    if (words[1].ToString() == "End of month")
                    {
                        int a, b;
                        a = Convert.ToInt32(ddlmonth);
                        b = Convert.ToInt32(ddlyear);
                        int x = DateTime.DaysInMonth(b, a);
                        firstpayperiodend = x.ToString();
                    }
                    else
                    {
                        firstpayperiodend = words[1].ToString();
                    }

                    //string firstpayperiodstart = paysliprecordspayperiod.Rows[indx][0].ToString();
                    //string firstpayperiodend = paysliprecordspayperiod.Rows[indx][1].ToString();
                    string bonusdayfrom = firstpayperiodstart;
                    string bonusdayto = firstpayperiodend;

                    string datefrom = ddlyear + '-' + ddlmonth + '-' + firstpayperiodstart;
                    string dateto = ddlyear + '-' + ddlmonth + '-' + firstpayperiodend;

                    if (firstpayperiodend == "End of month")
                    {
                        int x = DateTime.DaysInMonth(Convert.ToInt32(ddlyear), Convert.ToInt32(ddlmonth));
                        firstpayperiodend = x.ToString();
                    }

                    if (firstpayperiodstart == "1" || firstpayperiodstart == "2" || firstpayperiodstart == "3" || firstpayperiodstart == "4" || firstpayperiodstart == "5"
                        || firstpayperiodstart == "6" || firstpayperiodstart == "7" || firstpayperiodstart == "8" || firstpayperiodstart == "9")
                    {
                        hoursdatefrom = ddlyear + '-' + ddlmonth + '-' + ("0" + firstpayperiodstart);
                    }
                    else
                    {
                        hoursdatefrom = ddlyear + '-' + ddlmonth + '-' + firstpayperiodstart;
                    }
                    if (firstpayperiodend == "1" || firstpayperiodend == "2" || firstpayperiodend == "3" || firstpayperiodend == "4" || firstpayperiodend == "5"
                        || firstpayperiodend == "6" || firstpayperiodend == "7" || firstpayperiodend == "8" || firstpayperiodend == "9")
                    {
                        hoursdateto = ddlyear + '-' + ddlmonth + '-' + ("0" + firstpayperiodend);
                    }
                    else if (firstpayperiodend == "31")
                    {
                        if (ddlmonth == "02" || ddlmonth == "04" || ddlmonth == "06" || ddlmonth == "09" || ddlmonth == "11")
                        {
                            int x = DateTime.DaysInMonth(Convert.ToInt32(ddlyear), Convert.ToInt32(ddlmonth));
                            hoursdateto = ddlyear + '-' + ddlmonth + '-' + x;
                        }
                        else
                        {
                            hoursdateto = ddlyear + '-' + ddlmonth + '-' + firstpayperiodend;
                        }
                    }
                    else
                    {
                        hoursdateto = ddlyear + '-' + ddlmonth + '-' + firstpayperiodend;
                    }

                    //payslipworkedhours = PayMtd.PaySlipWorkedHours(empid, hoursdatefrom, hoursdateto);
                    //pswh = payslipworkedhours.Rows.Count;
                    payperiod = hoursdatefrom + '-' + hoursdateto;
                    //psrpsd = paysliprecordspayslipdetails.Rows.Count;
                    //paysliprecordsallowanceamount = PayMtd.getPayslipRecordsAllowanceAmount(empid);
                    //paysliprecordsbonuses = PayMtd.getPayslipRecordsBonuses(empid, datefrom + "/" + dateto);
                    //paysliprecordscompbenempcatbonuses = PayMtd.getPayslipCompBenEmpCatBonuses(EmpLevel, EmpCategory, hoursdatefrom, hoursdateto, bonusdayfrom, bonusdayto);
                    //BonusPayoutMonth(empid, hoursdatefrom, hoursdateto);
                    //CompBenDeminimis(empid, EmpCategory, hoursdatefrom, hoursdateto);
                    //PayrollDisputes(empid, hoursdatefrom, hoursdateto);
                    //SSSLoans(empid, hoursdatefrom, hoursdateto);
                    //PayrollAdjustments(empid, hoursdatefrom + '-' + hoursdateto);
                    paysliprecordspayslipdetails = PayMtd.PaySlipRecordsPaySlipDetails(empid, payperiod);
                    YearToDate = PayMtd.PayslipDetailsYearToDate(empid, ddlyear, hoursdatefrom + '-' + hoursdateto);
                }


                //Response.Redirect("~/pdf/BIR19021.pdf"); // view pdf
                string path = "https://ws.durusthr.com/ILM_WS_Live/pdf";
                var pdfReader1 = new PdfReader(path + "/PaySlipDefault.pdf");
                AcroFields af = pdfReader1.AcroFields;
                //LocationTextExtractionStrategy lteStrategy = new LocationTextExtractionStrategy();
                //string strExact = PdfTextExtractor.GetTextFromPage(pdfReader1,1, lteStrategy);
                //StringBuilder sb = new StringBuilder();
                string sam = "";
                foreach (var field in af.Fields)
                {
                    sam = sam + field.Key + "-"; // names of textfields
                }
                //viewpdf.InnerText = sam;
                // get the file paths


                // Template file path
                string formFile = path + "/PaySlipDefault.pdf";
                string path2 = HttpContext.Current.Server.MapPath("pdf");
                // Output file path
                string newFile = path2 + "/PaySlip-" + empid + ".pdf";


                // read the template file
                PdfReader reader = new PdfReader(formFile);


                // instantiate PDFStamper object
                // The Output file will be created from template file and edited by the PDFStamper
                PdfStamper stamper = new PdfStamper(reader, new FileStream(newFile, FileMode.Create));


                // Object to deal with the Output file's textfields
                AcroFields fields = stamper.AcroFields;

                // set form fields("Field Name in PDF", "String to be placed in this PDF text field");
                //for (int i = 0; i <= 1000; i++)
                //{
                //    string ii = i.ToString();
                //    
                //    fields.SetField("untitled" + i, "vt" + ii); //temporary values
                //}

                //------------------------------------------------
                //loop to display all field names
                if (paysliprecordsempmaster.Rows.Count > 0)
                {
                    //fields.SetFieldProperty("txtempid", "setflags", PdfAnnotation.FLAGS_HIDDEN,null);
                    fields.SetField("untitled2", paysliprecordsempmaster.Rows[0][0].ToString());//Employee Name
                    fields.SetField("employeeno", empid);//Employee No
                    fields.SetField("joined", paysliprecordsempmaster.Rows[0][2].ToString());//Joined
                    fields.SetField("resigned", paysliprecordsempmaster.Rows[0][3].ToString());//Resigned
                }
                else
                {

                }
                if (paysliprecordsconfidentialinfo.Rows.Count > 0)
                {
                    fields.SetField("pagibigno", paysliprecordsconfidentialinfo.Rows[0][0].ToString());//Pag-IBIG No
                    fields.SetField("philhealthno", paysliprecordsconfidentialinfo.Rows[0][1].ToString());//PhilHealth No
                    fields.SetField("sssno", paysliprecordsconfidentialinfo.Rows[0][2].ToString());//SSS No
                    fields.SetField("taxno", paysliprecordsconfidentialinfo.Rows[0][3].ToString());//Tax No
                    fields.SetField("taxcategory", paysliprecordsconfidentialinfo.Rows[0][4].ToString());//Tax Category
                }
                else
                {

                }
                if (paysliprecordsemployeepay.Rows.Count > 0)
                {
                    fields.SetField("bankaccount", paysliprecordsemployeepay.Rows[0][0].ToString());//Bank Account
                    fields.SetField("hourlyrate", paysliprecordsemployeepay.Rows[0][1].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordsemployeepay.Rows[0][1].ToString())));//Hourly Rate
                }
                else
                {
                    fields.SetField("hourlyrate", "0.00");
                }
                fields.SetField("payperiod", payperiod);//Pay Period
                if (paysliprecordspayslipdetails.Rows.Count > 0)
                {
                    fields.SetField("payday", paysliprecordspayslipdetails.Rows[0][1].ToString());//Pay Day
                    fields.SetField("basicsalarycurrent", paysliprecordspayslipdetails.Rows[0][2].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][2].ToString())));//Basic Salary
                    fields.SetField("nontaxableallowancecurrent", paysliprecordspayslipdetails.Rows[0][3].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][3].ToString())));//Non Taxable Allowance
                    fields.SetField("regularnightdiffcurrent", paysliprecordspayslipdetails.Rows[0][4].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][4].ToString())));//Regular Night Diff
                    fields.SetField("regularotcurrent", paysliprecordspayslipdetails.Rows[0][5].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][5].ToString())));//Regular Overtime
                    fields.SetField("regularholidaycurrent", paysliprecordspayslipdetails.Rows[0][6].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][6].ToString())));//Regular Holiday
                    fields.SetField("regularholidaynightdiffcurrent", paysliprecordspayslipdetails.Rows[0][7].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][7].ToString())));//Regular Holiday Night Diff
                    fields.SetField("regularholidayotcurrent", paysliprecordspayslipdetails.Rows[0][8].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][8].ToString())));//Regular Overtime
                    fields.SetField("paidtimeoffcurrent", paysliprecordspayslipdetails.Rows[0][9].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][9].ToString())));//Paid Time Off
                    fields.SetField("specialholidaycurrent", paysliprecordspayslipdetails.Rows[0][10].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][10].ToString())));//Special Holiday
                    fields.SetField("specialholidaynightdiffcurrent", paysliprecordspayslipdetails.Rows[0][11].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][11].ToString())));//Special Holiday Night Diff
                    fields.SetField("specialholidayotcurrent", paysliprecordspayslipdetails.Rows[0][12].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][12].ToString())));//Special Holiday OT
                    fields.SetField("allowancescurrent", paysliprecordspayslipdetails.Rows[0][13].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][13].ToString())));//Allowances
                    fields.SetField("bonuscurrent", paysliprecordspayslipdetails.Rows[0][14].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][14].ToString())));//Bonus
                    fields.SetField("adjustmentscurrent", paysliprecordspayslipdetails.Rows[0][15].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][15].ToString())));//adjustments
                    fields.SetField("totalcurrent", paysliprecordspayslipdetails.Rows[0][16].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][16].ToString())));//Gross Pay
                    fields.SetField("employeehdmfcurrent", paysliprecordspayslipdetails.Rows[0][17].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][17].ToString())));//Employee HDMF
                    fields.SetField("employeephilhealthcurrent", paysliprecordspayslipdetails.Rows[0][18].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][18].ToString())));//Employee PhilHealth
                    fields.SetField("employeesocialsecuritycurrent", paysliprecordspayslipdetails.Rows[0][19].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][19].ToString())));//Employee SSS
                    fields.SetField("withholdingtaxcurrent", paysliprecordspayslipdetails.Rows[0][20].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][20].ToString())));//Employee WithHoldingTax
                    fields.SetField("sssloandeductioncurrent", paysliprecordspayslipdetails.Rows[0][21].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][21].ToString())));//Loans SSS
                    fields.SetField("pagibigloandeductioncurrent", paysliprecordspayslipdetails.Rows[0][22].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][22].ToString())));//Loans Pagibig
                    fields.SetField("netpaycurrent", paysliprecordspayslipdetails.Rows[0][23].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][23].ToString())));//Net Pay
                    fields.SetField("employerhdmfcurrent", paysliprecordspayslipdetails.Rows[0][24].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][24].ToString())));//Employer HDMF
                    fields.SetField("employerphilhealthcurrent", paysliprecordspayslipdetails.Rows[0][25].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][25].ToString())));//Employer PhilHealth
                    fields.SetField("employersocialsecuritycurrent", paysliprecordspayslipdetails.Rows[0][26].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][26].ToString())));//Employer SSS
                    fields.SetField("employersocialsecurityeccurrent", paysliprecordspayslipdetails.Rows[0][27].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][27].ToString())));//Employer SSS EC
                }
                else
                {

                }

                if (YearToDate.Rows.Count > 0)
                {
                    fields.SetField("basicsalaryytd", YearToDate.Rows[0][0].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][0].ToString())));//Basic Salary
                    fields.SetField("nontaxableallowanceytd", YearToDate.Rows[0][1].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][1].ToString())));//Non Taxable Allowance
                    fields.SetField("regularnightdiffytd", YearToDate.Rows[0][2].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][2].ToString())));//Regular Night Diff
                    fields.SetField("regularotytd", YearToDate.Rows[0][3].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][3].ToString())));//Regular OT
                    fields.SetField("regularholidayytd", YearToDate.Rows[0][4].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][4].ToString())));//Regular Holiday
                    fields.SetField("regularholidaynightdiffytd", YearToDate.Rows[0][5].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][5].ToString())));//Regular Holiday Night Diff
                    fields.SetField("regularholidayotytd", YearToDate.Rows[0][6].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][6].ToString())));//Regular Holiday OT
                    fields.SetField("paidtimeoffytd", YearToDate.Rows[0][7].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][7].ToString())));//Paid Time Off
                    fields.SetField("specialholidayytd", YearToDate.Rows[0][8].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][8].ToString())));//Special Holiday
                    fields.SetField("specialholidaynightdiffytd", YearToDate.Rows[0][9].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][9].ToString())));//Special Holiday Night Diff
                    fields.SetField("specialholidayotytd", YearToDate.Rows[0][10].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][10].ToString())));//Special Holiday OT
                    fields.SetField("allowancesytd", YearToDate.Rows[0][11].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][11].ToString())));//Allowances
                    fields.SetField("bonusytd", YearToDate.Rows[0][12].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][12].ToString())));//Bonus
                    fields.SetField("adjustmentsytd", YearToDate.Rows[0][13].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][13].ToString())));//Adjustments
                    fields.SetField("totalytd", YearToDate.Rows[0][14].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][14].ToString())));//Gross Pay/Total
                    fields.SetField("employeehdmfytd", YearToDate.Rows[0][15].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][15].ToString())));//Employee HDMF
                    fields.SetField("employeephilhealthytd", YearToDate.Rows[0][16].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][16].ToString())));//Employee PhilHealth
                    fields.SetField("employeesocialsecurityytd", YearToDate.Rows[0][17].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][17].ToString())));//Employee Social Security
                    fields.SetField("withholdingtaxytd", YearToDate.Rows[0][18].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][18].ToString())));//WithHolding Tax
                    fields.SetField("sssloandeductionytd", YearToDate.Rows[0][19].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][19].ToString())));//SSS Loan Deduction
                    fields.SetField("pagibigloandeductionytd", YearToDate.Rows[0][20].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][20].ToString())));//Pag-ibig Loan Deduction
                    fields.SetField("netpayytd", YearToDate.Rows[0][21].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][21].ToString())));//Net Pay
                    fields.SetField("employerhdmfytd", YearToDate.Rows[0][22].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][22].ToString())));//Employer HDMF
                    fields.SetField("employerphilhealthytd", YearToDate.Rows[0][23].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][23].ToString())));//Employer PhilHealth
                    fields.SetField("employersocialsecurityytd", YearToDate.Rows[0][24].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][24].ToString())));//Employer Social Security
                    fields.SetField("employersocialsecurityecytd", YearToDate.Rows[0][25].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][25].ToString())));//Employer Social Security EC
                }
                else
                {

                }



                //foreach (var field in af.Fields)
                //{
                //    Console.WriteLine("{0}, {1}", field.Key, field.Value);
                //    //string sample = TextBox2.Text;
                //    string sample2 = field.Key.ToString();
                //    //TextBox2.Text = sample + field.Key.ToString() + ',';
                //    fields.SetField(field.Key.ToString(), field.ToString());
                //}


                //------------------------------------------------

                // form flattening rids the form of editable text fields so
                // the final output can't be edited
                stamper.FormFlattening = true;
                // closing the stamper
                stamper.Close();
                //viewpdf.Attributes.Add("src", "pdf\\PaySlipDefault-" + empid + "-" + hoursdatefrom + "-" + hoursdateto + ".pdf");
                //Response.Redirect("~/pdf/PaySlip-I101.pdf");
                #endregion
            }
            //}

            #endregion



            return "Success";


            //HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.BadRequest);
            //    HttpResponseMessage responses = Request.CreateResponse(HttpStatusCode.OK);

            //    GetEmpLevelandCategory = PayMtd.GetEmpLevelandCategory(empid);
            //    string EmpLevel = GetEmpLevelandCategory.Rows[0][0].ToString();
            //    string EmpCategory = GetEmpLevelandCategory.Rows[0][1].ToString();

            //    try
            //    {            

            //        PdfReader reader = new PdfReader("http://192.168.254.250:81/pdf/PaySlipDefault.pdf");
            //        PdfStamper stamper = new PdfStamper(reader, new FileStream(string.Concat(HttpContext.Current.Server.MapPath("~/pdf/"), "Payslip-"+ uProfile.NTID +".pdf"), FileMode.Create));


            //        // Object to deal with the Output file's textfields
            //        AcroFields fields = stamper.AcroFields;

            //        fields.SetField("txtempid", uProfile.NTID);//Employee No

            //        MemoryStream ms = new MemoryStream();
            //        stamper.FormFlattening = true;
            //        // closing the stamper
            //        stamper.Close();
            //        return "Success";

            //    }
            //     catch
            //      {
            //        return "Error";

            //      }


        }


        [HttpPost]
        [Route("GetCOE")]
        //{
        //"NTID": "i011"
        //}
        public string GetCOE(UserProfile uProfile)
        {
            Connection Connection = new Connection();
            DataTable dtPAYSLIP = new DataTable();
            dtPAYSLIP = Connection.GetDataTable("sp_GetActiveCOE_Mobile");
            if (dtPAYSLIP.Rows.Count > 0)
            {

                foreach (DataRow row in dtPAYSLIP.Rows)
                {
                    // GetPayslipResult.paysliplink = "http://illimitadodevs.azurewebsites.net/pdf/" + row["PDF"].ToString() + "-";
                    DataTable getCOE = PayMtd.getdataforCOE(uProfile.NTID);

                    try
                    {
                        //PdfReader reader1 = new PdfReader("http://192.168.1.250:81/pdf/" + row["PDF"].ToString() + ".pdf");
                        PdfReader reader1 = new PdfReader("https://ws.durusthr.com/ILM_WS_Live/pdf/" + row["PDF"].ToString() + ".pdf");
                        // PdfReader reader1 = new PdfReader(HttpContext.Current.Server.MapPath("~/pdf/") + row["PDF"].ToString() + ".pdf");
                        LocationTextExtractionStrategy lteStrategy = new LocationTextExtractionStrategy();
                        string strExact = PdfTextExtractor.GetTextFromPage(reader1, 1, lteStrategy);
                        StringBuilder sb = new StringBuilder();
                        sb.Append(PdfTextExtractor.GetTextFromPage(reader1, 1));

                        sb.Replace("[Employee ID]", uProfile.NTID);
                        sb.Replace("[Employee\nID]", uProfile.NTID);
                        if (getCOE.Rows.Count > 0)
                        {
                            sb.Replace("[Employee Name]", getCOE.Rows[0][0].ToString() == "" ? "N/A" : getCOE.Rows[0][0].ToString());
                            sb.Replace("[Date Joined]", getCOE.Rows[0][1].ToString() == "" ? "N/A" : getCOE.Rows[0][1].ToString());
                            sb.Replace("[Employee\nName]", getCOE.Rows[0][0].ToString() == "" ? "N/A" : getCOE.Rows[0][0].ToString());
                            sb.Replace("[Date\nJoined]", getCOE.Rows[0][1].ToString() == "" ? "N/A" : getCOE.Rows[0][1].ToString());

                            sb.Replace("[Job Description]", getCOE.Rows[0][2].ToString() == "" ? "N/A" : getCOE.Rows[0][2].ToString());
                            sb.Replace("[Department Code]", getCOE.Rows[0][4].ToString() == "" ? "N/A" : getCOE.Rows[0][4].ToString());
                            sb.Replace("[Job\nDescription]", getCOE.Rows[0][2].ToString() == "" ? "N/A" : getCOE.Rows[0][2].ToString());
                            sb.Replace("[Department\nCode]", getCOE.Rows[0][4].ToString() == "" ? "N/A" : getCOE.Rows[0][4].ToString());

                            //Employer Details Start
                            #region Employer Details
                            sb.Replace("[Company Name]", getCOE.Rows[0]["TradingName"].ToString() == "" ? "N/A" : getCOE.Rows[0]["TradingName"].ToString());
                            sb.Replace("[Address]", getCOE.Rows[0]["Address"].ToString() == "" ? "N/A" : getCOE.Rows[0]["Address"].ToString());
                            sb.Replace("[ZIP]", getCOE.Rows[0]["AddZipCode"].ToString() == "" ? "N/A" : getCOE.Rows[0]["AddZipCode"].ToString());
                            sb.Replace("[Telephone]", getCOE.Rows[0]["ContactNumber"].ToString() == "" ? "N/A" : getCOE.Rows[0]["ContactNumber"].ToString());
                            sb.Replace("[Fax]", "N/A");
                            sb.Replace("[General Email]", getCOE.Rows[0]["Emailtxt"].ToString() == "" ? "N/A" : getCOE.Rows[0]["Emailtxt"].ToString());
                            sb.Replace("[TIN]", getCOE.Rows[0]["Tin"].ToString() == "" ? "N/A" : getCOE.Rows[0]["Tin"].ToString());
                            sb.Replace("[SSS]", getCOE.Rows[0]["Sss"].ToString() == "" ? "N/A" : getCOE.Rows[0]["Sss"].ToString());
                            sb.Replace("[Philhealth]", getCOE.Rows[0]["Philhealth"].ToString() == "" ? "N/A" : getCOE.Rows[0]["Philhealth"].ToString());
                            sb.Replace("[HDMF]", getCOE.Rows[0]["Pagibig"].ToString() == "" ? "N/A" : getCOE.Rows[0]["Pagibig"].ToString());
                            sb.Replace("[Web Page]", "N/A");
                            sb.Replace("[Company Registration]", getCOE.Rows[0]["CompanyRegistrationNo"].ToString() == "" ? "N/A" : getCOE.Rows[0]["CompanyRegistrationNo"].ToString());
                            sb.Replace("[Date of Registration]", getCOE.Rows[0]["DateOfRegistration"].ToString() == "" ? "N/A" : getCOE.Rows[0]["DateOfRegistration"].ToString());
                            sb.Replace("[Date of Registration]", getCOE.Rows[0]["DateOfRegistration"].ToString() == "" ? "N/A" : getCOE.Rows[0]["DateOfRegistration"].ToString());
                            sb.Replace("[Resigned]", "N/A");
                            #endregion
                            //Employer Details End

                            //Employee Profile Start
                            #region Employee Profile

                            sb.Replace("[Contract End Date]", "N/A");
                            sb.Replace("[Hourly Rate]", getCOE.Rows[0]["hourly"].ToString() == "" ? "N/A" : getCOE.Rows[0]["hourly"].ToString());
                            sb.Replace("[Daily Rate]", getCOE.Rows[0]["daily"].ToString() == "" ? "N/A" : getCOE.Rows[0]["daily"].ToString());
                            sb.Replace("[Monthly Rate]", getCOE.Rows[0]["monthly"].ToString() == "" ? "N/A" : getCOE.Rows[0]["monthly"].ToString());
                            sb.Replace("[Tax Status]", getCOE.Rows[0]["tax_status"].ToString() == "" ? "N/A" : getCOE.Rows[0]["tax_status"].ToString());
                            sb.Replace("[Tax Category]", "N/A");
                            #endregion
                            //Employee Profile End

                        }


                        string url = "https://ws.durusthr.com/ILM_WS_Live/pdf/" + row["PDF"].ToString() + ".pdf";
                        //string url = "http://192.168.1.250:81/pdf/" + row["PDF"].ToString() + ".pdf";
                        // The easiest way to load our document from the internet is make use of the 
                        // System.Net.WebClient class. Create an instance of it and pass the URL
                        // to download from.
                        WebClient webClient = new WebClient();

                        // Download the bytes from the location referenced by the URL.
                        byte[] dataBytes = webClient.DownloadData(url);

                        // Wrap the bytes representing the document in memory into a MemoryStream object.
                        MemoryStream byteStream = new MemoryStream(dataBytes);


                        // Stream stream = File.OpenRead(path );
                        Aspose.Pdf.Document doc = new Aspose.Pdf.Document(byteStream);

                        sb.Replace("[Signature 1]", "__________");
                        sb.Replace("[Signature 2]", "__________");
                        sb.Replace("[Signature 3]", "__________");
                        sb.Replace("[Employee Signature]", "__________");
                        sb.Replace("[Signature\n1]", "__________");
                        sb.Replace("[Signature\n2]", "__________");
                        sb.Replace("[Signature\n2]", "__________");
                        sb.Replace("[Employee\nSignature]", "__________");


                        FileStream Newstream = new FileStream(string.Concat(HttpContext.Current.Server.MapPath("~/pdf/"), "COE-" + uProfile.NTID + ".pdf"), FileMode.Create);


                        MemoryStream ms = new MemoryStream();
                        Document document = new Document(PageSize.A4, 25, 25, 30, 30);
                        //Code for adding the string builder sb to the document
                        PdfWriter writer = PdfWriter.GetInstance(document, Newstream);

                        document.Open();
                        document.Add(new Paragraph(sb.ToString()));

                        PayMtd.GetSignatureSignPicture(uProfile.NTID, "sign_1");
                        //if (PayMtd.BitSign != null)
                        //{


                        //    List<Phrase> PhraseList = new List<Phrase>();
                        //    for (int page = 1; page <= reader1.NumberOfPages; page++)
                        //    {
                        //        ITextExtractionStrategy strategy = new SimpleTextExtractionStrategy();
                        //        string currentPageText = PdfTextExtractor.GetTextFromPage(reader1, page, strategy);
                        //        if (currentPageText.Contains("[Employee Signature]") || currentPageText.Contains("[Employee\nSignature]"))
                        //        {
                        //            TextFragmentAbsorber absorber = new TextFragmentAbsorber("[Employee Signature]");


                        //            // Accept the absorber for first page
                        //            doc.Pages[1].Accept(absorber);

                        //            // View text and placement info of first text occurrence
                        //            TextFragment firstOccurrence = absorber.TextFragments[1];
                        //            // document.Add(new Paragraph(Convert.ToDouble(firstOccurrence.Position.XIndent).ToString()));
                        //            float xx = (float)firstOccurrence.Position.XIndent + 20;
                        //            float yy = (float)firstOccurrence.Position.YIndent;


                        //            string Bitsignature = null;
                        //            Bitsignature = PayMtd.BitSign;
                        //            Byte[] SignatureBit = null;
                        //            iTextSharp.text.Image signatory = null;

                        //            Bitsignature = Bitsignature.Replace(' ', '+');
                        //            SignatureBit = Convert.FromBase64String(Bitsignature);
                        //            signatory = iTextSharp.text.Image.GetInstance(SignatureBit);
                        //            signatory.SetAbsolutePosition(xx, yy);
                        //            signatory.ScaleToFit(40, 40);

                        //            document.Add(signatory);
                        //        }
                        //        if (currentPageText.Contains("[President Signature]") || currentPageText.Contains("[President\nSignature]"))
                        //        {
                        //            TextFragmentAbsorber absorber = new TextFragmentAbsorber("[President Signature]");


                        //            // Accept the absorber for first page
                        //            doc.Pages[1].Accept(absorber);

                        //            // View text and placement info of first text occurrence
                        //            TextFragment firstOccurrence = absorber.TextFragments[1];
                        //            // document.Add(new Paragraph(Convert.ToDouble(firstOccurrence.Position.XIndent).ToString()));
                        //            float xx = (float)firstOccurrence.Position.XIndent + 20;
                        //            float yy = (float)firstOccurrence.Position.YIndent;


                        //            string Bitsignature = null;
                        //            Bitsignature = PayMtd.BitSign;
                        //            Byte[] SignatureBit = null;
                        //            iTextSharp.text.Image signatory = null;

                        //            Bitsignature = Bitsignature.Replace(' ', '+');
                        //            SignatureBit = Convert.FromBase64String(Bitsignature);
                        //            signatory = iTextSharp.text.Image.GetInstance(SignatureBit);
                        //            signatory.SetAbsolutePosition(xx, yy);

                        //            signatory.ScaleToFit(40, 40);

                        //            document.Add(signatory);
                        //        }

                        //        if (currentPageText.Contains("[HR Signature]") || currentPageText.Contains("[HR\nSignature]"))
                        //        {
                        //            TextFragmentAbsorber absorber = new TextFragmentAbsorber("[HR Signature]");


                        //            // Accept the absorber for first page
                        //            doc.Pages[1].Accept(absorber);

                        //            // View text and placement info of first text occurrence
                        //            TextFragment firstOccurrence = absorber.TextFragments[1];
                        //            // document.Add(new Paragraph(Convert.ToDouble(firstOccurrence.Position.XIndent).ToString()));
                        //            float xx = (float)firstOccurrence.Position.XIndent + 20;
                        //            float yy = (float)firstOccurrence.Position.YIndent;


                        //            string Bitsignature = null;
                        //            Bitsignature = PayMtd.BitSign;
                        //            Byte[] SignatureBit = null;
                        //            iTextSharp.text.Image signatory = null;

                        //            Bitsignature = Bitsignature.Replace(' ', '+');
                        //            SignatureBit = Convert.FromBase64String(Bitsignature);
                        //            signatory = iTextSharp.text.Image.GetInstance(SignatureBit);
                        //            signatory.SetAbsolutePosition(xx, yy);

                        //            signatory.ScaleToFit(40, 40);

                        //            document.Add(signatory);
                        //        }

                        //    }
                        //}

                        //PayMtd.GetSignatureSignPicturePres(empid);
                        PayMtd.GetSignatureALL("Signature 1");
                        PayMtd.GetSignatureALL("Signature 2");
                        PayMtd.GetSignatureALL("Signature 3");
                        if (PayMtd.BitSign != null)
                        {


                            List<Phrase> PhraseList = new List<Phrase>();
                            for (int page = 1; page <= reader1.NumberOfPages; page++)
                            {
                                ITextExtractionStrategy strategy = new SimpleTextExtractionStrategy();
                                string currentPageText = PdfTextExtractor.GetTextFromPage(reader1, page, strategy);
                                if (currentPageText.Contains("[Employee Signature]") || currentPageText.Contains("[Employee\nSignature]"))
                                {
                                    TextFragmentAbsorber absorber = new TextFragmentAbsorber("[Employee Signature]");


                                    // Accept the absorber for first page
                                    doc.Pages[1].Accept(absorber);

                                    // View text and placement info of first text occurrence
                                    TextFragment firstOccurrence = absorber.TextFragments[1];
                                    // document.Add(new Paragraph(Convert.ToDouble(firstOccurrence.Position.XIndent).ToString()));
                                    float xx = (float)firstOccurrence.Position.XIndent + 20;
                                    float yy = (float)firstOccurrence.Position.YIndent;


                                    string Bitsignature = null;
                                    Bitsignature = PayMtd.BitSign;
                                    Byte[] SignatureBit = null;
                                    iTextSharp.text.Image signatory = null;

                                    Bitsignature = Bitsignature.Replace(' ', '+');
                                    SignatureBit = Convert.FromBase64String(Bitsignature);
                                    signatory = iTextSharp.text.Image.GetInstance(SignatureBit);
                                    signatory.SetAbsolutePosition(xx, yy);
                                    signatory.ScaleToFit(50, 50);

                                    document.Add(signatory);
                                }

                            }
                        }

                        if (PayMtd.sig1 != null)
                        {


                            List<Phrase> PhraseList = new List<Phrase>();
                            for (int page = 1; page <= reader1.NumberOfPages; page++)
                            {
                                ITextExtractionStrategy strategy = new SimpleTextExtractionStrategy();
                                string currentPageText = PdfTextExtractor.GetTextFromPage(reader1, page, strategy);
                                if (currentPageText.Contains("[Signature 1]") || currentPageText.Contains("[Signature\n1]"))
                                {
                                    TextFragmentAbsorber absorber = new TextFragmentAbsorber("[Signature 1]");


                                    // Accept the absorber for first page
                                    doc.Pages[1].Accept(absorber);

                                    // View text and placement info of first text occurrence
                                    TextFragment firstOccurrence = absorber.TextFragments[1];
                                    // document.Add(new Paragraph(Convert.ToDouble(firstOccurrence.Position.XIndent).ToString()));
                                    float xx = (float)firstOccurrence.Position.XIndent + 20;
                                    float yy = (float)firstOccurrence.Position.YIndent;


                                    string Bitsignature = null;
                                    Bitsignature = PayMtd.sig1;
                                    Byte[] SignatureBit = null;
                                    iTextSharp.text.Image signatory = null;

                                    Bitsignature = Bitsignature.Replace(' ', '+');
                                    SignatureBit = Convert.FromBase64String(Bitsignature);
                                    signatory = iTextSharp.text.Image.GetInstance(SignatureBit);
                                    signatory.SetAbsolutePosition(xx, yy);

                                    signatory.ScaleToFit(50, 50);

                                    document.Add(signatory);
                                }


                            }
                        }

                        if (PayMtd.sig2 != null)
                        {


                            List<Phrase> PhraseList = new List<Phrase>();
                            for (int page = 1; page <= reader1.NumberOfPages; page++)
                            {
                                ITextExtractionStrategy strategy = new SimpleTextExtractionStrategy();
                                string currentPageText = PdfTextExtractor.GetTextFromPage(reader1, page, strategy);
                                if (currentPageText.Contains("[Signature 2]") || currentPageText.Contains("[Signature\n2]"))
                                {
                                    TextFragmentAbsorber absorber = new TextFragmentAbsorber("[Signature 2]");


                                    // Accept the absorber for first page
                                    doc.Pages[1].Accept(absorber);

                                    // View text and placement info of first text occurrence
                                    TextFragment firstOccurrence = absorber.TextFragments[1];
                                    // document.Add(new Paragraph(Convert.ToDouble(firstOccurrence.Position.XIndent).ToString()));
                                    float xx = (float)firstOccurrence.Position.XIndent + 20;
                                    float yy = (float)firstOccurrence.Position.YIndent;


                                    string Bitsignature = null;
                                    Bitsignature = PayMtd.sig2;
                                    Byte[] SignatureBit = null;
                                    iTextSharp.text.Image signatory = null;

                                    Bitsignature = Bitsignature.Replace(' ', '+');
                                    SignatureBit = Convert.FromBase64String(Bitsignature);
                                    signatory = iTextSharp.text.Image.GetInstance(SignatureBit);
                                    signatory.SetAbsolutePosition(xx, yy);

                                    signatory.ScaleToFit(50, 50);

                                    document.Add(signatory);
                                }


                            }
                        }

                        if (PayMtd.sig3 != null)
                        {


                            List<Phrase> PhraseList = new List<Phrase>();
                            for (int page = 1; page <= reader1.NumberOfPages; page++)
                            {
                                ITextExtractionStrategy strategy = new SimpleTextExtractionStrategy();
                                string currentPageText = PdfTextExtractor.GetTextFromPage(reader1, page, strategy);
                                if (currentPageText.Contains("[Signature 3]") || currentPageText.Contains("[Signature\n3]"))
                                {
                                    TextFragmentAbsorber absorber = new TextFragmentAbsorber("[Signature 3]");


                                    // Accept the absorber for first page
                                    doc.Pages[1].Accept(absorber);

                                    // View text and placement info of first text occurrence
                                    TextFragment firstOccurrence = absorber.TextFragments[1];
                                    // document.Add(new Paragraph(Convert.ToDouble(firstOccurrence.Position.XIndent).ToString()));
                                    float xx = (float)firstOccurrence.Position.XIndent + 20;
                                    float yy = (float)firstOccurrence.Position.YIndent;


                                    string Bitsignature = null;
                                    Bitsignature = PayMtd.sig3;
                                    Byte[] SignatureBit = null;
                                    iTextSharp.text.Image signatory = null;

                                    Bitsignature = Bitsignature.Replace(' ', '+');
                                    SignatureBit = Convert.FromBase64String(Bitsignature);
                                    signatory = iTextSharp.text.Image.GetInstance(SignatureBit);
                                    signatory.SetAbsolutePosition(xx, yy);

                                    signatory.ScaleToFit(50, 50);

                                    document.Add(signatory);
                                }


                            }
                        }






                        document.Close();
                        writer.Close();
                        // closing the stamper
                        uProfile.Status = "Success";

                    }
                    catch (Exception ex)
                    {
                        uProfile.Status = ex.ToString();
                        //this.Page.ClientScript.RegisterClientScriptBlock(this.GetType(),
                        //Guid.NewGuid().ToString("N"), "self.parent.location='login.aspx';", true);
                    }
                }
            }
            return uProfile.Status;
        }

        //public graph GetGraph()
        //{
        //    graph GetGetGraphResult = new graph();
        //    //try
        //    //{
        //    Connection Connection = new Connection();
        //    DataTable dtPAYSLIP = new DataTable();
        //    dtPAYSLIP = Connection.GetDataTable("sp_LM_Filtered");
        //    if (dtPAYSLIP.Rows.Count > 0)
        //    {

        //        foreach (DataRow row in dtPAYSLIP.Rows)
        //        {
        //            GetGetGraphResult.graphlocation =  row["Location"].ToString();
        //            GetGetGraphResult.graphtotal = row["TotalCount"].ToString();
        //            GetGetGraphResult.graphcolor = row["Color"].ToString();
        //        }
        //    }
        //    else
        //    {
        //    }

        //    return GetGetGraphResult;
        //}


        [HttpPost]
        [Route("GetAvailableBalance")]
        //{
        //"NTID": "i101",
        //"Start_Date": "2017-04-01",
        //"End_Date": "2017-04-30"
        //}
        public availablebalance GetBaking(banking bank)
        {
            availablebalance GetBankingResult = new availablebalance();
            //try
            //{
            Connection Connection = new Connection();
            DataTable dtbanking = new DataTable();
            Connection.myparameters.Add(new myParameters { ParameterName = "@userid", mytype = SqlDbType.NVarChar, Value = bank.userid });
            Connection.myparameters.Add(new myParameters { ParameterName = "@password", mytype = SqlDbType.NVarChar, Value = bank.password });

            dtbanking = Connection.GetDataTable("sp_GetMobileBankingBalance");
            if (dtbanking.Rows.Count > 0)
            {

                foreach (DataRow row in dtbanking.Rows)
                {
                    GetBankingResult.balance = row["AvailBal"].ToString();
                }
            }
            else
            {
                GetBankingResult.balance = "null";
            }

            return GetBankingResult;
        }

        [HttpPost]
        [Route("TransferBankingBalance")]

        public string TransferBankingBalance(banking bank)
        {
            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@useridTransferring", mytype = SqlDbType.NVarChar, Value = bank.useridTransferring });
            Connection.myparameters.Add(new myParameters { ParameterName = "@AmountBeingTransfer", mytype = SqlDbType.NVarChar, Value = bank.AmountBeingTransfer });
            Connection.myparameters.Add(new myParameters { ParameterName = "@useridRecipient", mytype = SqlDbType.NVarChar, Value = bank.useridRecipient });
            //  Connection.myparameters.Add(new myParameters { ParameterName = "@STARTOREND", mytype = SqlDbType.NVarChar, Value = uBreak.StartOrEnd });

            // DataTable EmployeeDt = new DataTable();
            String ID = Connection.ExecuteScalar("sp_TransferMobileBankingBalance");

            return ID;

        }


        //Loan Review
        [HttpPost]
        [Route("InsertLoan")]

        public GetLoanReviewResult InsertLoan(LoanReview LoanReview)
        {

            GetLoanReviewResult GetLoanReviewResult = new GetLoanReviewResult();

            Connection Connection = new Connection();
            List<LoanReview> LoanReviewList = new List<LoanReview>();
            DataTable dtLoanReview = new DataTable();

            Connection.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.NVarChar, Value = LoanReview.EmpID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Loan", mytype = SqlDbType.NVarChar, Value = LoanReview.Loan });
            Connection.myparameters.Add(new myParameters { ParameterName = "@ApplicationDate", mytype = SqlDbType.NVarChar, Value = LoanReview.ApplicationDate == "" ? null : Convert.ToDateTime(LoanReview.ApplicationDate).ToString("yyyy-MM-dd") });
            Connection.myparameters.Add(new myParameters { ParameterName = "@CheckReceiveDate", mytype = SqlDbType.NVarChar, Value = LoanReview.CheckReceiveDate == "" ? null : Convert.ToDateTime(LoanReview.CheckReceiveDate).ToString("yyyy-MM-dd") });
            Connection.myparameters.Add(new myParameters { ParameterName = "@CheckReleaseDate", mytype = SqlDbType.NVarChar, Value = LoanReview.CheckReleaseDate == "" ? null : Convert.ToDateTime(LoanReview.CheckReleaseDate).ToString("yyyy-MM-dd") });
            Connection.myparameters.Add(new myParameters { ParameterName = "@LoanTermValue", mytype = SqlDbType.NVarChar, Value = LoanReview.LoanTermValue });
            Connection.myparameters.Add(new myParameters { ParameterName = "@LoanTermType", mytype = SqlDbType.NVarChar, Value = LoanReview.LoanTermType });
            Connection.myparameters.Add(new myParameters { ParameterName = "@LoanAmount", mytype = SqlDbType.NVarChar, Value = LoanReview.LoanAmount });
            Connection.myparameters.Add(new myParameters { ParameterName = "@PaymentAmount", mytype = SqlDbType.NVarChar, Value = LoanReview.PaymentAmount });
            Connection.myparameters.Add(new myParameters { ParameterName = "@PaymentTerms", mytype = SqlDbType.NVarChar, Value = LoanReview.PaymentTerms });
            Connection.myparameters.Add(new myParameters { ParameterName = "@LoanBalance", mytype = SqlDbType.NVarChar, Value = LoanReview.LoanBalance });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Authorization", mytype = SqlDbType.NVarChar, Value = LoanReview.Authorization });

            dtLoanReview = Connection.GetDataTable("sp_InsertLoan_Mobile");

            if (dtLoanReview.Rows.Count > 0)
            {
                foreach (DataRow row in dtLoanReview.Rows)
                {
                    LoanReview LoanReviews = new LoanReview
                    {
                        ID = row["ID"].ToString(),
                        EmpID = row["EmpID"].ToString(),
                        Loan = row["Loan"].ToString(),
                        ApplicationDate = Convert.ToDateTime(row["DateRequested"]).ToString("yyyy-MM-dd"),
                        CheckReceiveDate = Convert.ToDateTime(row["CheckReceiveDate"]).ToString("yyyy-MM-dd"),
                        CheckReleaseDate = Convert.ToDateTime(row["CheckReleaseDate"]).ToString("yyyy-MM-dd"),
                        LoanTermValue = row["LoanTermsValue"].ToString(),
                        LoanTermType = row["LoanTermsType"].ToString(),
                        LoanAmount = row["LoanAmount"].ToString(),
                        PaymentAmount = row["PaymentAmount"].ToString(),
                        PaymentTerms = row["PaymentTerms"].ToString(),
                        LoanBalance = row["LoadBalance"].ToString(),
                        Authorization = row["AuthorizationToDeduct"].ToString(),
                        Status = row["Status"].ToString()


                    };
                    LoanReviewList.Add(LoanReviews);
                }
                GetLoanReviewResult.LoanReview = LoanReviewList;
                GetLoanReviewResult.myreturn = "Success";
            }
            else
            {
                GetLoanReviewResult.myreturn = "No Data Available";
            }

            return GetLoanReviewResult;

        }

        [HttpPost]
        [Route("ShowLoanReviewer")]

        public GetLoanReviewResult ShowLoanReviewer(LoanReview LoanReview)
        {

            GetLoanReviewResult GetLoanReviewResult = new GetLoanReviewResult();

            Connection Connection = new Connection();
            List<LoanReview> LoanReviewList = new List<LoanReview>();
            DataTable dtLoanReview = new DataTable();

            Connection.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.NVarChar, Value = LoanReview.EmpID });

            dtLoanReview = Connection.GetDataTable("sp_ShowLoanReviewer_Mobile");

            if (dtLoanReview.Rows.Count > 0)
            {
                foreach (DataRow row in dtLoanReview.Rows)
                {
                    LoanReview LoanReviews = new LoanReview
                    {
                        ID = row["ID"].ToString(),
                        EmpID = row["EmpID"].ToString(),
                        Loan = row["Loan"].ToString(),
                        ApplicationDate = Convert.ToDateTime(row["DateRequested"]).ToString("yyyy-MM-dd"),
                        CheckReceiveDate = Convert.ToDateTime(row["CheckReceiveDate"]).ToString("yyyy-MM-dd"),
                        CheckReleaseDate = Convert.ToDateTime(row["CheckReleaseDate"]).ToString("yyyy-MM-dd"),
                        LoanTermValue = row["LoanTermsValue"].ToString(),
                        LoanTermType = row["LoanTermsType"].ToString(),
                        LoanAmount = row["LoanAmount"].ToString(),
                        PaymentAmount = row["PaymentAmount"].ToString(),
                        PaymentTerms = row["PaymentTerms"].ToString(),
                        LoanBalance = row["LoadBalance"].ToString(),
                        Authorization = row["AuthorizationToDeduct"].ToString(),
                        Status = row["Status"].ToString()

                    };
                    LoanReviewList.Add(LoanReviews);
                }
                GetLoanReviewResult.LoanReview = LoanReviewList;
                GetLoanReviewResult.myreturn = "Success";
            }
            else
            {
                GetLoanReviewResult.myreturn = "No Data Available";
            }

            return GetLoanReviewResult;

        }
        //end Loan Review

        [HttpPost]
        [Route("GetCOEv2")]
        //{
        //"NTID": "i011"
        //}
        public string GetCOEv2(UserProfilev2 uProfile)
        {
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = uProfile.CN;
            SelfieRegistration2Controller.GetDB2(GDB);

            Connection Connection = new Connection();
            DataTable dtPAYSLIP = new DataTable();
            dtPAYSLIP = Connection.GetDataTable("sp_GetActiveCOE_Mobile");
            if (dtPAYSLIP.Rows.Count > 0)
            {

                foreach (DataRow row in dtPAYSLIP.Rows)
                {
                    // GetPayslipResult.paysliplink = "http://illimitadodevs.azurewebsites.net/pdf/" + row["PDF"].ToString() + "-";
                    DataTable getCOE = PayMtd.getdataforCOE(uProfile.NTID);

                    try
                    {
                        //PdfReader reader1 = new PdfReader("http://192.168.1.250:81/pdf/" + row["PDF"].ToString() + ".pdf");
                        PdfReader reader1 = new PdfReader("https://ws.durusthr.com/ILM_WS_Live/pdf/" + row["PDF"].ToString() + ".pdf");
                        // PdfReader reader1 = new PdfReader(HttpContext.Current.Server.MapPath("~/pdf/") + row["PDF"].ToString() + ".pdf");
                        LocationTextExtractionStrategy lteStrategy = new LocationTextExtractionStrategy();
                        string strExact = PdfTextExtractor.GetTextFromPage(reader1, 1, lteStrategy);
                        StringBuilder sb = new StringBuilder();
                        sb.Append(PdfTextExtractor.GetTextFromPage(reader1, 1));

                        sb.Replace("[Employee ID]", uProfile.NTID);
                        sb.Replace("[Employee\nID]", uProfile.NTID);
                        if (getCOE.Rows.Count > 0)
                        {
                            sb.Replace("[Employee Name]", getCOE.Rows[0][0].ToString() == "" ? "N/A" : getCOE.Rows[0][0].ToString());
                            sb.Replace("[Date Joined]", getCOE.Rows[0][1].ToString() == "" ? "N/A" : getCOE.Rows[0][1].ToString());
                            sb.Replace("[Employee\nName]", getCOE.Rows[0][0].ToString() == "" ? "N/A" : getCOE.Rows[0][0].ToString());
                            sb.Replace("[Date\nJoined]", getCOE.Rows[0][1].ToString() == "" ? "N/A" : getCOE.Rows[0][1].ToString());

                            sb.Replace("[Job Description]", getCOE.Rows[0][2].ToString() == "" ? "N/A" : getCOE.Rows[0][2].ToString());
                            sb.Replace("[Department Code]", getCOE.Rows[0][4].ToString() == "" ? "N/A" : getCOE.Rows[0][4].ToString());
                            sb.Replace("[Job\nDescription]", getCOE.Rows[0][2].ToString() == "" ? "N/A" : getCOE.Rows[0][2].ToString());
                            sb.Replace("[Department\nCode]", getCOE.Rows[0][4].ToString() == "" ? "N/A" : getCOE.Rows[0][4].ToString());

                            //Employer Details Start
                            #region Employer Details
                            sb.Replace("[Company Name]", getCOE.Rows[0]["TradingName"].ToString() == "" ? "N/A" : getCOE.Rows[0]["TradingName"].ToString());
                            sb.Replace("[Address]", getCOE.Rows[0]["Address"].ToString() == "" ? "N/A" : getCOE.Rows[0]["Address"].ToString());
                            sb.Replace("[ZIP]", getCOE.Rows[0]["AddZipCode"].ToString() == "" ? "N/A" : getCOE.Rows[0]["AddZipCode"].ToString());
                            sb.Replace("[Telephone]", getCOE.Rows[0]["ContactNumber"].ToString() == "" ? "N/A" : getCOE.Rows[0]["ContactNumber"].ToString());
                            sb.Replace("[Fax]", "N/A");
                            sb.Replace("[General Email]", getCOE.Rows[0]["Emailtxt"].ToString() == "" ? "N/A" : getCOE.Rows[0]["Emailtxt"].ToString());
                            sb.Replace("[TIN]", getCOE.Rows[0]["Tin"].ToString() == "" ? "N/A" : getCOE.Rows[0]["Tin"].ToString());
                            sb.Replace("[SSS]", getCOE.Rows[0]["Sss"].ToString() == "" ? "N/A" : getCOE.Rows[0]["Sss"].ToString());
                            sb.Replace("[Philhealth]", getCOE.Rows[0]["Philhealth"].ToString() == "" ? "N/A" : getCOE.Rows[0]["Philhealth"].ToString());
                            sb.Replace("[HDMF]", getCOE.Rows[0]["Pagibig"].ToString() == "" ? "N/A" : getCOE.Rows[0]["Pagibig"].ToString());
                            sb.Replace("[Web Page]", "N/A");
                            sb.Replace("[Company Registration]", getCOE.Rows[0]["CompanyRegistrationNo"].ToString() == "" ? "N/A" : getCOE.Rows[0]["CompanyRegistrationNo"].ToString());
                            sb.Replace("[Date of Registration]", getCOE.Rows[0]["DateOfRegistration"].ToString() == "" ? "N/A" : getCOE.Rows[0]["DateOfRegistration"].ToString());
                            sb.Replace("[Date of Registration]", getCOE.Rows[0]["DateOfRegistration"].ToString() == "" ? "N/A" : getCOE.Rows[0]["DateOfRegistration"].ToString());
                            sb.Replace("[Resigned]", "N/A");
                            #endregion
                            //Employer Details End

                            //Employee Profile Start
                            #region Employee Profile

                            sb.Replace("[Contract End Date]", "N/A");
                            sb.Replace("[Hourly Rate]", getCOE.Rows[0]["hourly"].ToString() == "" ? "N/A" : getCOE.Rows[0]["hourly"].ToString());
                            sb.Replace("[Daily Rate]", getCOE.Rows[0]["daily"].ToString() == "" ? "N/A" : getCOE.Rows[0]["daily"].ToString());
                            sb.Replace("[Monthly Rate]", getCOE.Rows[0]["monthly"].ToString() == "" ? "N/A" : getCOE.Rows[0]["monthly"].ToString());
                            sb.Replace("[Tax Status]", getCOE.Rows[0]["tax_status"].ToString() == "" ? "N/A" : getCOE.Rows[0]["tax_status"].ToString());
                            sb.Replace("[Tax Category]", "N/A");
                            #endregion
                            //Employee Profile End

                        }


                        string url = "https://ws.durusthr.com/ILM_WS_Live/pdf/" + row["PDF"].ToString() + ".pdf";
                        //string url = "http://192.168.1.250:81/pdf/" + row["PDF"].ToString() + ".pdf";
                        // The easiest way to load our document from the internet is make use of the 
                        // System.Net.WebClient class. Create an instance of it and pass the URL
                        // to download from.
                        WebClient webClient = new WebClient();

                        // Download the bytes from the location referenced by the URL.
                        byte[] dataBytes = webClient.DownloadData(url);

                        // Wrap the bytes representing the document in memory into a MemoryStream object.
                        MemoryStream byteStream = new MemoryStream(dataBytes);


                        // Stream stream = File.OpenRead(path );
                        Aspose.Pdf.Document doc = new Aspose.Pdf.Document(byteStream);

                        sb.Replace("[Signature 1]", "__________");
                        sb.Replace("[Signature 2]", "__________");
                        sb.Replace("[Signature 3]", "__________");
                        sb.Replace("[Employee Signature]", "__________");
                        sb.Replace("[Signature\n1]", "__________");
                        sb.Replace("[Signature\n2]", "__________");
                        sb.Replace("[Signature\n2]", "__________");
                        sb.Replace("[Employee\nSignature]", "__________");


                        FileStream Newstream = new FileStream(string.Concat(HttpContext.Current.Server.MapPath("~/pdf/"), "COE-" + uProfile.NTID + ".pdf"), FileMode.Create);


                        MemoryStream ms = new MemoryStream();
                        Document document = new Document(PageSize.A4, 25, 25, 30, 30);
                        //Code for adding the string builder sb to the document
                        PdfWriter writer = PdfWriter.GetInstance(document, Newstream);

                        document.Open();
                        document.Add(new Paragraph(sb.ToString()));

                        PayMtd.GetSignatureSignPicture(uProfile.NTID, "sign_1");
                        //if (PayMtd.BitSign != null)
                        //{


                        //    List<Phrase> PhraseList = new List<Phrase>();
                        //    for (int page = 1; page <= reader1.NumberOfPages; page++)
                        //    {
                        //        ITextExtractionStrategy strategy = new SimpleTextExtractionStrategy();
                        //        string currentPageText = PdfTextExtractor.GetTextFromPage(reader1, page, strategy);
                        //        if (currentPageText.Contains("[Employee Signature]") || currentPageText.Contains("[Employee\nSignature]"))
                        //        {
                        //            TextFragmentAbsorber absorber = new TextFragmentAbsorber("[Employee Signature]");


                        //            // Accept the absorber for first page
                        //            doc.Pages[1].Accept(absorber);

                        //            // View text and placement info of first text occurrence
                        //            TextFragment firstOccurrence = absorber.TextFragments[1];
                        //            // document.Add(new Paragraph(Convert.ToDouble(firstOccurrence.Position.XIndent).ToString()));
                        //            float xx = (float)firstOccurrence.Position.XIndent + 20;
                        //            float yy = (float)firstOccurrence.Position.YIndent;


                        //            string Bitsignature = null;
                        //            Bitsignature = PayMtd.BitSign;
                        //            Byte[] SignatureBit = null;
                        //            iTextSharp.text.Image signatory = null;

                        //            Bitsignature = Bitsignature.Replace(' ', '+');
                        //            SignatureBit = Convert.FromBase64String(Bitsignature);
                        //            signatory = iTextSharp.text.Image.GetInstance(SignatureBit);
                        //            signatory.SetAbsolutePosition(xx, yy);
                        //            signatory.ScaleToFit(40, 40);

                        //            document.Add(signatory);
                        //        }
                        //        if (currentPageText.Contains("[President Signature]") || currentPageText.Contains("[President\nSignature]"))
                        //        {
                        //            TextFragmentAbsorber absorber = new TextFragmentAbsorber("[President Signature]");


                        //            // Accept the absorber for first page
                        //            doc.Pages[1].Accept(absorber);

                        //            // View text and placement info of first text occurrence
                        //            TextFragment firstOccurrence = absorber.TextFragments[1];
                        //            // document.Add(new Paragraph(Convert.ToDouble(firstOccurrence.Position.XIndent).ToString()));
                        //            float xx = (float)firstOccurrence.Position.XIndent + 20;
                        //            float yy = (float)firstOccurrence.Position.YIndent;


                        //            string Bitsignature = null;
                        //            Bitsignature = PayMtd.BitSign;
                        //            Byte[] SignatureBit = null;
                        //            iTextSharp.text.Image signatory = null;

                        //            Bitsignature = Bitsignature.Replace(' ', '+');
                        //            SignatureBit = Convert.FromBase64String(Bitsignature);
                        //            signatory = iTextSharp.text.Image.GetInstance(SignatureBit);
                        //            signatory.SetAbsolutePosition(xx, yy);

                        //            signatory.ScaleToFit(40, 40);

                        //            document.Add(signatory);
                        //        }

                        //        if (currentPageText.Contains("[HR Signature]") || currentPageText.Contains("[HR\nSignature]"))
                        //        {
                        //            TextFragmentAbsorber absorber = new TextFragmentAbsorber("[HR Signature]");


                        //            // Accept the absorber for first page
                        //            doc.Pages[1].Accept(absorber);

                        //            // View text and placement info of first text occurrence
                        //            TextFragment firstOccurrence = absorber.TextFragments[1];
                        //            // document.Add(new Paragraph(Convert.ToDouble(firstOccurrence.Position.XIndent).ToString()));
                        //            float xx = (float)firstOccurrence.Position.XIndent + 20;
                        //            float yy = (float)firstOccurrence.Position.YIndent;


                        //            string Bitsignature = null;
                        //            Bitsignature = PayMtd.BitSign;
                        //            Byte[] SignatureBit = null;
                        //            iTextSharp.text.Image signatory = null;

                        //            Bitsignature = Bitsignature.Replace(' ', '+');
                        //            SignatureBit = Convert.FromBase64String(Bitsignature);
                        //            signatory = iTextSharp.text.Image.GetInstance(SignatureBit);
                        //            signatory.SetAbsolutePosition(xx, yy);

                        //            signatory.ScaleToFit(40, 40);

                        //            document.Add(signatory);
                        //        }

                        //    }
                        //}

                        //PayMtd.GetSignatureSignPicturePres(empid);
                        PayMtd.GetSignatureALL("Signature 1");
                        PayMtd.GetSignatureALL("Signature 2");
                        PayMtd.GetSignatureALL("Signature 3");
                        if (PayMtd.BitSign != null)
                        {


                            List<Phrase> PhraseList = new List<Phrase>();
                            for (int page = 1; page <= reader1.NumberOfPages; page++)
                            {
                                ITextExtractionStrategy strategy = new SimpleTextExtractionStrategy();
                                string currentPageText = PdfTextExtractor.GetTextFromPage(reader1, page, strategy);
                                if (currentPageText.Contains("[Employee Signature]") || currentPageText.Contains("[Employee\nSignature]"))
                                {
                                    TextFragmentAbsorber absorber = new TextFragmentAbsorber("[Employee Signature]");


                                    // Accept the absorber for first page
                                    doc.Pages[1].Accept(absorber);

                                    // View text and placement info of first text occurrence
                                    TextFragment firstOccurrence = absorber.TextFragments[1];
                                    // document.Add(new Paragraph(Convert.ToDouble(firstOccurrence.Position.XIndent).ToString()));
                                    float xx = (float)firstOccurrence.Position.XIndent + 20;
                                    float yy = (float)firstOccurrence.Position.YIndent;


                                    string Bitsignature = null;
                                    Bitsignature = PayMtd.BitSign;
                                    Byte[] SignatureBit = null;
                                    iTextSharp.text.Image signatory = null;

                                    Bitsignature = Bitsignature.Replace(' ', '+');
                                    SignatureBit = Convert.FromBase64String(Bitsignature);
                                    signatory = iTextSharp.text.Image.GetInstance(SignatureBit);
                                    signatory.SetAbsolutePosition(xx, yy);
                                    signatory.ScaleToFit(50, 50);

                                    document.Add(signatory);
                                }

                            }
                        }

                        if (PayMtd.sig1 != null)
                        {


                            List<Phrase> PhraseList = new List<Phrase>();
                            for (int page = 1; page <= reader1.NumberOfPages; page++)
                            {
                                ITextExtractionStrategy strategy = new SimpleTextExtractionStrategy();
                                string currentPageText = PdfTextExtractor.GetTextFromPage(reader1, page, strategy);
                                if (currentPageText.Contains("[Signature 1]") || currentPageText.Contains("[Signature\n1]"))
                                {
                                    TextFragmentAbsorber absorber = new TextFragmentAbsorber("[Signature 1]");


                                    // Accept the absorber for first page
                                    doc.Pages[1].Accept(absorber);

                                    // View text and placement info of first text occurrence
                                    TextFragment firstOccurrence = absorber.TextFragments[1];
                                    // document.Add(new Paragraph(Convert.ToDouble(firstOccurrence.Position.XIndent).ToString()));
                                    float xx = (float)firstOccurrence.Position.XIndent + 20;
                                    float yy = (float)firstOccurrence.Position.YIndent;


                                    string Bitsignature = null;
                                    Bitsignature = PayMtd.sig1;
                                    Byte[] SignatureBit = null;
                                    iTextSharp.text.Image signatory = null;

                                    Bitsignature = Bitsignature.Replace(' ', '+');
                                    SignatureBit = Convert.FromBase64String(Bitsignature);
                                    signatory = iTextSharp.text.Image.GetInstance(SignatureBit);
                                    signatory.SetAbsolutePosition(xx, yy);

                                    signatory.ScaleToFit(50, 50);

                                    document.Add(signatory);
                                }


                            }
                        }

                        if (PayMtd.sig2 != null)
                        {


                            List<Phrase> PhraseList = new List<Phrase>();
                            for (int page = 1; page <= reader1.NumberOfPages; page++)
                            {
                                ITextExtractionStrategy strategy = new SimpleTextExtractionStrategy();
                                string currentPageText = PdfTextExtractor.GetTextFromPage(reader1, page, strategy);
                                if (currentPageText.Contains("[Signature 2]") || currentPageText.Contains("[Signature\n2]"))
                                {
                                    TextFragmentAbsorber absorber = new TextFragmentAbsorber("[Signature 2]");


                                    // Accept the absorber for first page
                                    doc.Pages[1].Accept(absorber);

                                    // View text and placement info of first text occurrence
                                    TextFragment firstOccurrence = absorber.TextFragments[1];
                                    // document.Add(new Paragraph(Convert.ToDouble(firstOccurrence.Position.XIndent).ToString()));
                                    float xx = (float)firstOccurrence.Position.XIndent + 20;
                                    float yy = (float)firstOccurrence.Position.YIndent;


                                    string Bitsignature = null;
                                    Bitsignature = PayMtd.sig2;
                                    Byte[] SignatureBit = null;
                                    iTextSharp.text.Image signatory = null;

                                    Bitsignature = Bitsignature.Replace(' ', '+');
                                    SignatureBit = Convert.FromBase64String(Bitsignature);
                                    signatory = iTextSharp.text.Image.GetInstance(SignatureBit);
                                    signatory.SetAbsolutePosition(xx, yy);

                                    signatory.ScaleToFit(50, 50);

                                    document.Add(signatory);
                                }


                            }
                        }

                        if (PayMtd.sig3 != null)
                        {


                            List<Phrase> PhraseList = new List<Phrase>();
                            for (int page = 1; page <= reader1.NumberOfPages; page++)
                            {
                                ITextExtractionStrategy strategy = new SimpleTextExtractionStrategy();
                                string currentPageText = PdfTextExtractor.GetTextFromPage(reader1, page, strategy);
                                if (currentPageText.Contains("[Signature 3]") || currentPageText.Contains("[Signature\n3]"))
                                {
                                    TextFragmentAbsorber absorber = new TextFragmentAbsorber("[Signature 3]");


                                    // Accept the absorber for first page
                                    doc.Pages[1].Accept(absorber);

                                    // View text and placement info of first text occurrence
                                    TextFragment firstOccurrence = absorber.TextFragments[1];
                                    // document.Add(new Paragraph(Convert.ToDouble(firstOccurrence.Position.XIndent).ToString()));
                                    float xx = (float)firstOccurrence.Position.XIndent + 20;
                                    float yy = (float)firstOccurrence.Position.YIndent;


                                    string Bitsignature = null;
                                    Bitsignature = PayMtd.sig3;
                                    Byte[] SignatureBit = null;
                                    iTextSharp.text.Image signatory = null;

                                    Bitsignature = Bitsignature.Replace(' ', '+');
                                    SignatureBit = Convert.FromBase64String(Bitsignature);
                                    signatory = iTextSharp.text.Image.GetInstance(SignatureBit);
                                    signatory.SetAbsolutePosition(xx, yy);

                                    signatory.ScaleToFit(50, 50);

                                    document.Add(signatory);
                                }


                            }
                        }

                        document.Close();
                        writer.Close();
                        // closing the stamper
                        uProfile.Status = "Success";

                    }
                    catch (Exception ex)
                    {
                        uProfile.Status = ex.ToString();
                        //this.Page.ClientScript.RegisterClientScriptBlock(this.GetType(),
                        //Guid.NewGuid().ToString("N"), "self.parent.location='login.aspx';", true);
                    }
                }
            }
            return uProfile.Status;
        }


        [HttpPost]
        [Route("GetPayslipItems")]
        public PayslipDDL PayslipDDL(employeev2 emp)
        {
            PayslipDDL PDDL = new PayslipDDL();
            List<PayslipDDLItems> DDI = new List<PayslipDDLItems>();
            try
            {
                GetDatabase GDB = new GetDatabase();
                GDB.ClientName = emp.CN;
                SelfieRegistration2Controller.GetDB2(GDB);
                Connection Connection = new Connection();
                Connection.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = emp.empid });
                DataTable DT = Connection.GetDataTable("sp_GetPaySlipPayPeriodItems");
                foreach (DataRow row in DT.Rows)
                {
                    PayslipDDLItems DDL = new PayslipDDLItems()
                    {
                        Year = row["Year"].ToString(),
                        PayPeriod = row["Pay Period"].ToString()
                    };
                    DDI.Add(DDL);
                }
                PDDL.PayslipDDLItems = DDI;
                PDDL.myreturn = "Success";
            }
            catch (Exception e)
            {
                PDDL.myreturn = e.ToString();
            }
            return PDDL;
        }
        [HttpPost]
        [Route("GetPayslipItemsv2")]
        public PayslipDDL PayslipDDLv2(employeev2 emp)
        {
            PayslipDDL PDDL = new PayslipDDL();
            List<PayslipDDLItems> DDI = new List<PayslipDDLItems>();
            try
            {
                GetDatabase GDB = new GetDatabase();
                GDB.ClientName = emp.CN;
                SelfieRegistration2Controller.GetDB2(GDB);
                Connection Connection = new Connection();
                Connection.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.NVarChar, Value = emp.empid });
                DataTable DT = Connection.GetDataTable("sp_GetPaySlipPayPeriodItems");
                DataTable DT2 = Connection.GetDataTable("sp_GetPayoutSchemeList");
                //sp_GetPayoutSchemeList
                foreach (DataRow row in DT.Rows)
                {
                    PayslipDDLItems DDL = new PayslipDDLItems()
                    {
                        Year = row["Year"].ToString(),
                        PayPeriod = row["Pay Period"].ToString()
                    };
                    DDI.Add(DDL);
                }
                foreach (DataRow row in DT2.Rows)
                {
                    PayslipDDLItems DDL = new PayslipDDLItems()
                    {
                        Year = row["year"].ToString(),
                        PayPeriod = row["StartMonth"].ToString() + " " + row["StartDay"].ToString() + " " + "-" + " " + row["EndMonth"].ToString() + " " + row["EndDay"].ToString(),
                        PayoutDate = row["PayoutDate"].ToString()
                    };
                    DDI.Add(DDL);
                }
                PDDL.PayslipDDLItems = DDI;
                PDDL.myreturn = "Success";
            }
            catch (Exception e)
            {
                PDDL.myreturn = e.ToString();
            }
            return PDDL;
        }
        [HttpPost]
        [Route("GetPayslipItemsv3")]
        public PayslipDDL PayslipDDLv3(employeev2 emp)
        {
            PayslipDDL PDDL = new PayslipDDL();
            List<PayslipDDLItems> DDI = new List<PayslipDDLItems>();
            try
            {
                GetDatabase GDB = new GetDatabase();
                GDB.ClientName = emp.CN;
                SelfieRegistration2Controller.GetDB2(GDB);
                Connection Connection = new Connection();
                Connection.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.NVarChar, Value = emp.empid });
                DataTable DT = Connection.GetDataTable("sp_GetPaySlipPayPeriodItems");
                //sp_GetPayoutSchemeList
                foreach (DataRow row in DT.Rows)
                {
                    PayslipDDLItems DDL = new PayslipDDLItems()
                    {
                        Year = row["Year"].ToString(),
                        PayPeriod = row["Pay Period"].ToString()
                    };
                    DDI.Add(DDL);
                }
                PDDL.PayslipDDLItems = DDI;
                PDDL.myreturn = "Success";
            }
            catch (Exception e)
            {
                PDDL.myreturn = e.ToString();
            }
            return PDDL;
        }
        [HttpPost]
        [Route("GetPayslipv3")]
        public string Payslipv3(UserProfilev2 uProfile)
        {
            try
            {
                GetDatabase GDB = new GetDatabase();
                GDB.ClientName = uProfile.CN;
                SelfieRegistration2Controller.GetDB2(GDB);


                string DDLMonths = "08";
                string DDLPayPeriod = uProfile.PayDay;
                string DDLYear = uProfile.PayYear;
                empid = uProfile.NTID;

                empname = PayMtd.searchempname(empid);

                empidprof = empid;
                //GetEmpLevelandCategory = PayMtd.GetEmpLevelandCategory(empid);
                //string EmpLevel = GetEmpLevelandCategory.Rows[0][0].ToString();
                //string EmpCategory = GetEmpLevelandCategory.Rows[0][1].ToString();

                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = empid });
                paysliprecordsempmaster = con.GetDataTable("sp_GetPaySlipRecordsEmpMaster");

                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = empid });
                paysliprecordsempmaster = con.GetDataTable("sp_GetPaySlipRecordsEmpMaster");

                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = empid });
                paysliprecordsconfidentialinfo = con.GetDataTable("sp_GetPaySlipRecordsConfidentialInfo");
                //paysliprecordsconfidentialinfo = PayMtd.PaySlipRecordsConfidentialInfo(empid);

                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = empid });
                paysliprecordsemployeepay = con.GetDataTable("sp_GetPaySlipRecordsEmployeePay");
                //paysliprecordsemployeepay = PayMtd.PaySlipRecordsEmployeePay(empid);
                //int schmct = PayMtd.GetSalarySchemes(EmpLevel, EmpCategory).Rows.Count;

                #region "New Payslip"
                TotalValue = 0;
                int latestpayslipcounter = 0;

                con = new Connection();
                LatestPayslipForm = con.GetDataTable("sp_GetLatestPayslipForm");
                //LatestPayslipForm = PayMtd.LatestPayslipForm();
                latestpayslipcounter = LatestPayslipForm.Rows.Count;
                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = empid });
                con.myparameters.Add(new myParameters { ParameterName = "@MONTH", mytype = SqlDbType.NVarChar, Value = DDLMonths.ToString() });
                DataTable BonusPayoutMonths = con.GetDataTable("sp_GetBonusPayouts");
                //DataTable BonusPayoutMonths = PayMtd.BonusPayoutMonths(empid, DDLMonths.ToString());
                if (latestpayslipcounter == 1)
                {
                    #region Retrieve Neccessary Data
                    con = new Connection();
                    con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = empid });
                    GetEmpLevelandCategory = con.GetDataTable("sp_SearchMasterControl_LeaveManagement_GetEmpLevelandCategory");

                    //GetEmpLevelandCategory = PayMtd.GetEmpLevelandCategory(empid);
                    int psrpsd = 0;
                    int pswh = 0;
                    //paysliprecordspayperiod = PayMtd.PaySlipRecordsPayPeriod(EmpLevel, EmpCategory);                
                    payperiod = hoursdatefrom + '-' + hoursdateto;

                    con = new Connection();
                    con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = empid });
                    con.myparameters.Add(new myParameters { ParameterName = "@PAYPERIOD", mytype = SqlDbType.NVarChar, Value = DDLPayPeriod });
                    paysliprecordspayslipdetails = con.GetDataTable("sp_GetPaySlipRecordsPaySlipDetails");
                    //paysliprecordspayslipdetails = PayMtd.PaySlipRecordsPaySlipDetails(empid, DDLPayPeriod);
                    #endregion
                    string pathmoto;
                    //if (uProfile.CN.ToLower() == "newcore")
                    //{
                    //    pathmoto = "PaySlipDefaultNewcore";
                    //}
                    //else
                    //{
                    //    pathmoto = "PaySlipDefault";
                    //}
                    pathmoto = "PaySlipDefault";
                    //LatestPayslipForm.Rows[0][0].ToString();
                    PdfReader reader1 = new PdfReader(HttpContext.Current.Server.MapPath("~/pdf/") + pathmoto + ".pdf");
                    LocationTextExtractionStrategy lteStrategy = new LocationTextExtractionStrategy();
                    string strExact = PdfTextExtractor.GetTextFromPage(reader1, 1, lteStrategy);
                    StringBuilder sb = new StringBuilder();
                    //this code used for replacing the found text
                    sb.Append(PdfTextExtractor.GetTextFromPage(reader1, 1));
                    #region Passing Retrieved Datas
                    sb.Replace("[Employee ID]", empid);
                    if (paysliprecordsempmaster.Rows.Count > 0)
                    {
                        sb.Replace("[Employee Name]", paysliprecordsempmaster.Rows[0][0].ToString() == "" ? "N/A" : paysliprecordsempmaster.Rows[0][0].ToString());
                        sb.Replace("[Date Joined]", paysliprecordsempmaster.Rows[0][2].ToString() == "" ? "N/A" : paysliprecordsempmaster.Rows[0][2].ToString());
                        sb.Replace("[Resigned]", paysliprecordsempmaster.Rows[0][3].ToString() == "" ? "N/A" : paysliprecordsempmaster.Rows[0][3].ToString());
                    }
                    else
                    {
                        sb.Replace("[Employee Name]", "N/A");
                        sb.Replace("[Date Joined]", "N/A");
                        sb.Replace("[Resigned]", "N/A");
                    }
                    if (paysliprecordsconfidentialinfo.Rows.Count > 0)
                    {
                        sb.Replace("[HDMF]", paysliprecordsconfidentialinfo.Rows[0][0].ToString() == "" ? "N/A" : paysliprecordsconfidentialinfo.Rows[0][0].ToString());
                        sb.Replace("[PhilHealth No]", paysliprecordsconfidentialinfo.Rows[0][1].ToString() == "" ? "N/A" : paysliprecordsconfidentialinfo.Rows[0][1].ToString());
                        sb.Replace("[SSS No]", paysliprecordsconfidentialinfo.Rows[0][2].ToString() == "" ? "N/A" : paysliprecordsconfidentialinfo.Rows[0][2].ToString());
                        sb.Replace("[TIN]", paysliprecordsconfidentialinfo.Rows[0][3].ToString() == "" ? "N/A" : paysliprecordsconfidentialinfo.Rows[0][3].ToString());
                        sb.Replace("[Tax Category]", paysliprecordsconfidentialinfo.Rows[0][4].ToString() == "" ? "N/A" : paysliprecordsconfidentialinfo.Rows[0][4].ToString());
                    }
                    else
                    {
                        sb.Replace("[HDMF]", "N/A");
                        sb.Replace("[PhilHealth No]", "N/A");
                        sb.Replace("[SSS No]", "N/A");
                        sb.Replace("[TIN]", "N/A");
                        sb.Replace("[Tax Category]", "N/A");
                    }
                    if (paysliprecordsemployeepay.Rows.Count > 0)
                    {

                        sb.Replace("[Bank Account]", paysliprecordsemployeepay.Rows[0][0].ToString() == "" ? "N/A" : paysliprecordsemployeepay.Rows[0][0].ToString());
                        sb.Replace("[Hourly Rate]", paysliprecordsemployeepay.Rows[0][1].ToString() == "" ? "0.00" : paysliprecordsemployeepay.Rows[0][1].ToString());
                    }
                    else
                    {
                        sb.Replace("[Bank Account]", "N/A");
                        sb.Replace("[Hourly Rate]", "N/A");
                    }
                    sb.Replace("[Pay Period]", DDLPayPeriod == "" ? "N/A" : DDLPayPeriod);
                    if (paysliprecordspayslipdetails.Rows.Count > 0)
                    {
                        sb.Replace("[Pay Day]", paysliprecordspayslipdetails.Rows[0][1].ToString() == "" ? "N/A" : paysliprecordspayslipdetails.Rows[0][1].ToString());
                        sb.Replace("[Basic Salary]", paysliprecordspayslipdetails.Rows[0][2].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][2].ToString())));
                        sb.Replace("[Non Taxable Allowance]", paysliprecordspayslipdetails.Rows[0][3].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][3].ToString())));
                        sb.Replace("[Regular Night Differential]", paysliprecordspayslipdetails.Rows[0][4].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][4].ToString())));
                        sb.Replace("[Regular Over Time]", paysliprecordspayslipdetails.Rows[0][5].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][5].ToString())));
                        sb.Replace("[Regular Holiday]", paysliprecordspayslipdetails.Rows[0][6].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][6].ToString())));
                        sb.Replace("[Regular Holiday Night Differential]", paysliprecordspayslipdetails.Rows[0][7].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][7].ToString())));
                        sb.Replace("[Regular Holiday Over Time]", paysliprecordspayslipdetails.Rows[0][8].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][8].ToString())));
                        sb.Replace("[Paid Time Off]", paysliprecordspayslipdetails.Rows[0][9].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][9].ToString())));
                        sb.Replace("[Special Holiday]", paysliprecordspayslipdetails.Rows[0][10].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][10].ToString())));
                        sb.Replace("[Special Holiday Night Differential]", paysliprecordspayslipdetails.Rows[0][11].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][11].ToString())));
                        sb.Replace("[Special Holiday Over Time]", paysliprecordspayslipdetails.Rows[0][12].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][12].ToString())));
                        sb.Replace("[Allowance]", paysliprecordspayslipdetails.Rows[0][13].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][13].ToString())));
                        sb.Replace("[Bonuses]", paysliprecordspayslipdetails.Rows[0][14].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][14].ToString())));
                        sb.Replace("[Adjustments]", paysliprecordspayslipdetails.Rows[0][15].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][15].ToString())));
                        sb.Replace("[Total Salary]", paysliprecordspayslipdetails.Rows[0][16].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][16].ToString())));
                        sb.Replace("[Employee HDMF]", paysliprecordspayslipdetails.Rows[0][17].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][17].ToString())));
                        sb.Replace("[Employee Philhealth]", paysliprecordspayslipdetails.Rows[0][18].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][18].ToString())));
                        sb.Replace("[Employee SSS]", paysliprecordspayslipdetails.Rows[0][19].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][19].ToString())));
                        sb.Replace("[Employee WithHoldingTax]", paysliprecordspayslipdetails.Rows[0][20].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][20].ToString())));
                        sb.Replace("[SSS Loan Deduction]", paysliprecordspayslipdetails.Rows[0][21].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][21].ToString())));
                        sb.Replace("[Pag-ibig Loan Deduction]", paysliprecordspayslipdetails.Rows[0][22].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][22].ToString())));
                        sb.Replace("[Net Pay]", paysliprecordspayslipdetails.Rows[0][23].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][23].ToString())));
                        sb.Replace("[Employer HDMF]", paysliprecordspayslipdetails.Rows[0][24].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][24].ToString())));
                        sb.Replace("[Employer Philhealth]", paysliprecordspayslipdetails.Rows[0][25].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][25].ToString())));
                        sb.Replace("[Employer SSS]", paysliprecordspayslipdetails.Rows[0][26].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][26].ToString())));
                        sb.Replace("[Employer SSS EC]", paysliprecordspayslipdetails.Rows[0][27].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][27].ToString())));
                    }
                    else
                    {
                        sb.Replace("[Pay Day]", "N/A");
                        sb.Replace("[Basic Salary]", "N/A");
                        sb.Replace("[Non Taxable Allowance]", "N/A");
                        sb.Replace("[Regular Night Differential]", "N/A");
                        sb.Replace("[Regular Over Time]", "N/A");
                        sb.Replace("[Regular Holiday]", "N/A");
                        sb.Replace("[Regular Holiday Night Differential]", "N/A");
                        sb.Replace("[Regular Holiday Over Time]", "N/A");
                        sb.Replace("[Paid Time Off]", "N/A");
                        sb.Replace("[Special Holiday]", "N/A");
                        sb.Replace("[Special Holiday Night Differential]", "N/A");
                        sb.Replace("[Special Holiday Over Time]", "N/A");
                        sb.Replace("[Allowance]", "N/A");
                        sb.Replace("[Bonuses]", "N/A");
                        sb.Replace("[Adjustments]", "N/A");
                        sb.Replace("[Total Salary]", "N/A");
                        sb.Replace("[Employee HDMF]", "N/A");
                        sb.Replace("[Employee Philhealth]", "N/A");
                        sb.Replace("[Employee SSS]", "N/A");
                        sb.Replace("[Employee WithHoldingTax]", "N/A");
                        sb.Replace("[SSS Loan Deduction]", "N/A");
                        sb.Replace("[Pag-ibig Loan Deduction]", "N/A");
                        sb.Replace("[Net Pay]", "N/A");
                        sb.Replace("[Employer HDMF]", "N/A");
                        sb.Replace("[Employer Philhealth]", "N/A");
                        sb.Replace("[Employer SSS]", "N/A");
                        sb.Replace("[Employer SSS EC]", "N/A");
                    }

                    #endregion

                    FileStream Newstream = new FileStream(string.Concat(HttpContext.Current.Server.MapPath("~/pdf/"), empid + pathmoto + ".pdf"), FileMode.Create);
                    MemoryStream ms = new MemoryStream();
                    Document document = new Document(PageSize.A4, 25, 25, 30, 30);
                    //Code for adding the string builder sb to the document
                    PdfWriter writer = PdfWriter.GetInstance(document, Newstream);
                    document.Open();
                    document.Add(new Paragraph(sb.ToString()));
                    document.Close();
                    writer.Close();
                }
                else
                {
                    #region Fixed payslip

                    int psrpsd = 0;
                    int pswh = 0;


                    //paysliprecordspayslipdetails = PayMtd.PaySlipRecordsPaySlipDetails(empid, DDLPayPeriod);

                    con = new Connection();
                    con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = empid });
                    con.myparameters.Add(new myParameters { ParameterName = "@PAYPERIOD", mytype = SqlDbType.NVarChar, Value = DDLPayPeriod });
                    paysliprecordspayslipdetails = con.GetDataTable("sp_GetPaySlipRecordsPaySlipDetails");

                    con = new Connection();
                    con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = empid });
                    con.myparameters.Add(new myParameters { ParameterName = "@YEAR", mytype = SqlDbType.NVarChar, Value = DDLYear });
                    con.myparameters.Add(new myParameters { ParameterName = "@PAYPERIOD", mytype = SqlDbType.NVarChar, Value = DDLPayPeriod });
                    YearToDate = con.GetDataTable("sp_GetPaySlipRecordsPaySlipDetailsYearToDate");

                    con = new Connection();
                    con.myparameters.Add(new myParameters { ParameterName = "@empId", mytype = SqlDbType.NVarChar, Value = empid });
                    DataTable DTProfile = con.GetDataTable("sp_getCompleteProfile_Mobilev2");
                    //YearToDate = PayMtd.PayslipDetailsYearToDate(empid, DDLYear, DDLPayPeriod);

                    //Response.Redirect("~/pdf/BIR19021.pdf"); // view pdf
                    string path = "https://ws.durusthr.com/ILM_WS_Live/pdf";

                    //if (uProfile.CN.ToLower() == "newcore")
                    //{
                    //    pdfpath = "/PaySlipDefaultNewcore.pdf";
                    //}
                    //else
                    //{
                    //    pdfpath = "/PaySlipDefault.pdf";
                    //}
                    var pdfReader1 = new PdfReader(path + "/PaySlipDefault.pdf");

                    AcroFields af = pdfReader1.AcroFields;
                    //LocationTextExtractionStrategy lteStrategy = new LocationTextExtractionStrategy();
                    //string strExact = PdfTextExtractor.GetTextFromPage(pdfReader1,1, lteStrategy);
                    //StringBuilder sb = new StringBuilder();
                    string sam = "";
                    foreach (var field in af.Fields)
                    {
                        sam = sam + field.Key + "-"; // names of textfields
                    }
                    //viewpdf.InnerText = sam;
                    // get the file paths


                    // Template file path

                    //if (uProfile.CN.ToLower() == "newcore")
                    //{
                    //    formFile = path + "/PaySlipDefaultNewcore.pdf";
                    //}
                    //else
                    //{
                    //    formFile = path + "/PaySlipDefault.pdf";
                    //}
                    var lnameencrypt = paysliprecordsempmaster.Rows[0][4].ToString();
                    var lname = paysliprecordsempmaster.Rows[0][5].ToString();
                    var fname = paysliprecordsempmaster.Rows[0][6].ToString();
                    string formFile = "/PaySlipDefault.pdf";
                    string path2 = HttpContext.Current.Server.MapPath("pdf");
                    // Output file path
                    string newFile = path2 + "/" + fname + "_" + lname + "_" + DDLPayPeriod + ".pdf";
                    string pdfpass1 = "", pdfpass2 = "";

                    // read the template file
                    PdfReader reader;
                    if (uProfile.CN.ToLower() == "newcore")
                    {
                        reader = new PdfReader(path2 + "/PaySlipDefaultNewcore.pdf");
                    }
                    else
                    {
                        reader = new PdfReader(path2 + "/PaySlipDefault.pdf");
                    }




                    // instantiate PDFStamper object
                    // The Output file will be created from template file and edited by the PDFStamper
                    PdfStamper stamper = new PdfStamper(reader, new FileStream(newFile, FileMode.Create));
                    if (uProfile.CN.ToLower() == "newcore")
                    {
                        Connection Connection = new Connection();
                        Connection.myparameters.Add(new myParameters { ParameterName = "@empid", mytype = SqlDbType.NVarChar, Value = empid });
                        DataTable DT = Connection.GetDataTable("sp_getEmailsbyEmpID");
                        if (DT.Rows.Count > 0)
                        {
                            pdfpass1 = DT.Rows[0][2].ToString();
                            pdfpass2 = DT.Rows[0][2].ToString();
                        }
                        else
                        {
                            pdfpass1 = lnameencrypt.ToLower();
                            pdfpass2 = lname;
                        }
                    }
                    else
                    {
                        pdfpass1 = lnameencrypt.ToLower();
                        pdfpass2 = lname;
                    }
                    //stamper.SetEncryption(true, pdfpass1, pdfpass2, PdfWriter.ALLOW_SCREENREADERS);
                    //stamper.SetEncryption(true, lnameencrypt.ToLower(),lname, PdfWriter.ALLOW_SCREENREADERS);
                    //PdfEncryptor.Encrypt(reader, new FileStream(newFile, FileMode.Open), true, empid, "secret", PdfWriter.ALLOW_SCREENREADERS);
                    // Object to deal with the Output file's textfields
                    AcroFields fields = stamper.AcroFields;

                    // set form fields("Field Name in PDF", "String to be placed in this PDF text field");
                    //for (int i = 0; i <= 1000; i++)
                    //{
                    //    string ii = i.ToString();
                    //    
                    //    fields.SetField("untitled" + i, "vt" + ii); //temporary values
                    //}

                    //------------------------------------------------
                    //loop to display all field names
                    if (paysliprecordsempmaster.Rows.Count > 0)
                    {
                        //fields.SetFieldProperty("txtempid", "setflags", PdfAnnotation.FLAGS_HIDDEN,null);
                        fields.SetField("untitled2", paysliprecordsempmaster.Rows[0][0].ToString());//Employee Name
                        fields.SetField("employeeno", empid);//Employee No
                        fields.SetField("joined", paysliprecordsempmaster.Rows[0][2].ToString());//Joined
                        fields.SetField("resigned", paysliprecordsempmaster.Rows[0][3].ToString());//Resigned
                    }
                    else
                    {

                    }
                    if (paysliprecordsconfidentialinfo.Rows.Count > 0)
                    {
                        fields.SetField("pagibigno", paysliprecordsconfidentialinfo.Rows[0][0].ToString());//Pag-IBIG No
                        fields.SetField("philhealthno", paysliprecordsconfidentialinfo.Rows[0][1].ToString());//PhilHealth No
                        fields.SetField("sssno", paysliprecordsconfidentialinfo.Rows[0][2].ToString());//SSS No
                        fields.SetField("taxno", paysliprecordsconfidentialinfo.Rows[0][3].ToString());//Tax No
                        fields.SetField("taxcategory", paysliprecordsconfidentialinfo.Rows[0][4].ToString());//Tax Category
                    }
                    else
                    {

                    }
                    if (paysliprecordsemployeepay.Rows.Count > 0)
                    {
                        fields.SetField("bankaccount", paysliprecordsemployeepay.Rows[0][0].ToString());//Bank Account
                        fields.SetField("hourlyrate", paysliprecordsemployeepay.Rows[0][1].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordsemployeepay.Rows[0][1].ToString())));//Hourly Rate
                    }
                    else
                    {
                        fields.SetField("hourlyrate", "0.00");
                    }
                    fields.SetField("payperiod", DDLPayPeriod);//Pay Period
                    con = new Connection();
                    con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = empid });
                    con.myparameters.Add(new myParameters { ParameterName = "@PAYPERIOD", mytype = SqlDbType.NVarChar, Value = DDLPayPeriod });
                    if (uProfile.CN.ToLower() == "newcore")
                    {
                        paysliprecordspayslipdetails = con.GetDataTable("sp_GetPaySlipRecordsPaySlipDetailsNewcore");
                        if (paysliprecordspayslipdetails.Rows.Count > 0)
                        {
                            fields.SetField("payday", paysliprecordspayslipdetails.Rows[0][0].ToString());//Pay Day
                            fields.SetField("regularothrs", paysliprecordspayslipdetails.Rows[0][1].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][1].ToString())));//Basic Salary
                            fields.SetField("regularndothrs", paysliprecordspayslipdetails.Rows[0][2].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][2].ToString())));//Basic Salary
                            fields.SetField("restdayndothrs", paysliprecordspayslipdetails.Rows[0][3].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][3].ToString())));//Basic Salary
                            fields.SetField("restdayothrs", paysliprecordspayslipdetails.Rows[0][52].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][52].ToString())));//Basic Salary
                            fields.SetField("regularholidayhrs", paysliprecordspayslipdetails.Rows[0][4].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][4].ToString())));//Basic Salary
                            fields.SetField("regularholidayothrs", paysliprecordspayslipdetails.Rows[0][5].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][5].ToString())));//Basic Salary
                            fields.SetField("regularholidayndothrs", paysliprecordspayslipdetails.Rows[0][53].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][53].ToString())));//Basic Salary
                            fields.SetField("specialholidayhrs", paysliprecordspayslipdetails.Rows[0][6].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][6].ToString())));//Basic Salary
                            fields.SetField("specialholidayothrs", paysliprecordspayslipdetails.Rows[0][7].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][7].ToString())));//Basic Salary
                            fields.SetField("specialholidayndothrs", paysliprecordspayslipdetails.Rows[0][54].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][54].ToString())));//Basic Salary
                            fields.SetField("adjabsencehrs", paysliprecordspayslipdetails.Rows[0][59].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][59].ToString())));//Basic Salary
                            fields.SetField("adjlatehrs", paysliprecordspayslipdetails.Rows[0][61].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][61].ToString())));//Basic Salary
                            fields.SetField("adjrdndothrs", paysliprecordspayslipdetails.Rows[0][63].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][63].ToString())));//Basic Salary
                            fields.SetField("adjregularhrs", paysliprecordspayslipdetails.Rows[0][10].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][10].ToString())));//Basic Salary
                            fields.SetField("adjregularndothrs", paysliprecordspayslipdetails.Rows[0][11].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][11].ToString())));//Basic Salary
                            fields.SetField("adjregularothrs", paysliprecordspayslipdetails.Rows[0][12].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][12].ToString())));//Basic Salary
                            fields.SetField("adjrestdayothrs", paysliprecordspayslipdetails.Rows[0][13].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][13].ToString())));//Basic Salary
                            fields.SetField("basicsalarycurrent", paysliprecordspayslipdetails.Rows[0][14].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][14].ToString())));//Basic Salary
                            fields.SetField("deductionsabsencescurrent", paysliprecordspayslipdetails.Rows[0][15].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][15].ToString())));//Basic Salary
                            fields.SetField("deductionstardinesscurrent", paysliprecordspayslipdetails.Rows[0][16].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][16].ToString())));//Basic Salary
                            fields.SetField("13thmonthtaxablecurrent", paysliprecordspayslipdetails.Rows[0][17].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][17].ToString())));//Basic Salary
                            fields.SetField("regularotcurrent", paysliprecordspayslipdetails.Rows[0][18].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][18].ToString())));//Basic Salary
                            fields.SetField("regularndotcurrent", paysliprecordspayslipdetails.Rows[0][19].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][19].ToString())));//Basic Salary

                            fields.SetField("restdayotcurrent", paysliprecordspayslipdetails.Rows[0][50].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][50].ToString())));//Basic Salary
                            fields.SetField("restdayndotcurrent", paysliprecordspayslipdetails.Rows[0][20].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][20].ToString())));//Basic Salary
                            fields.SetField("regularholidaycurrent", paysliprecordspayslipdetails.Rows[0][21].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][21].ToString())));//Basic Salary
                            fields.SetField("regularholidayotcurrent", paysliprecordspayslipdetails.Rows[0][22].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][22].ToString())));//Basic Salary
                            fields.SetField("regularholidayndotcurrent", paysliprecordspayslipdetails.Rows[0][51].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][51].ToString())));//Basic Salary
                            fields.SetField("specialholidaycurrent", paysliprecordspayslipdetails.Rows[0][23].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][23].ToString())));//Basic Salary
                            fields.SetField("specialholidayotcurrent", paysliprecordspayslipdetails.Rows[0][24].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][24].ToString())));//Basic Salary
                            fields.SetField("specialholidayndotcurrent", paysliprecordspayslipdetails.Rows[0][55].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][55].ToString())));//Basic Salary
                            fields.SetField("adjabsencecurrent", paysliprecordspayslipdetails.Rows[0][60].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][60].ToString())));//Basic Salary
                            fields.SetField("adjlatecurrent", paysliprecordspayslipdetails.Rows[0][62].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][62].ToString())));//Basic Salary
                            fields.SetField("adjrdndotcurrent", paysliprecordspayslipdetails.Rows[0][64].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][64].ToString())));//Basic Salary
                            fields.SetField("adjregularhourscurrent", paysliprecordspayslipdetails.Rows[0][27].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][27].ToString())));//Basic Salary
                            fields.SetField("adjregularndotcurrent", paysliprecordspayslipdetails.Rows[0][28].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][28].ToString())));//Basic Salary
                            fields.SetField("adjregularotcurrent", paysliprecordspayslipdetails.Rows[0][29].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][29].ToString())));//Basic Salary
                            fields.SetField("adjrestdayotcurrent", paysliprecordspayslipdetails.Rows[0][30].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][30].ToString())));//Basic Salary
                            fields.SetField("grosspaycurrent", paysliprecordspayslipdetails.Rows[0][31].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][31].ToString())));//Basic Salary
                            fields.SetField("deductionsssscurrent", paysliprecordspayslipdetails.Rows[0][32].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][32].ToString())));//Basic Salary
                            fields.SetField("deductionsphilhealthcurrent", paysliprecordspayslipdetails.Rows[0][33].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][33].ToString())));//Basic Salary
                            fields.SetField("deductionspagibigcurrent", paysliprecordspayslipdetails.Rows[0][34].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][34].ToString())));//Basic Salary
                            fields.SetField("taxableincomecurrent", paysliprecordspayslipdetails.Rows[0][35].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][35].ToString())));//Basic Salary
                            fields.SetField("deductionswtaxcurrent", paysliprecordspayslipdetails.Rows[0][36].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][36].ToString())));//Basic Salary
                            fields.SetField("deductionsplacurrent", paysliprecordspayslipdetails.Rows[0][37].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][37].ToString())));//Basic Salary
                            fields.SetField("deductionsprulifecurrent", paysliprecordspayslipdetails.Rows[0][38].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][38].ToString())));//Basic Salary
                            fields.SetField("deductionssdcurrent", paysliprecordspayslipdetails.Rows[0][39].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][39].ToString())));//Basic Salary
                            fields.SetField("deductionssssloanamountcurrent", paysliprecordspayslipdetails.Rows[0][40].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][40].ToString())));//Basic Salary
                            fields.SetField("deductionspagibigloanamountcurrent", paysliprecordspayslipdetails.Rows[0][41].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][41].ToString())));//Basic Salary
                            fields.SetField("ecolacurrent", paysliprecordspayslipdetails.Rows[0][42].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][42].ToString())));//Basic Salary
                            fields.SetField("deminimiscurrent", paysliprecordspayslipdetails.Rows[0][43].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][43].ToString())));//Basic Salary
                            fields.SetField("adjnontaxableallowancecurrent", paysliprecordspayslipdetails.Rows[0][58].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][58].ToString())));//Basic Salary
                            fields.SetField("13thmonthnontaxablecurrent", paysliprecordspayslipdetails.Rows[0][44].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][44].ToString())));//Basic Salary
                            fields.SetField("netpaycurrent", paysliprecordspayslipdetails.Rows[0][45].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][45].ToString())));//Basic Salary
                            fields.SetField("sssemployersharecurrent", paysliprecordspayslipdetails.Rows[0][46].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][46].ToString())));//Basic Salary
                            fields.SetField("sssemployerecsharecurrent", paysliprecordspayslipdetails.Rows[0][47].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][47].ToString())));//Basic Salary
                            fields.SetField("philhealthemployersharecurrent", paysliprecordspayslipdetails.Rows[0][48].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][48].ToString())));//Basic Salary
                            fields.SetField("pagibigemployersharecurrent", paysliprecordspayslipdetails.Rows[0][49].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][49].ToString())));//Basic Salary
                            fields.SetField("restdayhrs", paysliprecordspayslipdetails.Rows[0][65].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][65].ToString())));//Rest Day Hours
                            fields.SetField("restdaycurrent", paysliprecordspayslipdetails.Rows[0][66].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][66].ToString())));//Rest Day Amount
                            fields.SetField("adjrdhrs", "0.00");//ADJ Rest Day Hours
                            fields.SetField("adjrdcurrent", "0.00");//ADJ Rest Day Amount
                        }
                        else
                        {

                        }
                    }
                    else
                    {
                        paysliprecordspayslipdetails = con.GetDataTable("sp_GetPaySlipRecordsPaySlipDetails");
                        if (paysliprecordspayslipdetails.Rows.Count > 0)
                        {
                            fields.SetField("payday", paysliprecordspayslipdetails.Rows[0][1].ToString());//Pay Day
                            fields.SetField("basicsalarycurrent", paysliprecordspayslipdetails.Rows[0][2].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][2].ToString())));//Basic Salary
                            fields.SetField("nontaxableallowancecurrent", paysliprecordspayslipdetails.Rows[0][3].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][3].ToString())));//Non Taxable Allowance
                            fields.SetField("regularnightdiffcurrent", paysliprecordspayslipdetails.Rows[0][4].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][4].ToString())));//Regular Night Diff
                            fields.SetField("regularotcurrent", paysliprecordspayslipdetails.Rows[0][5].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][5].ToString())));//Regular Overtime
                            fields.SetField("regularholidaycurrent", paysliprecordspayslipdetails.Rows[0][6].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][6].ToString())));//Regular Holiday
                            fields.SetField("regularholidaynightdiffcurrent", paysliprecordspayslipdetails.Rows[0][7].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][7].ToString())));//Regular Holiday Night Diff
                            fields.SetField("regularholidayotcurrent", paysliprecordspayslipdetails.Rows[0][8].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][8].ToString())));//Regular Overtime
                            fields.SetField("paidtimeoffcurrent", paysliprecordspayslipdetails.Rows[0][9].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][9].ToString())));//Paid Time Off
                            fields.SetField("specialholidaycurrent", paysliprecordspayslipdetails.Rows[0][10].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][10].ToString())));//Special Holiday
                            fields.SetField("specialholidaynightdiffcurrent", paysliprecordspayslipdetails.Rows[0][11].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][11].ToString())));//Special Holiday Night Diff
                            fields.SetField("specialholidayotcurrent", paysliprecordspayslipdetails.Rows[0][12].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][12].ToString())));//Special Holiday OT
                            fields.SetField("allowancescurrent", paysliprecordspayslipdetails.Rows[0][13].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][13].ToString())));//Allowances
                            fields.SetField("bonuscurrent", paysliprecordspayslipdetails.Rows[0][14].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][14].ToString())));//Bonus
                            fields.SetField("adjustmentscurrent", paysliprecordspayslipdetails.Rows[0][15].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][15].ToString())));//adjustments
                            fields.SetField("totalcurrent", paysliprecordspayslipdetails.Rows[0][16].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][16].ToString())));//Gross Pay
                            fields.SetField("employeehdmfcurrent", paysliprecordspayslipdetails.Rows[0][17].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][17].ToString())));//Employee HDMF
                            fields.SetField("employeephilhealthcurrent", paysliprecordspayslipdetails.Rows[0][18].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][18].ToString())));//Employee PhilHealth
                            fields.SetField("employeesocialsecuritycurrent", paysliprecordspayslipdetails.Rows[0][19].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][19].ToString())));//Employee SSS
                            fields.SetField("withholdingtaxcurrent", paysliprecordspayslipdetails.Rows[0][20].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][20].ToString())));//Employee WithHoldingTax
                            fields.SetField("sssloandeductioncurrent", paysliprecordspayslipdetails.Rows[0][21].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][21].ToString())));//Loans SSS
                            fields.SetField("pagibigloandeductioncurrent", paysliprecordspayslipdetails.Rows[0][22].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][22].ToString())));//Loans Pagibig
                            fields.SetField("netpaycurrent", paysliprecordspayslipdetails.Rows[0][23].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][23].ToString())));//Net Pay
                            fields.SetField("employerhdmfcurrent", paysliprecordspayslipdetails.Rows[0][24].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][24].ToString())));//Employer HDMF
                            fields.SetField("employerphilhealthcurrent", paysliprecordspayslipdetails.Rows[0][25].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][25].ToString())));//Employer PhilHealth
                            fields.SetField("employersocialsecuritycurrent", paysliprecordspayslipdetails.Rows[0][26].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][26].ToString())));//Employer SSS
                            fields.SetField("employersocialsecurityeccurrent", paysliprecordspayslipdetails.Rows[0][27].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][27].ToString())));//Employer SSS EC
                            fields.SetField("deductionsabsencescurrent", paysliprecordspayslipdetails.Rows[0][28].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][28].ToString())));//Deductions Absences
                            fields.SetField("deductionslatescurrent", paysliprecordspayslipdetails.Rows[0][29].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][29].ToString())));//Deductions Lates
                            fields.SetField("doubleholidaycurrent", paysliprecordspayslipdetails.Rows[0][30].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][30].ToString())));//Double Holiday
                            fields.SetField("doubleholidayndcurrent", paysliprecordspayslipdetails.Rows[0][31].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][31].ToString())));//Double Holiday ND
                            fields.SetField("doubleholidayotcurrent", paysliprecordspayslipdetails.Rows[0][32].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][32].ToString())));//Double Holiday OT
                        }
                        else
                        {

                        }
                    }

                    if (uProfile.CN.ToLower() == "newcore")
                    {
                        con = new Connection();
                        con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = empid });
                        con.myparameters.Add(new myParameters { ParameterName = "@PAYPERIOD", mytype = SqlDbType.NVarChar, Value = DDLPayPeriod });
                        con.myparameters.Add(new myParameters { ParameterName = "@YEAR", mytype = SqlDbType.NVarChar, Value = uProfile.PayYear });
                        YearToDate = con.GetDataTable("sp_GetPaySlipRecordsPaySlipDetailsYearToDateNewcore");
                        if (YearToDate.Rows.Count > 0)
                        {
                            //fields.SetField("payday", YearToDate.Rows[0][0].ToString());//Pay Day
                            //fields.SetField("regularothrs", YearToDate.Rows[0][1].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][1].ToString())));//Basic Salary
                            //fields.SetField("regularndothrs", YearToDate.Rows[0][2].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][2].ToString())));//Basic Salary
                            //fields.SetField("restdayndothrs", YearToDate.Rows[0][3].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][3].ToString())));//Basic Salary
                            //fields.SetField("regularholidayhrs", YearToDate.Rows[0][4].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][4].ToString())));//Basic Salary
                            //fields.SetField("regularholidayothrs", YearToDate.Rows[0][5].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][5].ToString())));//Basic Salary
                            //fields.SetField("specialholidayhrs", YearToDate.Rows[0][6].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][6].ToString())));//Basic Salary
                            //fields.SetField("specialholidayothrs", YearToDate.Rows[0][7].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][7].ToString())));//Basic Salary
                            //fields.SetField("doubleholidayhrs", YearToDate.Rows[0][8].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][8].ToString())));//Basic Salary
                            //fields.SetField("doubleholidayothrs", YearToDate.Rows[0][9].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][9].ToString())));//Basic Salary
                            //fields.SetField("adjregularhrs", YearToDate.Rows[0][10].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][10].ToString())));//Basic Salary
                            //fields.SetField("adjregularndothrs", YearToDate.Rows[0][11].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][11].ToString())));//Basic Salary
                            //fields.SetField("adjregularothrs", YearToDate.Rows[0][12].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][12].ToString())));//Basic Salary
                            //fields.SetField("adjrestdayothrs", YearToDate.Rows[0][13].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][13].ToString())));//Basic Salary
                            fields.SetField("basicsalaryytd", YearToDate.Rows[0][13].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][13].ToString())));//Basic Salary
                            fields.SetField("deductionsabsencesytd", YearToDate.Rows[0][14].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][14].ToString())));//Deduction absences
                            fields.SetField("deductionstardinessytd", YearToDate.Rows[0][15].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][15].ToString())));//Deduction lates
                            fields.SetField("13thmonthtaxableytd", YearToDate.Rows[0][16].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][16].ToString())));//13 month

                            fields.SetField("regularotytd", YearToDate.Rows[0][17].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][17].ToString())));//Regular OT
                            fields.SetField("regularndotytd", YearToDate.Rows[0][18].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][18].ToString())));//Regular Night Diff

                            fields.SetField("restdayotytd", YearToDate.Rows[0][52].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][52].ToString())));//Reg RDOT
                            fields.SetField("restdayndotytd", YearToDate.Rows[0][19].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][19].ToString())));//Reg RDNDOT

                            fields.SetField("regularholidayytd", YearToDate.Rows[0][20].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][20].ToString())));//Regular Holiday
                            fields.SetField("regularholidayndotytd", YearToDate.Rows[0][50].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][50].ToString())));//Regular Holiday Night Diff
                            fields.SetField("regularholidayotytd", YearToDate.Rows[0][21].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][21].ToString())));//Regular Holiday OT

                            fields.SetField("specialholidayytd", YearToDate.Rows[0][22].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][22].ToString())));//Special Holiday
                            fields.SetField("specialholidayndotytd", YearToDate.Rows[0][51].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][51].ToString())));//Special Holiday Night Diff
                            fields.SetField("specialholidayotytd", YearToDate.Rows[0][23].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][23].ToString())));//Special Holiday OT

                            fields.SetField("adjabsenceytd", YearToDate.Rows[0][56].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][56].ToString())));//Double Holiday
                            fields.SetField("adjlateytd", YearToDate.Rows[0][58].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][58].ToString())));//Double Holiday Night Diff
                            fields.SetField("adjrdndotytd", YearToDate.Rows[0][60].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][60].ToString())));//Double Holiday OT

                            fields.SetField("adjregularhoursytd", YearToDate.Rows[0][26].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][26].ToString())));//Double Holiday
                            fields.SetField("adjregularndotytd", YearToDate.Rows[0][27].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][27].ToString())));//Double Holiday Night Diff
                            fields.SetField("adjregularotytd", YearToDate.Rows[0][28].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][28].ToString())));//Double Holiday OT
                            fields.SetField("adjrestdayotytd", YearToDate.Rows[0][29].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][29].ToString())));//Double Holiday OT

                            fields.SetField("grosspayytd", YearToDate.Rows[0][30].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][30].ToString())));//Basic Salary
                            fields.SetField("deductionssssytd", YearToDate.Rows[0][31].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][31].ToString())));//Basic Salary
                            fields.SetField("deductionsphilhealthytd", YearToDate.Rows[0][32].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][32].ToString())));//Basic Salary
                            fields.SetField("deductionspagibigytd", YearToDate.Rows[0][33].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][33].ToString())));//Basic Salary
                            fields.SetField("taxableincomeytd", YearToDate.Rows[0][34].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][34].ToString())));//Basic Salary
                            fields.SetField("deductionswtaxytd", YearToDate.Rows[0][35].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][35].ToString())));//Basic Salary
                            fields.SetField("deductionsplaytd", YearToDate.Rows[0][36].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][36].ToString())));//Basic Salary
                            fields.SetField("deductionsprulifeytd", YearToDate.Rows[0][37].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][37].ToString())));//Basic Salary
                            fields.SetField("deductionssdytd", YearToDate.Rows[0][38].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][38].ToString())));//Basic Salary
                            fields.SetField("deductionssssloanamountytd", YearToDate.Rows[0][39].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][39].ToString())));//Basic Salary
                            fields.SetField("deductionspagibigloanamountytd", YearToDate.Rows[0][40].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][40].ToString())));//Basic Salary
                            fields.SetField("ecolaytd", YearToDate.Rows[0][41].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][41].ToString())));//Basic Salary
                            fields.SetField("deminimisytd", YearToDate.Rows[0][42].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][42].ToString())));//Basic Salary
                            fields.SetField("adjnontaxableallowanceytd", YearToDate.Rows[0][54].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][54].ToString())));//Basic Salary
                            fields.SetField("13thmonthnontaxableytd", YearToDate.Rows[0][43].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][43].ToString())));//Basic Salary
                            fields.SetField("netpayytd", YearToDate.Rows[0][44].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][44].ToString())));//Basic Salary
                            fields.SetField("sssemployershareytd", YearToDate.Rows[0][45].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][45].ToString())));//Basic Salary
                            fields.SetField("sssemployerecshareytd", YearToDate.Rows[0][46].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][46].ToString())));//Basic Salary
                            fields.SetField("philhealthemployershareytd", YearToDate.Rows[0][47].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][47].ToString())));//Basic Salary
                            fields.SetField("pagibigemployershareytd", YearToDate.Rows[0][48].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][48].ToString())));//Basic Salary
                            fields.SetField("restdayytd", YearToDate.Rows[0][62].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][62].ToString())));//Rest Day Amount Year to Date
                            fields.SetField("adjrdytd", "0.00");//ADJ Rest Day Amount Year to Date
                        }
                        else
                        {

                        }
                    }
                    else
                    {
                        if (YearToDate.Rows.Count > 0)
                        {
                            fields.SetField("basicsalaryytd", YearToDate.Rows[0][0].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][0].ToString())));//Basic Salary
                            fields.SetField("nontaxableallowanceytd", YearToDate.Rows[0][1].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][1].ToString())));//Non Taxable Allowance
                            fields.SetField("regularnightdiffytd", YearToDate.Rows[0][2].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][2].ToString())));//Regular Night Diff
                            fields.SetField("regularotytd", YearToDate.Rows[0][3].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][3].ToString())));//Regular OT
                            fields.SetField("regularholidayytd", YearToDate.Rows[0][4].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][4].ToString())));//Regular Holiday
                            fields.SetField("regularholidaynightdiffytd", YearToDate.Rows[0][5].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][5].ToString())));//Regular Holiday Night Diff
                            fields.SetField("regularholidayotytd", YearToDate.Rows[0][6].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][6].ToString())));//Regular Holiday OT
                            fields.SetField("paidtimeoffytd", YearToDate.Rows[0][7].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][7].ToString())));//Paid Time Off
                            fields.SetField("specialholidayytd", YearToDate.Rows[0][8].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][8].ToString())));//Special Holiday
                            fields.SetField("specialholidaynightdiffytd", YearToDate.Rows[0][9].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][9].ToString())));//Special Holiday Night Diff
                            fields.SetField("specialholidayotytd", YearToDate.Rows[0][10].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][10].ToString())));//Special Holiday OT
                            fields.SetField("allowancesytd", YearToDate.Rows[0][11].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][11].ToString())));//Allowances
                            fields.SetField("bonusytd", YearToDate.Rows[0][12].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][12].ToString())));//Bonus
                            fields.SetField("adjustmentsytd", YearToDate.Rows[0][13].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][13].ToString())));//Adjustments
                            fields.SetField("totalytd", YearToDate.Rows[0][14].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][14].ToString())));//Gross Pay/Total
                            fields.SetField("employeehdmfytd", YearToDate.Rows[0][15].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][15].ToString())));//Employee HDMF
                            fields.SetField("employeephilhealthytd", YearToDate.Rows[0][16].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][16].ToString())));//Employee PhilHealth
                            fields.SetField("employeesocialsecurityytd", YearToDate.Rows[0][17].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][17].ToString())));//Employee Social Security
                            fields.SetField("withholdingtaxytd", YearToDate.Rows[0][18].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][18].ToString())));//WithHolding Tax
                            fields.SetField("sssloandeductionytd", YearToDate.Rows[0][19].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][19].ToString())));//SSS Loan Deduction
                            fields.SetField("pagibigloandeductionytd", YearToDate.Rows[0][20].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][20].ToString())));//Pag-ibig Loan Deduction
                            fields.SetField("netpayytd", YearToDate.Rows[0][21].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][21].ToString())));//Net Pay
                            fields.SetField("employerhdmfytd", YearToDate.Rows[0][22].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][22].ToString())));//Employer HDMF
                            fields.SetField("employerphilhealthytd", YearToDate.Rows[0][23].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][23].ToString())));//Employer PhilHealth
                            fields.SetField("employersocialsecurityytd", YearToDate.Rows[0][24].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][24].ToString())));//Employer Social Security
                            fields.SetField("employersocialsecurityecytd", YearToDate.Rows[0][25].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][25].ToString())));//Employer Social Security EC
                            fields.SetField("deductionsabsencesytd", YearToDate.Rows[0][26].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][26].ToString())));//Deductions Absences
                            fields.SetField("deductionslatesytd", YearToDate.Rows[0][27].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][27].ToString())));//Deductions Lates
                            fields.SetField("doubleholidayytd", YearToDate.Rows[0][28].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][28].ToString())));//DH_REG_Amount
                            fields.SetField("doubleholidayndytd", YearToDate.Rows[0][29].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][29].ToString())));//DH_ND_Amount
                            fields.SetField("doubleholidayotytd", YearToDate.Rows[0][30].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][30].ToString())));//DH_OT_Amount
                        }
                        else
                        {

                        }
                    }

                    if (DTProfile.Rows.Count > 0)
                    {
                        fields.SetField("monthlyrate", DTProfile.Rows[0]["Salary"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(DTProfile.Rows[0]["Salary"].ToString())));
                    }
                    else
                    {

                    }

                    //foreach (var field in af.Fields)
                    //{
                    //    Console.WriteLine("{0}, {1}", field.Key, field.Value);
                    //    //string sample = TextBox2.Text;
                    //    string sample2 = field.Key.ToString();
                    //    //TextBox2.Text = sample + field.Key.ToString() + ',';
                    //    fields.SetField(field.Key.ToString(), field.ToString());
                    //}


                    //------------------------------------------------

                    // form flattening rids the form of editable text fields so
                    // the final output can't be edited
                    stamper.FormFlattening = true;
                    // closing the stamper

                    stamper.Close();



                    //viewpdf.Attributes.Add("src", "pdf\\PaySlipDefault-" + empid + "-" + hoursdatefrom + "-" + hoursdateto + ".pdf");
                    //Response.Redirect("~/pdf/PaySlip-I101.pdf");
                    #endregion
                }
                #endregion
                return "Success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }

        [HttpPost]
        [Route("GetPayslipv2")]
        public string Payslipv4(UserProfilev2 uProfile)
        {
            try
            {
                GetDatabase GDB = new GetDatabase();
                GDB.ClientName = uProfile.CN;
                SelfieRegistration2Controller.GetDB2(GDB);


                string DDLMonths = "08";
                string DDLPayPeriod = uProfile.PayDay;
                string DDLYear = uProfile.PayYear;
                empid = uProfile.NTID;

                //empname = PayMtd.searchempname(empid);

                empidprof = empid;
                //GetEmpLevelandCategory = PayMtd.GetEmpLevelandCategory(empid);
                //string EmpLevel = GetEmpLevelandCategory.Rows[0][0].ToString();
                //string EmpCategory = GetEmpLevelandCategory.Rows[0][1].ToString();

                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = empid });
                paysliprecordsempmaster = con.GetDataTable("sp_GetPaySlipRecordsEmpMaster");

                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = empid });
                paysliprecordsempmaster = con.GetDataTable("sp_GetPaySlipRecordsEmpMaster");

                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = empid });
                paysliprecordsconfidentialinfo = con.GetDataTable("sp_GetPaySlipRecordsConfidentialInfo");
                //paysliprecordsconfidentialinfo = PayMtd.PaySlipRecordsConfidentialInfo(empid);

                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = empid });
                paysliprecordsemployeepay = con.GetDataTable("sp_GetPaySlipRecordsEmployeePay");
                //paysliprecordsemployeepay = PayMtd.PaySlipRecordsEmployeePay(empid);
                //int schmct = PayMtd.GetSalarySchemes(EmpLevel, EmpCategory).Rows.Count;

                #region "New Payslip"
                TotalValue = 0;
                int latestpayslipcounter = 0;

                con = new Connection();
                LatestPayslipForm = con.GetDataTable("sp_GetLatestPayslipForm");
                //LatestPayslipForm = PayMtd.LatestPayslipForm();
                latestpayslipcounter = LatestPayslipForm.Rows.Count;
                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = empid });
                con.myparameters.Add(new myParameters { ParameterName = "@MONTH", mytype = SqlDbType.NVarChar, Value = DDLMonths.ToString() });
                DataTable BonusPayoutMonths = con.GetDataTable("sp_GetBonusPayouts");
                //DataTable BonusPayoutMonths = PayMtd.BonusPayoutMonths(empid, DDLMonths.ToString());
                if (latestpayslipcounter == 1)
                {
                    #region Retrieve Neccessary Data
                    con = new Connection();
                    con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = empid });
                    GetEmpLevelandCategory = con.GetDataTable("sp_SearchMasterControl_LeaveManagement_GetEmpLevelandCategory");

                    //GetEmpLevelandCategory = PayMtd.GetEmpLevelandCategory(empid);
                    int psrpsd = 0;
                    int pswh = 0;
                    //paysliprecordspayperiod = PayMtd.PaySlipRecordsPayPeriod(EmpLevel, EmpCategory);                
                    payperiod = hoursdatefrom + '-' + hoursdateto;

                    con = new Connection();
                    con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = empid });
                    con.myparameters.Add(new myParameters { ParameterName = "@PAYPERIOD", mytype = SqlDbType.NVarChar, Value = DDLPayPeriod });
                    paysliprecordspayslipdetails = con.GetDataTable("sp_GetPaySlipRecordsPaySlipDetails");

                    con = new Connection();
                    con.myparameters.Add(new myParameters { ParameterName = "@empId", mytype = SqlDbType.NVarChar, Value = empid });
                    DataTable DTProfile = con.GetDataTable("sp_getCompleteProfile_Mobilev2");
                    //paysliprecordspayslipdetails = PayMtd.PaySlipRecordsPaySlipDetails(empid, DDLPayPeriod);
                    #endregion
                    string pathmoto;
                    //if (uProfile.CN.ToLower() == "newcore")
                    //{
                    //    pathmoto = "PaySlipDefaultNewcore";
                    //}
                    //else
                    //{
                    //    pathmoto = "PaySlipDefault";
                    //}
                    pathmoto = "PaySlipDefault";
                    //LatestPayslipForm.Rows[0][0].ToString();
                    PdfReader reader1 = new PdfReader(HttpContext.Current.Server.MapPath("~/pdf/") + pathmoto + ".pdf");
                    LocationTextExtractionStrategy lteStrategy = new LocationTextExtractionStrategy();
                    string strExact = PdfTextExtractor.GetTextFromPage(reader1, 1, lteStrategy);
                    StringBuilder sb = new StringBuilder();
                    //this code used for replacing the found text
                    sb.Append(PdfTextExtractor.GetTextFromPage(reader1, 1));
                    #region Passing Retrieved Datas
                    sb.Replace("[Employee ID]", empid);
                    if (paysliprecordsempmaster.Rows.Count > 0)
                    {
                        sb.Replace("[Employee Name]", paysliprecordsempmaster.Rows[0][0].ToString() == "" ? "N/A" : paysliprecordsempmaster.Rows[0][0].ToString());
                        sb.Replace("[Date Joined]", paysliprecordsempmaster.Rows[0][2].ToString() == "" ? "N/A" : paysliprecordsempmaster.Rows[0][2].ToString());
                        sb.Replace("[Resigned]", paysliprecordsempmaster.Rows[0][3].ToString() == "" ? "N/A" : paysliprecordsempmaster.Rows[0][3].ToString());
                    }
                    else
                    {
                        sb.Replace("[Employee Name]", "N/A");
                        sb.Replace("[Date Joined]", "N/A");
                        sb.Replace("[Resigned]", "N/A");
                    }
                    if (paysliprecordsconfidentialinfo.Rows.Count > 0)
                    {
                        sb.Replace("[HDMF]", paysliprecordsconfidentialinfo.Rows[0][0].ToString() == "" ? "N/A" : paysliprecordsconfidentialinfo.Rows[0][0].ToString());
                        sb.Replace("[PhilHealth No]", paysliprecordsconfidentialinfo.Rows[0][1].ToString() == "" ? "N/A" : paysliprecordsconfidentialinfo.Rows[0][1].ToString());
                        sb.Replace("[SSS No]", paysliprecordsconfidentialinfo.Rows[0][2].ToString() == "" ? "N/A" : paysliprecordsconfidentialinfo.Rows[0][2].ToString());
                        sb.Replace("[TIN]", paysliprecordsconfidentialinfo.Rows[0][3].ToString() == "" ? "N/A" : paysliprecordsconfidentialinfo.Rows[0][3].ToString());
                        sb.Replace("[Tax Category]", paysliprecordsconfidentialinfo.Rows[0][4].ToString() == "" ? "N/A" : paysliprecordsconfidentialinfo.Rows[0][4].ToString());
                    }
                    else
                    {
                        sb.Replace("[HDMF]", "N/A");
                        sb.Replace("[PhilHealth No]", "N/A");
                        sb.Replace("[SSS No]", "N/A");
                        sb.Replace("[TIN]", "N/A");
                        sb.Replace("[Tax Category]", "N/A");
                    }
                    if (paysliprecordsemployeepay.Rows.Count > 0)
                    {

                        sb.Replace("[Bank Account]", paysliprecordsemployeepay.Rows[0][0].ToString() == "" ? "N/A" : paysliprecordsemployeepay.Rows[0][0].ToString());
                        sb.Replace("[Hourly Rate]", paysliprecordsemployeepay.Rows[0][1].ToString() == "" ? "0.00" : paysliprecordsemployeepay.Rows[0][1].ToString());
                    }
                    else
                    {
                        sb.Replace("[Bank Account]", "N/A");
                        sb.Replace("[Hourly Rate]", "N/A");
                    }
                    sb.Replace("[Pay Period]", DDLPayPeriod == "" ? "N/A" : DDLPayPeriod);
                    if (paysliprecordspayslipdetails.Rows.Count > 0)
                    {
                        sb.Replace("[Pay Day]", paysliprecordspayslipdetails.Rows[0][1].ToString() == "" ? "N/A" : paysliprecordspayslipdetails.Rows[0][1].ToString());
                        sb.Replace("[Basic Salary]", paysliprecordspayslipdetails.Rows[0][2].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][2].ToString())));
                        sb.Replace("[Non Taxable Allowance]", paysliprecordspayslipdetails.Rows[0][3].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][3].ToString())));
                        sb.Replace("[Regular Night Differential]", paysliprecordspayslipdetails.Rows[0][4].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][4].ToString())));
                        sb.Replace("[Regular Over Time]", paysliprecordspayslipdetails.Rows[0][5].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][5].ToString())));
                        sb.Replace("[Regular Holiday]", paysliprecordspayslipdetails.Rows[0][6].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][6].ToString())));
                        sb.Replace("[Regular Holiday Night Differential]", paysliprecordspayslipdetails.Rows[0][7].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][7].ToString())));
                        sb.Replace("[Regular Holiday Over Time]", paysliprecordspayslipdetails.Rows[0][8].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][8].ToString())));
                        sb.Replace("[Paid Time Off]", paysliprecordspayslipdetails.Rows[0][9].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][9].ToString())));
                        sb.Replace("[Special Holiday]", paysliprecordspayslipdetails.Rows[0][10].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][10].ToString())));
                        sb.Replace("[Special Holiday Night Differential]", paysliprecordspayslipdetails.Rows[0][11].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][11].ToString())));
                        sb.Replace("[Special Holiday Over Time]", paysliprecordspayslipdetails.Rows[0][12].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][12].ToString())));
                        sb.Replace("[Allowance]", paysliprecordspayslipdetails.Rows[0][13].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][13].ToString())));
                        sb.Replace("[Bonuses]", paysliprecordspayslipdetails.Rows[0][14].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][14].ToString())));
                        sb.Replace("[Adjustments]", paysliprecordspayslipdetails.Rows[0][15].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][15].ToString())));
                        sb.Replace("[Total Salary]", paysliprecordspayslipdetails.Rows[0][16].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][16].ToString())));
                        sb.Replace("[Employee HDMF]", paysliprecordspayslipdetails.Rows[0][17].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][17].ToString())));
                        sb.Replace("[Employee Philhealth]", paysliprecordspayslipdetails.Rows[0][18].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][18].ToString())));
                        sb.Replace("[Employee SSS]", paysliprecordspayslipdetails.Rows[0][19].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][19].ToString())));
                        sb.Replace("[Employee WithHoldingTax]", paysliprecordspayslipdetails.Rows[0][20].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][20].ToString())));
                        sb.Replace("[SSS Loan Deduction]", paysliprecordspayslipdetails.Rows[0][21].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][21].ToString())));
                        sb.Replace("[Pag-ibig Loan Deduction]", paysliprecordspayslipdetails.Rows[0][22].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][22].ToString())));
                        sb.Replace("[Net Pay]", paysliprecordspayslipdetails.Rows[0][23].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][23].ToString())));
                        sb.Replace("[Employer HDMF]", paysliprecordspayslipdetails.Rows[0][24].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][24].ToString())));
                        sb.Replace("[Employer Philhealth]", paysliprecordspayslipdetails.Rows[0][25].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][25].ToString())));
                        sb.Replace("[Employer SSS]", paysliprecordspayslipdetails.Rows[0][26].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][26].ToString())));
                        sb.Replace("[Employer SSS EC]", paysliprecordspayslipdetails.Rows[0][27].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][27].ToString())));
                    }
                    else
                    {
                        sb.Replace("[Pay Day]", "N/A");
                        sb.Replace("[Basic Salary]", "N/A");
                        sb.Replace("[Non Taxable Allowance]", "N/A");
                        sb.Replace("[Regular Night Differential]", "N/A");
                        sb.Replace("[Regular Over Time]", "N/A");
                        sb.Replace("[Regular Holiday]", "N/A");
                        sb.Replace("[Regular Holiday Night Differential]", "N/A");
                        sb.Replace("[Regular Holiday Over Time]", "N/A");
                        sb.Replace("[Paid Time Off]", "N/A");
                        sb.Replace("[Special Holiday]", "N/A");
                        sb.Replace("[Special Holiday Night Differential]", "N/A");
                        sb.Replace("[Special Holiday Over Time]", "N/A");
                        sb.Replace("[Allowance]", "N/A");
                        sb.Replace("[Bonuses]", "N/A");
                        sb.Replace("[Adjustments]", "N/A");
                        sb.Replace("[Total Salary]", "N/A");
                        sb.Replace("[Employee HDMF]", "N/A");
                        sb.Replace("[Employee Philhealth]", "N/A");
                        sb.Replace("[Employee SSS]", "N/A");
                        sb.Replace("[Employee WithHoldingTax]", "N/A");
                        sb.Replace("[SSS Loan Deduction]", "N/A");
                        sb.Replace("[Pag-ibig Loan Deduction]", "N/A");
                        sb.Replace("[Net Pay]", "N/A");
                        sb.Replace("[Employer HDMF]", "N/A");
                        sb.Replace("[Employer Philhealth]", "N/A");
                        sb.Replace("[Employer SSS]", "N/A");
                        sb.Replace("[Employer SSS EC]", "N/A");
                    }

                    if (DTProfile.Rows.Count > 0)
                    {
                        sb.Replace("[Salary]", DTProfile.Rows[0]["Salary"].ToString() == "" ? "0.00" : string.Format("{0:n}", Convert.ToDouble(DTProfile.Rows[0]["Salary"].ToString())));
                    }
                    else
                    {
                        sb.Replace("[Salary]", "N/A");
                    }

                    #endregion

                    FileStream Newstream = new FileStream(string.Concat(HttpContext.Current.Server.MapPath("~/pdf/"), empid + pathmoto + ".pdf"), FileMode.Create);
                    MemoryStream ms = new MemoryStream();
                    Document document = new Document(PageSize.A4, 25, 25, 30, 30);
                    //Code for adding the string builder sb to the document
                    PdfWriter writer = PdfWriter.GetInstance(document, Newstream);
                    document.Open();
                    document.Add(new Paragraph(sb.ToString()));
                    document.Close();
                    writer.Close();
                }
                else
                {
                    #region Fixed payslip

                    int psrpsd = 0;
                    int pswh = 0;


                    //paysliprecordspayslipdetails = PayMtd.PaySlipRecordsPaySlipDetails(empid, DDLPayPeriod);



                    con = new Connection();
                    con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = empid });
                    con.myparameters.Add(new myParameters { ParameterName = "@YEAR", mytype = SqlDbType.NVarChar, Value = DDLYear });
                    con.myparameters.Add(new myParameters { ParameterName = "@PAYPERIOD", mytype = SqlDbType.NVarChar, Value = DDLPayPeriod });
                    YearToDate = con.GetDataTable("sp_GetPaySlipRecordsPaySlipDetailsYearToDate");

                    con = new Connection();
                    con.myparameters.Add(new myParameters { ParameterName = "@empId", mytype = SqlDbType.NVarChar, Value = empid });
                    DataTable DTProfile = con.GetDataTable("sp_getCompleteProfile_Mobilev2");

                    //YearToDate = PayMtd.PayslipDetailsYearToDate(empid, DDLYear, DDLPayPeriod);

                    //Response.Redirect("~/pdf/BIR19021.pdf"); // view pdf
                    string path = "https://ws.durusthr.com/ILM_WS_Live/pdf";

                    //if (uProfile.CN.ToLower() == "newcore")
                    //{
                    //    pdfpath = "/PaySlipDefaultNewcore.pdf";
                    //}
                    //else
                    //{
                    //    pdfpath = "/PaySlipDefault.pdf";
                    //}
                    var pdfReader1 = new PdfReader(path + "/PaySlipDefault.pdf");

                    AcroFields af = pdfReader1.AcroFields;
                    //LocationTextExtractionStrategy lteStrategy = new LocationTextExtractionStrategy();
                    //string strExact = PdfTextExtractor.GetTextFromPage(pdfReader1,1, lteStrategy);
                    //StringBuilder sb = new StringBuilder();
                    string sam = "";
                    foreach (var field in af.Fields)
                    {
                        sam = sam + field.Key + "-"; // names of textfields
                    }
                    //viewpdf.InnerText = sam;
                    // get the file paths


                    // Template file path

                    //if (uProfile.CN.ToLower() == "newcore")
                    //{
                    //    formFile = path + "/PaySlipDefaultNewcore.pdf";
                    //}
                    //else
                    //{
                    //    formFile = path + "/PaySlipDefault.pdf";
                    //}
                    string formFile = "/PaySlipDefault.pdf";
                    string path2 = HttpContext.Current.Server.MapPath("pdf");
                    // Output file path
                    string newFile = path2 + "/PaySlip-" + empid + ".pdf";


                    // read the template file
                    PdfReader reader;
                    if (uProfile.CN.ToLower() == "newcore")
                    {
                        reader = new PdfReader(path2 + "/PaySlipDefaultNewcore.pdf");
                    }
                    else if (uProfile.CN.ToLower() == "generali_dev")
                    {
                        reader = new PdfReader(path2 + "/PaySlipDefaultGenerali.pdf");
                    }
                    else if (uProfile.CN.ToLower() == "generali")
                    {
                        reader = new PdfReader(path2 + "/PaySlipDefaultGenerali.pdf");
                    }
                    else if (uProfile.CN.ToLower() == "paat")
                    {
                        reader = new PdfReader(path2 + "/PAAT_PAYSLIP.pdf");
                    }
                    else
                    {
                        reader = new PdfReader(path2 + "/PaySlipDefault.pdf");
                    }




                    // instantiate PDFStamper object
                    // The Output file will be created from template file and edited by the PDFStamper
                    PdfStamper stamper = new PdfStamper(reader, new FileStream(newFile, FileMode.Create));
                    //stamper.SetEncryption(true, empid, "ilm", PdfWriter.ALLOW_SCREENREADERS);
                    //PdfEncryptor.Encrypt(reader, new FileStream(newFile, FileMode.Open), true, empid, "secret", PdfWriter.ALLOW_SCREENREADERS);
                    // Object to deal with the Output file's textfields
                    AcroFields fields = stamper.AcroFields;

                    // set form fields("Field Name in PDF", "String to be placed in this PDF text field");
                    //for (int i = 0; i <= 1000; i++)
                    //{
                    //    string ii = i.ToString();
                    //    
                    //    fields.SetField("untitled" + i, "vt" + ii); //temporary values
                    //}

                    //------------------------------------------------
                    //loop to display all field names
                    if (paysliprecordsempmaster.Rows.Count > 0)
                    {
                        //fields.SetFieldProperty("txtempid", "setflags", PdfAnnotation.FLAGS_HIDDEN,null);
                        fields.SetField("untitled2", paysliprecordsempmaster.Rows[0][0].ToString());//Employee Name
                        fields.SetField("employeeno", empid);//Employee No
                        fields.SetField("joined", paysliprecordsempmaster.Rows[0][2].ToString());//Joined
                        fields.SetField("resigned", paysliprecordsempmaster.Rows[0][3].ToString());//Resigned
                    }
                    else
                    {

                    }
                    if (paysliprecordsconfidentialinfo.Rows.Count > 0)
                    {
                        fields.SetField("pagibigno", paysliprecordsconfidentialinfo.Rows[0][0].ToString());//Pag-IBIG No
                        fields.SetField("philhealthno", paysliprecordsconfidentialinfo.Rows[0][1].ToString());//PhilHealth No
                        fields.SetField("sssno", paysliprecordsconfidentialinfo.Rows[0][2].ToString());//SSS No
                        fields.SetField("taxno", paysliprecordsconfidentialinfo.Rows[0][3].ToString());//Tax No
                        fields.SetField("taxcategory", paysliprecordsconfidentialinfo.Rows[0][4].ToString());//Tax Category
                    }
                    else
                    {

                    }
                    if (paysliprecordsemployeepay.Rows.Count > 0)
                    {
                        fields.SetField("bankaccount", paysliprecordsemployeepay.Rows[0][0].ToString());//Bank Account
                        fields.SetField("hourlyrate", paysliprecordsemployeepay.Rows[0][1].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordsemployeepay.Rows[0][1].ToString())));//Hourly Rate
                    }
                    else
                    {
                        fields.SetField("hourlyrate", "0.00");
                    }
                    fields.SetField("payperiod", DDLPayPeriod);//Pay Period

                    con = new Connection();
                    con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = empid });
                    con.myparameters.Add(new myParameters { ParameterName = "@PAYPERIOD", mytype = SqlDbType.NVarChar, Value = DDLPayPeriod });
                    if (uProfile.CN.ToLower() == "newcore")
                    {
                        paysliprecordspayslipdetails = con.GetDataTable("sp_GetPaySlipRecordsPaySlipDetailsNewcore");
                        if (paysliprecordspayslipdetails.Rows.Count > 0)
                        {
                            fields.SetField("payday", paysliprecordspayslipdetails.Rows[0][0].ToString());//Pay Day
                            fields.SetField("regularothrs", paysliprecordspayslipdetails.Rows[0][1].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][1].ToString())));//Basic Salary
                            fields.SetField("regularndothrs", paysliprecordspayslipdetails.Rows[0][2].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][2].ToString())));//Basic Salary
                            fields.SetField("restdayndothrs", paysliprecordspayslipdetails.Rows[0][3].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][3].ToString())));//Basic Salary
                            fields.SetField("restdayothrs", paysliprecordspayslipdetails.Rows[0][52].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][52].ToString())));//Basic Salary
                            fields.SetField("regularholidayhrs", paysliprecordspayslipdetails.Rows[0][4].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][4].ToString())));//Basic Salary
                            fields.SetField("regularholidayothrs", paysliprecordspayslipdetails.Rows[0][5].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][5].ToString())));//Basic Salary
                            fields.SetField("regularholidayndothrs", paysliprecordspayslipdetails.Rows[0][53].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][53].ToString())));//Basic Salary
                            fields.SetField("specialholidayhrs", paysliprecordspayslipdetails.Rows[0][6].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][6].ToString())));//Basic Salary
                            fields.SetField("specialholidayothrs", paysliprecordspayslipdetails.Rows[0][7].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][7].ToString())));//Basic Salary
                            fields.SetField("specialholidayndothrs", paysliprecordspayslipdetails.Rows[0][54].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][54].ToString())));//Basic Salary
                            fields.SetField("adjabsencehrs", paysliprecordspayslipdetails.Rows[0][59].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][59].ToString())));//Basic Salary
                            fields.SetField("adjlatehrs", paysliprecordspayslipdetails.Rows[0][61].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][61].ToString())));//Basic Salary
                            fields.SetField("adjrdndothrs", paysliprecordspayslipdetails.Rows[0][63].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][63].ToString())));//Basic Salary
                            fields.SetField("adjregularhrs", paysliprecordspayslipdetails.Rows[0][10].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][10].ToString())));//Basic Salary
                            fields.SetField("adjregularndothrs", paysliprecordspayslipdetails.Rows[0][11].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][11].ToString())));//Basic Salary
                            fields.SetField("adjregularothrs", paysliprecordspayslipdetails.Rows[0][12].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][12].ToString())));//Basic Salary
                            fields.SetField("adjrestdayothrs", paysliprecordspayslipdetails.Rows[0][13].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][13].ToString())));//Basic Salary
                            fields.SetField("basicsalarycurrent", paysliprecordspayslipdetails.Rows[0][14].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][14].ToString())));//Basic Salary
                            fields.SetField("deductionsabsencescurrent", paysliprecordspayslipdetails.Rows[0][15].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][15].ToString())));//Basic Salary
                            fields.SetField("deductionstardinesscurrent", paysliprecordspayslipdetails.Rows[0][16].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][16].ToString())));//Basic Salary
                            fields.SetField("13thmonthtaxablecurrent", paysliprecordspayslipdetails.Rows[0][17].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][17].ToString())));//Basic Salary
                            fields.SetField("regularotcurrent", paysliprecordspayslipdetails.Rows[0][18].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][18].ToString())));//Basic Salary
                            fields.SetField("regularndotcurrent", paysliprecordspayslipdetails.Rows[0][19].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][19].ToString())));//Basic Salary

                            fields.SetField("restdayotcurrent", paysliprecordspayslipdetails.Rows[0][50].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][50].ToString())));//Basic Salary
                            fields.SetField("restdayndotcurrent", paysliprecordspayslipdetails.Rows[0][20].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][20].ToString())));//Basic Salary
                            fields.SetField("regularholidaycurrent", paysliprecordspayslipdetails.Rows[0][21].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][21].ToString())));//Basic Salary
                            fields.SetField("regularholidayotcurrent", paysliprecordspayslipdetails.Rows[0][22].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][22].ToString())));//Basic Salary
                            fields.SetField("regularholidayndotcurrent", paysliprecordspayslipdetails.Rows[0][51].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][51].ToString())));//Basic Salary
                            fields.SetField("specialholidaycurrent", paysliprecordspayslipdetails.Rows[0][23].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][23].ToString())));//Basic Salary
                            fields.SetField("specialholidayotcurrent", paysliprecordspayslipdetails.Rows[0][24].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][24].ToString())));//Basic Salary
                            fields.SetField("specialholidayndotcurrent", paysliprecordspayslipdetails.Rows[0][55].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][55].ToString())));//Basic Salary
                            fields.SetField("adjabsencecurrent", paysliprecordspayslipdetails.Rows[0][60].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][60].ToString())));//Basic Salary
                            fields.SetField("adjlatecurrent", paysliprecordspayslipdetails.Rows[0][62].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][62].ToString())));//Basic Salary
                            fields.SetField("adjrdndotcurrent", paysliprecordspayslipdetails.Rows[0][64].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][64].ToString())));//Basic Salary
                            fields.SetField("adjregularhourscurrent", paysliprecordspayslipdetails.Rows[0][27].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][27].ToString())));//Basic Salary
                            fields.SetField("adjregularndotcurrent", paysliprecordspayslipdetails.Rows[0][28].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][28].ToString())));//Basic Salary
                            fields.SetField("adjregularotcurrent", paysliprecordspayslipdetails.Rows[0][29].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][29].ToString())));//Basic Salary
                            fields.SetField("adjrestdayotcurrent", paysliprecordspayslipdetails.Rows[0][30].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][30].ToString())));//Basic Salary
                            fields.SetField("grosspaycurrent", paysliprecordspayslipdetails.Rows[0][31].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][31].ToString())));//Basic Salary
                            fields.SetField("deductionsssscurrent", paysliprecordspayslipdetails.Rows[0][32].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][32].ToString())));//Basic Salary
                            fields.SetField("deductionsphilhealthcurrent", paysliprecordspayslipdetails.Rows[0][33].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][33].ToString())));//Basic Salary
                            fields.SetField("deductionspagibigcurrent", paysliprecordspayslipdetails.Rows[0][34].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][34].ToString())));//Basic Salary
                            fields.SetField("taxableincomecurrent", paysliprecordspayslipdetails.Rows[0][35].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][35].ToString())));//Basic Salary
                            fields.SetField("deductionswtaxcurrent", paysliprecordspayslipdetails.Rows[0][36].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][36].ToString())));//Basic Salary
                            fields.SetField("deductionsplacurrent", paysliprecordspayslipdetails.Rows[0][37].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][37].ToString())));//Basic Salary
                            fields.SetField("deductionsprulifecurrent", paysliprecordspayslipdetails.Rows[0][38].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][38].ToString())));//Basic Salary
                            fields.SetField("deductionssdcurrent", paysliprecordspayslipdetails.Rows[0][39].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][39].ToString())));//Basic Salary
                            fields.SetField("deductionssssloanamountcurrent", paysliprecordspayslipdetails.Rows[0][40].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][40].ToString())));//Basic Salary
                            fields.SetField("deductionspagibigloanamountcurrent", paysliprecordspayslipdetails.Rows[0][41].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][41].ToString())));//Basic Salary
                            fields.SetField("ecolacurrent", paysliprecordspayslipdetails.Rows[0][42].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][42].ToString())));//Basic Salary
                            fields.SetField("deminimiscurrent", paysliprecordspayslipdetails.Rows[0][43].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][43].ToString())));//Basic Salary
                            fields.SetField("adjnontaxableallowancecurrent", paysliprecordspayslipdetails.Rows[0][58].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][58].ToString())));//Basic Salary
                            fields.SetField("13thmonthnontaxablecurrent", paysliprecordspayslipdetails.Rows[0][44].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][44].ToString())));//Basic Salary
                            fields.SetField("netpaycurrent", paysliprecordspayslipdetails.Rows[0][45].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][45].ToString())));//Basic Salary
                            fields.SetField("sssemployersharecurrent", paysliprecordspayslipdetails.Rows[0][46].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][46].ToString())));//Basic Salary
                            fields.SetField("sssemployerecsharecurrent", paysliprecordspayslipdetails.Rows[0][47].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][47].ToString())));//Basic Salary
                            fields.SetField("philhealthemployersharecurrent", paysliprecordspayslipdetails.Rows[0][48].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][48].ToString())));//Basic Salary
                            fields.SetField("pagibigemployersharecurrent", paysliprecordspayslipdetails.Rows[0][49].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][49].ToString())));//Basic Salary
                            fields.SetField("restdayhrs", paysliprecordspayslipdetails.Rows[0][65].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][65].ToString())));//Rest Day Hours
                            fields.SetField("restdaycurrent", paysliprecordspayslipdetails.Rows[0][66].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][66].ToString())));//Rest Day Amount
                            fields.SetField("adjrdhrs", "0.00");//ADJ Rest Day Hours
                            fields.SetField("adjrdcurrent", "0.00");//ADJ Rest Day Amount
                        }
                        else
                        {

                        }
                    }
                    else
                    {
                        paysliprecordspayslipdetails = con.GetDataTable("sp_GetPaySlipRecordsPaySlipDetails");
                        if (paysliprecordspayslipdetails.Rows.Count > 0)
                        {
                            fields.SetField("payday", paysliprecordspayslipdetails.Rows[0][1].ToString());//Pay Day
                            fields.SetField("basicsalarycurrent", paysliprecordspayslipdetails.Rows[0][2].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][2].ToString())));//Basic Salary
                            fields.SetField("nontaxableallowancecurrent", paysliprecordspayslipdetails.Rows[0][3].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][3].ToString())));//Non Taxable Allowance
                            fields.SetField("regularnightdiffcurrent", paysliprecordspayslipdetails.Rows[0][4].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][4].ToString())));//Regular Night Diff
                            fields.SetField("regularotcurrent", paysliprecordspayslipdetails.Rows[0][5].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][5].ToString())));//Regular Overtime
                            fields.SetField("regularholidaycurrent", paysliprecordspayslipdetails.Rows[0][6].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][6].ToString())));//Regular Holiday
                            fields.SetField("regularholidaynightdiffcurrent", paysliprecordspayslipdetails.Rows[0][7].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][7].ToString())));//Regular Holiday Night Diff
                            fields.SetField("regularholidayotcurrent", paysliprecordspayslipdetails.Rows[0][8].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][8].ToString())));//Regular Overtime
                            fields.SetField("paidtimeoffcurrent", paysliprecordspayslipdetails.Rows[0][9].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][9].ToString())));//Paid Time Off
                            fields.SetField("specialholidaycurrent", paysliprecordspayslipdetails.Rows[0][10].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][10].ToString())));//Special Holiday
                            fields.SetField("specialholidaynightdiffcurrent", paysliprecordspayslipdetails.Rows[0][11].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][11].ToString())));//Special Holiday Night Diff
                            fields.SetField("specialholidayotcurrent", paysliprecordspayslipdetails.Rows[0][12].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][12].ToString())));//Special Holiday OT
                            fields.SetField("allowancescurrent", paysliprecordspayslipdetails.Rows[0][13].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][13].ToString())));//Allowances
                            fields.SetField("bonuscurrent", paysliprecordspayslipdetails.Rows[0][14].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][14].ToString())));//Bonus
                            fields.SetField("adjustmentscurrent", paysliprecordspayslipdetails.Rows[0][15].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][15].ToString())));//adjustments
                            fields.SetField("totalcurrent", paysliprecordspayslipdetails.Rows[0][16].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][16].ToString())));//Gross Pay
                            fields.SetField("employeehdmfcurrent", paysliprecordspayslipdetails.Rows[0][17].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][17].ToString())));//Employee HDMF
                            fields.SetField("employeephilhealthcurrent", paysliprecordspayslipdetails.Rows[0][18].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][18].ToString())));//Employee PhilHealth
                            fields.SetField("employeesocialsecuritycurrent", paysliprecordspayslipdetails.Rows[0][19].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][19].ToString())));//Employee SSS
                            fields.SetField("withholdingtaxcurrent", paysliprecordspayslipdetails.Rows[0][20].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][20].ToString())));//Employee WithHoldingTax
                            fields.SetField("sssloandeductioncurrent", paysliprecordspayslipdetails.Rows[0][21].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][21].ToString())));//Loans SSS
                            fields.SetField("pagibigloandeductioncurrent", paysliprecordspayslipdetails.Rows[0][22].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][22].ToString())));//Loans Pagibig
                            fields.SetField("netpaycurrent", paysliprecordspayslipdetails.Rows[0][23].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][23].ToString())));//Net Pay
                            fields.SetField("employerhdmfcurrent", paysliprecordspayslipdetails.Rows[0][24].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][24].ToString())));//Employer HDMF
                            fields.SetField("employerphilhealthcurrent", paysliprecordspayslipdetails.Rows[0][25].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][25].ToString())));//Employer PhilHealth
                            fields.SetField("employersocialsecuritycurrent", paysliprecordspayslipdetails.Rows[0][26].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][26].ToString())));//Employer SSS
                            fields.SetField("employersocialsecurityeccurrent", paysliprecordspayslipdetails.Rows[0][27].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][27].ToString())));//Employer SSS EC
                            fields.SetField("deductionsabsencescurrent", paysliprecordspayslipdetails.Rows[0][28].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][28].ToString())));//Deductions Absences
                            fields.SetField("deductionslatescurrent", paysliprecordspayslipdetails.Rows[0][29].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][29].ToString())));//Deductions Lates
                            fields.SetField("doubleholidaycurrent", paysliprecordspayslipdetails.Rows[0][30].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][30].ToString())));//Double Holiday
                            fields.SetField("doubleholidayndcurrent", paysliprecordspayslipdetails.Rows[0][31].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][31].ToString())));//Double Holiday ND
                            fields.SetField("doubleholidayotcurrent", paysliprecordspayslipdetails.Rows[0][32].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][32].ToString())));//Double Holiday OT
                        }
                        else
                        {

                        }
                    }

                    if (uProfile.CN.ToLower() == "newcore")
                    {
                        con = new Connection();
                        con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = empid });
                        con.myparameters.Add(new myParameters { ParameterName = "@PAYPERIOD", mytype = SqlDbType.NVarChar, Value = DDLPayPeriod });
                        con.myparameters.Add(new myParameters { ParameterName = "@YEAR", mytype = SqlDbType.NVarChar, Value = uProfile.PayYear });
                        YearToDate = con.GetDataTable("sp_GetPaySlipRecordsPaySlipDetailsYearToDateNewcore");
                        if (YearToDate.Rows.Count > 0)
                        {
                            //fields.SetField("payday", YearToDate.Rows[0][0].ToString());//Pay Day
                            //fields.SetField("regularothrs", YearToDate.Rows[0][1].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][1].ToString())));//Basic Salary
                            //fields.SetField("regularndothrs", YearToDate.Rows[0][2].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][2].ToString())));//Basic Salary
                            //fields.SetField("restdayndothrs", YearToDate.Rows[0][3].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][3].ToString())));//Basic Salary
                            //fields.SetField("regularholidayhrs", YearToDate.Rows[0][4].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][4].ToString())));//Basic Salary
                            //fields.SetField("regularholidayothrs", YearToDate.Rows[0][5].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][5].ToString())));//Basic Salary
                            //fields.SetField("specialholidayhrs", YearToDate.Rows[0][6].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][6].ToString())));//Basic Salary
                            //fields.SetField("specialholidayothrs", YearToDate.Rows[0][7].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][7].ToString())));//Basic Salary
                            //fields.SetField("doubleholidayhrs", YearToDate.Rows[0][8].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][8].ToString())));//Basic Salary
                            //fields.SetField("doubleholidayothrs", YearToDate.Rows[0][9].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][9].ToString())));//Basic Salary
                            //fields.SetField("adjregularhrs", YearToDate.Rows[0][10].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][10].ToString())));//Basic Salary
                            //fields.SetField("adjregularndothrs", YearToDate.Rows[0][11].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][11].ToString())));//Basic Salary
                            //fields.SetField("adjregularothrs", YearToDate.Rows[0][12].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][12].ToString())));//Basic Salary
                            //fields.SetField("adjrestdayothrs", YearToDate.Rows[0][13].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][13].ToString())));//Basic Salary
                            fields.SetField("basicsalaryytd", YearToDate.Rows[0][13].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][13].ToString())));//Basic Salary
                            fields.SetField("deductionsabsencesytd", YearToDate.Rows[0][14].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][14].ToString())));//Deduction absences
                            fields.SetField("deductionstardinessytd", YearToDate.Rows[0][15].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][15].ToString())));//Deduction lates
                            fields.SetField("13thmonthtaxableytd", YearToDate.Rows[0][16].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][16].ToString())));//13 month

                            fields.SetField("regularotytd", YearToDate.Rows[0][17].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][17].ToString())));//Regular OT
                            fields.SetField("regularndotytd", YearToDate.Rows[0][18].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][18].ToString())));//Regular Night Diff

                            fields.SetField("restdayotytd", YearToDate.Rows[0][52].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][52].ToString())));//Reg RDOT
                            fields.SetField("restdayndotytd", YearToDate.Rows[0][19].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][19].ToString())));//Reg RDNDOT

                            fields.SetField("regularholidayytd", YearToDate.Rows[0][20].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][20].ToString())));//Regular Holiday
                            fields.SetField("regularholidayndotytd", YearToDate.Rows[0][50].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][50].ToString())));//Regular Holiday Night Diff
                            fields.SetField("regularholidayotytd", YearToDate.Rows[0][21].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][21].ToString())));//Regular Holiday OT

                            fields.SetField("specialholidayytd", YearToDate.Rows[0][22].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][22].ToString())));//Special Holiday
                            fields.SetField("specialholidayndotytd", YearToDate.Rows[0][51].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][51].ToString())));//Special Holiday Night Diff
                            fields.SetField("specialholidayotytd", YearToDate.Rows[0][23].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][23].ToString())));//Special Holiday OT

                            fields.SetField("adjabsenceytd", YearToDate.Rows[0][56].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][56].ToString())));//Double Holiday
                            fields.SetField("adjlateytd", YearToDate.Rows[0][58].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][58].ToString())));//Double Holiday Night Diff
                            fields.SetField("adjrdndotytd", YearToDate.Rows[0][60].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][60].ToString())));//Double Holiday OT

                            fields.SetField("adjregularhoursytd", YearToDate.Rows[0][26].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][26].ToString())));//Double Holiday
                            fields.SetField("adjregularndotytd", YearToDate.Rows[0][27].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][27].ToString())));//Double Holiday Night Diff
                            fields.SetField("adjregularotytd", YearToDate.Rows[0][28].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][28].ToString())));//Double Holiday OT
                            fields.SetField("adjrestdayotytd", YearToDate.Rows[0][29].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][29].ToString())));//Double Holiday OT

                            fields.SetField("grosspayytd", YearToDate.Rows[0][30].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][30].ToString())));//Basic Salary
                            fields.SetField("deductionssssytd", YearToDate.Rows[0][31].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][31].ToString())));//Basic Salary
                            fields.SetField("deductionsphilhealthytd", YearToDate.Rows[0][32].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][32].ToString())));//Basic Salary
                            fields.SetField("deductionspagibigytd", YearToDate.Rows[0][33].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][33].ToString())));//Basic Salary
                            fields.SetField("taxableincomeytd", YearToDate.Rows[0][34].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][34].ToString())));//Basic Salary
                            fields.SetField("deductionswtaxytd", YearToDate.Rows[0][35].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][35].ToString())));//Basic Salary
                            fields.SetField("deductionsplaytd", YearToDate.Rows[0][36].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][36].ToString())));//Basic Salary
                            fields.SetField("deductionsprulifeytd", YearToDate.Rows[0][37].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][37].ToString())));//Basic Salary
                            fields.SetField("deductionssdytd", YearToDate.Rows[0][38].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][38].ToString())));//Basic Salary
                            fields.SetField("deductionssssloanamountytd", YearToDate.Rows[0][39].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][39].ToString())));//Basic Salary
                            fields.SetField("deductionspagibigloanamountytd", YearToDate.Rows[0][40].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][40].ToString())));//Basic Salary
                            fields.SetField("ecolaytd", YearToDate.Rows[0][41].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][41].ToString())));//Basic Salary
                            fields.SetField("deminimisytd", YearToDate.Rows[0][42].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][42].ToString())));//Basic Salary
                            fields.SetField("adjnontaxableallowanceytd", YearToDate.Rows[0][54].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][54].ToString())));//Basic Salary
                            fields.SetField("13thmonthnontaxableytd", YearToDate.Rows[0][43].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][43].ToString())));//Basic Salary
                            fields.SetField("netpayytd", YearToDate.Rows[0][44].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][44].ToString())));//Basic Salary
                            fields.SetField("sssemployershareytd", YearToDate.Rows[0][45].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][45].ToString())));//Basic Salary
                            fields.SetField("sssemployerecshareytd", YearToDate.Rows[0][46].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][46].ToString())));//Basic Salary
                            fields.SetField("philhealthemployershareytd", YearToDate.Rows[0][47].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][47].ToString())));//Basic Salary
                            fields.SetField("pagibigemployershareytd", YearToDate.Rows[0][48].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][48].ToString())));//Basic Salary
                            fields.SetField("restdayytd", YearToDate.Rows[0][62].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][62].ToString())));//Rest Day Amount Year to Date
                            fields.SetField("adjrdytd", "0.00");//ADJ Rest Day Amount Year to Date
                        }
                        else
                        {

                        }
                    }
                    else
                    {
                        if (YearToDate.Rows.Count > 0)
                        {
                            fields.SetField("basicsalaryytd", YearToDate.Rows[0][0].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][0].ToString())));//Basic Salary
                            fields.SetField("nontaxableallowanceytd", YearToDate.Rows[0][1].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][1].ToString())));//Non Taxable Allowance
                            fields.SetField("regularnightdiffytd", YearToDate.Rows[0][2].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][2].ToString())));//Regular Night Diff
                            fields.SetField("regularotytd", YearToDate.Rows[0][3].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][3].ToString())));//Regular OT
                            fields.SetField("regularholidayytd", YearToDate.Rows[0][4].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][4].ToString())));//Regular Holiday
                            fields.SetField("regularholidaynightdiffytd", YearToDate.Rows[0][5].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][5].ToString())));//Regular Holiday Night Diff
                            fields.SetField("regularholidayotytd", YearToDate.Rows[0][6].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][6].ToString())));//Regular Holiday OT
                            fields.SetField("paidtimeoffytd", YearToDate.Rows[0][7].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][7].ToString())));//Paid Time Off
                            fields.SetField("specialholidayytd", YearToDate.Rows[0][8].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][8].ToString())));//Special Holiday
                            fields.SetField("specialholidaynightdiffytd", YearToDate.Rows[0][9].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][9].ToString())));//Special Holiday Night Diff
                            fields.SetField("specialholidayotytd", YearToDate.Rows[0][10].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][10].ToString())));//Special Holiday OT
                            fields.SetField("allowancesytd", YearToDate.Rows[0][11].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][11].ToString())));//Allowances
                            fields.SetField("bonusytd", YearToDate.Rows[0][12].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][12].ToString())));//Bonus
                            fields.SetField("adjustmentsytd", YearToDate.Rows[0][13].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][13].ToString())));//Adjustments
                            fields.SetField("totalytd", YearToDate.Rows[0][14].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][14].ToString())));//Gross Pay/Total
                            fields.SetField("employeehdmfytd", YearToDate.Rows[0][15].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][15].ToString())));//Employee HDMF
                            fields.SetField("employeephilhealthytd", YearToDate.Rows[0][16].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][16].ToString())));//Employee PhilHealth
                            fields.SetField("employeesocialsecurityytd", YearToDate.Rows[0][17].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][17].ToString())));//Employee Social Security
                            fields.SetField("withholdingtaxytd", YearToDate.Rows[0][18].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][18].ToString())));//WithHolding Tax
                            fields.SetField("sssloandeductionytd", YearToDate.Rows[0][19].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][19].ToString())));//SSS Loan Deduction
                            fields.SetField("pagibigloandeductionytd", YearToDate.Rows[0][20].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][20].ToString())));//Pag-ibig Loan Deduction
                            fields.SetField("netpayytd", YearToDate.Rows[0][21].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][21].ToString())));//Net Pay
                            fields.SetField("employerhdmfytd", YearToDate.Rows[0][22].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][22].ToString())));//Employer HDMF
                            fields.SetField("employerphilhealthytd", YearToDate.Rows[0][23].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][23].ToString())));//Employer PhilHealth
                            fields.SetField("employersocialsecurityytd", YearToDate.Rows[0][24].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][24].ToString())));//Employer Social Security
                            fields.SetField("employersocialsecurityecytd", YearToDate.Rows[0][25].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][25].ToString())));//Employer Social Security EC
                            fields.SetField("deductionsabsencesytd", YearToDate.Rows[0][26].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][26].ToString())));//Deductions Absences
                            fields.SetField("deductionslatesytd", YearToDate.Rows[0][27].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][27].ToString())));//Deductions Lates
                            fields.SetField("doubleholidayytd", YearToDate.Rows[0][28].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][28].ToString())));//DH_REG_Amount
                            fields.SetField("doubleholidayndytd", YearToDate.Rows[0][29].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][29].ToString())));//DH_ND_Amount
                            fields.SetField("doubleholidayotytd", YearToDate.Rows[0][30].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][30].ToString())));//DH_OT_Amount
                        }
                        else
                        {

                        }
                    }

                    if (DTProfile.Rows.Count > 0)
                    {
                        fields.SetField("monthlyrate", DTProfile.Rows[0]["Salary"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(DTProfile.Rows[0]["Salary"].ToString())));
                    }
                    else
                    {

                    }


                    #region PAAT
                    if (uProfile.CN.Contains("paat"))
                    {
                        if (paysliprecordsempmaster.Rows.Count > 0)
                        {
                            //fields.SetFieldProperty("txtempid", "setflags", PdfAnnotation.FLAGS_HIDDEN,null);
                            fields.SetField("employeenumber", paysliprecordsempmaster.Rows[0][1].ToString());//Employee Numer
                            fields.SetField("EmployeeName", paysliprecordsempmaster.Rows[0][0].ToString());//Employee Name
                            fields.SetField("Position", "");//Position
                            fields.SetField("Class", "");//Class
                            fields.SetField("EmployeeNo", empid);//Employee No
                            fields.SetField("JoinedDate", paysliprecordsempmaster.Rows[0][2].ToString());//JoinedDate
                            fields.SetField("ProductionStartDate", paysliprecordsempmaster.Rows[0][2].ToString());//ProductionStartDate
                        }

                        if (paysliprecordsconfidentialinfo.Rows.Count > 0)
                        {
                            fields.SetField("PagibigNo", paysliprecordsconfidentialinfo.Rows[0][0].ToString());//Pag-IBIG No
                            fields.SetField("PhilHealthNo", paysliprecordsconfidentialinfo.Rows[0][1].ToString());//PhilHealth No
                            fields.SetField("SSSNo", paysliprecordsconfidentialinfo.Rows[0][2].ToString());//SSS No
                            fields.SetField("TaxNo", paysliprecordsconfidentialinfo.Rows[0][3].ToString());//Tax No
                            fields.SetField("Department", "");//Department
                            fields.SetField("DepartmentCode", "");//DepartmentCode
                            fields.SetField("taxcategory", paysliprecordsconfidentialinfo.Rows[0][4].ToString());//Tax Category
                        }

                        if (paysliprecordsemployeepay.Rows.Count > 0)
                        {
                            fields.SetField("BankAccount", paysliprecordsemployeepay.Rows[0][0].ToString());//Bank Account
                            fields.SetField("HourlyRate", paysliprecordsemployeepay.Rows[0][1].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordsemployeepay.Rows[0][1].ToString())));//Hourly Rate
                        }
                        else
                        {
                            fields.SetField("HourlyRate", "0.00");
                        }
                        fields.SetField("PayPeriod", payperiod);//Pay Period

                        DTProfile.Clear();
                        YearToDate.Clear();
                        paysliprecordspayslipdetails.Clear();
                        con = new Connection();
                        con.myparameters.Add(new myParameters { ParameterName = "@empId", mytype = SqlDbType.NVarChar, Value = empid });
                        DTProfile = con.GetDataTable("sp_getCompleteProfile_Mobilev2");

                        con.myparameters.Clear();
                        con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = empid });
                        con.myparameters.Add(new myParameters { ParameterName = "@PAYPERIOD", mytype = SqlDbType.NVarChar, Value = DDLPayPeriod });
                        paysliprecordspayslipdetails = con.GetDataTable("sp_GetPaySlipRecordsPaySlipDetailsPAAT");

                        con.myparameters.Clear();
                        con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = empid });
                        con.myparameters.Add(new myParameters { ParameterName = "@PAYPERIOD", mytype = SqlDbType.NVarChar, Value = paysliprecordspayslipdetails.Rows[0]["pay_day"].ToString() });
                        con.myparameters.Add(new myParameters { ParameterName = "@YEAR", mytype = SqlDbType.NVarChar, Value = DDLYear });
                        YearToDate = con.GetDataTable("sp_GetPaySlipRecordsPaySlipDetailsYearToDatePAAT");

                        if (DTProfile.Rows.Count > 0)
                        {
                            fields.SetField("MonthlyRate", DTProfile.Rows[0]["Salary"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(DTProfile.Rows[0]["Salary"].ToString())));
                        }

                        if (paysliprecordspayslipdetails.Rows.Count > 0)
                        {

                            ////HOURS////
                            fields.SetField("PayDay", paysliprecordspayslipdetails.Rows[0]["pay_day"].ToString());//Pay Day

                            fields.SetField("DeductionsAbsencesHour", "0.00");//DeductionsAbsencesHours
                            fields.SetField("DeductionsLatesHour", "0.00");//DeductionsLatesHours
                            fields.SetField("DeductionsTardinessHour", "0.0");//DeductionsTardinessHours
                            fields.SetField("DeductionsUndertimeHour", "0.0");//DeductionsUndertimeHours
                            fields.SetField("DeductionsLwopCurrent", "0.00");//DeductionsLwopHours
                            fields.SetField("NonTaxableAllowanceHours", "0.00");//DeductionsLwopHours
                            fields.SetField("ProductivityPayCurrent", paysliprecordspayslipdetails.Rows[0]["Ecola_Taxable"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Ecola_Taxable"].ToString())));//Car Allowance
                            fields.SetField("TaxableConvertedLeavesHours", "0.00");//TaxableConvertedLeavesHours

                            fields.SetField("NdAmountHour", paysliprecordspayslipdetails.Rows[0]["Regular ND Hours"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Regular ND Hours"].ToString())));//NdAmountHours
                            fields.SetField("NdOtAmountHour", paysliprecordspayslipdetails.Rows[0]["Regular ND OT Hours"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Regular ND OT Hours"].ToString())));//NdOtAmountHours

                            fields.SetField("RdAmountHour", paysliprecordspayslipdetails.Rows[0]["RestDay_Hours"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["RestDay_Hours"].ToString())));//RdAmountHours
                            fields.SetField("RdNdAmountHour", paysliprecordspayslipdetails.Rows[0]["Rest Day ND Hours"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Rest Day ND Hours"].ToString())));//RdNdAmountHours
                            fields.SetField("RdNdOtAmountHour", "0.00");//RdNdOtAmountHours

                            fields.SetField("RegularOvertimeAmountHour", paysliprecordspayslipdetails.Rows[0]["Regular OT Hours"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Regular OT Hours"].ToString())));//RegularOvertimeAmountHours

                            //fields.SetField("restdayothrs", paysliprecordspayslipdetails.Rows[0]["RH_ND_Amount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["RH_ND_Amount"].ToString())));//Basic Salary

                            fields.SetField("RegularHolidayAmountHour", paysliprecordspayslipdetails.Rows[0]["Regular Holiday Hours"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Regular Holiday Hours"].ToString())));//RegularHolidayAmountHours
                            fields.SetField("RegualarHolidayOvertimeAmountHour", paysliprecordspayslipdetails.Rows[0]["Regular Holiday OT Hours"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Regular Holiday OT Hours"].ToString())));//RegularHolidayOvertimeAmountHours
                            fields.SetField("Paid Time Off Hours", "0.00");//RegularHolidayRestDayAmountHours                                                                                                                                                                      
                            fields.SetField("RegularHolidayRestDayAmountHour", "0.00");//RegularHolidayRestDayAmountHours
                            fields.SetField("RegularHolidayRdOtAmountHour", paysliprecordspayslipdetails.Rows[0]["Regular Holiday ND Hours"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Regular Holiday ND Hours"].ToString()))); ;//RegularHolidayRdOtAmountHours

                            fields.SetField("SpecialHolidayAmountHour", paysliprecordspayslipdetails.Rows[0]["Special Holiday Hours"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Special Holiday Hours"].ToString())));//SpecialHolidayAmountHours
                            fields.SetField("SpecialHolidayOvertimeAmountHour", paysliprecordspayslipdetails.Rows[0]["Special Holiday OT Hours"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Special Holiday OT Hours"].ToString())));//SpecialHolidayOvertimeAmountHours
                            fields.SetField("SpecialHolidayNightDiffAmountHour", paysliprecordspayslipdetails.Rows[0]["Special Holiday ND Hours"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Special Holiday ND Hours"].ToString())));//SpecialHolidayNightDiffAmountHours
                            fields.SetField("SpecialHolidayRestDayAmountHour", paysliprecordspayslipdetails.Rows[0]["Special Holiday RD Hours"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Special Holiday RD Hours"].ToString())));//SpecialHolidayRestDayAmountHours
                            fields.SetField("SpecialHolidayRdOtAmountHour", "0.00");//SpecialHolidayRdOtAmountHours                                                                            

                            //fields.SetField("AdjustedRegularAmountHours", paysliprecordspayslipdetails.Rows[0]["Adj Regular Hours"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Adj Regular Hours"].ToString())));//AdjustedRegularAmountHours
                            //fields.SetField("AdjustedRdAmountHours", "");//AdjustedRdAmountHours
                            //fields.SetField("AdjustedRdOtAmountHours", paysliprecordspayslipdetails.Rows[0]["Adj Rest Day OT"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Adj Rest Day OT"].ToString())));//AdjustedRdOtAmountHours
                            //fields.SetField("AdjustedRegularOtAmountHours", paysliprecordspayslipdetails.Rows[0]["Adj Regular OT"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Adj Regular OT"].ToString())));//AdjustedRegularOtAmountHours
                            //fields.SetField("AdjustedNightDiffAmountHours", paysliprecordspayslipdetails.Rows[0]["Adj Regular OT"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Adj Regular OT"].ToString())));//AdjustedNightDiffAmountHours
                            //fields.SetField("AdjustedNightDiffOtAmountHours", paysliprecordspayslipdetails.Rows[0]["AdjND_Hrs"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["AdjND_Hrs"].ToString())));//AdjustedNightDiffOtAmountHours
                            //fields.SetField("AdjustedLateAmountHours", paysliprecordspayslipdetails.Rows[0]["Adj_Late_Hrs"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Adj_Late_Hrs"].ToString())));//AdjustedLateAmountHours
                            //fields.SetField("AdjustedLwopAmountHours", "0.00");//AdjustedLwopAmountHours


                            ///CURRENT///
                            fields.SetField("MonthlySalaryRateCurrent", DTProfile.Rows[0]["Salary"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(DTProfile.Rows[0]["Salary"].ToString())));//MonthlySalaryRateCurrent
                            fields.SetField("BasicSalaryAmountCurrent", paysliprecordspayslipdetails.Rows[0]["Basic Salary"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Basic Salary"].ToString())));//BasicSalaryAmountCurrent
                            fields.SetField("DeductionAbsencesCurrent", paysliprecordspayslipdetails.Rows[0]["Deductions Absences"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Deductions Absences"].ToString())));//DeductionsAbsencesCurrent
                            fields.SetField("DeductionLatesCurrent", "0.00");//DeductionsLateCurrent
                            fields.SetField("DeductionTardiness", paysliprecordspayslipdetails.Rows[0]["Deductions Tardiness"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Deductions Tardiness"].ToString())));//Basic Salary
                            fields.SetField("DeductionUndertimeCurrent", paysliprecordspayslipdetails.Rows[0]["undertime"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["undertime"].ToString())));//Basic Salary
                            fields.SetField("DeductionLWOPCurrent", "0.00");//DeductionsLWOPCurrent
                            fields.SetField("TaxableConvertedLeavesCurrent", "0.00");//TaxableConvertedLeavesCurrent
                            fields.SetField("MealAllowanceCurrent", "0.00");//MealAllowanceCurrent
                            fields.SetField("13thMonthTaxableCurrent", paysliprecordspayslipdetails.Rows[0]["13th Month Taxable"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["13th Month Taxable"].ToString())));//13thMonthTaxableCurrent
                                                                                                                                                                                                                                                                               //fields.SetField("14thMonthTaxableCurrent", paysliprecordspayslipdetails.Rows[0]["Taxable_14thMonth"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Taxable_14thMonth"].ToString())));//14thMonthTaxableCurrent
                                                                                                                                                                                                                                                                               //fields.SetField("STITaxableCurrent", paysliprecordspayslipdetails.Rows[0]["STI Taxable"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["STI Taxable"].ToString())));//STITaxableCurrent
                            fields.SetField("NdAmountCurrent", paysliprecordspayslipdetails.Rows[0]["Total_ND_Amount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Total_ND_Amount"].ToString())));//NdAmountCurrent
                            fields.SetField("NdOtAmountCurrent", paysliprecordspayslipdetails.Rows[0]["Regular ND OT Amount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Regular ND OT Amount"].ToString())));//NdOtAmountCurrent
                            fields.SetField("RdAmountCurrent", paysliprecordspayslipdetails.Rows[0]["RestDay_Amount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["RestDay_Amount"].ToString())));//RdAmountCurrent
                            fields.SetField("RdNdAmountCurrent", paysliprecordspayslipdetails.Rows[0]["Rest Day ND Amount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Rest Day ND Amount"].ToString())));//RdNdAmountCurrent
                            fields.SetField("RdNdOtAmountCurrent", "0.00");//RdNdAmountCurrent

                            fields.SetField("RegularOvertimeAmountCurrent", paysliprecordspayslipdetails.Rows[0]["Regular OT Amount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Regular OT Amount"].ToString())));//RegularOvertimeAmountCurrent
                            fields.SetField("RegularHolidayAmountCurrent", paysliprecordspayslipdetails.Rows[0]["Regular Holiday Amount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Regular Holiday Amount"].ToString())));//RegularHolidayAmountCurrent
                            fields.SetField("RegularHolidayOvertimeAmountCurrent", paysliprecordspayslipdetails.Rows[0]["Regular Holiday OT Amount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Regular Holiday OT Amount"].ToString())));//RegularHolidayOvertimeAmountCurrent
                            fields.SetField("regularholidayndotcurrent", "0.00");//Basic Salary
                            fields.SetField("RegularHolidayRestDayAmountCurrent", "0.00");//RegularHolidayRestDayAmountCurrent
                            fields.SetField("RegularHolidayRdOtAmountCurrent", paysliprecordspayslipdetails.Rows[0]["Regular Holiday ND Amount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Regular Holiday ND Amount"].ToString())));//RegularHolidayRdOtAmountCurrent
                            fields.SetField("SpecialHolidayAmountCurrent", paysliprecordspayslipdetails.Rows[0]["Special Holiday Amount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Special Holiday Amount"].ToString())));//SpecialHolidayAmountCurrent
                            fields.SetField("SpecialHolidayOvertimeAmountCurrent", paysliprecordspayslipdetails.Rows[0]["Special Holiday OT Amount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Special Holiday OT Amount"].ToString())));//SpecialHolidayOvertimeAmountCurrent
                            fields.SetField("SpecialHolidayNightDiffAmountCurrent", paysliprecordspayslipdetails.Rows[0]["Special Holiday ND Amount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Special Holiday ND Amount"].ToString())));//SpecialHolidayNightDiffAmountCurrent
                            fields.SetField("SpecialHolidayRestDayAmountCurrent", paysliprecordspayslipdetails.Rows[0]["Special Holiday RD Amount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Special Holiday RD Amount"].ToString())));//SpecialHolidayRestDayAmountCurrent
                            fields.SetField("SpecialHolidayRdOtAmountCurrent", "0.00");//SpecialHolidayRdOtAmountCurrent

                            fields.SetField("TaxableAllowanceCurrent", "0.00");//TaxableAllowanceCurrent
                            fields.SetField("TaxableBonusForCaptainsCurrent", paysliprecordspayslipdetails.Rows[0]["Deminimis Taxable"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Deminimis Taxable"].ToString())));//TaxableBonusForCaptainsCurrent
                            fields.SetField("HardshipAllowanceCurrent", paysliprecordspayslipdetails.Rows[0]["OtherBenefits_Taxable"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["OtherBenefits_Taxable"].ToString())));//TaxableBonusForCaptainsCurrent
                            fields.SetField("HousingAllowanceCurrent", paysliprecordspayslipdetails.Rows[0]["OtherBenefits_NonTaxable"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["OtherBenefits_NonTaxable"].ToString())));//TaxableBonusForCaptainsCurrent

                            fields.SetField("AdjustedRegularAmountCurrent", paysliprecordspayslipdetails.Rows[0]["Adj Regular Holiday Amount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Adj Regular Holiday Amount"].ToString())));//AdjustedRegularAmountCurrent
                            fields.SetField("AdjustedRdAmountCurrent", paysliprecordspayslipdetails.Rows[0]["Adj Regular Holiday OT Amount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Adj Regular Holiday OT Amount"].ToString())));//AdjustedRdAmountCurrent
                            fields.SetField("AdjustedRdOtAmountCurrent", paysliprecordspayslipdetails.Rows[0]["Adj Regular Holiday ND Amount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Adj Regular Holiday ND Amount"].ToString())));//AdjustedRdOtAmountCurrent
                            fields.SetField("AdjustedRegularOtAmountCurrent", paysliprecordspayslipdetails.Rows[0]["Adj Special Holiday Amount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Adj Special Holiday Amount"].ToString())));//AdjustedRegularOtAmountCurrent
                            fields.SetField("AdjustedNightDiffAmountCurrent", paysliprecordspayslipdetails.Rows[0]["Adj Special Holiday OT Amount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Adj Special Holiday OT Amount"].ToString())));//AdjustedNightDiffAmountCurrent
                            fields.SetField("AdjustedNightDiffOtAmountCurrent", paysliprecordspayslipdetails.Rows[0]["Adj Special Holiday ND Amount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Adj Special Holiday ND Amount"].ToString())));//AdjustedNightDiffOtAmountCurrent
                                                                                                                                                                                                                                                                                                              //fields.SetField("AdjustedLateAmountCurrent", paysliprecordspayslipdetails.Rows[0]["Adj_Late_Amount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Adj_Late_Amount"].ToString())));//AdjustedLateAmountCurrent
                                                                                                                                                                                                                                                                                                              //fields.SetField("AdjustedLwopAmountCurrent", "0.00");//AdjustedLwopAmountCurrent
                            fields.SetField("GrossPayCurrent", paysliprecordspayslipdetails.Rows[0]["Gross Pay"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Gross Pay"].ToString())));//GrossPayCurrent

                            fields.SetField("DeductionSSSCurrent", paysliprecordspayslipdetails.Rows[0]["Employee SSS"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Employee SSS"].ToString())));//DeductionSSSCurrent
                            fields.SetField("DeductionPhilHealthCurrent", paysliprecordspayslipdetails.Rows[0]["Employee PhilHealth"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Employee PhilHealth"].ToString())));//DeductionPhilHealthCurrent
                            fields.SetField("DeductionPagibigCurrent", paysliprecordspayslipdetails.Rows[0]["Employee Pagibig"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Employee Pagibig"].ToString())));//DeductionPagibigCurrent
                            fields.SetField("TaxableIncomeCurrent", paysliprecordspayslipdetails.Rows[0]["Taxable Income"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Taxable Income"].ToString())));//TaxableIncomeCurrent

                            fields.SetField("DeductionWTaxCurrent", paysliprecordspayslipdetails.Rows[0]["Tax"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Tax"].ToString())));//DeductionWTaxCurrent

                            fields.SetField("BonusAmount", paysliprecordspayslipdetails.Rows[0]["BonusAmount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["BonusAmount"].ToString())));//UniformAllowanceCurrent
                            fields.SetField("Deminimis_NonTaxable", paysliprecordspayslipdetails.Rows[0]["Deminimis_NonTaxable"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Deminimis_NonTaxable"].ToString())));//MedicalAllowanceCurrent
                            fields.SetField("13ThMonthNonTaxableAllowanceCurrent", paysliprecordspayslipdetails.Rows[0]["13th Month Non-Taxable"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["13th Month Non-Taxable"].ToString())));//13thMonthNonTaxableCurrent
                            fields.SetField("non_taxable_allowance", paysliprecordspayslipdetails.Rows[0]["Rice_Allowance"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Rice_Allowance"].ToString())));//Adjusted14thMonthNonTaxableCurrent
                            fields.SetField("TaxRefundCurrent", paysliprecordspayslipdetails.Rows[0]["taxrefund"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["taxrefund"].ToString())));//TaxRefundCurrent
                            fields.SetField("CoopCarLoanCurrent", paysliprecordspayslipdetails.Rows[0]["Coop Car Loan"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Coop Car Loan"].ToString())));//Adjusted14thMonthNonTaxableCurrent
                            fields.SetField("CoopLoan1Current", paysliprecordspayslipdetails.Rows[0]["Coop Loan 1"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Coop Loan 1"].ToString())));//Adjusted14thMonthNonTaxableCurrent
                            fields.SetField("CoopContributionCurrent", paysliprecordspayslipdetails.Rows[0]["Coop Contribution"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Coop Contribution"].ToString())));//Adjusted14thMonthNonTaxableCurrent
                            fields.SetField("HMOCurrent", paysliprecordspayslipdetails.Rows[0]["HMO"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["HMO"].ToString())));//Adjusted14thMonthNonTaxableCurrent
                            fields.SetField("DeductionTaxPayableCurrent", paysliprecordspayslipdetails.Rows[0]["tax_payable"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["tax_payable"].ToString())));//DeductionTaxPayableCurrent
                            fields.SetField("DeductionExcessOfBenefitCurrent", paysliprecordspayslipdetails.Rows[0]["Coop Loan 1"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Coop Loan 1"].ToString())));//DeductionExcessOfBenefitsCurrent
                            fields.SetField("DeductionModifiedPagibigIICurrent", paysliprecordspayslipdetails.Rows[0]["PagIBIG_MP2"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["PagIBIG_MP2"].ToString())));//DeductionModifiedPagibigIICurrent
                            fields.SetField("DeductionSSSLoanAmountCurrent", paysliprecordspayslipdetails.Rows[0]["SSS Loan Amount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["SSS Loan Amount"].ToString())));//DeductionSSSLoanAmountCurrent
                            fields.SetField("DeductionPagibiLoanAmountCurrent", paysliprecordspayslipdetails.Rows[0]["Pagibig Loan Amount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Pagibig Loan Amount"].ToString())));//Basic Salary
                            fields.SetField("NetPayCurrent", paysliprecordspayslipdetails.Rows[0]["Net Pay"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Net Pay"].ToString())));//NetPayCurrent

                            fields.SetField("SSSEmployerShareCurrent", paysliprecordspayslipdetails.Rows[0]["SSS Employer Share"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["SSS Employer Share"].ToString())));//SSSEmployerShareCurrent
                            fields.SetField("SSSEmployerECShareCurrent", paysliprecordspayslipdetails.Rows[0]["SSS Employer EC Share"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["SSS Employer EC Share"].ToString())));//SSSEmployerECShareCurrent
                            fields.SetField("PhilHealthEmployerShareCurrent", paysliprecordspayslipdetails.Rows[0]["PhilHealth Employer Share"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["PhilHealth Employer Share"].ToString())));//PhilHealthEmployerShareCurrent
                                                                                                                                                                                                                                                                                                    ////CURRENT////

                        }


                        if (YearToDate.Rows.Count > 0)
                        {

                            ////YEAR TO DATE////
                            fields.SetField("MonthlySalaryRateYearToDate", DTProfile.Rows[0]["Salary"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(DTProfile.Rows[0]["Salary"].ToString())));//MonthlySalaryRateYeartoDate
                            fields.SetField("BasicSalaryAmountYearToDate", YearToDate.Rows[0]["basicsalary"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["basicsalary"].ToString())));//BasicSalaryAmountYeartoDate
                            fields.SetField("DeductionsAbsencesYearToDate", YearToDate.Rows[0]["deductionsabsences"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["deductionsabsences"].ToString())));//DeductionsAbsencesYeartoDate
                            fields.SetField("DeductionsLatesYearToDate", YearToDate.Rows[0]["deductionstardiness"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["deductionstardiness"].ToString())));//DeductionsLatesYeartoDate
                            fields.SetField("DeductionsTardinessYearToDate", YearToDate.Rows[0]["deductionstardiness"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["deductionstardiness"].ToString())));//DeductionsTardinessYeartoDate
                            fields.SetField("DeductionsUndertimeYearToDate", YearToDate.Rows[0]["undertime"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["undertime"].ToString())));//DeductionsUndertimeYeartoDate
                            fields.SetField("DeductionsLwopYearToDate", YearToDate.Rows[0]["lwopdeduction"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["lwopdeduction"].ToString())));//DeductionsLwopYeartoDate
                            fields.SetField("CarAllowanceYearToDate", YearToDate.Rows[0]["carallowance"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["carallowance"].ToString())));//Car Allowance
                            fields.SetField("TaxableConvertedLeavesYearToDate", YearToDate.Rows[0]["taxableconvertedleaves"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["taxableconvertedleaves"].ToString())));//TaxableConvertedLeavesYeartoDate
                            fields.SetField("ProductivityPayYearToDate", YearToDate.Rows[0]["ecolatax"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["ecolatax"].ToString())));//ProductivityPayYearToDate
                            fields.SetField("MealAllowanceYearToDate", YearToDate.Rows[0]["mealallowance"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["mealallowance"].ToString())));//MealAllowanceYeartoDate
                            fields.SetField("13thMonthTaxableYearToDate", YearToDate.Rows[0]["13thmonthtaxable"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["13thmonthtaxable"].ToString())));//13thMonthTaxableYeartoDate
                            fields.SetField("14thMonthTaxableYeartoDate", YearToDate.Rows[0]["tax14month"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["tax14month"].ToString())));//14thMonthTaxableYeartoDate
                            fields.SetField("STITaxableYeartoDate", YearToDate.Rows[0]["stitaxable"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["stitaxable"].ToString())));//STITaxableYeartoDate
                            fields.SetField("NdAmountYearToDate", YearToDate.Rows[0]["regularndamount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["regularndamount"].ToString())));//NdAmountYeartoDate
                            fields.SetField("NdOtAmountYearToDate", YearToDate.Rows[0]["regularndotamount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["regularndotamount"].ToString())));//NdOtAmountYearToDate
                            fields.SetField("RdAmountYearToDate", YearToDate.Rows[0]["rdamt"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["rdamt"].ToString())));//RdAmountYearToDate
                            fields.SetField("RdNdAmountYearToDate", "0.00");//RdNdAmountYearToDate
                            fields.SetField("RdNdOtAmountYearToDate", YearToDate.Rows[0]["restdayndotamount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["restdayndotamount"].ToString())));//RdNdOtAmountYearToDate
                            fields.SetField("RegularOvertimeAmountYearToDate", YearToDate.Rows[0]["regularotamount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["regularotamount"].ToString())));//RegularOvertimeAmountYearToDate

                            fields.SetField("RegularHolidayAmountYearToDate", YearToDate.Rows[0]["regularholidayamount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["regularholidayamount"].ToString())));//RegularHolidayAmountYearToDate
                            fields.SetField("RegularHolidayOvertimeAmountYearToDate", YearToDate.Rows[0]["regularholidayotamount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["regularholidayotamount"].ToString())));//RegularHolidayOvertimeAmountYearToDate
                            fields.SetField("RegularHolidayRestDayAmountYearToDate", "0.00");//RegularHolidayRestDayAmountYearToDate
                            fields.SetField("RegularHolidayRdOtAmountYearToDate", "0.00");//RegularHolidayRdOtAmountYearToDate
                            fields.SetField("SpecialHolidayAmountYearToDate", YearToDate.Rows[0]["specialholidayamount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["specialholidayamount"].ToString())));//SpecialHolidayAmountYearToDate
                            fields.SetField("SpecialHolidayOverTimeAmountYearToDate", YearToDate.Rows[0]["specialholidayotamount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["specialholidayotamount"].ToString())));//SpecialHolidayOvertimeAmountYearToDate
                            fields.SetField("SpecialHolidayNightDiffAmountYearToDate", YearToDate.Rows[0]["specialholidayndamount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["specialholidayndamount"].ToString())));//SpecialHolidayNightDiffAmountYearToDate
                            fields.SetField("SpecialHolidayRestDayAmountYearToDate", "0.00");//SpecialHolidayRestDayAmountYearToDate
                            fields.SetField("SpecialHolidayRdOtAmountYearToDate", "0.00");//SpecialHolidayRdOtAmountYearToDate

                            fields.SetField("TaxableAllowanceYearToDate", "0.00");//TaxableAllowanceYearToDate
                            fields.SetField("TaxableBonusForCaptainsYearToDate", YearToDate.Rows[0]["stitaxable"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["stitaxable"].ToString())));//RdNdAmountYearToDatestitaxable
                            fields.SetField("HardshipAllowanceYearToDate", YearToDate.Rows[0]["otherbenefits_taxable"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["otherbenefits_taxable"].ToString())));//RdNdAmountYearToDate
                            fields.SetField("HousingAllowanceYearToDate", YearToDate.Rows[0]["ecola"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["ecola"].ToString())));//RdNdAmountYearToDate
                            fields.SetField("AdjustedRegularAmountYearToDate", YearToDate.Rows[0]["adjRegular_Holiday"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["adjRegular_Holiday"].ToString())));//AdjustedRegularAmountYearToDate
                            fields.SetField("AdjustedRdAmountYearToDate", YearToDate.Rows[0]["adjRegular_Holiday_Overtime"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["adjRegular_Holiday_Overtime"].ToString())));//AdjustedRdAmountYearToDate
                            fields.SetField("AdjustedRdOtAmountYearToDate", YearToDate.Rows[0]["adjRegular_Holiday_Night_Diff"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["adjRegular_Holiday_Night_Diff"].ToString())));//AdjustedRdOtAmountYearToDate
                            fields.SetField("AdjustedRegularOtAmountYearToDate", YearToDate.Rows[0]["adjSpecial_Holiday"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["adjSpecial_Holiday"].ToString())));//AdjustedRegularOtAmountYearToDate
                            fields.SetField("AdjustedNightDiffAmountYearToDate", YearToDate.Rows[0]["adjSpecial_Holiday_Overtime"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["adjSpecial_Holiday_Overtime"].ToString())));//AdjustedNightDiffAmountYearToDate
                            fields.SetField("AdjustedNightDiffOtAmountYearToDate", YearToDate.Rows[0]["adjSpecial_Holiday_Night_Diff"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["adjSpecial_Holiday_Night_Diff"].ToString())));//AdjustedNightDiffOtAmountYearToDate
                            fields.SetField("GrossPayYearToDate", YearToDate.Rows[0]["grosspay"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["grosspay"].ToString())));//GrossPayYearToDate

                            fields.SetField("DeductionSSSYearToDate", YearToDate.Rows[0]["employeesss"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["employeesss"].ToString())));//DeductionSSSYearToDate
                            fields.SetField("DeductionPhilHealthYearToDate", YearToDate.Rows[0]["employeephilhealth"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["employeephilhealth"].ToString())));//DeductionPhilHealthYearToDate
                            fields.SetField("DeductionPagibigYearToDate", YearToDate.Rows[0]["employeepagibig"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["employeepagibig"].ToString())));//DeductionPagibigYearToDate
                            fields.SetField("TaxableIncomeYearToDate", YearToDate.Rows[0]["taxableincome"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["taxableincome"].ToString())));//TaxableIncomeYearToDate

                            fields.SetField("DeductionWTaxYearToDate", YearToDate.Rows[0]["tax"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["tax"].ToString())));//DeductionWTaxYearToDate

                            fields.SetField("NonTaxableConvertedLeavesAmountYearToDate", YearToDate.Rows[0]["nontaxableconvertedleaves"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["nontaxableconvertedleaves"].ToString())));//NonTaxableConvertedLeavesAmountYearToDate
                            fields.SetField("STIFringeYearToDate", YearToDate.Rows[0]["deminimis"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["deminimis"].ToString())));//STIFringeYearToDate
                            fields.SetField("STINonTaxableYearToDate", YearToDate.Rows[0]["ecolanontax"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["ecolanontax"].ToString())));//STINonTaxableYearToDate
                            fields.SetField("UniformAllowanceYearToDate", YearToDate.Rows[0]["BonusAmount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["BonusAmount"].ToString())));//UniformAllowanceYearToDate
                            fields.SetField("MedicalAllowanceYearToDate", YearToDate.Rows[0]["deminimis"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["deminimis"].ToString())));//MedicalAllowanceYearToDate
                            fields.SetField("SocialRelatedAllowanceYearToDate", YearToDate.Rows[0]["socialrelatedallowance"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["socialrelatedallowance"].ToString())));//Social Related Allowance
                            fields.SetField("MobileAllowanceYearToDate", YearToDate.Rows[0]["mobileallowance"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["mobileallowance"].ToString())));//Mobile Allowance
                            fields.SetField("DataPlanYearToDate", YearToDate.Rows[0]["dataplan"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["dataplan"].ToString())));//Data Plan
                            fields.SetField("13thMonthNonTaxableYearToDate", YearToDate.Rows[0]["13thmonthnontaxable"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["13thmonthnontaxable"].ToString())));//13thMonthNonTaxableYearToDate
                            fields.SetField("14thMonthNonTaxableYearToDate", YearToDate.Rows[0]["pay14month"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["pay14month"].ToString())));//14thMonthNonTaxableYearToDate
                            fields.SetField("Adjusted14thMonthNonTaxableYearToDate", YearToDate.Rows[0]["Rice_Allowance"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["Rice_Allowance"].ToString())));//Adjusted14thMonthNonTaxableYearToDate
                            fields.SetField("TaxRefundYearToDate", YearToDate.Rows[0]["taxrefund"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["taxrefund"].ToString())));//TaxRefundYearToDate

                            fields.SetField("CoopCarLoanYearToDate", YearToDate.Rows[0]["parking"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["parking"].ToString())));//TaxableAllowanceYearToDate
                            fields.SetField("CoopLoan1YearToDate", YearToDate.Rows[0]["eob"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["eob"].ToString())));//RdNdAmountYearToDate
                            fields.SetField("CoopContributionYearToDate", YearToDate.Rows[0]["loans"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["loans"].ToString())));//RdNdAmountYearToDate
                            fields.SetField("HMOYearToDate", YearToDate.Rows[0]["hmo"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["hmo"].ToString())));//RdNdAmountYearToDate

                            fields.SetField("DeductionTaxPayableYearToDate", YearToDate.Rows[0]["taxpayable"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["taxpayable"].ToString())));//DeductionTaxPayableYearToDate
                            fields.SetField("DeductionExcessBenefitYearToDate", YearToDate.Rows[0]["eob"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["eob"].ToString())));//DeductionExcessOfBenefitYearToDate
                            fields.SetField("DeductionModifiedPagibigIIYearToDate", YearToDate.Rows[0]["pagibigmp2"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["pagibigmp2"].ToString())));//DeductionModifiedPagibigIIYearToDate
                            fields.SetField("DeductionGlapiMplYearToDate", YearToDate.Rows[0]["pla"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["pla"].ToString())));//DeductionGlapiMplYearToDate
                            fields.SetField("DeductionSSSLoanAmountYearToDate", YearToDate.Rows[0]["sssloanamount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["sssloanamount"].ToString())));//DeductionSSSLoanAmountYearToDate
                            fields.SetField("DeductionPagibigLoanAmountYearToDate", YearToDate.Rows[0]["pagibigloanamount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["pagibigloanamount"].ToString())));//DeductionPagibigLoanAmountYearToDate
                            fields.SetField("NetPayYearToDate", YearToDate.Rows[0]["netpay"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["netpay"].ToString())));//NetPayYearToDate

                            fields.SetField("SSSEmployerShareYearToDate", YearToDate.Rows[0]["sssemployershare"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["sssemployershare"].ToString())));//SSSEmployerShareYearToDate
                            fields.SetField("SSSEmployerECShareYearToDate", YearToDate.Rows[0]["sssemployerecshare"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["sssemployerecshare"].ToString())));//SSSEmployerECShareYearToDate
                            fields.SetField("PhilHealthEmployerShareYearToDate", YearToDate.Rows[0]["philhealthemployershare"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["philhealthemployershare"].ToString())));//PhilHealthEmployerShareYearToDate                                                                                                              ////YEAR TO DATE////
                        }
                    }
                    #endregion

                    //foreach (var field in af.Fields)
                    //{
                    //    Console.WriteLine("{0}, {1}", field.Key, field.Value);
                    //    //string sample = TextBox2.Text;
                    //    string sample2 = field.Key.ToString();
                    //    //TextBox2.Text = sample + field.Key.ToString() + ',';
                    //    fields.SetField(field.Key.ToString(), field.ToString());
                    //}


                    //------------------------------------------------

                    // form flattening rids the form of editable text fields so
                    // the final output can't be edited
                    stamper.FormFlattening = true;
                    // closing the stamper

                    stamper.Close();



                    //viewpdf.Attributes.Add("src", "pdf\\PaySlipDefault-" + empid + "-" + hoursdatefrom + "-" + hoursdateto + ".pdf");
                    //Response.Redirect("~/pdf/PaySlip-I101.pdf");
                    #endregion
                }
                #endregion
                return "Success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }

        [HttpPost]
        [Route("sendPayslip")]
        //{
        //"NTID": "i101",
        //"Start_Date": "2017-04-01",
        //"End_Date": "2017-04-30"
        //}
        public string sendPayslip(sendPay param)
        {
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = param.CN;
            SelfieRegistration2Controller.GetDB2(GDB);
            Connection Connection = new Connection();
            Connection.myparameters.Add(new myParameters { ParameterName = "@empid", mytype = SqlDbType.NVarChar, Value = param.empid });
            DataTable DT = Connection.GetDataTable("sp_getEmailsbyEmpID");
            foreach (DataRow row in DT.Rows)
            {
                //UserProfilev2 uProfile = new UserProfilev2();
                //uProfile.CN = param.CN;
                //uProfile.PayDay = param.PayDay;
                //uProfile.PayYear = param.PayYear;
                ////uProfile.PayDay = "2018-08-16-2018-08-31";
                ////uProfile.PayYear = "2018";
                //uProfile.NTID = row["empid"].ToString();
                //genPayslipv3(uProfile);
                string emailaccount = "payroll.noreply@illimitado.com";
                string emailpassword = "LimitLess!1@";
                string loopemail = row["email"].ToString();
                string prefix = HttpContext.Current.Server.MapPath("~/pdf/PaySlip-Encrypted");
                using (MailMessage mm = new MailMessage(emailaccount, loopemail))
                {
                    MailAddress aliasmail = new MailAddress("payroll.noreply@illimitado.com", "payroll.noreply@illimitado.com");
                    MailAddress aliasreplymail = new MailAddress("payroll.noreply@illimitado.com");
                    mm.From = aliasmail;
                    mm.Subject = "Payslip";
                    mm.Body = "";
                    mm.Attachments.Add(new Attachment(prefix + param.empid + ".pdf"));
                    mm.IsBodyHtml = false;
                    SmtpClient smtp = new SmtpClient();
                    smtp.Host = "box1256.bluehost.com";
                    smtp.EnableSsl = true;
                    NetworkCredential NetworkCred = new NetworkCredential(emailaccount, emailpassword);
                    smtp.UseDefaultCredentials = true;
                    smtp.Credentials = NetworkCred;
                    smtp.Port = 587;
                    smtp.Send(mm);
                }
            }
            return "success";

        }
        [HttpPost]
        [Route("sendpaysliplist")]
        public string paysliplist()
        {
            try
            {
                List<string> lstPayDay = new List<string>();
                //lstPayDay.Add("2020-12-18-2021-01-02");
                //lstPayDay.Add("2021-01-03-2021-01-17");
                //lstPayDay.Add("2021-01-18-2021-02-02");
                //lstPayDay.Add("2021-02-03-2021-02-17");
                //lstPayDay.Add("2021-02-18-2021-03-02");
                //lstPayDay.Add("2021-03-03-2021-03-17");
                //lstPayDay.Add("2021-03-18-2021-04-02");
                //lstPayDay.Add("2021-04-03-2021-04-17");
                //lstPayDay.Add("2021-04-18-2021-05-02");
                //lstPayDay.Add("2021-01-01-2021-05-16");
                //lstPayDay.Add("2021-05-03-2021-05-17");
                //lstPayDay.Add("2021-05-18-2021-06-02");
                //lstPayDay.Add("2021-06-03-2021-06-17");
                //lstPayDay.Add("2021-06-18-2021-07-02");
                //lstPayDay.Add("2021-07-03-2021-07-17");
                //lstPayDay.Add("2021-07-18-2021-08-02");
                //lstPayDay.Add("2021-08-03-2021-08-17");
                //lstPayDay.Add("2021-08-18-2021-09-02");
                //lstPayDay.Add("2021-09-03-2021-09-17");
                //lstPayDay.Add("2021-09-18-2021-10-02");
                //lstPayDay.Add("2021-10-03-2021-10-17");
                //lstPayDay.Add("2021-10-18-2021-11-02");
                //lstPayDay.Add("2021-08-03-2021-08-17");
                //lstPayDay.Add("2021-10-03-2021-10-17");
                foreach (string itm in lstPayDay)
                {
                    sendPayv2 param = new sendPayv2();
                    param.CN = "newcore";
                    param.PayDay = itm;
                    param.PayYear = itm.Substring(11, 4);
                    param.SendToEmail = "0";
                    sendPayslipToAll(param);
                }
                return "Success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }
        [HttpPost]
        [Route("sendPayslipToAll")]
        public string sendPayslipToAll(sendPayv2 param)
        {
            try
            {
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;

                GetDatabase GDB = new GetDatabase();
                GDB.ClientName = param.CN;
                SelfieRegistration2Controller.GetDB2(GDB);
                Connection Connection = new Connection();
                Connection.myparameters.Clear();
                Connection.myparameters.Add(new myParameters { ParameterName = "@payperiod", mytype = SqlDbType.VarChar, Value = param.PayDay });
                DataTable DT = Connection.GetDataTable("sp_getEmails");//newcore
                //DataTable DT = Connection.GetDataTable("sp_getEmails_Darvin");//darvin
                //DataTable DT = Connection.GetDataTable("sp_getEmails_NITS");//nits
                foreach (DataRow row in DT.Rows)
                {
                    UserProfilev2 uProfile = new UserProfilev2();
                    uProfile.CN = param.CN;
                    uProfile.PayDay = param.PayDay;
                    uProfile.PayYear = param.PayYear;
                    uProfile.NTID = row["EmpID"].ToString();
                    ////uProfile.PayDay = "2018-08-16-2018-08-31";
                    ////uProfile.PayYear = "2018";a
                    //uProfile.NTID = row["empid"].ToString();
                    string filename = genPayslipv3(uProfile);

                    if (param.SendToEmail == "1")
                    {
                        //comment this section for no email
                        string emailaccount = "durusthr@illimitado.com";
                        string emailpassword = "@1230Qwerty";
                        string loopemail = row["email"].ToString();
                        string prefix = HttpContext.Current.Server.MapPath("~/pdf/");
                        using (MailMessage mm = new MailMessage(emailaccount, loopemail))//  "rvitug@illimitado.com"))
                        {
                            MailAddress aliasmail = new MailAddress("durusthr@illimitado.com", "durusthr@illimitado.com");
                            MailAddress aliasreplymail = new MailAddress("durusthr@illimitado.com");
                            MailAddress aliasmailBCC = new MailAddress("durusthr@illimitado.com");
                            mm.From = aliasmail;
                            mm.Bcc.Add(aliasmailBCC);
                            //mm.Subject = "Payslip for Feb 11-25";
                            //mm.Body = @"Please find enclosed your payslip for the period Feb 11 to 25th. Password is your Lastname. Note: Password is case sensitive.";
                            //mm.Body = "Password is your Lastname.";
                            //mm.Body = "Note: Password is case sensitive.";
                            Connection con = new Connection();
                            con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = row["empid"].ToString() });
                            DataTable dt2 = con.GetDataTable("sp_GetPaySlipRecordsEmpMaster");
                            //string name = dt2.Rows[0][6].ToString() + "_" + dt2.Rows[0][5].ToString() + "_" + param.PayDay;
                            mm.Attachments.Add(new Attachment(HttpContext.Current.Server.MapPath("pdf") + "\\" + filename));

                            mm.IsBodyHtml = false;
                            SmtpClient smtp = new SmtpClient();
                            smtp.Host = "smtp.office365.com";
                            smtp.EnableSsl = true;
                            NetworkCredential NetworkCred = new NetworkCredential(emailaccount, emailpassword);
                            smtp.UseDefaultCredentials = true;
                            smtp.Credentials = NetworkCred;
                            smtp.Port = 587;
                            smtp.Send(mm);
                        }
                        //comment this section for no email
                    }
                    else
                    {

                    }
                }
                return "success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }

        public string genPayslipv3(UserProfilev2 uProfile)
        {
            try
            {
                GetDatabase GDB = new GetDatabase();
                GDB.ClientName = uProfile.CN;
                SelfieRegistration2Controller.GetDB2(GDB);


                string DDLMonths = "08";
                string DDLPayPeriod = uProfile.PayDay;
                string DDLYear = uProfile.PayYear;
                empid = uProfile.NTID;

                //empname = PayMtd.searchempname(empid);

                empidprof = empid;
                //GetEmpLevelandCategory = PayMtd.GetEmpLevelandCategory(empid);
                //string EmpLevel = GetEmpLevelandCategory.Rows[0][0].ToString();
                //string EmpCategory = GetEmpLevelandCategory.Rows[0][1].ToString();

                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = empid });
                paysliprecordsempmaster = con.GetDataTable("sp_GetPaySlipRecordsEmpMaster");

                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = empid });
                paysliprecordsempmaster = con.GetDataTable("sp_GetPaySlipRecordsEmpMaster");

                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = empid });
                paysliprecordsconfidentialinfo = con.GetDataTable("sp_GetPaySlipRecordsConfidentialInfo");
                //paysliprecordsconfidentialinfo = PayMtd.PaySlipRecordsConfidentialInfo(empid);

                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = empid });
                paysliprecordsemployeepay = con.GetDataTable("sp_GetPaySlipRecordsEmployeePay");
                //paysliprecordsemployeepay = PayMtd.PaySlipRecordsEmployeePay(empid);
                //int schmct = PayMtd.GetSalarySchemes(EmpLevel, EmpCategory).Rows.Count;

                #region "New Payslip"
                TotalValue = 0;
                int latestpayslipcounter = 0;

                con = new Connection();
                LatestPayslipForm = con.GetDataTable("sp_GetLatestPayslipForm");
                //LatestPayslipForm = PayMtd.LatestPayslipForm();
                latestpayslipcounter = LatestPayslipForm.Rows.Count;
                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = empid });
                con.myparameters.Add(new myParameters { ParameterName = "@MONTH", mytype = SqlDbType.NVarChar, Value = DDLMonths.ToString() });
                DataTable BonusPayoutMonths = con.GetDataTable("sp_GetBonusPayouts");
                //DataTable BonusPayoutMonths = PayMtd.BonusPayoutMonths(empid, DDLMonths.ToString());
                string filename = "no filename";
                if (latestpayslipcounter == 1)
                {
                    #region Retrieve Neccessary Data
                    con = new Connection();
                    con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = empid });
                    GetEmpLevelandCategory = con.GetDataTable("sp_SearchMasterControl_LeaveManagement_GetEmpLevelandCategory");

                    //GetEmpLevelandCategory = PayMtd.GetEmpLevelandCategory(empid);
                    int psrpsd = 0;
                    int pswh = 0;
                    //paysliprecordspayperiod = PayMtd.PaySlipRecordsPayPeriod(EmpLevel, EmpCategory);                
                    payperiod = hoursdatefrom + '-' + hoursdateto;

                    con = new Connection();
                    con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = empid });
                    con.myparameters.Add(new myParameters { ParameterName = "@PAYPERIOD", mytype = SqlDbType.NVarChar, Value = DDLPayPeriod });
                    paysliprecordspayslipdetails = con.GetDataTable("sp_GetPaySlipRecordsPaySlipDetails");
                    //paysliprecordspayslipdetails = con.GetDataTable("sp_GetPaySlipRecordsPaySlipDetailsNITS");
                    //paysliprecordspayslipdetails = PayMtd.PaySlipRecordsPaySlipDetails(empid, DDLPayPeriod);
                    #endregion
                    string pathmoto;
                    //if (uProfile.CN.ToLower() == "newcore")
                    //{
                    //    pathmoto = "PaySlipDefaultNewcore";
                    //}
                    //else
                    //{
                    //    pathmoto = "PaySlipDefault";
                    //}
                    pathmoto = "PaySlipDefault";
                    //LatestPayslipForm.Rows[0][0].ToString();
                    PdfReader reader1 = new PdfReader(HttpContext.Current.Server.MapPath("~/pdf/") + pathmoto + ".pdf");
                    LocationTextExtractionStrategy lteStrategy = new LocationTextExtractionStrategy();
                    string strExact = PdfTextExtractor.GetTextFromPage(reader1, 1, lteStrategy);
                    StringBuilder sb = new StringBuilder();
                    //this code used for replacing the found text
                    sb.Append(PdfTextExtractor.GetTextFromPage(reader1, 1));
                    #region Passing Retrieved Datas
                    sb.Replace("[Employee ID]", empid);
                    if (paysliprecordsempmaster.Rows.Count > 0)
                    {
                        sb.Replace("[Employee Name]", paysliprecordsempmaster.Rows[0][0].ToString() == "" ? "N/A" : paysliprecordsempmaster.Rows[0][0].ToString());
                        sb.Replace("[Date Joined]", paysliprecordsempmaster.Rows[0][2].ToString() == "" ? "N/A" : paysliprecordsempmaster.Rows[0][2].ToString());
                        sb.Replace("[Resigned]", paysliprecordsempmaster.Rows[0][3].ToString() == "" ? "N/A" : paysliprecordsempmaster.Rows[0][3].ToString());
                    }
                    else
                    {
                        sb.Replace("[Employee Name]", "N/A");
                        sb.Replace("[Date Joined]", "N/A");
                        sb.Replace("[Resigned]", "N/A");
                    }
                    if (paysliprecordsconfidentialinfo.Rows.Count > 0)
                    {
                        sb.Replace("[HDMF]", paysliprecordsconfidentialinfo.Rows[0][0].ToString() == "" ? "N/A" : paysliprecordsconfidentialinfo.Rows[0][0].ToString());
                        sb.Replace("[PhilHealth No]", paysliprecordsconfidentialinfo.Rows[0][1].ToString() == "" ? "N/A" : paysliprecordsconfidentialinfo.Rows[0][1].ToString());
                        sb.Replace("[SSS No]", paysliprecordsconfidentialinfo.Rows[0][2].ToString() == "" ? "N/A" : paysliprecordsconfidentialinfo.Rows[0][2].ToString());
                        sb.Replace("[TIN]", paysliprecordsconfidentialinfo.Rows[0][3].ToString() == "" ? "N/A" : paysliprecordsconfidentialinfo.Rows[0][3].ToString());
                        sb.Replace("[Tax Category]", paysliprecordsconfidentialinfo.Rows[0][4].ToString() == "" ? "N/A" : paysliprecordsconfidentialinfo.Rows[0][4].ToString());
                    }
                    else
                    {
                        sb.Replace("[HDMF]", "N/A");
                        sb.Replace("[PhilHealth No]", "N/A");
                        sb.Replace("[SSS No]", "N/A");
                        sb.Replace("[TIN]", "N/A");
                        sb.Replace("[Tax Category]", "N/A");
                    }
                    if (paysliprecordsemployeepay.Rows.Count > 0)
                    {

                        sb.Replace("[Bank Account]", paysliprecordsemployeepay.Rows[0][0].ToString() == "" ? "N/A" : paysliprecordsemployeepay.Rows[0][0].ToString());
                        sb.Replace("[Hourly Rate]", paysliprecordsemployeepay.Rows[0][1].ToString() == "" ? "0.00" : paysliprecordsemployeepay.Rows[0][1].ToString());
                    }
                    else
                    {
                        sb.Replace("[Bank Account]", "N/A");
                        sb.Replace("[Hourly Rate]", "N/A");
                    }
                    sb.Replace("[Pay Period]", DDLPayPeriod == "" ? "N/A" : DDLPayPeriod);
                    if (paysliprecordspayslipdetails.Rows.Count > 0)
                    {
                        sb.Replace("[Pay Day]", paysliprecordspayslipdetails.Rows[0][1].ToString() == "" ? "N/A" : paysliprecordspayslipdetails.Rows[0][1].ToString());
                        sb.Replace("[Basic Salary]", paysliprecordspayslipdetails.Rows[0][2].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][2].ToString())));
                        sb.Replace("[Non Taxable Allowance]", paysliprecordspayslipdetails.Rows[0][3].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][3].ToString())));
                        sb.Replace("[Regular Night Differential]", paysliprecordspayslipdetails.Rows[0][4].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][4].ToString())));
                        sb.Replace("[Regular Over Time]", paysliprecordspayslipdetails.Rows[0][5].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][5].ToString())));
                        sb.Replace("[Regular Holiday]", paysliprecordspayslipdetails.Rows[0][6].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][6].ToString())));
                        sb.Replace("[Regular Holiday Night Differential]", paysliprecordspayslipdetails.Rows[0][7].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][7].ToString())));
                        sb.Replace("[Regular Holiday Over Time]", paysliprecordspayslipdetails.Rows[0][8].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][8].ToString())));
                        sb.Replace("[Paid Time Off]", paysliprecordspayslipdetails.Rows[0][9].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][9].ToString())));
                        sb.Replace("[Special Holiday]", paysliprecordspayslipdetails.Rows[0][10].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][10].ToString())));
                        sb.Replace("[Special Holiday Night Differential]", paysliprecordspayslipdetails.Rows[0][11].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][11].ToString())));
                        sb.Replace("[Special Holiday Over Time]", paysliprecordspayslipdetails.Rows[0][12].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][12].ToString())));
                        sb.Replace("[Allowance]", paysliprecordspayslipdetails.Rows[0][13].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][13].ToString())));
                        sb.Replace("[Bonuses]", paysliprecordspayslipdetails.Rows[0][14].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][14].ToString())));
                        sb.Replace("[Adjustments]", paysliprecordspayslipdetails.Rows[0][15].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][15].ToString())));
                        sb.Replace("[Total Salary]", paysliprecordspayslipdetails.Rows[0][16].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][16].ToString())));
                        sb.Replace("[Employee HDMF]", paysliprecordspayslipdetails.Rows[0][17].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][17].ToString())));
                        sb.Replace("[Employee Philhealth]", paysliprecordspayslipdetails.Rows[0][18].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][18].ToString())));
                        sb.Replace("[Employee SSS]", paysliprecordspayslipdetails.Rows[0][19].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][19].ToString())));
                        sb.Replace("[Employee WithHoldingTax]", paysliprecordspayslipdetails.Rows[0][20].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][20].ToString())));
                        sb.Replace("[SSS Loan Deduction]", paysliprecordspayslipdetails.Rows[0][21].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][21].ToString())));
                        sb.Replace("[Pag-ibig Loan Deduction]", paysliprecordspayslipdetails.Rows[0][22].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][22].ToString())));
                        sb.Replace("[Net Pay]", paysliprecordspayslipdetails.Rows[0][23].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][23].ToString())));
                        sb.Replace("[Employer HDMF]", paysliprecordspayslipdetails.Rows[0][24].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][24].ToString())));
                        sb.Replace("[Employer Philhealth]", paysliprecordspayslipdetails.Rows[0][25].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][25].ToString())));
                        sb.Replace("[Employer SSS]", paysliprecordspayslipdetails.Rows[0][26].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][26].ToString())));
                        sb.Replace("[Employer SSS EC]", paysliprecordspayslipdetails.Rows[0][27].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][27].ToString())));
                    }
                    else
                    {
                        sb.Replace("[Pay Day]", "N/A");
                        sb.Replace("[Basic Salary]", "N/A");
                        sb.Replace("[Non Taxable Allowance]", "N/A");
                        sb.Replace("[Regular Night Differential]", "N/A");
                        sb.Replace("[Regular Over Time]", "N/A");
                        sb.Replace("[Regular Holiday]", "N/A");
                        sb.Replace("[Regular Holiday Night Differential]", "N/A");
                        sb.Replace("[Regular Holiday Over Time]", "N/A");
                        sb.Replace("[Paid Time Off]", "N/A");
                        sb.Replace("[Special Holiday]", "N/A");
                        sb.Replace("[Special Holiday Night Differential]", "N/A");
                        sb.Replace("[Special Holiday Over Time]", "N/A");
                        sb.Replace("[Allowance]", "N/A");
                        sb.Replace("[Bonuses]", "N/A");
                        sb.Replace("[Adjustments]", "N/A");
                        sb.Replace("[Total Salary]", "N/A");
                        sb.Replace("[Employee HDMF]", "N/A");
                        sb.Replace("[Employee Philhealth]", "N/A");
                        sb.Replace("[Employee SSS]", "N/A");
                        sb.Replace("[Employee WithHoldingTax]", "N/A");
                        sb.Replace("[SSS Loan Deduction]", "N/A");
                        sb.Replace("[Pag-ibig Loan Deduction]", "N/A");
                        sb.Replace("[Net Pay]", "N/A");
                        sb.Replace("[Employer HDMF]", "N/A");
                        sb.Replace("[Employer Philhealth]", "N/A");
                        sb.Replace("[Employer SSS]", "N/A");
                        sb.Replace("[Employer SSS EC]", "N/A");
                    }

                    #endregion

                    FileStream Newstream = new FileStream(string.Concat(HttpContext.Current.Server.MapPath("~/pdf/"), empid + pathmoto + ".pdf"), FileMode.Create);
                    MemoryStream ms = new MemoryStream();
                    Document document = new Document(PageSize.A4, 25, 25, 30, 30);
                    //Code for adding the string builder sb to the document
                    PdfWriter writer = PdfWriter.GetInstance(document, Newstream);
                    document.Open();
                    document.Add(new Paragraph(sb.ToString()));
                    document.Close();
                    writer.Close();
                }
                else
                {
                    #region Fixed payslip

                    int psrpsd = 0;
                    int pswh = 0;


                    //paysliprecordspayslipdetails = PayMtd.PaySlipRecordsPaySlipDetails(empid, DDLPayPeriod);

                    con = new Connection();
                    con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = empid });
                    con.myparameters.Add(new myParameters { ParameterName = "@PAYPERIOD", mytype = SqlDbType.NVarChar, Value = DDLPayPeriod });
                    paysliprecordspayslipdetails = con.GetDataTable("sp_GetPaySlipRecordsPaySlipDetails");


                    con = new Connection();
                    con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = empid });
                    con.myparameters.Add(new myParameters { ParameterName = "@YEAR", mytype = SqlDbType.NVarChar, Value = DDLYear });
                    con.myparameters.Add(new myParameters { ParameterName = "@PAYPERIOD", mytype = SqlDbType.NVarChar, Value = DDLPayPeriod });
                    YearToDate = con.GetDataTable("sp_GetPaySlipRecordsPaySlipDetailsYearToDate");

                    con = new Connection();
                    con.myparameters.Add(new myParameters { ParameterName = "@empId", mytype = SqlDbType.NVarChar, Value = empid });
                    DataTable DTProfile = con.GetDataTable("sp_getCompleteProfile_Mobilev2");
                    //YearToDate = PayMtd.PayslipDetailsYearToDate(empid, DDLYear, DDLPayPeriod);

                    //Response.Redirect("~/pdf/BIR19021.pdf"); // view pdf
                    string path = "https://ws.durusthr.com/ILM_WS_Live/pdf";

                    //if (uProfile.CN.ToLower() == "newcore")
                    //{
                    //    pdfpath = "/PaySlipDefaultNewcore.pdf";
                    //}
                    //else
                    //{
                    //    pdfpath = "/PaySlipDefault.pdf";
                    //}
                    var pdfReader1 = new PdfReader(path + "/PaySlipDefault.pdf");

                    AcroFields af = pdfReader1.AcroFields;
                    //LocationTextExtractionStrategy lteStrategy = new LocationTextExtractionStrategy();
                    //string strExact = PdfTextExtractor.GetTextFromPage(pdfReader1,1, lteStrategy);
                    //StringBuilder sb = new StringBuilder();
                    //string sam = "";
                    //foreach (var field in af.Fields)
                    //{
                    //    sam = sam + field.Key + "-"; // names of textfields
                    //}
                    //viewpdf.InnerText = sam;
                    // get the file paths


                    // Template file path

                    //if (uProfile.CN.ToLower() == "newcore")
                    //{
                    //    formFile = path + "/PaySlipDefaultNewcore.pdf";
                    //}
                    //else
                    //{
                    //    formFile = path + "/PaySlipDefault.pdf";
                    //}
                    var lnameencrypt = paysliprecordsempmaster.Rows[0][4].ToString();
                    var lname = paysliprecordsempmaster.Rows[0][5].ToString();
                    var fname = paysliprecordsempmaster.Rows[0][6].ToString();
                    string formFile = "/PaySlipDefault.pdf";
                    string path2 = HttpContext.Current.Server.MapPath("pdf");
                    // Output file path
                    string newFile = path2 + "/" + lname + " " + fname + "-payslip-" + DDLPayPeriod + ".pdf";
                    filename = lname + " " + fname + "-payslip-" + DDLPayPeriod + ".pdf";
                    string pdfpass1 = "", pdfpass2 = "";

                    // read the template file
                    PdfReader reader;
                    if (uProfile.CN.ToLower() == "newcore")
                    {
                        if (DDLPayPeriod == "2020-02-29-2020-02-29")
                            reader = new PdfReader(path2 + "/PaySlipDefaultNewcore(TaxableIncentive).pdf");
                        else
                            reader = new PdfReader(path2 + "/PaySlipDefaultNewcore.pdf");
                    }
                    else
                    {
                        reader = new PdfReader(path2 + "/PaySlipDefault.pdf");
                    }




                    // instantiate PDFStamper object
                    // The Output file will be created from template file and edited by the PDFStamper
                    PdfStamper stamper = new PdfStamper(reader, new FileStream(newFile, FileMode.Create));
                    if (uProfile.CN.ToLower() == "newcore")
                    {
                        Connection Connection = new Connection();
                        Connection.myparameters.Add(new myParameters { ParameterName = "@empid", mytype = SqlDbType.NVarChar, Value = empid });
                        //DataTable DT = Connection.GetDataTable("sp_getEmailsbyEmpID"); //with email
                        DataTable DT = Connection.GetDataTable("sp_getEmailsbyEmpID2"); //no emails and zips
                        if (DT.Rows.Count > 0)
                        {
                            if (empid == "049-0614" || empid == "314-0619")
                            {
                                pdfpass1 = empid;
                                pdfpass2 = empid;
                            }
                            else
                            {
                                pdfpass1 = DT.Rows[0][2].ToString();
                                pdfpass2 = DT.Rows[0][2].ToString();
                            }
                        }
                        else
                        {
                            pdfpass1 = lnameencrypt.ToLower();
                            pdfpass2 = lname;
                        }
                    }
                    else
                    {
                        if (empid == "049-0614" || empid == "314-0619")
                        {
                            pdfpass1 = empid;
                            pdfpass2 = empid;
                        }
                        else
                        {
                            pdfpass1 = lnameencrypt.ToLower();
                            pdfpass2 = lname;
                        }
                    }
                    if (empid == "002-0812")
                    {
                        pdfpass1 = "DanielDJ!";
                        pdfpass2 = "DanielDJ!";
                    }
                    else if (empid == "001-0812")
                    {
                        pdfpass1 = "JacquelineDJ!";
                        pdfpass2 = "JacquelineDJ!";
                    }
                    else if (empid == "356-0119")
                    {
                        pdfpass1 = "GraceOlg!";
                        pdfpass2 = "GraceOlg!";
                    }
                    else if (empid == "376-0120")
                    {
                        pdfpass1 = "SherwinS!";
                        pdfpass2 = "SherwinS!";
                    }
                    else if (empid == "154-0817")
                    {
                        pdfpass1 = "Newcore6";
                        pdfpass2 = "Newcore6";
                    }
                    else if (empid == "392-0120")
                    {
                        pdfpass1 = "rommelSA!";
                        pdfpass2 = "rommelSA!";
                    }
                    else if (empid == "049-0614" || empid == "314-0619")
                    {
                        pdfpass1 = "Aranes";
                        pdfpass2 = "Aranes";
                    }
                    stamper.SetEncryption(true, pdfpass1, pdfpass2, PdfWriter.ALLOW_SCREENREADERS); //UNCOMMENT LATER
                    //stamper.SetEncryption(true, "RommelSA!", "RommelSA!", PdfWriter.ALLOW_SCREENREADERS); //UNCOMMENT LATER

                    //stamper.SetEncryption(true, lnameencrypt.ToLower(), lname, PdfWriter.ALLOW_SCREENREADERS);
                    //PdfEncryptor.Encrypt(reader, new FileStream(newFile, FileMode.Open), true, empid, "secret", PdfWriter.ALLOW_SCREENREADERS);
                    // Object to deal with the Output file's textfields
                    AcroFields fields = stamper.AcroFields;

                    // set form fields("Field Name in PDF", "String to be placed in this PDF text field");
                    //for (int i = 0; i <= 1000; i++)
                    //{
                    //    string ii = i.ToString();
                    //    
                    //    fields.SetField("untitled" + i, "vt" + ii); //temporary values
                    //}

                    //------------------------------------------------
                    //loop to display all field names
                    fields.SetField("Company Name", "Newcore Industries International Inc.");//company Name Newcore
                    //fields.SetField("Company Name", "Darvin2305 Solutions Corp.");//company Name Darvin
                    //fields.SetField("Company Name", "Newcore IT Solutions");//company Name NITS
                    if (paysliprecordsempmaster.Rows.Count > 0)
                    {
                        //fields.SetFieldProperty("txtempid", "setflags", PdfAnnotation.FLAGS_HIDDEN,null);
                        fields.SetField("untitled2", paysliprecordsempmaster.Rows[0][0].ToString());//Employee Name
                        fields.SetField("employeeno", empid);//Employee No
                        fields.SetField("joined", paysliprecordsempmaster.Rows[0][2].ToString());//Joined
                        fields.SetField("resigned", paysliprecordsempmaster.Rows[0][3].ToString());//Resigned
                    }
                    else
                    {

                    }
                    if (paysliprecordsconfidentialinfo.Rows.Count > 0)
                    {
                        fields.SetField("pagibigno", paysliprecordsconfidentialinfo.Rows[0][0].ToString());//Pag-IBIG No
                        fields.SetField("philhealthno", paysliprecordsconfidentialinfo.Rows[0][1].ToString());//PhilHealth No
                        fields.SetField("sssno", paysliprecordsconfidentialinfo.Rows[0][2].ToString());//SSS No
                        fields.SetField("taxno", paysliprecordsconfidentialinfo.Rows[0][3].ToString());//Tax No
                        fields.SetField("taxcategory", paysliprecordsconfidentialinfo.Rows[0][4].ToString());//Tax Category
                    }
                    else
                    {

                    }
                    if (paysliprecordsemployeepay.Rows.Count > 0)
                    {
                        fields.SetField("bankaccount", paysliprecordsemployeepay.Rows[0][0].ToString());//Bank Account
                        fields.SetField("hourlyrate", paysliprecordsemployeepay.Rows[0][1].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordsemployeepay.Rows[0][1].ToString())));//Hourly Rate
                    }
                    else
                    {
                        fields.SetField("hourlyrate", "0.00");
                    }
                    fields.SetField("payperiod", DDLPayPeriod);//Pay Period


                    //--New Start
                    #region New PDF
                    con = new Connection();
                    con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = empid });
                    con.myparameters.Add(new myParameters { ParameterName = "@PAYPERIOD", mytype = SqlDbType.NVarChar, Value = DDLPayPeriod });
                    if (uProfile.CN.ToLower() == "newcore")
                    {
                        paysliprecordspayslipdetails = con.GetDataTable("sp_GetPaySlipRecordsPaySlipDetailsNewcore"); //Newcore
                        //paysliprecordspayslipdetails = con.GetDataTable("sp_GetPaySlipRecordsPaySlipDetailsDarvin");//Darvin
                        //paysliprecordspayslipdetails = con.GetDataTable("sp_GetPaySlipRecordsPaySlipDetailsNITS");//NITS
                        if (paysliprecordspayslipdetails.Rows.Count > 0)
                        {
                            fields.SetField("payday", paysliprecordspayslipdetails.Rows[0]["Pay_Day"].ToString());
                            fields.SetField("hourlyrate", "");
                            fields.SetField("basicsalarycurrent", paysliprecordspayslipdetails.Rows[0]["Basic Salary Amount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Basic Salary Amount"].ToString())));
                            fields.SetField("monthlyrate", paysliprecordspayslipdetails.Rows[0]["Monthly Salary Rate"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Monthly Salary Rate"].ToString())));
                            //fields.SetField("monthlyrate", paysliprecordspayslipdetails.Rows[0]["Monthly Salary Rate"].ToString() == "" ? "0.00" : String.Format("{0:n}", Math.Ceiling(Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Monthly Salary Rate"].ToString()))) + " USD");//darvin
                            fields.SetField("deductionsphilhealthcurrent", paysliprecordspayslipdetails.Rows[0]["PhilHealth Employee Share"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["PhilHealth Employee Share"].ToString())));
                            fields.SetField("deductionspagibigcurrent", paysliprecordspayslipdetails.Rows[0]["Pag-ibig Employee Share"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Pag-ibig Employee Share"].ToString())));
                            fields.SetField("deductionsssscurrent", paysliprecordspayslipdetails.Rows[0]["SSS Employee Share"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["SSS Employee Share"].ToString())));
                            fields.SetField("deductionswtaxcurrent", paysliprecordspayslipdetails.Rows[0]["W-Tax"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["W-Tax"].ToString())));
                            fields.SetField("pagibigemployersharecurrent", paysliprecordspayslipdetails.Rows[0]["Pag-ibig Employer Share"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Pag-ibig Employer Share"].ToString())));
                            fields.SetField("philhealthemployersharecurrent", paysliprecordspayslipdetails.Rows[0]["PhilHealth Employer Share"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["PhilHealth Employer Share"].ToString())));
                            fields.SetField("sssemployersharecurrent", paysliprecordspayslipdetails.Rows[0]["SSS Employer Share"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["SSS Employer Share"].ToString())));
                            fields.SetField("sssemployerecsharecurrent", paysliprecordspayslipdetails.Rows[0]["SSS Employer EC Share"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["SSS Employer EC Share"].ToString())));
                            fields.SetField("netpaycurrent", paysliprecordspayslipdetails.Rows[0]["Net Pay"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Net Pay"].ToString())));
                            fields.SetField("mealallowancecurrent", paysliprecordspayslipdetails.Rows[0]["Meal Allowance"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Meal Allowance"].ToString())));
                            fields.SetField("13thmonthnontaxablecurrent", paysliprecordspayslipdetails.Rows[0]["13th Month Non-Taxable"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["13th Month Non-Taxable"].ToString())));

                            fields.SetField("adjabsencecurrent", paysliprecordspayslipdetails.Rows[0]["Adjusted LH Amount (100%)"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Adjusted LH Amount (100%)"].ToString())));
                            fields.SetField("adjlatecurrent", paysliprecordspayslipdetails.Rows[0]["Adjusted LHOT Amt (260%)"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Adjusted LHOT Amt (260%)"].ToString())));
                            fields.SetField("adjregularhourscurrent", paysliprecordspayslipdetails.Rows[0]["Adjusted SH Amount (30%)"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Adjusted SH Amount (30%)"].ToString())));
                            fields.SetField("adjregularotcurrent", paysliprecordspayslipdetails.Rows[0]["Adjusted SHRD Amount (150%)"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Adjusted SHRD Amount (150%)"].ToString())));
                            fields.SetField("deductionsabsenceshrs", paysliprecordspayslipdetails.Rows[0]["Absences Hours"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Absences Hours"].ToString())));
                            fields.SetField("deductionstardinesscurrent", paysliprecordspayslipdetails.Rows[0]["Tardiness Amount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Tardiness Amount"].ToString())));
                            fields.SetField("deductionsabsencescurrent", paysliprecordspayslipdetails.Rows[0]["Absences Amount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Absences Amount"].ToString())));
                            fields.SetField("taxableincomecurrent", paysliprecordspayslipdetails.Rows[0]["Taxable Income"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Taxable Income"].ToString())));
                            fields.SetField("grosspaycurrent", paysliprecordspayslipdetails.Rows[0]["Gross Pay"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Gross Pay"].ToString())));


                            fields.SetField("regularothrs", paysliprecordspayslipdetails.Rows[0]["Regular Overtime Hours"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Regular Overtime Hours"].ToString())));
                            fields.SetField("regularotcurrent", paysliprecordspayslipdetails.Rows[0]["Regular Overtime Amount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Regular Overtime Amount"].ToString())));


                            fields.SetField("restdayothrs", paysliprecordspayslipdetails.Rows[0]["Regular Holiday Hours"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Regular Holiday Hours"].ToString())));
                            fields.SetField("restdayotcurrent", paysliprecordspayslipdetails.Rows[0]["Regular Holiday Amount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Regular Holiday Amount"].ToString())));
                            fields.SetField("restdayndothrs", paysliprecordspayslipdetails.Rows[0]["Regular Holiday Overtime Hours"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Regular Holiday Overtime Hours"].ToString())));
                            fields.SetField("restdayndotcurrent", paysliprecordspayslipdetails.Rows[0]["Regular Holiday Overtime Amount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Regular Holiday Overtime Amount"].ToString())));


                            fields.SetField("regularholidayhrs", paysliprecordspayslipdetails.Rows[0]["Special Holiday Hours"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Special Holiday Hours"].ToString())));
                            fields.SetField("regularholidaycurrent", paysliprecordspayslipdetails.Rows[0]["Special Holiday Amount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Special Holiday Amount"].ToString())));
                            fields.SetField("regularholidayotcurrent", paysliprecordspayslipdetails.Rows[0]["Special Holiday Overtime Hours"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Special Holiday Overtime Hours"].ToString())));

                            fields.SetField("adjregularndothrs", paysliprecordspayslipdetails.Rows[0]["LWP Hours"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["LWP Hours"].ToString())));
                            fields.SetField("adjregularndotcurrent", paysliprecordspayslipdetails.Rows[0]["LWP Amount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["LWP Amount"].ToString())));




                            fields.SetField("regularndotcurrent", paysliprecordspayslipdetails.Rows[0]["RD Amount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["RD Amount"].ToString())));
                            fields.SetField("regularndothrs", paysliprecordspayslipdetails.Rows[0]["RDOT Hours"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["RDOT Hours"].ToString())));
                            fields.SetField("restdaycurrent", paysliprecordspayslipdetails.Rows[0]["RDOT Amount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["RDOT Amount"].ToString())));

                            fields.SetField("taxrefundcurrent", paysliprecordspayslipdetails.Rows[0]["HDMF Loans Amt"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["HDMF Loans Amt"].ToString())));
                            fields.SetField("deductionstaxpayablecurrent", paysliprecordspayslipdetails.Rows[0]["SSS Loans Amt"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["SSS Loans Amt"].ToString())));
                            fields.SetField("deminimiscurrent", paysliprecordspayslipdetails.Rows[0]["De Minimis/Adj DM"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["De Minimis/Adj DM"].ToString())));



                            fields.SetField("regularholidayndothrs", paysliprecordspayslipdetails.Rows[0]["Adjusted Regular Hours"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Adjusted Regular Hours"].ToString())));
                            fields.SetField("regularholidayndotcurrent", paysliprecordspayslipdetails.Rows[0]["Adjusted Reg Hrs. Amount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Adjusted Reg Hrs. Amount"].ToString())));
                            fields.SetField("adjregularotcurrent", paysliprecordspayslipdetails.Rows[0]["Adjusted SHOT Amount (169%)"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Adjusted SHOT Amount (169%)"].ToString())));
                            fields.SetField("ecolacurrent", paysliprecordspayslipdetails.Rows[0]["Other NT Deductions"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Other NT Deductions"].ToString())));
                            fields.SetField("specialholidayotcurrent", paysliprecordspayslipdetails.Rows[0]["Adjusted RD Amount (130%)"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Adjusted RD Amount (130%)"].ToString())));
                            fields.SetField("adjregularhourshrs", paysliprecordspayslipdetails.Rows[0]["Adj Spl Hol Hours"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Adj Spl Hol Hours"].ToString())));

                            fields.SetField("deductionsplacurrent", paysliprecordspayslipdetails.Rows[0]["PLA"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["PLA"].ToString())));
                            fields.SetField("deductionstardinesshrs", paysliprecordspayslipdetails.Rows[0]["Tardiness Minutes"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Tardiness Minutes"].ToString())));

                            fields.SetField("regularndothrs", paysliprecordspayslipdetails.Rows[0]["RD Hours"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["RD Hours"].ToString())));


                            fields.SetField("specialholidaycurrent", paysliprecordspayslipdetails.Rows[0]["Adjusted Reg OT Amount (125%)"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Adjusted Reg OT Amount (125%)"].ToString())));


                            fields.SetField("13thmonthtaxablecurrent", paysliprecordspayslipdetails.Rows[0]["13th Month Taxable"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["13th Month Taxable"].ToString())));
                            fields.SetField("deductionsprulifecurrent", paysliprecordspayslipdetails.Rows[0]["Pru Life"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Pru Life"].ToString())));
                            fields.SetField("deductionssdcurrent", paysliprecordspayslipdetails.Rows[0]["SD"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["SD"].ToString())));

                            fields.SetField("deductionssssloanamountcurrent", paysliprecordspayslipdetails.Rows[0]["Tax Payable"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Tax Payable"].ToString())));


                            fields.SetField("specialholidayhrs", paysliprecordspayslipdetails.Rows[0]["Adjusted Regular OT Hours"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Adjusted Regular OT Hours"].ToString())));





                            fields.SetField("adjrestdayotcurrent", paysliprecordspayslipdetails.Rows[0]["Tax Incentive"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Tax Incentive"].ToString())));
                            fields.SetField("deductionspagibigloanamountcurrent", paysliprecordspayslipdetails.Rows[0]["AVEGA"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["AVEGA"].ToString())));
                            fields.SetField("adjrdcurrent", paysliprecordspayslipdetails.Rows[0]["Salary Adjustment"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Salary Adjustment"].ToString())));

                            fields.SetField("adjnontaxableallowancecurrent", paysliprecordspayslipdetails.Rows[0]["ECOLA"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["ECOLA"].ToString())));//Basic Salary


                        }
                        else
                        {

                        }
                    }
                    else
                    {
                        paysliprecordspayslipdetails = con.GetDataTable("sp_GetPaySlipRecordsPaySlipDetailsDarvin");
                        if (paysliprecordspayslipdetails.Rows.Count > 0)
                        {
                            fields.SetField("payday", paysliprecordspayslipdetails.Rows[0][1].ToString());//Pay Day
                            fields.SetField("basicsalarycurrent", paysliprecordspayslipdetails.Rows[0][2].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][2].ToString())));//Basic Salary
                            fields.SetField("nontaxableallowancecurrent", paysliprecordspayslipdetails.Rows[0][3].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][3].ToString())));//Non Taxable Allowance
                            fields.SetField("regularnightdiffcurrent", paysliprecordspayslipdetails.Rows[0][4].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][4].ToString())));//Regular Night Diff
                            fields.SetField("regularotcurrent", paysliprecordspayslipdetails.Rows[0][5].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][5].ToString())));//Regular Overtime
                            fields.SetField("regularholidaycurrent", paysliprecordspayslipdetails.Rows[0][6].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][6].ToString())));//Regular Holiday
                            fields.SetField("regularholidaynightdiffcurrent", paysliprecordspayslipdetails.Rows[0][7].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][7].ToString())));//Regular Holiday Night Diff
                            fields.SetField("regularholidayotcurrent", paysliprecordspayslipdetails.Rows[0][8].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][8].ToString())));//Regular Overtime
                            fields.SetField("paidtimeoffcurrent", paysliprecordspayslipdetails.Rows[0][9].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][9].ToString())));//Paid Time Off
                            fields.SetField("specialholidaycurrent", paysliprecordspayslipdetails.Rows[0][10].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][10].ToString())));//Special Holiday
                            fields.SetField("specialholidaynightdiffcurrent", paysliprecordspayslipdetails.Rows[0][11].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][11].ToString())));//Special Holiday Night Diff
                            fields.SetField("specialholidayotcurrent", paysliprecordspayslipdetails.Rows[0][12].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][12].ToString())));//Special Holiday OT
                            fields.SetField("allowancescurrent", paysliprecordspayslipdetails.Rows[0][13].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][13].ToString())));//Allowances
                            fields.SetField("bonuscurrent", paysliprecordspayslipdetails.Rows[0][14].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][14].ToString())));//Bonus
                            fields.SetField("adjustmentscurrent", paysliprecordspayslipdetails.Rows[0][15].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][15].ToString())));//adjustments
                            fields.SetField("totalcurrent", paysliprecordspayslipdetails.Rows[0][16].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][16].ToString())));//Gross Pay
                            fields.SetField("employeehdmfcurrent", paysliprecordspayslipdetails.Rows[0][17].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][17].ToString())));//Employee HDMF
                            fields.SetField("employeephilhealthcurrent", paysliprecordspayslipdetails.Rows[0][18].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][18].ToString())));//Employee PhilHealth
                            fields.SetField("employeesocialsecuritycurrent", paysliprecordspayslipdetails.Rows[0][19].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][19].ToString())));//Employee SSS
                            fields.SetField("withholdingtaxcurrent", paysliprecordspayslipdetails.Rows[0][20].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][20].ToString())));//Employee WithHoldingTax
                            fields.SetField("sssloandeductioncurrent", paysliprecordspayslipdetails.Rows[0][21].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][21].ToString())));//Loans SSS
                            fields.SetField("pagibigloandeductioncurrent", paysliprecordspayslipdetails.Rows[0][22].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][22].ToString())));//Loans Pagibig
                            fields.SetField("netpaycurrent", paysliprecordspayslipdetails.Rows[0][23].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][23].ToString())));//Net Pay
                            fields.SetField("employerhdmfcurrent", paysliprecordspayslipdetails.Rows[0][24].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][24].ToString())));//Employer HDMF
                            fields.SetField("employerphilhealthcurrent", paysliprecordspayslipdetails.Rows[0][25].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][25].ToString())));//Employer PhilHealth
                            fields.SetField("employersocialsecuritycurrent", paysliprecordspayslipdetails.Rows[0][26].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][26].ToString())));//Employer SSS
                            fields.SetField("employersocialsecurityeccurrent", paysliprecordspayslipdetails.Rows[0][27].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][27].ToString())));//Employer SSS EC
                            fields.SetField("deductionsabsencescurrent", paysliprecordspayslipdetails.Rows[0][28].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][28].ToString())));//Deductions Absences
                            fields.SetField("deductionslatescurrent", paysliprecordspayslipdetails.Rows[0][29].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][29].ToString())));//Deductions Lates
                            fields.SetField("doubleholidaycurrent", paysliprecordspayslipdetails.Rows[0][30].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][30].ToString())));//Double Holiday
                            fields.SetField("doubleholidayndcurrent", paysliprecordspayslipdetails.Rows[0][31].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][31].ToString())));//Double Holiday ND
                            fields.SetField("doubleholidayotcurrent", paysliprecordspayslipdetails.Rows[0][32].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][32].ToString())));//Double Holiday OT
                        }
                        else
                        {

                        }
                    }

                    if (uProfile.CN.ToLower() == "newcore")
                    {
                        con = new Connection();
                        con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = empid });
                        con.myparameters.Add(new myParameters { ParameterName = "@PAYPERIOD", mytype = SqlDbType.NVarChar, Value = paysliprecordspayslipdetails.Rows[0][0].ToString() });
                        con.myparameters.Add(new myParameters { ParameterName = "@YEAR", mytype = SqlDbType.NVarChar, Value = uProfile.PayYear });
                        YearToDate = con.GetDataTable("sp_GetPaySlipRecordsPaySlipDetailsYearToDateNewcore"); //Newcore
                        //YearToDate = con.GetDataTable("sp_GetPaySlipRecordsPaySlipDetailsYearToDateDarvin");//Darvin
                        //YearToDate = con.GetDataTable("sp_GetPaySlipRecordsPaySlipDetailsYearToDateNITS");//NITS
                        if (YearToDate.Rows.Count > 0)
                        {
                            fields.SetField("basicsalaryytd", YearToDate.Rows[0]["Basic Salary Amount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["Basic Salary Amount"].ToString())));
                            fields.SetField("deductionspagibigytd", YearToDate.Rows[0]["Pag-ibig Employee Share"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["Pag-ibig Employee Share"].ToString())));
                            fields.SetField("deductionsphilhealthytd", YearToDate.Rows[0]["PhilHealth Employee Share"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["PhilHealth Employee Share"].ToString())));
                            fields.SetField("deductionssssytd", YearToDate.Rows[0]["SSS Employee Share"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["SSS Employee Share"].ToString())));
                            fields.SetField("deductionswtaxytd", YearToDate.Rows[0]["W-Tax"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["W-Tax"].ToString())));
                            fields.SetField("pagibigemployershareytd", YearToDate.Rows[0]["Pag-ibig Employer Share"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["Pag-ibig Employer Share"].ToString())));
                            fields.SetField("philhealthemployershareytd", YearToDate.Rows[0]["PhilHealth Employer Share"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["PhilHealth Employer Share"].ToString())));
                            fields.SetField("sssemployershareytd", YearToDate.Rows[0]["SSS Employer Share"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["SSS Employer Share"].ToString())));
                            fields.SetField("sssemployerecshareytd", YearToDate.Rows[0]["SSS Employer EC Share"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["SSS Employer EC Share"].ToString())));
                            fields.SetField("netpayytd", YearToDate.Rows[0]["Net Pay"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["Net Pay"].ToString())));
                            fields.SetField("adjnontaxableallowanceytd", YearToDate.Rows[0]["Meal Allowance"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["Meal Allowance"].ToString())));
                            fields.SetField("13monthnontaxableytd", YearToDate.Rows[0]["13th Month Non-Taxable"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["13th Month Non-Taxable"].ToString())));

                            fields.SetField("adjabsenceytd", YearToDate.Rows[0]["Adjusted LH Amount (100%)"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["Adjusted LH Amount (100%)"].ToString())));
                            fields.SetField("adjlateytd", YearToDate.Rows[0]["Adjusted LHOT Amt (260%)"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["Adjusted LHOT Amt (260%)"].ToString())));
                            fields.SetField("adjregularhoursytd", YearToDate.Rows[0]["Adjusted SH Amount (30%)"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["Adjusted SH Amount (30%)"].ToString())));
                            fields.SetField("adjregularotytd", YearToDate.Rows[0]["Adjusted SHRD Amount (150%)"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["Adjusted SHRD Amount (150%)"].ToString())));

                            fields.SetField("deductionstardinessytd", YearToDate.Rows[0]["Tardiness Amount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["Tardiness Amount"].ToString())));
                            fields.SetField("deductionsabsencesytd", YearToDate.Rows[0]["Absences Amount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["Absences Amount"].ToString())));
                            fields.SetField("taxableincomeytd", YearToDate.Rows[0]["Taxable Income"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["Taxable Income"].ToString())));
                            fields.SetField("grosspayytd", YearToDate.Rows[0]["Gross Pay"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["Gross Pay"].ToString())));



                            fields.SetField("regularotytd", YearToDate.Rows[0]["Regular Overtime Amount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["Regular Overtime Amount"].ToString())));



                            fields.SetField("restdayotytd", YearToDate.Rows[0]["Regular Holiday Amount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["Regular Holiday Amount"].ToString())));

                            fields.SetField("restdayndotytd", YearToDate.Rows[0]["Regular Holiday Overtime Amount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["Regular Holiday Overtime Amount"].ToString())));



                            fields.SetField("regularholidayytd", YearToDate.Rows[0]["Special Holiday Amount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["Special Holiday Amount"].ToString())));
                            fields.SetField("regularholidayotytd", YearToDate.Rows[0]["Special Holiday Overtime Hours"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["Special Holiday Overtime Hours"].ToString())));


                            fields.SetField("adjregularndotytd", YearToDate.Rows[0]["LWP Amount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["LWP Amount"].ToString())));




                            fields.SetField("regularndotytd", YearToDate.Rows[0]["RD Amount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["RD Amount"].ToString())));

                            fields.SetField("restdayytd", YearToDate.Rows[0]["RDOT Amount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["RDOT Amount"].ToString())));

                            fields.SetField("taxrefundytd", YearToDate.Rows[0]["HDMF Loans Amt"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["HDMF Loans Amt"].ToString())));
                            fields.SetField("deductionstaxpayableytd", YearToDate.Rows[0]["SSS Loans Amt"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["SSS Loans Amt"].ToString())));
                            fields.SetField("deminimisytd", YearToDate.Rows[0]["De Minimis/Adj DM"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["De Minimis/Adj DM"].ToString())));




                            fields.SetField("regularholidayndotytd", YearToDate.Rows[0]["Adjusted Reg Hrs. Amount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["Adjusted Reg Hrs. Amount"].ToString())));
                            fields.SetField("adjregularotcurrent", YearToDate.Rows[0]["Adjusted SHOT Amount (169%)"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["Adjusted SHOT Amount (169%)"].ToString())));
                            fields.SetField("ecolaytd", YearToDate.Rows[0]["Other NT Deductions"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["Other NT Deductions"].ToString())));
                            fields.SetField("specialholidayotytd", YearToDate.Rows[0]["Adjusted RD Amount (130%)"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["Adjusted RD Amount (130%)"].ToString())));


                            fields.SetField("deductionsplaytd", YearToDate.Rows[0]["PLA"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["PLA"].ToString())));





                            fields.SetField("specialholidayytd", YearToDate.Rows[0]["Adjusted Reg OT Amount (125%)"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["Adjusted Reg OT Amount (125%)"].ToString())));


                            fields.SetField("13thmonthtaxableytd", YearToDate.Rows[0]["13th Month Taxable"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["13th Month Taxable"].ToString())));
                            fields.SetField("deductionsprulifeytd", YearToDate.Rows[0]["Pru Life"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["Pru Life"].ToString())));
                            fields.SetField("deductionssdytd", YearToDate.Rows[0]["SD"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["SD"].ToString())));

                            fields.SetField("deductionssssloanamountytd", YearToDate.Rows[0]["Tax Payable"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["Tax Payable"].ToString())));








                            fields.SetField("adjrestdayotytd", YearToDate.Rows[0]["Tax Incentive"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["Tax Incentive"].ToString())));
                            fields.SetField("deductionspagibigloanamountytd", YearToDate.Rows[0]["AVEGA"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["AVEGA"].ToString())));
                            fields.SetField("adjrdytd", YearToDate.Rows[0]["Salary Adjustment"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["Salary Adjustment"].ToString())));


                            fields.SetField("othernontaxableadjustmentytd", YearToDate.Rows[0]["ECOLA"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["ECOLA"].ToString())));

                        }
                        else
                        {

                        }
                    }
                    else
                    {
                        if (YearToDate.Rows.Count > 0)
                        {
                            fields.SetField("basicsalaryytd", YearToDate.Rows[0][13].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][13].ToString())));//Basic Salary
                            fields.SetField("deductionsabsencesytd", YearToDate.Rows[0][14].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][14].ToString())));//Deduction absences
                            fields.SetField("deductionstardinessytd", YearToDate.Rows[0][15].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][15].ToString())));//Deduction lates
                            fields.SetField("13thmonthtaxableytd", YearToDate.Rows[0][16].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][16].ToString())));//13 month

                            fields.SetField("regularotytd", YearToDate.Rows[0][17].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][17].ToString())));//Regular OT
                            fields.SetField("regularndotytd", YearToDate.Rows[0][18].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][18].ToString())));//Regular Night Diff

                            fields.SetField("restdayotytd", YearToDate.Rows[0][52].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][52].ToString())));//Reg RDOT
                            fields.SetField("restdayndotytd", YearToDate.Rows[0][19].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][19].ToString())));//Reg RDNDOT

                            fields.SetField("regularholidayytd", YearToDate.Rows[0][20].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][20].ToString())));//Regular Holiday
                            fields.SetField("regularholidayndotytd", YearToDate.Rows[0][50].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][50].ToString())));//Regular Holiday Night Diff
                            fields.SetField("regularholidayotytd", YearToDate.Rows[0][21].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][21].ToString())));//Regular Holiday OT

                            fields.SetField("specialholidayytd", YearToDate.Rows[0][22].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][22].ToString())));//Special Holiday
                            fields.SetField("specialholidayndotytd", YearToDate.Rows[0][51].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][51].ToString())));//Special Holiday Night Diff
                            fields.SetField("specialholidayotytd", YearToDate.Rows[0][23].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][23].ToString())));//Special Holiday OT

                            fields.SetField("adjabsenceytd", YearToDate.Rows[0][56].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][56].ToString())));//Double Holiday
                            fields.SetField("adjlateytd", YearToDate.Rows[0][58].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][58].ToString())));//Double Holiday Night Diff
                            fields.SetField("adjrdndotytd", YearToDate.Rows[0][60].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][60].ToString())));//Double Holiday OT

                            fields.SetField("adjregularhoursytd", YearToDate.Rows[0][26].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][26].ToString())));//Double Holiday
                            fields.SetField("adjregularndotytd", YearToDate.Rows[0][27].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][27].ToString())));//Double Holiday Night Diff
                            fields.SetField("adjregularotytd", YearToDate.Rows[0][28].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][28].ToString())));//Double Holiday OT
                            fields.SetField("adjrestdayotytd", YearToDate.Rows[0][29].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][29].ToString())));//Double Holiday OT

                            fields.SetField("grosspayytd", YearToDate.Rows[0][30].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][30].ToString())));//Basic Salary
                            fields.SetField("deductionssssytd", YearToDate.Rows[0][31].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][31].ToString())));//Basic Salary
                            fields.SetField("deductionsphilhealthytd", YearToDate.Rows[0][32].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][32].ToString())));//Basic Salary
                            fields.SetField("deductionspagibigytd", YearToDate.Rows[0][33].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][33].ToString())));//Basic Salary
                            fields.SetField("taxableincomeytd", YearToDate.Rows[0][34].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][34].ToString())));//Basic Salary
                            fields.SetField("deductionswtaxytd", YearToDate.Rows[0][35].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][35].ToString())));//Basic Salary
                            fields.SetField("deductionsplaytd", YearToDate.Rows[0][36].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][36].ToString())));//Basic Salary
                            fields.SetField("deductionsprulifeytd", YearToDate.Rows[0][37].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][37].ToString())));//Basic Salary
                            fields.SetField("deductionssdytd", YearToDate.Rows[0][38].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][38].ToString())));//Basic Salary
                            fields.SetField("deductionssssloanamountytd", YearToDate.Rows[0][39].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][39].ToString())));//Basic Salary
                            fields.SetField("deductionspagibigloanamountytd", YearToDate.Rows[0][40].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][40].ToString())));//Basic Salary
                            fields.SetField("ecolaytd", YearToDate.Rows[0][41].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][41].ToString())));//Basic Salary
                            fields.SetField("deminimisytd", YearToDate.Rows[0][42].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][42].ToString())));//Basic Salary
                            fields.SetField("adjnontaxableallowanceytd", YearToDate.Rows[0][54].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][54].ToString())));//Basic Salary
                            fields.SetField("13thmonthnontaxableytd", YearToDate.Rows[0][43].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][43].ToString())));//Basic Salary
                            fields.SetField("netpayytd", YearToDate.Rows[0][44].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][44].ToString())));//Basic Salary
                            fields.SetField("sssemployershareytd", YearToDate.Rows[0][45].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][45].ToString())));//Basic Salary
                            fields.SetField("sssemployerecshareytd", YearToDate.Rows[0][46].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][46].ToString())));//Basic Salary
                            fields.SetField("philhealthemployershareytd", YearToDate.Rows[0][47].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][47].ToString())));//Basic Salary
                            fields.SetField("pagibigemployershareytd", YearToDate.Rows[0][48].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][48].ToString())));//Basic Salary
                        }
                        else
                        {

                        }
                    }
                    #endregion
                    //-- New End

                    //foreach (var field in fields.Fields)
                    //{
                    //    Console.WriteLine("{0}, {1}", field.Key, field.Value);

                    //    string sample2 = field.Key.ToString();
                    //    //TextBox2.Text = sample + field.Key.ToString() + ',';
                    //    fields.SetField(sample2, sample2);
                    //}


                    //------------------------------------------------

                    // form flattening rids the form of editable text fields so
                    // the final output can't be edited
                    stamper.FormFlattening = true;
                    // closing the stamper

                    stamper.Close();



                    //viewpdf.Attributes.Add("src", "pdf\\PaySlipDefault-" + empid + "-" + hoursdatefrom + "-" + hoursdateto + ".pdf");
                    //Response.Redirect("~/pdf/PaySlip-I101.pdf");
                    #endregion
                }
                #endregion
                return filename;
            }
            catch (Exception e)
            {
                string xEmpID = uProfile.NTID;
                return e.ToString();
            }
        }

        [HttpPost]
        [Route("GenerateandSendPayslip")]
        public string GenerateandSendPayslip(sendPayv2 param)
        {
            try
            {
                GetDatabase GDB = new GetDatabase();
                GDB.ClientName = param.CN;
                SelfieRegistration2Controller.GetDB2(GDB);
                Connection Connection = new Connection();
                DataTable DT = Connection.GetDataTable("sp_getEmails");
                foreach (DataRow row in DT.Rows)
                {
                    UserProfilev2 uProfile = new UserProfilev2();
                    uProfile.CN = param.CN;
                    uProfile.PayDay = param.PayDay;
                    uProfile.PayYear = param.PayYear;
                    //uProfile.PayDay = "2018-08-16-2018-08-31";
                    //uProfile.PayYear = "2018";
                    uProfile.NTID = row["empid"].ToString();
                    GeneratePayslipEncrypted(uProfile);
                    //string emailaccount = "payroll.noreply@illimitado.com";
                    //string emailpassword = "LimitLess!1@";
                    //string loopemail = row["email"].ToString();
                    //string prefix = HttpContext.Current.Server.MapPath("~/pdf/");
                    //using (MailMessage mm = new MailMessage(emailaccount, loopemail))
                    //{
                    //    MailAddress aliasmail = new MailAddress("payroll.noreply@illimitado.com", "payroll.noreply@illimitado.com");
                    //    MailAddress aliasreplymail = new MailAddress("payroll.noreply@illimitado.com");
                    //    mm.From = aliasmail;
                    //    mm.Subject = "Payslip";
                    //    mm.Body = "";
                    //    Connection con = new Connection();
                    //    con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = row["empid"].ToString() });
                    //    DataTable dt2 = con.GetDataTable("sp_GetPaySlipRecordsEmpMaster");
                    //    string name = dt2.Rows[0][6].ToString() + "_" + dt2.Rows[0][5].ToString() + "_" + param.PayDay;
                    //    mm.Attachments.Add(new Attachment(prefix + name + ".pdf"));
                    //    mm.IsBodyHtml = false;
                    //    SmtpClient smtp = new SmtpClient();
                    //    smtp.Host = "box1256.bluehost.com";
                    //    smtp.EnableSsl = true;
                    //    NetworkCredential NetworkCred = new NetworkCredential(emailaccount, emailpassword);
                    //    smtp.UseDefaultCredentials = true;
                    //    smtp.Credentials = NetworkCred;
                    //    smtp.Port = 587;
                    //    smtp.Send(mm);
                    //}
                }
                return "success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }
        public string GeneratePayslipEncrypted(UserProfilev2 uProfile)
        {
            try
            {
                GetDatabase GDB = new GetDatabase();
                GDB.ClientName = uProfile.CN;
                SelfieRegistration2Controller.GetDB2(GDB);


                string DDLMonths = "08";
                string DDLPayPeriod = uProfile.PayDay;
                string DDLYear = uProfile.PayYear;
                empid = uProfile.NTID;

                empname = PayMtd.searchempname(empid);

                empidprof = empid;
                //GetEmpLevelandCategory = PayMtd.GetEmpLevelandCategory(empid);
                //string EmpLevel = GetEmpLevelandCategory.Rows[0][0].ToString();
                //string EmpCategory = GetEmpLevelandCategory.Rows[0][1].ToString();

                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = empid });
                paysliprecordsempmaster = con.GetDataTable("sp_GetPaySlipRecordsEmpMaster");

                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = empid });
                paysliprecordsempmaster = con.GetDataTable("sp_GetPaySlipRecordsEmpMaster");

                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = empid });
                paysliprecordsconfidentialinfo = con.GetDataTable("sp_GetPaySlipRecordsConfidentialInfo");
                //paysliprecordsconfidentialinfo = PayMtd.PaySlipRecordsConfidentialInfo(empid);

                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = empid });
                paysliprecordsemployeepay = con.GetDataTable("sp_GetPaySlipRecordsEmployeePay");
                //paysliprecordsemployeepay = PayMtd.PaySlipRecordsEmployeePay(empid);
                //int schmct = PayMtd.GetSalarySchemes(EmpLevel, EmpCategory).Rows.Count;

                #region "New Payslip"
                TotalValue = 0;
                int latestpayslipcounter = 0;

                con = new Connection();
                LatestPayslipForm = con.GetDataTable("sp_GetLatestPayslipForm");
                //LatestPayslipForm = PayMtd.LatestPayslipForm();
                //latestpayslipcounter = LatestPayslipForm.Rows.Count;
                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = empid });
                con.myparameters.Add(new myParameters { ParameterName = "@MONTH", mytype = SqlDbType.NVarChar, Value = DDLMonths.ToString() });
                DataTable BonusPayoutMonths = con.GetDataTable("sp_GetBonusPayouts");
                //DataTable BonusPayoutMonths = PayMtd.BonusPayoutMonths(empid, DDLMonths.ToString());
                if (latestpayslipcounter == 1)
                {
                    #region Retrieve Neccessary Data
                    con = new Connection();
                    con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = empid });
                    GetEmpLevelandCategory = con.GetDataTable("sp_SearchMasterControl_LeaveManagement_GetEmpLevelandCategory");

                    //GetEmpLevelandCategory = PayMtd.GetEmpLevelandCategory(empid);
                    int psrpsd = 0;
                    int pswh = 0;
                    //paysliprecordspayperiod = PayMtd.PaySlipRecordsPayPeriod(EmpLevel, EmpCategory);                
                    payperiod = hoursdatefrom + '-' + hoursdateto;

                    con = new Connection();
                    con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = empid });
                    con.myparameters.Add(new myParameters { ParameterName = "@PAYPERIOD", mytype = SqlDbType.NVarChar, Value = DDLPayPeriod });
                    paysliprecordspayslipdetails = con.GetDataTable("sp_GetPaySlipRecordsPaySlipDetails");

                    con = new Connection();
                    con.myparameters.Add(new myParameters { ParameterName = "@empId", mytype = SqlDbType.NVarChar, Value = empid });
                    DataTable DTProfile = con.GetDataTable("sp_getCompleteProfile_Mobilev2");
                    //paysliprecordspayslipdetails = PayMtd.PaySlipRecordsPaySlipDetails(empid, DDLPayPeriod);
                    #endregion
                    string pathmoto;
                    //if (uProfile.CN.ToLower() == "newcore")
                    //{
                    //    pathmoto = "PaySlipDefaultNewcore";
                    //}
                    //else
                    //{
                    //    pathmoto = "PaySlipDefault";
                    //}
                    pathmoto = "PaySlipDefault";
                    //LatestPayslipForm.Rows[0][0].ToString();
                    PdfReader reader1 = new PdfReader(HttpContext.Current.Server.MapPath("~/pdf/") + pathmoto + ".pdf");
                    LocationTextExtractionStrategy lteStrategy = new LocationTextExtractionStrategy();
                    string strExact = PdfTextExtractor.GetTextFromPage(reader1, 1, lteStrategy);
                    StringBuilder sb = new StringBuilder();
                    //this code used for replacing the found text
                    sb.Append(PdfTextExtractor.GetTextFromPage(reader1, 1));
                    #region Passing Retrieved Datas
                    sb.Replace("[Employee ID]", empid);
                    if (paysliprecordsempmaster.Rows.Count > 0)
                    {
                        sb.Replace("[Employee Name]", paysliprecordsempmaster.Rows[0][0].ToString() == "" ? "N/A" : paysliprecordsempmaster.Rows[0][0].ToString());
                        sb.Replace("[Date Joined]", paysliprecordsempmaster.Rows[0][2].ToString() == "" ? "N/A" : paysliprecordsempmaster.Rows[0][2].ToString());
                        sb.Replace("[Resigned]", paysliprecordsempmaster.Rows[0][3].ToString() == "" ? "N/A" : paysliprecordsempmaster.Rows[0][3].ToString());
                    }
                    else
                    {
                        sb.Replace("[Employee Name]", "N/A");
                        sb.Replace("[Date Joined]", "N/A");
                        sb.Replace("[Resigned]", "N/A");
                    }
                    if (paysliprecordsconfidentialinfo.Rows.Count > 0)
                    {
                        sb.Replace("[HDMF]", paysliprecordsconfidentialinfo.Rows[0][0].ToString() == "" ? "N/A" : paysliprecordsconfidentialinfo.Rows[0][0].ToString());
                        sb.Replace("[PhilHealth No]", paysliprecordsconfidentialinfo.Rows[0][1].ToString() == "" ? "N/A" : paysliprecordsconfidentialinfo.Rows[0][1].ToString());
                        sb.Replace("[SSS No]", paysliprecordsconfidentialinfo.Rows[0][2].ToString() == "" ? "N/A" : paysliprecordsconfidentialinfo.Rows[0][2].ToString());
                        sb.Replace("[TIN]", paysliprecordsconfidentialinfo.Rows[0][3].ToString() == "" ? "N/A" : paysliprecordsconfidentialinfo.Rows[0][3].ToString());
                        sb.Replace("[Tax Category]", paysliprecordsconfidentialinfo.Rows[0][4].ToString() == "" ? "N/A" : paysliprecordsconfidentialinfo.Rows[0][4].ToString());
                    }
                    else
                    {
                        sb.Replace("[HDMF]", "N/A");
                        sb.Replace("[PhilHealth No]", "N/A");
                        sb.Replace("[SSS No]", "N/A");
                        sb.Replace("[TIN]", "N/A");
                        sb.Replace("[Tax Category]", "N/A");
                    }
                    if (paysliprecordsemployeepay.Rows.Count > 0)
                    {

                        sb.Replace("[Bank Account]", paysliprecordsemployeepay.Rows[0][0].ToString() == "" ? "N/A" : paysliprecordsemployeepay.Rows[0][0].ToString());
                        sb.Replace("[Hourly Rate]", paysliprecordsemployeepay.Rows[0][1].ToString() == "" ? "0.00" : paysliprecordsemployeepay.Rows[0][1].ToString());
                    }
                    else
                    {
                        sb.Replace("[Bank Account]", "N/A");
                        sb.Replace("[Hourly Rate]", "N/A");
                    }
                    sb.Replace("[Pay Period]", DDLPayPeriod == "" ? "N/A" : DDLPayPeriod);
                    if (paysliprecordspayslipdetails.Rows.Count > 0)
                    {
                        sb.Replace("[Pay Day]", paysliprecordspayslipdetails.Rows[0][1].ToString() == "" ? "N/A" : paysliprecordspayslipdetails.Rows[0][1].ToString());
                        sb.Replace("[Basic Salary]", paysliprecordspayslipdetails.Rows[0][2].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][2].ToString())));
                        sb.Replace("[Non Taxable Allowance]", paysliprecordspayslipdetails.Rows[0][3].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][3].ToString())));
                        sb.Replace("[Regular Night Differential]", paysliprecordspayslipdetails.Rows[0][4].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][4].ToString())));
                        sb.Replace("[Regular Over Time]", paysliprecordspayslipdetails.Rows[0][5].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][5].ToString())));
                        sb.Replace("[Regular Holiday]", paysliprecordspayslipdetails.Rows[0][6].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][6].ToString())));
                        sb.Replace("[Regular Holiday Night Differential]", paysliprecordspayslipdetails.Rows[0][7].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][7].ToString())));
                        sb.Replace("[Regular Holiday Over Time]", paysliprecordspayslipdetails.Rows[0][8].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][8].ToString())));
                        sb.Replace("[Paid Time Off]", paysliprecordspayslipdetails.Rows[0][9].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][9].ToString())));
                        sb.Replace("[Special Holiday]", paysliprecordspayslipdetails.Rows[0][10].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][10].ToString())));
                        sb.Replace("[Special Holiday Night Differential]", paysliprecordspayslipdetails.Rows[0][11].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][11].ToString())));
                        sb.Replace("[Special Holiday Over Time]", paysliprecordspayslipdetails.Rows[0][12].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][12].ToString())));
                        sb.Replace("[Allowance]", paysliprecordspayslipdetails.Rows[0][13].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][13].ToString())));
                        sb.Replace("[Bonuses]", paysliprecordspayslipdetails.Rows[0][14].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][14].ToString())));
                        sb.Replace("[Adjustments]", paysliprecordspayslipdetails.Rows[0][15].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][15].ToString())));
                        sb.Replace("[Total Salary]", paysliprecordspayslipdetails.Rows[0][16].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][16].ToString())));
                        sb.Replace("[Employee HDMF]", paysliprecordspayslipdetails.Rows[0][17].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][17].ToString())));
                        sb.Replace("[Employee Philhealth]", paysliprecordspayslipdetails.Rows[0][18].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][18].ToString())));
                        sb.Replace("[Employee SSS]", paysliprecordspayslipdetails.Rows[0][19].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][19].ToString())));
                        sb.Replace("[Employee WithHoldingTax]", paysliprecordspayslipdetails.Rows[0][20].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][20].ToString())));
                        sb.Replace("[SSS Loan Deduction]", paysliprecordspayslipdetails.Rows[0][21].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][21].ToString())));
                        sb.Replace("[Pag-ibig Loan Deduction]", paysliprecordspayslipdetails.Rows[0][22].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][22].ToString())));
                        sb.Replace("[Net Pay]", paysliprecordspayslipdetails.Rows[0][23].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][23].ToString())));
                        sb.Replace("[Employer HDMF]", paysliprecordspayslipdetails.Rows[0][24].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][24].ToString())));
                        sb.Replace("[Employer Philhealth]", paysliprecordspayslipdetails.Rows[0][25].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][25].ToString())));
                        sb.Replace("[Employer SSS]", paysliprecordspayslipdetails.Rows[0][26].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][26].ToString())));
                        sb.Replace("[Employer SSS EC]", paysliprecordspayslipdetails.Rows[0][27].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][27].ToString())));
                    }
                    else
                    {
                        sb.Replace("[Pay Day]", "N/A");
                        sb.Replace("[Basic Salary]", "N/A");
                        sb.Replace("[Non Taxable Allowance]", "N/A");
                        sb.Replace("[Regular Night Differential]", "N/A");
                        sb.Replace("[Regular Over Time]", "N/A");
                        sb.Replace("[Regular Holiday]", "N/A");
                        sb.Replace("[Regular Holiday Night Differential]", "N/A");
                        sb.Replace("[Regular Holiday Over Time]", "N/A");
                        sb.Replace("[Paid Time Off]", "N/A");
                        sb.Replace("[Special Holiday]", "N/A");
                        sb.Replace("[Special Holiday Night Differential]", "N/A");
                        sb.Replace("[Special Holiday Over Time]", "N/A");
                        sb.Replace("[Allowance]", "N/A");
                        sb.Replace("[Bonuses]", "N/A");
                        sb.Replace("[Adjustments]", "N/A");
                        sb.Replace("[Total Salary]", "N/A");
                        sb.Replace("[Employee HDMF]", "N/A");
                        sb.Replace("[Employee Philhealth]", "N/A");
                        sb.Replace("[Employee SSS]", "N/A");
                        sb.Replace("[Employee WithHoldingTax]", "N/A");
                        sb.Replace("[SSS Loan Deduction]", "N/A");
                        sb.Replace("[Pag-ibig Loan Deduction]", "N/A");
                        sb.Replace("[Net Pay]", "N/A");
                        sb.Replace("[Employer HDMF]", "N/A");
                        sb.Replace("[Employer Philhealth]", "N/A");
                        sb.Replace("[Employer SSS]", "N/A");
                        sb.Replace("[Employer SSS EC]", "N/A");
                    }

                    if (DTProfile.Rows.Count > 0)
                    {
                        sb.Replace("[Salary]", DTProfile.Rows[0]["Salary"].ToString() == "" ? "0.00" : string.Format("{0:n}", Convert.ToDouble(DTProfile.Rows[0]["Salary"].ToString())));
                    }
                    else
                    {
                        sb.Replace("[Salary]", "N/A");
                    }

                    #endregion

                    FileStream Newstream = new FileStream(string.Concat(HttpContext.Current.Server.MapPath("~/pdf/"), empid + pathmoto + ".pdf"), FileMode.Create);
                    MemoryStream ms = new MemoryStream();
                    Document document = new Document(PageSize.A4, 25, 25, 30, 30);
                    //Code for adding the string builder sb to the document
                    PdfWriter writer = PdfWriter.GetInstance(document, Newstream);
                    document.Open();
                    document.Add(new Paragraph(sb.ToString()));
                    document.Close();
                    writer.Close();
                }
                else
                {
                    #region Fixed payslip

                    int psrpsd = 0;
                    int pswh = 0;


                    //paysliprecordspayslipdetails = PayMtd.PaySlipRecordsPaySlipDetails(empid, DDLPayPeriod);



                    con = new Connection();
                    con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = empid });
                    con.myparameters.Add(new myParameters { ParameterName = "@YEAR", mytype = SqlDbType.NVarChar, Value = DDLYear });
                    con.myparameters.Add(new myParameters { ParameterName = "@PAYPERIOD", mytype = SqlDbType.NVarChar, Value = DDLPayPeriod });
                    YearToDate = con.GetDataTable("sp_GetPaySlipRecordsPaySlipDetailsYearToDate");

                    con = new Connection();
                    con.myparameters.Add(new myParameters { ParameterName = "@empId", mytype = SqlDbType.NVarChar, Value = empid });
                    DataTable DTProfile = con.GetDataTable("sp_getCompleteProfile_Mobilev2");

                    //YearToDate = PayMtd.PayslipDetailsYearToDate(empid, DDLYear, DDLPayPeriod);

                    //Response.Redirect("~/pdf/BIR19021.pdf"); // view pdf
                    string path = "https://ws.durusthr.com/ILM_WS_Live/pdf";

                    //if (uProfile.CN.ToLower() == "newcore")
                    //{
                    //    pdfpath = "/PaySlipDefaultNewcore.pdf";
                    //}
                    //else
                    //{
                    //    pdfpath = "/PaySlipDefault.pdf";
                    //}
                    var pdfReader1 = new PdfReader(path + "/PaySlipDefault.pdf");

                    AcroFields af = pdfReader1.AcroFields;
                    //LocationTextExtractionStrategy lteStrategy = new LocationTextExtractionStrategy();
                    //string strExact = PdfTextExtractor.GetTextFromPage(pdfReader1,1, lteStrategy);
                    //StringBuilder sb = new StringBuilder();
                    string sam = "";
                    foreach (var field in af.Fields)
                    {
                        sam = sam + field.Key + "-"; // names of textfields
                        //AcroFields.Item item;
                        ////Center Text//
                        //item = af.GetFieldItem(field.Key);
                        //item.GetMerged(0).Put(PdfName.Q, new PdfNumber(PdfFormField.Q_RIGHT));

                    }
                    //viewpdf.InnerText = sam;
                    // get the file paths


                    // Template file path

                    //if (uProfile.CN.ToLower() == "newcore")
                    //{
                    //    formFile = path + "/PaySlipDefaultNewcore.pdf";
                    //}
                    //else
                    //{
                    //    formFile = path + "/PaySlipDefault.pdf";
                    //}

                    var lnameencrypt = paysliprecordsempmaster.Rows[0][4].ToString();
                    var lname = paysliprecordsempmaster.Rows[0][5].ToString();
                    var fname = paysliprecordsempmaster.Rows[0][6].ToString();
                    string pdfpass1 = "", pdfpass2 = "";
                    string formFile = "/PaySlipDefault.pdf";
                    string path2 = HttpContext.Current.Server.MapPath("pdf");
                    // Output file path
                    //string newFile = path2 + "/PaySlip-" + empid + ".pdf";
                    string newFile = path2 + "/" + fname + "_" + lname + "_" + DDLPayPeriod + ".pdf";

                    // read the template file
                    PdfReader reader;
                    if (uProfile.CN.ToLower() == "newcore")
                    {
                        reader = new PdfReader(path2 + "/PaySlipDefaultNewcore.pdf");
                    }
                    else if (uProfile.CN.ToLower() == "generali_dev")
                    {
                        reader = new PdfReader(path2 + "/PaySlipDefaultGenerali.pdf");
                    }
                    else if (uProfile.CN.ToLower() == "generali")
                    {
                        reader = new PdfReader(path2 + "/PaySlipDefaultGenerali.pdf");
                    }
                    else
                    {
                        reader = new PdfReader(path2 + "/PaySlipDefault.pdf");
                    }




                    // instantiate PDFStamper object
                    // The Output file will be created from template file and edited by the PDFStamper
                    PdfStamper stamper = new PdfStamper(reader, new FileStream(newFile, FileMode.Create));

                    Connection Connection = new Connection();
                    Connection.myparameters.Add(new myParameters { ParameterName = "@empid", mytype = SqlDbType.NVarChar, Value = empid });
                    DataTable DT = Connection.GetDataTable("sp_getEmailsbyEmpID");
                    if (DT.Rows.Count > 0)
                    {
                        pdfpass1 = DT.Rows[0][2].ToString();
                        pdfpass2 = DT.Rows[0][2].ToString();
                    }
                    else
                    {
                        pdfpass1 = lnameencrypt.ToLower();
                        pdfpass2 = lname;
                    }

                    //stamper.SetEncryption(true, pdfpass1, pdfpass2, PdfWriter.ALLOW_SCREENREADERS);

                    //stamper.SetEncryption(true, empid, "ilm", PdfWriter.ALLOW_SCREENREADERS);
                    //PdfEncryptor.Encrypt(reader, new FileStream(newFile, FileMode.Open), true, empid, "secret", PdfWriter.ALLOW_SCREENREADERS);
                    // Object to deal with the Output file's textfields
                    AcroFields fields = stamper.AcroFields;

                    // set form fields("Field Name in PDF", "String to be placed in this PDF text field");
                    //for (int i = 0; i <= 1000; i++)
                    //{
                    //    string ii = i.ToString();
                    //    
                    //    fields.SetField("untitled" + i, "vt" + ii); //temporary values
                    //}

                    //------------------------------------------------
                    //loop to display all field names
                    if (paysliprecordsempmaster.Rows.Count > 0)
                    {
                        //fields.SetFieldProperty("txtempid", "setflags", PdfAnnotation.FLAGS_HIDDEN,null);
                        fields.SetField("untitled2", paysliprecordsempmaster.Rows[0][0].ToString());//Employee Name
                        fields.SetField("employeeno", empid);//Employee No
                        fields.SetField("joined", paysliprecordsempmaster.Rows[0][2].ToString());//Joined
                        fields.SetField("resigned", paysliprecordsempmaster.Rows[0][3].ToString());//Resigned
                    }
                    else
                    {

                    }
                    if (paysliprecordsconfidentialinfo.Rows.Count > 0)
                    {
                        fields.SetField("pagibigno", paysliprecordsconfidentialinfo.Rows[0][0].ToString());//Pag-IBIG No
                        fields.SetField("philhealthno", paysliprecordsconfidentialinfo.Rows[0][1].ToString());//PhilHealth No
                        fields.SetField("sssno", paysliprecordsconfidentialinfo.Rows[0][2].ToString());//SSS No
                        fields.SetField("taxno", paysliprecordsconfidentialinfo.Rows[0][3].ToString());//Tax No
                        fields.SetField("taxcategory", paysliprecordsconfidentialinfo.Rows[0][4].ToString());//Tax Category
                    }
                    else
                    {

                    }
                    if (paysliprecordsemployeepay.Rows.Count > 0)
                    {
                        fields.SetField("bankaccount", paysliprecordsemployeepay.Rows[0][0].ToString());//Bank Account
                        fields.SetField("hourlyrate", paysliprecordsemployeepay.Rows[0][1].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordsemployeepay.Rows[0][1].ToString())));//Hourly Rate
                    }
                    else
                    {
                        fields.SetField("hourlyrate", "0.00");
                    }
                    fields.SetField("payperiod", DDLPayPeriod);//Pay Period

                    con = new Connection();
                    con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = empid });
                    con.myparameters.Add(new myParameters { ParameterName = "@PAYPERIOD", mytype = SqlDbType.NVarChar, Value = DDLPayPeriod });
                    if (uProfile.CN.ToLower() == "newcore")
                    {
                        paysliprecordspayslipdetails = con.GetDataTable("sp_GetPaySlipRecordsPaySlipDetailsNewcore");
                        if (paysliprecordspayslipdetails.Rows.Count > 0)
                        {
                            fields.SetField("payday", paysliprecordspayslipdetails.Rows[0][0].ToString());//Pay Day
                            fields.SetField("regularothrs", paysliprecordspayslipdetails.Rows[0][1].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][1].ToString())));//Basic Salary
                            fields.SetField("regularndothrs", paysliprecordspayslipdetails.Rows[0][2].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][2].ToString())));//Basic Salary
                            fields.SetField("restdayndothrs", paysliprecordspayslipdetails.Rows[0][3].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][3].ToString())));//Basic Salary
                            fields.SetField("restdayothrs", paysliprecordspayslipdetails.Rows[0][52].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][52].ToString())));//Basic Salary
                            fields.SetField("regularholidayhrs", paysliprecordspayslipdetails.Rows[0][4].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][4].ToString())));//Basic Salary
                            fields.SetField("regularholidayothrs", paysliprecordspayslipdetails.Rows[0][5].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][5].ToString())));//Basic Salary
                            fields.SetField("regularholidayndothrs", paysliprecordspayslipdetails.Rows[0][53].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][53].ToString())));//Basic Salary
                            fields.SetField("specialholidayhrs", paysliprecordspayslipdetails.Rows[0][6].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][6].ToString())));//Basic Salary
                            fields.SetField("specialholidayothrs", paysliprecordspayslipdetails.Rows[0][7].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][7].ToString())));//Basic Salary
                            fields.SetField("specialholidayndothrs", paysliprecordspayslipdetails.Rows[0][54].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][54].ToString())));//Basic Salary
                            fields.SetField("adjabsencehrs", paysliprecordspayslipdetails.Rows[0][59].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][59].ToString())));//Basic Salary
                            fields.SetField("adjlatehrs", paysliprecordspayslipdetails.Rows[0][61].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][61].ToString())));//Basic Salary
                            fields.SetField("adjrdndothrs", paysliprecordspayslipdetails.Rows[0][63].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][63].ToString())));//Basic Salary
                            fields.SetField("adjregularhrs", paysliprecordspayslipdetails.Rows[0][10].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][10].ToString())));//Basic Salary
                            fields.SetField("adjregularndothrs", paysliprecordspayslipdetails.Rows[0][11].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][11].ToString())));//Basic Salary
                            fields.SetField("adjregularothrs", paysliprecordspayslipdetails.Rows[0][12].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][12].ToString())));//Basic Salary
                            fields.SetField("adjrestdayothrs", paysliprecordspayslipdetails.Rows[0][13].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][13].ToString())));//Basic Salary
                            fields.SetField("basicsalarycurrent", paysliprecordspayslipdetails.Rows[0][14].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][14].ToString())));//Basic Salary
                            fields.SetField("deductionsabsencescurrent", paysliprecordspayslipdetails.Rows[0][15].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][15].ToString())));//Basic Salary
                            fields.SetField("deductionstardinesscurrent", paysliprecordspayslipdetails.Rows[0][16].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][16].ToString())));//Basic Salary
                            fields.SetField("13thmonthtaxablecurrent", paysliprecordspayslipdetails.Rows[0][17].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][17].ToString())));//Basic Salary
                            fields.SetField("regularotcurrent", paysliprecordspayslipdetails.Rows[0][18].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][18].ToString())));//Basic Salary
                            fields.SetField("regularndotcurrent", paysliprecordspayslipdetails.Rows[0][19].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][19].ToString())));//Basic Salary

                            fields.SetField("restdayotcurrent", paysliprecordspayslipdetails.Rows[0][50].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][50].ToString())));//Basic Salary
                            fields.SetField("restdayndotcurrent", paysliprecordspayslipdetails.Rows[0][20].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][20].ToString())));//Basic Salary
                            fields.SetField("regularholidaycurrent", paysliprecordspayslipdetails.Rows[0][21].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][21].ToString())));//Basic Salary
                            fields.SetField("regularholidayotcurrent", paysliprecordspayslipdetails.Rows[0][22].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][22].ToString())));//Basic Salary
                            fields.SetField("regularholidayndotcurrent", paysliprecordspayslipdetails.Rows[0][51].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][51].ToString())));//Basic Salary
                            fields.SetField("specialholidaycurrent", paysliprecordspayslipdetails.Rows[0][23].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][23].ToString())));//Basic Salary
                            fields.SetField("specialholidayotcurrent", paysliprecordspayslipdetails.Rows[0][24].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][24].ToString())));//Basic Salary
                            fields.SetField("specialholidayndotcurrent", paysliprecordspayslipdetails.Rows[0][55].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][55].ToString())));//Basic Salary
                            fields.SetField("adjabsencecurrent", paysliprecordspayslipdetails.Rows[0][60].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][60].ToString())));//Basic Salary
                            fields.SetField("adjlatecurrent", paysliprecordspayslipdetails.Rows[0][62].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][62].ToString())));//Basic Salary
                            fields.SetField("adjrdndotcurrent", paysliprecordspayslipdetails.Rows[0][64].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][64].ToString())));//Basic Salary
                            fields.SetField("adjregularhourscurrent", paysliprecordspayslipdetails.Rows[0][27].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][27].ToString())));//Basic Salary
                            fields.SetField("adjregularndotcurrent", paysliprecordspayslipdetails.Rows[0][28].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][28].ToString())));//Basic Salary
                            fields.SetField("adjregularotcurrent", paysliprecordspayslipdetails.Rows[0][29].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][29].ToString())));//Basic Salary
                            fields.SetField("adjrestdayotcurrent", paysliprecordspayslipdetails.Rows[0][30].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][30].ToString())));//Basic Salary
                            fields.SetField("grosspaycurrent", paysliprecordspayslipdetails.Rows[0][31].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][31].ToString())));//Basic Salary
                            fields.SetField("deductionsssscurrent", paysliprecordspayslipdetails.Rows[0][32].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][32].ToString())));//Basic Salary
                            fields.SetField("deductionsphilhealthcurrent", paysliprecordspayslipdetails.Rows[0][33].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][33].ToString())));//Basic Salary
                            fields.SetField("deductionspagibigcurrent", paysliprecordspayslipdetails.Rows[0][34].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][34].ToString())));//Basic Salary
                            fields.SetField("taxableincomecurrent", paysliprecordspayslipdetails.Rows[0][35].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][35].ToString())));//Basic Salary
                            fields.SetField("deductionswtaxcurrent", paysliprecordspayslipdetails.Rows[0][36].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][36].ToString())));//Basic Salary
                            fields.SetField("deductionsplacurrent", paysliprecordspayslipdetails.Rows[0][37].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][37].ToString())));//Basic Salary
                            fields.SetField("deductionsprulifecurrent", paysliprecordspayslipdetails.Rows[0][38].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][38].ToString())));//Basic Salary
                            fields.SetField("deductionssdcurrent", paysliprecordspayslipdetails.Rows[0][39].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][39].ToString())));//Basic Salary
                            fields.SetField("deductionssssloanamountcurrent", paysliprecordspayslipdetails.Rows[0][40].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][40].ToString())));//Basic Salary
                            fields.SetField("deductionspagibigloanamountcurrent", paysliprecordspayslipdetails.Rows[0][41].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][41].ToString())));//Basic Salary
                            fields.SetField("ecolacurrent", paysliprecordspayslipdetails.Rows[0][42].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][42].ToString())));//Basic Salary
                            fields.SetField("deminimiscurrent", paysliprecordspayslipdetails.Rows[0][43].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][43].ToString())));//Basic Salary
                            fields.SetField("adjnontaxableallowancecurrent", paysliprecordspayslipdetails.Rows[0][58].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][58].ToString())));//Basic Salary
                            fields.SetField("13thmonthnontaxablecurrent", paysliprecordspayslipdetails.Rows[0][44].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][44].ToString())));//Basic Salary
                            fields.SetField("netpaycurrent", paysliprecordspayslipdetails.Rows[0][45].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][45].ToString())));//Basic Salary
                            fields.SetField("sssemployersharecurrent", paysliprecordspayslipdetails.Rows[0][46].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][46].ToString())));//Basic Salary
                            fields.SetField("sssemployerecsharecurrent", paysliprecordspayslipdetails.Rows[0][47].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][47].ToString())));//Basic Salary
                            fields.SetField("philhealthemployersharecurrent", paysliprecordspayslipdetails.Rows[0][48].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][48].ToString())));//Basic Salary
                            fields.SetField("pagibigemployersharecurrent", paysliprecordspayslipdetails.Rows[0][49].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][49].ToString())));//Basic Salary
                            fields.SetField("restdayhrs", paysliprecordspayslipdetails.Rows[0][65].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][65].ToString())));//Rest Day Hours
                            fields.SetField("restdaycurrent", paysliprecordspayslipdetails.Rows[0][66].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][66].ToString())));//Rest Day Amount
                            fields.SetField("adjrdhrs", "0.00");//ADJ Rest Day Hours
                            fields.SetField("adjrdcurrent", "0.00");//ADJ Rest Day Amount
                        }
                        else
                        {

                        }
                    }
                    else
                    {
                        paysliprecordspayslipdetails = con.GetDataTable("sp_GetPaySlipRecordsPaySlipDetails");
                        if (paysliprecordspayslipdetails.Rows.Count > 0)
                        {
                            fields.SetField("payday", paysliprecordspayslipdetails.Rows[0][1].ToString());//Pay Day
                            fields.SetField("basicsalarycurrent", paysliprecordspayslipdetails.Rows[0][2].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][2].ToString())));//Basic Salary
                            fields.SetField("nontaxableallowancecurrent", paysliprecordspayslipdetails.Rows[0][3].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][3].ToString())));//Non Taxable Allowance
                            fields.SetField("regularnightdiffcurrent", paysliprecordspayslipdetails.Rows[0][4].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][4].ToString())));//Regular Night Diff
                            fields.SetField("regularotcurrent", paysliprecordspayslipdetails.Rows[0][5].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][5].ToString())));//Regular Overtime
                            fields.SetField("regularholidaycurrent", paysliprecordspayslipdetails.Rows[0][6].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][6].ToString())));//Regular Holiday
                            fields.SetField("regularholidaynightdiffcurrent", paysliprecordspayslipdetails.Rows[0][7].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][7].ToString())));//Regular Holiday Night Diff
                            fields.SetField("regularholidayotcurrent", paysliprecordspayslipdetails.Rows[0][8].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][8].ToString())));//Regular Overtime
                            fields.SetField("paidtimeoffcurrent", paysliprecordspayslipdetails.Rows[0][9].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][9].ToString())));//Paid Time Off
                            fields.SetField("specialholidaycurrent", paysliprecordspayslipdetails.Rows[0][10].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][10].ToString())));//Special Holiday
                            fields.SetField("specialholidaynightdiffcurrent", paysliprecordspayslipdetails.Rows[0][11].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][11].ToString())));//Special Holiday Night Diff
                            fields.SetField("specialholidayotcurrent", paysliprecordspayslipdetails.Rows[0][12].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][12].ToString())));//Special Holiday OT
                            fields.SetField("allowancescurrent", paysliprecordspayslipdetails.Rows[0][13].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][13].ToString())));//Allowances
                            fields.SetField("bonuscurrent", paysliprecordspayslipdetails.Rows[0][14].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][14].ToString())));//Bonus
                            fields.SetField("adjustmentscurrent", paysliprecordspayslipdetails.Rows[0][15].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][15].ToString())));//adjustments
                            fields.SetField("totalcurrent", paysliprecordspayslipdetails.Rows[0][16].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][16].ToString())));//Gross Pay
                            fields.SetField("employeehdmfcurrent", paysliprecordspayslipdetails.Rows[0][17].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][17].ToString())));//Employee HDMF
                            fields.SetField("employeephilhealthcurrent", paysliprecordspayslipdetails.Rows[0][18].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][18].ToString())));//Employee PhilHealth
                            fields.SetField("employeesocialsecuritycurrent", paysliprecordspayslipdetails.Rows[0][19].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][19].ToString())));//Employee SSS
                            fields.SetField("withholdingtaxcurrent", paysliprecordspayslipdetails.Rows[0][20].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][20].ToString())));//Employee WithHoldingTax
                            fields.SetField("sssloandeductioncurrent", paysliprecordspayslipdetails.Rows[0][21].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][21].ToString())));//Loans SSS
                            fields.SetField("pagibigloandeductioncurrent", paysliprecordspayslipdetails.Rows[0][22].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][22].ToString())));//Loans Pagibig
                            fields.SetField("netpaycurrent", paysliprecordspayslipdetails.Rows[0][23].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][23].ToString())));//Net Pay
                            fields.SetField("employerhdmfcurrent", paysliprecordspayslipdetails.Rows[0][24].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][24].ToString())));//Employer HDMF
                            fields.SetField("employerphilhealthcurrent", paysliprecordspayslipdetails.Rows[0][25].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][25].ToString())));//Employer PhilHealth
                            fields.SetField("employersocialsecuritycurrent", paysliprecordspayslipdetails.Rows[0][26].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][26].ToString())));//Employer SSS
                            fields.SetField("employersocialsecurityeccurrent", paysliprecordspayslipdetails.Rows[0][27].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][27].ToString())));//Employer SSS EC
                            fields.SetField("deductionsabsencescurrent", paysliprecordspayslipdetails.Rows[0][28].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][28].ToString())));//Deductions Absences
                            fields.SetField("deductionslatescurrent", paysliprecordspayslipdetails.Rows[0][29].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][29].ToString())));//Deductions Lates
                            fields.SetField("doubleholidaycurrent", paysliprecordspayslipdetails.Rows[0][30].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][30].ToString())));//Double Holiday
                            fields.SetField("doubleholidayndcurrent", paysliprecordspayslipdetails.Rows[0][31].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][31].ToString())));//Double Holiday ND
                            fields.SetField("doubleholidayotcurrent", paysliprecordspayslipdetails.Rows[0][32].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][32].ToString())));//Double Holiday OT
                        }
                        else
                        {

                        }
                    }

                    if (uProfile.CN.ToLower() == "newcore")
                    {
                        con = new Connection();
                        con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = empid });
                        con.myparameters.Add(new myParameters { ParameterName = "@PAYPERIOD", mytype = SqlDbType.NVarChar, Value = DDLPayPeriod });
                        con.myparameters.Add(new myParameters { ParameterName = "@YEAR", mytype = SqlDbType.NVarChar, Value = uProfile.PayYear });
                        YearToDate = con.GetDataTable("sp_GetPaySlipRecordsPaySlipDetailsYearToDateNewcore");
                        if (YearToDate.Rows.Count > 0)
                        {
                            //fields.SetField("payday", YearToDate.Rows[0][0].ToString());//Pay Day
                            //fields.SetField("regularothrs", YearToDate.Rows[0][1].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][1].ToString())));//Basic Salary
                            //fields.SetField("regularndothrs", YearToDate.Rows[0][2].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][2].ToString())));//Basic Salary
                            //fields.SetField("restdayndothrs", YearToDate.Rows[0][3].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][3].ToString())));//Basic Salary
                            //fields.SetField("regularholidayhrs", YearToDate.Rows[0][4].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][4].ToString())));//Basic Salary
                            //fields.SetField("regularholidayothrs", YearToDate.Rows[0][5].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][5].ToString())));//Basic Salary
                            //fields.SetField("specialholidayhrs", YearToDate.Rows[0][6].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][6].ToString())));//Basic Salary
                            //fields.SetField("specialholidayothrs", YearToDate.Rows[0][7].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][7].ToString())));//Basic Salary
                            //fields.SetField("doubleholidayhrs", YearToDate.Rows[0][8].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][8].ToString())));//Basic Salary
                            //fields.SetField("doubleholidayothrs", YearToDate.Rows[0][9].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][9].ToString())));//Basic Salary
                            //fields.SetField("adjregularhrs", YearToDate.Rows[0][10].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][10].ToString())));//Basic Salary
                            //fields.SetField("adjregularndothrs", YearToDate.Rows[0][11].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][11].ToString())));//Basic Salary
                            //fields.SetField("adjregularothrs", YearToDate.Rows[0][12].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][12].ToString())));//Basic Salary
                            //fields.SetField("adjrestdayothrs", YearToDate.Rows[0][13].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][13].ToString())));//Basic Salary
                            fields.SetField("basicsalaryytd", YearToDate.Rows[0][13].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][13].ToString())));//Basic Salary
                            fields.SetField("deductionsabsencesytd", YearToDate.Rows[0][14].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][14].ToString())));//Deduction absences
                            fields.SetField("deductionstardinessytd", YearToDate.Rows[0][15].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][15].ToString())));//Deduction lates
                            fields.SetField("13thmonthtaxableytd", YearToDate.Rows[0][16].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][16].ToString())));//13 month

                            fields.SetField("regularotytd", YearToDate.Rows[0][17].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][17].ToString())));//Regular OT
                            fields.SetField("regularndotytd", YearToDate.Rows[0][18].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][18].ToString())));//Regular Night Diff

                            fields.SetField("restdayotytd", YearToDate.Rows[0][52].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][52].ToString())));//Reg RDOT
                            fields.SetField("restdayndotytd", YearToDate.Rows[0][19].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][19].ToString())));//Reg RDNDOT

                            fields.SetField("regularholidayytd", YearToDate.Rows[0][20].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][20].ToString())));//Regular Holiday
                            fields.SetField("regularholidayndotytd", YearToDate.Rows[0][50].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][50].ToString())));//Regular Holiday Night Diff
                            fields.SetField("regularholidayotytd", YearToDate.Rows[0][21].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][21].ToString())));//Regular Holiday OT

                            fields.SetField("specialholidayytd", YearToDate.Rows[0][22].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][22].ToString())));//Special Holiday
                            fields.SetField("specialholidayndotytd", YearToDate.Rows[0][51].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][51].ToString())));//Special Holiday Night Diff
                            fields.SetField("specialholidayotytd", YearToDate.Rows[0][23].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][23].ToString())));//Special Holiday OT

                            fields.SetField("adjabsenceytd", YearToDate.Rows[0][56].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][56].ToString())));//Double Holiday
                            fields.SetField("adjlateytd", YearToDate.Rows[0][58].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][58].ToString())));//Double Holiday Night Diff
                            fields.SetField("adjrdndotytd", YearToDate.Rows[0][60].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][60].ToString())));//Double Holiday OT

                            fields.SetField("adjregularhoursytd", YearToDate.Rows[0][26].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][26].ToString())));//Double Holiday
                            fields.SetField("adjregularndotytd", YearToDate.Rows[0][27].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][27].ToString())));//Double Holiday Night Diff
                            fields.SetField("adjregularotytd", YearToDate.Rows[0][28].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][28].ToString())));//Double Holiday OT
                            fields.SetField("adjrestdayotytd", YearToDate.Rows[0][29].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][29].ToString())));//Double Holiday OT

                            fields.SetField("grosspayytd", YearToDate.Rows[0][30].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][30].ToString())));//Basic Salary
                            fields.SetField("deductionssssytd", YearToDate.Rows[0][31].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][31].ToString())));//Basic Salary
                            fields.SetField("deductionsphilhealthytd", YearToDate.Rows[0][32].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][32].ToString())));//Basic Salary
                            fields.SetField("deductionspagibigytd", YearToDate.Rows[0][33].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][33].ToString())));//Basic Salary
                            fields.SetField("taxableincomeytd", YearToDate.Rows[0][34].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][34].ToString())));//Basic Salary
                            fields.SetField("deductionswtaxytd", YearToDate.Rows[0][35].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][35].ToString())));//Basic Salary
                            fields.SetField("deductionsplaytd", YearToDate.Rows[0][36].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][36].ToString())));//Basic Salary
                            fields.SetField("deductionsprulifeytd", YearToDate.Rows[0][37].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][37].ToString())));//Basic Salary
                            fields.SetField("deductionssdytd", YearToDate.Rows[0][38].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][38].ToString())));//Basic Salary
                            fields.SetField("deductionssssloanamountytd", YearToDate.Rows[0][39].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][39].ToString())));//Basic Salary
                            fields.SetField("deductionspagibigloanamountytd", YearToDate.Rows[0][40].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][40].ToString())));//Basic Salary
                            fields.SetField("ecolaytd", YearToDate.Rows[0][41].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][41].ToString())));//Basic Salary
                            fields.SetField("deminimisytd", YearToDate.Rows[0][42].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][42].ToString())));//Basic Salary
                            fields.SetField("adjnontaxableallowanceytd", YearToDate.Rows[0][54].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][54].ToString())));//Basic Salary
                            fields.SetField("13thmonthnontaxableytd", YearToDate.Rows[0][43].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][43].ToString())));//Basic Salary
                            fields.SetField("netpayytd", YearToDate.Rows[0][44].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][44].ToString())));//Basic Salary
                            fields.SetField("sssemployershareytd", YearToDate.Rows[0][45].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][45].ToString())));//Basic Salary
                            fields.SetField("sssemployerecshareytd", YearToDate.Rows[0][46].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][46].ToString())));//Basic Salary
                            fields.SetField("philhealthemployershareytd", YearToDate.Rows[0][47].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][47].ToString())));//Basic Salary
                            fields.SetField("pagibigemployershareytd", YearToDate.Rows[0][48].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][48].ToString())));//Basic Salary
                            fields.SetField("restdayytd", YearToDate.Rows[0][62].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][62].ToString())));//Rest Day Amount Year to Date
                            fields.SetField("adjrdytd", "0.00");//ADJ Rest Day Amount Year to Date
                        }
                        else
                        {

                        }
                    }
                    else
                    {
                        if (YearToDate.Rows.Count > 0)
                        {
                            fields.SetField("basicsalaryytd", YearToDate.Rows[0][0].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][0].ToString())));//Basic Salary
                            fields.SetField("nontaxableallowanceytd", YearToDate.Rows[0][1].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][1].ToString())));//Non Taxable Allowance
                            fields.SetField("regularnightdiffytd", YearToDate.Rows[0][2].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][2].ToString())));//Regular Night Diff
                            fields.SetField("regularotytd", YearToDate.Rows[0][3].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][3].ToString())));//Regular OT
                            fields.SetField("regularholidayytd", YearToDate.Rows[0][4].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][4].ToString())));//Regular Holiday
                            fields.SetField("regularholidaynightdiffytd", YearToDate.Rows[0][5].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][5].ToString())));//Regular Holiday Night Diff
                            fields.SetField("regularholidayotytd", YearToDate.Rows[0][6].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][6].ToString())));//Regular Holiday OT
                            fields.SetField("paidtimeoffytd", YearToDate.Rows[0][7].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][7].ToString())));//Paid Time Off
                            fields.SetField("specialholidayytd", YearToDate.Rows[0][8].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][8].ToString())));//Special Holiday
                            fields.SetField("specialholidaynightdiffytd", YearToDate.Rows[0][9].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][9].ToString())));//Special Holiday Night Diff
                            fields.SetField("specialholidayotytd", YearToDate.Rows[0][10].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][10].ToString())));//Special Holiday OT
                            fields.SetField("allowancesytd", YearToDate.Rows[0][11].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][11].ToString())));//Allowances
                            fields.SetField("bonusytd", YearToDate.Rows[0][12].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][12].ToString())));//Bonus
                            fields.SetField("adjustmentsytd", YearToDate.Rows[0][13].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][13].ToString())));//Adjustments
                            fields.SetField("totalytd", YearToDate.Rows[0][14].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][14].ToString())));//Gross Pay/Total
                            fields.SetField("employeehdmfytd", YearToDate.Rows[0][15].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][15].ToString())));//Employee HDMF
                            fields.SetField("employeephilhealthytd", YearToDate.Rows[0][16].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][16].ToString())));//Employee PhilHealth
                            fields.SetField("employeesocialsecurityytd", YearToDate.Rows[0][17].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][17].ToString())));//Employee Social Security
                            fields.SetField("withholdingtaxytd", YearToDate.Rows[0][18].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][18].ToString())));//WithHolding Tax
                            fields.SetField("sssloandeductionytd", YearToDate.Rows[0][19].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][19].ToString())));//SSS Loan Deduction
                            fields.SetField("pagibigloandeductionytd", YearToDate.Rows[0][20].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][20].ToString())));//Pag-ibig Loan Deduction
                            fields.SetField("netpayytd", YearToDate.Rows[0][21].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][21].ToString())));//Net Pay
                            fields.SetField("employerhdmfytd", YearToDate.Rows[0][22].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][22].ToString())));//Employer HDMF
                            fields.SetField("employerphilhealthytd", YearToDate.Rows[0][23].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][23].ToString())));//Employer PhilHealth
                            fields.SetField("employersocialsecurityytd", YearToDate.Rows[0][24].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][24].ToString())));//Employer Social Security
                            fields.SetField("employersocialsecurityecytd", YearToDate.Rows[0][25].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][25].ToString())));//Employer Social Security EC
                            fields.SetField("deductionsabsencesytd", YearToDate.Rows[0][26].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][26].ToString())));//Deductions Absences
                            fields.SetField("deductionslatesytd", YearToDate.Rows[0][27].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][27].ToString())));//Deductions Lates
                            fields.SetField("doubleholidayytd", YearToDate.Rows[0][28].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][28].ToString())));//DH_REG_Amount
                            fields.SetField("doubleholidayndytd", YearToDate.Rows[0][29].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][29].ToString())));//DH_ND_Amount
                            fields.SetField("doubleholidayotytd", YearToDate.Rows[0][30].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][30].ToString())));//DH_OT_Amount
                        }
                        else
                        {

                        }
                    }

                    if (DTProfile.Rows.Count > 0)
                    {
                        fields.SetField("monthlyrate", DTProfile.Rows[0]["Salary"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(DTProfile.Rows[0]["Salary"].ToString())));
                    }
                    else
                    {

                    }
                    //foreach (var field in af.Fields)
                    //{
                    //    Console.WriteLine("{0}, {1}", field.Key, field.Value);
                    //    //string sample = TextBox2.Text;
                    //    string sample2 = field.Key.ToString();
                    //    //TextBox2.Text = sample + field.Key.ToString() + ',';
                    //    fields.SetField(field.Key.ToString(), field.ToString());
                    //}


                    //------------------------------------------------

                    // form flattening rids the form of editable text fields so
                    // the final output can't be edited
                    stamper.FormFlattening = true;
                    // closing the stamper

                    stamper.Close();



                    //viewpdf.Attributes.Add("src", "pdf\\PaySlipDefault-" + empid + "-" + hoursdatefrom + "-" + hoursdateto + ".pdf");
                    //Response.Redirect("~/pdf/PaySlip-I101.pdf");
                    #endregion
                }
                #endregion
                return "Success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }



        [HttpPost]
        [Route("GeneratePayslipGenerali")]
        public string GeneratePayslipGenerali(UserProfilev2 uProfile)
        {
            try
            {
                Connection con;

                GetDatabase GDB = new GetDatabase();
                GDB.ClientName = uProfile.CN;
                SelfieRegistration2Controller.GetDB2(GDB);

                string DDLPayPeriod = uProfile.PayDay;
                string DDLYear = uProfile.PayYear;
                empid = uProfile.NTID;

                //empname = PayMtd.searchempname(empid);


                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = empid });
                paysliprecordsempmaster = con.GetDataTable("sp_GetPaySlipRecordsEmpMaster");

                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = empid });
                paysliprecordsconfidentialinfo = con.GetDataTable("sp_GetPaySlipRecordsConfidentialInfo");

                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = empid });
                paysliprecordsemployeepay = con.GetDataTable("sp_GetPaySlipRecordsEmployeePay");

                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = empid });
                con.myparameters.Add(new myParameters { ParameterName = "@PAYPERIOD", mytype = SqlDbType.NVarChar, Value = DDLPayPeriod });
                DataTable currentdate = con.GetDataTable("sp_GetPaySlipRecordsPaySlipDetailsGenerali");

                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = empid });
                con.myparameters.Add(new myParameters { ParameterName = "@PAYPERIOD", mytype = SqlDbType.NVarChar, Value = currentdate.Rows[0][0].ToString() });
                con.myparameters.Add(new myParameters { ParameterName = "@YEAR", mytype = SqlDbType.NVarChar, Value = DDLYear });
                YearToDate = con.GetDataTable("sp_GetPaySlipRecordsPaySlipDetailsYearToDateGenerali");


                //string path2 = "https://ws.durusthr.com/ILM_WS_Live/pdf";
                //var pdfReader1 = new PdfReader(path + "/PayslipDefaultGenerali_Staff.pdf");

                //AcroFields af = pdfReader1.AcroFields;

                //string sam = "";
                //foreach (var field in af.Fields)
                //{
                //    sam = sam + field.Key + "-"; // names of textfields
                //}
                string path = "https://ws.durusthr.com/ILM_WS_Live/pdf";
                //string path = HttpContext.Current.Server.MapPath("pdf");
                //if (uProfile.CN.ToLower() == "newcore")
                //{
                //    pdfpath = "/PaySlipDefaultNewcore.pdf";
                //}
                //else
                //{
                //    pdfpath = "/PaySlipDefault.pdf";
                //}
                //viewpdf.InnerText = sam;
                // get the file paths


                // Template file path

                //if (uProfile.CN.ToLower() == "newcore")
                //{
                //    formFile = path + "/PaySlipDefaultNewcore.pdf";
                //}
                //else
                //{
                //    formFile = path + "/PaySlipDefault.pdf";
                //}
                // string formFile = "/PaySlipDefault.pdf";
                string path2 = HttpContext.Current.Server.MapPath("pdf");
                // Output file path
                // string newFile = path2 + "/PaySlip-" + empid + ".pdf";

                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = empid });
                DataTable EmpCatLevel = con.GetDataTable("sp_SearchMasterControl_LeaveManagement_GetEmpLevelandCategory");

                string emplevel = EmpCatLevel.Rows[0][0].ToString();



                var lnameencrypt = paysliprecordsempmaster.Rows[0][4].ToString();
                var lname = paysliprecordsempmaster.Rows[0][5].ToString();
                var fname = paysliprecordsempmaster.Rows[0][6].ToString();

                //string path2 = HttpContext.Current.Server.MapPath("pdf");
                // Output file path
                //string newFile = path2 + "/" + empid + "-" + DDLPayPeriod + ".pdf";
                string newFile = path2 + "/PaySlip-" + empid + ".pdf";
                PdfReader reader;
                if (emplevel == "SEVP" || emplevel == "VP" || emplevel == "SAVP" || emplevel == "AVP" || emplevel == "Administrator" || emplevel == "President" || emplevel == "Consultant")
                    reader = new PdfReader(path + "/PayslipDefaultGenerali_Avp.pdf");
                else
                    reader = new PdfReader(path + "/PayslipDefaultGenerali_Staff.pdf");
                //var pdfReader1 = new PdfReader(path + "/PaySlipDefault.pdf");

                string pdfpass1 = "", pdfpass2 = "";
                // instantiate PDFStamper object
                // The Output file will be created from template file and edited by the PDFStamper
                PdfStamper stamper = new PdfStamper(reader, new FileStream(newFile, FileMode.Create));

                stamper.SetEncryption(true, pdfpass1, pdfpass2, PdfWriter.ALLOW_SCREENREADERS);

                AcroFields fields = stamper.AcroFields;


                if (paysliprecordsempmaster.Rows.Count > 0)
                {
                    //fields.SetFieldProperty("txtempid", "setflags", PdfAnnotation.FLAGS_HIDDEN,null);
                    fields.SetField("employeeno", empid);//Employee No 
                    fields.SetField("employeename", paysliprecordsempmaster.Rows[0][0].ToString());//Employee Name
                    fields.SetField("employeenumber", paysliprecordsempmaster.Rows[0][0].ToString());
                    fields.SetField("joined", paysliprecordsempmaster.Rows[0][2].ToString());//Joined
                    fields.SetField("resigned", paysliprecordsempmaster.Rows[0][3].ToString());//Resigned
                    fields.SetField("hourlyrate", paysliprecordsempmaster.Rows[0][7].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordsempmaster.Rows[0][7].ToString())));//Hourly Rate
                    fields.SetField("monthlyrate", paysliprecordsempmaster.Rows[0][8].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordsempmaster.Rows[0][8].ToString())));//Monthly Rate
                }
                else
                {

                }

                if (paysliprecordsconfidentialinfo.Rows.Count > 0)
                {
                    fields.SetField("pagibigno", paysliprecordsconfidentialinfo.Rows[0][0].ToString());//Pag-IBIG No
                    fields.SetField("philhealthno", paysliprecordsconfidentialinfo.Rows[0][1].ToString());//PhilHealth No
                    fields.SetField("sssno", paysliprecordsconfidentialinfo.Rows[0][2].ToString());//SSS No
                    fields.SetField("taxno", paysliprecordsconfidentialinfo.Rows[0][3].ToString());//Tax No
                    fields.SetField("taxcategory", paysliprecordsconfidentialinfo.Rows[0][4].ToString());//Tax Category
                }
                else
                {

                }
                if (paysliprecordsemployeepay.Rows.Count > 0)
                {
                    fields.SetField("bankaccount", paysliprecordsemployeepay.Rows[0][0].ToString());//Bank Account
                    fields.SetField("hourlyrate", paysliprecordsemployeepay.Rows[0][1].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordsemployeepay.Rows[0][1].ToString())));//Hourly Rate
                    fields.SetField("monthlyrate", paysliprecordsemployeepay.Rows[0][0].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordsemployeepay.Rows[0][2].ToString())));//Monthly Rate
                }
                else
                {
                    fields.SetField("hourlyrate", "0.00");
                }
                fields.SetField("payperiod", DDLPayPeriod);//Pay Period


                if (emplevel.Contains("Staff"))
                {
                    //-- New Start
                    #region New PDF

                    if (currentdate.Rows.Count > 0)
                    {

                        #region New Custom Generali PDF
                        fields.SetField("payday", currentdate.Rows[0]["pay_day"].ToString());//Pay Day
                        fields.SetField("regularothrs", currentdate.Rows[0]["Regular OT Hours"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(currentdate.Rows[0]["Regular OT Hours"].ToString())));//Basic Salary
                        fields.SetField("regularndothrs", currentdate.Rows[0]["Regular ND OT Hours"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(currentdate.Rows[0]["Regular ND OT Hours"].ToString())));//Basic Salary
                        fields.SetField("restdayndothrs", currentdate.Rows[0]["Rest Day ND OT Hours"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(currentdate.Rows[0]["Rest Day ND OT Hours"].ToString())));//Basic Salary
                        fields.SetField("restdayothrs", currentdate.Rows[0]["RestDayOT"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(currentdate.Rows[0]["RestDayOT"].ToString())));//Basic Salary
                        fields.SetField("regularholidayhrs", currentdate.Rows[0]["Regular Holiday Hours"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(currentdate.Rows[0]["Regular Holiday Hours"].ToString())));//Basic Salary
                        fields.SetField("regularholidayothrs", currentdate.Rows[0]["Regular Holiday OT Hours"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(currentdate.Rows[0]["Regular Holiday OT Hours"].ToString())));//Basic Salary
                        fields.SetField("regularholidayndothrs", currentdate.Rows[0]["RH_ND_Hrs"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(currentdate.Rows[0]["RH_ND_Hrs"].ToString())));//Basic Salary
                        fields.SetField("specialholidayhrs", currentdate.Rows[0]["Special Holiday Hours"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(currentdate.Rows[0]["Special Holiday Hours"].ToString())));//Basic Salary
                        fields.SetField("specialholidayothrs", currentdate.Rows[0]["Special Holiday OT Hours"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(currentdate.Rows[0]["Special Holiday OT Hours"].ToString())));//Basic Salary
                        fields.SetField("specialholidayndothrs", currentdate.Rows[0]["SH_ND_Hrs"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(currentdate.Rows[0]["SH_ND_Hrs"].ToString())));//Basic Salary
                        fields.SetField("adjabsencehrs", currentdate.Rows[0]["adj_Absent_Hrs"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(currentdate.Rows[0]["adj_Absent_Hrs"].ToString())));//Basic Salary
                        fields.SetField("adjlatehrs", currentdate.Rows[0]["Adj_Late_Hrs"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(currentdate.Rows[0]["Adj_Late_Hrs"].ToString())));//Basic Salary
                        fields.SetField("adjrdndothrs", currentdate.Rows[0]["Adj_RDNDOT_Hrs"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(currentdate.Rows[0]["Adj_RDNDOT_Hrs"].ToString())));//Basic Salary
                        fields.SetField("adjregularhrs", currentdate.Rows[0]["Adj Regular Hours"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(currentdate.Rows[0]["Adj Regular Hours"].ToString())));//Basic Salary
                        fields.SetField("adjregularndothrs", currentdate.Rows[0]["Adj Regular ND OT"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(currentdate.Rows[0]["Adj Regular ND OT"].ToString())));//Basic Salary
                        fields.SetField("adjregularothrs", currentdate.Rows[0]["Adj Regular OT"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(currentdate.Rows[0]["Adj Regular OT"].ToString())));//Basic Salary
                        fields.SetField("adjrestdayothrs", currentdate.Rows[0]["Adj Rest Day OT"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(currentdate.Rows[0]["Adj Rest Day OT"].ToString())));//Basic Salary
                        fields.SetField("basicsalarycurrent", currentdate.Rows[0]["Basic Salary"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(currentdate.Rows[0]["Basic Salary"].ToString())));//Basic Salary
                                                                                                                                                                                                                        //fields.SetField("deductionabsenceshrs", currentdate.Rows[0]["Absences"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(currentdate.Rows[0]["Absences"].ToString())));//Basic Salary
                        fields.SetField("stitaxablecurrent", currentdate.Rows[0]["STI Taxable"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(currentdate.Rows[0]["STI Taxable"].ToString())));//Basic Salary
                        fields.SetField("deductionabsencescurrent", currentdate.Rows[0]["Deductions Absences"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(currentdate.Rows[0]["Deductions Absences"].ToString())));//Basic Salary
                        fields.SetField("deductiontardinesscurrent", currentdate.Rows[0]["Deductions Tardiness"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(currentdate.Rows[0]["Deductions Tardiness"].ToString())));//Basic Salary
                        fields.SetField("13monthtaxablecurrent", currentdate.Rows[0]["13th Month Taxable"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(currentdate.Rows[0]["13th Month Taxable"].ToString())));//Basic Salary
                        fields.SetField("regularotcurrent", currentdate.Rows[0]["Regular OT Amount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(currentdate.Rows[0]["Regular OT Amount"].ToString())));//Basic Salary
                        fields.SetField("regularndotcurrent", currentdate.Rows[0]["Regular ND OT Amount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(currentdate.Rows[0]["Regular ND OT Amount"].ToString())));//Basic Salary

                        fields.SetField("restdayotcurrent", currentdate.Rows[0]["RestDayOT_Amount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(currentdate.Rows[0]["RestDayOT_Amount"].ToString())));//Basic Salary
                        fields.SetField("restdayndotcurrent", currentdate.Rows[0]["Rest Day ND OT Amount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(currentdate.Rows[0]["Rest Day ND OT Amount"].ToString())));//Basic Salary
                        fields.SetField("regularholidaycurrent", currentdate.Rows[0]["Regular Holiday Amount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(currentdate.Rows[0]["Regular Holiday Amount"].ToString())));//Basic Salary
                        fields.SetField("regularholidayotcurrent", currentdate.Rows[0]["Regular Holiday OT Amount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(currentdate.Rows[0]["Regular Holiday OT Amount"].ToString())));//Basic Salary
                        fields.SetField("regularholidayndotcurrent", currentdate.Rows[0]["RH_ND_Amount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(currentdate.Rows[0]["RH_ND_Amount"].ToString())));//Basic Salary
                        fields.SetField("specialholidaycurrent", currentdate.Rows[0]["Special Holiday Amount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(currentdate.Rows[0]["Special Holiday Amount"].ToString())));//Basic Salary
                        fields.SetField("specialholidayotcurrent", currentdate.Rows[0]["Special Holiday OT Amount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(currentdate.Rows[0]["Special Holiday OT Amount"].ToString())));//Basic Salary
                        fields.SetField("specialholidayndotcurrent", currentdate.Rows[0]["SH_ND_Amount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(currentdate.Rows[0]["SH_ND_Amount"].ToString())));//Basic Salary
                        fields.SetField("adjabsencecurrent", currentdate.Rows[0]["Adj_Absent_Amount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(currentdate.Rows[0]["Adj_Absent_Amount"].ToString())));//Basic Salary
                        fields.SetField("adjlatecurrent", currentdate.Rows[0]["Adj_Late_Amount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(currentdate.Rows[0]["Adj_Late_Amount"].ToString())));//Basic Salary
                        fields.SetField("adjrdndotcurrent", currentdate.Rows[0]["Adj_RDNDOT_Amount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(currentdate.Rows[0]["Adj_RDNDOT_Amount"].ToString())));//Basic Salary
                        fields.SetField("adjregularhourscurrent", currentdate.Rows[0]["Adj Regular Hours Amount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(currentdate.Rows[0]["Adj Regular Hours Amount"].ToString())));//Basic Salary
                        fields.SetField("adjregularndotcurrent", currentdate.Rows[0]["Adj Regular ND OT Amount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(currentdate.Rows[0]["Adj Regular ND OT Amount"].ToString())));//Basic Salary
                        fields.SetField("adjregularotcurrent", currentdate.Rows[0]["Adj Regular OT Amount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(currentdate.Rows[0]["Adj Regular OT Amount"].ToString())));//Basic Salary
                        fields.SetField("adjrestdayotcurrent", currentdate.Rows[0]["Adj Rest Day OT Amount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(currentdate.Rows[0]["Adj Rest Day OT Amount"].ToString())));//Basic Salary
                        fields.SetField("grosspaycurrent", currentdate.Rows[0]["Gross Pay"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(currentdate.Rows[0]["Gross Pay"].ToString())));//Basic Salary
                        fields.SetField("deductionssscurrent", currentdate.Rows[0]["Employee SSS"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(currentdate.Rows[0]["Employee SSS"].ToString())));//Basic Salary
                        fields.SetField("deductionphilhealthcurrent", currentdate.Rows[0]["Employee PhilHealth"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(currentdate.Rows[0]["Employee PhilHealth"].ToString())));//Basic Salary
                        fields.SetField("deductionpagibigcurrent", currentdate.Rows[0]["Employee Pagibig"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(currentdate.Rows[0]["Employee Pagibig"].ToString())));//Basic Salary
                        fields.SetField("taxableincomecurrent", currentdate.Rows[0]["Taxable Income"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(currentdate.Rows[0]["Taxable Income"].ToString())));//Basic Salary
                        fields.SetField("deductionwtaxcurrent", currentdate.Rows[0]["Tax"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(currentdate.Rows[0]["Tax"].ToString())));//Basic Salary
                        fields.SetField("deductionsplacurrent", currentdate.Rows[0]["PLA"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(currentdate.Rows[0]["PLA"].ToString())));//Basic Salary
                        fields.SetField("deductionsprulifecurrent", currentdate.Rows[0]["Pru Life"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(currentdate.Rows[0]["Pru Life"].ToString())));//Basic Salary
                        fields.SetField("deductionssdcurrent", currentdate.Rows[0]["SD"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(currentdate.Rows[0]["SD"].ToString())));//Basic Salary
                        fields.SetField("deductionsssloanamountcurrent", currentdate.Rows[0]["SSS Loan Amount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(currentdate.Rows[0]["SSS Loan Amount"].ToString())));//Basic Salary
                        fields.SetField("deductionpagibigloanamountcurrent", currentdate.Rows[0]["Pagibig Loan Amount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(currentdate.Rows[0]["Pagibig Loan Amount"].ToString())));//Basic Salary
                        fields.SetField("stinontaxablecurrent", currentdate.Rows[0]["OtherBen"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(currentdate.Rows[0]["OtherBen"].ToString())));//Basic Salary
                        fields.SetField("stifringecurrent", currentdate.Rows[0]["De Minimis"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(currentdate.Rows[0]["De Minimis"].ToString())));//Basic Salary
                        fields.SetField("nontaxableallowancecurrent", currentdate.Rows[0]["Adj_NonTaxable_Allowance"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(currentdate.Rows[0]["Adj_NonTaxable_Allowance"].ToString())));//Basic Salary
                        fields.SetField("13thmonthnontaxablecurrent", currentdate.Rows[0]["13th Month Non-Taxable"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(currentdate.Rows[0]["13th Month Non-Taxable"].ToString())));//Basic Salary
                        fields.SetField("netpaycurrent", currentdate.Rows[0]["Net Pay"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(currentdate.Rows[0]["Net Pay"].ToString())));//Basic Salary
                        fields.SetField("sssemployersharecurrent", currentdate.Rows[0]["SSS Employer Share"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(currentdate.Rows[0]["SSS Employer Share"].ToString())));//Basic Salary
                        fields.SetField("sssemployerecsharecurrent", currentdate.Rows[0]["SSS Employer EC Share"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(currentdate.Rows[0]["SSS Employer EC Share"].ToString())));//Basic Salary
                        fields.SetField("philhealthemployersharecurrent", currentdate.Rows[0]["PhilHealth Employer Share"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(currentdate.Rows[0]["PhilHealth Employer Share"].ToString())));//Basic Salary
                        fields.SetField("pagibigemployersharecurrent", currentdate.Rows[0]["Pag-ibig Employer Share"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(currentdate.Rows[0]["Pag-ibig Employer Share"].ToString())));//Basic Salary

                        fields.SetField("taxableconvertedleavescurrent", currentdate.Rows[0]["ConvertedLeavesTaxable_Amount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(currentdate.Rows[0]["ConvertedLeavesTaxable_Amount"].ToString())));//Taxable Converted Leaves
                        fields.SetField("carallowancecurrent", currentdate.Rows[0]["car_allowance"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(currentdate.Rows[0]["car_allowance"].ToString())));//Car Allowance
                        fields.SetField("14thmonthtaxablecurrent", currentdate.Rows[0]["Taxable_14thMonth"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(currentdate.Rows[0]["Taxable_14thMonth"].ToString())));//14th Month Taxable
                        fields.SetField("14thmonthfringecurrent", currentdate.Rows[0]["AdjFringe_NonTaxable"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(currentdate.Rows[0]["AdjFringe_NonTaxable"].ToString())));//14th Month Taxable
                        fields.SetField("deductionundertimecurrent", currentdate.Rows[0]["undertime"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(currentdate.Rows[0]["undertime"].ToString())));//Undertime
                        fields.SetField("deductionlwopcurrent", currentdate.Rows[0]["LWOP_amt"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(currentdate.Rows[0]["LWOP_amt"].ToString())));//LWOP
                        fields.SetField("pagibigemployersharecurrent", currentdate.Rows[0]["Pag-ibig Employer Share"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(currentdate.Rows[0]["Pag-ibig Employer Share"].ToString())));//14th Month Taxable
                        fields.SetField("13thmonthtaxablecurrent", currentdate.Rows[0]["13th Month Taxable"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(currentdate.Rows[0]["13th Month Taxable"].ToString())));//13th Month Taxable

                        fields.SetField("deductioncarparkingcurrent", currentdate.Rows[0]["Parking"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(currentdate.Rows[0]["Parking"].ToString())));//Car Parking
                        fields.SetField("deductionexcessofbenefitcurrent", currentdate.Rows[0]["EOB"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(currentdate.Rows[0]["EOB"].ToString())));//EOB
                        fields.SetField("deductionmodifiedpagibig2current", currentdate.Rows[0]["PagIBIG_MP2"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(currentdate.Rows[0]["PagIBIG_MP2"].ToString())));//Pagibig MP2
                        fields.SetField("deductionglapimplcurrent", currentdate.Rows[0]["PLA"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(currentdate.Rows[0]["PLA"].ToString())));//GLAPI MPL
                        fields.SetField("deductiontaxpayablecurrent", currentdate.Rows[0]["tax_payable"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(currentdate.Rows[0]["tax_payable"].ToString())));//Tax Payable
                        fields.SetField("taxrefundcurrent", currentdate.Rows[0]["taxrefund"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(currentdate.Rows[0]["taxrefund"].ToString())));//Tax Refund
                        fields.SetField("nontaxableconvertedleavescurrent", currentdate.Rows[0]["ConvertedLeavesNontaxable_Amount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(currentdate.Rows[0]["ConvertedLeavesNontaxable_Amount"].ToString())));//Non Taxable Converted Leaves
                        fields.SetField("uniformallowancecurrent", currentdate.Rows[0]["UniformAllowance"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(currentdate.Rows[0]["UniformAllowance"].ToString())));//Uniform Allowance
                        fields.SetField("medicalallowancecurrent", currentdate.Rows[0]["Medical_Allowance"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(currentdate.Rows[0]["Medical_Allowance"].ToString())));//Medical Allowance
                        fields.SetField("socialrelatedallowancecurrent", currentdate.Rows[0]["SocialRelatedAllowance"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(currentdate.Rows[0]["SocialRelatedAllowance"].ToString())));//Social Related Allowance
                        fields.SetField("mobileallowancecurrent", currentdate.Rows[0]["MobileAllowance"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(currentdate.Rows[0]["MobileAllowance"].ToString())));//Mobile Allowance
                        fields.SetField("dataplancurrent", currentdate.Rows[0]["DataPlan"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(currentdate.Rows[0]["DataPlan"].ToString())));//Data Plan
                        fields.SetField("fringebenefitcurrent", currentdate.Rows[0]["Fringe_NonTaxable"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(currentdate.Rows[0]["Fringe_NonTaxable"].ToString())));//Fringe Benefit
                        fields.SetField("14thmonthnontaxablecurrent", currentdate.Rows[0]["Payable14thMonth"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(currentdate.Rows[0]["Payable14thMonth"].ToString())));//Fringe Benefit
                        #endregion                                                                                                                                                                                ////CURRENT////
                    }

                    if (YearToDate.Rows.Count > 0)
                    {

                        #region Old PDF
                        fields.SetField("basicsalaryytd", YearToDate.Rows[0]["basicsalary"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["basicsalary"].ToString())));//Basic Salary
                        fields.SetField("taxableconvertedleavesytd", YearToDate.Rows[0]["taxableconvertedleaves"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["taxableconvertedleaves"].ToString())));//Taxable Converted Leaves
                        fields.SetField("carallowanceytd", YearToDate.Rows[0]["carallowance"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["carallowance"].ToString())));//Car Allowance
                        fields.SetField("deductionabsencesytd", YearToDate.Rows[0]["deductionsabsences"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["deductionsabsences"].ToString())));//Deduction absences
                        fields.SetField("deductiontardinessytd", YearToDate.Rows[0]["deductionstardiness"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["deductionstardiness"].ToString())));//Deduction lates
                        fields.SetField("13thmonthtaxableytd", YearToDate.Rows[0]["13thmonthtaxable"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["13thmonthtaxable"].ToString())));//13th month
                        fields.SetField("14thmonthtaxableytd", YearToDate.Rows[0]["tax14month"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["tax14month"].ToString())));//14th month
                        fields.SetField("stitaxableytd", YearToDate.Rows[0]["stitaxable"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["stitaxable"].ToString())));//Basic Salary
                        fields.SetField("deductionundertimeytd", YearToDate.Rows[0]["undertime"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["undertime"].ToString())));//Undertime
                        fields.SetField("deductionlwopytd", YearToDate.Rows[0]["lwopdeduction"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["lwopdeduction"].ToString())));//LWOP

                        fields.SetField("regularotytd", YearToDate.Rows[0]["regularotamount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["regularotamount"].ToString())));//Regular OT
                        fields.SetField("regularndotytd", YearToDate.Rows[0]["regularndotamount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["regularndotamount"].ToString())));//Regular Night Diff

                        fields.SetField("restdayotytd", YearToDate.Rows[0]["rdotamount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["rdotamount"].ToString())));//Reg RDOT
                        fields.SetField("restdayndotytd", YearToDate.Rows[0]["restdayndotamount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["restdayndotamount"].ToString())));//Reg RDNDOT

                        fields.SetField("regularholidayytd", YearToDate.Rows[0]["regularholidayamount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["regularholidayamount"].ToString())));//Regular Holiday
                        fields.SetField("regularholidayndotytd", YearToDate.Rows[0]["regularholidayndamount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["regularholidayndamount"].ToString())));//Regular Holiday Night Diff
                        fields.SetField("regularholidayotytd", YearToDate.Rows[0]["regularholidayotamount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["regularholidayotamount"].ToString())));//Regular Holiday OT

                        fields.SetField("specialholidayytd", YearToDate.Rows[0]["specialholidayamount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["specialholidayamount"].ToString())));//Special Holiday
                        fields.SetField("specialholidayndotytd", YearToDate.Rows[0]["specialholidayndamount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["specialholidayndamount"].ToString())));//Special Holiday Night Diff
                        fields.SetField("specialholidayotytd", YearToDate.Rows[0]["specialholidayotamount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["specialholidayotamount"].ToString())));//Special Holiday OT

                        fields.SetField("adjabsenceytd", YearToDate.Rows[0]["adjabsenceamt"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["adjabsenceamt"].ToString())));//Double Holiday
                        fields.SetField("adjlateytd", YearToDate.Rows[0]["adjlateamt"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["adjlateamt"].ToString())));//Double Holiday Night Diff
                        fields.SetField("adjrdndotytd", YearToDate.Rows[0]["adjrdndotamt"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["adjrdndotamt"].ToString())));//Double Holiday OT

                        fields.SetField("adjregularhoursytd", YearToDate.Rows[0]["adjregularhoursamount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["adjregularhoursamount"].ToString())));//Double Holiday
                        fields.SetField("adjregularndotytd", YearToDate.Rows[0]["adjregularndotamount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["adjregularndotamount"].ToString())));//Double Holiday Night Diff
                        fields.SetField("adjregularotytd", YearToDate.Rows[0]["adjregularotamount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["adjregularotamount"].ToString())));//Double Holiday OT
                        fields.SetField("adjrestdayotytd", YearToDate.Rows[0]["adjrestdayotamount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["adjrestdayotamount"].ToString())));//Double Holiday OT

                        fields.SetField("grosspayytd", YearToDate.Rows[0]["grosspay"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["grosspay"].ToString())));//Basic Salary
                        fields.SetField("deductionsssytd", YearToDate.Rows[0]["employeesss"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["employeesss"].ToString())));//Basic Salary
                        fields.SetField("deductionphilhealthytd", YearToDate.Rows[0]["employeephilhealth"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["employeephilhealth"].ToString())));//Basic Salary
                        fields.SetField("deductionpagibigytd", YearToDate.Rows[0]["employeepagibig"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["employeepagibig"].ToString())));//Basic Salary
                        fields.SetField("taxableincomeytd", YearToDate.Rows[0]["taxableincome"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["taxableincome"].ToString())));//Basic Salary
                        fields.SetField("deductionwtaxytd", YearToDate.Rows[0]["tax"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["tax"].ToString())));//Basic Salary
                        fields.SetField("deductioncarparkingytd", YearToDate.Rows[0]["parking"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["parking"].ToString())));//Car Parking
                        fields.SetField("deductionexcessofbenefitytd", YearToDate.Rows[0]["eob"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["eob"].ToString())));//Excess of Benefit
                        fields.SetField("deductionmodifiedpagibig2ytd", YearToDate.Rows[0]["pagibigmp2"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["pagibigmp2"].ToString())));//Pagibig MP2
                        fields.SetField("deductionglapimplytd", YearToDate.Rows[0]["pla"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["pla"].ToString())));//Basic Salary
                        fields.SetField("deductionsprulifeytd", YearToDate.Rows[0]["prulife"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["prulife"].ToString())));//Basic Salary
                        fields.SetField("deductionssdytd", YearToDate.Rows[0]["sd"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["sd"].ToString())));//Basic Salary
                        fields.SetField("deductionsssloanamountytd", YearToDate.Rows[0]["sssloanamount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["sssloanamount"].ToString())));//Basic Salary
                        fields.SetField("deductionpagibigloanamountytd", YearToDate.Rows[0]["pagibigloanamount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["pagibigloanamount"].ToString())));//Basic Salary
                        fields.SetField("deductiontaxpayableytd", YearToDate.Rows[0]["taxpayable"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["taxpayable"].ToString())));//Tax Payable
                        fields.SetField("nontaxableconvertedleavesytd", YearToDate.Rows[0]["nontaxableconvertedleaves"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["nontaxableconvertedleaves"].ToString())));//Non-Taxable Converted Leaves
                        fields.SetField("taxrefundytd", YearToDate.Rows[0]["taxrefund"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["taxrefund"].ToString())));//Tax Refund
                        fields.SetField("stinontaxableytd", YearToDate.Rows[0]["ecola"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["ecola"].ToString())));//Basic Salary
                        fields.SetField("stifringeytd", YearToDate.Rows[0]["deminimis"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["deminimis"].ToString())));//Basic Salary
                        fields.SetField("uniformallowanceytd", YearToDate.Rows[0]["uniformallowance"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["uniformallowance"].ToString())));//Uniform Allowance
                        fields.SetField("medicalallowanceytd", YearToDate.Rows[0]["medicalallowance"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["medicalallowance"].ToString())));//Medical Allowance
                        fields.SetField("socialrelatedallowanceytd", YearToDate.Rows[0]["socialrelatedallowance"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["socialrelatedallowance"].ToString())));//Social Related Allowance
                        fields.SetField("mobileallowanceytd", YearToDate.Rows[0]["mobileallowance"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["mobileallowance"].ToString())));//Mobile Allowance
                        fields.SetField("dataplanytd", YearToDate.Rows[0]["dataplan"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["dataplan"].ToString())));//Data Plan
                        fields.SetField("fringebenefitytd", YearToDate.Rows[0]["fringe"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["fringe"].ToString())));//Fringe
                        fields.SetField("nontaxableallowanceytd", YearToDate.Rows[0]["adjnontaxableallowance"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["adjnontaxableallowance"].ToString())));//Basic Salary
                        fields.SetField("13thmonthnontaxableytd", YearToDate.Rows[0]["13thmonthnontaxable"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["13thmonthnontaxable"].ToString())));//Basic Salary
                        fields.SetField("14thmonthnontaxableytd", YearToDate.Rows[0]["pay14month"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["pay14month"].ToString())));//Basic Salary
                        fields.SetField("14thmonthfringeytd", YearToDate.Rows[0]["adjfringe"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["adjfringe"].ToString())));//Basic Salary
                        fields.SetField("netpayytd", YearToDate.Rows[0]["netpay"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["netpay"].ToString())));//Basic Salary
                        fields.SetField("sssemployershareytd", YearToDate.Rows[0]["sssemployershare"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["sssemployershare"].ToString())));//Basic Salary
                        fields.SetField("sssemployerecshareytd", YearToDate.Rows[0]["sssemployerecshare"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["sssemployerecshare"].ToString())));//Basic Salary
                        fields.SetField("philhealthemployershareytd", YearToDate.Rows[0]["philhealthemployershare"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["philhealthemployershare"].ToString())));//Basic Salary
                        fields.SetField("pagibigemployershareytd", YearToDate.Rows[0]["pagibigemployershare"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["pagibigemployershare"].ToString())));//Basic Salary

                        //fields.SetField("nontaxableallowanceytd", YearToDate.Rows[0][49].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][49].ToString())));//Non Taxable Allowance
                        //fields.SetField("paidtimeoffytd", YearToDate.Rows[0][7].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][7].ToString())));//Paid Time Off
                        //fields.SetField("allowancesytd", YearToDate.Rows[0][11].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][11].ToString())));//Allowances
                        //fields.SetField("bonusytd", YearToDate.Rows[0][12].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][12].ToString())));//Bonus
                        //fields.SetField("adjustmentsytd", YearToDate.Rows[0][13].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][13].ToString())));//Adjustments
                        //fields.SetField("totalytd", YearToDate.Rows[0][14].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][14].ToString())));//Gross Pay/Total
                        //fields.SetField("employeehdmfytd", YearToDate.Rows[0][15].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][15].ToString())));//Employee HDMF
                        //fields.SetField("employeephilhealthytd", YearToDate.Rows[0][16].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][16].ToString())));//Employee PhilHealth
                        //fields.SetField("employeesocialsecurityytd", YearToDate.Rows[0][17].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][17].ToString())));//Employee Social Security
                        //fields.SetField("withholdingtaxytd", YearToDate.Rows[0][18].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][18].ToString())));//WithHolding Tax
                        //fields.SetField("sssloandeductionytd", YearToDate.Rows[0][19].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][19].ToString())));//SSS Loan Deduction
                        //fields.SetField("pagibigloandeductionytd", YearToDate.Rows[0][20].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][20].ToString())));//Pag-ibig Loan Deduction
                        //fields.SetField("netpayytd", YearToDate.Rows[0][21].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][21].ToString())));//Net Pay
                        //fields.SetField("employerhdmfytd", YearToDate.Rows[0][22].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][22].ToString())));//Employer HDMF
                        //fields.SetField("employerphilhealthytd", YearToDate.Rows[0][23].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][23].ToString())));//Employer PhilHealth
                        //fields.SetField("employersocialsecurityytd", YearToDate.Rows[0][24].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][24].ToString())));//Employer Social Security
                        //fields.SetField("employersocialsecurityecytd", YearToDate.Rows[0][25].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][25].ToString())));//Employer Social Security EC

                        //fields.SetField("deductionsabsencesytd", paysliprecordspayslipdetails.Rows[0]["Absences"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Absences"].ToString())));//Deductions Absences
                        //fields.SetField("deductionslatesytd", paysliprecordspayslipdetails.Rows[0]["Late"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Late"].ToString())));//Deductions Lates
                        //fields.SetField("doubleholidayytd", paysliprecordspayslipdetails.Rows[0]["DH_REG_AMOUNT"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["DH_REG_AMOUNT"].ToString())));//DH_REG_Amount
                        //fields.SetField("doubleholidayndytd", paysliprecordspayslipdetails.Rows[0]["DH_ND_AMOUNT"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["DH_ND_AMOUNT"].ToString())));//DH_ND_Amount
                        //fields.SetField("doubleholidayotytd", paysliprecordspayslipdetails.Rows[0]["DH_OT_AMOUNT"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["DH_OT_AMOUNT"].ToString())));//DH_OT_Amount
                        #endregion                                                                                                                                                                    ////YEAR TO DATE////
                    }
                    #endregion
                    //-- New End
                }
                else
                {
                    #region PDF AVP
                    con = new Connection();
                    con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = empid });
                    con.myparameters.Add(new myParameters { ParameterName = "@PAYPERIOD", mytype = SqlDbType.NVarChar, Value = DDLPayPeriod });
                    paysliprecordspayslipdetails = con.GetDataTable("sp_GetPaySlipRecordsPaySlipDetailsGenerali");

                    if (paysliprecordspayslipdetails.Rows.Count > 0)
                    {
                        #region Old Pdf
                        #endregion
                        #region New Custom Generali PDF
                        fields.SetField("payday", paysliprecordspayslipdetails.Rows[0]["pay_day"].ToString());//Pay Day
                        fields.SetField("regularothrs", paysliprecordspayslipdetails.Rows[0]["Regular OT Hours"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Regular OT Hours"].ToString())));//Basic Salary
                        fields.SetField("regularndothrs", paysliprecordspayslipdetails.Rows[0]["Regular ND OT Hours"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Regular ND OT Hours"].ToString())));//Basic Salary
                        fields.SetField("restdayndothrs", paysliprecordspayslipdetails.Rows[0]["Rest Day ND OT Hours"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Rest Day ND OT Hours"].ToString())));//Basic Salary
                        fields.SetField("restdayothrs", paysliprecordspayslipdetails.Rows[0]["RestDayOT"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["RestDayOT"].ToString())));//Basic Salary
                        fields.SetField("regularholidayhrs", paysliprecordspayslipdetails.Rows[0]["Regular Holiday Hours"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Regular Holiday Hours"].ToString())));//Basic Salary
                        fields.SetField("regularholidayothrs", paysliprecordspayslipdetails.Rows[0]["Regular Holiday OT Hours"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Regular Holiday OT Hours"].ToString())));//Basic Salary
                        fields.SetField("regularholidayndothrs", paysliprecordspayslipdetails.Rows[0]["RH_ND_Hrs"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["RH_ND_Hrs"].ToString())));//Basic Salary
                        fields.SetField("specialholidayhrs", paysliprecordspayslipdetails.Rows[0]["Special Holiday Hours"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Special Holiday Hours"].ToString())));//Basic Salary
                        fields.SetField("specialholidayothrs", paysliprecordspayslipdetails.Rows[0]["Special Holiday OT Hours"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Special Holiday OT Hours"].ToString())));//Basic Salary
                        fields.SetField("specialholidayndothrs", paysliprecordspayslipdetails.Rows[0]["SH_ND_Hrs"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["SH_ND_Hrs"].ToString())));//Basic Salary
                        fields.SetField("adjabsencehrs", paysliprecordspayslipdetails.Rows[0]["adj_Absent_Hrs"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["adj_Absent_Hrs"].ToString())));//Basic Salary
                        fields.SetField("adjlatehrs", paysliprecordspayslipdetails.Rows[0]["Adj_Late_Hrs"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Adj_Late_Hrs"].ToString())));//Basic Salary
                        fields.SetField("adjrdndothrs", paysliprecordspayslipdetails.Rows[0]["Adj_RDNDOT_Hrs"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Adj_RDNDOT_Hrs"].ToString())));//Basic Salary
                        fields.SetField("adjregularhrs", paysliprecordspayslipdetails.Rows[0]["Adj Regular Hours"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Adj Regular Hours"].ToString())));//Basic Salary
                        fields.SetField("adjregularndothrs", paysliprecordspayslipdetails.Rows[0]["Adj Regular ND OT"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Adj Regular ND OT"].ToString())));//Basic Salary
                        fields.SetField("adjregularothrs", paysliprecordspayslipdetails.Rows[0]["Adj Regular OT"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Adj Regular OT"].ToString())));//Basic Salary
                        fields.SetField("adjrestdayothrs", paysliprecordspayslipdetails.Rows[0]["Adj Rest Day OT"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Adj Rest Day OT"].ToString())));//Basic Salary
                        fields.SetField("basicsalarycurrent", paysliprecordspayslipdetails.Rows[0]["Basic Salary"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Basic Salary"].ToString())));//Basic Salary
                                                                                                                                                                                                                                                          //fields.SetField("deductionabsenceshrs", paysliprecordspayslipdetails.Rows[0]["Absences"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Absences"].ToString())));//Basic Salary
                        fields.SetField("stitaxablecurrent", paysliprecordspayslipdetails.Rows[0]["STI Taxable"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["STI Taxable"].ToString())));//Basic Salary
                        fields.SetField("deductionabsencescurrent", paysliprecordspayslipdetails.Rows[0]["Deductions Absences"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Deductions Absences"].ToString())));//Basic Salary
                        fields.SetField("deductiontardinesscurrent", paysliprecordspayslipdetails.Rows[0]["Deductions Tardiness"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Deductions Tardiness"].ToString())));//Basic Salary
                        fields.SetField("13monthtaxablecurrent", paysliprecordspayslipdetails.Rows[0]["13th Month Taxable"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["13th Month Taxable"].ToString())));//Basic Salary
                        fields.SetField("regularotcurrent", paysliprecordspayslipdetails.Rows[0]["Regular OT Amount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Regular OT Amount"].ToString())));//Basic Salary
                        fields.SetField("regularndotcurrent", paysliprecordspayslipdetails.Rows[0]["Regular ND OT Amount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Regular ND OT Amount"].ToString())));//Basic Salary

                        fields.SetField("restdayotcurrent", paysliprecordspayslipdetails.Rows[0]["RestDayOT_Amount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["RestDayOT_Amount"].ToString())));//Basic Salary
                        fields.SetField("restdayndotcurrent", paysliprecordspayslipdetails.Rows[0]["Rest Day ND OT Amount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Rest Day ND OT Amount"].ToString())));//Basic Salary
                        fields.SetField("regularholidaycurrent", paysliprecordspayslipdetails.Rows[0]["Regular Holiday Amount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Regular Holiday Amount"].ToString())));//Basic Salary
                        fields.SetField("regularholidayotcurrent", paysliprecordspayslipdetails.Rows[0]["Regular Holiday OT Amount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Regular Holiday OT Amount"].ToString())));//Basic Salary
                        fields.SetField("regularholidayndotcurrent", paysliprecordspayslipdetails.Rows[0]["RH_ND_Amount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["RH_ND_Amount"].ToString())));//Basic Salary
                        fields.SetField("specialholidaycurrent", paysliprecordspayslipdetails.Rows[0]["Special Holiday Amount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Special Holiday Amount"].ToString())));//Basic Salary
                        fields.SetField("specialholidayotcurrent", paysliprecordspayslipdetails.Rows[0]["Special Holiday OT Amount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Special Holiday OT Amount"].ToString())));//Basic Salary
                        fields.SetField("specialholidayndotcurrent", paysliprecordspayslipdetails.Rows[0]["SH_ND_Amount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["SH_ND_Amount"].ToString())));//Basic Salary
                        fields.SetField("adjabsencecurrent", paysliprecordspayslipdetails.Rows[0]["Adj_Absent_Amount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Adj_Absent_Amount"].ToString())));//Basic Salary
                        fields.SetField("adjlatecurrent", paysliprecordspayslipdetails.Rows[0]["Adj_Late_Amount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Adj_Late_Amount"].ToString())));//Basic Salary
                        fields.SetField("adjrdndotcurrent", paysliprecordspayslipdetails.Rows[0]["Adj_RDNDOT_Amount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Adj_RDNDOT_Amount"].ToString())));//Basic Salary
                        fields.SetField("adjregularhourscurrent", paysliprecordspayslipdetails.Rows[0]["Adj Regular Hours Amount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Adj Regular Hours Amount"].ToString())));//Basic Salary
                        fields.SetField("adjregularndotcurrent", paysliprecordspayslipdetails.Rows[0]["Adj Regular ND OT Amount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Adj Regular ND OT Amount"].ToString())));//Basic Salary
                        fields.SetField("adjregularotcurrent", paysliprecordspayslipdetails.Rows[0]["Adj Regular OT Amount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Adj Regular OT Amount"].ToString())));//Basic Salary
                        fields.SetField("adjrestdayotcurrent", paysliprecordspayslipdetails.Rows[0]["Adj Rest Day OT Amount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Adj Rest Day OT Amount"].ToString())));//Basic Salary
                        fields.SetField("grosspaycurrent", paysliprecordspayslipdetails.Rows[0]["Gross Pay"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Gross Pay"].ToString())));//Basic Salary
                        fields.SetField("deductionssscurrent", paysliprecordspayslipdetails.Rows[0]["Employee SSS"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Employee SSS"].ToString())));//Basic Salary
                        fields.SetField("deductionphilhealthcurrent", paysliprecordspayslipdetails.Rows[0]["Employee PhilHealth"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Employee PhilHealth"].ToString())));//Basic Salary
                        fields.SetField("deductionpagibigcurrent", paysliprecordspayslipdetails.Rows[0]["Employee Pagibig"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Employee Pagibig"].ToString())));//Basic Salary
                        fields.SetField("taxableincomecurrent", paysliprecordspayslipdetails.Rows[0]["Taxable Income"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Taxable Income"].ToString())));//Basic Salary
                        fields.SetField("deductionwtaxcurrent", paysliprecordspayslipdetails.Rows[0]["Tax"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Tax"].ToString())));//Basic Salary
                        fields.SetField("deductionsplacurrent", paysliprecordspayslipdetails.Rows[0]["PLA"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["PLA"].ToString())));//Basic Salary
                        fields.SetField("deductionsprulifecurrent", paysliprecordspayslipdetails.Rows[0]["Pru Life"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Pru Life"].ToString())));//Basic Salary
                        fields.SetField("deductionssdcurrent", paysliprecordspayslipdetails.Rows[0]["SD"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["SD"].ToString())));//Basic Salary
                        fields.SetField("deductionsssloanamountcurrent", paysliprecordspayslipdetails.Rows[0]["SSS Loan Amount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["SSS Loan Amount"].ToString())));//Basic Salary
                        fields.SetField("deductionpagibigloanamountcurrent", paysliprecordspayslipdetails.Rows[0]["Pagibig Loan Amount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Pagibig Loan Amount"].ToString())));//Basic Salary
                        fields.SetField("stinontaxablecurrent", paysliprecordspayslipdetails.Rows[0]["OtherBen"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["OtherBen"].ToString())));//Basic Salary
                        fields.SetField("stifringecurrent", paysliprecordspayslipdetails.Rows[0]["De Minimis"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["De Minimis"].ToString())));//Basic Salary
                        fields.SetField("nontaxableallowancecurrent", paysliprecordspayslipdetails.Rows[0]["Adj_NonTaxable_Allowance"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Adj_NonTaxable_Allowance"].ToString())));//Basic Salary
                        fields.SetField("13thmonthnontaxablecurrent", paysliprecordspayslipdetails.Rows[0]["13th Month Non-Taxable"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["13th Month Non-Taxable"].ToString())));//Basic Salary
                        fields.SetField("netpaycurrent", paysliprecordspayslipdetails.Rows[0]["Net Pay"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Net Pay"].ToString())));//Basic Salary
                        fields.SetField("sssemployersharecurrent", paysliprecordspayslipdetails.Rows[0]["SSS Employer Share"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["SSS Employer Share"].ToString())));//Basic Salary
                        fields.SetField("sssemployerecsharecurrent", paysliprecordspayslipdetails.Rows[0]["SSS Employer EC Share"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["SSS Employer EC Share"].ToString())));//Basic Salary
                        fields.SetField("philhealthemployersharecurrent", paysliprecordspayslipdetails.Rows[0]["PhilHealth Employer Share"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["PhilHealth Employer Share"].ToString())));//Basic Salary
                        fields.SetField("pagibigemployersharecurrent", paysliprecordspayslipdetails.Rows[0]["Pag-ibig Employer Share"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Pag-ibig Employer Share"].ToString())));//Basic Salary

                        fields.SetField("taxableconvertedleavescurrent", paysliprecordspayslipdetails.Rows[0]["ConvertedLeavesTaxable_Amount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["ConvertedLeavesTaxable_Amount"].ToString())));//Taxable Converted Leaves
                        fields.SetField("carallowancecurrent", paysliprecordspayslipdetails.Rows[0]["car_allowance"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["car_allowance"].ToString())));//Car Allowance
                        fields.SetField("14thmonthtaxablecurrent", paysliprecordspayslipdetails.Rows[0]["Taxable_14thMonth"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Taxable_14thMonth"].ToString())));//14th Month Taxable
                        fields.SetField("14thmonthfringecurrent", paysliprecordspayslipdetails.Rows[0]["AdjFringe_NonTaxable"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["AdjFringe_NonTaxable"].ToString())));//14th Month Taxable
                        fields.SetField("deductionundertimecurrent", paysliprecordspayslipdetails.Rows[0]["undertime"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["undertime"].ToString())));//Undertime
                        fields.SetField("deductionlwopcurrent", paysliprecordspayslipdetails.Rows[0]["LWOP_amt"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["LWOP_amt"].ToString())));//LWOP
                        fields.SetField("pagibigemployersharecurrent", paysliprecordspayslipdetails.Rows[0]["Pag-ibig Employer Share"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Pag-ibig Employer Share"].ToString())));//14th Month Taxable
                        fields.SetField("13thmonthtaxablecurrent", paysliprecordspayslipdetails.Rows[0]["13th Month Taxable"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["13th Month Taxable"].ToString())));//13th Month Taxable

                        fields.SetField("deductioncarparkingcurrent", paysliprecordspayslipdetails.Rows[0]["Parking"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Parking"].ToString())));//Car Parking
                        fields.SetField("deductionexcessofbenefitcurrent", paysliprecordspayslipdetails.Rows[0]["EOB"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["EOB"].ToString())));//EOB
                        fields.SetField("deductionmodifiedpagibig2current", paysliprecordspayslipdetails.Rows[0]["PagIBIG_MP2"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["PagIBIG_MP2"].ToString())));//Pagibig MP2
                        fields.SetField("deductionglapimplcurrent", paysliprecordspayslipdetails.Rows[0]["PLA"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["PLA"].ToString())));//GLAPI MPL
                        fields.SetField("deductiontaxpayablecurrent", paysliprecordspayslipdetails.Rows[0]["tax_payable"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["tax_payable"].ToString())));//Tax Payable
                        fields.SetField("taxrefundcurrent", paysliprecordspayslipdetails.Rows[0]["taxrefund"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["taxrefund"].ToString())));//Tax Refund
                        fields.SetField("nontaxableconvertedleavescurrent", paysliprecordspayslipdetails.Rows[0]["ConvertedLeavesNontaxable_Amount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["ConvertedLeavesNontaxable_Amount"].ToString())));//Non Taxable Converted Leaves
                        fields.SetField("uniformallowancecurrent", paysliprecordspayslipdetails.Rows[0]["UniformAllowance"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["UniformAllowance"].ToString())));//Uniform Allowance
                        fields.SetField("medicalallowancecurrent", paysliprecordspayslipdetails.Rows[0]["Medical_Allowance"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Medical_Allowance"].ToString())));//Medical Allowance
                        fields.SetField("socialrelatedallowancecurrent", paysliprecordspayslipdetails.Rows[0]["SocialRelatedAllowance"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["SocialRelatedAllowance"].ToString())));//Social Related Allowance
                        fields.SetField("mobileallowancecurrent", paysliprecordspayslipdetails.Rows[0]["MobileAllowance"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["MobileAllowance"].ToString())));//Mobile Allowance
                        fields.SetField("dataplancurrent", paysliprecordspayslipdetails.Rows[0]["DataPlan"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["DataPlan"].ToString())));//Data Plan
                        fields.SetField("fringebenefitcurrent", paysliprecordspayslipdetails.Rows[0]["Fringe_NonTaxable"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Fringe_NonTaxable"].ToString())));//Fringe Benefit
                        fields.SetField("14thmonthnontaxablecurrent", paysliprecordspayslipdetails.Rows[0]["Payable14thMonth"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Payable14thMonth"].ToString())));//Fringe Benefit
                        #endregion

                    }
                    else
                    {

                    }
                    //ppyear = DDYear.SelectedValue.ToString();
                    con.myparameters.Clear();
                    con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.VarChar, Value = empid });
                    con.myparameters.Add(new myParameters { ParameterName = "@PAYPERIOD", mytype = SqlDbType.VarChar, Value = paysliprecordspayslipdetails.Rows[0][0].ToString() });
                    con.myparameters.Add(new myParameters { ParameterName = "@YEAR", mytype = SqlDbType.VarChar, Value = DDLYear });
                    YearToDate = con.GetDataTable("sp_GetPaySlipRecordsPaySlipDetailsYearToDateGenerali");
                    if (YearToDate.Rows.Count > 0)
                    {
                        #region Old PDF
                        fields.SetField("basicsalaryytd", YearToDate.Rows[0]["basicsalary"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["basicsalary"].ToString())));//Basic Salary
                        fields.SetField("taxableconvertedleavesytd", YearToDate.Rows[0]["taxableconvertedleaves"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["taxableconvertedleaves"].ToString())));//Taxable Converted Leaves
                        fields.SetField("carallowanceytd", YearToDate.Rows[0]["carallowance"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["carallowance"].ToString())));//Car Allowance
                        fields.SetField("deductionabsencesytd", YearToDate.Rows[0]["deductionsabsences"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["deductionsabsences"].ToString())));//Deduction absences
                        fields.SetField("deductiontardinessytd", YearToDate.Rows[0]["deductionstardiness"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["deductionstardiness"].ToString())));//Deduction lates
                        fields.SetField("13thmonthtaxableytd", YearToDate.Rows[0]["13thmonthtaxable"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["13thmonthtaxable"].ToString())));//13th month
                        fields.SetField("14thmonthtaxableytd", YearToDate.Rows[0]["tax14month"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["tax14month"].ToString())));//14th month
                        fields.SetField("stitaxableytd", YearToDate.Rows[0]["stitaxable"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["stitaxable"].ToString())));//Basic Salary
                        fields.SetField("deductionundertimeytd", YearToDate.Rows[0]["undertime"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["undertime"].ToString())));//Undertime
                        fields.SetField("deductionlwopytd", YearToDate.Rows[0]["lwopdeduction"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["lwopdeduction"].ToString())));//LWOP

                        fields.SetField("regularotytd", YearToDate.Rows[0]["regularotamount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["regularotamount"].ToString())));//Regular OT
                        fields.SetField("regularndotytd", YearToDate.Rows[0]["regularndotamount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["regularndotamount"].ToString())));//Regular Night Diff

                        fields.SetField("restdayotytd", YearToDate.Rows[0]["rdotamount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["rdotamount"].ToString())));//Reg RDOT
                        fields.SetField("restdayndotytd", YearToDate.Rows[0]["restdayndotamount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["restdayndotamount"].ToString())));//Reg RDNDOT

                        fields.SetField("regularholidayytd", YearToDate.Rows[0]["regularholidayamount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["regularholidayamount"].ToString())));//Regular Holiday
                        fields.SetField("regularholidayndotytd", YearToDate.Rows[0]["regularholidayndamount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["regularholidayndamount"].ToString())));//Regular Holiday Night Diff
                        fields.SetField("regularholidayotytd", YearToDate.Rows[0]["regularholidayotamount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["regularholidayotamount"].ToString())));//Regular Holiday OT

                        fields.SetField("specialholidayytd", YearToDate.Rows[0]["specialholidayamount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["specialholidayamount"].ToString())));//Special Holiday
                        fields.SetField("specialholidayndotytd", YearToDate.Rows[0]["specialholidayndamount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["specialholidayndamount"].ToString())));//Special Holiday Night Diff
                        fields.SetField("specialholidayotytd", YearToDate.Rows[0]["specialholidayotamount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["specialholidayotamount"].ToString())));//Special Holiday OT

                        fields.SetField("adjabsenceytd", YearToDate.Rows[0]["adjabsenceamt"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["adjabsenceamt"].ToString())));//Double Holiday
                        fields.SetField("adjlateytd", YearToDate.Rows[0]["adjlateamt"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["adjlateamt"].ToString())));//Double Holiday Night Diff
                        fields.SetField("adjrdndotytd", YearToDate.Rows[0]["adjrdndotamt"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["adjrdndotamt"].ToString())));//Double Holiday OT

                        fields.SetField("adjregularhoursytd", YearToDate.Rows[0]["adjregularhoursamount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["adjregularhoursamount"].ToString())));//Double Holiday
                        fields.SetField("adjregularndotytd", YearToDate.Rows[0]["adjregularndotamount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["adjregularndotamount"].ToString())));//Double Holiday Night Diff
                        fields.SetField("adjregularotytd", YearToDate.Rows[0]["adjregularotamount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["adjregularotamount"].ToString())));//Double Holiday OT
                        fields.SetField("adjrestdayotytd", YearToDate.Rows[0]["adjrestdayotamount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["adjrestdayotamount"].ToString())));//Double Holiday OT

                        fields.SetField("grosspayytd", YearToDate.Rows[0]["grosspay"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["grosspay"].ToString())));//Basic Salary
                        fields.SetField("deductionsssytd", YearToDate.Rows[0]["employeesss"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["employeesss"].ToString())));//Basic Salary
                        fields.SetField("deductionphilhealthytd", YearToDate.Rows[0]["employeephilhealth"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["employeephilhealth"].ToString())));//Basic Salary
                        fields.SetField("deductionpagibigytd", YearToDate.Rows[0]["employeepagibig"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["employeepagibig"].ToString())));//Basic Salary
                        fields.SetField("taxableincomeytd", YearToDate.Rows[0]["taxableincome"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["taxableincome"].ToString())));//Basic Salary
                        fields.SetField("deductionwtaxytd", YearToDate.Rows[0]["tax"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["tax"].ToString())));//Basic Salary
                        fields.SetField("deductioncarparkingytd", YearToDate.Rows[0]["parking"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["parking"].ToString())));//Car Parking
                        fields.SetField("deductionexcessofbenefitytd", YearToDate.Rows[0]["eob"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["eob"].ToString())));//Excess of Benefit
                        fields.SetField("deductionmodifiedpagibig2ytd", YearToDate.Rows[0]["pagibigmp2"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["pagibigmp2"].ToString())));//Pagibig MP2
                        fields.SetField("deductionglapimplytd", YearToDate.Rows[0]["pla"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["pla"].ToString())));//Basic Salary
                        fields.SetField("deductionsprulifeytd", YearToDate.Rows[0]["prulife"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["prulife"].ToString())));//Basic Salary
                        fields.SetField("deductionssdytd", YearToDate.Rows[0]["sd"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["sd"].ToString())));//Basic Salary
                        fields.SetField("deductionsssloanamountytd", YearToDate.Rows[0]["sssloanamount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["sssloanamount"].ToString())));//Basic Salary
                        fields.SetField("deductionpagibigloanamountytd", YearToDate.Rows[0]["pagibigloanamount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["pagibigloanamount"].ToString())));//Basic Salary
                        fields.SetField("deductiontaxpayableytd", YearToDate.Rows[0]["taxpayable"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["taxpayable"].ToString())));//Tax Payable
                        fields.SetField("nontaxableconvertedleavesytd", YearToDate.Rows[0]["nontaxableconvertedleaves"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["nontaxableconvertedleaves"].ToString())));//Non-Taxable Converted Leaves
                        fields.SetField("taxrefundytd", YearToDate.Rows[0]["taxrefund"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["taxrefund"].ToString())));//Tax Refund
                        fields.SetField("stinontaxableytd", YearToDate.Rows[0]["ecola"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["ecola"].ToString())));//Basic Salary
                        fields.SetField("stifringeytd", YearToDate.Rows[0]["deminimis"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["deminimis"].ToString())));//Basic Salary
                        fields.SetField("uniformallowanceytd", YearToDate.Rows[0]["uniformallowance"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["uniformallowance"].ToString())));//Uniform Allowance
                        fields.SetField("medicalallowanceytd", YearToDate.Rows[0]["medicalallowance"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["medicalallowance"].ToString())));//Medical Allowance
                        fields.SetField("socialrelatedallowanceytd", YearToDate.Rows[0]["socialrelatedallowance"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["socialrelatedallowance"].ToString())));//Social Related Allowance
                        fields.SetField("mobileallowanceytd", YearToDate.Rows[0]["mobileallowance"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["mobileallowance"].ToString())));//Mobile Allowance
                        fields.SetField("dataplanytd", YearToDate.Rows[0]["dataplan"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["dataplan"].ToString())));//Data Plan
                        fields.SetField("fringebenefitytd", YearToDate.Rows[0]["fringe"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["fringe"].ToString())));//Fringe
                        fields.SetField("nontaxableallowanceytd", YearToDate.Rows[0]["adjnontaxableallowance"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["adjnontaxableallowance"].ToString())));//Basic Salary
                        fields.SetField("13thmonthnontaxableytd", YearToDate.Rows[0]["13thmonthnontaxable"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["13thmonthnontaxable"].ToString())));//Basic Salary
                        fields.SetField("14thmonthnontaxableytd", YearToDate.Rows[0]["pay14month"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["pay14month"].ToString())));//Basic Salary
                        fields.SetField("14thmonthfringeytd", YearToDate.Rows[0]["adjfringe"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["adjfringe"].ToString())));//Basic Salary
                        fields.SetField("netpayytd", YearToDate.Rows[0]["netpay"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["netpay"].ToString())));//Basic Salary
                        fields.SetField("sssemployershareytd", YearToDate.Rows[0]["sssemployershare"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["sssemployershare"].ToString())));//Basic Salary
                        fields.SetField("sssemployerecshareytd", YearToDate.Rows[0]["sssemployerecshare"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["sssemployerecshare"].ToString())));//Basic Salary
                        fields.SetField("philhealthemployershareytd", YearToDate.Rows[0]["philhealthemployershare"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["philhealthemployershare"].ToString())));//Basic Salary
                        fields.SetField("pagibigemployershareytd", YearToDate.Rows[0]["pagibigemployershare"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["pagibigemployershare"].ToString())));//Basic Salary

                        //fields.SetField("nontaxableallowanceytd", YearToDate.Rows[0][49].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][49].ToString())));//Non Taxable Allowance
                        //fields.SetField("paidtimeoffytd", YearToDate.Rows[0][7].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][7].ToString())));//Paid Time Off
                        //fields.SetField("allowancesytd", YearToDate.Rows[0][11].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][11].ToString())));//Allowances
                        //fields.SetField("bonusytd", YearToDate.Rows[0][12].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][12].ToString())));//Bonus
                        //fields.SetField("adjustmentsytd", YearToDate.Rows[0][13].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][13].ToString())));//Adjustments
                        //fields.SetField("totalytd", YearToDate.Rows[0][14].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][14].ToString())));//Gross Pay/Total
                        //fields.SetField("employeehdmfytd", YearToDate.Rows[0][15].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][15].ToString())));//Employee HDMF
                        //fields.SetField("employeephilhealthytd", YearToDate.Rows[0][16].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][16].ToString())));//Employee PhilHealth
                        //fields.SetField("employeesocialsecurityytd", YearToDate.Rows[0][17].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][17].ToString())));//Employee Social Security
                        //fields.SetField("withholdingtaxytd", YearToDate.Rows[0][18].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][18].ToString())));//WithHolding Tax
                        //fields.SetField("sssloandeductionytd", YearToDate.Rows[0][19].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][19].ToString())));//SSS Loan Deduction
                        //fields.SetField("pagibigloandeductionytd", YearToDate.Rows[0][20].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][20].ToString())));//Pag-ibig Loan Deduction
                        //fields.SetField("netpayytd", YearToDate.Rows[0][21].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][21].ToString())));//Net Pay
                        //fields.SetField("employerhdmfytd", YearToDate.Rows[0][22].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][22].ToString())));//Employer HDMF
                        //fields.SetField("employerphilhealthytd", YearToDate.Rows[0][23].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][23].ToString())));//Employer PhilHealth
                        //fields.SetField("employersocialsecurityytd", YearToDate.Rows[0][24].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][24].ToString())));//Employer Social Security
                        //fields.SetField("employersocialsecurityecytd", YearToDate.Rows[0][25].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][25].ToString())));//Employer Social Security EC

                        //fields.SetField("deductionsabsencesytd", paysliprecordspayslipdetails.Rows[0]["Absences"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Absences"].ToString())));//Deductions Absences
                        //fields.SetField("deductionslatesytd", paysliprecordspayslipdetails.Rows[0]["Late"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Late"].ToString())));//Deductions Lates
                        //fields.SetField("doubleholidayytd", paysliprecordspayslipdetails.Rows[0]["DH_REG_AMOUNT"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["DH_REG_AMOUNT"].ToString())));//DH_REG_Amount
                        //fields.SetField("doubleholidayndytd", paysliprecordspayslipdetails.Rows[0]["DH_ND_AMOUNT"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["DH_ND_AMOUNT"].ToString())));//DH_ND_Amount
                        //fields.SetField("doubleholidayotytd", paysliprecordspayslipdetails.Rows[0]["DH_OT_AMOUNT"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["DH_OT_AMOUNT"].ToString())));//DH_OT_Amount
                        #endregion
                        //foreach (var field in af.Fields)
                        //{
                        //    Console.WriteLine("{0}, {1}", field.Key, field.Value);

                        //    string sample2 = field.Key.ToString();
                        //    //TextBox2.Text = sample + field.Key.ToString() + ',';
                        //    fields.SetField(sample2, sample2);
                        //}
                    }
                    else
                    {

                    }
                    #endregion
                }




                stamper.FormFlattening = true;
                // closing the stamper

                stamper.Close();


                return "Success";
            }
            catch (Exception e)
            {
                return "error:" + e;
            }
        }
        [HttpPost]
        [Route("GeneratePayslipCtrack")]
        public string GeneratePayslipCtrack(UserProfilev2 uProfile)
        {
            try
            {
                Connection con;

                GetDatabase GDB = new GetDatabase();
                GDB.ClientName = uProfile.CN;
                SelfieRegistration2Controller.GetDB2(GDB);

                string DDLPayPeriod = uProfile.PayDay;
                string DDLYear = uProfile.PayYear;
                empid = uProfile.NTID;

                //empname = PayMtd.searchempname(empid);


                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@empId", mytype = SqlDbType.NVarChar, Value = empid });
                DataTable DTProfile = con.GetDataTable("sp_getCompleteProfile_Mobilev2");

                con.myparameters.Clear();
                con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = empid });
                con.myparameters.Add(new myParameters { ParameterName = "@PAYPERIOD", mytype = SqlDbType.NVarChar, Value = DDLPayPeriod });
                paysliprecordspayslipdetails = con.GetDataTable("sp_GetPaySlipRecordsPaySlipDetailsCTI");

                con.myparameters.Clear();
                con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = empid });
                con.myparameters.Add(new myParameters { ParameterName = "@PAYPERIOD", mytype = SqlDbType.NVarChar, Value = paysliprecordspayslipdetails.Rows[0]["Pay Day"].ToString() });
                con.myparameters.Add(new myParameters { ParameterName = "@YEAR", mytype = SqlDbType.NVarChar, Value = DDLYear });
                YearToDate = con.GetDataTable("sp_GetPaySlipRecordsPaySlipDetailsYearToDateCTI");

                string path = "https://ws.durusthr.com/ILM_WS_Live/pdf";
                string path2 = HttpContext.Current.Server.MapPath("pdf");
                string newFile = path2 + "/PaySlip-" + empid + ".pdf";
                PdfReader reader;
                reader = new PdfReader(path + "/PAYSLIP_CTI.pdf");

                string pdfpass1 = "", pdfpass2 = "";
                // instantiate PDFStamper object
                // The Output file will be created from template file and edited by the PDFStamper
                PdfStamper stamper = new PdfStamper(reader, new FileStream(newFile, FileMode.Create));

                stamper.SetEncryption(true, pdfpass1, pdfpass2, PdfWriter.ALLOW_SCREENREADERS);

                AcroFields fields = stamper.AcroFields;
                string payslipid = "";
                if (DTProfile.Rows.Count > 0)
                {
                    fields.SetField("MonthlyRate", DTProfile.Rows[0]["Salary"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(DTProfile.Rows[0]["Salary"].ToString())));
                }
                if (paysliprecordspayslipdetails.Rows.Count > 0)
                {
                    payslipid = paysliprecordspayslipdetails.Rows[0]["ID"].ToString();
                    ////CURRENT////
                    fields.SetField("ComAllCurrent", paysliprecordspayslipdetails.Rows[0]["Fringe_NonTaxable"].ToString());
                    fields.SetField("MaxicareCurrent", paysliprecordspayslipdetails.Rows[0]["DisputesHours"].ToString());
                    fields.SetField("WFHCurrent", paysliprecordspayslipdetails.Rows[0]["Fringe_Taxable"].ToString());
                    fields.SetField("AbsencesCurrent", paysliprecordspayslipdetails.Rows[0]["Absences Amount"].ToString());
                    fields.SetField("ActingAllowanceCurrent", paysliprecordspayslipdetails.Rows[0]["Acting Allowance"].ToString());
                    fields.SetField("AdditionalHDMFContributionCurrent", paysliprecordspayslipdetails.Rows[0]["Additional HDMF Contribution"].ToString());
                    fields.SetField("AdjustmentsCurrent", paysliprecordspayslipdetails.Rows[0]["Adjusted Regular Amount (all)"].ToString());
                    fields.SetField("AUBSalaryLoanCurrent", paysliprecordspayslipdetails.Rows[0]["AUB Salary Loan"].ToString());
                    fields.SetField("BasicSalaryCurrent", paysliprecordspayslipdetails.Rows[0]["Basic Salary Amount"].ToString());
                    fields.SetField("CompanyLoanDeductionCurrent", paysliprecordspayslipdetails.Rows[0]["Company Loan"].ToString());
                    fields.SetField("EyeglassesDeductionCurrent", paysliprecordspayslipdetails.Rows[0]["Eyeglasses"].ToString());
                    fields.SetField("GrossEarningsCurrent", paysliprecordspayslipdetails.Rows[0]["Gross Pay"].ToString());
                    fields.SetField("HPVVaccineDeductionCurrent", paysliprecordspayslipdetails.Rows[0]["HPV Vaccine"].ToString());
                    fields.SetField("IntellicareDeductionCurrent", paysliprecordspayslipdetails.Rows[0]["Intellicare"].ToString());
                    fields.SetField("MonthlyRate", paysliprecordspayslipdetails.Rows[0]["Monthly Salary Rate"].ToString());
                    fields.SetField("RegularNightDiffCurrent", paysliprecordspayslipdetails.Rows[0]["ND Amount"].ToString());
                    fields.SetField("RegularNightDiffHours", paysliprecordspayslipdetails.Rows[0]["ND hours"].ToString());
                    fields.SetField("RegularNightDiffOTCurrent", paysliprecordspayslipdetails.Rows[0]["ND OT Amount"].ToString());
                    fields.SetField("RegularNightDiffOTHours", paysliprecordspayslipdetails.Rows[0]["ND OT hours"].ToString());
                    fields.SetField("NetPayCurrent", paysliprecordspayslipdetails.Rows[0]["Net Pay"].ToString());
                    fields.SetField("EmployeeHDMFCurrent", paysliprecordspayslipdetails.Rows[0]["Pag-ibig Employee Share"].ToString());
                    fields.SetField("PagibigLoanDeductionCurrent", paysliprecordspayslipdetails.Rows[0]["Pagibig Loan Amount"].ToString());
                    fields.SetField("PayDay", paysliprecordspayslipdetails.Rows[0]["Pay Day"].ToString());
                    fields.SetField("PayPeriod", paysliprecordspayslipdetails.Rows[0]["Pay Period"].ToString());
                    fields.SetField("EmployeePhilHealthCurrent", paysliprecordspayslipdetails.Rows[0]["PhilHealth Employee Share"].ToString());
                    fields.SetField("RestDayWorkCurrent", paysliprecordspayslipdetails.Rows[0]["RD Amount"].ToString());
                    fields.SetField("RestDayWorkHours", paysliprecordspayslipdetails.Rows[0]["RD Hours"].ToString());
                    fields.SetField("RestDayNDCurrent", paysliprecordspayslipdetails.Rows[0]["RD ND OT Amount"].ToString());
                    fields.SetField("RestDayNDHours", paysliprecordspayslipdetails.Rows[0]["RD ND OT Hours"].ToString());
                    fields.SetField("RestDayNDCurrent", paysliprecordspayslipdetails.Rows[0]["RDND Amount"].ToString());
                    fields.SetField("RestDayNDHours", paysliprecordspayslipdetails.Rows[0]["RDND Hours"].ToString());
                    fields.SetField("RestDayOTCurrent", paysliprecordspayslipdetails.Rows[0]["RDOT Amount"].ToString());
                    fields.SetField("RestDayOTHours", paysliprecordspayslipdetails.Rows[0]["RDOT Hours"].ToString());
                    fields.SetField("RegularHolidayWorkCurrent", paysliprecordspayslipdetails.Rows[0]["Regular Holiday Amount"].ToString());
                    fields.SetField("RegularHolidayWorkHours", paysliprecordspayslipdetails.Rows[0]["Regular Holiday Hours"].ToString());
                    fields.SetField("RegularHolidayNDCurrent", paysliprecordspayslipdetails.Rows[0]["Regular Holiday Night Diff Amount"].ToString());
                    fields.SetField("RegularHolidayNDHours", paysliprecordspayslipdetails.Rows[0]["Regular Holiday Night Diff Hours"].ToString());
                    fields.SetField("RegularHolidayNDOTCurrent", paysliprecordspayslipdetails.Rows[0]["Regular Holiday Night Diff OT Amount"].ToString());
                    fields.SetField("RegularHolidayNDOTHours", paysliprecordspayslipdetails.Rows[0]["Regular Holiday Night Diff OT Hours"].ToString());
                    fields.SetField("RegularHolidayOTCurrent", paysliprecordspayslipdetails.Rows[0]["Regular Holiday Overtime Amount"].ToString());
                    fields.SetField("RegularHolidayOTHours", paysliprecordspayslipdetails.Rows[0]["Regular Holiday Overtime Hours"].ToString());
                    fields.SetField("RegularHolidayRDOTCurrent", paysliprecordspayslipdetails.Rows[0]["Regular Holiday RDOT Amount"].ToString());
                    fields.SetField("RegularHolidayRDOTHours", paysliprecordspayslipdetails.Rows[0]["Regular Holiday RDOT Hours"].ToString());
                    fields.SetField("RegularHolidayRestDayWorkCurrent", paysliprecordspayslipdetails.Rows[0]["Regular Holiday Rest Day Amount"].ToString());
                    fields.SetField("RegularHolidayRestDayWorkHours", paysliprecordspayslipdetails.Rows[0]["Regular Holiday Rest Day Hours"].ToString());
                    fields.SetField("RegularHolidayRDNDCurrent", paysliprecordspayslipdetails.Rows[0]["Regular Holiday Rest Day Night Diff Amount"].ToString());
                    fields.SetField("RegularHolidayRDNDHours", paysliprecordspayslipdetails.Rows[0]["Regular Holiday Rest Day Night Diff Hours"].ToString());
                    fields.SetField("RegularHolidayRDNDOTCurrent", paysliprecordspayslipdetails.Rows[0]["Regular Holiday Rest Day Night Diff OT Amount"].ToString());
                    fields.SetField("RegularHolidayRDNDOTHours", paysliprecordspayslipdetails.Rows[0]["Regular Holiday Rest Day Night Diff OT Hours"].ToString());
                    fields.SetField("RegularOTCurrent", paysliprecordspayslipdetails.Rows[0]["Regular Overtime Amount"].ToString());
                    fields.SetField("RegularOTHours", paysliprecordspayslipdetails.Rows[0]["Regular Overtime Hours"].ToString());
                    fields.SetField("SpecialHolidayWorkCurrent", paysliprecordspayslipdetails.Rows[0]["Special Holiday Amount"].ToString());
                    fields.SetField("SpecialHolidayWorkHours", paysliprecordspayslipdetails.Rows[0]["Special Holiday Hours"].ToString());
                    fields.SetField("SpecialHolidayNDCurrent", paysliprecordspayslipdetails.Rows[0]["Special Holiday Night Diff Amount"].ToString());
                    fields.SetField("SpecialHolidayNDHours", paysliprecordspayslipdetails.Rows[0]["Special Holiday Night Diff Hours"].ToString());
                    fields.SetField("SpecialHolidayNDOTCurrent", paysliprecordspayslipdetails.Rows[0]["Special Holiday Night Diff OT Amount"].ToString());
                    fields.SetField("SpecialHolidayNDOTHours", paysliprecordspayslipdetails.Rows[0]["Special Holiday Night Diff OT Hours"].ToString());
                    fields.SetField("SpecialHolidayOTCurrent", paysliprecordspayslipdetails.Rows[0]["Special Holiday Overtime Amount"].ToString());
                    fields.SetField("SpecialHolidayOTHours", paysliprecordspayslipdetails.Rows[0]["Special Holiday Overtime Hours"].ToString());
                    fields.SetField("SpecialHolidayRDOTCurrent", paysliprecordspayslipdetails.Rows[0]["Special Holiday RD OT Amount"].ToString());
                    fields.SetField("SpecialHolidayRDOTHours", paysliprecordspayslipdetails.Rows[0]["Special Holiday RD OT Hours"].ToString());
                    fields.SetField("SpecialHolidayRestDayWorkCurrent", paysliprecordspayslipdetails.Rows[0]["Special Holiday Rest Day Amount"].ToString());
                    fields.SetField("SpecialHolidayRestDayWorkHours", paysliprecordspayslipdetails.Rows[0]["Special Holiday Rest Day Hours"].ToString());
                    fields.SetField("SpecialHolidayRDNDCurrent", paysliprecordspayslipdetails.Rows[0]["Special Holiday Rest Day ND Amount"].ToString());
                    fields.SetField("SpecialHolidayRDNDHours", paysliprecordspayslipdetails.Rows[0]["Special Holiday Rest Day ND Hours"].ToString());
                    fields.SetField("SpecialHolidayRDNDOTCurrent", paysliprecordspayslipdetails.Rows[0]["Special Holiday Rest Day Night Diff OT Amount"].ToString());
                    fields.SetField("SpecialHolidayRDNDOTHours", paysliprecordspayslipdetails.Rows[0]["Special Holiday Rest Day Night Diff OT Hours"].ToString());
                    fields.SetField("EmployeeSocialSecurityCurrent", paysliprecordspayslipdetails.Rows[0]["SSS Employee Share"].ToString());
                    fields.SetField("SSSLoanDeductionCurrent", paysliprecordspayslipdetails.Rows[0]["SSS Loan Amount"].ToString());
                    fields.SetField("TaxPayableCurrent", paysliprecordspayslipdetails.Rows[0]["Tax Payable"].ToString());
                    fields.SetField("WithholdingTaxCurrent", paysliprecordspayslipdetails.Rows[0]["W-Tax"].ToString());
                    fields.SetField("EmployeeNo", paysliprecordspayslipdetails.Rows[0]["Employee No"].ToString());
                    fields.SetField("Joined", paysliprecordspayslipdetails.Rows[0]["Joined"].ToString());
                    fields.SetField("bonuscurrent", paysliprecordspayslipdetails.Rows[0]["adjRegular"].ToString());
                    //fields.SetField("GrossEarningsCurrent", paysliprecordspayslipdetails.Rows[0]["adjRegular"].ToString());
                    fields.SetField("NetPayCurrent", paysliprecordspayslipdetails.Rows[0]["Net Pay"].ToString());

                    //zeroes
                    fields.SetField("13thMonthNonTaxableCurrent", "0.00");
                    fields.SetField("13thMonthNonTaxableYearToDate", "0.00");
                    fields.SetField("13thMonthTaxableCurrent", "0.00");
                    fields.SetField("13thMonthTaxableYearToDate", "0.00");
                    fields.SetField("AdjustedNDAmountCurrent", "0.00");
                    fields.SetField("AdjustedNDAmountYearToDate", "0.00");
                    fields.SetField("AdjustedRDAmountCurrent", "0.00");
                    fields.SetField("AdjustedRDAmountYearToDate", "0.00");
                    fields.SetField("AdjustedRDOTAmountCurrent", "0.00");
                    fields.SetField("AdjustedRDOTAmountYearToDate", "0.00");
                    fields.SetField("AdjustedRDOTNDAmountCurrent", "0.00");
                    fields.SetField("AdjustedRDOTNDAmountYearToDate", "0.00");
                    fields.SetField("AdjustedRegularAmountCurrent", "0.00");
                    fields.SetField("AdjustedRegularAmountYearToDate", "0.00");
                    fields.SetField("AdjustedRegularOTAmountCurrent", "0.00");
                    fields.SetField("AdjustedRegularOTAmountYearToDate", "0.00");
                    fields.SetField("AdjustedSpeciaHolidayNDAmountYearToDate", "0.00");
                    fields.SetField("BonusCurrent", "0.00");
                    fields.SetField("BonusYearToDate", "0.00");
                    fields.SetField("COLACurrent", "0.00");
                    fields.SetField("COLAYearToDate", "0.00");
                    fields.SetField("DeductionAdvancesCurrent", "0.00");
                    fields.SetField("DeductionAdvancesYearToDate", "0.00");
                    fields.SetField("DeductionOthersCurrent", "0.00");
                    fields.SetField("DeductionOthersYearToDate", "0.00");
                    fields.SetField("MedicalAllowanceCurrent", "0.00");
                    fields.SetField("MedicalAllowanceYearToDate", "0.00");
                    fields.SetField("NonTaxableConvertedLeavesAmountCurrent", "0.00");
                    fields.SetField("NonTaxableConvertedLeavesAmountYearToDate", "0.00");
                    fields.SetField("OtherTaxableIncomeCurrent", "0.00");
                    fields.SetField("OtherTaxableIncomeYearToDate", "0.00");
                    fields.SetField("RegularHolidayRestDayCurrent", "0.00");
                    fields.SetField("RegularHolidayRestDayHours", "0.00");
                    fields.SetField("RegularHolidayRestDayNDCurrent", "0.00");
                    fields.SetField("RegularHolidayRestDayNDHours", "0.00");
                    fields.SetField("RegularHolidayRestDayNDYearToDate", "0.00");
                    fields.SetField("RegularHolidayRestDayOvertimeCurrent", "0.00");
                    fields.SetField("RegularHolidayRestDayOvertimeHours", "0.00");
                    fields.SetField("RegularHolidayRestDayOvertimeYearToDate", "0.00");
                    fields.SetField("RegularHolidayRestDayYearToDate", "0.00");
                    fields.SetField("RegularNDOvertimeCurrent", "0.00");
                    fields.SetField("RegularNDOvertimeHours", "0.00");
                    fields.SetField("RegularNDOvertimeYearToDate", "0.00");
                    fields.SetField("RestDayCurrent", "0.00");
                    fields.SetField("RestDayHours", "0.00");
                    fields.SetField("RestDayNDOvertimeCurrent", "0.00");
                    fields.SetField("RestDayNDOvertimeHours", "0.00");
                    fields.SetField("RestDayNDOvertimeYearToDate", "0.00");
                    fields.SetField("RestDayOvertimeCurrent", "0.00");
                    fields.SetField("RestDayOvertimeHours", "0.00");
                    fields.SetField("RestDayOvertimeYearToDate", "0.00");
                    fields.SetField("RestDayYearToDate", "0.00");
                    fields.SetField("SpecialHolidayRestDayNDCurrent", "0.00");
                    fields.SetField("SpecialHolidayRestDayNDHours", "0.00");
                    fields.SetField("SpecialHolidayRestDayNDYearToDate", "0.00");
                    fields.SetField("SpecialHolidayRestdayOvertimeCurrent", "0.00");
                    fields.SetField("SpecialHolidayRestDayOvertimeHours", "0.00");
                    fields.SetField("SpecialHolidayRestDayOvertimeYearToDate", "0.00");
                    fields.SetField("SpecialHolidayRestDayYearToDate", "0.00");
                    fields.SetField("TaxableConvertedLeavesCurrent", "0.00");
                    fields.SetField("TaxableConvertedLeavesYearToDate", "0.00");
                    fields.SetField("TransportationAllowanceCurrent", "0.00");
                    fields.SetField("TransportationAllowanceYearToDate", "0.00");

                    fields.SetField("SSSNo", DTProfile.Rows[0]["SSS"].ToString());
                    fields.SetField("PagibigNo", DTProfile.Rows[0]["Pagibig"].ToString());
                    fields.SetField("PhilHealthNo", DTProfile.Rows[0]["PhilHealth"].ToString());
                    fields.SetField("TaxNo", DTProfile.Rows[0]["TIN"].ToString());
                    fields.SetField("EmployeeName", DTProfile.Rows[0]["Emp_Name"].ToString());
                    fields.SetField("HourlyRate", "".ToString());
                    fields.SetField("Position", DTProfile.Rows[0]["Employee_Level"].ToString());
                    fields.SetField("JoinedDate", DTProfile.Rows[0]["DateJoined"].ToString());

                }


                if (YearToDate.Rows.Count > 0)
                {

                    ////YEAR TO DATE////
                    fields.SetField("ComAllYearToDate", YearToDate.Rows[0]["Fringe_NonTaxable"].ToString());
                    fields.SetField("AbsencesYearToDate", YearToDate.Rows[0]["Absences Amount"].ToString());
                    fields.SetField("ActingAllowanceYearToDate", YearToDate.Rows[0]["Acting Allowance"].ToString());
                    fields.SetField("MaxicareYearToDate", YearToDate.Rows[0]["DisputesHours"].ToString());
                    fields.SetField("WFHYearToDate", YearToDate.Rows[0]["Fringe_Taxable"].ToString());
                    fields.SetField("AdditionalHDMFContributionYearToDate", YearToDate.Rows[0]["Additional HDMF Contribution"].ToString());
                    fields.SetField("AdjustmentsYearToDate", YearToDate.Rows[0]["Adjusted Regular Amount (all)"].ToString());
                    fields.SetField("AUBSalaryLoanYearToDate", YearToDate.Rows[0]["AUB Salary Loan"].ToString());
                    fields.SetField("BasicSalaryYearToDate", YearToDate.Rows[0]["Basic Salary Amount"].ToString());
                    fields.SetField("CompanyLoanDeductionYearToDate", YearToDate.Rows[0]["Company Loan"].ToString());
                    fields.SetField("EyeglassesDeductionYearToDate", YearToDate.Rows[0]["Eyeglasses"].ToString());
                    fields.SetField("GrossEarningsYearToDate", YearToDate.Rows[0]["Gross Pay"].ToString());
                    fields.SetField("HPVVaccineDeductionYearToDate", YearToDate.Rows[0]["HPV Vaccine"].ToString());
                    fields.SetField("IntellicareDeductionYearToDate", YearToDate.Rows[0]["Intellicare"].ToString());
                    fields.SetField("RegularNightDiffYearToDate", YearToDate.Rows[0]["ND Amount"].ToString());
                    fields.SetField("RegularNightDiffOTYearToDate", YearToDate.Rows[0]["ND OT Amount"].ToString());
                    fields.SetField("NetPayYearToDate", YearToDate.Rows[0]["Net Pay"].ToString());
                    fields.SetField("EmployeeHDMFYearToDate", YearToDate.Rows[0]["Pag-ibig Employee Share"].ToString());
                    fields.SetField("PagibigLoanDeductionYearToDate", YearToDate.Rows[0]["Pagibig Loan Amount"].ToString());
                    fields.SetField("EmployeePhilHealthYearToDate", YearToDate.Rows[0]["PhilHealth Employee Share"].ToString());
                    fields.SetField("RestDayWorkYearToDate", YearToDate.Rows[0]["RD Amount"].ToString());
                    fields.SetField("RestDayNDOTCurrent", YearToDate.Rows[0]["RD ND OT Amount"].ToString());
                    fields.SetField("RestDayNDYearToDate", YearToDate.Rows[0]["RDND Amount"].ToString());
                    fields.SetField("RestDayOTYearToDate", YearToDate.Rows[0]["RDOT Amount"].ToString());
                    fields.SetField("RegularHolidayWorkYearToDate", YearToDate.Rows[0]["Regular Holiday Amount"].ToString());
                    fields.SetField("RegularHolidayNDYearToDate", YearToDate.Rows[0]["Regular Holiday Night Diff Amount"].ToString());
                    fields.SetField("RegularHolidayNDOTYearToDate", YearToDate.Rows[0]["Regular Holiday Night Diff OT Amount"].ToString());
                    fields.SetField("RegularHolidayOTYearToDate", YearToDate.Rows[0]["Regular Holiday Overtime Amount"].ToString());
                    fields.SetField("RegularHolidayRDOTYearToDate", YearToDate.Rows[0]["Regular Holiday RDOT Amount"].ToString());
                    fields.SetField("RegularHolidayRestDayWorkYearToDate", YearToDate.Rows[0]["Regular Holiday Rest Day Amount"].ToString());
                    fields.SetField("RegularHolidayRDNDYearToDate", YearToDate.Rows[0]["Regular Holiday Rest Day Night Diff Amount"].ToString());
                    fields.SetField("RegularHolidayRDNDOTYearToDate", YearToDate.Rows[0]["Regular Holiday Rest Day Night Diff OT Amount"].ToString());
                    fields.SetField("RegularOTYearToDate", YearToDate.Rows[0]["Regular Overtime Amount"].ToString());
                    fields.SetField("SpecialHolidayWorkYearToDate", YearToDate.Rows[0]["Special Holiday Amount"].ToString());
                    fields.SetField("SpecialHolidayNDYearToDate", YearToDate.Rows[0]["Special Holiday Night Diff Amount"].ToString());
                    fields.SetField("SpecialHolidayNDOTYearToDate", YearToDate.Rows[0]["Special Holiday Night Diff OT Amount"].ToString());
                    fields.SetField("SpecialHolidayOTYearToDate", YearToDate.Rows[0]["Special Holiday Overtime Amount"].ToString());
                    fields.SetField("SpecialHolidayRDOTYearToDate", YearToDate.Rows[0]["Special Holiday RD OT Amount"].ToString());
                    fields.SetField("SpecialHolidayRestDayWorkYearToDate", YearToDate.Rows[0]["Special Holiday Rest Day Amount"].ToString());
                    fields.SetField("SpecialHolidayRDNDYearToDate", YearToDate.Rows[0]["Special Holiday Rest Day ND Amount"].ToString());
                    fields.SetField("SpecialHolidayRDNDOTYearToDate", YearToDate.Rows[0]["Special Holiday Rest Day Night Diff OT Amount"].ToString());
                    fields.SetField("EmployeeSocialSecurityYearToDate", YearToDate.Rows[0]["SSS Employee Share"].ToString());
                    fields.SetField("SSSLoanDeductionYearToDate", YearToDate.Rows[0]["SSS Loan Amount"].ToString());
                    fields.SetField("TaxPayableYearToDate", YearToDate.Rows[0]["Tax Payable"].ToString());
                    fields.SetField("WithholdingTaxYearToDate", YearToDate.Rows[0]["W-Tax"].ToString());
                    fields.SetField("BonusYearToDate", YearToDate.Rows[0]["adjRegular"].ToString());


                }




                stamper.FormFlattening = true;
                // closing the stamper

                stamper.Close();


                return "Success";
            }
            catch (Exception e)
            {
                return "error:" + e;
            }
        }
        #region "Payslipmoto"
        [HttpPost]
        [Route("RetrievePayslips")]
        public string RetrievePayslips(UserProfilev2 uProfile)
        {

            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = uProfile.CN;
            SelfieRegistration2Controller.GetDB2(GDB);

            string DDLMonths = uProfile.PayMonth;
            string DDLPayPeriod = uProfile.PayDay;
            string DDLYear = uProfile.PayYear;
            empid = uProfile.NTID;

            // empname = PayMtd.searchempname(empid);

            // empidprof = empid;
            //GetEmpLevelandCategory = PayMtd.GetEmpLevelandCategory(empid);
            // string EmpLevel = GetEmpLevelandCategory.Rows[0][0].ToString();
            //string EmpCategory = GetEmpLevelandCategory.Rows[0][1].ToString();
            // paysliprecordsempmaster = PayMtd.PaySlipRecordsEmpMaster(empid);
            //paysliprecordsconfidentialinfo = PayMtd.PaySlipRecordsConfidentialInfo(empid);
            //paysliprecordsemployeepay = PayMtd.PaySlipRecordsEmployeePay(empid);
            //int schmct = PayMtd.GetSalarySchemes(EmpLevel, EmpCategory).Rows.Count;
            empidprof = empid;
            //GetEmpLevelandCategory = PayMtd.GetEmpLevelandCategory(empid);
            //string EmpLevel = GetEmpLevelandCategory.Rows[0][0].ToString();
            //string EmpCategory = GetEmpLevelandCategory.Rows[0][1].ToString();
            Connection con = new Connection();
            con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = empid });
            paysliprecordsempmaster = con.GetDataTable("sp_GetPaySlipRecordsEmpMaster");

            con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = empid });
            paysliprecordsconfidentialinfo = con.GetDataTable("sp_GetPaySlipRecordsConfidentialInfo");
            //paysliprecordsconfidentialinfo = PayMtd.PaySlipRecordsConfidentialInfo(empid);

            con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = empid });
            paysliprecordsemployeepay = con.GetDataTable("sp_GetPaySlipRecordsEmployeePay");
            //paysliprecordsemployeepay = PayMtd.PaySlipRecordsEmployeePay(empid);
            //int schmct = PayMtd.GetSalarySchemes(EmpLevel, EmpCategory).Rows.Count;

            #region "New Payslip"
            TotalValue = 0;
            int latestpayslipcounter = 0;

            con = new Connection();
            LatestPayslipForm = con.GetDataTable("sp_GetLatestPayslipForm");
            //LatestPayslipForm = PayMtd.LatestPayslipForm();
            latestpayslipcounter = LatestPayslipForm.Rows.Count;
            //con = new Connection();
            //con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = empid });
            //con.myparameters.Add(new myParameters { ParameterName = "@MONTH", mytype = SqlDbType.NVarChar, Value = DDLMonths.ToString() });
            //DataTable BonusPayoutMonths = con.GetDataTable("sp_GetBonusPayouts");
            // TotalValue = 0;
            //viewingBuble();
            //countingInbox();
            // int latestpayslipcounter = 0;
            //  LatestPayslipForm = PayMtd.LatestPayslipForm();
            latestpayslipcounter = LatestPayslipForm.Rows.Count;
            // DataTable BonusPayoutMonths = PayMtd.BonusPayoutMonths(empid, DDLMonths.ToString());
            string firstpayperiod = "", secondpayperiod = "", firstmonth, secondmonth, firstday, secondday, ppyear;
            // if (DDLPayPeriod.SelectedIndex > ((DDLPayPeriod.Items.Count - BonusPayoutMonths.Rows.Count) - 1))
            // {
            //   BonusPayoutPDF();
            // }
            // else
            // {
            try
            {
                #region Necessary Data
                GDB.ClientName = uProfile.CN;
                SelfieRegistration2Controller.GetDB2(GDB);
                //Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = empid });
                PayslipItems = con.GetDataTable("sp_GetPaySlipPayPeriodItems");

                if (PayslipItems.Rows.Count > 0)
                {

                    string[] payperiodarr = DDLPayPeriod.ToString().Split('-');
                    ppyear = DDLYear.ToString();
                    firstmonth = payperiodarr[1].ToString();
                    firstday = payperiodarr[2].ToString();
                    secondmonth = payperiodarr[4].ToString();
                    secondday = payperiodarr[5].ToString();
                    firstmonth = monthname(firstmonth);
                    secondmonth = monthname(secondmonth);
                    firstpayperiod = ppyear + '-' + firstmonth + '-' + firstday;
                    if ((firstmonth == "12" && secondmonth == "01") && (Convert.ToInt32(firstday) > Convert.ToInt32(secondday)))
                    {
                        secondpayperiod = (Convert.ToInt32(ppyear) + 1).ToString() + '-' + secondmonth + '-' + secondday;
                    }
                    else
                    {
                        secondpayperiod = ppyear + '-' + secondmonth + '-' + secondday;
                    }
                    payperiod = firstpayperiod + '-' + secondpayperiod;
                    con = new Connection();
                    con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = empid });
                    con.myparameters.Add(new myParameters { ParameterName = "@PAYPERIOD", mytype = SqlDbType.NVarChar, Value = DDLPayPeriod });
                    paysliprecordspayslipdetails = con.GetDataTable("sp_GetPaySlipRecordsPaySlipDetails");


                    con = new Connection();
                    con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = empid });
                    con.myparameters.Add(new myParameters { ParameterName = "@YEAR", mytype = SqlDbType.NVarChar, Value = DDLYear });
                    con.myparameters.Add(new myParameters { ParameterName = "@PAYPERIOD", mytype = SqlDbType.NVarChar, Value = DDLPayPeriod });
                    YearToDate = con.GetDataTable("sp_GetPaySlipRecordsPaySlipDetailsYearToDate");

                    //paysliprecordspayslipdetails = PayMtd.PaySlipRecordsPaySlipDetails(empid, (firstpayperiod + '-' + secondpayperiod));
                    // YearToDate = PayMtd.PayslipDetailsYearToDate(empid, ppyear, firstpayperiod + '-' + secondpayperiod);
                }
                #endregion
                latestpayslipcounter = 0;
                #region Fixed payslip

                //indx = DDLPayPeriod.SelectedIndex;
                //GetEmpLevelandCategory = mav.GetEmpLevelandCategory(empid);
                //string EmpLevel = GetEmpLevelandCategory.Rows[0][0].ToString();
                //string EmpCategory = GetEmpLevelandCategory.Rows[0][1].ToString();
                int psrpsd = 0;
                int pswh = 0;

                //paysliprecordspayperiod = mav.PaySlipRecordsPayPeriod(EmpLevel, EmpCategory);
                string payslipid = "";
                #region Old Existing Method            
                //if (paysliprecordspayperiod.Rows.Count > 0)
                //{
                //string ddlmonth = DDLMonths.SelectedValue.ToString();
                //string ddlyear = DDLYear.SelectedValue.ToString();
                //string stringtobesplitted = DDLPayPeriod.SelectedValue.ToString();
                //string[] words = stringtobesplitted.Split('-');
                //string firstpayperiodstart;
                //string firstpayperiodend;
                //if (words[0].ToString() == "EOM")
                //{
                //    int a, b;
                //    a = Convert.ToInt32(ddlmonth);
                //    b = Convert.ToInt32(ddlyear);
                //    int x = DateTime.DaysInMonth(b, a);
                //    firstpayperiodstart = x.ToString();
                //}
                //else
                //{
                //    firstpayperiodstart = words[0].ToString();
                //}
                //if (words[1].ToString() == "EOM")
                //{
                //    int a, b;
                //    a = Convert.ToInt32(ddlmonth);
                //    b = Convert.ToInt32(ddlyear);
                //    int x = DateTime.DaysInMonth(b, a);
                //    firstpayperiodend = x.ToString();
                //}
                //else
                //{
                //    firstpayperiodend = words[1].ToString();
                //}

                ////string firstpayperiodstart = paysliprecordspayperiod.Rows[indx][0].ToString();
                ////string firstpayperiodend = paysliprecordspayperiod.Rows[indx][1].ToString();
                //string bonusdayfrom = firstpayperiodstart;
                //string bonusdayto = firstpayperiodend;

                //string datefrom = ddlyear + '-' + ddlmonth + '-' + firstpayperiodstart;
                //string dateto = ddlyear + '-' + ddlmonth + '-' + firstpayperiodend;

                //if (firstpayperiodend == "End of month")
                //{
                //    int x = DateTime.DaysInMonth(Convert.ToInt32(ddlyear), Convert.ToInt32(ddlmonth));
                //    firstpayperiodend = x.ToString();
                //}

                //if (firstpayperiodstart == "1" || firstpayperiodstart == "2" || firstpayperiodstart == "3" || firstpayperiodstart == "4" || firstpayperiodstart == "5"
                //    || firstpayperiodstart == "6" || firstpayperiodstart == "7" || firstpayperiodstart == "8" || firstpayperiodstart == "9")
                //{
                //    hoursdatefrom = ddlyear + '-' + ddlmonth + '-' + ("0" + firstpayperiodstart);
                //}
                //else
                //{
                //    hoursdatefrom = ddlyear + '-' + ddlmonth + '-' + firstpayperiodstart;
                //}
                //if (firstpayperiodend == "1" || firstpayperiodend == "2" || firstpayperiodend == "3" || firstpayperiodend == "4" || firstpayperiodend == "5"
                //    || firstpayperiodend == "6" || firstpayperiodend == "7" || firstpayperiodend == "8" || firstpayperiodend == "9")
                //{
                //    hoursdateto = ddlyear + '-' + ddlmonth + '-' + ("0" + firstpayperiodend);
                //}
                //else if (firstpayperiodend == "31")
                //{
                //    if (ddlmonth == "02" || ddlmonth == "04" || ddlmonth == "06" || ddlmonth == "09" || ddlmonth == "11")
                //    {
                //        int x = DateTime.DaysInMonth(Convert.ToInt32(ddlyear), Convert.ToInt32(ddlmonth));
                //        hoursdateto = ddlyear + '-' + ddlmonth + '-' + x;
                //    }
                //    else
                //    {
                //        hoursdateto = ddlyear + '-' + ddlmonth + '-' + firstpayperiodend;
                //    }
                //}
                //else
                //{
                //    hoursdateto = ddlyear + '-' + ddlmonth + '-' + firstpayperiodend;
                //}

                //if (Convert.ToInt32(firstpayperiodstart) > Convert.ToInt32(firstpayperiodend))
                //{
                //    hoursdateto = PayMtd.PayPeriod(ddlyear, ddlmonth, firstpayperiodend);
                //}

                //payslipworkedhours = mav.PaySlipWorkedHours(empid, hoursdatefrom, hoursdateto);
                //pswh = payslipworkedhours.Rows.Count;
                //payperiod = hoursdatefrom + '-' + hoursdateto;                    
                //psrpsd = paysliprecordspayslipdetails.Rows.Count;
                //paysliprecordsallowanceamount = mav.getPayslipRecordsAllowanceAmount(empid);
                //paysliprecordsbonuses = mav.getPayslipRecordsBonuses(empid, datefrom + "/" + dateto);
                //paysliprecordscompbenempcatbonuses = mav.getPayslipCompBenEmpCatBonuses(EmpLevel, EmpCategory, hoursdatefrom, hoursdateto, bonusdayfrom, bonusdayto);
                //BonusPayoutMonth(empid, hoursdatefrom, hoursdateto);
                //CompBenDeminimis(empid, EmpCategory, hoursdatefrom, hoursdateto);
                //PayrollDisputes(empid, hoursdatefrom, hoursdateto);
                //SSSLoans(empid, hoursdatefrom, hoursdateto);
                //PayrollAdjustments(empid, hoursdatefrom + '-' + hoursdateto);
                //paysliprecordspayslipdetails = PayMtd.PaySlipRecordsPaySlipDetails(empid, payperiod);
                //YearToDate = PayMtd.PayslipDetailsYearToDate(empid, ddlyear, hoursdatefrom + '-' + hoursdateto);
                //}
                #endregion
                //Response.Redirect("~/pdf/BIR19021.pdf"); // view pdf
                ///string clientname;
                // clientname = HttpContext.Current.Request.Url.Authority;
                //clientname = "jldeleon";//TEST CLIENT NAME
                string path = HttpContext.Current.Server.MapPath("pdf");
                var pdfReader1 = new PdfReader(path + "/PaySlipDefault.pdf"); ;
                string formFile = path + "/PaySlipDefault.pdf";

                if (uProfile.CN.Contains("paat"))
                {
                    pdfReader1 = new PdfReader(path + "/PAAT_PAYSLIP.pdf");
                    formFile = path + "/PAAT_PAYSLIP.pdf";
                }
                else if (uProfile.CN.Contains("campaigntrack"))
                {
                    pdfReader1 = new PdfReader(path + "/PAYSLIP_CTI.pdf");
                    formFile = path + "/PAYSLIP_CTI.pdf";
                }
                else if (uProfile.CN.Contains("epiroc"))
                {
                    pdfReader1 = new PdfReader(path + "/PayslipEpiroc.pdf");
                    formFile = path + "/PayslipEpiroc.pdf";
                }
                else if (uProfile.CN.Contains("jldeleon"))
                {
                    pdfReader1 = new PdfReader(path + "/Payslip-JLdeLeon.pdf");
                    formFile = path + "/Payslip-JLdeLeon.pdf";
                }

                else if (uProfile.CN.Contains("amplify"))
                {
                    pdfReader1 = new PdfReader(path + "/PAYSLIP-AMPLIFY.pdf");
                    formFile = path + "/PAYSLIP-AMPLIFY.pdf";
                }

                else if (uProfile.CN.Contains("local"))
                {
                    pdfReader1 = new PdfReader(path + "/PAYSLIP-OP360.pdf");
                    formFile = path + "/PAYSLIP-OP360.pdf";
                }

                else if (uProfile.CN.Contains("perrys"))
                {
                    Connection conns = new Connection();
                    conns.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.NVarChar, Value = empid });
                    string branchname = conns.GetSingleRow("sp_GetEmployeeBranchName")[0].ToString();

                    if (branchname.Contains("TRANS-GLOBAL"))
                    {
                        pdfReader1 = new PdfReader(path + "/PAYSLIP-TRANSGLOBAL.pdf");
                        formFile = path + "/PAYSLIP-TRANSGLOBAL.pdf";
                    }
                    if (branchname.Contains("APT"))
                    {
                        pdfReader1 = new PdfReader(path + "/PAYSLIP-APT_TRAVEL.pdf");
                        formFile = path + "/PAYSLIP-APT_TRAVEL.pdf";
                    }
                    if (branchname.Contains("COLLYER"))
                    {
                        pdfReader1 = new PdfReader(path + "/PAYSLIP-COLLYER.pdf");
                        formFile = path + "/PAYSLIP-COLLYER.pdf";
                    }
                    if (branchname.Contains("SOLART"))
                    {
                        pdfReader1 = new PdfReader(path + "/PAYSLIP-SOLART.pdf");
                        formFile = path + "/PAYSLIP-SOLART.pdf";
                    }
                }

                AcroFields af = pdfReader1.AcroFields;
                //LocationTextExtractionStrategy lteStrategy = new LocationTextExtractionStrategy();
                //string strExact = PdfTextExtractor.GetTextFromPage(pdfReader1,1, lteStrategy);
                //StringBuilder sb = new StringBuilder();
                //string sam = "";
                //foreach (var field in af.Fields)
                //{
                //    sam = sam + field.Key + "-"; // names of textfields
                //}
                //viewpdf.InnerText = sam;
                // get the file paths


                Connection con2 = new Connection();
                // Output file path
                string newFile = path + "/PaySlipDefault-" + firstpayperiod + "-" + secondpayperiod + "" + payslipid + ".pdf";
                if (uProfile.CN.Contains("campaigntrack"))
                {

                    con2.myparameters.Clear();
                    con2.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = empid });
                    con2.myparameters.Add(new myParameters { ParameterName = "@PAYPERIOD", mytype = SqlDbType.NVarChar, Value = (firstpayperiod + '-' + secondpayperiod) });
                    DataTable paysliprecordspayslipdetails2 = con2.GetDataTable("sp_GetPaySlipRecordsPaySlipDetailsCTI");
                    payslipid = paysliprecordspayslipdetails2.Rows[0]["ID"].ToString();
                    newFile = path + "/PaySlipDefault-" + firstpayperiod + "-" + secondpayperiod + "" + payslipid + ".pdf";
                }
                else if (uProfile.CN.Contains("paat"))
                {

                    con2.myparameters.Clear();
                    con2.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = empid });
                    con2.myparameters.Add(new myParameters { ParameterName = "@PAYPERIOD", mytype = SqlDbType.NVarChar, Value = (firstpayperiod + '-' + secondpayperiod) });
                    DataTable paysliprecordspayslipdetails2 = con2.GetDataTable("sp_GetPaySlipRecordsPaySlipDetailsPAAT");
                    payslipid = paysliprecordspayslipdetails2.Rows[0]["ID"].ToString();
                    newFile = path + "/PaySlipDefault-" + firstpayperiod + "-" + secondpayperiod + "" + payslipid + ".pdf";
                }
                else if (uProfile.CN.Contains("amplify"))
                {

                    con2.myparameters.Clear();
                    con2.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = empid });
                    con2.myparameters.Add(new myParameters { ParameterName = "@PAYPERIOD", mytype = SqlDbType.NVarChar, Value = (firstpayperiod + '-' + secondpayperiod) });
                    DataTable paysliprecordspayslipdetails2 = con2.GetDataTable("sp_GetPaySlipRecordsPaySlipDetailsAmplify");
                    payslipid = paysliprecordspayslipdetails2.Rows[0]["ID"].ToString();
                    newFile = path + "/PaySlipDefault-" + firstpayperiod + "-" + secondpayperiod + "" + payslipid + ".pdf";
                }
                else if (uProfile.CN.Contains("epiroc"))
                {

                    con2.myparameters.Clear();
                    con2.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = empid });
                    con2.myparameters.Add(new myParameters { ParameterName = "@PAYPERIOD", mytype = SqlDbType.NVarChar, Value = DDLPayPeriod });
                    DataTable paysliprecordspayslipdetails2 = con2.GetDataTable("sp_GetPaySlipRecordsPaySlipDetailsEpiroc");
                    payslipid = paysliprecordspayslipdetails2.Rows[0]["ID"].ToString();
                    newFile = path + "/Payslip-" + empid + ".pdf";
                }
                else if (uProfile.CN.Contains("jldeleon"))
                {

                    con2.myparameters.Clear();
                    con2.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = empid });
                    con2.myparameters.Add(new myParameters { ParameterName = "@PAYPERIOD", mytype = SqlDbType.NVarChar, Value = (firstpayperiod + '-' + secondpayperiod) });
                    DataTable paysliprecordspayslipdetails2 = con2.GetDataTable("sp_GetPaySlipRecordsPaySlipDetailsJLdeleon");
                    payslipid = paysliprecordspayslipdetails2.Rows[0]["ID"].ToString();
                    newFile = path + "/PaySlipDefault-" + firstpayperiod + "-" + secondpayperiod + "" + payslipid + ".pdf";
                }
                // read the template file
                PdfReader reader = new PdfReader(formFile);


                // instantiate PDFStamper object
                // The Output file will be created from template file and edited by the PDFStamper
                PdfStamper stamper = new PdfStamper(reader, new FileStream(newFile, FileMode.Create));
                //Connection con3 = new Connection();
                //con3.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.VarChar, Value = empid });
                //string pdfpass = con3.ExecuteScalar("sp_getCurrentPassword");
                //stamper.SetEncryption(true, pdfpass, pdfpass, PdfWriter.ALLOW_SCREENREADERS);

                // Object to deal with the Output file's textfields
                AcroFields fields = stamper.AcroFields;

                // set form fields("Field Name in PDF", "String to be placed in this PDF text field");
                //for (int i = 0; i <= 1000; i++)
                //{
                //    string ii = i.ToString();
                //    
                //    fields.SetField("untitled" + i, "vt" + ii); //temporary values
                //}

                //------------------------------------------------
                //loop to display all field names
                if (paysliprecordsempmaster.Rows.Count > 0)
                {
                    //fields.SetFieldProperty("txtempid", "setflags", PdfAnnotation.FLAGS_HIDDEN,null);
                    fields.SetField("employeenumber", paysliprecordsempmaster.Rows[0][1].ToString());//Employee Numer
                    fields.SetField("EmployeeName", paysliprecordsempmaster.Rows[0][0].ToString());//Employee Name
                    fields.SetField("Position", "");//Position
                    fields.SetField("Class", "");//Class
                    fields.SetField("EmployeeNo", empid);//Employee No
                    fields.SetField("joined", paysliprecordsempmaster.Rows[0][2].ToString());//JoinedDate
                    fields.SetField("ProductionStartDate", paysliprecordsempmaster.Rows[0][2].ToString());//ProductionStartDate
                }
                else
                {

                }
                if (paysliprecordsconfidentialinfo.Rows.Count > 0)
                {
                    fields.SetField("pagibigno", paysliprecordsconfidentialinfo.Rows[0][0].ToString());//Pag-IBIG No
                    fields.SetField("philhealthno", paysliprecordsconfidentialinfo.Rows[0][1].ToString());//PhilHealth No
                    fields.SetField("sssno", paysliprecordsconfidentialinfo.Rows[0][2].ToString());//SSS No
                    fields.SetField("taxno", paysliprecordsconfidentialinfo.Rows[0][3].ToString());//Tax No
                    fields.SetField("Department", "");//Department
                    fields.SetField("DepartmentCode", "");//DepartmentCode
                    fields.SetField("taxcategory", paysliprecordsconfidentialinfo.Rows[0][4].ToString());//Tax Category
                }
                else
                {

                }
                if (paysliprecordsemployeepay.Rows.Count > 0)
                {
                    fields.SetField("bankaccount", paysliprecordsemployeepay.Rows[0][0].ToString());//Bank Account
                    fields.SetField("hourlyrate", paysliprecordsemployeepay.Rows[0][1].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordsemployeepay.Rows[0][1].ToString())));//Hourly Rate
                }
                else
                {
                    fields.SetField("hourlyrate", "0.00");
                }
                fields.SetField("payperiod", payperiod);//Pay Period

                if (paysliprecordspayslipdetails.Rows.Count > 0 && uProfile.CN.Contains("default"))
                {
                    fields.SetField("payday", paysliprecordspayslipdetails.Rows[0][1].ToString());//Pay Day
                    fields.SetField("basicsalarycurrent", paysliprecordspayslipdetails.Rows[0][2].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][2].ToString())));//Basic Salary
                    fields.SetField("nontaxableallowancecurrent", paysliprecordspayslipdetails.Rows[0][3].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][3].ToString())));//Non Taxable Allowance
                    fields.SetField("regularnightdiffcurrent", paysliprecordspayslipdetails.Rows[0][4].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][4].ToString())));//Regular Night Diff
                    fields.SetField("regularotcurrent", paysliprecordspayslipdetails.Rows[0][5].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][5].ToString())));//Regular Overtime
                    fields.SetField("regularholidaycurrent", paysliprecordspayslipdetails.Rows[0][6].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][6].ToString())));//Regular Holiday
                    fields.SetField("regularholidaynightdiffcurrent", paysliprecordspayslipdetails.Rows[0][7].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][7].ToString())));//Regular Holiday Night Diff
                    fields.SetField("regularholidayotcurrent", paysliprecordspayslipdetails.Rows[0][8].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][8].ToString())));//Regular Overtime
                    fields.SetField("paidtimeoffcurrent", paysliprecordspayslipdetails.Rows[0][9].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][9].ToString())));//Paid Time Off
                    fields.SetField("specialholidaycurrent", paysliprecordspayslipdetails.Rows[0][10].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][10].ToString())));//Special Holiday
                    fields.SetField("specialholidaynightdiffcurrent", paysliprecordspayslipdetails.Rows[0][11].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][11].ToString())));//Special Holiday Night Diff
                    fields.SetField("specialholidayotcurrent", paysliprecordspayslipdetails.Rows[0][12].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][12].ToString())));//Special Holiday OT
                    fields.SetField("allowancescurrent", paysliprecordspayslipdetails.Rows[0][13].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][13].ToString())));//Allowances
                    fields.SetField("bonuscurrent", paysliprecordspayslipdetails.Rows[0][14].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][14].ToString())));//Bonus
                    fields.SetField("adjustmentscurrent", paysliprecordspayslipdetails.Rows[0][15].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][15].ToString())));//adjustments
                    fields.SetField("totalcurrent", paysliprecordspayslipdetails.Rows[0][16].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][16].ToString())));//Gross Pay
                    fields.SetField("employeehdmfcurrent", paysliprecordspayslipdetails.Rows[0][17].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][17].ToString())));//Employee HDMF
                    fields.SetField("employeephilhealthcurrent", paysliprecordspayslipdetails.Rows[0][18].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][18].ToString())));//Employee PhilHealth
                    fields.SetField("employeesocialsecuritycurrent", paysliprecordspayslipdetails.Rows[0][19].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][19].ToString())));//Employee SSS
                    fields.SetField("withholdingtaxcurrent", paysliprecordspayslipdetails.Rows[0][20].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][20].ToString())));//Employee WithHoldingTax
                    fields.SetField("sssloandeductioncurrent", paysliprecordspayslipdetails.Rows[0][21].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][21].ToString())));//Loans SSS
                    fields.SetField("pagibigloandeductioncurrent", paysliprecordspayslipdetails.Rows[0][22].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][22].ToString())));//Loans Pagibig
                    fields.SetField("netpaycurrent", paysliprecordspayslipdetails.Rows[0][23].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][23].ToString())));//Net Pay
                    fields.SetField("employerhdmfcurrent", paysliprecordspayslipdetails.Rows[0][24].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][24].ToString())));//Employer HDMF
                    fields.SetField("employerphilhealthcurrent", paysliprecordspayslipdetails.Rows[0][25].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][25].ToString())));//Employer PhilHealth
                    fields.SetField("employersocialsecuritycurrent", paysliprecordspayslipdetails.Rows[0][26].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][26].ToString())));//Employer SSS
                    fields.SetField("employersocialsecurityeccurrent", paysliprecordspayslipdetails.Rows[0][27].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][27].ToString())));//Employer SSS EC

                    fields.SetField("deductionsabsencescurrent", paysliprecordspayslipdetails.Rows[0][28].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][28].ToString())));//Deductions Absences
                    fields.SetField("deductionslatescurrent", paysliprecordspayslipdetails.Rows[0][29].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][29].ToString())));//Deductions Lates
                    fields.SetField("doubleholidaycurrent", paysliprecordspayslipdetails.Rows[0][30].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][30].ToString())));//DH_REG_Amount
                    fields.SetField("doubleholidayndcurrent", paysliprecordspayslipdetails.Rows[0][31].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][31].ToString())));//DH_ND_Amount
                    fields.SetField("doubleholidayotcurrent", paysliprecordspayslipdetails.Rows[0][32].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][32].ToString())));//DH_OT_Amount
                }
                else
                {

                }

                if (YearToDate.Rows.Count > 0 && uProfile.CN.Contains("default"))
                {
                    fields.SetField("basicsalaryytd", YearToDate.Rows[0][0].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][0].ToString())));//Basic Salary
                    fields.SetField("nontaxableallowanceytd", YearToDate.Rows[0][1].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][1].ToString())));//Non Taxable Allowance
                    fields.SetField("regularnightdiffytd", YearToDate.Rows[0][2].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][2].ToString())));//Regular Night Diff
                    fields.SetField("regularotytd", YearToDate.Rows[0][3].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][3].ToString())));//Regular OT
                    fields.SetField("regularholidayytd", YearToDate.Rows[0][4].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][4].ToString())));//Regular Holiday
                    fields.SetField("regularholidaynightdiffytd", YearToDate.Rows[0][5].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][5].ToString())));//Regular Holiday Night Diff
                    fields.SetField("regularholidayotytd", YearToDate.Rows[0][6].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][6].ToString())));//Regular Holiday OT
                    fields.SetField("paidtimeoffytd", YearToDate.Rows[0][7].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][7].ToString())));//Paid Time Off
                    fields.SetField("specialholidayytd", YearToDate.Rows[0][8].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][8].ToString())));//Special Holiday
                    fields.SetField("specialholidaynightdiffytd", YearToDate.Rows[0][9].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][9].ToString())));//Special Holiday Night Diff
                    fields.SetField("specialholidayotytd", YearToDate.Rows[0][10].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][10].ToString())));//Special Holiday OT
                    fields.SetField("allowancesytd", YearToDate.Rows[0][11].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][11].ToString())));//Allowances
                    fields.SetField("bonusytd", YearToDate.Rows[0][12].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][12].ToString())));//Bonus
                    fields.SetField("adjustmentsytd", YearToDate.Rows[0][13].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][13].ToString())));//Adjustments
                    fields.SetField("totalytd", YearToDate.Rows[0][14].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][14].ToString())));//Gross Pay/Total
                    fields.SetField("employeehdmfytd", YearToDate.Rows[0][15].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][15].ToString())));//Employee HDMF
                    fields.SetField("employeephilhealthytd", YearToDate.Rows[0][16].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][16].ToString())));//Employee PhilHealth
                    fields.SetField("employeesocialsecurityytd", YearToDate.Rows[0][17].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][17].ToString())));//Employee Social Security
                    fields.SetField("withholdingtaxytd", YearToDate.Rows[0][18].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][18].ToString())));//WithHolding Tax
                    fields.SetField("sssloandeductionytd", YearToDate.Rows[0][19].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][19].ToString())));//SSS Loan Deduction
                    fields.SetField("pagibigloandeductionytd", YearToDate.Rows[0][20].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][20].ToString())));//Pag-ibig Loan Deduction
                    fields.SetField("netpayytd", YearToDate.Rows[0][21].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][21].ToString())));//Net Pay
                    fields.SetField("employerhdmfytd", YearToDate.Rows[0][22].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][22].ToString())));//Employer HDMF
                    fields.SetField("employerphilhealthytd", YearToDate.Rows[0][23].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][23].ToString())));//Employer PhilHealth
                    fields.SetField("employersocialsecurityytd", YearToDate.Rows[0][24].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][24].ToString())));//Employer Social Security
                    fields.SetField("employersocialsecurityecytd", YearToDate.Rows[0][25].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][25].ToString())));//Employer Social Security EC

                    fields.SetField("deductionsabsencesytd", paysliprecordspayslipdetails.Rows[0]["Absences"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Absences"].ToString())));//Deductions Absences
                    fields.SetField("deductionslatesytd", paysliprecordspayslipdetails.Rows[0]["Late"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Late"].ToString())));//Deductions Lates
                    fields.SetField("doubleholidayytd", paysliprecordspayslipdetails.Rows[0]["DH_REG_AMOUNT"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["DH_REG_AMOUNT"].ToString())));//DH_REG_Amount
                    fields.SetField("doubleholidayndytd", paysliprecordspayslipdetails.Rows[0]["DH_ND_AMOUNT"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["DH_ND_AMOUNT"].ToString())));//DH_ND_Amount
                    fields.SetField("doubleholidayotytd", paysliprecordspayslipdetails.Rows[0]["DH_OT_AMOUNT"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["DH_OT_AMOUNT"].ToString())));//DH_OT_Amount
                }
                else
                {

                }

                #region OP360
                if (uProfile.CN.Contains("op360"))
                {
                    if (paysliprecordsempmaster.Rows.Count > 0)
                    {
                        //fields.SetFieldProperty("txtempid", "setflags", PdfAnnotation.FLAGS_HIDDEN,null);
                        fields.SetField("employeeno", paysliprecordsempmaster.Rows[0][1].ToString());//Employee Numer
                        fields.SetField("employeename", paysliprecordsempmaster.Rows[0][0].ToString());//Employee Name
                        fields.SetField("Position", "");//Position
                        fields.SetField("Class", "");//Class
                        fields.SetField("EmployeeNo", empid);//Employee No
                        fields.SetField("joined", paysliprecordsempmaster.Rows[0][2].ToString());//JoinedDate
                        fields.SetField("ProductionStartDate", paysliprecordsempmaster.Rows[0][2].ToString());//ProductionStartDate
                    }
                    else
                    {

                    }
                    if (paysliprecordsconfidentialinfo.Rows.Count > 0)
                    {
                        fields.SetField("PagibigNo", paysliprecordsconfidentialinfo.Rows[0][0].ToString());//Pag-IBIG No
                        fields.SetField("PhilHealthNo", paysliprecordsconfidentialinfo.Rows[0][1].ToString());//PhilHealth No
                        fields.SetField("SSSNo", paysliprecordsconfidentialinfo.Rows[0][2].ToString());//SSS No
                        fields.SetField("TaxNo", paysliprecordsconfidentialinfo.Rows[0][3].ToString());//Tax No
                        fields.SetField("Department", "");//Department
                        fields.SetField("DepartmentCode", "");//DepartmentCode
                        fields.SetField("taxcategory", paysliprecordsconfidentialinfo.Rows[0][4].ToString());//Tax Category
                    }
                    else
                    {

                    }
                    if (paysliprecordsemployeepay.Rows.Count > 0)
                    {
                        fields.SetField("BankAccount", paysliprecordsemployeepay.Rows[0][0].ToString());//Bank Account
                        fields.SetField("HourlyRate", paysliprecordsemployeepay.Rows[0][1].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordsemployeepay.Rows[0][1].ToString())));//Hourly Rate
                    }
                    else
                    {
                        fields.SetField("HourlyRate", "0.00");
                    }
                    fields.SetField("PayPeriod", payperiod);//Pay Period
                    if (paysliprecordspayslipdetails.Rows.Count > 0)
                    {
                        fields.SetField("payday", paysliprecordspayslipdetails.Rows[0][1].ToString());//Pay Day
                        fields.SetField("basicsalarycurrent", paysliprecordspayslipdetails.Rows[0][2].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][2].ToString())));//Basic Salary
                        fields.SetField("nontaxableallowancecurrent", paysliprecordspayslipdetails.Rows[0][3].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][3].ToString())));//Non Taxable Allowance
                        fields.SetField("regularnightdiffcurrent", paysliprecordspayslipdetails.Rows[0][4].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][4].ToString())));//Regular Night Diff
                        fields.SetField("regularotcurrent", paysliprecordspayslipdetails.Rows[0][5].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][5].ToString())));//Regular Overtime
                        fields.SetField("regularholidaycurrent", paysliprecordspayslipdetails.Rows[0][6].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][6].ToString())));//Regular Holiday
                        fields.SetField("regularholidaynightdiffcurrent", paysliprecordspayslipdetails.Rows[0][7].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][7].ToString())));//Regular Holiday Night Diff
                        fields.SetField("regularholidayotcurrent", paysliprecordspayslipdetails.Rows[0][8].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][8].ToString())));//Regular Overtime
                        fields.SetField("paidtimeoffcurrent", paysliprecordspayslipdetails.Rows[0][9].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][9].ToString())));//Paid Time Off
                        fields.SetField("specialholidaycurrent", paysliprecordspayslipdetails.Rows[0][10].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][10].ToString())));//Special Holiday
                        fields.SetField("specialholidaynightdiffcurrent", paysliprecordspayslipdetails.Rows[0][11].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][11].ToString())));//Special Holiday Night Diff
                        fields.SetField("specialholidayotcurrent", paysliprecordspayslipdetails.Rows[0][12].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][12].ToString())));//Special Holiday OT
                        fields.SetField("allowancescurrent", paysliprecordspayslipdetails.Rows[0][13].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][13].ToString())));//Allowances
                        fields.SetField("bonuscurrent", paysliprecordspayslipdetails.Rows[0][14].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][14].ToString())));//Bonus
                        fields.SetField("adjustmentscurrent", paysliprecordspayslipdetails.Rows[0][15].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][15].ToString())));//adjustments
                        fields.SetField("totalcurrent", paysliprecordspayslipdetails.Rows[0][16].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][16].ToString())));//Gross Pay
                        fields.SetField("employeehdmfcurrent", paysliprecordspayslipdetails.Rows[0][17].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][17].ToString())));//Employee HDMF
                        fields.SetField("employeephilhealthcurrent", paysliprecordspayslipdetails.Rows[0][18].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][18].ToString())));//Employee PhilHealth
                        fields.SetField("employeesocialsecuritycurrent", paysliprecordspayslipdetails.Rows[0][19].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][19].ToString())));//Employee SSS
                        fields.SetField("withholdingtaxcurrent", paysliprecordspayslipdetails.Rows[0][20].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][20].ToString())));//Employee WithHoldingTax
                        fields.SetField("sssloandeductioncurrent", paysliprecordspayslipdetails.Rows[0][21].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][21].ToString())));//Loans SSS
                        fields.SetField("pagibigloandeductioncurrent", paysliprecordspayslipdetails.Rows[0][22].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][22].ToString())));//Loans Pagibig
                        fields.SetField("netpaycurrent", paysliprecordspayslipdetails.Rows[0][23].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][23].ToString())));//Net Pay
                        fields.SetField("employerhdmfcurrent", paysliprecordspayslipdetails.Rows[0][24].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][24].ToString())));//Employer HDMF
                        fields.SetField("employerphilhealthcurrent", paysliprecordspayslipdetails.Rows[0][25].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][25].ToString())));//Employer PhilHealth
                        fields.SetField("employersocialsecuritycurrent", paysliprecordspayslipdetails.Rows[0][26].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][26].ToString())));//Employer SSS
                        fields.SetField("employersocialsecurityeccurrent", paysliprecordspayslipdetails.Rows[0][27].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][27].ToString())));//Employer SSS EC

                        fields.SetField("deductionsabsencescurrent", paysliprecordspayslipdetails.Rows[0][28].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][28].ToString())));//Deductions Absences
                        fields.SetField("deductionslatescurrent", paysliprecordspayslipdetails.Rows[0][29].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][29].ToString())));//Deductions Lates
                        fields.SetField("doubleholidaycurrent", paysliprecordspayslipdetails.Rows[0][30].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][30].ToString())));//DH_REG_Amount
                        fields.SetField("doubleholidayndcurrent", paysliprecordspayslipdetails.Rows[0][31].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][31].ToString())));//DH_ND_Amount
                        fields.SetField("doubleholidayotcurrent", paysliprecordspayslipdetails.Rows[0][32].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0][32].ToString())));//DH_OT_Amount
                    }
                    else
                    {

                    }

                    if (YearToDate.Rows.Count > 0)
                    {
                        fields.SetField("basicsalaryyeartodate", YearToDate.Rows[0][0].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][0].ToString())));//Basic Salary
                        fields.SetField("nontaxableallowanceytd", YearToDate.Rows[0][1].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][1].ToString())));//Non Taxable Allowance
                        fields.SetField("nightdiffpremiumyeartodate", YearToDate.Rows[0][2].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][2].ToString())));//Regular Night Diff
                        fields.SetField("regularotyeartodate", YearToDate.Rows[0][3].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][3].ToString())));//Regular OT
                        fields.SetField("regularholidayyeartodate", YearToDate.Rows[0][4].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][4].ToString())));//Regular Holiday
                        fields.SetField("regularholidaynightdiffytd", YearToDate.Rows[0][5].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][5].ToString())));//Regular Holiday Night Diff
                        fields.SetField("regularholidayotyeartodate", YearToDate.Rows[0][6].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][6].ToString())));//Regular Holiday OT
                        fields.SetField("paidtimeoffytd", YearToDate.Rows[0][7].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][7].ToString())));//Paid Time Off
                        fields.SetField("specialholidayyeartodate", YearToDate.Rows[0][8].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][8].ToString())));//Special Holiday
                        fields.SetField("specialholidaynightdiffytd", YearToDate.Rows[0][9].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][9].ToString())));//Special Holiday Night Diff
                        fields.SetField("specialholidayotyeartodate", YearToDate.Rows[0][10].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][10].ToString())));//Special Holiday OT
                        fields.SetField("allowancesytd", YearToDate.Rows[0][11].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][11].ToString())));//Allowances
                        fields.SetField("bonusytd", YearToDate.Rows[0][12].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][12].ToString())));//Bonus
                        fields.SetField("adjustmentsytd", YearToDate.Rows[0][13].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][13].ToString())));//Adjustments
                        fields.SetField("grossincomeyeartodate", YearToDate.Rows[0][14].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][14].ToString())));//Gross Pay/Total
                        fields.SetField("employeehdmfytd", YearToDate.Rows[0][15].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][15].ToString())));//Employee HDMF
                        fields.SetField("employeephilhealthytd", YearToDate.Rows[0][16].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][16].ToString())));//Employee PhilHealth
                        fields.SetField("employeesocialsecurityytd", YearToDate.Rows[0][17].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][17].ToString())));//Employee Social Security
                        fields.SetField("withholdingtaxytd", YearToDate.Rows[0][18].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][18].ToString())));//WithHolding Tax
                        fields.SetField("sssloandeductionytd", YearToDate.Rows[0][19].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][19].ToString())));//SSS Loan Deduction
                        fields.SetField("pagibigloandeductionytd", YearToDate.Rows[0][20].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][20].ToString())));//Pag-ibig Loan Deduction
                        fields.SetField("netpayytd", YearToDate.Rows[0][21].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][21].ToString())));//Net Pay
                        fields.SetField("employerhdmfytd", YearToDate.Rows[0][22].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][22].ToString())));//Employer HDMF
                        fields.SetField("employerphilhealthytd", YearToDate.Rows[0][23].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][23].ToString())));//Employer PhilHealth
                        fields.SetField("employersocialsecurityytd", YearToDate.Rows[0][24].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][24].ToString())));//Employer Social Security
                        fields.SetField("employersocialsecurityecytd", YearToDate.Rows[0][25].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][25].ToString())));//Employer Social Security EC

                        fields.SetField("deductionsabsencesytd", paysliprecordspayslipdetails.Rows[0]["Absences"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Absences"].ToString())));//Deductions Absences
                        fields.SetField("deductionslatesytd", paysliprecordspayslipdetails.Rows[0]["Late"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Late"].ToString())));//Deductions Lates
                        fields.SetField("doubleholidayytd", paysliprecordspayslipdetails.Rows[0]["DH_REG_AMOUNT"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["DH_REG_AMOUNT"].ToString())));//DH_REG_Amount
                        fields.SetField("doubleholidayndytd", paysliprecordspayslipdetails.Rows[0]["DH_ND_AMOUNT"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["DH_ND_AMOUNT"].ToString())));//DH_ND_Amount
                        fields.SetField("doubleholidayotytd", paysliprecordspayslipdetails.Rows[0]["DH_OT_AMOUNT"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["DH_OT_AMOUNT"].ToString())));//DH_OT_Amount
                    }
                    else
                    {

                    }
                }
                #endregion
                // Connection con = new Connection();
                #region PAAT
                //clientname = "paat";
                if (uProfile.CN.Contains("paat"))
                {

                    con = new Connection();
                    con.myparameters.Add(new myParameters { ParameterName = "@empId", mytype = SqlDbType.NVarChar, Value = empid });
                    DataTable DTProfile = con.GetDataTable("sp_getCompleteProfile_Mobilev2");

                    con.myparameters.Clear();
                    con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = empid });
                    con.myparameters.Add(new myParameters { ParameterName = "@PAYPERIOD", mytype = SqlDbType.NVarChar, Value = (firstpayperiod + '-' + secondpayperiod) });
                    paysliprecordspayslipdetails = con.GetDataTable("sp_GetPaySlipRecordsPaySlipDetailsPAAT");

                    con.myparameters.Clear();
                    con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = empid });
                    con.myparameters.Add(new myParameters { ParameterName = "@PAYPERIOD", mytype = SqlDbType.NVarChar, Value = paysliprecordspayslipdetails.Rows[0]["Pay Day"].ToString() });
                    con.myparameters.Add(new myParameters { ParameterName = "@YEAR", mytype = SqlDbType.NVarChar, Value = DDLYear.ToString() });
                    YearToDate = con.GetDataTable("sp_GetPaySlipRecordsPaySlipDetailsYearToDatePAATv2");

                    if (DTProfile.Rows.Count > 0)
                    {

                    }

                    if (paysliprecordspayslipdetails.Rows.Count > 0)
                    {
                        fields.SetField("MonthlyRate", paysliprecordspayslipdetails.Rows[0]["Monthly Rate"].ToString());
                        fields.SetField("PayPeriod", paysliprecordspayslipdetails.Rows[0]["Pay Period"].ToString());
                        fields.SetField("PayDay", paysliprecordspayslipdetails.Rows[0]["Pay Day"].ToString());

                        //HOURS
                        fields.SetField("RegularHolidayHours", paysliprecordspayslipdetails.Rows[0]["Regular Holiday Hours"].ToString());
                        fields.SetField("RegularHolidayNDHours", paysliprecordspayslipdetails.Rows[0]["Regular Holiday ND Hours"].ToString());
                        fields.SetField("RegularHolidayOvertimeHours", paysliprecordspayslipdetails.Rows[0]["Regular Holiday Overtime Hours"].ToString());
                        fields.SetField("RegularHolidayRestDayHours", paysliprecordspayslipdetails.Rows[0]["Regular Holiday Rest Day Hours"].ToString());
                        fields.SetField("RegularHolidayRestDayNDHours", paysliprecordspayslipdetails.Rows[0]["Regular Holiday Rest Day ND Hours"].ToString());
                        fields.SetField("RegularHolidayRestDayOvertimeHours", paysliprecordspayslipdetails.Rows[0]["Regular Holiday Rest Day Overtime Hours"].ToString());
                        fields.SetField("RegularNDHours", paysliprecordspayslipdetails.Rows[0]["Regular ND Hours Hours"].ToString());
                        fields.SetField("RegularNDOvertimeHours", paysliprecordspayslipdetails.Rows[0]["Regular ND Overtime Hours"].ToString());
                        fields.SetField("RegularOvertimeHours", paysliprecordspayslipdetails.Rows[0]["Regular Overtime Hours"].ToString());
                        fields.SetField("RestDayHours", paysliprecordspayslipdetails.Rows[0]["Rest Day Hours"].ToString());
                        fields.SetField("RestDayNDHours", paysliprecordspayslipdetails.Rows[0]["Rest Day ND Hours"].ToString());
                        fields.SetField("RestDayNDOvertimeHours", paysliprecordspayslipdetails.Rows[0]["Rest Day ND Overtime Hours"].ToString());
                        fields.SetField("RestDayOvertimeHours", paysliprecordspayslipdetails.Rows[0]["Rest Day Overtime Hours"].ToString());
                        fields.SetField("SpecialHolidayHours", paysliprecordspayslipdetails.Rows[0]["Special Holiday Hours"].ToString());
                        fields.SetField("SpecialHolidayNDHours", paysliprecordspayslipdetails.Rows[0]["Special Holiday ND Hours"].ToString());
                        fields.SetField("SpecialHolidayOvertimeHours", paysliprecordspayslipdetails.Rows[0]["Special Holiday Overtime Hours"].ToString());
                        fields.SetField("SpecialHolidayRestDayHours", paysliprecordspayslipdetails.Rows[0]["Special Holiday Rest Day Hours"].ToString());
                        fields.SetField("SpecialHolidayRestDayOvertimeHours", paysliprecordspayslipdetails.Rows[0]["Special Holiday Rest Day Overtime Hours"].ToString());


                        ////CURRENT////
                        fields.SetField("13thMonthNonTaxableCurrent", paysliprecordspayslipdetails.Rows[0]["13th Month Non-taxable"].ToString());
                        fields.SetField("13thMonthTaxableCurrent", paysliprecordspayslipdetails.Rows[0]["13th Month Taxable"].ToString());
                        fields.SetField("DeductionsAbsencesCurrent", paysliprecordspayslipdetails.Rows[0]["Absences Amount"].ToString());
                        fields.SetField("AdjustedNDAmountCurrent", paysliprecordspayslipdetails.Rows[0]["Adjusted ND Amount"].ToString());
                        fields.SetField("AdjustedRDAmountCurrent", paysliprecordspayslipdetails.Rows[0]["Adjusted RD Amount"].ToString());
                        fields.SetField("AdjustedRDOTAmountCurrent", paysliprecordspayslipdetails.Rows[0]["Adjusted RDOT Amount"].ToString());

                        fields.SetField("AdjustedRegularAmountCurrent", paysliprecordspayslipdetails.Rows[0]["Adjusted Regular Amount"].ToString());
                        fields.SetField("AdjustedRegularHolidayAmountCurrent", paysliprecordspayslipdetails.Rows[0]["Adjusted Regular Holiday Amount"].ToString());
                        fields.SetField("AdjustedRegularHolidayNDAmountCurrent", paysliprecordspayslipdetails.Rows[0]["Adjusted Regular Holiday ND Amount"].ToString());
                        fields.SetField("AdjustedRegularHolidayOTAmountCurrent", paysliprecordspayslipdetails.Rows[0]["Adjusted Regular Holiday OT Amount"].ToString());
                        fields.SetField("AdjustedRegularOTAmountCurrent", paysliprecordspayslipdetails.Rows[0]["Adjusted Regular OT Amount"].ToString());
                        fields.SetField("AdjustedSpecialHolidayAmountCurrent", paysliprecordspayslipdetails.Rows[0]["Adjusted Special Holiday Amount"].ToString());
                        fields.SetField("AdjustedSpecialHolidayNDAmountCurrent", paysliprecordspayslipdetails.Rows[0]["Adjusted Special Holiday ND Amount"].ToString());
                        fields.SetField("AdjustedSpecialHolidayOTAmountCurrent", paysliprecordspayslipdetails.Rows[0]["Adjusted Special Holiday OT Amount"].ToString());
                        fields.SetField("AMIEarningCurrent", paysliprecordspayslipdetails.Rows[0]["AMI Earning"].ToString());
                        fields.SetField("BasicSalaryAmountCurrent", paysliprecordspayslipdetails.Rows[0]["Basic Salary Amount"].ToString());
                        fields.SetField("BonusCurrent", paysliprecordspayslipdetails.Rows[0]["Bonus"].ToString());
                        fields.SetField("COLACurrent", paysliprecordspayslipdetails.Rows[0]["COLA"].ToString());
                        fields.SetField("DeMinimisCurrent", paysliprecordspayslipdetails.Rows[0]["De Minimis"].ToString());
                        fields.SetField("DeductionAdvancesCurrent", paysliprecordspayslipdetails.Rows[0]["Deduction: Advances"].ToString());
                        fields.SetField("DeductionCoopCarLoanCurrent", paysliprecordspayslipdetails.Rows[0]["Deduction: Coop Car Loan"].ToString());
                        fields.SetField("DeductionCoopContributionCurrent", paysliprecordspayslipdetails.Rows[0]["Deduction: Coop Contribution"].ToString());
                        fields.SetField("DeductionCoopLoan1Current", paysliprecordspayslipdetails.Rows[0]["Deduction: Coop Loan 1"].ToString());
                        fields.SetField("DeductionHMOCurrent", paysliprecordspayslipdetails.Rows[0]["Deduction: HMO"].ToString());

                        fields.SetField("DeductionPagibigCurrent", paysliprecordspayslipdetails.Rows[0]["Deduction: Pag-ibig"].ToString());
                        fields.SetField("DeductionPagibigLoanAmountCurrent", paysliprecordspayslipdetails.Rows[0]["Deduction: Pagibig Loan Amount"].ToString());
                        fields.SetField("DeductionPhilHealthCurrent", paysliprecordspayslipdetails.Rows[0]["Deduction: PhilHealth"].ToString());
                        fields.SetField("DeductionSSSCurrent", paysliprecordspayslipdetails.Rows[0]["Deduction: SSS"].ToString());
                        fields.SetField("DeductionSSSLoanAmountCurrent", paysliprecordspayslipdetails.Rows[0]["Deduction: SSS Loan Amount"].ToString());
                        fields.SetField("DeductionWTaxCurrent", paysliprecordspayslipdetails.Rows[0]["Deduction: W-Tax"].ToString());

                        fields.SetField("EmployeeNo", paysliprecordspayslipdetails.Rows[0]["Employee No"].ToString());
                        fields.SetField("GrossPayCurrent", paysliprecordspayslipdetails.Rows[0]["Gross Pay"].ToString());
                        fields.SetField("HardshipAllowanceCurrent", paysliprecordspayslipdetails.Rows[0]["Hardship Allowance"].ToString());

                        fields.SetField("HousingAllowanceCurrent", paysliprecordspayslipdetails.Rows[0]["Housing Allowance"].ToString());

                        fields.SetField("LumpsumBonusRewardCurrent", paysliprecordspayslipdetails.Rows[0]["Lumpsum Bonus/Reward"].ToString());
                        fields.SetField("MedicalAllowanceCurrent", paysliprecordspayslipdetails.Rows[0]["Medical Allowance"].ToString());

                        fields.SetField("NetPayCurrent", paysliprecordspayslipdetails.Rows[0]["Net Pay"].ToString());
                        fields.SetField("NonTaxableConvertedLeavesAmountCurrent", paysliprecordspayslipdetails.Rows[0]["Non-taxable Converted Leaves Amount"].ToString());
                        fields.SetField("OtherNonTaxableAllowanceCurrent", paysliprecordspayslipdetails.Rows[0]["Other Non-Taxable Allowance"].ToString());
                        fields.SetField("OtherTaxableIncomeCurrent", paysliprecordspayslipdetails.Rows[0]["Other Taxable Income"].ToString());
                        fields.SetField("PagibigEmployerShareCurrent", paysliprecordspayslipdetails.Rows[0]["Pag-ibig Employer Share"].ToString());

                        fields.SetField("PhilHealthEmployerShareCurrent", paysliprecordspayslipdetails.Rows[0]["PhilHealth Employer Share"].ToString());


                        fields.SetField("ProductivityPayCurrent", paysliprecordspayslipdetails.Rows[0]["Productivity Pay"].ToString());
                        fields.SetField("RegularHolidayCurrent", paysliprecordspayslipdetails.Rows[0]["Regular Holiday"].ToString());
                        fields.SetField("RegularHolidayNDCurrent", paysliprecordspayslipdetails.Rows[0]["Regular Holiday ND"].ToString());
                        fields.SetField("RegularHolidayOvertimeCurrent", paysliprecordspayslipdetails.Rows[0]["Regular Holiday Overtime"].ToString());
                        fields.SetField("RegularHolidayRestDayCurrent", paysliprecordspayslipdetails.Rows[0]["Regular Holiday Rest Day"].ToString());
                        fields.SetField("RegularHolidayRestDayNDCurrent", paysliprecordspayslipdetails.Rows[0]["Regular Holiday Rest Day ND"].ToString());
                        fields.SetField("RegularHolidayRestDayOvertimeCurrent", paysliprecordspayslipdetails.Rows[0]["Regular Holiday Rest Day Overtime"].ToString());
                        fields.SetField("RegularNDCurrent", paysliprecordspayslipdetails.Rows[0]["Regular ND Hours"].ToString());
                        fields.SetField("RegularNDOvertimeCurrent", paysliprecordspayslipdetails.Rows[0]["Regular ND Overtime"].ToString());
                        fields.SetField("RegularOvertimeCurrent", paysliprecordspayslipdetails.Rows[0]["Regular Overtime"].ToString());
                        fields.SetField("RestDayCurrent", paysliprecordspayslipdetails.Rows[0]["Rest Day"].ToString());
                        fields.SetField("RestDayNDCurrent", paysliprecordspayslipdetails.Rows[0]["Rest Day ND"].ToString());
                        fields.SetField("RestDayNDOvertimeCurrent", paysliprecordspayslipdetails.Rows[0]["Rest Day ND Overtime"].ToString());
                        fields.SetField("RestDayOvertimeCurrent", paysliprecordspayslipdetails.Rows[0]["Rest Day Overtime"].ToString());
                        fields.SetField("RiceAllowanceCurrent", paysliprecordspayslipdetails.Rows[0]["Rice Allowance"].ToString());
                        fields.SetField("SpecialHolidayCurrent", paysliprecordspayslipdetails.Rows[0]["Special Holiday"].ToString());
                        fields.SetField("SpecialHolidayNDCurrent", paysliprecordspayslipdetails.Rows[0]["Special Holiday ND"].ToString());
                        fields.SetField("SpecialHolidayOvertimeCurrent", paysliprecordspayslipdetails.Rows[0]["Special Holiday Overtime"].ToString());
                        fields.SetField("SpecialHolidayRestDayCurrent", paysliprecordspayslipdetails.Rows[0]["Special Holiday Rest Day"].ToString());

                        fields.SetField("SpecialHolidayRestdayOvertimeCurrent", paysliprecordspayslipdetails.Rows[0]["Special Holiday Rest Day Overtime"].ToString());
                        fields.SetField("SSSEmployerECShareCurrent", paysliprecordspayslipdetails.Rows[0]["SSS Employer EC Share"].ToString());
                        fields.SetField("SSSEmployerShareCurrent", paysliprecordspayslipdetails.Rows[0]["SSS Employer Share"].ToString());
                        fields.SetField("TaxableConvertedLeavesCurrent", paysliprecordspayslipdetails.Rows[0]["Taxable Converted Leaves"].ToString());
                        fields.SetField("TaxableIncomeCurrent", paysliprecordspayslipdetails.Rows[0]["Taxable Income"].ToString());
                        fields.SetField("TransportationAllowanceCurrent", paysliprecordspayslipdetails.Rows[0]["Transportation Allowance"].ToString());






                        fields.SetField("SSSNo", DTProfile.Rows[0]["SSS"].ToString());
                        fields.SetField("PagibigNo", DTProfile.Rows[0]["Pagibig"].ToString());
                        fields.SetField("PhilHealthNo", DTProfile.Rows[0]["PhilHealth"].ToString());
                        fields.SetField("TaxNo", DTProfile.Rows[0]["TIN"].ToString());
                        fields.SetField("EmployeeName", DTProfile.Rows[0]["Emp_Name"].ToString());
                        fields.SetField("HourlyRate", DTProfile.Rows[0]["HourlyRate"].ToString());
                        fields.SetField("Position", DTProfile.Rows[0]["Employee_Level"].ToString());
                        fields.SetField("JoinedDate", DTProfile.Rows[0]["DateJoined"].ToString());
                    }


                    if (YearToDate.Rows.Count > 0)
                    {

                        ////YEAR TO DATE////
                        fields.SetField("13thMonthNonTaxableYearToDate", YearToDate.Rows[0]["13th Month Non-taxable"].ToString());
                        fields.SetField("13thMonthTaxableYearToDate", YearToDate.Rows[0]["13th Month Taxable"].ToString());
                        fields.SetField("DeductionsAbsencesYearToDate", YearToDate.Rows[0]["Absences Amount"].ToString());
                        fields.SetField("AdjustedNDAmountYearToDate", YearToDate.Rows[0]["Adjusted ND Amount"].ToString());
                        fields.SetField("AdjustedRDAmountYearToDate", YearToDate.Rows[0]["Adjusted RD Amount"].ToString());
                        fields.SetField("AdjustedRDOTAmountYearToDate", YearToDate.Rows[0]["Adjusted RDOT Amount"].ToString());

                        fields.SetField("AdjustedRegularAmountYearToDate", YearToDate.Rows[0]["Adjusted Regular Amount"].ToString());
                        fields.SetField("AdjustedRegularHolidayAmountYearToDate", YearToDate.Rows[0]["Adjusted Regular Holiday Amount"].ToString());
                        fields.SetField("AdjustedRegularHolidayNDAmountYearToDate", YearToDate.Rows[0]["Adjusted Regular Holiday ND Amount"].ToString());
                        fields.SetField("AdjustedRegularHolidayOTAmountYearToDate", YearToDate.Rows[0]["Adjusted Regular Holiday OT Amount"].ToString());
                        fields.SetField("AdjustedRegularOTAmountYearToDate", YearToDate.Rows[0]["Adjusted Regular OT Amount"].ToString());
                        fields.SetField("AdjustedSpecialHolidayAmountYearToDate", YearToDate.Rows[0]["Adjusted Special Holiday Amount"].ToString());
                        fields.SetField("AdjustedSpeciaHolidayNDAmountYearToDate", YearToDate.Rows[0]["Adjusted Special Holiday ND Amount"].ToString());
                        fields.SetField("AdjustedSpecialHolidayOTAmountYearToDate", YearToDate.Rows[0]["Adjusted Special Holiday OT Amount"].ToString());
                        fields.SetField("AMIEarningYearToDate", YearToDate.Rows[0]["AMI Earning"].ToString());
                        fields.SetField("BasicSalaryAmountYearToDate", YearToDate.Rows[0]["Basic Salary Amount"].ToString());
                        fields.SetField("BonusYearToDate", YearToDate.Rows[0]["Bonus"].ToString());
                        fields.SetField("COLAYearToDate", YearToDate.Rows[0]["COLA"].ToString());
                        fields.SetField("DeMinimisYearToDate", YearToDate.Rows[0]["De Minimis"].ToString());
                        fields.SetField("DeductionAdvancesYearToDate", YearToDate.Rows[0]["Deduction: Advances"].ToString());
                        fields.SetField("DeductionCoopCarLoanYearToDate", YearToDate.Rows[0]["Deduction: Coop Car Loan"].ToString());
                        fields.SetField("DeductionCoopContributionYearToDate", YearToDate.Rows[0]["Deduction: Coop Contribution"].ToString());
                        fields.SetField("DeductionCoopLoan1YearToDate", YearToDate.Rows[0]["Deduction: Coop Loan 1"].ToString());
                        fields.SetField("DeductionHMOYearToDate", YearToDate.Rows[0]["Deduction: HMO"].ToString());

                        fields.SetField("DeductionPagibigYearToDate", YearToDate.Rows[0]["Deduction: Pag-ibig"].ToString());
                        fields.SetField("DeductionPagibigLoanAmountYearToDate", YearToDate.Rows[0]["Deduction: Pagibig Loan Amount"].ToString());
                        fields.SetField("DeductionPhilHealthYearToDate", YearToDate.Rows[0]["Deduction: PhilHealth"].ToString());
                        fields.SetField("DeductionSSSYearToDate", YearToDate.Rows[0]["Deduction: SSS"].ToString());
                        fields.SetField("DeductionSSSLoanAmountYearToDate", YearToDate.Rows[0]["Deduction: SSS Loan Amount"].ToString());
                        fields.SetField("DeductionWTaxYearToDate", YearToDate.Rows[0]["Deduction: W-Tax"].ToString());


                        fields.SetField("GrossPayYearToDate", YearToDate.Rows[0]["Gross Pay"].ToString());
                        fields.SetField("HardshipAllowanceYearToDate", YearToDate.Rows[0]["Hardship Allowance"].ToString());

                        fields.SetField("HousingAllowanceYearToDate", YearToDate.Rows[0]["Housing Allowance"].ToString());
                        fields.SetField("LumpsumBonusRewardYearToDate", YearToDate.Rows[0]["Lumpsum Bonus/Reward"].ToString());
                        fields.SetField("MedicalAllowanceYearToDate", YearToDate.Rows[0]["Medical Allowance"].ToString());
                        fields.SetField("NetPayYearToDate", YearToDate.Rows[0]["Net Pay"].ToString());
                        fields.SetField("NonTaxableConvertedLeavesAmountYearToDate", YearToDate.Rows[0]["Non-taxable Converted Leaves Amount"].ToString());
                        fields.SetField("OtherNonTaxableAllowanceYearToDate", YearToDate.Rows[0]["Other Non-Taxable Allowance"].ToString());
                        fields.SetField("OtherTaxableIncomeYearToDate", YearToDate.Rows[0]["Other Taxable Income"].ToString());
                        fields.SetField("PagibigEmployerShareYearToDate", YearToDate.Rows[0]["Pag-ibig Employer Share"].ToString());
                        fields.SetField("PhilHealthEmployerShareYearToDate", YearToDate.Rows[0]["PhilHealth Employer Share"].ToString());
                        fields.SetField("ProductivityPayYearToDate", YearToDate.Rows[0]["Productivity Pay"].ToString());
                        fields.SetField("RegularHolidayYearToDate", YearToDate.Rows[0]["Regular Holiday"].ToString());
                        fields.SetField("RegularHolidayNDYearToDate", YearToDate.Rows[0]["Regular Holiday ND"].ToString());
                        fields.SetField("RegularHolidayOvertimeYearToDate", YearToDate.Rows[0]["Regular Holiday Overtime"].ToString());
                        fields.SetField("RegularHolidayRestDayYearToDate", YearToDate.Rows[0]["Regular Holiday Rest Day"].ToString());
                        fields.SetField("RegularHolidayRestDayNDYearToDate", YearToDate.Rows[0]["Regular Holiday Rest Day ND"].ToString());
                        fields.SetField("RegularHolidayRestDayOvertimeYearToDate", YearToDate.Rows[0]["Regular Holiday Rest Day Overtime"].ToString());
                        fields.SetField("RegularNDYearToDate", YearToDate.Rows[0]["Regular ND Hours"].ToString());
                        fields.SetField("RegularNDOvertimeYearToDate", YearToDate.Rows[0]["Regular ND Overtime"].ToString());
                        fields.SetField("RegularOvertimeYearToDate", YearToDate.Rows[0]["Regular Overtime"].ToString());
                        fields.SetField("RestDayYearToDate", YearToDate.Rows[0]["Rest Day"].ToString());
                        fields.SetField("RestDayNDYearToDate", YearToDate.Rows[0]["Rest Day ND"].ToString());
                        fields.SetField("RestDayNDOvertimeYearToDate", YearToDate.Rows[0]["Rest Day ND Overtime"].ToString());
                        fields.SetField("RestDayOvertimeYearToDate", YearToDate.Rows[0]["Rest Day Overtime"].ToString());
                        fields.SetField("RiceAllowanceYearToDate", YearToDate.Rows[0]["Rice Allowance"].ToString());
                        fields.SetField("SpecialHolidayYearToDate", YearToDate.Rows[0]["Special Holiday"].ToString());
                        fields.SetField("SpecialHolidayNDYearToDate", YearToDate.Rows[0]["Special Holiday ND"].ToString());
                        fields.SetField("SpecialHolidayOvertimeYearToDate", YearToDate.Rows[0]["Special Holiday Overtime"].ToString());
                        fields.SetField("SpecialHolidayRestDayYearToDate", YearToDate.Rows[0]["Special Holiday Rest Day"].ToString());
                        fields.SetField("SpecialHolidayRestDayOvertimeYearToDate", YearToDate.Rows[0]["Special Holiday Rest Day Overtime"].ToString());
                        fields.SetField("SSSEmployerECShareYearToDate", YearToDate.Rows[0]["SSS Employer EC Share"].ToString());
                        fields.SetField("SSSEmployerShareYearToDate", YearToDate.Rows[0]["SSS Employer Share"].ToString());
                        fields.SetField("TaxableConvertedLeavesYearToDate", YearToDate.Rows[0]["Taxable Converted Leaves"].ToString());
                        fields.SetField("TaxableIncomeYearToDate", YearToDate.Rows[0]["Taxable Income"].ToString());
                        fields.SetField("TransportationAllowanceYearToDate", YearToDate.Rows[0]["Transportation Allowance"].ToString());


                    }
                }
                #endregion

                #region CampaignTrack
                //clientname = "paat";
                if (uProfile.CN.Contains("campaigntrack"))
                {

                    con = new Connection();
                    con.myparameters.Add(new myParameters { ParameterName = "@empId", mytype = SqlDbType.NVarChar, Value = empid });
                    DataTable DTProfile = con.GetDataTable("sp_getCompleteProfile_Mobilev2");

                    con.myparameters.Clear();
                    con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = empid });
                    con.myparameters.Add(new myParameters { ParameterName = "@PAYPERIOD", mytype = SqlDbType.NVarChar, Value = (firstpayperiod + '-' + secondpayperiod) });
                    paysliprecordspayslipdetails = con.GetDataTable("sp_GetPaySlipRecordsPaySlipDetailsCTI");

                    con.myparameters.Clear();
                    con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = empid });
                    con.myparameters.Add(new myParameters { ParameterName = "@PAYPERIOD", mytype = SqlDbType.NVarChar, Value = paysliprecordspayslipdetails.Rows[0]["Pay Day"].ToString() });
                    con.myparameters.Add(new myParameters { ParameterName = "@YEAR", mytype = SqlDbType.NVarChar, Value = DDLYear.ToString() });
                    YearToDate = con.GetDataTable("sp_GetPaySlipRecordsPaySlipDetailsYearToDateCTI");

                    if (DTProfile.Rows.Count > 0)
                    {
                        fields.SetField("MonthlyRate", DTProfile.Rows[0]["Salary"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(DTProfile.Rows[0]["Salary"].ToString())));
                    }

                    if (paysliprecordspayslipdetails.Rows.Count > 0)
                    {
                        payslipid = paysliprecordspayslipdetails.Rows[0]["ID"].ToString();
                        ////CURRENT////
                        fields.SetField("AbsencesCurrent", paysliprecordspayslipdetails.Rows[0]["Absences Amount"].ToString());
                        fields.SetField("ActingAllowanceCurrent", paysliprecordspayslipdetails.Rows[0]["Acting Allowance"].ToString());
                        fields.SetField("AdditionalHDMFContributionCurrent", paysliprecordspayslipdetails.Rows[0]["Additional HDMF Contribution"].ToString());
                        fields.SetField("AdjustmentsCurrent", paysliprecordspayslipdetails.Rows[0]["Adjusted Regular Amount (all)"].ToString());
                        fields.SetField("AUBSalaryLoanCurrent", paysliprecordspayslipdetails.Rows[0]["AUB Salary Loan"].ToString());
                        fields.SetField("BasicSalaryCurrent", paysliprecordspayslipdetails.Rows[0]["Basic Salary Amount"].ToString());
                        fields.SetField("CompanyLoanDeductionCurrent", paysliprecordspayslipdetails.Rows[0]["Company Loan"].ToString());
                        fields.SetField("EyeglassesDeductionCurrent", paysliprecordspayslipdetails.Rows[0]["Eyeglasses"].ToString());
                        fields.SetField("GrossEarningsCurrent", paysliprecordspayslipdetails.Rows[0]["Gross Pay"].ToString());
                        fields.SetField("HPVVaccineDeductionCurrent", paysliprecordspayslipdetails.Rows[0]["HPV Vaccine"].ToString());
                        fields.SetField("IntellicareDeductionCurrent", paysliprecordspayslipdetails.Rows[0]["Intellicare"].ToString());
                        fields.SetField("MonthlyRate", paysliprecordspayslipdetails.Rows[0]["Monthly Salary Rate"].ToString());
                        fields.SetField("RegularNightDiffCurrent", paysliprecordspayslipdetails.Rows[0]["ND Amount"].ToString());
                        fields.SetField("RegularNightDiffHours", paysliprecordspayslipdetails.Rows[0]["ND hours"].ToString());
                        fields.SetField("RegularNightDiffOTCurrent", paysliprecordspayslipdetails.Rows[0]["ND OT Amount"].ToString());
                        fields.SetField("RegularNightDiffOTHours", paysliprecordspayslipdetails.Rows[0]["ND OT hours"].ToString());
                        fields.SetField("NetPayCurrent", paysliprecordspayslipdetails.Rows[0]["Net Pay"].ToString());
                        fields.SetField("EmployeeHDMFCurrent", paysliprecordspayslipdetails.Rows[0]["Pag-ibig Employee Share"].ToString());
                        fields.SetField("PagibigLoanDeductionCurrent", paysliprecordspayslipdetails.Rows[0]["Pagibig Loan Amount"].ToString());
                        fields.SetField("PayDay", paysliprecordspayslipdetails.Rows[0]["Pay Day"].ToString());
                        fields.SetField("PayPeriod", paysliprecordspayslipdetails.Rows[0]["Pay Period"].ToString());
                        fields.SetField("EmployeePhilHealthCurrent", paysliprecordspayslipdetails.Rows[0]["PhilHealth Employee Share"].ToString());
                        fields.SetField("RestDayWorkCurrent", paysliprecordspayslipdetails.Rows[0]["RD Amount"].ToString());
                        fields.SetField("RestDayWorkHours", paysliprecordspayslipdetails.Rows[0]["RD Hours"].ToString());
                        fields.SetField("RestDayNDCurrent", paysliprecordspayslipdetails.Rows[0]["RD ND OT Amount"].ToString());
                        fields.SetField("RestDayNDHours", paysliprecordspayslipdetails.Rows[0]["RD ND OT Hours"].ToString());
                        fields.SetField("RestDayNDCurrent", paysliprecordspayslipdetails.Rows[0]["RDND Amount"].ToString());
                        fields.SetField("RestDayNDHours", paysliprecordspayslipdetails.Rows[0]["RDND Hours"].ToString());
                        fields.SetField("RestDayOTCurrent", paysliprecordspayslipdetails.Rows[0]["RDOT Amount"].ToString());
                        fields.SetField("RestDayOTHours", paysliprecordspayslipdetails.Rows[0]["RDOT Hours"].ToString());
                        fields.SetField("RegularHolidayWorkCurrent", paysliprecordspayslipdetails.Rows[0]["Regular Holiday Amount"].ToString());
                        fields.SetField("RegularHolidayWorkHours", paysliprecordspayslipdetails.Rows[0]["Regular Holiday Hours"].ToString());
                        fields.SetField("RegularHolidayNDCurrent", paysliprecordspayslipdetails.Rows[0]["Regular Holiday Night Diff Amount"].ToString());
                        fields.SetField("RegularHolidayNDHours", paysliprecordspayslipdetails.Rows[0]["Regular Holiday Night Diff Hours"].ToString());
                        fields.SetField("RegularHolidayNDOTCurrent", paysliprecordspayslipdetails.Rows[0]["Regular Holiday Night Diff OT Amount"].ToString());
                        fields.SetField("RegularHolidayNDOTHours", paysliprecordspayslipdetails.Rows[0]["Regular Holiday Night Diff OT Hours"].ToString());
                        fields.SetField("RegularHolidayOTCurrent", paysliprecordspayslipdetails.Rows[0]["Regular Holiday Overtime Amount"].ToString());
                        fields.SetField("RegularHolidayOTHours", paysliprecordspayslipdetails.Rows[0]["Regular Holiday Overtime Hours"].ToString());
                        fields.SetField("RegularHolidayRDOTCurrent", paysliprecordspayslipdetails.Rows[0]["Regular Holiday RDOT Amount"].ToString());
                        fields.SetField("RegularHolidayRDOTHours", paysliprecordspayslipdetails.Rows[0]["Regular Holiday RDOT Hours"].ToString());
                        fields.SetField("RegularHolidayRestDayWorkCurrent", paysliprecordspayslipdetails.Rows[0]["Regular Holiday Rest Day Amount"].ToString());
                        fields.SetField("RegularHolidayRestDayWorkHours", paysliprecordspayslipdetails.Rows[0]["Regular Holiday Rest Day Hours"].ToString());
                        fields.SetField("RegularHolidayRDNDCurrent", paysliprecordspayslipdetails.Rows[0]["Regular Holiday Rest Day Night Diff Amount"].ToString());
                        fields.SetField("RegularHolidayRDNDHours", paysliprecordspayslipdetails.Rows[0]["Regular Holiday Rest Day Night Diff Hours"].ToString());
                        fields.SetField("RegularHolidayRDNDOTCurrent", paysliprecordspayslipdetails.Rows[0]["Regular Holiday Rest Day Night Diff OT Amount"].ToString());
                        fields.SetField("RegularHolidayRDNDOTHours", paysliprecordspayslipdetails.Rows[0]["Regular Holiday Rest Day Night Diff OT Hours"].ToString());
                        fields.SetField("RegularOTCurrent", paysliprecordspayslipdetails.Rows[0]["Regular Overtime Amount"].ToString());
                        fields.SetField("RegularOTHours", paysliprecordspayslipdetails.Rows[0]["Regular Overtime Hours"].ToString());
                        fields.SetField("SpecialHolidayWorkCurrent", paysliprecordspayslipdetails.Rows[0]["Special Holiday Amount"].ToString());
                        fields.SetField("SpecialHolidayWorkHours", paysliprecordspayslipdetails.Rows[0]["Special Holiday Hours"].ToString());
                        fields.SetField("SpecialHolidayNDCurrent", paysliprecordspayslipdetails.Rows[0]["Special Holiday Night Diff Amount"].ToString());
                        fields.SetField("SpecialHolidayNDHours", paysliprecordspayslipdetails.Rows[0]["Special Holiday Night Diff Hours"].ToString());
                        fields.SetField("SpecialHolidayNDOTCurrent", paysliprecordspayslipdetails.Rows[0]["Special Holiday Night Diff OT Amount"].ToString());
                        fields.SetField("SpecialHolidayNDOTHours", paysliprecordspayslipdetails.Rows[0]["Special Holiday Night Diff OT Hours"].ToString());
                        fields.SetField("SpecialHolidayOTCurrent", paysliprecordspayslipdetails.Rows[0]["Special Holiday Overtime Amount"].ToString());
                        fields.SetField("SpecialHolidayOTHours", paysliprecordspayslipdetails.Rows[0]["Special Holiday Overtime Hours"].ToString());
                        fields.SetField("SpecialHolidayRDOTCurrent", paysliprecordspayslipdetails.Rows[0]["Special Holiday RD OT Amount"].ToString());
                        fields.SetField("SpecialHolidayRDOTHours", paysliprecordspayslipdetails.Rows[0]["Special Holiday RD OT Hours"].ToString());
                        fields.SetField("SpecialHolidayRestDayWorkCurrent", paysliprecordspayslipdetails.Rows[0]["Special Holiday Rest Day Amount"].ToString());
                        fields.SetField("SpecialHolidayRestDayWorkHours", paysliprecordspayslipdetails.Rows[0]["Special Holiday Rest Day Hours"].ToString());
                        fields.SetField("SpecialHolidayRDNDCurrent", paysliprecordspayslipdetails.Rows[0]["Special Holiday Rest Day ND Amount"].ToString());
                        fields.SetField("SpecialHolidayRDNDHours", paysliprecordspayslipdetails.Rows[0]["Special Holiday Rest Day ND Hours"].ToString());
                        fields.SetField("SpecialHolidayRDNDOTCurrent", paysliprecordspayslipdetails.Rows[0]["Special Holiday Rest Day Night Diff OT Amount"].ToString());
                        fields.SetField("SpecialHolidayRDNDOTHours", paysliprecordspayslipdetails.Rows[0]["Special Holiday Rest Day Night Diff OT Hours"].ToString());
                        fields.SetField("EmployeeSocialSecurityCurrent", paysliprecordspayslipdetails.Rows[0]["SSS Employee Share"].ToString());
                        fields.SetField("SSSLoanDeductionCurrent", paysliprecordspayslipdetails.Rows[0]["SSS Loan Amount"].ToString());
                        fields.SetField("TaxPayableCurrent", paysliprecordspayslipdetails.Rows[0]["Tax Payable"].ToString());
                        fields.SetField("WithholdingTaxCurrent", paysliprecordspayslipdetails.Rows[0]["W-Tax"].ToString());


                        //zeroes
                        fields.SetField("13thMonthNonTaxableCurrent", "0.00");
                        fields.SetField("13thMonthNonTaxableYearToDate", "0.00");
                        fields.SetField("13thMonthTaxableCurrent", "0.00");
                        fields.SetField("13thMonthTaxableYearToDate", "0.00");
                        fields.SetField("AdjustedNDAmountCurrent", "0.00");
                        fields.SetField("AdjustedNDAmountYearToDate", "0.00");
                        fields.SetField("AdjustedRDAmountCurrent", "0.00");
                        fields.SetField("AdjustedRDAmountYearToDate", "0.00");
                        fields.SetField("AdjustedRDOTAmountCurrent", "0.00");
                        fields.SetField("AdjustedRDOTAmountYearToDate", "0.00");
                        fields.SetField("AdjustedRDOTNDAmountCurrent", "0.00");
                        fields.SetField("AdjustedRDOTNDAmountYearToDate", "0.00");
                        fields.SetField("AdjustedRegularAmountCurrent", "0.00");
                        fields.SetField("AdjustedRegularAmountYearToDate", "0.00");
                        fields.SetField("AdjustedRegularOTAmountCurrent", "0.00");
                        fields.SetField("AdjustedRegularOTAmountYearToDate", "0.00");
                        fields.SetField("AdjustedSpeciaHolidayNDAmountYearToDate", "0.00");
                        fields.SetField("BonusCurrent", "0.00");
                        fields.SetField("BonusYearToDate", "0.00");
                        fields.SetField("COLACurrent", "0.00");
                        fields.SetField("COLAYearToDate", "0.00");
                        fields.SetField("DeductionAdvancesCurrent", "0.00");
                        fields.SetField("DeductionAdvancesYearToDate", "0.00");
                        fields.SetField("DeductionOthersCurrent", "0.00");
                        fields.SetField("DeductionOthersYearToDate", "0.00");
                        fields.SetField("MedicalAllowanceCurrent", "0.00");
                        fields.SetField("MedicalAllowanceYearToDate", "0.00");
                        fields.SetField("NonTaxableConvertedLeavesAmountCurrent", "0.00");
                        fields.SetField("NonTaxableConvertedLeavesAmountYearToDate", "0.00");
                        fields.SetField("OtherTaxableIncomeCurrent", "0.00");
                        fields.SetField("OtherTaxableIncomeYearToDate", "0.00");
                        fields.SetField("RegularHolidayRestDayCurrent", "0.00");
                        fields.SetField("RegularHolidayRestDayHours", "0.00");
                        fields.SetField("RegularHolidayRestDayNDCurrent", "0.00");
                        fields.SetField("RegularHolidayRestDayNDHours", "0.00");
                        fields.SetField("RegularHolidayRestDayNDYearToDate", "0.00");
                        fields.SetField("RegularHolidayRestDayOvertimeCurrent", "0.00");
                        fields.SetField("RegularHolidayRestDayOvertimeHours", "0.00");
                        fields.SetField("RegularHolidayRestDayOvertimeYearToDate", "0.00");
                        fields.SetField("RegularHolidayRestDayYearToDate", "0.00");
                        fields.SetField("RegularNDOvertimeCurrent", "0.00");
                        fields.SetField("RegularNDOvertimeHours", "0.00");
                        fields.SetField("RegularNDOvertimeYearToDate", "0.00");
                        fields.SetField("RestDayCurrent", "0.00");
                        fields.SetField("RestDayHours", "0.00");
                        fields.SetField("RestDayNDOvertimeCurrent", "0.00");
                        fields.SetField("RestDayNDOvertimeHours", "0.00");
                        fields.SetField("RestDayNDOvertimeYearToDate", "0.00");
                        fields.SetField("RestDayOvertimeCurrent", "0.00");
                        fields.SetField("RestDayOvertimeHours", "0.00");
                        fields.SetField("RestDayOvertimeYearToDate", "0.00");
                        fields.SetField("RestDayYearToDate", "0.00");
                        fields.SetField("SpecialHolidayRestDayNDCurrent", "0.00");
                        fields.SetField("SpecialHolidayRestDayNDHours", "0.00");
                        fields.SetField("SpecialHolidayRestDayNDYearToDate", "0.00");
                        fields.SetField("SpecialHolidayRestdayOvertimeCurrent", "0.00");
                        fields.SetField("SpecialHolidayRestDayOvertimeHours", "0.00");
                        fields.SetField("SpecialHolidayRestDayOvertimeYearToDate", "0.00");
                        fields.SetField("SpecialHolidayRestDayYearToDate", "0.00");
                        fields.SetField("TaxableConvertedLeavesCurrent", "0.00");
                        fields.SetField("TaxableConvertedLeavesYearToDate", "0.00");
                        fields.SetField("TransportationAllowanceCurrent", "0.00");
                        fields.SetField("TransportationAllowanceYearToDate", "0.00");

                        fields.SetField("SSSNo", DTProfile.Rows[0]["SSS"].ToString());
                        fields.SetField("PagibigNo", DTProfile.Rows[0]["Pagibig"].ToString());
                        fields.SetField("PhilHealthNo", DTProfile.Rows[0]["PhilHealth"].ToString());
                        fields.SetField("TaxNo", DTProfile.Rows[0]["TIN"].ToString());
                        fields.SetField("EmployeeName", DTProfile.Rows[0]["Emp_Name"].ToString());
                        fields.SetField("HourlyRate", "".ToString());
                        fields.SetField("Position", DTProfile.Rows[0]["Employee_Level"].ToString());
                        fields.SetField("JoinedDate", DTProfile.Rows[0]["DateJoined"].ToString());
                    }


                    if (YearToDate.Rows.Count > 0)
                    {

                        ////YEAR TO DATE////
                        fields.SetField("AbsencesYearToDate", YearToDate.Rows[0]["Absences Amount"].ToString());
                        fields.SetField("ActingAllowanceYearToDate", YearToDate.Rows[0]["Acting Allowance"].ToString());
                        fields.SetField("AdditionalHDMFContributionYearToDate", YearToDate.Rows[0]["Additional HDMF Contribution"].ToString());
                        fields.SetField("AdjustmentsYearToDate", YearToDate.Rows[0]["Adjusted Regular Amount (all)"].ToString());
                        fields.SetField("AUBSalaryLoanYearToDate", YearToDate.Rows[0]["AUB Salary Loan"].ToString());
                        fields.SetField("BasicSalaryYearToDate", YearToDate.Rows[0]["Basic Salary Amount"].ToString());
                        fields.SetField("CompanyLoanDeductionYearToDate", YearToDate.Rows[0]["Company Loan"].ToString());
                        fields.SetField("EyeglassesDeductionYearToDate", YearToDate.Rows[0]["Eyeglasses"].ToString());
                        fields.SetField("GrossEarningsYearToDate", YearToDate.Rows[0]["Gross Pay"].ToString());
                        fields.SetField("HPVVaccineDeductionYearToDate", YearToDate.Rows[0]["HPV Vaccine"].ToString());
                        fields.SetField("IntellicareDeductionYearToDate", YearToDate.Rows[0]["Intellicare"].ToString());
                        fields.SetField("RegularNightDiffYearToDate", YearToDate.Rows[0]["ND Amount"].ToString());
                        fields.SetField("RegularNightDiffOTYearToDate", YearToDate.Rows[0]["ND OT Amount"].ToString());
                        fields.SetField("NetPayYearToDate", YearToDate.Rows[0]["Net Pay"].ToString());
                        fields.SetField("EmployeeHDMFYearToDate", YearToDate.Rows[0]["Pag-ibig Employee Share"].ToString());
                        fields.SetField("PagibigLoanDeductionYearToDate", YearToDate.Rows[0]["Pagibig Loan Amount"].ToString());
                        fields.SetField("EmployeePhilHealthYearToDate", YearToDate.Rows[0]["PhilHealth Employee Share"].ToString());
                        fields.SetField("RestDayWorkYearToDate", YearToDate.Rows[0]["RD Amount"].ToString());
                        fields.SetField("RestDayNDOTCurrent", YearToDate.Rows[0]["RD ND OT Amount"].ToString());
                        fields.SetField("RestDayNDYearToDate", YearToDate.Rows[0]["RDND Amount"].ToString());
                        fields.SetField("RestDayOTYearToDate", YearToDate.Rows[0]["RDOT Amount"].ToString());
                        fields.SetField("RegularHolidayWorkYearToDate", YearToDate.Rows[0]["Regular Holiday Amount"].ToString());
                        fields.SetField("RegularHolidayNDYearToDate", YearToDate.Rows[0]["Regular Holiday Night Diff Amount"].ToString());
                        fields.SetField("RegularHolidayNDOTYearToDate", YearToDate.Rows[0]["Regular Holiday Night Diff OT Amount"].ToString());
                        fields.SetField("RegularHolidayOTYearToDate", YearToDate.Rows[0]["Regular Holiday Overtime Amount"].ToString());
                        fields.SetField("RegularHolidayRDOTYearToDate", YearToDate.Rows[0]["Regular Holiday RDOT Amount"].ToString());
                        fields.SetField("RegularHolidayRestDayWorkYearToDate", YearToDate.Rows[0]["Regular Holiday Rest Day Amount"].ToString());
                        fields.SetField("RegularHolidayRDNDYearToDate", YearToDate.Rows[0]["Regular Holiday Rest Day Night Diff Amount"].ToString());
                        fields.SetField("RegularHolidayRDNDOTYearToDate", YearToDate.Rows[0]["Regular Holiday Rest Day Night Diff OT Amount"].ToString());
                        fields.SetField("RegularOTYearToDate", YearToDate.Rows[0]["Regular Overtime Amount"].ToString());
                        fields.SetField("SpecialHolidayWorkYearToDate", YearToDate.Rows[0]["Special Holiday Amount"].ToString());
                        fields.SetField("SpecialHolidayNDYearToDate", YearToDate.Rows[0]["Special Holiday Night Diff Amount"].ToString());
                        fields.SetField("SpecialHolidayNDOTYearToDate", YearToDate.Rows[0]["Special Holiday Night Diff OT Amount"].ToString());
                        fields.SetField("SpecialHolidayOTYearToDate", YearToDate.Rows[0]["Special Holiday Overtime Amount"].ToString());
                        fields.SetField("SpecialHolidayRDOTYearToDate", YearToDate.Rows[0]["Special Holiday RD OT Amount"].ToString());
                        fields.SetField("SpecialHolidayRestDayWorkYearToDate", YearToDate.Rows[0]["Special Holiday Rest Day Amount"].ToString());
                        fields.SetField("SpecialHolidayRDNDYearToDate", YearToDate.Rows[0]["Special Holiday Rest Day ND Amount"].ToString());
                        fields.SetField("SpecialHolidayRDNDOTYearToDate", YearToDate.Rows[0]["Special Holiday Rest Day Night Diff OT Amount"].ToString());
                        fields.SetField("EmployeeSocialSecurityYearToDate", YearToDate.Rows[0]["SSS Employee Share"].ToString());
                        fields.SetField("SSSLoanDeductionYearToDate", YearToDate.Rows[0]["SSS Loan Amount"].ToString());
                        fields.SetField("TaxPayableYearToDate", YearToDate.Rows[0]["Tax Payable"].ToString());
                        fields.SetField("WithholdingTaxYearToDate", YearToDate.Rows[0]["W-Tax"].ToString());
                    }
                }
                #endregion
                ///FOR AMPLIFY////
                #region AMPLIFY

                if (uProfile.CN.Contains("amplify"))
                {

                    con = new Connection();
                    con.myparameters.Add(new myParameters { ParameterName = "@empId", mytype = SqlDbType.NVarChar, Value = empid });
                    DataTable DTProfile = con.GetDataTable("sp_getCompleteProfile_Mobilev2");

                    con.myparameters.Clear();
                    con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = empid });
                    con.myparameters.Add(new myParameters { ParameterName = "@PAYPERIOD", mytype = SqlDbType.NVarChar, Value = (firstpayperiod + '-' + secondpayperiod) });
                    paysliprecordspayslipdetails = con.GetDataTable("sp_GetPaySlipRecordsPaySlipDetailsAmplify");

                    con.myparameters.Clear();
                    con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = empid });
                    con.myparameters.Add(new myParameters { ParameterName = "@PAYPERIOD", mytype = SqlDbType.NVarChar, Value = paysliprecordspayslipdetails.Rows[0]["pay_day"].ToString() });
                    con.myparameters.Add(new myParameters { ParameterName = "@YEAR", mytype = SqlDbType.NVarChar, Value = DDLYear.ToString() });
                    YearToDate = con.GetDataTable("sp_GetPaySlipRecordsPaySlipDetailsYearToDateAmplify");


                    if (paysliprecordsempmaster.Rows.Count > 0)
                    {
                        //fields.SetFieldProperty("txtempid", "setflags", PdfAnnotation.FLAGS_HIDDEN,null);
                        fields.SetField("Employee ID", paysliprecordsempmaster.Rows[0][1].ToString());//Employee Numer
                        fields.SetField("Employee Name", paysliprecordsempmaster.Rows[0][0].ToString());//Employee Name
                        fields.SetField("Position", "");//Position
                        fields.SetField("Class", "");//Class
                        fields.SetField("EmployeeNo", empid);//Employee No
                        fields.SetField("JoinedDate", paysliprecordsempmaster.Rows[0][2].ToString());//JoinedDate
                        fields.SetField("ProductionStartDate", paysliprecordsempmaster.Rows[0][2].ToString());//ProductionStartDate
                    }
                    else
                    {

                    }
                    if (paysliprecordsconfidentialinfo.Rows.Count > 0)
                    {
                        fields.SetField("PagibigNo", paysliprecordsconfidentialinfo.Rows[0][0].ToString());//Pag-IBIG No
                        fields.SetField("PhilHealthNo", paysliprecordsconfidentialinfo.Rows[0][1].ToString());//PhilHealth No
                        fields.SetField("SSSNo", paysliprecordsconfidentialinfo.Rows[0][2].ToString());//SSS No
                        fields.SetField("TaxNo", paysliprecordsconfidentialinfo.Rows[0][3].ToString());//Tax No
                        fields.SetField("Department", "");//Department
                        fields.SetField("DepartmentCode", "");//DepartmentCode
                        fields.SetField("taxcategory", paysliprecordsconfidentialinfo.Rows[0][4].ToString());//Tax Category
                    }
                    else
                    {

                    }

                    if (DTProfile.Rows.Count > 0)
                    {
                        fields.SetField("MonthlyRate", DTProfile.Rows[0]["Salary"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(DTProfile.Rows[0]["Salary"].ToString())));
                    }

                    if (paysliprecordspayslipdetails.Rows.Count > 0)
                    {

                        ////HOURS////
                        fields.SetField("Payroll Period", paysliprecordspayslipdetails.Rows[0][0].ToString());//Pay Day

                        ///EARNINGS///
                        fields.SetField("Basic Pay", paysliprecordspayslipdetails.Rows[0]["Basic Salary"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Basic Salary"].ToString())));//BasicSalaryAmountCurrent
                        fields.SetField("Overtime", paysliprecordspayslipdetails.Rows[0]["Total OT Amount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Total OT Amount"].ToString())));//BasicSalaryAmountCurrent
                        fields.SetField("Non-taxable Allowance", paysliprecordspayslipdetails.Rows[0]["non_taxable_allowance"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["non_taxable_allowance"].ToString())));//BasicSalaryAmountCurrent
                        fields.SetField("Communication Allowance", paysliprecordspayslipdetails.Rows[0]["Comm Allowance"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Comm Allowance"].ToString())));//BasicSalaryAmountCurrent

                        fields.SetField("Late", paysliprecordspayslipdetails.Rows[0]["Deductions Tardiness"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Deductions Tardiness"].ToString())));//BasicSalaryAmountCurrent
                        fields.SetField("Undertime", paysliprecordspayslipdetails.Rows[0]["undertime"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["undertime"].ToString())));//BasicSalaryAmountCurrent
                        fields.SetField("Absences", paysliprecordspayslipdetails.Rows[0]["Absences Deduction"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Absences Deduction"].ToString())));//BasicSalaryAmountCurrent

                        fields.SetField("Adjustments", paysliprecordspayslipdetails.Rows[0]["Adjustments"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Adjustments"].ToString())));//BasicSalaryAmountCurrent

                        fields.SetField("Total Earnings", paysliprecordspayslipdetails.Rows[0]["Total Earnings"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Total Earnings"].ToString())));//BasicSalaryAmountCurrent


                        //DEDUCTIONS
                        fields.SetField("SSS", paysliprecordspayslipdetails.Rows[0]["Employee SSS"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Employee SSS"].ToString())));//BasicSalaryAmountCurrent
                        fields.SetField("PHIC", paysliprecordspayslipdetails.Rows[0]["Employee PhilHealth"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Employee PhilHealth"].ToString())));//BasicSalaryAmountCurrent
                        fields.SetField("HDMF", paysliprecordspayslipdetails.Rows[0]["Employee Pagibig"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Employee Pagibig"].ToString())));//BasicSalaryAmountCurrent
                        fields.SetField("Tax", paysliprecordspayslipdetails.Rows[0]["Tax"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Tax"].ToString())));//BasicSalaryAmountCurrent
                        fields.SetField("SSS Loan", paysliprecordspayslipdetails.Rows[0]["Loans_SSS"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Loans_SSS"].ToString())));//BasicSalaryAmountCurrent
                        fields.SetField("Pag-ibig Loan", paysliprecordspayslipdetails.Rows[0]["Loans_Pagibig"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Loans_Pagibig"].ToString())));//Employee SSS
                        fields.SetField("Car Loan", "0.00");//BasicSalaryAmountCurrent
                        fields.SetField("Phone Charges", "0.00");//BasicSalaryAmountCurrent
                        fields.SetField("Company Loan", paysliprecordspayslipdetails.Rows[0]["Loans"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Loans"].ToString())));//BasicSalaryAmountCurrent
                        fields.SetField("Total Deduction", paysliprecordspayslipdetails.Rows[0]["Total Deduction"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Total Deduction"].ToString())));//BasicSalaryAmountCurrent

                        //TOTALS
                        fields.SetField("Current NET Salary", paysliprecordspayslipdetails.Rows[0]["Net_Pay"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Net_Pay"].ToString())));//BasicSalaryAmountCurrent


                        //LEAVES
                        fields.SetField("Paid Vacation Leave Balance", paysliprecordspayslipdetails.Rows[0]["Vacation Leave"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Vacation Leave"].ToString())));//BasicSalaryAmountCurrent
                        fields.SetField("Paid Sick Leave Balance", paysliprecordspayslipdetails.Rows[0]["Sick Leave"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Sick Leave"].ToString())));//BasicSalaryAmountCurrent
                        fields.SetField("Total Leave Balance", paysliprecordspayslipdetails.Rows[0]["Total Leave"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Total Leave"].ToString())));//BasicSalaryAmountCurrent



                    }


                    if (YearToDate.Rows.Count > 0)
                    {
                        fields.SetField("YTD NET Salary", YearToDate.Rows[0]["netpay"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["netpay"].ToString())));//BasicSalaryAmountCurrent

                    }
                }
                #endregion

                if (uProfile.CN.Contains("amplify"))
                {

                    con = new Connection();
                    con.myparameters.Add(new myParameters { ParameterName = "@empId", mytype = SqlDbType.NVarChar, Value = empid });
                    DataTable DTProfile = con.GetDataTable("sp_getCompleteProfile_Mobilev2");

                    con.myparameters.Clear();
                    con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = empid });
                    con.myparameters.Add(new myParameters { ParameterName = "@PAYPERIOD", mytype = SqlDbType.NVarChar, Value = (firstpayperiod + '-' + secondpayperiod) });
                    paysliprecordspayslipdetails = con.GetDataTable("sp_GetPaySlipRecordsPaySlipDetailsAmplify");

                    con.myparameters.Clear();
                    con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = empid });
                    con.myparameters.Add(new myParameters { ParameterName = "@PAYPERIOD", mytype = SqlDbType.NVarChar, Value = paysliprecordspayslipdetails.Rows[0]["pay_day"].ToString() });
                    con.myparameters.Add(new myParameters { ParameterName = "@YEAR", mytype = SqlDbType.NVarChar, Value = DDLYear.ToString() });
                    YearToDate = con.GetDataTable("sp_GetPaySlipRecordsPaySlipDetailsYearToDateAmplify");

                    if (DTProfile.Rows.Count > 0)
                    {
                        fields.SetField("MonthlyRate", DTProfile.Rows[0]["Salary"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(DTProfile.Rows[0]["Salary"].ToString())));
                    }

                    if (paysliprecordspayslipdetails.Rows.Count > 0)
                    {

                        ////HOURS////
                        fields.SetField("PayDay", paysliprecordspayslipdetails.Rows[0]["pay_day"].ToString());//Pay Day
                        fields.SetField("Basic Salary Hour", "0.00"); //Basic Salary Hour
                        fields.SetField("DeductionsAbsencesHour", "0.00");//DeductionsAbsencesHours
                        fields.SetField("DeductionsLatesHour", "0.00");//DeductionsLatesHours
                        fields.SetField("NonTaxableAllowanceHours", "0.00");//DeductionsLwopHours
                        fields.SetField("NdAmountHour", paysliprecordspayslipdetails.Rows[0]["Regular ND Hours"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Regular ND Hours"].ToString())));//NdAmountHours
                        fields.SetField("RegularOvertimeAmountHour", paysliprecordspayslipdetails.Rows[0]["Regular OT Hours"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Regular OT Hours"].ToString())));//RegularOvertimeAmountHours
                        fields.SetField("RegularHolidayAmountHour", paysliprecordspayslipdetails.Rows[0]["Regular Holiday Hours"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Regular Holiday Hours"].ToString())));//RegularHolidayAmountHours
                        fields.SetField("RegualarHolidayOvertimeAmountHour", paysliprecordspayslipdetails.Rows[0]["Regular Holiday OT Hours"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Regular Holiday OT Hours"].ToString())));//RegularHolidayOvertimeAmountHours
                        fields.SetField("RegularHolidayRdOtAmountHour", "0.00");//RegularHolidayNDHour
                        fields.SetField("Paid Time Off Hours", "0.00");//Paid Time Off Hours                                                                                                
                        fields.SetField("SpecialHolidayAmountHour", paysliprecordspayslipdetails.Rows[0]["Special Holiday Hours"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Special Holiday Hours"].ToString())));//SpecialHolidayAmountHours
                        fields.SetField("SpecialHolidayOvertimeAmountHour", paysliprecordspayslipdetails.Rows[0]["Special Holiday OT Hours"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Special Holiday OT Hours"].ToString())));//SpecialHolidayOvertimeAmountHours
                        fields.SetField("SpecialHolidayNightDiffAmountHour", paysliprecordspayslipdetails.Rows[0]["Special Holiday ND Hours"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Special Holiday ND Hours"].ToString())));//SpecialHolidayNightDiffAmountHours
                        fields.SetField("SpecialHolidayRestDayAmountHour", paysliprecordspayslipdetails.Rows[0]["Special Holiday RD Hours"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Special Holiday RD Hours"].ToString())));//SpecialHolidayRestDayAmountHours
                        fields.SetField("SpecialHolidayRdOtAmountHour", "0.00");//SpecialHolidayRdOtAmountHours                                                                            
                        fields.SetField("doubleholidayhours", "0.00");//DH_REG_Hours
                        fields.SetField("doubleholidayndhours", "0.00");//DH_ND_Hours
                        fields.SetField("doubleholidayothours", "0.00");//DH_OT_Hours
                        fields.SetField("AdjustmentsHours", "0.00");//Adjustments

                        ///CURRENT///
                        fields.SetField("BasicSalaryCurrent", paysliprecordspayslipdetails.Rows[0]["Basic Salary"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Basic Salary"].ToString())));//BasicSalaryAmountCurrent
                        fields.SetField("DeductionAbsencesCurrent", paysliprecordspayslipdetails.Rows[0]["Deductions Absences"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Deductions Absences"].ToString())));//DeductionsAbsencesCurrent
                        fields.SetField("DeductionLatesCurrent", paysliprecordspayslipdetails.Rows[0]["Deductions Tardiness"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Deductions Tardiness"].ToString())));//DeductionsLateCurrent
                        fields.SetField("Non Taxable Allowance Current", "0.00");//Non Taxable Allowance Current
                        fields.SetField("NdAmountCurrent", paysliprecordspayslipdetails.Rows[0]["Total_ND_Amount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Total_ND_Amount"].ToString())));//NdAmountCurrent
                        fields.SetField("RegularOvertimeAmountCurrent", paysliprecordspayslipdetails.Rows[0]["Regular OT Amount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Regular OT Amount"].ToString())));//RegularOvertimeAmountCurrent
                        fields.SetField("RegularHolidayAmountCurrent", paysliprecordspayslipdetails.Rows[0]["Regular Holiday Amount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Regular Holiday Amount"].ToString())));//RegularHolidayAmountCurrent
                        fields.SetField("RegularHolidayOvertimeAmountCurrent", paysliprecordspayslipdetails.Rows[0]["Regular Holiday OT Amount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Regular Holiday OT Amount"].ToString())));//RegularHolidayOvertimeAmountCurrent
                        fields.SetField("RegularHolidayNDAmountCurrent", paysliprecordspayslipdetails.Rows[0]["Regular Holiday ND Amount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Regular Holiday ND Amount"].ToString())));//RegularHolidayRdOtAmountCurrent
                        fields.SetField("Paid Time Off Current", paysliprecordspayslipdetails.Rows[0]["adjPaid_Time_Off_Total"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["adjPaid_Time_Off_Total"].ToString())));//Paid Time Off Current
                        fields.SetField("SpecialHolidayAmountCurrent", paysliprecordspayslipdetails.Rows[0]["Special Holiday Amount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Special Holiday Amount"].ToString())));//SpecialHolidayAmountCurrent
                        fields.SetField("SpecialHolidayOvertimeAmountCurrent", paysliprecordspayslipdetails.Rows[0]["Special Holiday OT Amount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Special Holiday OT Amount"].ToString())));//SpecialHolidayOvertimeAmountCurrent
                        fields.SetField("SpecialHolidayNightDiffAmountCurrent", paysliprecordspayslipdetails.Rows[0]["Special Holiday ND Amount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Special Holiday ND Amount"].ToString())));//SpecialHolidayNightDiffAmountCurrent
                        fields.SetField("Double Holiday Current", paysliprecordspayslipdetails.Rows[0]["adjDouble_Holiday"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["adjDouble_Holiday"].ToString())));//Double Holiday Current
                        fields.SetField("Double Holiday ND Current", paysliprecordspayslipdetails.Rows[0]["adjDouble_Holiday_Night_Diff"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["adjDouble_Holiday_Night_Diff"].ToString())));//Double Holiday ND Current
                        fields.SetField("Double Holiday OT Current", paysliprecordspayslipdetails.Rows[0]["adjDouble_Holiday_Overtime"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["adjDouble_Holiday_Overtime"].ToString())));//Double Holiday OT Current
                        fields.SetField("Adjustments Current", paysliprecordspayslipdetails.Rows[0]["Adjustments"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Adjustments"].ToString())));//Paid Time Off Current
                        fields.SetField("Total Current", paysliprecordspayslipdetails.Rows[0]["Gross Pay"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Gross Pay"].ToString())));//Total Current

                        fields.SetField("DeductionSSSCurrent", paysliprecordspayslipdetails.Rows[0]["Employee SSS"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Employee SSS"].ToString())));//DeductionSSSCurrent
                        fields.SetField("DeductionPhilHealthCurrent", paysliprecordspayslipdetails.Rows[0]["Employee PhilHealth"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Employee PhilHealth"].ToString())));//DeductionPhilHealthCurrent
                        fields.SetField("DeductionPagibigCurrent", paysliprecordspayslipdetails.Rows[0]["Employee Pagibig"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Employee Pagibig"].ToString())));//DeductionPagibigCurrent
                        fields.SetField("WithHolding Tax Current", paysliprecordspayslipdetails.Rows[0]["Tax"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Tax"].ToString())));//DeductionWTaxCurrent

                        fields.SetField("Bonus Current", paysliprecordspayslipdetails.Rows[0]["BonusAmount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["BonusAmount"].ToString())));//Bonus Current
                        fields.SetField("non_taxable_allowance", paysliprecordspayslipdetails.Rows[0]["non_taxable_allowance"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["non_taxable_allowance"].ToString())));//non_taxable_allowance


                        fields.SetField("DeductionSSSLoanAmountCurrent", paysliprecordspayslipdetails.Rows[0]["SSS Loan Amount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["SSS Loan Amount"].ToString())));//DeductionSSSLoanAmountCurrent
                        fields.SetField("DeductionPagibiLoanAmountCurrent", paysliprecordspayslipdetails.Rows[0]["Pagibig Loan Amount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Pagibig Loan Amount"].ToString())));//Basic Salary
                        fields.SetField("CompanyLoanCurrent", paysliprecordspayslipdetails.Rows[0]["Loans"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Loans"].ToString())));//CompanyLoanCurrent  
                        fields.SetField("NetPayCurrent", paysliprecordspayslipdetails.Rows[0]["Net Pay"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Net Pay"].ToString())));//NetPayCurrent

                        fields.SetField("SSSEmployerShareCurrent", paysliprecordspayslipdetails.Rows[0]["SSS Employer Share"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["SSS Employer Share"].ToString())));//SSSEmployerShareCurrent
                        fields.SetField("SSSEmployerECShareCurrent", paysliprecordspayslipdetails.Rows[0]["SSS Employer EC Share"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["SSS Employer EC Share"].ToString())));//SSSEmployerECShareCurrent
                        fields.SetField("PhilHealthEmployerShareCurrent", paysliprecordspayslipdetails.Rows[0]["PhilHealth Employer Share"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["PhilHealth Employer Share"].ToString())));//PhilHealthEmployerShareCurrent
                        fields.SetField("PagibigEmployerShareCurrent", paysliprecordspayslipdetails.Rows[0]["Employer_HDMF"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Employer_HDMF"].ToString()))); //PagibigEmployerShareCurrent
                                                                                                                                                                                                                                                                      ////CURRENT////

                    }


                    if (YearToDate.Rows.Count > 0)
                    {

                        ////YEAR TO DATE////
                        fields.SetField("BasicSalaryAmountYearToDate", YearToDate.Rows[0]["basicsalary"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["basicsalary"].ToString())));//BasicSalaryAmountYeartoDate
                        fields.SetField("DeductionsAbsencesYearToDate", YearToDate.Rows[0]["deductionsabsences"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["deductionsabsences"].ToString())));//DeductionsAbsencesYeartoDate
                        fields.SetField("DeductionsLatesYearToDate", YearToDate.Rows[0]["deductionstardiness"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["deductionstardiness"].ToString())));//DeductionsLatesYeartoDate      
                        fields.SetField("Non Taxable Allowance Year To Date", YearToDate.Rows[0]["non_taxable_allowance"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["non_taxable_allowance"].ToString())));//Non Taxable Allowance Year To Date     
                        fields.SetField("NdAmountYearToDate", "0.00");//NdAmountYeartoDate                   
                        fields.SetField("RegularOvertimeAmountYearToDate", YearToDate.Rows[0]["regularotamount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["regularotamount"].ToString())));//RegularOvertimeAmountYearToDate
                        fields.SetField("RegularHolidayAmountYearToDate", YearToDate.Rows[0]["regularholidayndamount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["regularholidayndamount"].ToString())));//RegularHolidayAmountYearToDate
                        fields.SetField("Regular Holiday Night Diff Year To Date", YearToDate.Rows[0]["regularholidayamount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["regularholidayamount"].ToString())));//Regular Holiday Night Diff Year To Date
                        fields.SetField("RegularHolidayOvertimeAmountYearToDate", YearToDate.Rows[0]["regularholidayotamount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["regularholidayotamount"].ToString())));//RegularHolidayOvertimeAmountYearToDate
                        fields.SetField("Paid Time Off Year To Date", YearToDate.Rows[0]["adjPaid_Time_Off_Total"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["adjPaid_Time_Off_Total"].ToString())));//Paid Time Off Year To Date

                        fields.SetField("SpecialHolidayAmountYearToDate", YearToDate.Rows[0]["specialholidayamount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["specialholidayamount"].ToString())));//SpecialHolidayAmountYearToDate
                        fields.SetField("SpecialHolidayOverTimeAmountYearToDate", YearToDate.Rows[0]["specialholidayotamount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["specialholidayotamount"].ToString())));//SpecialHolidayOvertimeAmountYearToDate
                        fields.SetField("SpecialHolidayNightDiffAmountYearToDate", YearToDate.Rows[0]["specialholidayndamount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["specialholidayndamount"].ToString())));//SpecialHolidayNightDiffAmountYearToDate
                        fields.SetField("Double Holiday Year To Date", YearToDate.Rows[0]["adjDouble_Holiday"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["adjDouble_Holiday"].ToString())));//Double Holiday Year To Date
                        fields.SetField("Double Holiday ND Year To Date", YearToDate.Rows[0]["adjDouble_Holiday_Night_Diff"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["adjDouble_Holiday_Night_Diff"].ToString())));//Double Holiday ND Year To Date
                        fields.SetField("Double Holiday OT Year To Date", YearToDate.Rows[0]["adjDouble_Holiday_Overtime"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["adjDouble_Holiday_Overtime"].ToString())));//Double Holiday OT Year To Date
                        fields.SetField("Adjustments Year To Date", YearToDate.Rows[0]["Adjustments"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["Adjustments"].ToString())));//Adjustments Year To Date                    
                        fields.SetField("GrossPayYearToDate", YearToDate.Rows[0]["grosspay"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["grosspay"].ToString())));//GrossPayYearToDate

                        fields.SetField("DeductionSSSYearToDate", YearToDate.Rows[0]["employeesss"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["employeesss"].ToString())));//DeductionSSSYearToDate
                        fields.SetField("DeductionPhilHealthYearToDate", YearToDate.Rows[0]["employeephilhealth"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["employeephilhealth"].ToString())));//DeductionPhilHealthYearToDate
                        fields.SetField("DeductionPagibigYearToDate", YearToDate.Rows[0]["employeepagibig"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["employeepagibig"].ToString())));//DeductionPagibigYearToDate
                        fields.SetField("DeductionWTaxYearToDate", YearToDate.Rows[0]["tax"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["tax"].ToString())));//DeductionWTaxYearToDate

                        fields.SetField("NonTaxableConvertedLeavesAmountYearToDate", YearToDate.Rows[0]["nontaxableconvertedleaves"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["nontaxableconvertedleaves"].ToString())));//NonTaxableConvertedLeavesAmountYearToDate
                        fields.SetField("Bonus Year To Date", YearToDate.Rows[0]["BonusAmount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["BonusAmount"].ToString())));//Bonus Year To Date

                        fields.SetField("DeductionSSSLoanAmountYearToDate", YearToDate.Rows[0]["sssloanamount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["sssloanamount"].ToString())));//DeductionSSSLoanAmountYearToDate
                        fields.SetField("DeductionPagibigLoanAmountYearToDate", YearToDate.Rows[0]["pagibigloanamount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["pagibigloanamount"].ToString())));//DeductionPagibigLoanAmountYearToDate
                        fields.SetField("Company Loan Year To Date", YearToDate.Rows[0]["loans"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["loans"].ToString())));//Company Loan Year To Date 
                        fields.SetField("NetPayYearToDate", YearToDate.Rows[0]["netpay"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["netpay"].ToString())));//NetPayYearToDate

                        fields.SetField("SSSEmployerShareYearToDate", YearToDate.Rows[0]["sssemployershare"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["sssemployershare"].ToString())));//SSSEmployerShareYearToDate
                        fields.SetField("SSSEmployerECShareYearToDate", YearToDate.Rows[0]["sssemployerecshare"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["sssemployerecshare"].ToString())));//SSSEmployerECShareYearToDate
                        fields.SetField("PhilHealthEmployerShareYearToDate", YearToDate.Rows[0]["philhealthemployershare"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["philhealthemployershare"].ToString())));//PhilHealthEmployerShareYearToDate                                                
                        fields.SetField("PagibigEmployerShareYearToDate", YearToDate.Rows[0]["pagibigemployershare"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["pagibigemployershare"].ToString())));//PagibigEmployerShareYearToDate                                               
                                                                                                                                                                                                                                                  ////YEAR TO DATE////
                    }
                }
                #endregion

                #region PERRYS


                if (uProfile.CN.Contains("perrys"))
                {
                    //GetDatabase GDB = new GetDatabase();
                    GDB.ClientName = uProfile.CN;
                    SelfieRegistration2Controller.GetDB2(GDB);

                    con = new Connection();
                    con.myparameters.Add(new myParameters { ParameterName = "@empId", mytype = SqlDbType.NVarChar, Value = empid });
                    DataTable DTProfile = con.GetDataTable("sp_getCompleteProfile_Mobilev2");

                    con.myparameters.Clear();
                    con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = empid });
                    con.myparameters.Add(new myParameters { ParameterName = "@PAYPERIOD", mytype = SqlDbType.NVarChar, Value = (firstpayperiod + '-' + secondpayperiod) });
                    paysliprecordspayslipdetails = con.GetDataTable("sp_GetPaySlipRecordsPaySlipDetails");

                    con.myparameters.Clear();
                    con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = empid });
                    con.myparameters.Add(new myParameters { ParameterName = "@PAYPERIOD", mytype = SqlDbType.NVarChar, Value = paysliprecordspayslipdetails.Rows[0]["pay_day"].ToString() });
                    con.myparameters.Add(new myParameters { ParameterName = "@YEAR", mytype = SqlDbType.NVarChar, Value = DDLYear.ToString() });
                    YearToDate = con.GetDataTable("sp_GetPaySlipRecordsPaySlipDetailsYearToDate");

                    if (paysliprecordsempmaster.Rows.Count > 0)
                    {
                        //fields.SetFieldProperty("txtempid", "setflags", PdfAnnotation.FLAGS_HIDDEN,null);
                        fields.SetField("Employee Number", paysliprecordsempmaster.Rows[0][1].ToString());//Employee Numer
                        fields.SetField("Employee Name", paysliprecordsempmaster.Rows[0][0].ToString());//Employee Name
                        fields.SetField("Status", "");//Position
                        fields.SetField("Joined", paysliprecordsempmaster.Rows[0][2].ToString());//JoinedDate
                    }
                    else
                    {

                    }
                    if (paysliprecordsconfidentialinfo.Rows.Count > 0)
                    {
                        fields.SetField("Pag Ibig Number", paysliprecordsconfidentialinfo.Rows[0][0].ToString());//Pag-IBIG No
                        fields.SetField("PhilHealth Number", paysliprecordsconfidentialinfo.Rows[0][1].ToString());//PhilHealth No
                        fields.SetField("SSS Number", paysliprecordsconfidentialinfo.Rows[0][2].ToString());//SSS No
                        fields.SetField("Tax Number", paysliprecordsconfidentialinfo.Rows[0][3].ToString());//Tax No
                        fields.SetField("Tax Category", paysliprecordsconfidentialinfo.Rows[0][4].ToString());//Tax Category
                    }

                    if (DTProfile.Rows.Count > 0)
                    {
                        fields.SetField("Monthly Rate", DTProfile.Rows[0]["Salary"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(DTProfile.Rows[0]["Salary"].ToString())));
                    }
                    if (paysliprecordsemployeepay.Rows.Count > 0)
                    {
                        fields.SetField("Bank Account", paysliprecordsemployeepay.Rows[0][0].ToString());//Bank Account
                        fields.SetField("Hourly Rate", paysliprecordsemployeepay.Rows[0][1].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordsemployeepay.Rows[0][1].ToString())));//Hourly Rate
                    }
                    else
                    {
                        fields.SetField("Hourly Rate", "0.00");
                    }


                    fields.SetField("Pay Period", payperiod);

                    if (paysliprecordspayslipdetails.Rows.Count > 0)
                    {

                        ////HOURS////
                        fields.SetField("Pay Day", paysliprecordspayslipdetails.Rows[0]["Pay_Day"].ToString());//Pay Day
                        fields.SetField("Basic Salary Hour", "0.00"); //Basic Salary Hour
                        fields.SetField("Deductions: Absences Hours", "0.00");//DeductionsAbsencesHours
                        fields.SetField("Adjustments Hours", "0.00");//DeductionsLatesHours


                        ///CURRENT///
                        fields.SetField("Basic Salary Current", paysliprecordspayslipdetails.Rows[0]["Basic Salary"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Basic Salary"].ToString())));//BasicSalaryAmountCurrent
                        fields.SetField("Deductions: Absences Current", paysliprecordspayslipdetails.Rows[0]["Absences"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Absences"].ToString())));//DeductionsAbsencesCurrent
                        fields.SetField("Adjustments Current", paysliprecordspayslipdetails.Rows[0]["Adjustments"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Adjustments"].ToString())));//Non Taxable Allowance Current
                        fields.SetField("Gross Pay Current", paysliprecordspayslipdetails.Rows[0]["Gross Pay"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Gross Pay"].ToString())));//Total Current

                        fields.SetField("Employee Social Security Current", paysliprecordspayslipdetails.Rows[0]["Employee SSS"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Employee SSS"].ToString())));//DeductionSSSCurrent
                        fields.SetField("Employee PhilHealth Current", paysliprecordspayslipdetails.Rows[0]["Employee PhilHealth"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Employee PhilHealth"].ToString())));//DeductionPhilHealthCurrent
                        fields.SetField("Employee HDMF Current", paysliprecordspayslipdetails.Rows[0]["Employee Pagibig"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Employee Pagibig"].ToString())));//DeductionPagibigCurrent
                        fields.SetField("Withholding Tax Current", paysliprecordspayslipdetails.Rows[0]["Tax"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Tax"].ToString())));//DeductionWTaxCurrent

                        fields.SetField("Bonus Current", paysliprecordspayslipdetails.Rows[0]["BonusAmount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["BonusAmount"].ToString())));//Bonus Current
                        fields.SetField("Allowances Current", paysliprecordspayslipdetails.Rows[0]["non_taxable_allowance"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["non_taxable_allowance"].ToString())));//non_taxable_allowance
                        fields.SetField("13th Month Pay Current", paysliprecordspayslipdetails.Rows[0]["NonTaxable13MonthPay"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["NonTaxable13MonthPay"].ToString())));//Bonus Current

                        fields.SetField("Company Telephone  Current", paysliprecordspayslipdetails.Rows[0]["TelBil"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["TelBil"].ToString())));//Bonus Current
                        fields.SetField("Tax Payable Current", paysliprecordspayslipdetails.Rows[0]["Tax_Payable"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Tax_Payable"].ToString())));//Bonus Current
                        fields.SetField("Car Plan Current", paysliprecordspayslipdetails.Rows[0]["CarPlan"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["CarPlan"].ToString())));//Bonus Current
                        fields.SetField("Car Insurance Current", paysliprecordspayslipdetails.Rows[0]["CarInsurance"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["CarInsurance"].ToString())));//Bonus Current
                        fields.SetField("Car Repair Current", paysliprecordspayslipdetails.Rows[0]["CarRepair"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["CarRepair"].ToString())));//Bonus Current
                        fields.SetField("SSS Loan Current", paysliprecordspayslipdetails.Rows[0]["SSS Loan Amount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["SSS Loan Amount"].ToString())));//DeductionSSSLoanAmountCurrent
                        fields.SetField("HDMF Loan Current", paysliprecordspayslipdetails.Rows[0]["Pagibig Loan Amount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Pagibig Loan Amount"].ToString())));//Basic Salary
                        fields.SetField("Net Pay Current", paysliprecordspayslipdetails.Rows[0]["Net Pay"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Net Pay"].ToString())));//NetPayCurrent

                        fields.SetField("Employer Social Security Current", paysliprecordspayslipdetails.Rows[0]["SSS Employer Share"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["SSS Employer Share"].ToString())));//SSSEmployerShareCurrent
                        fields.SetField("Employer Social Security EC Current", paysliprecordspayslipdetails.Rows[0]["SSS Employer EC Share"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["SSS Employer EC Share"].ToString())));//SSSEmployerECShareCurrent
                        fields.SetField("Employer PhilHealth Current", paysliprecordspayslipdetails.Rows[0]["PhilHealth Employer Share"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["PhilHealth Employer Share"].ToString())));//PhilHealthEmployerShareCurrent
                        fields.SetField("Employer HDMF Current", paysliprecordspayslipdetails.Rows[0]["Employer_HDMF"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Employer_HDMF"].ToString()))); //PagibigEmployerShareCurrent
                                                                                                                                                                                                                                                                ////CURRENT////

                    }


                    if (YearToDate.Rows.Count > 0)
                    {

                        ////YEAR TO DATE////
                        fields.SetField("Basic Salary Year To Date", YearToDate.Rows[0]["BasicSalary"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["BasicSalary"].ToString())));//BasicSalaryAmountYeartoDate
                        fields.SetField("Deductions: Absences Year To Date", YearToDate.Rows[0]["Absences"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["Absences"].ToString())));//DeductionsAbsencesYeartoDate
                        fields.SetField("Adjustments Year To Date", YearToDate.Rows[0]["Adjustments"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["Adjustments"].ToString())));//Adjustments Year To Date                    
                        fields.SetField("Gross Pay Year To Date", YearToDate.Rows[0]["GrossPay/Total"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["GrossPay/Total"].ToString())));//GrossPayYearToDate

                        fields.SetField("Employee Social Security Year To Date", YearToDate.Rows[0]["EmployeeSocialSecurity"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["EmployeeSocialSecurity"].ToString())));//DeductionSSSYearToDate
                        fields.SetField("Employee PhilHealth Year To Date", YearToDate.Rows[0]["EmployeePhilHealth"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["EmployeePhilHealth"].ToString())));//DeductionPhilHealthYearToDate
                        fields.SetField("Employee HDMF Year To Date", YearToDate.Rows[0]["EmployeeHDMF"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["EmployeeHDMF"].ToString())));//DeductionPagibigYearToDate
                        fields.SetField("Withholding Tax Year To Date", YearToDate.Rows[0]["WithHoldingTax"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["WithHoldingTax"].ToString())));//DeductionWTaxYearToDate

                        fields.SetField("Allowances Year To Date", YearToDate.Rows[0]["non_taxable_allowance"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["non_taxable_allowance"].ToString())));//NonTaxableConvertedLeavesAmountYearToDate
                        fields.SetField("Bonus Year To Date", YearToDate.Rows[0]["BonusAmount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["BonusAmount"].ToString())));//Bonus Year To Date
                        fields.SetField("13th Month Pay  Year To Date", YearToDate.Rows[0]["NonTaxable13MonthPay"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["NonTaxable13MonthPay"].ToString())));//NonTaxableConvertedLeavesAmountYearToDate

                        fields.SetField("Company Telephone Year To Date", YearToDate.Rows[0]["TelBil"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["TelBil"].ToString())));//NonTaxableConvertedLeavesAmountYearToDate
                        fields.SetField("Tax Payable Year To Date", YearToDate.Rows[0]["Tax_Payable"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["Tax_Payable"].ToString())));//NonTaxableConvertedLeavesAmountYearToDate
                        fields.SetField("Car Plan Year To Date", YearToDate.Rows[0]["CarPlan"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["CarPlan"].ToString())));//NonTaxableConvertedLeavesAmountYearToDate
                        fields.SetField("Car Insurance Year To Date", YearToDate.Rows[0]["CarInsurance"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["CarInsurance"].ToString())));//NonTaxableConvertedLeavesAmountYearToDate
                        fields.SetField("Car Repair Year To Date", YearToDate.Rows[0]["CarRepair"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["CarRepair"].ToString())));//NonTaxableConvertedLeavesAmountYearToDate
                        fields.SetField("SSS Loan Year To Date", YearToDate.Rows[0]["SSSLoanDeduction"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["SSSLoanDeduction"].ToString())));//DeductionSSSLoanAmountYearToDate
                        fields.SetField("HDMF Loan Year To Date", YearToDate.Rows[0]["PagibigLoanDeduction"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["PagibigLoanDeduction"].ToString())));//DeductionPagibigLoanAmountYearToDate
                        fields.SetField("Net Pay Year To Date", YearToDate.Rows[0]["NetPay"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["NetPay"].ToString())));//NetPayYearToDate

                        fields.SetField("Employer Social Security Year To Date", YearToDate.Rows[0]["EmployerSocialSecurity"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["EmployerSocialSecurity"].ToString())));//SSSEmployerShareYearToDate
                        fields.SetField("Employer Social Security EC Year To Date", YearToDate.Rows[0]["EmployerSocialSecurityEC"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["EmployerSocialSecurityEC"].ToString())));//SSSEmployerECShareYearToDate
                        fields.SetField("Employer PhilHealth Year To Date", YearToDate.Rows[0]["EmployerPhilHealth"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["EmployerPhilHealth"].ToString())));//PhilHealthEmployerShareYearToDate                                                
                        fields.SetField("Net Pay Year To Date", YearToDate.Rows[0]["EmployerHDMF"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["EmployerHDMF"].ToString())));//PagibigEmployerShareYearToDate                                               
                                                                                                                                                                                                                        ////YEAR TO DATE////
                    }
                }


                #region Epiroc
                //clientname = "paat";
                if (uProfile.CN.Contains("epiroc"))
                {
                    //GetDatabase GDB = new GetDatabase();
                    GDB.ClientName = uProfile.CN;
                    SelfieRegistration2Controller.GetDB2(GDB);

                    con = new Connection();
                    con.myparameters.Add(new myParameters { ParameterName = "@empId", mytype = SqlDbType.NVarChar, Value = empid });
                    DataTable DTProfile = con.GetDataTable("sp_getCompleteProfile_Mobilev2");

                    con.myparameters.Clear();
                    con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = empid });
                    con.myparameters.Add(new myParameters { ParameterName = "@PAYPERIOD", mytype = SqlDbType.NVarChar, Value = DDLPayPeriod });
                    paysliprecordspayslipdetails = con.GetDataTable("sp_GetPaySlipRecordsPaySlipDetailsEpiroc");

                    con.myparameters.Clear();
                    con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = empid });
                    con.myparameters.Add(new myParameters { ParameterName = "@PAYPERIOD", mytype = SqlDbType.NVarChar, Value = paysliprecordspayslipdetails.Rows[0]["Pay Day"].ToString() });
                    con.myparameters.Add(new myParameters { ParameterName = "@YEAR", mytype = SqlDbType.NVarChar, Value = DDLYear.ToString() });
                    YearToDate = con.GetDataTable("sp_GetPaySlipRecordsPaySlipDetailsYearToDateEpiroc");

                    if (DTProfile.Rows.Count > 0)
                    {
                        fields.SetField("MonthlyRate", DTProfile.Rows[0]["Salary"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(DTProfile.Rows[0]["Salary"].ToString())));
                    }

                    if (paysliprecordspayslipdetails.Rows.Count > 0)
                    {
                        payslipid = paysliprecordspayslipdetails.Rows[0]["ID"].ToString();
                        ////CURRENT////
                        fields.SetField("BasicSalaryCurrent", paysliprecordspayslipdetails.Rows[0]["BasicPay"].ToString());
                        fields.SetField("DeductionsLWOPCurrent", paysliprecordspayslipdetails.Rows[0]["LWOP_Deduction"].ToString());
                        fields.SetField("RegularOTCurrent", paysliprecordspayslipdetails.Rows[0]["Total_OT_Amount"].ToString());
                        fields.SetField("RegularNightDiffCurrent", paysliprecordspayslipdetails.Rows[0]["Total_ND_Amount"].ToString());
                        fields.SetField("RegularNightDiffOTCurrent", "0.00");
                        fields.SetField("RestDayWorkCurrent", paysliprecordspayslipdetails.Rows[0]["RestDay_Amount"].ToString());
                        fields.SetField("RestDayOTCurrent", paysliprecordspayslipdetails.Rows[0]["RestDayOT_Amount"].ToString());
                        fields.SetField("RestDayNDCurrent", "0.00");
                        fields.SetField("RestDayNDOTCurrent", "0.00");
                        fields.SetField("RegularHolidayWorkCurrent", paysliprecordspayslipdetails.Rows[0]["RH_REG_Amount"].ToString());
                        fields.SetField("RegularHolidayPremiumCurrent", paysliprecordspayslipdetails.Rows[0]["adjRegular_Holiday"].ToString());
                        fields.SetField("RegularHolidayOTCurrent", "0.00");
                        fields.SetField("RegularHolidayNDCurrent", "0.00");
                        fields.SetField("RegularHolidayNDOTCurrent", "0.00");
                        fields.SetField("RegularHolidayRestDayWorkCurrent", "0.00");
                        fields.SetField("RegularHolidayRDOTCurrent", "0.00");
                        fields.SetField("RegularHolidayRDNDCurrent", "0.00");
                        fields.SetField("RegularHolidayRDNDOTCurrent", "0.00");
                        fields.SetField("SpecialHolidayWorkCurrent", paysliprecordspayslipdetails.Rows[0]["SH_REG_Amount"].ToString());
                        fields.SetField("SpecialHolidayOTCurrent", "0.00");
                        fields.SetField("SpecialHolidayNDCurrent", "0.00");
                        fields.SetField("SpecialHolidayNDOTCurrent", "0.00");
                        fields.SetField("SpecialHolidayRestDayWorkCurrent", "0.00");
                        fields.SetField("SpecialHolidayRDOTCurrent", "0.00");
                        fields.SetField("SpecialHolidayRDNDCurrent", "0.00");
                        fields.SetField("SpecialHolidayRDNDOTCurrent", "0.00");
                        fields.SetField("13thMonthTaxableCurrent", "0.00");
                        fields.SetField("CarAllowanceCurrent", paysliprecordspayslipdetails.Rows[0]["Adj_Taxable_Allowance"].ToString());
                        fields.SetField("CommissionCurrent", paysliprecordspayslipdetails.Rows[0]["TaxAdjsutment"].ToString());
                        fields.SetField("TaxableAllowanceCurrent", paysliprecordspayslipdetails.Rows[0]["ReimburseTaxable"].ToString());
                        fields.SetField("RewardCurrent", paysliprecordspayslipdetails.Rows[0]["Ecola_Taxable"].ToString());
                        fields.SetField("AdjustmentsCurrent", "0.00");
                        fields.SetField("GrossPayCurrent", paysliprecordspayslipdetails.Rows[0]["GrossPay"].ToString());
                        fields.SetField("EmployeeNoCurrent", paysliprecordspayslipdetails.Rows[0]["EmpID"].ToString());
                        fields.SetField("MonthlyRateCurrent", "0.00");
                        fields.SetField("HourlyRateCurrent", "0.00");
                        fields.SetField("TaxCategoryCurrent", "0.00");
                        fields.SetField("BankAccountCurrent", "0.00");
                        fields.SetField("EmployeeHDMFCurrent", paysliprecordspayslipdetails.Rows[0]["HDMF"].ToString());
                        fields.SetField("EmployeePhilHealthCurrent", paysliprecordspayslipdetails.Rows[0]["PhilHealth"].ToString());
                        fields.SetField("EmployeeSocialSecurityCurrent", paysliprecordspayslipdetails.Rows[0]["SSS"].ToString());
                        fields.SetField("WithholdingTaxCurrent", paysliprecordspayslipdetails.Rows[0]["Tax"].ToString());
                        fields.SetField("MealAllowanceCurrent", paysliprecordspayslipdetails.Rows[0]["non_taxable_allowance"].ToString());
                        fields.SetField("RiceAllowanceCurrent", paysliprecordspayslipdetails.Rows[0]["Rice_Allowance"].ToString());
                        fields.SetField("LoadAllowanceCurrent", paysliprecordspayslipdetails.Rows[0]["Adj_NonTaxable_Allowance"].ToString());
                        fields.SetField("13thMonthNon-TaxableCurrent", "0.00");
                        fields.SetField("SSSLoanCurrent", paysliprecordspayslipdetails.Rows[0]["Loans_SSS"].ToString());
                        fields.SetField("Pag-ibigLoanCurrent", paysliprecordspayslipdetails.Rows[0]["Loans_Pagibig"].ToString());
                        fields.SetField("CTBLoanCurrent", paysliprecordspayslipdetails.Rows[0]["LifeInsurance"].ToString());
                        fields.SetField("EmergencyLoanCurrent", "0.00");
                        fields.SetField("CompanySalaryLoanCurrent", paysliprecordspayslipdetails.Rows[0]["Loans"].ToString());
                        fields.SetField("VoluntaryHDMFCurrent", paysliprecordspayslipdetails.Rows[0]["PagIBIG_MP2"].ToString());
                        fields.SetField("053EmployeesAccountCurrent", paysliprecordspayslipdetails.Rows[0]["SalaryDeduction"].ToString());
                        fields.SetField("OtherDeductionCurrent", "0.00");
                        fields.SetField("GrossDeductionsCurrent", "0.00");
                        fields.SetField("NetPayCurrent", paysliprecordspayslipdetails.Rows[0]["Net_Pay"].ToString());




                        //zeroes


                        fields.SetFieldProperty("SSSNo", "textsize", 8f, null);
                        fields.SetField("SSSNo", DTProfile.Rows[0]["SSS"].ToString());

                        fields.SetFieldProperty("PagibigNo", "textsize", 8f, null);
                        fields.SetField("PagibigNo", DTProfile.Rows[0]["Pagibig"].ToString());

                        fields.SetFieldProperty("PhilHealthNo", "textsize", 8f, null);
                        fields.SetField("PhilHealthNo", DTProfile.Rows[0]["PhilHealth"].ToString());

                        fields.SetFieldProperty("TaxNo", "textsize", 8f, null);
                        fields.SetField("TaxNo", DTProfile.Rows[0]["TIN"].ToString());

                        fields.SetFieldProperty("EmployeeName", "textsize", 8f, null);
                        fields.SetField("EmployeeName", DTProfile.Rows[0]["Emp_Name"].ToString());

                        fields.SetFieldProperty("HourlyRate", "textsize", 8f, null);
                        fields.SetField("HourlyRate", DTProfile.Rows[0]["HourlyRate"].ToString());

                        fields.SetFieldProperty("Position", "textsize", 8f, null);
                        fields.SetField("Position", DTProfile.Rows[0]["Employee_Level"].ToString());

                        fields.SetFieldProperty("JoinedDate", "textsize", 8f, null);
                        fields.SetField("JoinedDate", DTProfile.Rows[0]["DateJoined"].ToString());

                        fields.SetFieldProperty("PayPeriod", "textsize", 8f, null);
                        fields.SetField("PayPeriod", paysliprecordspayslipdetails.Rows[0]["Pay_Period"].ToString());

                        fields.SetFieldProperty("PayDay", "textsize", 8f, null);
                        fields.SetField("PayDay", paysliprecordspayslipdetails.Rows[0]["Pay_Day"].ToString());
                    }


                    if (YearToDate.Rows.Count > 0)
                    {

                        ////YEAR TO DATE////
                        fields.SetField("BasicSalaryYTD", YearToDate.Rows[0]["BasicSalary"].ToString());
                        fields.SetField("DeductionsLWOPYTD", YearToDate.Rows[0]["DeductionsLWOP"].ToString());
                        fields.SetField("RegularOTYTD", YearToDate.Rows[0]["RegularOT"].ToString());
                        fields.SetField("RegularNightDiffYTD", YearToDate.Rows[0]["RegularNightDiff"].ToString());
                        fields.SetField("RegularNightDiffOTYTD", "0.00");
                        fields.SetField("RestDayWorkYTD", YearToDate.Rows[0]["RestDayWork"].ToString());
                        fields.SetField("RestDayOTYTD", YearToDate.Rows[0]["RestDayOT"].ToString());
                        fields.SetField("RestDayNDYTD", "0.00");
                        fields.SetField("RestDayNDOTYTD", "0.00");
                        fields.SetField("RegularHolidayWorkYTD", YearToDate.Rows[0]["RegularHolidayWork"].ToString());
                        fields.SetField("RegularHolidayPremiumYTD", YearToDate.Rows[0]["RegularHolidayPremium"].ToString());
                        fields.SetField("RegularHolidayOTYTD", "0.00");
                        fields.SetField("RegularHolidayNDYTD", "0.00");
                        fields.SetField("RegularHolidayNDOTYTD", "0.00");
                        fields.SetField("RegularHolidayRestDayWorkYTD", "0.00");
                        fields.SetField("RegularHolidayRDOTYTD", "0.00");
                        fields.SetField("RegularHolidayRDNDYTD", "0.00");
                        fields.SetField("RegularHolidayRDNDOTYTD", "0.00");
                        fields.SetField("SpecialHolidayWorkYTD", YearToDate.Rows[0]["SpecialHolidayWork"].ToString());
                        fields.SetField("SpecialHolidayOTYTD", "0.00");
                        fields.SetField("SpecialHolidayNDYTD", "0.00");
                        fields.SetField("SpecialHolidayNDOTYTD", "0.00");
                        fields.SetField("SpecialHolidayRestDayWorkYTD", "0.00");
                        fields.SetField("SpecialHolidayRDOTYTD", "0.00");
                        fields.SetField("SpecialHolidayRDNDYTD", "0.00");
                        fields.SetField("SpecialHolidayRDNDOTYTD", "0.00");
                        fields.SetField("13thMonthTaxableYTD", "0.00");
                        fields.SetField("CarAllowanceYTD", YearToDate.Rows[0]["CarAllowance"].ToString());
                        fields.SetField("CommissionYTD", YearToDate.Rows[0]["Commission"].ToString());
                        fields.SetField("TaxableAllowanceYTD", YearToDate.Rows[0]["TaxableAllowance"].ToString());
                        fields.SetField("RewardYTD", YearToDate.Rows[0]["Reward"].ToString());
                        fields.SetField("AdjustmentsYTD", "0.00");
                        fields.SetField("GrossPayYTD", YearToDate.Rows[0]["GrossPay"].ToString());
                        fields.SetField("EmployeeNoYTD", YearToDate.Rows[0]["EmployeeNo"].ToString());
                        fields.SetField("EmployeeNameYTD", "0.00");
                        fields.SetField("JoinedYTD", "0.00");
                        fields.SetField("StatusYTD", "0.00");
                        fields.SetField("PagibigNoYTD", "0.00");
                        fields.SetField("PhilHealthNoYTD", "0.00");
                        fields.SetField("SSSNoYTD", "0.00");
                        fields.SetField("TaxNoYTD", "0.00");
                        fields.SetField("PayDayYTD", YearToDate.Rows[0]["PayDay"].ToString());
                        fields.SetField("MonthlyRateYTD", "0.00");
                        fields.SetField("HourlyRateYTD", "0.00");
                        fields.SetField("TaxCategoryYTD", "0.00");
                        fields.SetField("BankAccountYTD", "0.00");
                        fields.SetField("EmployeeHDMFYTD", YearToDate.Rows[0]["EmployeeHDMF"].ToString());
                        fields.SetField("EmployeePhilHealthYTD", YearToDate.Rows[0]["EmployeePhilHealth"].ToString());
                        fields.SetField("EmployeeSocialSecurityYTD", YearToDate.Rows[0]["EmployeeSocialSecurity"].ToString());
                        fields.SetField("WithholdingTaxYTD", YearToDate.Rows[0]["WithholdingTax"].ToString());
                        fields.SetField("MealAllowanceYTD", YearToDate.Rows[0]["MealAllowance"].ToString());
                        fields.SetField("RiceAllowanceYTD", YearToDate.Rows[0]["RiceAllowance"].ToString());
                        fields.SetField("LoadAllowanceYTD", YearToDate.Rows[0]["LoadAllowance"].ToString());
                        fields.SetField("13thMonthNon-TaxableYTD", "0.00");
                        fields.SetField("SSSLoanYTD", YearToDate.Rows[0]["SSSLoan"].ToString());
                        fields.SetField("Pag-ibigLoanYTD", YearToDate.Rows[0]["Pag-ibigLoan"].ToString());
                        fields.SetField("CTBLoanYTD", YearToDate.Rows[0]["CTBLoan"].ToString());
                        fields.SetField("EmergencyLoanYTD", "0.00");
                        fields.SetField("CompanySalaryLoanYTD", YearToDate.Rows[0]["CompanySalaryLoan"].ToString());
                        fields.SetField("VoluntaryHDMFYTD", YearToDate.Rows[0]["VoluntaryHDMF"].ToString());
                        fields.SetField("053EmployeesAccountYTD", YearToDate.Rows[0]["053EmployeesAccount"].ToString());
                        fields.SetField("OtherDeductionYTD", "0.00");
                        fields.SetField("GrossDeductionsYTD", "0.00");
                        fields.SetField("NetPayYTD", YearToDate.Rows[0]["NetPay"].ToString());



                    }
                }
                #endregion

                #region JL De Leon
                //clientname = "paat";
                if (uProfile.CN.Contains("jldeleon"))
                {
                    // GetDatabase GDB = new GetDatabase();
                    GDB.ClientName = uProfile.CN;
                    SelfieRegistration2Controller.GetDB2(GDB);

                    con = new Connection();
                    con.myparameters.Add(new myParameters { ParameterName = "@empId", mytype = SqlDbType.NVarChar, Value = empid });
                    DataTable DTProfile = con.GetDataTable("sp_getCompleteProfile_Mobilev2");

                    con.myparameters.Clear();
                    con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = empid });
                    con.myparameters.Add(new myParameters { ParameterName = "@PAYPERIOD", mytype = SqlDbType.NVarChar, Value = (firstpayperiod + '-' + secondpayperiod) });
                    paysliprecordspayslipdetails = con.GetDataTable("sp_GetPaySlipRecordsPaySlipDetailsJLdeleon");

                    con.myparameters.Clear();
                    con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = empid });
                    con.myparameters.Add(new myParameters { ParameterName = "@PAYPERIOD", mytype = SqlDbType.NVarChar, Value = paysliprecordspayslipdetails.Rows[0]["Pay Day"].ToString() });
                    con.myparameters.Add(new myParameters { ParameterName = "@YEAR", mytype = SqlDbType.NVarChar, Value = DDLYear.ToString() });
                    YearToDate = con.GetDataTable("sp_GetPaySlipRecordsPaySlipDetailsYearToDatejldeleon");

                    if (DTProfile.Rows.Count > 0)
                    {
                        fields.SetField("MonthlyRate", DTProfile.Rows[0]["Salary"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(DTProfile.Rows[0]["Salary"].ToString())));
                    }

                    if (paysliprecordspayslipdetails.Rows.Count > 0)
                    {
                        payslipid = paysliprecordspayslipdetails.Rows[0]["ID"].ToString();
                        ////CURRENT////
                        fields.SetField("BasicSalaryCurrent", paysliprecordspayslipdetails.Rows[0]["Total_REG_Amount"].ToString());
                        fields.SetField("13thMonthTaxableCurrent", "0.00".ToString());
                        fields.SetField("AdjustmentsCurrent", "0.00".ToString());
                        fields.SetField("GrossPayCurrent", paysliprecordspayslipdetails.Rows[0]["GrossPay"].ToString());
                        fields.SetField("EmployeeHDMFCurrent", paysliprecordspayslipdetails.Rows[0]["HDMF"].ToString());
                        fields.SetField("EmployeePhilHealthCurrent", paysliprecordspayslipdetails.Rows[0]["PhilHealth"].ToString());
                        fields.SetField("EmployeeSocialSecurityCurrent", paysliprecordspayslipdetails.Rows[0]["SSS"].ToString());
                        fields.SetField("WithholdingTaxCurrent", paysliprecordspayslipdetails.Rows[0]["Tax"].ToString());
                        fields.SetField("NonTaxableAdditionsCurrent", "0.00".ToString());
                        fields.SetField("13thMonthNonTaxableCurrent", paysliprecordspayslipdetails.Rows[0]["Payable13Month"].ToString());
                        fields.SetField("SSSSalaryLoanCurrent", paysliprecordspayslipdetails.Rows[0]["Loans_SSS"].ToString());
                        fields.SetField("PagibigSalaryLoanCurrent", paysliprecordspayslipdetails.Rows[0]["Loans_Pagibig"].ToString());
                        fields.SetField("PagibigHousingLoanCurrent", paysliprecordspayslipdetails.Rows[0]["Loans"].ToString());
                        fields.SetField("MedicardCurrent", paysliprecordspayslipdetails.Rows[0]["SalaryDeduction"].ToString());
                        fields.SetField("CashAdvanceCurrent", paysliprecordspayslipdetails.Rows[0]["LifeInsurance"].ToString());
                        fields.SetField("GrossDeductionsCurrent", paysliprecordspayslipdetails.Rows[0]["GrossDeductions"].ToString());
                        fields.SetField("NetPayCurrent", paysliprecordspayslipdetails.Rows[0]["Net_Pay"].ToString());





                        //zeroes


                        fields.SetFieldProperty("SSSNo", "textsize", 8f, null);
                        fields.SetField("SSSNo", DTProfile.Rows[0]["SSS"].ToString());

                        fields.SetFieldProperty("PagibigNo", "textsize", 8f, null);
                        fields.SetField("PagibigNo", DTProfile.Rows[0]["Pagibig"].ToString());

                        fields.SetFieldProperty("PhilHealthNo", "textsize", 8f, null);
                        fields.SetField("PhilHealthNo", DTProfile.Rows[0]["PhilHealth"].ToString());

                        fields.SetFieldProperty("TaxNo", "textsize", 8f, null);
                        fields.SetField("TaxNo", DTProfile.Rows[0]["TIN"].ToString());

                        fields.SetFieldProperty("EmployeeName", "textsize", 8f, null);
                        fields.SetField("EmployeeName", DTProfile.Rows[0]["Emp_Name"].ToString());

                        fields.SetFieldProperty("HourlyRate", "textsize", 8f, null);
                        fields.SetField("HourlyRate", DTProfile.Rows[0]["HourlyRate"].ToString());

                        fields.SetFieldProperty("Position", "textsize", 8f, null);
                        fields.SetField("Position", DTProfile.Rows[0]["Employee_Level"].ToString());

                        fields.SetFieldProperty("JoinedDate", "textsize", 8f, null);
                        fields.SetField("JoinedDate", DTProfile.Rows[0]["DateJoined"].ToString());

                        fields.SetFieldProperty("PayPeriod", "textsize", 8f, null);
                        fields.SetField("PayPeriod", paysliprecordspayslipdetails.Rows[0]["Pay_Period"].ToString());

                        fields.SetFieldProperty("PayDay", "textsize", 8f, null);
                        fields.SetField("PayDay", paysliprecordspayslipdetails.Rows[0]["Pay_Day"].ToString());
                    }


                    if (YearToDate.Rows.Count > 0)
                    {

                        ////YEAR TO DATE////

                        fields.SetField("BasicSalaryYTD", YearToDate.Rows[0]["Total_REG_Amount"].ToString());
                        fields.SetField("13thMonthTaxableYTD", "0.00".ToString());
                        fields.SetField("AdjustmentsYTD", "0.00".ToString());
                        fields.SetField("GrossPayYTD", YearToDate.Rows[0]["GrossPay"].ToString());
                        fields.SetField("EmployeeHDMFYTD", YearToDate.Rows[0]["HDMF"].ToString());
                        fields.SetField("EmployeePhilHealthYTD", YearToDate.Rows[0]["PhilHealth"].ToString());
                        fields.SetField("EmployeeSocialSecurityYTD", YearToDate.Rows[0]["SSS"].ToString());
                        fields.SetField("WithholdingTaxYTD", YearToDate.Rows[0]["Tax"].ToString());
                        fields.SetField("NonTaxableAdditionsYTD", "0.00".ToString());
                        fields.SetField("13thMonthNonTaxable", YearToDate.Rows[0]["Payable13Month"].ToString());
                        fields.SetField("SSSSalaryLoanYTD", YearToDate.Rows[0]["Loans_SSS"].ToString());
                        fields.SetField("PagibigSalaryLoanYTD", YearToDate.Rows[0]["Loans_Pagibig"].ToString());
                        fields.SetField("PagibigHousingLoanYTD", YearToDate.Rows[0]["Loans"].ToString());
                        fields.SetField("MedicardYTD", YearToDate.Rows[0]["SalaryDeduction"].ToString());
                        fields.SetField("CashAdvanceYTD", YearToDate.Rows[0]["LifeInsurance"].ToString());
                        fields.SetField("GrossDeductionsYTD", YearToDate.Rows[0]["GrossDeductions"].ToString());
                        fields.SetField("NetPayYTD", YearToDate.Rows[0]["Net_Pay"].ToString());


                    }
                }
                #endregion
                //DataTable fieldses = new DataTable();
                //fieldses.Columns.Add("fieldname");
                //foreach (var field in af.Fields)
                //{
                //    Console.WriteLine("{0}, {1}", field.Key, field.Value);
                //    //string sample = TextBox2.Text;
                //    string sample2 = field.Key.ToString();
                //    DataRow dr = fieldses.NewRow();
                //    dr[0] = sample2;
                //    fieldses.Rows.Add(dr);

                //    //TextBox2.Text = sample + field.Key.ToString() + ',';
                //    fields.SetField(field.Key.ToString(), field.Key.ToString());
                //}


                //------------------------------------------------

                // form flattening rids the form of editable text fields so
                // the final output can't be edited




                stamper.FormFlattening = true;
                // closing the stamper
                stamper.Close();
                //if (clientname.Contains("campaigntrack"))
                //{
                //    viewpdf.Attributes.Add("src", "pdf\\PaySlipDefault-" + firstpayperiod + "-" + secondpayperiod + "" + payslipid + ".pdf");
                //}
                //else
                //{
                //    viewpdf.Attributes.Add("src", "pdf\\PaySlipDefault-" + firstpayperiod + "-" + secondpayperiod + "" + payslipid + ".pdf");
                //}
                //viewpdf.Attributes.Add("onload", "check_if_open()");
                //Response.Redirect("~/pdf/PaySlip-I101.pdf");
                #endregion

            }
            catch (Exception ex)
            {
                #region catch empty pdf            
                string path = HttpContext.Current.Server.MapPath("pdf");
                var pdfReader1 = new PdfReader(path + "/PaySlipDefault.pdf");
                AcroFields af = pdfReader1.AcroFields;
                //LocationTextExtractionStrategy lteStrategy = new LocationTextExtractionStrategy();
                //string strExact = PdfTextExtractor.GetTextFromPage(pdfReader1,1, lteStrategy);
                //StringBuilder sb = new StringBuilder();
                //string sam = "";
                //foreach (var field in af.Fields)
                //{
                //    sam = sam + field.Key + "-"; // names of textfields
                //}
                //viewpdf.InnerText = sam;
                // get the file paths

                // Template file path
                string formFile = path + "/PaySlipDefault.pdf";

                // Output file path
                string newFile = path + "/Payslip-" + empid + ".pdf";

                // read the template file
                PdfReader reader = new PdfReader(formFile);

                // instantiate PDFStamper object
                // The Output file will be created from template file and edited by the PDFStamper
                PdfStamper stamper = new PdfStamper(reader, new FileStream(newFile, FileMode.Create));

                // Object to deal with the Output file's textfields
                AcroFields fields = stamper.AcroFields;
                fields.SetField("CompanyLoanDeductionCurrent", ex.ToString());
                // form flattening rids the form of editable text fields so
                // the final output can't be edited
                //stamper.FormFlattening = true;
                // closing the stamper  
                stamper.Close();
                //viewpdf.Attributes.Add("src", "pdf\\PaySlipDefault-" + empid + ".pdf");
                //viewpdf.Attributes.Add("onload", "check_if_open()");

                #endregion
            }
            //}

            #endregion



            return "Success";


        }

        string monthname(string month)
        {
            switch (month)
            {
                case "January":
                    month = "01";
                    break;
                case "February":
                    month = "02";
                    break;
                case "March":
                    month = "03";
                    break;
                case "April":
                    month = "04";
                    break;
                case "May":
                    month = "05";
                    break;
                case "June":
                    month = "06";
                    break;
                case "July":
                    month = "07";
                    break;
                case "August":
                    month = "08";
                    break;
                case "September":
                    month = "09";
                    break;
                case "October":
                    month = "10";
                    break;
                case "November":
                    month = "11";
                    break;
                case "December":
                    month = "12";
                    break;
                default: month = "00"; break;
            }
            return month;
        }
        #endregion

        #region Newcore Dynamic Standard Payslip
        string NewcorePayslipParameterChecker(NewcorePayslipParameters p)
        {
            // Check Parameter if its null
            if (p == null) return "The parameters are null";

            // Branch Checker
            if (p.branch.ToLower() != "nci" && p.branch.ToLower() != "darvin" && p.branch.ToLower() != "nits")
                return "Branch name is invalid.\nValid branches [NCI, NITS, Darvin]";

            // Pay Period Checker
            if (p.pay_periods.Count < 1)
                return "Please add a pay period";

            // Payout Date Checker
            if (p.payout_date.Trim() == "" || p.payout_date.Trim() == null)
                return "Please add a payout date";

            return "ok";
        }

        string NewcorePayslipValuesChecker(string _result)
        {
            string response;

            switch (_result)
            {
                case "0":
                    response = "Is not existing in the employee master table";
                    break;
                case "1":
                    response = "Employee has no records of government IDs";
                    break;
                case "2":
                    response = "Employee has no netpay";
                    break;
                case "4":
                    response = "An error has occured in generating the payslip file";
                    break;
                default:
                    response = _result;
                    break;
            }
            return response;
        }

        [HttpPost]
        [Route("sendpaysliplist_new")]
        public string NewcoreDynamicPayslipList(NewcorePayslipParameters p)
        {
            string result = "";

            // Parameter Checker
            if (NewcorePayslipParameterChecker(p) != "ok") return NewcorePayslipParameterChecker(p);

            if (p.will_send_to_email.Trim() == "" || p.will_send_to_email.Trim() == null)
                p.will_send_to_email = "0";

            try
            {
                result = NewcoreGenerateAndSendPayslip("newcore", p);
            }
            catch (Exception e)
            {
                result = e.ToString();
                //throw;
            }
            return result;
        }

        string NewcoreGenerateAndSendPayslip(string database, NewcorePayslipParameters _p)
        {
            string result = "Success";
            string empIDTracker = null;
            int rowTracker = 0;
            string responseStatus = null;
            string filenameTracker = null;

            // Get Database Connection
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = database;
            SelfieRegistration2Controller.GetDB2(GDB);
            Connection conGetEmails = new Connection();

            // Instantiate a User Defined Table for pay Periods
            DataTable pay_period_table = new DataTable();
            pay_period_table.Columns.AddRange(new DataColumn[1]{
                new DataColumn("pay_period")
            });
            foreach (var pp in _p.pay_periods)
            {
                pay_period_table.Rows.Add(pp.period);
            }

            conGetEmails.myparameters.Clear();
            conGetEmails.myparameters.Add(new myParameters { ParameterName = "@pay_periods", mytype = SqlDbType.Structured, Value = pay_period_table });
            conGetEmails.myparameters.Add(new myParameters { ParameterName = "@pay_day", mytype = SqlDbType.VarChar, Value = _p.payout_date });
            conGetEmails.myparameters.Add(new myParameters { ParameterName = "@branch", mytype = SqlDbType.VarChar, Value = _p.branch });
            DataTable DT = conGetEmails.GetDataTable("sp_getEmails_v2");

            // Check if there is data to generate payslip
            if (DT.Rows.Count < 1) return "There's no data to generate...";

            // Set network connection to TLS
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;

            // Start Generating Payslips
            try
            {
                var webRootPath = HttpContext.Current.Server.MapPath("~/pdf/");
                string customDir = "NewcorePayslips/";
                string payslipDir = System.IO.Path.Combine(webRootPath, customDir);
                string payoutDateDir = System.IO.Path.Combine(payslipDir, _p.payout_date);
                string zipFileName = $"/Newcore_{_p.branch}_{_p.payout_date}.zip";

                if (!Directory.Exists(payslipDir))
                    Directory.CreateDirectory(payslipDir);

                if (!Directory.Exists(payoutDateDir))
                    Directory.CreateDirectory(payoutDateDir);

                if (File.Exists(payoutDateDir + zipFileName))
                    File.Delete(payoutDateDir + zipFileName);

                using (var archive = ZipFile.Open(payoutDateDir + zipFileName, ZipArchiveMode.Create))
                {
                    foreach (DataRow row in DT.Rows)
                    {
                        // Tracker
                        rowTracker++;
                        empIDTracker = row["EmpID"] == null ? "Null Emp ID" : row["EmpID"].ToString();

                        string pay_period = row["PayPeriod"].ToString();
                        bool hasEmail = row["Email"].ToString().Trim() != "" ? true : false;

                        string filename = GenPayslipV4(database, pay_period, _p.payout_date, pay_period.Substring(11, 4), _p.branch, row["EmpID"].ToString(), payslipDir, hasEmail);

                        string response = NewcorePayslipValuesChecker(filename);

                        if (response == filename)
                        {
                            filenameTracker = payoutDateDir + filename;

                            if (_p.will_send_to_email == "1" && row["Email"].ToString().Trim() != "")
                            {
                                #region Get company_email JSON
                                string path = HttpContext.Current.Server.MapPath("~/appsettings.json");
                                string jsonString = File.ReadAllText(path);
                                var jsonObject = JObject.Parse(jsonString);
                                var json_company_email = jsonObject["company_email"].ToString();
                                company_email sendPayslipCredentials = JsonConvert.DeserializeObject<company_email>(json_company_email);
                                #endregion

                                //comment this section for no email
                                string emailaccount = sendPayslipCredentials.email;
                                string emailpassword = sendPayslipCredentials.password;
                                string loopemail = row["Email"].ToString();
                                string prefix = HttpContext.Current.Server.MapPath("~/pdf/");
                                using (MailMessage mm = new MailMessage(emailaccount, loopemail))//  "rvitug@illimitado.com"))
                                {
                                    MailAddress aliasmail = new MailAddress(emailaccount, emailaccount);
                                    MailAddress aliasreplymail = new MailAddress(emailaccount);
                                    //MailAddress aliasmailBCC = new MailAddress("durusthr@illimitado.com");
                                    mm.From = aliasmail;
                                    //mm.Bcc.Add(aliasmailBCC);
                                    Connection con = new Connection();
                                    con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = row["EmpID"].ToString() });
                                    DataTable dt2 = con.GetDataTable("sp_GetPaySlipRecordsEmpMaster");
                                    mm.Attachments.Add(new Attachment(filenameTracker));

                                    mm.IsBodyHtml = false;
                                    SmtpClient smtp = new SmtpClient();
                                    smtp.Host = "smtp.office365.com";
                                    smtp.EnableSsl = true;
                                    NetworkCredential NetworkCred = new NetworkCredential(emailaccount, emailpassword);
                                    smtp.UseDefaultCredentials = true;
                                    smtp.Credentials = NetworkCred;
                                    smtp.Port = 587;
                                    smtp.Send(mm);
                                }
                                //comment this section for no email
                            }

                            archive.CreateEntryFromFile(payoutDateDir + filename, System.IO.Path.GetFileName(filenameTracker));
                        }
                        else
                        {
                            responseStatus += $"Row: {rowTracker}, EmployeeID: {empIDTracker}, Problem: {response}\n";
                        }
                    }
                }

                // local result
                //result = payoutDateDir + zipFileName;
                //ZipFile.CreateFromDirectory($"{payoutDateDir + zipFileName}_2", $"{payoutDateDir}\\with email");
                if (responseStatus == null)
                    responseStatus = "Successfully generated all the payslips";
                // webroot result
                string onlineDir = $"http://ws.durusthr.com/ILM_WS_Live/pdf/";
                string resultDir = $"{onlineDir}{customDir}{_p.payout_date}{zipFileName}";
                result = $"Link: [ {resultDir} ] \nStatus:[\n {responseStatus} \n]";
            }
            catch (Exception e)
            {
                result = $"Index: {rowTracker}, EmployeeID: {empIDTracker}, FileName: {filenameTracker} , Error: {e}";
                rowTracker = 0;
                empIDTracker = null;
                //throw;
            }
            
            return result;
        }

        public string GenPayslipV4(string db, string pay_period, string payout_day, string pay_year, string branch, string emp_id, string main_path, bool hasEmail)
        {
            try
            {
                // Get Connection
                GetDatabase GDB = new GetDatabase();
                GDB.ClientName = db;
                SelfieRegistration2Controller.GetDB2(GDB);

                #region Extra Params
                //string DDLMonths = "08";
                string DDLPayPeriod = pay_period;
                string DDLYear = pay_year;
                empid = emp_id;

                empidprof = empid;

                Connection con = new Connection();
                //con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = empid });
                //paysliprecordsempmaster = con.GetDataTable("sp_GetPaySlipRecordsEmpMaster");

                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = empid });
                paysliprecordsempmaster = con.GetDataTable("sp_GetPaySlipRecordsEmpMaster");

                if (paysliprecordsempmaster.Rows.Count < 1) return "0";

                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = empid });
                paysliprecordsconfidentialinfo = con.GetDataTable("sp_GetPaySlipRecordsConfidentialInfo");

                //if (paysliprecordsconfidentialinfo.Rows.Count < 1) return "1";

                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = empid });
                paysliprecordsemployeepay = con.GetDataTable("sp_GetPaySlipRecordsEmployeePay");

                //if (paysliprecordsemployeepay.Rows.Count < 1) return "2";
                #endregion

                #region "New Payslip"
                TotalValue = 0;
                
                string filename = "no filename";
                
                #region Fixed payslip
                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = empid });
                con.myparameters.Add(new myParameters { ParameterName = "@PAYPERIOD", mytype = SqlDbType.NVarChar, Value = DDLPayPeriod });
                paysliprecordspayslipdetails = con.GetDataTable("sp_GetPaySlipRecordsPaySlipDetails");


                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = empid });
                con.myparameters.Add(new myParameters { ParameterName = "@YEAR", mytype = SqlDbType.NVarChar, Value = DDLYear });
                con.myparameters.Add(new myParameters { ParameterName = "@PAYPERIOD", mytype = SqlDbType.NVarChar, Value = DDLPayPeriod });
                YearToDate = con.GetDataTable("sp_GetPaySlipRecordsPaySlipDetailsYearToDate");

                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@empId", mytype = SqlDbType.NVarChar, Value = empid });
                DataTable DTProfile = con.GetDataTable("sp_getCompleteProfile_Mobilev2");
                    
                var lnameencrypt = paysliprecordsempmaster.Rows[0][4].ToString().Trim();
                var lname = paysliprecordsempmaster.Rows[0][5].ToString().Trim();
                var fname = paysliprecordsempmaster.Rows[0][6].ToString().Trim();

                //string path2 = HttpContext.Current.Server.MapPath("pdf");

                // Output file path
                
                string payslipDir = main_path;
                string payoutDateDir = System.IO.Path.Combine(payslipDir, payout_day);
                string newFile = null;
                string emailDir = hasEmail ? "/with email" : "/without email";

                if (!Directory.Exists(payoutDateDir + emailDir))
                    Directory.CreateDirectory(payoutDateDir + emailDir);

                filename = lname + "_" + fname + "-payslip-" + DDLPayPeriod + ".pdf";
                newFile = payoutDateDir + emailDir + "/" + filename;

                // Get The Template File
                PdfReader reader;
                if (db.ToLower() == "newcore")
                {
                    if (DDLPayPeriod == "2020-02-29-2020-02-29")
                        reader = new PdfReader(payslipDir + "/PaySlipDefaultNewcore(TaxableIncentive).pdf");
                    else
                        reader = new PdfReader(payslipDir + "/PaySlipDefaultNewcore.pdf");
                }
                else
                {
                    reader = new PdfReader(payslipDir + "/PaySlipDefault.pdf");
                }

                // instantiate PDFStamper object
                // The Output file will be created from template file and edited by the PDFStamper
                PdfStamper stamper = new PdfStamper(reader, new FileStream(newFile, FileMode.Create));

                // PDF Encryption <<<<<<<<<<<<<<<<<=======================
                #region PDF Encryption
                string pdfpass1 = "", pdfpass2 = "";

                if (db == "newcore")
                {
                    Connection Connection = new Connection();
                    Connection.myparameters.Add(new myParameters { ParameterName = "@empid", mytype = SqlDbType.NVarChar, Value = empid });
                    //DataTable DT = Connection.GetDataTable("sp_getEmailsbyEmpID"); //with email
                    DataTable DT = Connection.GetDataTable("sp_getEmailsbyEmpID2"); //no emails and zips
                    if (DT.Rows.Count > 0)
                    {
                        if (empid == "049-0614" || empid == "314-0619")
                        {
                            pdfpass1 = empid;
                            pdfpass2 = empid;
                        }
                        else
                        {
                            pdfpass1 = DT.Rows[0][2].ToString();
                            pdfpass2 = DT.Rows[0][2].ToString();
                        }
                    }
                    else
                    {
                        pdfpass1 = lnameencrypt.ToLower();
                        pdfpass2 = lname;
                    }
                }
                else
                {
                    if (empid == "049-0614" || empid == "314-0619")
                    {
                        pdfpass1 = empid;
                        pdfpass2 = empid;
                    }
                    else
                    {
                        pdfpass1 = lnameencrypt.ToLower();
                        pdfpass2 = lname;
                    }
                }
                if (empid == "002-0812")
                {
                    pdfpass1 = "DanielDJ!";
                    pdfpass2 = "DanielDJ!";
                }
                else if (empid == "001-0812")
                {
                    pdfpass1 = "JacquelineDJ!";
                    pdfpass2 = "JacquelineDJ!";
                }
                else if (empid == "356-0119")
                {
                    pdfpass1 = "GraceOlg!";
                    pdfpass2 = "GraceOlg!";
                }
                else if (empid == "376-0120")
                {
                    pdfpass1 = "SherwinS!";
                    pdfpass2 = "SherwinS!";
                }
                else if (empid == "154-0817")
                {
                    pdfpass1 = "Newcore6";
                    pdfpass2 = "Newcore6";
                }
                else if (empid == "392-0120")
                {
                    pdfpass1 = "rommelSA!";
                    pdfpass2 = "rommelSA!";
                }
                else if (empid == "049-0614" || empid == "314-0619")
                {
                    pdfpass1 = "Aranes";
                    pdfpass2 = "Aranes";
                }
                stamper.SetEncryption(true, pdfpass1, "Illimitado", PdfWriter.ALLOW_SCREENREADERS);
                #endregion

                // Object to deal with the Output file's textfields
                AcroFields fields = stamper.AcroFields;

                //------------------------------------------------
                //loop to display all field names

                // Select company name based on the branch selected
                string companyNameDisplay;
                switch (branch.ToLower())
                {
                    case "nci":
                        companyNameDisplay = "Newcore Industries International Inc.";
                        break;
                    case "nits":
                        companyNameDisplay = "Newcore IT Solutions";
                        break;
                    case "darvin":
                        companyNameDisplay = "Darvin2305 Solutions Corp.";
                        break;
                    default:
                        companyNameDisplay = "Newcore Industries International Inc.";
                        break;
                }

                fields.SetField("Company Name", companyNameDisplay);//company Name Newcore
                if (paysliprecordsempmaster.Rows.Count > 0)
                {
                    //fields.SetFieldProperty("txtempid", "setflags", PdfAnnotation.FLAGS_HIDDEN,null);
                    fields.SetField("untitled2", paysliprecordsempmaster.Rows[0][0].ToString());//Employee Name
                    fields.SetField("employeeno", empid);//Employee No
                    fields.SetField("joined", paysliprecordsempmaster.Rows[0][2].ToString());//Joined
                    fields.SetField("resigned", paysliprecordsempmaster.Rows[0][3].ToString());//Resigned
                }
                else
                {

                }
                if (paysliprecordsconfidentialinfo.Rows.Count > 0)
                {
                    fields.SetField("pagibigno", paysliprecordsconfidentialinfo.Rows[0][0].ToString());//Pag-IBIG No
                    fields.SetField("philhealthno", paysliprecordsconfidentialinfo.Rows[0][1].ToString());//PhilHealth No
                    fields.SetField("sssno", paysliprecordsconfidentialinfo.Rows[0][2].ToString());//SSS No
                    fields.SetField("taxno", paysliprecordsconfidentialinfo.Rows[0][3].ToString());//Tax No
                    fields.SetField("taxcategory", paysliprecordsconfidentialinfo.Rows[0][4].ToString());//Tax Category
                }
                else
                {

                }
                if (paysliprecordsemployeepay.Rows.Count > 0)
                {
                    fields.SetField("bankaccount", paysliprecordsemployeepay.Rows[0][0].ToString());//Bank Account
                    fields.SetField("hourlyrate", paysliprecordsemployeepay.Rows[0][1].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordsemployeepay.Rows[0][1].ToString())));//Hourly Rate
                }
                else
                {
                    fields.SetField("hourlyrate", "0.00");
                }
                fields.SetField("payperiod", DDLPayPeriod);//Pay Period

                //--New Start
                #region New PDF
                  
                // Payslip Details Here ===========================================================================================
                if (db == "newcore")
                {
                    con = new Connection();
                    con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = empid });
                    con.myparameters.Add(new myParameters { ParameterName = "@PAYPERIOD", mytype = SqlDbType.NVarChar, Value = pay_period });

                    // Select branch payslip_details based on the branch selected
                    switch (branch.ToLower())
                    {
                        case "nci":
                            paysliprecordspayslipdetails = con.GetDataTable("sp_GetPaySlipRecordsPaySlipDetailsNewcore"); //Newcore
                            break;
                        case "nits":
                            paysliprecordspayslipdetails = con.GetDataTable("sp_GetPaySlipRecordsPaySlipDetailsNITS");//NITS
                            break;
                        case "darvin":
                            paysliprecordspayslipdetails = con.GetDataTable("sp_GetPaySlipRecordsPaySlipDetailsDarvin");//Darvin
                            break;
                        default:
                            paysliprecordspayslipdetails = con.GetDataTable("sp_GetPaySlipRecordsPaySlipDetailsNewcore"); //Newcore
                            break;
                    }

                    
                    if (paysliprecordspayslipdetails.Rows.Count > 0)
                    {
                        fields.SetField("payday", paysliprecordspayslipdetails.Rows[0]["Pay_Day"].ToString());
                        fields.SetField("hourlyrate", "");
                        fields.SetField("basicsalarycurrent", paysliprecordspayslipdetails.Rows[0]["Basic Salary Amount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Basic Salary Amount"].ToString())));
                        fields.SetField("monthlyrate", paysliprecordspayslipdetails.Rows[0]["Monthly Salary Rate"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Monthly Salary Rate"].ToString())));
                        //fields.SetField("monthlyrate", paysliprecordspayslipdetails.Rows[0]["Monthly Salary Rate"].ToString() == "" ? "0.00" : String.Format("{0:n}", Math.Ceiling(Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Monthly Salary Rate"].ToString()))) + " USD");//darvin
                        fields.SetField("deductionsphilhealthcurrent", paysliprecordspayslipdetails.Rows[0]["PhilHealth Employee Share"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["PhilHealth Employee Share"].ToString())));
                        fields.SetField("deductionspagibigcurrent", paysliprecordspayslipdetails.Rows[0]["Pag-ibig Employee Share"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Pag-ibig Employee Share"].ToString())));
                        fields.SetField("deductionsssscurrent", paysliprecordspayslipdetails.Rows[0]["SSS Employee Share"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["SSS Employee Share"].ToString())));
                        fields.SetField("deductionswtaxcurrent", paysliprecordspayslipdetails.Rows[0]["W-Tax"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["W-Tax"].ToString())));
                        fields.SetField("pagibigemployersharecurrent", paysliprecordspayslipdetails.Rows[0]["Pag-ibig Employer Share"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Pag-ibig Employer Share"].ToString())));
                        fields.SetField("philhealthemployersharecurrent", paysliprecordspayslipdetails.Rows[0]["PhilHealth Employer Share"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["PhilHealth Employer Share"].ToString())));
                        fields.SetField("sssemployersharecurrent", paysliprecordspayslipdetails.Rows[0]["SSS Employer Share"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["SSS Employer Share"].ToString())));
                        fields.SetField("sssemployerecsharecurrent", paysliprecordspayslipdetails.Rows[0]["SSS Employer EC Share"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["SSS Employer EC Share"].ToString())));
                        fields.SetField("netpaycurrent", paysliprecordspayslipdetails.Rows[0]["Net Pay"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Net Pay"].ToString())));
                        fields.SetField("mealallowancecurrent", paysliprecordspayslipdetails.Rows[0]["Meal Allowance"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Meal Allowance"].ToString())));
                        fields.SetField("13thmonthnontaxablecurrent", paysliprecordspayslipdetails.Rows[0]["13th Month Non-Taxable"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["13th Month Non-Taxable"].ToString())));

                        fields.SetField("adjabsencecurrent", paysliprecordspayslipdetails.Rows[0]["Adjusted LH Amount (100%)"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Adjusted LH Amount (100%)"].ToString())));
                        fields.SetField("adjlatecurrent", paysliprecordspayslipdetails.Rows[0]["Adjusted LHOT Amt (260%)"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Adjusted LHOT Amt (260%)"].ToString())));
                        fields.SetField("adjregularhourscurrent", paysliprecordspayslipdetails.Rows[0]["Adjusted SH Amount (30%)"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Adjusted SH Amount (30%)"].ToString())));
                        fields.SetField("adjregularotcurrent", paysliprecordspayslipdetails.Rows[0]["Adjusted SHRD Amount (150%)"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Adjusted SHRD Amount (150%)"].ToString())));
                        fields.SetField("deductionsabsenceshrs", paysliprecordspayslipdetails.Rows[0]["Absences Hours"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Absences Hours"].ToString())));
                        fields.SetField("deductionstardinesscurrent", paysliprecordspayslipdetails.Rows[0]["Tardiness Amount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Tardiness Amount"].ToString())));
                        fields.SetField("deductionsabsencescurrent", paysliprecordspayslipdetails.Rows[0]["Absences Amount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Absences Amount"].ToString())));
                        fields.SetField("taxableincomecurrent", paysliprecordspayslipdetails.Rows[0]["Taxable Income"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Taxable Income"].ToString())));
                        fields.SetField("grosspaycurrent", paysliprecordspayslipdetails.Rows[0]["Gross Pay"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Gross Pay"].ToString())));


                        fields.SetField("regularothrs", paysliprecordspayslipdetails.Rows[0]["Regular Overtime Hours"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Regular Overtime Hours"].ToString())));
                        fields.SetField("regularotcurrent", paysliprecordspayslipdetails.Rows[0]["Regular Overtime Amount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Regular Overtime Amount"].ToString())));


                        fields.SetField("restdayothrs", paysliprecordspayslipdetails.Rows[0]["Regular Holiday Hours"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Regular Holiday Hours"].ToString())));
                        fields.SetField("restdayotcurrent", paysliprecordspayslipdetails.Rows[0]["Regular Holiday Amount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Regular Holiday Amount"].ToString())));
                        fields.SetField("restdayndothrs", paysliprecordspayslipdetails.Rows[0]["Regular Holiday Overtime Hours"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Regular Holiday Overtime Hours"].ToString())));
                        fields.SetField("restdayndotcurrent", paysliprecordspayslipdetails.Rows[0]["Regular Holiday Overtime Amount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Regular Holiday Overtime Amount"].ToString())));


                        fields.SetField("regularholidayhrs", paysliprecordspayslipdetails.Rows[0]["Special Holiday Hours"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Special Holiday Hours"].ToString())));
                        fields.SetField("regularholidaycurrent", paysliprecordspayslipdetails.Rows[0]["Special Holiday Amount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Special Holiday Amount"].ToString())));
                        fields.SetField("regularholidayotcurrent", paysliprecordspayslipdetails.Rows[0]["Special Holiday Overtime Hours"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Special Holiday Overtime Hours"].ToString())));

                        fields.SetField("adjregularndothrs", paysliprecordspayslipdetails.Rows[0]["LWP Hours"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["LWP Hours"].ToString())));
                        fields.SetField("adjregularndotcurrent", paysliprecordspayslipdetails.Rows[0]["LWP Amount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["LWP Amount"].ToString())));




                        fields.SetField("regularndotcurrent", paysliprecordspayslipdetails.Rows[0]["RD Amount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["RD Amount"].ToString())));
                        fields.SetField("regularndothrs", paysliprecordspayslipdetails.Rows[0]["RDOT Hours"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["RDOT Hours"].ToString())));
                        fields.SetField("restdaycurrent", paysliprecordspayslipdetails.Rows[0]["RDOT Amount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["RDOT Amount"].ToString())));

                        fields.SetField("taxrefundcurrent", paysliprecordspayslipdetails.Rows[0]["HDMF Loans Amt"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["HDMF Loans Amt"].ToString())));
                        fields.SetField("deductionstaxpayablecurrent", paysliprecordspayslipdetails.Rows[0]["SSS Loans Amt"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["SSS Loans Amt"].ToString())));
                        fields.SetField("deminimiscurrent", paysliprecordspayslipdetails.Rows[0]["De Minimis/Adj DM"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["De Minimis/Adj DM"].ToString())));



                        fields.SetField("regularholidayndothrs", paysliprecordspayslipdetails.Rows[0]["Adjusted Regular Hours"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Adjusted Regular Hours"].ToString())));
                        fields.SetField("regularholidayndotcurrent", paysliprecordspayslipdetails.Rows[0]["Adjusted Reg Hrs. Amount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Adjusted Reg Hrs. Amount"].ToString())));
                        fields.SetField("adjregularotcurrent", paysliprecordspayslipdetails.Rows[0]["Adjusted SHOT Amount (169%)"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Adjusted SHOT Amount (169%)"].ToString())));
                        fields.SetField("ecolacurrent", paysliprecordspayslipdetails.Rows[0]["Other NT Deductions"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Other NT Deductions"].ToString())));
                        fields.SetField("specialholidayotcurrent", paysliprecordspayslipdetails.Rows[0]["Adjusted RD Amount (130%)"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Adjusted RD Amount (130%)"].ToString())));
                        fields.SetField("adjregularhourshrs", paysliprecordspayslipdetails.Rows[0]["Adj Spl Hol Hours"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Adj Spl Hol Hours"].ToString())));

                        fields.SetField("deductionsplacurrent", paysliprecordspayslipdetails.Rows[0]["PLA"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["PLA"].ToString())));
                        fields.SetField("deductionstardinesshrs", paysliprecordspayslipdetails.Rows[0]["Tardiness Minutes"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Tardiness Minutes"].ToString())));

                        fields.SetField("regularndothrs", paysliprecordspayslipdetails.Rows[0]["RD Hours"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["RD Hours"].ToString())));


                        fields.SetField("specialholidaycurrent", paysliprecordspayslipdetails.Rows[0]["Adjusted Reg OT Amount (125%)"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Adjusted Reg OT Amount (125%)"].ToString())));


                        fields.SetField("13thmonthtaxablecurrent", paysliprecordspayslipdetails.Rows[0]["13th Month Taxable"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["13th Month Taxable"].ToString())));
                        fields.SetField("deductionsprulifecurrent", paysliprecordspayslipdetails.Rows[0]["Pru Life"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Pru Life"].ToString())));
                        fields.SetField("deductionssdcurrent", paysliprecordspayslipdetails.Rows[0]["SD"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["SD"].ToString())));

                        fields.SetField("deductionssssloanamountcurrent", paysliprecordspayslipdetails.Rows[0]["Tax Payable"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Tax Payable"].ToString())));


                        fields.SetField("specialholidayhrs", paysliprecordspayslipdetails.Rows[0]["Adjusted Regular OT Hours"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Adjusted Regular OT Hours"].ToString())));





                        fields.SetField("adjrestdayotcurrent", paysliprecordspayslipdetails.Rows[0]["Tax Incentive"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Tax Incentive"].ToString())));
                        fields.SetField("deductionspagibigloanamountcurrent", paysliprecordspayslipdetails.Rows[0]["AVEGA"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["AVEGA"].ToString())));
                        fields.SetField("adjrdcurrent", paysliprecordspayslipdetails.Rows[0]["Salary Adjustment"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["Salary Adjustment"].ToString())));

                        fields.SetField("adjnontaxableallowancecurrent", paysliprecordspayslipdetails.Rows[0]["ECOLA"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(paysliprecordspayslipdetails.Rows[0]["ECOLA"].ToString())));//Basic Salary
                    }
                }

                // YTD Here ===========================================================================================
                if (db == "newcore")
                {
                    con = new Connection();
                    con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = empid });
                    con.myparameters.Add(new myParameters { ParameterName = "@PAYPERIOD", mytype = SqlDbType.NVarChar, Value = pay_period });
                    con.myparameters.Add(new myParameters { ParameterName = "@YEAR", mytype = SqlDbType.NVarChar, Value = pay_year});

                    // Select branch payslip_details year based on the branch selected
                    switch (branch.ToLower())
                    {
                        case "nci":
                            YearToDate = con.GetDataTable("sp_GetPaySlipRecordsPaySlipDetailsYearToDateNewcore"); //Newcore
                            break;
                        case "nits":
                            YearToDate = con.GetDataTable("sp_GetPaySlipRecordsPaySlipDetailsYearToDateNITS");//NITS
                            break;
                        case "darvin":
                            YearToDate = con.GetDataTable("sp_GetPaySlipRecordsPaySlipDetailsYearToDateDarvin");//Darvin
                            break;
                        default:
                            YearToDate = con.GetDataTable("sp_GetPaySlipRecordsPaySlipDetailsYearToDateNewcore"); //Newcore
                            break;
                    }

                    if (YearToDate.Rows.Count > 0)
                    {
                        fields.SetField("basicsalaryytd", YearToDate.Rows[0]["Basic Salary Amount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["Basic Salary Amount"].ToString())));
                        fields.SetField("deductionspagibigytd", YearToDate.Rows[0]["Pag-ibig Employee Share"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["Pag-ibig Employee Share"].ToString())));
                        fields.SetField("deductionsphilhealthytd", YearToDate.Rows[0]["PhilHealth Employee Share"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["PhilHealth Employee Share"].ToString())));
                        fields.SetField("deductionssssytd", YearToDate.Rows[0]["SSS Employee Share"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["SSS Employee Share"].ToString())));
                        fields.SetField("deductionswtaxytd", YearToDate.Rows[0]["W-Tax"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["W-Tax"].ToString())));
                        fields.SetField("pagibigemployershareytd", YearToDate.Rows[0]["Pag-ibig Employer Share"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["Pag-ibig Employer Share"].ToString())));
                        fields.SetField("philhealthemployershareytd", YearToDate.Rows[0]["PhilHealth Employer Share"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["PhilHealth Employer Share"].ToString())));
                        fields.SetField("sssemployershareytd", YearToDate.Rows[0]["SSS Employer Share"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["SSS Employer Share"].ToString())));
                        fields.SetField("sssemployerecshareytd", YearToDate.Rows[0]["SSS Employer EC Share"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["SSS Employer EC Share"].ToString())));
                        fields.SetField("netpayytd", YearToDate.Rows[0]["Net Pay"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["Net Pay"].ToString())));
                        fields.SetField("adjnontaxableallowanceytd", YearToDate.Rows[0]["Meal Allowance"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["Meal Allowance"].ToString())));
                        fields.SetField("13monthnontaxableytd", YearToDate.Rows[0]["13th Month Non-Taxable"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["13th Month Non-Taxable"].ToString())));

                        fields.SetField("adjabsenceytd", YearToDate.Rows[0]["Adjusted LH Amount (100%)"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["Adjusted LH Amount (100%)"].ToString())));
                        fields.SetField("adjlateytd", YearToDate.Rows[0]["Adjusted LHOT Amt (260%)"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["Adjusted LHOT Amt (260%)"].ToString())));
                        fields.SetField("adjregularhoursytd", YearToDate.Rows[0]["Adjusted SH Amount (30%)"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["Adjusted SH Amount (30%)"].ToString())));
                        fields.SetField("adjregularotytd", YearToDate.Rows[0]["Adjusted SHRD Amount (150%)"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["Adjusted SHRD Amount (150%)"].ToString())));

                        fields.SetField("deductionstardinessytd", YearToDate.Rows[0]["Tardiness Amount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["Tardiness Amount"].ToString())));
                        fields.SetField("deductionsabsencesytd", YearToDate.Rows[0]["Absences Amount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["Absences Amount"].ToString())));
                        fields.SetField("taxableincomeytd", YearToDate.Rows[0]["Taxable Income"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["Taxable Income"].ToString())));
                        fields.SetField("grosspayytd", YearToDate.Rows[0]["Gross Pay"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["Gross Pay"].ToString())));



                        fields.SetField("regularotytd", YearToDate.Rows[0]["Regular Overtime Amount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["Regular Overtime Amount"].ToString())));



                        fields.SetField("restdayotytd", YearToDate.Rows[0]["Regular Holiday Amount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["Regular Holiday Amount"].ToString())));

                        fields.SetField("restdayndotytd", YearToDate.Rows[0]["Regular Holiday Overtime Amount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["Regular Holiday Overtime Amount"].ToString())));



                        fields.SetField("regularholidayytd", YearToDate.Rows[0]["Special Holiday Amount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["Special Holiday Amount"].ToString())));
                        fields.SetField("regularholidayotytd", YearToDate.Rows[0]["Special Holiday Overtime Hours"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["Special Holiday Overtime Hours"].ToString())));


                        fields.SetField("adjregularndotytd", YearToDate.Rows[0]["LWP Amount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["LWP Amount"].ToString())));




                        fields.SetField("regularndotytd", YearToDate.Rows[0]["RD Amount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["RD Amount"].ToString())));

                        fields.SetField("restdayytd", YearToDate.Rows[0]["RDOT Amount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["RDOT Amount"].ToString())));

                        fields.SetField("taxrefundytd", YearToDate.Rows[0]["HDMF Loans Amt"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["HDMF Loans Amt"].ToString())));
                        fields.SetField("deductionstaxpayableytd", YearToDate.Rows[0]["SSS Loans Amt"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["SSS Loans Amt"].ToString())));
                        fields.SetField("deminimisytd", YearToDate.Rows[0]["De Minimis/Adj DM"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["De Minimis/Adj DM"].ToString())));




                        fields.SetField("regularholidayndotytd", YearToDate.Rows[0]["Adjusted Reg Hrs. Amount"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["Adjusted Reg Hrs. Amount"].ToString())));
                        fields.SetField("adjregularotcurrent", YearToDate.Rows[0]["Adjusted SHOT Amount (169%)"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["Adjusted SHOT Amount (169%)"].ToString())));
                        fields.SetField("ecolaytd", YearToDate.Rows[0]["Other NT Deductions"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["Other NT Deductions"].ToString())));
                        fields.SetField("specialholidayotytd", YearToDate.Rows[0]["Adjusted RD Amount (130%)"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["Adjusted RD Amount (130%)"].ToString())));


                        fields.SetField("deductionsplaytd", YearToDate.Rows[0]["PLA"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["PLA"].ToString())));





                        fields.SetField("specialholidayytd", YearToDate.Rows[0]["Adjusted Reg OT Amount (125%)"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["Adjusted Reg OT Amount (125%)"].ToString())));


                        fields.SetField("13thmonthtaxableytd", YearToDate.Rows[0]["13th Month Taxable"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["13th Month Taxable"].ToString())));
                        fields.SetField("deductionsprulifeytd", YearToDate.Rows[0]["Pru Life"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["Pru Life"].ToString())));
                        fields.SetField("deductionssdytd", YearToDate.Rows[0]["SD"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["SD"].ToString())));

                        fields.SetField("deductionssssloanamountytd", YearToDate.Rows[0]["Tax Payable"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["Tax Payable"].ToString())));








                        fields.SetField("adjrestdayotytd", YearToDate.Rows[0]["Tax Incentive"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["Tax Incentive"].ToString())));
                        fields.SetField("deductionspagibigloanamountytd", YearToDate.Rows[0]["AVEGA"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["AVEGA"].ToString())));
                        fields.SetField("adjrdytd", YearToDate.Rows[0]["Salary Adjustment"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["Salary Adjustment"].ToString())));


                        fields.SetField("othernontaxableadjustmentytd", YearToDate.Rows[0]["ECOLA"].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0]["ECOLA"].ToString())));

                    }
                }
                else
                {
                    if (YearToDate.Rows.Count > 0)
                    {
                        fields.SetField("basicsalaryytd", YearToDate.Rows[0][13].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][13].ToString())));//Basic Salary
                        fields.SetField("deductionsabsencesytd", YearToDate.Rows[0][14].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][14].ToString())));//Deduction absences
                        fields.SetField("deductionstardinessytd", YearToDate.Rows[0][15].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][15].ToString())));//Deduction lates
                        fields.SetField("13thmonthtaxableytd", YearToDate.Rows[0][16].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][16].ToString())));//13 month

                        fields.SetField("regularotytd", YearToDate.Rows[0][17].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][17].ToString())));//Regular OT
                        fields.SetField("regularndotytd", YearToDate.Rows[0][18].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][18].ToString())));//Regular Night Diff

                        fields.SetField("restdayotytd", YearToDate.Rows[0][52].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][52].ToString())));//Reg RDOT
                        fields.SetField("restdayndotytd", YearToDate.Rows[0][19].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][19].ToString())));//Reg RDNDOT

                        fields.SetField("regularholidayytd", YearToDate.Rows[0][20].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][20].ToString())));//Regular Holiday
                        fields.SetField("regularholidayndotytd", YearToDate.Rows[0][50].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][50].ToString())));//Regular Holiday Night Diff
                        fields.SetField("regularholidayotytd", YearToDate.Rows[0][21].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][21].ToString())));//Regular Holiday OT

                        fields.SetField("specialholidayytd", YearToDate.Rows[0][22].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][22].ToString())));//Special Holiday
                        fields.SetField("specialholidayndotytd", YearToDate.Rows[0][51].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][51].ToString())));//Special Holiday Night Diff
                        fields.SetField("specialholidayotytd", YearToDate.Rows[0][23].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][23].ToString())));//Special Holiday OT

                        fields.SetField("adjabsenceytd", YearToDate.Rows[0][56].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][56].ToString())));//Double Holiday
                        fields.SetField("adjlateytd", YearToDate.Rows[0][58].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][58].ToString())));//Double Holiday Night Diff
                        fields.SetField("adjrdndotytd", YearToDate.Rows[0][60].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][60].ToString())));//Double Holiday OT

                        fields.SetField("adjregularhoursytd", YearToDate.Rows[0][26].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][26].ToString())));//Double Holiday
                        fields.SetField("adjregularndotytd", YearToDate.Rows[0][27].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][27].ToString())));//Double Holiday Night Diff
                        fields.SetField("adjregularotytd", YearToDate.Rows[0][28].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][28].ToString())));//Double Holiday OT
                        fields.SetField("adjrestdayotytd", YearToDate.Rows[0][29].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][29].ToString())));//Double Holiday OT

                        fields.SetField("grosspayytd", YearToDate.Rows[0][30].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][30].ToString())));//Basic Salary
                        fields.SetField("deductionssssytd", YearToDate.Rows[0][31].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][31].ToString())));//Basic Salary
                        fields.SetField("deductionsphilhealthytd", YearToDate.Rows[0][32].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][32].ToString())));//Basic Salary
                        fields.SetField("deductionspagibigytd", YearToDate.Rows[0][33].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][33].ToString())));//Basic Salary
                        fields.SetField("taxableincomeytd", YearToDate.Rows[0][34].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][34].ToString())));//Basic Salary
                        fields.SetField("deductionswtaxytd", YearToDate.Rows[0][35].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][35].ToString())));//Basic Salary
                        fields.SetField("deductionsplaytd", YearToDate.Rows[0][36].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][36].ToString())));//Basic Salary
                        fields.SetField("deductionsprulifeytd", YearToDate.Rows[0][37].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][37].ToString())));//Basic Salary
                        fields.SetField("deductionssdytd", YearToDate.Rows[0][38].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][38].ToString())));//Basic Salary
                        fields.SetField("deductionssssloanamountytd", YearToDate.Rows[0][39].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][39].ToString())));//Basic Salary
                        fields.SetField("deductionspagibigloanamountytd", YearToDate.Rows[0][40].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][40].ToString())));//Basic Salary
                        fields.SetField("ecolaytd", YearToDate.Rows[0][41].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][41].ToString())));//Basic Salary
                        fields.SetField("deminimisytd", YearToDate.Rows[0][42].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][42].ToString())));//Basic Salary
                        fields.SetField("adjnontaxableallowanceytd", YearToDate.Rows[0][54].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][54].ToString())));//Basic Salary
                        fields.SetField("13thmonthnontaxableytd", YearToDate.Rows[0][43].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][43].ToString())));//Basic Salary
                        fields.SetField("netpayytd", YearToDate.Rows[0][44].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][44].ToString())));//Basic Salary
                        fields.SetField("sssemployershareytd", YearToDate.Rows[0][45].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][45].ToString())));//Basic Salary
                        fields.SetField("sssemployerecshareytd", YearToDate.Rows[0][46].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][46].ToString())));//Basic Salary
                        fields.SetField("philhealthemployershareytd", YearToDate.Rows[0][47].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][47].ToString())));//Basic Salary
                        fields.SetField("pagibigemployershareytd", YearToDate.Rows[0][48].ToString() == "" ? "0.00" : String.Format("{0:n}", Convert.ToDouble(YearToDate.Rows[0][48].ToString())));//Basic Salary
                    }
                }
                #endregion
                //-- New End
                //------------------------------------------------

                // form flattening rids the form of editable text fields so
                // the final output can't be edited
                stamper.FormFlattening = true;
                // closing the stamper

                stamper.Close();
                #endregion
                
                #endregion
                return $"{emailDir}/{filename}";
            }
            catch (Exception e)
            {
                string errorMessage = "4";
                return errorMessage;
            }
        }
        #endregion
    }



}
