﻿using illimitadoWepAPI.Models;
using illimitadoWepAPI.MyClass;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.pdf.parser;
using iTextSharp.text.html.simpleparser;
using System;
using System.Collections.Generic;
using System.Configuration;

using System.Data;
using System.Data.SqlClient;
using Kairos.Net;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Xml.Serialization;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Http;
using System.Net.Mail;
using System.Collections;
using MimeKit;
using System.Threading.Tasks;
using Microsoft.ProjectOxford.Face;
using Newtonsoft.Json.Linq;
using iTextSharp.text.pdf.parser;

using System.Text;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;

//using Pechkin;
//using System;
using iTextSharp.text.pdf;
//using System.IO;
//using iTextSharp.text;
//using System.Diagnostics;

namespace illimitadoWepAPI.Controllers
{
    public class ReportController : ApiController
    {
        public string testdata = " ";
        public void TempConnectionString()
        {
            var settings = ConfigurationManager.ConnectionStrings["connectionstring"];
            var fi = typeof(ConfigurationElement).GetField(
                          "_bReadOnly",
                          BindingFlags.Instance | BindingFlags.NonPublic);
            fi.SetValue(settings, false);
            //--Local--//
            //settings.ConnectionString = "Data Source=192.168.2.33,1444;Database=CRT_Dbase2019-03-29;User Id=DevUser;Password=securedmgt@1230;";
            //--AWS--//
            settings.ConnectionString = "Data Source=livedb-ilm.c3srcolvclfy.ap-southeast-1.rds.amazonaws.com,1412; Database = OP360DB;User Id=ILM_LiveDBUser;Password=securedmgt1230;";

        }
        public void TempConnectionStringv2()
        {
            var settings = ConfigurationManager.ConnectionStrings["connectionstring"];
            var fi = typeof(ConfigurationElement).GetField(
                          "_bReadOnly",
                          BindingFlags.Instance | BindingFlags.NonPublic);
            fi.SetValue(settings, false);
            //--Local--//
            //settings.ConnectionString = "Data Source=192.168.2.33,1444;Database=CRT_Dbase2019-03-29;User Id=DevUser;Password=securedmgt@1230;";
            //--AWS--//
            settings.ConnectionString = "Data Source=livedb-ilm.c3srcolvclfy.ap-southeast-1.rds.amazonaws.com,1412; Database = DurustHRdb1;User Id=ILM_LiveDBUser;Password=securedmgt1230;";

        }
        public void TempConnectionStringv3()
        {
            var settings = ConfigurationManager.ConnectionStrings["connectionstring"];
            var fi = typeof(ConfigurationElement).GetField(
                          "_bReadOnly",
                          BindingFlags.Instance | BindingFlags.NonPublic);
            fi.SetValue(settings, false);
            //--Local--//
            //settings.ConnectionString = "Data Source=192.168.2.33,1444;Database=CRT_Dbase2019-03-29;User Id=DevUser;Password=securedmgt@1230;";
            //--AWS--//
            settings.ConnectionString = "Data Source=livedb-ilm.c3srcolvclfy.ap-southeast-1.rds.amazonaws.com,1412; Database = DurustHRdb2;User Id=ILM_LiveDBUser;Password=securedmgt1230;";

        }
        [HttpPost]
        [Route("PHER2")]
        public string PHER2(ParamPHER2 parameters)
        {
            try
            {
                ConnectToDatabase_DurustHRDB1();
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@Company", mytype = SqlDbType.VarChar, Value = parameters.TenantID });
                con.myparameters.Add(new myParameters { ParameterName = "@Year", mytype = SqlDbType.VarChar, Value = parameters.Year });
                con.myparameters.Add(new myParameters { ParameterName = "@Month", mytype = SqlDbType.VarChar, Value = parameters.Month });
                DataTable PHER2 = con.GetDataTable("sp_getInfoPHEr2");

                DataTable CompanyInfo = GetCompanyInfo(parameters.TenantID);

                int countRows = PHER2.Rows.Count;
                int counter = 0;
                int decrement = 0;
                int fordecre = 443;
                int total = countRows;
                int totalperpage = 0;
                //decimal list = decimal.Divide(GetPHER2count(parameters.Company, parameters.Month, parameters.Year).Rows.Count, 27);
                decimal list = decimal.Divide(countRows, 27);
                decimal numPage = Math.Ceiling(list);
                string numMonth = "";

                string pathsource;
                pathsource = "PHER2CLEAN";
                string reader1 = HttpContext.Current.Server.MapPath("~/Files/PayrollOutSourcing/PDFTemplate/") + pathsource + ".pdf";
                string pathmoto;
                pathmoto = "PHER2";
                if (parameters.Month.Length == 1)
                {
                    numMonth = "0" + parameters.Month.ToString();
                }
                //string reader2 = HttpContext.Current.Server.MapPath("~/sFTP/PayrollOutsourcing/") + GetCompanyInfo(parameters.TenantID).Rows[0][5].ToString().ToUpper() + "_" + pathmoto + "_" + parameters.Year.ToString() + numMonth + ".pdf";
                string reader2 = HttpContext.Current.Server.MapPath("~/sFTP/PayrollOutsourcing/") + CompanyInfo.Rows[0][5].ToString().ToUpper() + "_" + pathmoto + "_" + parameters.Year.ToString() + numMonth + ".pdf";
                string fileName = reader1;
                string outputFileName = reader2;
                //Step 1: Create a Docuement-Object
                Document document = new Document();
                try
                {
                    //The current file path
                    string filename = fileName;

                    // we create a reader for the document
                    PdfReader reader = new PdfReader(filename);

                    //PdfStamper stamper = new PdfStamper(reader, new FileStream(outputFileName, FileMode.Create));
                    //Step 2: we create a writer that listens to the document
                    PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(outputFileName, FileMode.Create));

                    //Step 3: Open the document
                    document.Open();

                    PdfContentByte cb = writer.DirectContent;

                    for (int i = 1; i <= numPage; i++)
                    {
                        for (int pageNumber = 1; pageNumber < reader.NumberOfPages + 1; pageNumber++)
                        {
                            document.SetPageSize(reader.GetPageSizeWithRotation(1));
                            document.NewPage();

                            //Insert to Destination on the first page
                            if (pageNumber == 1)
                            {
                                Chunk fileRef = new Chunk(" ");
                                fileRef.SetLocalDestination(filename);
                                document.Add(fileRef);
                            }

                            PdfImportedPage page = writer.GetImportedPage(reader, pageNumber);
                            int rotation = reader.GetPageRotation(pageNumber);
                            if (rotation == 90 || rotation == 270)
                            {
                                cb.AddTemplate(page, 0, -1f, 1f, 0, 0, reader.GetPageSizeWithRotation(pageNumber).Height);
                            }
                            else
                            {
                                cb.AddTemplate(page, 1f, 0, 0, 1f, 0, 0);
                            }

                            BaseFont bf = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                            cb.SetColorFill(BaseColor.DARK_GRAY);
                            cb.SetFontAndSize(bf, 10);

                            cb.BeginText();
                            //cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, GetCompanyInfo(parameters.Company).Rows[0][0].ToString().ToUpper(), 172, 519, 0);
                            //cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, GetCompanyInfo(parameters.Company).Rows[0][2].ToString().ToUpper(), 650, 519, 0);
                            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, CompanyInfo.Rows[0][0].ToString().ToUpper(), 172, 519, 0);
                            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, CompanyInfo.Rows[0][2].ToString().ToUpper(), 650, 519, 0);
                            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "", 600, 501, 0);
                            cb.EndText();

                            cb.SetColorFill(BaseColor.DARK_GRAY);
                            cb.SetFontAndSize(bf, 7);

                            //int charString = GetCompanyInfo(parameters.Company).Rows[0][1].ToString().Length;
                            int charString = CompanyInfo.Rows[0][1].ToString().Length;
                            if (charString <= 90)
                            {
                                cb.BeginText();
                                //cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, GetCompanyInfo(parameters.Company).Rows[0][1].ToString().ToUpper().Substring(0, charString), 90, 503, 0);
                                cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, CompanyInfo.Rows[0][1].ToString().ToUpper().Substring(0, charString), 90, 503, 0);
                                cb.EndText();
                            }
                            else
                            {
                                cb.BeginText();
                                //cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, GetCompanyInfo(parameters.Company).Rows[0][1].ToString().ToUpper().Substring(0, 90), 90, 506, 0);
                                //cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, GetCompanyInfo(parameters.Company).Rows[0][1].ToString().ToUpper().Substring(91, charString - 91), 90, 500, 0);
                                cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, CompanyInfo.Rows[0][1].ToString().ToUpper().Substring(0, 90), 90, 506, 0);
                                cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, CompanyInfo.Rows[0][1].ToString().ToUpper().Substring(91, charString - 91), 90, 500, 0);
                                cb.EndText();
                            }

                            if (total > 27)
                            {
                                cb.SetColorFill(BaseColor.DARK_GRAY);
                                cb.SetFontAndSize(bf, 10);

                                totalperpage = 27;
                                total = total - totalperpage;
                                cb.BeginText();
                                cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, totalperpage.ToString(), 145, 67, 0);
                                cb.EndText();
                            }
                            else
                            {
                                cb.SetColorFill(BaseColor.DARK_GRAY);
                                cb.SetFontAndSize(bf, 10);

                                cb.BeginText();
                                cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, total.ToString(), 145, 67, 0);
                                cb.EndText();
                            }

                            cb.SetColorFill(BaseColor.DARK_GRAY);
                            cb.SetFontAndSize(bf, 10);

                            cb.BeginText();
                            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "PAGE " + i + " OF " + numPage + " SHEETS", 250, 40, 0);
                            cb.EndText();

                            cb.BeginText();
                            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "MILDRED GUERRA", 520, 46, 0);
                            cb.EndText();

                            fordecre = 443;

                            for (int j = 0; j < 27; j++)
                            {
                                decrement = j * 13;

                                if (j == 6 || j == 12 || j == 15 || j == 18 || j == 21)
                                {
                                    fordecre = fordecre - 2;
                                }

                                cb.SetColorFill(BaseColor.DARK_GRAY);
                                cb.SetFontAndSize(bf, 7);

                                cb.BeginText();
                                //cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, GetPHER2count(parameters.Company, parameters.Month, parameters.Year).Rows[counter][0].ToString().ToUpper(), 30, fordecre - decrement, 0);
                                //cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, GetPHER2count(parameters.Company, parameters.Month, parameters.Year).Rows[counter][1].ToString().ToUpper(), 118, fordecre - decrement, 0);
                                //cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, GetPHER2count(parameters.Company, parameters.Month, parameters.Year).Rows[counter][2].ToString().ToUpper(), 291, fordecre - decrement, 0);
                                //cb.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, GetPHER2count(parameters.Company, parameters.Month, parameters.Year).Rows[counter][3].ToString().ToUpper(), 480, fordecre - decrement, 0);
                                //cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, GetPHER2count(parameters.Company, parameters.Month, parameters.Year).Rows[counter][4].ToString().ToUpper(), 500, fordecre - decrement, 0);
                                cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, PHER2.Rows[counter][0].ToString().ToUpper(), 30, fordecre - decrement, 0);
                                cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, PHER2.Rows[counter][1].ToString().ToUpper(), 118, fordecre - decrement, 0);
                                cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, PHER2.Rows[counter][2].ToString().ToUpper(), 291, fordecre - decrement, 0);
                                cb.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, PHER2.Rows[counter][3].ToString().ToUpper(), 480, fordecre - decrement, 0);
                                cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, PHER2.Rows[counter][4].ToString().ToUpper(), 500, fordecre - decrement, 0);
                                cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "", 640, fordecre - decrement, 0);
                                cb.EndText();

                                counter = counter + 1;

                                if (counter == countRows)
                                {
                                    document.Close();
                                    break;
                                }
                            }

                            if (counter == countRows)
                            {
                                document.Close();
                                break;
                            }
                        }
                        if (counter == countRows)
                        {
                            document.Close();
                            break;
                        }
                    }
                }
                catch (Exception e)
                {
                    throw e;
                }
                finally
                {
                    document.Close();
                }
                string ReturnPath = "https://ws.durusthr.com/ILM_WS_Live/sFTP/PayrollOutsourcing/" + CompanyInfo.Rows[0][5].ToString().ToUpper() + "_" + pathmoto + "_" + parameters.Year.ToString() + numMonth + ".pdf";
                return ReturnPath;
            }
            catch (Exception e)
            {
                return "Error";
                throw;
            }
        }

        [HttpPost]
        [Route("PAGLM")]
        public string PAGLM(ParamPAGLM parameters)
        {
            try
            {
                ConnectToDatabase_DurustHRDB1();
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@Company", mytype = SqlDbType.VarChar, Value = parameters.TenantID });
                con.myparameters.Add(new myParameters { ParameterName = "@Year", mytype = SqlDbType.VarChar, Value = parameters.Year });
                con.myparameters.Add(new myParameters { ParameterName = "@Month", mytype = SqlDbType.VarChar, Value = parameters.Month });
                DataTable PAGLM = con.GetDataTable("sp_getInfoPAGLM");

                DataTable CompanyInfo = GetCompanyInfo(parameters.TenantID);

                //int countRows = GetPAGLMcount(parameters.Company, parameters.Month, parameters.Year).Rows.Count;
                int countRows = PAGLM.Rows.Count;
                int counter = 0;
                int decrement = 0;
                int fordecre = 532;

                //decimal list = decimal.Divide(GetPAGLMcount(parameters.Company, parameters.Month, parameters.Year).Rows.Count, 28);
                decimal list = decimal.Divide(countRows, 28);
                decimal numPage = Math.Ceiling(list);
                decimal total = 0;
                decimal totalperpage = 0;
                //string Monthword = "";
                string Monthword = CaseMonth(parameters.Month);
                string numMonth = parameters.Month;

                string pathsource;
                pathsource = "PAGLMCLEAN";
                //string reader1 = HttpContext.Current.Server.MapPath("~/pdf/") + pathsource + ".pdf";
                string reader1 = HttpContext.Current.Server.MapPath("~/Files/PayrollOutSourcing/PDFTemplate/") + pathsource + ".pdf";
                string pathmoto;
                pathmoto = "PAGLM";
                if (parameters.Month.Length == 1)
                {
                    numMonth = "0" + parameters.Month.ToString();
                }
                //string reader2 = HttpContext.Current.Server.MapPath("~/pdf/") + CompanyInfo.Rows[0][5].ToString().ToUpper() + "_" + pathmoto + "_" + parameters.Year.ToString() + numMonth + ".pdf";
                string reader2 = HttpContext.Current.Server.MapPath("~/sFTP/PayrollOutsourcing/") + CompanyInfo.Rows[0][5].ToString().ToUpper() + "_" + pathmoto + "_" + parameters.Year.ToString() + numMonth + ".pdf";
                string fileName = reader1;
                string outputFileName = reader2;
                //Step 1: Create a Docuement-Object
                Document document = new Document();
                try
                {
                    //The current file path
                    string filename = fileName;

                    // we create a reader for the document
                    PdfReader reader = new PdfReader(filename);

                    //PdfStamper stamper = new PdfStamper(reader, new FileStream(outputFileName, FileMode.Create));
                    //Step 2: we create a writer that listens to the document
                    PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(outputFileName, FileMode.Create));

                    //Step 3: Open the document
                    document.Open();

                    PdfContentByte cb = writer.DirectContent;

                    for (int i = 1; i <= numPage; i++)
                    {
                        for (int pageNumber = 1; pageNumber < reader.NumberOfPages + 1; pageNumber++)
                        {
                            document.SetPageSize(reader.GetPageSizeWithRotation(1));
                            document.NewPage();

                            //Insert to Destination on the first page
                            if (pageNumber == 1)
                            {
                                Chunk fileRef = new Chunk(" ");
                                fileRef.SetLocalDestination(filename);
                                document.Add(fileRef);
                            }

                            PdfImportedPage page = writer.GetImportedPage(reader, pageNumber);
                            int rotation = reader.GetPageRotation(pageNumber);
                            if (rotation == 90 || rotation == 270)
                            {
                                cb.AddTemplate(page, 0, -1f, 1f, 0, 0, reader.GetPageSizeWithRotation(pageNumber).Height);
                            }
                            else
                            {
                                cb.AddTemplate(page, 1f, 0, 0, 1f, 0, 0);
                            }

                            BaseFont bf = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                            cb.SetColorFill(BaseColor.DARK_GRAY);
                            cb.SetFontAndSize(bf, 10);

                            cb.BeginText();
                            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, CompanyInfo.Rows[0][0].ToString().ToUpper(), 40, 664, 0);
                            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, CompanyInfo.Rows[0][4].ToString().ToUpper(), 467, 715, 0);
                            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, Monthword.ToString() + " " + parameters.Year, 480, 630, 0);
                            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "", 480, 600, 0);
                            cb.EndText();

                            cb.SetColorFill(BaseColor.DARK_GRAY);
                            cb.SetFontAndSize(bf, 7);

                            int charString = CompanyInfo.Rows[0][1].ToString().Length;
                            if (charString <= 90)
                            {
                                cb.BeginText();
                                cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, CompanyInfo.Rows[0][1].ToString().ToUpper().Substring(0, charString), 40, 630, 0);
                                cb.EndText();
                            }
                            else
                            {
                                cb.BeginText();
                                cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, CompanyInfo.Rows[0][1].ToString().ToUpper().Substring(0, 90), 40, 630, 0);
                                cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, CompanyInfo.Rows[0][1].ToString().ToUpper().Substring(91, charString - 91), 40, 623, 0);
                                cb.EndText();
                            }

                            cb.SetColorFill(BaseColor.DARK_GRAY);
                            cb.SetFontAndSize(bf, 10);

                            cb.BeginText();
                            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "MILDRED GUERRA", 80, 74, 0);
                            cb.EndText();

                            cb.BeginText();
                            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "FINANCE DIRECTOR", 320, 74, 0);
                            cb.EndText();

                            fordecre = 532;
                            totalperpage = 0;
                            decrement = 0;

                            for (int j = 0; j < 28; j++)
                            {
                                if (j != 0)
                                {
                                    decrement = j * 13;
                                }

                                cb.SetColorFill(BaseColor.DARK_GRAY);
                                cb.SetFontAndSize(bf, 8);

                                cb.BeginText();
                                cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, PAGLM.Rows[counter][0].ToString().ToUpper(), 24, fordecre - decrement, 0);
                                cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, PAGLM.Rows[counter][1].ToString().ToUpper(), 88, fordecre - decrement, 0);
                                cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, (counter + 1).ToString() + ". " + PAGLM.Rows[counter][2].ToString().ToUpper(), 148, fordecre - decrement, 0);
                                cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, PAGLM.Rows[counter][3].ToString().ToUpper(), 415, fordecre - decrement, 0);
                                cb.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, PAGLM.Rows[counter][4].ToString().ToUpper(), 528, fordecre - decrement, 0);
                                cb.EndText();

                                totalperpage = totalperpage + Convert.ToDecimal(PAGLM.Rows[counter][4]);
                                total = total + Convert.ToDecimal(PAGLM.Rows[counter][4]);

                                counter = counter + 1;

                                if (counter == countRows)
                                {
                                    cb.SetColorFill(BaseColor.DARK_GRAY);
                                    cb.SetFontAndSize(bf, 8);

                                    cb.BeginText();
                                    cb.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, string.Format("{0:#,0.00}", totalperpage), 528, 162, 0);
                                    cb.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, string.Format("{0:#,0.00}", total), 528, 148, 0);
                                    cb.EndText();

                                    document.Close();
                                    break;
                                }
                            }
                            cb.SetColorFill(BaseColor.DARK_GRAY);
                            cb.SetFontAndSize(bf, 8);

                            cb.BeginText();
                            cb.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, string.Format("{0:#,0.00}", totalperpage), 528, 162, 0);
                            cb.EndText();

                            if (counter == countRows)
                            {
                                document.Close();
                                break;
                            }
                        }
                        if (counter == countRows)
                        {
                            document.Close();
                            break;
                        }
                    }
                }
                catch (Exception e)
                {
                    throw e;
                }
                finally
                {
                    document.Close();
                }
                string ReturnPath = "https://ws.durusthr.com/ILM_WS_Live/sFTP/PayrollOutsourcing/" + CompanyInfo.Rows[0][5].ToString().ToUpper() + "_" + pathmoto + "_" + parameters.Year.ToString() + numMonth + ".pdf"; ;
                return ReturnPath;
            }
            catch (Exception e)
            {
                return "Error";
            }
        }

        [HttpPost]
        [Route("GOVPH")]
        public void GOVPH()
        {
            decimal list = decimal.Divide(GetGOVPHcount("BSI").Rows.Count, 4);
            decimal numPage = Math.Ceiling(list);
            int counter = 0;
            int countRows = GetGOVPHcount("BSI").Rows.Count;

            string pathsource;
            pathsource = "GOVPHCLEAN";
            string reader1 = HttpContext.Current.Server.MapPath("~/Files/PayrollOutSourcing/PDFTemplate/") + pathsource + ".pdf";
            string pathmoto;
            pathmoto = "SamplePDFGOVPH";
            string reader2 = HttpContext.Current.Server.MapPath("~/sFTP/PayrollOutsourcing/") + pathmoto + ".pdf";
            string fileName = reader1;
            string outputFileName = reader2;

            //Step 1: Create a Docuement-Object
            Document document = new Document();
            try
            {
                //The current file path
                string filename = fileName;

                // we create a reader for the document
                PdfReader reader = new PdfReader(filename);

                //PdfStamper stamper = new PdfStamper(reader, new FileStream(outputFileName, FileMode.Create));
                //Step 2: we create a writer that listens to the document
                PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(outputFileName, FileMode.Create));

                //Step 3: Open the document
                document.Open();

                PdfContentByte cb = writer.DirectContent;

                for (int i = 0; i < numPage; i++)
                {
                    for (int pageNumber = 1; pageNumber < reader.NumberOfPages + 1; pageNumber++)
                    {
                        document.SetPageSize(reader.GetPageSizeWithRotation(1));
                        document.NewPage();

                        //Insert to Destination on the first page
                        if (pageNumber == 1)
                        {
                            Chunk fileRef = new Chunk(" ");
                            fileRef.SetLocalDestination(filename);
                            document.Add(fileRef);
                        }

                        PdfImportedPage page = writer.GetImportedPage(reader, pageNumber);
                        int rotation = reader.GetPageRotation(pageNumber);
                        if (rotation == 90 || rotation == 270)
                        {
                            cb.AddTemplate(page, 0, -1f, 1f, 0, 0, reader.GetPageSizeWithRotation(pageNumber).Height);
                        }
                        else
                        {
                            cb.AddTemplate(page, 1f, 0, 0, 1f, 0, 0);
                        }

                        BaseFont bf = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                        cb.SetColorFill(BaseColor.DARK_GRAY);
                        cb.SetFontAndSize(bf, 10);

                        for (int j = 0; j < 4; j++)
                        {
                            if (j == 0)
                            {
                                cb.SetColorFill(BaseColor.DARK_GRAY);
                                cb.SetFontAndSize(bf, 8);
                                if (GetGOVPHcount("BSI").Rows[counter][0].ToString() != "")
                                {
                                    cb.BeginText();
                                    cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, GetGOVPHcount("BSI").Rows[counter][0].ToString().ToUpper().Substring(0, 1), 112, 515, 0);
                                    cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, GetGOVPHcount("BSI").Rows[counter][0].ToString().ToUpper().Substring(1, 1), 131, 515, 0);
                                    cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, GetGOVPHcount("BSI").Rows[counter][0].ToString().ToUpper().Substring(2, 1), 150, 515, 0);
                                    cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, GetGOVPHcount("BSI").Rows[counter][0].ToString().ToUpper().Substring(3, 1), 169, 515, 0);
                                    cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, GetGOVPHcount("BSI").Rows[counter][0].ToString().ToUpper().Substring(4, 1), 188, 515, 0);
                                    cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, GetGOVPHcount("BSI").Rows[counter][0].ToString().ToUpper().Substring(5, 1), 207, 515, 0);
                                    cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, GetGOVPHcount("BSI").Rows[counter][0].ToString().ToUpper().Substring(6, 1), 226, 515, 0);
                                    cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, GetGOVPHcount("BSI").Rows[counter][0].ToString().ToUpper().Substring(7, 1), 245, 515, 0);
                                    cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, GetGOVPHcount("BSI").Rows[counter][0].ToString().ToUpper().Substring(8, 1), 264, 515, 0);
                                    cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, GetGOVPHcount("BSI").Rows[counter][0].ToString().ToUpper().Substring(9, 1), 283, 515, 0);
                                    cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, GetGOVPHcount("BSI").Rows[counter][0].ToString().ToUpper().Substring(10, 1), 302, 515, 0);
                                    cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, GetGOVPHcount("BSI").Rows[counter][0].ToString().ToUpper().Substring(11, 1), 321, 515, 0);
                                    cb.EndText();
                                }

                                cb.BeginText();
                                cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, GetGOVPHcount("BSI").Rows[counter][2].ToString().ToUpper(), 112, 488, 0);

                                cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, GetGOVPHcount("BSI").Rows[counter][1].ToString().ToUpper(), 140, 456, 0);

                                cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, GetGOVPHcount("BSI").Rows[counter][4].ToString().ToUpper(), 264, 360, 0);
                                cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, GetGOVPHcount("BSI").Rows[counter][5].ToString().ToUpper(), 264, 352, 0);

                                cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, GetGOVPHcount("BSI").Rows[counter][6].ToString().ToUpper(), 140, 320, 0);
                                cb.EndText();
                            }
                            else if (j == 1)
                            {
                                cb.SetColorFill(BaseColor.DARK_GRAY);
                                cb.SetFontAndSize(bf, 8);
                                if (GetGOVPHcount("BSI").Rows[counter][0].ToString() != "")
                                {
                                    cb.BeginText();
                                    cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, GetGOVPHcount("BSI").Rows[counter][0].ToString().ToUpper().Substring(0, 1), 517, 515, 0);
                                    cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, GetGOVPHcount("BSI").Rows[counter][0].ToString().ToUpper().Substring(1, 1), 536, 515, 0);
                                    cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, GetGOVPHcount("BSI").Rows[counter][0].ToString().ToUpper().Substring(2, 1), 555, 515, 0);
                                    cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, GetGOVPHcount("BSI").Rows[counter][0].ToString().ToUpper().Substring(3, 1), 574, 515, 0);
                                    cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, GetGOVPHcount("BSI").Rows[counter][0].ToString().ToUpper().Substring(4, 1), 593, 515, 0);
                                    cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, GetGOVPHcount("BSI").Rows[counter][0].ToString().ToUpper().Substring(5, 1), 612, 515, 0);
                                    cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, GetGOVPHcount("BSI").Rows[counter][0].ToString().ToUpper().Substring(6, 1), 631, 515, 0);
                                    cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, GetGOVPHcount("BSI").Rows[counter][0].ToString().ToUpper().Substring(7, 1), 650, 515, 0);
                                    cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, GetGOVPHcount("BSI").Rows[counter][0].ToString().ToUpper().Substring(8, 1), 669, 515, 0);
                                    cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, GetGOVPHcount("BSI").Rows[counter][0].ToString().ToUpper().Substring(9, 1), 688, 515, 0);
                                    cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, GetGOVPHcount("BSI").Rows[counter][0].ToString().ToUpper().Substring(10, 1), 707, 515, 0);
                                    cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, GetGOVPHcount("BSI").Rows[counter][0].ToString().ToUpper().Substring(11, 1), 726, 515, 0);
                                    cb.EndText();
                                }

                                cb.BeginText();
                                cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, GetGOVPHcount("BSI").Rows[counter][2].ToString().ToUpper(), 517, 488, 0);

                                cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, GetGOVPHcount("BSI").Rows[counter][1].ToString().ToUpper(), 545, 456, 0);

                                cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, GetGOVPHcount("BSI").Rows[counter][4].ToString().ToUpper(), 681, 360, 0);
                                cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, GetGOVPHcount("BSI").Rows[counter][5].ToString().ToUpper(), 681, 352, 0);

                                cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, GetGOVPHcount("BSI").Rows[counter][6].ToString().ToUpper(), 545, 320, 0);
                                cb.EndText();
                            }
                            else if (j == 2)
                            {
                                cb.SetColorFill(BaseColor.DARK_GRAY);
                                cb.SetFontAndSize(bf, 8);

                                if (GetGOVPHcount("BSI").Rows[counter][0].ToString() != "")
                                {
                                    cb.BeginText();
                                    cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, GetGOVPHcount("BSI").Rows[counter][0].ToString().ToUpper().Substring(0, 1), 112, 219, 0);
                                    cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, GetGOVPHcount("BSI").Rows[counter][0].ToString().ToUpper().Substring(1, 1), 131, 219, 0);
                                    cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, GetGOVPHcount("BSI").Rows[counter][0].ToString().ToUpper().Substring(2, 1), 150, 219, 0);
                                    cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, GetGOVPHcount("BSI").Rows[counter][0].ToString().ToUpper().Substring(3, 1), 169, 219, 0);
                                    cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, GetGOVPHcount("BSI").Rows[counter][0].ToString().ToUpper().Substring(4, 1), 188, 219, 0);
                                    cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, GetGOVPHcount("BSI").Rows[counter][0].ToString().ToUpper().Substring(5, 1), 207, 219, 0);
                                    cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, GetGOVPHcount("BSI").Rows[counter][0].ToString().ToUpper().Substring(6, 1), 226, 219, 0);
                                    cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, GetGOVPHcount("BSI").Rows[counter][0].ToString().ToUpper().Substring(7, 1), 245, 219, 0);
                                    cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, GetGOVPHcount("BSI").Rows[counter][0].ToString().ToUpper().Substring(8, 1), 264, 219, 0);
                                    cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, GetGOVPHcount("BSI").Rows[counter][0].ToString().ToUpper().Substring(9, 1), 283, 219, 0);
                                    cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, GetGOVPHcount("BSI").Rows[counter][0].ToString().ToUpper().Substring(10, 1), 302, 219, 0);
                                    cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, GetGOVPHcount("BSI").Rows[counter][0].ToString().ToUpper().Substring(11, 1), 321, 219, 0);
                                    cb.EndText();
                                }

                                cb.BeginText();
                                cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, GetGOVPHcount("BSI").Rows[counter][2].ToString().ToUpper(), 112, 193, 0);

                                cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, GetGOVPHcount("BSI").Rows[counter][1].ToString().ToUpper(), 140, 161, 0);

                                cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, GetGOVPHcount("BSI").Rows[counter][4].ToString().ToUpper(), 264, 67, 0);
                                cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, GetGOVPHcount("BSI").Rows[counter][5].ToString().ToUpper(), 264, 59, 0);

                                cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, GetGOVPHcount("BSI").Rows[counter][6].ToString().ToUpper(), 140, 27, 0);
                                cb.EndText();
                            }
                            else if (j == 3)
                            {
                                cb.SetColorFill(BaseColor.DARK_GRAY);
                                cb.SetFontAndSize(bf, 8);

                                if (GetGOVPHcount("BSI").Rows[counter][0].ToString() != "")
                                {
                                    cb.BeginText();
                                    cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, GetGOVPHcount("BSI").Rows[counter][0].ToString().ToUpper().Substring(0, 1), 517, 219, 0);
                                    cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, GetGOVPHcount("BSI").Rows[counter][0].ToString().ToUpper().Substring(1, 1), 536, 219, 0);
                                    cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, GetGOVPHcount("BSI").Rows[counter][0].ToString().ToUpper().Substring(2, 1), 555, 219, 0);
                                    cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, GetGOVPHcount("BSI").Rows[counter][0].ToString().ToUpper().Substring(3, 1), 574, 219, 0);
                                    cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, GetGOVPHcount("BSI").Rows[counter][0].ToString().ToUpper().Substring(4, 1), 593, 219, 0);
                                    cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, GetGOVPHcount("BSI").Rows[counter][0].ToString().ToUpper().Substring(5, 1), 612, 219, 0);
                                    cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, GetGOVPHcount("BSI").Rows[counter][0].ToString().ToUpper().Substring(6, 1), 631, 219, 0);
                                    cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, GetGOVPHcount("BSI").Rows[counter][0].ToString().ToUpper().Substring(7, 1), 650, 219, 0);
                                    cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, GetGOVPHcount("BSI").Rows[counter][0].ToString().ToUpper().Substring(8, 1), 669, 219, 0);
                                    cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, GetGOVPHcount("BSI").Rows[counter][0].ToString().ToUpper().Substring(9, 1), 688, 219, 0);
                                    cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, GetGOVPHcount("BSI").Rows[counter][0].ToString().ToUpper().Substring(10, 1), 707, 219, 0);
                                    cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, GetGOVPHcount("BSI").Rows[counter][0].ToString().ToUpper().Substring(11, 1), 726, 219, 0);
                                    cb.EndText();
                                }

                                cb.BeginText();
                                cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, GetGOVPHcount("BSI").Rows[counter][2].ToString().ToUpper(), 517, 193, 0);

                                cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, GetGOVPHcount("BSI").Rows[counter][1].ToString().ToUpper(), 545, 161, 0);

                                cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, GetGOVPHcount("BSI").Rows[counter][4].ToString().ToUpper(), 681, 67, 0);
                                cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, GetGOVPHcount("BSI").Rows[counter][5].ToString().ToUpper(), 681, 59, 0);

                                cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, GetGOVPHcount("BSI").Rows[counter][6].ToString().ToUpper(), 545, 27, 0);
                                cb.EndText();
                            }

                            counter = counter + 1;

                            if (counter == countRows)
                            {
                                document.Close();
                                break;
                            }
                        }
                        if (counter == countRows)
                        {
                            document.Close();
                            break;
                        }
                    }
                    if (counter == countRows)
                    {
                        document.Close();
                        break;
                    }

                }
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                document.Close();
            }
            //return outputFileName;
        }

        [HttpPost]
        [Route("SSSFIL")]
        public string SSSFIL(ParamSSSFIL parameters)
        {
            try
            {
                string Monthword = CaseMonth(parameters.Month);
                string numMonth = parameters.Month;

                ConnectToDatabase_DurustHRDB1();
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@Company", mytype = SqlDbType.VarChar, Value = parameters.TenantID });
                con.myparameters.Add(new myParameters { ParameterName = "@Year", mytype = SqlDbType.VarChar, Value = parameters.Year });
                con.myparameters.Add(new myParameters { ParameterName = "@Month", mytype = SqlDbType.VarChar, Value = parameters.Month });
                DataTable SSSFIL = con.GetDataTable("sp_getInfoSSSFIL");

                DataTable CompanyInfo = GetCompanyInfo(parameters.TenantID);

                string pathsource;
                pathsource = "SSSFILCLEAN";
                //string reader1 = HttpContext.Current.Server.MapPath("~/pdf/") + pathsource;
                string reader1 = HttpContext.Current.Server.MapPath("~/Files/PayrollOutSourcing/PDFTemplate/") + pathsource;
                string pathmoto;
                pathmoto = "SSSFIL";
                if (parameters.Month.Length == 1)
                {
                    numMonth = "0" + parameters.Month.ToString();
                }
                //string reader2 = HttpContext.Current.Server.MapPath("~/pdf/") + GetCompanyInfo(parameters.Company).Rows[0][5].ToString().ToUpper() + "_" + pathmoto + "_" + parameters.Year.ToString() + numMonth + ".pdf";
                string reader2 = HttpContext.Current.Server.MapPath("~/sFTP/PayrollOutsourcing/") + CompanyInfo.Rows[0][5].ToString().ToUpper() + "_" + pathmoto + "_" + parameters.Year.ToString() + numMonth + ".pdf";

                string formFile = reader1;
                // Output file path
                string newFile = reader2;
                // read the template file
                PdfReader reader = new PdfReader(formFile + ".pdf");
                // instantiate PDFStamper object
                // The Output file will be created from template file and edited by the PDFStamper
                PdfStamper stamper = new PdfStamper(reader, new FileStream(newFile, FileMode.Create));

                // Object to deal with the Output file's textfields
                AcroFields fields = stamper.AcroFields;

                fields.SetField("DisketteNumber", "");
                //fields.SetField("EmployerName", GetCompanyInfo(parameters.Company).Rows[0][0].ToString().ToUpper());
                //fields.SetField("IDNumber", GetCompanyInfo(parameters.Company).Rows[0][3].ToString().ToUpper());
                fields.SetField("EmployerName", CompanyInfo.Rows[0][0].ToString().ToUpper());
                fields.SetField("IDNumber", CompanyInfo.Rows[0][3].ToString().ToUpper());
                fields.SetField("Month", Monthword.ToString() + " " + parameters.Year);

                //if (parameters.Month == "1" || parameters.Month == "4" || parameters.Month == "7" || parameters.Month == "10")
                if (Monthword == "Jan" || parameters.Month == "Apr" || parameters.Month == "Jul" || parameters.Month == "Oct")
                {
                    //fields.SetField("FirstSSS", GetSSSFILcount(parameters.Company, parameters.Month, parameters.Year).Rows[0][1].ToString());
                    fields.SetField("FirstSSS", SSSFIL.Rows[0][1].ToString());
                    fields.SetField("SecondSSS", "0.00");
                    fields.SetField("ThirdSSS", "0.00");
                    //fields.SetField("SSSTotal", GetSSSFILcount(parameters.Company, parameters.Month, parameters.Year).Rows[0][1].ToString());
                    fields.SetField("SSSTotal", SSSFIL.Rows[0][1].ToString());
                    fields.SetField("FirstMedicare", "0.00");
                    fields.SetField("SecondMedicare", "0.00");
                    fields.SetField("ThirdMedicare", "0.00");
                    fields.SetField("MedicareTotal", "0.00");
                    //fields.SetField("FirstEC", GetSSSFILcount(parameters.Company, parameters.Month, parameters.Year).Rows[0][2].ToString());
                    fields.SetField("FirstEC", SSSFIL.Rows[0][2].ToString());
                    fields.SetField("SecondEC", "0.00");
                    fields.SetField("ThirdEC", "0.00");
                    //fields.SetField("ECTotal", GetSSSFILcount(parameters.Company, parameters.Month, parameters.Year).Rows[0][2].ToString());
                    //fields.SetField("FirstTotal", GetSSSFILcount(parameters.Company, parameters.Month, parameters.Year).Rows[0][3].ToString());
                    fields.SetField("ECTotal", SSSFIL.Rows[0][2].ToString());
                    fields.SetField("FirstTotal", SSSFIL.Rows[0][3].ToString());
                    fields.SetField("SecondTotal", "0.00");
                    fields.SetField("ThirdTotal", "0.00");
                    //fields.SetField("TotalTotal", GetSSSFILcount(parameters.Company, parameters.Month, parameters.Year).Rows[0][3].ToString());
                    fields.SetField("TotalTotal", SSSFIL.Rows[0][3].ToString());
                }
                //else if(parameters.Month == "2" || parameters.Month == "5" || parameters.Month == "8" || parameters.Month == "11")
                else if (parameters.Month == "Feb" || parameters.Month == "May" || parameters.Month == "Aug" || parameters.Month == "Nov")
                {
                    fields.SetField("FirstSSS", "0.00");
                    //fields.SetField("SecondSSS", GetSSSFILcount(parameters.Company, parameters.Month, parameters.Year).Rows[0][1].ToString());
                    fields.SetField("SecondSSS", SSSFIL.Rows[0][1].ToString());
                    fields.SetField("ThirdSSS", "0.00");
                    //fields.SetField("SSSTotal", GetSSSFILcount(parameters.Company, parameters.Month, parameters.Year).Rows[0][1].ToString());
                    fields.SetField("SSSTotal", SSSFIL.Rows[0][1].ToString());
                    fields.SetField("FirstMedicare", "0.00");
                    fields.SetField("SecondMedicare", "0.00");
                    fields.SetField("ThirdMedicare", "0.00");
                    fields.SetField("MedicareTotal", "0.00");
                    fields.SetField("FirstEC", "0.00");
                    //fields.SetField("SecondEC", GetSSSFILcount(parameters.Company, parameters.Month, parameters.Year).Rows[0][2].ToString());
                    fields.SetField("SecondEC", SSSFIL.Rows[0][2].ToString());
                    fields.SetField("ThirdEC", "0.00");
                    //fields.SetField("ECTotal", GetSSSFILcount(parameters.Company, parameters.Month, parameters.Year).Rows[0][2].ToString());
                    fields.SetField("ECTotal", SSSFIL.Rows[0][2].ToString());
                    fields.SetField("FirstTotal", "0.00");
                    //fields.SetField("SecondTotal", GetSSSFILcount(parameters.Company, parameters.Month, parameters.Year).Rows[0][3].ToString());
                    fields.SetField("SecondTotal", SSSFIL.Rows[0][3].ToString());
                    fields.SetField("ThirdTotal", "0.00");
                    //fields.SetField("TotalTotal", GetSSSFILcount(parameters.Company, parameters.Month, parameters.Year).Rows[0][3].ToString());
                    fields.SetField("TotalTotal", SSSFIL.Rows[0][3].ToString());
                }
                //else if (parameters.Month == "3" || parameters.Month == "6" || parameters.Month == "9" || parameters.Month == "12")
                else if (parameters.Month == "Mar" || parameters.Month == "Jun" || parameters.Month == "Sep" || parameters.Month == "Dec")
                {
                    fields.SetField("FirstSSS", "0.00");
                    fields.SetField("SecondSSS", "0.00");
                    //fields.SetField("ThirdSSS", GetSSSFILcount(parameters.Company, parameters.Month, parameters.Year).Rows[0][1].ToString());
                    //fields.SetField("SSSTotal", GetSSSFILcount(parameters.Company, parameters.Month, parameters.Year).Rows[0][1].ToString());
                    fields.SetField("ThirdSSS", SSSFIL.Rows[0][1].ToString());
                    fields.SetField("SSSTotal", SSSFIL.Rows[0][1].ToString());
                    fields.SetField("FirstMedicare", "0.00");
                    fields.SetField("SecondMedicare", "0.00");
                    fields.SetField("ThirdMedicare", "0.00");
                    fields.SetField("MedicareTotal", "0.00");
                    fields.SetField("FirstEC", "0.00");
                    fields.SetField("SecondEC", "0.00");
                    //fields.SetField("ThirdEC", GetSSSFILcount(parameters.Company, parameters.Month, parameters.Year).Rows[0][2].ToString());
                    //fields.SetField("ECTotal", GetSSSFILcount(parameters.Company, parameters.Month, parameters.Year).Rows[0][2].ToString());
                    fields.SetField("ThirdEC", SSSFIL.Rows[0][2].ToString());
                    fields.SetField("ECTotal", SSSFIL.Rows[0][2].ToString());
                    fields.SetField("FirstTotal", "0.00");
                    fields.SetField("SecondTotal", "0.00");
                    //fields.SetField("ThirdTotal", GetSSSFILcount(parameters.Company, parameters.Month, parameters.Year).Rows[0][3].ToString());
                    //fields.SetField("TotalTotal", GetSSSFILcount(parameters.Company, parameters.Month, parameters.Year).Rows[0][3].ToString());
                    fields.SetField("ThirdTotal", SSSFIL.Rows[0][3].ToString());
                    fields.SetField("TotalTotal", SSSFIL.Rows[0][3].ToString());
                }

                //fields.SetField("Diskette", GetSSSFILcount(parameters.Company, parameters.Month, parameters.Year).Rows[0][0].ToString());
                fields.SetField("Diskette", SSSFIL.Rows[0][0].ToString());
                fields.SetField("Name", "MILDRED GUERRA");

                // "Flatten" the form so it wont be editable/usable anymore
                stamper.FormFlattening = true;
                // closing the stamper
                stamper.Close();
                string ReturnPath = "https://ws.durusthr.com/ILM_WS_Live/sFTP/PayrollOutsourcing/" + CompanyInfo.Rows[0][5].ToString().ToUpper() + "_" + pathmoto + "_" + parameters.Year.ToString() + numMonth + ".pdf"; ;
                return ReturnPath;
            }
            catch (Exception e)
            {
                return "Error";
            }
        }

        [HttpPost]
        [Route("QTRS")]
        public string QTRS(ParamQTRS parameters)
        {
            try
            {
                ConnectToDatabase_DurustHRDB1();
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@Company", mytype = SqlDbType.VarChar, Value = parameters.TenantID });
                con.myparameters.Add(new myParameters { ParameterName = "@Year", mytype = SqlDbType.VarChar, Value = parameters.Year });
                con.myparameters.Add(new myParameters { ParameterName = "@Month", mytype = SqlDbType.VarChar, Value = parameters.Month });
                DataTable QTRS = con.GetDataTable("sp_getInfoQTRS");

                DataTable CompanyInfo = GetCompanyInfo(parameters.TenantID);

                int countRows = QTRS.Rows.Count;
                int counter = 0;
                int decrement = 0;
                int fordecre = 550;
                //decimal list = decimal.Divide(GetQTRScount(parameters.Company, parameters.Month, parameters.Year).Rows.Count, 37);
                decimal list = decimal.Divide(countRows, 37);
                decimal numPage = Math.Ceiling(list);
                decimal total = 0;
                decimal totalperpage = 0;
                string Monthword = CaseMonth(parameters.Month);
                string numMonth = parameters.Month;

                string pathsource;
                pathsource = "QTRSSTARTCLEAN2";
                //string reader1 = HttpContext.Current.Server.MapPath("~/pdf/") + pathsource + ".pdf";
                string reader1 = HttpContext.Current.Server.MapPath("~/Files/PayrollOutSourcing/PDFTemplate/") + pathsource + ".pdf";
                string pathsourceend;
                pathsourceend = "QTRSENDCLEAN2";
                //string reader3 = HttpContext.Current.Server.MapPath("~/pdf/") + pathsourceend + ".pdf";
                string reader3 = HttpContext.Current.Server.MapPath("~/Files/PayrollOutSourcing/PDFTemplate/") + pathsourceend + ".pdf";
                string pathmoto;
                pathmoto = "QTRS";
                if (parameters.Month.Length == 1)
                {
                    numMonth = "0" + parameters.Month.ToString();
                }
                //string reader2 = HttpContext.Current.Server.MapPath("~/pdf/") + GetCompanyInfo(parameters.Company).Rows[0][5].ToString().ToUpper() + "_" + pathmoto + "_" + parameters.Year.ToString() + numMonth + ".pdf";
                string reader2 = HttpContext.Current.Server.MapPath("~/sFTP/PayrollOutsourcing/") + CompanyInfo.Rows[0][5].ToString().ToUpper() + "_" + pathmoto + "_" + parameters.Year.ToString() + numMonth + ".pdf";
                string fileName = reader1;
                string fileName2 = reader3;
                string outputFileName = reader2;
                //Step 1: Create a Docuement-Object
                Document document = new Document();
                try
                {
                    //The current file path
                    string filename = fileName;

                    // we create a reader for the document
                    PdfReader reader = new PdfReader(filename);

                    //PdfStamper stamper = new PdfStamper(reader, new FileStream(outputFileName, FileMode.Create));
                    //Step 2: we create a writer that listens to the document
                    PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(outputFileName, FileMode.Create));

                    //Step 3: Open the document
                    document.Open();

                    PdfContentByte cb = writer.DirectContent;

                    for (int i = 1; i <= numPage; i++)
                    {
                        for (int pageNumber = 1; pageNumber < reader.NumberOfPages + 1; pageNumber++)
                        {
                            document.SetPageSize(reader.GetPageSizeWithRotation(1));
                            document.NewPage();

                            //Insert to Destination on the first page
                            if (pageNumber == 1)
                            {
                                Chunk fileRef = new Chunk(" ");
                                fileRef.SetLocalDestination(filename);
                                document.Add(fileRef);
                            }

                            PdfImportedPage page = writer.GetImportedPage(reader, pageNumber);
                            int rotation = reader.GetPageRotation(pageNumber);
                            if (rotation == 90 || rotation == 270)
                            {
                                cb.AddTemplate(page, 0, -1f, 1f, 0, 0, reader.GetPageSizeWithRotation(pageNumber).Height);
                            }
                            else
                            {
                                cb.AddTemplate(page, 1f, 0, 0, 1f, 0, 0);
                            }

                            BaseFont bf = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                            cb.SetColorFill(BaseColor.DARK_GRAY);
                            cb.SetFontAndSize(bf, 9);

                            cb.BeginText();
                            //cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, GetCompanyInfo(parameters.Company).Rows[0][0].ToString().ToUpper(), 130, 625, 0);
                            //cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, GetCompanyInfo(parameters.Company).Rows[0][3].ToString().ToUpper(), 130, 613, 0);
                            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, CompanyInfo.Rows[0][0].ToString().ToUpper(), 130, 625, 0);
                            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, CompanyInfo.Rows[0][3].ToString().ToUpper(), 130, 613, 0);
                            cb.EndText();

                            var lastDayOfMonth = DateTime.DaysInMonth(Convert.ToInt32(parameters.Year), Convert.ToInt32(parameters.Month));

                            cb.SetColorFill(BaseColor.BLACK);
                            cb.SetFontAndSize(bf, 9);

                            cb.BeginText();
                            cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "As of " + Monthword.ToString() + " " + lastDayOfMonth.ToString() + ", " + parameters.Year, 305, 668, 0);
                            cb.EndText();

                            cb.SetColorFill(BaseColor.BLACK);
                            cb.SetFontAndSize(bf, 8);

                            cb.BeginText();
                            cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "Page " + i + " of " + (numPage + 1) + " Pages", 545, 625, 0);
                            cb.EndText();
                            fordecre = 550;
                            totalperpage = 0;
                            decrement = 0;

                            for (int j = 0; j < 37; j++)
                            {

                                if (j == 8 || j == 16 || j == 24 || j == 32)
                                {
                                    fordecre = fordecre + 3;
                                }

                                if (j != 0)
                                {
                                    decrement = j * 13;
                                }

                                cb.SetColorFill(BaseColor.DARK_GRAY);
                                cb.SetFontAndSize(bf, 7);

                                cb.BeginText();
                                //cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, GetQTRScount(parameters.Company, parameters.Month, parameters.Year).Rows[counter][0].ToString().ToUpper(), 29, fordecre - decrement, 0);
                                //cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, GetQTRScount(parameters.Company, parameters.Month, parameters.Year).Rows[counter][1].ToString().ToUpper(), 87, fordecre - decrement, 0);
                                //cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, GetQTRScount(parameters.Company, parameters.Month, parameters.Year).Rows[counter][2].ToString().ToUpper(), 233, fordecre - decrement, 0);
                                //cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, GetQTRScount(parameters.Company, parameters.Month, parameters.Year).Rows[counter][3].ToString().ToUpper(), 257, fordecre - decrement, 0);
                                //cb.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, GetQTRScount(parameters.Company, parameters.Month, parameters.Year).Rows[counter][4].ToString().ToUpper(), 354, fordecre - decrement, 0);
                                //cb.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, GetQTRScount(parameters.Company, parameters.Month, parameters.Year).Rows[counter][5].ToString().ToUpper(), 410, fordecre - decrement, 0);
                                cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, QTRS.Rows[counter][0].ToString().ToUpper(), 29, fordecre - decrement, 0);
                                cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, QTRS.Rows[counter][1].ToString().ToUpper(), 87, fordecre - decrement, 0);
                                cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, QTRS.Rows[counter][2].ToString().ToUpper(), 233, fordecre - decrement, 0);
                                cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, QTRS.Rows[counter][3].ToString().ToUpper(), 257, fordecre - decrement, 0);
                                cb.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, QTRS.Rows[counter][4].ToString().ToUpper(), 354, fordecre - decrement, 0);
                                cb.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, QTRS.Rows[counter][5].ToString().ToUpper(), 410, fordecre - decrement, 0);
                                cb.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, "0.00", 459, fordecre - decrement, 0);
                                //cb.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, GetQTRScount(parameters.Company, parameters.Month, parameters.Year).Rows[counter][5].ToString().ToUpper(), 515, fordecre - decrement, 0);
                                cb.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, QTRS.Rows[counter][5].ToString().ToUpper(), 515, fordecre - decrement, 0);
                                cb.EndText();

                                //totalperpage = totalperpage + Convert.ToDecimal(GetQTRScount(parameters.Company, parameters.Month, parameters.Year).Rows[counter][5]);
                                //total = total + Convert.ToDecimal(GetQTRScount(parameters.Company, parameters.Month, parameters.Year).Rows[counter][5]);
                                totalperpage = totalperpage + Convert.ToDecimal(QTRS.Rows[counter][5]);
                                total = total + Convert.ToDecimal(QTRS.Rows[counter][5]);

                                counter = counter + 1;

                                if (counter == countRows)
                                {
                                    cb.SetColorFill(BaseColor.BLACK);
                                    cb.SetFontAndSize(bf, 8);

                                    cb.BeginText();
                                    cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "PAGE TOTAL", 233, 82, 0);
                                    cb.EndText();

                                    cb.SetColorFill(BaseColor.DARK_GRAY);
                                    cb.SetFontAndSize(bf, 7);

                                    cb.BeginText();
                                    cb.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, string.Format("{0:#,0.00}", totalperpage), 410, 82, 0);
                                    cb.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, "0.00", 459, 82, 0);
                                    cb.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, string.Format("{0:#,0.00}", totalperpage), 515, 82, 0);
                                    cb.EndText();

                                    break;
                                }
                            }
                            cb.SetColorFill(BaseColor.BLACK);
                            cb.SetFontAndSize(bf, 8);

                            cb.BeginText();
                            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "PAGE TOTAL", 233, 82, 0);
                            cb.EndText();

                            cb.SetColorFill(BaseColor.DARK_GRAY);
                            cb.SetFontAndSize(bf, 7);

                            cb.BeginText();
                            cb.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, string.Format("{0:#,0.00}", totalperpage), 410, 82, 0);
                            cb.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, "0.00", 459, 82, 0);
                            cb.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, string.Format("{0:#,0.00}", totalperpage), 515, 82, 0);
                            cb.EndText();

                            if (counter == countRows)
                            {
                                //document.Close();
                                break;
                            }
                        }
                        if (counter == countRows)
                        {
                            //document.Close();
                            break;
                        }
                    }
                    string filename1 = fileName2;

                    PdfReader readerr = new PdfReader(filename1);

                    //PdfWriter writerr = PdfWriter.GetInstance(document, new FileStream(outputFileName, FileMode.OpenOrCreate));

                    //document.Open();

                    for (int pageNumber = 1; pageNumber < readerr.NumberOfPages + 1; pageNumber++)
                    {
                        document.SetPageSize(readerr.GetPageSizeWithRotation(1));
                        document.NewPage();

                        //Insert to Destination on the first page
                        if (pageNumber == 1)
                        {
                            Chunk fileRef = new Chunk(" ");
                            fileRef.SetLocalDestination(filename1);
                            document.Add(fileRef);
                        }

                        PdfImportedPage page = writer.GetImportedPage(readerr, pageNumber);
                        int rotation = readerr.GetPageRotation(pageNumber);
                        if (rotation == 90 || rotation == 270)
                        {
                            cb.AddTemplate(page, 0, -1f, 1f, 0, 0, readerr.GetPageSizeWithRotation(pageNumber).Height);
                        }
                        else
                        {
                            cb.AddTemplate(page, 1f, 0, 0, 1f, 0, 0);
                        }

                        BaseFont bf = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                        cb.SetColorFill(BaseColor.DARK_GRAY);
                        cb.SetFontAndSize(bf, 9);

                        cb.BeginText();
                        //cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, GetCompanyInfo(parameters.Company).Rows[0][0].ToString().ToUpper(), 130, 630, 0);
                        //cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, GetCompanyInfo(parameters.Company).Rows[0][3].ToString().ToUpper(), 130, 618, 0);
                        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, CompanyInfo.Rows[0][0].ToString().ToUpper(), 130, 630, 0);
                        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, CompanyInfo.Rows[0][3].ToString().ToUpper(), 130, 618, 0);
                        cb.EndText();

                        var lastDayOfMonth = DateTime.DaysInMonth(Convert.ToInt32(parameters.Year), Convert.ToInt32(parameters.Month));

                        cb.SetColorFill(BaseColor.BLACK);
                        cb.SetFontAndSize(bf, 9);

                        cb.BeginText();
                        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "As of " + Monthword.ToString() + " " + lastDayOfMonth.ToString() + ", " + parameters.Year, 305, 668, 0);
                        cb.EndText();

                        cb.SetColorFill(BaseColor.BLACK);
                        cb.SetFontAndSize(bf, 8);

                        cb.BeginText();
                        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "Page " + (numPage + 1) + " of " + (numPage + 1) + " Pages", 545, 630, 0);
                        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "TOTAL AMOUNT DUE", 233, 575, 0);
                        cb.EndText();

                        cb.SetColorFill(BaseColor.DARK_GRAY);
                        cb.SetFontAndSize(bf, 7);

                        cb.BeginText();
                        cb.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, string.Format("{0:#,0.00}", total), 410, 575, 0);
                        cb.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, "0.00", 459, 575, 0);
                        cb.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, string.Format("{0:#,0.00}", total), 515, 575, 0);
                        cb.EndText();

                        document.Close();
                        break;

                    }
                }
                catch (Exception e)
                {
                    throw e;
                }
                finally
                {
                    document.Close();
                }
                string ReturnPath = "https://ws.durusthr.com/ILM_WS_Live/sFTP/PayrollOutsourcing/" + CompanyInfo.Rows[0][5].ToString().ToUpper() + "_" + pathmoto + "_" + parameters.Year.ToString() + numMonth + ".pdf";
                return ReturnPath;
            }
            catch (Exception e)
            {
                return "Error";
            }
        }

        [HttpPost]
        [Route("SSSR1A")]
        public string SSSR1A(ParamSSSR1A parameters)
        {
            try
            {
                ConnectToDatabase_DurustHRDB1();
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@Company", mytype = SqlDbType.VarChar, Value = parameters.TenantID });
                con.myparameters.Add(new myParameters { ParameterName = "@Year", mytype = SqlDbType.VarChar, Value = parameters.Year });
                con.myparameters.Add(new myParameters { ParameterName = "@Month", mytype = SqlDbType.VarChar, Value = parameters.Month });
                DataTable SSSR1 = con.GetDataTable("sp_getInfoSSSR1A");

                DataTable CompanyInfo = GetCompanyInfo(parameters.TenantID);

                //int countRows = GetSSSR1Acount(parameters.Company, parameters.Month, parameters.Year).Rows.Count;
                int countRows = SSSR1.Rows.Count;
                int counter = 0;
                int decrement = 0;
                int fordecre = 390;
                //int total = GetSSSR1Acount(parameters.Company, parameters.Month, parameters.Year).Rows.Count;
                int total = SSSR1.Rows.Count;
                int totalperpage = 0;
                //decimal list = decimal.Divide(GetSSSR1Acount(parameters.Company, parameters.Month, parameters.Year).Rows.Count, 15);
                decimal list = decimal.Divide(countRows, 15);
                decimal numPage = Math.Ceiling(list);
                string numMonth = "";

                string pathsource;
                pathsource = "SSSR1ACLEAN";
                //string reader1 = HttpContext.Current.Server.MapPath("~/pdf/") + pathsource + ".pdf";
                string reader1 = HttpContext.Current.Server.MapPath("~/Files/PayrollOutSourcing/PDFTemplate/") + pathsource + ".pdf";
                string pathmoto;
                pathmoto = "SSSR1A";
                if (parameters.Month.Length == 1)
                {
                    numMonth = "0" + parameters.Month.ToString();
                }
                //string reader2 = HttpContext.Current.Server.MapPath("~/pdf/") + GetCompanyInfo(parameters.Company).Rows[0][5].ToString().ToUpper() + "_" + pathmoto + "_" + parameters.Year.ToString() + numMonth + ".pdf";
                string reader2 = HttpContext.Current.Server.MapPath("~/sFTP/PayrollOutsourcing/") + CompanyInfo.Rows[0][5].ToString().ToUpper() + "_" + pathmoto + "_" + parameters.Year.ToString() + numMonth + ".pdf";
                string fileName = reader1;
                string outputFileName = reader2;
                //Step 1: Create a Docuement-Object
                Document document = new Document();
                try
                {
                    //The current file path
                    string filename = fileName;

                    // we create a reader for the document
                    PdfReader reader = new PdfReader(filename);

                    //PdfStamper stamper = new PdfStamper(reader, new FileStream(outputFileName, FileMode.Create));
                    //Step 2: we create a writer that listens to the document
                    PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(outputFileName, FileMode.Create));

                    //Step 3: Open the document
                    document.Open();

                    PdfContentByte cb = writer.DirectContent;

                    for (int i = 1; i <= numPage; i++)
                    {
                        for (int pageNumber = 1; pageNumber < reader.NumberOfPages + 1; pageNumber++)
                        {
                            document.SetPageSize(reader.GetPageSizeWithRotation(1));
                            document.NewPage();

                            //Insert to Destination on the first page
                            if (pageNumber == 1)
                            {
                                Chunk fileRef = new Chunk(" ");
                                fileRef.SetLocalDestination(filename);
                                document.Add(fileRef);
                            }

                            PdfImportedPage page = writer.GetImportedPage(reader, pageNumber);
                            int rotation = reader.GetPageRotation(pageNumber);
                            if (rotation == 90 || rotation == 270)
                            {
                                cb.AddTemplate(page, 0, -1f, 1f, 0, 0, reader.GetPageSizeWithRotation(pageNumber).Height);
                            }
                            else
                            {
                                cb.AddTemplate(page, 1f, 0, 0, 1f, 0, 0);
                            }

                            BaseFont bf = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                            cb.SetColorFill(BaseColor.DARK_GRAY);
                            cb.SetFontAndSize(bf, 10);

                            cb.BeginText();
                            //cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, GetCompanyInfo(parameters.Company).Rows[0][3].ToString().ToUpper(), 30, 480, 0);
                            //cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, GetCompanyInfo(parameters.Company).Rows[0][0].ToString().ToUpper(), 178, 480, 0);
                            //cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, GetCompanyInfo(parameters.Company).Rows[0][6].ToString().ToUpper(), 792, 427, 0);
                            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, CompanyInfo.Rows[0][3].ToString().ToUpper(), 30, 480, 0);
                            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, CompanyInfo.Rows[0][0].ToString().ToUpper(), 178, 480, 0);
                            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, CompanyInfo.Rows[0][6].ToString().ToUpper(), 792, 427, 0);
                            cb.EndText();

                            cb.SetColorFill(BaseColor.DARK_GRAY);
                            cb.SetFontAndSize(bf, 8);

                            cb.BeginText();
                            //cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, GetCompanyInfo(parameters.Company).Rows[0][1].ToString().ToUpper(), 30, 462, 0);
                            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, CompanyInfo.Rows[0][1].ToString().ToUpper(), 30, 462, 0);
                            cb.EndText();

                            if (total > 15)
                            {
                                cb.SetColorFill(BaseColor.DARK_GRAY);
                                cb.SetFontAndSize(bf, 10);

                                totalperpage = 15;
                                total = total - totalperpage;
                                cb.BeginText();
                                cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, totalperpage.ToString(), 70, 136, 0);
                                cb.EndText();
                            }
                            else
                            {
                                cb.SetColorFill(BaseColor.DARK_GRAY);
                                cb.SetFontAndSize(bf, 10);

                                cb.BeginText();
                                cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, total.ToString(), 70, 136, 0);
                                cb.EndText();
                            }

                            cb.SetColorFill(BaseColor.DARK_GRAY);
                            cb.SetFontAndSize(bf, 8);

                            cb.BeginText();
                            cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "MILDRED GUERRA", 325, 134, 0);
                            cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "FINANCE DIRECTOR", 496, 134, 0);
                            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, i.ToString(), 626, 138, 0);
                            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, numPage.ToString(), 647, 138, 0);
                            cb.EndText();

                            fordecre = 390;

                            for (int j = 0; j < 15; j++)
                            {
                                decrement = j * 16;

                                cb.SetColorFill(BaseColor.DARK_GRAY);
                                cb.SetFontAndSize(bf, 10);

                                cb.BeginText();
                                //cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, GetSSSR1Acount(parameters.Company, parameters.Month, parameters.Year).Rows[counter][0].ToString().ToUpper(), 50, fordecre - decrement, 0);
                                //cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, (counter + 1).ToString() + ". " + GetSSSR1Acount(parameters.Company, parameters.Month, parameters.Year).Rows[counter][1].ToString().ToUpper(), 145, fordecre - decrement, 0);
                                //cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, GetSSSR1Acount(parameters.Company, parameters.Month, parameters.Year).Rows[counter][2].ToString().ToUpper(), 420, fordecre - decrement, 0);
                                //cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, GetSSSR1Acount(parameters.Company, parameters.Month, parameters.Year).Rows[counter][3].ToString().ToUpper(), 515, fordecre - decrement, 0);
                                //cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, GetSSSR1Acount(parameters.Company, parameters.Month, parameters.Year).Rows[counter][4].ToString().ToUpper(), 609, fordecre - decrement, 0);
                                //cb.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, GetSSSR1Acount(parameters.Company, parameters.Month, parameters.Year).Rows[counter][5].ToString().ToUpper(), 735, fordecre - decrement, 0);
                                cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, SSSR1.Rows[counter][0].ToString().ToUpper(), 50, fordecre - decrement, 0);
                                cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, (counter + 1).ToString() + ". " + SSSR1.Rows[counter][1].ToString().ToUpper(), 145, fordecre - decrement, 0);
                                cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, SSSR1.Rows[counter][2].ToString().ToUpper(), 420, fordecre - decrement, 0);
                                cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, SSSR1.Rows[counter][3].ToString().ToUpper(), 515, fordecre - decrement, 0);
                                cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, SSSR1.Rows[counter][4].ToString().ToUpper(), 609, fordecre - decrement, 0);
                                cb.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, SSSR1.Rows[counter][5].ToString().ToUpper(), 735, fordecre - decrement, 0);
                                cb.EndText();

                                cb.SetColorFill(BaseColor.DARK_GRAY);
                                cb.SetFontAndSize(bf, 7);

                                cb.BeginText();
                                //cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, GetSSSR1Acount(parameters.Company, parameters.Month, parameters.Year).Rows[counter][6].ToString().ToUpper(), 752, fordecre - decrement, 0);
                                cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, SSSR1.Rows[counter][6].ToString().ToUpper(), 752, fordecre - decrement, 0);
                                cb.EndText();

                                counter = counter + 1;

                                if (counter == countRows)
                                {
                                    document.Close();
                                    break;
                                }
                            }

                            if (counter == countRows)
                            {
                                document.Close();
                                break;
                            }
                        }
                        if (counter == countRows)
                        {
                            document.Close();
                            break;
                        }
                    }
                }
                catch (Exception e)
                {
                    throw e;
                }
                finally
                {
                    document.Close();
                }

                string ReturnPath = "https://ws.durusthr.com/ILM_WS_Live/sFTP/PayrollOutsourcing/" + CompanyInfo.Rows[0][5].ToString().ToUpper() + "_" + pathmoto + "_" + parameters.Year.ToString() + numMonth + ".pdf";
                return ReturnPath;
            }
            catch (Exception e)
            {
                return "Error";
            }
        }

        public DataTable GetPHER2count(string Company, string Month, string Year)
        {
            ConnectToDatabase_DurustHRDB1();
            Connection con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@Company", mytype = SqlDbType.VarChar, Value = Company });
            con.myparameters.Add(new myParameters { ParameterName = "@Year", mytype = SqlDbType.VarChar, Value = Year });
            con.myparameters.Add(new myParameters { ParameterName = "@Month", mytype = SqlDbType.VarChar, Value = Month });
            DataTable dt = con.GetDataTable("sp_getInfoPHEr2");
            return dt;
        }
        public DataTable GetCompanyInfo(string Company)
        {
            ConnectToDatabase_DurustHRDB1();
            Connection con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@Company", mytype = SqlDbType.VarChar, Value = Company });
            DataTable dt = con.GetDataTable("sp_getCompanyInfo");
            return dt;
        }

        public DataTable GetPAGLMcount(string Company, string Month, string Year)
        {
            ConnectToDatabase_DurustHRDB1();
            Connection con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@Company", mytype = SqlDbType.VarChar, Value = Company });
            con.myparameters.Add(new myParameters { ParameterName = "@Year", mytype = SqlDbType.VarChar, Value = Year });
            con.myparameters.Add(new myParameters { ParameterName = "@Month", mytype = SqlDbType.VarChar, Value = Month });
            DataTable dt = con.GetDataTable("sp_getInfoPAGLM");
            return dt;
        }

        public DataTable GetGOVPHcount(string Company)
        {
            ConnectToDatabase_DurustHRDB1();
            Connection con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@Company", mytype = SqlDbType.VarChar, Value = Company });
            DataTable dt = con.GetDataTable("sp_getInfoGOVPH");
            return dt;
        }
        public DataTable GetSSSFILcount(string Company, string Month, string Year)
        {
            ConnectToDatabase_DurustHRDB1();
            Connection con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@Company", mytype = SqlDbType.VarChar, Value = Company });
            con.myparameters.Add(new myParameters { ParameterName = "@Year", mytype = SqlDbType.VarChar, Value = Year });
            con.myparameters.Add(new myParameters { ParameterName = "@Month", mytype = SqlDbType.VarChar, Value = Month });
            DataTable dt = con.GetDataTable("sp_getInfoSSSFIL");
            return dt;
        }
        public DataTable GetQTRScount(string Company, string Month, string Year)
        {
            ConnectToDatabase_DurustHRDB1();
            Connection con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@Company", mytype = SqlDbType.VarChar, Value = Company });
            con.myparameters.Add(new myParameters { ParameterName = "@Year", mytype = SqlDbType.VarChar, Value = Year });
            con.myparameters.Add(new myParameters { ParameterName = "@Month", mytype = SqlDbType.VarChar, Value = Month });
            DataTable dt = con.GetDataTable("sp_getInfoQTRS");
            return dt;
        }

        public DataTable GetSSSR1Acount(string Company, string Month, string Year)
        {
            ConnectToDatabase_DurustHRDB1();
            Connection con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@Company", mytype = SqlDbType.VarChar, Value = Company });
            con.myparameters.Add(new myParameters { ParameterName = "@Year", mytype = SqlDbType.VarChar, Value = Year });
            con.myparameters.Add(new myParameters { ParameterName = "@Month", mytype = SqlDbType.VarChar, Value = Month });
            DataTable dt = con.GetDataTable("sp_getInfoSSSR1A");
            return dt;
        }

        private string CaseMonth(string IntMonth)
        {
            string MonthName = "";
            switch (IntMonth)
            {
                case "1":
                    MonthName = "Jan";
                    break;
                case "01":
                    MonthName = "Jan";
                    break;
                case "2":
                    MonthName = "Feb";
                    break;
                case "02":
                    MonthName = "Feb";
                    break;
                case "3":
                    MonthName = "Mar";
                    break;
                case "03":
                    MonthName = "Mar";
                    break;
                case "4":
                    MonthName = "Apr";
                    break;
                case "04":
                    MonthName = "Apr";
                    break;
                case "5":
                    MonthName = "May";
                    break;
                case "05":
                    MonthName = "May";
                    break;
                case "6":
                    MonthName = "Jun";
                    break;
                case "06":
                    MonthName = "Jun";
                    break;
                case "7":
                    MonthName = "Jul";
                    break;
                case "07":
                    MonthName = "Jul";
                    break;
                case "8":
                    MonthName = "Aug";
                    break;
                case "08":
                    MonthName = "Aug";
                    break;
                case "9":
                    MonthName = "Sep";
                    break;
                case "09":
                    MonthName = "Sep";
                    break;
                case "10":
                    MonthName = "Oct";
                    break;
                case "11":
                    MonthName = "Nov";
                    break;
                case "12":
                    MonthName = "Dec";
                    break;
            }
            return MonthName;
        }

        #region Connect To Database
        public void ConnectToDatabase_DurustHRDB1()
        {
            var settings = ConfigurationManager.ConnectionStrings["connectionstring"];
            var fi = typeof(ConfigurationElement).GetField(
                          "_bReadOnly",
                          BindingFlags.Instance | BindingFlags.NonPublic);
            fi.SetValue(settings, false);

            settings.ConnectionString = "Data Source=livedb-ilm.c3srcolvclfy.ap-southeast-1.rds.amazonaws.com,1412; Database = DurustHRdb1;User Id=ILM_LiveDBUser;Password=ILMV3ry53cur3dMGTpa55w0rd2020;";

        }
        #endregion

        #region Miscellaneous
        string GetCurrentDate()
        {
            string date = DateTime.UtcNow.ToString("yyyy-MM-dd");
            return date;
        }

        string NumberToWords(int number)
        {

            try
            {
                if (number == 0)
                    return "zero";

                if (number < 0)
                    return "minus " + NumberToWords(Math.Abs(number));

                string words = "";

                if ((number / 1000000) > 0)
                {
                    words += NumberToWords(number / 1000000) + " Million ";
                    number %= 1000000;
                }

                if ((number / 1000) > 0)
                {
                    words += NumberToWords(number / 1000) + " Thousand ";
                    number %= 1000;
                }

                if ((number / 100) > 0)
                {
                    words += NumberToWords(number / 100) + " Hundred ";
                    number %= 100;
                }

                if (number > 0)
                {
                    if (words != "")
                        words += "and ";

                    var unitsMap = new[] { "Zero", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eighteen", "Nineteen" };
                    var tensMap = new[] { "Zero", "Ten", "Twenty", "Thirty", "Forty", "Fifty", "Sixty", "Seventy", "Eighty", "Ninety" };

                    if (number < 20)
                        words += unitsMap[number];
                    else
                    {
                        words += tensMap[number / 10];
                        if ((number % 10) > 0)
                            words += "-" + unitsMap[number % 10];
                    }
                }

                return words;
            }
            catch (Exception e)
            {
                return "";
                throw;
            }
        }
        #endregion

        #region GeneratePDF 
        [HttpPost]
        [Route("GeneratePDF_GovTax")]
        public string GeneratePDFGovTax(GovTaxParamaters param)
        {
            try
            {
                DateTimeFormatInfo mfi = new DateTimeFormatInfo();
                float total = 0f;

                //Operation Folder Path
                string filePath = HttpContext.Current.Server.MapPath("~/pdf/"); // Create new pathfolder
                string operationPath = HttpContext.Current.Server.MapPath("~/sFTP/PayrollOutsourcing/"); // Operation Folder

                //Output file path
                //string outputFile = operationPath + "output\\GovernmentTax-" + GetCurrentDate() + ".pdf";
                string outputFile = operationPath + param.CompanyCode + "_GovernmentTax_" + param.year + ".pdf";

                //Get Template File
                //string templateFile = operationPath + "pdf_templates\\GovermentTax_Template.pdf";
                string templateFile = HttpContext.Current.Server.MapPath("~/Files/PayrollOutSourcing/PDFTemplate/GovermentTax_Template.pdf");


                //Set PDF Files
                PdfReader pdfReader = new PdfReader(templateFile);
                PdfStamper pdfStamper = new PdfStamper(pdfReader, new FileStream(outputFile, FileMode.Create));
                AcroFields acroFields = pdfStamper.AcroFields;

                //Database Area
                List<GovTaxDataTable> govTaxDB = new List<GovTaxDataTable>();

                ConnectToDatabase_DurustHRDB1();
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@i_tenantID", mytype = SqlDbType.VarChar, Value = param.tenantID });
                con.myparameters.Add(new myParameters { ParameterName = "@i_Year", mytype = SqlDbType.VarChar, Value = param.year });
                DataTable dt = con.GetDataTable("sp_GeneratePDF_GOVTAX");

                foreach (DataRow row in dt.Rows)
                {
                    GovTaxDataTable tempTable = new GovTaxDataTable()
                    {
                        companyName = row["Company Name"].ToString(),
                        TaxTable = row["Tax Table"].ToString(),
                        TaxWithheld = float.Parse(row["Tax Withheld"].ToString())
                    };
                    govTaxDB.Add(tempTable);
                }

                int j = 0;
                //Action 
                for (int i = 0; i <= 11; i++)
                {
                    j++;
                    acroFields.SetField("month" + j, mfi.GetMonthName(j).ToString() + " " + DateTime.Now.Year.ToString());
                    acroFields.SetField("taxT" + j, govTaxDB[i].TaxTable).ToString();
                    acroFields.SetField("taxW" + j, string.Format("{0:#,0.00}", govTaxDB[i].TaxWithheld));

                    if (govTaxDB[i].TaxWithheld > 0f)
                        total = total + govTaxDB[i].TaxWithheld;
                }

                // Set Data to PDF Text Field for GOVTAX
                acroFields.SetField("txtBranch", govTaxDB[0].companyName.ToString());
                acroFields.SetField("txtTaxTable", govTaxDB[0].TaxTable.ToString());
                acroFields.SetField("currentDate", GetCurrentDate());
                acroFields.SetField("total", string.Format("{0:#,0.00}", total));

                // FormFlattening to remove textfields in pdf when opened.
                pdfStamper.FormFlattening = true;
                // Close Stamper
                pdfStamper.Close();

                //return "Successfully Executed the Method";
                string ReturnPath = "https://ws.durusthr.com/ILM_WS_Live/sFTP/PayrollOutsourcing/" + param.CompanyCode + "_GovernmentTax_" + param.year + ".pdf";
                return ReturnPath;


            }
            catch (Exception e)
            {
                return "Error: " + e.ToString();
                throw;
            }
        }

        [HttpPost]
        [Route("GeneratePDF_SSLoan")]
        public string GeneratePDFSSLoan(SSLoanParameters param)
        {
            try
            {
                //Operation Folder Path
                //string filePath = HttpContext.Current.Server.MapPath("~/pdf/"); // Create new pathfolder
                string filePath = HttpContext.Current.Server.MapPath("~/sFTP/PayrollOutsourcing/"); // Generation Path Folder
                //string operationPath = "C:\\Users\\i078\\Downloads\\PdfWriter\\"; // Operation Folder
                string operationPath = HttpContext.Current.Server.MapPath("~/Files/PayrollOutSourcing/PDFTemplate/"); // Template Path Folder

                //Output file path
                string outputFile = filePath + param.CompanyCode + "_SSSLoan_" + param.year + param.month + ".pdf";

                //Get Template File
                string templateFile = HttpContext.Current.Server.MapPath("~/Files/PayrollOutSourcing/PDFTemplate/SSLOAN_Template.pdf");

                //Set PDF Files
                PdfReader pdfReader = new PdfReader(templateFile);
                PdfStamper pdfStamper = new PdfStamper(pdfReader, new FileStream(outputFile, FileMode.Create));
                AcroFields acroFields = pdfStamper.AcroFields;

                //Database Area
                List<SSLoanDataTable> ssloanDB = new List<SSLoanDataTable>();

                ConnectToDatabase_DurustHRDB1();
                Connection con = new Connection();

                con.myparameters.Add(new myParameters { ParameterName = "@i_tenantID", mytype = SqlDbType.VarChar, Value = param.tenantID });
                con.myparameters.Add(new myParameters { ParameterName = "@i_month", mytype = SqlDbType.VarChar, Value = param.month });
                con.myparameters.Add(new myParameters { ParameterName = "@i_year", mytype = SqlDbType.VarChar, Value = param.year });
                DataTable dt = con.GetDataTable("sp_GeneratePDF_SSLoan");

                foreach (DataRow row in dt.Rows)
                {
                    SSLoanDataTable tempTable = new SSLoanDataTable()
                    {
                        companyName = row["Company Name"].ToString(),
                        companyAdress = row["Company Address"].ToString(),
                        sssNumber = row["SSS Number"].ToString(),
                        billingMonth = param.month + "-" + param.year,
                        totalAmountPaid = float.Parse(row["Total Amount Paid"].ToString()),
                        totalRecords = row["Total Records"].ToString()
                    };
                    ssloanDB.Add(tempTable);
                }

                string totalAmountPaid = string.Format("{0:#,0.00}", ssloanDB[0].totalAmountPaid);

                // Font Size
                acroFields.SetFieldProperty("txtBranch", "textsize", 13f, null);
                acroFields.SetFieldProperty("txtBranchAddress", "textsize", 10f, null);
                acroFields.SetFieldProperty("txtIDNumber", "textsize", 10f, null);
                acroFields.SetFieldProperty("txtBillingMonth", "textsize", 10f, null);
                acroFields.SetFieldProperty("txtTotalNumberOfRecords", "textsize", 10f, null);
                acroFields.SetFieldProperty("txtTotalAmountPaid", "textsize", 10f, null);
                acroFields.SetFieldProperty("txtAmountPaid", "textsize", 10f, null);
                acroFields.SetFieldProperty("txtTransSBR", "textsize", 10f, null);
                acroFields.SetFieldProperty("txtDatePaid", "textsize", 10f, null);
                acroFields.SetFieldProperty("txtRepresentative", "textsize", 12f, null);

                //Set TextFields Area
                acroFields.SetField("txtBranch", ssloanDB[0].companyName);
                acroFields.SetField("txtBranchAddress", ssloanDB[0].companyAdress);
                acroFields.SetField("txtIDNumber", ssloanDB[0].sssNumber);
                acroFields.SetField("txtBillingMonth", ssloanDB[0].billingMonth);
                acroFields.SetField("txtTotalNumberOfRecords", ssloanDB[0].totalRecords);
                acroFields.SetField("txtTotalAmountPaid", string.Format("{0:#,0.00}", ssloanDB[0].totalAmountPaid));
                acroFields.SetField("txtAmountPaid", string.Format("{0:#,0.00}", ssloanDB[0].totalAmountPaid));
                acroFields.SetField("txtTransSBR", testdata);
                acroFields.SetField("txtDatePaid", testdata);
                acroFields.SetField("txtRepresentative", "MILDRED GUERRA " + Environment.NewLine + "FINANCE DIRECTOR");

                // FormFlattening to remove textfields in pdf when opened.
                pdfStamper.FormFlattening = true;
                // Close Stamper
                pdfStamper.Close();

                // return "Successfully Executed the Method";
                string ReturnPath = "https://ws.durusthr.com/ILM_WS_Live/sFTP/PayrollOutsourcing/" + param.CompanyCode + "_SSSLoan_" + param.year + param.month + ".pdf";
                return ReturnPath;
            }
            catch (Exception e)
            {
                return "Error: " + e.ToString();
                throw;
            }
        }

        [HttpPost]
        [Route("GeneratePDF_GOVR5")]
        public string GeneratePDFGOVR5(GovR5Paramaters param)
        {
            try
            {
                #region ERASE THIS WHEN UPLOADING TO THE SERVER
                ////Operation Folder Path
                //string filePath = HttpContext.Current.Server.MapPath("~/pdf/"); // Create new pathfolder
                //string operationPath = "C:\\Users\\i078\\Downloads\\PdfWriter\\"; // Operation Folder

                ////Output file path
                //string outputFile = operationPath + "output\\" + param.CompanyCode + "_GOVR5_" + param.year + "-" + param.month + ".pdf";

                ////Get Template File
                //string templateFile = operationPath + "pdf_templates\\GOVR5_Template.pdf";
                #endregion

                //Operation Folder Path
                string filePath = HttpContext.Current.Server.MapPath("~/sFTP/PayrollOutsourcing/"); // Generation Path Folder
                //string operationPath = "C:\\Users\\i078\\Downloads\\PdfWriter\\"; // Operation Folder
                string operationPath = HttpContext.Current.Server.MapPath("~/Files/PayrollOutSourcing/PDFTemplate/"); // Template Path Folder

                //Output file path
                string outputFile = filePath + param.CompanyCode + "_GOVR5_" + param.year + param.month + ".pdf";

                //Get Template File
                string templateFile = HttpContext.Current.Server.MapPath("~/Files/PayrollOutSourcing/PDFTemplate/GOVR5_Template.pdf");

                //Set PDF Files
                PdfReader pdfReader = new PdfReader(templateFile);
                PdfStamper pdfStamper = new PdfStamper(pdfReader, new FileStream(outputFile, FileMode.Create));
                AcroFields acroFields = pdfStamper.AcroFields;

                //Database Area
                List<GovR5DataTable> govr5DB = new List<GovR5DataTable>();

                ConnectToDatabase_DurustHRDB1();
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@i_tenantID", mytype = SqlDbType.VarChar, Value = param.tenantID });
                con.myparameters.Add(new myParameters { ParameterName = "@i_month", mytype = SqlDbType.VarChar, Value = param.month });
                con.myparameters.Add(new myParameters { ParameterName = "@i_year", mytype = SqlDbType.VarChar, Value = param.year });
                DataTable dt = con.GetDataTable("sp_GeneratePDF_GOVR5");

                foreach (DataRow row in dt.Rows)
                {
                    GovR5DataTable tempTable = new GovR5DataTable()
                    {
                        sssNumber = row["SSS Number"].ToString(),
                        tinNumber = row["TIN Number"].ToString(),
                        employerName = row["Company Name"].ToString(),
                        address = row["Address"].ToString(),
                        sssAmount_ER = float.Parse(row["SSS Contribution"].ToString()),
                        sssAmount_EC = float.Parse(row["SSS EC"].ToString()),
                    };
                    govr5DB.Add(tempTable);
                }

                // Splitted ID
                List<char> sssID = new List<char>();
                List<char> tinID = new List<char>();

                foreach (char c in govr5DB[0].sssNumber.ToString())
                {
                    sssID.Add(c);
                }

                foreach (char c in govr5DB[0].tinNumber.ToString())
                {
                    tinID.Add(c);
                }

                // Set Fields
                int j = 0;
                for (int i = 0; i < sssID.Count; i++)
                {
                    j++;
                    acroFields.SetField("emprNum" + j, sssID[i].ToString());
                }

                j = 0;
                for (int i = 0; i < tinID.Count; i++)
                {
                    j++;
                    acroFields.SetField("tin" + j, tinID[i].ToString());
                }

                float totalContribution = govr5DB[0].sssAmount_ER + govr5DB[0].sssAmount_EC;

                acroFields.SetField("month" + param.month, param.year);
                acroFields.SetField("ssc" + param.month, string.Format("{0:#,0.00}", govr5DB[0].sssAmount_ER));
                acroFields.SetField("sscTotal", string.Format("{0:#,0.00}", govr5DB[0].sssAmount_ER));
                acroFields.SetField("sscTotalFinal", string.Format("{0:#,0.00}", govr5DB[0].sssAmount_ER));

                acroFields.SetField("ecc" + param.month, string.Format("{0:#,0.00}", govr5DB[0].sssAmount_EC));
                acroFields.SetField("eccTotal", string.Format("{0:#,0.00}", govr5DB[0].sssAmount_EC));
                acroFields.SetField("eccTotalFinal", string.Format("{0:#,0.00}", govr5DB[0].sssAmount_EC));

                acroFields.SetField("total" + param.month, string.Format("{0:#,0.00}", totalContribution));
                acroFields.SetField("totalTotal", string.Format("{0:#,0.00}", totalContribution));
                acroFields.SetField("totalTotalFinal", string.Format("{0:#,0.00}", totalContribution));

                acroFields.SetField("checkAmount", string.Format("{0:#,0.00}", totalContribution));
                acroFields.SetField("totalFigures", string.Format("{0:#,0.00}", totalContribution));

                acroFields.SetField("emprName", govr5DB[0].employerName.ToString());
                acroFields.SetField("emprAddress", govr5DB[0].address.ToString());

                acroFields.SetField("totalWords", "PESOS " + NumberToWords(Convert.ToInt32(totalContribution)));

                acroFields.SetField("printedName", "MILDRED GUERRA");
                acroFields.SetField("positionTitle", "FINANCE DIRECTOR");

                // FormFlattening to remove textfields in pdf when opened.
                pdfStamper.FormFlattening = true;
                // Close Stamper
                pdfStamper.Close();

                // return "Successfully Executed the Method";
                string ReturnPath = "https://ws.durusthr.com/ILM_WS_Live/sFTP/PayrollOutsourcing/" + param.CompanyCode + "_GOVR5_" + param.year + param.month + ".pdf";
                return ReturnPath;
                //return "Successfully Executed the Method";
            }
            catch (Exception e)
            {
                return "Error: " + e.ToString();
                throw;
            }
        }

        [HttpPost]
        [Route("GeneratePDF_PHQTR")]
        public string GeneratePDFPHQTR(PHQTRParamaters param)
        {
            try
                {
                ConnectToDatabase_DurustHRDB1();
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@i_tenantID", mytype = SqlDbType.VarChar, Value = param.tenantID });
                con.myparameters.Add(new myParameters { ParameterName = "@i_month", mytype = SqlDbType.VarChar, Value = param.month });
                con.myparameters.Add(new myParameters { ParameterName = "@i_year", mytype = SqlDbType.VarChar, Value = param.year });
                DataTable PHQTRTable = con.GetDataTable("sp_GeneratePDF_PHQTR");

                DataTable CompanyInfo = GetCompanyInfo(param.tenantID);

                //int countRows = GetSSSR1Acount(parameters.Company, parameters.Month, parameters.Year).Rows.Count;
                int countRows = PHQTRTable.Rows.Count;
                int counter = 0;
                int decrement = 0;
                int fordecre = 352;

                float subtotalPS = 0f;
                float subtotalES = 0f;
                float subtotalTotal = 0f;

                //int total = GetSSSR1Acount(parameters.Company, parameters.Month, parameters.Year).Rows.Count;
                int total = PHQTRTable.Rows.Count;
                int totalperpage = 0;

                //decimal list = decimal.Divide(GetSSSR1Acount(parameters.Company, parameters.Month, parameters.Year).Rows.Count, 15);
                decimal list = decimal.Divide(countRows, 10);
                decimal numPage = Math.Ceiling(list);
                string numMonth = "";

                string pathsource;
                pathsource = "PHQTR_Template";
                //string reader1 = HttpContext.Current.Server.MapPath("~/pdf/") + pathsource + ".pdf";
                string reader1 = HttpContext.Current.Server.MapPath("~/Files/PayrollOutSourcing/PDFTemplate/") + pathsource + ".pdf";
                string pathmoto;
                pathmoto = "PHQTR";
                if (param.month.Length == 1)
                {
                    numMonth = "0" + param.month.ToString();
                }
                //string reader2 = HttpContext.Current.Server.MapPath("~/pdf/") + GetCompanyInfo(parameters.Company).Rows[0][5].ToString().ToUpper() + "_" + pathmoto + "_" + parameters.Year.ToString() + numMonth + ".pdf";
                string reader2 = HttpContext.Current.Server.MapPath("~/sFTP/PayrollOutsourcing/") + CompanyInfo.Rows[0][5].ToString().ToUpper() + "_" + pathmoto + "_" + param.year + param.month + ".pdf";
                string fileName = reader1;
                string outputFileName = reader2;

                //Remitted Amount
                double totalRemittedAmount = 0f;
                double totalPS = 0f, totalES = 0f;

                for (int i = 0; i < PHQTRTable.Rows.Count; i++)
                {
                    totalPS += double.Parse(PHQTRTable.Rows[i][4].ToString());
                    totalES += double.Parse(PHQTRTable.Rows[i][5].ToString());
                }

                totalRemittedAmount = totalPS + totalES;

                //Step 1: Create a Docuement-Object
                Document document = new Document();

                try
                {
                    //The current file path
                    string filename = fileName;

                    // we create a reader for the document
                    PdfReader reader = new PdfReader(filename);

                    //Step 2: we create a writer that listens to the document
                    PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(outputFileName, FileMode.Create));

                    //Step 3: Open the document
                    document.Open();

                    PdfContentByte cb = writer.DirectContent;

                    for (int i = 0; i <= numPage; i++)
                    {
                        for (int pageNumber = 1; pageNumber < reader.NumberOfPages + 1; pageNumber++)
                        {
                            document.SetPageSize(reader.GetPageSizeWithRotation(1));
                            document.NewPage();

                            //Insert to Destination on the first page
                            if (pageNumber == 1)
                            {
                                Chunk fileRef = new Chunk(" ");
                                fileRef.SetLocalDestination(filename);
                                document.Add(fileRef);
                            }

                            PdfImportedPage page = writer.GetImportedPage(reader, pageNumber);
                            int rotation = reader.GetPageRotation(pageNumber);
                            if (rotation == 90 || rotation == 270)
                            {
                                cb.AddTemplate(page, 0, -1f, 1f, 0, 0, reader.GetPageSizeWithRotation(pageNumber).Height);
                            }
                            else
                            {
                                cb.AddTemplate(page, 1f, 0, 0, 1f, 0, 0);
                            }


                            //Company Info
                            BaseFont bf = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                            cb.SetColorFill(BaseColor.DARK_GRAY);
                            cb.SetFontAndSize(bf, 10);

                            cb.BeginText();

                            string applicablePeriod = CaseMonth(param.month) + "-" + param.year.Substring(2);



                            #region PhilHealthNumm space for PHQTR

                            String philnum = CompanyInfo.Rows[0][2].ToString();
                            philnum = philnum.Replace("-", "");

                            List<char> philID = new List<char>();

                            foreach (var c in philnum)
                            {
                                philID.Add(c);
                            }

                            float philSpace = 165f;
                            float philGap = 16.5f;
                            int xx = 0;
                            for (int z = 0; z < philID.Count; z++)
                            {
                                switch (xx)
                                {
                                    case 2:
                                        philSpace += 7.5f;
                                        break;

                                    case 11:
                                        philSpace += 7.5f;
                                        break;
                                }
                                xx++;

                                cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, philID[z].ToString(), philSpace, 488, 0); // Philhealth Number
                                philSpace += philGap;
                            }
                            #endregion

                            // Tin Number SubString
                            #region Tin space for PHQTR
                            List<char> tinID = new List<char>();

                            foreach (char c in CompanyInfo.Rows[0][6].ToString())
                            {
                                tinID.Add(c);
                            }

                            float tinSpace = 165f;
                            float tinGap = 16.5f;
                            int x = 0;
                            for (int z = 0; z < tinID.Count; z++)
                            {
                                if (x == 3)
                                {
                                    tinSpace += 7.5f;
                                    x = 0;
                                }

                                x++;

                                cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, tinID[z].ToString(), tinSpace, 474.5f, 0);
                                tinSpace += tinGap;
                            }
                            #endregion

                            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, CompanyInfo.Rows[0][0].ToString().ToUpper(), 193, 458, 0); // Company Name

                            // Company Adress SubString
                            int charString = CompanyInfo.Rows[0][1].ToString().Length;
                            if (charString <= 50)
                            {
                                cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, CompanyInfo.Rows[0][1].ToString().ToUpper(), 200, 445, 0);
                            }
                            else //if (charString <= 100)
                            {
                                cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, CompanyInfo.Rows[0][1].ToString().ToUpper().Substring(0, 50), 200, 445, 0);
                                cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, CompanyInfo.Rows[0][1].ToString().ToUpper().Substring(50, charString - 50), 200, 432, 0);
                            }

                            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, applicablePeriod, 140, 105, 0); // Applicable Period
                            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, PHQTRTable.Rows[0][6].ToString(), 455, 105, 0); // Applicable Period
                            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, string.Format("{0:#,0.00}", totalRemittedAmount), 205, 105, 0); // Applicable Period
                            cb.EndText();

                            cb.BeginText();
                            cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "x", 554, 441, 0); // Employer Type
                            cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "x", 623, 441, 0); // Report Type
                            cb.EndText();

                            if (total > 10)
                            {
                                cb.SetColorFill(BaseColor.DARK_GRAY);
                                cb.SetFontAndSize(bf, 10);

                                totalperpage = 10;
                                total = total - totalperpage;
                                cb.BeginText();
                                cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, totalperpage.ToString(), 70, 144, 0);
                                cb.EndText();
                            }
                            else
                            {
                                cb.SetColorFill(BaseColor.DARK_GRAY);
                                cb.SetFontAndSize(bf, 10);

                                cb.BeginText();
                                cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, total.ToString(), 70, 144, 0);
                                cb.EndText();
                            }

                            // Finance Director
                            cb.SetColorFill(BaseColor.DARK_GRAY);
                            cb.SetFontAndSize(bf, 8);

                            cb.BeginText();
                            cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "MILDRED GUERRA", 231, 70, 0);
                            cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "FINANCE DIRECTOR", 461, 70, 0);
                            cb.EndText();

                            for (int j = 0; j < 10; j++)
                            {
                                decrement = j * 20;

                                cb.SetColorFill(BaseColor.DARK_GRAY);
                                cb.SetFontAndSize(bf, 9);

                                cb.BeginText();

                                #region PhiNum2
                                String philnum2 = PHQTRTable.Rows[counter][0].ToString();
                                philnum2 = philnum2.Replace("-", "");

                                List<char> philID2 = new List<char>();

                                foreach (var c in philnum2)
                                {
                                    philID2.Add(c);
                                }

                                float philSpace2 = 51;
                                float philGap2 = 11.5f;
                                int xx2 = 0;
                                for (int z = 0; z < philID2.Count; z++)
                                {
                                    switch (xx2)
                                    {
                                        case 2:
                                            philSpace2 += 5f;
                                            break;

                                        case 11:
                                            philSpace2 += 5f;
                                            break;
                                    }
                                    xx2++;

                                    cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, philnum2[z].ToString(), philSpace2, fordecre - decrement, 0); // Phil Health Num
                                    philSpace2 += philGap2;
                                }
                                #endregion

                                cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, PHQTRTable.Rows[counter][1].ToString().ToUpper(), 202, fordecre - decrement, 0); // Last Name
                                cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, PHQTRTable.Rows[counter][2].ToString().ToUpper(), 292, fordecre - decrement, 0); // First Name
                                cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, PHQTRTable.Rows[counter][3].ToString().ToUpper(), 412, fordecre - decrement, 0); // Middle Name

                                cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, PHQTRTable.Rows[counter][4].ToString().ToUpper(), 647, fordecre - decrement, 0); // PS
                                cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, PHQTRTable.Rows[counter][5].ToString().ToUpper(), 678, fordecre - decrement, 0); // ES
                                cb.EndText();

                                subtotalPS += float.Parse(PHQTRTable.Rows[counter][4].ToString());
                                subtotalES += float.Parse(PHQTRTable.Rows[counter][5].ToString());

                                counter = counter + 1;

                                if (counter == countRows)
                                {
                                    cb.SetColorFill(BaseColor.DARK_GRAY);
                                    cb.SetFontAndSize(bf, 8);

                                    cb.BeginText();

                                    subtotalTotal = subtotalPS + subtotalES;

                                    cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, string.Format("{0:#,0.00}", subtotalPS), 647, 150, 0); // Subtotal PS
                                    cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, string.Format("{0:#,0.00}", subtotalES), 678, 150, 0); // Subtotal ES
                                    cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, string.Format("{0:#,0.00}", subtotalTotal), 675, 130, 0); // Subtotal Total

                                    cb.EndText();

                                    subtotalTotal = subtotalES = subtotalPS = 0f;

                                    document.Close();
                                    break;
                                }
                            }

                            cb.SetColorFill(BaseColor.DARK_GRAY);
                            cb.SetFontAndSize(bf, 8);

                            cb.BeginText();

                            subtotalTotal = subtotalPS + subtotalES;

                            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, string.Format("{0:#,0.00}", subtotalPS), 647, 150, 0); // Subtotal PS
                            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, string.Format("{0:#,0.00}", subtotalES), 678, 150, 0); // Subtotal ES
                            cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, string.Format("{0:#,0.00}", subtotalTotal), 675, 130, 0); // Subtotal Total

                            cb.EndText();

                            subtotalTotal = subtotalES = subtotalPS = 0f;
                        }

                        if (counter == countRows)
                        {
                            document.Close();
                            break;
                        }
                    }
                }
                catch (Exception e)
                {
                    throw e;
                }
                finally
                {
                    document.Close();
                }

                string ReturnPath = "https://ws.durusthr.com/ILM_WS_Live/sFTP/PayrollOutsourcing/" + CompanyInfo.Rows[0][5].ToString().ToUpper() + "_" + pathmoto + "_" + param.year + param.month + ".pdf";

                return ReturnPath;
            }
            catch (Exception e)
            {
                return e.ToString();
                throw;
            }
        }
        #endregion


        #region Execute Batch File
        [HttpPost]
        [Route("RunBatchFile")]
        public string RunBatchFile()
        {
            try
            {
                // Java.exe Path and Java Arguments
                string javaPath = "C:\\Program Files (x86)\\Java\\jre1.8.0_251\\bin\\java.exe";
                string arguments = "-jar C:\\Users\\i078\\Downloads\\PdfWriter\\PdfWriter.jar";
                string data = "\"C:\\Users\\i078\\Downloads\\PdfWriter\\input\\sample_Input_1601c.txt\"";

                // Process the data to .jar file
                Process process = new Process();
                process.StartInfo.UseShellExecute = false;
                process.StartInfo.FileName = javaPath;
                process.StartInfo.Arguments = arguments + " " + data;
                process.Start();

                #region Different Way To Call .jar File
                //string filePath = "C:\\Users\\i078\\Downloads\\PdfWriter\\runPdfWriter.bat";
                //Process.Start(javaPath, arguments + " " + data);
                #endregion

                return "Successfully Running Batch File";
            }
            catch (Exception e)
            {
                return "Error";
                throw;
            }

        }

        public string GetDBILMMain()
        {
            // com.connectionstring = ConfigurationManager.ConnectionStrings["connectionstring"].ConnectionString.ToString();
            var settings = ConfigurationManager.ConnectionStrings["connectionstring"];
            var fi = typeof(ConfigurationElement).GetField(
                          "_bReadOnly",
                          BindingFlags.Instance | BindingFlags.NonPublic);
            fi.SetValue(settings, false);
            // settings.ConnectionString = "Data Source=tcp:ilmuat.c3srcolvclfy.ap-southeast-1.rds.amazonaws.com,1400;Database=ILM_Main;User Id=devuatuser;Password=securedmgt1230;";
            settings.ConnectionString = "Data Source=livedb-ilm.c3srcolvclfy.ap-southeast-1.rds.amazonaws.com,1412;Database=DurustHRdb1;User Id=ILM_LiveDBUser;Password=securedmgt1230;";

            return settings.ToString();
        }

        //[HttpPost]
        //[Route("GetClientList")]
        //public List<getClientList> GetClientList(getClientList clientList)
        //{
        //    //GetDatabase GDB = new GetDatabase();
        //    //GDB.ClientName = "ILM";
        //    //SelfieRegistration2Controller.GetDB2(GDB);

        //    GetDBILMMain();

        //    Connection con = new Connection();

        //    DataTable DT = new DataTable();
        //    DT = con.GetDataTable("sp_GetClientList");
        //    string x = ConfigurationManager.ConnectionStrings["connectionstring"].ToString();
        //    List<getClientList> ListReturned = new List<getClientList>();

        //    for (int i = 0; i < DT.Rows.Count; i++)
        //    {

        //        getClientList Get = new getClientList();
        //        Get.ClientName = DT.Rows[i]["ClientName"].ToString();
        //        Get.DB_Settings = DT.Rows[i]["DB_Settings"].ToString();

        //        ListReturned.Add(Get);
        //    }
        //    return ListReturned;

        //}

        [HttpPost]
        [Route("GenerateHDMPay")]
        public string HDMPayList()
        {
            //GetDatabase GDB = new GetDatabase();
            //GDB.ClientName = hmdp.CN;
            //CN = hmdp.CN;

            //Connection con = new Connection();
            //con.myparameters.Add(new myParameters { ParameterName = "@CN", mytype = SqlDbType.NVarChar, Value = CN });
            //hdmpayrecord = con.GetDataTable("");

            string pathmoto;
            string pathmoto1;

            pathmoto = "HDMPAY";
            pathmoto1 = "HDMPAY2";

            //PdfReader reader1 = new PdfReader(HttpContext.Current.Server.MapPath("~/pdf/") + pathmoto + ".pdf");
            //LocationTextExtractionStrategy lteStrategy = new LocationTextExtractionStrategy();
            //string strExact = PdfTextExtractor.GetTextFromPage(reader1, 1, lteStrategy);
            //StringBuilder sb = new StringBuilder();
            //this code used for replacing the found text

            //for (var i = 0; i > hdmpayrecord.Length; i++)
            //{
            // read the template file
            PdfReader reader = new PdfReader(HttpContext.Current.Server.MapPath("~/Files/PayrollOutSourcing/PDFTemplate/") + pathmoto + ".pdf");


            // instantiate PDFStamper object
            // The Output file will be created from template file and edited by the PDFStamper
            PdfStamper stamper = new PdfStamper(reader, new FileStream(HttpContext.Current.Server.MapPath("~/pdf/") + pathmoto1 + ".pdf", FileMode.Create));


            // Object to deal with the Output file's textfields
            //for (var i = 0; i > 2; i++)
            //{

            AcroFields fields = stamper.AcroFields;
            fields.SetField("Date", "07-16-2020");
            fields.SetField("PageNo", "Page 1");
            fields.SetField("RecNo", "1");
            fields.SetField("LastName", "Moya");
            fields.SetField("FirstName", "Joshua");
            fields.SetField("MiddleName", "Santos");
            fields.SetField("EE", "100");
            fields.SetField("ER", "111");
            fields.SetField("EYEENO", "i018");
            fields.SetField("TIN", "111-111-111");
            fields.SetField("BIRTHDATE", "10-24-1996");
            //    StringBuilder sb = new StringBuilder();
            //    sb.Append(PdfTextExtractor.GetTextFromPage(reader, 1));
            //}




            // the final output can't be edited
            //stamper.FormFlattening = true;
            // closing the stamper
            stamper.Close();

            return "Success";
        }




        [HttpPost]
        [Route("hdmpaygen")]
        public List<HDMPayList> HDMPayList(HDMPayList hmdp)
        {
            GetDBILMMain();

            Connection con = new Connection();
            DataTable DT = new DataTable();
            con.myparameters.Add(new myParameters { ParameterName = "@code", mytype = SqlDbType.NVarChar, Value = hmdp.code });
            con.myparameters.Add(new myParameters { ParameterName = "@month", mytype = SqlDbType.NVarChar, Value = hmdp.mmonth });
            con.myparameters.Add(new myParameters { ParameterName = "@year", mytype = SqlDbType.NVarChar, Value = hmdp.yyear });
            DT = con.GetDataTable("sp_GetHDMPAY");

            string x = ConfigurationManager.ConnectionStrings["connectionstring"].ToString();
            List<HDMPayList> ListReturned = new List<HDMPayList>();

            //return ListReturned;
            string oldFile = HttpContext.Current.Server.MapPath("~/Files/PayrollOutSourcing/PDFTemplate/") + "HDMPAY" + ".pdf";
            string newFile = HttpContext.Current.Server.MapPath("~/Files/PayrollOutSourcing/PDFTemplate/") + "HDMPAY2" + ".pdf";

            // open the reader
            PdfReader reader = new PdfReader(oldFile);
            Rectangle size = reader.GetPageSizeWithRotation(1);
            Document document = new Document(size);

            // open the writer
            FileStream fs = new FileStream(newFile, FileMode.Create, FileAccess.Write);
            PdfWriter writer = PdfWriter.GetInstance(document, fs);
            document.Open();

            // the pdf content
            PdfContentByte cb = writer.DirectContent;

            // select the font properties
            BaseFont bf = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
            cb.SetColorFill(BaseColor.DARK_GRAY);
            cb.SetFontAndSize(bf, 8);

            PdfImportedPage page = writer.GetImportedPage(reader, 1);
            cb.AddTemplate(page, 0, 0);
            // write the text in the pdf content

            cb.BeginText();
            //text = DateTime.Today;
            // put the alignment and coordinates here
            cb.ShowTextAligned(0, DateTime.Now.ToString("MM-dd-yyyy"), 50, 750, 0);
            cb.EndText();

            cb.BeginText();
            //text = DT.Rows[i]["CompanyName"].ToString();
            // put the alignment and coordinates here
            cb.ShowTextAligned(0, DT.Rows[0]["CompanyName"].ToString(), 240, 750, 0);
            cb.EndText();

            int numb1 = DT.Rows.Count - 1;
            var couterr = numb1.ToString();
            int NOOFDATA = DT.Rows.Count;
            Double pages = NOOFDATA / 76;
            //Double finalnoofpages = 1;
            //Double baseno = 1.000;
            int txtheight = 680;
            int txtheight2 = 100;
            int baseno2 = 57;
            int baseno = 00000;


            for (int i = 0; i < DT.Rows.Count; i++)
            {
                var counters = i.ToString();
                HDMPayList Get = new HDMPayList();
                Get.firstname = DT.Rows[i]["FirstName"].ToString();
                Get.middlename = DT.Rows[i]["MiddleName"].ToString();
                Get.lastname = DT.Rows[i]["LastName"].ToString();
                Get.birthdate = DT.Rows[i]["BirthDate"].ToString();
                Get.tinnum = DT.Rows[i]["TINnum"].ToString();
                Get.ee = DT.Rows[i]["PagIbigAmount"].ToString();
                Get.er = DT.Rows[i]["PagibigERAmount"].ToString();
                Get.companycode = DT.Rows[i]["CompanyCode"].ToString();
                Get.companyname = DT.Rows[i]["CompanyName"].ToString();
                Get.totalee = DT.Rows[i]["TotalPagIbigAmount"].ToString();
                Get.totaler = DT.Rows[i]["TotalPagibigERAmount"].ToString();
                Get.finaltotal = DT.Rows[i]["FinalTotal"].ToString();
                ListReturned.Add(Get);

                if (i < baseno2)
                {

                    //cb.BeginText();
                    ////text = DateTime.Today;
                    //// put the alignment and coordinates here
                    //cb.ShowTextAligned(0, DateTime.Now.ToString("MM-dd-yyyy"), 50, 750, 0);
                    //cb.EndText();

                    //cb.BeginText();
                    ////text = DT.Rows[i]["CompanyName"].ToString();
                    //// put the alignment and coordinates here
                    //cb.ShowTextAligned(0, DT.Rows[0]["CompanyName"].ToString(), 200, 750, 0);
                    //cb.EndText();

                    if (counters == couterr)
                    {
                        cb.BeginText();
                        //text = DT.Rows[i]["Empid"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(0, "Totals:" + NOOFDATA, 75, txtheight2, 0);
                        cb.EndText();

                        cb.BeginText();
                        //text = DT.Rows[i]["EmployeePHealth"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(0, DT.Rows[0]["TotalPagIbigAmount"].ToString(), 355, txtheight2, 0);
                        cb.EndText();


                        cb.BeginText();
                        //text = DT.Rows[i]["EmployerPHealth"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(0, DT.Rows[0]["TotalPagibigERAmount"].ToString(), 399, txtheight2, 0);
                        cb.EndText();

                        cb.BeginText();
                        //text = DT.Rows[i]["EmployeePHealth"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(0, "Total PH:", 355, txtheight2 - 10, 0);
                        cb.EndText();


                        cb.BeginText();
                        //text = DT.Rows[i]["EmployerPHealth"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(0, DT.Rows[0]["FinalTotal"].ToString(), 399, txtheight2 - 10, 0);
                        cb.EndText();


                    }

                    cb.BeginText();
                    string text = i.ToString();
                    // put the alignment and coordinates here
                    cb.ShowTextAligned(0, text, 30, txtheight, 0);
                    cb.EndText();

                    cb.BeginText();
                    text = DT.Rows[i]["LastName"].ToString();
                    // put the alignment and coordinates here
                    cb.ShowTextAligned(0, text.Length > 17 ? text.Remove(17) : text, 58, txtheight, 0);
                    cb.EndText();

                    cb.BeginText();
                    text = DT.Rows[i]["FirstName"].ToString();
                    // put the alignment and coordinates here
                    cb.ShowTextAligned(0, text.Length > 17 ? text.Remove(17) : text, 165, txtheight, 0);
                    cb.EndText();

                    cb.BeginText();
                    text = DT.Rows[i]["MiddleName"].ToString();
                    // put the alignment and coordinates here
                    cb.ShowTextAligned(0, text.Length > 17 ? text.Remove(17) : text, 270, txtheight, 0);
                    cb.EndText();

                    cb.BeginText();
                    text = DT.Rows[i]["PagIbigAmount"].ToString();
                    // put the alignment and coordinates here
                    cb.ShowTextAligned(0, text, 355, txtheight, 0);
                    cb.EndText();

                    cb.BeginText();
                    text = DT.Rows[i]["PagibigERAmount"].ToString();
                    // put the alignment and coordinates here
                    cb.ShowTextAligned(0, text, 399, txtheight, 0);
                    cb.EndText();

                    cb.BeginText();
                    text = (baseno + i.ToString());
                    // put the alignment and coordinates here
                    cb.ShowTextAligned(0, text, 445, txtheight, 0);
                    cb.EndText();

                    cb.BeginText();
                    text = DT.Rows[i]["TINnum"].ToString();
                    // put the alignment and coordinates here
                    cb.ShowTextAligned(0, text, 484, txtheight, 0);
                    cb.EndText();

                    cb.BeginText();
                    text = DT.Rows[i]["BirthDate"].ToString();
                    // put the alignment and coordinates here
                    cb.ShowTextAligned(0, text, 538, txtheight, 0);
                    cb.EndText();

                    int decheight = txtheight - 10;
                    txtheight = decheight;

                }
                else if ((i % baseno2) == 0)
                {
                    txtheight = 680;
                    document.NewPage();
                    cb.SetColorFill(BaseColor.DARK_GRAY);
                    cb.SetFontAndSize(bf, 8);
                    //PdfImportedPage page = writer.GetImportedPage(reader, 2);
                    PdfImportedPage page2 = writer.GetImportedPage(reader, 1);
                    cb.AddTemplate(page2, 0, 0);
                    //BaseFont bf = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);

                    if (counters == couterr)
                    {
                        cb.BeginText();
                        //text = DT.Rows[i]["Empid"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(0, "Totals:" + NOOFDATA, 75, txtheight2, 0);
                        cb.EndText();

                        cb.BeginText();
                        //text = DT.Rows[i]["EmployeePHealth"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(0, DT.Rows[0]["TotalPagIbigAmount"].ToString(), 355, txtheight2, 0);
                        cb.EndText();


                        cb.BeginText();
                        //text = DT.Rows[i]["EmployerPHealth"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(0, DT.Rows[0]["TotalPagibigERAmount"].ToString(), 399, txtheight2, 0);
                        cb.EndText();

                        cb.BeginText();
                        //text = DT.Rows[i]["EmployeePHealth"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(0, "Total PH:", 355, txtheight2 - 10, 0);
                        cb.EndText();


                        cb.BeginText();
                        //text = DT.Rows[i]["EmployerPHealth"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(0, DT.Rows[0]["FinalTotal"].ToString(), 399, txtheight2 - 10, 0);
                        cb.EndText();


                    }

                    cb.BeginText();
                    //text = DateTime.Today;
                    // put the alignment and coordinates here
                    cb.ShowTextAligned(0, DateTime.Now.ToString("MM-dd-yyyy"), 50, 750, 0);
                    cb.EndText();

                    cb.BeginText();
                    //text = DT.Rows[i]["CompanyName"].ToString();
                    // put the alignment and coordinates here
                    cb.ShowTextAligned(0, DT.Rows[0]["CompanyName"].ToString(), 240, 750, 0);
                    cb.EndText();

                    cb.BeginText();
                    string text = i.ToString();
                    // put the alignment and coordinates here
                    cb.ShowTextAligned(0, text, 30, txtheight, 0);
                    cb.EndText();

                    cb.BeginText();
                    text = DT.Rows[i]["LastName"].ToString();
                    // put the alignment and coordinates here
                    cb.ShowTextAligned(0, text.Length > 17 ? text.Remove(17) : text, 58, txtheight, 0);
                    cb.EndText();

                    cb.BeginText();
                    text = DT.Rows[i]["FirstName"].ToString();
                    // put the alignment and coordinates here
                    cb.ShowTextAligned(0, text.Length > 17 ? text.Remove(17) : text, 165, txtheight, 0);
                    cb.EndText();

                    cb.BeginText();
                    text = DT.Rows[i]["MiddleName"].ToString();
                    // put the alignment and coordinates here
                    cb.ShowTextAligned(0, text.Length > 17 ? text.Remove(17) : text, 270, txtheight, 0);
                    cb.EndText();

                    cb.BeginText();
                    text = DT.Rows[i]["PagIbigAmount"].ToString();
                    // put the alignment and coordinates here
                    cb.ShowTextAligned(0, text, 355, txtheight, 0);
                    cb.EndText();

                    cb.BeginText();
                    text = DT.Rows[i]["PagibigERAmount"].ToString();
                    // put the alignment and coordinates here
                    cb.ShowTextAligned(0, text, 399, txtheight, 0);
                    cb.EndText();

                    cb.BeginText();
                    text = (baseno + i.ToString());
                    // put the alignment and coordinates here
                    cb.ShowTextAligned(0, text, 445, txtheight, 0);
                    cb.EndText();

                    cb.BeginText();
                    text = DT.Rows[i]["TINnum"].ToString();
                    // put the alignment and coordinates here
                    cb.ShowTextAligned(0, text, 484, txtheight, 0);
                    cb.EndText();

                    cb.BeginText();
                    text = DT.Rows[i]["BirthDate"].ToString();
                    // put the alignment and coordinates here
                    cb.ShowTextAligned(0, text, 538, txtheight, 0);
                    cb.EndText();

                    int decheight = txtheight - 10;
                    txtheight = decheight;
                }
                else
                {

                    if (counters == couterr)
                    {
                        cb.BeginText();
                        //text = DT.Rows[i]["Empid"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(0, "Totals:" + NOOFDATA, 75, txtheight2, 0);
                        cb.EndText();

                        cb.BeginText();
                        //text = DT.Rows[i]["EmployeePHealth"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(0, DT.Rows[0]["TotalPagIbigAmount"].ToString(), 355, txtheight2, 0);
                        cb.EndText();


                        cb.BeginText();
                        //text = DT.Rows[i]["EmployerPHealth"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(0, DT.Rows[0]["TotalPagibigERAmount"].ToString(), 399, txtheight2, 0);
                        cb.EndText();

                        cb.BeginText();
                        //text = DT.Rows[i]["EmployeePHealth"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(0, "Total PH:", 355, txtheight2 - 10, 0);
                        cb.EndText();


                        cb.BeginText();
                        //text = DT.Rows[i]["EmployerPHealth"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(0, DT.Rows[0]["FinalTotal"].ToString(), 399, txtheight2 - 10, 0);
                        cb.EndText();


                    }

                    cb.BeginText();
                    string text = i.ToString();
                    // put the alignment and coordinates here
                    cb.ShowTextAligned(0, text, 30, txtheight, 0);
                    cb.EndText();

                    cb.BeginText();
                    text = DT.Rows[i]["LastName"].ToString();
                    // put the alignment and coordinates here
                    cb.ShowTextAligned(0, text.Length > 17 ? text.Remove(17) : text, 58, txtheight, 0);
                    cb.EndText();

                    cb.BeginText();
                    text = DT.Rows[i]["FirstName"].ToString();
                    // put the alignment and coordinates here
                    cb.ShowTextAligned(0, text.Length > 17 ? text.Remove(17) : text, 165, txtheight, 0);
                    cb.EndText();

                    cb.BeginText();
                    text = DT.Rows[i]["MiddleName"].ToString();
                    // put the alignment and coordinates here
                    cb.ShowTextAligned(0, text.Length > 17 ? text.Remove(17) : text, 270, txtheight, 0);
                    cb.EndText();

                    cb.BeginText();
                    text = DT.Rows[i]["PagIbigAmount"].ToString();
                    // put the alignment and coordinates here
                    cb.ShowTextAligned(0, text, 355, txtheight, 0);
                    cb.EndText();

                    cb.BeginText();
                    text = DT.Rows[i]["PagibigERAmount"].ToString();
                    // put the alignment and coordinates here
                    cb.ShowTextAligned(0, text, 399, txtheight, 0);
                    cb.EndText();

                    cb.BeginText();
                    text = (baseno + i.ToString());
                    // put the alignment and coordinates here
                    cb.ShowTextAligned(0, text, 445, txtheight, 0);
                    cb.EndText();

                    cb.BeginText();
                    text = DT.Rows[i]["TINnum"].ToString();
                    // put the alignment and coordinates here
                    cb.ShowTextAligned(0, text, 484, txtheight, 0);
                    cb.EndText();

                    cb.BeginText();
                    text = DT.Rows[i]["BirthDate"].ToString();
                    // put the alignment and coordinates here
                    cb.ShowTextAligned(0, text, 538, txtheight, 0);
                    cb.EndText();

                    int decheight = txtheight - 10;
                    txtheight = decheight;



                }
            }






            // close the streams and voilá the file should be changed :)
            document.Close();
            fs.Close();
            writer.Close();
            reader.Close();

            return ListReturned;
        }

        [HttpPost]
        [Route("ssphh2gen")]
        public List<SSPHH2List> SSPHH2List(SSPHH2List ssphh2)
        {
            GetDBILMMain();

            Connection con = new Connection();
            DataTable DT = new DataTable();
            con.myparameters.Add(new myParameters { ParameterName = "@code", mytype = SqlDbType.NVarChar, Value = ssphh2.code });
            con.myparameters.Add(new myParameters { ParameterName = "@month", mytype = SqlDbType.NVarChar, Value = ssphh2.mmonth });
            con.myparameters.Add(new myParameters { ParameterName = "@year", mytype = SqlDbType.NVarChar, Value = ssphh2.yyear });
            DT = con.GetDataTable("sp_GetSSSPH2");

            string x = ConfigurationManager.ConnectionStrings["connectionstring"].ToString();
            List<SSPHH2List> ListReturned = new List<SSPHH2List>();

            //return ListReturned;
            string oldFile = HttpContext.Current.Server.MapPath("~/Files/PayrollOutSourcing/PDFTemplate/") + "SSPHH2" + ".pdf";
            string newFile = HttpContext.Current.Server.MapPath("~/Files/PayrollOutSourcing/PDFTemplate/") + "SSPHH2 v2" + ".pdf";

            // open the reader
            PdfReader reader = new PdfReader(oldFile);
            Rectangle size = reader.GetPageSizeWithRotation(1);
            Document document = new Document(size);

            // open the writer
            FileStream fs = new FileStream(newFile, FileMode.Create, FileAccess.Write);
            PdfWriter writer = PdfWriter.GetInstance(document, fs);
            document.Open();

            // the pdf content
            PdfContentByte cb = writer.DirectContent;

            // select the font properties
            BaseFont bf = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
            cb.SetColorFill(BaseColor.DARK_GRAY);
            cb.SetFontAndSize(bf, 8);

            PdfImportedPage page = writer.GetImportedPage(reader, 1);
            cb.AddTemplate(page, 0, 0);

            int NOOFDATA = DT.Rows.Count;
            Double pages = NOOFDATA / 76;
            //Double finalnoofpages = 1;
            //Double baseno = 1.000;
            int txtheight = 640;
            int txtheight2 = 130;
            int baseno2 = 45;
            //int baseno = 00000;

            cb.BeginText();
            //text = DateTime.Today;
            // put the alignment and coordinates here
            cb.ShowTextAligned(0, DateTime.Now.ToString("MM-dd-yyyy"), 50, 750, 0);
            cb.EndText();

            cb.BeginText();
            //text = DT.Rows[i]["CompanyName"].ToString();
            // put the alignment and coordinates here
            cb.ShowTextAligned(0, DT.Rows[0]["CompanyName"].ToString(), 225, 740, 0);
            cb.EndText();

            cb.BeginText();
            //text = DT.Rows[i]["CompanyName"].ToString();
            // put the alignment and coordinates here
            cb.ShowTextAligned(0, DT.Rows[0]["companycode"].ToString(), 292, 722, 0);
            cb.EndText();

            cb.BeginText();
            //text = DT.Rows[i]["CompanyName"].ToString();
            // put the alignment and coordinates here
            cb.ShowTextAligned(0, DT.Rows[0]["startdate"].ToString(), 310, 710, 0);
            cb.EndText();

            cb.BeginText();
            //text = DT.Rows[i]["CompanyName"].ToString();
            // put the alignment and coordinates here
            cb.ShowTextAligned(0, DT.Rows[0]["enddate"].ToString(), 373, 710, 0);
            cb.EndText();

            int numb1 = DT.Rows.Count - 1;
            var couterr = numb1.ToString();
            int counter = 0;

            string text = "";

            //for (int o = 0; o < DT.Rows.Count; o++)
            //{
            //    if (o == numb1)
            //    {
            //        cb.BeginText();
            //        //text = DT.Rows[i]["Empid"].ToString();
            //        // put the alignment and coordinates here
            //        cb.ShowTextAligned(0, "Totals:" + NOOFDATA, 75, txtheight2, 0);
            //        cb.EndText();
            //    }
            //    else {

            //    }
            //}

            for (int i = 0; i < DT.Rows.Count; i++)
            {
                var counters = i.ToString();
                SSPHH2List Get = new SSPHH2List();
                Get.empid = DT.Rows[i]["Empid"].ToString();
                Get.sss = DT.Rows[i]["SSSnum"].ToString();
                Get.hdmf = DT.Rows[i]["HDMF"].ToString();
                Get.empname = DT.Rows[i]["EmployeeName"].ToString();
                Get.employeephealth = DT.Rows[i]["EmployeePhilHealth"].ToString();
                Get.employerphealth = DT.Rows[i]["EmployerPhilHealth"].ToString();
                Get.totalemployeephealth = DT.Rows[i]["TotalEmployeePhilHealth"].ToString();
                Get.totalemployerphealth = DT.Rows[i]["TotalEmployerPhilHealth"].ToString();
                Get.finaltotal = DT.Rows[i]["FinalTotal"].ToString();
                Get.companycode = DT.Rows[i]["CompanyCode"].ToString();
                Get.companyname = DT.Rows[i]["CompanyName"].ToString();
                Get.startdate = DT.Rows[i]["StartDate"].ToString();
                Get.enddate = DT.Rows[i]["EndDate"].ToString();
                //Get.payoutdate = DT.Rows[i]["PayoutDate"].ToString();
                ListReturned.Add(Get);



                if (i < baseno2)
                {
                    //string text = "";
                    // put the alignment and coordinates here
                    //cb.BeginText();
                    ////text = DateTime.Today;
                    //// put the alignment and coordinates here
                    //cb.ShowTextAligned(0, DateTime.Now.ToString("MM-dd-yyyy"), 50, 750, 0);
                    //cb.EndText();

                    if (counters == couterr)
                    {
                        cb.BeginText();
                        //text = DT.Rows[i]["Empid"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(0, "Totals:" + NOOFDATA, 75, txtheight2, 0);
                        cb.EndText();

                        cb.BeginText();
                        //text = DT.Rows[i]["EmployeePHealth"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(0, DT.Rows[0]["TotalEmployeePhilHealth"].ToString(), 442, txtheight2, 0);
                        cb.EndText();


                        cb.BeginText();
                        //text = DT.Rows[i]["EmployerPHealth"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(0, DT.Rows[0]["TotalEmployerPhilHealth"].ToString(), 495, txtheight2, 0);
                        cb.EndText();

                        cb.BeginText();
                        //text = DT.Rows[i]["EmployeePHealth"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(0, "Total PH:", 442, txtheight2 - 10, 0);
                        cb.EndText();


                        cb.BeginText();
                        //text = DT.Rows[i]["EmployerPHealth"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(0, DT.Rows[0]["FinalTotal"].ToString(), 495, txtheight2 - 10, 0);
                        cb.EndText();

                        cb.BeginText();
                        //text = DT.Rows[i]["Empid"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(0, "Certified Correct :", 75, txtheight2 - 55, 0);
                        cb.EndText();

                        cb.BeginText();
                        //text = DT.Rows[i]["Empid"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(0, "-----------------------------------------------------------------", 75, txtheight2 - 65, 0);
                        cb.EndText();

                        cb.BeginText();
                        //text = DT.Rows[i]["Empid"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(0, "MILDRED GUERRA", 75, txtheight2 - 75, 0);
                        cb.EndText();

                        cb.BeginText();
                        //text = DT.Rows[i]["Empid"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(0, "FINANCE DIRECTOR", 75, txtheight2 - 85, 0);
                        cb.EndText();
                    }


                    cb.BeginText();
                    text = DT.Rows[i]["Empid"].ToString();
                    // put the alignment and coordinates here
                    cb.ShowTextAligned(0, text, 75, txtheight, 0);
                    cb.EndText();

                    cb.BeginText();
                    text = DT.Rows[i]["SSSnum"].ToString();
                    // put the alignment and coordinates here
                    cb.ShowTextAligned(0, text, 132, txtheight, 0);
                    cb.EndText();

                    cb.BeginText();
                    text = DT.Rows[i]["HDMF"].ToString();
                    // put the alignment and coordinates here
                    cb.ShowTextAligned(0, text, 199, txtheight, 0);
                    cb.EndText();

                    cb.BeginText();
                    text = DT.Rows[i]["EmployeeName"].ToString();
                    // put the alignment and coordinates here
                    cb.ShowTextAligned(0, text.Length > 17 ? text.Remove(17) : text, 275, txtheight, 0);
                    cb.EndText();

                    cb.BeginText();
                    text = DT.Rows[i]["EmployeePhilHealth"].ToString();
                    // put the alignment and coordinates here
                    cb.ShowTextAligned(0, text, 442, txtheight, 0);
                    cb.EndText();


                    cb.BeginText();
                    text = DT.Rows[i]["EmployerPhilHealth"].ToString();
                    // put the alignment and coordinates here
                    cb.ShowTextAligned(0, text, 495, txtheight, 0);
                    cb.EndText();

                    if (i % 3 == 0)
                    {
                        int decheight = txtheight - 10;
                        txtheight = decheight;
                    }
                    else
                    {
                        int decheight = txtheight - 12;
                        txtheight = decheight;
                    }


                }
                else if ((i % baseno2) == 0)
                {
                    txtheight = 640;
                    document.NewPage();
                    cb.SetColorFill(BaseColor.DARK_GRAY);
                    cb.SetFontAndSize(bf, 8);
                    //PdfImportedPage page = writer.GetImportedPage(reader, 2);
                    PdfImportedPage page2 = writer.GetImportedPage(reader, 1);
                    cb.AddTemplate(page2, 0, 0);
                    //BaseFont bf = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                    //string text = "";
                    // put the alignment and coordinates here

                    //if (i == numb1)
                    //{
                    //    cb.BeginText();
                    //    //text = DT.Rows[i]["Empid"].ToString();
                    //    // put the alignment and coordinates here
                    //    cb.ShowTextAligned(0, "Totals:" + NOOFDATA, 75, txtheight2, 0);
                    //    cb.EndText();
                    //}

                    if (counters == couterr)
                    {
                        cb.BeginText();
                        //text = DT.Rows[i]["Empid"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(0, "Totals:" + NOOFDATA, 75, txtheight2, 0);
                        cb.EndText();

                        cb.BeginText();
                        //text = DT.Rows[i]["EmployeePHealth"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(0, DT.Rows[0]["TotalEmployeePhilHealth"].ToString(), 442, txtheight2, 0);
                        cb.EndText();


                        cb.BeginText();
                        //text = DT.Rows[i]["EmployerPHealth"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(0, DT.Rows[0]["TotalEmployerPhilHealth"].ToString(), 495, txtheight2, 0);
                        cb.EndText();

                        cb.BeginText();
                        //text = DT.Rows[i]["EmployeePHealth"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(0, "Total PH:", 442, txtheight2 - 10, 0);
                        cb.EndText();


                        cb.BeginText();
                        //text = DT.Rows[i]["EmployerPHealth"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(0, DT.Rows[0]["FinalTotal"].ToString(), 495, txtheight2 - 10, 0);
                        cb.EndText();

                        cb.BeginText();
                        //text = DT.Rows[i]["Empid"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(0, "Certified Correct :", 75, txtheight2 - 55, 0);
                        cb.EndText();

                        cb.BeginText();
                        //text = DT.Rows[i]["Empid"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(0, "-----------------------------------------------------------------", 75, txtheight2 - 65, 0);
                        cb.EndText();

                        cb.BeginText();
                        //text = DT.Rows[i]["Empid"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(0, "MILDRED GUERRA", 75, txtheight2 - 75, 0);
                        cb.EndText();

                        cb.BeginText();
                        //text = DT.Rows[i]["Empid"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(0, "FINANCE DIRECTOR", 75, txtheight2 - 85, 0);
                        cb.EndText();
                    }

                    cb.BeginText();
                    //text = DateTime.Today;
                    // put the alignment and coordinates here
                    cb.ShowTextAligned(0, DateTime.Now.ToString("MM-dd-yyyy"), 50, 750, 0);
                    cb.EndText();

                    cb.BeginText();
                    //text = DT.Rows[i]["CompanyName"].ToString();
                    // put the alignment and coordinates here
                    cb.ShowTextAligned(0, DT.Rows[0]["CompanyName"].ToString(), 225, 740, 0);
                    cb.EndText();

                    cb.BeginText();
                    //text = DT.Rows[i]["CompanyName"].ToString();
                    // put the alignment and coordinates here
                    cb.ShowTextAligned(0, DT.Rows[0]["companycode"].ToString(), 292, 722, 0);
                    cb.EndText();

                    cb.BeginText();
                    //text = DT.Rows[i]["CompanyName"].ToString();
                    // put the alignment and coordinates here
                    cb.ShowTextAligned(0, DT.Rows[0]["startdate"].ToString(), 310, 710, 0);
                    cb.EndText();

                    cb.BeginText();
                    //text = DT.Rows[i]["CompanyName"].ToString();
                    // put the alignment and coordinates here
                    cb.ShowTextAligned(0, DT.Rows[0]["enddate"].ToString(), 373, 710, 0);
                    cb.EndText();

                    cb.BeginText();
                    text = DT.Rows[i]["Empid"].ToString();
                    // put the alignment and coordinates here
                    cb.ShowTextAligned(0, text, 75, txtheight, 0);
                    cb.EndText();

                    cb.BeginText();
                    text = DT.Rows[i]["SSSnum"].ToString();
                    // put the alignment and coordinates here
                    cb.ShowTextAligned(0, text, 132, txtheight, 0);
                    cb.EndText();

                    cb.BeginText();
                    text = DT.Rows[i]["HDMF"].ToString();
                    // put the alignment and coordinates here
                    cb.ShowTextAligned(0, text, 199, txtheight, 0);
                    cb.EndText();

                    cb.BeginText();
                    text = DT.Rows[i]["EmployeeName"].ToString();
                    // put the alignment and coordinates here
                    cb.ShowTextAligned(0, text.Length > 17 ? text.Remove(17) : text, 275, txtheight, 0);
                    cb.EndText();

                    cb.BeginText();
                    text = DT.Rows[i]["EmployeePhilHealth"].ToString();
                    // put the alignment and coordinates here
                    cb.ShowTextAligned(0, text, 442, txtheight, 0);
                    cb.EndText();


                    cb.BeginText();
                    text = DT.Rows[i]["EmployerPhilHealth"].ToString();
                    // put the alignment and coordinates here
                    cb.ShowTextAligned(0, text, 495, txtheight, 0);
                    cb.EndText();


                    if (i % 3 == 0)
                    {
                        int decheight = txtheight - 10;
                        txtheight = decheight;
                    }
                    else
                    {
                        int decheight = txtheight - 12;
                        txtheight = decheight;
                    }
                }
                else
                {


                    //string text = "";
                    // put the alignment and coordinates here

                    //if (i == numb1)
                    //{
                    //    cb.BeginText();
                    //    //text = DT.Rows[i]["Empid"].ToString();
                    //    // put the alignment and coordinates here
                    //    cb.ShowTextAligned(0, "Totals:" + NOOFDATA, 75, txtheight2, 0);
                    //    cb.EndText();
                    //}

                    if (counters == couterr)
                    {
                        cb.BeginText();
                        //text = DT.Rows[i]["Empid"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(0, "Totals:" + NOOFDATA, 75, txtheight2, 0);
                        cb.EndText();

                        cb.BeginText();
                        //text = DT.Rows[i]["EmployeePHealth"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(0, DT.Rows[0]["TotalEmployeePhilHealth"].ToString(), 442, txtheight2, 0);
                        cb.EndText();


                        cb.BeginText();
                        //text = DT.Rows[i]["EmployerPHealth"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(0, DT.Rows[0]["TotalEmployerPhilHealth"].ToString(), 495, txtheight2, 0);
                        cb.EndText();

                        cb.BeginText();
                        //text = DT.Rows[i]["EmployeePHealth"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(0, "Total PH:", 442, txtheight2 - 10, 0);
                        cb.EndText();


                        cb.BeginText();
                        //text = DT.Rows[i]["EmployerPHealth"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(0, DT.Rows[0]["FinalTotal"].ToString(), 495, txtheight2 - 10, 0);
                        cb.EndText();

                        cb.BeginText();
                        //text = DT.Rows[i]["Empid"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(0, "Certified Correct :", 75, txtheight2 - 55, 0);
                        cb.EndText();

                        cb.BeginText();
                        //text = DT.Rows[i]["Empid"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(0, "-----------------------------------------------------------------", 75, txtheight2 - 65, 0);
                        cb.EndText();

                        cb.BeginText();
                        //text = DT.Rows[i]["Empid"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(0, "MILDRED GUERRA", 75, txtheight2 - 75, 0);
                        cb.EndText();

                        cb.BeginText();
                        //text = DT.Rows[i]["Empid"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(0, "FINANCE DIRECTOR", 75, txtheight2 - 85, 0);
                        cb.EndText();
                    }



                    cb.BeginText();
                    text = DT.Rows[i]["Empid"].ToString();
                    // put the alignment and coordinates here
                    cb.ShowTextAligned(0, text, 75, txtheight, 0);
                    cb.EndText();

                    cb.BeginText();
                    text = DT.Rows[i]["SSSnum"].ToString();
                    // put the alignment and coordinates here
                    cb.ShowTextAligned(0, text, 132, txtheight, 0);
                    cb.EndText();

                    cb.BeginText();
                    text = DT.Rows[i]["HDMF"].ToString();
                    // put the alignment and coordinates here
                    cb.ShowTextAligned(0, text, 199, txtheight, 0);
                    cb.EndText();

                    cb.BeginText();
                    text = DT.Rows[i]["EmployeeName"].ToString();
                    // put the alignment and coordinates here
                    cb.ShowTextAligned(0, text.Length > 17 ? text.Remove(17) : text, 275, txtheight, 0);
                    cb.EndText();

                    cb.BeginText();
                    text = DT.Rows[i]["EmployeePhilHealth"].ToString();
                    // put the alignment and coordinates here
                    cb.ShowTextAligned(0, text, 442, txtheight, 0);
                    cb.EndText();


                    cb.BeginText();
                    text = DT.Rows[i]["EmployerPhilHealth"].ToString();
                    // put the alignment and coordinates here
                    cb.ShowTextAligned(0, text, 495, txtheight, 0);
                    cb.EndText();


                    if (i % 3 == 0)
                    {
                        int decheight = txtheight - 10;
                        txtheight = decheight;
                    }
                    else
                    {
                        int decheight = txtheight - 12;
                        txtheight = decheight;
                    }
                    //if (i % NOOFDATA == 0)
                    //{
                    //    cb.BeginText();
                    //    //text = DT.Rows[i]["Empid"].ToString();
                    //    // put the alignment and coordinates here
                    //    cb.ShowTextAligned(0, "Totals:" + NOOFDATA, 75, txtheight2, 0);
                    //    cb.EndText();
                    //}
                    counter = counter + 1;
                }
            }

            //PdfImportedPage page = writer.GetImportedPage(reader, 2);
            //PdfImportedPage page3 = writer.GetImportedPage(reader, 1);
            //cb.AddTemplate(page3, 0, 0);
            //document.NewPage();
            ////BaseFont bf = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
            ////string text = "";
            //// put the alignment and coordinates here
            //cb.SetColorFill(BaseColor.DARK_GRAY);
            //cb.SetFontAndSize(bf, 8);
            //int txtheight2 = 740;

            //cb.BeginText();
            ////text = DT.Rows[i]["Empid"].ToString();
            //// put the alignment and coordinates here
            //cb.ShowTextAligned(0, "Totals:"+ NOOFDATA, 75, txtheight2, 0);
            //cb.EndText();


            //cb.BeginText();
            ////text = DT.Rows[i]["EmployeePHealth"].ToString();
            //// put the alignment and coordinates here
            //cb.ShowTextAligned(0, DT.Rows[0]["TotalEmployeePhilHealth"].ToString(), 442, txtheight2, 0);
            //cb.EndText();


            //cb.BeginText();
            ////text = DT.Rows[i]["EmployerPHealth"].ToString();
            //// put the alignment and coordinates here
            //cb.ShowTextAligned(0, DT.Rows[0]["TotalEmployerPhilHealth"].ToString(), 495, txtheight2, 0);
            //cb.EndText();

            //cb.BeginText();
            ////text = DT.Rows[i]["EmployeePHealth"].ToString();
            //// put the alignment and coordinates here
            //cb.ShowTextAligned(0, "Total PH:", 442, txtheight2 - 10, 0);
            //cb.EndText();


            //cb.BeginText();
            ////text = DT.Rows[i]["EmployerPHealth"].ToString();
            //// put the alignment and coordinates here
            //cb.ShowTextAligned(0, DT.Rows[0]["FinalTotal"].ToString(), 495, txtheight2 - 10, 0);
            //cb.EndText();

            //cb.BeginText();
            ////text = DT.Rows[i]["Empid"].ToString();
            //// put the alignment and coordinates here
            //cb.ShowTextAligned(0, "Certified Correct :", 75, txtheight2 - 55, 0);
            //cb.EndText();

            //cb.BeginText();
            ////text = DT.Rows[i]["Empid"].ToString();
            //// put the alignment and coordinates here
            //cb.ShowTextAligned(0, "-----------------------------------------------------------------", 75, txtheight2 - 65, 0);
            //cb.EndText();

            //cb.BeginText();
            ////text = DT.Rows[i]["Empid"].ToString();
            //// put the alignment and coordinates here
            //cb.ShowTextAligned(0, "MILDRED GUERRA" , 75, txtheight2 - 75, 0);
            //cb.EndText();

            //cb.BeginText();
            ////text = DT.Rows[i]["Empid"].ToString();
            //// put the alignment and coordinates here
            //cb.ShowTextAligned(0, "FINANCE DIRECTOR" , 75, txtheight2 - 85, 0);
            //cb.EndText();

            // close the streams and voilá the file should be changed :)
            document.Close();
            fs.Close();
            writer.Close();
            reader.Close();

            return ListReturned;
        }

        [HttpPost]
        [Route("pwhtaxmgen")]
        public List<PWHTAXMList> PWHTAXList(PWHTAXMList pwhtaxm)
        {
            GetDBILMMain();


            Connection con = new Connection();
            DataTable DT = new DataTable();
            con.myparameters.Add(new myParameters { ParameterName = "@code", mytype = SqlDbType.NVarChar, Value = pwhtaxm.code });
            con.myparameters.Add(new myParameters { ParameterName = "@month", mytype = SqlDbType.NVarChar, Value = pwhtaxm.mmonth });
            con.myparameters.Add(new myParameters { ParameterName = "@year", mytype = SqlDbType.NVarChar, Value = pwhtaxm.yyear });
            DT = con.GetDataTable("sp_GetPWHTAXM");

            string x = ConfigurationManager.ConnectionStrings["connectionstring"].ToString();
            List<PWHTAXMList> ListReturned = new List<PWHTAXMList>();

            //return ListReturned;
            string oldFile = HttpContext.Current.Server.MapPath("~/Files/PayrollOutSourcing/PDFTemplate/") + "WHTAXM" + ".pdf";
            string newFile = HttpContext.Current.Server.MapPath("~/Files/PayrollOutSourcing/PDFTemplate/") + "WHTAXM v2" + ".pdf";

            // open the reader
            PdfReader reader = new PdfReader(oldFile);
            Rectangle size = reader.GetPageSizeWithRotation(1);
            Document document = new Document(size);

            // open the writer
            FileStream fs = new FileStream(newFile, FileMode.Create, FileAccess.Write);
            PdfWriter writer = PdfWriter.GetInstance(document, fs);
            document.Open();

            // the pdf content
            PdfContentByte cb = writer.DirectContent;

            // select the font properties
            BaseFont bf = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
            cb.SetColorFill(BaseColor.DARK_GRAY);
            cb.SetFontAndSize(bf, 8);

            PdfImportedPage page = writer.GetImportedPage(reader, 1);
            cb.AddTemplate(page, 0, 0);


            cb.BeginText();
            //text = DateTime.Today;
            // put the alignment and coordinates here
            cb.ShowTextAligned(0, DateTime.Now.ToString("MM-dd-yyyy"), 50, 750, 0);
            cb.EndText();

            cb.BeginText();
            //text = DT.Rows[i]["CompanyName"].ToString();
            // put the alignment and coordinates here
            cb.ShowTextAligned(0, DT.Rows[0]["CompanyName"].ToString(), 225, 748, 0);
            cb.EndText();

            cb.BeginText();
            //text = DT.Rows[i]["CompanyName"].ToString();
            // put the alignment and coordinates here
            cb.ShowTextAligned(0, DT.Rows[0]["startdate"].ToString() + " " + "TO", 345, 725, 0);
            cb.EndText();

            cb.BeginText();
            //text = DT.Rows[i]["CompanyName"].ToString();
            // put the alignment and coordinates here
            cb.ShowTextAligned(0, DT.Rows[0]["enddate"].ToString(), 403, 725, 0);
            cb.EndText();

            int NOOFDATA = DT.Rows.Count;
            Double pages = NOOFDATA / 76;
            //Double finalnoofpages = 1;
            //Double baseno = 1.000;
            int txtheight = 668;
            int txtheight2 = 100;
            int baseno2 = 43;
            //int baseno = 00000;

            int numb1 = DT.Rows.Count - 1;
            var couterr = numb1.ToString();

            string text = "";

            for (int i = 0; i < NOOFDATA; i++)
            {
                var counters = i.ToString();
                PWHTAXMList Get = new PWHTAXMList();
                Get.empid = DT.Rows[i]["Empid"].ToString();
                Get.empname = DT.Rows[i]["EmployeeName"].ToString();
                Get.companycode = DT.Rows[i]["CompanyCode"].ToString();
                Get.companyname = DT.Rows[i]["CompanyName"].ToString();
                Get.taxstatus = DT.Rows[i]["TaxStatus"].ToString();
                Get.tinnum = DT.Rows[i]["TINnum"].ToString();
                Get.whtax = DT.Rows[i]["WHTax"].ToString();
                Get.startdate = DT.Rows[i]["StartDate"].ToString();
                Get.enddate = DT.Rows[i]["EndDate"].ToString();
                Get.finaltotal = DT.Rows[i]["FinalTotal"].ToString();
                //Get.payoutdate = DT.Rows[i]["PayoutDate"].ToString();
                ListReturned.Add(Get);


                if (i < baseno2)
                {
                    //string text = "";
                    // put the alignment and coordinates here
                    if (counters == couterr)
                    {
                        cb.BeginText();
                        //text = DT.Rows[i]["Empid"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(0, "Totals:", 75, txtheight2, 0);
                        cb.EndText();

                        cb.BeginText();
                        //text = DT.Rows[i]["Empid"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(0, "Totals:", 132, txtheight2, 0);
                        cb.EndText();
                        cb.BeginText();
                        //text = DT.Rows[i]["Empid"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(0, "Totals:", 132, txtheight2, 0);
                        cb.EndText();

                        cb.BeginText();
                        //text = DT.Rows[i]["EmployeePHealth"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(0, "" + DT.Rows.Count, 360, txtheight2, 0);
                        cb.EndText();


                        cb.BeginText();
                        //text = DT.Rows[i]["EmployerPHealth"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(0, DT.Rows[0]["FinalTotal"].ToString(), 458, txtheight2, 0);
                        cb.EndText();


                    }

                    cb.BeginText();
                    text = DT.Rows[i]["Empid"].ToString();
                    // put the alignment and coordinates here
                    cb.ShowTextAligned(0, text, 75, txtheight, 0);
                    cb.EndText();

                    cb.BeginText();
                    text = DT.Rows[i]["EmployeeName"].ToString();
                    // put the alignment and coordinates here
                    cb.ShowTextAligned(0, text.Length > 17 ? text.Remove(17) : text, 132, txtheight, 0);
                    cb.EndText();

                    cb.BeginText();
                    text = DT.Rows[i]["TaxStatus"].ToString();
                    // put the alignment and coordinates here
                    cb.ShowTextAligned(0, text, 300, txtheight, 0);
                    cb.EndText();

                    cb.BeginText();
                    text = DT.Rows[i]["TINnum"].ToString();
                    // put the alignment and coordinates here
                    cb.ShowTextAligned(0, text, 365, txtheight, 0);
                    cb.EndText();

                    cb.BeginText();
                    text = DT.Rows[i]["WHTax"].ToString();
                    // put the alignment and coordinates here
                    cb.ShowTextAligned(0, text, 458, txtheight, 0);
                    cb.EndText();


                    if (i % 3 == 0)
                    {
                        int decheight = txtheight - 13;
                        txtheight = decheight;
                    }
                    else if (i % 5 == 0)
                    {
                        int decheight = txtheight - 14;
                        txtheight = decheight;
                    }
                    else
                    {
                        int decheight = txtheight - 13;
                        txtheight = decheight;
                    }

                }
                else if ((i % baseno2) == 0)
                {
                    txtheight = 668;
                    document.NewPage();
                    cb.SetColorFill(BaseColor.DARK_GRAY);
                    cb.SetFontAndSize(bf, 8);
                    //PdfImportedPage page = writer.GetImportedPage(reader, 2);
                    PdfImportedPage page2 = writer.GetImportedPage(reader, 1);
                    cb.AddTemplate(page2, 0, 0);
                    //BaseFont bf = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                    //string text = "";
                    // put the alignment and coordinates here

                    if (counters == couterr)
                    {
                        cb.BeginText();
                        //text = DT.Rows[i]["Empid"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(0, "Totals:", 75, txtheight2, 0);
                        cb.EndText();

                        cb.BeginText();
                        //text = DT.Rows[i]["Empid"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(0, "Totals:", 132, txtheight2, 0);
                        cb.EndText();
                        cb.BeginText();
                        //text = DT.Rows[i]["Empid"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(0, "Totals:", 132, txtheight2, 0);
                        cb.EndText();

                        cb.BeginText();
                        //text = DT.Rows[i]["EmployeePHealth"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(0, "" + DT.Rows.Count, 360, txtheight2, 0);
                        cb.EndText();


                        cb.BeginText();
                        //text = DT.Rows[i]["EmployerPHealth"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(0, DT.Rows[0]["FinalTotal"].ToString(), 458, txtheight2, 0);
                        cb.EndText();


                    }

                    cb.BeginText();
                    //text = DateTime.Today;
                    // put the alignment and coordinates here
                    cb.ShowTextAligned(0, DateTime.Now.ToString("MM-dd-yyyy"), 50, 750, 0);
                    cb.EndText();

                    cb.BeginText();
                    //text = DT.Rows[i]["CompanyName"].ToString();
                    // put the alignment and coordinates here
                    cb.ShowTextAligned(0, DT.Rows[0]["CompanyName"].ToString(), 225, 748, 0);
                    cb.EndText();

                    cb.BeginText();
                    //text = DT.Rows[i]["CompanyName"].ToString();
                    // put the alignment and coordinates here
                    cb.ShowTextAligned(0, DT.Rows[0]["startdate"].ToString() + " " + "TO", 345, 725, 0);
                    cb.EndText();

                    cb.BeginText();
                    //text = DT.Rows[i]["CompanyName"].ToString();
                    // put the alignment and coordinates here
                    cb.ShowTextAligned(0, DT.Rows[0]["enddate"].ToString(), 403, 725, 0);
                    cb.EndText();

                    cb.BeginText();
                    text = DT.Rows[i]["Empid"].ToString();
                    // put the alignment and coordinates here
                    cb.ShowTextAligned(0, text, 75, txtheight, 0);
                    cb.EndText();

                    cb.BeginText();
                    text = DT.Rows[i]["EmployeeName"].ToString();
                    // put the alignment and coordinates here
                    cb.ShowTextAligned(0, text.Length > 17 ? text.Remove(17) : text, 132, txtheight, 0);
                    cb.EndText();

                    cb.BeginText();
                    text = DT.Rows[i]["TaxStatus"].ToString();
                    // put the alignment and coordinates here
                    cb.ShowTextAligned(0, text, 300, txtheight, 0);
                    cb.EndText();

                    cb.BeginText();
                    text = DT.Rows[i]["TINnum"].ToString();
                    // put the alignment and coordinates here
                    cb.ShowTextAligned(0, text, 365, txtheight, 0);
                    cb.EndText();

                    cb.BeginText();
                    text = DT.Rows[i]["WHTax"].ToString();
                    // put the alignment and coordinates here
                    cb.ShowTextAligned(0, text, 458, txtheight, 0);
                    cb.EndText();


                    if (i % 3 == 0)
                    {
                        int decheight = txtheight - 13;
                        txtheight = decheight;
                    }
                    else if (i % 5 == 0)
                    {
                        int decheight = txtheight - 14;
                        txtheight = decheight;
                    }
                    else
                    {
                        int decheight = txtheight - 13;
                        txtheight = decheight;
                    }
                }
                else
                {


                    //string text = "";
                    // put the alignment and coordinates here

                    if (counters == couterr)
                    {
                        cb.BeginText();
                        //text = DT.Rows[i]["Empid"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(0, "Totals:", 75, txtheight2, 0);
                        cb.EndText();

                        cb.BeginText();
                        //text = DT.Rows[i]["Empid"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(0, "Totals:", 132, txtheight2, 0);
                        cb.EndText();
                        cb.BeginText();
                        //text = DT.Rows[i]["Empid"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(0, "Totals:", 132, txtheight2, 0);
                        cb.EndText();

                        cb.BeginText();
                        //text = DT.Rows[i]["EmployeePHealth"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(0, "" + DT.Rows.Count, 360, txtheight2, 0);
                        cb.EndText();


                        cb.BeginText();
                        //text = DT.Rows[i]["EmployerPHealth"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(0, DT.Rows[0]["FinalTotal"].ToString(), 458, txtheight2, 0);
                        cb.EndText();


                    }

                    cb.BeginText();
                    text = DT.Rows[i]["Empid"].ToString();
                    // put the alignment and coordinates here
                    cb.ShowTextAligned(0, text, 75, txtheight, 0);
                    cb.EndText();

                    cb.BeginText();
                    text = DT.Rows[i]["EmployeeName"].ToString();
                    // put the alignment and coordinates here
                    cb.ShowTextAligned(0, text.Length > 17 ? text.Remove(17) : text, 132, txtheight, 0);
                    cb.EndText();

                    cb.BeginText();
                    text = DT.Rows[i]["TaxStatus"].ToString();
                    // put the alignment and coordinates here
                    cb.ShowTextAligned(0, text, 300, txtheight, 0);
                    cb.EndText();

                    cb.BeginText();
                    text = DT.Rows[i]["TINnum"].ToString();
                    // put the alignment and coordinates here
                    cb.ShowTextAligned(0, text, 365, txtheight, 0);
                    cb.EndText();

                    cb.BeginText();
                    text = DT.Rows[i]["WHTax"].ToString();
                    // put the alignment and coordinates here
                    cb.ShowTextAligned(0, text, 458, txtheight, 0);
                    cb.EndText();


                    if (i % 3 == 0)
                    {
                        int decheight = txtheight - 13;
                        txtheight = decheight;
                    }
                    else if (i % 5 == 0)
                    {
                        int decheight = txtheight - 14;
                        txtheight = decheight;
                    }
                    else
                    {
                        int decheight = txtheight - 13;
                        txtheight = decheight;
                    }



                }
            }




            // close the streams and voilá the file should be changed :)
            document.Close();
            fs.Close();
            writer.Close();
            reader.Close();

            return ListReturned;
        }
        [HttpPost]
        [Route("ssphhdgen")]
        public List<SSPHHDList> SSPHHDList(SSPHHDList ssphhd)
        {
            GetDBILMMain();


            Connection con = new Connection();
            DataTable DT = new DataTable();
            con.myparameters.Add(new myParameters { ParameterName = "@code", mytype = SqlDbType.NVarChar, Value = ssphhd.code });
            con.myparameters.Add(new myParameters { ParameterName = "@month", mytype = SqlDbType.NVarChar, Value = ssphhd.mmonth });
            con.myparameters.Add(new myParameters { ParameterName = "@year", mytype = SqlDbType.NVarChar, Value = ssphhd.yyear });
            DT = con.GetDataTable("sp_GetSSSPHD");

            string x = ConfigurationManager.ConnectionStrings["connectionstring"].ToString();
            List<SSPHHDList> ListReturned = new List<SSPHHDList>();

            //return ListReturned;
            string oldFile = HttpContext.Current.Server.MapPath("~/Files/PayrollOutSourcing/PDFTemplate/") + "SSPHHD" + ".pdf";
            string newFile = HttpContext.Current.Server.MapPath("~/Files/PayrollOutSourcing/PDFTemplate/") + "SSPHHD v2" + ".pdf";

            // open the reader
            PdfReader reader = new PdfReader(oldFile);
            Rectangle size = reader.GetPageSizeWithRotation(1);
            Document document = new Document(size);

            // open the writer
            FileStream fs = new FileStream(newFile, FileMode.Create, FileAccess.Write);
            PdfWriter writer = PdfWriter.GetInstance(document, fs);
            document.Open();

            // the pdf content
            PdfContentByte cb = writer.DirectContent;

            // select the font properties
            BaseFont bf = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
            cb.SetColorFill(BaseColor.DARK_GRAY);
            cb.SetFontAndSize(bf, 8);

            PdfImportedPage page = writer.GetImportedPage(reader, 1);
            cb.AddTemplate(page, 0, 0);


            cb.BeginText();
            //text = DateTime.Today;
            // put the alignment and coordinates here
            cb.ShowTextAligned(0, DateTime.Now.ToString("MM-dd-yyyy"), 30, 565, 0);
            cb.EndText();

            cb.BeginText();
            //text = DT.Rows[i]["CompanyName"].ToString();
            // put the alignment and coordinates here
            cb.ShowTextAligned(Element.ALIGN_CENTER, DT.Rows[0]["CompanyName"].ToString(), 380, 565, 0);
            cb.EndText();

            cb.BeginText();
            //text = DT.Rows[i]["CompanyName"].ToString();
            // put the alignment and coordinates here
            cb.ShowTextAligned(0, DT.Rows[0]["CompanyCode"].ToString(), 377, 543, 0);
            cb.EndText();

            cb.BeginText();
            //text = DT.Rows[i]["CompanyName"].ToString();
            // put the alignment and coordinates here
            cb.ShowTextAligned(0, DT.Rows[0]["startdate"].ToString() + " " + "TO", 403, 532, 0);
            cb.EndText();

            cb.BeginText();
            //text = DT.Rows[i]["CompanyName"].ToString();
            // put the alignment and coordinates here
            cb.ShowTextAligned(0, DT.Rows[0]["enddate"].ToString(), 460, 532, 0);
            cb.EndText();

            int NOOFDATA = DT.Rows.Count;
            Double pages = NOOFDATA / 76;
            int txtheight = 460;
            int txtheight2 = 60;
            int baseno2 = 35;

            int numb1 = DT.Rows.Count - 1;
            var couterr = numb1.ToString();

            string text = "";

            for (int i = 0; i < NOOFDATA; i++)
            {
                var counters = i.ToString();

                SSPHHDList Get = new SSPHHDList();

                Get.Empid = DT.Rows[i]["Empid"].ToString();
                Get.SSSnum = DT.Rows[i]["SSSnum"].ToString();
                Get.HDMF = DT.Rows[i]["HDMF"].ToString();
                Get.EmployeeName = DT.Rows[i]["EmployeeName"].ToString();
                Get.SSSPremEmployee = DT.Rows[i]["SSSPremEmployee"].ToString();
                Get.SSSPremEcEmployer = DT.Rows[i]["SSSPremEcEmployer"].ToString();
                Get.EmployerEC = DT.Rows[i]["EmployerEC"].ToString();
                Get.EmployeePhilHealth = DT.Rows[i]["EmployeePhilHealth"].ToString();
                Get.EmployerPhilHealth = DT.Rows[i]["EmployerPhilHealth"].ToString();
                Get.PagIbigAmount = DT.Rows[i]["PagIbigAmount"].ToString();
                Get.PagibigERAmount = DT.Rows[i]["PagibigERAmount"].ToString();
                Get.TotalEmployeeSSS = DT.Rows[i]["TotalEmployeeSSS"].ToString();
                Get.TotalEmployerSSS = DT.Rows[i]["TotalEmployerSSS"].ToString();
                Get.TotalEmployerECSSS = DT.Rows[i]["TotalEmployerECSSS"].ToString();
                Get.FinalTotalSSS = DT.Rows[i]["FinalTotalSSS"].ToString();
                Get.TotalEmployeePhilHealth = DT.Rows[i]["TotalEmployeePhilHealth"].ToString();
                Get.TotalEmployerPhilHealth = DT.Rows[i]["TotalEmployerPhilHealth"].ToString();
                Get.FinalTotalPhilHealth = DT.Rows[i]["FinalTotalPhilHealth"].ToString();
                Get.TotalPagIbigAmount = DT.Rows[i]["TotalPagIbigAmount"].ToString();
                Get.TotalPagibigERAmount = DT.Rows[i]["TotalPagibigERAmount"].ToString();
                Get.FinalTotalPagibig = DT.Rows[i]["FinalTotalPagibig"].ToString();
                Get.StartDate = DT.Rows[i]["StartDate"].ToString();
                Get.EndDate = DT.Rows[i]["EndDate"].ToString();
                Get.CompanyCode = DT.Rows[i]["CompanyCode"].ToString();
                Get.CompanyName = DT.Rows[i]["CompanyName"].ToString();

                ListReturned.Add(Get);

                if (i < baseno2)
                {
                    //string text = "";
                    // put the alignment and coordinates here
                    if (counters == couterr)
                    {
                        cb.BeginText();
                        //text = DT.Rows[i]["Empid"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(0, "Totals:", 30, txtheight2, 0);
                        cb.EndText();

                        cb.BeginText();
                        //text = DT.Rows[i]["Empid"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(0, "" + DT.Rows.Count, 92, txtheight2, 0);
                        cb.EndText();


                        cb.BeginText();
                        //text = DT.Rows[i]["EmployeePHealth"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(Element.ALIGN_RIGHT, DT.Rows[i]["TotalEmployeeSSS"].ToString(), 447, txtheight2, 0);
                        cb.EndText();

                        cb.BeginText();
                        //text = DT.Rows[i]["EmployeePHealth"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(Element.ALIGN_RIGHT, DT.Rows[i]["TotalEmployerSSS"].ToString(), 507, txtheight2, 0);
                        cb.EndText();

                        cb.BeginText();
                        //text = DT.Rows[i]["EmployeePHealth"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(Element.ALIGN_RIGHT, DT.Rows[i]["TotalEmployerECSSS"].ToString(), 557, txtheight2, 0);
                        cb.EndText();

                        cb.BeginText();
                        //text = DT.Rows[i]["EmployeePHealth"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(Element.ALIGN_RIGHT, "Total SSS:", 507, txtheight2 - 10, 0);
                        cb.EndText();

                        cb.BeginText();
                        //text = DT.Rows[i]["EmployeePHealth"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(Element.ALIGN_RIGHT, DT.Rows[i]["FinalTotalSSS"].ToString(), 557, txtheight2 - 10, 0);
                        cb.EndText();

                        cb.BeginText();
                        //text = DT.Rows[i]["EmployeePHealth"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(Element.ALIGN_RIGHT, DT.Rows[i]["TotalEmployeePhilHealth"].ToString(), 608, txtheight2, 0);
                        cb.EndText();

                        cb.BeginText();
                        //text = DT.Rows[i]["EmployeePHealth"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(Element.ALIGN_RIGHT, DT.Rows[i]["TotalEmployerPhilHealth"].ToString(), 660, txtheight2, 0);
                        cb.EndText();

                        cb.BeginText();
                        //text = DT.Rows[i]["EmployeePHealth"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(Element.ALIGN_RIGHT, "Total PH:", 608, txtheight2 - 10, 0);
                        cb.EndText();

                        cb.BeginText();
                        //text = DT.Rows[i]["EmployeePHealth"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(Element.ALIGN_RIGHT, DT.Rows[i]["FinalTotalPhilHealth"].ToString(), 660, txtheight2 - 10, 0);
                        cb.EndText();


                        cb.BeginText();
                        //text = DT.Rows[i]["EmployeePHealth"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(Element.ALIGN_RIGHT, DT.Rows[i]["TotalPagIbigAmount"].ToString(), 712, txtheight2, 0);
                        cb.EndText();

                        cb.BeginText();
                        //text = DT.Rows[i]["EmployeePHealth"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(Element.ALIGN_RIGHT, DT.Rows[i]["TotalPagibigERAmount"].ToString(), 761, txtheight2, 0);
                        cb.EndText();

                        cb.BeginText();
                        //text = DT.Rows[i]["EmployeePHealth"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(Element.ALIGN_RIGHT, "Total HDMF:", 712, txtheight2 - 10, 0);
                        cb.EndText();

                        cb.BeginText();
                        //text = DT.Rows[i]["EmployeePHealth"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(Element.ALIGN_RIGHT, DT.Rows[i]["FinalTotalPagibig"].ToString(), 761, txtheight2 - 10, 0);
                        cb.EndText();




                    }

                    cb.BeginText();
                    text = DT.Rows[i]["Empid"].ToString();
                    // put the alignment and coordinates here
                    cb.ShowTextAligned(0, text, 30, txtheight, 0);
                    cb.EndText();

                    cb.BeginText();
                    text = DT.Rows[i]["SSSnum"].ToString();
                    // put the alignment and coordinates here
                    cb.ShowTextAligned(0, text, 92, txtheight, 0);
                    cb.EndText();

                    cb.BeginText();
                    text = DT.Rows[i]["HDMF"].ToString();
                    // put the alignment and coordinates here
                    cb.ShowTextAligned(0, text, 156, txtheight, 0);
                    cb.EndText();

                    cb.BeginText();
                    text = DT.Rows[i]["EmployeeName"].ToString();
                    // put the alignment and coordinates here
                    cb.ShowTextAligned(0, text.Length > 17 ? text.Remove(17) : text, 233, txtheight, 0);
                    cb.EndText();

                    cb.BeginText();
                    text = DT.Rows[i]["SSSPremEmployee"].ToString();
                    // put the alignment and coordinates here
                    cb.ShowTextAligned(Element.ALIGN_RIGHT, text, 447, txtheight, 0);
                    cb.EndText();

                    cb.BeginText();
                    text = DT.Rows[i]["SSSPremEcEmployer"].ToString();
                    // put the alignment and coordinates here
                    cb.ShowTextAligned(Element.ALIGN_RIGHT, text, 507, txtheight, 0);
                    cb.EndText();

                    cb.BeginText();
                    text = DT.Rows[i]["EmployerEC"].ToString();
                    // put the alignment and coordinates here
                    cb.ShowTextAligned(Element.ALIGN_RIGHT, text, 557, txtheight, 0);
                    cb.EndText();

                    cb.BeginText();
                    text = DT.Rows[i]["EmployeePhilHealth"].ToString();
                    // put the alignment and coordinates here
                    cb.ShowTextAligned(Element.ALIGN_RIGHT, text, 608, txtheight, 0);
                    cb.EndText();

                    cb.BeginText();
                    text = DT.Rows[i]["EmployerPhilHealth"].ToString();
                    // put the alignment and coordinates here
                    cb.ShowTextAligned(Element.ALIGN_RIGHT, text, 660, txtheight, 0);
                    cb.EndText();

                    cb.BeginText();
                    text = DT.Rows[i]["PagIbigAmount"].ToString();
                    // put the alignment and coordinates here
                    cb.ShowTextAligned(Element.ALIGN_RIGHT, text, 712, txtheight, 0);
                    cb.EndText();

                    cb.BeginText();
                    text = DT.Rows[i]["PagibigERAmount"].ToString();
                    // put the alignment and coordinates here
                    cb.ShowTextAligned(Element.ALIGN_RIGHT, text, 761, txtheight, 0);
                    cb.EndText();




                    if (i % 4 == 0)
                    {
                        int decheight = txtheight - 11;
                        txtheight = decheight;
                    }
                    else if (i % 5 == 0)
                    {
                        int decheight = txtheight - 10;
                        txtheight = decheight;
                    }
                    else if (i % 7 == 0)
                    {
                        int decheight = txtheight - 11;
                        txtheight = decheight;
                    }
                    else
                    {
                        int decheight = txtheight - 12;
                        txtheight = decheight;
                    }

                }
                else if ((i % baseno2) == 0)
                {
                    txtheight = 460;
                    document.NewPage();
                    cb.SetColorFill(BaseColor.DARK_GRAY);
                    cb.SetFontAndSize(bf, 8);
                    //PdfImportedPage page = writer.GetImportedPage(reader, 2);
                    PdfImportedPage page2 = writer.GetImportedPage(reader, 1);
                    cb.AddTemplate(page2, 0, 0);
                    //BaseFont bf = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                    //string text = "";
                    // put the alignment and coordinates here

                    if (counters == couterr)
                    {
                        cb.BeginText();
                        //text = DT.Rows[i]["Empid"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(0, "Totals:", 30, txtheight2, 0);
                        cb.EndText();

                        cb.BeginText();
                        //text = DT.Rows[i]["Empid"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(0, "" + DT.Rows.Count, 92, txtheight2, 0);
                        cb.EndText();


                        cb.BeginText();
                        //text = DT.Rows[i]["EmployeePHealth"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(Element.ALIGN_RIGHT, DT.Rows[i]["TotalEmployeeSSS"].ToString(), 447, txtheight2, 0);
                        cb.EndText();

                        cb.BeginText();
                        //text = DT.Rows[i]["EmployeePHealth"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(Element.ALIGN_RIGHT, DT.Rows[i]["TotalEmployerSSS"].ToString(), 507, txtheight2, 0);
                        cb.EndText();

                        cb.BeginText();
                        //text = DT.Rows[i]["EmployeePHealth"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(Element.ALIGN_RIGHT, DT.Rows[i]["TotalEmployerECSSS"].ToString(), 557, txtheight2, 0);
                        cb.EndText();

                        cb.BeginText();
                        //text = DT.Rows[i]["EmployeePHealth"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(Element.ALIGN_RIGHT, "Total SSS:", 507, txtheight2 - 10, 0);
                        cb.EndText();

                        cb.BeginText();
                        //text = DT.Rows[i]["EmployeePHealth"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(Element.ALIGN_RIGHT, DT.Rows[i]["FinalTotalSSS"].ToString(), 557, txtheight2 - 10, 0);
                        cb.EndText();

                        cb.BeginText();
                        //text = DT.Rows[i]["EmployeePHealth"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(Element.ALIGN_RIGHT, DT.Rows[i]["TotalEmployeePhilHealth"].ToString(), 608, txtheight2, 0);
                        cb.EndText();

                        cb.BeginText();
                        //text = DT.Rows[i]["EmployeePHealth"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(Element.ALIGN_RIGHT, DT.Rows[i]["TotalEmployerPhilHealth"].ToString(), 660, txtheight2, 0);
                        cb.EndText();

                        cb.BeginText();
                        //text = DT.Rows[i]["EmployeePHealth"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(Element.ALIGN_RIGHT, "Total PH:", 608, txtheight2 - 10, 0);
                        cb.EndText();

                        cb.BeginText();
                        //text = DT.Rows[i]["EmployeePHealth"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(Element.ALIGN_RIGHT, DT.Rows[i]["FinalTotalPhilHealth"].ToString(), 660, txtheight2 - 10, 0);
                        cb.EndText();


                        cb.BeginText();
                        //text = DT.Rows[i]["EmployeePHealth"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(Element.ALIGN_RIGHT, DT.Rows[i]["TotalPagIbigAmount"].ToString(), 712, txtheight2, 0);
                        cb.EndText();

                        cb.BeginText();
                        //text = DT.Rows[i]["EmployeePHealth"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(Element.ALIGN_RIGHT, DT.Rows[i]["TotalPagibigERAmount"].ToString(), 761, txtheight2, 0);
                        cb.EndText();

                        cb.BeginText();
                        //text = DT.Rows[i]["EmployeePHealth"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(Element.ALIGN_RIGHT, "Total HDMF:", 712, txtheight2 - 10, 0);
                        cb.EndText();

                        cb.BeginText();
                        //text = DT.Rows[i]["EmployeePHealth"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(Element.ALIGN_RIGHT, DT.Rows[i]["FinalTotalPagibig"].ToString(), 761, txtheight2 - 10, 0);
                        cb.EndText();





                    }

                    cb.BeginText();
                    //text = DateTime.Today;
                    // put the alignment and coordinates here
                    cb.ShowTextAligned(0, DateTime.Now.ToString("MM-dd-yyyy"), 30, 565, 0);
                    cb.EndText();

                    cb.BeginText();
                    //text = DT.Rows[i]["CompanyName"].ToString();
                    // put the alignment and coordinates here
                    cb.ShowTextAligned(Element.ALIGN_CENTER, DT.Rows[0]["CompanyName"].ToString(), 380, 565, 0);
                    cb.EndText();

                    cb.BeginText();
                    //text = DT.Rows[i]["CompanyName"].ToString();
                    // put the alignment and coordinates here
                    cb.ShowTextAligned(0, DT.Rows[0]["CompanyCode"].ToString(), 377, 543, 0);
                    cb.EndText();

                    cb.BeginText();
                    //text = DT.Rows[i]["CompanyName"].ToString();
                    // put the alignment and coordinates here
                    cb.ShowTextAligned(0, DT.Rows[0]["startdate"].ToString() + " " + "TO", 403, 532, 0);
                    cb.EndText();

                    cb.BeginText();
                    //text = DT.Rows[i]["CompanyName"].ToString();
                    // put the alignment and coordinates here
                    cb.ShowTextAligned(0, DT.Rows[0]["enddate"].ToString(), 460, 532, 0);
                    cb.EndText();

                    cb.BeginText();
                    text = DT.Rows[i]["Empid"].ToString();
                    // put the alignment and coordinates here
                    cb.ShowTextAligned(0, text, 30, txtheight, 0);
                    cb.EndText();

                    cb.BeginText();
                    text = DT.Rows[i]["SSSnum"].ToString();
                    // put the alignment and coordinates here
                    cb.ShowTextAligned(0, text, 92, txtheight, 0);
                    cb.EndText();

                    cb.BeginText();
                    text = DT.Rows[i]["HDMF"].ToString();
                    // put the alignment and coordinates here
                    cb.ShowTextAligned(0, text, 156, txtheight, 0);
                    cb.EndText();

                    cb.BeginText();
                    text = DT.Rows[i]["EmployeeName"].ToString();
                    // put the alignment and coordinates here
                    cb.ShowTextAligned(0, text.Length > 17 ? text.Remove(17) : text, 233, txtheight, 0);
                    cb.EndText();

                    cb.BeginText();
                    text = DT.Rows[i]["SSSPremEmployee"].ToString();
                    // put the alignment and coordinates here
                    cb.ShowTextAligned(Element.ALIGN_RIGHT, text, 447, txtheight, 0);
                    cb.EndText();

                    cb.BeginText();
                    text = DT.Rows[i]["SSSPremEcEmployer"].ToString();
                    // put the alignment and coordinates here
                    cb.ShowTextAligned(Element.ALIGN_RIGHT, text, 507, txtheight, 0);
                    cb.EndText();

                    cb.BeginText();
                    text = DT.Rows[i]["EmployerEC"].ToString();
                    // put the alignment and coordinates here
                    cb.ShowTextAligned(Element.ALIGN_RIGHT, text, 557, txtheight, 0);
                    cb.EndText();

                    cb.BeginText();
                    text = DT.Rows[i]["EmployeePhilHealth"].ToString();
                    // put the alignment and coordinates here
                    cb.ShowTextAligned(Element.ALIGN_RIGHT, text, 608, txtheight, 0);
                    cb.EndText();

                    cb.BeginText();
                    text = DT.Rows[i]["EmployerPhilHealth"].ToString();
                    // put the alignment and coordinates here
                    cb.ShowTextAligned(Element.ALIGN_RIGHT, text, 660, txtheight, 0);
                    cb.EndText();

                    cb.BeginText();
                    text = DT.Rows[i]["PagIbigAmount"].ToString();
                    // put the alignment and coordinates here
                    cb.ShowTextAligned(Element.ALIGN_RIGHT, text, 712, txtheight, 0);
                    cb.EndText();

                    cb.BeginText();
                    text = DT.Rows[i]["PagibigERAmount"].ToString();
                    // put the alignment and coordinates here
                    cb.ShowTextAligned(Element.ALIGN_RIGHT, text, 761, txtheight, 0);
                    cb.EndText();


                    if (i % 4 == 0)
                    {
                        int decheight = txtheight - 11;
                        txtheight = decheight;
                    }
                    else if (i % 5 == 0)
                    {
                        int decheight = txtheight - 10;
                        txtheight = decheight;
                    }
                    else if (i % 7 == 0)
                    {
                        int decheight = txtheight - 11;
                        txtheight = decheight;
                    }
                    else
                    {
                        int decheight = txtheight - 12;
                        txtheight = decheight;
                    }
                }
                else
                {


                    //string text = "";
                    // put the alignment and coordinates here

                    if (counters == couterr)
                    {
                        cb.BeginText();
                        //text = DT.Rows[i]["Empid"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(0, "Totals:", 30, txtheight2, 0);
                        cb.EndText();

                        cb.BeginText();
                        //text = DT.Rows[i]["Empid"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(0, "" + DT.Rows.Count, 92, txtheight2, 0);
                        cb.EndText();


                        cb.BeginText();
                        //text = DT.Rows[i]["EmployeePHealth"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(Element.ALIGN_RIGHT, DT.Rows[i]["TotalEmployeeSSS"].ToString(), 447, txtheight2, 0);
                        cb.EndText();

                        cb.BeginText();
                        //text = DT.Rows[i]["EmployeePHealth"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(Element.ALIGN_RIGHT, DT.Rows[i]["TotalEmployerSSS"].ToString(), 507, txtheight2, 0);
                        cb.EndText();

                        cb.BeginText();
                        //text = DT.Rows[i]["EmployeePHealth"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(Element.ALIGN_RIGHT, DT.Rows[i]["TotalEmployerECSSS"].ToString(), 557, txtheight2, 0);
                        cb.EndText();

                        cb.BeginText();
                        //text = DT.Rows[i]["EmployeePHealth"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(Element.ALIGN_RIGHT, "Total SSS:", 507, txtheight2 - 10, 0);
                        cb.EndText();

                        cb.BeginText();
                        //text = DT.Rows[i]["EmployeePHealth"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(Element.ALIGN_RIGHT, DT.Rows[i]["FinalTotalSSS"].ToString(), 557, txtheight2 - 10, 0);
                        cb.EndText();

                        cb.BeginText();
                        //text = DT.Rows[i]["EmployeePHealth"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(Element.ALIGN_RIGHT, DT.Rows[i]["TotalEmployeePhilHealth"].ToString(), 608, txtheight2, 0);
                        cb.EndText();

                        cb.BeginText();
                        //text = DT.Rows[i]["EmployeePHealth"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(Element.ALIGN_RIGHT, DT.Rows[i]["TotalEmployerPhilHealth"].ToString(), 660, txtheight2, 0);
                        cb.EndText();

                        cb.BeginText();
                        //text = DT.Rows[i]["EmployeePHealth"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(Element.ALIGN_RIGHT, "Total PH:", 608, txtheight2 - 10, 0);
                        cb.EndText();

                        cb.BeginText();
                        //text = DT.Rows[i]["EmployeePHealth"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(Element.ALIGN_RIGHT, DT.Rows[i]["FinalTotalPhilHealth"].ToString(), 660, txtheight2 - 10, 0);
                        cb.EndText();


                        cb.BeginText();
                        //text = DT.Rows[i]["EmployeePHealth"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(Element.ALIGN_RIGHT, DT.Rows[i]["TotalPagIbigAmount"].ToString(), 712, txtheight2, 0);
                        cb.EndText();

                        cb.BeginText();
                        //text = DT.Rows[i]["EmployeePHealth"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(Element.ALIGN_RIGHT, DT.Rows[i]["TotalPagibigERAmount"].ToString(), 761, txtheight2, 0);
                        cb.EndText();

                        cb.BeginText();
                        //text = DT.Rows[i]["EmployeePHealth"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(Element.ALIGN_RIGHT, "Total HDMF:", 712, txtheight2 - 10, 0);
                        cb.EndText();

                        cb.BeginText();
                        //text = DT.Rows[i]["EmployeePHealth"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(Element.ALIGN_RIGHT, DT.Rows[i]["FinalTotalPagibig"].ToString(), 761, txtheight2 - 10, 0);
                        cb.EndText();




                    }

                    cb.BeginText();
                    text = DT.Rows[i]["Empid"].ToString();
                    // put the alignment and coordinates here
                    cb.ShowTextAligned(0, text, 30, txtheight, 0);
                    cb.EndText();

                    cb.BeginText();
                    text = DT.Rows[i]["SSSnum"].ToString();
                    // put the alignment and coordinates here
                    cb.ShowTextAligned(0, text, 92, txtheight, 0);
                    cb.EndText();

                    cb.BeginText();
                    text = DT.Rows[i]["HDMF"].ToString();
                    // put the alignment and coordinates here
                    cb.ShowTextAligned(0, text, 156, txtheight, 0);
                    cb.EndText();

                    cb.BeginText();
                    text = DT.Rows[i]["EmployeeName"].ToString();
                    // put the alignment and coordinates here
                    cb.ShowTextAligned(0, text.Length > 17 ? text.Remove(17) : text, 233, txtheight, 0);
                    cb.EndText();

                    cb.BeginText();
                    text = DT.Rows[i]["SSSPremEmployee"].ToString();
                    // put the alignment and coordinates here
                    cb.ShowTextAligned(Element.ALIGN_RIGHT, text, 447, txtheight, 0);
                    cb.EndText();

                    cb.BeginText();
                    text = DT.Rows[i]["SSSPremEcEmployer"].ToString();
                    // put the alignment and coordinates here
                    cb.ShowTextAligned(Element.ALIGN_RIGHT, text, 507, txtheight, 0);
                    cb.EndText();

                    cb.BeginText();
                    text = DT.Rows[i]["EmployerEC"].ToString();
                    // put the alignment and coordinates here
                    cb.ShowTextAligned(Element.ALIGN_RIGHT, text, 557, txtheight, 0);
                    cb.EndText();

                    cb.BeginText();
                    text = DT.Rows[i]["EmployeePhilHealth"].ToString();
                    // put the alignment and coordinates here
                    cb.ShowTextAligned(Element.ALIGN_RIGHT, text, 608, txtheight, 0);
                    cb.EndText();

                    cb.BeginText();
                    text = DT.Rows[i]["EmployerPhilHealth"].ToString();
                    // put the alignment and coordinates here
                    cb.ShowTextAligned(Element.ALIGN_RIGHT, text, 660, txtheight, 0);
                    cb.EndText();

                    cb.BeginText();
                    text = DT.Rows[i]["PagIbigAmount"].ToString();
                    // put the alignment and coordinates here
                    cb.ShowTextAligned(Element.ALIGN_RIGHT, text, 712, txtheight, 0);
                    cb.EndText();

                    cb.BeginText();
                    text = DT.Rows[i]["PagibigERAmount"].ToString();
                    // put the alignment and coordinates here
                    cb.ShowTextAligned(Element.ALIGN_RIGHT, text, 761, txtheight, 0);
                    cb.EndText();


                    if (i % 4 == 0)
                    {
                        int decheight = txtheight - 11;
                        txtheight = decheight;
                    }
                    else if (i % 5 == 0)
                    {
                        int decheight = txtheight - 10;
                        txtheight = decheight;
                    }
                    else if (i % 7 == 0)
                    {
                        int decheight = txtheight - 11;
                        txtheight = decheight;
                    }
                    else
                    {
                        int decheight = txtheight - 12;
                        txtheight = decheight;
                    }




                }


            }




            // close the streams and voilá the file should be changed :)
            document.Close();
            fs.Close();
            writer.Close();
            reader.Close();

            return ListReturned;
        }

        //    [HttpPost]
        //    [Route("pagqtrgen")]
        //    public List<PAGQTRList> PAGQTRList(PAGQTRList ssphhd)
        //    {
        //        GetDBILMMain();


        //        Connection con = new Connection();
        //        DataTable DT = new DataTable();
        //        con.myparameters.Add(new myParameters { ParameterName = "@code", mytype = SqlDbType.NVarChar, Value = ssphhd.code });
        //        con.myparameters.Add(new myParameters { ParameterName = "@month", mytype = SqlDbType.NVarChar, Value = ssphhd.mmonth });
        //        con.myparameters.Add(new myParameters { ParameterName = "@year", mytype = SqlDbType.NVarChar, Value = ssphhd.yyear });
        //        DT = con.GetDataTable("sp_GetPAGQTR");

        //        string x = ConfigurationManager.ConnectionStrings["connectionstring"].ToString();
        //        List<PAGQTRList> ListReturned = new List<PAGQTRList>();

        //        //return ListReturned;
        //        string oldFile = HttpContext.Current.Server.MapPath("~/pdf/") + "SSPHHD" + ".pdf";
        //        string newFile = HttpContext.Current.Server.MapPath("~/pdf/") + "SSPHHD v2" + ".pdf";

        //        // open the reader
        //        PdfReader reader = new PdfReader(oldFile);
        //        Rectangle size = reader.GetPageSizeWithRotation(1);
        //        Document document = new Document(size);

        //        // open the writer
        //        FileStream fs = new FileStream(newFile, FileMode.Create, FileAccess.Write);
        //        PdfWriter writer = PdfWriter.GetInstance(document, fs);
        //        document.Open();

        //        // the pdf content
        //        PdfContentByte cb = writer.DirectContent;

        //        // select the font properties
        //        BaseFont bf = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
        //        cb.SetColorFill(BaseColor.DARK_GRAY);
        //        cb.SetFontAndSize(bf, 8);

        //        PdfImportedPage page = writer.GetImportedPage(reader, 1);
        //        cb.AddTemplate(page, 0, 0);


        //        cb.BeginText();
        //        //text = DateTime.Today;
        //        // put the alignment and coordinates here
        //        cb.ShowTextAligned(0, DateTime.Now.ToString("MM-dd-yyyy"), 30, 565, 0);
        //        cb.EndText();

        //        cb.BeginText();
        //        //text = DT.Rows[i]["CompanyName"].ToString();
        //        // put the alignment and coordinates here
        //        cb.ShowTextAligned(Element.ALIGN_CENTER, DT.Rows[0]["CompanyName"].ToString(), 380, 565, 0);
        //        cb.EndText();

        //        cb.BeginText();
        //        //text = DT.Rows[i]["CompanyName"].ToString();
        //        // put the alignment and coordinates here
        //        cb.ShowTextAligned(0, DT.Rows[0]["CompanyCode"].ToString(), 377, 543, 0);
        //        cb.EndText();

        //        cb.BeginText();
        //        //text = DT.Rows[i]["CompanyName"].ToString();
        //        // put the alignment and coordinates here
        //        cb.ShowTextAligned(0, DT.Rows[0]["startdate"].ToString() + " " + "TO", 403, 532, 0);
        //        cb.EndText();

        //        cb.BeginText();
        //        //text = DT.Rows[i]["CompanyName"].ToString();
        //        // put the alignment and coordinates here
        //        cb.ShowTextAligned(0, DT.Rows[0]["enddate"].ToString(), 460, 532, 0);
        //        cb.EndText();

        //        int NOOFDATA = DT.Rows.Count;
        //        Double pages = NOOFDATA / 76;
        //        int txtheight = 460;
        //        int txtheight2 = 60;
        //        int baseno2 = 35;

        //        int numb1 = DT.Rows.Count - 1;
        //        var couterr = numb1.ToString();

        //        string text = "";

        //        for (int i = 0; i < NOOFDATA; i++)
        //        {
        //            var counters = i.ToString();

        //            PAGQTRList Get = new PAGQTRList();

        //            Get.Empid = DT.Rows[i]["Empid"].ToString();
        //            Get.HDMF = DT.Rows[i]["HDMF"].ToString();
        //            Get.LastName = DT.Rows[i]["LastName"].ToString();
        //            Get.FirstName = DT.Rows[i]["FirstName"].ToString();
        //            Get.MiddleName = DT.Rows[i]["MiddleName"].ToString();
        //            Get.PagIbigAmount = DT.Rows[i]["PagIbigAmount"].ToString();
        //            Get.PagibigERAmount = DT.Rows[i]["PagibigERAmount"].ToString();
        //            Get.TotalEEER = DT.Rows[i]["TotalEEER"].ToString();
        //            Get.PeriodCover = DT.Rows[i]["PeriodCover"].ToString();
        //            Get.TotalPagIbigAmount = DT.Rows[i]["TotalPagIbigAmount"].ToString();
        //            Get.TotalPagibigERAmount = DT.Rows[i]["TotalPagibigERAmount"].ToString();
        //            Get.FinalTotalPagibig = DT.Rows[i]["FinalTotalPagibig"].ToString();
        //            Get.StartDate = DT.Rows[i]["StartDate"].ToString();
        //            Get.EndDate = DT.Rows[i]["EndDate"].ToString();
        //            Get.CompanyCode = DT.Rows[i]["CompanyCode"].ToString();
        //            Get.CompanyName = DT.Rows[i]["CompanyName"].ToString();
        //            Get.CompanyAddress = DT.Rows[i]["CompanyAddress"].ToString();

        //            ListReturned.Add(Get);

        //            if (i < baseno2)
        //            {
        //                //string text = "";
        //                // put the alignment and coordinates here
        //                if (counters == couterr)
        //                {
        //                    cb.BeginText();
        //                    //text = DT.Rows[i]["Empid"].ToString();
        //                    // put the alignment and coordinates here
        //                    cb.ShowTextAligned(0, "Totals:", 30, txtheight2, 0);
        //                    cb.EndText();

        //                    cb.BeginText();
        //                    //text = DT.Rows[i]["Empid"].ToString();
        //                    // put the alignment and coordinates here
        //                    cb.ShowTextAligned(0, "" + DT.Rows.Count, 92, txtheight2, 0);
        //                    cb.EndText();


        //                    cb.BeginText();
        //                    //text = DT.Rows[i]["EmployeePHealth"].ToString();
        //                    // put the alignment and coordinates here
        //                    cb.ShowTextAligned(Element.ALIGN_RIGHT, DT.Rows[i]["TotalEmployeeSSS"].ToString(), 447, txtheight2, 0);
        //                    cb.EndText();

        //                    cb.BeginText();
        //                    //text = DT.Rows[i]["EmployeePHealth"].ToString();
        //                    // put the alignment and coordinates here
        //                    cb.ShowTextAligned(Element.ALIGN_RIGHT, DT.Rows[i]["TotalEmployerSSS"].ToString(), 507, txtheight2, 0);
        //                    cb.EndText();

        //                    cb.BeginText();
        //                    //text = DT.Rows[i]["EmployeePHealth"].ToString();
        //                    // put the alignment and coordinates here
        //                    cb.ShowTextAligned(Element.ALIGN_RIGHT, DT.Rows[i]["TotalEmployerECSSS"].ToString(), 557, txtheight2, 0);
        //                    cb.EndText();

        //                    cb.BeginText();
        //                    //text = DT.Rows[i]["EmployeePHealth"].ToString();
        //                    // put the alignment and coordinates here
        //                    cb.ShowTextAligned(Element.ALIGN_RIGHT, "Total SSS:", 507, txtheight2 - 10, 0);
        //                    cb.EndText();

        //                    cb.BeginText();
        //                    //text = DT.Rows[i]["EmployeePHealth"].ToString();
        //                    // put the alignment and coordinates here
        //                    cb.ShowTextAligned(Element.ALIGN_RIGHT, DT.Rows[i]["FinalTotalSSS"].ToString(), 557, txtheight2 - 10, 0);
        //                    cb.EndText();

        //                    cb.BeginText();
        //                    //text = DT.Rows[i]["EmployeePHealth"].ToString();
        //                    // put the alignment and coordinates here
        //                    cb.ShowTextAligned(Element.ALIGN_RIGHT, DT.Rows[i]["TotalEmployeePhilHealth"].ToString(), 608, txtheight2, 0);
        //                    cb.EndText();

        //                    cb.BeginText();
        //                    //text = DT.Rows[i]["EmployeePHealth"].ToString();
        //                    // put the alignment and coordinates here
        //                    cb.ShowTextAligned(Element.ALIGN_RIGHT, DT.Rows[i]["TotalEmployerPhilHealth"].ToString(), 660, txtheight2, 0);
        //                    cb.EndText();

        //                    cb.BeginText();
        //                    //text = DT.Rows[i]["EmployeePHealth"].ToString();
        //                    // put the alignment and coordinates here
        //                    cb.ShowTextAligned(Element.ALIGN_RIGHT, "Total PH:", 608, txtheight2 - 10, 0);
        //                    cb.EndText();

        //                    cb.BeginText();
        //                    //text = DT.Rows[i]["EmployeePHealth"].ToString();
        //                    // put the alignment and coordinates here
        //                    cb.ShowTextAligned(Element.ALIGN_RIGHT, DT.Rows[i]["FinalTotalPhilHealth"].ToString(), 660, txtheight2 - 10, 0);
        //                    cb.EndText();


        //                    cb.BeginText();
        //                    //text = DT.Rows[i]["EmployeePHealth"].ToString();
        //                    // put the alignment and coordinates here
        //                    cb.ShowTextAligned(Element.ALIGN_RIGHT, DT.Rows[i]["TotalPagIbigAmount"].ToString(), 712, txtheight2, 0);
        //                    cb.EndText();

        //                    cb.BeginText();
        //                    //text = DT.Rows[i]["EmployeePHealth"].ToString();
        //                    // put the alignment and coordinates here
        //                    cb.ShowTextAligned(Element.ALIGN_RIGHT, DT.Rows[i]["TotalPagibigERAmount"].ToString(), 761, txtheight2, 0);
        //                    cb.EndText();

        //                    cb.BeginText();
        //                    //text = DT.Rows[i]["EmployeePHealth"].ToString();
        //                    // put the alignment and coordinates here
        //                    cb.ShowTextAligned(Element.ALIGN_RIGHT, "Total HDMF:", 712, txtheight2 - 10, 0);
        //                    cb.EndText();

        //                    cb.BeginText();
        //                    //text = DT.Rows[i]["EmployeePHealth"].ToString();
        //                    // put the alignment and coordinates here
        //                    cb.ShowTextAligned(Element.ALIGN_RIGHT, DT.Rows[i]["FinalTotalPagibig"].ToString(), 761, txtheight2 - 10, 0);
        //                    cb.EndText();




        //                }

        //                cb.BeginText();
        //                text = DT.Rows[i]["Empid"].ToString();
        //                // put the alignment and coordinates here
        //                cb.ShowTextAligned(0, text, 30, txtheight, 0);
        //                cb.EndText();

        //                cb.BeginText();
        //                text = DT.Rows[i]["SSSnum"].ToString();
        //                // put the alignment and coordinates here
        //                cb.ShowTextAligned(0, text, 92, txtheight, 0);
        //                cb.EndText();

        //                cb.BeginText();
        //                text = DT.Rows[i]["HDMF"].ToString();
        //                // put the alignment and coordinates here
        //                cb.ShowTextAligned(0, text, 156, txtheight, 0);
        //                cb.EndText();

        //                cb.BeginText();
        //                text = DT.Rows[i]["EmployeeName"].ToString();
        //                // put the alignment and coordinates here
        //                cb.ShowTextAligned(0, text.Length > 17 ? text.Remove(17) : text, 233, txtheight, 0);
        //                cb.EndText();

        //                cb.BeginText();
        //                text = DT.Rows[i]["SSSPremEmployee"].ToString();
        //                // put the alignment and coordinates here
        //                cb.ShowTextAligned(Element.ALIGN_RIGHT, text, 447, txtheight, 0);
        //                cb.EndText();

        //                cb.BeginText();
        //                text = DT.Rows[i]["SSSPremEcEmployer"].ToString();
        //                // put the alignment and coordinates here
        //                cb.ShowTextAligned(Element.ALIGN_RIGHT, text, 507, txtheight, 0);
        //                cb.EndText();

        //                cb.BeginText();
        //                text = DT.Rows[i]["EmployerEC"].ToString();
        //                // put the alignment and coordinates here
        //                cb.ShowTextAligned(Element.ALIGN_RIGHT, text, 557, txtheight, 0);
        //                cb.EndText();

        //                cb.BeginText();
        //                text = DT.Rows[i]["EmployeePhilHealth"].ToString();
        //                // put the alignment and coordinates here
        //                cb.ShowTextAligned(Element.ALIGN_RIGHT, text, 608, txtheight, 0);
        //                cb.EndText();

        //                cb.BeginText();
        //                text = DT.Rows[i]["EmployerPhilHealth"].ToString();
        //                // put the alignment and coordinates here
        //                cb.ShowTextAligned(Element.ALIGN_RIGHT, text, 660, txtheight, 0);
        //                cb.EndText();

        //                cb.BeginText();
        //                text = DT.Rows[i]["PagIbigAmount"].ToString();
        //                // put the alignment and coordinates here
        //                cb.ShowTextAligned(Element.ALIGN_RIGHT, text, 712, txtheight, 0);
        //                cb.EndText();

        //                cb.BeginText();
        //                text = DT.Rows[i]["PagibigERAmount"].ToString();
        //                // put the alignment and coordinates here
        //                cb.ShowTextAligned(Element.ALIGN_RIGHT, text, 761, txtheight, 0);
        //                cb.EndText();




        //                if (i % 4 == 0)
        //                {
        //                    int decheight = txtheight - 11;
        //                    txtheight = decheight;
        //                }
        //                else if (i % 5 == 0)
        //                {
        //                    int decheight = txtheight - 10;
        //                    txtheight = decheight;
        //                }
        //                else if (i % 7 == 0)
        //                {
        //                    int decheight = txtheight - 11;
        //                    txtheight = decheight;
        //                }
        //                else
        //                {
        //                    int decheight = txtheight - 12;
        //                    txtheight = decheight;
        //                }

        //            }
        //            else if ((i % baseno2) == 0)
        //            {
        //                txtheight = 460;
        //                document.NewPage();
        //                cb.SetColorFill(BaseColor.DARK_GRAY);
        //                cb.SetFontAndSize(bf, 8);
        //                //PdfImportedPage page = writer.GetImportedPage(reader, 2);
        //                PdfImportedPage page2 = writer.GetImportedPage(reader, 1);
        //                cb.AddTemplate(page2, 0, 0);
        //                //BaseFont bf = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
        //                //string text = "";
        //                // put the alignment and coordinates here

        //                if (counters == couterr)
        //                {
        //                    cb.BeginText();
        //                    //text = DT.Rows[i]["Empid"].ToString();
        //                    // put the alignment and coordinates here
        //                    cb.ShowTextAligned(0, "Totals:", 30, txtheight2, 0);
        //                    cb.EndText();

        //                    cb.BeginText();
        //                    //text = DT.Rows[i]["Empid"].ToString();
        //                    // put the alignment and coordinates here
        //                    cb.ShowTextAligned(0, "" + DT.Rows.Count, 92, txtheight2, 0);
        //                    cb.EndText();


        //                    cb.BeginText();
        //                    //text = DT.Rows[i]["EmployeePHealth"].ToString();
        //                    // put the alignment and coordinates here
        //                    cb.ShowTextAligned(Element.ALIGN_RIGHT, DT.Rows[i]["TotalEmployeeSSS"].ToString(), 447, txtheight2, 0);
        //                    cb.EndText();

        //                    cb.BeginText();
        //                    //text = DT.Rows[i]["EmployeePHealth"].ToString();
        //                    // put the alignment and coordinates here
        //                    cb.ShowTextAligned(Element.ALIGN_RIGHT, DT.Rows[i]["TotalEmployerSSS"].ToString(), 507, txtheight2, 0);
        //                    cb.EndText();

        //                    cb.BeginText();
        //                    //text = DT.Rows[i]["EmployeePHealth"].ToString();
        //                    // put the alignment and coordinates here
        //                    cb.ShowTextAligned(Element.ALIGN_RIGHT, DT.Rows[i]["TotalEmployerECSSS"].ToString(), 557, txtheight2, 0);
        //                    cb.EndText();

        //                    cb.BeginText();
        //                    //text = DT.Rows[i]["EmployeePHealth"].ToString();
        //                    // put the alignment and coordinates here
        //                    cb.ShowTextAligned(Element.ALIGN_RIGHT, "Total SSS:", 507, txtheight2 - 10, 0);
        //                    cb.EndText();

        //                    cb.BeginText();
        //                    //text = DT.Rows[i]["EmployeePHealth"].ToString();
        //                    // put the alignment and coordinates here
        //                    cb.ShowTextAligned(Element.ALIGN_RIGHT, DT.Rows[i]["FinalTotalSSS"].ToString(), 557, txtheight2 - 10, 0);
        //                    cb.EndText();

        //                    cb.BeginText();
        //                    //text = DT.Rows[i]["EmployeePHealth"].ToString();
        //                    // put the alignment and coordinates here
        //                    cb.ShowTextAligned(Element.ALIGN_RIGHT, DT.Rows[i]["TotalEmployeePhilHealth"].ToString(), 608, txtheight2, 0);
        //                    cb.EndText();

        //                    cb.BeginText();
        //                    //text = DT.Rows[i]["EmployeePHealth"].ToString();
        //                    // put the alignment and coordinates here
        //                    cb.ShowTextAligned(Element.ALIGN_RIGHT, DT.Rows[i]["TotalEmployerPhilHealth"].ToString(), 660, txtheight2, 0);
        //                    cb.EndText();

        //                    cb.BeginText();
        //                    //text = DT.Rows[i]["EmployeePHealth"].ToString();
        //                    // put the alignment and coordinates here
        //                    cb.ShowTextAligned(Element.ALIGN_RIGHT, "Total PH:", 608, txtheight2 - 10, 0);
        //                    cb.EndText();

        //                    cb.BeginText();
        //                    //text = DT.Rows[i]["EmployeePHealth"].ToString();
        //                    // put the alignment and coordinates here
        //                    cb.ShowTextAligned(Element.ALIGN_RIGHT, DT.Rows[i]["FinalTotalPhilHealth"].ToString(), 660, txtheight2 - 10, 0);
        //                    cb.EndText();


        //                    cb.BeginText();
        //                    //text = DT.Rows[i]["EmployeePHealth"].ToString();
        //                    // put the alignment and coordinates here
        //                    cb.ShowTextAligned(Element.ALIGN_RIGHT, DT.Rows[i]["TotalPagIbigAmount"].ToString(), 712, txtheight2, 0);
        //                    cb.EndText();

        //                    cb.BeginText();
        //                    //text = DT.Rows[i]["EmployeePHealth"].ToString();
        //                    // put the alignment and coordinates here
        //                    cb.ShowTextAligned(Element.ALIGN_RIGHT, DT.Rows[i]["TotalPagibigERAmount"].ToString(), 761, txtheight2, 0);
        //                    cb.EndText();

        //                    cb.BeginText();
        //                    //text = DT.Rows[i]["EmployeePHealth"].ToString();
        //                    // put the alignment and coordinates here
        //                    cb.ShowTextAligned(Element.ALIGN_RIGHT, "Total HDMF:", 712, txtheight2 - 10, 0);
        //                    cb.EndText();

        //                    cb.BeginText();
        //                    //text = DT.Rows[i]["EmployeePHealth"].ToString();
        //                    // put the alignment and coordinates here
        //                    cb.ShowTextAligned(Element.ALIGN_RIGHT, DT.Rows[i]["FinalTotalPagibig"].ToString(), 761, txtheight2 - 10, 0);
        //                    cb.EndText();





        //                }

        //                cb.BeginText();
        //                //text = DateTime.Today;
        //                // put the alignment and coordinates here
        //                cb.ShowTextAligned(0, DateTime.Now.ToString("MM-dd-yyyy"), 30, 565, 0);
        //                cb.EndText();

        //                cb.BeginText();
        //                //text = DT.Rows[i]["CompanyName"].ToString();
        //                // put the alignment and coordinates here
        //                cb.ShowTextAligned(Element.ALIGN_CENTER, DT.Rows[0]["CompanyName"].ToString(), 380, 565, 0);
        //                cb.EndText();

        //                cb.BeginText();
        //                //text = DT.Rows[i]["CompanyName"].ToString();
        //                // put the alignment and coordinates here
        //                cb.ShowTextAligned(0, DT.Rows[0]["CompanyCode"].ToString(), 377, 543, 0);
        //                cb.EndText();

        //                cb.BeginText();
        //                //text = DT.Rows[i]["CompanyName"].ToString();
        //                // put the alignment and coordinates here
        //                cb.ShowTextAligned(0, DT.Rows[0]["startdate"].ToString() + " " + "TO", 403, 532, 0);
        //                cb.EndText();

        //                cb.BeginText();
        //                //text = DT.Rows[i]["CompanyName"].ToString();
        //                // put the alignment and coordinates here
        //                cb.ShowTextAligned(0, DT.Rows[0]["enddate"].ToString(), 460, 532, 0);
        //                cb.EndText();

        //                cb.BeginText();
        //                text = DT.Rows[i]["Empid"].ToString();
        //                // put the alignment and coordinates here
        //                cb.ShowTextAligned(0, text, 30, txtheight, 0);
        //                cb.EndText();

        //                cb.BeginText();
        //                text = DT.Rows[i]["SSSnum"].ToString();
        //                // put the alignment and coordinates here
        //                cb.ShowTextAligned(0, text, 92, txtheight, 0);
        //                cb.EndText();

        //                cb.BeginText();
        //                text = DT.Rows[i]["HDMF"].ToString();
        //                // put the alignment and coordinates here
        //                cb.ShowTextAligned(0, text, 156, txtheight, 0);
        //                cb.EndText();

        //                cb.BeginText();
        //                text = DT.Rows[i]["EmployeeName"].ToString();
        //                // put the alignment and coordinates here
        //                cb.ShowTextAligned(0, text.Length > 17 ? text.Remove(17) : text, 233, txtheight, 0);
        //                cb.EndText();

        //                cb.BeginText();
        //                text = DT.Rows[i]["SSSPremEmployee"].ToString();
        //                // put the alignment and coordinates here
        //                cb.ShowTextAligned(Element.ALIGN_RIGHT, text, 447, txtheight, 0);
        //                cb.EndText();

        //                cb.BeginText();
        //                text = DT.Rows[i]["SSSPremEcEmployer"].ToString();
        //                // put the alignment and coordinates here
        //                cb.ShowTextAligned(Element.ALIGN_RIGHT, text, 507, txtheight, 0);
        //                cb.EndText();

        //                cb.BeginText();
        //                text = DT.Rows[i]["EmployerEC"].ToString();
        //                // put the alignment and coordinates here
        //                cb.ShowTextAligned(Element.ALIGN_RIGHT, text, 557, txtheight, 0);
        //                cb.EndText();

        //                cb.BeginText();
        //                text = DT.Rows[i]["EmployeePhilHealth"].ToString();
        //                // put the alignment and coordinates here
        //                cb.ShowTextAligned(Element.ALIGN_RIGHT, text, 608, txtheight, 0);
        //                cb.EndText();

        //                cb.BeginText();
        //                text = DT.Rows[i]["EmployerPhilHealth"].ToString();
        //                // put the alignment and coordinates here
        //                cb.ShowTextAligned(Element.ALIGN_RIGHT, text, 660, txtheight, 0);
        //                cb.EndText();

        //                cb.BeginText();
        //                text = DT.Rows[i]["PagIbigAmount"].ToString();
        //                // put the alignment and coordinates here
        //                cb.ShowTextAligned(Element.ALIGN_RIGHT, text, 712, txtheight, 0);
        //                cb.EndText();

        //                cb.BeginText();
        //                text = DT.Rows[i]["PagibigERAmount"].ToString();
        //                // put the alignment and coordinates here
        //                cb.ShowTextAligned(Element.ALIGN_RIGHT, text, 761, txtheight, 0);
        //                cb.EndText();


        //                if (i % 4 == 0)
        //                {
        //                    int decheight = txtheight - 11;
        //                    txtheight = decheight;
        //                }
        //                else if (i % 5 == 0)
        //                {
        //                    int decheight = txtheight - 10;
        //                    txtheight = decheight;
        //                }
        //                else if (i % 7 == 0)
        //                {
        //                    int decheight = txtheight - 11;
        //                    txtheight = decheight;
        //                }
        //                else
        //                {
        //                    int decheight = txtheight - 12;
        //                    txtheight = decheight;
        //                }
        //            }
        //            else
        //            {


        //                //string text = "";
        //                // put the alignment and coordinates here

        //                if (counters == couterr)
        //                {
        //                    cb.BeginText();
        //                    //text = DT.Rows[i]["Empid"].ToString();
        //                    // put the alignment and coordinates here
        //                    cb.ShowTextAligned(0, "Totals:", 30, txtheight2, 0);
        //                    cb.EndText();

        //                    cb.BeginText();
        //                    //text = DT.Rows[i]["Empid"].ToString();
        //                    // put the alignment and coordinates here
        //                    cb.ShowTextAligned(0, "" + DT.Rows.Count, 92, txtheight2, 0);
        //                    cb.EndText();


        //                    cb.BeginText();
        //                    //text = DT.Rows[i]["EmployeePHealth"].ToString();
        //                    // put the alignment and coordinates here
        //                    cb.ShowTextAligned(Element.ALIGN_RIGHT, DT.Rows[i]["TotalEmployeeSSS"].ToString(), 447, txtheight2, 0);
        //                    cb.EndText();

        //                    cb.BeginText();
        //                    //text = DT.Rows[i]["EmployeePHealth"].ToString();
        //                    // put the alignment and coordinates here
        //                    cb.ShowTextAligned(Element.ALIGN_RIGHT, DT.Rows[i]["TotalEmployerSSS"].ToString(), 507, txtheight2, 0);
        //                    cb.EndText();

        //                    cb.BeginText();
        //                    //text = DT.Rows[i]["EmployeePHealth"].ToString();
        //                    // put the alignment and coordinates here
        //                    cb.ShowTextAligned(Element.ALIGN_RIGHT, DT.Rows[i]["TotalEmployerECSSS"].ToString(), 557, txtheight2, 0);
        //                    cb.EndText();

        //                    cb.BeginText();
        //                    //text = DT.Rows[i]["EmployeePHealth"].ToString();
        //                    // put the alignment and coordinates here
        //                    cb.ShowTextAligned(Element.ALIGN_RIGHT, "Total SSS:", 507, txtheight2 - 10, 0);
        //                    cb.EndText();

        //                    cb.BeginText();
        //                    //text = DT.Rows[i]["EmployeePHealth"].ToString();
        //                    // put the alignment and coordinates here
        //                    cb.ShowTextAligned(Element.ALIGN_RIGHT, DT.Rows[i]["FinalTotalSSS"].ToString(), 557, txtheight2 - 10, 0);
        //                    cb.EndText();

        //                    cb.BeginText();
        //                    //text = DT.Rows[i]["EmployeePHealth"].ToString();
        //                    // put the alignment and coordinates here
        //                    cb.ShowTextAligned(Element.ALIGN_RIGHT, DT.Rows[i]["TotalEmployeePhilHealth"].ToString(), 608, txtheight2, 0);
        //                    cb.EndText();

        //                    cb.BeginText();
        //                    //text = DT.Rows[i]["EmployeePHealth"].ToString();
        //                    // put the alignment and coordinates here
        //                    cb.ShowTextAligned(Element.ALIGN_RIGHT, DT.Rows[i]["TotalEmployerPhilHealth"].ToString(), 660, txtheight2, 0);
        //                    cb.EndText();

        //                    cb.BeginText();
        //                    //text = DT.Rows[i]["EmployeePHealth"].ToString();
        //                    // put the alignment and coordinates here
        //                    cb.ShowTextAligned(Element.ALIGN_RIGHT, "Total PH:", 608, txtheight2 - 10, 0);
        //                    cb.EndText();

        //                    cb.BeginText();
        //                    //text = DT.Rows[i]["EmployeePHealth"].ToString();
        //                    // put the alignment and coordinates here
        //                    cb.ShowTextAligned(Element.ALIGN_RIGHT, DT.Rows[i]["FinalTotalPhilHealth"].ToString(), 660, txtheight2 - 10, 0);
        //                    cb.EndText();


        //                    cb.BeginText();
        //                    //text = DT.Rows[i]["EmployeePHealth"].ToString();
        //                    // put the alignment and coordinates here
        //                    cb.ShowTextAligned(Element.ALIGN_RIGHT, DT.Rows[i]["TotalPagIbigAmount"].ToString(), 712, txtheight2, 0);
        //                    cb.EndText();

        //                    cb.BeginText();
        //                    //text = DT.Rows[i]["EmployeePHealth"].ToString();
        //                    // put the alignment and coordinates here
        //                    cb.ShowTextAligned(Element.ALIGN_RIGHT, DT.Rows[i]["TotalPagibigERAmount"].ToString(), 761, txtheight2, 0);
        //                    cb.EndText();

        //                    cb.BeginText();
        //                    //text = DT.Rows[i]["EmployeePHealth"].ToString();
        //                    // put the alignment and coordinates here
        //                    cb.ShowTextAligned(Element.ALIGN_RIGHT, "Total HDMF:", 712, txtheight2 - 10, 0);
        //                    cb.EndText();

        //                    cb.BeginText();
        //                    //text = DT.Rows[i]["EmployeePHealth"].ToString();
        //                    // put the alignment and coordinates here
        //                    cb.ShowTextAligned(Element.ALIGN_RIGHT, DT.Rows[i]["FinalTotalPagibig"].ToString(), 761, txtheight2 - 10, 0);
        //                    cb.EndText();




        //                }

        //                cb.BeginText();
        //                text = DT.Rows[i]["Empid"].ToString();
        //                // put the alignment and coordinates here
        //                cb.ShowTextAligned(0, text, 30, txtheight, 0);
        //                cb.EndText();

        //                cb.BeginText();
        //                text = DT.Rows[i]["SSSnum"].ToString();
        //                // put the alignment and coordinates here
        //                cb.ShowTextAligned(0, text, 92, txtheight, 0);
        //                cb.EndText();

        //                cb.BeginText();
        //                text = DT.Rows[i]["HDMF"].ToString();
        //                // put the alignment and coordinates here
        //                cb.ShowTextAligned(0, text, 156, txtheight, 0);
        //                cb.EndText();

        //                cb.BeginText();
        //                text = DT.Rows[i]["EmployeeName"].ToString();
        //                // put the alignment and coordinates here
        //                cb.ShowTextAligned(0, text.Length > 17 ? text.Remove(17) : text, 233, txtheight, 0);
        //                cb.EndText();

        //                cb.BeginText();
        //                text = DT.Rows[i]["SSSPremEmployee"].ToString();
        //                // put the alignment and coordinates here
        //                cb.ShowTextAligned(Element.ALIGN_RIGHT, text, 447, txtheight, 0);
        //                cb.EndText();

        //                cb.BeginText();
        //                text = DT.Rows[i]["SSSPremEcEmployer"].ToString();
        //                // put the alignment and coordinates here
        //                cb.ShowTextAligned(Element.ALIGN_RIGHT, text, 507, txtheight, 0);
        //                cb.EndText();

        //                cb.BeginText();
        //                text = DT.Rows[i]["EmployerEC"].ToString();
        //                // put the alignment and coordinates here
        //                cb.ShowTextAligned(Element.ALIGN_RIGHT, text, 557, txtheight, 0);
        //                cb.EndText();

        //                cb.BeginText();
        //                text = DT.Rows[i]["EmployeePhilHealth"].ToString();
        //                // put the alignment and coordinates here
        //                cb.ShowTextAligned(Element.ALIGN_RIGHT, text, 608, txtheight, 0);
        //                cb.EndText();

        //                cb.BeginText();
        //                text = DT.Rows[i]["EmployerPhilHealth"].ToString();
        //                // put the alignment and coordinates here
        //                cb.ShowTextAligned(Element.ALIGN_RIGHT, text, 660, txtheight, 0);
        //                cb.EndText();

        //                cb.BeginText();
        //                text = DT.Rows[i]["PagIbigAmount"].ToString();
        //                // put the alignment and coordinates here
        //                cb.ShowTextAligned(Element.ALIGN_RIGHT, text, 712, txtheight, 0);
        //                cb.EndText();

        //                cb.BeginText();
        //                text = DT.Rows[i]["PagibigERAmount"].ToString();
        //                // put the alignment and coordinates here
        //                cb.ShowTextAligned(Element.ALIGN_RIGHT, text, 761, txtheight, 0);
        //                cb.EndText();


        //                if (i % 4 == 0)
        //                {
        //                    int decheight = txtheight - 11;
        //                    txtheight = decheight;
        //                }
        //                else if (i % 5 == 0)
        //                {
        //                    int decheight = txtheight - 10;
        //                    txtheight = decheight;
        //                }
        //                else if (i % 7 == 0)
        //                {
        //                    int decheight = txtheight - 11;
        //                    txtheight = decheight;
        //                }
        //                else
        //                {
        //                    int decheight = txtheight - 12;
        //                    txtheight = decheight;
        //                }




        //            }


        //        }




        //        // close the streams and voilá the file should be changed :)
        //        document.Close();
        //        fs.Close();
        //        writer.Close();
        //        reader.Close();

        //        return ListReturned;
        //    }

        //}

        //    Connection Connection = new Connection();
        //    Connection.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.VarChar, Value = filterLeave.EMPID });
        //    Connection.myparameters.Add(new myParameters { ParameterName = "@startdate", mytype = SqlDbType.VarChar, Value = filterLeave.startdate });
        //    Connection.myparameters.Add(new myParameters { ParameterName = "@enddate", mytype = SqlDbType.VarChar, Value = filterLeave.enddate });
        //    Connection.myparameters.Add(new myParameters { ParameterName = "@Leave", mytype = SqlDbType.Structured, Value = filterUser });
        //    DataTable DT = Connection.GetDataTable("sp_ViewLeaveSlot");
        //    foreach (DataRow row in DT.Rows)
        //    {
        //        FilterLeaveSlot Users = new FilterLeaveSlot()
        //        {
        //            dayleavetype = row["dayleavetype"].ToString(),
        //            leavereason = row["leavereason"].ToString(),
        //            date = row["date"].ToString(),
        //            Slot = row["Slot"].ToString(),

        //        };
        //        DU.Add(Users);
        //    }
        //    return DU;
        //}
        #endregion

        #region GhonPDf
        //GovDueStart
        [HttpPost]
        [Route("GetGovDue")]
        //public void TryReport(string empid, string month, string year)
        public string GetGovDue(GovDueParam govdata)
        {
            try
            {
                TempConnectionStringv2();
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@tenantid", mytype = SqlDbType.NVarChar, Value = govdata.tenantid });
                con.myparameters.Add(new myParameters { ParameterName = "@month", mytype = SqlDbType.NVarChar, Value = govdata.month });
                con.myparameters.Add(new myParameters { ParameterName = "@year", mytype = SqlDbType.NVarChar, Value = govdata.year });
                con.myparameters.Add(new myParameters { ParameterName = "@day", mytype = SqlDbType.NVarChar, Value = govdata.seldate });
                DataTable GovDueDetails = con.GetDataTable("sp_GetGovDueDetails");
                string name = GovDueDetails.Rows[0][0].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][0].ToString();
                string position = GovDueDetails.Rows[0][1].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][1].ToString();
                string company = GovDueDetails.Rows[0][2].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][2].ToString();
                string companyadd = GovDueDetails.Rows[0][3].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][3].ToString();
                string payoutmonthyear = GovDueDetails.Rows[0][4].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][4].ToString();
                string SemiMonthAmount = GovDueDetails.Rows[0][5].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][5].ToString();
                string SSSPremium = GovDueDetails.Rows[0][6].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][6].ToString();
                string PhilHealth = GovDueDetails.Rows[0][7].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][7].ToString();
                string HomeFund = GovDueDetails.Rows[0][8].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][8].ToString();
                string SSSLoans = GovDueDetails.Rows[0][9].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][9].ToString();
                string SSSOtherLoans = GovDueDetails.Rows[0][10].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][10].ToString();
                string HDMFLoans = GovDueDetails.Rows[0][11].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][11].ToString();
                string deadline = GovDueDetails.Rows[0][12].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][12].ToString();
                string ppname = GovDueDetails.Rows[0][13].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][13].ToString();
                string GrossTaxIncome = GovDueDetails.Rows[0][14].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][14].ToString();
                string NonTaxIncome = GovDueDetails.Rows[0][15].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][15].ToString();
                string NetTaxIncome = GovDueDetails.Rows[0][16].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][16].ToString();
                string CompanyCode = GovDueDetails.Rows[0][17].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][17].ToString();
                string pathmoto;
                pathmoto = "GovDueBlank";
                PdfReader reader1 = new PdfReader(HttpContext.Current.Server.MapPath("~/Files/PayrollOutSourcing/PDFTemplate/") + pathmoto + ".pdf");
                string reader2 = HttpContext.Current.Server.MapPath("~/Files/PayrollOutSourcing/PDFTemplate/") + pathmoto;
                var pdfReader1 = new PdfReader(reader1);
                AcroFields af = pdfReader1.AcroFields;


                string formFile = reader2;
                // Output file path
                string newFile = HttpContext.Current.Server.MapPath("~/sFTP/PayrollOutsourcing/") + "GovDue" + DateTime.Now.ToString("dd-H-mm") + ".pdf";
                // read the template file
                PdfReader reader = new PdfReader(formFile + ".pdf");
                // instantiate PDFStamper object
                // The Output file will be created from template file and edited by the PDFStamper
                PdfStamper stamper = new PdfStamper(reader, new FileStream(newFile, FileMode.Create));

                // Object to deal with the Output file's textfields
                AcroFields fields = stamper.AcroFields;

                fields.SetField("date", DateTime.Now.ToString("dd MMMMM yyyy"));
                fields.SetField("nameadd", name +
                                      Environment.NewLine + position +
                                      Environment.NewLine + company +
                                      Environment.NewLine + companyadd);
                fields.SetField("greetings", "Dear Sir/Madam,");
                fields.SetField("firstpar", "Please prepare cheques immediately for the payment of the following government agencies covering the remittances of SSS, PHILHEALTH, HDMF and BIR premiums/withholding taxes for the the month of " + payoutmonthyear + " for " + company + " : " +
                                      Environment.NewLine +
                                      Environment.NewLine + "           CCLI-SEMI" +
                                      Environment.NewLine + "           BIR WH Tax : SEMI-MONTHLY              PHP " + SemiMonthAmount + ""
                                      +
                                      Environment.NewLine + "               (Note: BIR Form 1601 Gross Taxable Income should be " + GrossTaxIncome + ")  " +
                                      Environment.NewLine + "               (Note: BIR Form 1601 Non-Taxable Income should be " + NonTaxIncome + ") " +
                                      Environment.NewLine + "               (Note: BIR Form 1601 NET Taxable Income should be " + NetTaxIncome + ")  ");

                fields.SetField("secondpar", "           Social Security System Premium :           PHP " + SSSPremium + "" +
                       Environment.NewLine + "           PhilHealth Insurance Corp.          :           PHP " + PhilHealth + "" +
                       Environment.NewLine + "           Home Dev't. Mutual Fund            :           PHP " + HomeFund + "" +
                       Environment.NewLine + "           SSS SL/EL/CL Loans                   :           PHP " + SSSLoans + "" +
                       Environment.NewLine + "           SSS Other Loans                         :           PHP " + SSSOtherLoans + "" +
                       Environment.NewLine + "           HDMF Loans                                :           PHP " + HDMFLoans + "");


                fields.SetField("thirdpar", "   Kindly double check the above payments against your payroll register and payroll journal entries and remit to the corresponding agencies through your bank on or before the " + deadline + ". ");


                fields.SetField("farewell", "Yours truly," + Environment.NewLine +
                                      Environment.NewLine + ppname +
                                      Environment.NewLine + "Payroll Processor");
                // "Flatten" the form so it wont be editable/usable anymore
                stamper.FormFlattening = true;
                // closing the stamper
                stamper.Close();

                HttpContext.Current.Response.ContentType = "Application/pdf";
                HttpContext.Current.Response.AppendHeader("Content-Disposition", "attachment; filename= " + CompanyCode + "_GovDue_" + DateTime.Now.ToString("YYYYMM") + ".pdf");
                //output
                HttpContext.Current.Response.TransmitFile(HttpContext.Current.Server.MapPath("~/sFTP/PayrollOutsourcing/") + "GovDue" + DateTime.Now.ToString("dd-H-mm") + ".pdf");
                HttpContext.Current.Response.End();
                return "Successfully Running File";
            }
            catch (Exception e)
            {
                return "Error";
                throw;
            }
        }
        //GovDue End
        //GovML1 Start
        [HttpPost]
        [Route("GetGovML1")]
        public string GetGovML1(GovDueParam govdata)
        {
            try
            {
                TempConnectionStringv2();
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@tenantid", mytype = SqlDbType.NVarChar, Value = govdata.tenantid });
                con.myparameters.Add(new myParameters { ParameterName = "@month", mytype = SqlDbType.NVarChar, Value = govdata.month });
                con.myparameters.Add(new myParameters { ParameterName = "@year", mytype = SqlDbType.NVarChar, Value = govdata.year });
                DataTable GovDueDetails = con.GetDataTable("sp_GetGovGovML1Details");
                string name = GovDueDetails.Rows[0][0].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][0].ToString();
                string sssnum = GovDueDetails.Rows[0][1].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][1].ToString();
                string company = GovDueDetails.Rows[0][2].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][2].ToString();
                string companyadd = GovDueDetails.Rows[0][3].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][3].ToString();
                string amount1 = GovDueDetails.Rows[0][4].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][4].ToString();
                string amount2 = GovDueDetails.Rows[0][5].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][5].ToString();
                string saltype = GovDueDetails.Rows[0][6].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][6].ToString();
                string appmonth = GovDueDetails.Rows[0][7].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][7].ToString();
                string amount3 = GovDueDetails.Rows[0][8].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][8].ToString();
                string amount4 = GovDueDetails.Rows[0][9].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][9].ToString();
                string amount5 = GovDueDetails.Rows[0][10].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][10].ToString();
                string CompanyCode = GovDueDetails.Rows[0][11].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][11].ToString();
                string pathmoto;
                pathmoto = "GovML1";
                PdfReader reader1 = new PdfReader(HttpContext.Current.Server.MapPath("~/Files/PayrollOutSourcing/PDFTemplate/") + pathmoto + ".pdf");
                string reader2 = HttpContext.Current.Server.MapPath("~/Files/PayrollOutSourcing/PDFTemplate/") + pathmoto;
                var pdfReader1 = new PdfReader(reader1);
                AcroFields af = pdfReader1.AcroFields;


                string formFile = reader2;
                // Output file path
                string newFile = HttpContext.Current.Server.MapPath("~/sFTP/PayrollOutsourcing/") + "GovML1" + DateTime.Now.ToString("dd-H-mm") + ".pdf";
                // read the template file
                PdfReader reader = new PdfReader(formFile + ".pdf");
                // instantiate PDFStamper object
                // The Output file will be created from template file and edited by the PDFStamper
                PdfStamper stamper = new PdfStamper(reader, new FileStream(newFile, FileMode.Create));

                // Object to deal with the Output file's textfields
                AcroFields fields = stamper.AcroFields;

                fields.SetField("Amount1", amount1);
                fields.SetField("Amount2", amount2);
                fields.SetField("Amount3", amount3);
                fields.SetField("Amount4", amount4);
                //fields.SetField("Amount5", DateTime.Now.ToString("dd MMMMM yyyy"));
                //fields.SetField("Amount6", DateTime.Now.ToString("dd MMMMM yyyy"));
                //fields.SetField("Amount7", DateTime.Now.ToString("dd MMMMM yyyy"));
                //fields.SetField("Amoun8", DateTime.Now.ToString("dd MMMMM yyyy"));
                //fields.SetField("Amount9", DateTime.Now.ToString("dd MMMMM yyyy"));
                //fields.SetField("Amount10", DateTime.Now.ToString("dd MMMMM yyyy"));
                fields.SetField("PayType", saltype);
                fields.SetField("PayMonth", appmonth);
                fields.SetField("CashWord", ConvertToWords(amount5));
                //fields.SetField("PrintedName", DateTime.Now.ToString("dd MMMMM yyyy"));
                fields.SetField("EmployersIDNum", sssnum); //
                fields.SetField("EmployersSSNum", "");
                fields.SetField("EmployersCompName", company);
                fields.SetField("EmployersCompAdd", companyadd);
                stamper.FormFlattening = true;
                // closing the stamper
                stamper.Close();
                HttpContext.Current.Response.ContentType = "Application/pdf";
                HttpContext.Current.Response.AppendHeader("Content-Disposition", "attachment; filename= " + CompanyCode + "_GovML1_" + DateTime.Now.ToString("YYYYMM") + ".pdf");
                HttpContext.Current.Response.TransmitFile(HttpContext.Current.Server.MapPath("~/sFTP/PayrollOutsourcing/" + "GovML1" + DateTime.Now.ToString("dd-H-mm") + ".pdf"));
                HttpContext.Current.Response.End();
                return "Successfully Running File";
            }
            catch (Exception e)
            {
                return "Error";
                throw;
            }
        }
        //GovM1601C Start
        [HttpPost]
        [Route("GetGov1601C")]
        public string GetGov1601C(GovDueParam govdata)
        {
            try
            {
                TempConnectionStringv2();
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@tenantid", mytype = SqlDbType.NVarChar, Value = govdata.tenantid });
                con.myparameters.Add(new myParameters { ParameterName = "@month", mytype = SqlDbType.NVarChar, Value = govdata.month });
                con.myparameters.Add(new myParameters { ParameterName = "@year", mytype = SqlDbType.NVarChar, Value = govdata.year });
                DataTable GovDueDetails = con.GetDataTable("sp_GetGov1601-C");
                string month = GovDueDetails.Rows[0][0].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][0].ToString();//       [1month]
                string year = GovDueDetails.Rows[0][1].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][1].ToString();//        [1year],	
                string two = GovDueDetails.Rows[0][2].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][2].ToString();//         [2],		
                string three = GovDueDetails.Rows[0][3].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][3].ToString();//       [3],		
                string four = GovDueDetails.Rows[0][4].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][4].ToString();//        [4],	
                string five = GovDueDetails.Rows[0][5].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][5].ToString();//        [5],	
                string six = GovDueDetails.Rows[0][6].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][6].ToString();//         [6],	
                string seven = GovDueDetails.Rows[0][7].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][7].ToString();//       [7],	
                string eigth = GovDueDetails.Rows[0][8].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][8].ToString();//       [8]
                string nine = GovDueDetails.Rows[0][9].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][9].ToString();//        [9]
                string ten = GovDueDetails.Rows[0][10].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][10].ToString();//       [10],
                string oneone = GovDueDetails.Rows[0][11].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][11].ToString();//    [11],
                string onetwo = GovDueDetails.Rows[0][12].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][12].ToString();//    [12],
                string onethree = GovDueDetails.Rows[0][13].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][13].ToString();//  [13],
                string onefour = GovDueDetails.Rows[0][14].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][14].ToString();//   [14],
                string onefive = GovDueDetails.Rows[0][15].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][15].ToString();//   [15]
                string onesixa = GovDueDetails.Rows[0][16].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][16].ToString();//   [16A],
                string onesivb = GovDueDetails.Rows[0][17].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][17].ToString();//   [16B],
                string onesixc = GovDueDetails.Rows[0][18].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][18].ToString();//   [16C]
                string oneseven = GovDueDetails.Rows[0][19].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][19].ToString();//  [17]
                string oneeigth = GovDueDetails.Rows[0][20].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][20].ToString();//   [18]
                string onenine = GovDueDetails.Rows[0][21].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][21].ToString();//   [19]
                string twozero = GovDueDetails.Rows[0][22].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][22].ToString();//   [20]
                string twoonea = GovDueDetails.Rows[0][23].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][23].ToString();//   [21A]
                string twooneb = GovDueDetails.Rows[0][24].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][24].ToString();//   [21B]
                string twotwo = GovDueDetails.Rows[0][25].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][25].ToString();//    [22],
                string twothree = GovDueDetails.Rows[0][26].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][26].ToString();//  [23]
                string twofoura = GovDueDetails.Rows[0][27].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][27].ToString();//  [24A],
                string twofourb = GovDueDetails.Rows[0][28].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][28].ToString();//  [24B],
                string twofourc = GovDueDetails.Rows[0][29].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][29].ToString();//  [24C],
                string twofourd = GovDueDetails.Rows[0][30].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][30].ToString();//  [24D],
                string twofive = GovDueDetails.Rows[0][31].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][31].ToString();//   [25]
                string twoseven = GovDueDetails.Rows[0][32].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][32].ToString();//  [27],
                string position = GovDueDetails.Rows[0][33].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][33].ToString();//  [POSITION],
                string twoeigth = GovDueDetails.Rows[0][34].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][34].ToString();//  [28],
                string twonine = GovDueDetails.Rows[0][35].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][35].ToString();//   [29],
                string threezera = GovDueDetails.Rows[0][36].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][36].ToString();// [30A]
                string threezerb = GovDueDetails.Rows[0][37].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][37].ToString();// [30B]
                string threezerc = GovDueDetails.Rows[0][38].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][38].ToString();// [30C]
                string threezerd = GovDueDetails.Rows[0][39].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][39].ToString();// [30D]
                string threeone = GovDueDetails.Rows[0][40].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][40].ToString();//  [31]
                string CompanyCode = GovDueDetails.Rows[0][41].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][41].ToString();
                string input = five;
                double partSize = 3;
                int k = 0;
                var output = input
                    .ToLookup(c => Math.Floor(k++ / partSize))
                    .Select(e => new String(e.ToArray()));
                string[] tinlist = output.ToArray();
                string tin1 = String.Join("   ", tinlist[0].Reverse());
                string tin2 = String.Join("   ", tinlist[1].Reverse());
                string tin3 = String.Join("   ", tinlist[2].Reverse());
                string tin4 = "000";
                string pathmoto = "1601C";
                PdfReader reader1 = new PdfReader(HttpContext.Current.Server.MapPath("~/Files/PayrollOutSourcing/PDFTemplate/") + pathmoto + ".pdf");
                string reader2 = HttpContext.Current.Server.MapPath("~/Files/PayrollOutSourcing/PDFTemplate/") + pathmoto;
                var pdfReader1 = new PdfReader(reader1);
                AcroFields af = pdfReader1.AcroFields;


                string formFile = reader2;
                // Output file path
                string newFile = HttpContext.Current.Server.MapPath("~/sFTP/PayrollOutsourcing/") + "1601C" + DateTime.Now.ToString("dd-H-mm") + ".pdf";
                // read the template file
                PdfReader reader = new PdfReader(formFile + ".pdf");
                // instantiate PDFStamper object
                // The Output file will be created from template file and edited by the PDFStamper
                PdfStamper stamper = new PdfStamper(reader, new FileStream(newFile, FileMode.Create));

                // Object to deal with the Output file's textfields
                AcroFields fields = stamper.AcroFields;
                string month2 = String.Join("   ", month.Reverse());
                string year2 = String.Join("   ", year.Reverse());
                fields.SetField("MONTH", String.Join("", month2.Reverse()));
                fields.SetField("YEAR", String.Join("", year2.Reverse()));
                fields.SetField("2", two);
                fields.SetField("3", three);
                fields.SetField("4", four);
                fields.SetField("TIN1", String.Join("", tin1.Reverse()));
                fields.SetField("TIN2", String.Join("", tin2.Reverse()));
                fields.SetField("TIN3", String.Join("", tin3.Reverse()));
                fields.SetField("TIN4", String.Join("   ", tin4.Reverse()));
                fields.SetField("6", six);
                fields.SetField("7", seven);
                fields.SetField("8", eigth);
                fields.SetField("9", nine);
                fields.SetField("10", ten);
                fields.SetField("11", oneone);
                fields.SetField("12", onetwo);
                fields.SetField("13", onethree);
                fields.SetField("14", onefour);
                fields.SetField("15", onefive);
                fields.SetField("16A", onesixa);
                fields.SetField("16B", onesivb);
                fields.SetField("16C", onesixc);
                fields.SetField("17", oneseven);
                fields.SetField("18", oneeigth);
                fields.SetField("19", onenine);
                fields.SetField("20", twozero);
                fields.SetField("21A", twoonea);
                fields.SetField("21B", twooneb);
                fields.SetField("22", twotwo);
                fields.SetField("23", twothree);
                fields.SetField("24A", twofoura);
                fields.SetField("24B", twofourb);
                fields.SetField("24C", twofourc);
                fields.SetField("24D", twofourd);
                fields.SetField("25", twofive);
                fields.SetField("27", twoseven);
                fields.SetField("POSITION", position);
                fields.SetField("28", twoeigth);
                fields.SetField("29", twonine);
                fields.SetField("30A", threezera);
                fields.SetField("30B", threezerb);
                fields.SetField("30C", threezerc);
                fields.SetField("30D", threezerd);
                fields.SetField("31", threeone);

                stamper.FormFlattening = true;
                stamper.Close();
                HttpContext.Current.Response.ContentType = "Application/pdf";
                HttpContext.Current.Response.AppendHeader("Content-Disposition", "attachment; filename= " + CompanyCode + "_1601C_" + DateTime.Now.ToString("YYYYMM") + ".pdf");
                HttpContext.Current.Response.TransmitFile(HttpContext.Current.Server.MapPath("~/sFTP/PayrollOutsourcing/" + "1601C" + DateTime.Now.ToString("dd-H-mm") + ".pdf"));
                HttpContext.Current.Response.End();
                return "Successfully Running File";
            }
            catch (Exception e)
            {
                return "Error";
                throw;
            }
        }
        //Gov1601c End
        //Gov1601CNEW Start
        [HttpPost]
        [Route("GetGov1601CNEW")]
        public string GetGov1601CNEW(GovDueParam govdata)
        {
            try
            {
                TempConnectionStringv2();
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@tenantid", mytype = SqlDbType.NVarChar, Value = govdata.tenantid });
                con.myparameters.Add(new myParameters { ParameterName = "@month", mytype = SqlDbType.NVarChar, Value = govdata.month });
                con.myparameters.Add(new myParameters { ParameterName = "@year", mytype = SqlDbType.NVarChar, Value = govdata.year });
                DataTable GovDueDetails = con.GetDataTable("sp_GetGov1601-C-2018");
                string month = GovDueDetails.Rows[0][0].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][0].ToString();//       [1month]
                string year = GovDueDetails.Rows[0][1].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][1].ToString();//        [1year],	
                string two = GovDueDetails.Rows[0][2].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][2].ToString();//         [2],		
                string three = GovDueDetails.Rows[0][3].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][3].ToString();//       [3],		
                string four = GovDueDetails.Rows[0][4].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][4].ToString();//        [4],	
                string five = GovDueDetails.Rows[0][5].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][5].ToString();//        [5],	
                string six = GovDueDetails.Rows[0][6].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][6].ToString();//         [6],	
                string seven = GovDueDetails.Rows[0][7].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][7].ToString();//       [7],	
                string eigth = GovDueDetails.Rows[0][8].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][8].ToString();//       [8]
                string nine = GovDueDetails.Rows[0][9].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][9].ToString();//        [9]
                string ten = GovDueDetails.Rows[0][10].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][10].ToString();//       [10],
                string oneone = GovDueDetails.Rows[0][11].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][11].ToString();//    [11],
                string onetwo = GovDueDetails.Rows[0][12].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][12].ToString();//    [12],
                string onethree = GovDueDetails.Rows[0][13].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][13].ToString();//  [13],
                string onefour = GovDueDetails.Rows[0][14].ToString().Replace(",", " ") == "" ? "N/A" : GovDueDetails.Rows[0][14].ToString().Replace(",", " ");//   [14],
                string onefive = GovDueDetails.Rows[0][15].ToString().Replace(",", " ") == "" ? "N/A" : GovDueDetails.Rows[0][15].ToString().Replace(",", " ");//   [15]
                string onesixa = GovDueDetails.Rows[0][16].ToString().Replace(",", " ") == "" ? "N/A" : GovDueDetails.Rows[0][16].ToString().Replace(",", " ");//   [16],
                string onesivb = GovDueDetails.Rows[0][17].ToString().Replace(",", " ") == "" ? "N/A" : GovDueDetails.Rows[0][17].ToString().Replace(",", " ");//   [17],
                string onesixc = GovDueDetails.Rows[0][18].ToString().Replace(",", " ") == "" ? "N/A" : GovDueDetails.Rows[0][18].ToString().Replace(",", " ");//   [18]
                string oneseven = GovDueDetails.Rows[0][19].ToString().Replace(",", " ") == "" ? "N/A" : GovDueDetails.Rows[0][19].ToString().Replace(",", " ");//  [19]
                string oneeigth = GovDueDetails.Rows[0][20].ToString().Replace(",", " ") == "" ? "N/A" : GovDueDetails.Rows[0][20].ToString().Replace(",", " ");//   [20]
                string onenine = GovDueDetails.Rows[0][21].ToString().Replace(",", " ") == "" ? "N/A" : GovDueDetails.Rows[0][21].ToString().Replace(",", " ");//   [21]
                string twozero = GovDueDetails.Rows[0][22].ToString().Replace(",", " ") == "" ? "N/A" : GovDueDetails.Rows[0][22].ToString().Replace(",", " ");//   [22]
                string twoonea = GovDueDetails.Rows[0][23].ToString().Replace(",", " ") == "" ? "N/A" : GovDueDetails.Rows[0][23].ToString().Replace(",", " ");//   [23]
                string twooneb = GovDueDetails.Rows[0][24].ToString().Replace(",", " ") == "" ? "N/A" : GovDueDetails.Rows[0][24].ToString().Replace(",", " ");//   [24]
                string twotwo = GovDueDetails.Rows[0][25].ToString().Replace(",", " ") == "" ? "N/A" : GovDueDetails.Rows[0][25].ToString().Replace(",", " ");//    [25],
                string twothree = GovDueDetails.Rows[0][26].ToString().Replace(",", " ") == "" ? "N/A" : GovDueDetails.Rows[0][26].ToString().Replace(",", " ");//  [26]
                string twofoura = GovDueDetails.Rows[0][27].ToString().Replace(",", " ") == "" ? "N/A" : GovDueDetails.Rows[0][27].ToString().Replace(",", " ");//  [27],
                string twofourb = GovDueDetails.Rows[0][28].ToString().Replace(",", " ") == "" ? "N/A" : GovDueDetails.Rows[0][28].ToString().Replace(",", " ");//  [28],
                string twofourc = GovDueDetails.Rows[0][29].ToString().Replace(",", " ") == "" ? "N/A" : GovDueDetails.Rows[0][29].ToString().Replace(",", " ");//  [29],
                string twofourd = GovDueDetails.Rows[0][30].ToString().Replace(",", " ") == "" ? "N/A" : GovDueDetails.Rows[0][30].ToString().Replace(",", " ");//  [30],
                string twofive = GovDueDetails.Rows[0][31].ToString().Replace(",", " ") == "" ? "N/A" : GovDueDetails.Rows[0][31].ToString().Replace(",", " ");//   [31]
                string twoseven = GovDueDetails.Rows[0][32].ToString().Replace(",", " ") == "" ? "N/A" : GovDueDetails.Rows[0][32].ToString().Replace(",", " ");//  [32],
                string position = GovDueDetails.Rows[0][33].ToString().Replace(",", " ") == "" ? "N/A" : GovDueDetails.Rows[0][33].ToString().Replace(",", " ");//  [33],
                string twoeigth = GovDueDetails.Rows[0][34].ToString().Replace(",", " ") == "" ? "N/A" : GovDueDetails.Rows[0][34].ToString().Replace(",", " ");//  [34],
                string twonine = GovDueDetails.Rows[0][35].ToString().Replace(",", " ") == "" ? "N/A" : GovDueDetails.Rows[0][35].ToString().Replace(",", " ");//   [35],
                string threezera = GovDueDetails.Rows[0][36].ToString().Replace(",", " ") == "" ? "N/A" : GovDueDetails.Rows[0][36].ToString().Replace(",", " ");// [w36]
                //string threezerb = GovDueDetails.Rows[0][37].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][37].ToString();// [30B]
                //string threezerc = GovDueDetails.Rows[0][38].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][38].ToString();// [30C]
                //string threezerd = GovDueDetails.Rows[0][39].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][39].ToString();// [30D]
                //string threeone = GovDueDetails.Rows[0][40].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][40].ToString();//  [31]
                string CompanyCode = GovDueDetails.Rows[0][37].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][37].ToString();
                string input = six;
                double partSize = 3;
                int k = 0;
                var output = input
                    .ToLookup(c => Math.Floor(k++ / partSize))
                    .Select(e => new String(e.ToArray()));
                string[] tinlist = output.ToArray();
                string tin1 = String.Join("   ", tinlist[0].Reverse());
                string tin2 = String.Join("   ", tinlist[1].Reverse());
                string tin3 = String.Join("   ", tinlist[2].Reverse());
                string tin4 = "000";
                string onefour1 = String.Join("   ", onefour.Reverse());
                string onefive1 = String.Join("   ", onefive.Reverse());
                string onesixa1 = String.Join("   ", onesixa.Reverse());
                string onesivb1 = String.Join("   ", onesivb.Reverse());
                string onesixc1 = String.Join("   ", onesixc.Reverse());
                string oneseven1 = String.Join("   ", oneseven.Reverse());
                string oneeigth1 = String.Join("   ", oneeigth.Reverse());
                string onenine1 = String.Join("   ", onenine.Reverse());
                string twozero1 = String.Join("   ", twozero.Reverse());
                string twoonea1 = String.Join("   ", twoonea.Reverse());
                string twooneb1 = String.Join("   ", twooneb.Reverse());
                string twotwo1 = String.Join("   ", twotwo.Reverse());
                string twothree1 = String.Join("   ", twothree.Reverse());
                string twofoura1 = String.Join("   ", twofoura.Reverse());
                string twofourb1 = String.Join("   ", twofourb.Reverse());
                string twofourc1 = String.Join("   ", twofourc.Reverse());
                string twofourd1 = String.Join("   ", twofourd.Reverse());
                string twofive1 = String.Join("   ", twofive.Reverse());
                string twoseven1 = String.Join("   ", twoseven.Reverse());
                string position1 = String.Join("   ", position.Reverse());
                string twoeigth1 = String.Join("   ", twoeigth.Reverse());
                string twonine1 = String.Join("   ", twonine.Reverse());
                string threezera1 = String.Join("   ", threezera.Reverse());
                string pathmoto = "1601C-NEW";
                PdfReader reader1 = new PdfReader(HttpContext.Current.Server.MapPath("~/Files/PayrollOutSourcing/PDFTemplate/") + pathmoto + ".pdf");
                string reader2 = HttpContext.Current.Server.MapPath("~/Files/PayrollOutSourcing/PDFTemplate/") + pathmoto;
                var pdfReader1 = new PdfReader(reader1);
                AcroFields af = pdfReader1.AcroFields;


                string formFile = reader2;
                // Output file path
                string newFile = HttpContext.Current.Server.MapPath("~/sFTP/PayrollOutsourcing/") + "1601C" + DateTime.Now.ToString("dd-H-mm") + ".pdf";
                // read the template file
                PdfReader reader = new PdfReader(formFile + ".pdf");
                // instantiate PDFStamper object
                // The Output file will be created from template file and edited by the PDFStamper
                PdfStamper stamper = new PdfStamper(reader, new FileStream(newFile, FileMode.Create));

                // Object to deal with the Output file's textfields
                AcroFields fields = stamper.AcroFields;
                string month2 = String.Join("   ", month.Reverse());
                string year2 = String.Join("   ", year.Reverse());
                fields.SetField("MONTH", String.Join("", month2.Reverse()));
                fields.SetField("YEAR", String.Join("", year2.Reverse()));
                //fields.SetField("2", two);
                //fields.SetField("3", three);
                //fields.SetField("4", four);
                fields.SetField("TIN1", String.Join("", tin1.Reverse()));
                fields.SetField("TIN2", String.Join("", tin2.Reverse()));
                fields.SetField("TIN3", String.Join("", tin3.Reverse()));
                fields.SetField("TIN4", String.Join("   ", tin4.Reverse()));
                //fields.SetField("6", six);
                fields.SetField("7", seven);
                fields.SetField("8", eigth);
                fields.SetField("9", nine);
                fields.SetField("10", ten);
                fields.SetField("11", oneone);
                fields.SetField("12", onetwo);
                fields.SetField("13", onethree);
                fields.SetField("14", String.Join("", onefour1.Reverse()).Replace(".", "  "));
                fields.SetField("15", String.Join("", onefive1.Reverse()).Replace(".", "  "));
                fields.SetField("16", String.Join("", onesixa1.Reverse()).Replace(".", "  "));
                fields.SetField("17", String.Join("", onesivb1.Reverse()).Replace(".", "  "));
                fields.SetField("18", String.Join("", onesixc1.Reverse()).Replace(".", "  "));
                fields.SetField("19", String.Join("", oneseven1.Reverse()).Replace(".", "  "));
                fields.SetField("20", String.Join("", oneeigth1.Reverse()).Replace(".", "  "));
                fields.SetField("21", String.Join("", onenine1.Reverse()).Replace(".", "  "));
                fields.SetField("22", String.Join("", twozero1.Reverse()).Replace(".", "  "));
                fields.SetField("23", String.Join("", twoonea1.Reverse()).Replace(".", "  "));
                fields.SetField("24", String.Join("", twooneb1.Reverse()).Replace(".", "  "));
                fields.SetField("25", String.Join("", twotwo1.Reverse()).Replace(".", "  "));
                fields.SetField("26", String.Join("", twothree1.Reverse()).Replace(".", "  "));
                fields.SetField("27", String.Join("", twofoura1.Reverse()).Replace(".", "  "));
                fields.SetField("28", String.Join("", twofourb1.Reverse()).Replace(".", "  "));
                fields.SetField("29", String.Join("", twofourc1.Reverse()).Replace(".", "  "));
                fields.SetField("30", String.Join("", twofourd1.Reverse()).Replace(".", "  "));
                fields.SetField("31", String.Join("", twofive1.Reverse()).Replace(".", "  "));
                fields.SetField("32", String.Join("", twoseven1.Reverse()).Replace(".", "  "));
                fields.SetField("33", String.Join("", position1.Reverse()).Replace(".", "  "));
                fields.SetField("34", String.Join("", twoeigth1.Reverse()).Replace(".", "  "));
                fields.SetField("35", String.Join("", twonine1.Reverse()).Replace(".", "  "));
                fields.SetField("36", String.Join("", threezera1.Reverse()).Replace(".", "  "));
                //fields.SetField("30B", threezera);
                //fields.SetField("30C", threezerc);
                //fields.SetField("30D", threezerd);
                //fields.SetField("31", threeone);

                stamper.FormFlattening = true;
                stamper.Close();
                HttpContext.Current.Response.ContentType = "Application/pdf";
                HttpContext.Current.Response.AppendHeader("Content-Disposition", "attachment; filename= " + CompanyCode + "_1601C_" + DateTime.Now.ToString("YYYYMM") + ".pdf");
                HttpContext.Current.Response.TransmitFile(HttpContext.Current.Server.MapPath("~/sFTP/PayrollOutsourcing/" + "1601C" + DateTime.Now.ToString("dd-H-mm") + ".pdf"));
                HttpContext.Current.Response.End();
                return "Successfully Running File";
            }
            catch (Exception e)
            {
                return "Error";
                throw;
            }
        }
        //Gov1601CNEW End
        #endregion


        //Number to words Start
        private static String ones(String Number)
        {
            int _Number = Convert.ToInt32(Number);
            String name = "";
            switch (_Number)
            {

                case 1:
                    name = "One";
                    break;
                case 2:
                    name = "Two";
                    break;
                case 3:
                    name = "Three";
                    break;
                case 4:
                    name = "Four";
                    break;
                case 5:
                    name = "Five";
                    break;
                case 6:
                    name = "Six";
                    break;
                case 7:
                    name = "Seven";
                    break;
                case 8:
                    name = "Eight";
                    break;
                case 9:
                    name = "Nine";
                    break;
            }
            return name;
        }
        private static String tens(String Number)
        {
            int _Number = Convert.ToInt32(Number);
            String name = null;
            switch (_Number)
            {
                case 10:
                    name = "Ten";
                    break;
                case 11:
                    name = "Eleven";
                    break;
                case 12:
                    name = "Twelve";
                    break;
                case 13:
                    name = "Thirteen";
                    break;
                case 14:
                    name = "Fourteen";
                    break;
                case 15:
                    name = "Fifteen";
                    break;
                case 16:
                    name = "Sixteen";
                    break;
                case 17:
                    name = "Seventeen";
                    break;
                case 18:
                    name = "Eighteen";
                    break;
                case 19:
                    name = "Nineteen";
                    break;
                case 20:
                    name = "Twenty";
                    break;
                case 30:
                    name = "Thirty";
                    break;
                case 40:
                    name = "Fourty";
                    break;
                case 50:
                    name = "Fifty";
                    break;
                case 60:
                    name = "Sixty";
                    break;
                case 70:
                    name = "Seventy";
                    break;
                case 80:
                    name = "Eighty";
                    break;
                case 90:
                    name = "Ninety";
                    break;
                default:
                    if (_Number > 0)
                    {
                        name = tens(Number.Substring(0, 1) + "0") + " " + ones(Number.Substring(1));
                    }
                    break;
            }
            return name;
        }
        private static String ConvertToWords(String numb)
        {
            String val = "", wholeNo = numb, points = "", andStr = "", pointStr = "";
            String endStr = "Peso";
            try
            {
                int decimalPlace = numb.IndexOf(".");
                if (decimalPlace > 0)
                {
                    wholeNo = numb.Substring(0, decimalPlace);
                    points = numb.Substring(decimalPlace + 1);
                    if (Convert.ToInt32(points) > 0)
                    {
                        andStr = "Pesos and ";// just to separate whole numbers from points/cents    
                        endStr = "Centavos";//Cents    
                        pointStr = tens(points);
                    }
                }
                val = String.Format("{0} {1}{2} {3}", ConvertWholeNumber(wholeNo).Trim(), andStr, pointStr, endStr);
            }
            catch { }
            return val;
        }
        private static String ConvertDecimals(String number)
        {
            String cd = "", digit = "", engOne = "";
            for (int i = 0; i < number.Length; i++)
            {
                digit = number[i].ToString();
                if (digit.Equals("0"))
                {
                    engOne = "Zero";
                }
                else
                {
                    engOne = ones(digit);
                }
                cd += " " + engOne;
            }
            return cd;
        }
        private static String ConvertWholeNumber(String Number)
        {
            string word = "";
            try
            {
                bool beginsZero = false;//tests for 0XX    
                bool isDone = false;//test if already translated    
                double dblAmt = (Convert.ToDouble(Number));
                //if ((dblAmt > 0) && number.StartsWith("0"))    
                if (dblAmt > 0)
                {//test for zero or digit zero in a nuemric    
                    beginsZero = Number.StartsWith("0");

                    int numDigits = Number.Length;
                    int pos = 0;//store digit grouping    
                    String place = "";//digit grouping name:hundres,thousand,etc...    
                    switch (numDigits)
                    {
                        case 1://ones' range    

                            word = ones(Number);
                            isDone = true;
                            break;
                        case 2://tens' range    
                            word = tens(Number);
                            isDone = true;
                            break;
                        case 3://hundreds' range    
                            pos = (numDigits % 3) + 1;
                            place = " Hundred ";
                            break;
                        case 4://thousands' range    
                        case 5:
                        case 6:
                            pos = (numDigits % 4) + 1;
                            place = " Thousand ";
                            break;
                        case 7://millions' range    
                        case 8:
                        case 9:
                            pos = (numDigits % 7) + 1;
                            place = " Million ";
                            break;
                        case 10://Billions's range    
                        case 11:
                        case 12:

                            pos = (numDigits % 10) + 1;
                            place = " Billion ";
                            break;
                        //add extra case options for anything above Billion...    
                        default:
                            isDone = true;
                            break;
                    }
                    if (!isDone)
                    {//if transalation is not done, continue...(Recursion comes in now!!)    
                        if (Number.Substring(0, pos) != "0" && Number.Substring(pos) != "0")
                        {
                            try
                            {
                                word = ConvertWholeNumber(Number.Substring(0, pos)) + place + ConvertWholeNumber(Number.Substring(pos));
                            }
                            catch { }
                        }
                        else
                        {
                            word = ConvertWholeNumber(Number.Substring(0, pos)) + ConvertWholeNumber(Number.Substring(pos));
                        }

                        //check for trailing zeros    
                        //if (beginsZero) word = " and " + word.Trim();    
                    }
                    //ignore digit grouping names    
                    if (word.Trim().Equals(place.Trim())) word = "";
                }
            }
            catch { }
            return word.Trim();
        }
    }
    //Number to words End
}
