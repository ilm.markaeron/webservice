﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using illimitadoWepAPI.Models.LoanMakerModels;
using illimitadoWepAPI.MyClass;
using System.Data;
using System.Configuration;
using System.Reflection;

namespace illimitadoWepAPI.Controllers.LoanMakerController
{
    public class LoanMultiselectItemsController : ApiController
    {
        public void TempConnectionString()
        {
            var settings = ConfigurationManager.ConnectionStrings["connectionstring"];
            var fi = typeof(ConfigurationElement).GetField(
                          "_bReadOnly",
                          BindingFlags.Instance | BindingFlags.NonPublic);
            fi.SetValue(settings, false);
            settings.ConnectionString = "Data Source=livedb-ilm.c3srcolvclfy.ap-southeast-1.rds.amazonaws.com,1412; Database = ILM_DevSvr;User Id=ILM_LiveDBUser;Password=ILMV3ry53cur3dMGTpa55w0rd2020;";
        }
        [HttpPost]
        [Route("LoanMultiSelectItems")]
        public List<LoanMutliselectItems> MultiSelectItems(Master master)
        {
            TempConnectionString();
            Connection con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@EmpLevel", mytype = SqlDbType.NVarChar, Value = master.EmpLevel == "" ? (object)DBNull.Value : master.EmpLevel });
            con.myparameters.Add(new myParameters { ParameterName = "@Gender", mytype = SqlDbType.NVarChar, Value = master.Gender == "" ? (object)DBNull.Value : master.Gender });
            con.myparameters.Add(new myParameters { ParameterName = "@MonthlySalary", mytype = SqlDbType.NVarChar, Value = master.MonthlySalary == "" ? (object)DBNull.Value : master.MonthlySalary });
            con.myparameters.Add(new myParameters { ParameterName = "@Tenure", mytype = SqlDbType.NVarChar, Value = master.Tenure == "" ? (object)DBNull.Value : master.Tenure });
            con.myparameters.Add(new myParameters { ParameterName = "@Industry", mytype = SqlDbType.NVarChar, Value = master.Industry == "" ? (object)DBNull.Value : master.Industry });
            DataTable DT = con.GetDataTable("sp_FirstTime");
            List<LoanMutliselectItems> ListReturned = new List<LoanMutliselectItems>();
            for (int i = 0; i < DT.Rows.Count; i++)
            {
                LoanMutliselectItems LMSI = new LoanMutliselectItems();
                LMSI.Value = DT.Rows[i]["EmpID"].ToString();
                LMSI.Text = DT.Rows[i]["EmpName"].ToString();
                ListReturned.Add(LMSI);
            }
            return ListReturned;
        }
        [HttpPost]
        [Route("LoanMainInsert")]
        public string loanMainInsert(LoanMain loanInsert)
        {
            loanInsert.b64 = loanInsert.b64.Replace("data:image/jpeg;base64,", "");
            loanInsert.b64 = loanInsert.b64.Replace("data:image/png;base64,", "");
            try
            {
                TempConnectionString();
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@ID", mytype = SqlDbType.NVarChar, Value = loanInsert._ID });
                con.myparameters.Add(new myParameters { ParameterName = "@LoanType", mytype = SqlDbType.NVarChar, Value = loanInsert._LoanType });
                string Country = "";
                Country = loanInsert._CountryID.Replace("\"", "");
                Country = Country.Replace("[", "");
                Country = Country.Replace("]", "");
                con.myparameters.Add(new myParameters { ParameterName = "@CountryID", mytype = SqlDbType.NVarChar, Value = Country });
                string Region = "";
                Region = loanInsert._RegionStateID.Replace("\"", "");
                Region = Region.Replace("[", "");
                Region = Region.Replace("]", "");
                con.myparameters.Add(new myParameters { ParameterName = "@RegionStateID", mytype = SqlDbType.NVarChar, Value = Region });
                string City = "";
                City = loanInsert._CityMunicipalityID.Replace("\"", "");
                City = City.Replace("[", "");
                City = City.Replace("]", "");
                con.myparameters.Add(new myParameters { ParameterName = "@CityMunicipalityID", mytype = SqlDbType.NVarChar, Value = City });
                string ResidentialStatus = "";
                ResidentialStatus = loanInsert._ResidentialStatus.Replace("\"", "");
                ResidentialStatus = ResidentialStatus.Replace("[", "");
                ResidentialStatus = ResidentialStatus.Replace("]", "");
                con.myparameters.Add(new myParameters { ParameterName = "@ResidentialStatus", mytype = SqlDbType.NVarChar, Value = ResidentialStatus });
                con.myparameters.Add(new myParameters { ParameterName = "@NoCarsOwned", mytype = SqlDbType.NVarChar, Value = loanInsert._NoCarsOwned });
                string TypeOfEmployement = "";
                TypeOfEmployement = loanInsert._TypeOfEmployement.Replace("\"", "");
                TypeOfEmployement = TypeOfEmployement.Replace("[", "");
                TypeOfEmployement = TypeOfEmployement.Replace("]", "");
                con.myparameters.Add(new myParameters { ParameterName = "@TypeOfEmployement", mytype = SqlDbType.NVarChar, Value = TypeOfEmployement });
                string EmployeeStatus = "";
                EmployeeStatus = loanInsert._EmployeeStatus.Replace("\"", "");
                EmployeeStatus = EmployeeStatus.Replace("[", "");
                EmployeeStatus = EmployeeStatus.Replace("]", "");
                con.myparameters.Add(new myParameters { ParameterName = "@EmployeeStatus", mytype = SqlDbType.NVarChar, Value = EmployeeStatus });
                string Industry = "";
                Industry = loanInsert._IndustryID.Replace("\"", "");
                Industry = Industry.Replace("[", "");
                Industry = Industry.Replace("]", "");
                con.myparameters.Add(new myParameters { ParameterName = "@IndustryID", mytype = SqlDbType.NVarChar, Value = Industry });
                string Company = "";
                Company = loanInsert._CompanyID.Replace("\"", "");
                Company = Company.Replace("[", "");
                Company = Company.Replace("]", "");
                con.myparameters.Add(new myParameters { ParameterName = "@CompanyID", mytype = SqlDbType.NVarChar, Value = Company });
                con.myparameters.Add(new myParameters { ParameterName = "@RankDD", mytype = SqlDbType.NVarChar, Value = loanInsert._RankDD });
                string Rank = "";
                Rank = loanInsert._Rank.Replace("\"", "");
                Rank = Rank.Replace("[", "");
                Rank = Rank.Replace("]", "");
                con.myparameters.Add(new myParameters { ParameterName = "@Rank", mytype = SqlDbType.NVarChar, Value = Rank });
                con.myparameters.Add(new myParameters { ParameterName = "@TenureDD", mytype = SqlDbType.NVarChar, Value = loanInsert._TenureDD });
                con.myparameters.Add(new myParameters { ParameterName = "@TenureNo", mytype = SqlDbType.NVarChar, Value = loanInsert._TenureNo });
                con.myparameters.Add(new myParameters { ParameterName = "@TenureType", mytype = SqlDbType.NVarChar, Value = loanInsert._TenureType });
                con.myparameters.Add(new myParameters { ParameterName = "@SecurityBankHolder", mytype = SqlDbType.NVarChar, Value = loanInsert._SecurityBankHolder });
                con.myparameters.Add(new myParameters { ParameterName = "@CreditCardHolder", mytype = SqlDbType.NVarChar, Value = loanInsert._CreditCardHolder });
                //con.myparameters.Add(new myParameters { ParameterName = "@ExitPayRangeFromDD", mytype = SqlDbType.NVarChar, Value = loanInsert._ExitPayRangeFromDD });
                con.myparameters.Add(new myParameters { ParameterName = "@ExitPayRangeFromNo", mytype = SqlDbType.NVarChar, Value = loanInsert._ExitPayRangeFromNo });
                //con.myparameters.Add(new myParameters { ParameterName = "@ExitPayRangeToDD", mytype = SqlDbType.NVarChar, Value = loanInsert._ExitPayRangeToDD });
                con.myparameters.Add(new myParameters { ParameterName = "@ExitPayRangeToNo", mytype = SqlDbType.NVarChar, Value = loanInsert._ExitPayRangeToNo });
                //con.myparameters.Add(new myParameters { ParameterName = "@SalaryRangeFromDD", mytype = SqlDbType.NVarChar, Value = loanInsert._SalaryRangeFromDD });
                con.myparameters.Add(new myParameters { ParameterName = "@SalaryRangeFromNo", mytype = SqlDbType.NVarChar, Value = loanInsert._SalaryRangeFromNo });
                //con.myparameters.Add(new myParameters { ParameterName = "@SalaryRangeToDD", mytype = SqlDbType.NVarChar, Value = loanInsert._SalaryRangeToDD });
                con.myparameters.Add(new myParameters { ParameterName = "@SalaryRangeToNo", mytype = SqlDbType.NVarChar, Value = loanInsert._SalaryRangeToNo });
                //con.myparameters.Add(new myParameters { ParameterName = "@CreditScoreFromDD", mytype = SqlDbType.NVarChar, Value = loanInsert._CreditScoreFromDD });
                con.myparameters.Add(new myParameters { ParameterName = "@CreditScoreFromNo", mytype = SqlDbType.NVarChar, Value = loanInsert._CreditScoreFromNo });
                //con.myparameters.Add(new myParameters { ParameterName = "@CreditScoreToDD", mytype = SqlDbType.NVarChar, Value = loanInsert._CreditScoreToDD });
                con.myparameters.Add(new myParameters { ParameterName = "@CreditScoreToNo", mytype = SqlDbType.NVarChar, Value = loanInsert._CreditScoreToNo });
                con.myparameters.Add(new myParameters { ParameterName = "@CriminalOffenseDD", mytype = SqlDbType.NVarChar, Value = loanInsert._CriminalOffenseDD });
                con.myparameters.Add(new myParameters { ParameterName = "@CriminalOffenseNo", mytype = SqlDbType.NVarChar, Value = loanInsert._CriminalOffenseNo });
                string Disciplinary = "";
                Disciplinary = loanInsert._DisciplinaryDD1.Replace("\"", "");
                Disciplinary = Disciplinary.Replace("[", "");
                Disciplinary = Disciplinary.Replace("]", "");
                con.myparameters.Add(new myParameters { ParameterName = "@DisciplinaryDD1", mytype = SqlDbType.NVarChar, Value = Disciplinary });
                con.myparameters.Add(new myParameters { ParameterName = "@DisciplinaryDD2", mytype = SqlDbType.NVarChar, Value = loanInsert._DisciplinaryDD2 });
                con.myparameters.Add(new myParameters { ParameterName = "@DisciplinaryNo", mytype = SqlDbType.NVarChar, Value = loanInsert._DisciplinaryNo });
                //con.myparameters.Add(new myParameters { ParameterName = "@AttritionFromDD", mytype = SqlDbType.NVarChar, Value = loanInsert._AttritionFromDD });
                con.myparameters.Add(new myParameters { ParameterName = "@AttritionFromNo", mytype = SqlDbType.NVarChar, Value = loanInsert._AttritionFromNo });
                //con.myparameters.Add(new myParameters { ParameterName = "@AttritionToDD", mytype = SqlDbType.NVarChar, Value = loanInsert._AttritionToDD });
                con.myparameters.Add(new myParameters { ParameterName = "@AttritionToNo", mytype = SqlDbType.NVarChar, Value = loanInsert._AttritionToNo });
                //con.myparameters.Add(new myParameters { ParameterName = "@EmpReliabilityFromDD", mytype = SqlDbType.NVarChar, Value = loanInsert._EmpReliabilityFromDD });
                con.myparameters.Add(new myParameters { ParameterName = "@EmpReliabilityFromNo", mytype = SqlDbType.NVarChar, Value = loanInsert._EmpReliabilityFromNo });
                //con.myparameters.Add(new myParameters { ParameterName = "@EmpReliabilityToDD", mytype = SqlDbType.NVarChar, Value = loanInsert._EmpReliabilityToDD });
                con.myparameters.Add(new myParameters { ParameterName = "@EmpReliabilityToNo", mytype = SqlDbType.NVarChar, Value = loanInsert._EmpReliabilityToNo });
                string Gender = "";
                Gender = loanInsert._Gender.Replace("\"", "");
                Gender = Gender.Replace("[", "");
                Gender = Gender.Replace("]", "");
                Gender = Gender.Replace("Female", "F");
                Gender = Gender.Replace("Male", "M");
                con.myparameters.Add(new myParameters { ParameterName = "@Gender", mytype = SqlDbType.NVarChar, Value = Gender });
                //con.myparameters.Add(new myParameters { ParameterName = "@AgeFromDD", mytype = SqlDbType.NVarChar, Value = loanInsert._AgeFromDD });
                con.myparameters.Add(new myParameters { ParameterName = "@AgeFromNo", mytype = SqlDbType.NVarChar, Value = loanInsert._AgeFromNo });
                //con.myparameters.Add(new myParameters { ParameterName = "@AgeToDD", mytype = SqlDbType.NVarChar, Value = loanInsert._AgeToDD });
                con.myparameters.Add(new myParameters { ParameterName = "@AgeToNo", mytype = SqlDbType.NVarChar, Value = loanInsert._AgeToNo });
                string TaxStatus = "";
                TaxStatus = loanInsert._TaxStatus.Replace("\"", "");
                TaxStatus = TaxStatus.Replace("[", "");
                TaxStatus = TaxStatus.Replace("]", "");
                con.myparameters.Add(new myParameters { ParameterName = "@TaxStatus", mytype = SqlDbType.NVarChar, Value = TaxStatus });
                string Dependent = "";
                Dependent = loanInsert._DependentDetails.Replace("\"", "");
                Dependent = Dependent.Replace("[", "");
                Dependent = Dependent.Replace("]", "");
                con.myparameters.Add(new myParameters { ParameterName = "@DependentDetails", mytype = SqlDbType.NVarChar, Value = Dependent });
                con.myparameters.Add(new myParameters { ParameterName = "@DepDetAgeDD", mytype = SqlDbType.NVarChar, Value = loanInsert._DepDetAgeDD });
                con.myparameters.Add(new myParameters { ParameterName = "@DepDetAgeNo", mytype = SqlDbType.NVarChar, Value = loanInsert._DepDetAgeNo });
                string Educational = "";
                Educational = loanInsert._EducationalAttainment.Replace("\"", "");
                Educational = Educational.Replace("[", "");
                Educational = Educational.Replace("]", "");
                con.myparameters.Add(new myParameters { ParameterName = "@EducationalAttainment", mytype = SqlDbType.NVarChar, Value = Educational });
                con.myparameters.Add(new myParameters { ParameterName = "@Img", mytype = SqlDbType.NVarChar, Value = loanInsert.b64 });
                con.myparameters.Add(new myParameters { ParameterName = "@NoCarsOwnedDD", mytype = SqlDbType.NVarChar, Value = loanInsert._NoCarsOwnedDD });
                DateTime ReqDateTime = DateTime.Parse(loanInsert._RequestDateTime);
                con.myparameters.Add(new myParameters { ParameterName = "@RequestDateTime", mytype = SqlDbType.NVarChar, Value = ReqDateTime.ToString("yyyy-MM-dd HH:mm") });
                con.myparameters.Add(new myParameters { ParameterName = "@CreatedBy", mytype = SqlDbType.NVarChar, Value = loanInsert._CreatedBy });
                con.myparameters.Add(new myParameters { ParameterName = "@ModifiedBy", mytype = SqlDbType.NVarChar, Value = loanInsert._ModifiedBy });
                return con.ExecuteScalar("sp_LoanMaker_Insert").ToString();
            }
            catch (Exception)
            {
                return "False";
            }

        }

        [HttpPost]
        [Route("AutoLoanDetailsInsert")]
        public string autoLoanDetailsInsert(AutoLoanDetails loanInsert)
        {
            try
            {
                TempConnectionString();
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@LoanID", mytype = SqlDbType.NVarChar, Value = loanInsert._LoanID });
                con.myparameters.Add(new myParameters { ParameterName = "@LoanName", mytype = SqlDbType.NVarChar, Value = loanInsert._LoanName });
                con.myparameters.Add(new myParameters { ParameterName = "@DebtIncomeDD", mytype = SqlDbType.NVarChar, Value = loanInsert._DebtIncomeDD });
                con.myparameters.Add(new myParameters { ParameterName = "@DebtIncomeNo", mytype = SqlDbType.NVarChar, Value = loanInsert._DebtIncomeNo });
                con.myparameters.Add(new myParameters { ParameterName = "@AmortizationType", mytype = SqlDbType.NVarChar, Value = loanInsert._AmortizationType });
                con.myparameters.Add(new myParameters { ParameterName = "@CarType", mytype = SqlDbType.NVarChar, Value = loanInsert._CarType });
                con.myparameters.Add(new myParameters { ParameterName = "@CashPriceOpt", mytype = SqlDbType.NVarChar, Value = loanInsert._CashPriceOpt });
                //con.myparameters.Add(new myParameters { ParameterName = "@CashPriceFromDD", mytype = SqlDbType.NVarChar, Value = loanInsert._CashPriceFromDD });
                con.myparameters.Add(new myParameters { ParameterName = "@CashPriceFromNo", mytype = SqlDbType.NVarChar, Value = loanInsert._CashPriceFromNo });
                //con.myparameters.Add(new myParameters { ParameterName = "@CashPriceToDD", mytype = SqlDbType.NVarChar, Value = loanInsert._CashPriceToDD });
                con.myparameters.Add(new myParameters { ParameterName = "@CashPriceToNo", mytype = SqlDbType.NVarChar, Value = loanInsert._CashPriceToNo });
                con.myparameters.Add(new myParameters { ParameterName = "@CashPrice", mytype = SqlDbType.NVarChar, Value = loanInsert._CashPrice });
                con.myparameters.Add(new myParameters { ParameterName = "@CashPriceDD", mytype = SqlDbType.NVarChar, Value = loanInsert._CashPriceDD });
                con.myparameters.Add(new myParameters { ParameterName = "@PaymentOption", mytype = SqlDbType.NVarChar, Value = loanInsert._PaymentOption });
                con.myparameters.Add(new myParameters { ParameterName = "@LoanTerm", mytype = SqlDbType.NVarChar, Value = loanInsert._LoanTerm });
                con.myparameters.Add(new myParameters { ParameterName = "@InterestRate", mytype = SqlDbType.NVarChar, Value = loanInsert._InterestRate });
                con.myparameters.Add(new myParameters { ParameterName = "@InterestType", mytype = SqlDbType.NVarChar, Value = loanInsert._InterestType });
                con.myparameters.Add(new myParameters { ParameterName = "@InterestOptionNo", mytype = SqlDbType.NVarChar, Value = loanInsert._InterestOptionNo });
                con.myparameters.Add(new myParameters { ParameterName = "@InterestOptionDateType", mytype = SqlDbType.NVarChar, Value = loanInsert._InterestOptionDateType });
                con.myparameters.Add(new myParameters { ParameterName = "@PaymentDate", mytype = SqlDbType.NVarChar, Value = loanInsert._PaymentDate });
                con.myparameters.Add(new myParameters { ParameterName = "@LimitedSlotOpt", mytype = SqlDbType.NVarChar, Value = loanInsert._LimitedSlotOpt });
                con.myparameters.Add(new myParameters { ParameterName = "@LimitedSlotNo", mytype = SqlDbType.NVarChar, Value = loanInsert._LimitedSlotNo });
                return con.ExecuteScalar("sp_LoanMaker_AutoLoan_Insert");
                //return "True";
            }
            catch (Exception)
            {
                return "False";
            }

        }

        [HttpPost]
        [Route("HomeLoanDetailsInsert")]
        public string homeLoanDetailsInsert(HomeLoanDetails loanInsert)
        {
            try
            {
                TempConnectionString();
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@LoanID", mytype = SqlDbType.NVarChar, Value = loanInsert._LoanID });
                con.myparameters.Add(new myParameters { ParameterName = "@LoanName", mytype = SqlDbType.NVarChar, Value = loanInsert._LoanName });
                con.myparameters.Add(new myParameters { ParameterName = "@DebtIncomeDD", mytype = SqlDbType.NVarChar, Value = loanInsert._DebtIncomeDD });
                con.myparameters.Add(new myParameters { ParameterName = "@DebtIncomeNo", mytype = SqlDbType.NVarChar, Value = loanInsert._DebtIncomeNo });
                con.myparameters.Add(new myParameters { ParameterName = "@PropertyType", mytype = SqlDbType.NVarChar, Value = loanInsert._PropertyType });
                con.myparameters.Add(new myParameters { ParameterName = "@PropertyStatus", mytype = SqlDbType.NVarChar, Value = loanInsert._PropertyStatus });
                con.myparameters.Add(new myParameters { ParameterName = "@CashPriceOpt", mytype = SqlDbType.NVarChar, Value = loanInsert._CashPriceOpt });
                //con.myparameters.Add(new myParameters { ParameterName = "@CPO1FromDD", mytype = SqlDbType.NVarChar, Value = loanInsert._CPO1FromDD });
                con.myparameters.Add(new myParameters { ParameterName = "@CPO1FromNo", mytype = SqlDbType.NVarChar, Value = loanInsert._CPO1FromNo });
                //con.myparameters.Add(new myParameters { ParameterName = "@CPO1ToDD", mytype = SqlDbType.NVarChar, Value = loanInsert._CPO1ToDD });
                con.myparameters.Add(new myParameters { ParameterName = "@CPO1ToNo", mytype = SqlDbType.NVarChar, Value = loanInsert._CPO1ToNo });
                con.myparameters.Add(new myParameters { ParameterName = "@CPO2No", mytype = SqlDbType.NVarChar, Value = loanInsert._CPO2No });
                con.myparameters.Add(new myParameters { ParameterName = "@CPO2DD", mytype = SqlDbType.NVarChar, Value = loanInsert._CPO2DD });
                con.myparameters.Add(new myParameters { ParameterName = "@PaymentOption", mytype = SqlDbType.NVarChar, Value = loanInsert._PaymentOption });
                con.myparameters.Add(new myParameters { ParameterName = "@LoanTerm", mytype = SqlDbType.NVarChar, Value = loanInsert._LoanTerm });
                con.myparameters.Add(new myParameters { ParameterName = "@InterestRate", mytype = SqlDbType.NVarChar, Value = loanInsert._InterestRate });
                con.myparameters.Add(new myParameters { ParameterName = "@InterestType", mytype = SqlDbType.NVarChar, Value = loanInsert._InterestType });
                con.myparameters.Add(new myParameters { ParameterName = "@InterestOptionNo", mytype = SqlDbType.NVarChar, Value = loanInsert._InterestOptionNo });
                con.myparameters.Add(new myParameters { ParameterName = "@InterestOptionDD", mytype = SqlDbType.NVarChar, Value = loanInsert._InterestOptionDD });
                con.myparameters.Add(new myParameters { ParameterName = "@PaymentDate", mytype = SqlDbType.NVarChar, Value = loanInsert._PaymentDate });
                con.myparameters.Add(new myParameters { ParameterName = "@LimitedSlotsOpt", mytype = SqlDbType.NVarChar, Value = loanInsert._LimitedSlotsOpt });
                con.myparameters.Add(new myParameters { ParameterName = "@LimitedSlotsNo", mytype = SqlDbType.NVarChar, Value = loanInsert._LimitedSlotsNo });
                con.myparameters.Add(new myParameters { ParameterName = "@BrokerName", mytype = SqlDbType.NVarChar, Value = loanInsert._BrokerName });
                con.myparameters.Add(new myParameters { ParameterName = "@BrokerContactNo", mytype = SqlDbType.NVarChar, Value = loanInsert._BrokerContactNo });
                con.myparameters.Add(new myParameters { ParameterName = "@ProjectName", mytype = SqlDbType.NVarChar, Value = loanInsert._ProjectName });
                con.myparameters.Add(new myParameters { ParameterName = "@Developer", mytype = SqlDbType.NVarChar, Value = loanInsert._Developer });
                con.myparameters.Add(new myParameters { ParameterName = "@PropertyContactPerson", mytype = SqlDbType.NVarChar, Value = loanInsert._PropertyContactPerson });
                con.myparameters.Add(new myParameters { ParameterName = "@PropertyContactNo", mytype = SqlDbType.NVarChar, Value = loanInsert._PropertyContactNo });
                con.myparameters.Add(new myParameters { ParameterName = "@AppraisedProperty", mytype = SqlDbType.NVarChar, Value = loanInsert._AppraisedProperty });
                return con.ExecuteScalar("sp_LoanMaker_HomeLoan_Insert");
                //return "True";
            }
            catch (Exception)
            {
                return "False";
            }

        }

        [HttpPost]
        [Route("PersonalLoanDetailsInsert")]
        public string personalLoanDetailsInsert(PersonalLoanDetails loanInsert)
        {
            try
            {
                TempConnectionString();
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@LoanID", mytype = SqlDbType.NVarChar, Value = loanInsert._LoanID });
                con.myparameters.Add(new myParameters { ParameterName = "@LoanName", mytype = SqlDbType.NVarChar, Value = loanInsert._LoanName });
                con.myparameters.Add(new myParameters { ParameterName = "@LoanAmountOpt", mytype = SqlDbType.NVarChar, Value = loanInsert._LoanAmountOpt });
                con.myparameters.Add(new myParameters { ParameterName = "@LoanAmount", mytype = SqlDbType.NVarChar, Value = loanInsert._LoanAmount });
                con.myparameters.Add(new myParameters { ParameterName = "@LoanAmountPercent", mytype = SqlDbType.NVarChar, Value = loanInsert._LoanAmountPercent });
                con.myparameters.Add(new myParameters { ParameterName = "@LoanAmountDD", mytype = SqlDbType.NVarChar, Value = loanInsert._LoanAmountDD });
                con.myparameters.Add(new myParameters { ParameterName = "@DebtIncomeRatioDD", mytype = SqlDbType.NVarChar, Value = loanInsert._DebtIncomeRatioDD });
                con.myparameters.Add(new myParameters { ParameterName = "@DebtIncomeRatio", mytype = SqlDbType.NVarChar, Value = loanInsert._DebtIncomeRatio });
                con.myparameters.Add(new myParameters { ParameterName = "@InterestRate", mytype = SqlDbType.NVarChar, Value = loanInsert._InterestRate });
                con.myparameters.Add(new myParameters { ParameterName = "@InterestTypeDD", mytype = SqlDbType.NVarChar, Value = loanInsert._InterestTypeDD });
                con.myparameters.Add(new myParameters { ParameterName = "@LoanTerm", mytype = SqlDbType.NVarChar, Value = loanInsert._LoanTerm });
                con.myparameters.Add(new myParameters { ParameterName = "@LoanTermDD", mytype = SqlDbType.NVarChar, Value = loanInsert._LoanTermDD });
                con.myparameters.Add(new myParameters { ParameterName = "@LoanTermPayback", mytype = SqlDbType.NVarChar, Value = loanInsert._LoanTermPayback });
                con.myparameters.Add(new myParameters { ParameterName = "@LimtedSlotsOpt", mytype = SqlDbType.NVarChar, Value = loanInsert._LimtedSlotsOpt });
                con.myparameters.Add(new myParameters { ParameterName = "@LimitedSlotsApp", mytype = SqlDbType.NVarChar, Value = loanInsert._LimitedSlotsApp });
                con.myparameters.Add(new myParameters { ParameterName = "@PaymentDateDD", mytype = SqlDbType.NVarChar, Value = loanInsert._PaymentDateDD });
                return con.ExecuteScalar("sp_LoanMaker_PersonalLoan_Insert");
                //return "True";
            }
            catch (Exception)
            {
                return "False";
            }
        }
        [HttpPost]
        [Route("LoanMainSelect")]
        public List<LoanMain> LoanMain(LoanMain LoanMain)
        {
            List<LoanMain> ListReturned = new List<LoanMain>();
            DataTable DT = new DataTable();
            try
            {
                TempConnectionString();
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@ID", mytype = SqlDbType.NVarChar, Value = LoanMain._ID });
                DT = con.GetDataTable("sp_LoanMaker_Select");
                for (int i = 0; i < DT.Rows.Count; i++)
                {
                    LoanMain LM = new LoanMain();
                    LM._ID = DT.Rows[i]["ID"].ToString();
                    LM._LoanType = DT.Rows[i]["LoanType"].ToString();
                    LM._CountryID = DT.Rows[i]["CountryID"].ToString();
                    LM._RegionStateID = DT.Rows[i]["RegionStateID"].ToString();
                    LM._CityMunicipalityID = DT.Rows[i]["CityMunicipalityID"].ToString();
                    LM._ResidentialStatus = DT.Rows[i]["ResidentialStatus"].ToString();
                    LM._NoCarsOwnedDD = DT.Rows[i]["NoCarsOwnedDD"].ToString();
                    LM._NoCarsOwned = DT.Rows[i]["NoCarsOwned"].ToString();
                    LM._TypeOfEmployement = DT.Rows[i]["TypeOfEmployement"].ToString();
                    LM._EmployeeStatus = DT.Rows[i]["EmployeeStatus"].ToString();
                    LM._IndustryID = DT.Rows[i]["IndustryID"].ToString();
                    LM._CompanyID = DT.Rows[i]["CompanyID"].ToString();
                    LM._RankDD = DT.Rows[i]["RankDD"].ToString();
                    LM._Rank = DT.Rows[i]["Rank"].ToString();
                    LM._TenureDD = DT.Rows[i]["TenureDD"].ToString();
                    LM._TenureNo = DT.Rows[i]["TenureNo"].ToString();
                    LM._TenureType = DT.Rows[i]["TenureType"].ToString();
                    LM._SecurityBankHolder = DT.Rows[i]["SecurityBankHolder"].ToString();
                    LM._CreditCardHolder = DT.Rows[i]["CreditCardHolder"].ToString();
                    //LM._ExitPayRangeFromDD = DT.Rows[i]["ExitPayRangeFromDD"].ToString();
                    LM._ExitPayRangeFromNo = DT.Rows[i]["ExitPayRangeFromNo"].ToString();
                    //LM._ExitPayRangeToDD = DT.Rows[i]["ExitPayRangeToDD"].ToString();
                    LM._ExitPayRangeToNo = DT.Rows[i]["ExitPayRangeToNo"].ToString();
                    //LM._SalaryRangeFromDD = DT.Rows[i]["SalaryRangeFromDD"].ToString();
                    LM._SalaryRangeFromNo = DT.Rows[i]["SalaryRangeFromNo"].ToString();
                    //LM._SalaryRangeToDD = DT.Rows[i]["SalaryRangeToDD"].ToString();
                    LM._SalaryRangeToNo = DT.Rows[i]["SalaryRangeToNo"].ToString();
                    //LM._CreditScoreFromDD = DT.Rows[i]["CreditScoreFromDD"].ToString();
                    LM._CreditScoreFromNo = DT.Rows[i]["CreditScoreFromNo"].ToString();
                    //LM._CreditScoreToDD = DT.Rows[i]["CreditScoreToDD"].ToString();
                    LM._CreditScoreToNo = DT.Rows[i]["CreditScoreToNo"].ToString();
                    LM._CriminalOffenseDD = DT.Rows[i]["CriminalOffenseDD"].ToString();
                    LM._CriminalOffenseNo = DT.Rows[i]["CriminalOffenseNo"].ToString();
                    LM._DisciplinaryDD1 = DT.Rows[i]["DisciplinaryDD1"].ToString();
                    LM._DisciplinaryDD2 = DT.Rows[i]["DisciplinaryDD2"].ToString();
                    LM._DisciplinaryNo = DT.Rows[i]["DisciplinaryNo"].ToString();
                    //LM._AttritionFromDD = DT.Rows[i]["AttritionFromDD"].ToString();
                    LM._AttritionFromNo = DT.Rows[i]["AttritionFromNo"].ToString();
                    //LM._AttritionToDD = DT.Rows[i]["AttritionToDD"].ToString();
                    LM._AttritionToNo = DT.Rows[i]["AttritionToNo"].ToString();
                    //LM._EmpReliabilityFromDD = DT.Rows[i]["EmpReliabilityFromDD"].ToString();
                    LM._EmpReliabilityFromNo = DT.Rows[i]["EmpReliabilityFromNo"].ToString();
                    //LM._EmpReliabilityToDD = DT.Rows[i]["EmpReliabilityToDD"].ToString();
                    LM._EmpReliabilityToNo = DT.Rows[i]["EmpReliabilityToNo"].ToString();
                    LM._Gender = DT.Rows[i]["Gender"].ToString();
                    //LM._AgeFromDD = DT.Rows[i]["AgeFromDD"].ToString();
                    LM._AgeFromNo = DT.Rows[i]["AgeFromNo"].ToString();
                    //LM._AgeToDD = DT.Rows[i]["AgeToDD"].ToString();
                    LM._AgeToNo = DT.Rows[i]["AgeToNo"].ToString();
                    LM._TaxStatus = DT.Rows[i]["TaxStatus"].ToString();
                    LM._DependentDetails = DT.Rows[i]["DependentDetails"].ToString();
                    LM._DepDetAgeDD = DT.Rows[i]["DepDetAgeDD"].ToString();
                    LM._DepDetAgeNo = DT.Rows[i]["DepDetAgeNo"].ToString();
                    LM._EducationalAttainment = DT.Rows[i]["EducationalAttainment"].ToString();
                    LM.b64 = "data:image/jpeg;base64," + DT.Rows[i]["Img"].ToString();
                    DateTime RDT = Convert.ToDateTime(DT.Rows[i]["RequestDateTime"] == DBNull.Value ? "1900-01-01 00:00:00.000" : DT.Rows[i]["RequestDateTime"].ToString());
                    //LM._RequestDateTime = RDT.ToString("yyyy-MM-dd hh:mm") + ' ' + (Convert.ToInt32(RDT.ToShortTimeString().Substring(0, 2)) > 11 ? "PM" : "AM");
                    LM._RequestDateTime = RDT.ToString("yyyy-MM-dd hh:mm").Substring(0, 10) + ' ' + RDT.ToShortTimeString();
                    LM.message = "Success";
                    List<LoanMainList> LMG = new List<LoanMainList>();
                    string[] genders = DT.Rows[i]["Gender"].ToString().Split(',');
                    foreach (string item in genders)
                    {
                        LoanMainList LML = new LoanMainList();
                        if (item == "M")
                        {
                            LML.ListItem = "Male";
                        }
                        else
                        {
                            LML.ListItem = "Female";
                        }
                        LMG.Add(LML);
                    }
                    LM.GenderList = LMG;

                    LMG = new List<LoanMainList>();
                    string[] residentialstatuses = DT.Rows[i]["ResidentialStatus"].ToString().Split(',');
                    foreach (string item in residentialstatuses)
                    {
                        LoanMainList LML = new LoanMainList();
                        LML.ListItem = item;
                        LMG.Add(LML);
                    }
                    LM.ResidentialList = LMG;

                    LMG = new List<LoanMainList>();
                    string[] employement = DT.Rows[i]["TypeOfEmployement"].ToString().Split(',');
                    foreach (string item in employement)
                    {
                        LoanMainList LML = new LoanMainList();
                        LML.ListItem = item;
                        LMG.Add(LML);
                    }
                    LM.EmploymentList = LMG;

                    LMG = new List<LoanMainList>();
                    string[] employementstatus = DT.Rows[i]["EmployeeStatus"].ToString().Split(',');
                    foreach (string item in employementstatus)
                    {
                        LoanMainList LML = new LoanMainList();
                        LML.ListItem = item;
                        LMG.Add(LML);
                    }
                    LM.EmploymentStatusList = LMG;

                    LMG = new List<LoanMainList>();
                    string[] company = DT.Rows[i]["CompanyID"].ToString().Split(',');
                    foreach (string item in company)
                    {
                        LoanMainList LML = new LoanMainList();
                        LML.ListItem = item;
                        LMG.Add(LML);
                    }
                    LM.CompanyList = LMG;

                    LMG = new List<LoanMainList>();
                    string[] disciplinary = DT.Rows[i]["DisciplinaryDD1"].ToString().Split(',');
                    foreach (string item in disciplinary)
                    {
                        LoanMainList LML = new LoanMainList();
                        LML.ListItem = item;
                        LMG.Add(LML);
                    }
                    LM.DisciplinaryList = LMG;

                    LMG = new List<LoanMainList>();
                    string[] tax = DT.Rows[i]["TaxStatus"].ToString().Split(',');
                    foreach (string item in tax)
                    {
                        LoanMainList LML = new LoanMainList();
                        LML.ListItem = item;
                        LMG.Add(LML);
                    }
                    LM.TaxList = LMG;

                    LMG = new List<LoanMainList>();
                    string[] dependent = DT.Rows[i]["DependentDetails"].ToString().Split(',');
                    foreach (string item in dependent)
                    {
                        LoanMainList LML = new LoanMainList();
                        LML.ListItem = item;
                        LMG.Add(LML);
                    }
                    LM.DependentList = LMG;

                    LMG = new List<LoanMainList>();
                    string[] educational = DT.Rows[i]["EducationalAttainment"].ToString().Split(',');
                    foreach (string item in educational)
                    {
                        LoanMainList LML = new LoanMainList();
                        LML.ListItem = item;
                        LMG.Add(LML);
                    }
                    LM.EducationalList = LMG;

                    LMG = new List<LoanMainList>();
                    string[] country = DT.Rows[i]["CountryID"].ToString().Split(',');
                    foreach (string item in country)
                    {
                        LoanMainList LML = new LoanMainList();
                        LML.ListItem = item;
                        LMG.Add(LML);
                    }
                    LM.CountryList = LMG;

                    LMG = new List<LoanMainList>();
                    string[] region = DT.Rows[i]["RegionStateID"].ToString().Split(',');
                    foreach (string item in region)
                    {
                        LoanMainList LML = new LoanMainList();
                        LML.ListItem = item;
                        LMG.Add(LML);
                    }
                    LM.RegionList = LMG;

                    LMG = new List<LoanMainList>();
                    string[] city = DT.Rows[i]["CityMunicipalityID"].ToString().Split(',');
                    foreach (string item in city)
                    {
                        LoanMainList LML = new LoanMainList();
                        LML.ListItem = item;
                        LMG.Add(LML);
                    }
                    LM.CityList = LMG;

                    LMG = new List<LoanMainList>();
                    string[] industry = DT.Rows[i]["IndustryID"].ToString().Split(',');
                    foreach (string item in industry)
                    {
                        LoanMainList LML = new LoanMainList();
                        LML.ListItem = item;
                        LMG.Add(LML);
                    }
                    LM.IndustryList = LMG;

                    LMG = new List<LoanMainList>();
                    string[] rank = DT.Rows[i]["Rank"].ToString().Split(',');
                    foreach (string item in rank)
                    {
                        LoanMainList LML = new LoanMainList();
                        LML.ListItem = item;
                        LMG.Add(LML);
                    }
                    LM.RankList = LMG;

                    ListReturned.Add(LM);
                }
                return ListReturned;
            }
            catch (Exception e)
            {
                LoanMain LM = new LoanMain();
                LM.message = e.Message;
                ListReturned.Add(LM);
                return ListReturned;
            }
        }
        [HttpPost]
        [Route("AutoLoanDetailsSelect")]
        public List<AutoLoanDetails> AutoLoanDetailsSelect(AutoLoanDetails ALDs)
        {
            List<AutoLoanDetails> ListReturned = new List<AutoLoanDetails>();
            DataTable DT = new DataTable();
            try
            {
                TempConnectionString();
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@LoanID", mytype = SqlDbType.NVarChar, Value = ALDs._LoanID });
                DT = con.GetDataTable("sp_LoanMaker_LoanDetails_Select");
                for (int i = 0; i < DT.Rows.Count; i++)
                {
                    AutoLoanDetails ALD = new AutoLoanDetails();
                    ALD._LoanID = DT.Rows[i]["LoanID"].ToString();
                    ALD._LoanName = DT.Rows[i]["LoanName"].ToString();
                    ALD._DebtIncomeDD = DT.Rows[i]["DebtIncomeDD"].ToString();
                    ALD._DebtIncomeNo = DT.Rows[i]["DebtIncomeNo"].ToString();
                    ALD._AmortizationType = DT.Rows[i]["AmortizationType"].ToString();
                    ALD._CarType = DT.Rows[i]["CarType"].ToString();
                    ALD._CashPriceOpt = DT.Rows[i]["CashPriceOpt"].ToString();
                    //ALD._CashPriceFromDD = DT.Rows[i]["CashPriceFromDD"].ToString();
                    ALD._CashPriceFromNo = DT.Rows[i]["CashPriceFromNo"].ToString();
                    //ALD._CashPriceToDD = DT.Rows[i]["CashPriceToDD"].ToString();
                    ALD._CashPriceToNo = DT.Rows[i]["CashPriceToNo"].ToString();
                    ALD._CashPrice = DT.Rows[i]["CashPrice"].ToString();
                    ALD._CashPriceDD = DT.Rows[i]["CashPriceDD"].ToString();
                    ALD._PaymentOption = DT.Rows[i]["PaymentOption"].ToString();
                    ALD._LoanTerm = DT.Rows[i]["LoanTerm"].ToString();
                    ALD._InterestRate = DT.Rows[i]["InterestRate"].ToString();
                    ALD._InterestType = DT.Rows[i]["InterestType"].ToString();
                    ALD._InterestOptionNo = DT.Rows[i]["InterestOptionNo"].ToString();
                    ALD._InterestOptionDateType = DT.Rows[i]["InterestOptionDateType"].ToString();
                    ALD._PaymentDate = DT.Rows[i]["PaymentDate"].ToString();
                    ALD._LimitedSlotOpt = DT.Rows[i]["LimitedSlotOpt"].ToString();
                    ALD._LimitedSlotNo = DT.Rows[i]["LimitedSlotNo"].ToString();
                    ALD.message = "Success";
                    ListReturned.Add(ALD);
                }
                return ListReturned;
            }
            catch (Exception e)
            {
                AutoLoanDetails ALD = new AutoLoanDetails();
                ALD.message = e.Message;
                ListReturned.Add(ALD);
                return ListReturned;
            }
        }
        [HttpPost]
        [Route("HomeLoanDetailsSelect")]
        public List<HomeLoanDetails> HomeLoanDetailsSelect(HomeLoanDetails HLDs)
        {
            List<HomeLoanDetails> ListReturned = new List<HomeLoanDetails>();
            DataTable DT = new DataTable();
            try
            {
                TempConnectionString();
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@LoanID", mytype = SqlDbType.NVarChar, Value = HLDs._LoanID });
                DT = con.GetDataTable("sp_LoanMaker_LoanDetails_Select");
                for (int i = 0; i < DT.Rows.Count; i++)
                {
                    HomeLoanDetails HLD = new HomeLoanDetails();
                    HLD._LoanID = DT.Rows[i]["LoanID"].ToString();
                    HLD._LoanName = DT.Rows[i]["LoanName"].ToString();
                    HLD._DebtIncomeDD = DT.Rows[i]["DebtIncomeDD"].ToString();
                    HLD._DebtIncomeNo = DT.Rows[i]["DebtIncomeNo"].ToString();
                    HLD._PropertyType = DT.Rows[i]["PropertyType"].ToString();
                    HLD._PropertyStatus = DT.Rows[i]["PropertyStatus"].ToString();
                    HLD._CashPriceOpt = DT.Rows[i]["CashPriceOpt"].ToString();
                    //HLD._CPO1FromDD = DT.Rows[i]["CPO1FromDD"].ToString();
                    HLD._CPO1FromNo = DT.Rows[i]["CPO1FromNo"].ToString();
                    //HLD._CPO1ToDD = DT.Rows[i]["CPO1ToDD"].ToString();
                    HLD._CPO1ToNo = DT.Rows[i]["CPO1ToNo"].ToString();
                    HLD._CPO2No = DT.Rows[i]["CPO2No"].ToString();
                    HLD._CPO2DD = DT.Rows[i]["CPO2DD"].ToString();
                    HLD._PaymentOption = DT.Rows[i]["PaymentOption"].ToString();
                    HLD._LoanTerm = DT.Rows[i]["LoanTerm"].ToString();
                    HLD._InterestRate = DT.Rows[i]["InterestRate"].ToString();
                    HLD._InterestType = DT.Rows[i]["InterestType"].ToString();
                    HLD._InterestOptionNo = DT.Rows[i]["InterestOptionNo"].ToString();
                    HLD._InterestOptionDD = DT.Rows[i]["InterestOptionDD"].ToString();
                    HLD._PaymentDate = DT.Rows[i]["PaymentDate"].ToString();
                    HLD._LimitedSlotsOpt = DT.Rows[i]["LimitedSlotsOpt"].ToString();
                    HLD._LimitedSlotsNo = DT.Rows[i]["LimitedSlotsNo"].ToString();
                    HLD._BrokerName = DT.Rows[i]["BrokerName"].ToString();
                    HLD._BrokerContactNo = DT.Rows[i]["BrokerContactNo"].ToString();
                    HLD._ProjectName = DT.Rows[i]["ProjectName"].ToString();
                    HLD._Developer = DT.Rows[i]["Developer"].ToString();
                    HLD._PropertyContactPerson = DT.Rows[i]["PropertyContactPerson"].ToString();
                    HLD._PropertyContactNo = DT.Rows[i]["PropertyContactNo"].ToString();
                    HLD._AppraisedProperty = DT.Rows[i]["AppraisedProperty"].ToString();
                    HLD.message = "Success";
                    ListReturned.Add(HLD);
                }
                return ListReturned;
            }
            catch (Exception e)
            {
                HomeLoanDetails HLD = new HomeLoanDetails();
                HLD.message = e.Message;
                ListReturned.Add(HLD);
                return ListReturned;
            }
        }
        [HttpPost]
        [Route("PersonalLoanDetailsSelect")]
        public List<PersonalLoanDetails> PersonalLoanDetailsSelect(PersonalLoanDetails PLDs)
        {
            List<PersonalLoanDetails> ListReturned = new List<PersonalLoanDetails>();
            DataTable DT = new DataTable();
            try
            {
                TempConnectionString();
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@LoanID", mytype = SqlDbType.NVarChar, Value = PLDs._LoanID });
                DT = con.GetDataTable("sp_LoanMaker_LoanDetails_Select");
                for (int i = 0; i < DT.Rows.Count; i++)
                {
                    PersonalLoanDetails PLD = new PersonalLoanDetails();
                    PLD._LoanID = DT.Rows[i]["LoanID"].ToString();
                    PLD._LoanName = DT.Rows[i]["LoanName"].ToString();
                    PLD._LoanAmountOpt = DT.Rows[i]["LoanAmountOpt"].ToString();
                    PLD._LoanAmount = DT.Rows[i]["LoanAmount"].ToString();
                    PLD._LoanAmountPercent = DT.Rows[i]["LoanAmountPercent"].ToString();
                    PLD._LoanAmountDD = DT.Rows[i]["LoanAmountDD"].ToString();
                    PLD._DebtIncomeRatioDD = DT.Rows[i]["DebtIncomeRatioDD"].ToString();
                    PLD._DebtIncomeRatio = DT.Rows[i]["DebtIncomeRatio"].ToString();
                    PLD._InterestRate = DT.Rows[i]["InterestRate"].ToString();
                    PLD._InterestTypeDD = DT.Rows[i]["InterestTypeDD"].ToString();
                    PLD._LoanTerm = DT.Rows[i]["LoanTerm"].ToString();
                    PLD._LoanTermDD = DT.Rows[i]["LoanTermDD"].ToString();
                    PLD._LoanTermPayback = DT.Rows[i]["LoanTermPayback"].ToString();
                    PLD._LimtedSlotsOpt = DT.Rows[i]["LimtedSlotsOpt"].ToString();
                    PLD._LimitedSlotsApp = DT.Rows[i]["LimitedSlotsApp"].ToString();
                    PLD._PaymentDateDD = DT.Rows[i]["PaymentDateDD"].ToString();
                    PLD.message = "Success";
                    ListReturned.Add(PLD);
                }
                return ListReturned;
            }
            catch (Exception e)
            {
                PersonalLoanDetails PLD = new PersonalLoanDetails();
                PLD.message = e.Message;
                ListReturned.Add(PLD);
                return ListReturned;
            }
        }

        //[HttpPost]
        //[Route("InsertEmplistFilter")]
        //public string InsertFilter(insertemplist IE)
        //{
        //    Connection con = new Connection();
        //    con.myparameters.Add(new myParameters { ParameterName = "@LoanID", mytype = SqlDbType.NVarChar, Value = IE._LoanID });
        //    con.ExecuteScalar("sp_LoanMaker_Insert_Emplist");
        //    return "success";
        //}

        [HttpPost]
        [Route("GetLoanList")]
        public List<LoanList> LoanList()
        {
            List<LoanList> ListReturned = new List<LoanList>();
            DataTable DT = new DataTable();
            try
            {
                TempConnectionString();
                Connection con = new Connection();
                DT = con.GetDataTable("sp_LoanMaker_LoanList");
                for (int i = 0; i < DT.Rows.Count; i++)
                {
                    LoanList LL = new LoanList();
                    LL._LoanID = DT.Rows[i]["ID"].ToString();
                    LL._LoanName = DT.Rows[i]["LoanName"].ToString();
                    LL._LoanType = DT.Rows[i]["LoanType"].ToString();
                    LL._CreatedBy = DT.Rows[i]["CreatedBy"].ToString();
                    LL._CreatedDate = DT.Rows[i]["CreatedDate"].ToString();
                    LL._ModifiedBy = DT.Rows[i]["ModifiedBy"].ToString();
                    LL._ModifiedDate = DT.Rows[i]["ModifiedDate"].ToString();
                    LL.Message = "True";
                    ListReturned.Add(LL);
                }
                return ListReturned;
            }
            catch (Exception e)
            {
                LoanList LL = new LoanList();
                LL.Message = e.Message;
                ListReturned.Add(LL);
                return ListReturned;
            }
        }

        [HttpPost]
        [Route("GetLoanStatusList")]
        public List<LoanStatusList> LoanStatusList()
        {
            List<LoanStatusList> ListReturned = new List<LoanStatusList>();
            DataTable DT = new DataTable();
            try
            {
                TempConnectionString();
                Connection con = new Connection();
                DT = con.GetDataTable("sp_LoanMaker_LoanStatus");
                for (int i = 0; i < DT.Rows.Count; i++)
                {
                    LoanStatusList LSL = new LoanStatusList();
                    LSL._LoanID = DT.Rows[i]["ID"].ToString();
                    LSL._LoanName = DT.Rows[i]["LoanName"].ToString();
                    LSL._LoanType = DT.Rows[i]["LoanType"].ToString();
                    DateTime RDT = Convert.ToDateTime(DT.Rows[i]["RequestDateTime"] == DBNull.Value ? "1900-01-01 00:00:00.000" : DT.Rows[i]["RequestDateTime"].ToString());
                    LSL._StartDateTime = RDT.ToString("yyyy-MM-dd hh:mm").Substring(0, 10) + ' ' + RDT.ToShortTimeString();
                    LSL._AccessType = DT.Rows[i]["AccessType"].ToString();
                    LSL._CreatedBy = DT.Rows[i]["CreatedBy"].ToString();
                    LSL._CreatedDate = DT.Rows[i]["CreatedDate"].ToString();
                    LSL._ModifiedBy = DT.Rows[i]["ModifiedBy"].ToString();
                    LSL._ModifiedDate = DT.Rows[i]["ModifiedDate"].ToString();
                    LSL._Status = DT.Rows[i]["Status"].ToString();

                    LSL.Message = "True";
                    ListReturned.Add(LSL);
                }
                return ListReturned;
            }
            catch (Exception e)
            {
                LoanStatusList LSL = new LoanStatusList();
                LSL.Message = e.Message;
                ListReturned.Add(LSL);
                return ListReturned;
            }
        }

        // create User Loan Maker
        [HttpPost]
        [Route("LoanMakerUpdateUserType")]
        public string updateUserType(userType userType)
        {
            try
            {
                TempConnectionString();
                string USERID = "";
                USERID = userType._userID.Replace("\"", "");
                USERID = USERID.Replace("[", "");
                USERID = USERID.Replace("]", "");
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@UserID", mytype = SqlDbType.NVarChar, Value = USERID });
                con.myparameters.Add(new myParameters { ParameterName = "@UserType", mytype = SqlDbType.NVarChar, Value = userType._userType });
                con.ExecuteNonQuery("sp_LoanMaker_updateUserType");
                return "Success";
            }
            catch (Exception e)
            {
                return "Failed";
            }
        }

        [HttpPost]
        [Route("LoanMakerSearchEmployee")] //Para sa viewing sa User Mgt pag nag search
        public employeeInfo SearchEmployee(employeeInfoList employeeInfo)
        {
            TempConnectionString();
            Connection con = new Connection();
            string AREA = "";
            AREA = employeeInfo._area.Replace("\"", "");
            AREA = AREA.Replace("[", "");
            AREA = AREA.Replace("]", "");
            con.myparameters.Add(new myParameters { ParameterName = "@AREA", mytype = SqlDbType.NVarChar, Value = AREA });
            string BRANCH = "";
            BRANCH = employeeInfo._branch.Replace("\"", "");
            BRANCH = BRANCH.Replace("[", "");
            BRANCH = BRANCH.Replace("]", "");
            con.myparameters.Add(new myParameters { ParameterName = "@BRANCH", mytype = SqlDbType.NVarChar, Value = BRANCH });
            string DEPARTMENT = "";
            DEPARTMENT = employeeInfo._department.Replace("\"", "");
            DEPARTMENT = DEPARTMENT.Replace("[", "");
            DEPARTMENT = DEPARTMENT.Replace("]", "");
            con.myparameters.Add(new myParameters { ParameterName = "@DEPARTMENT", mytype = SqlDbType.NVarChar, Value = DEPARTMENT });
            string SUBDEPARTMENT = "";
            SUBDEPARTMENT = employeeInfo._subdepartment.Replace("\"", "");
            SUBDEPARTMENT = SUBDEPARTMENT.Replace("[", "");
            SUBDEPARTMENT = SUBDEPARTMENT.Replace("]", "");
            con.myparameters.Add(new myParameters { ParameterName = "@SUBDEPARTMENT", mytype = SqlDbType.NVarChar, Value = SUBDEPARTMENT });
            string REQUESTOR = "";
            REQUESTOR = employeeInfo._userid.Replace("\"", "");
            REQUESTOR = REQUESTOR.Replace("[", "");
            REQUESTOR = REQUESTOR.Replace("]", "");
            con.myparameters.Add(new myParameters { ParameterName = "@REQUESTOR", mytype = SqlDbType.NVarChar, Value = REQUESTOR });
            employeeInfo empInfo = new employeeInfo();
            List<employeeInfoList> list = new List<employeeInfoList>();
            //DataSet ds = new DataSet();
            DataTable dt = con.GetDataTable("sp_LoanMaker_SearchEmployee");

            foreach (DataRow row in dt.Rows)
            {
                employeeInfoList param = new employeeInfoList()
                {
                    _userid = row["EmpID"].ToString(),
                    _name = row["Name"].ToString(),
                    _area = row["Area"].ToString(),
                    _branch = row["Branch"].ToString(),
                    _department = row["Department"].ToString(),
                    _subdepartment = row["SubDepartment"].ToString(),
                    _accessType = row["AccessID"].ToString(),
                    _email = row["Email"].ToString(),
                    _phoneNumber = row["PhoneNumber"].ToString(),
                    _status = row["Status"].ToString(),
                    _loginid = row["LoginID"].ToString()
                };
                list.Add(param);
            }
            empInfo.employeeInfoList = list;
            empInfo._myreturn = "Success";
            return empInfo;
        }

        [HttpPost]
        [Route("LoanMakerCreateUser")]
        public string CreateUser(createuser cu)
        {
            try
            {
                TempConnectionString();
                Connection con = new Connection();
                string USERID = "";
                USERID = cu._userid.Replace("\"", "");
                USERID = USERID.Replace("[", "");
                USERID = USERID.Replace("]", "");
                con.myparameters.Add(new myParameters { ParameterName = "@USERID", mytype = SqlDbType.NVarChar, Value = USERID });
                con.myparameters.Add(new myParameters { ParameterName = "@LOGINID", mytype = SqlDbType.NVarChar, Value = cu._loginid });
                con.myparameters.Add(new myParameters { ParameterName = "@USERTYPE", mytype = SqlDbType.NVarChar, Value = cu._usertype });
                con.myparameters.Add(new myParameters { ParameterName = "@FNAME", mytype = SqlDbType.NVarChar, Value = cu._fname });
                con.myparameters.Add(new myParameters { ParameterName = "@MNAME", mytype = SqlDbType.NVarChar, Value = cu._mname });
                con.myparameters.Add(new myParameters { ParameterName = "@LNAME", mytype = SqlDbType.NVarChar, Value = cu._lname });
                con.myparameters.Add(new myParameters { ParameterName = "@TITLE", mytype = SqlDbType.NVarChar, Value = cu._title });
                con.myparameters.Add(new myParameters { ParameterName = "@ROLE", mytype = SqlDbType.NVarChar, Value = cu._role });
                con.myparameters.Add(new myParameters { ParameterName = "@AREA", mytype = SqlDbType.NVarChar, Value = cu._area });
                con.myparameters.Add(new myParameters { ParameterName = "@BRANCH", mytype = SqlDbType.NVarChar, Value = cu._branch });
                con.myparameters.Add(new myParameters { ParameterName = "@DEPARTMENT", mytype = SqlDbType.NVarChar, Value = cu._department });
                con.myparameters.Add(new myParameters { ParameterName = "@SUBDEPARTMENT", mytype = SqlDbType.NVarChar, Value = cu._subdepartment });
                con.myparameters.Add(new myParameters { ParameterName = "@COMPANY", mytype = SqlDbType.NVarChar, Value = cu._company });
                con.myparameters.Add(new myParameters { ParameterName = "@EMAIL", mytype = SqlDbType.NVarChar, Value = cu._email });
                con.myparameters.Add(new myParameters { ParameterName = "@PHONE", mytype = SqlDbType.NVarChar, Value = cu._phone });
                con.ExecuteNonQuery("sp_LoanMaker_InsertUpdateUser");
                return "Success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }

        [HttpPost]
        [Route("LoanMakerSelectEmployee")]
        public employeeInfo SelectEmployee(changePassword cp)
        {
            try
            {
                TempConnectionString();
                Connection con = new Connection();
                string USERID = "";
                USERID = cp._UID.Replace("\"", "");
                USERID = USERID.Replace("[", "");
                USERID = USERID.Replace("]", "");
                con.myparameters.Add(new myParameters { ParameterName = "@USERID", mytype = SqlDbType.NVarChar, Value = USERID });
                DataTable dt = con.GetDataTable("sp_LoanMaker_SelectEmployee");
                employeeInfo empInfo = new employeeInfo();
                List<employeeInfoList> list = new List<employeeInfoList>();
                //DataSet ds = new DataSet();


                foreach (DataRow row in dt.Rows)
                {
                    employeeInfoList param = new employeeInfoList()
                    {
                        _empid = row["EmpID"].ToString(),
                        _firstName = row["First_Name"].ToString(),
                        _middleName = row["Middle_Name"].ToString(),
                        _lastName = row["Last_Name"].ToString(),
                        _title = row["Title"].ToString(),
                        _role = row["Role"].ToString(),
                        _area = row["Area"].ToString(),
                        _branch = row["Branch"].ToString(),
                        _department = row["Department"].ToString(),
                        _subdepartment = row["SubDepartment"].ToString(),
                        _accessType = row["AccessID"].ToString(),
                        _email = row["Email"].ToString(),
                        _phoneNumber = row["PhoneNumber"].ToString()
                    };
                    list.Add(param);
                }
                empInfo.employeeInfoList = list;
                empInfo._myreturn = "Success";
                return empInfo;
            }
            catch (Exception e)
            {
                employeeInfo empInfo = new employeeInfo();
                List<employeeInfoList> list = new List<employeeInfoList>();
                empInfo.employeeInfoList = list;
                empInfo._myreturn = e.ToString();
                return empInfo;
            }
        }

        [HttpPost]
        [Route("LoanMakerSubSubjects")] //Listahan ng SubSubjects sa labels, pasahan ng value 0 yung parameter para magdisplay lahat ng subsubjects else selected subsubjects lang lalabas
        public subsubjects loanmakerSubSubjects(subjectlist subject)
        {
            try
            {
                TempConnectionString();
                Connection con = new Connection();
                subsubjects subsubjects = new subsubjects();
                List<subsubjectlist> list = new List<subsubjectlist>();
                //DataSet ds = new DataSet();
                con.myparameters.Add(new myParameters { ParameterName = "@SUBJECTID", mytype = SqlDbType.NVarChar, Value = subject._SID });
                DataTable dt = con.GetDataTable("sp_LoanMaker_SelectShowSubSubjects");

                foreach (DataRow row in dt.Rows)
                {
                    subsubjectlist param = new subsubjectlist()
                    {
                        _RowID = row["RowID"].ToString(),
                        _SID = row["SID"].ToString(),
                        _Subject = row["SubSubject"].ToString(),
                        _Status = Convert.ToBoolean(row["Status"].ToString()),
                        _ModifiedBy = row["ModifiedBy"].ToString(),
                        _ModifiedDate = row["ModifiedDate"].ToString(),
                        _Edit = false
                    };
                    list.Add(param);
                }
                subsubjects.subsubjectlist = list;
                subsubjects._myreturn = "Success";
                return subsubjects;
            }
            catch (Exception e)
            {
                subsubjects subsubjects = new subsubjects();
                List<subsubjectlist> list = new List<subsubjectlist>();
                subsubjects.subsubjectlist = list;
                subsubjects._myreturn = e.ToString();
                return subsubjects;
            }
        }

        [HttpPost]
        [Route("LoanMakerAccessType")] //Listahan ng SubSubjects sa labels, pasahan ng value 0 yung parameter para magdisplay lahat ng subsubjects else selected subsubjects lang lalabas
        public accesstype loanmakerSubSubjects(accesslist subject)
        {
            try
            {
                TempConnectionString();
                Connection con = new Connection();
                accesstype accesstype = new accesstype();
                List<accesstypelist> list = new List<accesstypelist>();
                //DataSet ds = new DataSet();
                con.myparameters.Add(new myParameters { ParameterName = "@SUBJECTID", mytype = SqlDbType.NVarChar, Value = subject._SID });
                DataTable dt = con.GetDataTable("sp_LoanMaker_SelectShowAccessTypes");

                foreach (DataRow row in dt.Rows)
                {
                    accesstypelist param = new accesstypelist()
                    {
                        _RowID = row["RowID"].ToString(),
                        _SID = row["SID"].ToString(),
                        _Subject = row["SubSubject"].ToString()
                    };
                    list.Add(param);
                }
                accesstype.accesstypelist = list;
                accesstype._myreturn = "Success";
                return accesstype;
            }
            catch (Exception e)
            {
                accesstype accesstype = new accesstype();
                List<accesstypelist> list = new List<accesstypelist>();
                accesstype.accesstypelist = list;
                accesstype._myreturn = e.ToString();
                return accesstype;
            }
        }


        [HttpPost]
        [Route("LoanMakerRequestorList")]
        public requestorlist RequestorList(requestor requestor)
        {
            try
            {
                TempConnectionString();
                Connection con = new Connection();
                //string USERTYPE = "";
                //USERTYPE = requestor._usertype.Replace("\"", "");
                //USERTYPE = USERTYPE.Replace("[", "");
                //USERTYPE = USERTYPE.Replace("]", "");
                //con.myparameters.Add(new myParameters { ParameterName = "@USERTYPE", mytype = SqlDbType.NVarChar, Value = USERTYPE });
                DataTable dt = con.GetDataTable("sp_LoanMaker_RequestorList");
                requestorlist RL = new requestorlist();
                List<requestor> R = new List<requestor>();
                foreach (DataRow row in dt.Rows)
                {
                    requestor req = new requestor()
                    {
                        _userid = row["EmpID"].ToString(),
                        _requestorname = row["Requestor"].ToString()
                    };
                    R.Add(req);
                }
                RL.requestor = R;
                RL._myreturn = "Success";
                return RL;
            }
            catch (Exception e)
            {
                requestorlist RL = new requestorlist();
                List<requestor> R = new List<requestor>();
                RL.requestor = R;
                RL._myreturn = e.ToString();
                return RL;
            }
        }

        [HttpPost]
        [Route("LoanMakerSubjectLabels")]
        public subjectlabellist SubjectLabelList()
        {
            try
            {
                TempConnectionString();
                Connection con = new Connection();
                DataTable dt = con.GetDataTable("sp_LoanMaker_LabelSubjects");
                subjectlabellist SubjectLabelList = new subjectlabellist();
                List<subjectlabel> SL = new List<subjectlabel>();
                foreach (DataRow row in dt.Rows)
                {
                    subjectlabel param = new subjectlabel()
                    {
                        _title = row["Title"].ToString(),
                        _role = row["Role"].ToString(),
                        _area = row["Area"].ToString(),
                        _branch = row["Branch"].ToString(),
                        _department = row["Department"].ToString(),
                        _subdepartment = row["SubDepartment"].ToString(),
                    };
                    SL.Add(param);
                }
                SubjectLabelList.subjectlabel = SL;
                SubjectLabelList._myreturn = "Success";
                return SubjectLabelList;
            }
            catch (Exception e)
            {
                subjectlabellist SubjectLabelList = new subjectlabellist();
                List<subjectlabel> SL = new List<subjectlabel>();
                SubjectLabelList.subjectlabel = SL;
                SubjectLabelList._myreturn = e.ToString();
                return SubjectLabelList;
            }
        }

        [HttpPost]
        [Route("LoanMakerDropdownHierarchy")]
        public DropdownHierarchyList DDHL(DropdownHierarchy DDH)
        {
            try
            {
                TempConnectionString();
                DropdownHierarchyList DHL = new DropdownHierarchyList();
                List<DropdownHierarchyItems> DHI = new List<DropdownHierarchyItems>();
                DataTable DT = new DataTable();
                DT.Columns.AddRange(new DataColumn[1] { new DataColumn("Items") });
                foreach (string item in DDH._items)
                {
                    DT.Rows.Add(item);
                }
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@SOURCE", mytype = SqlDbType.NVarChar, Value = DDH._source });
                con.myparameters.Add(new myParameters { ParameterName = "@TARGET", mytype = SqlDbType.NVarChar, Value = DDH._target });
                con.myparameters.Add(new myParameters { ParameterName = "@HIERARCHYITEMS", mytype = SqlDbType.Structured, Value = DT });
                DataTable DT2 = con.GetDataTable("sp_LoanMaker_DropdownHierarchy");
                foreach (DataRow row in DT2.Rows)
                {
                    DropdownHierarchyItems DI = new DropdownHierarchyItems()
                    {
                        _itemid = row["ItemID"].ToString(),
                        _itemname = row["ItemName"].ToString()
                    };
                    DHI.Add(DI);
                }
                DHL.DDITEMS = DHI;
                DHL._myreturn = "Success";
                return DHL;
            }
            catch (Exception e)
            {
                DropdownHierarchyList DHL = new DropdownHierarchyList();
                List<DropdownHierarchyItems> DHI = new List<DropdownHierarchyItems>();
                DHL.DDITEMS = DHI;
                DHL._myreturn = e.ToString();
                return DHL;
            }
        }

        [HttpPost]
        [Route("LoanMakerDropdownHierarchyAU")]
        public subsubjects DDHAU(DropdownHierarchy DDHAU)
        {
            try
            {
                TempConnectionString();
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@SOURCE", mytype = SqlDbType.VarChar, Value = DDHAU._source });
                con.myparameters.Add(new myParameters { ParameterName = "@TARGET", mytype = SqlDbType.VarChar, Value = DDHAU._target });
                con.myparameters.Add(new myParameters { ParameterName = "@HIERARCHYITEMS", mytype = SqlDbType.VarChar, Value = DDHAU._paramater });
                DataTable DT2 = con.GetDataTable("sp_LoanMaker_DropdownHierarchy_AddUser");
                subsubjects DHL = new subsubjects();
                List<subsubjectlist> DHI = new List<subsubjectlist>();
                foreach (DataRow row in DT2.Rows)
                {
                    subsubjectlist DI = new subsubjectlist()
                    {
                        _RowID = row["ItemID"].ToString(),
                        _Subject = row["ItemName"].ToString()
                    };
                    DHI.Add(DI);
                }
                DHL.subsubjectlist = DHI;
                DHL._myreturn = "Success";
                return DHL;
            }
            catch (Exception e)
            {
                subsubjects DHL = new subsubjects();
                List<subsubjectlist> DHI = new List<subsubjectlist>();
                DHL.subsubjectlist = DHI;
                DHL._myreturn = e.ToString();
                return DHL;
            }
        }

        [HttpPost]
        [Route("LoanMakerUserMap")]
        public UserList UserMapList(UserDataMappingItems UDMI)
        {
            try
            {
                TempConnectionString();
                UserList UserList = new UserList();
                List<UserMap> ListReturned = new List<UserMap>();
                DataTable DT = new DataTable();
                DT.Columns.AddRange(new DataColumn[13] { new DataColumn("Userid"), new DataColumn("Fname"), new DataColumn("Mname"), new DataColumn("Lname"), new DataColumn("Title"), new DataColumn("Role"), new DataColumn("Area"), new DataColumn("Branch"), new DataColumn("Department"), new DataColumn("SubDepartment"), new DataColumn("AccessType"), new DataColumn("Email"), new DataColumn("PhoneNumber") });
                for (int i = 0; i < UDMI._fname.Length; i++)
                {
                    DT.Rows.Add(UDMI._userid[i], UDMI._fname[i], UDMI._mname[i], UDMI._lname[i], UDMI._title[i], UDMI._role[i], UDMI._area[i], UDMI._branch[i], UDMI._department[i], UDMI._subdepartment[i], UDMI._usertype[i], UDMI._email[i], UDMI._phonenumber[i]);
                }
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@IMPORT", mytype = SqlDbType.Structured, Value = DT });
                DataTable DT2 = con.GetDataTable("sp_LoanMaker_UserMapping");
                foreach (DataRow row in DT2.Rows)
                {
                    UserMap UM = new UserMap()
                    {
                        _rowid = row["RowID"].ToString(),
                        _fname = row["FirstName"].ToString(),
                        _mname = row["MiddleName"].ToString(),
                        _lname = row["LastName"].ToString(),
                        _title = row["Title"].ToString(),
                        _role = row["Role"].ToString(),
                        _area = row["Area"].ToString(),
                        _branch = row["Branch"].ToString(),
                        _department = row["Department"].ToString(),
                        _subdepartment = row["SubDepartment"].ToString(),
                        _usertype = row["AccessType"].ToString(),
                        _email = row["Email"].ToString(),
                        _phonenumber = row["PhoneNumber"].ToString()
                    };
                    ListReturned.Add(UM);
                }
                UserList.UserMapList = ListReturned;
                UserList._myreturn = "Success";
                return UserList;
            }
            catch (Exception e)
            {
                UserList UserList = new UserList();
                List<UserMap> ListReturned = new List<UserMap>();
                UserList.UserMapList = ListReturned;
                UserList._myreturn = e.ToString();
                return UserList;
            }
        }

        // End create User Loan Maker

        [HttpPost]
        [Route("GetLoanDeniedStatus")]
        public List<LoanDeniedStatusList> LoanDeniedStatus()
        {
            List<LoanDeniedStatusList> ListReturned = new List<LoanDeniedStatusList>();
            DataTable DT = new DataTable();
            try
            {
                TempConnectionString();
                Connection con = new Connection();
                DT = con.GetDataTable("sp_LoanMaker_LoanStatusDenied");
                for (int i = 0; i < DT.Rows.Count; i++)
                {
                    LoanDeniedStatusList LDSL = new LoanDeniedStatusList();
                    LDSL._LoanID = DT.Rows[i]["ID"].ToString();
                    LDSL._LoanName = DT.Rows[i]["LoanName"].ToString();
                    LDSL._LoanType = DT.Rows[i]["LoanType"].ToString();
                    LDSL._AccessType = DT.Rows[i]["AccessType"].ToString();
                    LDSL._CreatedBy = DT.Rows[i]["CreatedBy"].ToString();
                    LDSL._CreatedDate = DT.Rows[i]["CreatedDate"].ToString();
                    LDSL._ModifiedBy = DT.Rows[i]["ModifiedBy"].ToString();
                    LDSL._ModifiedDate = DT.Rows[i]["ModifiedDate"].ToString();
                    LDSL._Status = DT.Rows[i]["Status"].ToString();
                    LDSL.Message = "True";
                    ListReturned.Add(LDSL);
                }
                return ListReturned;
            }
            catch (Exception e)
            {
                LoanDeniedStatusList LDSL = new LoanDeniedStatusList();
                LDSL.Message = e.Message;
                ListReturned.Add(LDSL);
                return ListReturned;
            }
        }

        [HttpPost]
        [Route("GetZipMunRegCountry")]
        public ZipMunRegCountry getZipMunRegCountry()
        {
            TempConnectionString();
            ZipMunRegCountry getZMRG = new ZipMunRegCountry();
            Connection con = new Connection();
            List<ZipMunRegCountryParam> ListAllSwap = new List<ZipMunRegCountryParam>();
            //DataSet ds = new DataSet();
            DataTable dt = con.GetDataTable("sp_ZipMunRegCountry");

            foreach (DataRow row in dt.Rows)
            {
                ZipMunRegCountryParam param = new ZipMunRegCountryParam()
                {
                    ID = row[0].ToString(),
                    Zipcode = row[1].ToString(),
                    Country = row[2].ToString(),
                    Region = row[3].ToString(),
                    Province = row[4].ToString(),
                    Municipal = row[5].ToString()
                };
                ListAllSwap.Add(param);
            }
            getZMRG.ZipMunRegCountryParam = ListAllSwap;
            getZMRG.myreturn = "Success";
            return getZMRG;
        }


        [HttpPost]
        [Route("Delete")]
        public string DeleteLoanMaker(deleteLoan dl)
        {
            TempConnectionString();
            Connection con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@LoanID", mytype = SqlDbType.NVarChar, Value = dl.ID });
            con.ExecuteNonQuery("sp_LoanMaker_Delete");
            return "success";
        }

        [HttpPost]
        [Route("DeleteUserAccessType")]
        public string DeleteUserAccessType(DeleteUserAccessType deleteuseraccesslist)
        {
            TempConnectionString();
            Connection con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@ID", mytype = SqlDbType.NVarChar, Value = deleteuseraccesslist._Id });
            con.ExecuteScalar("sp_LoanMaker_DeleteAccessType");
            return "success";

        }

        [HttpPost]
        [Route("InsertUserAccessType")]
        public string InsertUserAccessType(InsertUserAccessType insertuseraccesslist)
        {
            TempConnectionString();
            Connection con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@Functions", mytype = SqlDbType.NVarChar, Value = insertuseraccesslist._Functions });
            con.myparameters.Add(new myParameters { ParameterName = "@AccessTypes", mytype = SqlDbType.NVarChar, Value = insertuseraccesslist._AccessTypes });
            con.ExecuteScalar("sp_Loanmaker_InsertAccessType");
            return "success";
        }

        [HttpPost]
        [Route("UpdateStatus_Loan")]
        public string UpdateLoanStatusLoan(updatestat us)
        {
            TempConnectionString();
            Connection con = new Connection();
            string ID = "";
            ID = us._ID.Replace("\"", "");
            ID = ID.Replace("[", "");
            ID = ID.Replace("]", "");
            con.myparameters.Add(new myParameters { ParameterName = "@LoanID", mytype = SqlDbType.NVarChar, Value = ID });
            con.myparameters.Add(new myParameters { ParameterName = "@Status", mytype = SqlDbType.NVarChar, Value = us._Status });
            con.ExecuteNonQuery("sp_LoanMaker_UpdateStatus");
            return "success";
        }

        [HttpPost]
        [Route("AcuiroInsert")]
        public string AcuiroInsert(AcuiroInsurance AI)
        {
            try
            {
                TempConnectionString();
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@LoanID", mytype = SqlDbType.NVarChar, Value = AI._LoanID });
                con.myparameters.Add(new myParameters { ParameterName = "@SmokingHistory", mytype = SqlDbType.NVarChar, Value = "" });
                con.myparameters.Add(new myParameters { ParameterName = "@AlcoholHistory", mytype = SqlDbType.NVarChar, Value = "" });
                con.myparameters.Add(new myParameters { ParameterName = "@ExcludedIllnesses", mytype = SqlDbType.NVarChar, Value = "" });
                con.myparameters.Add(new myParameters { ParameterName = "@PremiumIncomeDD", mytype = SqlDbType.NVarChar, Value = "Equal to" });
                con.myparameters.Add(new myParameters { ParameterName = "@PremiumIncomeNo", mytype = SqlDbType.NVarChar, Value = "100000" });
                con.myparameters.Add(new myParameters { ParameterName = "@InterestRate", mytype = SqlDbType.NVarChar, Value = "20%" });
                con.myparameters.Add(new myParameters { ParameterName = "@InterestType", mytype = SqlDbType.NVarChar, Value = "Fixed" });
                con.myparameters.Add(new myParameters { ParameterName = "@PaymentTermNo", mytype = SqlDbType.NVarChar, Value = "60" });
                con.myparameters.Add(new myParameters { ParameterName = "@PaymentTermDD", mytype = SqlDbType.NVarChar, Value = "Months" });
                con.myparameters.Add(new myParameters { ParameterName = "@LimitedSlots", mytype = SqlDbType.NVarChar, Value = "" });
                con.myparameters.Add(new myParameters { ParameterName = "@LimitedSlotsNo", mytype = SqlDbType.NVarChar, Value = "" });
                con.myparameters.Add(new myParameters { ParameterName = "@PaymentDate", mytype = SqlDbType.NVarChar, Value = "1st" });
                return con.ExecuteScalar("sp_AcuiroInsurance_Insert");
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }

        [HttpPost]
        [Route("LoanMakerGetCount")]
        public loanMakerGetCount loanMakerGetCount()
        {
            TempConnectionString();
            loanMakerGetCount getLoanCount = new loanMakerGetCount();
            Connection con = new Connection();
            List<loanMakerGetCountParam> countList = new List<loanMakerGetCountParam>();
            DataTable dt = con.GetDataTable("sp_LoanMaker_getCount");

            foreach (DataRow row in dt.Rows)
            {
                loanMakerGetCountParam param = new loanMakerGetCountParam()
                {
                    autoLoan = row["AutoLoan"].ToString(),
                    homeLoan = row["HomeLoan"].ToString(),
                    personalLoan = row["PersonalLoan"].ToString()
                };
                countList.Add(param);
            }
            getLoanCount.loanMakerGetCountParam = countList;
            getLoanCount.myreturn = "Success";
            return getLoanCount;
        }

        [HttpPost]
        [Route("LoanMakerGetWebNotif")]
        public loanMakerGetCountWebNotif LoanMakerGetWebNotif()
        {
            TempConnectionString();
            loanMakerGetCountWebNotif getLoanCount = new loanMakerGetCountWebNotif();
            Connection con = new Connection();
            List<loangetwebnotif> countList = new List<loangetwebnotif>();
            DataTable dt = con.GetDataTable("sp_LoanMaker_NotifWeb");

            foreach (DataRow row in dt.Rows)
            {
                loangetwebnotif param = new loangetwebnotif()
                {
                    getNotif = row["Notif"].ToString()
                };
                countList.Add(param);
            }
            getLoanCount.loanMakerGetCountParam = countList;
            getLoanCount.myreturn = "Success";
            return getLoanCount;
        }

        [HttpPost]
        [Route("GetDropdownList")]
        public DropDownList LoanList1()
        {
            try
            {
                TempConnectionString();
                Connection con = new Connection();
                DropDownList subsubjects = new DropDownList();
                List<DropList> list = new List<DropList>();
                DataTable dt = con.GetDataTable("sp_LoanMaker_DropdownLabel");

                foreach (DataRow row in dt.Rows)
                {
                    DropList param = new DropList()
                    {
                        _Id = row["id"].ToString(),
                        _Subject = row["Subject"].ToString(),
                        _Status = Convert.ToBoolean(row["tag"]),
                        _DefaultName = row["DefaultName"].ToString(),
                        _ModifiedBy = row["modifiedBy"].ToString(),
                        _ModifiedDate = row["modifiedDate"].ToString(),
                        _Edit = false
                    };
                    list.Add(param);
                }
                subsubjects.SampleList = list;
                subsubjects._myreturn = "Success";
                return subsubjects;
            }
            catch (Exception e)
            {
                DropDownList subsubjects = new DropDownList();
                List<DropList> list = new List<DropList>();
                subsubjects.SampleList = list;
                subsubjects._myreturn = e.ToString();
                return subsubjects;
            }
        }
        [HttpPost]
        [Route("GetSubjectList")]
        public SubjectList SubjectList1(DropDownSubjectList droplist)
        {
            try
            {
                TempConnectionString();
                Connection con = new Connection();
                SubjectList subsubjects = new SubjectList();
                List<DropDownSubjectList> list = new List<DropDownSubjectList>();
                con.myparameters.Add(new myParameters { ParameterName = "@SID", mytype = SqlDbType.NVarChar, Value = droplist._Sid });
                DataTable dt = con.GetDataTable("sp_LoanMaker_DropdownList");

                foreach (DataRow row in dt.Rows)
                {
                    DropDownSubjectList param = new DropDownSubjectList()
                    {
                        _Id = row["id"].ToString(),
                        _Sid = row["sId"].ToString(),
                        _SubSubject = row["SubSubject"].ToString(),
                        _Status = Convert.ToBoolean(row["tag"]),
                        _ModifiedDate = row["modifiedDate"].ToString(),
                        _ModifiedBy = row["modifiedBy"].ToString(),
                        _Edit = false

                    };
                    list.Add(param);
                }
                subsubjects.SubjectListData = list;
                subsubjects._myreturn = "Success";
                return subsubjects;
            }
            catch (Exception e)
            {
                SubjectList subsubjects = new SubjectList();
                List<DropDownSubjectList> list = new List<DropDownSubjectList>();
                subsubjects.SubjectListData = list;
                subsubjects._myreturn = e.ToString();
                return subsubjects;
            }
        }


        [HttpPost]
        [Route("UpdateUserAccessType")]
        public string UpdateUserAccessType(UserAccessTypeList updateuseraccesslist)
        {
            try
            {
                TempConnectionString();
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@ID", mytype = SqlDbType.NVarChar, Value = updateuseraccesslist._Id });
                con.myparameters.Add(new myParameters { ParameterName = "@AccessTypes", mytype = SqlDbType.NVarChar, Value = updateuseraccesslist._AccessTypes });
                return con.ExecuteScalar("sp_LoanMaker_UpdateAccessType");
            }
            catch (Exception)
            {
                return "False";
            }

        }

        [HttpPost]
        [Route("GetUserAccessList")]
        public AcuiroUserAccess UserAccessList(FunctionUserAccessType functionuseraccesstype)
        {
            try
            {
                TempConnectionString();
                Connection con = new Connection();
                AcuiroUserAccess useraccess = new AcuiroUserAccess();
                List<UserAccessList> list = new List<UserAccessList>();
                con.myparameters.Add(new myParameters { ParameterName = "@Functions", mytype = SqlDbType.NVarChar, Value = functionuseraccesstype._Functions });
                DataTable dt = con.GetDataTable("sp_LoanMaker_UserType");

                foreach (DataRow row in dt.Rows)
                {
                    UserAccessList param = new UserAccessList()
                    {
                        _Id = row["Id"].ToString(),
                        _UserType = row["UserType"].ToString(),
                    };
                    list.Add(param);
                }
                useraccess.UserAccessListData = list;
                useraccess._myreturn = "Success";
                return useraccess;
            }
            catch (Exception e)
            {
                AcuiroUserAccess useraccess = new AcuiroUserAccess();
                List<UserAccessList> list = new List<UserAccessList>();
                useraccess.UserAccessListData = list;
                useraccess._myreturn = e.ToString();
                return useraccess;
            }
        }
        [HttpPost]
        [Route("GetAllFunctionAccessType")]
        public AllFunctionAccessType AllFunctionList(AllFunctionID accesstype)
        {
            try
            {
                TempConnectionString();
                Connection con = new Connection();
                AllFunctionAccessType useraccess = new AllFunctionAccessType();
                List<AllFunctionAccessList> list = new List<AllFunctionAccessList>();
                con.myparameters.Add(new myParameters { ParameterName = "@AccessType", mytype = SqlDbType.NVarChar, Value = accesstype.AccessType });
                DataTable dt = con.GetDataTable("sp_LoanMaker_getAllFunctions_AccessType");

                foreach (DataRow row in dt.Rows)
                {
                    AllFunctionAccessList param = new AllFunctionAccessList()
                    {
                        _AccessTypeID = row["AccessType"].ToString(),
                        _Parameter2 = row["Parameter2"].ToString()
                    };
                    list.Add(param);
                }
                useraccess.AllFunctionListAccessData = list;
                useraccess._myreturn = "Success";
                return useraccess;
            }
            catch (Exception e)
            {
                AllFunctionAccessType useraccess = new AllFunctionAccessType();
                List<AllFunctionAccessList> list = new List<AllFunctionAccessList>();
                useraccess.AllFunctionListAccessData = list;
                useraccess._myreturn = e.ToString();
                return useraccess;
            }
        }

        [HttpPost]
        [Route("GetSelectedFunctionAccessType")]
        public SelectedFunctionAccessType SelectedFunctionList(SelectFunctionID accesstype)
        {
            try
            {
                TempConnectionString();
                Connection con = new Connection();
                SelectedFunctionAccessType selectedaccess = new SelectedFunctionAccessType();
                List<SelectedFunctionAccessList> list = new List<SelectedFunctionAccessList>();
                con.myparameters.Add(new myParameters { ParameterName = "@AccessType", mytype = SqlDbType.NVarChar, Value = accesstype.AccessType });
                DataTable dt = con.GetDataTable("sp_LoanMaker_SelectedFunctions_AccessType");

                foreach (DataRow row in dt.Rows)
                {
                    SelectedFunctionAccessList param = new SelectedFunctionAccessList()
                    {
                        _AccessTypeID = row["AccessType"].ToString(),
                        _Parameter1 = row["Parameter1"].ToString()
                    };
                    list.Add(param);
                }
                selectedaccess.SelectedFunctionListAccessData = list;
                selectedaccess._myreturn = "Success";
                return selectedaccess;
            }
            catch (Exception e)
            {
                SelectedFunctionAccessType selectedaccess = new SelectedFunctionAccessType();
                List<SelectedFunctionAccessList> list = new List<SelectedFunctionAccessList>();
                selectedaccess.SelectedFunctionListAccessData = list;
                selectedaccess._myreturn = e.ToString();
                return selectedaccess;
            }
        }

        [HttpPost]
        [Route("UpdateDropdownList")]
        public string UpdateDropdownType(UpdateDropDownTypeList updatedropdownlist)
        {
            try
            {
                TempConnectionString();
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@ID", mytype = SqlDbType.NVarChar, Value = updatedropdownlist._Id });
                con.myparameters.Add(new myParameters { ParameterName = "@Subject", mytype = SqlDbType.NVarChar, Value = updatedropdownlist._Subject });
                con.myparameters.Add(new myParameters { ParameterName = "@ModifiedBy", mytype = SqlDbType.NVarChar, Value = updatedropdownlist._ModifiedBy });
                return con.ExecuteScalar("sp_LoanMaker_UpdateDropdownList");
            }
            catch (Exception)
            {
                return "False";
            }
        }
        [HttpPost]
        [Route("SubjectListInsertUpdate")]
        public string SubjectListInsertUpdate(SUbjectListDetails subjectlistdetails)
        {
            try
            {
                TempConnectionString();
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@USERID", mytype = SqlDbType.NVarChar, Value = subjectlistdetails._userID });
                con.myparameters.Add(new myParameters { ParameterName = "@SUBJECTID", mytype = SqlDbType.NVarChar, Value = subjectlistdetails._Id });
                con.myparameters.Add(new myParameters { ParameterName = "@SUBSUBJECTID ", mytype = SqlDbType.NVarChar, Value = subjectlistdetails._sId });
                con.myparameters.Add(new myParameters { ParameterName = "@SUBSUBJECTNAME", mytype = SqlDbType.NVarChar, Value = subjectlistdetails._SubSubject });
                con.ExecuteScalar("sp_LoanMaker_InsertUpdateSubjectList");
                return "True";
            }
            catch (Exception)
            {
                return "False";
            }
        }
        ///updating 1-0
        [HttpPost]
        [Route("SubjectListInactiveActive")]
        public string SubjectListInactiveActive(SubjectListInactiveActive subjectlistinactiveactive)
        {
            try
            {
                TempConnectionString();
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@USERID", mytype = SqlDbType.NVarChar, Value = subjectlistinactiveactive._UserID });
                con.myparameters.Add(new myParameters { ParameterName = "@SUBJECTID", mytype = SqlDbType.NVarChar, Value = subjectlistinactiveactive._SubjectID });
                con.ExecuteScalar("sp_LoanMakerSubjectActiveInactive");
                return "True";
            }
            catch (Exception)
            {
                return "False";
            }
        }
        ///updating 1-0
        [HttpPost]
        [Route("SubjectInactiveActive")]
        public string SubjectInactiveActive(SubjectInactiveActive subjectinactiveactive)
        {
            try
            {
                TempConnectionString();
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@USERID", mytype = SqlDbType.NVarChar, Value = subjectinactiveactive._UserID });
                con.myparameters.Add(new myParameters { ParameterName = "@SUBJECTID", mytype = SqlDbType.NVarChar, Value = subjectinactiveactive._SubjectID });
                con.ExecuteScalar("sp_LoanMaker_ActiveInactiveTag");
                return "True";
            }
            catch (Exception)
            {
                return "False";
            }
        }
        //sample with datatable 
        [HttpPost]
        [Route("InsertSelectedFunction")]
        public string InsertSelectedFunction(DataTableSelInsert datatablesample)
        {
            try
            {
                TempConnectionString();
                Connection con = new Connection();
                DataTable pangalanngdatatable = new DataTable();
                pangalanngdatatable.Columns.AddRange(new DataColumn[2] { new DataColumn("AccessType"), new DataColumn("Parameter") });
                for (int i = 0; i < datatablesample.list.Length; i++)
                {
                    pangalanngdatatable.Rows.Add(datatablesample._AccessTypeID, datatablesample.list[i]);
                }
                con.myparameters.Add(new myParameters { ParameterName = "@USERTYPE", mytype = SqlDbType.NVarChar, Value = datatablesample._UserType });
                con.myparameters.Add(new myParameters { ParameterName = "@FUNCTION", mytype = SqlDbType.NVarChar, Value = datatablesample._Function });
                con.myparameters.Add(new myParameters { ParameterName = "@AccessTable1", mytype = SqlDbType.Structured, Value = pangalanngdatatable });
                con.ExecuteScalar("sp_LoanMaker_InsertSelectedFunctions");
                return "True";
            }
            catch (Exception)
            {
                return "False";
            }
        }
        [HttpPost]
        [Route("InsertAllFunction")]
        public string InsertAllFunction(DataTableAllInsert datatablesample)
        {
            try
            {
                TempConnectionString();
                Connection con = new Connection();
                DataTable pangalanngdatatable = new DataTable();
                pangalanngdatatable.Columns.AddRange(new DataColumn[2] { new DataColumn("AccessType"), new DataColumn("Parameter") });
                for (int i = 0; i < datatablesample.list.Length; i++)
                {
                    pangalanngdatatable.Rows.Add(datatablesample._AccessTypeID, datatablesample.list[i]);
                }
                con.myparameters.Add(new myParameters { ParameterName = "@USERTYPE", mytype = SqlDbType.NVarChar, Value = datatablesample._UserType });
                con.myparameters.Add(new myParameters { ParameterName = "@FUNCTION", mytype = SqlDbType.NVarChar, Value = datatablesample._Function });
                con.myparameters.Add(new myParameters { ParameterName = "@AccessTable2", mytype = SqlDbType.Structured, Value = pangalanngdatatable });
                con.ExecuteScalar("sp_LoanMaker_InsertAllFunctions");
                return "True";
            }
            catch (Exception)
            {
                return "False";
            }
        }
        [HttpPost]
        [Route("GetEmpSelectedFunction")]
        public DataTable SelectedFunction(GetSelectedFunctionList selectedfunctionlist)
        {
            TempConnectionString();
            Connection con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = selectedfunctionlist._EMPID });
            con.myparameters.Add(new myParameters { ParameterName = "@FUNCTION", mytype = SqlDbType.NVarChar, Value = selectedfunctionlist._FUNCTION });
            DataTable dt = con.GetDataTable("sp_LoanMaker_EmpSelectedFunction");

            return dt;

        }

        [HttpPost]
        [Route("SaveReports")]
        public string saveReport(SaveReport sr)
        {
            try
            {
                TempConnectionString();
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@_SaveAs", mytype = SqlDbType.VarChar, Value = sr._SaveAs });
                con.myparameters.Add(new myParameters { ParameterName = "@_ReportName", mytype = SqlDbType.VarChar, Value = sr._ReportName });
                con.myparameters.Add(new myParameters { ParameterName = "@_Description ", mytype = SqlDbType.VarChar, Value = sr._Description });
                con.myparameters.Add(new myParameters { ParameterName = "@_ValidUntil", mytype = SqlDbType.VarChar, Value = sr._ValidUntil });
                con.myparameters.Add(new myParameters { ParameterName = "@_Type", mytype = SqlDbType.VarChar, Value = sr._Type });
                con.ExecuteScalar("sp_Acuiro_SaveReport");
                return "True";
            }
            catch (Exception)
            {
                return "False";
            }
        }

        [HttpPost]
        [Route("RunReport")]
        public RunReportIDs RRID(SelectedFieldID selectedFID)
        {
            TempConnectionString();
            Connection con = new Connection();
            RunReportIDs RunReportIDs = new RunReportIDs();
            List<Report_FieldIds> list = new List<Report_FieldIds>();
            con.myparameters.Add(new myParameters { ParameterName = "@FieldID", mytype = SqlDbType.NVarChar, Value = selectedFID.FieldID });
            DataTable dt = con.GetDataTable("sp_Acuiro_FiledID");

            foreach (DataRow row in dt.Rows)
            {
                Report_FieldIds param = new Report_FieldIds()
                {
                    _ID = row["ID"].ToString(),
                };
                list.Add(param);
            }
            RunReportIDs.FId = list;
            RunReportIDs._myreturn = "Success";
            return RunReportIDs;
        }

        [HttpPost]
        [Route("Acuiro_ViewReport")]
        public reportList RList()
        {
            reportList RL = new reportList();
            List<view_report> reports = new List<view_report>();
            TempConnectionString();
            Connection con = new Connection();
            System.Data.DataTable DT = con.GetDataTable("sp_Acuiro_GetReport");
            foreach (DataRow row in DT.Rows)
            {
                view_report viewreport = new view_report()
                {
                    _Id = row["ID"].ToString(),
                    _ReportName = row["ReportName"].ToString(),
                    _CreatedDate = row["CreatedDate"].ToString(),
                    _CreatedBy = row["CreatedBy"].ToString()
                };
                reports.Add(viewreport);
            }
            RL.RList = reports;
            RL._myreturn = "Success";

            return RL;
        }
        [HttpPost]
        [Route("Acuiro_DeleteReport")]
        public string Acuiro_DeleteReport(ReportCred report)
        {
            try
            {
                TempConnectionString();
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@id", mytype = SqlDbType.NVarChar, Value = report.ID });
                con.ExecuteScalar("sp_Acuiro_DeleteSelectedReport");
                return "Success";
            }
            catch
            {
                return "Failed";
            }
        }
    }
}
