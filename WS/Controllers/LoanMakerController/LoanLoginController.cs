﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using illimitadoWepAPI.Models.LoanMakerModels;
using illimitadoWepAPI.MyClass;
using illimitadoWepAPI.Models;
using System.Data;
using System.Configuration;
using System.Reflection;

namespace illimitadoWepAPI.Controllers.LoanMakerController
{
    public class LoanLoginController : ApiController
    {
        public void TempConnectionString()
        {
            var settings = ConfigurationManager.ConnectionStrings["connectionstring"];
            var fi = typeof(ConfigurationElement).GetField(
                          "_bReadOnly",
                          BindingFlags.Instance | BindingFlags.NonPublic);
            fi.SetValue(settings, false);
            settings.ConnectionString = "Data Source=livedb-ilm.c3srcolvclfy.ap-southeast-1.rds.amazonaws.com,1412; Database = ILM_DevSvr;User Id=ILM_LiveDBUser;Password=ILMV3ry53cur3dMGTpa55w0rd2020;";
        }
        [HttpPost]
        [Route("LoanGetName")] //Para sa login
        public string LoginGetName(LoginName LN)
        {
            try
            {
                TempConnectionString();
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@ID", mytype = SqlDbType.NVarChar, Value = LN._username });
                return con.ExecuteScalar("sp_LoanMaker_getName");
            }
            catch (Exception e)
            {
                return "Failed";
            }
        }

        [HttpPost]
        [Route("LoanMakerLogin")]
        public List<LoanLoginContainer> logins(LoanLogin login)
        {
            TempConnectionString();
            DataTable DT = new DataTable();
            Connection con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@USERID", mytype = SqlDbType.NVarChar, Value = login.UserName });
            con.myparameters.Add(new myParameters { ParameterName = "@password", mytype = SqlDbType.NVarChar, Value = login.Password });            
            DT = con.GetDataTable("spLogin2");
            List<LoanLoginContainer> ListReturned = new List<LoanLoginContainer>();
            for (int i = 0; i < DT.Rows.Count; i++)
            {
                LoanLoginContainer LLC = new LoanLoginContainer();
                LLC.EmpID = DT.Rows[i][0].ToString();
                LLC.Password = DT.Rows[i][1].ToString();
                LLC.isForget = DT.Rows[i][2].ToString();
                LLC.NTID = DT.Rows[i][3].ToString();
                LLC.DefaultPassword = DT.Rows[i][4].ToString();
                ListReturned.Add(LLC);
            }
            return ListReturned;
        }
        [HttpPost]
        [Route("LoanMakerBoolean")]
        public string ReturnBoolean (LoanLogin Login)
        {
            try
            {
                TempConnectionString();
                string ret = "";
                DataTable DT = new DataTable();
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@USERID", mytype = SqlDbType.NVarChar, Value = Login.UserName });
                con.myparameters.Add(new myParameters { ParameterName = "@password", mytype = SqlDbType.NVarChar, Value = Login.Password });
                DT = con.GetDataTable("sp_LoanMaker_Login");
                if (DT.Rows.Count > 0)
                {
                    ret = "True";
                }
                else
                {
                    ret = "False";
                }
                return ret;
            }
            catch (Exception e)
            {
                return e.ToString();
            }            
        }

        [HttpPost]
        [Route("GetGraph")]
        //{
        //"NTID": "i101",
        //"Start_Date": "2017-04-01",
        //"End_Date": "2017-04-30"
        //}

        public GetgraphResult GetAllSwapRequest(LoanGraph LG)
        {
            GetgraphResult GetAllSwap = new GetgraphResult();
            try
            {
                TempConnectionString();
                Connection Connection = new Connection();
                List<graph> ListAllSwap = new List<graph>();
                DataSet ds = new DataSet();
                DataTable dtSwapreq = new DataTable();

                Connection.myparameters.Add(new myParameters { ParameterName = "@LoanID", mytype = SqlDbType.NVarChar, Value = LG._LoanType });

                ds = Connection.GetDataset("sp_LM_Filtered");
                dtSwapreq = ds.Tables[0];
                if (dtSwapreq.Rows.Count > 0)
                {
                    foreach (DataRow row in dtSwapreq.Rows)
                    {
                        graph SwapRequest = new graph
                        {


                            graphlocation = row["Location"].ToString(),
                            graphtotal = Convert.ToInt32(row["TotalCount"]),
                            graphcolor = row["Color"].ToString()



                        };
                        ListAllSwap.Add(SwapRequest);
                    }
                    GetAllSwap.graph = ListAllSwap;
                    GetAllSwap.myreturn = "Success";
                }
                else
                {
                    GetAllSwap.myreturn = "No Data Available";
                }
            }
            catch (Exception ex)
            {
                GetAllSwap.myreturn = ex.Message;
            }
            return GetAllSwap;
        }
        [HttpPost]
        [Route("GetRegion_Loan")]
        //{
        //"NTID": "i101",
        //"Start_Date": "2017-04-01",
        //"End_Date": "2017-04-30"
        //}

        public ListAllLocationResult GetRegionLoan(Location Loc)
        {
            ListAllLocationResult GetAllLocation = new ListAllLocationResult();
            try
            {
                TempConnectionString();
                Connection Connection = new Connection();
                List<Location> ListAllLocation = new List<Location>();
                DataSet ds = new DataSet();
                DataTable dtLoc = new DataTable();

                string L = "";
                L = Loc.Loc.Replace("\"", "");
                L = L.Replace("[", "");
                L = L.Replace("]", "");

                Connection.myparameters.Add(new myParameters { ParameterName = "@COUNTRY", mytype = SqlDbType.NVarChar, Value = L });

                ds = Connection.GetDataset("sp_Vue_GetRegion");
                dtLoc = ds.Tables[0];
                if (dtLoc.Rows.Count > 0)
                {
                    foreach (DataRow row in dtLoc.Rows)
                    {
                        Location LocRequest = new Location
                        {


                            Loc = row["regDesc"].ToString()



                        };
                        ListAllLocation.Add(LocRequest);
                    }
                    GetAllLocation.Locations = ListAllLocation;
                    GetAllLocation.myreturn = "Success";
                }
                else
                {
                    GetAllLocation.myreturn = "No Data Available";
                }
            }
            catch (Exception ex)
            {
                GetAllLocation.myreturn = ex.Message;
            }
            return GetAllLocation;
        }

        [HttpPost]
        [Route("GetCityMun")]
        //{
        //"NTID": "i101",
        //"Start_Date": "2017-04-01",
        //"End_Date": "2017-04-30"
        //}

        public ListAllLocationResult GetCityMun(Location Loc)
        {
            ListAllLocationResult GetAllLocation = new ListAllLocationResult();
            try
            {
                TempConnectionString();
                Connection Connection = new Connection();
                List<Location> ListAllLocation = new List<Location>();
                DataSet ds = new DataSet();
                DataTable dtLoc = new DataTable();

                string L = "";
                L = Loc.Loc.Replace("\"", "");
                L = L.Replace("[", "");
                L = L.Replace("]", "");

                Connection.myparameters.Add(new myParameters { ParameterName = "@REGION", mytype = SqlDbType.NVarChar, Value = L });

                ds = Connection.GetDataset("sp_Vue_GetCityMun");
                dtLoc = ds.Tables[0];
                if (dtLoc.Rows.Count > 0)
                {
                    foreach (DataRow row in dtLoc.Rows)
                    {
                        Location LocRequest = new Location
                        {


                            Loc = row["citymunDesc"].ToString()



                        };
                        ListAllLocation.Add(LocRequest);
                    }
                    GetAllLocation.Locations = ListAllLocation;
                    GetAllLocation.myreturn = "Success";
                }
                else
                {
                    GetAllLocation.myreturn = "No Data Available";
                }
            }
            catch (Exception ex)
            {
                GetAllLocation.myreturn = ex.Message;
            }
            return GetAllLocation;
        }
        [HttpPost]
        [Route("GetCityMun2")]
        //{
        //"NTID": "i101",
        //"Start_Date": "2017-04-01",
        //"End_Date": "2017-04-30"
        //}

        public ListAllLocationResult GetCityMun2()
        {
            ListAllLocationResult GetAllLocation = new ListAllLocationResult();
            try
            {
                TempConnectionString();
                Connection Connection = new Connection();
                List<Location> ListAllLocation = new List<Location>();
                DataSet ds = new DataSet();
                DataTable dtLoc = new DataTable();

                ds = Connection.GetDataset("sp_Vue_GetCityMun2");
                dtLoc = ds.Tables[0];
                if (dtLoc.Rows.Count > 0)
                {
                    foreach (DataRow row in dtLoc.Rows)
                    {
                        Location LocRequest = new Location
                        {


                            Loc = row["citymunDesc"].ToString()



                        };
                        ListAllLocation.Add(LocRequest);
                    }
                    GetAllLocation.Locations = ListAllLocation;
                    GetAllLocation.myreturn = "Success";
                }
                else
                {
                    GetAllLocation.myreturn = "No Data Available";
                }
            }
            catch (Exception ex)
            {
                GetAllLocation.myreturn = ex.Message;
            }
            return GetAllLocation;
        }
        [HttpPost]
        [Route("GetDDItems_Loan")]
        public List<MultiSelectItems> MultiSelectItemsLoan(MultiSelectItems MSI)
        {
            List<MultiSelectItems> ListReturned = new List<Models.LoanMakerModels.MultiSelectItems>();
            DataTable DT = new DataTable();
            try
            {
                TempConnectionString();
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@ITEMTYPE", mytype = SqlDbType.NVarChar, Value = MSI.ItemType });
                DT = con.GetDataTable("sp_LoanMaker_GetDDItems");
                for (int i = 0; i < DT.Rows.Count; i++)
                {
                    MultiSelectItems MS = new MultiSelectItems();
                    MS.ItemID = DT.Rows[i]["ItemID"].ToString();
                    MS.ItemName = DT.Rows[i]["ItemName"].ToString();
                    MS.Message = "Success";
                    ListReturned.Add(MS);
                }
                return ListReturned;
            }
            catch (Exception e)
            {
                MultiSelectItems MS = new MultiSelectItems();
                MS.Message = e.Message;
                ListReturned.Add(MS);
                return ListReturned;
            }
        }
        [HttpPost]
        [Route("GetProvinces")]
        public List<MultiSelectItems> Provinces(Location Loc)
        {
            List<MultiSelectItems> ListReturned = new List<Models.LoanMakerModels.MultiSelectItems>();
            DataTable DT = new DataTable();
            try
            {
                TempConnectionString();
                Connection con = new Connection();
                string L = "";
                L = Loc.Loc.Replace("\"", "");
                L = L.Replace("[", "");
                L = L.Replace("]", "");
                con.myparameters.Add(new myParameters { ParameterName = "@LOC", mytype = SqlDbType.NVarChar, Value = L });
                DT = con.GetDataTable("sp_Vue_GetProvince");
                for (int i = 0; i < DT.Rows.Count; i++)
                {
                    MultiSelectItems MS = new MultiSelectItems();
                    MS.ItemName = DT.Rows[i]["ItemName"].ToString();
                    MS.Message = "Success";
                    ListReturned.Add(MS);
                }
                return ListReturned;
            }
            catch (Exception e)
            {
                MultiSelectItems MS = new MultiSelectItems();
                MS.Message = e.Message;
                ListReturned.Add(MS);
                return ListReturned;
            }
        }

    }
}
