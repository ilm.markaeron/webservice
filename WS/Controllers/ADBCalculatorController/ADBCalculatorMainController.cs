﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using illimitadoWepAPI.Models.ADBCalculatorModel;
using illimitadoWepAPI.ADBCalculatorClass;
using System.Data;

namespace illimitadoWepAPI.Controllers.ADBCalculatorController
{
    public class ADBCalculatorMainController : ApiController
    {
        [HttpPost]
        [Route("ADBCalcPlanList")]
        public PlanListClass PlanListClass()
        {
            PlanListClass PLC = new PlanListClass();
            List<PlanList> PL = new List<PlanList>();
            try
            {
                Connection con = new Connection();
                DataTable DT = con.GetDataTable("sp_ADB_PlanList");
                foreach (DataRow row in DT.Rows)
                {
                    PlanList P = new PlanList()
                    {
                        _planid = row["PlanID"].ToString(),
                        _contractyears = row["ContractYears"].ToString(),
                        _planname = row["PlanName"].ToString(),
                        _pricingcolumns = row["PricingColumns"].ToString(),
                        _plansequenceid = row["PlanSequenceID"].ToString()
                    };
                    PL.Add(P);
                }
                PLC._myreturn = "Success";
            }
            catch (Exception e)
            {
                PLC._myreturn = e.ToString();
            }
            PLC._planlist = PL;
            return PLC;
        }
        [HttpPost]
        [Route("ADBCalcPricingList")]
        public PricingListClass PricingListClass()
        {
            PricingListClass PLC = new PricingListClass();
            List<PricingList> PL = new List<PricingList>();
            try
            {
                Connection con = new Connection();
                DataTable DT = con.GetDataTable("sp_ADB_PriceList");
                foreach (DataRow row in DT.Rows)
                {
                    PricingList P = new PricingList()
                    {
                        _pricingid = row["PricingID"].ToString(),
                        _usercount = row["UserCount"].ToString(),
                        _oneautomatic = row["OneAutomatic"].ToString(),
                        _onecruisecontrol = row["OneCruiseControl"].ToString(),
                        _oneselfdrive = row["OneSelfDrive"].ToString(),
                        _twocruisecontrol = row["TwoCruiseControl"].ToString(),
                        _twoselfdrive = row["TwoSelfDrive"].ToString(),
                        _threeselfdrive = row["ThreeSelfDrive"].ToString(),
                        _fiveselfdrive = row["FiveSelfDrive"].ToString()
                    };
                    PL.Add(P);
                }
                PLC._myreturn = "Success";
            }
            catch (Exception e)
            {
                PLC._myreturn = e.ToString();
            }
            PLC._pricinglist = PL;
            return PLC;
        }
        [HttpPost]
        [Route("ADBCalcInsertUpdatePlanList")]
        public string InsertUpdatePlanList(PlanList PL)
        {
            try
            {
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@PLANID", mytype = SqlDbType.Int, Value = PL._planid });
                con.myparameters.Add(new myParameters { ParameterName = "@CONTRACTYEARS", mytype = SqlDbType.VarChar, Value = PL._contractyears });
                con.myparameters.Add(new myParameters { ParameterName = "@PLANNAME", mytype = SqlDbType.VarChar, Value = PL._planname });
                con.myparameters.Add(new myParameters { ParameterName = "@PLANCOL", mytype = SqlDbType.VarChar, Value = PL._pricingcolumns });
                con.ExecuteNonQuery("sp_ADB_InsertUpdatePlanList");
                return "Success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }
        [HttpPost]
        [Route("ADBCalcInsertUpdatePricingList")]
        public string InsertUpdatePricingList(PricingList PL)
        {
            try
            {
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@PRICINGID", mytype = SqlDbType.Float, Value = PL._pricingid });
                con.myparameters.Add(new myParameters { ParameterName = "@USERCOUNT", mytype = SqlDbType.Float, Value = PL._usercount });
                con.myparameters.Add(new myParameters { ParameterName = "@ONEAUTOMATIC", mytype = SqlDbType.Float, Value = PL._oneautomatic });
                con.myparameters.Add(new myParameters { ParameterName = "@ONECRUISECONTROL", mytype = SqlDbType.Float, Value = PL._onecruisecontrol });
                con.myparameters.Add(new myParameters { ParameterName = "@ONESELFDRIVE", mytype = SqlDbType.Float, Value = PL._oneselfdrive });
                con.myparameters.Add(new myParameters { ParameterName = "@TWOCRUISECONTROL", mytype = SqlDbType.Float, Value = PL._twocruisecontrol });
                con.myparameters.Add(new myParameters { ParameterName = "@TWOSELFDRIVE", mytype = SqlDbType.Float, Value = PL._twoselfdrive });
                con.myparameters.Add(new myParameters { ParameterName = "@THREESELFDRIVE", mytype = SqlDbType.Float, Value = PL._threeselfdrive });
                con.myparameters.Add(new myParameters { ParameterName = "@FIVESELFDRIVE", mytype = SqlDbType.Float, Value = PL._fiveselfdrive });
                con.ExecuteNonQuery("sp_ADB_InsertUpdatePricingList");
                return "Success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }

        [HttpPost]
        [Route("DeletePricing")]
        public string DeletePricing(deletePricing dp)
        {
            Connection con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@ID", mytype = SqlDbType.NVarChar, Value = dp.ID });
            con.ExecuteNonQuery("sp_ADB_Pricing_delete");
            return "success";
        }

        [HttpPost]
        [Route("DeletePlan")]
        public string DeletePlan(deletePlan dp)
        {
            Connection con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@ID", mytype = SqlDbType.NVarChar, Value = dp.ID });
            con.ExecuteNonQuery("sp_ADB_Plan_delete");
            return "success";
        }


        [HttpPost]
        [Route("ADBComputation")]
        public PlanResultClass PricingResult(PlanParam pri)
        {
            PlanResultClass PLC = new PlanResultClass();
            List<PlanResult> PL = new List<PlanResult>();
            try
            {
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@userCount", mytype = SqlDbType.VarChar, Value = pri._usercount });
                con.myparameters.Add(new myParameters { ParameterName = "@pricingColumn", mytype = SqlDbType.VarChar, Value = pri._pricingcolumn });
                DataTable DT = con.GetDataTable("sp_ADB_Computation");
                foreach (DataRow row in DT.Rows)
                {
                    PlanResult P = new PlanResult()
                    {
                        _result = row["parameterised_column"].ToString()
                    };
                    PL.Add(P);
                }
                PLC._myreturn = "Success";
            }
            catch (Exception e)
            {
                PLC._myreturn = e.ToString();
            }
            PLC._planresult = PL;
            return PLC;
        }

    }
}
