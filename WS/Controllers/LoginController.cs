﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using illimitadoWepAPI.Models;
using System.Xml.Serialization;
using System.IO;
using System.Data;
using System.Data.SqlClient;
using illimitadoWepAPI.MyClass;
using Kairos.Net;
using System.Configuration;
using System.Reflection;
using System.Net.Mail;
using System.Collections;
using MimeKit;
using System.Web;
using iTextSharp.text.pdf;
using System.Threading.Tasks;
using Microsoft.ProjectOxford.Face;
using Newtonsoft.Json.Linq;
using iTextSharp.text;
using System.Web.Hosting;
using ExpertXls.ExcelLib;
using System.IO.Compression;

namespace illimitadoWepAPI.Controllers
{
    public class LoginController : ApiController
    {
        public void tempconnectionstring()
        {
            var settings = ConfigurationManager.ConnectionStrings["connectionstring"];
            var fi = typeof(ConfigurationElement).GetField(
                          "_bReadOnly",
                          BindingFlags.Instance | BindingFlags.NonPublic);
            fi.SetValue(settings, false);
            //--Local--//
            //settings.ConnectionString = "Data Source=192.168.2.33,1444;Database=CRT_Dbase2019-03-29;User Id=DevUser;Password=securedmgt@1230;";
            //--AWS--//
            settings.ConnectionString = "Data Source=livedb-ilm.c3srcolvclfy.ap-southeast-1.rds.amazonaws.com,1412; Database = ILM_Main;User Id=ILM_LiveDBUser;Password=ILMV3ry53cur3dMGTpa55w0rd2020;";

        }
        /// Get All sample info
        /// </summary>
        /// <returns></returns>
        // GET: api/Login
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }
        /// <summary>
        /// Sample Again.....
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        // GET: api/Login/5


        [HttpPost]
        [Route("Login")]
        public Login Post(string id)
        {


            LoginDB logDB = new LoginDB();
            Login User = logDB.searchLogin(id);

            //XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
            //ns.Add("", "");
            //XmlSerializer s = new XmlSerializer(typeof(Login));
            //StringWriter myWriter = new StringWriter();
            //s.Serialize(myWriter, User, ns);

            return User;
        }




        [HttpPost]
        [Route("DisabledFeatures_Mobile")]
        public string DisabledFeatures_Mobile(Loginv2 Client)
        {
            try
            {
                tempconnectionstring();
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@CN", mytype = SqlDbType.NVarChar, Value = Client.CN });
                return con.ExecuteScalar("sp_DisabledFeatures_Mobile").ToString();
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }




        // POST: api/Login
        [HttpPost]
        [Route("LoginNow")]
        public void Post([FromBody] Login value)
        {

            LoginDB logDB = new LoginDB();
            string id;
            id = logDB.saveLogin(value);



        }

        // PUT: api/Login/5
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/Login/5
        public void Delete(int id)
        {

        }


        //public HttpPostAttribute ValidateUser([FromBody]Login value)
        //{



        //    LoginDB logDB = new LoginDB();
        //    UserProfile User = logDB.validateLogin(value);

        //    return User;




        //}


        /// <summary>
        ///    {
        ///    "LoginID": "i0078",
        ///    "Password": "password"
        ///        }
        /// </summary>
        /// <param name="userToSave"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("validateLogin")]
        public UserProfile validatelogin(Login userToSave)
        {
            Connection Connection = new Connection();
            DataTable DT = new DataTable();
            Connection.myparameters.Add(new myParameters { ParameterName = "@empId", mytype = SqlDbType.NVarChar, Value = userToSave.LoginID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@password", mytype = SqlDbType.NVarChar, Value = userToSave.Password });
            DT = Connection.GetDataTable("spLogin_Mobile");
            UserProfile User = new UserProfile();
            if (DT.Rows.Count > 0)
            {
                User = new UserProfile();
                User.LoginID = DT.Rows[0]["username"].ToString();

                User.Password = DT.Rows[0]["userpassword"].ToString();
                User.FIRSTNAME = DT.Rows[0]["First_Name"].ToString();
                User.LASTNAME = DT.Rows[0]["Last_Name"].ToString();
                User.MIDDLENAME = DT.Rows[0]["Middle_Name"].ToString();
                User.NTID = DT.Rows[0]["username"].ToString();
                User.FaceID = DT.Rows[0]["faceID"].ToString();
                User.Access = DT.Rows[0]["access"].ToString();
                User.JoinDate = DT.Rows[0]["DateJoined"].ToString();
                User.BIRTHDAY = DT.Rows[0]["DOB"].ToString();
                User.CIVILSTATUS = DT.Rows[0]["MaritalStatus"].ToString();
                if (User.CIVILSTATUS.ToLower().Trim() == "married")
                {
                    User.Marrieddate = DT.Rows[0]["MarriedDate"].ToString();
                    User.MaritalStatus = DT.Rows[0]["MaritalStatus"].ToString();
                }
                else
                {
                    User.Marrieddate = string.Empty;
                }
                User.Coordinates = DT.Rows[0]["Coordinates"].ToString();
                User.EmpStatus = DT.Rows[0]["EmpStatus"].ToString();
            }
            return User;

        }




        [HttpPost]
        [Route("validateLoginURL")]
        public UserProfile validateloginURL([FromUri] Login userToSave)
        {
            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@empId", mytype = SqlDbType.NVarChar, Value = userToSave.LoginID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@password", mytype = SqlDbType.NVarChar, Value = userToSave.Password });
            DataRow dr;

            dr = Connection.GetSingleRow("spLogin_Mobile");

            UserProfile userprofile = new UserProfile
            {
                LoginID = dr["username"].ToString(),
                Password = dr["userpassword"].ToString(),
                FIRSTNAME = dr["First_Name"].ToString(),
                LASTNAME = dr["Last_Name"].ToString(),
                MIDDLENAME = dr["Middle_Name"].ToString(),
                NTID = dr["username"].ToString(),
                FaceID = dr["faceID"].ToString()
            };
            return userprofile;

        }

        [HttpPost]
        [Route("validateFaceLogin")]
        public UserProfile validateFacelogin(Login userToSave)
        {
            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@FaceId", mytype = SqlDbType.NVarChar, Value = userToSave.FaceID });

            DataRow dr;

            dr = Connection.GetSingleRow("spFacialLogin_Mobile");

            UserProfile userprofile = new UserProfile
            {
                LoginID = dr["username"].ToString(),
                Password = dr["userpassword"].ToString(),
                FIRSTNAME = dr["First_Name"].ToString(),
                LASTNAME = dr["Last_Name"].ToString(),
                MIDDLENAME = dr["Middle_Name"].ToString(),
                NTID = dr["username"].ToString(),
                FaceID = dr["faceID"].ToString(),
                Access = dr["access"].ToString(),
                JoinDate = dr["DateJoined"].ToString(),
                Marrieddate = dr["MarriedDate"].ToString(),
                BIRTHDAY = dr["DOB"].ToString(),
                CIVILSTATUS = dr["MaritalStatus"].ToString(),
                Coordinates = dr["Coordinates"].ToString(),
                EmpStatus = dr["EmpStatus"].ToString()
            };
            return userprofile;

        }


        [HttpPost]
        [Route("AdsFilename")]
        public GetCount AdsFilename(Login userToSave)
        {
            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@empId", mytype = SqlDbType.NVarChar, Value = userToSave.LoginID });

            DataRow dr;

            dr = Connection.GetSingleRow("sp_GetAdsCount_Mobile");

            GetCount userprofile = new GetCount
            {
                AdFilename = dr["AdCount"].ToString(),


            };
            return userprofile;

        }


        //[HttpPost]
        //[Route("GetAdmin")]
        //public string GetAdmins(Login login)
        //{

        //        Connection Connection = new Connection();
        //        Connection.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = login.LoginID });
        //        DataTable dr = new DataTable();
        //        dr = Connection.GetDataTable("sp_Vue_GetVueAccount");
        //    if (dr.Rows.Count > 0)
        //    {
        //        return "Admin";
        //    }
        //    else
        //    {
        //        return "User";
        //    }


        //}

        [HttpPost]
        [Route("UpdateAdName")]
        public string UpdateAdsName()
        {
            Connection Connection = new Connection();



            // DataTable EmployeeDt = new DataTable();
            String ID = Connection.ExecuteScalar("sp_UpdateAdsCount_Mobile");

            return ID;

        }

        [HttpPost]
        [Route("LocationSettings")]
        public string FeatureUpdate(AdminSetting update)
        {
            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = update.EmpID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@GEOSTATUS", mytype = SqlDbType.NVarChar, Value = update.GeoLocStatus });
            Connection.myparameters.Add(new myParameters { ParameterName = "@ELEVATIONSTATUS", mytype = SqlDbType.NVarChar, Value = update.ElevationStatus });
            Connection.myparameters.Add(new myParameters { ParameterName = "@LOCATION", mytype = SqlDbType.NVarChar, Value = update.DefaultLoc });
            String ID = Connection.ExecuteScalar("sp_UpdateLocationSettings_Mobile");

            return ID;

        }

        //greetings
        [HttpPost]
        [Route("Greetings")]
        public List<Greetings> greetings()
        {
            List<Greetings> GetGreetings = new List<Greetings>();
            try
            {
                Connection Connection = new Connection();
                DataTable dr = new DataTable();
                dr = Connection.GetDataTable("sp_GetGreetings_Mobile");
                foreach (DataRow row in dr.Rows)
                {
                    Greetings LoginVue = new Greetings
                    {
                        Images = "http://illimitadodevs.azurewebsites.net/images/logo_gold.png",
                        Message = row["Message"].ToString(),
                        Title = row["Title"].ToString(),
                        Sender = row["sender"].ToString(),
                        Type = row["Type"].ToString(),

                    };
                    GetGreetings.Add(LoginVue);
                }
                return GetGreetings;
            }
            catch
            {
                return GetGreetings;
            }


        }
        [HttpPost]
        [Route("Greetingsv3")]
        public List<GreetingModel> greetingsv3(Loginv2 log)
        {
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = log.CN;
            SelfieRegistration2Controller.GetDB2(GDB);

            List<GreetingModel> GetGreetings = new List<GreetingModel>();
            try
            {
                Connection Connection = new Connection();
                DataTable dr = new DataTable();
                dr = Connection.GetDataTable("sp_GetGreetings_Mobile");
                foreach (DataRow row in dr.Rows)
                {
                    GreetingModel LoginVue = new GreetingModel
                    {
                        Image = row["Image"].ToString(),
                        Message = row["Message"].ToString(),
                        Title = row["Title"].ToString(),
                        Sender = row["sender"].ToString(),
                        Type = row["Type"].ToString(),
                        DateCreated = row["DateCreated"].ToString(),
                        status = row["Status"].ToString()
                    };
                    GetGreetings.Add(LoginVue);
                }
                return GetGreetings;
            }
            catch
            {
                return GetGreetings;
            }
        }


        [HttpPost]
        [Route("Greetingsv4")]
        public List<GreetingModel> greetingsv4(Loginv2 log)
        {
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = log.CN;
            SelfieRegistration2Controller.GetDB2(GDB);

            List<GreetingModel> GetGreetings = new List<GreetingModel>();
            try
            {
                Connection Connection = new Connection();
                DataTable dr = new DataTable();
                Connection.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = log.LoginID });
                dr = Connection.GetDataTable("sp_GetGreetings_Mobilev2");
                foreach (DataRow row in dr.Rows)
                {
                    GreetingModel LoginVue = new GreetingModel
                    {
                        ID = row["ID"].ToString(),
                        Image = row["Image"].ToString(),
                        Message = row["Message"].ToString(),
                        Title = row["Title"].ToString(),
                        Sender = row["sender"].ToString(),
                        Type = row["Type"].ToString(),
                        DateCreated = row["DateCreated"].ToString(),
                        status = row["Status"].ToString(),
                        isSeen = row["isSeen"].ToString(),
                        isShow = row["isShow"].ToString(),
                        NotifType = row["NotifType"].ToString(),
                        NotifLike = row["NotifLike"].ToString()
                    };
                    GetGreetings.Add(LoginVue);
                }
                return GetGreetings;
            }
            catch
            {
                return GetGreetings;
            }
        }

        [HttpPost]
        [Route("GetNotificationGreetingsv1")]
        public List<GreetingModel> GetNotificationGreetingsv1(Loginv2 log)
        {
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = log.CN;
            SelfieRegistration2Controller.GetDB2(GDB);

            List<GreetingModel> GetGreetings = new List<GreetingModel>();
            try
            {
                Connection Connection = new Connection();
                DataTable dr = new DataTable();
                Connection.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = log.LoginID });
                dr = Connection.GetDataTable("sp_GetGreetings_Mobilev2");
                foreach (DataRow row in dr.Rows)
                {
                    byte[] PIC = Convert.FromBase64String(row["Image"].ToString().Contains("data:image/gif") ? row["Image"].ToString().Replace("data:image/gif;base64,", "") : row["Image"].ToString());
                    var path = HttpContext.Current.Server.MapPath("~/ProfilePhotos/" + log.CN.ToLower() + "/" + "");
                    var folder = Path.Combine(path, log.LoginID);
                    Directory.CreateDirectory(folder);
                    MemoryStream stream = new MemoryStream();
                    using (stream)
                    {
                        if (row["Image"].ToString().Contains("data:image/gif"))
                        {
                            File.WriteAllBytes(folder + "/" + log.LoginID + "_greet_" + row["ID"].ToString() + ".gif", PIC);
                            GreetingModel LoginVue = new GreetingModel
                            {
                                ID = row["ID"].ToString(),
                                Image = "/" + log.LoginID + "_greet_" + row["ID"].ToString() + ".gif",
                                Message = row["Message"].ToString(),
                                Title = row["Title"].ToString(),
                                Sender = row["sender"].ToString(),
                                Type = row["Type"].ToString(),
                                DateCreated = row["DateCreated"].ToString(),
                                status = row["Status"].ToString(),
                                isSeen = row["isSeen"].ToString(),
                                isShow = row["isShow"].ToString(),
                                NotifType = row["NotifType"].ToString(),
                                NotifLike = row["NotifLike"].ToString()
                            };
                            GetGreetings.Add(LoginVue);
                        }
                        else
                        {
                            File.WriteAllBytes(folder + "/" + log.LoginID + "_greet_" + row["ID"].ToString() + ".jpeg", PIC);

                            GreetingModel LoginVue = new GreetingModel
                            {
                                ID = row["ID"].ToString(),
                                Image = "/" + log.LoginID + "_greet_" + row["ID"].ToString() + ".jpeg",
                                Message = row["Message"].ToString(),
                                Title = row["Title"].ToString(),
                                Sender = row["sender"].ToString(),
                                Type = row["Type"].ToString(),
                                DateCreated = row["DateCreated"].ToString(),
                                status = row["Status"].ToString(),
                                isSeen = row["isSeen"].ToString(),
                                isShow = row["isShow"].ToString(),
                                NotifType = row["NotifType"].ToString(),
                                NotifLike = row["NotifLike"].ToString()
                            };
                            GetGreetings.Add(LoginVue);

                        }


                    }

                }
                return GetGreetings;
            }
            catch
            {
                return GetGreetings;
            }
        }

        [HttpPost]
        [Route("UpdateShowNotification")]
        public string UpdateShowNotification(NewsAndUpdateModel model)
        {
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = model.CN;
            SelfieRegistration2Controller.GetDB2(GDB);

            try
            {
                Connection con = new Connection();
                DataTable dt = new DataTable();
                con.myparameters.Add(new myParameters { ParameterName = "@newsId", mytype = SqlDbType.NVarChar, Value = model.NewsID });
                con.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.NVarChar, Value = model.EmpID });
                con.myparameters.Add(new myParameters { ParameterName = "@NotifType", mytype = SqlDbType.NVarChar, Value = model.NotifType });
                con.ExecuteScalar("sp_UpdateShowNotification");
                return "Show Update";
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }
        //greetings end

        [HttpPost]
        [Route("GetLocationSettings")]
        public GetAdminSettingsResult GetLocSettings()
        {
            GetAdminSettingsResult GetAdminSettingsResult = new GetAdminSettingsResult();
            List<AdminSetting> AdminSettings = new List<AdminSetting>();
            try
            {
                Connection Connection = new Connection();
                DataTable dtAdminSetting = new DataTable();
                dtAdminSetting = Connection.GetDataTable("sp_GetLocationSettings_Mobile");
                // CultureInfo provider = CultureInfo.InvariantCulture;
                foreach (DataRow row in dtAdminSetting.Rows)
                {
                    AdminSetting AdminSetting = new AdminSetting
                    {
                        EmpID = row["EmpID"].ToString(),
                        GeoLocStatus = row["GeoLocStatus"].ToString(),
                        ElevationStatus = row["ElevationStatus"].ToString(),
                        DefaultLoc = row["DefaultLoc"].ToString()
                    };

                    AdminSettings.Add(AdminSetting);
                    GetAdminSettingsResult.AdminSetting = AdminSettings;
                    GetAdminSettingsResult.myreturn = "Success";
                }
                return GetAdminSettingsResult;
            }
            catch
            {
                GetAdminSettingsResult.myreturn = "Error";
                return GetAdminSettingsResult;
            }


        }

        [HttpPost]
        [Route("GetVueAccount")]
        public List<LoginVue> VueAccount(LoginVue login)
        {
            List<LoginVue> GetLoginVue = new List<LoginVue>();
            try
            {
                Connection Connection = new Connection();
                Connection.myparameters.Add(new myParameters { ParameterName = "@USERNAME", mytype = SqlDbType.NVarChar, Value = login.LoginID });
                Connection.myparameters.Add(new myParameters { ParameterName = "@PASSWORD", mytype = SqlDbType.NVarChar, Value = login.Password });
                DataTable dr = new DataTable();
                dr = Connection.GetDataTable("sp_Vue_GetVueAccount");
                foreach (DataRow row in dr.Rows)
                {
                    LoginVue LoginVue = new LoginVue
                    {
                        LoginID = row["empid"].ToString(),
                        Password = row["password"].ToString(),
                        Picture = row["ProfPic"].ToString()

                    };
                    GetLoginVue.Add(LoginVue);
                }
                return GetLoginVue;
            }
            catch
            {
                return GetLoginVue;
            }


        }

        [HttpPost]
        [Route("InsertVueAccount")]

        public string InsertVueAccount(LoginVue login)
        {
            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = login.LoginID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@PICTURE", mytype = SqlDbType.NVarChar, Value = login.Picture });
            //  Connection.myparameters.Add(new myParameters { ParameterName = "@STARTOREND", mytype = SqlDbType.NVarChar, Value = uBreak.StartOrEnd });

            // DataTable EmployeeDt = new DataTable();
            String ID = Connection.ExecuteScalar("sp_VUE_Insert_VueAccounts");

            string empid = login.LoginID;
            string image = login.Picture;

            KairosClient client = new KairosClient();

            client.ApplicationID = "56302ff1";
            client.ApplicationKey = "727663717176360ed1825e958f5c3d00";

            try
            {
                var enrollResponse = client.Enroll(image, empid, "Illimitado", "FULL");

                // Get the user enrollment transaction info
                var userImage = enrollResponse.Images[0].Transaction;

                // Enroll the user
                return userImage.subject_id;
            }
            catch
            {
                return "null";
            }


        }


        [HttpPost]
        [Route("GetDB")]
        public string GetDB(GetDatabase GetDatabase)
        {
            // com.connectionstring = ConfigurationManager.ConnectionStrings["connectionstring"].ConnectionString.ToString();
            var settings = ConfigurationManager.ConnectionStrings["connectionstring"];
            var fi = typeof(ConfigurationElement).GetField(
                          "_bReadOnly",
                          BindingFlags.Instance | BindingFlags.NonPublic);
            fi.SetValue(settings, false);
            // settings.ConnectionString = "Data Source=tcp:ilmuat.c3srcolvclfy.ap-southeast-1.rds.amazonaws.com,1400;Database=ILM_Main;User Id=devuatuser;Password=ILMV3ry53cur3dMGTpa55w0rd2020;";
            settings.ConnectionString = "Data Source=livedb-ilm.c3srcolvclfy.ap-southeast-1.rds.amazonaws.com,1412;Database=ILM_Main;User Id=ILM_LiveDBUser;Password=ILMV3ry53cur3dMGTpa55w0rd2020;";
            //settings.ConnectionString = "Data Source=192.168.2.4,1444;Database=ILM_Local;User Id=DevUser;Password=securedmgt@1230;";



            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@ClientName", mytype = SqlDbType.NVarChar, Value = GetDatabase.ClientName });
            // DataTable EmployeeDt = new DataTable();
            DataRow dr;
            dr = Connection.GetSingleRow("sp_CheckClientDB");

            GetDatabase dab = new GetDatabase
            {
                ClientName = dr["DB_Settings"].ToString()
            };

            if (dab.ClientName == "ILM_Live")
            // if (dab.ClientName == "ILM_DevSvr")
            {
                // settings.ConnectionString = "Data Source=tcp:ilm-dev-uat.c3srcolvclfy.ap-southeast-1.rds.amazonaws.com,1400;Database=ILM_DevSvr;User Id=devuatuser;Password=ILMV3ry53cur3dMGTpa55w0rd2020;";
                settings.ConnectionString = "Data Source=livedb-ilm.c3srcolvclfy.ap-southeast-1.rds.amazonaws.com,1412;Database=ILM_Live;User Id=ILM_LiveDBUser;Password=ILMV3ry53cur3dMGTpa55w0rd2020;";

            }
            //else if (dab.ClientName == "ILM_DevSvr")
            //{
            //    settings.ConnectionString = "Data Source=tcp:ilm-dev-uat.c3srcolvclfy.ap-southeast-1.rds.amazonaws.com,1400;Database=ILM_DevSvr;User Id=devuatuser;Password=ILMV3ry53cur3dMGTpa55w0rd2020;";
            //    // settings.ConnectionString = "Data Source=livedb-ilm.c3srcolvclfy.ap-southeast-1.rds.amazonaws.com,1412;Database=ILM_DemoA;User Id=ILM_LiveDBUser;Password=ILMV3ry53cur3dMGTpa55w0rd2020;";

            //}
            else if (dab.ClientName == "StarDB")
            {
                settings.ConnectionString = "Data Source=livedb-ilm.c3srcolvclfy.ap-southeast-1.rds.amazonaws.com,1412;Database=StarDB;User Id=ILM_LiveDBUser;Password=ILMV3ry53cur3dMGTpa55w0rd2020;";
                //settings.ConnectionString = "Data Source=tcp:ilmuat.c3srcolvclfy.ap-southeast-1.rds.amazonaws.com,1433;Database=StarDB;User Id=ILMUATSA;Password=ILMV3ry53cur3dMGTpa55w0rd2020;";

            }
            else if (dab.ClientName == "PetBoweDB")
            {
                settings.ConnectionString = "Data Source=tcp:ilmuat.c3srcolvclfy.ap-southeast-1.rds.amazonaws.com,1433;Database=PetBoweDB;User Id=ILMUATSA;Password=ILMV3ry53cur3dMGTpa55w0rd2020;";

            }
            else if (dab.ClientName == "UnionBankDB")
            {
                settings.ConnectionString = "Data Source=tcp:ilmuat.c3srcolvclfy.ap-southeast-1.rds.amazonaws.com,1433;Database=UnionBankDB;User Id=ILMUATSA;Password=ILMV3ry53cur3dMGTpa55w0rd2020;";

            }
            else if (dab.ClientName == "ILM_UAT")
            {
                settings.ConnectionString = "Data Source=tcp:ilmuat.c3srcolvclfy.ap-southeast-1.rds.amazonaws.com,1433;Database=ILM_UAT;User Id=ILMUATSA;Password=ILMV3ry53cur3dMGTpa55w0rd2020;";

            }
            //else if (dab.ClientName == "ILM_Local")
            //{
            //    settings.ConnectionString = "Data Source=192.168.2.4,1444;Database=ILM_Local;User Id=DevUser;Password=securedmgt@1230;";
            //}
            else
            {
                settings.ConnectionString = "Data Source=livedb-ilm.c3srcolvclfy.ap-southeast-1.rds.amazonaws.com,1412;Database=" + dab.ClientName + ";User Id=ILM_LiveDBUser;Password=ILMV3ry53cur3dMGTpa55w0rd2020;";

            }


            return settings.ToString();

        }


        [HttpPost]

        [Route("ChangePassword")]
        public string ChangePassword(ChangePass ChangePass)
        {
            //GetDailyResult GetDailyResult = new GetDailyResult();
            //try
            //{
            Connection Connection = new Connection();
            //List<Daily> DailyList = new List<Daily>();
            //DataTable dtDAILY = new DataTable();
            //BasicFunction BasicFunction = new BasicFunction();
            Connection.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.NVarChar, Value = ChangePass.EmpID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Password", mytype = SqlDbType.NVarChar, Value = ChangePass.NewPassword });

            String ID = Connection.ExecuteScalar("sp_UpdatePassword_Mobile");

            return "Success";


        }
        private static Random random = new Random();
        public static string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        [HttpPost]
        [Route("ForgotPasswordEmail")]

        public string EmailForgot(Emails Emails)
        {
            Connection Connection = new Connection();
            Connection.myparameters.Add(new myParameters { ParameterName = "@EMAIL", mytype = SqlDbType.NVarChar, Value = Emails.Email });
            DataRow dr;
            dr = Connection.GetSingleRow("sp_CheckEmail_ForgotPass_Mobile");

            return dr["EmpID"].ToString();
        }

        [HttpPost]
        [Route("ForgotPassword")]

        public string PasswordForgot(Emails Emails)
        {
            //GetDailyResult GetDailyResult = new GetDailyResult();
            //try
            //{
            Connection Connection = new Connection();
            //List<Daily> DailyList = new List<Daily>();
            //DataTable dtDAILY = new DataTable();
            //BasicFunction BasicFunction = new BasicFunction();
            Connection.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.NVarChar, Value = Emails.EmpID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Code", mytype = SqlDbType.NVarChar, Value = Emails.Code });
            GenCode = Emails.Code;
            String ID = Connection.ExecuteScalar("sp_UpdateCode_Mobile");

            return "Success";


        }
        //public string EmailAddCheck(Emails Emails)
        //{

        //    try
        //    {
        //        Connection Connection = new Connection();

        //        Connection.myparameters.Add(new myParameters { ParameterName = "@EMAIL", mytype = SqlDbType.NVarChar, Value = Emails.Email });
        //        DataRow dr;
        //        dr = Connection.GetSingleRow("sp_CheckEmail_ForgotPass_Mobile");
        //        IDEMP = dr["EmpID"].ToString();
        //        try
        //        {
        //            string GeneratedCode = RandomString(5);
        //            string receiver = "payroll.noreply@illimitado.com";
        //            string emailto = Emails.Email;
        //            string gmailpassword = "LimitLess!1@";
        //            using (MailMessage mm = new MailMessage(receiver, emailto))
        //            {
        //                MailAddress aliasmail = new MailAddress("payroll.noreply@illimitado.com", "payroll.noreply@illimitado.com");
        //                MailAddress aliasreplymail = new MailAddress("payroll.noreply@illimitado.com");
        //                mm.From = aliasmail;
        //                mm.Subject = "Password Reset";
        //                mm.Body = "Please use this randomly generated code as your temporary password to login and change your password. " + '"' + GeneratedCode + '"';
        //                mm.IsBodyHtml = false;
        //                mm.Headers.Add("X-SES-CONFIGURATION-SET", "ConfigSet");
        //                SmtpClient smtp = new SmtpClient();
        //                smtp.Host = "box1256.bluehost.com";
        //                smtp.EnableSsl = true;
        //                NetworkCredential NetworkCred = new NetworkCredential(receiver, gmailpassword);
        //                smtp.UseDefaultCredentials = true;
        //                smtp.Credentials = NetworkCred;
        //                smtp.Port = 587;
        //                smtp.Send(mm);
        //            }
        //            Connection con = new Connection();

        //            con.myparameters.Add(new myParameters { ParameterName = "@NewPassword", mytype = SqlDbType.NVarChar, Value = GeneratedCode });
        //            con.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.NVarChar, Value = dr["EmpID"].ToString() });
        //            con.myparameters.Add(new myParameters { ParameterName = "@CODE", mytype = SqlDbType.NVarChar, Value = '0' });

        //            con.ExecuteNonQuery("sp_Forget_Password_Mobile");
        //            GenCode = GeneratedCode;
        //            return GeneratedCode;
        //        }
        //        catch (Exception e)
        //        {
        //            return e.ToString();
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        return e.ToString();
        //    }

        //}

        // public static string IDEMP;
        public static string GenCode;

        [HttpPost]
        [Route("ForgotPasswordUpdate")]
        public string ForgotPass(Emails Emails)
        {

            Connection con = new Connection();

            con.myparameters.Add(new myParameters { ParameterName = "@NewPassword", mytype = SqlDbType.NVarChar, Value = Emails.Password });
            con.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.NVarChar, Value = Emails.EmpID });
            con.myparameters.Add(new myParameters { ParameterName = "@CODE", mytype = SqlDbType.NVarChar, Value = Emails.Code });

            string Status = con.ExecuteScalar("sp_Forget_Password_Mobile");

            if (Status == "true")
            {
                return "Success";
            }
            else
            {
                return "Invalid Code";
            }
        }

        //[HttpPost]
        //[Route("LoanMakerAds")]
        //public List<Ads> AdsLoanMaker(Adsparam ads)
        //{
        //    List<Ads> Ads = new List<Ads>();
        //    Connection con = new Connection();
        //    DataTable dt = new DataTable();
        //    con.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.NVarChar, Value = ads.EmpID });
        //    dt = con.GetDataTable("sp_LoanMakerAds_Mobile"); // Stored Proc
        //    for (int i = 0; i < dt.Rows.Count; i++)
        //    {
        //        Ads Adss = new Ads();
        //        Adss.Image = dt.Rows[i][0].ToString();
        //        Adss.LoanType = dt.Rows[i][1].ToString();
        //        Adss.LoanID = dt.Rows[i][2].ToString();
        //        Ads.Add(Adss);
        //    }
        //    return Ads;
        //}


        [HttpPost]
        [Route("LoanMakerAds")]
        public GetAdsResult AdsLoanMaker(Adsparam ads)
        {
            GetAdsResult GetDailyResult = new GetAdsResult();
            Connection Connection = new Connection();
            List<Ads> AdsList = new List<Ads>();
            DataTable dtAds = new DataTable();
            Connection.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.NVarChar, Value = ads.EmpID });   //    Connection.myparameters.Add(new myParameters { ParameterName = "@STARTDATE", mytype = SqlDbType.DateTime, Value = Convert.ToDateTime(DailyParameters.Start_Date) });
            dtAds = Connection.GetDataTable("sp_LoanMakerAds_Mobile");

            if (dtAds.Rows.Count > 0)
            {
                foreach (DataRow row in dtAds.Rows)
                {
                    Ads Daily = new Ads
                    {
                        Image = row["img"].ToString(),
                        LoanType = row["LoanType"].ToString(),
                        LoanID = row["LoanID"].ToString()
                    };
                    AdsList.Add(Daily);
                }
                GetDailyResult.Ads = AdsList;
                GetDailyResult.myreturn = "Success";
            }
            else
            {
                GetDailyResult.myreturn = "No Data Available";
            }

            return GetDailyResult;
        }

        [HttpPost]
        [Route("LoanMakerAdsDetails")]
        public GetAdsDetails AdsLoanMakerDetails(Adsparam ads)
        {
            GetAdsDetails GetAdsDetailsResult = new GetAdsDetails();
            Connection Connection = new Connection();
            List<AdsDetails> AdsList = new List<AdsDetails>();
            DataTable dtAds = new DataTable();
            Connection.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.NVarChar, Value = ads.EmpID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@LoanID", mytype = SqlDbType.NVarChar, Value = ads.LoanID });
            dtAds = Connection.GetDataTable("sp_LoanMakerAdsDetails_Mobile");

            if (dtAds.Rows.Count > 0)
            {
                foreach (DataRow row in dtAds.Rows)
                {
                    AdsDetails adddetails = new AdsDetails
                    {
                        PaymentDate = row["PaymentDate"].ToString(),
                        LoanAmount = row["Loan amount"].ToString(),
                        MaxAmount = row["Max Amount"].ToString()
                    };
                    AdsList.Add(adddetails);
                }
                GetAdsDetailsResult.AdsDetails = AdsList;
                GetAdsDetailsResult.myreturn = "Success";
            }
            else
            {
                GetAdsDetailsResult.myreturn = "No Data Available";
            }

            return GetAdsDetailsResult;
        }


        [HttpPost]
        [Route("LoanMakerAdsResult")]
        public string AdsLoanMakerResult(Adsparam ads)
        {
            Connection con = new Connection();
            DataTable dt = new DataTable();
            con.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.NVarChar, Value = ads.EmpID });
            con.myparameters.Add(new myParameters { ParameterName = "@LoanID", mytype = SqlDbType.NVarChar, Value = ads.LoanID });
            con.ExecuteScalar("sp_LoanMakerAdsResult_Mobile");
            return "Success";
        }


        [HttpPost]
        [Route("GetValidConnection")]
        public bool GetValidConnection()
        {
            return true;

        }


        [HttpPost]
        [Route("DeleteFaceID")]
        public string DeleteFaceID(Loginv2 fID)
        {
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = fID.CN;
            SelfieRegistration2Controller.GetDB2(GDB);
            try
            {
                Connection con = new Connection();
                DataTable dt = new DataTable();
                con.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.NVarChar, Value = fID.FaceID });
                con.ExecuteScalar("sp_DeleteFaceID");
                return "Success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }

        }




        //Get News and Update Start 
        [HttpPost]
        [Route("GetNewsAndUpdate")]
        public List<GetNewsAndUpdate> GetNewsAndUpdate(Login Log)
        {
            List<GetNewsAndUpdate> GetNewsAndUpdate = new List<GetNewsAndUpdate>();

            Connection Connection = new Connection();
            DataTable dr = new DataTable();
            Connection.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = Log.LoginID });
            dr = Connection.GetDataTable("sp_GetNewsAndUpdateMobile");
            if (dr.Rows.Count > 0)
            {
                foreach (DataRow row in dr.Rows)
                {
                    GetNewsAndUpdate Logs = new GetNewsAndUpdate
                    {
                        Image = row["Image"].ToString(),
                        Title = row["Title"].ToString(),
                        Message = row["Message"].ToString(),
                        SenderName = row["SenderName"].ToString(),
                        SenderID = row["SenderID"].ToString(),
                        IsSeen = row["IsSeen"].ToString(),
                        NewAndUpdateID = row["ID"].ToString()
                    };
                    GetNewsAndUpdate.Add(Logs);
                }
                return GetNewsAndUpdate;
            }
            else
            {
                return GetNewsAndUpdate;
            }

        }

        //Get News and Update  End

        //Update as Seen News and Update  Start
        [HttpPost]
        [Route("UpdateNewsAndUpdate")]
        public string UpdateNewsAndUpdate(GetNewsAndUpdate GNU)
        {
            Connection con = new Connection();
            DataTable dt = new DataTable();
            con.myparameters.Add(new myParameters { ParameterName = "@NEWANDUPDATEID", mytype = SqlDbType.NVarChar, Value = GNU.NewAndUpdateID });
            con.ExecuteScalar("sp_UpdateNewsAndUpdate");
            return "Success";
        }
        //Update as Seen News and Update  End


        //Get Survey Start
        [HttpPost]
        [Route("GetSurvey")]
        public surveyL GetSurvey(Login Log)
        {



            surveyL ListUs = new surveyL();

            List<SList> ListReturned = new List<SList>();
            DataTable DT = new DataTable();
            Connection con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.NVarChar, Value = Log.LoginID });
            DT = con.GetDataTable("sp_GetSurveyQuestionMobileDetails");

            for (int i = 0; i < DT.Rows.Count; i++)
            {
                //Survey
                SList SL = new SList();
                SL.SurveyID = DT.Rows[i]["surveyID"].ToString();
                SL.SurveyTitle = DT.Rows[i]["surveyName"].ToString();
                SL.SurveyDescription = DT.Rows[i]["Description"].ToString();
                string UID = DT.Rows[i]["surveyID"].ToString();

                DataTable DTg = new DataTable();
                Connection acon = new Connection();
                acon.myparameters.Add(new myParameters { ParameterName = "@surveyID", mytype = SqlDbType.NVarChar, Value = UID });
                DTg = acon.GetDataTable("sp_QSurveyLoop");
                List<SurveyList> SALL = new List<SurveyList>();
                for (int ii = 0; ii < DTg.Rows.Count; ii++)
                {

                    //SurveyList
                    SurveyList SVL = new SurveyList();
                    SVL.QuestionNum = DTg.Rows[ii]["QNO"].ToString();
                    SVL.Question = DTg.Rows[ii]["QUESTION"].ToString();
                    SVL.QuestionType = DTg.Rows[ii]["QUESTIONTYPE"].ToString();
                    SVL.MinScale = DTg.Rows[ii]["MINSCALEG"].ToString();
                    SVL.MaxScale = DTg.Rows[ii]["MAXSCALEG"].ToString();



                    //Choice List
                    SChoiceList CL = new SChoiceList();

                    //string allchoices = DTg.Rows[ii]["OPT1"].ToString() + "," + DTg.Rows[ii]["OPT2"].ToString() + "," + DTg.Rows[ii]["OPT3"].ToString() + "," + DTg.Rows[ii]["OPT4"].ToString() + "," + DTg.Rows[ii]["OPT5"].ToString() + "," + DTg.Rows[ii]["OPT6"].ToString() + "," + DTg.Rows[ii]["OPT7"].ToString() + "," + DTg.Rows[ii]["OPT8"].ToString() + "," + DTg.Rows[ii]["OPT9"].ToString() + "," + DTg.Rows[ii]["OPT10"].ToString();

                    List<string> allchoices = new List<string>();
                    allchoices.Clear();


                    for (int k = 1; k <= 10; k++)
                    {
                        string data = "";
                        data = DTg.Rows[ii]["OPT" + k + ""].ToString();

                        if (DTg.Rows[ii]["OPT" + k + ""].ToString() != "")
                        {
                            allchoices.Add(DTg.Rows[ii]["OPT" + k + ""].ToString());
                        }

                    }




                    string ScaleGuide = DTg.Rows[ii]["MINSCALEV"].ToString() + "," + DTg.Rows[ii]["MAXCALEV"].ToString();
                    List<SChoiceList> GSL = new List<SChoiceList>();
                    if (SVL.QuestionType == "Multi" || SVL.QuestionType == "Single")
                    {

                        foreach (string item in allchoices)
                        {
                            SChoiceList LML = new SChoiceList();
                            LML.Choicedata = item;
                            GSL.Add(LML);
                        }
                        SVL.Choices = GSL;


                    }
                    else
                    {
                        //List<SChoiceList> GSL = new List<SChoiceList>();
                        string[] scales = ScaleGuide.Split(',');
                        foreach (string item in scales)
                        {
                            SChoiceList LML = new SChoiceList();
                            LML.Choicedata = item;
                            GSL.Add(LML);
                        }

                        SVL.Choices = GSL;
                    }


                    SALL.Add(SVL);

                    SL.SurveyList = SALL;



                }

                ListReturned.Add(SL);

                ListUs.Survey = ListReturned;

            }
            ListUs.myreturn = "Success";
            return ListUs;
        }

        //Get EmpAccess Start
        [HttpPost]
        [Route("GetMobileEmpAcces")]
        public getMobileAccess GetMobileEmpAcces(employee emp)
        {
            getMobileAccess ListUs = new getMobileAccess();
            DataTable DT = new DataTable();
            Connection con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = emp.empid });
            DT = con.GetDataTable("sp_GetEmpidandAccess");
            string access = "False";
            for (int i = 0; i < DT.Rows.Count; i++)
            {
                if (DT.Rows[i]["parameter1"].ToString() == "Mobile Time In")
                {
                    access = "True";
                }
            }
            ListUs.isAccess = access;

            return ListUs;
            //Get EmpAccess  End
        }

        [HttpPost]
        [Route("GetReportsAccess")]
        public getMobileAccess GetReportsAccess(employeev2 emp)
        {
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = emp.CN;
            SelfieRegistration2Controller.GetDB2(GDB);

            getMobileAccess ListUs = new getMobileAccess();
            DataTable DT = new DataTable();
            Connection con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = emp.empid });
            DT = con.GetDataTable("sp_GetEmpidandAccessv2");
            string access = "False";
            for (int i = 0; i < DT.Rows.Count; i++)
            {
                if (DT.Rows[i]["parameter1"].ToString() == "Reports")
                {
                    access = "True";
                }
            }
            ListUs.isAccess = access;

            return ListUs;
            //Get EmpAccess  End
        }


        [HttpPost]
        [Route("GetMasterControlAccess")]
        public getMobileAccess GetMasterControlAccess(employeev2 emp)
        {
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = emp.CN;
            SelfieRegistration2Controller.GetDB2(GDB);

            getMobileAccess ListUs = new getMobileAccess();
            DataTable DT = new DataTable();
            Connection con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = emp.empid });
            DT = con.GetDataTable("sp_GetEmpidandAccessv2");
            string access = "False";
            for (int i = 0; i < DT.Rows.Count; i++)
            {
                if (DT.Rows[i]["parameter1"].ToString() == "Master Control")
                {
                    access = "True";
                }
            }
            ListUs.isAccess = access;

            return ListUs;
            //Get EmpAccess  End
        }



        //---------------------------------------------------------------Revised Change DB Start------------------------------------------------------------------------
        [HttpPost]
        [Route("validateFaceLoginv2")]
        public UserProfilev2 validateFacelogin2(Loginv2 userToSave)
        {
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = userToSave.CN;
            SelfieRegistration2Controller.GetDB2(GDB);
            Connection Connection = new Connection();
            Connection.myparameters.Add(new myParameters { ParameterName = "@FaceId", mytype = SqlDbType.NVarChar, Value = userToSave.FaceID });
            DataRow dr;
            dr = Connection.GetSingleRow("spFacialLogin_Mobile");
            UserProfilev2 userprofile = new UserProfilev2
            {
                LoginID = dr["username"].ToString(),
                Password = dr["userpassword"].ToString(),
                FIRSTNAME = dr["First_Name"].ToString(),
                LASTNAME = dr["Last_Name"].ToString(),
                MIDDLENAME = dr["Middle_Name"].ToString(),
                NTID = dr["username"].ToString(),
                FaceID = dr["faceID"].ToString(),
                Access = dr["access"].ToString(),
                JoinDate = dr["DateJoined"].ToString(),
                Marrieddate = dr["MarriedDate"].ToString(),
                BIRTHDAY = dr["DOB"].ToString(),
                CIVILSTATUS = dr["MaritalStatus"].ToString(),
                Coordinates = dr["Coordinates"].ToString(),
                EmpStatus = dr["EmpStatus"].ToString()
            };
            return userprofile;
        }

        [HttpPost]
        [Route("LocationSettingsv2")]
        public string FeatureUpdatev2(AdminSettingv2 update)
        {
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = update.CN;
            SelfieRegistration2Controller.GetDB2(GDB);
            Connection Connection = new Connection();
            Connection.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = update.EmpID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@GEOSTATUS", mytype = SqlDbType.NVarChar, Value = update.GeoLocStatus });
            Connection.myparameters.Add(new myParameters { ParameterName = "@ELEVATIONSTATUS", mytype = SqlDbType.NVarChar, Value = update.ElevationStatus });
            Connection.myparameters.Add(new myParameters { ParameterName = "@LOCATION", mytype = SqlDbType.NVarChar, Value = update.DefaultLoc });
            String ID = Connection.ExecuteScalar("sp_UpdateLocationSettings_Mobile");
            return ID;
        }

        [HttpPost]
        [Route("GetLocationSettingsv2")]
        public GetAdminSettingsResultv2 GetLocSettings(Loginv2 login)
        {
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = login.CN;
            SelfieRegistration2Controller.GetDB2(GDB);
            GetAdminSettingsResultv2 GetAdminSettingsResult = new GetAdminSettingsResultv2();
            List<AdminSettingv2> AdminSettings = new List<AdminSettingv2>();
            try
            {
                Connection Connection = new Connection();
                DataTable dtAdminSetting = new DataTable();
                dtAdminSetting = Connection.GetDataTable("sp_GetLocationSettings_Mobile");
                // CultureInfo provider = CultureInfo.InvariantCulture;
                foreach (DataRow row in dtAdminSetting.Rows)
                {
                    AdminSettingv2 AdminSetting = new AdminSettingv2
                    {
                        EmpID = row["EmpID"].ToString(),
                        GeoLocStatus = row["GeoLocStatus"].ToString(),
                        ElevationStatus = row["ElevationStatus"].ToString(),
                        DefaultLoc = row["DefaultLoc"].ToString()
                    };

                    AdminSettings.Add(AdminSetting);
                    GetAdminSettingsResult.AdminSetting = AdminSettings;
                    GetAdminSettingsResult.myreturn = "Success";
                }
                return GetAdminSettingsResult;
            }
            catch
            {
                GetAdminSettingsResult.myreturn = "Error";
                return GetAdminSettingsResult;
            }
        }
        [HttpPost]
        [Route("InsertIntoGenBiov3")]
        public async Task<BundyReturnParams> InsertIntoGenBiov3(BundyParams us)
        {
            BundyReturnParams brp = new BundyReturnParams();
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = us.CN;
            SelfieRegistration2Controller.GetDB2(GDB);
            Connection con = new Connection();


            if (us.base64.ToLower() == "luxand")
            {
                try
                {
                    con = new Connection();
                    con.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.VarChar, Value = us.EmpID });
                    con.myparameters.Add(new myParameters { ParameterName = "@TIME", mytype = SqlDbType.DateTime, Value = us.DTInserted });
                    con.ExecuteNonQuery("sp_InsertBioLuxand");
                }
                catch (Exception ex)
                {
                    brp.myReturn = ex.ToString();
                }
            }
            else
            {
                byte[] returnByte = Convert.FromBase64String(us.base64);
                Stream inputStream = new MemoryStream(returnByte);
                try
                {
                    FaceServiceClient faceServiceClient = new FaceServiceClient("bc6cf65c8dab414f92383ed4830341dd", "https://southeastasia.api.cognitive.microsoft.com/face/v1.0");
                    string personGroupId = "";
                    //string personGroupId = "ilmdevs";
                    if (us.CN.ToLower() == "lomedagroupofcompanies")
                    {
                        personGroupId = "hondacarsbatangas";
                    }
                    else
                    {
                        personGroupId = us.CN.ToLower();
                    }
                    var faces = await faceServiceClient.DetectAsync(inputStream);
                    var faceIds = faces.Select(face => face.FaceId).ToArray();

                    try
                    {
                        var results = await faceServiceClient.IdentifyAsync(personGroupId, faceIds, 0.8f, 1);

                        if (results.Count() > 1)
                        {
                            brp.myReturn = "Multiple Faces Detected";
                            return brp;
                        }
                        else
                        {
                            var candidateId = results.FirstOrDefault().Candidates.FirstOrDefault().PersonId;
                            var confidthres = results.FirstOrDefault().Candidates.FirstOrDefault().Confidence;
                            var person = await faceServiceClient.GetPersonAsync(personGroupId, candidateId);

                            if (person != null)
                            {
                                con.myparameters.Add(new myParameters { ParameterName = "@faceid", mytype = SqlDbType.VarChar, Value = person.PersonId.ToString() });
                                DataTable dt = con.GetDataTable("sp_hrms_bundyclock_newflow");
                                foreach (DataRow item in dt.Rows)
                                {
                                    brp.EmpID = item["empID"].ToString();
                                    brp.firstName = item["first_name"].ToString();
                                    brp.middleName = item["middle_name"].ToString();
                                    brp.lastName = item["last_name"].ToString();
                                    brp.myReturn = "Success";

                                    con = new Connection();
                                    con.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.NVarChar, Value = item["empID"].ToString() });
                                    con.ExecuteNonQuery("sp_Insert_GenBio_Events");

                                    con = new Connection();
                                    con.myparameters.Add(new myParameters { ParameterName = "@FaceID", mytype = SqlDbType.NVarChar, Value = person.PersonId.ToString() });
                                    con.myparameters.Add(new myParameters { ParameterName = "@Confid", mytype = SqlDbType.NVarChar, Value = confidthres.ToString() });
                                    con.ExecuteNonQuery("sp_InsertConfidThresh");
                                }
                            }

                        }
                    }
                    catch (Exception ex)
                    {
                        brp.myReturn = ex.ToString();
                    }
                }
                catch (Exception ex)
                {
                    brp.myReturn = ex.ToString();
                }
            }



            return brp;
        }
        [HttpPost]
        [Route("InsertIntoGenBiov2")]
        public async Task<BundyReturnParams> InsertIntoGenBiov2(BundyParams us)
        {
            BundyReturnParams brp = new BundyReturnParams();
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = us.CN;
            SelfieRegistration2Controller.GetDB2(GDB);
            Connection con = new Connection();

            byte[] returnByte = Convert.FromBase64String(us.base64);
            Stream inputStream = new MemoryStream(returnByte);
            try
            {
                FaceServiceClient faceServiceClient = new FaceServiceClient("bc6cf65c8dab414f92383ed4830341dd", "https://southeastasia.api.cognitive.microsoft.com/face/v1.0");
                string personGroupId = us.CN.ToLower();
                var faces = await faceServiceClient.DetectAsync(inputStream);
                var faceIds = faces.Select(face => face.FaceId).ToArray();

                try
                {
                    var results = await faceServiceClient.IdentifyAsync(personGroupId, faceIds);

                    if (results.Count() > 1)
                    {
                        brp.myReturn = "Multiple Faces Detected";
                        return brp;
                    }
                    else
                    {
                        var candidateId = results.FirstOrDefault().Candidates.FirstOrDefault().PersonId;
                        var confidthres = results.FirstOrDefault().Candidates.FirstOrDefault().Confidence;
                        var person = await faceServiceClient.GetPersonAsync(personGroupId, candidateId);

                        if (person != null)
                        {
                            con.myparameters.Add(new myParameters { ParameterName = "@faceid", mytype = SqlDbType.VarChar, Value = person.PersonId.ToString() });
                            DataTable dt = con.GetDataTable("sp_hrms_bundyclock_newflow");
                            foreach (DataRow item in dt.Rows)
                            {
                                brp.EmpID = item["empID"].ToString();
                                brp.firstName = item["first_name"].ToString();
                                brp.middleName = item["middle_name"].ToString();
                                brp.lastName = item["last_name"].ToString();
                                brp.myReturn = "Success";

                                con = new Connection();
                                con.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.NVarChar, Value = item["empID"].ToString() });
                                con.ExecuteNonQuery("sp_Insert_GenBio_Events");
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    brp.myReturn = ex.ToString();
                }
            }
            catch (Exception ex)
            {
                brp.myReturn = ex.ToString();
            }

            return brp;
        }
        [HttpPost]
        [Route("CheckAvailablePayslip_Mobile")]
        public string CheckAvailablePayslip_Mobile(Loginv2 Client)
        {
            try
            {
                var settings = ConfigurationManager.ConnectionStrings["connectionstring"];
                var fi = typeof(ConfigurationElement).GetField(
                              "_bReadOnly",
                              BindingFlags.Instance | BindingFlags.NonPublic);
                fi.SetValue(settings, false);
                settings.ConnectionString = "Data Source=livedb-ilm.c3srcolvclfy.ap-southeast-1.rds.amazonaws.com,1412;Database=ILM_Main;User Id=ILM_LiveDBUser;Password=ILMV3ry53cur3dMGTpa55w0rd2020;";

                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@CN", mytype = SqlDbType.NVarChar, Value = Client.CN });
                return con.ExecuteScalar("sp_CheckAvailablePayslip").ToString();
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }
        [HttpPost]
        [Route("AccessToOfflineMode_Mobile")]
        public string AccessToOfflineMode_Mobile(Loginv2 Client)
        {
            try
            {
                var settings = ConfigurationManager.ConnectionStrings["connectionstring"];
                var fi = typeof(ConfigurationElement).GetField(
                              "_bReadOnly",
                              BindingFlags.Instance | BindingFlags.NonPublic);
                fi.SetValue(settings, false);
                settings.ConnectionString = "Data Source=livedb-ilm.c3srcolvclfy.ap-southeast-1.rds.amazonaws.com,1412;Database=ILM_Main;User Id=ILM_LiveDBUser;Password=ILMV3ry53cur3dMGTpa55w0rd2020;";

                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@CN", mytype = SqlDbType.NVarChar, Value = Client.CN });
                return con.ExecuteScalar("sp_AccessToOfflineMode").ToString();
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }
        [HttpPost]
        [Route("MaxHours_Mobile")]
        public string MaxHours_Mobile(Loginv2 Client)
        {
            try
            {
                var settings = ConfigurationManager.ConnectionStrings["connectionstring"];
                var fi = typeof(ConfigurationElement).GetField(
                              "_bReadOnly",
                              BindingFlags.Instance | BindingFlags.NonPublic);
                fi.SetValue(settings, false);
                settings.ConnectionString = "Data Source=livedb-ilm.c3srcolvclfy.ap-southeast-1.rds.amazonaws.com,1412;Database=ILM_Main;User Id=ILM_LiveDBUser;Password=ILMV3ry53cur3dMGTpa55w0rd2020;";

                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@CN", mytype = SqlDbType.NVarChar, Value = Client.CN });
                return con.ExecuteScalar("sp_MaxHours_Mobile").ToString();
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }
        [HttpPost]
        [Route("GetOnboardingSettingsv1")]
        public string OnboardingSettings(Loginv2 Client)
        {
            try
            {
                var settings = ConfigurationManager.ConnectionStrings["connectionstring"];
                var fi = typeof(ConfigurationElement).GetField(
                              "_bReadOnly",
                              BindingFlags.Instance | BindingFlags.NonPublic);
                fi.SetValue(settings, false);
                settings.ConnectionString = "Data Source=livedb-ilm.c3srcolvclfy.ap-southeast-1.rds.amazonaws.com,1412;Database=ILM_Main;User Id=ILM_LiveDBUser;Password=ILMV3ry53cur3dMGTpa55w0rd2020;";

                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@DBName", mytype = SqlDbType.NVarChar, Value = Client.CN });
                return con.ExecuteScalar("sp_OnboardingSettings").ToString();
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }
        [HttpPost]
        [Route("GetDHROnboardingEmpInfoSettings")]
        public string EmployeeInfoSettings(Loginv2 Client)
        {
            try
            {
                var settings = ConfigurationManager.ConnectionStrings["connectionstring"];
                var fi = typeof(ConfigurationElement).GetField(
                              "_bReadOnly",
                              BindingFlags.Instance | BindingFlags.NonPublic);
                fi.SetValue(settings, false);
                settings.ConnectionString = "Data Source=livedb-ilm.c3srcolvclfy.ap-southeast-1.rds.amazonaws.com,1412;Database=ILM_Main;User Id=ILM_LiveDBUser;Password=ILMV3ry53cur3dMGTpa55w0rd2020;";

                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@DBName", mytype = SqlDbType.NVarChar, Value = Client.CN });
                return con.ExecuteScalar("sp_OnboardingEmpInfoSettings").ToString();
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }
        [HttpPost]
        [Route("DHRChechILMorGen")]
        public string CheckILMorGen(Loginv2 Client)
        {
            try
            {
                var settings = ConfigurationManager.ConnectionStrings["connectionstring"];
                var fi = typeof(ConfigurationElement).GetField(
                              "_bReadOnly",
                              BindingFlags.Instance | BindingFlags.NonPublic);
                fi.SetValue(settings, false);
                settings.ConnectionString = "Data Source=livedb-ilm.c3srcolvclfy.ap-southeast-1.rds.amazonaws.com,1412;Database=ILM_Main;User Id=ILM_LiveDBUser;Password=ILMV3ry53cur3dMGTpa55w0rd2020;";

                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@ClientName", mytype = SqlDbType.NVarChar, Value = Client.CN });
                return con.ExecuteScalar("sp_CheckILMorGen").ToString();
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }
        [HttpPost]
        [Route("InsertIntoGenBio")]
        public string InsertIntoGenBio(Loginv2 us)
        {
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = us.CN;
            SelfieRegistration2Controller.GetDB2(GDB);
            Connection con = new Connection(); ;
            con.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.NVarChar, Value = us.LoginID });
            con.ExecuteNonQuery("sp_Insert_GenBio_Events");
            return "success";
        }

        [HttpPost]
        [Route("validateLoginv2")]
        public UserProfilev2 validateloginv2(Loginv2 userToSave)
        {
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = userToSave.CN;
            SelfieRegistration2Controller.GetDB2(GDB);
            Connection Connection = new Connection();
            DataTable DT = new DataTable();
            Connection.myparameters.Add(new myParameters { ParameterName = "@empId", mytype = SqlDbType.NVarChar, Value = userToSave.LoginID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@password", mytype = SqlDbType.NVarChar, Value = userToSave.Password });
            DT = Connection.GetDataTable("spLogin_Mobile");

            tempconnectionstring();
            Connection = new Connection();
            Connection.myparameters.Add(new myParameters { ParameterName = "@CN", mytype = SqlDbType.NVarChar, Value = userToSave.CN });
            string d = Connection.ExecuteScalar("sp_getMobileOnboardingValue");
            UserProfilev2 User = new UserProfilev2();
            if (DT.Rows.Count > 0)
            {
                User = new UserProfilev2();
                User.LoginID = DT.Rows[0]["username"].ToString();
                User.onBoardstatus = d;
                User.Password = DT.Rows[0]["userpassword"].ToString();
                User.FIRSTNAME = DT.Rows[0]["First_Name"].ToString();
                User.LASTNAME = DT.Rows[0]["Last_Name"].ToString();
                User.MIDDLENAME = DT.Rows[0]["Middle_Name"].ToString();
                User.NTID = DT.Rows[0]["username"].ToString();
                User.FaceID = DT.Rows[0]["faceID"].ToString();
                User.Access = DT.Rows[0]["access"].ToString();
                User.JoinDate = DT.Rows[0]["DateJoined"].ToString();
                User.BIRTHDAY = DT.Rows[0]["DOB"].ToString();
                User.CIVILSTATUS = DT.Rows[0]["MaritalStatus"].ToString();
                if (User.CIVILSTATUS.ToLower().Trim() == "married")
                {
                    User.Marrieddate = DT.Rows[0]["MarriedDate"].ToString();
                    User.MaritalStatus = DT.Rows[0]["MaritalStatus"].ToString();
                }
                else
                {
                    User.Marrieddate = string.Empty;
                }
                User.Coordinates = DT.Rows[0]["Coordinates"].ToString();
                User.EmpStatus = DT.Rows[0]["EmpStatus"].ToString();
            }
            return User;

        }


        [HttpPost]
        [Route("validateLoginv3")]
        public LoginLuxandModels validateloginv3(Loginv2 userToSave)
        {
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = userToSave.CN;
            SelfieRegistration2Controller.GetDB2(GDB);
            Connection Connection = new Connection();
            DataTable DT = new DataTable();
            Connection.myparameters.Add(new myParameters { ParameterName = "@empId", mytype = SqlDbType.NVarChar, Value = userToSave.LoginID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@password", mytype = SqlDbType.NVarChar, Value = userToSave.Password });
            DT = Connection.GetDataTable("spLogin_Mobilev2");
            LoginLuxandModels User = new LoginLuxandModels();
            if (DT.Rows.Count > 0)
            {
                User = new LoginLuxandModels();
                User.LoginID = DT.Rows[0]["username"].ToString();

                User.Password = DT.Rows[0]["userpassword"].ToString();
                User.FIRSTNAME = DT.Rows[0]["First_Name"].ToString();
                User.LASTNAME = DT.Rows[0]["Last_Name"].ToString();
                User.MIDDLENAME = DT.Rows[0]["Middle_Name"].ToString();
                User.NTID = DT.Rows[0]["username"].ToString();
                User.FaceID = DT.Rows[0]["faceID"].ToString();
                User.Access = DT.Rows[0]["access"].ToString();
                User.JoinDate = DT.Rows[0]["DateJoined"].ToString();
                User.BIRTHDAY = DT.Rows[0]["DOB"].ToString();
                User.CIVILSTATUS = DT.Rows[0]["MaritalStatus"].ToString();
                if (User.CIVILSTATUS.ToLower().Trim() == "married")
                {
                    User.Marrieddate = DT.Rows[0]["MarriedDate"].ToString();
                    User.MaritalStatus = DT.Rows[0]["MaritalStatus"].ToString();
                }
                else
                {
                    User.Marrieddate = string.Empty;
                }
                User.Coordinates = DT.Rows[0]["Coordinates"].ToString();
                User.EmpStatus = DT.Rows[0]["EmpStatus"].ToString();
                User.LuxandKey = DT.Rows[0]["LuxandKey"].ToString();
            }
            return User;

        }

        [HttpPost]
        [Route("CheckDefaultPassword")]
        public async Task<bool> CheckDefaultPassword(CheckDefault user)
        {
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = user.CN;
            SelfieRegistration2Controller.GetDB2(GDB);
            Connection Connection = new Connection();
            DataTable DT = new DataTable();
            Connection.myparameters.Add(new myParameters { ParameterName = "@USERID", mytype = SqlDbType.NVarChar, Value = user.LoginID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@password", mytype = SqlDbType.NVarChar, Value = user.Password });
            DT = Connection.GetDataTable("spLogin2");
            CheckDefault User = new CheckDefault();
            if (DT.Rows.Count > 0)
            {
                User = new CheckDefault();
                User.LoginID = DT.Rows[0]["empId"].ToString();
                User.Password = DT.Rows[0]["password"].ToString();
                User.DefaultPassword = DT.Rows[0]["DefaultPassword"].ToString();
                User.CustomPassword = DT.Rows[0]["CustomPassword"].ToString();
            }

            if (string.IsNullOrEmpty(User.CustomPassword))
            {
                if (user.Password == User.DefaultPassword)
                {
                    return true;
                }
            }
            else
            {
                if (user.Password == User.CustomPassword)
                {
                    return true;
                }
            }
            return false;
        }
        [HttpPost]
        [Route("testvalidatelogin")]
        public UserProfilev2 testvalidatelogin(Loginv2 userToSave)
        {
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = userToSave.CN;
            SelfieRegistration2Controller.GetDB2(GDB);
            Connection Connection = new Connection();
            DataTable DT = new DataTable();
            Connection.myparameters.Add(new myParameters { ParameterName = "@empId", mytype = SqlDbType.NVarChar, Value = userToSave.LoginID });
            DT = Connection.GetDataTable("spLogin_Mobile_test");
            UserProfilev2 User = new UserProfilev2();
            if (DT.Rows.Count > 0)
            {
                User = new UserProfilev2();
                User.LoginID = DT.Rows[0]["username"].ToString();
                User.FIRSTNAME = DT.Rows[0]["First_Name"].ToString();
                User.LASTNAME = DT.Rows[0]["Last_Name"].ToString();
                User.MIDDLENAME = DT.Rows[0]["Middle_Name"].ToString();
                User.NTID = DT.Rows[0]["username"].ToString();
                User.FaceID = DT.Rows[0]["faceID"].ToString();
                User.Access = DT.Rows[0]["access"].ToString();
                User.JoinDate = DT.Rows[0]["DateJoined"].ToString();
                User.BIRTHDAY = DT.Rows[0]["DOB"].ToString();
                User.CIVILSTATUS = DT.Rows[0]["MaritalStatus"].ToString();
                if (User.CIVILSTATUS.ToLower().Trim() == "married")
                {
                    User.Marrieddate = DT.Rows[0]["MarriedDate"].ToString();
                    User.MaritalStatus = DT.Rows[0]["MaritalStatus"].ToString();
                }
                else
                {
                    User.Marrieddate = string.Empty;
                }
                User.Coordinates = DT.Rows[0]["Coordinates"].ToString();
                User.EmpStatus = DT.Rows[0]["EmpStatus"].ToString();
            }
            return User;

        }

        [HttpPost]
        [Route("GetSurveyv2")]
        public surveyL GetSurvey(Loginv2 Log)
        {
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = Log.CN;
            SelfieRegistration2Controller.GetDB2(GDB);

            surveyL ListUs = new surveyL();
            List<SList> ListReturned = new List<SList>();
            DataTable DT = new DataTable();
            Connection con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.NVarChar, Value = Log.LoginID });
            DT = con.GetDataTable("sp_GetSurveyQuestionMobileDetails");
            for (int i = 0; i < DT.Rows.Count; i++)
            {
                //Survey
                SList SL = new SList();
                SL.SurveyID = DT.Rows[i]["surveyID"].ToString();
                SL.SurveyTitle = DT.Rows[i]["surveyName"].ToString();
                SL.SurveyDescription = DT.Rows[i]["Description"].ToString();
                string UID = DT.Rows[i]["surveyID"].ToString();
                DataTable DTg = new DataTable();
                Connection acon = new Connection();
                acon.myparameters.Add(new myParameters { ParameterName = "@surveyID", mytype = SqlDbType.NVarChar, Value = UID });
                DTg = acon.GetDataTable("sp_QSurveyLoop");
                List<SurveyList> SALL = new List<SurveyList>();
                for (int ii = 0; ii < DTg.Rows.Count; ii++)
                {
                    //SurveyList
                    SurveyList SVL = new SurveyList();
                    SVL.QuestionNum = DTg.Rows[ii]["QNO"].ToString();
                    SVL.Question = DTg.Rows[ii]["QUESTION"].ToString();
                    SVL.QuestionType = DTg.Rows[ii]["QUESTIONTYPE"].ToString();
                    SVL.MinScale = DTg.Rows[ii]["MINSCALEG"].ToString();
                    SVL.MaxScale = DTg.Rows[ii]["MAXSCALEG"].ToString();
                    //Choice List
                    SChoiceList CL = new SChoiceList();
                    List<string> allchoices = new List<string>();
                    allchoices.Clear();
                    for (int k = 1; k <= 10; k++)
                    {
                        string data = "";
                        data = DTg.Rows[ii]["OPT" + k + ""].ToString();
                        if (DTg.Rows[ii]["OPT" + k + ""].ToString() != "")
                        {
                            allchoices.Add(DTg.Rows[ii]["OPT" + k + ""].ToString());
                        }
                    }
                    string ScaleGuide = DTg.Rows[ii]["MINSCALEV"].ToString() + "," + DTg.Rows[ii]["MAXCALEV"].ToString();
                    List<SChoiceList> GSL = new List<SChoiceList>();
                    if (SVL.QuestionType == "Multi" || SVL.QuestionType == "Single")
                    {
                        foreach (string item in allchoices)
                        {
                            SChoiceList LML = new SChoiceList();
                            LML.Choicedata = item;
                            GSL.Add(LML);
                        }
                        SVL.Choices = GSL;
                    }
                    else
                    {
                        //List<SChoiceList> GSL = new List<SChoiceList>();
                        string[] scales = ScaleGuide.Split(',');
                        foreach (string item in scales)
                        {
                            SChoiceList LML = new SChoiceList();
                            LML.Choicedata = item;
                            GSL.Add(LML);
                        }
                        SVL.Choices = GSL;
                    }
                    SALL.Add(SVL);
                    SL.SurveyList = SALL;
                }
                ListReturned.Add(SL);
                ListUs.Survey = ListReturned;
            }
            ListUs.myreturn = "Success";
            return ListUs;
        }

        [HttpPost]
        [Route("LoanMakerAdsDetailsv2")]
        public GetAdsDetails AdsLoanMakerDetailsv2(Adsparamv2 ads)
        {
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = ads.CN;
            SelfieRegistration2Controller.GetDB2(GDB);

            GetAdsDetails GetAdsDetailsResult = new GetAdsDetails();
            Connection Connection = new Connection();
            List<AdsDetails> AdsList = new List<AdsDetails>();
            DataTable dtAds = new DataTable();
            Connection.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.NVarChar, Value = ads.EmpID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@LoanID", mytype = SqlDbType.NVarChar, Value = ads.LoanID });
            dtAds = Connection.GetDataTable("sp_LoanMakerAdsDetails_Mobile");

            if (dtAds.Rows.Count > 0)
            {
                foreach (DataRow row in dtAds.Rows)
                {
                    AdsDetails adddetails = new AdsDetails
                    {
                        PaymentDate = row["PaymentDate"].ToString(),
                        LoanAmount = row["Loan amount"].ToString(),
                        MaxAmount = row["Max Amount"].ToString()
                    };
                    AdsList.Add(adddetails);
                }
                GetAdsDetailsResult.AdsDetails = AdsList;
                GetAdsDetailsResult.myreturn = "Success";
            }
            else
            {
                GetAdsDetailsResult.myreturn = "No Data Available";
            }

            return GetAdsDetailsResult;
        }

        [HttpPost]
        [Route("LoanMakerAdsv2")]
        public GetAdsResult AdsLoanMakerv2(Adsparamv2 ads)
        {
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = ads.CN;
            SelfieRegistration2Controller.GetDB2(GDB);

            GetAdsResult GetDailyResult = new GetAdsResult();
            Connection Connection = new Connection();
            List<Ads> AdsList = new List<Ads>();
            DataTable dtAds = new DataTable();
            Connection.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.NVarChar, Value = ads.EmpID });   //    Connection.myparameters.Add(new myParameters { ParameterName = "@STARTDATE", mytype = SqlDbType.DateTime, Value = Convert.ToDateTime(DailyParameters.Start_Date) });
            dtAds = Connection.GetDataTable("sp_LoanMakerAds_Mobile");

            if (dtAds.Rows.Count > 0)
            {
                foreach (DataRow row in dtAds.Rows)
                {
                    Ads Daily = new Ads
                    {
                        Image = row["img"].ToString(),
                        LoanType = row["LoanType"].ToString(),
                        LoanID = row["LoanID"].ToString()
                    };
                    AdsList.Add(Daily);
                }
                GetDailyResult.Ads = AdsList;
                GetDailyResult.myreturn = "Success";
            }
            else
            {
                GetDailyResult.myreturn = "No Data Available";
            }

            return GetDailyResult;
        }

        [HttpPost]
        [Route("GetMobileEmpAccesv2")]
        public getMobileAccess GetMobileEmpAccesv2(employeev2 emp)
        {
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = emp.CN;
            SelfieRegistration2Controller.GetDB2(GDB);

            getMobileAccess ListUs = new getMobileAccess();
            DataTable DT = new DataTable();
            Connection con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = emp.empid });
            DT = con.GetDataTable("sp_GetEmpidandAccess");
            string access = "False";
            for (int i = 0; i < DT.Rows.Count; i++)
            {
                if (DT.Rows[i]["parameter1"].ToString() == "Mobile Time In")
                {
                    access = "True";
                }
            }
            ListUs.isAccess = access;

            return ListUs;
            //Get EmpAccess  End
        }

        [HttpPost]
        [Route("GetGeoFenceAccessv2")]
        public getMobileAccess GetGeoFenceAccess(employeev2 emp)
        {
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = emp.CN;
            SelfieRegistration2Controller.GetDB2(GDB);

            getMobileAccess ListUs = new getMobileAccess();
            DataTable DT = new DataTable();
            Connection con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = emp.empid });
            DT = con.GetDataTable("sp_GetEmpidandAccess");
            string access = "False";
            for (int i = 0; i < DT.Rows.Count; i++)
            {
                if (DT.Rows[i]["parameter1"].ToString() == "GeoFencing")
                {
                    access = "True";
                }
            }
            ListUs.isAccess = access;

            return ListUs;
            //Get EmpAccess  End
        }

        [HttpPost]
        [Route("Greetingsv2")]
        public List<Greetings> greetingsv2(Loginv2 log)
        {
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = log.CN;
            SelfieRegistration2Controller.GetDB2(GDB);

            List<Greetings> GetGreetings = new List<Greetings>();
            try
            {
                Connection Connection = new Connection();
                DataTable dr = new DataTable();
                dr = Connection.GetDataTable("sp_GetGreetings_Mobile");
                foreach (DataRow row in dr.Rows)
                {
                    Greetings LoginVue = new Greetings
                    {
                        Images = "http://illimitadodevs.azurewebsites.net/images/logo_gold.png",
                        Message = row["Message"].ToString(),
                        Title = row["Title"].ToString(),
                        Sender = row["sender"].ToString(),
                        Type = row["Type"].ToString(),
                    };
                    GetGreetings.Add(LoginVue);
                }
                return GetGreetings;
            }
            catch
            {
                return GetGreetings;
            }
        }

        [HttpPost]
        [Route("DeleteFaceIDv2")]
        public string DeleteFaceIDv2(Loginv2 fID)
        {
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = fID.CN;
            SelfieRegistration2Controller.GetDB2(GDB);

            Connection con = new Connection();
            DataTable dt = new DataTable();
            con.myparameters.Add(new myParameters { ParameterName = "@FACEID", mytype = SqlDbType.NVarChar, Value = fID.FaceID });
            con.ExecuteScalar("sp_DeleteFaceID");
            return "Success";
        }

        [HttpPost]
        [Route("ChangePasswordv2")]
        public string ChangePasswordv2(ChangePassv2 ChangePass)
        {
            try
            {
                GetDatabase GDB = new GetDatabase();
                GDB.ClientName = ChangePass.CN;
                SelfieRegistration2Controller.GetDB2(GDB);

                Connection Connection = new Connection();
                Connection.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.NVarChar, Value = ChangePass.EmpID });
                Connection.myparameters.Add(new myParameters { ParameterName = "@Password", mytype = SqlDbType.NVarChar, Value = ChangePass.NewPassword });
                String ID = Connection.ExecuteScalar("sp_UpdatePassword_Mobile");
                return "Success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }

        [HttpPost]
        [Route("GetBarangayv2")]
        public Brgyv2 GetBarangayv2(GetBrgyv2 GB)
        {
            try
            {
                GetDatabase GDB = new GetDatabase();
                GDB.ClientName = GB.CN;
                SelfieRegistration2Controller.GetDB2(GDB);

                Connection Connection = new Connection();
                Connection.myparameters.Add(new myParameters { ParameterName = "@ZIPCODE", mytype = SqlDbType.NVarChar, Value = GB.ZipCode });
                DataTable DT = Connection.GetDataTable("sp_GetBrgyCode");
                Brgyv2 Brgy = new Brgyv2();
                List<BrgyListv2> BrgyList = new List<BrgyListv2>();
                foreach (DataRow row in DT.Rows)
                {
                    BrgyListv2 BL = new BrgyListv2
                    {
                        BrgyID = row["brgyCode"].ToString(),
                        BrgyName = row["brgyDesc"].ToString()
                    };
                    BrgyList.Add(BL);
                }
                Brgy.BrgyList = BrgyList;
                Brgy.myreturn = "Success";
                return Brgy;
            }
            catch (Exception e)
            {
                Brgyv2 Brgy = new Brgyv2();
                List<BrgyListv2> BrgyList = new List<BrgyListv2>();
                Brgy.BrgyList = BrgyList;
                Brgy.myreturn = e.ToString();
                return Brgy;
            }
        }

        [HttpPost]
        [Route("GetNewsAndUpdatev2")]
        public List<GetNewsAndUpdate> GetNewsAndUpdatev2(Loginv2 Log)
        {
            List<GetNewsAndUpdate> GetNewsAndUpdate = new List<GetNewsAndUpdate>();

            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = Log.CN;
            SelfieRegistration2Controller.GetDB2(GDB);

            Connection Connection = new Connection();
            DataTable dr = new DataTable();
            Connection.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = Log.LoginID });
            dr = Connection.GetDataTable("sp_GetNewsAndUpdateMobile");
            if (dr.Rows.Count > 0)
            {
                foreach (DataRow row in dr.Rows)
                {
                    GetNewsAndUpdate Logs = new GetNewsAndUpdate
                    {
                        Image = row["Image"].ToString(),
                        Title = row["Title"].ToString(),
                        Message = row["Message"].ToString(),
                        SenderName = row["SenderName"].ToString(),
                        SenderID = row["SenderID"].ToString(),
                        IsSeen = row["IsSeen"].ToString(),
                        NewAndUpdateID = row["ID"].ToString(),
                        Date = row["Date"].ToString()
                    };
                    GetNewsAndUpdate.Add(Logs);
                }
                return GetNewsAndUpdate;
            }
            else
            {
                return GetNewsAndUpdate;
            }

        }
        [HttpPost]
        [Route("GetNewsAndUpdatev3")]
        public List<GetNewsAndUpdate> GetNewsAndUpdatev3(Loginv2 Log)
        {
            List<GetNewsAndUpdate> GetNewsAndUpdate = new List<GetNewsAndUpdate>();

            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = Log.CN;
            SelfieRegistration2Controller.GetDB2(GDB);

            Connection Connection = new Connection();
            DataTable dr = new DataTable();
            Connection.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = Log.LoginID });
            dr = Connection.GetDataTable("sp_GetNewsAndUpdateMobilev2");
            if (dr.Rows.Count > 0)
            {
                foreach (DataRow row in dr.Rows)
                {
                    GetNewsAndUpdate Logs = new GetNewsAndUpdate
                    {
                        Image = row["Image"].ToString(),
                        Title = row["Title"].ToString(),
                        Message = row["Message"].ToString(),
                        SenderName = row["SenderName"].ToString(),
                        SenderID = row["SenderID"].ToString(),
                        SenderImage = row["SenderImage"].ToString(),
                        IsSeen = row["IsSeen"].ToString(),
                        NewAndUpdateID = row["ID"].ToString(),
                        Date = row["Date"].ToString(),
                        Likes = row["Likes"].ToString(),
                    };
                    GetNewsAndUpdate.Add(Logs);
                }
                return GetNewsAndUpdate;
            }
            else
            {
                return GetNewsAndUpdate;
            }

        }
        [HttpPost]
        [Route("GetNewsAndUpdatev4")]
        public List<GetNewsAndUpdate> GetNewsAndUpdatev4(Loginv2 Log)
        {
            List<GetNewsAndUpdate> GetNewsAndUpdate = new List<GetNewsAndUpdate>();

            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = Log.CN;
            SelfieRegistration2Controller.GetDB2(GDB);

            Connection Connection = new Connection();
            DataTable dr = new DataTable();
            Connection.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = Log.LoginID });
            dr = Connection.GetDataTable("sp_GetNewsAndUpdateMobilev3");
            if (dr.Rows.Count > 0)
            {
                foreach (DataRow row in dr.Rows)
                {
                    GetNewsAndUpdate Logs = new GetNewsAndUpdate
                    {
                        Image = row["Image"].ToString(),
                        Title = row["Title"].ToString(),
                        Message = row["Message"].ToString(),
                        SenderName = row["SenderName"].ToString(),
                        SenderID = row["SenderID"].ToString(),
                        SenderImage = row["SenderImage"].ToString(),
                        IsSeen = row["isSeen"].ToString(),
                        NewAndUpdateID = row["NewsID"].ToString(),
                        Date = row["Date"].ToString(),
                        Likes = row["Likes"].ToString(),
                        NotifLike = row["NotifLike"].ToString(),
                        NotifType = row["NotifType"].ToString(),
                        IsShow = row["isShow"].ToString()
                    };
                    GetNewsAndUpdate.Add(Logs);
                }
                return GetNewsAndUpdate;
            }
            else
            {
                return GetNewsAndUpdate;
            }

        }
        [HttpPost]
        [Route("GetNewsAndUpdatev5")]
        public List<GetNewsAndUpdate> GetNewsAndUpdatev5(Loginv2 Log)
        {
            List<GetNewsAndUpdate> GetNewsAndUpdate = new List<GetNewsAndUpdate>();

            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = Log.CN;
            SelfieRegistration2Controller.GetDB2(GDB);

            Connection Connection = new Connection();
            DataTable dr = new DataTable();
            Connection.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = Log.LoginID });
            dr = Connection.GetDataTable("sp_GetNewsAndUpdateMobilev3");
            if (dr.Rows.Count > 0)
            {
                foreach (DataRow row in dr.Rows)
                {
                    byte[] PIC = Convert.FromBase64String(row["Image"].ToString().Contains("data:image/gif") ? row["Image"].ToString().Replace("data:image/gif;base64,", "") : row["Image"].ToString());
                    var path = HttpContext.Current.Server.MapPath("~/ProfilePhotos/" + Log.CN.ToLower() + "/" + "");
                    var folder = Path.Combine(path, Log.LoginID);
                    Directory.CreateDirectory(folder);
                    MemoryStream stream = new MemoryStream();
                    using (stream)
                    {
                        if (row["Image"].ToString().Contains("data:image/gif"))
                        {
                            File.WriteAllBytes(folder + "/" + Log.LoginID + "_news_" + row["NewsID"].ToString() + ".gif", PIC);
                            GetNewsAndUpdate Logs = new GetNewsAndUpdate
                            {
                                Image = "/" + Log.LoginID + "_news_" + row["NewsID"].ToString() + ".gif",
                                Title = row["Title"].ToString(),
                                Message = row["Message"].ToString(),
                                SenderName = row["SenderName"].ToString(),
                                SenderID = row["SenderID"].ToString(),
                                SenderImage = "",
                                IsSeen = row["isSeen"].ToString(),
                                NewAndUpdateID = row["NewsID"].ToString(),
                                Date = row["Date"].ToString(),
                                Likes = row["Likes"].ToString(),
                                NotifLike = row["NotifLike"].ToString(),
                                NotifType = row["NotifType"].ToString(),
                                IsShow = row["isShow"].ToString()
                            };
                            GetNewsAndUpdate.Add(Logs);
                        }
                        else
                        {
                            File.WriteAllBytes(folder + "/" + Log.LoginID + "_news_" + row["NewsID"].ToString() + ".jpeg", PIC);
                            GetNewsAndUpdate Logs = new GetNewsAndUpdate
                            {
                                Image = "/" + Log.LoginID + "_news_" + row["NewsID"].ToString() + ".jpeg",
                                Title = row["Title"].ToString(),
                                Message = row["Message"].ToString(),
                                SenderName = row["SenderName"].ToString(),
                                SenderID = row["SenderID"].ToString(),
                                SenderImage = "",
                                IsSeen = row["isSeen"].ToString(),
                                NewAndUpdateID = row["NewsID"].ToString(),
                                Date = row["Date"].ToString(),
                                Likes = row["Likes"].ToString(),
                                NotifLike = row["NotifLike"].ToString(),
                                NotifType = row["NotifType"].ToString(),
                                IsShow = row["isShow"].ToString()
                            };
                            GetNewsAndUpdate.Add(Logs);
                        }


                    }


                }
                return GetNewsAndUpdate;
            }
            else
            {
                return GetNewsAndUpdate;
            }

        }

        [HttpPost]
        [Route("UpdateLikeNewsAndUpdate")]
        public string UpdateLikeNewsAndUpdate(NewsAndUpdateModel model)
        {
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = model.CN;
            SelfieRegistration2Controller.GetDB2(GDB);

            try
            {
                Connection con = new Connection();
                DataTable dt = new DataTable();
                con.myparameters.Add(new myParameters { ParameterName = "@newsId", mytype = SqlDbType.NVarChar, Value = model.NewsID });
                con.myparameters.Add(new myParameters { ParameterName = "@likeValue", mytype = SqlDbType.NVarChar, Value = model.NewLikeCount });
                con.ExecuteScalar("sp_UpdateLikeNewsAndUpdateMobilev2");
                return "Like Added";
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }

        [Route("UpdateLikeNewsAndUpdatev2")]
        public string UpdateLikeNewsAndUpdatev2(NewsAndUpdateModel model)
        {
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = model.CN;
            SelfieRegistration2Controller.GetDB2(GDB);

            try
            {
                Connection con = new Connection();
                DataTable dt = new DataTable();
                con.myparameters.Add(new myParameters { ParameterName = "@newsId", mytype = SqlDbType.NVarChar, Value = model.NewsID });
                con.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.NVarChar, Value = model.EmpID });
                con.ExecuteScalar("sp_UpdateLikeNewsAndUpdateMobilev3");
                return "Like Added";
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }


        [HttpPost]
        [Route("UpdateNewsAndUpdatev2")]
        public string UpdateNewsAndUpdatev2(GetNewsAndUpdatev2 GNU)
        {
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = GNU.CN;
            SelfieRegistration2Controller.GetDB2(GDB);

            Connection con = new Connection();
            DataTable dt = new DataTable();
            con.myparameters.Add(new myParameters { ParameterName = "@NEWANDUPDATEID", mytype = SqlDbType.NVarChar, Value = GNU.NewAndUpdateID });
            con.ExecuteScalar("sp_UpdateNewsAndUpdate");
            return "Success";
        }


        [HttpPost]
        [Route("LoanMakerAdsResultv2")]
        public string AdsLoanMakerResultv2(Adsparamv2 ads)
        {

            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = ads.CN;
            SelfieRegistration2Controller.GetDB2(GDB);

            Connection con = new Connection();
            DataTable dt = new DataTable();
            con.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.NVarChar, Value = ads.EmpID });
            con.myparameters.Add(new myParameters { ParameterName = "@LoanID", mytype = SqlDbType.NVarChar, Value = ads.LoanID });
            con.ExecuteScalar("sp_LoanMakerAdsResult_Mobile");
            return "Success";
        }

        [HttpPost]
        [Route("ForgotPasswordEmailv2")]

        public string EmailForgotv2(Emails Emails)
        {
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = Emails.CN;
            SelfieRegistration2Controller.GetDB2(GDB);

            Connection Connection = new Connection();
            Connection.myparameters.Add(new myParameters { ParameterName = "@EMAIL", mytype = SqlDbType.NVarChar, Value = Emails.Email });
            DataRow dr;
            dr = Connection.GetSingleRow("sp_CheckEmail_ForgotPass_Mobile");

            return dr["EmpID"].ToString();
        }

        [HttpPost]
        [Route("ForgotPasswordv2")]

        public string PasswordForgotv2(Emails Emails)
        {
            //GetDailyResult GetDailyResult = new GetDailyResult();
            //try
            //{
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = Emails.CN;
            SelfieRegistration2Controller.GetDB2(GDB);

            Connection Connection = new Connection();
            //List<Daily> DailyList = new List<Daily>();
            //DataTable dtDAILY = new DataTable();
            //BasicFunction BasicFunction = new BasicFunction();
            Connection.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.NVarChar, Value = Emails.EmpID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Code", mytype = SqlDbType.NVarChar, Value = Emails.Code });
            GenCode = Emails.Code;
            String ID = Connection.ExecuteScalar("sp_UpdateCode_Mobile");

            return "Success";


        }

        [HttpPost]
        [Route("ForgotPasswordUpdatev2")]
        public string ForgotPassv2(Emails Emails)
        {
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = Emails.CN;
            SelfieRegistration2Controller.GetDB2(GDB);

            Connection con = new Connection();

            con.myparameters.Add(new myParameters { ParameterName = "@NewPassword", mytype = SqlDbType.NVarChar, Value = Emails.Password });
            con.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.NVarChar, Value = Emails.EmpID });
            con.myparameters.Add(new myParameters { ParameterName = "@CODE", mytype = SqlDbType.NVarChar, Value = Emails.Code });

            string Status = con.ExecuteScalar("sp_Forget_Password_Mobile");

            if (Status == "true")
            {
                return "Success";
            }
            else
            {
                return "Invalid Code";
            }
        }
        //Forgot Password Start Revised
        //private static Random random = new Random();


        [HttpPost]
        [Route("ForgotPasswordDHR")]
        public string ForgotPasswordHR(ForgotPassword FP)
        {
            //GetDatabase GDB = new GetDatabase();
            //GDB.ClientName = FP.CN;
            //SelfieRegistration2Controller.GetDB2(GDB);


            string RetRandomCode = RandomString(6);
            try
            {
                Connection Con = new Connection();
                Con.myparameters.Add(new myParameters { ParameterName = "@EMAIL", mytype = SqlDbType.NVarChar, Value = FP.EMAIL });
                Con.myparameters.Add(new myParameters { ParameterName = "@GENERATECODE", mytype = SqlDbType.NVarChar, Value = RetRandomCode });
                FP.VALID = Con.ExecuteScalar("sp_ForgotPasswordNotif");
                FP.GENERATECODE = RetRandomCode;
                if (FP.VALID == "INVALID EMAIL")
                {
                    return FP.VALID;
                }
                else
                {
                    try
                    {

                        var message = new MimeMessage();
                        message.From.Add(new MailboxAddress("password.recovery@illimitado.com"));
                        message.To.Add(new MailboxAddress("" + FP.EMAIL + ""));
                        message.Subject = "Reset Password";
                        message.Body = new TextPart("plain")
                        {
                            Text = @"Please use this generated code as your temporary password " + RetRandomCode + ""

                        };

                        using (var client = new MailKit.Net.Smtp.SmtpClient())
                        {
                            client.ServerCertificateValidationCallback = (s, c, h, e) => true;
                            client.Connect("mail.illimitado.com", 465, true);
                            client.Authenticate("password.recovery@illimitado.com", "p@ssw0rd1LM");
                            client.Send(message);
                            client.Disconnect(true);
                        }
                    }
                    catch (Exception e)
                    {
                        return "" + e;
                    }
                }

                return FP.VALID;
            }
            catch (Exception e)
            {
                return "failed" + e;
            }

        }


        [HttpPost]
        [Route("ForgotPasswordDHRv2")]
        public string ForgotPasswordHRv2(ForgotPassword FP)
        {
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = FP.CN;
            SelfieRegistration2Controller.GetDB2(GDB);


            string RetRandomCode = RandomString(6);
            try
            {
                Connection Con = new Connection();
                Con.myparameters.Add(new myParameters { ParameterName = "@EMAIL", mytype = SqlDbType.NVarChar, Value = FP.EMAIL });
                Con.myparameters.Add(new myParameters { ParameterName = "@GENERATECODE", mytype = SqlDbType.NVarChar, Value = RetRandomCode });
                FP.VALID = Con.ExecuteScalar("sp_ForgotPasswordNotif");
                FP.GENERATECODE = RetRandomCode;
                if (FP.VALID == "INVALID EMAIL")
                {
                    return FP.VALID;
                }
                else
                {
                    try
                    {

                        var message = new MimeMessage();
                        message.From.Add(new MailboxAddress("password.recovery@illimitado.com"));
                        message.To.Add(new MailboxAddress("" + FP.EMAIL + ""));
                        message.Subject = "Reset Password";
                        message.Body = new TextPart("plain")
                        {
                            Text = @"Please use this generated code as your temporary password " + RetRandomCode + ""

                        };

                        using (var client = new MailKit.Net.Smtp.SmtpClient())
                        {
                            client.ServerCertificateValidationCallback = (s, c, h, e) => true;
                            client.Connect("mail.illimitado.com", 465, true);
                            client.Authenticate("password.recovery@illimitado.com", "p@ssw0rd1LM");
                            client.Send(message);
                            client.Disconnect(true);
                        }
                    }
                    catch (Exception e)
                    {
                        return "" + e;
                    }
                }

                return FP.VALID;
            }
            catch (Exception e)
            {
                return "failed" + e;
            }

        }


        [HttpPost]
        [Route("UpdatePasswordDHR")]
        public string UpdateUserPassword(ForgotPassword FP)
        {
            try
            {
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@UserEmail", mytype = SqlDbType.NVarChar, Value = FP.EMAIL });
                con.myparameters.Add(new myParameters { ParameterName = "@Password", mytype = SqlDbType.NVarChar, Value = FP.PASSWORD });
                con.myparameters.Add(new myParameters { ParameterName = "@GENERATEDCODE", mytype = SqlDbType.NVarChar, Value = FP.GENERATECODE });
                FP.VALID = (con.ExecuteScalar("sp_UpdateUserAccount"));

                return FP.VALID;
            }
            catch (Exception e)
            {
                return "failed" + e;
            }
        }

        [HttpPost]
        [Route("UpdatePasswordDHRv2")]
        public string UpdateUserPasswordv2(ForgotPassword FP)
        {
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = FP.CN;
            SelfieRegistration2Controller.GetDB2(GDB);
            try
            {
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@UserEmail", mytype = SqlDbType.NVarChar, Value = FP.EMAIL });
                con.myparameters.Add(new myParameters { ParameterName = "@Password", mytype = SqlDbType.NVarChar, Value = FP.PASSWORD });
                con.myparameters.Add(new myParameters { ParameterName = "@GENERATEDCODE", mytype = SqlDbType.NVarChar, Value = FP.GENERATECODE });
                FP.VALID = (con.ExecuteScalar("sp_UpdateUserAccount"));

                return FP.VALID;
            }
            catch (Exception e)
            {
                return "failed" + e;
            }
        }
        [HttpPost]
        [Route("isEqualGeneratedCode")]
        public string DisplayGeneratedCode(GeneratedParameter genpara)
        {
            try
            {
                GetDatabase GDB = new GetDatabase();
                GDB.ClientName = genpara.CN;
                SelfieRegistration2Controller.GetDB2(GDB);

                GeneratedCodev1 GeneratedList = new GeneratedCodev1();

                List<GeneratedCodeData> ListReturned = new List<GeneratedCodeData>();
                System.Data.DataTable DTCode = new System.Data.DataTable();

                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@EMAIL", mytype = SqlDbType.NVarChar, Value = genpara.EMAIL });
                return con.ExecuteScalar("sp_ValidateGeneratedCode");
            }
            catch (Exception e)
            {
                return e.ToString();
            }


        }

        //Forgot Password End   Revised



        [HttpPost]
        [Route("DHRSendAttachment")]
        public string SendPDFAttachment(DHRSendAttachParam emp)
        {
            try
            {

                var message = new MimeMessage();
                message.From.Add(new MailboxAddress("Acuiro", "acuiro@illimitado.com"));
                message.To.Add(new MailboxAddress("JE", "eya@illimitado.com"));
                //message.To.Add(new MailboxAddress("", "polo@illimitado.com"));
                message.Subject = "Salary Loan Application";

                var builder = new BodyBuilder();

                // Set the plain-text version of the message text
                builder.TextBody = @"";
                // We may also want to attach a calendar event for Monica's party...
                // builder.Attachments.Add(@"C:\Users\DEV01\Downloads\49163003868-812252258-ticket.pdf");
                string path = HttpContext.Current.Server.MapPath("pdf");
                builder.Attachments.Add(@"" + path + "/GovID_SpecimenSignature.pdf");
                builder.Attachments.Add(@"" + path + "/Disclosure Statement_ilmdemo_i143.pdf");
                builder.Attachments.Add(@"" + path + "/Promissory Note_ilmdemo_i143.pdf");
                // Now we just need to set the message body and we're done
                message.Body = builder.ToMessageBody();

                using (var client = new MailKit.Net.Smtp.SmtpClient())
                {
                    client.ServerCertificateValidationCallback = (s, c, h, e) => true;
                    client.Connect("mail.illimitado.com", 465, true);
                    client.Authenticate("acuiro@illimitado.com", "Secured@1230");
                    client.Send(message);
                    client.Disconnect(true);
                }



                var message2 = new MimeMessage();
                message2.From.Add(new MailboxAddress("Acuiro", "acuiro@illimitado.com"));
                message2.To.Add(new MailboxAddress("PS", "polo@illimitado.com"));
                //message2.To.Add(new MailboxAddress("", "polo@illimitado.com"));
                message2.Subject = "Salary Loan Application";

                var builder2 = new BodyBuilder();

                // Set the plain-text version of the message text
                builder2.TextBody = @"";
                // We may also want to attach a calendar event for Monica's party...
                // builder.Attachments.Add(@"C:\Users\DEV01\Downloads\49163003868-812252258-ticket.pdf");
                string path2 = HttpContext.Current.Server.MapPath("pdf");
                builder2.Attachments.Add(@"" + path2 + "/GovID_SpecimenSignature.pdf");
                builder2.Attachments.Add(@"" + path2 + "/Disclosure Statement_ilmdemo_i143.pdf");
                builder2.Attachments.Add(@"" + path2 + "/Promissory Note_ilmdemo_i143.pdf");

                // Now we just need to set the message body and we're done
                message2.Body = builder2.ToMessageBody();

                using (var client = new MailKit.Net.Smtp.SmtpClient())
                {
                    client.ServerCertificateValidationCallback = (s, c, h, e) => true;
                    client.Connect("mail.illimitado.com", 465, true);
                    client.Authenticate("acuiro@illimitado.com", "Secured@1230");
                    client.Send(message2);
                    client.Disconnect(true);
                }



                var message3 = new MimeMessage();
                message3.From.Add(new MailboxAddress("Acuiro", "acuiro@illimitado.com"));
                message3.To.Add(new MailboxAddress("AM", "akramicio.magpusao23@gmail.com"));
                //message2.To.Add(new MailboxAddress("", "polo@illimitado.com"));
                message3.Subject = "Salary Loan Application";

                var builder3 = new BodyBuilder();

                // Set the plain-text version of the message text
                builder3.TextBody = @"";
                // We may also want to attach a calendar event for Monica's party...
                // builder.Attachments.Add(@"C:\Users\DEV01\Downloads\49163003868-812252258-ticket.pdf");
                string path3 = HttpContext.Current.Server.MapPath("pdf");
                builder3.Attachments.Add(@"" + path3 + "/GovID_SpecimenSignature.pdf");
                builder3.Attachments.Add(@"" + path3 + "/Disclosure Statement_ilmdemo_i143.pdf");
                builder3.Attachments.Add(@"" + path3 + "/Promissory Note_ilmdemo_i143.pdf");

                // Now we just need to set the message body and we're done
                message3.Body = builder3.ToMessageBody();

                using (var client = new MailKit.Net.Smtp.SmtpClient())
                {
                    client.ServerCertificateValidationCallback = (s, c, h, e) => true;
                    client.Connect("mail.illimitado.com", 465, true);
                    client.Authenticate("acuiro@illimitado.com", "Secured@1230");
                    client.Send(message3);
                    client.Disconnect(true);
                }

                GetDatabase GDB = new GetDatabase();
                GDB.ClientName = emp.CN;
                SelfieRegistration2Controller.GetDB2(GDB);

                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = emp.EmpID });
                con.ExecuteNonQuery("sp_ClearAdHistory");

                return "Success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }

        //[HttpPost]
        //[Route("DHRSendAttachment")]
        //public string SendPDFAttachment(DHRSendAttachParam emp)
        //{
        //    try
        //    {

        //        GetDatabase GDB = new GetDatabase();
        //        GDB.ClientName = emp.CN;
        //        SelfieRegistration2Controller.GetDB2(GDB);



        //        #region generatepdf
        //        String PIC1 = "", SIGN1 = "", SIGN2 = "", SIGN3 = "", IDTYPE = "", ID = "";
        //        Connection con = new Connection();
        //        con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = emp.EmpID });
        //        System.Data.DataTable DT = new System.Data.DataTable();
        //        DT = con.GetDataTable("sp_GetNewHiredDocument");
        //        if (DT.Rows.Count > 0)
        //        {

        //            PIC1 = DT.Rows[0][2].ToString();
        //            SIGN1 = DT.Rows[0][4].ToString();
        //            SIGN2 = DT.Rows[0][5].ToString();
        //            SIGN3 = DT.Rows[0][6].ToString();
        //            ID = DT.Rows[0][7].ToString();
        //            IDTYPE = DT.Rows[0][9].ToString();

        //            //creatinf dynamic pdf 

        //            // Template file path
        //            string formFile = System.Web.HttpContext.Current.Server.MapPath(@"pdf/NewHireDoc.pdf");
        //            // Output file path
        //            string newFile = System.Web.HttpContext.Current.Server.MapPath(@"pdf/NewHireDoc_" + emp.CN + "_" + emp.EmpID + ".pdf");
        //            // read the template file
        //            PdfReader reader = new PdfReader(formFile);
        //            // The Output file will be created from template file and edited by the PDFStamper
        //            PdfStamper stamper = new PdfStamper(reader, new FileStream(newFile, FileMode.Create));


        //            var pdfContentByte = stamper.GetOverContent(1);
        //            Byte[] pic1 = null;
        //            iTextSharp.text.Image picturegenerated = null;
        //            if (string.IsNullOrWhiteSpace(PIC1) == false)
        //            {
        //                PIC1 = PIC1.Replace(' ', '+');
        //                pic1 = Convert.FromBase64String(PIC1);
        //                picturegenerated = iTextSharp.text.Image.GetInstance(pic1);
        //                picturegenerated.SetAbsolutePosition(170, 350);
        //                picturegenerated.ScaleAbsolute(250, 300);
        //                pdfContentByte.AddImage(picturegenerated);
        //            }


        //            ////generated signature postion 1 ,2 and 3
        //            var pdf2ndpage = stamper.GetOverContent(2);

        //            Byte[] signa1 = null;
        //            iTextSharp.text.Image sign1 = null;
        //            Byte[] signa2 = null;
        //            iTextSharp.text.Image sign2 = null;
        //            Byte[] signa3 = null;
        //            iTextSharp.text.Image sign3 = null;
        //            if (string.IsNullOrWhiteSpace(SIGN1) == false)
        //            {
        //                SIGN1 = SIGN1.Replace(' ', '+');
        //                signa1 = Convert.FromBase64String(SIGN1);
        //                sign1 = iTextSharp.text.Image.GetInstance(signa1);
        //                sign1.SetAbsolutePosition(420, 580);
        //                sign1.ScaleToFit(100, 50);

        //                SIGN2 = SIGN2.Replace(' ', '+');
        //                signa2 = Convert.FromBase64String(SIGN2);
        //                sign2 = iTextSharp.text.Image.GetInstance(signa2);
        //                sign2.SetAbsolutePosition(420, 450);
        //                sign2.ScaleToFit(100, 50);


        //                SIGN3 = SIGN3.Replace(' ', '+');
        //                signa3 = Convert.FromBase64String(SIGN3);
        //                sign3 = iTextSharp.text.Image.GetInstance(signa3);
        //                sign3.SetAbsolutePosition(420, 320);
        //                sign3.ScaleToFit(100, 50);


        //                Byte[] valid2 = null;
        //                iTextSharp.text.Image alidpic2 = null;

        //                if (string.IsNullOrWhiteSpace(ID) == false)
        //                {
        //                    ID = ID.Replace(' ', '+');
        //                    int mod4 = ID.Length % 4;
        //                    if (mod4 > 0)
        //                    {
        //                        ID += new string('=', 4 - mod4);
        //                    }
        //                    valid2 = Convert.FromBase64String(ID);
        //                    alidpic2 = iTextSharp.text.Image.GetInstance(valid2);
        //                    alidpic2.SetAbsolutePosition(30, 430);
        //                    alidpic2.ScaleToFit(250, 200);
        //                    pdf2ndpage.AddImage(alidpic2);
        //                }

        //                pdf2ndpage.AddImage(sign1);
        //                pdf2ndpage.AddImage(sign2);
        //                pdf2ndpage.AddImage(sign3);


        //                AcroFields fields = stamper.AcroFields;
        //                fields.SetFieldProperty("untitled1", "textsize", 18f, null);
        //                fields.SetField("untitled1", IDTYPE);

        //                ////to read only
        //                stamper.FormFlattening = true;
        //                // closing the stamper
        //                stamper.Close();


        //            }

        //        }
        //        #endregion

        //        var message = new MimeMessage();
        //        message.From.Add(new MailboxAddress("SendAttach", "password.recovery@illimitado.com"));
        //        message.To.Add(new MailboxAddress("JM", "akramicio.magpusao23@gmail.com"));
        //        //message.To.Add(new MailboxAddress("", "polo@illimitado.com"));
        //        message.Subject = "This is a text Subject from akram";

        //        var builder = new BodyBuilder();

        //        // Set the plain-text version of the message text
        //        builder.TextBody = @"This is a test email from akram";
        //        // We may also want to attach a calendar event for Monica's party...
        //        // builder.Attachments.Add(@"C:\Users\DEV01\Downloads\49163003868-812252258-ticket.pdf");
        //        string path = HttpContext.Current.Server.MapPath("pdf");
        //        builder.Attachments.Add(@"" + path + "/NewHireDoc_" + emp.CN + "_" + emp.EmpID + ".pdf");
        //        if (emp.CN.ToLower() == "ilmdemo" && emp.EmpID.ToLower() == "i143")
        //        {
        //            builder.Attachments.Add(@"" + path + "/Disclosure Statement_" + emp.CN + "_" + emp.EmpID + ".pdf");
        //            builder.Attachments.Add(@"" + path + "/Promissory Note_" + emp.CN + "_" + emp.EmpID + ".pdf");
        //        }
        //        else
        //        {

        //        }

        //        // Now we just need to set the message body and we're done
        //        message.Body = builder.ToMessageBody();

        //        using (var client = new MailKit.Net.Smtp.SmtpClient())
        //        {
        //            client.ServerCertificateValidationCallback = (s, c, h, e) => true;
        //            client.Connect("mail.illimitado.com", 465, true);
        //            client.Authenticate("password.recovery@illimitado.com", "p@ssw0rd1LM");
        //            client.Send(message);
        //            client.Disconnect(true);
        //        }

        //        return "Success";
        //    }
        //    catch (Exception e)
        //    {
        //        return e.ToString();
        //    }
        //}


        #region 2018-10-15
        [HttpPost]
        [Route("GetDHRGreetingsv1")]

        public GetGreetingResult GetDHRGreetingsv1(Loginv2 DH)
        {
            GetGreetingResult GetGreetResult = new GetGreetingResult();
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = DH.CN;
            SelfieRegistration2Controller.GetDB2(GDB);

            try
            {

                Connection Connection = new Connection();
                List<Greetings> ListGreetings = new List<Greetings>();
                DataSet ds = new DataSet();
                System.Data.DataTable dt = new System.Data.DataTable();
                Connection.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = DH.LoginID });
                ds = Connection.GetDataset("sp_GetDHRGreetingsv1");
                dt = ds.Tables[0];

                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        Greetings DP = new Greetings
                        {
                            Images = row["Image"].ToString(),
                            Message = row["Message"].ToString(),
                            Title = row["Title"].ToString(),
                            Sender = row["Sender"].ToString(),
                            Type = row["Type"].ToString()
                        };
                        ListGreetings.Add(DP);
                    }
                    GetGreetResult.Greeting = ListGreetings;
                    GetGreetResult.myreturn = "Success";
                }
                else
                {
                    GetGreetResult.myreturn = "No Data Available";
                }
            }
            catch (Exception ex)
            {
                GetGreetResult.myreturn = ex.Message;
            }
            return GetGreetResult;
        }

        #endregion


        //---------------------------------------------------------------Revised Change DB End------------------------------------------------------------------------
        //--------------------------------------------------------------------with CN---------------------------------------------------------------------------------
        //Brandon Start
        [HttpPost]
        [Route("testvalidateloginpractice")]
        public BrandonUserProfiletesting testvalidateloginpractice(Loginv2 userToSave)
        {
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = userToSave.CN;
            SelfieRegistration2Controller.GetDB2(GDB);
            Connection Connection = new Connection();
            DataTable DT = new DataTable();
            Connection.myparameters.Add(new myParameters { ParameterName = "@empId", mytype = SqlDbType.NVarChar, Value = userToSave.LoginID });
            DT = Connection.GetDataTable("spLogin_Mobile_test");
            BrandonUserProfiletesting User = new BrandonUserProfiletesting();
            if (DT.Rows.Count > 0)
            {
                User = new BrandonUserProfiletesting();
                User.FIRSTNAMEMO = DT.Rows[0]["First_Name"].ToString();
                User.LASTNAMEMO = DT.Rows[0]["Last_Name"].ToString();
                User.MIDDLENAMEMO = DT.Rows[0]["Middle_Name"].ToString();
            }
            return User;
        }
        //Brandon End
        [HttpPost]
        [Route("InsertTokenID")]
        public string InsertTokenID(FaceToken us)
        {
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = us.CN;
            SelfieRegistration2Controller.GetDB2(GDB);
            Connection con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.NVarChar, Value = us.LoginID });
            con.myparameters.Add(new myParameters { ParameterName = "@TokenID", mytype = SqlDbType.NVarChar, Value = us.TokenID });
            con.ExecuteNonQuery("sp_InsertTokenID");
            return "success";
        }

        [HttpPost]
        [Route("GetFaceID")]
        public UserProfile GetFaceID(Loginv2 userToSave)
        {
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = userToSave.CN;
            SelfieRegistration2Controller.GetDB2(GDB);

            Connection Connection = new Connection();
            DataTable DT = new DataTable();
            Connection.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.NVarChar, Value = userToSave.LoginID });
            DT = Connection.GetDataTable("sp_GetFaceId");
            UserProfile User = new UserProfile();
            if (DT.Rows.Count > 0)
            {
                User = new UserProfile();
                User.FaceID = DT.Rows[0]["FaceID"].ToString();

            }
            return User;
        }

        //[HttpPost]
        //[Route("GetFaceID")]
        //public string GetFaceID(Loginv2 userToSave)
        //{
        //    try
        //    {
        //        GetDatabase GDB = new GetDatabase();
        //        GDB.ClientName = userToSave.CN;
        //        SelfieRegistration2Controller.GetDB2(GDB);

        //        Connection con = new Connection();
        //        con.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.NVarChar, Value = userToSave.LoginID });
        //        return con.ExecuteScalar("sp_GetFaceId");
        //    }
        //    catch (Exception e)
        //    {
        //        return e.ToString();
        //    }
        //}



        [HttpPost]
        [Route("InsertBioLuxand")]
        public string InsertBioLuxand(FaceToken us)
        {
            try
            {
                GetDatabase GDB = new GetDatabase();
                GDB.ClientName = us.CN;
                SelfieRegistration2Controller.GetDB2(GDB);
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.NVarChar, Value = us.LoginID });
                con.myparameters.Add(new myParameters { ParameterName = "@TIME", mytype = SqlDbType.DateTime, Value = us.DTInserted });

                con.ExecuteNonQuery("sp_InsertBioLuxand");
                return "success";
            }
            catch (Exception)
            {

                return "error";
            }
        }
        [HttpPost]
        [Route("TKLux")]
        public string TKLux(FaceToken us)
        {
            try
            {
                GetDatabase GDB = new GetDatabase();
                GDB.ClientName = us.CN;
                SelfieRegistration2Controller.GetDB2(GDB);
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.NVarChar, Value = us.LoginID });
                con.myparameters.Add(new myParameters { ParameterName = "@TIME", mytype = SqlDbType.DateTime, Value = us.DTInserted });

                con.ExecuteNonQuery("sp_InsertBioLuxand");
                return "success";
            }
            catch (Exception)
            {

                return "error";
            }
        }
        [HttpPost]
        [Route("RegisterLux")]
        public string RegisterLux(LuxParam us)
        {
            try
            {
                GetDatabase GDB = new GetDatabase();
                GDB.ClientName = us.CN;
                SelfieRegistration2Controller.GetDB2(GDB);
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@LoginID", mytype = SqlDbType.NVarChar, Value = us.LoginID });
                return con.ExecuteScalar("sp_RegisterLux");
            }
            catch (Exception)
            {
                return "Error";
            }


        }

        [HttpPost]
        [Route("DeleteLux")]
        public string DeleteLux(LuxParam us)
        {
            try
            {
                GetDatabase GDB = new GetDatabase();
                GDB.ClientName = us.CN;
                SelfieRegistration2Controller.GetDB2(GDB);
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@LoginID", mytype = SqlDbType.NVarChar, Value = us.LoginID });
                return con.ExecuteScalar("sp_DeleteLux");
            }
            catch (Exception)
            {
                return "Error";
            }
        }
        [HttpPost]
        [Route("QRCode")]
        public qrret qrcode(qrparams qr)
        {

            //qrret rc = new qrret();

            //GetDatabase GDB = new GetDatabase();
            //GDB.ClientName = qr.CN;
            //SelfieRegistration2Controller.GetDB2(GDB);
            //Connection con = new Connection();
            //con.myparameters.Add(new myParameters { ParameterName = "EmpID", mytype = SqlDbType.NVarChar, Value = qr.EmpID });
            //con.myparameters.Add(new myParameters { ParameterName = "Time", mytype = SqlDbType.NVarChar, Value = qr.Time });
            //DataTable dt = con.GetDataTable("sp_QRCode_Reader");

            //foreach (DataRow item in dt.Rows)
            //{
            //    rc.URL = item["URL"].ToString();
            //    rc.fullname = item["Fullname"].ToString();
            //    rc.activity = item["Activity"].ToString();
            //}

            //return rc;
            qrret rc = new qrret();


            if (qr.CN.ToLower() == "bataan2020" || qr.CN.ToLower() == "bataan2020-samal" || qr.CN.ToLower() == "ayhpi")
            {
                tempconnectionstring();

                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "EmpID", mytype = SqlDbType.NVarChar, Value = qr.EmpID });


                if (con.ExecuteScalar("sp_QRBundy_CheckDB").ToString() != "User not found")
                {


                    GetDatabase GDB = new GetDatabase();
                    GDB.ClientName = con.ExecuteScalar("sp_QRBundy_CheckDB").ToString();
                    SelfieRegistration2Controller.GetDB2(GDB);
                    con = new Connection();
                    con.myparameters.Add(new myParameters { ParameterName = "EmpID", mytype = SqlDbType.NVarChar, Value = qr.EmpID });
                    con.myparameters.Add(new myParameters { ParameterName = "Time", mytype = SqlDbType.NVarChar, Value = qr.Time });
                    DataTable dt = con.GetDataTable("sp_QRCode_Reader");

                    foreach (DataRow item in dt.Rows)
                    {
                        rc.URL = item["URL"].ToString();
                        rc.fullname = item["Fullname"].ToString();
                        rc.activity = item["Activity"].ToString();
                    }

                }
                else
                {

                }
            }
            else
            {
                GetDatabase GDB = new GetDatabase();
                GDB.ClientName = qr.CN;
                SelfieRegistration2Controller.GetDB2(GDB);
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "EmpID", mytype = SqlDbType.NVarChar, Value = qr.EmpID });
                con.myparameters.Add(new myParameters { ParameterName = "Time", mytype = SqlDbType.NVarChar, Value = qr.Time });
                DataTable dt = con.GetDataTable("sp_QRCode_Reader");

                foreach (DataRow item in dt.Rows)
                {
                    rc.URL = item["URL"].ToString();
                    rc.fullname = item["Fullname"].ToString();
                    rc.activity = item["Activity"].ToString();
                }

            }

            return rc;
        }

        [HttpPost]
        [Route("QRCodev2")]
        public string qrcodev2(qrparamsv2 qr)
        {
            try
            {
                if (qr.CN.ToLower() == "bataan2020" || qr.CN.ToLower() == "bataan2020-samal" || qr.CN.ToLower() == "ayhpi")
                {
                    tempconnectionstring();

                    DataTable EmpList = new DataTable();
                    Connection con = new Connection();
                    EmpList.Columns.AddRange(new DataColumn[2] { new DataColumn("EmpID"), new DataColumn("Time") });
                    for (int i = 0; i < qr.listEmpid.Length; i++)
                    {
                        EmpList.Rows.Add(qr.listEmpid[i].Split('-')[0], qr.listEmpid[i].Split('-')[1]);
                    }
                    con.myparameters.Add(new myParameters { ParameterName = "EmpID", mytype = SqlDbType.Structured, Value = EmpList });
                    //DataTable dt = con.GetDataTable("sp_QRCode_Readerv2");
                    //return "True";
                    return "True" + ", WS Rows: " + qr.listEmpid.Length.ToString() + ",SP rows: " + con.ExecuteScalar("sp_QRBundy_CheckDBv2").ToString();
                }
                else
                {
                    GetDatabase GDB = new GetDatabase();
                    GDB.ClientName = qr.CN;
                    SelfieRegistration2Controller.GetDB2(GDB);
                    Connection con = new Connection();

                    DataTable EmpID = new DataTable();
                    EmpID.Columns.AddRange(new DataColumn[2] { new DataColumn("EmpID"), new DataColumn("Time") });
                    for (int i = 0; i < qr.listEmpid.Length; i++)
                    {
                        EmpID.Rows.Add(qr.listEmpid[i].Split('-')[0], qr.listEmpid[i].Split('-')[1]);
                    }
                    con.myparameters.Add(new myParameters { ParameterName = "EmpID", mytype = SqlDbType.Structured, Value = EmpID });
                    //DataTable dt = con.GetDataTable("sp_QRCode_Readerv2");
                    //return "True";
                    return "True" + ", WS Rows: " + qr.listEmpid.Length.ToString() + ",SP rows: " + con.ExecuteScalar("sp_QRCode_Readerv2").ToString();
                }

            }
            catch (Exception)
            {
                return "False";
            }
            //try
            //{

            //    GetDatabase GDB = new GetDatabase();
            //    GDB.ClientName = qr.CN;
            //    SelfieRegistration2Controller.GetDB2(GDB);
            //    Connection con = new Connection();

            //    DataTable EmpID = new DataTable();
            //    EmpID.Columns.AddRange(new DataColumn[2] { new DataColumn("EmpID"), new DataColumn("Time") });
            //    for (int i = 0; i < qr.listEmpid.Length; i++)
            //    {
            //        EmpID.Rows.Add(qr.listEmpid[i].Split('-')[0], qr.listEmpid[i].Split('-')[1]);
            //    }
            //    con.myparameters.Add(new myParameters { ParameterName = "EmpID", mytype = SqlDbType.Structured, Value = EmpID });
            //    //DataTable dt = con.GetDataTable("sp_QRCode_Readerv2");
            //    //return "True";
            //    return "True" + ", WS Rows: " + qr.listEmpid.Length.ToString() + ",SP rows: " + con.ExecuteScalar("sp_QRCode_Readerv2").ToString();

            //}
            //catch (Exception)
            //{
            //    return "False";
            //}
        }
        [HttpPost]
        [Route("QRCodev3")]
        public qrret qrcodev3(qrparams qr)
        {
            qrret rc = new qrret();


            if (qr.CN.ToLower() == "bataan2020" || qr.CN.ToLower() == "bataan2020-samal" || qr.CN.ToLower() == "ayhpi")
            {
                tempconnectionstring();

                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "EmpID", mytype = SqlDbType.NVarChar, Value = qr.EmpID });


                if (con.ExecuteScalar("sp_QRBundy_CheckDB").ToString() != "User not found")
                {


                    GetDatabase GDB = new GetDatabase();
                    GDB.ClientName = con.ExecuteScalar("sp_QRBundy_CheckDB").ToString();
                    SelfieRegistration2Controller.GetDB2(GDB);
                    con = new Connection();
                    con.myparameters.Add(new myParameters { ParameterName = "EmpID", mytype = SqlDbType.NVarChar, Value = qr.EmpID });
                    con.myparameters.Add(new myParameters { ParameterName = "Time", mytype = SqlDbType.NVarChar, Value = qr.Time });
                    DataTable dt = con.GetDataTable("sp_QRCode_Reader");

                    foreach (DataRow item in dt.Rows)
                    {
                        rc.URL = item["URL"].ToString();
                        rc.fullname = item["Fullname"].ToString();
                        rc.activity = item["Activity"].ToString();
                    }

                }
                else
                {

                }
            }
            else
            {
                GetDatabase GDB = new GetDatabase();
                GDB.ClientName = qr.CN;
                SelfieRegistration2Controller.GetDB2(GDB);
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "EmpID", mytype = SqlDbType.NVarChar, Value = qr.EmpID });
                con.myparameters.Add(new myParameters { ParameterName = "Time", mytype = SqlDbType.NVarChar, Value = qr.Time });
                DataTable dt = con.GetDataTable("sp_QRCode_Reader");

                foreach (DataRow item in dt.Rows)
                {
                    rc.URL = item["URL"].ToString();
                    rc.fullname = item["Fullname"].ToString();
                    rc.activity = item["Activity"].ToString();
                }

            }

            return rc;
        }
        [HttpPost]
        [Route("QRBadgeValidate")]
        public string ValidateBadgeInterval(qrparams qr)
        {
            //try
            //{
            //    GetDatabase GDB = new GetDatabase();
            //    GDB.ClientName = qr.CN;
            //    SelfieRegistration2Controller.GetDB2(GDB);
            //    Connection con = new Connection();
            //    con.myparameters.Add(new myParameters { ParameterName = "EmpID", mytype = SqlDbType.NVarChar, Value = qr.EmpID });
            //    return con.ExecuteScalar("sp_QRCode_ValidateInterval");
            //}
            //catch (Exception e)
            //{
            //    return e.ToString();
            //}
            try
            {
                if (qr.CN.ToLower() == "bataan2020" || qr.CN.ToLower() == "bataan2020-samal" || qr.CN.ToLower() == "ayhpi")
                {
                    tempconnectionstring();
                    Connection con = new Connection();
                    con.myparameters.Add(new myParameters { ParameterName = "EmpID", mytype = SqlDbType.NVarChar, Value = qr.EmpID });
                    if (con.ExecuteScalar("sp_QRBundy_CheckDB").ToString() != "User not found")
                    {
                        GetDatabase GDB = new GetDatabase();
                        GDB.ClientName = con.ExecuteScalar("sp_QRBundy_CheckDB").ToString();
                        SelfieRegistration2Controller.GetDB2(GDB);
                        con = new Connection();
                        con.myparameters.Add(new myParameters { ParameterName = "EmpID", mytype = SqlDbType.NVarChar, Value = qr.EmpID });
                        return con.ExecuteScalar("sp_QRCode_ValidateInterval");
                    }
                    else
                    {
                        return "Unavailable";
                    }
                }
                else
                {
                    GetDatabase GDB = new GetDatabase();
                    GDB.ClientName = qr.CN;
                    SelfieRegistration2Controller.GetDB2(GDB);
                    Connection con = new Connection();
                    con.myparameters.Add(new myParameters { ParameterName = "EmpID", mytype = SqlDbType.NVarChar, Value = qr.EmpID });
                    return con.ExecuteScalar("sp_QRCode_ValidateInterval");
                }

            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }

        [HttpPost]
        [Route("QRValidatePic1")]
        public string validatepic1(qrvalidate prms)
        {
            try
            {
                GetDatabase GDB = new GetDatabase();
                GDB.ClientName = prms.CN;
                SelfieRegistration2Controller.GetDB2(GDB);
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "EmpID", mytype = SqlDbType.NVarChar, Value = prms.EmpID });
                return con.ExecuteScalar("sp_QR_CheckPic1");
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }

        [HttpPost]
        [Route("FaceAppInsert")]
        public string faceappinsert(faceAppInsert prms)
        {
            try
            {
                if (prms.CN.ToLower() == "bataan2020" || prms.CN.ToLower() == "bataan2020-samal" || prms.CN.ToLower() == "ayhpi")
                {
                    GetDatabase GDB = new GetDatabase();
                    GDB.ClientName = "Bataan2020";
                    SelfieRegistration2Controller.GetDB2(GDB);
                    Connection con = new Connection();
                    con.myparameters.Add(new myParameters { ParameterName = "EmpID", mytype = SqlDbType.NVarChar, Value = prms.EmpID });
                    con.myparameters.Add(new myParameters { ParameterName = "BaseID", mytype = SqlDbType.NVarChar, Value = prms.BaseID });
                    con.myparameters.Add(new myParameters { ParameterName = "value", mytype = SqlDbType.NVarChar, Value = prms.value });
                    con.ExecuteNonQuery("sp_Insert_FaceRecogApp");

                    GetDatabase GDB1 = new GetDatabase();
                    GDB1.ClientName = "Bataan2020-Samal";
                    SelfieRegistration2Controller.GetDB2(GDB1);
                    Connection con1 = new Connection();
                    con1.myparameters.Add(new myParameters { ParameterName = "EmpID", mytype = SqlDbType.NVarChar, Value = prms.EmpID });
                    con1.myparameters.Add(new myParameters { ParameterName = "BaseID", mytype = SqlDbType.NVarChar, Value = prms.BaseID });
                    con1.myparameters.Add(new myParameters { ParameterName = "value", mytype = SqlDbType.NVarChar, Value = prms.value });
                    con1.ExecuteNonQuery("sp_Insert_FaceRecogApp");

                    GetDatabase GDB2 = new GetDatabase();
                    GDB2.ClientName = "AYHPI";
                    SelfieRegistration2Controller.GetDB2(GDB2);
                    Connection con2 = new Connection();
                    con2.myparameters.Add(new myParameters { ParameterName = "EmpID", mytype = SqlDbType.NVarChar, Value = prms.EmpID });
                    con2.myparameters.Add(new myParameters { ParameterName = "BaseID", mytype = SqlDbType.NVarChar, Value = prms.BaseID });
                    con2.myparameters.Add(new myParameters { ParameterName = "value", mytype = SqlDbType.NVarChar, Value = prms.value });
                    con2.ExecuteNonQuery("sp_Insert_FaceRecogApp");

                    return "success";
                }
                else
                {
                    GetDatabase GDB = new GetDatabase();
                    GDB.ClientName = prms.CN;
                    SelfieRegistration2Controller.GetDB2(GDB);
                    Connection con = new Connection();
                    con.myparameters.Add(new myParameters { ParameterName = "EmpID", mytype = SqlDbType.NVarChar, Value = prms.EmpID });
                    con.myparameters.Add(new myParameters { ParameterName = "BaseID", mytype = SqlDbType.NVarChar, Value = prms.BaseID });
                    con.myparameters.Add(new myParameters { ParameterName = "value", mytype = SqlDbType.NVarChar, Value = prms.value });
                    con.ExecuteNonQuery("sp_Insert_FaceRecogApp");

                    return "success";
                }
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }

        [HttpPost]
        [Route("SaveQRImage")]
        public string SaveQRImage(saveQRImage prms)
        {


            byte[] Photo = Convert.FromBase64String(prms.base64);
            var path = HttpContext.Current.Server.MapPath("~/ProfilePhotos/QR_Bundy_Images/" + prms.CN + "/" + "");
            //var folder = Path.Combine(path, prms.EmpID);
            Directory.CreateDirectory(path);
            MemoryStream stream = new MemoryStream();

            try
            {
                using (stream)
                {
                    //File.WriteAllBytes(folder + "/" + NTID + "_pic" + Set + ".png", Photo);
                    File.WriteAllBytes(path + prms.EmpID + ".png", Photo);
                }

                return "success";
            }
            catch
            {
                return "error";
            }


        }

        [HttpPost]
        [Route("Display_OrgChart")]
        public string displayOrgChart(displayOrgChart prms)
        {
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = prms.CN;
            SelfieRegistration2Controller.GetDB2(GDB);
            Connection con = new Connection();
            return con.ExecuteScalar("sp_Orgchart");
        }
        [HttpPost]
        [Route("jsnobj")]
        public JObject retobj(displayOrgChart prms)
        {
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = prms.CN;
            SelfieRegistration2Controller.GetDB2(GDB);
            Connection con = new Connection();

            jsnclss retclass = new jsnclss();

            con.myparameters.Add(new myParameters { ParameterName = "Root_id", mytype = SqlDbType.NVarChar, Value = prms.RootID });
            return JObject.Parse(con.ExecuteScalar("sp_Orgchart").ToString());
        }
        [HttpPost]
        [Route("getRoot")]
        public getRootList root(displayOrgChart prms)
        {
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = prms.CN;
            SelfieRegistration2Controller.GetDB2(GDB);
            Connection con = new Connection();
            DataTable dt = con.GetDataTable("sp_OrgChart_GetRoot");
            getRootList rl = new getRootList();
            List<getRoot> getRT = new List<getRoot>();

            foreach (DataRow row in dt.Rows)
            {
                getRoot gr = new getRoot
                {
                    EmpID = row["EmpName"].ToString(),
                    EmpName = row["EmpID"].ToString(),
                    EmpLevel = row["EmpLevel"].ToString()
                };
                getRT.Add(gr);
            }
            rl.getRoot = getRT;
            rl.myreturn = "success";
            return rl;
        }
        [HttpPost]
        [Route("adj_EmpSalary")]
        public string adjEmpSalary(EmpSal param)
        {
            try
            {
                GetDatabase GDB = new GetDatabase();
                GDB.ClientName = param.CN;
                SelfieRegistration2Controller.GetDB2(GDB);

                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.VarChar, Value = param.EmpID });
                con.myparameters.Add(new myParameters { ParameterName = "@Salary", mytype = SqlDbType.VarChar, Value = param.Salary });
                con.ExecuteNonQuery("sp_Adj_EmpSalary");
                return "Success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }
        [HttpPost]
        [Route("get_EmpLogsLeave")]
        public EmployeeLogsLeave EmpLogsLeave(EmpLogLeaveParam param)
        {
            EmployeeLogsLeave retclass = new EmployeeLogsLeave();
            List<EmpLogLeave> ret = new List<EmpLogLeave>();
            try
            {
                GetDatabase GDB = new GetDatabase();
                GDB.ClientName = param.CN;
                SelfieRegistration2Controller.GetDB2(GDB);


                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.VarChar, Value = param.EmpID });
                con.myparameters.Add(new myParameters { ParameterName = "@Scheddate", mytype = SqlDbType.VarChar, Value = param.SchedDate });
                DataTable DT = con.GetDataTable("sp_Get_EmpLogsLeave");

                foreach (DataRow row in DT.Rows)
                {
                    EmpLogLeave ELL = new EmpLogLeave
                    {
                        TimeIN = row["Time In"].ToString(),
                        TimeOUT = row["Time Out"].ToString(),
                        ApprovedOT = row["Approved Overtime"].ToString(),
                        ApprovedLeave = row["Approved Leave"].ToString()
                    };
                    ret.Add(ELL);
                }
                retclass.EmpLogLeave = ret;
                retclass.myreturn = "Success";
            }
            catch (Exception e)
            {
                retclass.EmpLogLeave = ret;
                retclass.myreturn = e.ToString();
            }
            return retclass;
        }
        [HttpPost]
        [Route("Emp_Payslip")]
        public EmpPayslip EmpPayslip(EmpPayslipParam param)
        {
            EmpPayslip retclass = new EmpPayslip();
            List<EmployeePayslipDetails> ret = new List<EmployeePayslipDetails>();
            try
            {
                GetDatabase GDB = new GetDatabase();
                GDB.ClientName = param.CN;
                SelfieRegistration2Controller.GetDB2(GDB);

                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.VarChar, Value = param.EmpID });
                con.myparameters.Add(new myParameters { ParameterName = "@PayPeriod", mytype = SqlDbType.VarChar, Value = param.PayPeriod });
                DataTable DT = con.GetDataTable("sp_Get_EmpPayslipDetails");

                foreach (DataRow row in DT.Rows)
                {
                    EmployeePayslipDetails EPD = new EmployeePayslipDetails
                    {
                        EmpID = row["EmpID"].ToString(),
                        PayPeriod = row["Pay_Period"].ToString(),
                        PayDay = row["Pay_Day"].ToString(),
                        HourlyRate = row["Hourly_Rate"].ToString(),
                        HDMF = row["HDMF"].ToString(),
                        PhilHealth = row["PhilHealth"].ToString(),
                        SSS = row["SSS"].ToString(),
                        Tax = row["Tax"].ToString(),
                        Employer_HDMF = row["Employer_HDMF"].ToString(),
                        Employer_PhilHealth = row["Employe_PhilHealth"].ToString(),
                        Employer_SSS = row["Employer_SSS"].ToString(),
                        Employer_SSS_EC = row["Employe_SSS_EC"].ToString(),
                        NetPay = row["Net_Pay"].ToString(),
                        NightDiff = row["NightDiff"].ToString(),
                        BasicPay = row["BasicPay"].ToString(),
                        DailyRate = row["DailyRate"].ToString(),
                        GrossPay = row["GrossPay"].ToString(),
                        Total_Reg_Hrs = row["Total_REG_Hrs"].ToString(),
                        Total_Reg_Amt = row["Total_REG_Amount"].ToString()
                    };
                    ret.Add(EPD);
                }

                retclass.Payslip = ret;
                retclass.myreturn = "Success";
            }
            catch (Exception e)
            {
                retclass.Payslip = ret;
                retclass.myreturn = e.ToString();
            }
            return retclass;
        }
        [HttpPost]
        [Route("generatepdfBulk")]
        public string generatepdfBulk(generatePDF prms)
        {



            string datebuild = DateTime.Now.ToString("yyyyMMddHHmmss");

            string dataDir = HostingEnvironment.MapPath("~/pdf/");
            string zipFileName = prms.CN + "_" + datebuild + ".zip";
            using (var archive = ZipFile.Open(dataDir + zipFileName, ZipArchiveMode.Create))
            {
                try
                {
                    GetDatabase GDB = new GetDatabase();
                    GDB.ClientName = prms.CN;
                    SelfieRegistration2Controller.GetDB2(GDB);
                    for (int x = 0; x < prms.id.Length; x++)
                    {
                        prms.EmpID = prms.id[x];
                        // DATA VARIABLE DECLARATION
                        string startdate = "", enddate = "";
                        string payday = "";
                        string fullname = "", empid = "", datejoined = "", position = "", department = "", TIN = "", SSS = "", PHILHEALTH = "", PAGIBIG = "";
                        string basicpay = "", grosspay = "", netpay = "", whtax = "", ytdBasicPay = "", ytdGrossPay = "", ytdNetPay = "", ytdWhTax = "";
                        string HourlyRate = "", HoursWorked = "", AbsencesDesc = "Absences", TardiDesc = "Tardiness", UndertimeDesc = "Undertime", LWOPDesc = "LWOP", Monthly = "";
                        string Absences_Hours = "", Tardi_Hours = "", Undertime_Hours = "", LWOP_Hours = "";
                        string Absences_Amount = "", Tardi_Amount = "", Undertime_Amount = "", LWOP_Amount = "";
                        string Absences_YTD_Amount = "", Tardi_YTD_Amount = "", Undertime_YTD_Amount = "", LWOP_YTD_Amount = "";
                        string CompanyName = "";

                        // FOR COMPANY NAME
                        Connection conn = new Connection();
                        conn.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.VarChar, Value = prms.EmpID });
                        DataTable dtbl = conn.GetDataTable("sp_get_CompanyName_MobileDynamicPayslip");
                        foreach (DataRow item in dtbl.Rows)
                        {
                            CompanyName = item["TradingName"].ToString();
                        }
                        // FOR PAY PERIOD FIELD
                        Connection con = new Connection();
                        con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID });
                        con.myparameters.Add(new myParameters { ParameterName = "@payoutDate", mytype = SqlDbType.VarChar, Value = prms.PayoutDate });
                        DataTable DT = con.GetDataTable("sp_GetPayPeriod_Payslip");
                        foreach (DataRow item in DT.Rows)
                        {
                            startdate = item["StartDate"].ToString();
                            enddate = item["EndDate"].ToString();
                        }


                        GDB = new GetDatabase();
                        GDB.ClientName = prms.CN;
                        SelfieRegistration2Controller.GetDB2(GDB);
                        // FOR PAY DATE FIELD
                        con = new Connection();
                        con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID });
                        DataTable DT1 = con.GetDataTable("sp_GetPayoutSchemeList");
                        foreach (DataRow item in DT1.Rows)
                        {
                            payday = item["PayoutDate"].ToString();
                        }

                        GDB = new GetDatabase();
                        GDB.ClientName = prms.CN;
                        SelfieRegistration2Controller.GetDB2(GDB);
                        // EMPLOYEE DETAILS FIELD
                        con = new Connection();
                        con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID });
                        DataTable dt2 = con.GetDataTable("sp_GetEmployeeInfo_Payslip");
                        foreach (DataRow item in dt2.Rows)
                        {
                            fullname = item["EmpName"].ToString();
                            empid = item["EmpID"].ToString();
                            datejoined = item["DateJoined"].ToString();
                            department = item["BusinessUnit"].ToString();
                            position = item["JobDesc"].ToString();
                        }

                        GDB = new GetDatabase();
                        GDB.ClientName = prms.CN;
                        SelfieRegistration2Controller.GetDB2(GDB);
                        // FOR GOVERNMENT ID FIELDS
                        con = new Connection();
                        con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID });
                        DataTable dt3 = con.GetDataTable("sp_GetGovIDs_Payslip");
                        foreach (DataRow item in dt3.Rows)
                        {
                            if (item["IDTypes"].ToString() == "SSS")
                            {
                                SSS = item["IDCode"].ToString();
                            }
                            else if (item["IDTypes"].ToString() == "Philhealth")
                            {
                                PHILHEALTH = item["IDCode"].ToString();
                            }
                            else if (item["IDTypes"].ToString() == "Pag-Ibig")
                            {
                                PAGIBIG = item["IDCode"].ToString();
                            }
                            else
                            {
                                TIN = item["IDCode"].ToString();
                            }
                        }

                        GDB = new GetDatabase();
                        GDB.ClientName = prms.CN;
                        SelfieRegistration2Controller.GetDB2(GDB);
                        // FOR GROSS PAY, BASIC PAY AND NET PAY FIELDS
                        con = new Connection();
                        con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID });
                        con.myparameters.Add(new myParameters { ParameterName = "@payoutDate", mytype = SqlDbType.VarChar, Value = prms.PayoutDate });
                        DataTable dt4 = con.GetDataTable("sp_GetPay_Payslip");
                        foreach (DataRow item in dt4.Rows)
                        {
                            basicpay = item["Basic Pay"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(item["Basic Pay"].ToString()));
                            grosspay = item["Gross Pay"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(item["Gross Pay"].ToString()));
                            netpay = item["Net Pay"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(item["Net Pay"].ToString()));
                            whtax = item["WithHolding Tax"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(item["WithHolding Tax"].ToString()));

                            //basicpay = String.Format("{0:n2}", decimal.Parse(basicpay));
                        }
                        // END


                        GDB = new GetDatabase();
                        GDB.ClientName = prms.CN;
                        SelfieRegistration2Controller.GetDB2(GDB);
                        // YTD GROSS PAY, BASIC PAY AND NET PAY FIELDS
                        con = new Connection();
                        con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID });
                        con.myparameters.Add(new myParameters { ParameterName = "@year", mytype = SqlDbType.VarChar, Value = prms.ytd_year });
                        con.myparameters.Add(new myParameters { ParameterName = "@payoutDate", mytype = SqlDbType.VarChar, Value = prms.PayoutDate });
                        DataTable dt5 = con.GetDataTable("sp_GetPay_YTD_Payslip");
                        foreach (DataRow item in dt5.Rows)
                        {
                            ytdBasicPay = item["basicpay"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(item["basicpay"].ToString()));
                            ytdGrossPay = item["grosspay"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(item["grosspay"].ToString()));
                            ytdWhTax = item["whtax"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(item["whtax"].ToString()));
                            ytdNetPay = item["netpay"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(item["netpay"].ToString()));
                        }

                        GDB = new GetDatabase();
                        GDB.ClientName = prms.CN;
                        SelfieRegistration2Controller.GetDB2(GDB);
                        // FOR LOGS INFO
                        con = new Connection();
                        con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID });
                        con.myparameters.Add(new myParameters { ParameterName = "@payoutDate", mytype = SqlDbType.VarChar, Value = prms.PayoutDate });
                        DataTable dt16 = con.GetDataTable("sp_GetLogInfo_Payslip");

                        if (dt16.Rows.Count > 0)
                        {
                            HourlyRate = dt16.Rows[0]["HourlyRate"] == DBNull.Value || dt16.Rows[0]["HourlyRate"].ToString() == "" ? "" : String.Format("{0:n2}", decimal.Parse(dt16.Rows[0]["HourlyRate"].ToString()));
                            HoursWorked = dt16.Rows[0]["HoursWorked"] == DBNull.Value || dt16.Rows[0]["HoursWorked"].ToString() == "" ? "" : String.Format("{0:n2}", decimal.Parse(dt16.Rows[0]["HoursWorked"].ToString()));
                            //Monthly = dt16.Rows[0]["Monthly"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt16.Rows[0]["Monthly"].ToString()));
                        }


                        GDB = new GetDatabase();
                        GDB.ClientName = prms.CN;
                        SelfieRegistration2Controller.GetDB2(GDB);
                        con = new Connection();
                        con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID });
                        con.myparameters.Add(new myParameters { ParameterName = "@year", mytype = SqlDbType.VarChar, Value = prms.ytd_year });
                        con.myparameters.Add(new myParameters { ParameterName = "@payoutDate", mytype = SqlDbType.VarChar, Value = prms.PayoutDate });
                        DataTable dt17 = con.GetDataTable("sp_GetLogInfo_YTD_Payslip");
                        string filename = "payslip-" + prms.EmpID + "-" + prms.PayoutDate + ".pdf";
                        System.IO.FileStream fs = new FileStream(HttpContext.Current.Server.MapPath("pdf") + "\\" + "payslip-" + prms.EmpID + "-" + prms.PayoutDate + ".pdf", FileMode.Create);
                        //System.IO.FileStream fb = new FileStream(HttpContext.Current.Server.MapPath("pdf") + "\\" + "ilm.png", FileMode.Open);
                        string imagepath = HttpContext.Current.Server.MapPath("Images");
                        Document document = new Document(PageSize.A4, 25, 25, 30, 30);
                        PdfWriter writer = PdfWriter.GetInstance(document, fs);

                        // Set Password
                        DataTable dt_payslip_password = DynamicPayslip_GetPassword(prms.EmpID);

                        if (dt_payslip_password != null && dt_payslip_password.Rows.Count > 0)
                            if ((dt_payslip_password.Rows[0][0].ToString() != null || dt_payslip_password.Rows[0][0].ToString() != "") && dt_payslip_password.Rows[0][1].ToString().ToLower() == "active")
                                writer.SetEncryption(true, dt_payslip_password.Rows[0][0].ToString(), "Illimitado", PdfWriter.ALLOW_SCREENREADERS);

                        //PdfWriter writering = PdfWriter.GetInstance(document, fb);
                        document.Open();

                        PdfPTable table0 = new PdfPTable(1);

                        PdfPCell cell0 = new PdfPCell(new iTextSharp.text.Phrase(CompanyName, FontFactory.GetFont("Arial", 12, Font.BOLD))) { Border = 0, HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT, VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE };
                        cell0.PaddingTop = 10;
                        //PdfPCell cell01 = new PdfPCell(new iTextSharp.text.Phrase("Lima Technology Center  Lipa City, Batangas Philippines 4217", FontFactory.GetFont("Arial", 11, Font.NORMAL))) { Border = 0, HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT, VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE };
                        //cell01.PaddingBottom = 7;

                        //Image logoimage = Image.GetInstance(Path.Combine(imagepath, "yokohamalogo.jpg"));
                        //logoimage.ScaleAbsolute(100, 40);
                        //logoimage.SetAbsolutePosition(100, 30);

                        //PdfPCell LogoCell = new PdfPCell(logoimage, true);

                        table0.AddCell(cell0);
                        ////table0.AddCell(LogoCell);
                        //table0.AddCell(cell01);
                        //document.Add(LogoCell);


                        PdfPTable table1 = new PdfPTable(2);

                        /*var titleFont = new Font(Font.FontFamily.UNDEFINED, 12);
                        var informationDetails = new Font(Font.FontFamily.UNDEFINED, 12);*/

                        PdfPCell cell11 = new PdfPCell(new iTextSharp.text.Phrase("PAY PERIOD\n" + startdate + " - " + enddate)) { Border = PdfPCell.BOTTOM_BORDER | PdfPCell.TOP_BORDER, HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER, VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE };
                        PdfPCell cell12 = new PdfPCell(new iTextSharp.text.Phrase("PAY DAY\n" + prms.PayoutDate)) { Border = PdfPCell.BOTTOM_BORDER | PdfPCell.TOP_BORDER, HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER, VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE };
                        cell11.PaddingTop = 10;
                        cell12.PaddingTop = 10;
                        cell11.PaddingBottom = 7;
                        cell12.PaddingBottom = 7;
                        /* cell12.AddElement(new Paragraph("PAY DAY", titleFont));
                        cell12.AddElement(new Paragraph("-", informationDetails));*/

                        PdfPTable table2 = new PdfPTable(2);
                        float[] widths = new float[] { 100f, 200f };
                        float[] heights = new float[] { 300f };
                        table2.SetWidths(widths);
                        //table2.DefaultCell.FixedHeight = 500f;
                        table2.DefaultCell.Border = Rectangle.NO_BORDER;
                        //PdfPCell cell21 = new PdfPCell(new Paragraph("Danilo Vicente Sulla Jr")) { Border = PdfPCell.RIGHT_BORDER, HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER, VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE };
                        //cell21.PaddingTop = 20;

                        PdfPTable table3 = new PdfPTable(2);
                        table3.TotalWidth = 100;
                        table3.DefaultCell.FixedHeight = 600f;
                        table3.DefaultCell.Border = Rectangle.RIGHT_BORDER;

                        PdfPCell tabl3cell0 = new PdfPCell(new PdfPCell(new Paragraph(fullname, FontFactory.GetFont("Arial", 12, Font.BOLD)))) { Border = PdfPCell.NO_BORDER, HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER, VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE };
                        tabl3cell0.Colspan = 2;
                        tabl3cell0.PaddingTop = 20;
                        tabl3cell0.PaddingBottom = 20;
                        tabl3cell0.BorderWidth = 0;
                        tabl3cell0.BorderWidthRight = 1;
                        table3.AddCell(tabl3cell0);


                        PdfPCell tabl3cell1 = new PdfPCell(new PdfPCell(new Paragraph("EmpID", FontFactory.GetFont("arial", 8))));
                        tabl3cell1.PaddingBottom = 7;
                        tabl3cell1.BorderWidth = 0;
                        tabl3cell1.BorderWidthBottom = 1;
                        table3.AddCell(tabl3cell1);

                        PdfPCell tabl3cell2 = new PdfPCell(new PdfPCell(new Paragraph(empid, FontFactory.GetFont("arial", 8, Font.BOLD))));
                        tabl3cell2.HorizontalAlignment = Element.ALIGN_RIGHT;
                        tabl3cell2.PaddingBottom = 7;
                        tabl3cell2.BorderWidth = 0;
                        tabl3cell2.BorderWidthBottom = 1;
                        tabl3cell2.BorderWidthRight = 1;
                        table3.AddCell(tabl3cell2);

                        PdfPCell tabl3cell3 = new PdfPCell(new PdfPCell(new Paragraph("Tax Category", FontFactory.GetFont("arial", 8))));
                        tabl3cell3.PaddingBottom = 7;
                        tabl3cell3.BorderWidth = 0;
                        tabl3cell3.BorderWidthBottom = 1;
                        table3.AddCell(tabl3cell3);

                        PdfPCell tabl3cell4 = new PdfPCell(new PdfPCell(new Paragraph("--", FontFactory.GetFont("arial", 8, Font.BOLD))));
                        tabl3cell4.HorizontalAlignment = Element.ALIGN_RIGHT;
                        tabl3cell4.PaddingBottom = 7;
                        tabl3cell4.BorderWidth = 0;
                        tabl3cell4.BorderWidthBottom = 1;
                        tabl3cell4.BorderWidthRight = 1;
                        table3.AddCell(tabl3cell4);

                        PdfPCell tabl3cell5 = new PdfPCell(new PdfPCell(new Paragraph("Hourly Rate", FontFactory.GetFont("arial", 8))));
                        tabl3cell5.PaddingBottom = 7;
                        tabl3cell5.BorderWidth = 0;
                        tabl3cell5.BorderWidthBottom = 1;
                        table3.AddCell(tabl3cell5);

                        PdfPCell tabl3cell6 = new PdfPCell(new PdfPCell(new Paragraph(HourlyRate, FontFactory.GetFont("arial", 8, Font.BOLD))));
                        tabl3cell6.HorizontalAlignment = Element.ALIGN_RIGHT;
                        tabl3cell6.PaddingBottom = 7;
                        tabl3cell6.BorderWidth = 0;
                        tabl3cell6.BorderWidthBottom = 1;
                        tabl3cell6.BorderWidthRight = 1;
                        table3.AddCell(tabl3cell6);

                        PdfPCell tabl3cell7 = new PdfPCell(new PdfPCell(new Paragraph("Regular Hours", FontFactory.GetFont("arial", 8))));
                        tabl3cell7.PaddingBottom = 7;
                        tabl3cell7.BorderWidth = 0;
                        tabl3cell7.BorderWidthBottom = 1;
                        table3.AddCell(tabl3cell7);

                        PdfPCell tabl3cell8 = new PdfPCell(new PdfPCell(new Paragraph(HoursWorked, FontFactory.GetFont("arial", 8, Font.BOLD))));
                        tabl3cell8.HorizontalAlignment = Element.ALIGN_RIGHT;
                        tabl3cell8.PaddingBottom = 7;
                        tabl3cell8.BorderWidth = 0;
                        tabl3cell8.BorderWidthBottom = 1;
                        tabl3cell8.BorderWidthRight = 1;
                        table3.AddCell(tabl3cell8);

                        PdfPCell tabl3cell9 = new PdfPCell(new PdfPCell(new Paragraph(getSalaryScheme(Monthly.ToString()) + " Salary", FontFactory.GetFont("arial", 8))));
                        tabl3cell9.PaddingBottom = 7;
                        tabl3cell9.BorderWidth = 0;
                        tabl3cell9.BorderWidthBottom = 1;
                        table3.AddCell(tabl3cell9);

                        PdfPCell tabl3cell10 = new PdfPCell(new PdfPCell(new Paragraph(Monthly, FontFactory.GetFont("arial", 8, Font.BOLD))));
                        tabl3cell10.HorizontalAlignment = Element.ALIGN_RIGHT;
                        tabl3cell10.PaddingBottom = 7;
                        tabl3cell10.BorderWidth = 0;
                        tabl3cell10.BorderWidthBottom = 1;
                        tabl3cell10.BorderWidthRight = 1;
                        table3.AddCell(tabl3cell10);

                        PdfPCell tabl3cell11 = new PdfPCell(new PdfPCell(new Paragraph("Date Joined", FontFactory.GetFont("arial", 8))));
                        tabl3cell11.PaddingBottom = 7;
                        tabl3cell11.BorderWidth = 0;
                        tabl3cell11.BorderWidthBottom = 1;
                        table3.AddCell(tabl3cell11);

                        PdfPCell tabl3cell12 = new PdfPCell(new PdfPCell(new Paragraph(datejoined, FontFactory.GetFont("arial", 8, Font.BOLD))));
                        tabl3cell12.HorizontalAlignment = Element.ALIGN_RIGHT;
                        tabl3cell12.PaddingBottom = 7;
                        tabl3cell12.BorderWidth = 0;
                        tabl3cell12.BorderWidthBottom = 1;
                        tabl3cell12.BorderWidthRight = 1;
                        table3.AddCell(tabl3cell12);

                        PdfPCell tabl3cell13 = new PdfPCell(new PdfPCell(new Paragraph("Position", FontFactory.GetFont("arial", 8))));
                        tabl3cell13.PaddingBottom = 7;
                        tabl3cell13.BorderWidth = 0;
                        tabl3cell13.BorderWidthBottom = 1;
                        table3.AddCell(tabl3cell13);

                        PdfPCell tabl3cell14 = new PdfPCell(new PdfPCell(new Paragraph(position, FontFactory.GetFont("arial", 8, Font.BOLD))));
                        tabl3cell14.HorizontalAlignment = Element.ALIGN_RIGHT;
                        tabl3cell14.PaddingBottom = 7;
                        tabl3cell14.BorderWidth = 0;
                        tabl3cell14.BorderWidthBottom = 1;
                        tabl3cell14.BorderWidthRight = 1;
                        table3.AddCell(tabl3cell14);

                        PdfPCell tabl3cell15 = new PdfPCell(new PdfPCell(new Paragraph("Department", FontFactory.GetFont("arial", 8))));
                        tabl3cell15.PaddingBottom = 7;
                        tabl3cell15.BorderWidth = 0;
                        tabl3cell15.BorderWidthBottom = 1;
                        table3.AddCell(tabl3cell15);

                        PdfPCell tabl3cell16 = new PdfPCell(new PdfPCell(new Paragraph(department, FontFactory.GetFont("arial", 8, Font.BOLD))));
                        tabl3cell16.HorizontalAlignment = Element.ALIGN_RIGHT;
                        tabl3cell16.PaddingBottom = 7;
                        tabl3cell16.BorderWidth = 0;
                        tabl3cell16.BorderWidthBottom = 1;
                        tabl3cell16.BorderWidthRight = 1;
                        table3.AddCell(tabl3cell16);

                        PdfPCell tabl3cell17 = new PdfPCell(new PdfPCell(new Paragraph("Philhealth", FontFactory.GetFont("arial", 8))));
                        tabl3cell17.PaddingBottom = 7;
                        tabl3cell17.BorderWidth = 0;
                        tabl3cell17.BorderWidthBottom = 1;
                        table3.AddCell(tabl3cell17);

                        PdfPCell tabl3cell18 = new PdfPCell(new PdfPCell(new Paragraph(PHILHEALTH, FontFactory.GetFont("arial", 8, Font.BOLD))));
                        tabl3cell18.HorizontalAlignment = Element.ALIGN_RIGHT;
                        tabl3cell18.PaddingBottom = 7;
                        tabl3cell18.BorderWidth = 0;
                        tabl3cell18.BorderWidthBottom = 1;
                        tabl3cell18.BorderWidthRight = 1;
                        table3.AddCell(tabl3cell18);

                        PdfPCell tabl3cell19 = new PdfPCell(new PdfPCell(new Paragraph("Pag-Ibig", FontFactory.GetFont("arial", 8))));
                        tabl3cell19.PaddingBottom = 7;
                        tabl3cell19.BorderWidth = 0;
                        tabl3cell19.BorderWidthBottom = 1;
                        table3.AddCell(tabl3cell19);

                        PdfPCell tabl3cell20 = new PdfPCell(new PdfPCell(new Paragraph(PAGIBIG, FontFactory.GetFont("arial", 8, Font.BOLD))));
                        tabl3cell20.HorizontalAlignment = Element.ALIGN_RIGHT;
                        tabl3cell20.PaddingBottom = 7;
                        tabl3cell20.BorderWidth = 0;
                        tabl3cell20.BorderWidthBottom = 1;
                        tabl3cell20.BorderWidthRight = 1;
                        table3.AddCell(tabl3cell20);

                        PdfPCell tabl3cell21 = new PdfPCell(new PdfPCell(new Paragraph("TIN", FontFactory.GetFont("arial", 8))));
                        tabl3cell21.PaddingBottom = 7;
                        tabl3cell21.BorderWidth = 0;
                        tabl3cell21.BorderWidthBottom = 1;

                        table3.AddCell(tabl3cell21);

                        PdfPCell tabl3cell22 = new PdfPCell(new PdfPCell(new Paragraph(TIN, FontFactory.GetFont("arial", 8, Font.BOLD))));
                        tabl3cell22.HorizontalAlignment = Element.ALIGN_RIGHT;
                        tabl3cell22.PaddingBottom = 7;
                        tabl3cell22.BorderWidth = 0;
                        tabl3cell22.BorderWidthBottom = 1;
                        tabl3cell22.BorderWidthRight = 1;
                        table3.AddCell(tabl3cell22);

                        PdfPCell tabl3cell23 = new PdfPCell(new PdfPCell(new Paragraph("SSS", FontFactory.GetFont("arial", 8))));
                        tabl3cell23.PaddingBottom = 7;
                        tabl3cell23.BorderWidth = 0;
                        table3.AddCell(tabl3cell23);

                        PdfPCell tabl3cell24 = new PdfPCell(new PdfPCell(new Paragraph(SSS, FontFactory.GetFont("arial", 8, Font.BOLD))));
                        tabl3cell24.HorizontalAlignment = Element.ALIGN_RIGHT;
                        tabl3cell24.PaddingBottom = 7;
                        tabl3cell24.BorderWidth = 0;
                        tabl3cell24.BorderWidthRight = 1;
                        table3.AddCell(tabl3cell24);


                        table2.AddCell(table3);

                        //
                        PdfPTable table4 = new PdfPTable(4);
                        table4.TotalWidth = 100;
                        table4.DefaultCell.Border = Rectangle.NO_BORDER;

                        PdfPCell tabl4cell1 = new PdfPCell(new PdfPCell(new Paragraph("", FontFactory.GetFont("arial", 8))));
                        tabl4cell1.PaddingBottom = 7;
                        tabl4cell1.BorderWidth = 0;
                        table4.AddCell(tabl4cell1);

                        PdfPCell tabl4cell2 = new PdfPCell(new PdfPCell(new Paragraph("HOURS", FontFactory.GetFont("arial", 8, Font.BOLD))));
                        tabl4cell2.HorizontalAlignment = Element.ALIGN_RIGHT;
                        tabl4cell2.PaddingBottom = 7;
                        tabl4cell2.BorderWidth = 0;
                        table4.AddCell(tabl4cell2);

                        PdfPCell tabl4cell3 = new PdfPCell(new PdfPCell(new Paragraph("CURRENT (PHP)", FontFactory.GetFont("arial", 8, Font.BOLD))));
                        tabl4cell3.HorizontalAlignment = Element.ALIGN_CENTER;
                        tabl4cell3.PaddingBottom = 7;
                        tabl4cell3.BorderWidth = 0;
                        table4.AddCell(tabl4cell3);

                        PdfPCell tabl4cell4 = new PdfPCell(new PdfPCell(new Paragraph("YEAR TO DATE(PHP)", FontFactory.GetFont("arial", 8, Font.BOLD))));
                        tabl4cell4.HorizontalAlignment = Element.ALIGN_RIGHT;
                        tabl4cell4.PaddingBottom = 7;
                        tabl4cell4.BorderWidth = 0;
                        table4.AddCell(tabl4cell4);


                        //
                        PdfPCell tabl4cell5 = new PdfPCell(new PdfPCell(new Paragraph("Basic Pay", FontFactory.GetFont("arial", 8))));
                        tabl4cell5.PaddingBottom = 7;
                        tabl4cell5.BorderWidth = 0;
                        table4.AddCell(tabl4cell5);

                        PdfPCell tabl4cell6 = new PdfPCell(new PdfPCell(new Paragraph("", FontFactory.GetFont("arial", 8))));
                        tabl4cell6.HorizontalAlignment = Element.ALIGN_RIGHT;
                        tabl4cell6.PaddingBottom = 7;
                        tabl4cell6.BorderWidth = 0;
                        table4.AddCell(tabl4cell6);

                        PdfPCell tabl4cell7 = new PdfPCell(new PdfPCell(new Paragraph(basicpay, FontFactory.GetFont("arial", 8))));
                        tabl4cell7.HorizontalAlignment = Element.ALIGN_CENTER;
                        tabl4cell7.PaddingBottom = 7;
                        tabl4cell7.BorderWidth = 0;
                        table4.AddCell(tabl4cell7);

                        PdfPCell tabl4cell8 = new PdfPCell(new PdfPCell(new Paragraph(ytdBasicPay, FontFactory.GetFont("arial", 8))));
                        tabl4cell8.HorizontalAlignment = Element.ALIGN_RIGHT;
                        tabl4cell8.PaddingBottom = 7;
                        tabl4cell8.BorderWidth = 0;
                        table4.AddCell(tabl4cell8);
                        //

                        //
                        PdfPCell tabl4cell9 = new PdfPCell(new PdfPCell(new Paragraph("Gross Pay", FontFactory.GetFont("arial", 8))));
                        tabl4cell9.PaddingBottom = 7;
                        tabl4cell9.BorderWidth = 0;
                        table4.AddCell(tabl4cell9);

                        PdfPCell tabl4cell10 = new PdfPCell(new PdfPCell(new Paragraph("", FontFactory.GetFont("arial", 8))));
                        tabl4cell10.HorizontalAlignment = Element.ALIGN_RIGHT;
                        tabl4cell10.PaddingBottom = 7;
                        tabl4cell10.BorderWidth = 0;
                        table4.AddCell(tabl4cell10);

                        PdfPCell tabl4cell11 = new PdfPCell(new PdfPCell(new Paragraph(grosspay, FontFactory.GetFont("arial", 8))));
                        tabl4cell11.HorizontalAlignment = Element.ALIGN_CENTER;
                        tabl4cell11.PaddingBottom = 7;
                        tabl4cell11.BorderWidth = 0;
                        table4.AddCell(tabl4cell11);

                        PdfPCell tabl4cell12 = new PdfPCell(new PdfPCell(new Paragraph(ytdGrossPay, FontFactory.GetFont("arial", 8))));
                        tabl4cell12.HorizontalAlignment = Element.ALIGN_RIGHT;
                        tabl4cell12.PaddingBottom = 7;
                        tabl4cell12.BorderWidth = 0;
                        table4.AddCell(tabl4cell12);
                        //


                        GDB = new GetDatabase();
                        GDB.ClientName = prms.CN;
                        SelfieRegistration2Controller.GetDB2(GDB);
                        // FOR INCOME FIELDS
                        // INCOME UNDER EARNINGS
                        con = new Connection();
                        con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID });
                        con.myparameters.Add(new myParameters { ParameterName = "@payoutDate", mytype = SqlDbType.VarChar, Value = prms.PayoutDate });
                        DataTable dt6 = con.GetDataTable("sp_Earnings_Payslip");


                        GDB = new GetDatabase();
                        GDB.ClientName = prms.CN;
                        SelfieRegistration2Controller.GetDB2(GDB);
                        con = new Connection();
                        con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID });
                        con.myparameters.Add(new myParameters { ParameterName = "@year", mytype = SqlDbType.VarChar, Value = prms.ytd_year });
                        con.myparameters.Add(new myParameters { ParameterName = "@payoutDate", mytype = SqlDbType.VarChar, Value = prms.PayoutDate });
                        DataTable dt7 = con.GetDataTable("sp_Earnings_YTD_Payslip");

                        // HEADER
                        PdfPCell tabl4cell13 = new PdfPCell(new PdfPCell(new Paragraph("INCOME", FontFactory.GetFont("arial", 8, Font.UNDERLINE)))) { Border = PdfPCell.NO_BORDER, HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT };
                        tabl4cell13.Colspan = 4;
                        tabl4cell13.PaddingTop = 10;
                        tabl4cell13.PaddingBottom = 7;
                        tabl4cell13.BorderWidth = 0;
                        tabl4cell13.BackgroundColor = BaseColor.LIGHT_GRAY;
                        table4.AddCell(tabl4cell13);

                        if (dt6.Rows.Count > 0)
                        {
                            string desc = "", amount = "", ytdAmount = "";
                            for (int i = 0; i < dt6.Rows.Count; i++)
                            {
                                desc = dt6.Rows[i]["EarningsDescription"].ToString();
                                amount = dt6.Rows[i]["EarningsAmount"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt6.Rows[i]["EarningsAmount"].ToString()));

                                for (int ii = 0; ii < dt7.Rows.Count; ii++)
                                {
                                    if (desc == dt7.Rows[ii]["EarningsDescription"].ToString())
                                    {
                                        ytdAmount = dt7.Rows[ii]["EarningsAmount"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt7.Rows[ii]["EarningsAmount"].ToString()));
                                    }
                                }


                                PdfPCell tabl4cel13a = new PdfPCell(new PdfPCell(new Paragraph(desc, FontFactory.GetFont("arial", 8))));
                                tabl4cel13a.PaddingBottom = 7;
                                tabl4cel13a.BorderWidth = 0;
                                tabl4cel13a.BackgroundColor = BaseColor.LIGHT_GRAY;
                                table4.AddCell(tabl4cel13a);

                                PdfPCell tabl4cel13b = new PdfPCell(new PdfPCell(new Paragraph("", FontFactory.GetFont("arial", 8))));
                                tabl4cel13b.HorizontalAlignment = Element.ALIGN_RIGHT;
                                tabl4cel13b.PaddingBottom = 7;
                                tabl4cel13b.BorderWidth = 0;
                                tabl4cel13b.BackgroundColor = BaseColor.LIGHT_GRAY;
                                table4.AddCell(tabl4cel13b);

                                PdfPCell tabl4cel13c = new PdfPCell(new PdfPCell(new Paragraph(amount, FontFactory.GetFont("arial", 8))));
                                tabl4cel13c.HorizontalAlignment = Element.ALIGN_CENTER;
                                tabl4cel13c.PaddingBottom = 7;
                                tabl4cel13c.BorderWidth = 0;
                                tabl4cel13c.BackgroundColor = BaseColor.LIGHT_GRAY;
                                table4.AddCell(tabl4cel13c);

                                PdfPCell tabl4cel13d = new PdfPCell(new PdfPCell(new Paragraph(ytdAmount, FontFactory.GetFont("arial", 8))));
                                tabl4cel13d.HorizontalAlignment = Element.ALIGN_RIGHT;
                                tabl4cel13d.PaddingBottom = 7;
                                tabl4cel13d.BorderWidth = 0;
                                tabl4cel13d.BackgroundColor = BaseColor.LIGHT_GRAY;
                                table4.AddCell(tabl4cel13d);
                            }
                        }


                        GDB = new GetDatabase();
                        GDB.ClientName = prms.CN;
                        SelfieRegistration2Controller.GetDB2(GDB);
                        // INCOME UNDER OVERTIME
                        con = new Connection();
                        con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID });
                        con.myparameters.Add(new myParameters { ParameterName = "@payoutDate", mytype = SqlDbType.VarChar, Value = prms.PayoutDate });
                        DataTable dt8 = con.GetDataTable("sp_GetOvertimeInfo_Payslip");


                        GDB = new GetDatabase();
                        GDB.ClientName = prms.CN;
                        SelfieRegistration2Controller.GetDB2(GDB);
                        con = new Connection();
                        con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID });
                        con.myparameters.Add(new myParameters { ParameterName = "@year", mytype = SqlDbType.VarChar, Value = prms.ytd_year });
                        con.myparameters.Add(new myParameters { ParameterName = "@payoutDate", mytype = SqlDbType.VarChar, Value = prms.PayoutDate });
                        DataTable dt9 = con.GetDataTable("sp_GetOvertimeInfo_YTD_Payslip");

                        if (dt8.Rows.Count > 0)
                        {
                            string OTDesc = "", OTHours = "", OTAmount = "", OT_YTDAmount = "";

                            for (int i = 0; i < dt8.Rows.Count; i++)
                            {
                                OTDesc = dt8.Rows[i]["overtimedescription"].ToString();
                                OTHours = dt8.Rows[i]["overtimehours"].ToString();
                                OTAmount = dt8.Rows[i]["overtimeamount"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt8.Rows[i]["overtimeamount"].ToString()));
                                OT_YTDAmount = dt9.Rows[i]["OvertimeAmount"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt9.Rows[i]["OvertimeAmount"].ToString()));

                                PdfPCell OT_tabl4cel13a = new PdfPCell(new PdfPCell(new Paragraph(OTDesc, FontFactory.GetFont("arial", 8))));
                                OT_tabl4cel13a.PaddingBottom = 7;
                                OT_tabl4cel13a.BorderWidth = 0;
                                OT_tabl4cel13a.BackgroundColor = BaseColor.LIGHT_GRAY;
                                table4.AddCell(OT_tabl4cel13a);

                                PdfPCell OT_tabl4cel13b = new PdfPCell(new PdfPCell(new Paragraph(OTHours, FontFactory.GetFont("arial", 8))));
                                OT_tabl4cel13b.HorizontalAlignment = Element.ALIGN_RIGHT;
                                OT_tabl4cel13b.PaddingBottom = 7;
                                OT_tabl4cel13b.BorderWidth = 0;
                                OT_tabl4cel13b.BackgroundColor = BaseColor.LIGHT_GRAY;
                                table4.AddCell(OT_tabl4cel13b);

                                PdfPCell OT_tabl4cel13c = new PdfPCell(new PdfPCell(new Paragraph(OTAmount, FontFactory.GetFont("arial", 8))));
                                OT_tabl4cel13c.HorizontalAlignment = Element.ALIGN_CENTER;
                                OT_tabl4cel13c.PaddingBottom = 7;
                                OT_tabl4cel13c.BorderWidth = 0;
                                OT_tabl4cel13c.BackgroundColor = BaseColor.LIGHT_GRAY;
                                table4.AddCell(OT_tabl4cel13c);

                                PdfPCell OT_tabl4cel13d = new PdfPCell(new PdfPCell(new Paragraph(OT_YTDAmount, FontFactory.GetFont("arial", 8))));
                                OT_tabl4cel13d.HorizontalAlignment = Element.ALIGN_RIGHT;
                                OT_tabl4cel13d.PaddingBottom = 7;
                                OT_tabl4cel13d.BorderWidth = 0;
                                OT_tabl4cel13d.BackgroundColor = BaseColor.LIGHT_GRAY;
                                table4.AddCell(OT_tabl4cel13d);
                            }
                        }
                        // DEDUCTIONS FROM LOG INFO
                        if (dt16.Rows.Count > 0)
                        {
                            if (dt17.Rows[0]["absencesAmount"].ToString() != "0" && dt17.Rows[0]["absencesAmount"].ToString() != "0.0000")
                            {
                                Absences_Hours = dt16.Rows[0]["Absences"].ToString();
                                Absences_Amount = dt16.Rows[0]["AbsentAmount"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt16.Rows[0]["AbsentAmount"].ToString()));
                                Absences_YTD_Amount = dt17.Rows[0]["absencesAmount"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt17.Rows[0]["absencesAmount"].ToString()));

                                PdfPCell Absences_tabl4cel14a = new PdfPCell(new PdfPCell(new Paragraph(AbsencesDesc, FontFactory.GetFont("arial", 8))));
                                Absences_tabl4cel14a.PaddingBottom = 7;
                                Absences_tabl4cel14a.BorderWidth = 0;
                                table4.AddCell(Absences_tabl4cel14a);

                                PdfPCell Absences_tabl4cel14b = new PdfPCell(new PdfPCell(new Paragraph(Absences_Hours, FontFactory.GetFont("arial", 8))));
                                Absences_tabl4cel14b.HorizontalAlignment = Element.ALIGN_RIGHT;
                                Absences_tabl4cel14b.PaddingBottom = 7;
                                Absences_tabl4cel14b.BorderWidth = 0;
                                table4.AddCell(Absences_tabl4cel14b);

                                PdfPCell Absences_tabl4cel14c = new PdfPCell(new PdfPCell(new Paragraph(Absences_Amount, FontFactory.GetFont("arial", 8))));
                                Absences_tabl4cel14c.HorizontalAlignment = Element.ALIGN_CENTER;
                                Absences_tabl4cel14c.PaddingBottom = 7;
                                Absences_tabl4cel14c.BorderWidth = 0;
                                table4.AddCell(Absences_tabl4cel14c);

                                PdfPCell Absences_tabl4cel14d = new PdfPCell(new PdfPCell(new Paragraph(Absences_YTD_Amount, FontFactory.GetFont("arial", 8))));
                                Absences_tabl4cel14d.HorizontalAlignment = Element.ALIGN_RIGHT;
                                Absences_tabl4cel14d.PaddingBottom = 7;
                                Absences_tabl4cel14d.BorderWidth = 0;
                                table4.AddCell(Absences_tabl4cel14d);
                            }

                            if (dt17.Rows[0]["tardinessAmount"].ToString() != "0" && dt17.Rows[0]["tardinessAmount"].ToString() != "0.0000")
                            {
                                Tardi_Hours = dt16.Rows[0]["Tardiness"].ToString();
                                Tardi_Amount = dt16.Rows[0]["TardinessAmount"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt16.Rows[0]["TardinessAmount"].ToString()));
                                Tardi_YTD_Amount = dt17.Rows[0]["tardinessAmount"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt17.Rows[0]["tardinessAmount"].ToString()));

                                PdfPCell Tardi_tabl4cel14a = new PdfPCell(new PdfPCell(new Paragraph(TardiDesc, FontFactory.GetFont("arial", 8))));
                                Tardi_tabl4cel14a.PaddingBottom = 7;
                                Tardi_tabl4cel14a.BorderWidth = 0;
                                table4.AddCell(Tardi_tabl4cel14a);

                                PdfPCell Tardi_tabl4cel14b = new PdfPCell(new PdfPCell(new Paragraph(Tardi_Hours, FontFactory.GetFont("arial", 8))));
                                Tardi_tabl4cel14b.HorizontalAlignment = Element.ALIGN_RIGHT;
                                Tardi_tabl4cel14b.PaddingBottom = 7;
                                Tardi_tabl4cel14b.BorderWidth = 0;
                                table4.AddCell(Tardi_tabl4cel14b);

                                PdfPCell Tardi_tabl4cel14c = new PdfPCell(new PdfPCell(new Paragraph(Tardi_Amount, FontFactory.GetFont("arial", 8))));
                                Tardi_tabl4cel14c.HorizontalAlignment = Element.ALIGN_CENTER;
                                Tardi_tabl4cel14c.PaddingBottom = 7;
                                Tardi_tabl4cel14c.BorderWidth = 0;
                                table4.AddCell(Tardi_tabl4cel14c);

                                PdfPCell Tardi_tabl4cel14d = new PdfPCell(new PdfPCell(new Paragraph(Tardi_YTD_Amount, FontFactory.GetFont("arial", 8))));
                                Tardi_tabl4cel14d.HorizontalAlignment = Element.ALIGN_RIGHT;
                                Tardi_tabl4cel14d.PaddingBottom = 7;
                                Tardi_tabl4cel14d.BorderWidth = 0;
                                table4.AddCell(Tardi_tabl4cel14d);
                            }

                            if (dt17.Rows[0]["undertimeAmount"].ToString() != "0" && dt17.Rows[0]["undertimeAmount"].ToString() != "0.0000")
                            {
                                Undertime_Hours = dt16.Rows[0]["undertime"].ToString();
                                Undertime_Amount = dt16.Rows[0]["undertimeAmount"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt16.Rows[0]["undertimeAmount"].ToString()));
                                Undertime_YTD_Amount = dt17.Rows[0]["undertimeAmount"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt17.Rows[0]["undertimeAmount"].ToString()));

                                PdfPCell Undertime_tabl4cel14a = new PdfPCell(new PdfPCell(new Paragraph(UndertimeDesc, FontFactory.GetFont("arial", 8))));
                                Undertime_tabl4cel14a.PaddingBottom = 7;
                                Undertime_tabl4cel14a.BorderWidth = 0;
                                table4.AddCell(Undertime_tabl4cel14a);

                                PdfPCell Undertime_tabl4cel14b = new PdfPCell(new PdfPCell(new Paragraph(Undertime_Hours, FontFactory.GetFont("arial", 8))));
                                Undertime_tabl4cel14b.HorizontalAlignment = Element.ALIGN_RIGHT;
                                Undertime_tabl4cel14b.PaddingBottom = 7;
                                Undertime_tabl4cel14b.BorderWidth = 0;
                                table4.AddCell(Undertime_tabl4cel14b);

                                PdfPCell Undertime_tabl4cel14c = new PdfPCell(new PdfPCell(new Paragraph(Undertime_Amount, FontFactory.GetFont("arial", 8))));
                                Undertime_tabl4cel14c.HorizontalAlignment = Element.ALIGN_CENTER;
                                Undertime_tabl4cel14c.PaddingBottom = 7;
                                Undertime_tabl4cel14c.BorderWidth = 0;
                                table4.AddCell(Undertime_tabl4cel14c);

                                PdfPCell Undertime_tabl4cel14d = new PdfPCell(new PdfPCell(new Paragraph(Undertime_YTD_Amount, FontFactory.GetFont("arial", 8))));
                                Undertime_tabl4cel14d.HorizontalAlignment = Element.ALIGN_RIGHT;
                                Undertime_tabl4cel14d.PaddingBottom = 7;
                                Undertime_tabl4cel14d.BorderWidth = 0;
                                table4.AddCell(Undertime_tabl4cel14d);
                            }

                            if (dt17.Rows[0]["LWOPAmount"].ToString() != "0" && !string.IsNullOrWhiteSpace(dt17.Rows[0]["LWOPAmount"].ToString()) && dt17.Rows[0]["LWOPAmount"].ToString() != "0.0000" && dt17.Rows[0]["LWOPAmount"].ToString() != "0.00")
                            {
                                LWOP_Hours = dt16.Rows[0]["LWOP"].ToString();
                                LWOP_Amount = dt16.Rows[0]["LWOPAmount"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt16.Rows[0]["LWOPAmount"].ToString()));
                                LWOP_YTD_Amount = dt17.Rows[0]["LWOPAmount"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt17.Rows[0]["LWOPAmount"].ToString()));

                                PdfPCell LWOP_tabl4cel14a = new PdfPCell(new PdfPCell(new Paragraph(LWOPDesc, FontFactory.GetFont("arial", 8))));
                                LWOP_tabl4cel14a.PaddingBottom = 7;
                                LWOP_tabl4cel14a.BorderWidth = 0;
                                table4.AddCell(LWOP_tabl4cel14a);

                                PdfPCell LWOP_tabl4cel14b = new PdfPCell(new PdfPCell(new Paragraph(LWOP_Hours, FontFactory.GetFont("arial", 8))));
                                LWOP_tabl4cel14b.HorizontalAlignment = Element.ALIGN_RIGHT;
                                LWOP_tabl4cel14b.PaddingBottom = 7;
                                LWOP_tabl4cel14b.BorderWidth = 0;
                                table4.AddCell(LWOP_tabl4cel14b);

                                PdfPCell LWOP_tabl4cel14c = new PdfPCell(new PdfPCell(new Paragraph(LWOP_Amount, FontFactory.GetFont("arial", 8))));
                                LWOP_tabl4cel14c.HorizontalAlignment = Element.ALIGN_CENTER;
                                LWOP_tabl4cel14c.PaddingBottom = 7;
                                LWOP_tabl4cel14c.BorderWidth = 0;
                                table4.AddCell(LWOP_tabl4cel14c);

                                PdfPCell LWOP_tabl4cel14d = new PdfPCell(new PdfPCell(new Paragraph(LWOP_YTD_Amount, FontFactory.GetFont("arial", 8))));
                                LWOP_tabl4cel14d.HorizontalAlignment = Element.ALIGN_RIGHT;
                                LWOP_tabl4cel14d.PaddingBottom = 7;
                                LWOP_tabl4cel14d.BorderWidth = 0;
                                table4.AddCell(LWOP_tabl4cel14d);
                            }
                        }
                        // END

                        // FOR DEDUCTIONS FIELDS
                        // HEADER
                        PdfPCell tabl4cell14 = new PdfPCell(new PdfPCell(new Paragraph("CONTRIBUTIONS", FontFactory.GetFont("arial", 8, Font.UNDERLINE)))) { Border = PdfPCell.NO_BORDER, HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT };
                        tabl4cell14.Colspan = 4;
                        tabl4cell14.PaddingTop = 10;
                        tabl4cell14.PaddingBottom = 7;
                        tabl4cell14.BorderWidth = 0;
                        table4.AddCell(tabl4cell14);


                        GDB = new GetDatabase();
                        GDB.ClientName = prms.CN;
                        SelfieRegistration2Controller.GetDB2(GDB);
                        // DEDUCTION FOR GOV CONTRIBUTIONS
                        // FOR SSS
                        con = new Connection();
                        con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID });
                        con.myparameters.Add(new myParameters { ParameterName = "@payoutDate", mytype = SqlDbType.VarChar, Value = prms.PayoutDate });
                        DataTable dt10 = con.GetDataTable("sp_GetGovernmentAmounts_SSS_Payslip");


                        GDB = new GetDatabase();
                        GDB.ClientName = prms.CN;
                        SelfieRegistration2Controller.GetDB2(GDB);
                        con = new Connection();
                        con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID });
                        con.myparameters.Add(new myParameters { ParameterName = "@year", mytype = SqlDbType.VarChar, Value = prms.ytd_year });
                        con.myparameters.Add(new myParameters { ParameterName = "@payoutDate", mytype = SqlDbType.VarChar, Value = prms.PayoutDate });
                        DataTable dt11 = con.GetDataTable("sp_GetGovernmentAmounts_SSS_YTD_Payslip");

                        if (dt10.Rows.Count > 0)
                        {
                            string SSSDesc = "SSS", SSSAmount = "", SSS_YTD_Amount = "";

                            SSSAmount = dt10.Rows[0]["SSS"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt10.Rows[0]["SSS"].ToString()));
                            SSS_YTD_Amount = dt11.Rows[0]["SSS"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt11.Rows[0]["SSS"].ToString()));

                            PdfPCell SSS_tabl4cel14a = new PdfPCell(new PdfPCell(new Paragraph(SSSDesc, FontFactory.GetFont("arial", 8))));
                            SSS_tabl4cel14a.PaddingBottom = 7;
                            SSS_tabl4cel14a.BorderWidth = 0;
                            table4.AddCell(SSS_tabl4cel14a);

                            PdfPCell SSS_tabl4cel14b = new PdfPCell(new PdfPCell(new Paragraph("", FontFactory.GetFont("arial", 8))));
                            SSS_tabl4cel14b.HorizontalAlignment = Element.ALIGN_RIGHT;
                            SSS_tabl4cel14b.PaddingBottom = 7;
                            SSS_tabl4cel14b.BorderWidth = 0;
                            table4.AddCell(SSS_tabl4cel14b);

                            PdfPCell SSS_tabl4cel14c = new PdfPCell(new PdfPCell(new Paragraph(SSSAmount, FontFactory.GetFont("arial", 8))));
                            SSS_tabl4cel14c.HorizontalAlignment = Element.ALIGN_CENTER;
                            SSS_tabl4cel14c.PaddingBottom = 7;
                            SSS_tabl4cel14c.BorderWidth = 0;
                            table4.AddCell(SSS_tabl4cel14c);

                            PdfPCell SSS_tabl4cel14d = new PdfPCell(new PdfPCell(new Paragraph(SSS_YTD_Amount, FontFactory.GetFont("arial", 8))));
                            SSS_tabl4cel14d.HorizontalAlignment = Element.ALIGN_RIGHT;
                            SSS_tabl4cel14d.PaddingBottom = 7;
                            SSS_tabl4cel14d.BorderWidth = 0;
                            table4.AddCell(SSS_tabl4cel14d);
                        }


                        GDB = new GetDatabase();
                        GDB.ClientName = prms.CN;
                        SelfieRegistration2Controller.GetDB2(GDB);
                        // FOR PHILHEALTH
                        con = new Connection();
                        con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID });
                        con.myparameters.Add(new myParameters { ParameterName = "@payoutDate", mytype = SqlDbType.VarChar, Value = prms.PayoutDate });
                        DataTable dt12 = con.GetDataTable("sp_GetGovernmentAmounts_Philhealth_Payslip");



                        GDB = new GetDatabase();
                        GDB.ClientName = prms.CN;
                        SelfieRegistration2Controller.GetDB2(GDB);
                        con = new Connection();
                        con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID });
                        con.myparameters.Add(new myParameters { ParameterName = "@year", mytype = SqlDbType.VarChar, Value = prms.ytd_year });
                        con.myparameters.Add(new myParameters { ParameterName = "@payoutDate", mytype = SqlDbType.VarChar, Value = prms.PayoutDate });
                        DataTable dt13 = con.GetDataTable("sp_GetGovernmentAmounts_Philhealth_YTD_Payslip");

                        if (dt12.Rows.Count > 0)
                        {
                            string PHealthDesc = "Philhealth", PHealthAmount = "", PHealth_YTD_Amount = "";

                            PHealthAmount = dt12.Rows[0]["Philhealth"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt12.Rows[0]["Philhealth"].ToString()));
                            PHealth_YTD_Amount = dt13.Rows[0]["philhealth"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt13.Rows[0]["philhealth"].ToString()));

                            PdfPCell PHLHEALTH_tabl4cel14a = new PdfPCell(new PdfPCell(new Paragraph(PHealthDesc, FontFactory.GetFont("arial", 8))));
                            PHLHEALTH_tabl4cel14a.PaddingBottom = 7;
                            PHLHEALTH_tabl4cel14a.BorderWidth = 0;
                            table4.AddCell(PHLHEALTH_tabl4cel14a);

                            PdfPCell PHLHEALTH_tabl4cel14b = new PdfPCell(new PdfPCell(new Paragraph("", FontFactory.GetFont("arial", 8))));
                            PHLHEALTH_tabl4cel14b.HorizontalAlignment = Element.ALIGN_RIGHT;
                            PHLHEALTH_tabl4cel14b.PaddingBottom = 7;
                            PHLHEALTH_tabl4cel14b.BorderWidth = 0;
                            table4.AddCell(PHLHEALTH_tabl4cel14b);

                            PdfPCell PHLHEALTH_tabl4cel14c = new PdfPCell(new PdfPCell(new Paragraph(PHealthAmount, FontFactory.GetFont("arial", 8))));
                            PHLHEALTH_tabl4cel14c.HorizontalAlignment = Element.ALIGN_CENTER;
                            PHLHEALTH_tabl4cel14c.PaddingBottom = 7;
                            PHLHEALTH_tabl4cel14c.BorderWidth = 0;
                            table4.AddCell(PHLHEALTH_tabl4cel14c);

                            PdfPCell PHLHEALTH_tabl4cel14d = new PdfPCell(new PdfPCell(new Paragraph(PHealth_YTD_Amount, FontFactory.GetFont("arial", 8))));
                            PHLHEALTH_tabl4cel14d.HorizontalAlignment = Element.ALIGN_RIGHT;
                            PHLHEALTH_tabl4cel14d.PaddingBottom = 7;
                            PHLHEALTH_tabl4cel14d.BorderWidth = 0;
                            table4.AddCell(PHLHEALTH_tabl4cel14d);
                        }


                        GDB = new GetDatabase();
                        GDB.ClientName = prms.CN;
                        SelfieRegistration2Controller.GetDB2(GDB);
                        // FOR HDMF
                        con = new Connection();
                        con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID });
                        con.myparameters.Add(new myParameters { ParameterName = "@payoutDate", mytype = SqlDbType.VarChar, Value = prms.PayoutDate });
                        DataTable dt14 = con.GetDataTable("sp_GetGovernmentAmounts_Pagibig_Payslip");


                        GDB = new GetDatabase();
                        GDB.ClientName = prms.CN;
                        SelfieRegistration2Controller.GetDB2(GDB);
                        con = new Connection();
                        con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID });
                        con.myparameters.Add(new myParameters { ParameterName = "@year", mytype = SqlDbType.VarChar, Value = prms.ytd_year });
                        con.myparameters.Add(new myParameters { ParameterName = "@payoutDate", mytype = SqlDbType.VarChar, Value = prms.PayoutDate });
                        DataTable dt15 = con.GetDataTable("sp_GetGovernmentAmounts_Pagibig_YTD_Payslip");

                        if (dt14.Rows.Count > 0)
                        {
                            string HDMFDesc = "HDMF", HDMFAmount = "", HDMF_YTD_Amount = "";
                            HDMFAmount = dt14.Rows[0]["HDMF"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt14.Rows[0]["HDMF"].ToString()));
                            HDMF_YTD_Amount = dt15.Rows[0]["Pagibig_Employee"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt15.Rows[0]["Pagibig_Employee"].ToString()));

                            PdfPCell HDMF_tabl4cel14a = new PdfPCell(new PdfPCell(new Paragraph(HDMFDesc, FontFactory.GetFont("arial", 8))));
                            HDMF_tabl4cel14a.PaddingBottom = 7;
                            HDMF_tabl4cel14a.BorderWidth = 0;
                            table4.AddCell(HDMF_tabl4cel14a);

                            PdfPCell HDMF_tabl4cel14b = new PdfPCell(new PdfPCell(new Paragraph("", FontFactory.GetFont("arial", 8))));
                            HDMF_tabl4cel14b.HorizontalAlignment = Element.ALIGN_RIGHT;
                            HDMF_tabl4cel14b.PaddingBottom = 7;
                            HDMF_tabl4cel14b.BorderWidth = 0;
                            table4.AddCell(HDMF_tabl4cel14b);

                            PdfPCell HDMF_tabl4cel14c = new PdfPCell(new PdfPCell(new Paragraph(HDMFAmount, FontFactory.GetFont("arial", 8))));
                            HDMF_tabl4cel14c.HorizontalAlignment = Element.ALIGN_CENTER;
                            HDMF_tabl4cel14c.PaddingBottom = 7;
                            HDMF_tabl4cel14c.BorderWidth = 0;
                            table4.AddCell(HDMF_tabl4cel14c);

                            PdfPCell HDMF_tabl4cel14d = new PdfPCell(new PdfPCell(new Paragraph(HDMF_YTD_Amount, FontFactory.GetFont("arial", 8))));
                            HDMF_tabl4cel14d.HorizontalAlignment = Element.ALIGN_RIGHT;
                            HDMF_tabl4cel14d.PaddingBottom = 7;
                            HDMF_tabl4cel14d.BorderWidth = 0;
                            table4.AddCell(HDMF_tabl4cel14d);
                        }




                        GDB = new GetDatabase();
                        GDB.ClientName = prms.CN;
                        SelfieRegistration2Controller.GetDB2(GDB);


                        // FOR LOANS FIELD
                        PdfPCell tabl4cell15 = new PdfPCell(new PdfPCell(new Paragraph("DEDUCTIONS", FontFactory.GetFont("arial", 8, Font.UNDERLINE))));
                        tabl4cell14.PaddingTop = 30;
                        tabl4cell15.PaddingBottom = 7;
                        tabl4cell15.BorderWidth = 0;
                        table4.AddCell(tabl4cell15);

                        //PdfPCell tabl4cell16 = new PdfPCell(new PdfPCell(new Paragraph("BALANCE LEFT", FontFactory.GetFont("arial", 8, Font.BOLD))));
                        PdfPCell tabl4cell16 = new PdfPCell(new PdfPCell(new Paragraph("", FontFactory.GetFont("arial", 8, Font.BOLD))));
                        tabl4cell16.HorizontalAlignment = Element.ALIGN_RIGHT;
                        tabl4cell16.PaddingBottom = 7;
                        tabl4cell14.PaddingTop = 20;
                        tabl4cell16.BorderWidth = 0;
                        table4.AddCell(tabl4cell16);

                        PdfPCell tabl4cell17 = new PdfPCell(new PdfPCell(new Paragraph("", FontFactory.GetFont("arial", 8, Font.BOLD))));
                        tabl4cell17.PaddingBottom = 7;
                        tabl4cell17.BorderWidth = 0;
                        table4.AddCell(tabl4cell17);

                        PdfPCell tabl4cell18 = new PdfPCell(new PdfPCell(new Paragraph("", FontFactory.GetFont("arial", 8, Font.BOLD))));
                        tabl4cell18.HorizontalAlignment = Element.ALIGN_RIGHT;
                        tabl4cell18.PaddingBottom = 7;
                        tabl4cell18.BorderWidth = 0;
                        table4.AddCell(tabl4cell18);


                        GDB = new GetDatabase();
                        GDB.ClientName = prms.CN;
                        SelfieRegistration2Controller.GetDB2(GDB);
                        con = new Connection();
                        con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID });
                        con.myparameters.Add(new myParameters { ParameterName = "@payoutDate", mytype = SqlDbType.VarChar, Value = prms.PayoutDate });
                        DataTable dt20 = con.GetDataTable("sp_GetLoans_Payslip");


                        GDB = new GetDatabase();
                        GDB.ClientName = prms.CN;
                        SelfieRegistration2Controller.GetDB2(GDB);
                        con = new Connection();
                        con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID });
                        con.myparameters.Add(new myParameters { ParameterName = "@year", mytype = SqlDbType.VarChar, Value = prms.ytd_year });
                        con.myparameters.Add(new myParameters { ParameterName = "@payoutDate", mytype = SqlDbType.VarChar, Value = prms.PayoutDate });
                        DataTable dt21 = con.GetDataTable("sp_GetLoans_YTD_Payslip");

                        if (dt20.Rows.Count > 0)
                        {
                            string LoanName = "", Amortization_Amount = "", Amortization_YTD_Amount = "";

                            for (int i = 0; i < dt20.Rows.Count; i++)
                            {
                                LoanName = dt20.Rows[i]["LoanName"].ToString();
                                Amortization_Amount = dt20.Rows[i]["AmortizationAmount"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt20.Rows[i]["AmortizationAmount"].ToString()));
                                Amortization_YTD_Amount = dt21.Rows[i]["AmortizationAmount"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt21.Rows[i]["AmortizationAmount"].ToString()));

                                PdfPCell Loan_tabl4cel14a = new PdfPCell(new PdfPCell(new Paragraph(LoanName, FontFactory.GetFont("arial", 8))));
                                Loan_tabl4cel14a.PaddingBottom = 7;
                                Loan_tabl4cel14a.BorderWidth = 0;
                                table4.AddCell(Loan_tabl4cel14a);

                                PdfPCell Loan_tabl4cel14b = new PdfPCell(new PdfPCell(new Paragraph("", FontFactory.GetFont("arial", 8))));
                                Loan_tabl4cel14b.HorizontalAlignment = Element.ALIGN_RIGHT;
                                Loan_tabl4cel14b.PaddingBottom = 7;
                                Loan_tabl4cel14b.BorderWidth = 0;
                                table4.AddCell(Loan_tabl4cel14b);

                                PdfPCell Loan_tabl4cel14c = new PdfPCell(new PdfPCell(new Paragraph(Amortization_Amount, FontFactory.GetFont("arial", 8))));
                                Loan_tabl4cel14c.HorizontalAlignment = Element.ALIGN_CENTER;
                                Loan_tabl4cel14c.PaddingBottom = 7;
                                Loan_tabl4cel14c.BorderWidth = 0;
                                table4.AddCell(Loan_tabl4cel14c);

                                PdfPCell Loan_tabl4cel14d = new PdfPCell(new PdfPCell(new Paragraph(Amortization_YTD_Amount, FontFactory.GetFont("arial", 8))));
                                Loan_tabl4cel14d.HorizontalAlignment = Element.ALIGN_RIGHT;
                                Loan_tabl4cel14d.PaddingBottom = 7;
                                Loan_tabl4cel14d.BorderWidth = 0;
                                table4.AddCell(Loan_tabl4cel14d);
                            }
                        }
                        //
                        // OTHER DEDUCTIONS
                        con = new Connection();
                        con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID });
                        con.myparameters.Add(new myParameters { ParameterName = "@payoutDate", mytype = SqlDbType.VarChar, Value = prms.PayoutDate });
                        DataTable dt18 = con.GetDataTable("sp_GetDeductions_Payslip");


                        GDB = new GetDatabase();
                        GDB.ClientName = prms.CN;
                        SelfieRegistration2Controller.GetDB2(GDB);
                        con = new Connection();
                        con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID });
                        con.myparameters.Add(new myParameters { ParameterName = "@year", mytype = SqlDbType.VarChar, Value = prms.ytd_year });
                        con.myparameters.Add(new myParameters { ParameterName = "@payoutDate", mytype = SqlDbType.VarChar, Value = prms.PayoutDate });
                        DataTable dt19 = con.GetDataTable("sp_GetDeductions_YTD_Payslip");

                        if (dt18.Rows.Count > 0)
                        {
                            for (int i = 0; i < dt18.Rows.Count; i++)
                            {
                                string DeductionDesc = "", Deduction_Amount = "", Deduction_YTD_Amount = "";

                                DeductionDesc = dt18.Rows[i]["deductiondescription"].ToString();
                                Deduction_Amount = dt18.Rows[i]["deductionamount"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt18.Rows[i]["deductionamount"].ToString()));
                                Deduction_YTD_Amount = dt19.Rows[i]["deductionAmount"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt19.Rows[i]["deductionAmount"].ToString()));

                                PdfPCell OtherDeduction_tabl4cel14a = new PdfPCell(new PdfPCell(new Paragraph(DeductionDesc, FontFactory.GetFont("arial", 8))));
                                OtherDeduction_tabl4cel14a.PaddingBottom = 7;
                                OtherDeduction_tabl4cel14a.BorderWidth = 0;
                                table4.AddCell(OtherDeduction_tabl4cel14a);

                                PdfPCell OtherDeduction_tabl4cel14b = new PdfPCell(new PdfPCell(new Paragraph("", FontFactory.GetFont("arial", 8))));
                                OtherDeduction_tabl4cel14b.HorizontalAlignment = Element.ALIGN_RIGHT;
                                OtherDeduction_tabl4cel14b.PaddingBottom = 7;
                                OtherDeduction_tabl4cel14b.BorderWidth = 0;
                                table4.AddCell(OtherDeduction_tabl4cel14b);

                                PdfPCell OtherDeduction_tabl4cel14c = new PdfPCell(new PdfPCell(new Paragraph(Deduction_Amount, FontFactory.GetFont("arial", 8))));
                                OtherDeduction_tabl4cel14c.HorizontalAlignment = Element.ALIGN_CENTER;
                                OtherDeduction_tabl4cel14c.PaddingBottom = 7;
                                OtherDeduction_tabl4cel14c.BorderWidth = 0;
                                table4.AddCell(OtherDeduction_tabl4cel14c);

                                PdfPCell OtherDeduction_tabl4cel14d = new PdfPCell(new PdfPCell(new Paragraph(Deduction_YTD_Amount, FontFactory.GetFont("arial", 8))));
                                OtherDeduction_tabl4cel14d.HorizontalAlignment = Element.ALIGN_RIGHT;
                                OtherDeduction_tabl4cel14d.PaddingBottom = 7;
                                OtherDeduction_tabl4cel14d.BorderWidth = 0;
                                table4.AddCell(OtherDeduction_tabl4cel14d);
                            }
                        }
                        //
                        //
                        PdfPCell tabl4cell19 = new PdfPCell(new PdfPCell(new Paragraph("WithHolding Tax", FontFactory.GetFont("arial", 8))));
                        tabl4cell19.PaddingBottom = 7;
                        tabl4cell19.BorderWidth = 0;
                        tabl4cell19.BackgroundColor = BaseColor.LIGHT_GRAY;
                        table4.AddCell(tabl4cell19);

                        PdfPCell tabl4cell20 = new PdfPCell(new PdfPCell(new Paragraph("", FontFactory.GetFont("arial", 8))));
                        tabl4cell20.HorizontalAlignment = Element.ALIGN_RIGHT;
                        tabl4cell20.PaddingBottom = 7;
                        tabl4cell20.BorderWidth = 0;
                        tabl4cell20.BackgroundColor = BaseColor.LIGHT_GRAY;
                        table4.AddCell(tabl4cell20);

                        PdfPCell tabl4cell21 = new PdfPCell(new PdfPCell(new Paragraph(whtax, FontFactory.GetFont("arial", 8))));
                        tabl4cell21.HorizontalAlignment = Element.ALIGN_CENTER;
                        tabl4cell21.PaddingBottom = 7;
                        tabl4cell21.BorderWidth = 0;
                        tabl4cell21.BackgroundColor = BaseColor.LIGHT_GRAY;
                        table4.AddCell(tabl4cell21);

                        PdfPCell tabl4cell22 = new PdfPCell(new PdfPCell(new Paragraph(ytdWhTax, FontFactory.GetFont("arial", 8))));
                        tabl4cell22.HorizontalAlignment = Element.ALIGN_RIGHT;
                        tabl4cell22.PaddingBottom = 7;
                        tabl4cell22.BorderWidth = 0;
                        tabl4cell22.BackgroundColor = BaseColor.LIGHT_GRAY;
                        table4.AddCell(tabl4cell22);
                        //

                        //
                        PdfPCell tabl4cell23 = new PdfPCell(new PdfPCell(new Paragraph("Net Pay", FontFactory.GetFont("arial", 8))));
                        tabl4cell23.PaddingBottom = 7;
                        tabl4cell23.BorderWidth = 0;
                        table4.AddCell(tabl4cell23);

                        PdfPCell tabl4cell24 = new PdfPCell(new PdfPCell(new Paragraph("", FontFactory.GetFont("arial", 8))));
                        tabl4cell24.HorizontalAlignment = Element.ALIGN_RIGHT;
                        tabl4cell24.PaddingBottom = 7;
                        tabl4cell24.BorderWidth = 0;
                        table4.AddCell(tabl4cell24);

                        PdfPCell tabl4cell25 = new PdfPCell(new PdfPCell(new Paragraph(netpay, FontFactory.GetFont("arial", 8))));
                        tabl4cell25.HorizontalAlignment = Element.ALIGN_CENTER;
                        tabl4cell25.PaddingBottom = 7;
                        tabl4cell25.BorderWidth = 0;
                        table4.AddCell(tabl4cell25);

                        PdfPCell tabl4cell26 = new PdfPCell(new PdfPCell(new Paragraph(ytdNetPay, FontFactory.GetFont("arial", 8))));
                        tabl4cell26.HorizontalAlignment = Element.ALIGN_RIGHT;
                        tabl4cell26.PaddingBottom = 7;
                        tabl4cell26.BorderWidth = 0;
                        table4.AddCell(tabl4cell26);
                        //

                        //
                        PdfPCell tabl4cell27 = new PdfPCell(new PdfPCell(new Paragraph("", FontFactory.GetFont("arial", 8, Font.UNDERLINE)))) { Border = PdfPCell.NO_BORDER, HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT };
                        tabl4cell27.Colspan = 4;
                        tabl4cell27.PaddingTop = 10;
                        tabl4cell27.PaddingBottom = 7;
                        tabl4cell27.BorderWidth = 0;
                        table4.AddCell(tabl4cell27);
                        //
                        table2.AddCell(table4);


                        PdfPCell cell22 = new PdfPCell() { Border = PdfPCell.NO_BORDER };
                        cell22.Colspan = 2;


                        table1.AddCell(cell11);
                        table1.AddCell(cell12);
                        table2.AddCell(cell22);
                        document.Add(table0);
                        document.Add(table1);
                        document.Add(table2);
                        document.Close();

                        archive.CreateEntryFromFile(dataDir + filename, Path.GetFileName(dataDir + filename));
                    }

                }
                catch (Exception e)
                {
                    return e.ToString();
                }

            }
            string onlineDIR = "http://ws.durusthr.com/ilm_ws_live/pdf/";
            return onlineDIR + zipFileName;
        }

        [HttpPost]
        [Route("generatepdfV2")]
        public string generatepdfsV2(generatePDF prms)
        {
            try
            {
                GetDatabase GDB = new GetDatabase();
                GDB.ClientName = prms.CN;
                SelfieRegistration2Controller.GetDB2(GDB);

                // DATA VARIABLE DECLARATION
                string startdate = "", enddate = "";
                string payday = "";
                string fullname = "", empid = "", datejoined = "", position = "", department = "", TIN = "", SSS = "", PHILHEALTH = "", PAGIBIG = "";
                string basicpay = "", grosspay = "", netpay = "", whtax = "", ytdBasicPay = "", ytdGrossPay = "", ytdNetPay = "", ytdWhTax = "";
                string HourlyRate = "", HoursWorked = "", AbsencesDesc = "Absences", TardiDesc = "Tardiness", UndertimeDesc = "Undertime", LWOPDesc = "LWOP", Monthly = "";
                string Absences_Hours = "", Tardi_Hours = "", Undertime_Hours = "", LWOP_Hours = "";
                string Absences_Amount = "", Tardi_Amount = "", Undertime_Amount = "", LWOP_Amount = "";
                string Absences_YTD_Amount = "", Tardi_YTD_Amount = "", Undertime_YTD_Amount = "", LWOP_YTD_Amount = "";
                string CompanyName = "";
                string filename = "";

                // FOR COMPANY NAME

                Connection conn = new Connection();
                conn.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.VarChar, Value = prms.EmpID });
                DataTable dtbl = conn.GetDataTable("sp_get_CompanyName_MobileDynamicPayslip");
                foreach (DataRow item in dtbl.Rows)
                {
                    CompanyName = item["TradingName"].ToString();
                }
                // FOR PAY PERIOD FIELD

                GDB = new GetDatabase();
                GDB.ClientName = prms.CN;
                SelfieRegistration2Controller.GetDB2(GDB);
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID });
                con.myparameters.Add(new myParameters { ParameterName = "@payoutDate", mytype = SqlDbType.VarChar, Value = prms.PayoutDate });
                DataTable DT = con.GetDataTable("sp_GetPayPeriod_Payslip");
                foreach (DataRow item in DT.Rows)
                {
                    startdate = item["StartDate"].ToString();
                    enddate = item["EndDate"].ToString();
                }


                GDB = new GetDatabase();
                GDB.ClientName = prms.CN;
                SelfieRegistration2Controller.GetDB2(GDB);
                // FOR PAY DATE FIELD
                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID });
                DataTable DT1 = con.GetDataTable("sp_GetPayoutSchemeList");
                foreach (DataRow item in DT1.Rows)
                {
                    payday = item["PayoutDate"].ToString();
                }

                GDB = new GetDatabase();
                GDB.ClientName = prms.CN;
                SelfieRegistration2Controller.GetDB2(GDB);
                // EMPLOYEE DETAILS FIELD
                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID });
                DataTable dt2 = con.GetDataTable("sp_GetEmployeeInfo_Payslip");
                foreach (DataRow item in dt2.Rows)
                {
                    fullname = item["EmpName"].ToString();
                    empid = item["EmpID"].ToString();
                    datejoined = item["DateJoined"].ToString();
                    department = item["BusinessUnit"].ToString();
                    position = item["JobDesc"].ToString();
                }

                GDB = new GetDatabase();
                GDB.ClientName = prms.CN;
                SelfieRegistration2Controller.GetDB2(GDB);
                // FOR GOVERNMENT ID FIELDS
                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID });
                DataTable dt3 = con.GetDataTable("sp_GetGovIDs_Payslip");
                foreach (DataRow item in dt3.Rows)
                {
                    if (item["IDTypes"].ToString() == "SSS")
                    {
                        SSS = item["IDCode"].ToString();
                    }
                    else if (item["IDTypes"].ToString() == "Philhealth")
                    {
                        PHILHEALTH = item["IDCode"].ToString();
                    }
                    else if (item["IDTypes"].ToString() == "Pag-Ibig")
                    {
                        PAGIBIG = item["IDCode"].ToString();
                    }
                    else
                    {
                        TIN = item["IDCode"].ToString();
                    }
                }

                GDB = new GetDatabase();
                GDB.ClientName = prms.CN;
                SelfieRegistration2Controller.GetDB2(GDB);
                // FOR GROSS PAY, BASIC PAY AND NET PAY FIELDS
                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID });
                con.myparameters.Add(new myParameters { ParameterName = "@payoutDate", mytype = SqlDbType.VarChar, Value = prms.PayoutDate });
                DataTable dt4 = con.GetDataTable("sp_GetPay_Payslip");
                foreach (DataRow item in dt4.Rows)
                {
                    basicpay = item["Basic Pay"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(item["Basic Pay"].ToString()));
                    grosspay = item["Gross Pay"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(item["Gross Pay"].ToString()));
                    netpay = item["Net Pay"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(item["Net Pay"].ToString()));
                    whtax = item["WithHolding Tax"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(item["WithHolding Tax"].ToString()));

                    //basicpay = String.Format("{0:n2}", decimal.Parse(basicpay));
                }
                // END


                GDB = new GetDatabase();
                GDB.ClientName = prms.CN;
                SelfieRegistration2Controller.GetDB2(GDB);
                // YTD GROSS PAY, BASIC PAY AND NET PAY FIELDS
                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID });
                con.myparameters.Add(new myParameters { ParameterName = "@year", mytype = SqlDbType.VarChar, Value = prms.ytd_year });
                con.myparameters.Add(new myParameters { ParameterName = "@payoutDate", mytype = SqlDbType.VarChar, Value = prms.PayoutDate });
                DataTable dt5 = con.GetDataTable("sp_GetPay_YTD_Payslip");
                foreach (DataRow item in dt5.Rows)
                {
                    ytdBasicPay = item["basicpay"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(item["basicpay"].ToString()));
                    ytdGrossPay = item["grosspay"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(item["grosspay"].ToString()));
                    ytdWhTax = item["whtax"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(item["whtax"].ToString()));
                    ytdNetPay = item["netpay"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(item["netpay"].ToString()));
                }

                GDB = new GetDatabase();
                GDB.ClientName = prms.CN;
                SelfieRegistration2Controller.GetDB2(GDB);
                // FOR LOGS INFO
                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID });
                con.myparameters.Add(new myParameters { ParameterName = "@payoutDate", mytype = SqlDbType.VarChar, Value = prms.PayoutDate });
                DataTable dt16 = con.GetDataTable("sp_GetLogInfo_Payslip");

                if (dt16.Rows.Count > 0)
                {
                    HourlyRate = dt16.Rows[0]["HourlyRate"] == DBNull.Value || dt16.Rows[0]["HourlyRate"].ToString() == "" ? "" : String.Format("{0:n2}", decimal.Parse(dt16.Rows[0]["HourlyRate"].ToString()));
                    HoursWorked = dt16.Rows[0]["HoursWorked"] == DBNull.Value || dt16.Rows[0]["HoursWorked"].ToString() == "" ? "" : String.Format("{0:n2}", decimal.Parse(dt16.Rows[0]["HoursWorked"].ToString()));
                    //Monthly = dt16.Rows[0]["Monthly"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt16.Rows[0]["Monthly"].ToString()));
                }

                // GET MONTHLY SALARY

                GDB = new GetDatabase();
                GDB.ClientName = prms.CN;
                SelfieRegistration2Controller.GetDB2(GDB);
                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@empid", mytype = SqlDbType.VarChar, Value = prms.EmpID });
                con.myparameters.Add(new myParameters { ParameterName = "@payoutDate", mytype = SqlDbType.VarChar, Value = prms.PayoutDate });
                DataTable dtMontlySalary = con.GetDataTable("sp_GetMonthlySalary_Payslip");

                if (dtMontlySalary.Rows.Count > 0)
                {
                    Monthly = dtMontlySalary.Rows[0]["Salary"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dtMontlySalary.Rows[0]["Salary"].ToString()));
                }

                GDB = new GetDatabase();
                GDB.ClientName = prms.CN;
                SelfieRegistration2Controller.GetDB2(GDB);
                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID });
                con.myparameters.Add(new myParameters { ParameterName = "@year", mytype = SqlDbType.VarChar, Value = prms.ytd_year });
                con.myparameters.Add(new myParameters { ParameterName = "@payoutDate", mytype = SqlDbType.VarChar, Value = prms.PayoutDate });
                DataTable dt17 = con.GetDataTable("sp_GetLogInfo_YTD_Payslip");
                //  string timebuild = DateTime.Now.ToString("hhmmtt");
                filename = "payslip-" + prms.EmpID + "-" + prms.PayoutDate + ".pdf";
                System.IO.FileStream fs = new FileStream(HttpContext.Current.Server.MapPath("pdf") + "\\" + "payslip-" + prms.EmpID + "-" + prms.PayoutDate + ".pdf", FileMode.Create);
                //System.IO.FileStream fb = new FileStream(HttpContext.Current.Server.MapPath("pdf") + "\\" + "ilm.png", FileMode.Open);
                string imagepath = HttpContext.Current.Server.MapPath("Images");
                Document document = new Document(PageSize.A4, 25, 25, 30, 30);
                PdfWriter writer = PdfWriter.GetInstance(document, fs);

                // Set Password
                DataTable dt_payslip_password = DynamicPayslip_GetPassword(prms.EmpID);

                if(dt_payslip_password != null && dt_payslip_password.Rows.Count > 0)
                    if (dt_payslip_password.Rows[0][0].ToString() != null && dt_payslip_password.Rows[0][1].ToString().ToLower() == "active")
                        writer.SetEncryption(true, dt_payslip_password.Rows[0][0].ToString(), "Illimitado", PdfWriter.ALLOW_SCREENREADERS);

                //PdfWriter writering = PdfWriter.GetInstance(document, fb);
                document.Open();

                PdfPTable table0 = new PdfPTable(1);

                PdfPCell cell0 = new PdfPCell(new iTextSharp.text.Phrase(CompanyName, FontFactory.GetFont("Arial", 12, Font.BOLD))) { Border = 0, HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT, VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE };
                cell0.PaddingTop = 10;
                //PdfPCell cell01 = new PdfPCell(new iTextSharp.text.Phrase("Lima Technology Center  Lipa City, Batangas Philippines 4217", FontFactory.GetFont("Arial", 11, Font.NORMAL))) { Border = 0, HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT, VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE };
                //cell01.PaddingBottom = 7;

                //Image logoimage = Image.GetInstance(Path.Combine(imagepath, "yokohamalogo.jpg"));
                //logoimage.ScaleAbsolute(100, 40);
                //logoimage.SetAbsolutePosition(100, 30);

                //PdfPCell LogoCell = new PdfPCell(logoimage, true);

                table0.AddCell(cell0);
                ////table0.AddCell(LogoCell);
                //table0.AddCell(cell01);
                //document.Add(LogoCell);


                PdfPTable table1 = new PdfPTable(2);

                /*var titleFont = new Font(Font.FontFamily.UNDEFINED, 12);
                var informationDetails = new Font(Font.FontFamily.UNDEFINED, 12);*/

                PdfPCell cell11 = new PdfPCell(new iTextSharp.text.Phrase("PAY PERIOD\n" + startdate + " - " + enddate)) { Border = PdfPCell.BOTTOM_BORDER | PdfPCell.TOP_BORDER, HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER, VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE };
                PdfPCell cell12 = new PdfPCell(new iTextSharp.text.Phrase("PAY DAY\n" + prms.PayoutDate)) { Border = PdfPCell.BOTTOM_BORDER | PdfPCell.TOP_BORDER, HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER, VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE };
                cell11.PaddingTop = 10;
                cell12.PaddingTop = 10;
                cell11.PaddingBottom = 7;
                cell12.PaddingBottom = 7;
                /* cell12.AddElement(new Paragraph("PAY DAY", titleFont));
                cell12.AddElement(new Paragraph("-", informationDetails));*/

                PdfPTable table2 = new PdfPTable(2);
                float[] widths = new float[] { 100f, 200f };
                float[] heights = new float[] { 300f };
                table2.SetWidths(widths);
                //table2.DefaultCell.FixedHeight = 500f;
                table2.DefaultCell.Border = Rectangle.NO_BORDER;
                //PdfPCell cell21 = new PdfPCell(new Paragraph("Danilo Vicente Sulla Jr")) { Border = PdfPCell.RIGHT_BORDER, HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER, VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE };
                //cell21.PaddingTop = 20;

                PdfPTable table3 = new PdfPTable(2);
                table3.TotalWidth = 100;
                table3.DefaultCell.FixedHeight = 600f;
                table3.DefaultCell.Border = Rectangle.RIGHT_BORDER;

                PdfPCell tabl3cell0 = new PdfPCell(new PdfPCell(new Paragraph(fullname, FontFactory.GetFont("Arial", 12, Font.BOLD)))) { Border = PdfPCell.NO_BORDER, HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER, VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE };
                tabl3cell0.Colspan = 2;
                tabl3cell0.PaddingTop = 20;
                tabl3cell0.PaddingBottom = 20;
                tabl3cell0.BorderWidth = 0;
                tabl3cell0.BorderWidthRight = 1;
                table3.AddCell(tabl3cell0);


                PdfPCell tabl3cell1 = new PdfPCell(new PdfPCell(new Paragraph("EmpID", FontFactory.GetFont("arial", 8))));
                tabl3cell1.PaddingBottom = 7;
                tabl3cell1.BorderWidth = 0;
                tabl3cell1.BorderWidthBottom = 1;
                table3.AddCell(tabl3cell1);

                PdfPCell tabl3cell2 = new PdfPCell(new PdfPCell(new Paragraph(empid, FontFactory.GetFont("arial", 8, Font.BOLD))));
                tabl3cell2.HorizontalAlignment = Element.ALIGN_RIGHT;
                tabl3cell2.PaddingBottom = 7;
                tabl3cell2.BorderWidth = 0;
                tabl3cell2.BorderWidthBottom = 1;
                tabl3cell2.BorderWidthRight = 1;
                table3.AddCell(tabl3cell2);

                PdfPCell tabl3cell3 = new PdfPCell(new PdfPCell(new Paragraph("Tax Category", FontFactory.GetFont("arial", 8))));
                tabl3cell3.PaddingBottom = 7;
                tabl3cell3.BorderWidth = 0;
                tabl3cell3.BorderWidthBottom = 1;
                table3.AddCell(tabl3cell3);

                PdfPCell tabl3cell4 = new PdfPCell(new PdfPCell(new Paragraph("--", FontFactory.GetFont("arial", 8, Font.BOLD))));
                tabl3cell4.HorizontalAlignment = Element.ALIGN_RIGHT;
                tabl3cell4.PaddingBottom = 7;
                tabl3cell4.BorderWidth = 0;
                tabl3cell4.BorderWidthBottom = 1;
                tabl3cell4.BorderWidthRight = 1;
                table3.AddCell(tabl3cell4);

                PdfPCell tabl3cell5 = new PdfPCell(new PdfPCell(new Paragraph("Hourly Rate", FontFactory.GetFont("arial", 8))));
                tabl3cell5.PaddingBottom = 7;
                tabl3cell5.BorderWidth = 0;
                tabl3cell5.BorderWidthBottom = 1;
                table3.AddCell(tabl3cell5);

                PdfPCell tabl3cell6 = new PdfPCell(new PdfPCell(new Paragraph(HourlyRate, FontFactory.GetFont("arial", 8, Font.BOLD))));
                tabl3cell6.HorizontalAlignment = Element.ALIGN_RIGHT;
                tabl3cell6.PaddingBottom = 7;
                tabl3cell6.BorderWidth = 0;
                tabl3cell6.BorderWidthBottom = 1;
                tabl3cell6.BorderWidthRight = 1;
                table3.AddCell(tabl3cell6);

                PdfPCell tabl3cell7 = new PdfPCell(new PdfPCell(new Paragraph("Regular Hours", FontFactory.GetFont("arial", 8))));
                tabl3cell7.PaddingBottom = 7;
                tabl3cell7.BorderWidth = 0;
                tabl3cell7.BorderWidthBottom = 1;
                table3.AddCell(tabl3cell7);

                PdfPCell tabl3cell8 = new PdfPCell(new PdfPCell(new Paragraph(HoursWorked, FontFactory.GetFont("arial", 8, Font.BOLD))));
                tabl3cell8.HorizontalAlignment = Element.ALIGN_RIGHT;
                tabl3cell8.PaddingBottom = 7;
                tabl3cell8.BorderWidth = 0;
                tabl3cell8.BorderWidthBottom = 1;
                tabl3cell8.BorderWidthRight = 1;
                table3.AddCell(tabl3cell8);

                PdfPCell tabl3cell9 = new PdfPCell(new PdfPCell(new Paragraph(getSalaryScheme(Monthly.ToString()) + " Salary", FontFactory.GetFont("arial", 8))));
                tabl3cell9.PaddingBottom = 7;
                tabl3cell9.BorderWidth = 0;
                tabl3cell9.BorderWidthBottom = 1;
                table3.AddCell(tabl3cell9);

                PdfPCell tabl3cell10 = new PdfPCell(new PdfPCell(new Paragraph(Monthly, FontFactory.GetFont("arial", 8, Font.BOLD))));
                tabl3cell10.HorizontalAlignment = Element.ALIGN_RIGHT;
                tabl3cell10.PaddingBottom = 7;
                tabl3cell10.BorderWidth = 0;
                tabl3cell10.BorderWidthBottom = 1;
                tabl3cell10.BorderWidthRight = 1;
                table3.AddCell(tabl3cell10);

                PdfPCell tabl3cell11 = new PdfPCell(new PdfPCell(new Paragraph("Date Joined", FontFactory.GetFont("arial", 8))));
                tabl3cell11.PaddingBottom = 7;
                tabl3cell11.BorderWidth = 0;
                tabl3cell11.BorderWidthBottom = 1;
                table3.AddCell(tabl3cell11);

                PdfPCell tabl3cell12 = new PdfPCell(new PdfPCell(new Paragraph(datejoined, FontFactory.GetFont("arial", 8, Font.BOLD))));
                tabl3cell12.HorizontalAlignment = Element.ALIGN_RIGHT;
                tabl3cell12.PaddingBottom = 7;
                tabl3cell12.BorderWidth = 0;
                tabl3cell12.BorderWidthBottom = 1;
                tabl3cell12.BorderWidthRight = 1;
                table3.AddCell(tabl3cell12);

                PdfPCell tabl3cell13 = new PdfPCell(new PdfPCell(new Paragraph("Position", FontFactory.GetFont("arial", 8))));
                tabl3cell13.PaddingBottom = 7;
                tabl3cell13.BorderWidth = 0;
                tabl3cell13.BorderWidthBottom = 1;
                table3.AddCell(tabl3cell13);

                PdfPCell tabl3cell14 = new PdfPCell(new PdfPCell(new Paragraph(position, FontFactory.GetFont("arial", 8, Font.BOLD))));
                tabl3cell14.HorizontalAlignment = Element.ALIGN_RIGHT;
                tabl3cell14.PaddingBottom = 7;
                tabl3cell14.BorderWidth = 0;
                tabl3cell14.BorderWidthBottom = 1;
                tabl3cell14.BorderWidthRight = 1;
                table3.AddCell(tabl3cell14);

                PdfPCell tabl3cell15 = new PdfPCell(new PdfPCell(new Paragraph("Department", FontFactory.GetFont("arial", 8))));
                tabl3cell15.PaddingBottom = 7;
                tabl3cell15.BorderWidth = 0;
                tabl3cell15.BorderWidthBottom = 1;
                table3.AddCell(tabl3cell15);

                PdfPCell tabl3cell16 = new PdfPCell(new PdfPCell(new Paragraph(department, FontFactory.GetFont("arial", 8, Font.BOLD))));
                tabl3cell16.HorizontalAlignment = Element.ALIGN_RIGHT;
                tabl3cell16.PaddingBottom = 7;
                tabl3cell16.BorderWidth = 0;
                tabl3cell16.BorderWidthBottom = 1;
                tabl3cell16.BorderWidthRight = 1;
                table3.AddCell(tabl3cell16);

                PdfPCell tabl3cell17 = new PdfPCell(new PdfPCell(new Paragraph("Philhealth", FontFactory.GetFont("arial", 8))));
                tabl3cell17.PaddingBottom = 7;
                tabl3cell17.BorderWidth = 0;
                tabl3cell17.BorderWidthBottom = 1;
                table3.AddCell(tabl3cell17);

                PdfPCell tabl3cell18 = new PdfPCell(new PdfPCell(new Paragraph(PHILHEALTH, FontFactory.GetFont("arial", 8, Font.BOLD))));
                tabl3cell18.HorizontalAlignment = Element.ALIGN_RIGHT;
                tabl3cell18.PaddingBottom = 7;
                tabl3cell18.BorderWidth = 0;
                tabl3cell18.BorderWidthBottom = 1;
                tabl3cell18.BorderWidthRight = 1;
                table3.AddCell(tabl3cell18);

                PdfPCell tabl3cell19 = new PdfPCell(new PdfPCell(new Paragraph("Pag-Ibig", FontFactory.GetFont("arial", 8))));
                tabl3cell19.PaddingBottom = 7;
                tabl3cell19.BorderWidth = 0;
                tabl3cell19.BorderWidthBottom = 1;
                table3.AddCell(tabl3cell19);

                PdfPCell tabl3cell20 = new PdfPCell(new PdfPCell(new Paragraph(PAGIBIG, FontFactory.GetFont("arial", 8, Font.BOLD))));
                tabl3cell20.HorizontalAlignment = Element.ALIGN_RIGHT;
                tabl3cell20.PaddingBottom = 7;
                tabl3cell20.BorderWidth = 0;
                tabl3cell20.BorderWidthBottom = 1;
                tabl3cell20.BorderWidthRight = 1;
                table3.AddCell(tabl3cell20);

                PdfPCell tabl3cell21 = new PdfPCell(new PdfPCell(new Paragraph("TIN", FontFactory.GetFont("arial", 8))));
                tabl3cell21.PaddingBottom = 7;
                tabl3cell21.BorderWidth = 0;
                tabl3cell21.BorderWidthBottom = 1;

                table3.AddCell(tabl3cell21);

                PdfPCell tabl3cell22 = new PdfPCell(new PdfPCell(new Paragraph(TIN, FontFactory.GetFont("arial", 8, Font.BOLD))));
                tabl3cell22.HorizontalAlignment = Element.ALIGN_RIGHT;
                tabl3cell22.PaddingBottom = 7;
                tabl3cell22.BorderWidth = 0;
                tabl3cell22.BorderWidthBottom = 1;
                tabl3cell22.BorderWidthRight = 1;
                table3.AddCell(tabl3cell22);

                PdfPCell tabl3cell23 = new PdfPCell(new PdfPCell(new Paragraph("SSS", FontFactory.GetFont("arial", 8))));
                tabl3cell23.PaddingBottom = 7;
                tabl3cell23.BorderWidth = 0;
                table3.AddCell(tabl3cell23);

                PdfPCell tabl3cell24 = new PdfPCell(new PdfPCell(new Paragraph(SSS, FontFactory.GetFont("arial", 8, Font.BOLD))));
                tabl3cell24.HorizontalAlignment = Element.ALIGN_RIGHT;
                tabl3cell24.PaddingBottom = 7;
                tabl3cell24.BorderWidth = 0;
                tabl3cell24.BorderWidthRight = 1;
                table3.AddCell(tabl3cell24);


                table2.AddCell(table3);

                //
                PdfPTable table4 = new PdfPTable(4);
                table4.TotalWidth = 100;
                table4.DefaultCell.Border = Rectangle.NO_BORDER;

                PdfPCell tabl4cell1 = new PdfPCell(new PdfPCell(new Paragraph("", FontFactory.GetFont("arial", 8))));
                tabl4cell1.PaddingBottom = 7;
                tabl4cell1.BorderWidth = 0;
                table4.AddCell(tabl4cell1);

                PdfPCell tabl4cell2 = new PdfPCell(new PdfPCell(new Paragraph("HOURS", FontFactory.GetFont("arial", 8, Font.BOLD))));
                tabl4cell2.HorizontalAlignment = Element.ALIGN_RIGHT;
                tabl4cell2.PaddingBottom = 7;
                tabl4cell2.BorderWidth = 0;
                table4.AddCell(tabl4cell2);

                PdfPCell tabl4cell3 = new PdfPCell(new PdfPCell(new Paragraph("CURRENT (PHP)", FontFactory.GetFont("arial", 8, Font.BOLD))));
                tabl4cell3.HorizontalAlignment = Element.ALIGN_CENTER;
                tabl4cell3.PaddingBottom = 7;
                tabl4cell3.BorderWidth = 0;
                table4.AddCell(tabl4cell3);

                PdfPCell tabl4cell4 = new PdfPCell(new PdfPCell(new Paragraph("YEAR TO DATE(PHP)\n January 1 to present ", FontFactory.GetFont("arial", 8, Font.BOLD))));
                tabl4cell4.HorizontalAlignment = Element.ALIGN_RIGHT;
                tabl4cell4.PaddingBottom = 7;
                tabl4cell4.BorderWidth = 0;
                table4.AddCell(tabl4cell4);


                //
                PdfPCell tabl4cell5 = new PdfPCell(new PdfPCell(new Paragraph("Basic Pay", FontFactory.GetFont("arial", 8))));
                tabl4cell5.PaddingBottom = 7;
                tabl4cell5.BorderWidth = 0;
                table4.AddCell(tabl4cell5);

                PdfPCell tabl4cell6 = new PdfPCell(new PdfPCell(new Paragraph("", FontFactory.GetFont("arial", 8))));
                tabl4cell6.HorizontalAlignment = Element.ALIGN_RIGHT;
                tabl4cell6.PaddingBottom = 7;
                tabl4cell6.BorderWidth = 0;
                table4.AddCell(tabl4cell6);

                PdfPCell tabl4cell7 = new PdfPCell(new PdfPCell(new Paragraph(basicpay, FontFactory.GetFont("arial", 8))));
                tabl4cell7.HorizontalAlignment = Element.ALIGN_CENTER;
                tabl4cell7.PaddingBottom = 7;
                tabl4cell7.BorderWidth = 0;
                table4.AddCell(tabl4cell7);

                PdfPCell tabl4cell8 = new PdfPCell(new PdfPCell(new Paragraph(ytdBasicPay, FontFactory.GetFont("arial", 8))));
                tabl4cell8.HorizontalAlignment = Element.ALIGN_RIGHT;
                tabl4cell8.PaddingBottom = 7;
                tabl4cell8.BorderWidth = 0;
                table4.AddCell(tabl4cell8);
                //

                //
                PdfPCell tabl4cell9 = new PdfPCell(new PdfPCell(new Paragraph("Gross Pay", FontFactory.GetFont("arial", 8))));
                tabl4cell9.PaddingBottom = 7;
                tabl4cell9.BorderWidth = 0;
                table4.AddCell(tabl4cell9);

                PdfPCell tabl4cell10 = new PdfPCell(new PdfPCell(new Paragraph("", FontFactory.GetFont("arial", 8))));
                tabl4cell10.HorizontalAlignment = Element.ALIGN_RIGHT;
                tabl4cell10.PaddingBottom = 7;
                tabl4cell10.BorderWidth = 0;
                table4.AddCell(tabl4cell10);

                PdfPCell tabl4cell11 = new PdfPCell(new PdfPCell(new Paragraph(grosspay, FontFactory.GetFont("arial", 8))));
                tabl4cell11.HorizontalAlignment = Element.ALIGN_CENTER;
                tabl4cell11.PaddingBottom = 7;
                tabl4cell11.BorderWidth = 0;
                table4.AddCell(tabl4cell11);

                PdfPCell tabl4cell12 = new PdfPCell(new PdfPCell(new Paragraph(ytdGrossPay, FontFactory.GetFont("arial", 8))));
                tabl4cell12.HorizontalAlignment = Element.ALIGN_RIGHT;
                tabl4cell12.PaddingBottom = 7;
                tabl4cell12.BorderWidth = 0;
                table4.AddCell(tabl4cell12);
                //


                GDB = new GetDatabase();
                GDB.ClientName = prms.CN;
                SelfieRegistration2Controller.GetDB2(GDB);
                // FOR INCOME FIELDS
                // INCOME UNDER EARNINGS
                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID });
                con.myparameters.Add(new myParameters { ParameterName = "@payoutDate", mytype = SqlDbType.VarChar, Value = prms.PayoutDate });
                DataTable dt6 = con.GetDataTable("sp_Earnings_Payslip");


                GDB = new GetDatabase();
                GDB.ClientName = prms.CN;
                SelfieRegistration2Controller.GetDB2(GDB);
                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID });
                con.myparameters.Add(new myParameters { ParameterName = "@year", mytype = SqlDbType.VarChar, Value = prms.ytd_year });
                con.myparameters.Add(new myParameters { ParameterName = "@payoutDate", mytype = SqlDbType.VarChar, Value = prms.PayoutDate });
                DataTable dt7 = con.GetDataTable("sp_Earnings_YTD_Payslip");

                // HEADER
                PdfPCell tabl4cell13 = new PdfPCell(new PdfPCell(new Paragraph("INCOME", FontFactory.GetFont("arial", 8, Font.UNDERLINE)))) { Border = PdfPCell.NO_BORDER, HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT };
                tabl4cell13.Colspan = 4;
                tabl4cell13.PaddingTop = 10;
                tabl4cell13.PaddingBottom = 7;
                tabl4cell13.BorderWidth = 0;
                tabl4cell13.BackgroundColor = BaseColor.LIGHT_GRAY;
                table4.AddCell(tabl4cell13);

                if (dt6.Rows.Count > 0)
                {
                    string desc = "", amount = "", ytdAmount = "";
                    for (int i = 0; i < dt6.Rows.Count; i++)
                    {
                        desc = dt6.Rows[i]["EarningsDescription"].ToString();
                        amount = dt6.Rows[i]["EarningsAmount"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt6.Rows[i]["EarningsAmount"].ToString()));

                        for (int ii = 0; ii < dt7.Rows.Count; ii++)
                        {
                            if (desc == dt7.Rows[ii]["EarningsDescription"].ToString())
                            {
                                ytdAmount = dt7.Rows[ii]["EarningsAmount"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt7.Rows[ii]["EarningsAmount"].ToString()));
                            }
                        }


                        PdfPCell tabl4cel13a = new PdfPCell(new PdfPCell(new Paragraph(desc, FontFactory.GetFont("arial", 8))));
                        tabl4cel13a.PaddingBottom = 7;
                        tabl4cel13a.BorderWidth = 0;
                        tabl4cel13a.BackgroundColor = BaseColor.LIGHT_GRAY;
                        table4.AddCell(tabl4cel13a);

                        PdfPCell tabl4cel13b = new PdfPCell(new PdfPCell(new Paragraph("", FontFactory.GetFont("arial", 8))));
                        tabl4cel13b.HorizontalAlignment = Element.ALIGN_RIGHT;
                        tabl4cel13b.PaddingBottom = 7;
                        tabl4cel13b.BorderWidth = 0;
                        tabl4cel13b.BackgroundColor = BaseColor.LIGHT_GRAY;
                        table4.AddCell(tabl4cel13b);

                        PdfPCell tabl4cel13c = new PdfPCell(new PdfPCell(new Paragraph(amount, FontFactory.GetFont("arial", 8))));
                        tabl4cel13c.HorizontalAlignment = Element.ALIGN_CENTER;
                        tabl4cel13c.PaddingBottom = 7;
                        tabl4cel13c.BorderWidth = 0;
                        tabl4cel13c.BackgroundColor = BaseColor.LIGHT_GRAY;
                        table4.AddCell(tabl4cel13c);

                        PdfPCell tabl4cel13d = new PdfPCell(new PdfPCell(new Paragraph(ytdAmount, FontFactory.GetFont("arial", 8))));
                        tabl4cel13d.HorizontalAlignment = Element.ALIGN_RIGHT;
                        tabl4cel13d.PaddingBottom = 7;
                        tabl4cel13d.BorderWidth = 0;
                        tabl4cel13d.BackgroundColor = BaseColor.LIGHT_GRAY;
                        table4.AddCell(tabl4cel13d);
                    }
                }


                GDB = new GetDatabase();
                GDB.ClientName = prms.CN;
                SelfieRegistration2Controller.GetDB2(GDB);
                // INCOME UNDER OVERTIME
                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID });
                con.myparameters.Add(new myParameters { ParameterName = "@payoutDate", mytype = SqlDbType.VarChar, Value = prms.PayoutDate });
                DataTable dt8 = con.GetDataTable("sp_GetOvertimeInfo_Payslip");


                GDB = new GetDatabase();
                GDB.ClientName = prms.CN;
                SelfieRegistration2Controller.GetDB2(GDB);
                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID });
                con.myparameters.Add(new myParameters { ParameterName = "@year", mytype = SqlDbType.VarChar, Value = prms.ytd_year });
                con.myparameters.Add(new myParameters { ParameterName = "@payoutDate", mytype = SqlDbType.VarChar, Value = prms.PayoutDate });
                DataTable dt9 = con.GetDataTable("sp_GetOvertimeInfo_YTD_Payslip");

                if (dt8.Rows.Count > 0)
                {
                    string OTDesc = "", OTHours = "", OTAmount = "", OT_YTDAmount = "";

                    for (int i = 0; i < dt8.Rows.Count; i++)
                    {
                        OTDesc = dt8.Rows[i]["overtimedescription"].ToString();
                        OTHours = dt8.Rows[i]["overtimehours"].ToString();
                        OTAmount = dt8.Rows[i]["overtimeamount"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt8.Rows[i]["overtimeamount"].ToString()));
                        OT_YTDAmount = dt9.Rows[i]["OvertimeAmount"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt9.Rows[i]["OvertimeAmount"].ToString()));

                        PdfPCell OT_tabl4cel13a = new PdfPCell(new PdfPCell(new Paragraph(OTDesc, FontFactory.GetFont("arial", 8))));
                        OT_tabl4cel13a.PaddingBottom = 7;
                        OT_tabl4cel13a.BorderWidth = 0;
                        OT_tabl4cel13a.BackgroundColor = BaseColor.LIGHT_GRAY;
                        table4.AddCell(OT_tabl4cel13a);

                        PdfPCell OT_tabl4cel13b = new PdfPCell(new PdfPCell(new Paragraph(OTHours, FontFactory.GetFont("arial", 8))));
                        OT_tabl4cel13b.HorizontalAlignment = Element.ALIGN_RIGHT;
                        OT_tabl4cel13b.PaddingBottom = 7;
                        OT_tabl4cel13b.BorderWidth = 0;
                        OT_tabl4cel13b.BackgroundColor = BaseColor.LIGHT_GRAY;
                        table4.AddCell(OT_tabl4cel13b);

                        PdfPCell OT_tabl4cel13c = new PdfPCell(new PdfPCell(new Paragraph(OTAmount, FontFactory.GetFont("arial", 8))));
                        OT_tabl4cel13c.HorizontalAlignment = Element.ALIGN_CENTER;
                        OT_tabl4cel13c.PaddingBottom = 7;
                        OT_tabl4cel13c.BorderWidth = 0;
                        OT_tabl4cel13c.BackgroundColor = BaseColor.LIGHT_GRAY;
                        table4.AddCell(OT_tabl4cel13c);

                        PdfPCell OT_tabl4cel13d = new PdfPCell(new PdfPCell(new Paragraph(OT_YTDAmount, FontFactory.GetFont("arial", 8))));
                        OT_tabl4cel13d.HorizontalAlignment = Element.ALIGN_RIGHT;
                        OT_tabl4cel13d.PaddingBottom = 7;
                        OT_tabl4cel13d.BorderWidth = 0;
                        OT_tabl4cel13d.BackgroundColor = BaseColor.LIGHT_GRAY;
                        table4.AddCell(OT_tabl4cel13d);
                    }
                }
                // DEDUCTIONS FROM LOG INFO
                if (dt16.Rows.Count > 0)
                {
                    if (dt17.Rows[0]["absencesAmount"].ToString() != "0" && dt17.Rows[0]["absencesAmount"].ToString() != "0.0000")
                    {
                        Absences_Hours = dt16.Rows[0]["Absences"].ToString();
                        Absences_Amount = dt16.Rows[0]["AbsentAmount"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt16.Rows[0]["AbsentAmount"].ToString()));
                        Absences_YTD_Amount = dt17.Rows[0]["absencesAmount"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt17.Rows[0]["absencesAmount"].ToString()));

                        PdfPCell Absences_tabl4cel14a = new PdfPCell(new PdfPCell(new Paragraph(AbsencesDesc, FontFactory.GetFont("arial", 8))));
                        Absences_tabl4cel14a.PaddingBottom = 7;
                        Absences_tabl4cel14a.BorderWidth = 0;
                        table4.AddCell(Absences_tabl4cel14a);

                        PdfPCell Absences_tabl4cel14b = new PdfPCell(new PdfPCell(new Paragraph(Absences_Hours, FontFactory.GetFont("arial", 8))));
                        Absences_tabl4cel14b.HorizontalAlignment = Element.ALIGN_RIGHT;
                        Absences_tabl4cel14b.PaddingBottom = 7;
                        Absences_tabl4cel14b.BorderWidth = 0;
                        table4.AddCell(Absences_tabl4cel14b);

                        PdfPCell Absences_tabl4cel14c = new PdfPCell(new PdfPCell(new Paragraph(Absences_Amount, FontFactory.GetFont("arial", 8))));
                        Absences_tabl4cel14c.HorizontalAlignment = Element.ALIGN_CENTER;
                        Absences_tabl4cel14c.PaddingBottom = 7;
                        Absences_tabl4cel14c.BorderWidth = 0;
                        table4.AddCell(Absences_tabl4cel14c);

                        PdfPCell Absences_tabl4cel14d = new PdfPCell(new PdfPCell(new Paragraph(Absences_YTD_Amount, FontFactory.GetFont("arial", 8))));
                        Absences_tabl4cel14d.HorizontalAlignment = Element.ALIGN_RIGHT;
                        Absences_tabl4cel14d.PaddingBottom = 7;
                        Absences_tabl4cel14d.BorderWidth = 0;
                        table4.AddCell(Absences_tabl4cel14d);
                    }

                    if (dt17.Rows[0]["tardinessAmount"].ToString() != "0" && dt17.Rows[0]["tardinessAmount"].ToString() != "0.0000")
                    {
                        Tardi_Hours = dt16.Rows[0]["Tardiness"].ToString();
                        Tardi_Amount = dt16.Rows[0]["TardinessAmount"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt16.Rows[0]["TardinessAmount"].ToString()));
                        Tardi_YTD_Amount = dt17.Rows[0]["tardinessAmount"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt17.Rows[0]["tardinessAmount"].ToString()));

                        PdfPCell Tardi_tabl4cel14a = new PdfPCell(new PdfPCell(new Paragraph(TardiDesc, FontFactory.GetFont("arial", 8))));
                        Tardi_tabl4cel14a.PaddingBottom = 7;
                        Tardi_tabl4cel14a.BorderWidth = 0;
                        table4.AddCell(Tardi_tabl4cel14a);

                        PdfPCell Tardi_tabl4cel14b = new PdfPCell(new PdfPCell(new Paragraph(Tardi_Hours, FontFactory.GetFont("arial", 8))));
                        Tardi_tabl4cel14b.HorizontalAlignment = Element.ALIGN_RIGHT;
                        Tardi_tabl4cel14b.PaddingBottom = 7;
                        Tardi_tabl4cel14b.BorderWidth = 0;
                        table4.AddCell(Tardi_tabl4cel14b);

                        PdfPCell Tardi_tabl4cel14c = new PdfPCell(new PdfPCell(new Paragraph(Tardi_Amount, FontFactory.GetFont("arial", 8))));
                        Tardi_tabl4cel14c.HorizontalAlignment = Element.ALIGN_CENTER;
                        Tardi_tabl4cel14c.PaddingBottom = 7;
                        Tardi_tabl4cel14c.BorderWidth = 0;
                        table4.AddCell(Tardi_tabl4cel14c);

                        PdfPCell Tardi_tabl4cel14d = new PdfPCell(new PdfPCell(new Paragraph(Tardi_YTD_Amount, FontFactory.GetFont("arial", 8))));
                        Tardi_tabl4cel14d.HorizontalAlignment = Element.ALIGN_RIGHT;
                        Tardi_tabl4cel14d.PaddingBottom = 7;
                        Tardi_tabl4cel14d.BorderWidth = 0;
                        table4.AddCell(Tardi_tabl4cel14d);
                    }

                    if (dt17.Rows[0]["undertimeAmount"].ToString() != "0" && dt17.Rows[0]["undertimeAmount"].ToString() != "0.0000")
                    {
                        Undertime_Hours = dt16.Rows[0]["undertime"].ToString();
                        Undertime_Amount = dt16.Rows[0]["undertimeAmount"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt16.Rows[0]["undertimeAmount"].ToString()));
                        Undertime_YTD_Amount = dt17.Rows[0]["undertimeAmount"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt17.Rows[0]["undertimeAmount"].ToString()));

                        PdfPCell Undertime_tabl4cel14a = new PdfPCell(new PdfPCell(new Paragraph(UndertimeDesc, FontFactory.GetFont("arial", 8))));
                        Undertime_tabl4cel14a.PaddingBottom = 7;
                        Undertime_tabl4cel14a.BorderWidth = 0;
                        table4.AddCell(Undertime_tabl4cel14a);

                        PdfPCell Undertime_tabl4cel14b = new PdfPCell(new PdfPCell(new Paragraph(Undertime_Hours, FontFactory.GetFont("arial", 8))));
                        Undertime_tabl4cel14b.HorizontalAlignment = Element.ALIGN_RIGHT;
                        Undertime_tabl4cel14b.PaddingBottom = 7;
                        Undertime_tabl4cel14b.BorderWidth = 0;
                        table4.AddCell(Undertime_tabl4cel14b);

                        PdfPCell Undertime_tabl4cel14c = new PdfPCell(new PdfPCell(new Paragraph(Undertime_Amount, FontFactory.GetFont("arial", 8))));
                        Undertime_tabl4cel14c.HorizontalAlignment = Element.ALIGN_CENTER;
                        Undertime_tabl4cel14c.PaddingBottom = 7;
                        Undertime_tabl4cel14c.BorderWidth = 0;
                        table4.AddCell(Undertime_tabl4cel14c);

                        PdfPCell Undertime_tabl4cel14d = new PdfPCell(new PdfPCell(new Paragraph(Undertime_YTD_Amount, FontFactory.GetFont("arial", 8))));
                        Undertime_tabl4cel14d.HorizontalAlignment = Element.ALIGN_RIGHT;
                        Undertime_tabl4cel14d.PaddingBottom = 7;
                        Undertime_tabl4cel14d.BorderWidth = 0;
                        table4.AddCell(Undertime_tabl4cel14d);
                    }

                    if (dt17.Rows[0]["LWOPAmount"].ToString() != "0" && !string.IsNullOrWhiteSpace(dt17.Rows[0]["LWOPAmount"].ToString()) && dt17.Rows[0]["LWOPAmount"].ToString() != "0.0000" && dt17.Rows[0]["LWOPAmount"].ToString() != "0.00")
                    {
                        LWOP_Hours = dt16.Rows[0]["LWOP"].ToString();
                        LWOP_Amount = dt16.Rows[0]["LWOPAmount"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt16.Rows[0]["LWOPAmount"].ToString()));
                        LWOP_YTD_Amount = dt17.Rows[0]["LWOPAmount"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt17.Rows[0]["LWOPAmount"].ToString()));

                        PdfPCell LWOP_tabl4cel14a = new PdfPCell(new PdfPCell(new Paragraph(LWOPDesc, FontFactory.GetFont("arial", 8))));
                        LWOP_tabl4cel14a.PaddingBottom = 7;
                        LWOP_tabl4cel14a.BorderWidth = 0;
                        table4.AddCell(LWOP_tabl4cel14a);

                        PdfPCell LWOP_tabl4cel14b = new PdfPCell(new PdfPCell(new Paragraph(LWOP_Hours, FontFactory.GetFont("arial", 8))));
                        LWOP_tabl4cel14b.HorizontalAlignment = Element.ALIGN_RIGHT;
                        LWOP_tabl4cel14b.PaddingBottom = 7;
                        LWOP_tabl4cel14b.BorderWidth = 0;
                        table4.AddCell(LWOP_tabl4cel14b);

                        PdfPCell LWOP_tabl4cel14c = new PdfPCell(new PdfPCell(new Paragraph(LWOP_Amount, FontFactory.GetFont("arial", 8))));
                        LWOP_tabl4cel14c.HorizontalAlignment = Element.ALIGN_CENTER;
                        LWOP_tabl4cel14c.PaddingBottom = 7;
                        LWOP_tabl4cel14c.BorderWidth = 0;
                        table4.AddCell(LWOP_tabl4cel14c);

                        PdfPCell LWOP_tabl4cel14d = new PdfPCell(new PdfPCell(new Paragraph(LWOP_YTD_Amount, FontFactory.GetFont("arial", 8))));
                        LWOP_tabl4cel14d.HorizontalAlignment = Element.ALIGN_RIGHT;
                        LWOP_tabl4cel14d.PaddingBottom = 7;
                        LWOP_tabl4cel14d.BorderWidth = 0;
                        table4.AddCell(LWOP_tabl4cel14d);
                    }
                }
                // END

                // FOR DEDUCTIONS FIELDS
                // HEADER
                PdfPCell tabl4cell14 = new PdfPCell(new PdfPCell(new Paragraph("CONTRIBUTIONS", FontFactory.GetFont("arial", 8, Font.UNDERLINE)))) { Border = PdfPCell.NO_BORDER, HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT };
                tabl4cell14.Colspan = 4;
                tabl4cell14.PaddingTop = 10;
                tabl4cell14.PaddingBottom = 7;
                tabl4cell14.BorderWidth = 0;
                table4.AddCell(tabl4cell14);


                GDB = new GetDatabase();
                GDB.ClientName = prms.CN;
                SelfieRegistration2Controller.GetDB2(GDB);
                // DEDUCTION FOR GOV CONTRIBUTIONS
                // FOR SSS
                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID });
                con.myparameters.Add(new myParameters { ParameterName = "@payoutDate", mytype = SqlDbType.VarChar, Value = prms.PayoutDate });
                DataTable dt10 = con.GetDataTable("sp_GetGovernmentAmounts_SSS_Payslip");


                GDB = new GetDatabase();
                GDB.ClientName = prms.CN;
                SelfieRegistration2Controller.GetDB2(GDB);
                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID });
                con.myparameters.Add(new myParameters { ParameterName = "@year", mytype = SqlDbType.VarChar, Value = prms.ytd_year });
                con.myparameters.Add(new myParameters { ParameterName = "@payoutDate", mytype = SqlDbType.VarChar, Value = prms.PayoutDate });
                DataTable dt11 = con.GetDataTable("sp_GetGovernmentAmounts_SSS_YTD_Payslip");

                if (dt10.Rows.Count > 0)
                {
                    string SSSDesc = "SSS", SSSAmount = "", SSS_YTD_Amount = "";

                    SSSAmount = dt10.Rows[0]["SSS"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt10.Rows[0]["SSS"].ToString()));
                    SSS_YTD_Amount = dt11.Rows[0]["SSS"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt11.Rows[0]["SSS"].ToString()));

                    PdfPCell SSS_tabl4cel14a = new PdfPCell(new PdfPCell(new Paragraph(SSSDesc, FontFactory.GetFont("arial", 8))));
                    SSS_tabl4cel14a.PaddingBottom = 7;
                    SSS_tabl4cel14a.BorderWidth = 0;
                    table4.AddCell(SSS_tabl4cel14a);

                    PdfPCell SSS_tabl4cel14b = new PdfPCell(new PdfPCell(new Paragraph("", FontFactory.GetFont("arial", 8))));
                    SSS_tabl4cel14b.HorizontalAlignment = Element.ALIGN_RIGHT;
                    SSS_tabl4cel14b.PaddingBottom = 7;
                    SSS_tabl4cel14b.BorderWidth = 0;
                    table4.AddCell(SSS_tabl4cel14b);

                    PdfPCell SSS_tabl4cel14c = new PdfPCell(new PdfPCell(new Paragraph(SSSAmount, FontFactory.GetFont("arial", 8))));
                    SSS_tabl4cel14c.HorizontalAlignment = Element.ALIGN_CENTER;
                    SSS_tabl4cel14c.PaddingBottom = 7;
                    SSS_tabl4cel14c.BorderWidth = 0;
                    table4.AddCell(SSS_tabl4cel14c);

                    PdfPCell SSS_tabl4cel14d = new PdfPCell(new PdfPCell(new Paragraph(SSS_YTD_Amount, FontFactory.GetFont("arial", 8))));
                    SSS_tabl4cel14d.HorizontalAlignment = Element.ALIGN_RIGHT;
                    SSS_tabl4cel14d.PaddingBottom = 7;
                    SSS_tabl4cel14d.BorderWidth = 0;
                    table4.AddCell(SSS_tabl4cel14d);
                }


                GDB = new GetDatabase();
                GDB.ClientName = prms.CN;
                SelfieRegistration2Controller.GetDB2(GDB);
                // FOR PHILHEALTH
                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID });
                con.myparameters.Add(new myParameters { ParameterName = "@payoutDate", mytype = SqlDbType.VarChar, Value = prms.PayoutDate });
                DataTable dt12 = con.GetDataTable("sp_GetGovernmentAmounts_Philhealth_Payslip");



                GDB = new GetDatabase();
                GDB.ClientName = prms.CN;
                SelfieRegistration2Controller.GetDB2(GDB);
                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID });
                con.myparameters.Add(new myParameters { ParameterName = "@year", mytype = SqlDbType.VarChar, Value = prms.ytd_year });
                con.myparameters.Add(new myParameters { ParameterName = "@payoutDate", mytype = SqlDbType.VarChar, Value = prms.PayoutDate });
                DataTable dt13 = con.GetDataTable("sp_GetGovernmentAmounts_Philhealth_YTD_Payslip");

                if (dt12.Rows.Count > 0)
                {
                    string PHealthDesc = "Philhealth", PHealthAmount = "", PHealth_YTD_Amount = "";

                    PHealthAmount = dt12.Rows[0]["Philhealth"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt12.Rows[0]["Philhealth"].ToString()));
                    PHealth_YTD_Amount = dt13.Rows[0]["philhealth"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt13.Rows[0]["philhealth"].ToString()));

                    PdfPCell PHLHEALTH_tabl4cel14a = new PdfPCell(new PdfPCell(new Paragraph(PHealthDesc, FontFactory.GetFont("arial", 8))));
                    PHLHEALTH_tabl4cel14a.PaddingBottom = 7;
                    PHLHEALTH_tabl4cel14a.BorderWidth = 0;
                    table4.AddCell(PHLHEALTH_tabl4cel14a);

                    PdfPCell PHLHEALTH_tabl4cel14b = new PdfPCell(new PdfPCell(new Paragraph("", FontFactory.GetFont("arial", 8))));
                    PHLHEALTH_tabl4cel14b.HorizontalAlignment = Element.ALIGN_RIGHT;
                    PHLHEALTH_tabl4cel14b.PaddingBottom = 7;
                    PHLHEALTH_tabl4cel14b.BorderWidth = 0;
                    table4.AddCell(PHLHEALTH_tabl4cel14b);

                    PdfPCell PHLHEALTH_tabl4cel14c = new PdfPCell(new PdfPCell(new Paragraph(PHealthAmount, FontFactory.GetFont("arial", 8))));
                    PHLHEALTH_tabl4cel14c.HorizontalAlignment = Element.ALIGN_CENTER;
                    PHLHEALTH_tabl4cel14c.PaddingBottom = 7;
                    PHLHEALTH_tabl4cel14c.BorderWidth = 0;
                    table4.AddCell(PHLHEALTH_tabl4cel14c);

                    PdfPCell PHLHEALTH_tabl4cel14d = new PdfPCell(new PdfPCell(new Paragraph(PHealth_YTD_Amount, FontFactory.GetFont("arial", 8))));
                    PHLHEALTH_tabl4cel14d.HorizontalAlignment = Element.ALIGN_RIGHT;
                    PHLHEALTH_tabl4cel14d.PaddingBottom = 7;
                    PHLHEALTH_tabl4cel14d.BorderWidth = 0;
                    table4.AddCell(PHLHEALTH_tabl4cel14d);
                }


                GDB = new GetDatabase();
                GDB.ClientName = prms.CN;
                SelfieRegistration2Controller.GetDB2(GDB);
                // FOR HDMF
                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID });
                con.myparameters.Add(new myParameters { ParameterName = "@payoutDate", mytype = SqlDbType.VarChar, Value = prms.PayoutDate });
                DataTable dt14 = con.GetDataTable("sp_GetGovernmentAmounts_Pagibig_Payslip");


                GDB = new GetDatabase();
                GDB.ClientName = prms.CN;
                SelfieRegistration2Controller.GetDB2(GDB);
                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID });
                con.myparameters.Add(new myParameters { ParameterName = "@year", mytype = SqlDbType.VarChar, Value = prms.ytd_year });
                con.myparameters.Add(new myParameters { ParameterName = "@payoutDate", mytype = SqlDbType.VarChar, Value = prms.PayoutDate });
                DataTable dt15 = con.GetDataTable("sp_GetGovernmentAmounts_Pagibig_YTD_Payslip");

                if (dt14.Rows.Count > 0)
                {
                    string HDMFDesc = "HDMF", HDMFAmount = "", HDMF_YTD_Amount = "";
                    HDMFAmount = dt14.Rows[0]["HDMF"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt14.Rows[0]["HDMF"].ToString()));
                    HDMF_YTD_Amount = dt15.Rows[0]["Pagibig_Employee"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt15.Rows[0]["Pagibig_Employee"].ToString()));

                    PdfPCell HDMF_tabl4cel14a = new PdfPCell(new PdfPCell(new Paragraph(HDMFDesc, FontFactory.GetFont("arial", 8))));
                    HDMF_tabl4cel14a.PaddingBottom = 7;
                    HDMF_tabl4cel14a.BorderWidth = 0;
                    table4.AddCell(HDMF_tabl4cel14a);

                    PdfPCell HDMF_tabl4cel14b = new PdfPCell(new PdfPCell(new Paragraph("", FontFactory.GetFont("arial", 8))));
                    HDMF_tabl4cel14b.HorizontalAlignment = Element.ALIGN_RIGHT;
                    HDMF_tabl4cel14b.PaddingBottom = 7;
                    HDMF_tabl4cel14b.BorderWidth = 0;
                    table4.AddCell(HDMF_tabl4cel14b);

                    PdfPCell HDMF_tabl4cel14c = new PdfPCell(new PdfPCell(new Paragraph(HDMFAmount, FontFactory.GetFont("arial", 8))));
                    HDMF_tabl4cel14c.HorizontalAlignment = Element.ALIGN_CENTER;
                    HDMF_tabl4cel14c.PaddingBottom = 7;
                    HDMF_tabl4cel14c.BorderWidth = 0;
                    table4.AddCell(HDMF_tabl4cel14c);

                    PdfPCell HDMF_tabl4cel14d = new PdfPCell(new PdfPCell(new Paragraph(HDMF_YTD_Amount, FontFactory.GetFont("arial", 8))));
                    HDMF_tabl4cel14d.HorizontalAlignment = Element.ALIGN_RIGHT;
                    HDMF_tabl4cel14d.PaddingBottom = 7;
                    HDMF_tabl4cel14d.BorderWidth = 0;
                    table4.AddCell(HDMF_tabl4cel14d);
                }




                GDB = new GetDatabase();
                GDB.ClientName = prms.CN;
                SelfieRegistration2Controller.GetDB2(GDB);


                // FOR LOANS FIELD
                PdfPCell tabl4cell15 = new PdfPCell(new PdfPCell(new Paragraph("DEDUCTIONS", FontFactory.GetFont("arial", 8, Font.UNDERLINE))));
                tabl4cell14.PaddingTop = 30;
                tabl4cell15.PaddingBottom = 7;
                tabl4cell15.BorderWidth = 0;
                table4.AddCell(tabl4cell15);

                //PdfPCell tabl4cell16 = new PdfPCell(new PdfPCell(new Paragraph("BALANCE LEFT", FontFactory.GetFont("arial", 8, Font.BOLD))));
                PdfPCell tabl4cell16 = new PdfPCell(new PdfPCell(new Paragraph("", FontFactory.GetFont("arial", 8, Font.BOLD))));
                tabl4cell16.HorizontalAlignment = Element.ALIGN_RIGHT;
                tabl4cell16.PaddingBottom = 7;
                tabl4cell14.PaddingTop = 20;
                tabl4cell16.BorderWidth = 0;
                table4.AddCell(tabl4cell16);

                PdfPCell tabl4cell17 = new PdfPCell(new PdfPCell(new Paragraph("", FontFactory.GetFont("arial", 8, Font.BOLD))));
                tabl4cell17.PaddingBottom = 7;
                tabl4cell17.BorderWidth = 0;
                table4.AddCell(tabl4cell17);

                PdfPCell tabl4cell18 = new PdfPCell(new PdfPCell(new Paragraph("", FontFactory.GetFont("arial", 8, Font.BOLD))));
                tabl4cell18.HorizontalAlignment = Element.ALIGN_RIGHT;
                tabl4cell18.PaddingBottom = 7;
                tabl4cell18.BorderWidth = 0;
                table4.AddCell(tabl4cell18);


                GDB = new GetDatabase();
                GDB.ClientName = prms.CN;
                SelfieRegistration2Controller.GetDB2(GDB);
                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID });
                con.myparameters.Add(new myParameters { ParameterName = "@payoutDate", mytype = SqlDbType.VarChar, Value = prms.PayoutDate });
                DataTable dt20 = con.GetDataTable("sp_GetLoans_Payslip");


                GDB = new GetDatabase();
                GDB.ClientName = prms.CN;
                SelfieRegistration2Controller.GetDB2(GDB);
                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID });
                con.myparameters.Add(new myParameters { ParameterName = "@year", mytype = SqlDbType.VarChar, Value = prms.ytd_year });
                con.myparameters.Add(new myParameters { ParameterName = "@payoutDate", mytype = SqlDbType.VarChar, Value = prms.PayoutDate });
                DataTable dt21 = con.GetDataTable("sp_GetLoans_YTD_Payslip");

                if (dt20.Rows.Count > 0)
                {
                    string LoanName = "", Amortization_Amount = "", Amortization_YTD_Amount = "";

                    for (int i = 0; i < dt20.Rows.Count; i++)
                    {
                        LoanName = dt20.Rows[i]["LoanName"].ToString();
                        Amortization_Amount = dt20.Rows[i]["AmortizationAmount"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt20.Rows[i]["AmortizationAmount"].ToString()));
                        Amortization_YTD_Amount = dt21.Rows[i]["AmortizationAmount"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt21.Rows[i]["AmortizationAmount"].ToString()));

                        PdfPCell Loan_tabl4cel14a = new PdfPCell(new PdfPCell(new Paragraph(LoanName, FontFactory.GetFont("arial", 8))));
                        Loan_tabl4cel14a.PaddingBottom = 7;
                        Loan_tabl4cel14a.BorderWidth = 0;
                        table4.AddCell(Loan_tabl4cel14a);

                        PdfPCell Loan_tabl4cel14b = new PdfPCell(new PdfPCell(new Paragraph("", FontFactory.GetFont("arial", 8))));
                        Loan_tabl4cel14b.HorizontalAlignment = Element.ALIGN_RIGHT;
                        Loan_tabl4cel14b.PaddingBottom = 7;
                        Loan_tabl4cel14b.BorderWidth = 0;
                        table4.AddCell(Loan_tabl4cel14b);

                        PdfPCell Loan_tabl4cel14c = new PdfPCell(new PdfPCell(new Paragraph(Amortization_Amount, FontFactory.GetFont("arial", 8))));
                        Loan_tabl4cel14c.HorizontalAlignment = Element.ALIGN_CENTER;
                        Loan_tabl4cel14c.PaddingBottom = 7;
                        Loan_tabl4cel14c.BorderWidth = 0;
                        table4.AddCell(Loan_tabl4cel14c);

                        PdfPCell Loan_tabl4cel14d = new PdfPCell(new PdfPCell(new Paragraph(Amortization_YTD_Amount, FontFactory.GetFont("arial", 8))));
                        Loan_tabl4cel14d.HorizontalAlignment = Element.ALIGN_RIGHT;
                        Loan_tabl4cel14d.PaddingBottom = 7;
                        Loan_tabl4cel14d.BorderWidth = 0;
                        table4.AddCell(Loan_tabl4cel14d);
                    }
                }
                //
                // OTHER DEDUCTIONS
                GDB = new GetDatabase();
                GDB.ClientName = prms.CN;
                SelfieRegistration2Controller.GetDB2(GDB);
                Connection con3 = new Connection();
                con3.myparameters.Clear();
                con3.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID });
                con3.myparameters.Add(new myParameters { ParameterName = "@payoutDate", mytype = SqlDbType.VarChar, Value = prms.PayoutDate });
                DataTable dt18 = con3.GetDataTable("sp_GetDeductions_Payslip");


                GDB = new GetDatabase();
                GDB.ClientName = prms.CN;
                SelfieRegistration2Controller.GetDB2(GDB);
                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID });
                con.myparameters.Add(new myParameters { ParameterName = "@year", mytype = SqlDbType.VarChar, Value = prms.ytd_year });
                con.myparameters.Add(new myParameters { ParameterName = "@payoutDate", mytype = SqlDbType.VarChar, Value = prms.PayoutDate });
                DataTable dt19 = con.GetDataTable("sp_GetDeductions_YTD_Payslip");

                if (dt18.Rows.Count > 0)
                {
                    for (int i = 0; i < dt18.Rows.Count; i++)
                    {
                        string DeductionDesc = "", Deduction_Amount = "", Deduction_YTD_Amount = "";

                        DeductionDesc = dt18.Rows[i]["deductiondescription"].ToString();
                        Deduction_Amount = dt18.Rows[i]["deductionamount"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt18.Rows[i]["deductionamount"].ToString()));
                        Deduction_YTD_Amount = dt19.Rows[i]["deductionAmount"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt19.Rows[i]["deductionAmount"].ToString()));

                        PdfPCell OtherDeduction_tabl4cel14a = new PdfPCell(new PdfPCell(new Paragraph(DeductionDesc, FontFactory.GetFont("arial", 8))));
                        OtherDeduction_tabl4cel14a.PaddingBottom = 7;
                        OtherDeduction_tabl4cel14a.BorderWidth = 0;
                        table4.AddCell(OtherDeduction_tabl4cel14a);

                        PdfPCell OtherDeduction_tabl4cel14b = new PdfPCell(new PdfPCell(new Paragraph("", FontFactory.GetFont("arial", 8))));
                        OtherDeduction_tabl4cel14b.HorizontalAlignment = Element.ALIGN_RIGHT;
                        OtherDeduction_tabl4cel14b.PaddingBottom = 7;
                        OtherDeduction_tabl4cel14b.BorderWidth = 0;
                        table4.AddCell(OtherDeduction_tabl4cel14b);

                        PdfPCell OtherDeduction_tabl4cel14c = new PdfPCell(new PdfPCell(new Paragraph(Deduction_Amount, FontFactory.GetFont("arial", 8))));
                        OtherDeduction_tabl4cel14c.HorizontalAlignment = Element.ALIGN_CENTER;
                        OtherDeduction_tabl4cel14c.PaddingBottom = 7;
                        OtherDeduction_tabl4cel14c.BorderWidth = 0;
                        table4.AddCell(OtherDeduction_tabl4cel14c);

                        PdfPCell OtherDeduction_tabl4cel14d = new PdfPCell(new PdfPCell(new Paragraph(Deduction_YTD_Amount, FontFactory.GetFont("arial", 8))));
                        OtherDeduction_tabl4cel14d.HorizontalAlignment = Element.ALIGN_RIGHT;
                        OtherDeduction_tabl4cel14d.PaddingBottom = 7;
                        OtherDeduction_tabl4cel14d.BorderWidth = 0;
                        table4.AddCell(OtherDeduction_tabl4cel14d);
                    }
                }
                //
                //
                PdfPCell tabl4cell19 = new PdfPCell(new PdfPCell(new Paragraph("WithHolding Tax", FontFactory.GetFont("arial", 8))));
                tabl4cell19.PaddingBottom = 7;
                tabl4cell19.BorderWidth = 0;
                tabl4cell19.BackgroundColor = BaseColor.LIGHT_GRAY;
                table4.AddCell(tabl4cell19);

                PdfPCell tabl4cell20 = new PdfPCell(new PdfPCell(new Paragraph("", FontFactory.GetFont("arial", 8))));
                tabl4cell20.HorizontalAlignment = Element.ALIGN_RIGHT;
                tabl4cell20.PaddingBottom = 7;
                tabl4cell20.BorderWidth = 0;
                tabl4cell20.BackgroundColor = BaseColor.LIGHT_GRAY;
                table4.AddCell(tabl4cell20);

                PdfPCell tabl4cell21 = new PdfPCell(new PdfPCell(new Paragraph(whtax, FontFactory.GetFont("arial", 8))));
                tabl4cell21.HorizontalAlignment = Element.ALIGN_CENTER;
                tabl4cell21.PaddingBottom = 7;
                tabl4cell21.BorderWidth = 0;
                tabl4cell21.BackgroundColor = BaseColor.LIGHT_GRAY;
                table4.AddCell(tabl4cell21);

                PdfPCell tabl4cell22 = new PdfPCell(new PdfPCell(new Paragraph(ytdWhTax, FontFactory.GetFont("arial", 8))));
                tabl4cell22.HorizontalAlignment = Element.ALIGN_RIGHT;
                tabl4cell22.PaddingBottom = 7;
                tabl4cell22.BorderWidth = 0;
                tabl4cell22.BackgroundColor = BaseColor.LIGHT_GRAY;
                table4.AddCell(tabl4cell22);
                //

                //
                PdfPCell tabl4cell23 = new PdfPCell(new PdfPCell(new Paragraph("Net Pay", FontFactory.GetFont("arial", 8))));
                tabl4cell23.PaddingBottom = 7;
                tabl4cell23.BorderWidth = 0;
                table4.AddCell(tabl4cell23);

                PdfPCell tabl4cell24 = new PdfPCell(new PdfPCell(new Paragraph("", FontFactory.GetFont("arial", 8))));
                tabl4cell24.HorizontalAlignment = Element.ALIGN_RIGHT;
                tabl4cell24.PaddingBottom = 7;
                tabl4cell24.BorderWidth = 0;
                table4.AddCell(tabl4cell24);

                PdfPCell tabl4cell25 = new PdfPCell(new PdfPCell(new Paragraph(netpay, FontFactory.GetFont("arial", 8))));
                tabl4cell25.HorizontalAlignment = Element.ALIGN_CENTER;
                tabl4cell25.PaddingBottom = 7;
                tabl4cell25.BorderWidth = 0;
                table4.AddCell(tabl4cell25);

                PdfPCell tabl4cell26 = new PdfPCell(new PdfPCell(new Paragraph(ytdNetPay, FontFactory.GetFont("arial", 8))));
                tabl4cell26.HorizontalAlignment = Element.ALIGN_RIGHT;
                tabl4cell26.PaddingBottom = 7;
                tabl4cell26.BorderWidth = 0;
                table4.AddCell(tabl4cell26);


                //
                PdfPCell tabl4cell27 = new PdfPCell(new PdfPCell(new Paragraph("", FontFactory.GetFont("arial", 8, Font.UNDERLINE)))) { Border = PdfPCell.NO_BORDER, HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT };
                tabl4cell27.Colspan = 4;
                tabl4cell27.PaddingTop = 10;
                tabl4cell27.PaddingBottom = 7;
                tabl4cell27.BorderWidth = 0;
                table4.AddCell(tabl4cell27);
                //
                table2.AddCell(table4);


                PdfPCell cell22 = new PdfPCell() { Border = PdfPCell.NO_BORDER };
                cell22.Colspan = 2;


                table1.AddCell(cell11);
                table1.AddCell(cell12);
                table2.AddCell(cell22);
                document.Add(table0);
                document.Add(table1);
                document.Add(table2);
                document.Close();
                string onlineDIR = "https://ws.durusthr.com/ILM_WS_Live/pdf/";
                return onlineDIR + filename;
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }

        [HttpPost]
        [Route("generatepdfV3")]
        public string generatepdfsV3(generatePDF prms)
        {
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = prms.CN;
            SelfieRegistration2Controller.GetDB2(GDB);

            // DATA VARIABLE DECLARATION
            string startdate = "", enddate = "";
            string payday = "";
            string fullname = "", empid = "", datejoined = "", position = "", department = "", TIN = "", SSS = "", PHILHEALTH = "", PAGIBIG = "";
            string basicpay = "", grosspay = "", netpay = "", whtax = "", ytdBasicPay = "", ytdGrossPay = "", ytdNetPay = "", ytdWhTax = "";
            string HourlyRate = "", HoursWorked = "", AbsencesDesc = "Absences", TardiDesc = "Tardiness", UndertimeDesc = "Undertime", LWOPDesc = "LWOP", Monthly = "";
            string Absences_Hours = "", Tardi_Hours = "", Undertime_Hours = "", LWOP_Hours = "";
            string Absences_Amount = "", Tardi_Amount = "", Undertime_Amount = "", LWOP_Amount = "";
            string Absences_YTD_Amount = "", Tardi_YTD_Amount = "", Undertime_YTD_Amount = "", LWOP_YTD_Amount = "";
            string CompanyName = "";

            // FOR COMPANY NAME
            Connection conn = new Connection();
            conn.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.VarChar, Value = prms.EmpID });
            DataTable dtbl = conn.GetDataTable("sp_get_CompanyName_MobileDynamicPayslip");
            foreach (DataRow item in dtbl.Rows)
            {
                CompanyName = item["TradingName"].ToString();
            }
            // FOR PAY PERIOD FIELD
            Connection con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID });
            con.myparameters.Add(new myParameters { ParameterName = "@payoutDate", mytype = SqlDbType.VarChar, Value = prms.PayoutDate });
            DataTable DT = con.GetDataTable("sp_GetPayPeriod_Payslip");
            foreach (DataRow item in DT.Rows)
            {
                startdate = item["StartDate"].ToString();
                enddate = item["EndDate"].ToString();
            }


            GDB = new GetDatabase();
            GDB.ClientName = prms.CN;
            SelfieRegistration2Controller.GetDB2(GDB);
            // FOR PAY DATE FIELD
            con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID });
            DataTable DT1 = con.GetDataTable("sp_GetPayoutSchemeList");
            foreach (DataRow item in DT1.Rows)
            {
                payday = item["PayoutDate"].ToString();
            }

            GDB = new GetDatabase();
            GDB.ClientName = prms.CN;
            SelfieRegistration2Controller.GetDB2(GDB);
            // EMPLOYEE DETAILS FIELD
            con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID });
            DataTable dt2 = con.GetDataTable("sp_GetEmployeeInfo_Payslip");
            foreach (DataRow item in dt2.Rows)
            {
                fullname = item["EmpName"].ToString();
                empid = item["EmpID"].ToString();
                datejoined = item["DateJoined"].ToString();
                department = item["BusinessUnit"].ToString();
                position = item["JobDesc"].ToString();
            }

            GDB = new GetDatabase();
            GDB.ClientName = prms.CN;
            SelfieRegistration2Controller.GetDB2(GDB);
            // FOR GOVERNMENT ID FIELDS
            con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID });
            DataTable dt3 = con.GetDataTable("sp_GetGovIDs_Payslip");
            foreach (DataRow item in dt3.Rows)
            {
                if (item["IDTypes"].ToString() == "SSS")
                {
                    SSS = item["IDCode"].ToString();
                }
                else if (item["IDTypes"].ToString() == "Philhealth")
                {
                    PHILHEALTH = item["IDCode"].ToString();
                }
                else if (item["IDTypes"].ToString() == "Pag-Ibig")
                {
                    PAGIBIG = item["IDCode"].ToString();
                }
                else
                {
                    TIN = item["IDCode"].ToString();
                }
            }

            GDB = new GetDatabase();
            GDB.ClientName = prms.CN;
            SelfieRegistration2Controller.GetDB2(GDB);
            // FOR GROSS PAY, BASIC PAY AND NET PAY FIELDS
            con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID });
            con.myparameters.Add(new myParameters { ParameterName = "@payoutDate", mytype = SqlDbType.VarChar, Value = prms.PayoutDate });
            DataTable dt4 = con.GetDataTable("sp_GetPay_Payslip");
            foreach (DataRow item in dt4.Rows)
            {
                basicpay = item["Basic Pay"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(item["Basic Pay"].ToString()));
                grosspay = item["Gross Pay"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(item["Gross Pay"].ToString()));
                netpay = item["Net Pay"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(item["Net Pay"].ToString()));
                whtax = item["WithHolding Tax"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(item["WithHolding Tax"].ToString()));

                //basicpay = String.Format("{0:n2}", decimal.Parse(basicpay));
            }
            // END


            //GDB = new GetDatabase();
            //GDB.ClientName = prms.CN;
            //SelfieRegistration2Controller.GetDB2(GDB);
            // YTD GROSS PAY, BASIC PAY AND NET PAY FIELDS
            //con = new Connection();
            //con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID });
            //con.myparameters.Add(new myParameters { ParameterName = "@year", mytype = SqlDbType.VarChar, Value = prms.ytd_year });
            //con.myparameters.Add(new myParameters { ParameterName = "@payoutDate", mytype = SqlDbType.VarChar, Value = prms.PayoutDate });
            //DataTable dt5 = con.GetDataTable("sp_GetPay_YTD_Payslip");
            //foreach (DataRow item in dt5.Rows)
            //{
            //    ytdBasicPay = item["basicpay"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(item["basicpay"].ToString()));
            //    ytdGrossPay = item["grosspay"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(item["grosspay"].ToString()));
            //    ytdWhTax = item["whtax"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(item["whtax"].ToString()));
            //    ytdNetPay = item["netpay"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(item["netpay"].ToString()));
            //}

            GDB = new GetDatabase();
            GDB.ClientName = prms.CN;
            SelfieRegistration2Controller.GetDB2(GDB);
            // FOR LOGS INFO
            con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID });
            con.myparameters.Add(new myParameters { ParameterName = "@payoutDate", mytype = SqlDbType.VarChar, Value = prms.PayoutDate });
            DataTable dt16 = con.GetDataTable("sp_GetLogInfo_Payslip");

            if (dt16.Rows.Count > 0)
            {
                HourlyRate = dt16.Rows[0]["HourlyRate"] == DBNull.Value || dt16.Rows[0]["HourlyRate"].ToString() == "" ? "" : String.Format("{0:n2}", decimal.Parse(dt16.Rows[0]["HourlyRate"].ToString()));
                HoursWorked = dt16.Rows[0]["HoursWorked"] == DBNull.Value || dt16.Rows[0]["HoursWorked"].ToString() == "" ? "" : String.Format("{0:n2}", decimal.Parse(dt16.Rows[0]["HoursWorked"].ToString()));
                //Monthly = dt16.Rows[0]["Monthly"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt16.Rows[0]["Monthly"].ToString()));
            }


            con.myparameters.Clear();
            con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID });
            con.myparameters.Add(new myParameters { ParameterName = "@payoutDate", mytype = SqlDbType.VarChar, Value = prms.PayoutDate });
            Monthly = con.ExecuteScalar("sp_GetMonthlySalary_Payslip");

            GDB = new GetDatabase();
            GDB.ClientName = prms.CN;
            SelfieRegistration2Controller.GetDB2(GDB);
            con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID });
            con.myparameters.Add(new myParameters { ParameterName = "@year", mytype = SqlDbType.VarChar, Value = prms.ytd_year });
            con.myparameters.Add(new myParameters { ParameterName = "@payoutDate", mytype = SqlDbType.VarChar, Value = prms.PayoutDate });
            DataTable dt17 = con.GetDataTable("sp_GetLogInfo_YTD_Payslip");

            System.IO.FileStream fs = new FileStream(HttpContext.Current.Server.MapPath("pdf") + "\\" + "payslip-" + prms.EmpID + "-" + prms.PayoutDate + ".pdf", FileMode.Create);
            //System.IO.FileStream fb = new FileStream(HttpContext.Current.Server.MapPath("pdf") + "\\" + "ilm.png", FileMode.Open);
            string imagepath = HttpContext.Current.Server.MapPath("Images");
            Document document = new Document(PageSize.A4, 25, 25, 30, 30);
            PdfWriter writer = PdfWriter.GetInstance(document, fs);
            //PdfWriter writering = PdfWriter.GetInstance(document, fb);
            document.Open();

            PdfPTable table0 = new PdfPTable(1);

            PdfPCell cell0 = new PdfPCell(new iTextSharp.text.Phrase(CompanyName, FontFactory.GetFont("Arial", 12, iTextSharp.text.Font.BOLD))) { Border = 0, HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT, VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE };
            cell0.PaddingTop = 10;
            //PdfPCell cell01 = new PdfPCell(new iTextSharp.text.Phrase("Lima Technology Center  Lipa City, Batangas Philippines 4217", FontFactory.GetFont("Arial", 11, iTextSharp.text.Font.NORMAL))) { Border = 0, HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT, VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE };
            //cell01.PaddingBottom = 7;

            //Image logoimage = Image.GetInstance(Path.Combine(imagepath, "yokohamalogo.jpg"));
            //logoimage.ScaleAbsolute(100, 40);
            //logoimage.SetAbsolutePosition(100, 30);

            //PdfPCell LogoCell = new PdfPCell(logoimage, true);

            table0.AddCell(cell0);
            ////table0.AddCell(LogoCell);
            //table0.AddCell(cell01);
            //document.Add(LogoCell);


            PdfPTable table1 = new PdfPTable(2);

            /*var titleFont = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.UNDEFINED, 12);
            var informationDetails = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.UNDEFINED, 12);*/

            PdfPCell cell11 = new PdfPCell(new iTextSharp.text.Phrase("PAY PERIOD\n" + startdate + " - " + enddate)) { Border = PdfPCell.BOTTOM_BORDER | PdfPCell.TOP_BORDER, HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER, VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE };
            PdfPCell cell12 = new PdfPCell(new iTextSharp.text.Phrase("PAY DAY\n" + prms.PayoutDate)) { Border = PdfPCell.BOTTOM_BORDER | PdfPCell.TOP_BORDER, HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER, VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE };
            cell11.PaddingTop = 10;
            cell12.PaddingTop = 10;
            cell11.PaddingBottom = 7;
            cell12.PaddingBottom = 7;
            /* cell12.AddElement(new Paragraph("PAY DAY", titleFont));
            cell12.AddElement(new Paragraph("-", informationDetails));*/

            PdfPTable table2 = new PdfPTable(2);
            float[] widths = new float[] { 100f, 200f };
            float[] heights = new float[] { 300f };
            table2.SetWidths(widths);
            //table2.DefaultCell.FixedHeight = 500f;
            table2.DefaultCell.Border = Rectangle.NO_BORDER;
            //PdfPCell cell21 = new PdfPCell(new Paragraph("Danilo Vicente Sulla Jr")) { Border = PdfPCell.RIGHT_BORDER, HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER, VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE };
            //cell21.PaddingTop = 20;

            PdfPTable table3 = new PdfPTable(2);
            table3.TotalWidth = 100;
            table3.DefaultCell.FixedHeight = 600f;
            table3.DefaultCell.Border = Rectangle.RIGHT_BORDER;

            PdfPCell tabl3cell0 = new PdfPCell(new PdfPCell(new Paragraph(fullname, FontFactory.GetFont("Arial", 12, iTextSharp.text.Font.BOLD)))) { Border = PdfPCell.NO_BORDER, HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER, VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE };
            tabl3cell0.Colspan = 2;
            tabl3cell0.PaddingTop = 20;
            tabl3cell0.PaddingBottom = 20;
            tabl3cell0.BorderWidth = 0;
            tabl3cell0.BorderWidthRight = 1;
            table3.AddCell(tabl3cell0);


            PdfPCell tabl3cell1 = new PdfPCell(new PdfPCell(new Paragraph("EmpID", FontFactory.GetFont("arial", 8))));
            tabl3cell1.PaddingBottom = 7;
            tabl3cell1.BorderWidth = 0;
            tabl3cell1.BorderWidthBottom = 1;
            table3.AddCell(tabl3cell1);

            PdfPCell tabl3cell2 = new PdfPCell(new PdfPCell(new Paragraph(empid, FontFactory.GetFont("arial", 8, iTextSharp.text.Font.BOLD))));
            tabl3cell2.HorizontalAlignment = Element.ALIGN_RIGHT;
            tabl3cell2.PaddingBottom = 7;
            tabl3cell2.BorderWidth = 0;
            tabl3cell2.BorderWidthBottom = 1;
            tabl3cell2.BorderWidthRight = 1;
            table3.AddCell(tabl3cell2);

            PdfPCell tabl3cell3 = new PdfPCell(new PdfPCell(new Paragraph("Tax Category", FontFactory.GetFont("arial", 8))));
            tabl3cell3.PaddingBottom = 7;
            tabl3cell3.BorderWidth = 0;
            tabl3cell3.BorderWidthBottom = 1;
            table3.AddCell(tabl3cell3);

            PdfPCell tabl3cell4 = new PdfPCell(new PdfPCell(new Paragraph("--", FontFactory.GetFont("arial", 8, iTextSharp.text.Font.BOLD))));
            tabl3cell4.HorizontalAlignment = Element.ALIGN_RIGHT;
            tabl3cell4.PaddingBottom = 7;
            tabl3cell4.BorderWidth = 0;
            tabl3cell4.BorderWidthBottom = 1;
            tabl3cell4.BorderWidthRight = 1;
            table3.AddCell(tabl3cell4);

            PdfPCell tabl3cell5 = new PdfPCell(new PdfPCell(new Paragraph("Hourly Rate", FontFactory.GetFont("arial", 8))));
            tabl3cell5.PaddingBottom = 7;
            tabl3cell5.BorderWidth = 0;
            tabl3cell5.BorderWidthBottom = 1;
            table3.AddCell(tabl3cell5);

            PdfPCell tabl3cell6 = new PdfPCell(new PdfPCell(new Paragraph(HourlyRate, FontFactory.GetFont("arial", 8, iTextSharp.text.Font.BOLD))));
            tabl3cell6.HorizontalAlignment = Element.ALIGN_RIGHT;
            tabl3cell6.PaddingBottom = 7;
            tabl3cell6.BorderWidth = 0;
            tabl3cell6.BorderWidthBottom = 1;
            tabl3cell6.BorderWidthRight = 1;
            table3.AddCell(tabl3cell6);

            PdfPCell tabl3cell7 = new PdfPCell(new PdfPCell(new Paragraph("Regular Hours", FontFactory.GetFont("arial", 8))));
            tabl3cell7.PaddingBottom = 7;
            tabl3cell7.BorderWidth = 0;
            tabl3cell7.BorderWidthBottom = 1;
            table3.AddCell(tabl3cell7);

            PdfPCell tabl3cell8 = new PdfPCell(new PdfPCell(new Paragraph(HoursWorked, FontFactory.GetFont("arial", 8, iTextSharp.text.Font.BOLD))));
            tabl3cell8.HorizontalAlignment = Element.ALIGN_RIGHT;
            tabl3cell8.PaddingBottom = 7;
            tabl3cell8.BorderWidth = 0;
            tabl3cell8.BorderWidthBottom = 1;
            tabl3cell8.BorderWidthRight = 1;
            table3.AddCell(tabl3cell8);

            PdfPCell tabl3cell9 = new PdfPCell(new PdfPCell(new Paragraph(getSalaryScheme(Monthly.ToString()) + " Salary", FontFactory.GetFont("arial", 8))));
            tabl3cell9.PaddingBottom = 7;
            tabl3cell9.BorderWidth = 0;
            tabl3cell9.BorderWidthBottom = 1;
            table3.AddCell(tabl3cell9);

            PdfPCell tabl3cell10 = new PdfPCell(new PdfPCell(new Paragraph(Monthly, FontFactory.GetFont("arial", 8, iTextSharp.text.Font.BOLD))));
            tabl3cell10.HorizontalAlignment = Element.ALIGN_RIGHT;
            tabl3cell10.PaddingBottom = 7;
            tabl3cell10.BorderWidth = 0;
            tabl3cell10.BorderWidthBottom = 1;
            tabl3cell10.BorderWidthRight = 1;
            table3.AddCell(tabl3cell10);

            PdfPCell tabl3cell11 = new PdfPCell(new PdfPCell(new Paragraph("Date Joined", FontFactory.GetFont("arial", 8))));
            tabl3cell11.PaddingBottom = 7;
            tabl3cell11.BorderWidth = 0;
            tabl3cell11.BorderWidthBottom = 1;
            table3.AddCell(tabl3cell11);

            PdfPCell tabl3cell12 = new PdfPCell(new PdfPCell(new Paragraph(datejoined, FontFactory.GetFont("arial", 8, iTextSharp.text.Font.BOLD))));
            tabl3cell12.HorizontalAlignment = Element.ALIGN_RIGHT;
            tabl3cell12.PaddingBottom = 7;
            tabl3cell12.BorderWidth = 0;
            tabl3cell12.BorderWidthBottom = 1;
            tabl3cell12.BorderWidthRight = 1;
            table3.AddCell(tabl3cell12);

            PdfPCell tabl3cell13 = new PdfPCell(new PdfPCell(new Paragraph("Position", FontFactory.GetFont("arial", 8))));
            tabl3cell13.PaddingBottom = 7;
            tabl3cell13.BorderWidth = 0;
            tabl3cell13.BorderWidthBottom = 1;
            table3.AddCell(tabl3cell13);

            PdfPCell tabl3cell14 = new PdfPCell(new PdfPCell(new Paragraph(position, FontFactory.GetFont("arial", 8, iTextSharp.text.Font.BOLD))));
            tabl3cell14.HorizontalAlignment = Element.ALIGN_RIGHT;
            tabl3cell14.PaddingBottom = 7;
            tabl3cell14.BorderWidth = 0;
            tabl3cell14.BorderWidthBottom = 1;
            tabl3cell14.BorderWidthRight = 1;
            table3.AddCell(tabl3cell14);

            PdfPCell tabl3cell15 = new PdfPCell(new PdfPCell(new Paragraph("Department", FontFactory.GetFont("arial", 8))));
            tabl3cell15.PaddingBottom = 7;
            tabl3cell15.BorderWidth = 0;
            tabl3cell15.BorderWidthBottom = 1;
            table3.AddCell(tabl3cell15);

            PdfPCell tabl3cell16 = new PdfPCell(new PdfPCell(new Paragraph(department, FontFactory.GetFont("arial", 8, iTextSharp.text.Font.BOLD))));
            tabl3cell16.HorizontalAlignment = Element.ALIGN_RIGHT;
            tabl3cell16.PaddingBottom = 7;
            tabl3cell16.BorderWidth = 0;
            tabl3cell16.BorderWidthBottom = 1;
            tabl3cell16.BorderWidthRight = 1;
            table3.AddCell(tabl3cell16);

            PdfPCell tabl3cell17 = new PdfPCell(new PdfPCell(new Paragraph("Philhealth", FontFactory.GetFont("arial", 8))));
            tabl3cell17.PaddingBottom = 7;
            tabl3cell17.BorderWidth = 0;
            tabl3cell17.BorderWidthBottom = 1;
            table3.AddCell(tabl3cell17);

            PdfPCell tabl3cell18 = new PdfPCell(new PdfPCell(new Paragraph(PHILHEALTH, FontFactory.GetFont("arial", 8, iTextSharp.text.Font.BOLD))));
            tabl3cell18.HorizontalAlignment = Element.ALIGN_RIGHT;
            tabl3cell18.PaddingBottom = 7;
            tabl3cell18.BorderWidth = 0;
            tabl3cell18.BorderWidthBottom = 1;
            tabl3cell18.BorderWidthRight = 1;
            table3.AddCell(tabl3cell18);

            PdfPCell tabl3cell19 = new PdfPCell(new PdfPCell(new Paragraph("Pag-Ibig", FontFactory.GetFont("arial", 8))));
            tabl3cell19.PaddingBottom = 7;
            tabl3cell19.BorderWidth = 0;
            tabl3cell19.BorderWidthBottom = 1;
            table3.AddCell(tabl3cell19);

            PdfPCell tabl3cell20 = new PdfPCell(new PdfPCell(new Paragraph(PAGIBIG, FontFactory.GetFont("arial", 8, iTextSharp.text.Font.BOLD))));
            tabl3cell20.HorizontalAlignment = Element.ALIGN_RIGHT;
            tabl3cell20.PaddingBottom = 7;
            tabl3cell20.BorderWidth = 0;
            tabl3cell20.BorderWidthBottom = 1;
            tabl3cell20.BorderWidthRight = 1;
            table3.AddCell(tabl3cell20);

            PdfPCell tabl3cell21 = new PdfPCell(new PdfPCell(new Paragraph("TIN", FontFactory.GetFont("arial", 8))));
            tabl3cell21.PaddingBottom = 7;
            tabl3cell21.BorderWidth = 0;
            tabl3cell21.BorderWidthBottom = 1;

            table3.AddCell(tabl3cell21);

            PdfPCell tabl3cell22 = new PdfPCell(new PdfPCell(new Paragraph(TIN, FontFactory.GetFont("arial", 8, iTextSharp.text.Font.BOLD))));
            tabl3cell22.HorizontalAlignment = Element.ALIGN_RIGHT;
            tabl3cell22.PaddingBottom = 7;
            tabl3cell22.BorderWidth = 0;
            tabl3cell22.BorderWidthBottom = 1;
            tabl3cell22.BorderWidthRight = 1;
            table3.AddCell(tabl3cell22);

            PdfPCell tabl3cell23 = new PdfPCell(new PdfPCell(new Paragraph("SSS", FontFactory.GetFont("arial", 8))));
            tabl3cell23.PaddingBottom = 7;
            tabl3cell23.BorderWidth = 0;
            table3.AddCell(tabl3cell23);

            PdfPCell tabl3cell24 = new PdfPCell(new PdfPCell(new Paragraph(SSS, FontFactory.GetFont("arial", 8, iTextSharp.text.Font.BOLD))));
            tabl3cell24.HorizontalAlignment = Element.ALIGN_RIGHT;
            tabl3cell24.PaddingBottom = 7;
            tabl3cell24.BorderWidth = 0;
            tabl3cell24.BorderWidthRight = 1;
            table3.AddCell(tabl3cell24);


            table2.AddCell(table3);

            //
            PdfPTable table4 = new PdfPTable(4);
            table4.TotalWidth = 100;
            table4.DefaultCell.Border = Rectangle.NO_BORDER;

            PdfPCell tabl4cell1 = new PdfPCell(new PdfPCell(new Paragraph("", FontFactory.GetFont("arial", 8))));
            tabl4cell1.PaddingBottom = 7;
            tabl4cell1.BorderWidth = 0;
            table4.AddCell(tabl4cell1);

            PdfPCell tabl4cell2 = new PdfPCell(new PdfPCell(new Paragraph("HOURS", FontFactory.GetFont("arial", 8, iTextSharp.text.Font.BOLD))));
            tabl4cell2.HorizontalAlignment = Element.ALIGN_RIGHT;
            tabl4cell2.PaddingBottom = 7;
            tabl4cell2.BorderWidth = 0;
            table4.AddCell(tabl4cell2);

            PdfPCell tabl4cell3 = new PdfPCell(new PdfPCell(new Paragraph("CURRENT (PHP)", FontFactory.GetFont("arial", 8, iTextSharp.text.Font.BOLD))));
            tabl4cell3.HorizontalAlignment = Element.ALIGN_CENTER;
            tabl4cell3.PaddingBottom = 7;
            tabl4cell3.BorderWidth = 0;
            table4.AddCell(tabl4cell3);

            PdfPCell tabl4cell4 = new PdfPCell(new PdfPCell(new Paragraph("", FontFactory.GetFont("arial", 8, iTextSharp.text.Font.BOLD))));
            tabl4cell4.HorizontalAlignment = Element.ALIGN_RIGHT;
            tabl4cell4.PaddingBottom = 7;
            tabl4cell4.BorderWidth = 0;
            table4.AddCell(tabl4cell4);


            //
            PdfPCell tabl4cell5 = new PdfPCell(new PdfPCell(new Paragraph("Basic Pay", FontFactory.GetFont("arial", 8))));
            tabl4cell5.PaddingBottom = 7;
            tabl4cell5.BorderWidth = 0;
            table4.AddCell(tabl4cell5);

            PdfPCell tabl4cell6 = new PdfPCell(new PdfPCell(new Paragraph("", FontFactory.GetFont("arial", 8))));
            tabl4cell6.HorizontalAlignment = Element.ALIGN_RIGHT;
            tabl4cell6.PaddingBottom = 7;
            tabl4cell6.BorderWidth = 0;
            table4.AddCell(tabl4cell6);

            PdfPCell tabl4cell7 = new PdfPCell(new PdfPCell(new Paragraph(basicpay, FontFactory.GetFont("arial", 8))));
            tabl4cell7.HorizontalAlignment = Element.ALIGN_CENTER;
            tabl4cell7.PaddingBottom = 7;
            tabl4cell7.BorderWidth = 0;
            table4.AddCell(tabl4cell7);

            PdfPCell tabl4cell8 = new PdfPCell(new PdfPCell(new Paragraph("", FontFactory.GetFont("arial", 8))));
            tabl4cell8.HorizontalAlignment = Element.ALIGN_RIGHT;
            tabl4cell8.PaddingBottom = 7;
            tabl4cell8.BorderWidth = 0;
            table4.AddCell(tabl4cell8);
            //

            //
            PdfPCell tabl4cell9 = new PdfPCell(new PdfPCell(new Paragraph("Gross Pay", FontFactory.GetFont("arial", 8))));
            tabl4cell9.PaddingBottom = 7;
            tabl4cell9.BorderWidth = 0;
            table4.AddCell(tabl4cell9);

            PdfPCell tabl4cell10 = new PdfPCell(new PdfPCell(new Paragraph("", FontFactory.GetFont("arial", 8))));
            tabl4cell10.HorizontalAlignment = Element.ALIGN_RIGHT;
            tabl4cell10.PaddingBottom = 7;
            tabl4cell10.BorderWidth = 0;
            table4.AddCell(tabl4cell10);

            PdfPCell tabl4cell11 = new PdfPCell(new PdfPCell(new Paragraph(grosspay, FontFactory.GetFont("arial", 8))));
            tabl4cell11.HorizontalAlignment = Element.ALIGN_CENTER;
            tabl4cell11.PaddingBottom = 7;
            tabl4cell11.BorderWidth = 0;
            table4.AddCell(tabl4cell11);

            PdfPCell tabl4cell12 = new PdfPCell(new PdfPCell(new Paragraph("", FontFactory.GetFont("arial", 8))));
            tabl4cell12.HorizontalAlignment = Element.ALIGN_RIGHT;
            tabl4cell12.PaddingBottom = 7;
            tabl4cell12.BorderWidth = 0;
            table4.AddCell(tabl4cell12);
            //


            GDB = new GetDatabase();
            GDB.ClientName = prms.CN;
            SelfieRegistration2Controller.GetDB2(GDB);
            // FOR INCOME FIELDS
            // INCOME UNDER EARNINGS
            con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID });
            con.myparameters.Add(new myParameters { ParameterName = "@payoutDate", mytype = SqlDbType.VarChar, Value = prms.PayoutDate });
            DataTable dt6 = con.GetDataTable("sp_Earnings_Payslip");


            GDB = new GetDatabase();
            GDB.ClientName = prms.CN;
            SelfieRegistration2Controller.GetDB2(GDB);
            con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID });
            con.myparameters.Add(new myParameters { ParameterName = "@year", mytype = SqlDbType.VarChar, Value = prms.ytd_year });
            con.myparameters.Add(new myParameters { ParameterName = "@payoutDate", mytype = SqlDbType.VarChar, Value = prms.PayoutDate });
            DataTable dt7 = con.GetDataTable("sp_Earnings_YTD_Payslip");

            // HEADER
            PdfPCell tabl4cell13 = new PdfPCell(new PdfPCell(new Paragraph("INCOME", FontFactory.GetFont("arial", 8, iTextSharp.text.Font.UNDERLINE)))) { Border = PdfPCell.NO_BORDER, HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT };
            tabl4cell13.Colspan = 4;
            tabl4cell13.PaddingTop = 10;
            tabl4cell13.PaddingBottom = 7;
            tabl4cell13.BorderWidth = 0;
            tabl4cell13.BackgroundColor = BaseColor.LIGHT_GRAY;
            table4.AddCell(tabl4cell13);

            if (dt6.Rows.Count > 0)
            {
                string desc = "", amount = "", ytdAmount = "";
                for (int i = 0; i < dt6.Rows.Count; i++)
                {
                    desc = dt6.Rows[i]["EarningsDescription"].ToString();
                    amount = dt6.Rows[i]["EarningsAmount"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt6.Rows[i]["EarningsAmount"].ToString()));

                    for (int ii = 0; ii < dt7.Rows.Count; ii++)
                    {
                        if (desc == dt7.Rows[ii]["EarningsDescription"].ToString())
                        {
                            ytdAmount = dt7.Rows[ii]["EarningsAmount"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt7.Rows[ii]["EarningsAmount"].ToString()));
                        }
                    }


                    PdfPCell tabl4cel13a = new PdfPCell(new PdfPCell(new Paragraph(desc, FontFactory.GetFont("arial", 8))));
                    tabl4cel13a.PaddingBottom = 7;
                    tabl4cel13a.BorderWidth = 0;
                    tabl4cel13a.BackgroundColor = BaseColor.LIGHT_GRAY;
                    table4.AddCell(tabl4cel13a);

                    PdfPCell tabl4cel13b = new PdfPCell(new PdfPCell(new Paragraph("", FontFactory.GetFont("arial", 8))));
                    tabl4cel13b.HorizontalAlignment = Element.ALIGN_RIGHT;
                    tabl4cel13b.PaddingBottom = 7;
                    tabl4cel13b.BorderWidth = 0;
                    tabl4cel13b.BackgroundColor = BaseColor.LIGHT_GRAY;
                    table4.AddCell(tabl4cel13b);

                    PdfPCell tabl4cel13c = new PdfPCell(new PdfPCell(new Paragraph(amount, FontFactory.GetFont("arial", 8))));
                    tabl4cel13c.HorizontalAlignment = Element.ALIGN_CENTER;
                    tabl4cel13c.PaddingBottom = 7;
                    tabl4cel13c.BorderWidth = 0;
                    tabl4cel13c.BackgroundColor = BaseColor.LIGHT_GRAY;
                    table4.AddCell(tabl4cel13c);

                    PdfPCell tabl4cel13d = new PdfPCell(new PdfPCell(new Paragraph("", FontFactory.GetFont("arial", 8))));
                    tabl4cel13d.HorizontalAlignment = Element.ALIGN_RIGHT;
                    tabl4cel13d.PaddingBottom = 7;
                    tabl4cel13d.BorderWidth = 0;
                    tabl4cel13d.BackgroundColor = BaseColor.LIGHT_GRAY;
                    table4.AddCell(tabl4cel13d);
                }
            }


            GDB = new GetDatabase();
            GDB.ClientName = prms.CN;
            SelfieRegistration2Controller.GetDB2(GDB);
            // INCOME UNDER OVERTIME
            con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID });
            con.myparameters.Add(new myParameters { ParameterName = "@payoutDate", mytype = SqlDbType.VarChar, Value = prms.PayoutDate });
            DataTable dt8 = con.GetDataTable("sp_GetOvertimeInfo_Payslip");


            GDB = new GetDatabase();
            GDB.ClientName = prms.CN;
            SelfieRegistration2Controller.GetDB2(GDB);
            con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID });
            con.myparameters.Add(new myParameters { ParameterName = "@year", mytype = SqlDbType.VarChar, Value = prms.ytd_year });
            con.myparameters.Add(new myParameters { ParameterName = "@payoutDate", mytype = SqlDbType.VarChar, Value = prms.PayoutDate });
            DataTable dt9 = con.GetDataTable("sp_GetOvertimeInfo_YTD_Payslip");

            if (dt8.Rows.Count > 0)
            {
                string OTDesc = "", OTHours = "", OTAmount = "", OT_YTDAmount = "";

                for (int i = 0; i < dt8.Rows.Count; i++)
                {
                    OTDesc = dt8.Rows[i]["overtimedescription"].ToString();
                    OTHours = dt8.Rows[i]["overtimehours"].ToString();
                    OTAmount = dt8.Rows[i]["overtimeamount"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt8.Rows[i]["overtimeamount"].ToString()));
                    OT_YTDAmount = dt9.Rows[i]["OvertimeAmount"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt9.Rows[i]["OvertimeAmount"].ToString()));

                    PdfPCell OT_tabl4cel13a = new PdfPCell(new PdfPCell(new Paragraph(OTDesc, FontFactory.GetFont("arial", 8))));
                    OT_tabl4cel13a.PaddingBottom = 7;
                    OT_tabl4cel13a.BorderWidth = 0;
                    OT_tabl4cel13a.BackgroundColor = BaseColor.LIGHT_GRAY;
                    table4.AddCell(OT_tabl4cel13a);

                    PdfPCell OT_tabl4cel13b = new PdfPCell(new PdfPCell(new Paragraph(OTHours, FontFactory.GetFont("arial", 8))));
                    OT_tabl4cel13b.HorizontalAlignment = Element.ALIGN_RIGHT;
                    OT_tabl4cel13b.PaddingBottom = 7;
                    OT_tabl4cel13b.BorderWidth = 0;
                    OT_tabl4cel13b.BackgroundColor = BaseColor.LIGHT_GRAY;
                    table4.AddCell(OT_tabl4cel13b);

                    PdfPCell OT_tabl4cel13c = new PdfPCell(new PdfPCell(new Paragraph(OTAmount, FontFactory.GetFont("arial", 8))));
                    OT_tabl4cel13c.HorizontalAlignment = Element.ALIGN_CENTER;
                    OT_tabl4cel13c.PaddingBottom = 7;
                    OT_tabl4cel13c.BorderWidth = 0;
                    OT_tabl4cel13c.BackgroundColor = BaseColor.LIGHT_GRAY;
                    table4.AddCell(OT_tabl4cel13c);

                    PdfPCell OT_tabl4cel13d = new PdfPCell(new PdfPCell(new Paragraph("", FontFactory.GetFont("arial", 8))));
                    OT_tabl4cel13d.HorizontalAlignment = Element.ALIGN_RIGHT;
                    OT_tabl4cel13d.PaddingBottom = 7;
                    OT_tabl4cel13d.BorderWidth = 0;
                    OT_tabl4cel13d.BackgroundColor = BaseColor.LIGHT_GRAY;
                    table4.AddCell(OT_tabl4cel13d);
                }
            }
            // DEDUCTIONS FROM LOG INFO
            if (dt16.Rows.Count > 0)
            {
                if (dt17.Rows[0]["absencesAmount"].ToString() != "0" && dt17.Rows[0]["absencesAmount"].ToString() != "0.0000")
                {
                    Absences_Hours = dt16.Rows[0]["Absences"].ToString();
                    Absences_Amount = dt16.Rows[0]["AbsentAmount"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt16.Rows[0]["AbsentAmount"].ToString()));
                    Absences_YTD_Amount = dt17.Rows[0]["absencesAmount"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt17.Rows[0]["absencesAmount"].ToString()));

                    PdfPCell Absences_tabl4cel14a = new PdfPCell(new PdfPCell(new Paragraph(AbsencesDesc, FontFactory.GetFont("arial", 8))));
                    Absences_tabl4cel14a.PaddingBottom = 7;
                    Absences_tabl4cel14a.BorderWidth = 0;
                    table4.AddCell(Absences_tabl4cel14a);

                    PdfPCell Absences_tabl4cel14b = new PdfPCell(new PdfPCell(new Paragraph(Absences_Hours, FontFactory.GetFont("arial", 8))));
                    Absences_tabl4cel14b.HorizontalAlignment = Element.ALIGN_RIGHT;
                    Absences_tabl4cel14b.PaddingBottom = 7;
                    Absences_tabl4cel14b.BorderWidth = 0;
                    table4.AddCell(Absences_tabl4cel14b);

                    PdfPCell Absences_tabl4cel14c = new PdfPCell(new PdfPCell(new Paragraph(Absences_Amount, FontFactory.GetFont("arial", 8))));
                    Absences_tabl4cel14c.HorizontalAlignment = Element.ALIGN_CENTER;
                    Absences_tabl4cel14c.PaddingBottom = 7;
                    Absences_tabl4cel14c.BorderWidth = 0;
                    table4.AddCell(Absences_tabl4cel14c);

                    PdfPCell Absences_tabl4cel14d = new PdfPCell(new PdfPCell(new Paragraph("", FontFactory.GetFont("arial", 8))));
                    Absences_tabl4cel14d.HorizontalAlignment = Element.ALIGN_RIGHT;
                    Absences_tabl4cel14d.PaddingBottom = 7;
                    Absences_tabl4cel14d.BorderWidth = 0;
                    table4.AddCell(Absences_tabl4cel14d);
                }

                if (dt17.Rows[0]["tardinessAmount"].ToString() != "0" && dt17.Rows[0]["tardinessAmount"].ToString() != "0.0000")
                {
                    Tardi_Hours = dt16.Rows[0]["Tardiness"].ToString();
                    Tardi_Amount = dt16.Rows[0]["TardinessAmount"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt16.Rows[0]["TardinessAmount"].ToString()));
                    Tardi_YTD_Amount = dt17.Rows[0]["tardinessAmount"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt17.Rows[0]["tardinessAmount"].ToString()));

                    PdfPCell Tardi_tabl4cel14a = new PdfPCell(new PdfPCell(new Paragraph(TardiDesc, FontFactory.GetFont("arial", 8))));
                    Tardi_tabl4cel14a.PaddingBottom = 7;
                    Tardi_tabl4cel14a.BorderWidth = 0;
                    table4.AddCell(Tardi_tabl4cel14a);

                    PdfPCell Tardi_tabl4cel14b = new PdfPCell(new PdfPCell(new Paragraph(Tardi_Hours, FontFactory.GetFont("arial", 8))));
                    Tardi_tabl4cel14b.HorizontalAlignment = Element.ALIGN_RIGHT;
                    Tardi_tabl4cel14b.PaddingBottom = 7;
                    Tardi_tabl4cel14b.BorderWidth = 0;
                    table4.AddCell(Tardi_tabl4cel14b);

                    PdfPCell Tardi_tabl4cel14c = new PdfPCell(new PdfPCell(new Paragraph(Tardi_Amount, FontFactory.GetFont("arial", 8))));
                    Tardi_tabl4cel14c.HorizontalAlignment = Element.ALIGN_CENTER;
                    Tardi_tabl4cel14c.PaddingBottom = 7;
                    Tardi_tabl4cel14c.BorderWidth = 0;
                    table4.AddCell(Tardi_tabl4cel14c);

                    PdfPCell Tardi_tabl4cel14d = new PdfPCell(new PdfPCell(new Paragraph("", FontFactory.GetFont("arial", 8))));
                    Tardi_tabl4cel14d.HorizontalAlignment = Element.ALIGN_RIGHT;
                    Tardi_tabl4cel14d.PaddingBottom = 7;
                    Tardi_tabl4cel14d.BorderWidth = 0;
                    table4.AddCell(Tardi_tabl4cel14d);
                }

                if (dt17.Rows[0]["undertimeAmount"].ToString() != "0" && dt17.Rows[0]["undertimeAmount"].ToString() != "0.0000")
                {
                    Undertime_Hours = dt16.Rows[0]["undertime"].ToString();
                    Undertime_Amount = dt16.Rows[0]["undertimeAmount"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt16.Rows[0]["undertimeAmount"].ToString()));
                    Undertime_YTD_Amount = dt17.Rows[0]["undertimeAmount"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt17.Rows[0]["undertimeAmount"].ToString()));

                    PdfPCell Undertime_tabl4cel14a = new PdfPCell(new PdfPCell(new Paragraph(UndertimeDesc, FontFactory.GetFont("arial", 8))));
                    Undertime_tabl4cel14a.PaddingBottom = 7;
                    Undertime_tabl4cel14a.BorderWidth = 0;
                    table4.AddCell(Undertime_tabl4cel14a);

                    PdfPCell Undertime_tabl4cel14b = new PdfPCell(new PdfPCell(new Paragraph(Undertime_Hours, FontFactory.GetFont("arial", 8))));
                    Undertime_tabl4cel14b.HorizontalAlignment = Element.ALIGN_RIGHT;
                    Undertime_tabl4cel14b.PaddingBottom = 7;
                    Undertime_tabl4cel14b.BorderWidth = 0;
                    table4.AddCell(Undertime_tabl4cel14b);

                    PdfPCell Undertime_tabl4cel14c = new PdfPCell(new PdfPCell(new Paragraph(Undertime_Amount, FontFactory.GetFont("arial", 8))));
                    Undertime_tabl4cel14c.HorizontalAlignment = Element.ALIGN_CENTER;
                    Undertime_tabl4cel14c.PaddingBottom = 7;
                    Undertime_tabl4cel14c.BorderWidth = 0;
                    table4.AddCell(Undertime_tabl4cel14c);

                    PdfPCell Undertime_tabl4cel14d = new PdfPCell(new PdfPCell(new Paragraph("", FontFactory.GetFont("arial", 8))));
                    Undertime_tabl4cel14d.HorizontalAlignment = Element.ALIGN_RIGHT;
                    Undertime_tabl4cel14d.PaddingBottom = 7;
                    Undertime_tabl4cel14d.BorderWidth = 0;
                    table4.AddCell(Undertime_tabl4cel14d);
                }

                if (dt17.Rows[0]["LWOPAmount"].ToString() != "0" && !string.IsNullOrWhiteSpace(dt17.Rows[0]["LWOPAmount"].ToString()) && dt17.Rows[0]["LWOPAmount"].ToString() != "0.0000" && dt17.Rows[0]["LWOPAmount"].ToString() != "0.00")
                {
                    LWOP_Hours = dt16.Rows[0]["LWOP"].ToString();
                    LWOP_Amount = dt16.Rows[0]["LWOPAmount"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt16.Rows[0]["LWOPAmount"].ToString()));
                    LWOP_YTD_Amount = dt17.Rows[0]["LWOPAmount"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt17.Rows[0]["LWOPAmount"].ToString()));

                    PdfPCell LWOP_tabl4cel14a = new PdfPCell(new PdfPCell(new Paragraph(LWOPDesc, FontFactory.GetFont("arial", 8))));
                    LWOP_tabl4cel14a.PaddingBottom = 7;
                    LWOP_tabl4cel14a.BorderWidth = 0;
                    table4.AddCell(LWOP_tabl4cel14a);

                    PdfPCell LWOP_tabl4cel14b = new PdfPCell(new PdfPCell(new Paragraph(LWOP_Hours, FontFactory.GetFont("arial", 8))));
                    LWOP_tabl4cel14b.HorizontalAlignment = Element.ALIGN_RIGHT;
                    LWOP_tabl4cel14b.PaddingBottom = 7;
                    LWOP_tabl4cel14b.BorderWidth = 0;
                    table4.AddCell(LWOP_tabl4cel14b);

                    PdfPCell LWOP_tabl4cel14c = new PdfPCell(new PdfPCell(new Paragraph(LWOP_Amount, FontFactory.GetFont("arial", 8))));
                    LWOP_tabl4cel14c.HorizontalAlignment = Element.ALIGN_CENTER;
                    LWOP_tabl4cel14c.PaddingBottom = 7;
                    LWOP_tabl4cel14c.BorderWidth = 0;
                    table4.AddCell(LWOP_tabl4cel14c);

                    PdfPCell LWOP_tabl4cel14d = new PdfPCell(new PdfPCell(new Paragraph("", FontFactory.GetFont("arial", 8))));
                    LWOP_tabl4cel14d.HorizontalAlignment = Element.ALIGN_RIGHT;
                    LWOP_tabl4cel14d.PaddingBottom = 7;
                    LWOP_tabl4cel14d.BorderWidth = 0;
                    table4.AddCell(LWOP_tabl4cel14d);
                }
            }
            // END

            // FOR DEDUCTIONS FIELDS
            // HEADER
            PdfPCell tabl4cell14 = new PdfPCell(new PdfPCell(new Paragraph("CONTRIBUTIONS", FontFactory.GetFont("arial", 8, iTextSharp.text.Font.UNDERLINE)))) { Border = PdfPCell.NO_BORDER, HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT };
            tabl4cell14.Colspan = 4;
            tabl4cell14.PaddingTop = 10;
            tabl4cell14.PaddingBottom = 7;
            tabl4cell14.BorderWidth = 0;
            table4.AddCell(tabl4cell14);


            GDB = new GetDatabase();
            GDB.ClientName = prms.CN;
            SelfieRegistration2Controller.GetDB2(GDB);
            // DEDUCTION FOR GOV CONTRIBUTIONS
            // FOR SSS
            con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID });
            con.myparameters.Add(new myParameters { ParameterName = "@payoutDate", mytype = SqlDbType.VarChar, Value = prms.PayoutDate });
            DataTable dt10 = con.GetDataTable("sp_GetGovernmentAmounts_SSS_Payslip");


            GDB = new GetDatabase();
            GDB.ClientName = prms.CN;
            SelfieRegistration2Controller.GetDB2(GDB);
            con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID });
            con.myparameters.Add(new myParameters { ParameterName = "@year", mytype = SqlDbType.VarChar, Value = prms.ytd_year });
            con.myparameters.Add(new myParameters { ParameterName = "@payoutDate", mytype = SqlDbType.VarChar, Value = prms.PayoutDate });
            DataTable dt11 = con.GetDataTable("sp_GetGovernmentAmounts_SSS_YTD_Payslip");

            if (dt10.Rows.Count > 0)
            {
                string SSSDesc = "SSS", SSSAmount = "", SSS_YTD_Amount = "";

                SSSAmount = dt10.Rows[0]["SSS"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt10.Rows[0]["SSS"].ToString()));
                SSS_YTD_Amount = dt11.Rows[0]["SSS"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt11.Rows[0]["SSS"].ToString()));

                PdfPCell SSS_tabl4cel14a = new PdfPCell(new PdfPCell(new Paragraph(SSSDesc, FontFactory.GetFont("arial", 8))));
                SSS_tabl4cel14a.PaddingBottom = 7;
                SSS_tabl4cel14a.BorderWidth = 0;
                table4.AddCell(SSS_tabl4cel14a);

                PdfPCell SSS_tabl4cel14b = new PdfPCell(new PdfPCell(new Paragraph("", FontFactory.GetFont("arial", 8))));
                SSS_tabl4cel14b.HorizontalAlignment = Element.ALIGN_RIGHT;
                SSS_tabl4cel14b.PaddingBottom = 7;
                SSS_tabl4cel14b.BorderWidth = 0;
                table4.AddCell(SSS_tabl4cel14b);

                PdfPCell SSS_tabl4cel14c = new PdfPCell(new PdfPCell(new Paragraph(SSSAmount, FontFactory.GetFont("arial", 8))));
                SSS_tabl4cel14c.HorizontalAlignment = Element.ALIGN_CENTER;
                SSS_tabl4cel14c.PaddingBottom = 7;
                SSS_tabl4cel14c.BorderWidth = 0;
                table4.AddCell(SSS_tabl4cel14c);

                PdfPCell SSS_tabl4cel14d = new PdfPCell(new PdfPCell(new Paragraph("", FontFactory.GetFont("arial", 8))));
                SSS_tabl4cel14d.HorizontalAlignment = Element.ALIGN_RIGHT;
                SSS_tabl4cel14d.PaddingBottom = 7;
                SSS_tabl4cel14d.BorderWidth = 0;
                table4.AddCell(SSS_tabl4cel14d);
            }


            GDB = new GetDatabase();
            GDB.ClientName = prms.CN;
            SelfieRegistration2Controller.GetDB2(GDB);
            // FOR PHILHEALTH
            con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID });
            con.myparameters.Add(new myParameters { ParameterName = "@payoutDate", mytype = SqlDbType.VarChar, Value = prms.PayoutDate });
            DataTable dt12 = con.GetDataTable("sp_GetGovernmentAmounts_Philhealth_Payslip");



            GDB = new GetDatabase();
            GDB.ClientName = prms.CN;
            SelfieRegistration2Controller.GetDB2(GDB);
            con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID });
            con.myparameters.Add(new myParameters { ParameterName = "@year", mytype = SqlDbType.VarChar, Value = prms.ytd_year });
            con.myparameters.Add(new myParameters { ParameterName = "@payoutDate", mytype = SqlDbType.VarChar, Value = prms.PayoutDate });
            DataTable dt13 = con.GetDataTable("sp_GetGovernmentAmounts_Philhealth_YTD_Payslip");

            if (dt12.Rows.Count > 0)
            {
                string PHealthDesc = "Philhealth", PHealthAmount = "", PHealth_YTD_Amount = "";

                PHealthAmount = dt12.Rows[0]["Philhealth"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt12.Rows[0]["Philhealth"].ToString()));
                PHealth_YTD_Amount = dt13.Rows[0]["philhealth"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt13.Rows[0]["philhealth"].ToString()));

                PdfPCell PHLHEALTH_tabl4cel14a = new PdfPCell(new PdfPCell(new Paragraph(PHealthDesc, FontFactory.GetFont("arial", 8))));
                PHLHEALTH_tabl4cel14a.PaddingBottom = 7;
                PHLHEALTH_tabl4cel14a.BorderWidth = 0;
                table4.AddCell(PHLHEALTH_tabl4cel14a);

                PdfPCell PHLHEALTH_tabl4cel14b = new PdfPCell(new PdfPCell(new Paragraph("", FontFactory.GetFont("arial", 8))));
                PHLHEALTH_tabl4cel14b.HorizontalAlignment = Element.ALIGN_RIGHT;
                PHLHEALTH_tabl4cel14b.PaddingBottom = 7;
                PHLHEALTH_tabl4cel14b.BorderWidth = 0;
                table4.AddCell(PHLHEALTH_tabl4cel14b);

                PdfPCell PHLHEALTH_tabl4cel14c = new PdfPCell(new PdfPCell(new Paragraph(PHealthAmount, FontFactory.GetFont("arial", 8))));
                PHLHEALTH_tabl4cel14c.HorizontalAlignment = Element.ALIGN_CENTER;
                PHLHEALTH_tabl4cel14c.PaddingBottom = 7;
                PHLHEALTH_tabl4cel14c.BorderWidth = 0;
                table4.AddCell(PHLHEALTH_tabl4cel14c);

                PdfPCell PHLHEALTH_tabl4cel14d = new PdfPCell(new PdfPCell(new Paragraph("", FontFactory.GetFont("arial", 8))));
                PHLHEALTH_tabl4cel14d.HorizontalAlignment = Element.ALIGN_RIGHT;
                PHLHEALTH_tabl4cel14d.PaddingBottom = 7;
                PHLHEALTH_tabl4cel14d.BorderWidth = 0;
                table4.AddCell(PHLHEALTH_tabl4cel14d);
            }


            GDB = new GetDatabase();
            GDB.ClientName = prms.CN;
            SelfieRegistration2Controller.GetDB2(GDB);
            // FOR HDMF
            con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID });
            con.myparameters.Add(new myParameters { ParameterName = "@payoutDate", mytype = SqlDbType.VarChar, Value = prms.PayoutDate });
            DataTable dt14 = con.GetDataTable("sp_GetGovernmentAmounts_Pagibig_Payslip");


            GDB = new GetDatabase();
            GDB.ClientName = prms.CN;
            SelfieRegistration2Controller.GetDB2(GDB);
            con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID });
            con.myparameters.Add(new myParameters { ParameterName = "@year", mytype = SqlDbType.VarChar, Value = prms.ytd_year });
            con.myparameters.Add(new myParameters { ParameterName = "@payoutDate", mytype = SqlDbType.VarChar, Value = prms.PayoutDate });
            DataTable dt15 = con.GetDataTable("sp_GetGovernmentAmounts_Pagibig_YTD_Payslip");

            if (dt14.Rows.Count > 0)
            {
                string HDMFDesc = "HDMF", HDMFAmount = "", HDMF_YTD_Amount = "";
                HDMFAmount = dt14.Rows[0]["HDMF"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt14.Rows[0]["HDMF"].ToString()));
                HDMF_YTD_Amount = dt15.Rows[0]["Pagibig_Employee"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt15.Rows[0]["Pagibig_Employee"].ToString()));

                PdfPCell HDMF_tabl4cel14a = new PdfPCell(new PdfPCell(new Paragraph(HDMFDesc, FontFactory.GetFont("arial", 8))));
                HDMF_tabl4cel14a.PaddingBottom = 7;
                HDMF_tabl4cel14a.BorderWidth = 0;
                table4.AddCell(HDMF_tabl4cel14a);

                PdfPCell HDMF_tabl4cel14b = new PdfPCell(new PdfPCell(new Paragraph("", FontFactory.GetFont("arial", 8))));
                HDMF_tabl4cel14b.HorizontalAlignment = Element.ALIGN_RIGHT;
                HDMF_tabl4cel14b.PaddingBottom = 7;
                HDMF_tabl4cel14b.BorderWidth = 0;
                table4.AddCell(HDMF_tabl4cel14b);

                PdfPCell HDMF_tabl4cel14c = new PdfPCell(new PdfPCell(new Paragraph(HDMFAmount, FontFactory.GetFont("arial", 8))));
                HDMF_tabl4cel14c.HorizontalAlignment = Element.ALIGN_CENTER;
                HDMF_tabl4cel14c.PaddingBottom = 7;
                HDMF_tabl4cel14c.BorderWidth = 0;
                table4.AddCell(HDMF_tabl4cel14c);

                PdfPCell HDMF_tabl4cel14d = new PdfPCell(new PdfPCell(new Paragraph("", FontFactory.GetFont("arial", 8))));
                HDMF_tabl4cel14d.HorizontalAlignment = Element.ALIGN_RIGHT;
                HDMF_tabl4cel14d.PaddingBottom = 7;
                HDMF_tabl4cel14d.BorderWidth = 0;
                table4.AddCell(HDMF_tabl4cel14d);
            }




            GDB = new GetDatabase();
            GDB.ClientName = prms.CN;
            SelfieRegistration2Controller.GetDB2(GDB);


            // FOR LOANS FIELD
            PdfPCell tabl4cell15 = new PdfPCell(new PdfPCell(new Paragraph("DEDUCTIONS", FontFactory.GetFont("arial", 8, iTextSharp.text.Font.UNDERLINE))));
            tabl4cell14.PaddingTop = 30;
            tabl4cell15.PaddingBottom = 7;
            tabl4cell15.BorderWidth = 0;
            table4.AddCell(tabl4cell15);

            //PdfPCell tabl4cell16 = new PdfPCell(new PdfPCell(new Paragraph("BALANCE LEFT", FontFactory.GetFont("arial", 8, iTextSharp.text.Font.BOLD))));
            PdfPCell tabl4cell16 = new PdfPCell(new PdfPCell(new Paragraph("", FontFactory.GetFont("arial", 8, iTextSharp.text.Font.BOLD))));
            tabl4cell16.HorizontalAlignment = Element.ALIGN_RIGHT;
            tabl4cell16.PaddingBottom = 7;
            tabl4cell14.PaddingTop = 20;
            tabl4cell16.BorderWidth = 0;
            table4.AddCell(tabl4cell16);

            PdfPCell tabl4cell17 = new PdfPCell(new PdfPCell(new Paragraph("", FontFactory.GetFont("arial", 8, iTextSharp.text.Font.BOLD))));
            tabl4cell17.PaddingBottom = 7;
            tabl4cell17.BorderWidth = 0;
            table4.AddCell(tabl4cell17);

            PdfPCell tabl4cell18 = new PdfPCell(new PdfPCell(new Paragraph("", FontFactory.GetFont("arial", 8, iTextSharp.text.Font.BOLD))));
            tabl4cell18.HorizontalAlignment = Element.ALIGN_RIGHT;
            tabl4cell18.PaddingBottom = 7;
            tabl4cell18.BorderWidth = 0;
            table4.AddCell(tabl4cell18);


            GDB = new GetDatabase();
            GDB.ClientName = prms.CN;
            SelfieRegistration2Controller.GetDB2(GDB);
            con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID });
            con.myparameters.Add(new myParameters { ParameterName = "@payoutDate", mytype = SqlDbType.VarChar, Value = prms.PayoutDate });
            DataTable dt20 = con.GetDataTable("sp_GetLoans_Payslip");


            GDB = new GetDatabase();
            GDB.ClientName = prms.CN;
            SelfieRegistration2Controller.GetDB2(GDB);
            con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID });
            con.myparameters.Add(new myParameters { ParameterName = "@year", mytype = SqlDbType.VarChar, Value = prms.ytd_year });
            con.myparameters.Add(new myParameters { ParameterName = "@payoutDate", mytype = SqlDbType.VarChar, Value = prms.PayoutDate });
            DataTable dt21 = con.GetDataTable("sp_GetLoans_YTD_Payslip");

            if (dt20.Rows.Count > 0)
            {
                string LoanName = "", Amortization_Amount = "", Amortization_YTD_Amount = "";

                for (int i = 0; i < dt20.Rows.Count; i++)
                {
                    LoanName = dt20.Rows[i]["LoanName"].ToString();
                    Amortization_Amount = dt20.Rows[i]["AmortizationAmount"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt20.Rows[i]["AmortizationAmount"].ToString()));
                    Amortization_YTD_Amount = dt21.Rows[i]["AmortizationAmount"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt21.Rows[i]["AmortizationAmount"].ToString()));

                    PdfPCell Loan_tabl4cel14a = new PdfPCell(new PdfPCell(new Paragraph(LoanName, FontFactory.GetFont("arial", 8))));
                    Loan_tabl4cel14a.PaddingBottom = 7;
                    Loan_tabl4cel14a.BorderWidth = 0;
                    table4.AddCell(Loan_tabl4cel14a);

                    PdfPCell Loan_tabl4cel14b = new PdfPCell(new PdfPCell(new Paragraph("", FontFactory.GetFont("arial", 8))));
                    Loan_tabl4cel14b.HorizontalAlignment = Element.ALIGN_RIGHT;
                    Loan_tabl4cel14b.PaddingBottom = 7;
                    Loan_tabl4cel14b.BorderWidth = 0;
                    table4.AddCell(Loan_tabl4cel14b);

                    PdfPCell Loan_tabl4cel14c = new PdfPCell(new PdfPCell(new Paragraph(Amortization_Amount, FontFactory.GetFont("arial", 8))));
                    Loan_tabl4cel14c.HorizontalAlignment = Element.ALIGN_CENTER;
                    Loan_tabl4cel14c.PaddingBottom = 7;
                    Loan_tabl4cel14c.BorderWidth = 0;
                    table4.AddCell(Loan_tabl4cel14c);

                    PdfPCell Loan_tabl4cel14d = new PdfPCell(new PdfPCell(new Paragraph("", FontFactory.GetFont("arial", 8))));
                    Loan_tabl4cel14d.HorizontalAlignment = Element.ALIGN_RIGHT;
                    Loan_tabl4cel14d.PaddingBottom = 7;
                    Loan_tabl4cel14d.BorderWidth = 0;
                    table4.AddCell(Loan_tabl4cel14d);
                }
            }
            //
            // OTHER DEDUCTIONS
            con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID });
            con.myparameters.Add(new myParameters { ParameterName = "@payoutDate", mytype = SqlDbType.VarChar, Value = prms.PayoutDate });
            DataTable dt18 = con.GetDataTable("sp_GetDeductions_Payslip");


            GDB = new GetDatabase();
            GDB.ClientName = prms.CN;
            SelfieRegistration2Controller.GetDB2(GDB);
            con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID });
            con.myparameters.Add(new myParameters { ParameterName = "@year", mytype = SqlDbType.VarChar, Value = prms.ytd_year });
            con.myparameters.Add(new myParameters { ParameterName = "@payoutDate", mytype = SqlDbType.VarChar, Value = prms.PayoutDate });
            DataTable dt19 = con.GetDataTable("sp_GetDeductions_YTD_Payslip");

            if (dt18.Rows.Count > 0)
            {
                for (int i = 0; i < dt18.Rows.Count; i++)
                {
                    string DeductionDesc = "", Deduction_Amount = "", Deduction_YTD_Amount = "";

                    DeductionDesc = dt18.Rows[i]["deductiondescription"].ToString();
                    Deduction_Amount = dt18.Rows[i]["deductionamount"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt18.Rows[i]["deductionamount"].ToString()));
                    Deduction_YTD_Amount = dt19.Rows[i]["deductionAmount"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt19.Rows[i]["deductionAmount"].ToString()));

                    PdfPCell OtherDeduction_tabl4cel14a = new PdfPCell(new PdfPCell(new Paragraph(DeductionDesc, FontFactory.GetFont("arial", 8))));
                    OtherDeduction_tabl4cel14a.PaddingBottom = 7;
                    OtherDeduction_tabl4cel14a.BorderWidth = 0;
                    table4.AddCell(OtherDeduction_tabl4cel14a);

                    PdfPCell OtherDeduction_tabl4cel14b = new PdfPCell(new PdfPCell(new Paragraph("", FontFactory.GetFont("arial", 8))));
                    OtherDeduction_tabl4cel14b.HorizontalAlignment = Element.ALIGN_RIGHT;
                    OtherDeduction_tabl4cel14b.PaddingBottom = 7;
                    OtherDeduction_tabl4cel14b.BorderWidth = 0;
                    table4.AddCell(OtherDeduction_tabl4cel14b);

                    PdfPCell OtherDeduction_tabl4cel14c = new PdfPCell(new PdfPCell(new Paragraph(Deduction_Amount, FontFactory.GetFont("arial", 8))));
                    OtherDeduction_tabl4cel14c.HorizontalAlignment = Element.ALIGN_CENTER;
                    OtherDeduction_tabl4cel14c.PaddingBottom = 7;
                    OtherDeduction_tabl4cel14c.BorderWidth = 0;
                    table4.AddCell(OtherDeduction_tabl4cel14c);

                    PdfPCell OtherDeduction_tabl4cel14d = new PdfPCell(new PdfPCell(new Paragraph("", FontFactory.GetFont("arial", 8))));
                    OtherDeduction_tabl4cel14d.HorizontalAlignment = Element.ALIGN_RIGHT;
                    OtherDeduction_tabl4cel14d.PaddingBottom = 7;
                    OtherDeduction_tabl4cel14d.BorderWidth = 0;
                    table4.AddCell(OtherDeduction_tabl4cel14d);
                }
            }
            //
            //
            PdfPCell tabl4cell19 = new PdfPCell(new PdfPCell(new Paragraph("WithHolding Tax", FontFactory.GetFont("arial", 8))));
            tabl4cell19.PaddingBottom = 7;
            tabl4cell19.BorderWidth = 0;
            tabl4cell19.BackgroundColor = BaseColor.LIGHT_GRAY;
            table4.AddCell(tabl4cell19);

            PdfPCell tabl4cell20 = new PdfPCell(new PdfPCell(new Paragraph("", FontFactory.GetFont("arial", 8))));
            tabl4cell20.HorizontalAlignment = Element.ALIGN_RIGHT;
            tabl4cell20.PaddingBottom = 7;
            tabl4cell20.BorderWidth = 0;
            tabl4cell20.BackgroundColor = BaseColor.LIGHT_GRAY;
            table4.AddCell(tabl4cell20);

            PdfPCell tabl4cell21 = new PdfPCell(new PdfPCell(new Paragraph(whtax, FontFactory.GetFont("arial", 8))));
            tabl4cell21.HorizontalAlignment = Element.ALIGN_CENTER;
            tabl4cell21.PaddingBottom = 7;
            tabl4cell21.BorderWidth = 0;
            tabl4cell21.BackgroundColor = BaseColor.LIGHT_GRAY;
            table4.AddCell(tabl4cell21);

            PdfPCell tabl4cell22 = new PdfPCell(new PdfPCell(new Paragraph("", FontFactory.GetFont("arial", 8))));
            tabl4cell22.HorizontalAlignment = Element.ALIGN_RIGHT;
            tabl4cell22.PaddingBottom = 7;
            tabl4cell22.BorderWidth = 0;
            tabl4cell22.BackgroundColor = BaseColor.LIGHT_GRAY;
            table4.AddCell(tabl4cell22);
            //

            //
            PdfPCell tabl4cell23 = new PdfPCell(new PdfPCell(new Paragraph("Net Pay", FontFactory.GetFont("arial", 8))));
            tabl4cell23.PaddingBottom = 7;
            tabl4cell23.BorderWidth = 0;
            table4.AddCell(tabl4cell23);

            PdfPCell tabl4cell24 = new PdfPCell(new PdfPCell(new Paragraph("", FontFactory.GetFont("arial", 8))));
            tabl4cell24.HorizontalAlignment = Element.ALIGN_RIGHT;
            tabl4cell24.PaddingBottom = 7;
            tabl4cell24.BorderWidth = 0;
            table4.AddCell(tabl4cell24);

            PdfPCell tabl4cell25 = new PdfPCell(new PdfPCell(new Paragraph(netpay, FontFactory.GetFont("arial", 8))));
            tabl4cell25.HorizontalAlignment = Element.ALIGN_CENTER;
            tabl4cell25.PaddingBottom = 7;
            tabl4cell25.BorderWidth = 0;
            table4.AddCell(tabl4cell25);

            PdfPCell tabl4cell26 = new PdfPCell(new PdfPCell(new Paragraph("", FontFactory.GetFont("arial", 8))));
            tabl4cell26.HorizontalAlignment = Element.ALIGN_RIGHT;
            tabl4cell26.PaddingBottom = 7;
            tabl4cell26.BorderWidth = 0;
            table4.AddCell(tabl4cell26);
            //

            //
            PdfPCell tabl4cell27 = new PdfPCell(new PdfPCell(new Paragraph("", FontFactory.GetFont("arial", 8, iTextSharp.text.Font.UNDERLINE)))) { Border = PdfPCell.NO_BORDER, HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT };
            tabl4cell27.Colspan = 4;
            tabl4cell27.PaddingTop = 10;
            tabl4cell27.PaddingBottom = 7;
            tabl4cell27.BorderWidth = 0;
            table4.AddCell(tabl4cell27);
            //
            table2.AddCell(table4);


            PdfPCell cell22 = new PdfPCell() { Border = PdfPCell.NO_BORDER };
            cell22.Colspan = 2;


            table1.AddCell(cell11);
            table1.AddCell(cell12);
            table2.AddCell(cell22);
            document.Add(table0);
            document.Add(table1);
            document.Add(table2);
            document.Close();

            return "success";

        }

        [HttpPost]
        [Route("UploadOfflineActivity")]
        public string UploadOfflineActivity(OfflineActivity param)
        {

            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = param.CN;
            SelfieRegistration2Controller.GetDB2(GDB);

            try
            {
                DataTable dt = new DataTable();
                dt.Columns.Add("EmpID");
                dt.Columns.Add("RecordDT");
                dt.Columns.Add("Reason");
                DataRow dr = null;
                for (int i = 0; i < param.EmpID.Length; i++)
                {
                    dr = dt.NewRow();
                    dr["EmpID"] = param.EmpID[i].ToString();
                    dr["RecordDT"] = param.RecordDT[i].ToString();
                    dr["Reason"] = param.Reason[i].ToString();
                    dt.Rows.Add(dr);
                }

                Connection conn = new Connection();
                conn.myparameters.Add(new myParameters { ParameterName = "@OfflineData", mytype = SqlDbType.Structured, Value = dt });
                DataTable dtbl = conn.GetDataTable("sp_InsertOfflineActivity");

                return "Success";
            }
            catch (Exception e)
            {
                return e.Message.ToString();
            }
        }

        [HttpPost]
        [Route("generatepdfV4")]
        public string generatepdfsV4(generatePDF prms)
        {
            try
            {
                GetDatabase GDB = new GetDatabase();
                GDB.ClientName = prms.CN;
                SelfieRegistration2Controller.GetDB2(GDB);

                // DATA VARIABLE DECLARATION
                string startdate = "", enddate = "";
                string payday = "";
                string fullname = "", empid = "", datejoined = "", position = "", department = "", TIN = "", SSS = "", PHILHEALTH = "", PAGIBIG = "";
                string basicpay = "", grosspay = "", netpay = "", whtax = "", ytdBasicPay = "", ytdGrossPay = "", ytdNetPay = "", ytdWhTax = "";
                string HourlyRate = "", HoursWorked = "", AbsencesDesc = "Absences", TardiDesc = "Tardiness", UndertimeDesc = "Undertime", LWOPDesc = "LWOP", Monthly = "";
                string Absences_Hours = "", Tardi_Hours = "", Undertime_Hours = "", LWOP_Hours = "";
                string Absences_Amount = "", Tardi_Amount = "", Undertime_Amount = "", LWOP_Amount = "";
                string Absences_YTD_Amount = "", Tardi_YTD_Amount = "", Undertime_YTD_Amount = "", LWOP_YTD_Amount = "";
                string CompanyName = "";
                string filename = "";
                // FOR COMPANY NAME

                Connection conn = new Connection();
                conn.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.VarChar, Value = prms.EmpID });
                DataTable dtbl = conn.GetDataTable("sp_get_CompanyName_MobileDynamicPayslip");
                foreach (DataRow item in dtbl.Rows)
                {
                    CompanyName = item["TradingName"].ToString();
                }
                // FOR PAY PERIOD FIELD

                GDB = new GetDatabase();
                GDB.ClientName = prms.CN;
                SelfieRegistration2Controller.GetDB2(GDB);
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID });
                con.myparameters.Add(new myParameters { ParameterName = "@payoutDate", mytype = SqlDbType.VarChar, Value = prms.PayoutDate });
                DataTable DT = con.GetDataTable("sp_GetPayPeriod_Payslip");
                foreach (DataRow item in DT.Rows)
                {
                    startdate = item["StartDate"].ToString();
                    enddate = item["EndDate"].ToString();
                }


                GDB = new GetDatabase();
                GDB.ClientName = prms.CN;
                SelfieRegistration2Controller.GetDB2(GDB);
                // FOR PAY DATE FIELD
                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID });
                DataTable DT1 = con.GetDataTable("sp_GetPayoutSchemeList");
                foreach (DataRow item in DT1.Rows)
                {
                    payday = item["PayoutDate"].ToString();
                }

                GDB = new GetDatabase();
                GDB.ClientName = prms.CN;
                SelfieRegistration2Controller.GetDB2(GDB);
                // EMPLOYEE DETAILS FIELD
                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID });
                DataTable dt2 = con.GetDataTable("sp_GetEmployeeInfo_Payslip");
                foreach (DataRow item in dt2.Rows)
                {
                    fullname = item["EmpName"].ToString();
                    empid = item["EmpID"].ToString();
                    datejoined = item["DateJoined"].ToString();
                    department = item["BusinessUnit"].ToString();
                    position = item["JobDesc"].ToString();
                }

                GDB = new GetDatabase();
                GDB.ClientName = prms.CN;
                SelfieRegistration2Controller.GetDB2(GDB);
                // FOR GOVERNMENT ID FIELDS
                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID });
                DataTable dt3 = con.GetDataTable("sp_GetGovIDs_Payslip");
                foreach (DataRow item in dt3.Rows)
                {
                    if (item["IDTypes"].ToString() == "SSS")
                    {
                        SSS = item["IDCode"].ToString();
                    }
                    else if (item["IDTypes"].ToString() == "Philhealth")
                    {
                        PHILHEALTH = item["IDCode"].ToString();
                    }
                    else if (item["IDTypes"].ToString() == "Pag-Ibig")
                    {
                        PAGIBIG = item["IDCode"].ToString();
                    }
                    else
                    {
                        TIN = item["IDCode"].ToString();
                    }
                }

                GDB = new GetDatabase();
                GDB.ClientName = prms.CN;
                SelfieRegistration2Controller.GetDB2(GDB);
                // FOR GROSS PAY, BASIC PAY AND NET PAY FIELDS
                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID });
                con.myparameters.Add(new myParameters { ParameterName = "@payoutDate", mytype = SqlDbType.VarChar, Value = prms.PayoutDate });
                DataTable dt4 = con.GetDataTable("sp_GetPay_Payslip");
                foreach (DataRow item in dt4.Rows)
                {
                    basicpay = item["Basic Pay"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(item["Basic Pay"].ToString()));
                    grosspay = item["Gross Pay"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(item["Gross Pay"].ToString()));
                    netpay = item["Net Pay"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(item["Net Pay"].ToString()));
                    whtax = item["WithHolding Tax"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(item["WithHolding Tax"].ToString()));

                    //basicpay = String.Format("{0:n2}", decimal.Parse(basicpay));
                }
                // END


                GDB = new GetDatabase();
                GDB.ClientName = prms.CN;
                SelfieRegistration2Controller.GetDB2(GDB);
                // YTD GROSS PAY, BASIC PAY AND NET PAY FIELDS
                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID });
                con.myparameters.Add(new myParameters { ParameterName = "@year", mytype = SqlDbType.VarChar, Value = prms.ytd_year });
                con.myparameters.Add(new myParameters { ParameterName = "@payoutDate", mytype = SqlDbType.VarChar, Value = prms.PayoutDate });
                DataTable dt5 = con.GetDataTable("sp_GetPay_YTD_Payslip");
                foreach (DataRow item in dt5.Rows)
                {
                    ytdBasicPay = item["basicpay"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(item["basicpay"].ToString()));
                    ytdGrossPay = item["grosspay"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(item["grosspay"].ToString()));
                    ytdWhTax = item["whtax"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(item["whtax"].ToString()));
                    ytdNetPay = item["netpay"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(item["netpay"].ToString()));
                }

                GDB = new GetDatabase();
                GDB.ClientName = prms.CN;
                SelfieRegistration2Controller.GetDB2(GDB);
                // FOR LOGS INFO
                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID });
                con.myparameters.Add(new myParameters { ParameterName = "@payoutDate", mytype = SqlDbType.VarChar, Value = prms.PayoutDate });
                DataTable dt16 = con.GetDataTable("sp_GetLogInfo_Payslip");

                if (dt16.Rows.Count > 0)
                {
                    HourlyRate = dt16.Rows[0]["HourlyRate"] == DBNull.Value || dt16.Rows[0]["HourlyRate"].ToString() == "" ? "" : String.Format("{0:n2}", decimal.Parse(dt16.Rows[0]["HourlyRate"].ToString()));
                    HoursWorked = dt16.Rows[0]["HoursWorked"] == DBNull.Value || dt16.Rows[0]["HoursWorked"].ToString() == "" ? "" : String.Format("{0:n2}", decimal.Parse(dt16.Rows[0]["HoursWorked"].ToString()));
                    //Monthly = dt16.Rows[0]["Monthly"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt16.Rows[0]["Monthly"].ToString()));
                }

                // GET MONTHLY SALARY

                GDB = new GetDatabase();
                GDB.ClientName = prms.CN;
                SelfieRegistration2Controller.GetDB2(GDB);
                DataTable dtMontlySalary;
                if (prms.CN == "NPB")
                {
                    con = new Connection();
                    con.myparameters.Add(new myParameters { ParameterName = "@empid", mytype = SqlDbType.VarChar, Value = prms.EmpID });
                    con.myparameters.Add(new myParameters { ParameterName = "@payoutdate", mytype = SqlDbType.VarChar, Value = prms.PayoutDate });
                    dtMontlySalary = con.GetDataTable("sp_GetMonthlySalary_Payslip");
                }
                else
                {
                    con = new Connection();
                    con.myparameters.Add(new myParameters { ParameterName = "@empid", mytype = SqlDbType.VarChar, Value = prms.EmpID });
                    dtMontlySalary = con.GetDataTable("sp_GetMonthlySalary_Payslip");
                }

                if (dtMontlySalary.Rows.Count > 0)
                {
                    Monthly = dtMontlySalary.Rows[0]["Salary"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dtMontlySalary.Rows[0]["Salary"].ToString()));
                }

                GDB = new GetDatabase();
                GDB.ClientName = prms.CN;
                SelfieRegistration2Controller.GetDB2(GDB);
                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID });
                con.myparameters.Add(new myParameters { ParameterName = "@year", mytype = SqlDbType.VarChar, Value = prms.ytd_year });
                con.myparameters.Add(new myParameters { ParameterName = "@payoutDate", mytype = SqlDbType.VarChar, Value = prms.PayoutDate });
                DataTable dt17 = con.GetDataTable("sp_GetLogInfo_YTD_Payslip");
                string timebuild = DateTime.Now.ToString("hhmmtt");
                filename = "payslip-" + prms.EmpID + "-" + prms.PayoutDate + "_" + timebuild + ".pdf";
                System.IO.FileStream fs = new FileStream(HttpContext.Current.Server.MapPath("pdf") + "\\" + "payslip-" + prms.EmpID + "-" + prms.PayoutDate + "_" + timebuild + ".pdf", FileMode.Create);
                //System.IO.FileStream fb = new FileStream(HttpContext.Current.Server.MapPath("pdf") + "\\" + "ilm.png", FileMode.Open);
                string imagepath = HttpContext.Current.Server.MapPath("Images");
                Document document = new Document(PageSize.A4, 25, 25, 30, 30);
                PdfWriter writer = PdfWriter.GetInstance(document, fs);
                //PdfWriter writering = PdfWriter.GetInstance(document, fb);
                document.Open();

                PdfPTable table0 = new PdfPTable(1);

                PdfPCell cell0 = new PdfPCell(new iTextSharp.text.Phrase(CompanyName, FontFactory.GetFont("Arial", 12, Font.BOLD))) { Border = 0, HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT, VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE };
                cell0.PaddingTop = 10;
                //PdfPCell cell01 = new PdfPCell(new iTextSharp.text.Phrase("Lima Technology Center  Lipa City, Batangas Philippines 4217", FontFactory.GetFont("Arial", 11, Font.NORMAL))) { Border = 0, HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT, VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE };
                //cell01.PaddingBottom = 7;

                //Image logoimage = Image.GetInstance(Path.Combine(imagepath, "yokohamalogo.jpg"));
                //logoimage.ScaleAbsolute(100, 40);
                //logoimage.SetAbsolutePosition(100, 30);

                //PdfPCell LogoCell = new PdfPCell(logoimage, true);

                table0.AddCell(cell0);
                ////table0.AddCell(LogoCell);
                //table0.AddCell(cell01);
                //document.Add(LogoCell);


                PdfPTable table1 = new PdfPTable(2);

                /*var titleFont = new Font(Font.FontFamily.UNDEFINED, 12);
                var informationDetails = new Font(Font.FontFamily.UNDEFINED, 12);*/

                PdfPCell cell11 = new PdfPCell(new iTextSharp.text.Phrase("PAY PERIOD\n" + startdate + " - " + enddate)) { Border = PdfPCell.BOTTOM_BORDER | PdfPCell.TOP_BORDER, HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER, VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE };
                PdfPCell cell12 = new PdfPCell(new iTextSharp.text.Phrase("PAY DAY\n" + prms.PayoutDate)) { Border = PdfPCell.BOTTOM_BORDER | PdfPCell.TOP_BORDER, HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER, VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE };
                cell11.PaddingTop = 10;
                cell12.PaddingTop = 10;
                cell11.PaddingBottom = 7;
                cell12.PaddingBottom = 7;
                /* cell12.AddElement(new Paragraph("PAY DAY", titleFont));
                cell12.AddElement(new Paragraph("-", informationDetails));*/

                PdfPTable table2 = new PdfPTable(2);
                float[] widths = new float[] { 100f, 200f };
                float[] heights = new float[] { 300f };
                table2.SetWidths(widths);
                //table2.DefaultCell.FixedHeight = 500f;
                table2.DefaultCell.Border = Rectangle.NO_BORDER;
                //PdfPCell cell21 = new PdfPCell(new Paragraph("Danilo Vicente Sulla Jr")) { Border = PdfPCell.RIGHT_BORDER, HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER, VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE };
                //cell21.PaddingTop = 20;

                PdfPTable table3 = new PdfPTable(2);
                table3.TotalWidth = 100;
                table3.DefaultCell.FixedHeight = 600f;
                table3.DefaultCell.Border = Rectangle.RIGHT_BORDER;

                PdfPCell tabl3cell0 = new PdfPCell(new PdfPCell(new Paragraph(fullname, FontFactory.GetFont("Arial", 12, Font.BOLD)))) { Border = PdfPCell.NO_BORDER, HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER, VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE };
                tabl3cell0.Colspan = 2;
                tabl3cell0.PaddingTop = 20;
                tabl3cell0.PaddingBottom = 20;
                tabl3cell0.BorderWidth = 0;
                tabl3cell0.BorderWidthRight = 1;
                table3.AddCell(tabl3cell0);


                PdfPCell tabl3cell1 = new PdfPCell(new PdfPCell(new Paragraph("EmpID", FontFactory.GetFont("arial", 8))));
                tabl3cell1.PaddingBottom = 7;
                tabl3cell1.BorderWidth = 0;
                tabl3cell1.BorderWidthBottom = 1;
                table3.AddCell(tabl3cell1);

                PdfPCell tabl3cell2 = new PdfPCell(new PdfPCell(new Paragraph(empid, FontFactory.GetFont("arial", 8, Font.BOLD))));
                tabl3cell2.HorizontalAlignment = Element.ALIGN_RIGHT;
                tabl3cell2.PaddingBottom = 7;
                tabl3cell2.BorderWidth = 0;
                tabl3cell2.BorderWidthBottom = 1;
                tabl3cell2.BorderWidthRight = 1;
                table3.AddCell(tabl3cell2);

                PdfPCell tabl3cell3 = new PdfPCell(new PdfPCell(new Paragraph("Tax Category", FontFactory.GetFont("arial", 8))));
                tabl3cell3.PaddingBottom = 7;
                tabl3cell3.BorderWidth = 0;
                tabl3cell3.BorderWidthBottom = 1;
                table3.AddCell(tabl3cell3);

                PdfPCell tabl3cell4 = new PdfPCell(new PdfPCell(new Paragraph("--", FontFactory.GetFont("arial", 8, Font.BOLD))));
                tabl3cell4.HorizontalAlignment = Element.ALIGN_RIGHT;
                tabl3cell4.PaddingBottom = 7;
                tabl3cell4.BorderWidth = 0;
                tabl3cell4.BorderWidthBottom = 1;
                tabl3cell4.BorderWidthRight = 1;
                table3.AddCell(tabl3cell4);

                PdfPCell tabl3cell5 = new PdfPCell(new PdfPCell(new Paragraph("Hourly Rate", FontFactory.GetFont("arial", 8))));
                tabl3cell5.PaddingBottom = 7;
                tabl3cell5.BorderWidth = 0;
                tabl3cell5.BorderWidthBottom = 1;
                table3.AddCell(tabl3cell5);

                PdfPCell tabl3cell6 = new PdfPCell(new PdfPCell(new Paragraph(HourlyRate, FontFactory.GetFont("arial", 8, Font.BOLD))));
                tabl3cell6.HorizontalAlignment = Element.ALIGN_RIGHT;
                tabl3cell6.PaddingBottom = 7;
                tabl3cell6.BorderWidth = 0;
                tabl3cell6.BorderWidthBottom = 1;
                tabl3cell6.BorderWidthRight = 1;
                table3.AddCell(tabl3cell6);

                PdfPCell tabl3cell7 = new PdfPCell(new PdfPCell(new Paragraph("Regular Hours", FontFactory.GetFont("arial", 8))));
                tabl3cell7.PaddingBottom = 7;
                tabl3cell7.BorderWidth = 0;
                tabl3cell7.BorderWidthBottom = 1;
                table3.AddCell(tabl3cell7);

                PdfPCell tabl3cell8 = new PdfPCell(new PdfPCell(new Paragraph(HoursWorked, FontFactory.GetFont("arial", 8, Font.BOLD))));
                tabl3cell8.HorizontalAlignment = Element.ALIGN_RIGHT;
                tabl3cell8.PaddingBottom = 7;
                tabl3cell8.BorderWidth = 0;
                tabl3cell8.BorderWidthBottom = 1;
                tabl3cell8.BorderWidthRight = 1;
                table3.AddCell(tabl3cell8);

                PdfPCell tabl3cell9 = new PdfPCell(new PdfPCell(new Paragraph(getSalaryScheme(Monthly.ToString()) + " Salary", FontFactory.GetFont("arial", 8))));
                tabl3cell9.PaddingBottom = 7;
                tabl3cell9.BorderWidth = 0;
                tabl3cell9.BorderWidthBottom = 1;
                table3.AddCell(tabl3cell9);

                PdfPCell tabl3cell10 = new PdfPCell(new PdfPCell(new Paragraph(Monthly, FontFactory.GetFont("arial", 8, Font.BOLD))));
                tabl3cell10.HorizontalAlignment = Element.ALIGN_RIGHT;
                tabl3cell10.PaddingBottom = 7;
                tabl3cell10.BorderWidth = 0;
                tabl3cell10.BorderWidthBottom = 1;
                tabl3cell10.BorderWidthRight = 1;
                table3.AddCell(tabl3cell10);

                PdfPCell tabl3cell11 = new PdfPCell(new PdfPCell(new Paragraph("Date Joined", FontFactory.GetFont("arial", 8))));
                tabl3cell11.PaddingBottom = 7;
                tabl3cell11.BorderWidth = 0;
                tabl3cell11.BorderWidthBottom = 1;
                table3.AddCell(tabl3cell11);

                PdfPCell tabl3cell12 = new PdfPCell(new PdfPCell(new Paragraph(datejoined, FontFactory.GetFont("arial", 8, Font.BOLD))));
                tabl3cell12.HorizontalAlignment = Element.ALIGN_RIGHT;
                tabl3cell12.PaddingBottom = 7;
                tabl3cell12.BorderWidth = 0;
                tabl3cell12.BorderWidthBottom = 1;
                tabl3cell12.BorderWidthRight = 1;
                table3.AddCell(tabl3cell12);

                PdfPCell tabl3cell13 = new PdfPCell(new PdfPCell(new Paragraph("Position", FontFactory.GetFont("arial", 8))));
                tabl3cell13.PaddingBottom = 7;
                tabl3cell13.BorderWidth = 0;
                tabl3cell13.BorderWidthBottom = 1;
                table3.AddCell(tabl3cell13);

                PdfPCell tabl3cell14 = new PdfPCell(new PdfPCell(new Paragraph(position, FontFactory.GetFont("arial", 8, Font.BOLD))));
                tabl3cell14.HorizontalAlignment = Element.ALIGN_RIGHT;
                tabl3cell14.PaddingBottom = 7;
                tabl3cell14.BorderWidth = 0;
                tabl3cell14.BorderWidthBottom = 1;
                tabl3cell14.BorderWidthRight = 1;
                table3.AddCell(tabl3cell14);

                PdfPCell tabl3cell15 = new PdfPCell(new PdfPCell(new Paragraph("Department", FontFactory.GetFont("arial", 8))));
                tabl3cell15.PaddingBottom = 7;
                tabl3cell15.BorderWidth = 0;
                tabl3cell15.BorderWidthBottom = 1;
                table3.AddCell(tabl3cell15);

                PdfPCell tabl3cell16 = new PdfPCell(new PdfPCell(new Paragraph(department, FontFactory.GetFont("arial", 8, Font.BOLD))));
                tabl3cell16.HorizontalAlignment = Element.ALIGN_RIGHT;
                tabl3cell16.PaddingBottom = 7;
                tabl3cell16.BorderWidth = 0;
                tabl3cell16.BorderWidthBottom = 1;
                tabl3cell16.BorderWidthRight = 1;
                table3.AddCell(tabl3cell16);

                PdfPCell tabl3cell17 = new PdfPCell(new PdfPCell(new Paragraph("Philhealth", FontFactory.GetFont("arial", 8))));
                tabl3cell17.PaddingBottom = 7;
                tabl3cell17.BorderWidth = 0;
                tabl3cell17.BorderWidthBottom = 1;
                table3.AddCell(tabl3cell17);

                PdfPCell tabl3cell18 = new PdfPCell(new PdfPCell(new Paragraph(PHILHEALTH, FontFactory.GetFont("arial", 8, Font.BOLD))));
                tabl3cell18.HorizontalAlignment = Element.ALIGN_RIGHT;
                tabl3cell18.PaddingBottom = 7;
                tabl3cell18.BorderWidth = 0;
                tabl3cell18.BorderWidthBottom = 1;
                tabl3cell18.BorderWidthRight = 1;
                table3.AddCell(tabl3cell18);

                PdfPCell tabl3cell19 = new PdfPCell(new PdfPCell(new Paragraph("Pag-Ibig", FontFactory.GetFont("arial", 8))));
                tabl3cell19.PaddingBottom = 7;
                tabl3cell19.BorderWidth = 0;
                tabl3cell19.BorderWidthBottom = 1;
                table3.AddCell(tabl3cell19);

                PdfPCell tabl3cell20 = new PdfPCell(new PdfPCell(new Paragraph(PAGIBIG, FontFactory.GetFont("arial", 8, Font.BOLD))));
                tabl3cell20.HorizontalAlignment = Element.ALIGN_RIGHT;
                tabl3cell20.PaddingBottom = 7;
                tabl3cell20.BorderWidth = 0;
                tabl3cell20.BorderWidthBottom = 1;
                tabl3cell20.BorderWidthRight = 1;
                table3.AddCell(tabl3cell20);

                PdfPCell tabl3cell21 = new PdfPCell(new PdfPCell(new Paragraph("TIN", FontFactory.GetFont("arial", 8))));
                tabl3cell21.PaddingBottom = 7;
                tabl3cell21.BorderWidth = 0;
                tabl3cell21.BorderWidthBottom = 1;

                table3.AddCell(tabl3cell21);

                PdfPCell tabl3cell22 = new PdfPCell(new PdfPCell(new Paragraph(TIN, FontFactory.GetFont("arial", 8, Font.BOLD))));
                tabl3cell22.HorizontalAlignment = Element.ALIGN_RIGHT;
                tabl3cell22.PaddingBottom = 7;
                tabl3cell22.BorderWidth = 0;
                tabl3cell22.BorderWidthBottom = 1;
                tabl3cell22.BorderWidthRight = 1;
                table3.AddCell(tabl3cell22);

                PdfPCell tabl3cell23 = new PdfPCell(new PdfPCell(new Paragraph("SSS", FontFactory.GetFont("arial", 8))));
                tabl3cell23.PaddingBottom = 7;
                tabl3cell23.BorderWidth = 0;
                table3.AddCell(tabl3cell23);

                PdfPCell tabl3cell24 = new PdfPCell(new PdfPCell(new Paragraph(SSS, FontFactory.GetFont("arial", 8, Font.BOLD))));
                tabl3cell24.HorizontalAlignment = Element.ALIGN_RIGHT;
                tabl3cell24.PaddingBottom = 7;
                tabl3cell24.BorderWidth = 0;
                tabl3cell24.BorderWidthRight = 1;
                table3.AddCell(tabl3cell24);


                table2.AddCell(table3);

                //
                PdfPTable table4 = new PdfPTable(4);
                table4.TotalWidth = 100;
                table4.DefaultCell.Border = Rectangle.NO_BORDER;

                PdfPCell tabl4cell1 = new PdfPCell(new PdfPCell(new Paragraph("", FontFactory.GetFont("arial", 8))));
                tabl4cell1.PaddingBottom = 7;
                tabl4cell1.BorderWidth = 0;
                table4.AddCell(tabl4cell1);

                PdfPCell tabl4cell2 = new PdfPCell(new PdfPCell(new Paragraph("HOURS", FontFactory.GetFont("arial", 8, Font.BOLD))));
                tabl4cell2.HorizontalAlignment = Element.ALIGN_RIGHT;
                tabl4cell2.PaddingBottom = 7;
                tabl4cell2.BorderWidth = 0;
                table4.AddCell(tabl4cell2);

                PdfPCell tabl4cell3 = new PdfPCell(new PdfPCell(new Paragraph("CURRENT (PHP)", FontFactory.GetFont("arial", 8, Font.BOLD))));
                tabl4cell3.HorizontalAlignment = Element.ALIGN_CENTER;
                tabl4cell3.PaddingBottom = 7;
                tabl4cell3.BorderWidth = 0;
                table4.AddCell(tabl4cell3);

                PdfPCell tabl4cell4 = new PdfPCell(new PdfPCell(new Paragraph("YEAR TO DATE(PHP)", FontFactory.GetFont("arial", 8, Font.BOLD))));
                tabl4cell4.HorizontalAlignment = Element.ALIGN_RIGHT;
                tabl4cell4.PaddingBottom = 7;
                tabl4cell4.BorderWidth = 0;
                table4.AddCell(tabl4cell4);


                //
                PdfPCell tabl4cell5 = new PdfPCell(new PdfPCell(new Paragraph("Basic Pay", FontFactory.GetFont("arial", 8))));
                tabl4cell5.PaddingBottom = 7;
                tabl4cell5.BorderWidth = 0;
                table4.AddCell(tabl4cell5);

                PdfPCell tabl4cell6 = new PdfPCell(new PdfPCell(new Paragraph("", FontFactory.GetFont("arial", 8))));
                tabl4cell6.HorizontalAlignment = Element.ALIGN_RIGHT;
                tabl4cell6.PaddingBottom = 7;
                tabl4cell6.BorderWidth = 0;
                table4.AddCell(tabl4cell6);

                PdfPCell tabl4cell7 = new PdfPCell(new PdfPCell(new Paragraph(basicpay, FontFactory.GetFont("arial", 8))));
                tabl4cell7.HorizontalAlignment = Element.ALIGN_CENTER;
                tabl4cell7.PaddingBottom = 7;
                tabl4cell7.BorderWidth = 0;
                table4.AddCell(tabl4cell7);

                PdfPCell tabl4cell8 = new PdfPCell(new PdfPCell(new Paragraph(ytdBasicPay, FontFactory.GetFont("arial", 8))));
                tabl4cell8.HorizontalAlignment = Element.ALIGN_RIGHT;
                tabl4cell8.PaddingBottom = 7;
                tabl4cell8.BorderWidth = 0;
                table4.AddCell(tabl4cell8);
                //

                //
                PdfPCell tabl4cell9 = new PdfPCell(new PdfPCell(new Paragraph("Gross Pay", FontFactory.GetFont("arial", 8))));
                tabl4cell9.PaddingBottom = 7;
                tabl4cell9.BorderWidth = 0;
                table4.AddCell(tabl4cell9);

                PdfPCell tabl4cell10 = new PdfPCell(new PdfPCell(new Paragraph("", FontFactory.GetFont("arial", 8))));
                tabl4cell10.HorizontalAlignment = Element.ALIGN_RIGHT;
                tabl4cell10.PaddingBottom = 7;
                tabl4cell10.BorderWidth = 0;
                table4.AddCell(tabl4cell10);

                PdfPCell tabl4cell11 = new PdfPCell(new PdfPCell(new Paragraph(grosspay, FontFactory.GetFont("arial", 8))));
                tabl4cell11.HorizontalAlignment = Element.ALIGN_CENTER;
                tabl4cell11.PaddingBottom = 7;
                tabl4cell11.BorderWidth = 0;
                table4.AddCell(tabl4cell11);

                PdfPCell tabl4cell12 = new PdfPCell(new PdfPCell(new Paragraph(ytdGrossPay, FontFactory.GetFont("arial", 8))));
                tabl4cell12.HorizontalAlignment = Element.ALIGN_RIGHT;
                tabl4cell12.PaddingBottom = 7;
                tabl4cell12.BorderWidth = 0;
                table4.AddCell(tabl4cell12);
                //


                GDB = new GetDatabase();
                GDB.ClientName = prms.CN;
                SelfieRegistration2Controller.GetDB2(GDB);
                // FOR INCOME FIELDS
                // INCOME UNDER EARNINGS
                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID });
                con.myparameters.Add(new myParameters { ParameterName = "@payoutDate", mytype = SqlDbType.VarChar, Value = prms.PayoutDate });
                DataTable dt6 = con.GetDataTable("sp_Earnings_Payslip");


                GDB = new GetDatabase();
                GDB.ClientName = prms.CN;
                SelfieRegistration2Controller.GetDB2(GDB);
                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID });
                con.myparameters.Add(new myParameters { ParameterName = "@year", mytype = SqlDbType.VarChar, Value = prms.ytd_year });
                con.myparameters.Add(new myParameters { ParameterName = "@payoutDate", mytype = SqlDbType.VarChar, Value = prms.PayoutDate });
                DataTable dt7 = con.GetDataTable("sp_Earnings_YTD_Payslip");

                // HEADER
                PdfPCell tabl4cell13 = new PdfPCell(new PdfPCell(new Paragraph("INCOME", FontFactory.GetFont("arial", 8, Font.UNDERLINE)))) { Border = PdfPCell.NO_BORDER, HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT };
                tabl4cell13.Colspan = 4;
                tabl4cell13.PaddingTop = 10;
                tabl4cell13.PaddingBottom = 7;
                tabl4cell13.BorderWidth = 0;
                tabl4cell13.BackgroundColor = BaseColor.LIGHT_GRAY;
                table4.AddCell(tabl4cell13);

                if (dt6.Rows.Count > 0)
                {
                    string desc = "", amount = "", ytdAmount = "";
                    for (int i = 0; i < dt6.Rows.Count; i++)
                    {
                        desc = dt6.Rows[i]["EarningsDescription"].ToString();
                        amount = dt6.Rows[i]["EarningsAmount"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt6.Rows[i]["EarningsAmount"].ToString()));

                        for (int ii = 0; ii < dt7.Rows.Count; ii++)
                        {
                            if (desc == dt7.Rows[ii]["EarningsDescription"].ToString())
                            {
                                ytdAmount = dt7.Rows[ii]["EarningsAmount"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt7.Rows[ii]["EarningsAmount"].ToString()));
                            }
                        }


                        PdfPCell tabl4cel13a = new PdfPCell(new PdfPCell(new Paragraph(desc, FontFactory.GetFont("arial", 8))));
                        tabl4cel13a.PaddingBottom = 7;
                        tabl4cel13a.BorderWidth = 0;
                        tabl4cel13a.BackgroundColor = BaseColor.LIGHT_GRAY;
                        table4.AddCell(tabl4cel13a);

                        PdfPCell tabl4cel13b = new PdfPCell(new PdfPCell(new Paragraph("", FontFactory.GetFont("arial", 8))));
                        tabl4cel13b.HorizontalAlignment = Element.ALIGN_RIGHT;
                        tabl4cel13b.PaddingBottom = 7;
                        tabl4cel13b.BorderWidth = 0;
                        tabl4cel13b.BackgroundColor = BaseColor.LIGHT_GRAY;
                        table4.AddCell(tabl4cel13b);

                        PdfPCell tabl4cel13c = new PdfPCell(new PdfPCell(new Paragraph(amount, FontFactory.GetFont("arial", 8))));
                        tabl4cel13c.HorizontalAlignment = Element.ALIGN_CENTER;
                        tabl4cel13c.PaddingBottom = 7;
                        tabl4cel13c.BorderWidth = 0;
                        tabl4cel13c.BackgroundColor = BaseColor.LIGHT_GRAY;
                        table4.AddCell(tabl4cel13c);

                        PdfPCell tabl4cel13d = new PdfPCell(new PdfPCell(new Paragraph(ytdAmount, FontFactory.GetFont("arial", 8))));
                        tabl4cel13d.HorizontalAlignment = Element.ALIGN_RIGHT;
                        tabl4cel13d.PaddingBottom = 7;
                        tabl4cel13d.BorderWidth = 0;
                        tabl4cel13d.BackgroundColor = BaseColor.LIGHT_GRAY;
                        table4.AddCell(tabl4cel13d);
                    }
                }


                GDB = new GetDatabase();
                GDB.ClientName = prms.CN;
                SelfieRegistration2Controller.GetDB2(GDB);
                // INCOME UNDER OVERTIME
                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID });
                con.myparameters.Add(new myParameters { ParameterName = "@payoutDate", mytype = SqlDbType.VarChar, Value = prms.PayoutDate });
                DataTable dt8 = con.GetDataTable("sp_GetOvertimeInfo_Payslip");


                GDB = new GetDatabase();
                GDB.ClientName = prms.CN;
                SelfieRegistration2Controller.GetDB2(GDB);
                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID });
                con.myparameters.Add(new myParameters { ParameterName = "@year", mytype = SqlDbType.VarChar, Value = prms.ytd_year });
                con.myparameters.Add(new myParameters { ParameterName = "@payoutDate", mytype = SqlDbType.VarChar, Value = prms.PayoutDate });
                DataTable dt9 = con.GetDataTable("sp_GetOvertimeInfo_YTD_Payslip");

                if (dt8.Rows.Count > 0)
                {
                    string OTDesc = "", OTHours = "", OTAmount = "", OT_YTDAmount = "";

                    for (int i = 0; i < dt8.Rows.Count; i++)
                    {
                        OTDesc = dt8.Rows[i]["overtimedescription"].ToString();
                        OTHours = dt8.Rows[i]["overtimehours"].ToString();
                        OTAmount = dt8.Rows[i]["overtimeamount"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt8.Rows[i]["overtimeamount"].ToString()));
                        OT_YTDAmount = dt9.Rows[i]["OvertimeAmount"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt9.Rows[i]["OvertimeAmount"].ToString()));

                        PdfPCell OT_tabl4cel13a = new PdfPCell(new PdfPCell(new Paragraph(OTDesc, FontFactory.GetFont("arial", 8))));
                        OT_tabl4cel13a.PaddingBottom = 7;
                        OT_tabl4cel13a.BorderWidth = 0;
                        OT_tabl4cel13a.BackgroundColor = BaseColor.LIGHT_GRAY;
                        table4.AddCell(OT_tabl4cel13a);

                        PdfPCell OT_tabl4cel13b = new PdfPCell(new PdfPCell(new Paragraph(OTHours, FontFactory.GetFont("arial", 8))));
                        OT_tabl4cel13b.HorizontalAlignment = Element.ALIGN_RIGHT;
                        OT_tabl4cel13b.PaddingBottom = 7;
                        OT_tabl4cel13b.BorderWidth = 0;
                        OT_tabl4cel13b.BackgroundColor = BaseColor.LIGHT_GRAY;
                        table4.AddCell(OT_tabl4cel13b);

                        PdfPCell OT_tabl4cel13c = new PdfPCell(new PdfPCell(new Paragraph(OTAmount, FontFactory.GetFont("arial", 8))));
                        OT_tabl4cel13c.HorizontalAlignment = Element.ALIGN_CENTER;
                        OT_tabl4cel13c.PaddingBottom = 7;
                        OT_tabl4cel13c.BorderWidth = 0;
                        OT_tabl4cel13c.BackgroundColor = BaseColor.LIGHT_GRAY;
                        table4.AddCell(OT_tabl4cel13c);

                        PdfPCell OT_tabl4cel13d = new PdfPCell(new PdfPCell(new Paragraph(OT_YTDAmount, FontFactory.GetFont("arial", 8))));
                        OT_tabl4cel13d.HorizontalAlignment = Element.ALIGN_RIGHT;
                        OT_tabl4cel13d.PaddingBottom = 7;
                        OT_tabl4cel13d.BorderWidth = 0;
                        OT_tabl4cel13d.BackgroundColor = BaseColor.LIGHT_GRAY;
                        table4.AddCell(OT_tabl4cel13d);
                    }
                }
                // DEDUCTIONS FROM LOG INFO
                if (dt16.Rows.Count > 0)
                {
                    if (dt17.Rows[0]["absencesAmount"].ToString() != "0" && dt17.Rows[0]["absencesAmount"].ToString() != "0.0000")
                    {
                        Absences_Hours = dt16.Rows[0]["Absences"].ToString();
                        Absences_Amount = dt16.Rows[0]["AbsentAmount"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt16.Rows[0]["AbsentAmount"].ToString()));
                        Absences_YTD_Amount = dt17.Rows[0]["absencesAmount"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt17.Rows[0]["absencesAmount"].ToString()));

                        PdfPCell Absences_tabl4cel14a = new PdfPCell(new PdfPCell(new Paragraph(AbsencesDesc, FontFactory.GetFont("arial", 8))));
                        Absences_tabl4cel14a.PaddingBottom = 7;
                        Absences_tabl4cel14a.BorderWidth = 0;
                        table4.AddCell(Absences_tabl4cel14a);

                        PdfPCell Absences_tabl4cel14b = new PdfPCell(new PdfPCell(new Paragraph(Absences_Hours, FontFactory.GetFont("arial", 8))));
                        Absences_tabl4cel14b.HorizontalAlignment = Element.ALIGN_RIGHT;
                        Absences_tabl4cel14b.PaddingBottom = 7;
                        Absences_tabl4cel14b.BorderWidth = 0;
                        table4.AddCell(Absences_tabl4cel14b);

                        PdfPCell Absences_tabl4cel14c = new PdfPCell(new PdfPCell(new Paragraph(Absences_Amount, FontFactory.GetFont("arial", 8))));
                        Absences_tabl4cel14c.HorizontalAlignment = Element.ALIGN_CENTER;
                        Absences_tabl4cel14c.PaddingBottom = 7;
                        Absences_tabl4cel14c.BorderWidth = 0;
                        table4.AddCell(Absences_tabl4cel14c);

                        PdfPCell Absences_tabl4cel14d = new PdfPCell(new PdfPCell(new Paragraph(Absences_YTD_Amount, FontFactory.GetFont("arial", 8))));
                        Absences_tabl4cel14d.HorizontalAlignment = Element.ALIGN_RIGHT;
                        Absences_tabl4cel14d.PaddingBottom = 7;
                        Absences_tabl4cel14d.BorderWidth = 0;
                        table4.AddCell(Absences_tabl4cel14d);
                    }

                    if (dt17.Rows[0]["tardinessAmount"].ToString() != "0" && dt17.Rows[0]["tardinessAmount"].ToString() != "0.0000")
                    {
                        Tardi_Hours = dt16.Rows[0]["Tardiness"].ToString();
                        Tardi_Amount = dt16.Rows[0]["TardinessAmount"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt16.Rows[0]["TardinessAmount"].ToString()));
                        Tardi_YTD_Amount = dt17.Rows[0]["tardinessAmount"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt17.Rows[0]["tardinessAmount"].ToString()));

                        PdfPCell Tardi_tabl4cel14a = new PdfPCell(new PdfPCell(new Paragraph(TardiDesc, FontFactory.GetFont("arial", 8))));
                        Tardi_tabl4cel14a.PaddingBottom = 7;
                        Tardi_tabl4cel14a.BorderWidth = 0;
                        table4.AddCell(Tardi_tabl4cel14a);

                        PdfPCell Tardi_tabl4cel14b = new PdfPCell(new PdfPCell(new Paragraph(Tardi_Hours, FontFactory.GetFont("arial", 8))));
                        Tardi_tabl4cel14b.HorizontalAlignment = Element.ALIGN_RIGHT;
                        Tardi_tabl4cel14b.PaddingBottom = 7;
                        Tardi_tabl4cel14b.BorderWidth = 0;
                        table4.AddCell(Tardi_tabl4cel14b);

                        PdfPCell Tardi_tabl4cel14c = new PdfPCell(new PdfPCell(new Paragraph(Tardi_Amount, FontFactory.GetFont("arial", 8))));
                        Tardi_tabl4cel14c.HorizontalAlignment = Element.ALIGN_CENTER;
                        Tardi_tabl4cel14c.PaddingBottom = 7;
                        Tardi_tabl4cel14c.BorderWidth = 0;
                        table4.AddCell(Tardi_tabl4cel14c);

                        PdfPCell Tardi_tabl4cel14d = new PdfPCell(new PdfPCell(new Paragraph(Tardi_YTD_Amount, FontFactory.GetFont("arial", 8))));
                        Tardi_tabl4cel14d.HorizontalAlignment = Element.ALIGN_RIGHT;
                        Tardi_tabl4cel14d.PaddingBottom = 7;
                        Tardi_tabl4cel14d.BorderWidth = 0;
                        table4.AddCell(Tardi_tabl4cel14d);
                    }

                    if (dt17.Rows[0]["undertimeAmount"].ToString() != "0" && dt17.Rows[0]["undertimeAmount"].ToString() != "0.0000")
                    {
                        Undertime_Hours = dt16.Rows[0]["undertime"].ToString();
                        Undertime_Amount = dt16.Rows[0]["undertimeAmount"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt16.Rows[0]["undertimeAmount"].ToString()));
                        Undertime_YTD_Amount = dt17.Rows[0]["undertimeAmount"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt17.Rows[0]["undertimeAmount"].ToString()));

                        PdfPCell Undertime_tabl4cel14a = new PdfPCell(new PdfPCell(new Paragraph(UndertimeDesc, FontFactory.GetFont("arial", 8))));
                        Undertime_tabl4cel14a.PaddingBottom = 7;
                        Undertime_tabl4cel14a.BorderWidth = 0;
                        table4.AddCell(Undertime_tabl4cel14a);

                        PdfPCell Undertime_tabl4cel14b = new PdfPCell(new PdfPCell(new Paragraph(Undertime_Hours, FontFactory.GetFont("arial", 8))));
                        Undertime_tabl4cel14b.HorizontalAlignment = Element.ALIGN_RIGHT;
                        Undertime_tabl4cel14b.PaddingBottom = 7;
                        Undertime_tabl4cel14b.BorderWidth = 0;
                        table4.AddCell(Undertime_tabl4cel14b);

                        PdfPCell Undertime_tabl4cel14c = new PdfPCell(new PdfPCell(new Paragraph(Undertime_Amount, FontFactory.GetFont("arial", 8))));
                        Undertime_tabl4cel14c.HorizontalAlignment = Element.ALIGN_CENTER;
                        Undertime_tabl4cel14c.PaddingBottom = 7;
                        Undertime_tabl4cel14c.BorderWidth = 0;
                        table4.AddCell(Undertime_tabl4cel14c);

                        PdfPCell Undertime_tabl4cel14d = new PdfPCell(new PdfPCell(new Paragraph(Undertime_YTD_Amount, FontFactory.GetFont("arial", 8))));
                        Undertime_tabl4cel14d.HorizontalAlignment = Element.ALIGN_RIGHT;
                        Undertime_tabl4cel14d.PaddingBottom = 7;
                        Undertime_tabl4cel14d.BorderWidth = 0;
                        table4.AddCell(Undertime_tabl4cel14d);
                    }

                    if (dt17.Rows[0]["LWOPAmount"].ToString() != "0" && !string.IsNullOrWhiteSpace(dt17.Rows[0]["LWOPAmount"].ToString()) && dt17.Rows[0]["LWOPAmount"].ToString() != "0.0000" && dt17.Rows[0]["LWOPAmount"].ToString() != "0.00")
                    {
                        LWOP_Hours = dt16.Rows[0]["LWOP"].ToString();
                        LWOP_Amount = dt16.Rows[0]["LWOPAmount"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt16.Rows[0]["LWOPAmount"].ToString()));
                        LWOP_YTD_Amount = dt17.Rows[0]["LWOPAmount"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt17.Rows[0]["LWOPAmount"].ToString()));

                        PdfPCell LWOP_tabl4cel14a = new PdfPCell(new PdfPCell(new Paragraph(LWOPDesc, FontFactory.GetFont("arial", 8))));
                        LWOP_tabl4cel14a.PaddingBottom = 7;
                        LWOP_tabl4cel14a.BorderWidth = 0;
                        table4.AddCell(LWOP_tabl4cel14a);

                        PdfPCell LWOP_tabl4cel14b = new PdfPCell(new PdfPCell(new Paragraph(LWOP_Hours, FontFactory.GetFont("arial", 8))));
                        LWOP_tabl4cel14b.HorizontalAlignment = Element.ALIGN_RIGHT;
                        LWOP_tabl4cel14b.PaddingBottom = 7;
                        LWOP_tabl4cel14b.BorderWidth = 0;
                        table4.AddCell(LWOP_tabl4cel14b);

                        PdfPCell LWOP_tabl4cel14c = new PdfPCell(new PdfPCell(new Paragraph(LWOP_Amount, FontFactory.GetFont("arial", 8))));
                        LWOP_tabl4cel14c.HorizontalAlignment = Element.ALIGN_CENTER;
                        LWOP_tabl4cel14c.PaddingBottom = 7;
                        LWOP_tabl4cel14c.BorderWidth = 0;
                        table4.AddCell(LWOP_tabl4cel14c);

                        PdfPCell LWOP_tabl4cel14d = new PdfPCell(new PdfPCell(new Paragraph(LWOP_YTD_Amount, FontFactory.GetFont("arial", 8))));
                        LWOP_tabl4cel14d.HorizontalAlignment = Element.ALIGN_RIGHT;
                        LWOP_tabl4cel14d.PaddingBottom = 7;
                        LWOP_tabl4cel14d.BorderWidth = 0;
                        table4.AddCell(LWOP_tabl4cel14d);
                    }
                }
                // END

                // FOR DEDUCTIONS FIELDS
                // HEADER
                PdfPCell tabl4cell14 = new PdfPCell(new PdfPCell(new Paragraph("CONTRIBUTIONS", FontFactory.GetFont("arial", 8, Font.UNDERLINE)))) { Border = PdfPCell.NO_BORDER, HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT };
                tabl4cell14.Colspan = 4;
                tabl4cell14.PaddingTop = 10;
                tabl4cell14.PaddingBottom = 7;
                tabl4cell14.BorderWidth = 0;
                table4.AddCell(tabl4cell14);


                GDB = new GetDatabase();
                GDB.ClientName = prms.CN;
                SelfieRegistration2Controller.GetDB2(GDB);
                // DEDUCTION FOR GOV CONTRIBUTIONS
                // FOR SSS
                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID });
                con.myparameters.Add(new myParameters { ParameterName = "@payoutDate", mytype = SqlDbType.VarChar, Value = prms.PayoutDate });
                DataTable dt10 = con.GetDataTable("sp_GetGovernmentAmounts_SSS_Payslip");


                GDB = new GetDatabase();
                GDB.ClientName = prms.CN;
                SelfieRegistration2Controller.GetDB2(GDB);
                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID });
                con.myparameters.Add(new myParameters { ParameterName = "@year", mytype = SqlDbType.VarChar, Value = prms.ytd_year });
                con.myparameters.Add(new myParameters { ParameterName = "@payoutDate", mytype = SqlDbType.VarChar, Value = prms.PayoutDate });
                DataTable dt11 = con.GetDataTable("sp_GetGovernmentAmounts_SSS_YTD_Payslip");

                if (dt10.Rows.Count > 0)
                {
                    string SSSDesc = "SSS", SSSAmount = "", SSS_YTD_Amount = "";

                    SSSAmount = dt10.Rows[0]["SSS"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt10.Rows[0]["SSS"].ToString()));
                    SSS_YTD_Amount = dt11.Rows[0]["SSS"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt11.Rows[0]["SSS"].ToString()));

                    PdfPCell SSS_tabl4cel14a = new PdfPCell(new PdfPCell(new Paragraph(SSSDesc, FontFactory.GetFont("arial", 8))));
                    SSS_tabl4cel14a.PaddingBottom = 7;
                    SSS_tabl4cel14a.BorderWidth = 0;
                    table4.AddCell(SSS_tabl4cel14a);

                    PdfPCell SSS_tabl4cel14b = new PdfPCell(new PdfPCell(new Paragraph("", FontFactory.GetFont("arial", 8))));
                    SSS_tabl4cel14b.HorizontalAlignment = Element.ALIGN_RIGHT;
                    SSS_tabl4cel14b.PaddingBottom = 7;
                    SSS_tabl4cel14b.BorderWidth = 0;
                    table4.AddCell(SSS_tabl4cel14b);

                    PdfPCell SSS_tabl4cel14c = new PdfPCell(new PdfPCell(new Paragraph(SSSAmount, FontFactory.GetFont("arial", 8))));
                    SSS_tabl4cel14c.HorizontalAlignment = Element.ALIGN_CENTER;
                    SSS_tabl4cel14c.PaddingBottom = 7;
                    SSS_tabl4cel14c.BorderWidth = 0;
                    table4.AddCell(SSS_tabl4cel14c);

                    PdfPCell SSS_tabl4cel14d = new PdfPCell(new PdfPCell(new Paragraph(SSS_YTD_Amount, FontFactory.GetFont("arial", 8))));
                    SSS_tabl4cel14d.HorizontalAlignment = Element.ALIGN_RIGHT;
                    SSS_tabl4cel14d.PaddingBottom = 7;
                    SSS_tabl4cel14d.BorderWidth = 0;
                    table4.AddCell(SSS_tabl4cel14d);
                }


                GDB = new GetDatabase();
                GDB.ClientName = prms.CN;
                SelfieRegistration2Controller.GetDB2(GDB);
                // FOR PHILHEALTH
                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID });
                con.myparameters.Add(new myParameters { ParameterName = "@payoutDate", mytype = SqlDbType.VarChar, Value = prms.PayoutDate });
                DataTable dt12 = con.GetDataTable("sp_GetGovernmentAmounts_Philhealth_Payslip");



                GDB = new GetDatabase();
                GDB.ClientName = prms.CN;
                SelfieRegistration2Controller.GetDB2(GDB);
                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID });
                con.myparameters.Add(new myParameters { ParameterName = "@year", mytype = SqlDbType.VarChar, Value = prms.ytd_year });
                con.myparameters.Add(new myParameters { ParameterName = "@payoutDate", mytype = SqlDbType.VarChar, Value = prms.PayoutDate });
                DataTable dt13 = con.GetDataTable("sp_GetGovernmentAmounts_Philhealth_YTD_Payslip");

                if (dt12.Rows.Count > 0)
                {
                    string PHealthDesc = "Philhealth", PHealthAmount = "", PHealth_YTD_Amount = "";

                    PHealthAmount = dt12.Rows[0]["Philhealth"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt12.Rows[0]["Philhealth"].ToString()));
                    PHealth_YTD_Amount = dt13.Rows[0]["philhealth"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt13.Rows[0]["philhealth"].ToString()));

                    PdfPCell PHLHEALTH_tabl4cel14a = new PdfPCell(new PdfPCell(new Paragraph(PHealthDesc, FontFactory.GetFont("arial", 8))));
                    PHLHEALTH_tabl4cel14a.PaddingBottom = 7;
                    PHLHEALTH_tabl4cel14a.BorderWidth = 0;
                    table4.AddCell(PHLHEALTH_tabl4cel14a);

                    PdfPCell PHLHEALTH_tabl4cel14b = new PdfPCell(new PdfPCell(new Paragraph("", FontFactory.GetFont("arial", 8))));
                    PHLHEALTH_tabl4cel14b.HorizontalAlignment = Element.ALIGN_RIGHT;
                    PHLHEALTH_tabl4cel14b.PaddingBottom = 7;
                    PHLHEALTH_tabl4cel14b.BorderWidth = 0;
                    table4.AddCell(PHLHEALTH_tabl4cel14b);

                    PdfPCell PHLHEALTH_tabl4cel14c = new PdfPCell(new PdfPCell(new Paragraph(PHealthAmount, FontFactory.GetFont("arial", 8))));
                    PHLHEALTH_tabl4cel14c.HorizontalAlignment = Element.ALIGN_CENTER;
                    PHLHEALTH_tabl4cel14c.PaddingBottom = 7;
                    PHLHEALTH_tabl4cel14c.BorderWidth = 0;
                    table4.AddCell(PHLHEALTH_tabl4cel14c);

                    PdfPCell PHLHEALTH_tabl4cel14d = new PdfPCell(new PdfPCell(new Paragraph(PHealth_YTD_Amount, FontFactory.GetFont("arial", 8))));
                    PHLHEALTH_tabl4cel14d.HorizontalAlignment = Element.ALIGN_RIGHT;
                    PHLHEALTH_tabl4cel14d.PaddingBottom = 7;
                    PHLHEALTH_tabl4cel14d.BorderWidth = 0;
                    table4.AddCell(PHLHEALTH_tabl4cel14d);
                }


                GDB = new GetDatabase();
                GDB.ClientName = prms.CN;
                SelfieRegistration2Controller.GetDB2(GDB);
                // FOR HDMF
                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID });
                con.myparameters.Add(new myParameters { ParameterName = "@payoutDate", mytype = SqlDbType.VarChar, Value = prms.PayoutDate });
                DataTable dt14 = con.GetDataTable("sp_GetGovernmentAmounts_Pagibig_Payslip");


                GDB = new GetDatabase();
                GDB.ClientName = prms.CN;
                SelfieRegistration2Controller.GetDB2(GDB);
                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID });
                con.myparameters.Add(new myParameters { ParameterName = "@year", mytype = SqlDbType.VarChar, Value = prms.ytd_year });
                con.myparameters.Add(new myParameters { ParameterName = "@payoutDate", mytype = SqlDbType.VarChar, Value = prms.PayoutDate });
                DataTable dt15 = con.GetDataTable("sp_GetGovernmentAmounts_Pagibig_YTD_Payslip");

                if (dt14.Rows.Count > 0)
                {
                    string HDMFDesc = "HDMF", HDMFAmount = "", HDMF_YTD_Amount = "";
                    HDMFAmount = dt14.Rows[0]["HDMF"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt14.Rows[0]["HDMF"].ToString()));
                    HDMF_YTD_Amount = dt15.Rows[0]["Pagibig_Employee"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt15.Rows[0]["Pagibig_Employee"].ToString()));

                    PdfPCell HDMF_tabl4cel14a = new PdfPCell(new PdfPCell(new Paragraph(HDMFDesc, FontFactory.GetFont("arial", 8))));
                    HDMF_tabl4cel14a.PaddingBottom = 7;
                    HDMF_tabl4cel14a.BorderWidth = 0;
                    table4.AddCell(HDMF_tabl4cel14a);

                    PdfPCell HDMF_tabl4cel14b = new PdfPCell(new PdfPCell(new Paragraph("", FontFactory.GetFont("arial", 8))));
                    HDMF_tabl4cel14b.HorizontalAlignment = Element.ALIGN_RIGHT;
                    HDMF_tabl4cel14b.PaddingBottom = 7;
                    HDMF_tabl4cel14b.BorderWidth = 0;
                    table4.AddCell(HDMF_tabl4cel14b);

                    PdfPCell HDMF_tabl4cel14c = new PdfPCell(new PdfPCell(new Paragraph(HDMFAmount, FontFactory.GetFont("arial", 8))));
                    HDMF_tabl4cel14c.HorizontalAlignment = Element.ALIGN_CENTER;
                    HDMF_tabl4cel14c.PaddingBottom = 7;
                    HDMF_tabl4cel14c.BorderWidth = 0;
                    table4.AddCell(HDMF_tabl4cel14c);

                    PdfPCell HDMF_tabl4cel14d = new PdfPCell(new PdfPCell(new Paragraph(HDMF_YTD_Amount, FontFactory.GetFont("arial", 8))));
                    HDMF_tabl4cel14d.HorizontalAlignment = Element.ALIGN_RIGHT;
                    HDMF_tabl4cel14d.PaddingBottom = 7;
                    HDMF_tabl4cel14d.BorderWidth = 0;
                    table4.AddCell(HDMF_tabl4cel14d);
                }




                GDB = new GetDatabase();
                GDB.ClientName = prms.CN;
                SelfieRegistration2Controller.GetDB2(GDB);


                // FOR LOANS FIELD
                PdfPCell tabl4cell15 = new PdfPCell(new PdfPCell(new Paragraph("DEDUCTIONS", FontFactory.GetFont("arial", 8, Font.UNDERLINE))));
                tabl4cell14.PaddingTop = 30;
                tabl4cell15.PaddingBottom = 7;
                tabl4cell15.BorderWidth = 0;
                table4.AddCell(tabl4cell15);

                //PdfPCell tabl4cell16 = new PdfPCell(new PdfPCell(new Paragraph("BALANCE LEFT", FontFactory.GetFont("arial", 8, Font.BOLD))));
                PdfPCell tabl4cell16 = new PdfPCell(new PdfPCell(new Paragraph("", FontFactory.GetFont("arial", 8, Font.BOLD))));
                tabl4cell16.HorizontalAlignment = Element.ALIGN_RIGHT;
                tabl4cell16.PaddingBottom = 7;
                tabl4cell14.PaddingTop = 20;
                tabl4cell16.BorderWidth = 0;
                table4.AddCell(tabl4cell16);

                PdfPCell tabl4cell17 = new PdfPCell(new PdfPCell(new Paragraph("", FontFactory.GetFont("arial", 8, Font.BOLD))));
                tabl4cell17.PaddingBottom = 7;
                tabl4cell17.BorderWidth = 0;
                table4.AddCell(tabl4cell17);

                PdfPCell tabl4cell18 = new PdfPCell(new PdfPCell(new Paragraph("", FontFactory.GetFont("arial", 8, Font.BOLD))));
                tabl4cell18.HorizontalAlignment = Element.ALIGN_RIGHT;
                tabl4cell18.PaddingBottom = 7;
                tabl4cell18.BorderWidth = 0;
                table4.AddCell(tabl4cell18);


                GDB = new GetDatabase();
                GDB.ClientName = prms.CN;
                SelfieRegistration2Controller.GetDB2(GDB);
                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID });
                con.myparameters.Add(new myParameters { ParameterName = "@payoutDate", mytype = SqlDbType.VarChar, Value = prms.PayoutDate });
                DataTable dt20 = con.GetDataTable("sp_GetLoans_Payslip");


                GDB = new GetDatabase();
                GDB.ClientName = prms.CN;
                SelfieRegistration2Controller.GetDB2(GDB);
                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID });
                con.myparameters.Add(new myParameters { ParameterName = "@year", mytype = SqlDbType.VarChar, Value = prms.ytd_year });
                con.myparameters.Add(new myParameters { ParameterName = "@payoutDate", mytype = SqlDbType.VarChar, Value = prms.PayoutDate });
                DataTable dt21 = con.GetDataTable("sp_GetLoans_YTD_Payslip");

                if (dt20.Rows.Count > 0)
                {
                    string LoanName = "", Amortization_Amount = "", Amortization_YTD_Amount = "";

                    for (int i = 0; i < dt20.Rows.Count; i++)
                    {
                        LoanName = dt20.Rows[i]["LoanName"].ToString();
                        Amortization_Amount = dt20.Rows[i]["AmortizationAmount"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt20.Rows[i]["AmortizationAmount"].ToString()));
                        Amortization_YTD_Amount = dt21.Rows[i]["AmortizationAmount"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt21.Rows[i]["AmortizationAmount"].ToString()));

                        PdfPCell Loan_tabl4cel14a = new PdfPCell(new PdfPCell(new Paragraph(LoanName, FontFactory.GetFont("arial", 8))));
                        Loan_tabl4cel14a.PaddingBottom = 7;
                        Loan_tabl4cel14a.BorderWidth = 0;
                        table4.AddCell(Loan_tabl4cel14a);

                        PdfPCell Loan_tabl4cel14b = new PdfPCell(new PdfPCell(new Paragraph("", FontFactory.GetFont("arial", 8))));
                        Loan_tabl4cel14b.HorizontalAlignment = Element.ALIGN_RIGHT;
                        Loan_tabl4cel14b.PaddingBottom = 7;
                        Loan_tabl4cel14b.BorderWidth = 0;
                        table4.AddCell(Loan_tabl4cel14b);

                        PdfPCell Loan_tabl4cel14c = new PdfPCell(new PdfPCell(new Paragraph(Amortization_Amount, FontFactory.GetFont("arial", 8))));
                        Loan_tabl4cel14c.HorizontalAlignment = Element.ALIGN_CENTER;
                        Loan_tabl4cel14c.PaddingBottom = 7;
                        Loan_tabl4cel14c.BorderWidth = 0;
                        table4.AddCell(Loan_tabl4cel14c);

                        PdfPCell Loan_tabl4cel14d = new PdfPCell(new PdfPCell(new Paragraph(Amortization_YTD_Amount, FontFactory.GetFont("arial", 8))));
                        Loan_tabl4cel14d.HorizontalAlignment = Element.ALIGN_RIGHT;
                        Loan_tabl4cel14d.PaddingBottom = 7;
                        Loan_tabl4cel14d.BorderWidth = 0;
                        table4.AddCell(Loan_tabl4cel14d);
                    }
                }
                //
                // OTHER DEDUCTIONS
                GDB = new GetDatabase();
                GDB.ClientName = prms.CN;
                SelfieRegistration2Controller.GetDB2(GDB);
                Connection con3 = new Connection();
                con3.myparameters.Clear();
                con3.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID });
                con3.myparameters.Add(new myParameters { ParameterName = "@payoutDate", mytype = SqlDbType.VarChar, Value = prms.PayoutDate });
                DataTable dt18 = con3.GetDataTable("sp_GetDeductions_Payslip");


                GDB = new GetDatabase();
                GDB.ClientName = prms.CN;
                SelfieRegistration2Controller.GetDB2(GDB);
                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID });
                con.myparameters.Add(new myParameters { ParameterName = "@year", mytype = SqlDbType.VarChar, Value = prms.ytd_year });
                con.myparameters.Add(new myParameters { ParameterName = "@payoutDate", mytype = SqlDbType.VarChar, Value = prms.PayoutDate });
                DataTable dt19 = con.GetDataTable("sp_GetDeductions_YTD_Payslip");

                if (dt18.Rows.Count > 0)
                {
                    for (int i = 0; i < dt18.Rows.Count; i++)
                    {
                        string DeductionDesc = "", Deduction_Amount = "", Deduction_YTD_Amount = "";

                        DeductionDesc = dt18.Rows[i]["deductiondescription"].ToString();
                        Deduction_Amount = dt18.Rows[i]["deductionamount"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt18.Rows[i]["deductionamount"].ToString()));
                        Deduction_YTD_Amount = dt19.Rows[i]["deductionAmount"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt19.Rows[i]["deductionAmount"].ToString()));

                        PdfPCell OtherDeduction_tabl4cel14a = new PdfPCell(new PdfPCell(new Paragraph(DeductionDesc, FontFactory.GetFont("arial", 8))));
                        OtherDeduction_tabl4cel14a.PaddingBottom = 7;
                        OtherDeduction_tabl4cel14a.BorderWidth = 0;
                        table4.AddCell(OtherDeduction_tabl4cel14a);

                        PdfPCell OtherDeduction_tabl4cel14b = new PdfPCell(new PdfPCell(new Paragraph("", FontFactory.GetFont("arial", 8))));
                        OtherDeduction_tabl4cel14b.HorizontalAlignment = Element.ALIGN_RIGHT;
                        OtherDeduction_tabl4cel14b.PaddingBottom = 7;
                        OtherDeduction_tabl4cel14b.BorderWidth = 0;
                        table4.AddCell(OtherDeduction_tabl4cel14b);

                        PdfPCell OtherDeduction_tabl4cel14c = new PdfPCell(new PdfPCell(new Paragraph(Deduction_Amount, FontFactory.GetFont("arial", 8))));
                        OtherDeduction_tabl4cel14c.HorizontalAlignment = Element.ALIGN_CENTER;
                        OtherDeduction_tabl4cel14c.PaddingBottom = 7;
                        OtherDeduction_tabl4cel14c.BorderWidth = 0;
                        table4.AddCell(OtherDeduction_tabl4cel14c);

                        PdfPCell OtherDeduction_tabl4cel14d = new PdfPCell(new PdfPCell(new Paragraph(Deduction_YTD_Amount, FontFactory.GetFont("arial", 8))));
                        OtherDeduction_tabl4cel14d.HorizontalAlignment = Element.ALIGN_RIGHT;
                        OtherDeduction_tabl4cel14d.PaddingBottom = 7;
                        OtherDeduction_tabl4cel14d.BorderWidth = 0;
                        table4.AddCell(OtherDeduction_tabl4cel14d);
                    }
                }
                //
                //
                PdfPCell tabl4cell19 = new PdfPCell(new PdfPCell(new Paragraph("WithHolding Tax", FontFactory.GetFont("arial", 8))));
                tabl4cell19.PaddingBottom = 7;
                tabl4cell19.BorderWidth = 0;
                tabl4cell19.BackgroundColor = BaseColor.LIGHT_GRAY;
                table4.AddCell(tabl4cell19);

                PdfPCell tabl4cell20 = new PdfPCell(new PdfPCell(new Paragraph("", FontFactory.GetFont("arial", 8))));
                tabl4cell20.HorizontalAlignment = Element.ALIGN_RIGHT;
                tabl4cell20.PaddingBottom = 7;
                tabl4cell20.BorderWidth = 0;
                tabl4cell20.BackgroundColor = BaseColor.LIGHT_GRAY;
                table4.AddCell(tabl4cell20);

                PdfPCell tabl4cell21 = new PdfPCell(new PdfPCell(new Paragraph(whtax, FontFactory.GetFont("arial", 8))));
                tabl4cell21.HorizontalAlignment = Element.ALIGN_CENTER;
                tabl4cell21.PaddingBottom = 7;
                tabl4cell21.BorderWidth = 0;
                tabl4cell21.BackgroundColor = BaseColor.LIGHT_GRAY;
                table4.AddCell(tabl4cell21);

                PdfPCell tabl4cell22 = new PdfPCell(new PdfPCell(new Paragraph(ytdWhTax, FontFactory.GetFont("arial", 8))));
                tabl4cell22.HorizontalAlignment = Element.ALIGN_RIGHT;
                tabl4cell22.PaddingBottom = 7;
                tabl4cell22.BorderWidth = 0;
                tabl4cell22.BackgroundColor = BaseColor.LIGHT_GRAY;
                table4.AddCell(tabl4cell22);
                //

                //
                PdfPCell tabl4cell23 = new PdfPCell(new PdfPCell(new Paragraph("Net Pay", FontFactory.GetFont("arial", 8))));
                tabl4cell23.PaddingBottom = 7;
                tabl4cell23.BorderWidth = 0;
                table4.AddCell(tabl4cell23);

                PdfPCell tabl4cell24 = new PdfPCell(new PdfPCell(new Paragraph("", FontFactory.GetFont("arial", 8))));
                tabl4cell24.HorizontalAlignment = Element.ALIGN_RIGHT;
                tabl4cell24.PaddingBottom = 7;
                tabl4cell24.BorderWidth = 0;
                table4.AddCell(tabl4cell24);

                PdfPCell tabl4cell25 = new PdfPCell(new PdfPCell(new Paragraph(netpay, FontFactory.GetFont("arial", 8))));
                tabl4cell25.HorizontalAlignment = Element.ALIGN_CENTER;
                tabl4cell25.PaddingBottom = 7;
                tabl4cell25.BorderWidth = 0;
                table4.AddCell(tabl4cell25);

                PdfPCell tabl4cell26 = new PdfPCell(new PdfPCell(new Paragraph(ytdNetPay, FontFactory.GetFont("arial", 8))));
                tabl4cell26.HorizontalAlignment = Element.ALIGN_RIGHT;
                tabl4cell26.PaddingBottom = 7;
                tabl4cell26.BorderWidth = 0;
                table4.AddCell(tabl4cell26);


                //
                PdfPCell tabl4cell27 = new PdfPCell(new PdfPCell(new Paragraph("", FontFactory.GetFont("arial", 8, Font.UNDERLINE)))) { Border = PdfPCell.NO_BORDER, HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT };
                tabl4cell27.Colspan = 4;
                tabl4cell27.PaddingTop = 10;
                tabl4cell27.PaddingBottom = 7;
                tabl4cell27.BorderWidth = 0;
                table4.AddCell(tabl4cell27);
                //
                table2.AddCell(table4);


                PdfPCell cell22 = new PdfPCell() { Border = PdfPCell.NO_BORDER };
                cell22.Colspan = 2;


                table1.AddCell(cell11);
                table1.AddCell(cell12);
                table2.AddCell(cell22);
                document.Add(table0);
                document.Add(table1);
                document.Add(table2);
                document.Close();
                string onlineDIR = "https://ws.durusthr.com/ILM_WS_Live/pdf/";
                return onlineDIR + filename;
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }

        [HttpPost]
        [Route("generatepdf")]
        public string generatepdfs(generatePDF prms)
        {
            try
            {
                GetDatabase GDB = new GetDatabase();
                GDB.ClientName = prms.CN;
                SelfieRegistration2Controller.GetDB2(GDB);

                // DATA VARIABLE DECLARATION
                string startdate = "", enddate = "";
                string payday = "";
                string fullname = "", empid = "", datejoined = "", position = "", department = "", TIN = "", SSS = "", PHILHEALTH = "", PAGIBIG = "";
                string basicpay = "", grosspay = "", netpay = "", whtax = "", ytdBasicPay = "", ytdGrossPay = "", ytdNetPay = "", ytdWhTax = "";
                string HourlyRate = "", HoursWorked = "", AbsencesDesc = "Absences", TardiDesc = "Tardiness", UndertimeDesc = "Undertime", LWOPDesc = "LWOP", Monthly = "";
                string Absences_Hours = "", Tardi_Hours = "", Undertime_Hours = "", LWOP_Hours = "";
                string Absences_Amount = "", Tardi_Amount = "", Undertime_Amount = "", LWOP_Amount = "";
                string Absences_YTD_Amount = "", Tardi_YTD_Amount = "", Undertime_YTD_Amount = "", LWOP_YTD_Amount = "";
                string CompanyName = "";

                // FOR COMPANY NAME
                Connection conn = new Connection();
                conn.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.VarChar, Value = prms.EmpID });
                DataTable dtbl = conn.GetDataTable("sp_get_CompanyName_MobileDynamicPayslip");
                foreach (DataRow item in dtbl.Rows)
                {
                    CompanyName = item["TradingName"].ToString();
                }
                // FOR PAY PERIOD FIELD
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID });
                con.myparameters.Add(new myParameters { ParameterName = "@payoutDate", mytype = SqlDbType.VarChar, Value = prms.PayoutDate });
                DataTable DT = con.GetDataTable("sp_GetPayPeriod_Payslip");
                foreach (DataRow item in DT.Rows)
                {
                    startdate = item["StartDate"].ToString();
                    enddate = item["EndDate"].ToString();
                }


                GDB = new GetDatabase();
                GDB.ClientName = prms.CN;
                SelfieRegistration2Controller.GetDB2(GDB);
                // FOR PAY DATE FIELD
                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID });
                DataTable DT1 = con.GetDataTable("sp_GetPayoutSchemeList");
                foreach (DataRow item in DT1.Rows)
                {
                    payday = item["PayoutDate"].ToString();
                }

                GDB = new GetDatabase();
                GDB.ClientName = prms.CN;
                SelfieRegistration2Controller.GetDB2(GDB);
                // EMPLOYEE DETAILS FIELD
                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID });
                DataTable dt2 = con.GetDataTable("sp_GetEmployeeInfo_Payslip");
                foreach (DataRow item in dt2.Rows)
                {
                    fullname = item["EmpName"].ToString();
                    empid = item["EmpID"].ToString();
                    datejoined = item["DateJoined"].ToString();
                    department = item["BusinessUnit"].ToString();
                    position = item["JobDesc"].ToString();
                }

                GDB = new GetDatabase();
                GDB.ClientName = prms.CN;
                SelfieRegistration2Controller.GetDB2(GDB);
                // FOR GOVERNMENT ID FIELDS
                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID });
                DataTable dt3 = con.GetDataTable("sp_GetGovIDs_Payslip");
                foreach (DataRow item in dt3.Rows)
                {
                    if (item["IDTypes"].ToString() == "SSS")
                    {
                        SSS = item["IDCode"].ToString();
                    }
                    else if (item["IDTypes"].ToString() == "Philhealth")
                    {
                        PHILHEALTH = item["IDCode"].ToString();
                    }
                    else if (item["IDTypes"].ToString() == "Pag-Ibig")
                    {
                        PAGIBIG = item["IDCode"].ToString();
                    }
                    else
                    {
                        TIN = item["IDCode"].ToString();
                    }
                }

                GDB = new GetDatabase();
                GDB.ClientName = prms.CN;
                SelfieRegistration2Controller.GetDB2(GDB);
                // FOR GROSS PAY, BASIC PAY AND NET PAY FIELDS
                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID });
                con.myparameters.Add(new myParameters { ParameterName = "@payoutDate", mytype = SqlDbType.VarChar, Value = prms.PayoutDate });
                DataTable dt4 = con.GetDataTable("sp_GetPay_Payslip");
                foreach (DataRow item in dt4.Rows)
                {
                    basicpay = item["Basic Pay"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(item["Basic Pay"].ToString()));
                    grosspay = item["Gross Pay"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(item["Gross Pay"].ToString()));
                    netpay = item["Net Pay"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(item["Net Pay"].ToString()));
                    whtax = item["WithHolding Tax"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(item["WithHolding Tax"].ToString()));

                    //basicpay = String.Format("{0:n2}", decimal.Parse(basicpay));
                }
                // END


                GDB = new GetDatabase();
                GDB.ClientName = prms.CN;
                SelfieRegistration2Controller.GetDB2(GDB);
                // YTD GROSS PAY, BASIC PAY AND NET PAY FIELDS
                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID });
                con.myparameters.Add(new myParameters { ParameterName = "@year", mytype = SqlDbType.VarChar, Value = prms.ytd_year });
                con.myparameters.Add(new myParameters { ParameterName = "@payoutDate", mytype = SqlDbType.VarChar, Value = prms.PayoutDate });
                DataTable dt5 = con.GetDataTable("sp_GetPay_YTD_Payslip");
                foreach (DataRow item in dt5.Rows)
                {
                    ytdBasicPay = item["basicpay"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(item["basicpay"].ToString()));
                    ytdGrossPay = item["grosspay"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(item["grosspay"].ToString()));
                    ytdWhTax = item["whtax"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(item["whtax"].ToString()));
                    ytdNetPay = item["netpay"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(item["netpay"].ToString()));
                }

                GDB = new GetDatabase();
                GDB.ClientName = prms.CN;
                SelfieRegistration2Controller.GetDB2(GDB);
                // FOR LOGS INFO
                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID });
                con.myparameters.Add(new myParameters { ParameterName = "@payoutDate", mytype = SqlDbType.VarChar, Value = prms.PayoutDate });
                DataTable dt16 = con.GetDataTable("sp_GetLogInfo_Payslip");

                if (dt16.Rows.Count > 0)
                {
                    HourlyRate = dt16.Rows[0]["HourlyRate"] == DBNull.Value || dt16.Rows[0]["HourlyRate"].ToString() == "" ? "" : String.Format("{0:n2}", decimal.Parse(dt16.Rows[0]["HourlyRate"].ToString()));
                    HoursWorked = dt16.Rows[0]["HoursWorked"] == DBNull.Value || dt16.Rows[0]["HoursWorked"].ToString() == "" ? "" : String.Format("{0:n2}", decimal.Parse(dt16.Rows[0]["HoursWorked"].ToString()));
                    //Monthly = dt16.Rows[0]["Monthly"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt16.Rows[0]["Monthly"].ToString()));
                }

                // GET MONTHLY SALARY
                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@empid", mytype = SqlDbType.VarChar, Value = prms.EmpID });
                DataTable dtMontlySalary = con.GetDataTable("sp_GetMonthlySalary_Payslip");

                if (dtMontlySalary.Rows.Count > 0)
                {
                    Monthly = dtMontlySalary.Rows[0]["Salary"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dtMontlySalary.Rows[0]["Salary"].ToString()));
                }

                GDB = new GetDatabase();
                GDB.ClientName = prms.CN;
                SelfieRegistration2Controller.GetDB2(GDB);
                Connection con1 = new Connection();
                con1.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID });
                con1.myparameters.Add(new myParameters { ParameterName = "@year", mytype = SqlDbType.VarChar, Value = prms.ytd_year });
                con1.myparameters.Add(new myParameters { ParameterName = "@payoutDate", mytype = SqlDbType.VarChar, Value = prms.PayoutDate });
                DataTable dt17 = con1.GetDataTable("sp_GetLogInfo_YTD_Payslip");
                string filename = "";
                filename = "payslip-" + prms.EmpID + "-" + prms.PayoutDate + ".pdf";
                System.IO.FileStream fs = new FileStream(HttpContext.Current.Server.MapPath("pdf") + "\\" + "payslip-" + prms.EmpID + "-" + prms.PayoutDate + ".pdf", FileMode.Create);
                //System.IO.FileStream fb = new FileStream(HttpContext.Current.Server.MapPath("pdf") + "\\" + "ilm.png", FileMode.Open);
                string imagepath = HttpContext.Current.Server.MapPath("Images");
                Document document = new Document(PageSize.A4, 25, 25, 30, 30);
                PdfWriter writer = PdfWriter.GetInstance(document, fs);
                //PdfWriter writering = PdfWriter.GetInstance(document, fb);
                document.Open();

                PdfPTable table0 = new PdfPTable(1);

                PdfPCell cell0 = new PdfPCell(new iTextSharp.text.Phrase(CompanyName, FontFactory.GetFont("Arial", 12, Font.BOLD))) { Border = 0, HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT, VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE };
                cell0.PaddingTop = 10;
                //PdfPCell cell01 = new PdfPCell(new iTextSharp.text.Phrase("Lima Technology Center  Lipa City, Batangas Philippines 4217", FontFactory.GetFont("Arial", 11, Font.NORMAL))) { Border = 0, HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT, VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE };
                //cell01.PaddingBottom = 7;

                //Image logoimage = Image.GetInstance(Path.Combine(imagepath, "yokohamalogo.jpg"));
                //logoimage.ScaleAbsolute(100, 40);
                //logoimage.SetAbsolutePosition(100, 30);

                //PdfPCell LogoCell = new PdfPCell(logoimage, true);

                table0.AddCell(cell0);
                ////table0.AddCell(LogoCell);
                //table0.AddCell(cell01);
                //document.Add(LogoCell);


                PdfPTable table1 = new PdfPTable(2);

                /*var titleFont = new Font(Font.FontFamily.UNDEFINED, 12);
                var informationDetails = new Font(Font.FontFamily.UNDEFINED, 12);*/

                PdfPCell cell11 = new PdfPCell(new iTextSharp.text.Phrase("PAY PERIOD\n" + startdate + " - " + enddate)) { Border = PdfPCell.BOTTOM_BORDER | PdfPCell.TOP_BORDER, HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER, VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE };
                PdfPCell cell12 = new PdfPCell(new iTextSharp.text.Phrase("PAY DAY\n" + prms.PayoutDate)) { Border = PdfPCell.BOTTOM_BORDER | PdfPCell.TOP_BORDER, HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER, VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE };
                cell11.PaddingTop = 10;
                cell12.PaddingTop = 10;
                cell11.PaddingBottom = 7;
                cell12.PaddingBottom = 7;
                /* cell12.AddElement(new Paragraph("PAY DAY", titleFont));
                cell12.AddElement(new Paragraph("-", informationDetails));*/

                PdfPTable table2 = new PdfPTable(2);
                float[] widths = new float[] { 100f, 200f };
                float[] heights = new float[] { 300f };
                table2.SetWidths(widths);
                //table2.DefaultCell.FixedHeight = 500f;
                table2.DefaultCell.Border = Rectangle.NO_BORDER;
                //PdfPCell cell21 = new PdfPCell(new Paragraph("Danilo Vicente Sulla Jr")) { Border = PdfPCell.RIGHT_BORDER, HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER, VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE };
                //cell21.PaddingTop = 20;

                PdfPTable table3 = new PdfPTable(2);
                table3.TotalWidth = 100;
                table3.DefaultCell.FixedHeight = 600f;
                table3.DefaultCell.Border = Rectangle.RIGHT_BORDER;

                PdfPCell tabl3cell0 = new PdfPCell(new PdfPCell(new Paragraph(fullname, FontFactory.GetFont("Arial", 12, Font.BOLD)))) { Border = PdfPCell.NO_BORDER, HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER, VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE };
                tabl3cell0.Colspan = 2;
                tabl3cell0.PaddingTop = 20;
                tabl3cell0.PaddingBottom = 20;
                tabl3cell0.BorderWidth = 0;
                tabl3cell0.BorderWidthRight = 1;
                table3.AddCell(tabl3cell0);


                PdfPCell tabl3cell1 = new PdfPCell(new PdfPCell(new Paragraph("EmpID", FontFactory.GetFont("arial", 8))));
                tabl3cell1.PaddingBottom = 7;
                tabl3cell1.BorderWidth = 0;
                tabl3cell1.BorderWidthBottom = 1;
                table3.AddCell(tabl3cell1);

                PdfPCell tabl3cell2 = new PdfPCell(new PdfPCell(new Paragraph(empid, FontFactory.GetFont("arial", 8, Font.BOLD))));
                tabl3cell2.HorizontalAlignment = Element.ALIGN_RIGHT;
                tabl3cell2.PaddingBottom = 7;
                tabl3cell2.BorderWidth = 0;
                tabl3cell2.BorderWidthBottom = 1;
                tabl3cell2.BorderWidthRight = 1;
                table3.AddCell(tabl3cell2);

                PdfPCell tabl3cell3 = new PdfPCell(new PdfPCell(new Paragraph("Tax Category", FontFactory.GetFont("arial", 8))));
                tabl3cell3.PaddingBottom = 7;
                tabl3cell3.BorderWidth = 0;
                tabl3cell3.BorderWidthBottom = 1;
                table3.AddCell(tabl3cell3);

                PdfPCell tabl3cell4 = new PdfPCell(new PdfPCell(new Paragraph("--", FontFactory.GetFont("arial", 8, Font.BOLD))));
                tabl3cell4.HorizontalAlignment = Element.ALIGN_RIGHT;
                tabl3cell4.PaddingBottom = 7;
                tabl3cell4.BorderWidth = 0;
                tabl3cell4.BorderWidthBottom = 1;
                tabl3cell4.BorderWidthRight = 1;
                table3.AddCell(tabl3cell4);

                PdfPCell tabl3cell5 = new PdfPCell(new PdfPCell(new Paragraph("Hourly Rate", FontFactory.GetFont("arial", 8))));
                tabl3cell5.PaddingBottom = 7;
                tabl3cell5.BorderWidth = 0;
                tabl3cell5.BorderWidthBottom = 1;
                table3.AddCell(tabl3cell5);

                PdfPCell tabl3cell6 = new PdfPCell(new PdfPCell(new Paragraph(HourlyRate, FontFactory.GetFont("arial", 8, Font.BOLD))));
                tabl3cell6.HorizontalAlignment = Element.ALIGN_RIGHT;
                tabl3cell6.PaddingBottom = 7;
                tabl3cell6.BorderWidth = 0;
                tabl3cell6.BorderWidthBottom = 1;
                tabl3cell6.BorderWidthRight = 1;
                table3.AddCell(tabl3cell6);

                PdfPCell tabl3cell7 = new PdfPCell(new PdfPCell(new Paragraph("Regular Hours", FontFactory.GetFont("arial", 8))));
                tabl3cell7.PaddingBottom = 7;
                tabl3cell7.BorderWidth = 0;
                tabl3cell7.BorderWidthBottom = 1;
                table3.AddCell(tabl3cell7);

                PdfPCell tabl3cell8 = new PdfPCell(new PdfPCell(new Paragraph(HoursWorked, FontFactory.GetFont("arial", 8, Font.BOLD))));
                tabl3cell8.HorizontalAlignment = Element.ALIGN_RIGHT;
                tabl3cell8.PaddingBottom = 7;
                tabl3cell8.BorderWidth = 0;
                tabl3cell8.BorderWidthBottom = 1;
                tabl3cell8.BorderWidthRight = 1;
                table3.AddCell(tabl3cell8);

                PdfPCell tabl3cell9 = new PdfPCell(new PdfPCell(new Paragraph(getSalaryScheme(Monthly.ToString()) + " Salary", FontFactory.GetFont("arial", 8))));
                tabl3cell9.PaddingBottom = 7;
                tabl3cell9.BorderWidth = 0;
                tabl3cell9.BorderWidthBottom = 1;
                table3.AddCell(tabl3cell9);

                PdfPCell tabl3cell10 = new PdfPCell(new PdfPCell(new Paragraph(Monthly, FontFactory.GetFont("arial", 8, Font.BOLD))));
                tabl3cell10.HorizontalAlignment = Element.ALIGN_RIGHT;
                tabl3cell10.PaddingBottom = 7;
                tabl3cell10.BorderWidth = 0;
                tabl3cell10.BorderWidthBottom = 1;
                tabl3cell10.BorderWidthRight = 1;
                table3.AddCell(tabl3cell10);

                PdfPCell tabl3cell11 = new PdfPCell(new PdfPCell(new Paragraph("Date Joined", FontFactory.GetFont("arial", 8))));
                tabl3cell11.PaddingBottom = 7;
                tabl3cell11.BorderWidth = 0;
                tabl3cell11.BorderWidthBottom = 1;
                table3.AddCell(tabl3cell11);

                PdfPCell tabl3cell12 = new PdfPCell(new PdfPCell(new Paragraph(datejoined, FontFactory.GetFont("arial", 8, Font.BOLD))));
                tabl3cell12.HorizontalAlignment = Element.ALIGN_RIGHT;
                tabl3cell12.PaddingBottom = 7;
                tabl3cell12.BorderWidth = 0;
                tabl3cell12.BorderWidthBottom = 1;
                tabl3cell12.BorderWidthRight = 1;
                table3.AddCell(tabl3cell12);

                PdfPCell tabl3cell13 = new PdfPCell(new PdfPCell(new Paragraph("Position", FontFactory.GetFont("arial", 8))));
                tabl3cell13.PaddingBottom = 7;
                tabl3cell13.BorderWidth = 0;
                tabl3cell13.BorderWidthBottom = 1;
                table3.AddCell(tabl3cell13);

                PdfPCell tabl3cell14 = new PdfPCell(new PdfPCell(new Paragraph(position, FontFactory.GetFont("arial", 8, Font.BOLD))));
                tabl3cell14.HorizontalAlignment = Element.ALIGN_RIGHT;
                tabl3cell14.PaddingBottom = 7;
                tabl3cell14.BorderWidth = 0;
                tabl3cell14.BorderWidthBottom = 1;
                tabl3cell14.BorderWidthRight = 1;
                table3.AddCell(tabl3cell14);

                PdfPCell tabl3cell15 = new PdfPCell(new PdfPCell(new Paragraph("Department", FontFactory.GetFont("arial", 8))));
                tabl3cell15.PaddingBottom = 7;
                tabl3cell15.BorderWidth = 0;
                tabl3cell15.BorderWidthBottom = 1;
                table3.AddCell(tabl3cell15);

                PdfPCell tabl3cell16 = new PdfPCell(new PdfPCell(new Paragraph(department, FontFactory.GetFont("arial", 8, Font.BOLD))));
                tabl3cell16.HorizontalAlignment = Element.ALIGN_RIGHT;
                tabl3cell16.PaddingBottom = 7;
                tabl3cell16.BorderWidth = 0;
                tabl3cell16.BorderWidthBottom = 1;
                tabl3cell16.BorderWidthRight = 1;
                table3.AddCell(tabl3cell16);

                PdfPCell tabl3cell17 = new PdfPCell(new PdfPCell(new Paragraph("Philhealth", FontFactory.GetFont("arial", 8))));
                tabl3cell17.PaddingBottom = 7;
                tabl3cell17.BorderWidth = 0;
                tabl3cell17.BorderWidthBottom = 1;
                table3.AddCell(tabl3cell17);

                PdfPCell tabl3cell18 = new PdfPCell(new PdfPCell(new Paragraph(PHILHEALTH, FontFactory.GetFont("arial", 8, Font.BOLD))));
                tabl3cell18.HorizontalAlignment = Element.ALIGN_RIGHT;
                tabl3cell18.PaddingBottom = 7;
                tabl3cell18.BorderWidth = 0;
                tabl3cell18.BorderWidthBottom = 1;
                tabl3cell18.BorderWidthRight = 1;
                table3.AddCell(tabl3cell18);

                PdfPCell tabl3cell19 = new PdfPCell(new PdfPCell(new Paragraph("Pag-Ibig", FontFactory.GetFont("arial", 8))));
                tabl3cell19.PaddingBottom = 7;
                tabl3cell19.BorderWidth = 0;
                tabl3cell19.BorderWidthBottom = 1;
                table3.AddCell(tabl3cell19);

                PdfPCell tabl3cell20 = new PdfPCell(new PdfPCell(new Paragraph(PAGIBIG, FontFactory.GetFont("arial", 8, Font.BOLD))));
                tabl3cell20.HorizontalAlignment = Element.ALIGN_RIGHT;
                tabl3cell20.PaddingBottom = 7;
                tabl3cell20.BorderWidth = 0;
                tabl3cell20.BorderWidthBottom = 1;
                tabl3cell20.BorderWidthRight = 1;
                table3.AddCell(tabl3cell20);

                PdfPCell tabl3cell21 = new PdfPCell(new PdfPCell(new Paragraph("TIN", FontFactory.GetFont("arial", 8))));
                tabl3cell21.PaddingBottom = 7;
                tabl3cell21.BorderWidth = 0;
                tabl3cell21.BorderWidthBottom = 1;

                table3.AddCell(tabl3cell21);

                PdfPCell tabl3cell22 = new PdfPCell(new PdfPCell(new Paragraph(TIN, FontFactory.GetFont("arial", 8, Font.BOLD))));
                tabl3cell22.HorizontalAlignment = Element.ALIGN_RIGHT;
                tabl3cell22.PaddingBottom = 7;
                tabl3cell22.BorderWidth = 0;
                tabl3cell22.BorderWidthBottom = 1;
                tabl3cell22.BorderWidthRight = 1;
                table3.AddCell(tabl3cell22);

                PdfPCell tabl3cell23 = new PdfPCell(new PdfPCell(new Paragraph("SSS", FontFactory.GetFont("arial", 8))));
                tabl3cell23.PaddingBottom = 7;
                tabl3cell23.BorderWidth = 0;
                table3.AddCell(tabl3cell23);

                PdfPCell tabl3cell24 = new PdfPCell(new PdfPCell(new Paragraph(SSS, FontFactory.GetFont("arial", 8, Font.BOLD))));
                tabl3cell24.HorizontalAlignment = Element.ALIGN_RIGHT;
                tabl3cell24.PaddingBottom = 7;
                tabl3cell24.BorderWidth = 0;
                tabl3cell24.BorderWidthRight = 1;
                table3.AddCell(tabl3cell24);


                table2.AddCell(table3);

                //
                PdfPTable table4 = new PdfPTable(4);
                table4.TotalWidth = 100;
                table4.DefaultCell.Border = Rectangle.NO_BORDER;

                PdfPCell tabl4cell1 = new PdfPCell(new PdfPCell(new Paragraph("", FontFactory.GetFont("arial", 8))));
                tabl4cell1.PaddingBottom = 7;
                tabl4cell1.BorderWidth = 0;
                table4.AddCell(tabl4cell1);

                PdfPCell tabl4cell2 = new PdfPCell(new PdfPCell(new Paragraph("HOURS", FontFactory.GetFont("arial", 8, Font.BOLD))));
                tabl4cell2.HorizontalAlignment = Element.ALIGN_RIGHT;
                tabl4cell2.PaddingBottom = 7;
                tabl4cell2.BorderWidth = 0;
                table4.AddCell(tabl4cell2);

                PdfPCell tabl4cell3 = new PdfPCell(new PdfPCell(new Paragraph("CURRENT (PHP)", FontFactory.GetFont("arial", 8, Font.BOLD))));
                tabl4cell3.HorizontalAlignment = Element.ALIGN_CENTER;
                tabl4cell3.PaddingBottom = 7;
                tabl4cell3.BorderWidth = 0;
                table4.AddCell(tabl4cell3);

                PdfPCell tabl4cell4 = new PdfPCell(new PdfPCell(new Paragraph("YEAR TO DATE(PHP)", FontFactory.GetFont("arial", 8, Font.BOLD))));
                tabl4cell4.HorizontalAlignment = Element.ALIGN_RIGHT;
                tabl4cell4.PaddingBottom = 7;
                tabl4cell4.BorderWidth = 0;
                table4.AddCell(tabl4cell4);


                //
                PdfPCell tabl4cell5 = new PdfPCell(new PdfPCell(new Paragraph("Basic Pay", FontFactory.GetFont("arial", 8))));
                tabl4cell5.PaddingBottom = 7;
                tabl4cell5.BorderWidth = 0;
                table4.AddCell(tabl4cell5);

                PdfPCell tabl4cell6 = new PdfPCell(new PdfPCell(new Paragraph("", FontFactory.GetFont("arial", 8))));
                tabl4cell6.HorizontalAlignment = Element.ALIGN_RIGHT;
                tabl4cell6.PaddingBottom = 7;
                tabl4cell6.BorderWidth = 0;
                table4.AddCell(tabl4cell6);

                PdfPCell tabl4cell7 = new PdfPCell(new PdfPCell(new Paragraph(basicpay, FontFactory.GetFont("arial", 8))));
                tabl4cell7.HorizontalAlignment = Element.ALIGN_CENTER;
                tabl4cell7.PaddingBottom = 7;
                tabl4cell7.BorderWidth = 0;
                table4.AddCell(tabl4cell7);

                PdfPCell tabl4cell8 = new PdfPCell(new PdfPCell(new Paragraph(ytdBasicPay, FontFactory.GetFont("arial", 8))));
                tabl4cell8.HorizontalAlignment = Element.ALIGN_RIGHT;
                tabl4cell8.PaddingBottom = 7;
                tabl4cell8.BorderWidth = 0;
                table4.AddCell(tabl4cell8);
                //

                //
                PdfPCell tabl4cell9 = new PdfPCell(new PdfPCell(new Paragraph("Gross Pay", FontFactory.GetFont("arial", 8))));
                tabl4cell9.PaddingBottom = 7;
                tabl4cell9.BorderWidth = 0;
                table4.AddCell(tabl4cell9);

                PdfPCell tabl4cell10 = new PdfPCell(new PdfPCell(new Paragraph("", FontFactory.GetFont("arial", 8))));
                tabl4cell10.HorizontalAlignment = Element.ALIGN_RIGHT;
                tabl4cell10.PaddingBottom = 7;
                tabl4cell10.BorderWidth = 0;
                table4.AddCell(tabl4cell10);

                PdfPCell tabl4cell11 = new PdfPCell(new PdfPCell(new Paragraph(grosspay, FontFactory.GetFont("arial", 8))));
                tabl4cell11.HorizontalAlignment = Element.ALIGN_CENTER;
                tabl4cell11.PaddingBottom = 7;
                tabl4cell11.BorderWidth = 0;
                table4.AddCell(tabl4cell11);

                PdfPCell tabl4cell12 = new PdfPCell(new PdfPCell(new Paragraph(ytdGrossPay, FontFactory.GetFont("arial", 8))));
                tabl4cell12.HorizontalAlignment = Element.ALIGN_RIGHT;
                tabl4cell12.PaddingBottom = 7;
                tabl4cell12.BorderWidth = 0;
                table4.AddCell(tabl4cell12);
                //


                GDB = new GetDatabase();
                GDB.ClientName = prms.CN;
                SelfieRegistration2Controller.GetDB2(GDB);
                // FOR INCOME FIELDS
                // INCOME UNDER EARNINGS
                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID });
                con.myparameters.Add(new myParameters { ParameterName = "@payoutDate", mytype = SqlDbType.VarChar, Value = prms.PayoutDate });
                DataTable dt6 = con.GetDataTable("sp_Earnings_Payslip");


                GDB = new GetDatabase();
                GDB.ClientName = prms.CN;
                SelfieRegistration2Controller.GetDB2(GDB);
                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID });
                con.myparameters.Add(new myParameters { ParameterName = "@year", mytype = SqlDbType.VarChar, Value = prms.ytd_year });
                con.myparameters.Add(new myParameters { ParameterName = "@payoutDate", mytype = SqlDbType.VarChar, Value = prms.PayoutDate });
                DataTable dt7 = con.GetDataTable("sp_Earnings_YTD_Payslip");

                // HEADER
                PdfPCell tabl4cell13 = new PdfPCell(new PdfPCell(new Paragraph("INCOME", FontFactory.GetFont("arial", 8, Font.UNDERLINE)))) { Border = PdfPCell.NO_BORDER, HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT };
                tabl4cell13.Colspan = 4;
                tabl4cell13.PaddingTop = 10;
                tabl4cell13.PaddingBottom = 7;
                tabl4cell13.BorderWidth = 0;
                tabl4cell13.BackgroundColor = BaseColor.LIGHT_GRAY;
                table4.AddCell(tabl4cell13);

                if (dt6.Rows.Count > 0)
                {
                    string desc = "", amount = "", ytdAmount = "";
                    for (int i = 0; i < dt6.Rows.Count; i++)
                    {
                        desc = dt6.Rows[i]["EarningsDescription"].ToString();
                        amount = dt6.Rows[i]["EarningsAmount"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt6.Rows[i]["EarningsAmount"].ToString()));

                        for (int ii = 0; ii < dt7.Rows.Count; ii++)
                        {
                            if (desc == dt7.Rows[ii]["EarningsDescription"].ToString())
                            {
                                ytdAmount = dt7.Rows[ii]["EarningsAmount"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt7.Rows[ii]["EarningsAmount"].ToString()));
                            }
                        }


                        PdfPCell tabl4cel13a = new PdfPCell(new PdfPCell(new Paragraph(desc, FontFactory.GetFont("arial", 8))));
                        tabl4cel13a.PaddingBottom = 7;
                        tabl4cel13a.BorderWidth = 0;
                        tabl4cel13a.BackgroundColor = BaseColor.LIGHT_GRAY;
                        table4.AddCell(tabl4cel13a);

                        PdfPCell tabl4cel13b = new PdfPCell(new PdfPCell(new Paragraph("", FontFactory.GetFont("arial", 8))));
                        tabl4cel13b.HorizontalAlignment = Element.ALIGN_RIGHT;
                        tabl4cel13b.PaddingBottom = 7;
                        tabl4cel13b.BorderWidth = 0;
                        tabl4cel13b.BackgroundColor = BaseColor.LIGHT_GRAY;
                        table4.AddCell(tabl4cel13b);

                        PdfPCell tabl4cel13c = new PdfPCell(new PdfPCell(new Paragraph(amount, FontFactory.GetFont("arial", 8))));
                        tabl4cel13c.HorizontalAlignment = Element.ALIGN_CENTER;
                        tabl4cel13c.PaddingBottom = 7;
                        tabl4cel13c.BorderWidth = 0;
                        tabl4cel13c.BackgroundColor = BaseColor.LIGHT_GRAY;
                        table4.AddCell(tabl4cel13c);

                        PdfPCell tabl4cel13d = new PdfPCell(new PdfPCell(new Paragraph(ytdAmount, FontFactory.GetFont("arial", 8))));
                        tabl4cel13d.HorizontalAlignment = Element.ALIGN_RIGHT;
                        tabl4cel13d.PaddingBottom = 7;
                        tabl4cel13d.BorderWidth = 0;
                        tabl4cel13d.BackgroundColor = BaseColor.LIGHT_GRAY;
                        table4.AddCell(tabl4cel13d);
                    }
                }


                GDB = new GetDatabase();
                GDB.ClientName = prms.CN;
                SelfieRegistration2Controller.GetDB2(GDB);
                // INCOME UNDER OVERTIME
                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID });
                con.myparameters.Add(new myParameters { ParameterName = "@payoutDate", mytype = SqlDbType.VarChar, Value = prms.PayoutDate });
                DataTable dt8 = con.GetDataTable("sp_GetOvertimeInfo_Payslip");


                GDB = new GetDatabase();
                GDB.ClientName = prms.CN;
                SelfieRegistration2Controller.GetDB2(GDB);
                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID });
                con.myparameters.Add(new myParameters { ParameterName = "@year", mytype = SqlDbType.VarChar, Value = prms.ytd_year });
                con.myparameters.Add(new myParameters { ParameterName = "@payoutDate", mytype = SqlDbType.VarChar, Value = prms.PayoutDate });
                DataTable dt9 = con.GetDataTable("sp_GetOvertimeInfo_YTD_Payslip");

                if (dt8.Rows.Count > 0)
                {
                    string OTDesc = "", OTHours = "", OTAmount = "", OT_YTDAmount = "";

                    for (int i = 0; i < dt8.Rows.Count; i++)
                    {
                        OTDesc = dt8.Rows[i]["overtimedescription"].ToString();
                        OTHours = dt8.Rows[i]["overtimehours"].ToString();
                        OTAmount = dt8.Rows[i]["overtimeamount"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt8.Rows[i]["overtimeamount"].ToString()));
                        OT_YTDAmount = dt9.Rows[i]["OvertimeAmount"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt9.Rows[i]["OvertimeAmount"].ToString()));

                        PdfPCell OT_tabl4cel13a = new PdfPCell(new PdfPCell(new Paragraph(OTDesc, FontFactory.GetFont("arial", 8))));
                        OT_tabl4cel13a.PaddingBottom = 7;
                        OT_tabl4cel13a.BorderWidth = 0;
                        OT_tabl4cel13a.BackgroundColor = BaseColor.LIGHT_GRAY;
                        table4.AddCell(OT_tabl4cel13a);

                        PdfPCell OT_tabl4cel13b = new PdfPCell(new PdfPCell(new Paragraph(OTHours, FontFactory.GetFont("arial", 8))));
                        OT_tabl4cel13b.HorizontalAlignment = Element.ALIGN_RIGHT;
                        OT_tabl4cel13b.PaddingBottom = 7;
                        OT_tabl4cel13b.BorderWidth = 0;
                        OT_tabl4cel13b.BackgroundColor = BaseColor.LIGHT_GRAY;
                        table4.AddCell(OT_tabl4cel13b);

                        PdfPCell OT_tabl4cel13c = new PdfPCell(new PdfPCell(new Paragraph(OTAmount, FontFactory.GetFont("arial", 8))));
                        OT_tabl4cel13c.HorizontalAlignment = Element.ALIGN_CENTER;
                        OT_tabl4cel13c.PaddingBottom = 7;
                        OT_tabl4cel13c.BorderWidth = 0;
                        OT_tabl4cel13c.BackgroundColor = BaseColor.LIGHT_GRAY;
                        table4.AddCell(OT_tabl4cel13c);

                        PdfPCell OT_tabl4cel13d = new PdfPCell(new PdfPCell(new Paragraph(OT_YTDAmount, FontFactory.GetFont("arial", 8))));
                        OT_tabl4cel13d.HorizontalAlignment = Element.ALIGN_RIGHT;
                        OT_tabl4cel13d.PaddingBottom = 7;
                        OT_tabl4cel13d.BorderWidth = 0;
                        OT_tabl4cel13d.BackgroundColor = BaseColor.LIGHT_GRAY;
                        table4.AddCell(OT_tabl4cel13d);
                    }
                }
                // DEDUCTIONS FROM LOG INFO
                if (dt16.Rows.Count > 0)
                {
                    if (dt17.Rows[0]["absencesAmount"].ToString() != "0" && dt17.Rows[0]["absencesAmount"].ToString() != "0.0000")
                    {
                        Absences_Hours = dt16.Rows[0]["Absences"].ToString();
                        Absences_Amount = dt16.Rows[0]["AbsentAmount"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt16.Rows[0]["AbsentAmount"].ToString()));
                        Absences_YTD_Amount = dt17.Rows[0]["absencesAmount"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt17.Rows[0]["absencesAmount"].ToString()));

                        PdfPCell Absences_tabl4cel14a = new PdfPCell(new PdfPCell(new Paragraph(AbsencesDesc, FontFactory.GetFont("arial", 8))));
                        Absences_tabl4cel14a.PaddingBottom = 7;
                        Absences_tabl4cel14a.BorderWidth = 0;
                        table4.AddCell(Absences_tabl4cel14a);

                        PdfPCell Absences_tabl4cel14b = new PdfPCell(new PdfPCell(new Paragraph(Absences_Hours, FontFactory.GetFont("arial", 8))));
                        Absences_tabl4cel14b.HorizontalAlignment = Element.ALIGN_RIGHT;
                        Absences_tabl4cel14b.PaddingBottom = 7;
                        Absences_tabl4cel14b.BorderWidth = 0;
                        table4.AddCell(Absences_tabl4cel14b);

                        PdfPCell Absences_tabl4cel14c = new PdfPCell(new PdfPCell(new Paragraph(Absences_Amount, FontFactory.GetFont("arial", 8))));
                        Absences_tabl4cel14c.HorizontalAlignment = Element.ALIGN_CENTER;
                        Absences_tabl4cel14c.PaddingBottom = 7;
                        Absences_tabl4cel14c.BorderWidth = 0;
                        table4.AddCell(Absences_tabl4cel14c);

                        PdfPCell Absences_tabl4cel14d = new PdfPCell(new PdfPCell(new Paragraph(Absences_YTD_Amount, FontFactory.GetFont("arial", 8))));
                        Absences_tabl4cel14d.HorizontalAlignment = Element.ALIGN_RIGHT;
                        Absences_tabl4cel14d.PaddingBottom = 7;
                        Absences_tabl4cel14d.BorderWidth = 0;
                        table4.AddCell(Absences_tabl4cel14d);
                    }

                    if (dt17.Rows[0]["tardinessAmount"].ToString() != "0" && dt17.Rows[0]["tardinessAmount"].ToString() != "0.0000")
                    {
                        Tardi_Hours = dt16.Rows[0]["Tardiness"].ToString();
                        Tardi_Amount = dt16.Rows[0]["TardinessAmount"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt16.Rows[0]["TardinessAmount"].ToString()));
                        Tardi_YTD_Amount = dt17.Rows[0]["tardinessAmount"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt17.Rows[0]["tardinessAmount"].ToString()));

                        PdfPCell Tardi_tabl4cel14a = new PdfPCell(new PdfPCell(new Paragraph(TardiDesc, FontFactory.GetFont("arial", 8))));
                        Tardi_tabl4cel14a.PaddingBottom = 7;
                        Tardi_tabl4cel14a.BorderWidth = 0;
                        table4.AddCell(Tardi_tabl4cel14a);

                        PdfPCell Tardi_tabl4cel14b = new PdfPCell(new PdfPCell(new Paragraph(Tardi_Hours, FontFactory.GetFont("arial", 8))));
                        Tardi_tabl4cel14b.HorizontalAlignment = Element.ALIGN_RIGHT;
                        Tardi_tabl4cel14b.PaddingBottom = 7;
                        Tardi_tabl4cel14b.BorderWidth = 0;
                        table4.AddCell(Tardi_tabl4cel14b);

                        PdfPCell Tardi_tabl4cel14c = new PdfPCell(new PdfPCell(new Paragraph(Tardi_Amount, FontFactory.GetFont("arial", 8))));
                        Tardi_tabl4cel14c.HorizontalAlignment = Element.ALIGN_CENTER;
                        Tardi_tabl4cel14c.PaddingBottom = 7;
                        Tardi_tabl4cel14c.BorderWidth = 0;
                        table4.AddCell(Tardi_tabl4cel14c);

                        PdfPCell Tardi_tabl4cel14d = new PdfPCell(new PdfPCell(new Paragraph(Tardi_YTD_Amount, FontFactory.GetFont("arial", 8))));
                        Tardi_tabl4cel14d.HorizontalAlignment = Element.ALIGN_RIGHT;
                        Tardi_tabl4cel14d.PaddingBottom = 7;
                        Tardi_tabl4cel14d.BorderWidth = 0;
                        table4.AddCell(Tardi_tabl4cel14d);
                    }

                    if (dt17.Rows[0]["undertimeAmount"].ToString() != "0" && dt17.Rows[0]["undertimeAmount"].ToString() != "0.0000")
                    {
                        Undertime_Hours = dt16.Rows[0]["undertime"].ToString();
                        Undertime_Amount = dt16.Rows[0]["undertimeAmount"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt16.Rows[0]["undertimeAmount"].ToString()));
                        Undertime_YTD_Amount = dt17.Rows[0]["undertimeAmount"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt17.Rows[0]["undertimeAmount"].ToString()));

                        PdfPCell Undertime_tabl4cel14a = new PdfPCell(new PdfPCell(new Paragraph(UndertimeDesc, FontFactory.GetFont("arial", 8))));
                        Undertime_tabl4cel14a.PaddingBottom = 7;
                        Undertime_tabl4cel14a.BorderWidth = 0;
                        table4.AddCell(Undertime_tabl4cel14a);

                        PdfPCell Undertime_tabl4cel14b = new PdfPCell(new PdfPCell(new Paragraph(Undertime_Hours, FontFactory.GetFont("arial", 8))));
                        Undertime_tabl4cel14b.HorizontalAlignment = Element.ALIGN_RIGHT;
                        Undertime_tabl4cel14b.PaddingBottom = 7;
                        Undertime_tabl4cel14b.BorderWidth = 0;
                        table4.AddCell(Undertime_tabl4cel14b);

                        PdfPCell Undertime_tabl4cel14c = new PdfPCell(new PdfPCell(new Paragraph(Undertime_Amount, FontFactory.GetFont("arial", 8))));
                        Undertime_tabl4cel14c.HorizontalAlignment = Element.ALIGN_CENTER;
                        Undertime_tabl4cel14c.PaddingBottom = 7;
                        Undertime_tabl4cel14c.BorderWidth = 0;
                        table4.AddCell(Undertime_tabl4cel14c);

                        PdfPCell Undertime_tabl4cel14d = new PdfPCell(new PdfPCell(new Paragraph(Undertime_YTD_Amount, FontFactory.GetFont("arial", 8))));
                        Undertime_tabl4cel14d.HorizontalAlignment = Element.ALIGN_RIGHT;
                        Undertime_tabl4cel14d.PaddingBottom = 7;
                        Undertime_tabl4cel14d.BorderWidth = 0;
                        table4.AddCell(Undertime_tabl4cel14d);
                    }

                    if (dt17.Rows[0]["LWOPAmount"].ToString() != "0" && !string.IsNullOrWhiteSpace(dt17.Rows[0]["LWOPAmount"].ToString()) && dt17.Rows[0]["LWOPAmount"].ToString() != "0.0000" && dt17.Rows[0]["LWOPAmount"].ToString() != "0.00")
                    {
                        LWOP_Hours = dt16.Rows[0]["LWOP"].ToString();
                        LWOP_Amount = dt16.Rows[0]["LWOPAmount"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt16.Rows[0]["LWOPAmount"].ToString()));
                        LWOP_YTD_Amount = dt17.Rows[0]["LWOPAmount"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt17.Rows[0]["LWOPAmount"].ToString()));

                        PdfPCell LWOP_tabl4cel14a = new PdfPCell(new PdfPCell(new Paragraph(LWOPDesc, FontFactory.GetFont("arial", 8))));
                        LWOP_tabl4cel14a.PaddingBottom = 7;
                        LWOP_tabl4cel14a.BorderWidth = 0;
                        table4.AddCell(LWOP_tabl4cel14a);

                        PdfPCell LWOP_tabl4cel14b = new PdfPCell(new PdfPCell(new Paragraph(LWOP_Hours, FontFactory.GetFont("arial", 8))));
                        LWOP_tabl4cel14b.HorizontalAlignment = Element.ALIGN_RIGHT;
                        LWOP_tabl4cel14b.PaddingBottom = 7;
                        LWOP_tabl4cel14b.BorderWidth = 0;
                        table4.AddCell(LWOP_tabl4cel14b);

                        PdfPCell LWOP_tabl4cel14c = new PdfPCell(new PdfPCell(new Paragraph(LWOP_Amount, FontFactory.GetFont("arial", 8))));
                        LWOP_tabl4cel14c.HorizontalAlignment = Element.ALIGN_CENTER;
                        LWOP_tabl4cel14c.PaddingBottom = 7;
                        LWOP_tabl4cel14c.BorderWidth = 0;
                        table4.AddCell(LWOP_tabl4cel14c);

                        PdfPCell LWOP_tabl4cel14d = new PdfPCell(new PdfPCell(new Paragraph(LWOP_YTD_Amount, FontFactory.GetFont("arial", 8))));
                        LWOP_tabl4cel14d.HorizontalAlignment = Element.ALIGN_RIGHT;
                        LWOP_tabl4cel14d.PaddingBottom = 7;
                        LWOP_tabl4cel14d.BorderWidth = 0;
                        table4.AddCell(LWOP_tabl4cel14d);
                    }
                }
                // END

                // FOR DEDUCTIONS FIELDS
                // HEADER
                PdfPCell tabl4cell14 = new PdfPCell(new PdfPCell(new Paragraph("CONTRIBUTIONS", FontFactory.GetFont("arial", 8, Font.UNDERLINE)))) { Border = PdfPCell.NO_BORDER, HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT };
                tabl4cell14.Colspan = 4;
                tabl4cell14.PaddingTop = 10;
                tabl4cell14.PaddingBottom = 7;
                tabl4cell14.BorderWidth = 0;
                table4.AddCell(tabl4cell14);


                GDB = new GetDatabase();
                GDB.ClientName = prms.CN;
                SelfieRegistration2Controller.GetDB2(GDB);
                // DEDUCTION FOR GOV CONTRIBUTIONS
                // FOR SSS
                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID });
                con.myparameters.Add(new myParameters { ParameterName = "@payoutDate", mytype = SqlDbType.VarChar, Value = prms.PayoutDate });
                DataTable dt10 = con.GetDataTable("sp_GetGovernmentAmounts_SSS_Payslip");


                GDB = new GetDatabase();
                GDB.ClientName = prms.CN;
                SelfieRegistration2Controller.GetDB2(GDB);
                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID });
                con.myparameters.Add(new myParameters { ParameterName = "@year", mytype = SqlDbType.VarChar, Value = prms.ytd_year });
                con.myparameters.Add(new myParameters { ParameterName = "@payoutDate", mytype = SqlDbType.VarChar, Value = prms.PayoutDate });
                DataTable dt11 = con.GetDataTable("sp_GetGovernmentAmounts_SSS_YTD_Payslip");

                if (dt10.Rows.Count > 0)
                {
                    string SSSDesc = "SSS", SSSAmount = "", SSS_YTD_Amount = "";

                    SSSAmount = dt10.Rows[0]["SSS"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt10.Rows[0]["SSS"].ToString()));
                    SSS_YTD_Amount = dt11.Rows[0]["SSS"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt11.Rows[0]["SSS"].ToString()));

                    PdfPCell SSS_tabl4cel14a = new PdfPCell(new PdfPCell(new Paragraph(SSSDesc, FontFactory.GetFont("arial", 8))));
                    SSS_tabl4cel14a.PaddingBottom = 7;
                    SSS_tabl4cel14a.BorderWidth = 0;
                    table4.AddCell(SSS_tabl4cel14a);

                    PdfPCell SSS_tabl4cel14b = new PdfPCell(new PdfPCell(new Paragraph("", FontFactory.GetFont("arial", 8))));
                    SSS_tabl4cel14b.HorizontalAlignment = Element.ALIGN_RIGHT;
                    SSS_tabl4cel14b.PaddingBottom = 7;
                    SSS_tabl4cel14b.BorderWidth = 0;
                    table4.AddCell(SSS_tabl4cel14b);

                    PdfPCell SSS_tabl4cel14c = new PdfPCell(new PdfPCell(new Paragraph(SSSAmount, FontFactory.GetFont("arial", 8))));
                    SSS_tabl4cel14c.HorizontalAlignment = Element.ALIGN_CENTER;
                    SSS_tabl4cel14c.PaddingBottom = 7;
                    SSS_tabl4cel14c.BorderWidth = 0;
                    table4.AddCell(SSS_tabl4cel14c);

                    PdfPCell SSS_tabl4cel14d = new PdfPCell(new PdfPCell(new Paragraph(SSS_YTD_Amount, FontFactory.GetFont("arial", 8))));
                    SSS_tabl4cel14d.HorizontalAlignment = Element.ALIGN_RIGHT;
                    SSS_tabl4cel14d.PaddingBottom = 7;
                    SSS_tabl4cel14d.BorderWidth = 0;
                    table4.AddCell(SSS_tabl4cel14d);
                }


                GDB = new GetDatabase();
                GDB.ClientName = prms.CN;
                SelfieRegistration2Controller.GetDB2(GDB);
                // FOR PHILHEALTH
                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID });
                con.myparameters.Add(new myParameters { ParameterName = "@payoutDate", mytype = SqlDbType.VarChar, Value = prms.PayoutDate });
                DataTable dt12 = con.GetDataTable("sp_GetGovernmentAmounts_Philhealth_Payslip");



                GDB = new GetDatabase();
                GDB.ClientName = prms.CN;
                SelfieRegistration2Controller.GetDB2(GDB);
                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID });
                con.myparameters.Add(new myParameters { ParameterName = "@year", mytype = SqlDbType.VarChar, Value = prms.ytd_year });
                con.myparameters.Add(new myParameters { ParameterName = "@payoutDate", mytype = SqlDbType.VarChar, Value = prms.PayoutDate });
                DataTable dt13 = con.GetDataTable("sp_GetGovernmentAmounts_Philhealth_YTD_Payslip");

                if (dt12.Rows.Count > 0)
                {
                    string PHealthDesc = "Philhealth", PHealthAmount = "", PHealth_YTD_Amount = "";

                    PHealthAmount = dt12.Rows[0]["Philhealth"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt12.Rows[0]["Philhealth"].ToString()));
                    PHealth_YTD_Amount = dt13.Rows[0]["philhealth"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt13.Rows[0]["philhealth"].ToString()));

                    PdfPCell PHLHEALTH_tabl4cel14a = new PdfPCell(new PdfPCell(new Paragraph(PHealthDesc, FontFactory.GetFont("arial", 8))));
                    PHLHEALTH_tabl4cel14a.PaddingBottom = 7;
                    PHLHEALTH_tabl4cel14a.BorderWidth = 0;
                    table4.AddCell(PHLHEALTH_tabl4cel14a);

                    PdfPCell PHLHEALTH_tabl4cel14b = new PdfPCell(new PdfPCell(new Paragraph("", FontFactory.GetFont("arial", 8))));
                    PHLHEALTH_tabl4cel14b.HorizontalAlignment = Element.ALIGN_RIGHT;
                    PHLHEALTH_tabl4cel14b.PaddingBottom = 7;
                    PHLHEALTH_tabl4cel14b.BorderWidth = 0;
                    table4.AddCell(PHLHEALTH_tabl4cel14b);

                    PdfPCell PHLHEALTH_tabl4cel14c = new PdfPCell(new PdfPCell(new Paragraph(PHealthAmount, FontFactory.GetFont("arial", 8))));
                    PHLHEALTH_tabl4cel14c.HorizontalAlignment = Element.ALIGN_CENTER;
                    PHLHEALTH_tabl4cel14c.PaddingBottom = 7;
                    PHLHEALTH_tabl4cel14c.BorderWidth = 0;
                    table4.AddCell(PHLHEALTH_tabl4cel14c);

                    PdfPCell PHLHEALTH_tabl4cel14d = new PdfPCell(new PdfPCell(new Paragraph(PHealth_YTD_Amount, FontFactory.GetFont("arial", 8))));
                    PHLHEALTH_tabl4cel14d.HorizontalAlignment = Element.ALIGN_RIGHT;
                    PHLHEALTH_tabl4cel14d.PaddingBottom = 7;
                    PHLHEALTH_tabl4cel14d.BorderWidth = 0;
                    table4.AddCell(PHLHEALTH_tabl4cel14d);
                }


                GDB = new GetDatabase();
                GDB.ClientName = prms.CN;
                SelfieRegistration2Controller.GetDB2(GDB);
                // FOR HDMF
                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID });
                con.myparameters.Add(new myParameters { ParameterName = "@payoutDate", mytype = SqlDbType.VarChar, Value = prms.PayoutDate });
                DataTable dt14 = con.GetDataTable("sp_GetGovernmentAmounts_Pagibig_Payslip");


                GDB = new GetDatabase();
                GDB.ClientName = prms.CN;
                SelfieRegistration2Controller.GetDB2(GDB);
                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID });
                con.myparameters.Add(new myParameters { ParameterName = "@year", mytype = SqlDbType.VarChar, Value = prms.ytd_year });
                con.myparameters.Add(new myParameters { ParameterName = "@payoutDate", mytype = SqlDbType.VarChar, Value = prms.PayoutDate });
                DataTable dt15 = con.GetDataTable("sp_GetGovernmentAmounts_Pagibig_YTD_Payslip");

                if (dt14.Rows.Count > 0)
                {
                    string HDMFDesc = "HDMF", HDMFAmount = "", HDMF_YTD_Amount = "";
                    HDMFAmount = dt14.Rows[0]["HDMF"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt14.Rows[0]["HDMF"].ToString()));
                    HDMF_YTD_Amount = dt15.Rows[0]["Pagibig_Employee"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt15.Rows[0]["Pagibig_Employee"].ToString()));

                    PdfPCell HDMF_tabl4cel14a = new PdfPCell(new PdfPCell(new Paragraph(HDMFDesc, FontFactory.GetFont("arial", 8))));
                    HDMF_tabl4cel14a.PaddingBottom = 7;
                    HDMF_tabl4cel14a.BorderWidth = 0;
                    table4.AddCell(HDMF_tabl4cel14a);

                    PdfPCell HDMF_tabl4cel14b = new PdfPCell(new PdfPCell(new Paragraph("", FontFactory.GetFont("arial", 8))));
                    HDMF_tabl4cel14b.HorizontalAlignment = Element.ALIGN_RIGHT;
                    HDMF_tabl4cel14b.PaddingBottom = 7;
                    HDMF_tabl4cel14b.BorderWidth = 0;
                    table4.AddCell(HDMF_tabl4cel14b);

                    PdfPCell HDMF_tabl4cel14c = new PdfPCell(new PdfPCell(new Paragraph(HDMFAmount, FontFactory.GetFont("arial", 8))));
                    HDMF_tabl4cel14c.HorizontalAlignment = Element.ALIGN_CENTER;
                    HDMF_tabl4cel14c.PaddingBottom = 7;
                    HDMF_tabl4cel14c.BorderWidth = 0;
                    table4.AddCell(HDMF_tabl4cel14c);

                    PdfPCell HDMF_tabl4cel14d = new PdfPCell(new PdfPCell(new Paragraph(HDMF_YTD_Amount, FontFactory.GetFont("arial", 8))));
                    HDMF_tabl4cel14d.HorizontalAlignment = Element.ALIGN_RIGHT;
                    HDMF_tabl4cel14d.PaddingBottom = 7;
                    HDMF_tabl4cel14d.BorderWidth = 0;
                    table4.AddCell(HDMF_tabl4cel14d);
                }




                GDB = new GetDatabase();
                GDB.ClientName = prms.CN;
                SelfieRegistration2Controller.GetDB2(GDB);


                // FOR LOANS FIELD
                PdfPCell tabl4cell15 = new PdfPCell(new PdfPCell(new Paragraph("DEDUCTIONS", FontFactory.GetFont("arial", 8, Font.UNDERLINE))));
                tabl4cell14.PaddingTop = 30;
                tabl4cell15.PaddingBottom = 7;
                tabl4cell15.BorderWidth = 0;
                table4.AddCell(tabl4cell15);

                //PdfPCell tabl4cell16 = new PdfPCell(new PdfPCell(new Paragraph("BALANCE LEFT", FontFactory.GetFont("arial", 8, Font.BOLD))));
                PdfPCell tabl4cell16 = new PdfPCell(new PdfPCell(new Paragraph("", FontFactory.GetFont("arial", 8, Font.BOLD))));
                tabl4cell16.HorizontalAlignment = Element.ALIGN_RIGHT;
                tabl4cell16.PaddingBottom = 7;
                tabl4cell14.PaddingTop = 20;
                tabl4cell16.BorderWidth = 0;
                table4.AddCell(tabl4cell16);

                PdfPCell tabl4cell17 = new PdfPCell(new PdfPCell(new Paragraph("", FontFactory.GetFont("arial", 8, Font.BOLD))));
                tabl4cell17.PaddingBottom = 7;
                tabl4cell17.BorderWidth = 0;
                table4.AddCell(tabl4cell17);

                PdfPCell tabl4cell18 = new PdfPCell(new PdfPCell(new Paragraph("", FontFactory.GetFont("arial", 8, Font.BOLD))));
                tabl4cell18.HorizontalAlignment = Element.ALIGN_RIGHT;
                tabl4cell18.PaddingBottom = 7;
                tabl4cell18.BorderWidth = 0;
                table4.AddCell(tabl4cell18);


                GDB = new GetDatabase();
                GDB.ClientName = prms.CN;
                SelfieRegistration2Controller.GetDB2(GDB);
                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID });
                con.myparameters.Add(new myParameters { ParameterName = "@payoutDate", mytype = SqlDbType.VarChar, Value = prms.PayoutDate });
                DataTable dt20 = con.GetDataTable("sp_GetLoans_Payslip");


                GDB = new GetDatabase();
                GDB.ClientName = prms.CN;
                SelfieRegistration2Controller.GetDB2(GDB);
                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID });
                con.myparameters.Add(new myParameters { ParameterName = "@year", mytype = SqlDbType.VarChar, Value = prms.ytd_year });
                con.myparameters.Add(new myParameters { ParameterName = "@payoutDate", mytype = SqlDbType.VarChar, Value = prms.PayoutDate });
                DataTable dt21 = con.GetDataTable("sp_GetLoans_YTD_Payslip");

                if (dt20.Rows.Count > 0)
                {
                    string LoanName = "", Amortization_Amount = "", Amortization_YTD_Amount = "";

                    for (int i = 0; i < dt20.Rows.Count; i++)
                    {
                        LoanName = dt20.Rows[i]["LoanName"].ToString();
                        Amortization_Amount = dt20.Rows[i]["AmortizationAmount"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt20.Rows[i]["AmortizationAmount"].ToString()));
                        Amortization_YTD_Amount = dt21.Rows[i]["AmortizationAmount"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt21.Rows[i]["AmortizationAmount"].ToString()));

                        PdfPCell Loan_tabl4cel14a = new PdfPCell(new PdfPCell(new Paragraph(LoanName, FontFactory.GetFont("arial", 8))));
                        Loan_tabl4cel14a.PaddingBottom = 7;
                        Loan_tabl4cel14a.BorderWidth = 0;
                        table4.AddCell(Loan_tabl4cel14a);

                        PdfPCell Loan_tabl4cel14b = new PdfPCell(new PdfPCell(new Paragraph("", FontFactory.GetFont("arial", 8))));
                        Loan_tabl4cel14b.HorizontalAlignment = Element.ALIGN_RIGHT;
                        Loan_tabl4cel14b.PaddingBottom = 7;
                        Loan_tabl4cel14b.BorderWidth = 0;
                        table4.AddCell(Loan_tabl4cel14b);

                        PdfPCell Loan_tabl4cel14c = new PdfPCell(new PdfPCell(new Paragraph(Amortization_Amount, FontFactory.GetFont("arial", 8))));
                        Loan_tabl4cel14c.HorizontalAlignment = Element.ALIGN_CENTER;
                        Loan_tabl4cel14c.PaddingBottom = 7;
                        Loan_tabl4cel14c.BorderWidth = 0;
                        table4.AddCell(Loan_tabl4cel14c);

                        PdfPCell Loan_tabl4cel14d = new PdfPCell(new PdfPCell(new Paragraph(Amortization_YTD_Amount, FontFactory.GetFont("arial", 8))));
                        Loan_tabl4cel14d.HorizontalAlignment = Element.ALIGN_RIGHT;
                        Loan_tabl4cel14d.PaddingBottom = 7;
                        Loan_tabl4cel14d.BorderWidth = 0;
                        table4.AddCell(Loan_tabl4cel14d);
                    }
                }
                //
                // OTHER DEDUCTIONS
                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID });
                con.myparameters.Add(new myParameters { ParameterName = "@payoutDate", mytype = SqlDbType.VarChar, Value = prms.PayoutDate });
                DataTable dt18 = con.GetDataTable("sp_GetDeductions_Payslip");


                GDB = new GetDatabase();
                GDB.ClientName = prms.CN;
                SelfieRegistration2Controller.GetDB2(GDB);
                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID });
                con.myparameters.Add(new myParameters { ParameterName = "@year", mytype = SqlDbType.VarChar, Value = prms.ytd_year });
                con.myparameters.Add(new myParameters { ParameterName = "@payoutDate", mytype = SqlDbType.VarChar, Value = prms.PayoutDate });
                DataTable dt19 = con.GetDataTable("sp_GetDeductions_YTD_Payslip");

                if (dt18.Rows.Count > 0)
                {
                    for (int i = 0; i < dt18.Rows.Count; i++)
                    {
                        string DeductionDesc = "", Deduction_Amount = "", Deduction_YTD_Amount = "";

                        DeductionDesc = dt18.Rows[i]["deductiondescription"].ToString();
                        Deduction_Amount = dt18.Rows[i]["deductionamount"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt18.Rows[i]["deductionamount"].ToString()));
                        Deduction_YTD_Amount = dt19.Rows[i]["deductionAmount"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt19.Rows[i]["deductionAmount"].ToString()));

                        PdfPCell OtherDeduction_tabl4cel14a = new PdfPCell(new PdfPCell(new Paragraph(DeductionDesc, FontFactory.GetFont("arial", 8))));
                        OtherDeduction_tabl4cel14a.PaddingBottom = 7;
                        OtherDeduction_tabl4cel14a.BorderWidth = 0;
                        table4.AddCell(OtherDeduction_tabl4cel14a);

                        PdfPCell OtherDeduction_tabl4cel14b = new PdfPCell(new PdfPCell(new Paragraph("", FontFactory.GetFont("arial", 8))));
                        OtherDeduction_tabl4cel14b.HorizontalAlignment = Element.ALIGN_RIGHT;
                        OtherDeduction_tabl4cel14b.PaddingBottom = 7;
                        OtherDeduction_tabl4cel14b.BorderWidth = 0;
                        table4.AddCell(OtherDeduction_tabl4cel14b);

                        PdfPCell OtherDeduction_tabl4cel14c = new PdfPCell(new PdfPCell(new Paragraph(Deduction_Amount, FontFactory.GetFont("arial", 8))));
                        OtherDeduction_tabl4cel14c.HorizontalAlignment = Element.ALIGN_CENTER;
                        OtherDeduction_tabl4cel14c.PaddingBottom = 7;
                        OtherDeduction_tabl4cel14c.BorderWidth = 0;
                        table4.AddCell(OtherDeduction_tabl4cel14c);

                        PdfPCell OtherDeduction_tabl4cel14d = new PdfPCell(new PdfPCell(new Paragraph(Deduction_YTD_Amount, FontFactory.GetFont("arial", 8))));
                        OtherDeduction_tabl4cel14d.HorizontalAlignment = Element.ALIGN_RIGHT;
                        OtherDeduction_tabl4cel14d.PaddingBottom = 7;
                        OtherDeduction_tabl4cel14d.BorderWidth = 0;
                        table4.AddCell(OtherDeduction_tabl4cel14d);
                    }
                }
                //
                //
                PdfPCell tabl4cell19 = new PdfPCell(new PdfPCell(new Paragraph("WithHolding Tax", FontFactory.GetFont("arial", 8))));
                tabl4cell19.PaddingBottom = 7;
                tabl4cell19.BorderWidth = 0;
                tabl4cell19.BackgroundColor = BaseColor.LIGHT_GRAY;
                table4.AddCell(tabl4cell19);

                PdfPCell tabl4cell20 = new PdfPCell(new PdfPCell(new Paragraph("", FontFactory.GetFont("arial", 8))));
                tabl4cell20.HorizontalAlignment = Element.ALIGN_RIGHT;
                tabl4cell20.PaddingBottom = 7;
                tabl4cell20.BorderWidth = 0;
                tabl4cell20.BackgroundColor = BaseColor.LIGHT_GRAY;
                table4.AddCell(tabl4cell20);

                PdfPCell tabl4cell21 = new PdfPCell(new PdfPCell(new Paragraph(whtax, FontFactory.GetFont("arial", 8))));
                tabl4cell21.HorizontalAlignment = Element.ALIGN_CENTER;
                tabl4cell21.PaddingBottom = 7;
                tabl4cell21.BorderWidth = 0;
                tabl4cell21.BackgroundColor = BaseColor.LIGHT_GRAY;
                table4.AddCell(tabl4cell21);

                PdfPCell tabl4cell22 = new PdfPCell(new PdfPCell(new Paragraph(ytdWhTax, FontFactory.GetFont("arial", 8))));
                tabl4cell22.HorizontalAlignment = Element.ALIGN_RIGHT;
                tabl4cell22.PaddingBottom = 7;
                tabl4cell22.BorderWidth = 0;
                tabl4cell22.BackgroundColor = BaseColor.LIGHT_GRAY;
                table4.AddCell(tabl4cell22);
                //

                //
                PdfPCell tabl4cell23 = new PdfPCell(new PdfPCell(new Paragraph("Net Pay", FontFactory.GetFont("arial", 8))));
                tabl4cell23.PaddingBottom = 7;
                tabl4cell23.BorderWidth = 0;
                table4.AddCell(tabl4cell23);

                PdfPCell tabl4cell24 = new PdfPCell(new PdfPCell(new Paragraph("", FontFactory.GetFont("arial", 8))));
                tabl4cell24.HorizontalAlignment = Element.ALIGN_RIGHT;
                tabl4cell24.PaddingBottom = 7;
                tabl4cell24.BorderWidth = 0;
                table4.AddCell(tabl4cell24);

                PdfPCell tabl4cell25 = new PdfPCell(new PdfPCell(new Paragraph(netpay, FontFactory.GetFont("arial", 8))));
                tabl4cell25.HorizontalAlignment = Element.ALIGN_CENTER;
                tabl4cell25.PaddingBottom = 7;
                tabl4cell25.BorderWidth = 0;
                table4.AddCell(tabl4cell25);

                PdfPCell tabl4cell26 = new PdfPCell(new PdfPCell(new Paragraph(ytdNetPay, FontFactory.GetFont("arial", 8))));
                tabl4cell26.HorizontalAlignment = Element.ALIGN_RIGHT;
                tabl4cell26.PaddingBottom = 7;
                tabl4cell26.BorderWidth = 0;
                table4.AddCell(tabl4cell26);


                //
                PdfPCell tabl4cell27 = new PdfPCell(new PdfPCell(new Paragraph("", FontFactory.GetFont("arial", 8, Font.UNDERLINE)))) { Border = PdfPCell.NO_BORDER, HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT };
                tabl4cell27.Colspan = 4;
                tabl4cell27.PaddingTop = 10;
                tabl4cell27.PaddingBottom = 7;
                tabl4cell27.BorderWidth = 0;
                table4.AddCell(tabl4cell27);
                //
                table2.AddCell(table4);


                PdfPCell cell22 = new PdfPCell() { Border = PdfPCell.NO_BORDER };
                cell22.Colspan = 2;


                table1.AddCell(cell11);
                table1.AddCell(cell12);
                table2.AddCell(cell22);
                document.Add(table0);
                document.Add(table1);
                document.Add(table2);
                document.Close();
                string onlineDIR = "http://ws.durusthr.com/ILM_WS_Live/pdf/";
                //return onlineDIR + filename;
                return "success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
            #region comment ios payslip generations

            //try
            //{
            //    GetDatabase GDB = new GetDatabase();
            //    GDB.ClientName = prms.CN;
            //    SelfieRegistration2Controller.GetDB2(GDB);

            //    // DATA VARIABLE DECLARATION
            //    string startdate = "", enddate = "";
            //    string payday = "";
            //    string fullname = "", empid = "", datejoined = "", position = "", department = "", TIN = "", SSS = "", PHILHEALTH = "", PAGIBIG = "";
            //    string basicpay = "", grosspay = "", netpay = "", whtax = "", ytdBasicPay = "", ytdGrossPay = "", ytdNetPay = "", ytdWhTax = "";
            //    string HourlyRate = "", HoursWorked = "", AbsencesDesc = "Absences", TardiDesc = "Tardiness", UndertimeDesc = "Undertime", LWOPDesc = "LWOP", Monthly = "";
            //    string Absences_Hours = "", Tardi_Hours = "", Undertime_Hours = "", LWOP_Hours = "";
            //    string Absences_Amount = "", Tardi_Amount = "", Undertime_Amount = "", LWOP_Amount = "";
            //    string Absences_YTD_Amount = "", Tardi_YTD_Amount = "", Undertime_YTD_Amount = "", LWOP_YTD_Amount = "";

            //    // FOR PAY PERIOD FIELD
            //    Connection con = new Connection();
            //    con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID });
            //    con.myparameters.Add(new myParameters { ParameterName = "@payoutDate", mytype = SqlDbType.VarChar, Value = prms.PayoutDate });
            //    DataTable DT = con.GetDataTable("sp_GetPayPeriod_Payslip");
            //    foreach (DataRow item in DT.Rows)
            //    {
            //        startdate = item["StartDate"].ToString();
            //        enddate = item["EndDate"].ToString();
            //    }


            //    GDB = new GetDatabase();
            //    GDB.ClientName = prms.CN;
            //    SelfieRegistration2Controller.GetDB2(GDB);
            //    // FOR PAY DATE FIELD
            //    con = new Connection();
            //    con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID });
            //    DataTable DT1 = con.GetDataTable("sp_GetPayoutSchemeList");
            //    foreach (DataRow item in DT1.Rows)
            //    {
            //        payday = item["PayoutDate"].ToString();
            //    }

            //    GDB = new GetDatabase();
            //    GDB.ClientName = prms.CN;
            //    SelfieRegistration2Controller.GetDB2(GDB);
            //    // EMPLOYEE DETAILS FIELD
            //    con = new Connection();
            //    con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID });
            //    DataTable dt2 = con.GetDataTable("sp_GetEmployeeInfo_Payslip");
            //    foreach (DataRow item in dt2.Rows)
            //    {
            //        fullname = item["EmpName"].ToString();
            //        empid = item["EmpID"].ToString();
            //        datejoined = item["DateJoined"].ToString();
            //        department = item["BusinessUnit"].ToString();
            //        position = item["JobDesc"].ToString();
            //    }

            //    GDB = new GetDatabase();
            //    GDB.ClientName = prms.CN;
            //    SelfieRegistration2Controller.GetDB2(GDB);
            //    // FOR GOVERNMENT ID FIELDS
            //    con = new Connection();
            //    con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID });
            //    DataTable dt3 = con.GetDataTable("sp_GetGovIDs_Payslip");
            //    foreach (DataRow item in dt3.Rows)
            //    {
            //        if (item["IDTypes"].ToString() == "SSS")
            //        {
            //            SSS = item["IDCode"].ToString();
            //        }
            //        else if (item["IDTypes"].ToString() == "Philhealth")
            //        {
            //            PHILHEALTH = item["IDCode"].ToString();
            //        }
            //        else if (item["IDTypes"].ToString() == "Pag-Ibig")
            //        {
            //            PAGIBIG = item["IDCode"].ToString();
            //        }
            //        else
            //        {
            //            TIN = item["IDCode"].ToString();
            //        }
            //    }

            //    GDB = new GetDatabase();
            //    GDB.ClientName = prms.CN;
            //    SelfieRegistration2Controller.GetDB2(GDB);
            //    // FOR GROSS PAY, BASIC PAY AND NET PAY FIELDS
            //    con = new Connection();
            //    con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID });
            //    con.myparameters.Add(new myParameters { ParameterName = "@payoutDate", mytype = SqlDbType.VarChar, Value = prms.PayoutDate });
            //    DataTable dt4 = con.GetDataTable("sp_GetPay_Payslip");
            //    foreach (DataRow item in dt4.Rows)
            //    {
            //        basicpay = item["Basic Pay"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(item["Basic Pay"].ToString()));
            //        grosspay = item["Gross Pay"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(item["Gross Pay"].ToString()));
            //        netpay = item["Net Pay"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(item["Net Pay"].ToString()));
            //        whtax = item["WithHolding Tax"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(item["WithHolding Tax"].ToString()));

            //        //basicpay = String.Format("{0:n2}", decimal.Parse(basicpay));
            //    }
            //    // END


            //    GDB = new GetDatabase();
            //    GDB.ClientName = prms.CN;
            //    SelfieRegistration2Controller.GetDB2(GDB);
            //    // YTD GROSS PAY, BASIC PAY AND NET PAY FIELDS
            //    con = new Connection();
            //    con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID });
            //    con.myparameters.Add(new myParameters { ParameterName = "@year", mytype = SqlDbType.VarChar, Value = prms.ytd_year });
            //    con.myparameters.Add(new myParameters { ParameterName = "@payoutDate", mytype = SqlDbType.VarChar, Value = prms.PayoutDate });
            //    DataTable dt5 = con.GetDataTable("sp_GetPay_YTD_Payslip");
            //    foreach (DataRow item in dt5.Rows)
            //    {
            //        ytdBasicPay = item["basicpay"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(item["basicpay"].ToString()));
            //        ytdGrossPay = item["grosspay"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(item["grosspay"].ToString()));
            //        ytdWhTax = item["whtax"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(item["whtax"].ToString()));
            //        ytdNetPay = item["netpay"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(item["netpay"].ToString()));
            //    }

            //    GDB = new GetDatabase();
            //    GDB.ClientName = prms.CN;
            //    SelfieRegistration2Controller.GetDB2(GDB);
            //    // FOR LOGS INFO
            //    con = new Connection();
            //    con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID });
            //    con.myparameters.Add(new myParameters { ParameterName = "@payoutDate", mytype = SqlDbType.VarChar, Value = prms.PayoutDate });
            //    DataTable dt16 = con.GetDataTable("sp_GetLogInfo_Payslip");

            //    HourlyRate = dt16.Rows[0]["HourlyRate"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt16.Rows[0]["HourlyRate"].ToString()));
            //    HoursWorked = dt16.Rows[0]["HoursWorked"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt16.Rows[0]["HoursWorked"].ToString()));
            //    //Monthly = dt16.Rows[0]["Monthly"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt16.Rows[0]["Monthly"].ToString()));

            //    GDB = new GetDatabase();
            //    GDB.ClientName = prms.CN;
            //    SelfieRegistration2Controller.GetDB2(GDB);
            //    con = new Connection();
            //    con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID });
            //    con.myparameters.Add(new myParameters { ParameterName = "@year", mytype = SqlDbType.VarChar, Value = prms.ytd_year });
            //    con.myparameters.Add(new myParameters { ParameterName = "@payoutDate", mytype = SqlDbType.VarChar, Value = prms.PayoutDate });
            //    DataTable dt17 = con.GetDataTable("sp_GetLogInfo_YTD_Payslip");

            //    System.IO.FileStream fs = new FileStream(HttpContext.Current.Server.MapPath("pdf") + "\\" + "payslip-" + prms.EmpID + "-" + prms.PayoutDate + ".pdf", FileMode.Create);
            //    //System.IO.FileStream fb = new FileStream(HttpContext.Current.Server.MapPath("pdf") + "\\" + "ilm.png", FileMode.Open);
            //    string imagepath = HttpContext.Current.Server.MapPath("Images");
            //    Document document = new Document(PageSize.A4, 25, 25, 30, 30);
            //    PdfWriter writer = PdfWriter.GetInstance(document, fs);
            //    //PdfWriter writering = PdfWriter.GetInstance(document, fb);
            //    document.Open();

            //    PdfPTable table0 = new PdfPTable(1);

            //    PdfPCell cell0 = new PdfPCell(new iTextSharp.text.Phrase("HITACHI CABLE PHILIPPINES, INC", FontFactory.GetFont("Arial", 12, Font.BOLD))) { Border = 0, HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT, VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE };
            //    cell0.PaddingTop = 10;
            //    PdfPCell cell01 = new PdfPCell(new iTextSharp.text.Phrase("Lima Technology Center  Lipa City, Batangas Philippines 4217", FontFactory.GetFont("Arial", 11, Font.NORMAL))) { Border = 0, HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT, VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE };
            //    cell01.PaddingBottom = 7;

            //    Image logoimage = Image.GetInstance(Path.Combine(imagepath, "yokohamalogo.jpg"));
            //    logoimage.ScaleAbsolute(100, 40);
            //    logoimage.SetAbsolutePosition(100, 30);

            //    PdfPCell LogoCell = new PdfPCell(logoimage, true);

            //    table0.AddCell(cell0);
            //    //table0.AddCell(LogoCell);
            //    table0.AddCell(cell01);
            //    //document.Add(LogoCell);


            //    PdfPTable table1 = new PdfPTable(2);

            //    /*var titleFont = new Font(Font.FontFamily.UNDEFINED, 12);
            //    var informationDetails = new Font(Font.FontFamily.UNDEFINED, 12);*/

            //    PdfPCell cell11 = new PdfPCell(new iTextSharp.text.Phrase("PAY PERIOD\n" + startdate + " - " + enddate)) { Border = PdfPCell.BOTTOM_BORDER | PdfPCell.TOP_BORDER, HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER, VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE };
            //    PdfPCell cell12 = new PdfPCell(new iTextSharp.text.Phrase("PAY DAY\n" + prms.PayoutDate)) { Border = PdfPCell.BOTTOM_BORDER | PdfPCell.TOP_BORDER, HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER, VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE };
            //    cell11.PaddingTop = 10;
            //    cell12.PaddingTop = 10;
            //    cell11.PaddingBottom = 7;
            //    cell12.PaddingBottom = 7;
            //    /* cell12.AddElement(new Paragraph("PAY DAY", titleFont));
            //    cell12.AddElement(new Paragraph("-", informationDetails));*/

            //    PdfPTable table2 = new PdfPTable(2);
            //    float[] widths = new float[] { 100f, 200f };
            //    float[] heights = new float[] { 300f };
            //    table2.SetWidths(widths);
            //    //table2.DefaultCell.FixedHeight = 500f;
            //    table2.DefaultCell.Border = Rectangle.NO_BORDER;
            //    //PdfPCell cell21 = new PdfPCell(new Paragraph("Danilo Vicente Sulla Jr")) { Border = PdfPCell.RIGHT_BORDER, HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER, VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE };
            //    //cell21.PaddingTop = 20;

            //    PdfPTable table3 = new PdfPTable(2);
            //    table3.TotalWidth = 100;
            //    table3.DefaultCell.FixedHeight = 600f;
            //    table3.DefaultCell.Border = Rectangle.RIGHT_BORDER;

            //    PdfPCell tabl3cell0 = new PdfPCell(new PdfPCell(new Paragraph(fullname, FontFactory.GetFont("Arial", 12, Font.BOLD)))) { Border = PdfPCell.NO_BORDER, HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER, VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE };
            //    tabl3cell0.Colspan = 2;
            //    tabl3cell0.PaddingTop = 20;
            //    tabl3cell0.PaddingBottom = 20;
            //    tabl3cell0.BorderWidth = 0;
            //    tabl3cell0.BorderWidthRight = 1;
            //    table3.AddCell(tabl3cell0);


            //    PdfPCell tabl3cell1 = new PdfPCell(new PdfPCell(new Paragraph("EmpID", FontFactory.GetFont("arial", 8))));
            //    tabl3cell1.PaddingBottom = 7;
            //    tabl3cell1.BorderWidth = 0;
            //    tabl3cell1.BorderWidthBottom = 1;
            //    table3.AddCell(tabl3cell1);

            //    PdfPCell tabl3cell2 = new PdfPCell(new PdfPCell(new Paragraph(empid, FontFactory.GetFont("arial", 8, Font.BOLD))));
            //    tabl3cell2.HorizontalAlignment = Element.ALIGN_RIGHT;
            //    tabl3cell2.PaddingBottom = 7;
            //    tabl3cell2.BorderWidth = 0;
            //    tabl3cell2.BorderWidthBottom = 1;
            //    tabl3cell2.BorderWidthRight = 1;
            //    table3.AddCell(tabl3cell2);

            //    PdfPCell tabl3cell3 = new PdfPCell(new PdfPCell(new Paragraph("Tax Category", FontFactory.GetFont("arial", 8))));
            //    tabl3cell3.PaddingBottom = 7;
            //    tabl3cell3.BorderWidth = 0;
            //    tabl3cell3.BorderWidthBottom = 1;
            //    table3.AddCell(tabl3cell3);

            //    PdfPCell tabl3cell4 = new PdfPCell(new PdfPCell(new Paragraph("--", FontFactory.GetFont("arial", 8, Font.BOLD))));
            //    tabl3cell4.HorizontalAlignment = Element.ALIGN_RIGHT;
            //    tabl3cell4.PaddingBottom = 7;
            //    tabl3cell4.BorderWidth = 0;
            //    tabl3cell4.BorderWidthBottom = 1;
            //    tabl3cell4.BorderWidthRight = 1;
            //    table3.AddCell(tabl3cell4);

            //    PdfPCell tabl3cell5 = new PdfPCell(new PdfPCell(new Paragraph("Hourly Rate", FontFactory.GetFont("arial", 8))));
            //    tabl3cell5.PaddingBottom = 7;
            //    tabl3cell5.BorderWidth = 0;
            //    tabl3cell5.BorderWidthBottom = 1;
            //    table3.AddCell(tabl3cell5);

            //    PdfPCell tabl3cell6 = new PdfPCell(new PdfPCell(new Paragraph(HourlyRate, FontFactory.GetFont("arial", 8, Font.BOLD))));
            //    tabl3cell6.HorizontalAlignment = Element.ALIGN_RIGHT;
            //    tabl3cell6.PaddingBottom = 7;
            //    tabl3cell6.BorderWidth = 0;
            //    tabl3cell6.BorderWidthBottom = 1;
            //    tabl3cell6.BorderWidthRight = 1;
            //    table3.AddCell(tabl3cell6);

            //    PdfPCell tabl3cell7 = new PdfPCell(new PdfPCell(new Paragraph("Regular Hours", FontFactory.GetFont("arial", 8))));
            //    tabl3cell7.PaddingBottom = 7;
            //    tabl3cell7.BorderWidth = 0;
            //    tabl3cell7.BorderWidthBottom = 1;
            //    table3.AddCell(tabl3cell7);

            //    PdfPCell tabl3cell8 = new PdfPCell(new PdfPCell(new Paragraph(HoursWorked, FontFactory.GetFont("arial", 8, Font.BOLD))));
            //    tabl3cell8.HorizontalAlignment = Element.ALIGN_RIGHT;
            //    tabl3cell8.PaddingBottom = 7;
            //    tabl3cell8.BorderWidth = 0;
            //    tabl3cell8.BorderWidthBottom = 1;
            //    tabl3cell8.BorderWidthRight = 1;
            //    table3.AddCell(tabl3cell8);

            //    PdfPCell tabl3cell9 = new PdfPCell(new PdfPCell(new Paragraph(getSalaryScheme(Monthly.ToString()) + " Salary", FontFactory.GetFont("arial", 8))));
            //    tabl3cell9.PaddingBottom = 7;
            //    tabl3cell9.BorderWidth = 0;
            //    tabl3cell9.BorderWidthBottom = 1;
            //    table3.AddCell(tabl3cell9);

            //    PdfPCell tabl3cell10 = new PdfPCell(new PdfPCell(new Paragraph(Monthly, FontFactory.GetFont("arial", 8, Font.BOLD))));
            //    tabl3cell10.HorizontalAlignment = Element.ALIGN_RIGHT;
            //    tabl3cell10.PaddingBottom = 7;
            //    tabl3cell10.BorderWidth = 0;
            //    tabl3cell10.BorderWidthBottom = 1;
            //    tabl3cell10.BorderWidthRight = 1;
            //    table3.AddCell(tabl3cell10);

            //    PdfPCell tabl3cell11 = new PdfPCell(new PdfPCell(new Paragraph("Date Joined", FontFactory.GetFont("arial", 8))));
            //    tabl3cell11.PaddingBottom = 7;
            //    tabl3cell11.BorderWidth = 0;
            //    tabl3cell11.BorderWidthBottom = 1;
            //    table3.AddCell(tabl3cell11);

            //    PdfPCell tabl3cell12 = new PdfPCell(new PdfPCell(new Paragraph(datejoined, FontFactory.GetFont("arial", 8, Font.BOLD))));
            //    tabl3cell12.HorizontalAlignment = Element.ALIGN_RIGHT;
            //    tabl3cell12.PaddingBottom = 7;
            //    tabl3cell12.BorderWidth = 0;
            //    tabl3cell12.BorderWidthBottom = 1;
            //    tabl3cell12.BorderWidthRight = 1;
            //    table3.AddCell(tabl3cell12);

            //    PdfPCell tabl3cell13 = new PdfPCell(new PdfPCell(new Paragraph("Position", FontFactory.GetFont("arial", 8))));
            //    tabl3cell13.PaddingBottom = 7;
            //    tabl3cell13.BorderWidth = 0;
            //    tabl3cell13.BorderWidthBottom = 1;
            //    table3.AddCell(tabl3cell13);

            //    PdfPCell tabl3cell14 = new PdfPCell(new PdfPCell(new Paragraph(position, FontFactory.GetFont("arial", 8, Font.BOLD))));
            //    tabl3cell14.HorizontalAlignment = Element.ALIGN_RIGHT;
            //    tabl3cell14.PaddingBottom = 7;
            //    tabl3cell14.BorderWidth = 0;
            //    tabl3cell14.BorderWidthBottom = 1;
            //    tabl3cell14.BorderWidthRight = 1;
            //    table3.AddCell(tabl3cell14);

            //    PdfPCell tabl3cell15 = new PdfPCell(new PdfPCell(new Paragraph("Department", FontFactory.GetFont("arial", 8))));
            //    tabl3cell15.PaddingBottom = 7;
            //    tabl3cell15.BorderWidth = 0;
            //    tabl3cell15.BorderWidthBottom = 1;
            //    table3.AddCell(tabl3cell15);

            //    PdfPCell tabl3cell16 = new PdfPCell(new PdfPCell(new Paragraph(department, FontFactory.GetFont("arial", 8, Font.BOLD))));
            //    tabl3cell16.HorizontalAlignment = Element.ALIGN_RIGHT;
            //    tabl3cell16.PaddingBottom = 7;
            //    tabl3cell16.BorderWidth = 0;
            //    tabl3cell16.BorderWidthBottom = 1;
            //    tabl3cell16.BorderWidthRight = 1;
            //    table3.AddCell(tabl3cell16);

            //    PdfPCell tabl3cell17 = new PdfPCell(new PdfPCell(new Paragraph("Philhealth", FontFactory.GetFont("arial", 8))));
            //    tabl3cell17.PaddingBottom = 7;
            //    tabl3cell17.BorderWidth = 0;
            //    tabl3cell17.BorderWidthBottom = 1;
            //    table3.AddCell(tabl3cell17);

            //    PdfPCell tabl3cell18 = new PdfPCell(new PdfPCell(new Paragraph(PHILHEALTH, FontFactory.GetFont("arial", 8, Font.BOLD))));
            //    tabl3cell18.HorizontalAlignment = Element.ALIGN_RIGHT;
            //    tabl3cell18.PaddingBottom = 7;
            //    tabl3cell18.BorderWidth = 0;
            //    tabl3cell18.BorderWidthBottom = 1;
            //    tabl3cell18.BorderWidthRight = 1;
            //    table3.AddCell(tabl3cell18);

            //    PdfPCell tabl3cell19 = new PdfPCell(new PdfPCell(new Paragraph("Pag-Ibig", FontFactory.GetFont("arial", 8))));
            //    tabl3cell19.PaddingBottom = 7;
            //    tabl3cell19.BorderWidth = 0;
            //    tabl3cell19.BorderWidthBottom = 1;
            //    table3.AddCell(tabl3cell19);

            //    PdfPCell tabl3cell20 = new PdfPCell(new PdfPCell(new Paragraph(PAGIBIG, FontFactory.GetFont("arial", 8, Font.BOLD))));
            //    tabl3cell20.HorizontalAlignment = Element.ALIGN_RIGHT;
            //    tabl3cell20.PaddingBottom = 7;
            //    tabl3cell20.BorderWidth = 0;
            //    tabl3cell20.BorderWidthBottom = 1;
            //    tabl3cell20.BorderWidthRight = 1;
            //    table3.AddCell(tabl3cell20);

            //    PdfPCell tabl3cell21 = new PdfPCell(new PdfPCell(new Paragraph("TIN", FontFactory.GetFont("arial", 8))));
            //    tabl3cell21.PaddingBottom = 7;
            //    tabl3cell21.BorderWidth = 0;
            //    tabl3cell21.BorderWidthBottom = 1;

            //    table3.AddCell(tabl3cell21);

            //    PdfPCell tabl3cell22 = new PdfPCell(new PdfPCell(new Paragraph(TIN, FontFactory.GetFont("arial", 8, Font.BOLD))));
            //    tabl3cell22.HorizontalAlignment = Element.ALIGN_RIGHT;
            //    tabl3cell22.PaddingBottom = 7;
            //    tabl3cell22.BorderWidth = 0;
            //    tabl3cell22.BorderWidthBottom = 1;
            //    tabl3cell22.BorderWidthRight = 1;
            //    table3.AddCell(tabl3cell22);

            //    PdfPCell tabl3cell23 = new PdfPCell(new PdfPCell(new Paragraph("SSS", FontFactory.GetFont("arial", 8))));
            //    tabl3cell23.PaddingBottom = 7;
            //    tabl3cell23.BorderWidth = 0;
            //    table3.AddCell(tabl3cell23);

            //    PdfPCell tabl3cell24 = new PdfPCell(new PdfPCell(new Paragraph(SSS, FontFactory.GetFont("arial", 8, Font.BOLD))));
            //    tabl3cell24.HorizontalAlignment = Element.ALIGN_RIGHT;
            //    tabl3cell24.PaddingBottom = 7;
            //    tabl3cell24.BorderWidth = 0;
            //    tabl3cell24.BorderWidthRight = 1;
            //    table3.AddCell(tabl3cell24);


            //    table2.AddCell(table3);

            //    //
            //    PdfPTable table4 = new PdfPTable(4);
            //    table4.TotalWidth = 100;
            //    table4.DefaultCell.Border = Rectangle.NO_BORDER;

            //    PdfPCell tabl4cell1 = new PdfPCell(new PdfPCell(new Paragraph("", FontFactory.GetFont("arial", 8))));
            //    tabl4cell1.PaddingBottom = 7;
            //    tabl4cell1.BorderWidth = 0;
            //    table4.AddCell(tabl4cell1);

            //    PdfPCell tabl4cell2 = new PdfPCell(new PdfPCell(new Paragraph("HOURS", FontFactory.GetFont("arial", 8, Font.BOLD))));
            //    tabl4cell2.HorizontalAlignment = Element.ALIGN_RIGHT;
            //    tabl4cell2.PaddingBottom = 7;
            //    tabl4cell2.BorderWidth = 0;
            //    table4.AddCell(tabl4cell2);

            //    PdfPCell tabl4cell3 = new PdfPCell(new PdfPCell(new Paragraph("CURRENT (PHP)", FontFactory.GetFont("arial", 8, Font.BOLD))));
            //    tabl4cell3.HorizontalAlignment = Element.ALIGN_CENTER;
            //    tabl4cell3.PaddingBottom = 7;
            //    tabl4cell3.BorderWidth = 0;
            //    table4.AddCell(tabl4cell3);

            //    PdfPCell tabl4cell4 = new PdfPCell(new PdfPCell(new Paragraph("YEAR TO DATE(PHP)", FontFactory.GetFont("arial", 8, Font.BOLD))));
            //    tabl4cell4.HorizontalAlignment = Element.ALIGN_RIGHT;
            //    tabl4cell4.PaddingBottom = 7;
            //    tabl4cell4.BorderWidth = 0;
            //    table4.AddCell(tabl4cell4);


            //    //
            //    PdfPCell tabl4cell5 = new PdfPCell(new PdfPCell(new Paragraph("Basic Pay", FontFactory.GetFont("arial", 8))));
            //    tabl4cell5.PaddingBottom = 7;
            //    tabl4cell5.BorderWidth = 0;
            //    table4.AddCell(tabl4cell5);

            //    PdfPCell tabl4cell6 = new PdfPCell(new PdfPCell(new Paragraph("", FontFactory.GetFont("arial", 8))));
            //    tabl4cell6.HorizontalAlignment = Element.ALIGN_RIGHT;
            //    tabl4cell6.PaddingBottom = 7;
            //    tabl4cell6.BorderWidth = 0;
            //    table4.AddCell(tabl4cell6);

            //    PdfPCell tabl4cell7 = new PdfPCell(new PdfPCell(new Paragraph(basicpay, FontFactory.GetFont("arial", 8))));
            //    tabl4cell7.HorizontalAlignment = Element.ALIGN_CENTER;
            //    tabl4cell7.PaddingBottom = 7;
            //    tabl4cell7.BorderWidth = 0;
            //    table4.AddCell(tabl4cell7);

            //    PdfPCell tabl4cell8 = new PdfPCell(new PdfPCell(new Paragraph(ytdBasicPay, FontFactory.GetFont("arial", 8))));
            //    tabl4cell8.HorizontalAlignment = Element.ALIGN_RIGHT;
            //    tabl4cell8.PaddingBottom = 7;
            //    tabl4cell8.BorderWidth = 0;
            //    table4.AddCell(tabl4cell8);
            //    //

            //    //
            //    PdfPCell tabl4cell9 = new PdfPCell(new PdfPCell(new Paragraph("Gross Pay", FontFactory.GetFont("arial", 8))));
            //    tabl4cell9.PaddingBottom = 7;
            //    tabl4cell9.BorderWidth = 0;
            //    table4.AddCell(tabl4cell9);

            //    PdfPCell tabl4cell10 = new PdfPCell(new PdfPCell(new Paragraph("", FontFactory.GetFont("arial", 8))));
            //    tabl4cell10.HorizontalAlignment = Element.ALIGN_RIGHT;
            //    tabl4cell10.PaddingBottom = 7;
            //    tabl4cell10.BorderWidth = 0;
            //    table4.AddCell(tabl4cell10);

            //    PdfPCell tabl4cell11 = new PdfPCell(new PdfPCell(new Paragraph(grosspay, FontFactory.GetFont("arial", 8))));
            //    tabl4cell11.HorizontalAlignment = Element.ALIGN_CENTER;
            //    tabl4cell11.PaddingBottom = 7;
            //    tabl4cell11.BorderWidth = 0;
            //    table4.AddCell(tabl4cell11);

            //    PdfPCell tabl4cell12 = new PdfPCell(new PdfPCell(new Paragraph(ytdGrossPay, FontFactory.GetFont("arial", 8))));
            //    tabl4cell12.HorizontalAlignment = Element.ALIGN_RIGHT;
            //    tabl4cell12.PaddingBottom = 7;
            //    tabl4cell12.BorderWidth = 0;
            //    table4.AddCell(tabl4cell12);
            //    //


            //    GDB = new GetDatabase();
            //    GDB.ClientName = prms.CN;
            //    SelfieRegistration2Controller.GetDB2(GDB);
            //    // FOR INCOME FIELDS
            //    // INCOME UNDER EARNINGS
            //    con = new Connection();
            //    con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID });
            //    con.myparameters.Add(new myParameters { ParameterName = "@payoutDate", mytype = SqlDbType.VarChar, Value = prms.PayoutDate });
            //    DataTable dt6 = con.GetDataTable("sp_Earnings_Payslip");


            //    GDB = new GetDatabase();
            //    GDB.ClientName = prms.CN;
            //    SelfieRegistration2Controller.GetDB2(GDB);
            //    con = new Connection();
            //    con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID });
            //    con.myparameters.Add(new myParameters { ParameterName = "@year", mytype = SqlDbType.VarChar, Value = prms.ytd_year });
            //    con.myparameters.Add(new myParameters { ParameterName = "@payoutDate", mytype = SqlDbType.VarChar, Value = prms.PayoutDate });
            //    DataTable dt7 = con.GetDataTable("sp_Earnings_YTD_Payslip");

            //    // HEADER
            //    PdfPCell tabl4cell13 = new PdfPCell(new PdfPCell(new Paragraph("INCOME", FontFactory.GetFont("arial", 8, Font.UNDERLINE)))) { Border = PdfPCell.NO_BORDER, HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT };
            //    tabl4cell13.Colspan = 4;
            //    tabl4cell13.PaddingTop = 10;
            //    tabl4cell13.PaddingBottom = 7;
            //    tabl4cell13.BorderWidth = 0;
            //    tabl4cell13.BackgroundColor = BaseColor.LIGHT_GRAY;
            //    table4.AddCell(tabl4cell13);

            //    if (dt6.Rows.Count > 0)
            //    {
            //        string desc = "", amount = "", ytdAmount = "";
            //        for (int i = 0; i < dt7.Rows.Count; i++)
            //        {
            //            desc = dt6.Rows[i]["EarningsDescription"].ToString();
            //            amount = dt6.Rows[i]["EarningsAmount"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt6.Rows[i]["EarningsAmount"].ToString()));
            //            ytdAmount = dt7.Rows[i]["EarningsAmount"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt7.Rows[i]["EarningsAmount"].ToString()));

            //            PdfPCell tabl4cel13a = new PdfPCell(new PdfPCell(new Paragraph(desc, FontFactory.GetFont("arial", 8))));
            //            tabl4cel13a.PaddingBottom = 7;
            //            tabl4cel13a.BorderWidth = 0;
            //            tabl4cel13a.BackgroundColor = BaseColor.LIGHT_GRAY;
            //            table4.AddCell(tabl4cel13a);

            //            PdfPCell tabl4cel13b = new PdfPCell(new PdfPCell(new Paragraph("", FontFactory.GetFont("arial", 8))));
            //            tabl4cel13b.HorizontalAlignment = Element.ALIGN_RIGHT;
            //            tabl4cel13b.PaddingBottom = 7;
            //            tabl4cel13b.BorderWidth = 0;
            //            tabl4cel13b.BackgroundColor = BaseColor.LIGHT_GRAY;
            //            table4.AddCell(tabl4cel13b);

            //            PdfPCell tabl4cel13c = new PdfPCell(new PdfPCell(new Paragraph(amount, FontFactory.GetFont("arial", 8))));
            //            tabl4cel13c.HorizontalAlignment = Element.ALIGN_CENTER;
            //            tabl4cel13c.PaddingBottom = 7;
            //            tabl4cel13c.BorderWidth = 0;
            //            tabl4cel13c.BackgroundColor = BaseColor.LIGHT_GRAY;
            //            table4.AddCell(tabl4cel13c);

            //            PdfPCell tabl4cel13d = new PdfPCell(new PdfPCell(new Paragraph(ytdAmount, FontFactory.GetFont("arial", 8))));
            //            tabl4cel13d.HorizontalAlignment = Element.ALIGN_RIGHT;
            //            tabl4cel13d.PaddingBottom = 7;
            //            tabl4cel13d.BorderWidth = 0;
            //            tabl4cel13d.BackgroundColor = BaseColor.LIGHT_GRAY;
            //            table4.AddCell(tabl4cel13d);
            //        }
            //    }


            //    GDB = new GetDatabase();
            //    GDB.ClientName = prms.CN;
            //    SelfieRegistration2Controller.GetDB2(GDB);
            //    // INCOME UNDER OVERTIME
            //    con = new Connection();
            //    con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID });
            //    con.myparameters.Add(new myParameters { ParameterName = "@payoutDate", mytype = SqlDbType.VarChar, Value = prms.PayoutDate });
            //    DataTable dt8 = con.GetDataTable("sp_GetOvertimeInfo_Payslip");


            //    GDB = new GetDatabase();
            //    GDB.ClientName = prms.CN;
            //    SelfieRegistration2Controller.GetDB2(GDB);
            //    con = new Connection();
            //    con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID });
            //    con.myparameters.Add(new myParameters { ParameterName = "@year", mytype = SqlDbType.VarChar, Value = prms.ytd_year });
            //    con.myparameters.Add(new myParameters { ParameterName = "@payoutDate", mytype = SqlDbType.VarChar, Value = prms.PayoutDate });
            //    DataTable dt9 = con.GetDataTable("sp_GetOvertimeInfo_YTD_Payslip");

            //    if (dt8.Rows.Count > 0)
            //    {
            //        string OTDesc = "", OTHours = "", OTAmount = "", OT_YTDAmount = "";

            //        for (int i = 0; i < dt8.Rows.Count; i++)
            //        {
            //            OTDesc = dt8.Rows[i]["overtimedescription"].ToString();
            //            OTHours = dt8.Rows[i]["overtimehours"].ToString();
            //            OTAmount = dt8.Rows[i]["overtimeamount"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt8.Rows[i]["overtimeamount"].ToString()));
            //            OT_YTDAmount = dt9.Rows[i]["OvertimeAmount"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt9.Rows[i]["OvertimeAmount"].ToString()));

            //            PdfPCell OT_tabl4cel13a = new PdfPCell(new PdfPCell(new Paragraph(OTDesc, FontFactory.GetFont("arial", 8))));
            //            OT_tabl4cel13a.PaddingBottom = 7;
            //            OT_tabl4cel13a.BorderWidth = 0;
            //            OT_tabl4cel13a.BackgroundColor = BaseColor.LIGHT_GRAY;
            //            table4.AddCell(OT_tabl4cel13a);

            //            PdfPCell OT_tabl4cel13b = new PdfPCell(new PdfPCell(new Paragraph(OTHours, FontFactory.GetFont("arial", 8))));
            //            OT_tabl4cel13b.HorizontalAlignment = Element.ALIGN_RIGHT;
            //            OT_tabl4cel13b.PaddingBottom = 7;
            //            OT_tabl4cel13b.BorderWidth = 0;
            //            OT_tabl4cel13b.BackgroundColor = BaseColor.LIGHT_GRAY;
            //            table4.AddCell(OT_tabl4cel13b);

            //            PdfPCell OT_tabl4cel13c = new PdfPCell(new PdfPCell(new Paragraph(OTAmount, FontFactory.GetFont("arial", 8))));
            //            OT_tabl4cel13c.HorizontalAlignment = Element.ALIGN_CENTER;
            //            OT_tabl4cel13c.PaddingBottom = 7;
            //            OT_tabl4cel13c.BorderWidth = 0;
            //            OT_tabl4cel13c.BackgroundColor = BaseColor.LIGHT_GRAY;
            //            table4.AddCell(OT_tabl4cel13c);

            //            PdfPCell OT_tabl4cel13d = new PdfPCell(new PdfPCell(new Paragraph(OT_YTDAmount, FontFactory.GetFont("arial", 8))));
            //            OT_tabl4cel13d.HorizontalAlignment = Element.ALIGN_RIGHT;
            //            OT_tabl4cel13d.PaddingBottom = 7;
            //            OT_tabl4cel13d.BorderWidth = 0;
            //            OT_tabl4cel13d.BackgroundColor = BaseColor.LIGHT_GRAY;
            //            table4.AddCell(OT_tabl4cel13d);
            //        }
            //    }
            //    // DEDUCTIONS FROM LOG INFO
            //    if (dt16.Rows.Count > 0)
            //    {
            //        if (dt17.Rows[0]["absencesAmount"].ToString() != "0" && dt17.Rows[0]["absencesAmount"].ToString() != "0.0000")
            //        {
            //            Absences_Hours = dt16.Rows[0]["Absences"].ToString();
            //            Absences_Amount = dt16.Rows[0]["AbsentAmount"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt16.Rows[0]["AbsentAmount"].ToString()));
            //            Absences_YTD_Amount = dt17.Rows[0]["absencesAmount"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt17.Rows[0]["absencesAmount"].ToString()));

            //            PdfPCell Absences_tabl4cel14a = new PdfPCell(new PdfPCell(new Paragraph(AbsencesDesc, FontFactory.GetFont("arial", 8))));
            //            Absences_tabl4cel14a.PaddingBottom = 7;
            //            Absences_tabl4cel14a.BorderWidth = 0;
            //            table4.AddCell(Absences_tabl4cel14a);

            //            PdfPCell Absences_tabl4cel14b = new PdfPCell(new PdfPCell(new Paragraph(Absences_Hours, FontFactory.GetFont("arial", 8))));
            //            Absences_tabl4cel14b.HorizontalAlignment = Element.ALIGN_RIGHT;
            //            Absences_tabl4cel14b.PaddingBottom = 7;
            //            Absences_tabl4cel14b.BorderWidth = 0;
            //            table4.AddCell(Absences_tabl4cel14b);

            //            PdfPCell Absences_tabl4cel14c = new PdfPCell(new PdfPCell(new Paragraph(Absences_Amount, FontFactory.GetFont("arial", 8))));
            //            Absences_tabl4cel14c.HorizontalAlignment = Element.ALIGN_CENTER;
            //            Absences_tabl4cel14c.PaddingBottom = 7;
            //            Absences_tabl4cel14c.BorderWidth = 0;
            //            table4.AddCell(Absences_tabl4cel14c);

            //            PdfPCell Absences_tabl4cel14d = new PdfPCell(new PdfPCell(new Paragraph(Absences_YTD_Amount, FontFactory.GetFont("arial", 8))));
            //            Absences_tabl4cel14d.HorizontalAlignment = Element.ALIGN_RIGHT;
            //            Absences_tabl4cel14d.PaddingBottom = 7;
            //            Absences_tabl4cel14d.BorderWidth = 0;
            //            table4.AddCell(Absences_tabl4cel14d);
            //        }

            //        if (dt17.Rows[0]["tardinessAmount"].ToString() != "0" && dt17.Rows[0]["tardinessAmount"].ToString() != "0.0000")
            //        {
            //            Tardi_Hours = dt16.Rows[0]["Tardiness"].ToString();
            //            Tardi_Amount = dt16.Rows[0]["TardinessAmount"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt16.Rows[0]["TardinessAmount"].ToString()));
            //            Tardi_YTD_Amount = dt17.Rows[0]["tardinessAmount"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt17.Rows[0]["tardinessAmount"].ToString()));

            //            PdfPCell Tardi_tabl4cel14a = new PdfPCell(new PdfPCell(new Paragraph(TardiDesc, FontFactory.GetFont("arial", 8))));
            //            Tardi_tabl4cel14a.PaddingBottom = 7;
            //            Tardi_tabl4cel14a.BorderWidth = 0;
            //            table4.AddCell(Tardi_tabl4cel14a);

            //            PdfPCell Tardi_tabl4cel14b = new PdfPCell(new PdfPCell(new Paragraph(Tardi_Hours, FontFactory.GetFont("arial", 8))));
            //            Tardi_tabl4cel14b.HorizontalAlignment = Element.ALIGN_RIGHT;
            //            Tardi_tabl4cel14b.PaddingBottom = 7;
            //            Tardi_tabl4cel14b.BorderWidth = 0;
            //            table4.AddCell(Tardi_tabl4cel14b);

            //            PdfPCell Tardi_tabl4cel14c = new PdfPCell(new PdfPCell(new Paragraph(Tardi_Amount, FontFactory.GetFont("arial", 8))));
            //            Tardi_tabl4cel14c.HorizontalAlignment = Element.ALIGN_CENTER;
            //            Tardi_tabl4cel14c.PaddingBottom = 7;
            //            Tardi_tabl4cel14c.BorderWidth = 0;
            //            table4.AddCell(Tardi_tabl4cel14c);

            //            PdfPCell Tardi_tabl4cel14d = new PdfPCell(new PdfPCell(new Paragraph(Tardi_YTD_Amount, FontFactory.GetFont("arial", 8))));
            //            Tardi_tabl4cel14d.HorizontalAlignment = Element.ALIGN_RIGHT;
            //            Tardi_tabl4cel14d.PaddingBottom = 7;
            //            Tardi_tabl4cel14d.BorderWidth = 0;
            //            table4.AddCell(Tardi_tabl4cel14d);
            //        }

            //        if (dt17.Rows[0]["undertimeAmount"].ToString() != "0" && dt17.Rows[0]["undertimeAmount"].ToString() != "0.0000")
            //        {
            //            Undertime_Hours = dt16.Rows[0]["undertime"].ToString();
            //            Undertime_Amount = dt16.Rows[0]["undertimeAmount"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt16.Rows[0]["undertimeAmount"].ToString()));
            //            Undertime_YTD_Amount = dt17.Rows[0]["undertimeAmount"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt17.Rows[0]["undertimeAmount"].ToString()));

            //            PdfPCell Undertime_tabl4cel14a = new PdfPCell(new PdfPCell(new Paragraph(UndertimeDesc, FontFactory.GetFont("arial", 8))));
            //            Undertime_tabl4cel14a.PaddingBottom = 7;
            //            Undertime_tabl4cel14a.BorderWidth = 0;
            //            table4.AddCell(Undertime_tabl4cel14a);

            //            PdfPCell Undertime_tabl4cel14b = new PdfPCell(new PdfPCell(new Paragraph(Undertime_Hours, FontFactory.GetFont("arial", 8))));
            //            Undertime_tabl4cel14b.HorizontalAlignment = Element.ALIGN_RIGHT;
            //            Undertime_tabl4cel14b.PaddingBottom = 7;
            //            Undertime_tabl4cel14b.BorderWidth = 0;
            //            table4.AddCell(Undertime_tabl4cel14b);

            //            PdfPCell Undertime_tabl4cel14c = new PdfPCell(new PdfPCell(new Paragraph(Undertime_Amount, FontFactory.GetFont("arial", 8))));
            //            Undertime_tabl4cel14c.HorizontalAlignment = Element.ALIGN_CENTER;
            //            Undertime_tabl4cel14c.PaddingBottom = 7;
            //            Undertime_tabl4cel14c.BorderWidth = 0;
            //            table4.AddCell(Undertime_tabl4cel14c);

            //            PdfPCell Undertime_tabl4cel14d = new PdfPCell(new PdfPCell(new Paragraph(Undertime_YTD_Amount, FontFactory.GetFont("arial", 8))));
            //            Undertime_tabl4cel14d.HorizontalAlignment = Element.ALIGN_RIGHT;
            //            Undertime_tabl4cel14d.PaddingBottom = 7;
            //            Undertime_tabl4cel14d.BorderWidth = 0;
            //            table4.AddCell(Undertime_tabl4cel14d);
            //        }

            //        if (dt17.Rows[0]["LWOPAmount"].ToString() != "0" && !string.IsNullOrWhiteSpace(dt17.Rows[0]["LWOPAmount"].ToString()) && dt17.Rows[0]["LWOPAmount"].ToString() != "0.0000" && dt17.Rows[0]["LWOPAmount"].ToString() != "0.00")
            //        {
            //            LWOP_Hours = dt16.Rows[0]["LWOP"].ToString();
            //            LWOP_Amount = dt16.Rows[0]["LWOPAmount"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt16.Rows[0]["LWOPAmount"].ToString()));
            //            LWOP_YTD_Amount = dt17.Rows[0]["LWOPAmount"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt17.Rows[0]["LWOPAmount"].ToString()));

            //            PdfPCell LWOP_tabl4cel14a = new PdfPCell(new PdfPCell(new Paragraph(LWOPDesc, FontFactory.GetFont("arial", 8))));
            //            LWOP_tabl4cel14a.PaddingBottom = 7;
            //            LWOP_tabl4cel14a.BorderWidth = 0;
            //            table4.AddCell(LWOP_tabl4cel14a);

            //            PdfPCell LWOP_tabl4cel14b = new PdfPCell(new PdfPCell(new Paragraph(LWOP_Hours, FontFactory.GetFont("arial", 8))));
            //            LWOP_tabl4cel14b.HorizontalAlignment = Element.ALIGN_RIGHT;
            //            LWOP_tabl4cel14b.PaddingBottom = 7;
            //            LWOP_tabl4cel14b.BorderWidth = 0;
            //            table4.AddCell(LWOP_tabl4cel14b);

            //            PdfPCell LWOP_tabl4cel14c = new PdfPCell(new PdfPCell(new Paragraph(LWOP_Amount, FontFactory.GetFont("arial", 8))));
            //            LWOP_tabl4cel14c.HorizontalAlignment = Element.ALIGN_CENTER;
            //            LWOP_tabl4cel14c.PaddingBottom = 7;
            //            LWOP_tabl4cel14c.BorderWidth = 0;
            //            table4.AddCell(LWOP_tabl4cel14c);

            //            PdfPCell LWOP_tabl4cel14d = new PdfPCell(new PdfPCell(new Paragraph(LWOP_YTD_Amount, FontFactory.GetFont("arial", 8))));
            //            LWOP_tabl4cel14d.HorizontalAlignment = Element.ALIGN_RIGHT;
            //            LWOP_tabl4cel14d.PaddingBottom = 7;
            //            LWOP_tabl4cel14d.BorderWidth = 0;
            //            table4.AddCell(LWOP_tabl4cel14d);
            //        }
            //    }
            //    // END

            //    // FOR DEDUCTIONS FIELDS
            //    // HEADER
            //    PdfPCell tabl4cell14 = new PdfPCell(new PdfPCell(new Paragraph("CONTRIBUTIONS", FontFactory.GetFont("arial", 8, Font.UNDERLINE)))) { Border = PdfPCell.NO_BORDER, HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT };
            //    tabl4cell14.Colspan = 4;
            //    tabl4cell14.PaddingTop = 10;
            //    tabl4cell14.PaddingBottom = 7;
            //    tabl4cell14.BorderWidth = 0;
            //    table4.AddCell(tabl4cell14);


            //    GDB = new GetDatabase();
            //    GDB.ClientName = prms.CN;
            //    SelfieRegistration2Controller.GetDB2(GDB);
            //    // DEDUCTION FOR GOV CONTRIBUTIONS
            //    // FOR SSS
            //    con = new Connection();
            //    con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID });
            //    con.myparameters.Add(new myParameters { ParameterName = "@payoutDate", mytype = SqlDbType.VarChar, Value = prms.PayoutDate });
            //    DataTable dt10 = con.GetDataTable("sp_GetGovernmentAmounts_SSS_Payslip");


            //    GDB = new GetDatabase();
            //    GDB.ClientName = prms.CN;
            //    SelfieRegistration2Controller.GetDB2(GDB);
            //    con = new Connection();
            //    con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID });
            //    con.myparameters.Add(new myParameters { ParameterName = "@year", mytype = SqlDbType.VarChar, Value = prms.ytd_year });
            //    con.myparameters.Add(new myParameters { ParameterName = "@payoutDate", mytype = SqlDbType.VarChar, Value = prms.PayoutDate });
            //    DataTable dt11 = con.GetDataTable("sp_GetGovernmentAmounts_SSS_YTD_Payslip");

            //    if (dt10.Rows.Count > 0)
            //    {
            //        string SSSDesc = "SSS", SSSAmount = "", SSS_YTD_Amount = "";

            //        SSSAmount = dt10.Rows[0]["SSS"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt10.Rows[0]["SSS"].ToString()));
            //        SSS_YTD_Amount = dt11.Rows[0]["SSS"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt11.Rows[0]["SSS"].ToString()));

            //        PdfPCell SSS_tabl4cel14a = new PdfPCell(new PdfPCell(new Paragraph(SSSDesc, FontFactory.GetFont("arial", 8))));
            //        SSS_tabl4cel14a.PaddingBottom = 7;
            //        SSS_tabl4cel14a.BorderWidth = 0;
            //        table4.AddCell(SSS_tabl4cel14a);

            //        PdfPCell SSS_tabl4cel14b = new PdfPCell(new PdfPCell(new Paragraph("", FontFactory.GetFont("arial", 8))));
            //        SSS_tabl4cel14b.HorizontalAlignment = Element.ALIGN_RIGHT;
            //        SSS_tabl4cel14b.PaddingBottom = 7;
            //        SSS_tabl4cel14b.BorderWidth = 0;
            //        table4.AddCell(SSS_tabl4cel14b);

            //        PdfPCell SSS_tabl4cel14c = new PdfPCell(new PdfPCell(new Paragraph(SSSAmount, FontFactory.GetFont("arial", 8))));
            //        SSS_tabl4cel14c.HorizontalAlignment = Element.ALIGN_CENTER;
            //        SSS_tabl4cel14c.PaddingBottom = 7;
            //        SSS_tabl4cel14c.BorderWidth = 0;
            //        table4.AddCell(SSS_tabl4cel14c);

            //        PdfPCell SSS_tabl4cel14d = new PdfPCell(new PdfPCell(new Paragraph(SSS_YTD_Amount, FontFactory.GetFont("arial", 8))));
            //        SSS_tabl4cel14d.HorizontalAlignment = Element.ALIGN_RIGHT;
            //        SSS_tabl4cel14d.PaddingBottom = 7;
            //        SSS_tabl4cel14d.BorderWidth = 0;
            //        table4.AddCell(SSS_tabl4cel14d);
            //    }


            //    GDB = new GetDatabase();
            //    GDB.ClientName = prms.CN;
            //    SelfieRegistration2Controller.GetDB2(GDB);
            //    // FOR PHILHEALTH
            //    con = new Connection();
            //    con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID });
            //    con.myparameters.Add(new myParameters { ParameterName = "@payoutDate", mytype = SqlDbType.VarChar, Value = prms.PayoutDate });
            //    DataTable dt12 = con.GetDataTable("sp_GetGovernmentAmounts_Philhealth_Payslip");



            //    GDB = new GetDatabase();
            //    GDB.ClientName = prms.CN;
            //    SelfieRegistration2Controller.GetDB2(GDB);
            //    con = new Connection();
            //    con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID });
            //    con.myparameters.Add(new myParameters { ParameterName = "@year", mytype = SqlDbType.VarChar, Value = prms.ytd_year });
            //    con.myparameters.Add(new myParameters { ParameterName = "@payoutDate", mytype = SqlDbType.VarChar, Value = prms.PayoutDate });
            //    DataTable dt13 = con.GetDataTable("sp_GetGovernmentAmounts_Philhealth_YTD_Payslip");

            //    if (dt12.Rows.Count > 0)
            //    {
            //        string PHealthDesc = "Philhealth", PHealthAmount = "", PHealth_YTD_Amount = "";

            //        PHealthAmount = dt12.Rows[0]["Philhealth"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt12.Rows[0]["Philhealth"].ToString()));
            //        PHealth_YTD_Amount = dt13.Rows[0]["philhealth"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt13.Rows[0]["philhealth"].ToString()));

            //        PdfPCell PHLHEALTH_tabl4cel14a = new PdfPCell(new PdfPCell(new Paragraph(PHealthDesc, FontFactory.GetFont("arial", 8))));
            //        PHLHEALTH_tabl4cel14a.PaddingBottom = 7;
            //        PHLHEALTH_tabl4cel14a.BorderWidth = 0;
            //        table4.AddCell(PHLHEALTH_tabl4cel14a);

            //        PdfPCell PHLHEALTH_tabl4cel14b = new PdfPCell(new PdfPCell(new Paragraph("", FontFactory.GetFont("arial", 8))));
            //        PHLHEALTH_tabl4cel14b.HorizontalAlignment = Element.ALIGN_RIGHT;
            //        PHLHEALTH_tabl4cel14b.PaddingBottom = 7;
            //        PHLHEALTH_tabl4cel14b.BorderWidth = 0;
            //        table4.AddCell(PHLHEALTH_tabl4cel14b);

            //        PdfPCell PHLHEALTH_tabl4cel14c = new PdfPCell(new PdfPCell(new Paragraph(PHealthAmount, FontFactory.GetFont("arial", 8))));
            //        PHLHEALTH_tabl4cel14c.HorizontalAlignment = Element.ALIGN_CENTER;
            //        PHLHEALTH_tabl4cel14c.PaddingBottom = 7;
            //        PHLHEALTH_tabl4cel14c.BorderWidth = 0;
            //        table4.AddCell(PHLHEALTH_tabl4cel14c);

            //        PdfPCell PHLHEALTH_tabl4cel14d = new PdfPCell(new PdfPCell(new Paragraph(PHealth_YTD_Amount, FontFactory.GetFont("arial", 8))));
            //        PHLHEALTH_tabl4cel14d.HorizontalAlignment = Element.ALIGN_RIGHT;
            //        PHLHEALTH_tabl4cel14d.PaddingBottom = 7;
            //        PHLHEALTH_tabl4cel14d.BorderWidth = 0;
            //        table4.AddCell(PHLHEALTH_tabl4cel14d);
            //    }


            //    GDB = new GetDatabase();
            //    GDB.ClientName = prms.CN;
            //    SelfieRegistration2Controller.GetDB2(GDB);
            //    // FOR HDMF
            //    con = new Connection();
            //    con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID });
            //    con.myparameters.Add(new myParameters { ParameterName = "@payoutDate", mytype = SqlDbType.VarChar, Value = prms.PayoutDate });
            //    DataTable dt14 = con.GetDataTable("sp_GetGovernmentAmounts_Pagibig_Payslip");


            //    GDB = new GetDatabase();
            //    GDB.ClientName = prms.CN;
            //    SelfieRegistration2Controller.GetDB2(GDB);
            //    con = new Connection();
            //    con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID });
            //    con.myparameters.Add(new myParameters { ParameterName = "@year", mytype = SqlDbType.VarChar, Value = prms.ytd_year });
            //    con.myparameters.Add(new myParameters { ParameterName = "@payoutDate", mytype = SqlDbType.VarChar, Value = prms.PayoutDate });
            //    DataTable dt15 = con.GetDataTable("sp_GetGovernmentAmounts_Pagibig_YTD_Payslip");

            //    if (dt14.Rows.Count > 0)
            //    {
            //        string HDMFDesc = "HDMF", HDMFAmount = "", HDMF_YTD_Amount = "";
            //        HDMFAmount = dt14.Rows[0]["HDMF"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt14.Rows[0]["HDMF"].ToString()));
            //        HDMF_YTD_Amount = dt15.Rows[0]["Pagibig_Employee"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt15.Rows[0]["Pagibig_Employee"].ToString()));

            //        PdfPCell HDMF_tabl4cel14a = new PdfPCell(new PdfPCell(new Paragraph(HDMFDesc, FontFactory.GetFont("arial", 8))));
            //        HDMF_tabl4cel14a.PaddingBottom = 7;
            //        HDMF_tabl4cel14a.BorderWidth = 0;
            //        table4.AddCell(HDMF_tabl4cel14a);

            //        PdfPCell HDMF_tabl4cel14b = new PdfPCell(new PdfPCell(new Paragraph("", FontFactory.GetFont("arial", 8))));
            //        HDMF_tabl4cel14b.HorizontalAlignment = Element.ALIGN_RIGHT;
            //        HDMF_tabl4cel14b.PaddingBottom = 7;
            //        HDMF_tabl4cel14b.BorderWidth = 0;
            //        table4.AddCell(HDMF_tabl4cel14b);

            //        PdfPCell HDMF_tabl4cel14c = new PdfPCell(new PdfPCell(new Paragraph(HDMFAmount, FontFactory.GetFont("arial", 8))));
            //        HDMF_tabl4cel14c.HorizontalAlignment = Element.ALIGN_CENTER;
            //        HDMF_tabl4cel14c.PaddingBottom = 7;
            //        HDMF_tabl4cel14c.BorderWidth = 0;
            //        table4.AddCell(HDMF_tabl4cel14c);

            //        PdfPCell HDMF_tabl4cel14d = new PdfPCell(new PdfPCell(new Paragraph(HDMF_YTD_Amount, FontFactory.GetFont("arial", 8))));
            //        HDMF_tabl4cel14d.HorizontalAlignment = Element.ALIGN_RIGHT;
            //        HDMF_tabl4cel14d.PaddingBottom = 7;
            //        HDMF_tabl4cel14d.BorderWidth = 0;
            //        table4.AddCell(HDMF_tabl4cel14d);
            //    }




            //    GDB = new GetDatabase();
            //    GDB.ClientName = prms.CN;
            //    SelfieRegistration2Controller.GetDB2(GDB);


            //    // FOR LOANS FIELD
            //    PdfPCell tabl4cell15 = new PdfPCell(new PdfPCell(new Paragraph("DEDUCTIONS", FontFactory.GetFont("arial", 8, Font.UNDERLINE))));
            //    tabl4cell14.PaddingTop = 30;
            //    tabl4cell15.PaddingBottom = 7;
            //    tabl4cell15.BorderWidth = 0;
            //    table4.AddCell(tabl4cell15);

            //    //PdfPCell tabl4cell16 = new PdfPCell(new PdfPCell(new Paragraph("BALANCE LEFT", FontFactory.GetFont("arial", 8, Font.BOLD))));
            //    PdfPCell tabl4cell16 = new PdfPCell(new PdfPCell(new Paragraph("", FontFactory.GetFont("arial", 8, Font.BOLD))));
            //    tabl4cell16.HorizontalAlignment = Element.ALIGN_RIGHT;
            //    tabl4cell16.PaddingBottom = 7;
            //    tabl4cell14.PaddingTop = 20;
            //    tabl4cell16.BorderWidth = 0;
            //    table4.AddCell(tabl4cell16);

            //    PdfPCell tabl4cell17 = new PdfPCell(new PdfPCell(new Paragraph("", FontFactory.GetFont("arial", 8, Font.BOLD))));
            //    tabl4cell17.PaddingBottom = 7;
            //    tabl4cell17.BorderWidth = 0;
            //    table4.AddCell(tabl4cell17);

            //    PdfPCell tabl4cell18 = new PdfPCell(new PdfPCell(new Paragraph("", FontFactory.GetFont("arial", 8, Font.BOLD))));
            //    tabl4cell18.HorizontalAlignment = Element.ALIGN_RIGHT;
            //    tabl4cell18.PaddingBottom = 7;
            //    tabl4cell18.BorderWidth = 0;
            //    table4.AddCell(tabl4cell18);


            //    GDB = new GetDatabase();
            //    GDB.ClientName = prms.CN;
            //    SelfieRegistration2Controller.GetDB2(GDB);
            //    con = new Connection();
            //    con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID });
            //    con.myparameters.Add(new myParameters { ParameterName = "@payoutDate", mytype = SqlDbType.VarChar, Value = prms.PayoutDate });
            //    DataTable dt20 = con.GetDataTable("sp_GetLoans_Payslip");


            //    GDB = new GetDatabase();
            //    GDB.ClientName = prms.CN;
            //    SelfieRegistration2Controller.GetDB2(GDB);
            //    con = new Connection();
            //    con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID });
            //    con.myparameters.Add(new myParameters { ParameterName = "@year", mytype = SqlDbType.VarChar, Value = prms.ytd_year });
            //    con.myparameters.Add(new myParameters { ParameterName = "@payoutDate", mytype = SqlDbType.VarChar, Value = prms.PayoutDate });
            //    DataTable dt21 = con.GetDataTable("sp_GetLoans_YTD_Payslip");

            //    if (dt20.Rows.Count > 0)
            //    {
            //        string LoanName = "", Amortization_Amount = "", Amortization_YTD_Amount = "";

            //        for (int i = 0; i < dt20.Rows.Count; i++)
            //        {
            //            LoanName = dt20.Rows[i]["LoanName"].ToString();
            //            Amortization_Amount = dt20.Rows[i]["AmortizationAmount"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt20.Rows[i]["AmortizationAmount"].ToString()));
            //            Amortization_YTD_Amount = dt21.Rows[i]["AmortizationAmount"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt21.Rows[i]["AmortizationAmount"].ToString()));

            //            PdfPCell Loan_tabl4cel14a = new PdfPCell(new PdfPCell(new Paragraph(LoanName, FontFactory.GetFont("arial", 8))));
            //            Loan_tabl4cel14a.PaddingBottom = 7;
            //            Loan_tabl4cel14a.BorderWidth = 0;
            //            table4.AddCell(Loan_tabl4cel14a);

            //            PdfPCell Loan_tabl4cel14b = new PdfPCell(new PdfPCell(new Paragraph("", FontFactory.GetFont("arial", 8))));
            //            Loan_tabl4cel14b.HorizontalAlignment = Element.ALIGN_RIGHT;
            //            Loan_tabl4cel14b.PaddingBottom = 7;
            //            Loan_tabl4cel14b.BorderWidth = 0;
            //            table4.AddCell(Loan_tabl4cel14b);

            //            PdfPCell Loan_tabl4cel14c = new PdfPCell(new PdfPCell(new Paragraph(Amortization_Amount, FontFactory.GetFont("arial", 8))));
            //            Loan_tabl4cel14c.HorizontalAlignment = Element.ALIGN_CENTER;
            //            Loan_tabl4cel14c.PaddingBottom = 7;
            //            Loan_tabl4cel14c.BorderWidth = 0;
            //            table4.AddCell(Loan_tabl4cel14c);

            //            PdfPCell Loan_tabl4cel14d = new PdfPCell(new PdfPCell(new Paragraph(Amortization_YTD_Amount, FontFactory.GetFont("arial", 8))));
            //            Loan_tabl4cel14d.HorizontalAlignment = Element.ALIGN_RIGHT;
            //            Loan_tabl4cel14d.PaddingBottom = 7;
            //            Loan_tabl4cel14d.BorderWidth = 0;
            //            table4.AddCell(Loan_tabl4cel14d);
            //        }
            //    }
            //    //
            //    // OTHER DEDUCTIONS
            //    con = new Connection();
            //    con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID });
            //    con.myparameters.Add(new myParameters { ParameterName = "@payoutDate", mytype = SqlDbType.VarChar, Value = prms.PayoutDate });
            //    DataTable dt18 = con.GetDataTable("sp_GetDeductions_Payslip");


            //    GDB = new GetDatabase();
            //    GDB.ClientName = prms.CN;
            //    SelfieRegistration2Controller.GetDB2(GDB);
            //    con = new Connection();
            //    con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID });
            //    con.myparameters.Add(new myParameters { ParameterName = "@year", mytype = SqlDbType.VarChar, Value = prms.ytd_year });
            //    con.myparameters.Add(new myParameters { ParameterName = "@payoutDate", mytype = SqlDbType.VarChar, Value = prms.PayoutDate });
            //    DataTable dt19 = con.GetDataTable("sp_GetDeductions_YTD_Payslip");

            //    if (dt18.Rows.Count > 0)
            //    {
            //        for (int i = 0; i < dt18.Rows.Count; i++)
            //        {
            //            string DeductionDesc = "", Deduction_Amount = "", Deduction_YTD_Amount = "";

            //            DeductionDesc = dt18.Rows[i]["deductiondescription"].ToString();
            //            Deduction_Amount = dt18.Rows[i]["deductionamount"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt18.Rows[i]["deductionamount"].ToString()));
            //            Deduction_YTD_Amount = dt19.Rows[i]["deductionAmount"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt19.Rows[i]["deductionAmount"].ToString()));

            //            PdfPCell OtherDeduction_tabl4cel14a = new PdfPCell(new PdfPCell(new Paragraph(DeductionDesc, FontFactory.GetFont("arial", 8))));
            //            OtherDeduction_tabl4cel14a.PaddingBottom = 7;
            //            OtherDeduction_tabl4cel14a.BorderWidth = 0;
            //            table4.AddCell(OtherDeduction_tabl4cel14a);

            //            PdfPCell OtherDeduction_tabl4cel14b = new PdfPCell(new PdfPCell(new Paragraph("", FontFactory.GetFont("arial", 8))));
            //            OtherDeduction_tabl4cel14b.HorizontalAlignment = Element.ALIGN_RIGHT;
            //            OtherDeduction_tabl4cel14b.PaddingBottom = 7;
            //            OtherDeduction_tabl4cel14b.BorderWidth = 0;
            //            table4.AddCell(OtherDeduction_tabl4cel14b);

            //            PdfPCell OtherDeduction_tabl4cel14c = new PdfPCell(new PdfPCell(new Paragraph(Deduction_Amount, FontFactory.GetFont("arial", 8))));
            //            OtherDeduction_tabl4cel14c.HorizontalAlignment = Element.ALIGN_CENTER;
            //            OtherDeduction_tabl4cel14c.PaddingBottom = 7;
            //            OtherDeduction_tabl4cel14c.BorderWidth = 0;
            //            table4.AddCell(OtherDeduction_tabl4cel14c);

            //            PdfPCell OtherDeduction_tabl4cel14d = new PdfPCell(new PdfPCell(new Paragraph(Deduction_YTD_Amount, FontFactory.GetFont("arial", 8))));
            //            OtherDeduction_tabl4cel14d.HorizontalAlignment = Element.ALIGN_RIGHT;
            //            OtherDeduction_tabl4cel14d.PaddingBottom = 7;
            //            OtherDeduction_tabl4cel14d.BorderWidth = 0;
            //            table4.AddCell(OtherDeduction_tabl4cel14d);
            //        }
            //    }
            //    //
            //    //
            //    PdfPCell tabl4cell19 = new PdfPCell(new PdfPCell(new Paragraph("WithHolding Tax", FontFactory.GetFont("arial", 8))));
            //    tabl4cell19.PaddingBottom = 7;
            //    tabl4cell19.BorderWidth = 0;
            //    tabl4cell19.BackgroundColor = BaseColor.LIGHT_GRAY;
            //    table4.AddCell(tabl4cell19);

            //    PdfPCell tabl4cell20 = new PdfPCell(new PdfPCell(new Paragraph("", FontFactory.GetFont("arial", 8))));
            //    tabl4cell20.HorizontalAlignment = Element.ALIGN_RIGHT;
            //    tabl4cell20.PaddingBottom = 7;
            //    tabl4cell20.BorderWidth = 0;
            //    tabl4cell20.BackgroundColor = BaseColor.LIGHT_GRAY;
            //    table4.AddCell(tabl4cell20);

            //    PdfPCell tabl4cell21 = new PdfPCell(new PdfPCell(new Paragraph(whtax, FontFactory.GetFont("arial", 8))));
            //    tabl4cell21.HorizontalAlignment = Element.ALIGN_CENTER;
            //    tabl4cell21.PaddingBottom = 7;
            //    tabl4cell21.BorderWidth = 0;
            //    tabl4cell21.BackgroundColor = BaseColor.LIGHT_GRAY;
            //    table4.AddCell(tabl4cell21);

            //    PdfPCell tabl4cell22 = new PdfPCell(new PdfPCell(new Paragraph(ytdWhTax, FontFactory.GetFont("arial", 8))));
            //    tabl4cell22.HorizontalAlignment = Element.ALIGN_RIGHT;
            //    tabl4cell22.PaddingBottom = 7;
            //    tabl4cell22.BorderWidth = 0;
            //    tabl4cell22.BackgroundColor = BaseColor.LIGHT_GRAY;
            //    table4.AddCell(tabl4cell22);
            //    //

            //    //
            //    PdfPCell tabl4cell23 = new PdfPCell(new PdfPCell(new Paragraph("Net Pay", FontFactory.GetFont("arial", 8))));
            //    tabl4cell23.PaddingBottom = 7;
            //    tabl4cell23.BorderWidth = 0;
            //    table4.AddCell(tabl4cell23);

            //    PdfPCell tabl4cell24 = new PdfPCell(new PdfPCell(new Paragraph("", FontFactory.GetFont("arial", 8))));
            //    tabl4cell24.HorizontalAlignment = Element.ALIGN_RIGHT;
            //    tabl4cell24.PaddingBottom = 7;
            //    tabl4cell24.BorderWidth = 0;
            //    table4.AddCell(tabl4cell24);

            //    PdfPCell tabl4cell25 = new PdfPCell(new PdfPCell(new Paragraph(netpay, FontFactory.GetFont("arial", 8))));
            //    tabl4cell25.HorizontalAlignment = Element.ALIGN_CENTER;
            //    tabl4cell25.PaddingBottom = 7;
            //    tabl4cell25.BorderWidth = 0;
            //    table4.AddCell(tabl4cell25);

            //    PdfPCell tabl4cell26 = new PdfPCell(new PdfPCell(new Paragraph(ytdNetPay, FontFactory.GetFont("arial", 8))));
            //    tabl4cell26.HorizontalAlignment = Element.ALIGN_RIGHT;
            //    tabl4cell26.PaddingBottom = 7;
            //    tabl4cell26.BorderWidth = 0;
            //    table4.AddCell(tabl4cell26);
            //    //

            //    //
            //    PdfPCell tabl4cell27 = new PdfPCell(new PdfPCell(new Paragraph("", FontFactory.GetFont("arial", 8, Font.UNDERLINE)))) { Border = PdfPCell.NO_BORDER, HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT };
            //    tabl4cell27.Colspan = 4;
            //    tabl4cell27.PaddingTop = 10;
            //    tabl4cell27.PaddingBottom = 7;
            //    tabl4cell27.BorderWidth = 0;
            //    table4.AddCell(tabl4cell27);
            //    //
            //    table2.AddCell(table4);


            //    PdfPCell cell22 = new PdfPCell() { Border = PdfPCell.NO_BORDER };
            //    cell22.Colspan = 2;


            //    table1.AddCell(cell11);
            //    table1.AddCell(cell12);
            //    table2.AddCell(cell22);
            //    document.Add(table0);
            //    document.Add(table1);
            //    document.Add(table2);
            //    document.Close();

            //    return "success";
            //}
            //catch (Exception e)
            //{
            //    return e.ToString();
            //}
            #endregion
        }

        [HttpPost]
        [Route("GenerateDynamicPayslipsAll")]
        public string GetallPayslipPDF(genpayslipparameters param)
        {
            try
            {
                string retval = "";
                GetDatabase GDB = new GetDatabase();
                GDB.ClientName = param.CN;
                SelfieRegistration2Controller.GetDB2(GDB);

                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@payday", mytype = SqlDbType.VarChar, Value = param.payday });
                DataTable dt = con.GetDataTable("sp_GetEmployeesForPayslip");
                foreach (DataRow dr in dt.Rows)
                {
                    generatePDF gp = new generatePDF();
                    gp.CN = param.CN;
                    gp.EmpID = dr[0].ToString();
                    gp.PayoutDate = param.payday;
                    gp.ytd_year = param.payyear;
                    string retsample = generatepdfs(gp);
                    if (retsample == "success")
                        retval = "success";
                    else
                        return retsample;
                }
                return retval;
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }

        private string getSalaryScheme(string salary)
        {
            try
            {
                if (Convert.ToDouble(salary) <= 1000.00)
                    return "Daily";
                else
                    return "Monthly";
            }
            catch
            {
                return "Monthly";
            }
        }

        [HttpPost]
        [Route("GenerateDynamicPayslipsAll")]
        public string GetallPayslipPDF()
        {
            try
            {
                GetDatabase GDB = new GetDatabase();
                GDB.ClientName = "Hitachi";
                SelfieRegistration2Controller.GetDB2(GDB);

                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@payday", mytype = SqlDbType.VarChar, Value = "2020-01-15" });
                DataTable dt = con.GetDataTable("sp_GetEmployeesForPayslip");
                foreach (DataRow dr in dt.Rows)
                {
                    generatePDF gp = new generatePDF();
                    gp.CN = "Hitachi";
                    gp.EmpID = dr[0].ToString();
                    gp.PayoutDate = "2020-01-15";
                    gp.ytd_year = "2020";

                    generatepdfs(gp);
                }
                return "success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }

        [HttpPost]
        [Route("ActiveUser_AutomatedEmail")]
        public string AutoEmail()
        {
            FileStream stream = null;
            try
            {
                var emailadd = new List<string>();
                //emailadd.Add("velascomikeec@gmail.com");
                emailadd.Add("jsangco@illimitado.com");
                emailadd.Add("mvelasco@illimitado.com");
                emailadd.Add("arodil@illimitado.com");
                emailadd.Add("bharat@illimitado.com");
                emailadd.ToArray();
                tempconnectionstring();
                Connection con = new Connection();
                DataTable data = con.GetDataTable("sp_EmailAutomation_ClientCount");
                string dataDir = HostingEnvironment.MapPath("~/Files/AutomatedEmail/");
                stream = new FileStream(dataDir + "Active_UserCount.xls", FileMode.Open);
                string filename;
                string datebuild = DateTime.Now.ToString("MMddyyyy");
                string timebuild = DateTime.Now.ToString("hhmmtt");
                filename = "Active_Users" + "_" + datebuild + "_" + timebuild + ".xls";
                using (var fileStream = new FileStream(dataDir + filename, FileMode.Create, FileAccess.Write))
                {
                    stream.CopyTo(fileStream);
                }
                stream.Close();
                ExcelWorkbookFormat workbookFormat = ExcelWorkbookFormat.Xls_2003;
                string testDocFile = dataDir + "Active_UserCount.xls";
                ExcelWorkbook workbook = new ExcelWorkbook(testDocFile);
                ExcelWorksheet firstWorksheet = workbook.Worksheets[0];
                ExcelWorksheet UserCount = workbook.Worksheets[0];
                int x = 2;
                for (int i = 0; i < data.Rows.Count; i++, x++)
                {
                    UserCount["A" + x].Text = data.Rows[i]["clientname"].ToString();
                    UserCount["B" + x].Text = data.Rows[i]["empcount"].ToString();
                }
                HttpResponse httpResponse = System.Web.HttpContext.Current.Response;
                httpResponse.Clear();
                httpResponse.ContentType = "Application/x-msexcel";
                httpResponse.AddHeader("Content-Disposition", String.Format("attachment; filename={0}", filename));
                string newDIR = HttpContext.Current.Server.MapPath("~/sFTP/AutomatedEmail/");
                //string onlineDIR = "http://ws.durusthr.com/ILM_WS_Live/sFTP/PayrollOutsourcing/";
                workbook.Save(newDIR + filename);
                stream.Close();
                foreach (var email in emailadd)
                {
                    string emailaccount = "durusthr@illimitado.com";
                    string emailpassword = "@1230Qwerty";
                    string prefix = HttpContext.Current.Server.MapPath("~/sFTP/AutomatedEmail/");
                    using (MailMessage mm = new MailMessage(emailaccount, email))
                    {
                        MailAddress aliasmail = new MailAddress("durusthr@illimitado.com", "durusthr@illimitado.com");
                        MailAddress aliasreplymail = new MailAddress("durusthr@illimitado.com");
                        mm.From = aliasmail;
                        mm.Subject = "Notifications";
                        mm.Body = "Monthly report of active users per client";
                        mm.Attachments.Add(new Attachment(prefix + filename));

                        mm.IsBodyHtml = false;
                        SmtpClient smtp = new SmtpClient();
                        smtp.Host = "smtp.office365.com";
                        smtp.EnableSsl = true;
                        NetworkCredential NetworkCred = new NetworkCredential(emailaccount, emailpassword);
                        smtp.UseDefaultCredentials = true;
                        smtp.Credentials = NetworkCred;
                        smtp.Port = 587;
                        smtp.Send(mm);
                    }
                }
                return "Done";
            }
            catch (Exception e)
            {
                stream.Close();
                return "Error: " + e.ToString();
            }
        }

        [HttpPost]
        [Route("DurustRDSProcesses")]
        public string SQLProcessesEmail()
        {
            FileStream stream = null;
            var emailadd = new List<string>();

            emailadd.Add("jsangco@illimitado.com");
            emailadd.Add("eabano@illimitado.com");
            emailadd.Add("jpetre@illimitado.com");
            emailadd.Add("bharat@illimitado.com");
            emailadd.ToArray();
            try
            {
                // connect to server
                tempconnectionstring();

                // instantiate connection class for dynamic parameter calling
                Connection con = new Connection();

                // insert data into a collection for iteration later
                DataTable DT = con.GetDataTable("sp_SQLProcessesList");

                if (DT.Rows.Count > 0)
                {
                    string dataDir = HostingEnvironment.MapPath("~/Files/");
                    stream = new FileStream(dataDir + "SQL_Process_List_Template.xls", FileMode.Open);
                    string filename;
                    filename = "SQL_Process_List.xls";
                    using (var fileStream = new FileStream(dataDir + filename, FileMode.Create, FileAccess.Write))
                    {
                        stream.CopyTo(fileStream);
                    }
                    stream.Close();
                    ExcelWorkbookFormat workbookFormat = ExcelWorkbookFormat.Xls_2003;
                    string testDocFile = dataDir + "SQL_Process_List_Template.xls";
                    ExcelWorkbook workbook = new ExcelWorkbook(testDocFile);
                    ExcelWorksheet firstWorksheet = workbook.Worksheets[0];

                    int x = 2;
                    for (int i = 0; i < DT.Rows.Count; i++, x++)
                    {
                        firstWorksheet["A" + x].Text = DT.Rows[i]["session_id"].ToString();
                        firstWorksheet["B" + x].Text = DT.Rows[i]["command_text"].ToString();
                        firstWorksheet["C" + x].Text = DT.Rows[i]["statement_text"].ToString();
                        firstWorksheet["D" + x].Text = DT.Rows[i]["Wait_M"].ToString();
                        firstWorksheet["E" + x].Text = DT.Rows[i]["cpu_time"].ToString();
                        firstWorksheet["F" + x].Text = DT.Rows[i]["Elaps_M"].ToString();
                        firstWorksheet["G" + x].Text = DT.Rows[i]["status"].ToString();
                        firstWorksheet["H" + x].Text = DT.Rows[i]["LogDate"].ToString();
                    }

                    HttpResponse httpResponse = System.Web.HttpContext.Current.Response;
                    httpResponse.Clear();
                    httpResponse.ContentType = "Application/x-msexcel";
                    httpResponse.AddHeader("Content-Disposition", String.Format("attachment; filename={0}", filename));
                    string newDIR = HttpContext.Current.Server.MapPath("~/sFTP/");
                    workbook.Save(newDIR + filename);
                    stream.Close();

                    foreach (var email in emailadd)
                    {
                        string emailaccount = "durusthr@illimitado.com";
                        string emailpassword = "@1230Qwerty";
                        string prefix = HttpContext.Current.Server.MapPath("~/sFTP/");
                        using (MailMessage mm = new MailMessage(emailaccount, email))
                        {
                            MailAddress aliasmail = new MailAddress("durusthr@illimitado.com", "durusthr@illimitado.com");
                            MailAddress aliasreplymail = new MailAddress("durusthr@illimitado.com");
                            mm.From = aliasmail;
                            mm.Subject = "SQL Processes";
                            mm.Body = string.Concat("There are ", DT.Rows.Count, " processes that surpassed 20 minutes of processing time.");
                            mm.Attachments.Add(new Attachment(prefix + filename));

                            mm.IsBodyHtml = false;
                            SmtpClient smtp = new SmtpClient();
                            smtp.Host = "smtp.office365.com";
                            smtp.EnableSsl = true;
                            NetworkCredential NetworkCred = new NetworkCredential(emailaccount, emailpassword);
                            smtp.UseDefaultCredentials = true;
                            smtp.Credentials = NetworkCred;
                            smtp.Port = 587;
                            smtp.Send(mm);
                        }
                    }
                }
                return "Success";
            }
            catch (Exception e)
            {
                stream.Close();
                return e.ToString();
            }
        }

        [HttpPost]
        [Route("GenerateSendEmailPayslip")]
        public string GenerateSendEmailPayslip(generatePDFv2 prms)
        {
            try
            {
                string timebuild = DateTime.Now.ToString("hhmmtt");
                string filename = "";


                string dirPath = HttpContext.Current.Server.MapPath("~/pdf/");
                Directory.CreateDirectory(dirPath + "\\" + prms.CN.ToLower());
                for (int index = 0; index < prms.EmpID.Length; index++)
                {

                    GetDatabase GDB = new GetDatabase();
                    GDB.ClientName = prms.CN;
                    SelfieRegistration2Controller.GetDB2(GDB);

                    // DATA VARIABLE DECLARATION
                    #region Variables
                    string startdate = "", enddate = "";
                    string payday = "";
                    string fullname = "", empid = "", datejoined = "", position = "", department = "", TIN = "", SSS = "", PHILHEALTH = "", PAGIBIG = "";
                    string basicpay = "", grosspay = "", netpay = "", whtax = "", ytdBasicPay = "", ytdGrossPay = "", ytdNetPay = "", ytdWhTax = "";
                    string HourlyRate = "", HoursWorked = "", AbsencesDesc = "Absences", TardiDesc = "Tardiness", UndertimeDesc = "Undertime", LWOPDesc = "LWOP", Monthly = "";
                    string Absences_Hours = "", Tardi_Hours = "", Undertime_Hours = "", LWOP_Hours = "";
                    string Absences_Amount = "", Tardi_Amount = "", Undertime_Amount = "", LWOP_Amount = "";
                    string Absences_YTD_Amount = "", Tardi_YTD_Amount = "", Undertime_YTD_Amount = "", LWOP_YTD_Amount = "";
                    string CompanyName = "";
                    #endregion

                    //string filename = "";
                    // FOR COMPANY NAME

                    #region Get Necessary Data
                    Connection conn = new Connection();
                    conn.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.VarChar, Value = prms.EmpID[index].ToString() });
                    DataTable dtbl = conn.GetDataTable("sp_get_CompanyName_MobileDynamicPayslip");
                    foreach (DataRow item in dtbl.Rows)
                    {
                        CompanyName = item["TradingName"].ToString();
                    }
                    // FOR PAY PERIOD FIELD

                    GDB = new GetDatabase();
                    GDB.ClientName = prms.CN;
                    SelfieRegistration2Controller.GetDB2(GDB);
                    Connection con = new Connection();
                    con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID[index].ToString() });
                    con.myparameters.Add(new myParameters { ParameterName = "@payoutDate", mytype = SqlDbType.VarChar, Value = prms.PayoutDate });
                    DataTable DT = con.GetDataTable("sp_GetPayPeriod_Payslip");
                    foreach (DataRow item in DT.Rows)
                    {
                        startdate = item["StartDate"].ToString();
                        enddate = item["EndDate"].ToString();
                    }

                    GDB = new GetDatabase();
                    GDB.ClientName = prms.CN;
                    SelfieRegistration2Controller.GetDB2(GDB);
                    // FOR PAY DATE FIELD
                    con = new Connection();
                    con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID[index].ToString() });
                    DataTable DT1 = con.GetDataTable("sp_GetPayoutSchemeList");
                    foreach (DataRow item in DT1.Rows)
                    {
                        payday = item["PayoutDate"].ToString();
                    }

                    GDB = new GetDatabase();
                    GDB.ClientName = prms.CN;
                    SelfieRegistration2Controller.GetDB2(GDB);
                    // EMPLOYEE DETAILS FIELD
                    con = new Connection();
                    con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID[index].ToString() });
                    DataTable dt2 = con.GetDataTable("sp_GetEmployeeInfo_Payslip");
                    foreach (DataRow item in dt2.Rows)
                    {
                        fullname = item["EmpName"].ToString();
                        empid = item["EmpID"].ToString();
                        datejoined = item["DateJoined"].ToString();
                        department = item["BusinessUnit"].ToString();
                        position = item["JobDesc"].ToString();
                    }

                    GDB = new GetDatabase();
                    GDB.ClientName = prms.CN;
                    SelfieRegistration2Controller.GetDB2(GDB);
                    // FOR GOVERNMENT ID FIELDS
                    con = new Connection();
                    con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID[index].ToString() });
                    DataTable dt3 = con.GetDataTable("sp_GetGovIDs_Payslip");
                    foreach (DataRow item in dt3.Rows)
                    {
                        if (item["IDTypes"].ToString() == "SSS")
                        {
                            SSS = item["IDCode"].ToString();
                        }
                        else if (item["IDTypes"].ToString() == "Philhealth")
                        {
                            PHILHEALTH = item["IDCode"].ToString();
                        }
                        else if (item["IDTypes"].ToString() == "Pag-Ibig")
                        {
                            PAGIBIG = item["IDCode"].ToString();
                        }
                        else
                        {
                            TIN = item["IDCode"].ToString();
                        }
                    }

                    GDB = new GetDatabase();
                    GDB.ClientName = prms.CN;
                    SelfieRegistration2Controller.GetDB2(GDB);
                    // FOR GROSS PAY, BASIC PAY AND NET PAY FIELDS
                    con = new Connection();
                    con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID[index].ToString() });
                    con.myparameters.Add(new myParameters { ParameterName = "@payoutDate", mytype = SqlDbType.VarChar, Value = prms.PayoutDate });
                    DataTable dt4 = con.GetDataTable("sp_GetPay_Payslip");
                    foreach (DataRow item in dt4.Rows)
                    {
                        basicpay = item["Basic Pay"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(item["Basic Pay"].ToString()));
                        grosspay = item["Gross Pay"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(item["Gross Pay"].ToString()));
                        netpay = item["Net Pay"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(item["Net Pay"].ToString()));
                        whtax = item["WithHolding Tax"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(item["WithHolding Tax"].ToString()));

                        //basicpay = String.Format("{0:n2}", decimal.Parse(basicpay));
                    }
                    // END


                    GDB = new GetDatabase();
                    GDB.ClientName = prms.CN;
                    SelfieRegistration2Controller.GetDB2(GDB);
                    // YTD GROSS PAY, BASIC PAY AND NET PAY FIELDS
                    con = new Connection();
                    con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID[index].ToString() });
                    con.myparameters.Add(new myParameters { ParameterName = "@year", mytype = SqlDbType.VarChar, Value = prms.ytd_year });
                    con.myparameters.Add(new myParameters { ParameterName = "@payoutDate", mytype = SqlDbType.VarChar, Value = prms.PayoutDate });
                    DataTable dt5 = con.GetDataTable("sp_GetPay_YTD_Payslip");
                    foreach (DataRow item in dt5.Rows)
                    {
                        ytdBasicPay = item["basicpay"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(item["basicpay"].ToString()));
                        ytdGrossPay = item["grosspay"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(item["grosspay"].ToString()));
                        ytdWhTax = item["whtax"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(item["whtax"].ToString()));
                        ytdNetPay = item["netpay"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(item["netpay"].ToString()));
                    }

                    GDB = new GetDatabase();
                    GDB.ClientName = prms.CN;
                    SelfieRegistration2Controller.GetDB2(GDB);
                    // FOR LOGS INFO
                    con = new Connection();
                    con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID[index].ToString() });
                    con.myparameters.Add(new myParameters { ParameterName = "@payoutDate", mytype = SqlDbType.VarChar, Value = prms.PayoutDate });
                    DataTable dt16 = con.GetDataTable("sp_GetLogInfo_Payslip");

                    if (dt16.Rows.Count > 0)
                    {
                        HourlyRate = dt16.Rows[0]["HourlyRate"] == DBNull.Value || dt16.Rows[0]["HourlyRate"].ToString() == "" ? "" : String.Format("{0:n2}", decimal.Parse(dt16.Rows[0]["HourlyRate"].ToString()));
                        HoursWorked = dt16.Rows[0]["HoursWorked"] == DBNull.Value || dt16.Rows[0]["HoursWorked"].ToString() == "" ? "" : String.Format("{0:n2}", decimal.Parse(dt16.Rows[0]["HoursWorked"].ToString()));
                        //Monthly = dt16.Rows[0]["Monthly"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt16.Rows[0]["Monthly"].ToString()));
                    }

                    // GET MONTHLY SALARY

                    GDB = new GetDatabase();
                    GDB.ClientName = prms.CN;
                    SelfieRegistration2Controller.GetDB2(GDB);
                    DataTable dtMontlySalary;
                    if (prms.CN == "NPB")
                    {
                        con = new Connection();
                        con.myparameters.Add(new myParameters { ParameterName = "@empid", mytype = SqlDbType.VarChar, Value = prms.EmpID[index].ToString() });
                        con.myparameters.Add(new myParameters { ParameterName = "@payoutdate", mytype = SqlDbType.VarChar, Value = prms.PayoutDate });
                        dtMontlySalary = con.GetDataTable("sp_GetMonthlySalary_Payslip");
                    }
                    else
                    {
                        con = new Connection();
                        con.myparameters.Add(new myParameters { ParameterName = "@empid", mytype = SqlDbType.VarChar, Value = prms.EmpID[index].ToString() });
                        dtMontlySalary = con.GetDataTable("sp_GetMonthlySalary_Payslip");
                    }

                    if (dtMontlySalary.Rows.Count > 0)
                    {
                        Monthly = dtMontlySalary.Rows[0]["Salary"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dtMontlySalary.Rows[0]["Salary"].ToString()));
                    }

                    GDB = new GetDatabase();
                    GDB.ClientName = prms.CN;
                    SelfieRegistration2Controller.GetDB2(GDB);
                    con = new Connection();
                    con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID[index].ToString() });
                    con.myparameters.Add(new myParameters { ParameterName = "@year", mytype = SqlDbType.VarChar, Value = prms.ytd_year });
                    con.myparameters.Add(new myParameters { ParameterName = "@payoutDate", mytype = SqlDbType.VarChar, Value = prms.PayoutDate });
                    DataTable dt17 = con.GetDataTable("sp_GetLogInfo_YTD_Payslip");
                    #endregion

                    #region Create Directory and File

                    filename = "payslip-" + prms.EmpID[index].ToString() + "-" + prms.PayoutDate + "_" + timebuild + ".pdf";
                    System.IO.FileStream fs = new FileStream(HttpContext.Current.Server.MapPath("pdf") + "\\" + prms.CN.ToLower() + "\\" + filename, FileMode.Create);

                    #endregion

                    #region Create PDF and Write all objects with values
                    //string timebuild = DateTime.Now.ToString("hhmmtt");
                    //filename = "payslip-" + prms.EmpID[index].ToString() + "-" + prms.PayoutDate + "_" + timebuild + ".pdf";
                    //System.IO.FileStream fs = new FileStream(HttpContext.Current.Server.MapPath("pdf") + "\\" + "payslip-" + prms.EmpID[index].ToString() + "-" + prms.PayoutDate + "_" + timebuild + ".pdf", FileMode.Create);
                    //System.IO.FileStream fb = new FileStream(HttpContext.Current.Server.MapPath("pdf") + "\\" + "ilm.png", FileMode.Open);
                    string imagepath = HttpContext.Current.Server.MapPath("Images");
                    Document document = new Document(PageSize.A4, 25, 25, 30, 30);
                    PdfWriter writer = PdfWriter.GetInstance(document, fs);
                    //PdfWriter writering = PdfWriter.GetInstance(document, fb);
                    document.Open();

                    PdfPTable table0 = new PdfPTable(1);

                    PdfPCell cell0 = new PdfPCell(new iTextSharp.text.Phrase(CompanyName, FontFactory.GetFont("Arial", 12, Font.BOLD))) { Border = 0, HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT, VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE };
                    cell0.PaddingTop = 10;
                    //PdfPCell cell01 = new PdfPCell(new iTextSharp.text.Phrase("Lima Technology Center  Lipa City, Batangas Philippines 4217", FontFactory.GetFont("Arial", 11, Font.NORMAL))) { Border = 0, HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT, VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE };
                    //cell01.PaddingBottom = 7;

                    //Image logoimage = Image.GetInstance(Path.Combine(imagepath, "yokohamalogo.jpg"));
                    //logoimage.ScaleAbsolute(100, 40);
                    //logoimage.SetAbsolutePosition(100, 30);

                    //PdfPCell LogoCell = new PdfPCell(logoimage, true);

                    table0.AddCell(cell0);
                    ////table0.AddCell(LogoCell);
                    //table0.AddCell(cell01);
                    //document.Add(LogoCell);


                    PdfPTable table1 = new PdfPTable(2);

                    /*var titleFont = new Font(Font.FontFamily.UNDEFINED, 12);
                    var informationDetails = new Font(Font.FontFamily.UNDEFINED, 12);*/

                    PdfPCell cell11 = new PdfPCell(new iTextSharp.text.Phrase("PAY PERIOD\n" + startdate + " - " + enddate)) { Border = PdfPCell.BOTTOM_BORDER | PdfPCell.TOP_BORDER, HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER, VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE };
                    PdfPCell cell12 = new PdfPCell(new iTextSharp.text.Phrase("PAY DAY\n" + prms.PayoutDate)) { Border = PdfPCell.BOTTOM_BORDER | PdfPCell.TOP_BORDER, HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER, VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE };
                    cell11.PaddingTop = 10;
                    cell12.PaddingTop = 10;
                    cell11.PaddingBottom = 7;
                    cell12.PaddingBottom = 7;
                    /* cell12.AddElement(new Paragraph("PAY DAY", titleFont));
                    cell12.AddElement(new Paragraph("-", informationDetails));*/

                    PdfPTable table2 = new PdfPTable(2);
                    float[] widths = new float[] { 100f, 200f };
                    float[] heights = new float[] { 300f };
                    table2.SetWidths(widths);
                    //table2.DefaultCell.FixedHeight = 500f;
                    table2.DefaultCell.Border = Rectangle.NO_BORDER;
                    //PdfPCell cell21 = new PdfPCell(new Paragraph("Danilo Vicente Sulla Jr")) { Border = PdfPCell.RIGHT_BORDER, HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER, VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE };
                    //cell21.PaddingTop = 20;

                    PdfPTable table3 = new PdfPTable(2);
                    table3.TotalWidth = 100;
                    table3.DefaultCell.FixedHeight = 600f;
                    table3.DefaultCell.Border = Rectangle.RIGHT_BORDER;

                    PdfPCell tabl3cell0 = new PdfPCell(new PdfPCell(new Paragraph(fullname, FontFactory.GetFont("Arial", 12, Font.BOLD)))) { Border = PdfPCell.NO_BORDER, HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER, VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE };
                    tabl3cell0.Colspan = 2;
                    tabl3cell0.PaddingTop = 20;
                    tabl3cell0.PaddingBottom = 20;
                    tabl3cell0.BorderWidth = 0;
                    tabl3cell0.BorderWidthRight = 1;
                    table3.AddCell(tabl3cell0);


                    PdfPCell tabl3cell1 = new PdfPCell(new PdfPCell(new Paragraph("EmpID", FontFactory.GetFont("arial", 8))));
                    tabl3cell1.PaddingBottom = 7;
                    tabl3cell1.BorderWidth = 0;
                    tabl3cell1.BorderWidthBottom = 1;
                    table3.AddCell(tabl3cell1);

                    PdfPCell tabl3cell2 = new PdfPCell(new PdfPCell(new Paragraph(empid, FontFactory.GetFont("arial", 8, Font.BOLD))));
                    tabl3cell2.HorizontalAlignment = Element.ALIGN_RIGHT;
                    tabl3cell2.PaddingBottom = 7;
                    tabl3cell2.BorderWidth = 0;
                    tabl3cell2.BorderWidthBottom = 1;
                    tabl3cell2.BorderWidthRight = 1;
                    table3.AddCell(tabl3cell2);

                    PdfPCell tabl3cell3 = new PdfPCell(new PdfPCell(new Paragraph("Tax Category", FontFactory.GetFont("arial", 8))));
                    tabl3cell3.PaddingBottom = 7;
                    tabl3cell3.BorderWidth = 0;
                    tabl3cell3.BorderWidthBottom = 1;
                    table3.AddCell(tabl3cell3);

                    PdfPCell tabl3cell4 = new PdfPCell(new PdfPCell(new Paragraph("--", FontFactory.GetFont("arial", 8, Font.BOLD))));
                    tabl3cell4.HorizontalAlignment = Element.ALIGN_RIGHT;
                    tabl3cell4.PaddingBottom = 7;
                    tabl3cell4.BorderWidth = 0;
                    tabl3cell4.BorderWidthBottom = 1;
                    tabl3cell4.BorderWidthRight = 1;
                    table3.AddCell(tabl3cell4);

                    PdfPCell tabl3cell5 = new PdfPCell(new PdfPCell(new Paragraph("Hourly Rate", FontFactory.GetFont("arial", 8))));
                    tabl3cell5.PaddingBottom = 7;
                    tabl3cell5.BorderWidth = 0;
                    tabl3cell5.BorderWidthBottom = 1;
                    table3.AddCell(tabl3cell5);

                    PdfPCell tabl3cell6 = new PdfPCell(new PdfPCell(new Paragraph(HourlyRate, FontFactory.GetFont("arial", 8, Font.BOLD))));
                    tabl3cell6.HorizontalAlignment = Element.ALIGN_RIGHT;
                    tabl3cell6.PaddingBottom = 7;
                    tabl3cell6.BorderWidth = 0;
                    tabl3cell6.BorderWidthBottom = 1;
                    tabl3cell6.BorderWidthRight = 1;
                    table3.AddCell(tabl3cell6);

                    PdfPCell tabl3cell7 = new PdfPCell(new PdfPCell(new Paragraph("Regular Hours", FontFactory.GetFont("arial", 8))));
                    tabl3cell7.PaddingBottom = 7;
                    tabl3cell7.BorderWidth = 0;
                    tabl3cell7.BorderWidthBottom = 1;
                    table3.AddCell(tabl3cell7);

                    PdfPCell tabl3cell8 = new PdfPCell(new PdfPCell(new Paragraph(HoursWorked, FontFactory.GetFont("arial", 8, Font.BOLD))));
                    tabl3cell8.HorizontalAlignment = Element.ALIGN_RIGHT;
                    tabl3cell8.PaddingBottom = 7;
                    tabl3cell8.BorderWidth = 0;
                    tabl3cell8.BorderWidthBottom = 1;
                    tabl3cell8.BorderWidthRight = 1;
                    table3.AddCell(tabl3cell8);

                    PdfPCell tabl3cell9 = new PdfPCell(new PdfPCell(new Paragraph(getSalaryScheme(Monthly.ToString()) + " Salary", FontFactory.GetFont("arial", 8))));
                    tabl3cell9.PaddingBottom = 7;
                    tabl3cell9.BorderWidth = 0;
                    tabl3cell9.BorderWidthBottom = 1;
                    table3.AddCell(tabl3cell9);

                    PdfPCell tabl3cell10 = new PdfPCell(new PdfPCell(new Paragraph(Monthly, FontFactory.GetFont("arial", 8, Font.BOLD))));
                    tabl3cell10.HorizontalAlignment = Element.ALIGN_RIGHT;
                    tabl3cell10.PaddingBottom = 7;
                    tabl3cell10.BorderWidth = 0;
                    tabl3cell10.BorderWidthBottom = 1;
                    tabl3cell10.BorderWidthRight = 1;
                    table3.AddCell(tabl3cell10);

                    PdfPCell tabl3cell11 = new PdfPCell(new PdfPCell(new Paragraph("Date Joined", FontFactory.GetFont("arial", 8))));
                    tabl3cell11.PaddingBottom = 7;
                    tabl3cell11.BorderWidth = 0;
                    tabl3cell11.BorderWidthBottom = 1;
                    table3.AddCell(tabl3cell11);

                    PdfPCell tabl3cell12 = new PdfPCell(new PdfPCell(new Paragraph(datejoined, FontFactory.GetFont("arial", 8, Font.BOLD))));
                    tabl3cell12.HorizontalAlignment = Element.ALIGN_RIGHT;
                    tabl3cell12.PaddingBottom = 7;
                    tabl3cell12.BorderWidth = 0;
                    tabl3cell12.BorderWidthBottom = 1;
                    tabl3cell12.BorderWidthRight = 1;
                    table3.AddCell(tabl3cell12);

                    PdfPCell tabl3cell13 = new PdfPCell(new PdfPCell(new Paragraph("Position", FontFactory.GetFont("arial", 8))));
                    tabl3cell13.PaddingBottom = 7;
                    tabl3cell13.BorderWidth = 0;
                    tabl3cell13.BorderWidthBottom = 1;
                    table3.AddCell(tabl3cell13);

                    PdfPCell tabl3cell14 = new PdfPCell(new PdfPCell(new Paragraph(position, FontFactory.GetFont("arial", 8, Font.BOLD))));
                    tabl3cell14.HorizontalAlignment = Element.ALIGN_RIGHT;
                    tabl3cell14.PaddingBottom = 7;
                    tabl3cell14.BorderWidth = 0;
                    tabl3cell14.BorderWidthBottom = 1;
                    tabl3cell14.BorderWidthRight = 1;
                    table3.AddCell(tabl3cell14);

                    PdfPCell tabl3cell15 = new PdfPCell(new PdfPCell(new Paragraph("Department", FontFactory.GetFont("arial", 8))));
                    tabl3cell15.PaddingBottom = 7;
                    tabl3cell15.BorderWidth = 0;
                    tabl3cell15.BorderWidthBottom = 1;
                    table3.AddCell(tabl3cell15);

                    PdfPCell tabl3cell16 = new PdfPCell(new PdfPCell(new Paragraph(department, FontFactory.GetFont("arial", 8, Font.BOLD))));
                    tabl3cell16.HorizontalAlignment = Element.ALIGN_RIGHT;
                    tabl3cell16.PaddingBottom = 7;
                    tabl3cell16.BorderWidth = 0;
                    tabl3cell16.BorderWidthBottom = 1;
                    tabl3cell16.BorderWidthRight = 1;
                    table3.AddCell(tabl3cell16);

                    PdfPCell tabl3cell17 = new PdfPCell(new PdfPCell(new Paragraph("Philhealth", FontFactory.GetFont("arial", 8))));
                    tabl3cell17.PaddingBottom = 7;
                    tabl3cell17.BorderWidth = 0;
                    tabl3cell17.BorderWidthBottom = 1;
                    table3.AddCell(tabl3cell17);

                    PdfPCell tabl3cell18 = new PdfPCell(new PdfPCell(new Paragraph(PHILHEALTH, FontFactory.GetFont("arial", 8, Font.BOLD))));
                    tabl3cell18.HorizontalAlignment = Element.ALIGN_RIGHT;
                    tabl3cell18.PaddingBottom = 7;
                    tabl3cell18.BorderWidth = 0;
                    tabl3cell18.BorderWidthBottom = 1;
                    tabl3cell18.BorderWidthRight = 1;
                    table3.AddCell(tabl3cell18);

                    PdfPCell tabl3cell19 = new PdfPCell(new PdfPCell(new Paragraph("Pag-Ibig", FontFactory.GetFont("arial", 8))));
                    tabl3cell19.PaddingBottom = 7;
                    tabl3cell19.BorderWidth = 0;
                    tabl3cell19.BorderWidthBottom = 1;
                    table3.AddCell(tabl3cell19);

                    PdfPCell tabl3cell20 = new PdfPCell(new PdfPCell(new Paragraph(PAGIBIG, FontFactory.GetFont("arial", 8, Font.BOLD))));
                    tabl3cell20.HorizontalAlignment = Element.ALIGN_RIGHT;
                    tabl3cell20.PaddingBottom = 7;
                    tabl3cell20.BorderWidth = 0;
                    tabl3cell20.BorderWidthBottom = 1;
                    tabl3cell20.BorderWidthRight = 1;
                    table3.AddCell(tabl3cell20);

                    PdfPCell tabl3cell21 = new PdfPCell(new PdfPCell(new Paragraph("TIN", FontFactory.GetFont("arial", 8))));
                    tabl3cell21.PaddingBottom = 7;
                    tabl3cell21.BorderWidth = 0;
                    tabl3cell21.BorderWidthBottom = 1;

                    table3.AddCell(tabl3cell21);

                    PdfPCell tabl3cell22 = new PdfPCell(new PdfPCell(new Paragraph(TIN, FontFactory.GetFont("arial", 8, Font.BOLD))));
                    tabl3cell22.HorizontalAlignment = Element.ALIGN_RIGHT;
                    tabl3cell22.PaddingBottom = 7;
                    tabl3cell22.BorderWidth = 0;
                    tabl3cell22.BorderWidthBottom = 1;
                    tabl3cell22.BorderWidthRight = 1;
                    table3.AddCell(tabl3cell22);

                    PdfPCell tabl3cell23 = new PdfPCell(new PdfPCell(new Paragraph("SSS", FontFactory.GetFont("arial", 8))));
                    tabl3cell23.PaddingBottom = 7;
                    tabl3cell23.BorderWidth = 0;
                    table3.AddCell(tabl3cell23);

                    PdfPCell tabl3cell24 = new PdfPCell(new PdfPCell(new Paragraph(SSS, FontFactory.GetFont("arial", 8, Font.BOLD))));
                    tabl3cell24.HorizontalAlignment = Element.ALIGN_RIGHT;
                    tabl3cell24.PaddingBottom = 7;
                    tabl3cell24.BorderWidth = 0;
                    tabl3cell24.BorderWidthRight = 1;
                    table3.AddCell(tabl3cell24);


                    table2.AddCell(table3);

                    //
                    PdfPTable table4 = new PdfPTable(4);
                    table4.TotalWidth = 100;
                    table4.DefaultCell.Border = Rectangle.NO_BORDER;

                    PdfPCell tabl4cell1 = new PdfPCell(new PdfPCell(new Paragraph("", FontFactory.GetFont("arial", 8))));
                    tabl4cell1.PaddingBottom = 7;
                    tabl4cell1.BorderWidth = 0;
                    table4.AddCell(tabl4cell1);

                    PdfPCell tabl4cell2 = new PdfPCell(new PdfPCell(new Paragraph("HOURS", FontFactory.GetFont("arial", 8, Font.BOLD))));
                    tabl4cell2.HorizontalAlignment = Element.ALIGN_RIGHT;
                    tabl4cell2.PaddingBottom = 7;
                    tabl4cell2.BorderWidth = 0;
                    table4.AddCell(tabl4cell2);

                    PdfPCell tabl4cell3 = new PdfPCell(new PdfPCell(new Paragraph("CURRENT (PHP)", FontFactory.GetFont("arial", 8, Font.BOLD))));
                    tabl4cell3.HorizontalAlignment = Element.ALIGN_CENTER;
                    tabl4cell3.PaddingBottom = 7;
                    tabl4cell3.BorderWidth = 0;
                    table4.AddCell(tabl4cell3);

                    PdfPCell tabl4cell4 = new PdfPCell(new PdfPCell(new Paragraph("YEAR TO DATE(PHP)", FontFactory.GetFont("arial", 8, Font.BOLD))));
                    tabl4cell4.HorizontalAlignment = Element.ALIGN_RIGHT;
                    tabl4cell4.PaddingBottom = 7;
                    tabl4cell4.BorderWidth = 0;
                    table4.AddCell(tabl4cell4);


                    //
                    PdfPCell tabl4cell5 = new PdfPCell(new PdfPCell(new Paragraph("Basic Pay", FontFactory.GetFont("arial", 8))));
                    tabl4cell5.PaddingBottom = 7;
                    tabl4cell5.BorderWidth = 0;
                    table4.AddCell(tabl4cell5);

                    PdfPCell tabl4cell6 = new PdfPCell(new PdfPCell(new Paragraph("", FontFactory.GetFont("arial", 8))));
                    tabl4cell6.HorizontalAlignment = Element.ALIGN_RIGHT;
                    tabl4cell6.PaddingBottom = 7;
                    tabl4cell6.BorderWidth = 0;
                    table4.AddCell(tabl4cell6);

                    PdfPCell tabl4cell7 = new PdfPCell(new PdfPCell(new Paragraph(basicpay, FontFactory.GetFont("arial", 8))));
                    tabl4cell7.HorizontalAlignment = Element.ALIGN_CENTER;
                    tabl4cell7.PaddingBottom = 7;
                    tabl4cell7.BorderWidth = 0;
                    table4.AddCell(tabl4cell7);

                    PdfPCell tabl4cell8 = new PdfPCell(new PdfPCell(new Paragraph(ytdBasicPay, FontFactory.GetFont("arial", 8))));
                    tabl4cell8.HorizontalAlignment = Element.ALIGN_RIGHT;
                    tabl4cell8.PaddingBottom = 7;
                    tabl4cell8.BorderWidth = 0;
                    table4.AddCell(tabl4cell8);
                    //

                    //
                    PdfPCell tabl4cell9 = new PdfPCell(new PdfPCell(new Paragraph("Gross Pay", FontFactory.GetFont("arial", 8))));
                    tabl4cell9.PaddingBottom = 7;
                    tabl4cell9.BorderWidth = 0;
                    table4.AddCell(tabl4cell9);

                    PdfPCell tabl4cell10 = new PdfPCell(new PdfPCell(new Paragraph("", FontFactory.GetFont("arial", 8))));
                    tabl4cell10.HorizontalAlignment = Element.ALIGN_RIGHT;
                    tabl4cell10.PaddingBottom = 7;
                    tabl4cell10.BorderWidth = 0;
                    table4.AddCell(tabl4cell10);

                    PdfPCell tabl4cell11 = new PdfPCell(new PdfPCell(new Paragraph(grosspay, FontFactory.GetFont("arial", 8))));
                    tabl4cell11.HorizontalAlignment = Element.ALIGN_CENTER;
                    tabl4cell11.PaddingBottom = 7;
                    tabl4cell11.BorderWidth = 0;
                    table4.AddCell(tabl4cell11);

                    PdfPCell tabl4cell12 = new PdfPCell(new PdfPCell(new Paragraph(ytdGrossPay, FontFactory.GetFont("arial", 8))));
                    tabl4cell12.HorizontalAlignment = Element.ALIGN_RIGHT;
                    tabl4cell12.PaddingBottom = 7;
                    tabl4cell12.BorderWidth = 0;
                    table4.AddCell(tabl4cell12);
                    //


                    GDB = new GetDatabase();
                    GDB.ClientName = prms.CN;
                    SelfieRegistration2Controller.GetDB2(GDB);
                    // FOR INCOME FIELDS
                    // INCOME UNDER EARNINGS
                    con = new Connection();
                    con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID[index].ToString() });
                    con.myparameters.Add(new myParameters { ParameterName = "@payoutDate", mytype = SqlDbType.VarChar, Value = prms.PayoutDate });
                    DataTable dt6 = con.GetDataTable("sp_Earnings_Payslip");


                    GDB = new GetDatabase();
                    GDB.ClientName = prms.CN;
                    SelfieRegistration2Controller.GetDB2(GDB);
                    con = new Connection();
                    con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID[index].ToString() });
                    con.myparameters.Add(new myParameters { ParameterName = "@year", mytype = SqlDbType.VarChar, Value = prms.ytd_year });
                    con.myparameters.Add(new myParameters { ParameterName = "@payoutDate", mytype = SqlDbType.VarChar, Value = prms.PayoutDate });
                    DataTable dt7 = con.GetDataTable("sp_Earnings_YTD_Payslip");

                    // HEADER
                    PdfPCell tabl4cell13 = new PdfPCell(new PdfPCell(new Paragraph("INCOME", FontFactory.GetFont("arial", 8, Font.UNDERLINE)))) { Border = PdfPCell.NO_BORDER, HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT };
                    tabl4cell13.Colspan = 4;
                    tabl4cell13.PaddingTop = 10;
                    tabl4cell13.PaddingBottom = 7;
                    tabl4cell13.BorderWidth = 0;
                    tabl4cell13.BackgroundColor = BaseColor.LIGHT_GRAY;
                    table4.AddCell(tabl4cell13);

                    if (dt6.Rows.Count > 0)
                    {
                        string desc = "", amount = "", ytdAmount = "";
                        for (int i = 0; i < dt6.Rows.Count; i++)
                        {
                            desc = dt6.Rows[i]["EarningsDescription"].ToString();
                            amount = dt6.Rows[i]["EarningsAmount"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt6.Rows[i]["EarningsAmount"].ToString()));

                            for (int ii = 0; ii < dt7.Rows.Count; ii++)
                            {
                                if (desc == dt7.Rows[ii]["EarningsDescription"].ToString())
                                {
                                    ytdAmount = dt7.Rows[ii]["EarningsAmount"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt7.Rows[ii]["EarningsAmount"].ToString()));
                                }
                            }


                            PdfPCell tabl4cel13a = new PdfPCell(new PdfPCell(new Paragraph(desc, FontFactory.GetFont("arial", 8))));
                            tabl4cel13a.PaddingBottom = 7;
                            tabl4cel13a.BorderWidth = 0;
                            tabl4cel13a.BackgroundColor = BaseColor.LIGHT_GRAY;
                            table4.AddCell(tabl4cel13a);

                            PdfPCell tabl4cel13b = new PdfPCell(new PdfPCell(new Paragraph("", FontFactory.GetFont("arial", 8))));
                            tabl4cel13b.HorizontalAlignment = Element.ALIGN_RIGHT;
                            tabl4cel13b.PaddingBottom = 7;
                            tabl4cel13b.BorderWidth = 0;
                            tabl4cel13b.BackgroundColor = BaseColor.LIGHT_GRAY;
                            table4.AddCell(tabl4cel13b);

                            PdfPCell tabl4cel13c = new PdfPCell(new PdfPCell(new Paragraph(amount, FontFactory.GetFont("arial", 8))));
                            tabl4cel13c.HorizontalAlignment = Element.ALIGN_CENTER;
                            tabl4cel13c.PaddingBottom = 7;
                            tabl4cel13c.BorderWidth = 0;
                            tabl4cel13c.BackgroundColor = BaseColor.LIGHT_GRAY;
                            table4.AddCell(tabl4cel13c);

                            PdfPCell tabl4cel13d = new PdfPCell(new PdfPCell(new Paragraph(ytdAmount, FontFactory.GetFont("arial", 8))));
                            tabl4cel13d.HorizontalAlignment = Element.ALIGN_RIGHT;
                            tabl4cel13d.PaddingBottom = 7;
                            tabl4cel13d.BorderWidth = 0;
                            tabl4cel13d.BackgroundColor = BaseColor.LIGHT_GRAY;
                            table4.AddCell(tabl4cel13d);
                        }
                    }


                    GDB = new GetDatabase();
                    GDB.ClientName = prms.CN;
                    SelfieRegistration2Controller.GetDB2(GDB);
                    // INCOME UNDER OVERTIME
                    con = new Connection();
                    con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID[index].ToString() });
                    con.myparameters.Add(new myParameters { ParameterName = "@payoutDate", mytype = SqlDbType.VarChar, Value = prms.PayoutDate });
                    DataTable dt8 = con.GetDataTable("sp_GetOvertimeInfo_Payslip");


                    GDB = new GetDatabase();
                    GDB.ClientName = prms.CN;
                    SelfieRegistration2Controller.GetDB2(GDB);
                    con = new Connection();
                    con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID[index].ToString() });
                    con.myparameters.Add(new myParameters { ParameterName = "@year", mytype = SqlDbType.VarChar, Value = prms.ytd_year });
                    con.myparameters.Add(new myParameters { ParameterName = "@payoutDate", mytype = SqlDbType.VarChar, Value = prms.PayoutDate });
                    DataTable dt9 = con.GetDataTable("sp_GetOvertimeInfo_YTD_Payslip");

                    if (dt8.Rows.Count > 0)
                    {
                        string OTDesc = "", OTHours = "", OTAmount = "", OT_YTDAmount = "";

                        for (int i = 0; i < dt8.Rows.Count; i++)
                        {
                            OTDesc = dt8.Rows[i]["overtimedescription"].ToString();
                            OTHours = dt8.Rows[i]["overtimehours"].ToString();
                            OTAmount = dt8.Rows[i]["overtimeamount"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt8.Rows[i]["overtimeamount"].ToString()));
                            OT_YTDAmount = dt9.Rows[i]["OvertimeAmount"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt9.Rows[i]["OvertimeAmount"].ToString()));

                            PdfPCell OT_tabl4cel13a = new PdfPCell(new PdfPCell(new Paragraph(OTDesc, FontFactory.GetFont("arial", 8))));
                            OT_tabl4cel13a.PaddingBottom = 7;
                            OT_tabl4cel13a.BorderWidth = 0;
                            OT_tabl4cel13a.BackgroundColor = BaseColor.LIGHT_GRAY;
                            table4.AddCell(OT_tabl4cel13a);

                            PdfPCell OT_tabl4cel13b = new PdfPCell(new PdfPCell(new Paragraph(OTHours, FontFactory.GetFont("arial", 8))));
                            OT_tabl4cel13b.HorizontalAlignment = Element.ALIGN_RIGHT;
                            OT_tabl4cel13b.PaddingBottom = 7;
                            OT_tabl4cel13b.BorderWidth = 0;
                            OT_tabl4cel13b.BackgroundColor = BaseColor.LIGHT_GRAY;
                            table4.AddCell(OT_tabl4cel13b);

                            PdfPCell OT_tabl4cel13c = new PdfPCell(new PdfPCell(new Paragraph(OTAmount, FontFactory.GetFont("arial", 8))));
                            OT_tabl4cel13c.HorizontalAlignment = Element.ALIGN_CENTER;
                            OT_tabl4cel13c.PaddingBottom = 7;
                            OT_tabl4cel13c.BorderWidth = 0;
                            OT_tabl4cel13c.BackgroundColor = BaseColor.LIGHT_GRAY;
                            table4.AddCell(OT_tabl4cel13c);

                            PdfPCell OT_tabl4cel13d = new PdfPCell(new PdfPCell(new Paragraph(OT_YTDAmount, FontFactory.GetFont("arial", 8))));
                            OT_tabl4cel13d.HorizontalAlignment = Element.ALIGN_RIGHT;
                            OT_tabl4cel13d.PaddingBottom = 7;
                            OT_tabl4cel13d.BorderWidth = 0;
                            OT_tabl4cel13d.BackgroundColor = BaseColor.LIGHT_GRAY;
                            table4.AddCell(OT_tabl4cel13d);
                        }
                    }
                    // DEDUCTIONS FROM LOG INFO
                    if (dt16.Rows.Count > 0)
                    {
                        if (dt17.Rows[0]["absencesAmount"].ToString() != "0" && dt17.Rows[0]["absencesAmount"].ToString() != "0.0000")
                        {
                            Absences_Hours = dt16.Rows[0]["Absences"].ToString();
                            Absences_Amount = dt16.Rows[0]["AbsentAmount"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt16.Rows[0]["AbsentAmount"].ToString()));
                            Absences_YTD_Amount = dt17.Rows[0]["absencesAmount"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt17.Rows[0]["absencesAmount"].ToString()));

                            PdfPCell Absences_tabl4cel14a = new PdfPCell(new PdfPCell(new Paragraph(AbsencesDesc, FontFactory.GetFont("arial", 8))));
                            Absences_tabl4cel14a.PaddingBottom = 7;
                            Absences_tabl4cel14a.BorderWidth = 0;
                            table4.AddCell(Absences_tabl4cel14a);

                            PdfPCell Absences_tabl4cel14b = new PdfPCell(new PdfPCell(new Paragraph(Absences_Hours, FontFactory.GetFont("arial", 8))));
                            Absences_tabl4cel14b.HorizontalAlignment = Element.ALIGN_RIGHT;
                            Absences_tabl4cel14b.PaddingBottom = 7;
                            Absences_tabl4cel14b.BorderWidth = 0;
                            table4.AddCell(Absences_tabl4cel14b);

                            PdfPCell Absences_tabl4cel14c = new PdfPCell(new PdfPCell(new Paragraph(Absences_Amount, FontFactory.GetFont("arial", 8))));
                            Absences_tabl4cel14c.HorizontalAlignment = Element.ALIGN_CENTER;
                            Absences_tabl4cel14c.PaddingBottom = 7;
                            Absences_tabl4cel14c.BorderWidth = 0;
                            table4.AddCell(Absences_tabl4cel14c);

                            PdfPCell Absences_tabl4cel14d = new PdfPCell(new PdfPCell(new Paragraph(Absences_YTD_Amount, FontFactory.GetFont("arial", 8))));
                            Absences_tabl4cel14d.HorizontalAlignment = Element.ALIGN_RIGHT;
                            Absences_tabl4cel14d.PaddingBottom = 7;
                            Absences_tabl4cel14d.BorderWidth = 0;
                            table4.AddCell(Absences_tabl4cel14d);
                        }

                        if (dt17.Rows[0]["tardinessAmount"].ToString() != "0" && dt17.Rows[0]["tardinessAmount"].ToString() != "0.0000")
                        {
                            Tardi_Hours = dt16.Rows[0]["Tardiness"].ToString();
                            Tardi_Amount = dt16.Rows[0]["TardinessAmount"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt16.Rows[0]["TardinessAmount"].ToString()));
                            Tardi_YTD_Amount = dt17.Rows[0]["tardinessAmount"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt17.Rows[0]["tardinessAmount"].ToString()));

                            PdfPCell Tardi_tabl4cel14a = new PdfPCell(new PdfPCell(new Paragraph(TardiDesc, FontFactory.GetFont("arial", 8))));
                            Tardi_tabl4cel14a.PaddingBottom = 7;
                            Tardi_tabl4cel14a.BorderWidth = 0;
                            table4.AddCell(Tardi_tabl4cel14a);

                            PdfPCell Tardi_tabl4cel14b = new PdfPCell(new PdfPCell(new Paragraph(Tardi_Hours, FontFactory.GetFont("arial", 8))));
                            Tardi_tabl4cel14b.HorizontalAlignment = Element.ALIGN_RIGHT;
                            Tardi_tabl4cel14b.PaddingBottom = 7;
                            Tardi_tabl4cel14b.BorderWidth = 0;
                            table4.AddCell(Tardi_tabl4cel14b);

                            PdfPCell Tardi_tabl4cel14c = new PdfPCell(new PdfPCell(new Paragraph(Tardi_Amount, FontFactory.GetFont("arial", 8))));
                            Tardi_tabl4cel14c.HorizontalAlignment = Element.ALIGN_CENTER;
                            Tardi_tabl4cel14c.PaddingBottom = 7;
                            Tardi_tabl4cel14c.BorderWidth = 0;
                            table4.AddCell(Tardi_tabl4cel14c);

                            PdfPCell Tardi_tabl4cel14d = new PdfPCell(new PdfPCell(new Paragraph(Tardi_YTD_Amount, FontFactory.GetFont("arial", 8))));
                            Tardi_tabl4cel14d.HorizontalAlignment = Element.ALIGN_RIGHT;
                            Tardi_tabl4cel14d.PaddingBottom = 7;
                            Tardi_tabl4cel14d.BorderWidth = 0;
                            table4.AddCell(Tardi_tabl4cel14d);
                        }

                        if (dt17.Rows[0]["undertimeAmount"].ToString() != "0" && dt17.Rows[0]["undertimeAmount"].ToString() != "0.0000")
                        {
                            Undertime_Hours = dt16.Rows[0]["undertime"].ToString();
                            Undertime_Amount = dt16.Rows[0]["undertimeAmount"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt16.Rows[0]["undertimeAmount"].ToString()));
                            Undertime_YTD_Amount = dt17.Rows[0]["undertimeAmount"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt17.Rows[0]["undertimeAmount"].ToString()));

                            PdfPCell Undertime_tabl4cel14a = new PdfPCell(new PdfPCell(new Paragraph(UndertimeDesc, FontFactory.GetFont("arial", 8))));
                            Undertime_tabl4cel14a.PaddingBottom = 7;
                            Undertime_tabl4cel14a.BorderWidth = 0;
                            table4.AddCell(Undertime_tabl4cel14a);

                            PdfPCell Undertime_tabl4cel14b = new PdfPCell(new PdfPCell(new Paragraph(Undertime_Hours, FontFactory.GetFont("arial", 8))));
                            Undertime_tabl4cel14b.HorizontalAlignment = Element.ALIGN_RIGHT;
                            Undertime_tabl4cel14b.PaddingBottom = 7;
                            Undertime_tabl4cel14b.BorderWidth = 0;
                            table4.AddCell(Undertime_tabl4cel14b);

                            PdfPCell Undertime_tabl4cel14c = new PdfPCell(new PdfPCell(new Paragraph(Undertime_Amount, FontFactory.GetFont("arial", 8))));
                            Undertime_tabl4cel14c.HorizontalAlignment = Element.ALIGN_CENTER;
                            Undertime_tabl4cel14c.PaddingBottom = 7;
                            Undertime_tabl4cel14c.BorderWidth = 0;
                            table4.AddCell(Undertime_tabl4cel14c);

                            PdfPCell Undertime_tabl4cel14d = new PdfPCell(new PdfPCell(new Paragraph(Undertime_YTD_Amount, FontFactory.GetFont("arial", 8))));
                            Undertime_tabl4cel14d.HorizontalAlignment = Element.ALIGN_RIGHT;
                            Undertime_tabl4cel14d.PaddingBottom = 7;
                            Undertime_tabl4cel14d.BorderWidth = 0;
                            table4.AddCell(Undertime_tabl4cel14d);
                        }

                        if (dt17.Rows[0]["LWOPAmount"].ToString() != "0" && !string.IsNullOrWhiteSpace(dt17.Rows[0]["LWOPAmount"].ToString()) && dt17.Rows[0]["LWOPAmount"].ToString() != "0.0000" && dt17.Rows[0]["LWOPAmount"].ToString() != "0.00")
                        {
                            LWOP_Hours = dt16.Rows[0]["LWOP"].ToString();
                            LWOP_Amount = dt16.Rows[0]["LWOPAmount"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt16.Rows[0]["LWOPAmount"].ToString()));
                            LWOP_YTD_Amount = dt17.Rows[0]["LWOPAmount"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt17.Rows[0]["LWOPAmount"].ToString()));

                            PdfPCell LWOP_tabl4cel14a = new PdfPCell(new PdfPCell(new Paragraph(LWOPDesc, FontFactory.GetFont("arial", 8))));
                            LWOP_tabl4cel14a.PaddingBottom = 7;
                            LWOP_tabl4cel14a.BorderWidth = 0;
                            table4.AddCell(LWOP_tabl4cel14a);

                            PdfPCell LWOP_tabl4cel14b = new PdfPCell(new PdfPCell(new Paragraph(LWOP_Hours, FontFactory.GetFont("arial", 8))));
                            LWOP_tabl4cel14b.HorizontalAlignment = Element.ALIGN_RIGHT;
                            LWOP_tabl4cel14b.PaddingBottom = 7;
                            LWOP_tabl4cel14b.BorderWidth = 0;
                            table4.AddCell(LWOP_tabl4cel14b);

                            PdfPCell LWOP_tabl4cel14c = new PdfPCell(new PdfPCell(new Paragraph(LWOP_Amount, FontFactory.GetFont("arial", 8))));
                            LWOP_tabl4cel14c.HorizontalAlignment = Element.ALIGN_CENTER;
                            LWOP_tabl4cel14c.PaddingBottom = 7;
                            LWOP_tabl4cel14c.BorderWidth = 0;
                            table4.AddCell(LWOP_tabl4cel14c);

                            PdfPCell LWOP_tabl4cel14d = new PdfPCell(new PdfPCell(new Paragraph(LWOP_YTD_Amount, FontFactory.GetFont("arial", 8))));
                            LWOP_tabl4cel14d.HorizontalAlignment = Element.ALIGN_RIGHT;
                            LWOP_tabl4cel14d.PaddingBottom = 7;
                            LWOP_tabl4cel14d.BorderWidth = 0;
                            table4.AddCell(LWOP_tabl4cel14d);
                        }
                    }
                    // END

                    // FOR DEDUCTIONS FIELDS
                    // HEADER
                    PdfPCell tabl4cell14 = new PdfPCell(new PdfPCell(new Paragraph("CONTRIBUTIONS", FontFactory.GetFont("arial", 8, Font.UNDERLINE)))) { Border = PdfPCell.NO_BORDER, HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT };
                    tabl4cell14.Colspan = 4;
                    tabl4cell14.PaddingTop = 10;
                    tabl4cell14.PaddingBottom = 7;
                    tabl4cell14.BorderWidth = 0;
                    table4.AddCell(tabl4cell14);


                    GDB = new GetDatabase();
                    GDB.ClientName = prms.CN;
                    SelfieRegistration2Controller.GetDB2(GDB);
                    // DEDUCTION FOR GOV CONTRIBUTIONS
                    // FOR SSS
                    con = new Connection();
                    con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID[index].ToString() });
                    con.myparameters.Add(new myParameters { ParameterName = "@payoutDate", mytype = SqlDbType.VarChar, Value = prms.PayoutDate });
                    DataTable dt10 = con.GetDataTable("sp_GetGovernmentAmounts_SSS_Payslip");


                    GDB = new GetDatabase();
                    GDB.ClientName = prms.CN;
                    SelfieRegistration2Controller.GetDB2(GDB);
                    con = new Connection();
                    con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID[index].ToString() });
                    con.myparameters.Add(new myParameters { ParameterName = "@year", mytype = SqlDbType.VarChar, Value = prms.ytd_year });
                    con.myparameters.Add(new myParameters { ParameterName = "@payoutDate", mytype = SqlDbType.VarChar, Value = prms.PayoutDate });
                    DataTable dt11 = con.GetDataTable("sp_GetGovernmentAmounts_SSS_YTD_Payslip");

                    if (dt10.Rows.Count > 0)
                    {
                        string SSSDesc = "SSS", SSSAmount = "", SSS_YTD_Amount = "";

                        SSSAmount = dt10.Rows[0]["SSS"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt10.Rows[0]["SSS"].ToString()));
                        SSS_YTD_Amount = dt11.Rows[0]["SSS"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt11.Rows[0]["SSS"].ToString()));

                        PdfPCell SSS_tabl4cel14a = new PdfPCell(new PdfPCell(new Paragraph(SSSDesc, FontFactory.GetFont("arial", 8))));
                        SSS_tabl4cel14a.PaddingBottom = 7;
                        SSS_tabl4cel14a.BorderWidth = 0;
                        table4.AddCell(SSS_tabl4cel14a);

                        PdfPCell SSS_tabl4cel14b = new PdfPCell(new PdfPCell(new Paragraph("", FontFactory.GetFont("arial", 8))));
                        SSS_tabl4cel14b.HorizontalAlignment = Element.ALIGN_RIGHT;
                        SSS_tabl4cel14b.PaddingBottom = 7;
                        SSS_tabl4cel14b.BorderWidth = 0;
                        table4.AddCell(SSS_tabl4cel14b);

                        PdfPCell SSS_tabl4cel14c = new PdfPCell(new PdfPCell(new Paragraph(SSSAmount, FontFactory.GetFont("arial", 8))));
                        SSS_tabl4cel14c.HorizontalAlignment = Element.ALIGN_CENTER;
                        SSS_tabl4cel14c.PaddingBottom = 7;
                        SSS_tabl4cel14c.BorderWidth = 0;
                        table4.AddCell(SSS_tabl4cel14c);

                        PdfPCell SSS_tabl4cel14d = new PdfPCell(new PdfPCell(new Paragraph(SSS_YTD_Amount, FontFactory.GetFont("arial", 8))));
                        SSS_tabl4cel14d.HorizontalAlignment = Element.ALIGN_RIGHT;
                        SSS_tabl4cel14d.PaddingBottom = 7;
                        SSS_tabl4cel14d.BorderWidth = 0;
                        table4.AddCell(SSS_tabl4cel14d);
                    }


                    GDB = new GetDatabase();
                    GDB.ClientName = prms.CN;
                    SelfieRegistration2Controller.GetDB2(GDB);
                    // FOR PHILHEALTH
                    con = new Connection();
                    con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID[index].ToString() });
                    con.myparameters.Add(new myParameters { ParameterName = "@payoutDate", mytype = SqlDbType.VarChar, Value = prms.PayoutDate });
                    DataTable dt12 = con.GetDataTable("sp_GetGovernmentAmounts_Philhealth_Payslip");



                    GDB = new GetDatabase();
                    GDB.ClientName = prms.CN;
                    SelfieRegistration2Controller.GetDB2(GDB);
                    con = new Connection();
                    con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID[index].ToString() });
                    con.myparameters.Add(new myParameters { ParameterName = "@year", mytype = SqlDbType.VarChar, Value = prms.ytd_year });
                    con.myparameters.Add(new myParameters { ParameterName = "@payoutDate", mytype = SqlDbType.VarChar, Value = prms.PayoutDate });
                    DataTable dt13 = con.GetDataTable("sp_GetGovernmentAmounts_Philhealth_YTD_Payslip");

                    if (dt12.Rows.Count > 0)
                    {
                        string PHealthDesc = "Philhealth", PHealthAmount = "", PHealth_YTD_Amount = "";

                        PHealthAmount = dt12.Rows[0]["Philhealth"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt12.Rows[0]["Philhealth"].ToString()));
                        PHealth_YTD_Amount = dt13.Rows[0]["philhealth"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt13.Rows[0]["philhealth"].ToString()));

                        PdfPCell PHLHEALTH_tabl4cel14a = new PdfPCell(new PdfPCell(new Paragraph(PHealthDesc, FontFactory.GetFont("arial", 8))));
                        PHLHEALTH_tabl4cel14a.PaddingBottom = 7;
                        PHLHEALTH_tabl4cel14a.BorderWidth = 0;
                        table4.AddCell(PHLHEALTH_tabl4cel14a);

                        PdfPCell PHLHEALTH_tabl4cel14b = new PdfPCell(new PdfPCell(new Paragraph("", FontFactory.GetFont("arial", 8))));
                        PHLHEALTH_tabl4cel14b.HorizontalAlignment = Element.ALIGN_RIGHT;
                        PHLHEALTH_tabl4cel14b.PaddingBottom = 7;
                        PHLHEALTH_tabl4cel14b.BorderWidth = 0;
                        table4.AddCell(PHLHEALTH_tabl4cel14b);

                        PdfPCell PHLHEALTH_tabl4cel14c = new PdfPCell(new PdfPCell(new Paragraph(PHealthAmount, FontFactory.GetFont("arial", 8))));
                        PHLHEALTH_tabl4cel14c.HorizontalAlignment = Element.ALIGN_CENTER;
                        PHLHEALTH_tabl4cel14c.PaddingBottom = 7;
                        PHLHEALTH_tabl4cel14c.BorderWidth = 0;
                        table4.AddCell(PHLHEALTH_tabl4cel14c);

                        PdfPCell PHLHEALTH_tabl4cel14d = new PdfPCell(new PdfPCell(new Paragraph(PHealth_YTD_Amount, FontFactory.GetFont("arial", 8))));
                        PHLHEALTH_tabl4cel14d.HorizontalAlignment = Element.ALIGN_RIGHT;
                        PHLHEALTH_tabl4cel14d.PaddingBottom = 7;
                        PHLHEALTH_tabl4cel14d.BorderWidth = 0;
                        table4.AddCell(PHLHEALTH_tabl4cel14d);
                    }


                    GDB = new GetDatabase();
                    GDB.ClientName = prms.CN;
                    SelfieRegistration2Controller.GetDB2(GDB);
                    // FOR HDMF
                    con = new Connection();
                    con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID[index].ToString() });
                    con.myparameters.Add(new myParameters { ParameterName = "@payoutDate", mytype = SqlDbType.VarChar, Value = prms.PayoutDate });
                    DataTable dt14 = con.GetDataTable("sp_GetGovernmentAmounts_Pagibig_Payslip");


                    GDB = new GetDatabase();
                    GDB.ClientName = prms.CN;
                    SelfieRegistration2Controller.GetDB2(GDB);
                    con = new Connection();
                    con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID[index].ToString() });
                    con.myparameters.Add(new myParameters { ParameterName = "@year", mytype = SqlDbType.VarChar, Value = prms.ytd_year });
                    con.myparameters.Add(new myParameters { ParameterName = "@payoutDate", mytype = SqlDbType.VarChar, Value = prms.PayoutDate });
                    DataTable dt15 = con.GetDataTable("sp_GetGovernmentAmounts_Pagibig_YTD_Payslip");

                    if (dt14.Rows.Count > 0)
                    {
                        string HDMFDesc = "HDMF", HDMFAmount = "", HDMF_YTD_Amount = "";
                        HDMFAmount = dt14.Rows[0]["HDMF"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt14.Rows[0]["HDMF"].ToString()));
                        HDMF_YTD_Amount = dt15.Rows[0]["Pagibig_Employee"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt15.Rows[0]["Pagibig_Employee"].ToString()));

                        PdfPCell HDMF_tabl4cel14a = new PdfPCell(new PdfPCell(new Paragraph(HDMFDesc, FontFactory.GetFont("arial", 8))));
                        HDMF_tabl4cel14a.PaddingBottom = 7;
                        HDMF_tabl4cel14a.BorderWidth = 0;
                        table4.AddCell(HDMF_tabl4cel14a);

                        PdfPCell HDMF_tabl4cel14b = new PdfPCell(new PdfPCell(new Paragraph("", FontFactory.GetFont("arial", 8))));
                        HDMF_tabl4cel14b.HorizontalAlignment = Element.ALIGN_RIGHT;
                        HDMF_tabl4cel14b.PaddingBottom = 7;
                        HDMF_tabl4cel14b.BorderWidth = 0;
                        table4.AddCell(HDMF_tabl4cel14b);

                        PdfPCell HDMF_tabl4cel14c = new PdfPCell(new PdfPCell(new Paragraph(HDMFAmount, FontFactory.GetFont("arial", 8))));
                        HDMF_tabl4cel14c.HorizontalAlignment = Element.ALIGN_CENTER;
                        HDMF_tabl4cel14c.PaddingBottom = 7;
                        HDMF_tabl4cel14c.BorderWidth = 0;
                        table4.AddCell(HDMF_tabl4cel14c);

                        PdfPCell HDMF_tabl4cel14d = new PdfPCell(new PdfPCell(new Paragraph(HDMF_YTD_Amount, FontFactory.GetFont("arial", 8))));
                        HDMF_tabl4cel14d.HorizontalAlignment = Element.ALIGN_RIGHT;
                        HDMF_tabl4cel14d.PaddingBottom = 7;
                        HDMF_tabl4cel14d.BorderWidth = 0;
                        table4.AddCell(HDMF_tabl4cel14d);
                    }




                    GDB = new GetDatabase();
                    GDB.ClientName = prms.CN;
                    SelfieRegistration2Controller.GetDB2(GDB);


                    // FOR LOANS FIELD
                    PdfPCell tabl4cell15 = new PdfPCell(new PdfPCell(new Paragraph("DEDUCTIONS", FontFactory.GetFont("arial", 8, Font.UNDERLINE))));
                    tabl4cell14.PaddingTop = 30;
                    tabl4cell15.PaddingBottom = 7;
                    tabl4cell15.BorderWidth = 0;
                    table4.AddCell(tabl4cell15);

                    //PdfPCell tabl4cell16 = new PdfPCell(new PdfPCell(new Paragraph("BALANCE LEFT", FontFactory.GetFont("arial", 8, Font.BOLD))));
                    PdfPCell tabl4cell16 = new PdfPCell(new PdfPCell(new Paragraph("", FontFactory.GetFont("arial", 8, Font.BOLD))));
                    tabl4cell16.HorizontalAlignment = Element.ALIGN_RIGHT;
                    tabl4cell16.PaddingBottom = 7;
                    tabl4cell14.PaddingTop = 20;
                    tabl4cell16.BorderWidth = 0;
                    table4.AddCell(tabl4cell16);

                    PdfPCell tabl4cell17 = new PdfPCell(new PdfPCell(new Paragraph("", FontFactory.GetFont("arial", 8, Font.BOLD))));
                    tabl4cell17.PaddingBottom = 7;
                    tabl4cell17.BorderWidth = 0;
                    table4.AddCell(tabl4cell17);

                    PdfPCell tabl4cell18 = new PdfPCell(new PdfPCell(new Paragraph("", FontFactory.GetFont("arial", 8, Font.BOLD))));
                    tabl4cell18.HorizontalAlignment = Element.ALIGN_RIGHT;
                    tabl4cell18.PaddingBottom = 7;
                    tabl4cell18.BorderWidth = 0;
                    table4.AddCell(tabl4cell18);


                    GDB = new GetDatabase();
                    GDB.ClientName = prms.CN;
                    SelfieRegistration2Controller.GetDB2(GDB);
                    con = new Connection();
                    con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID[index].ToString() });
                    con.myparameters.Add(new myParameters { ParameterName = "@payoutDate", mytype = SqlDbType.VarChar, Value = prms.PayoutDate });
                    DataTable dt20 = con.GetDataTable("sp_GetLoans_Payslip");


                    GDB = new GetDatabase();
                    GDB.ClientName = prms.CN;
                    SelfieRegistration2Controller.GetDB2(GDB);
                    con = new Connection();
                    con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID[index].ToString() });
                    con.myparameters.Add(new myParameters { ParameterName = "@year", mytype = SqlDbType.VarChar, Value = prms.ytd_year });
                    con.myparameters.Add(new myParameters { ParameterName = "@payoutDate", mytype = SqlDbType.VarChar, Value = prms.PayoutDate });
                    DataTable dt21 = con.GetDataTable("sp_GetLoans_YTD_Payslip");

                    if (dt20.Rows.Count > 0)
                    {
                        string LoanName = "", Amortization_Amount = "", Amortization_YTD_Amount = "";

                        for (int i = 0; i < dt20.Rows.Count; i++)
                        {
                            LoanName = dt20.Rows[i]["LoanName"].ToString();
                            Amortization_Amount = dt20.Rows[i]["AmortizationAmount"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt20.Rows[i]["AmortizationAmount"].ToString()));
                            Amortization_YTD_Amount = dt21.Rows[i]["AmortizationAmount"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt21.Rows[i]["AmortizationAmount"].ToString()));

                            PdfPCell Loan_tabl4cel14a = new PdfPCell(new PdfPCell(new Paragraph(LoanName, FontFactory.GetFont("arial", 8))));
                            Loan_tabl4cel14a.PaddingBottom = 7;
                            Loan_tabl4cel14a.BorderWidth = 0;
                            table4.AddCell(Loan_tabl4cel14a);

                            PdfPCell Loan_tabl4cel14b = new PdfPCell(new PdfPCell(new Paragraph("", FontFactory.GetFont("arial", 8))));
                            Loan_tabl4cel14b.HorizontalAlignment = Element.ALIGN_RIGHT;
                            Loan_tabl4cel14b.PaddingBottom = 7;
                            Loan_tabl4cel14b.BorderWidth = 0;
                            table4.AddCell(Loan_tabl4cel14b);

                            PdfPCell Loan_tabl4cel14c = new PdfPCell(new PdfPCell(new Paragraph(Amortization_Amount, FontFactory.GetFont("arial", 8))));
                            Loan_tabl4cel14c.HorizontalAlignment = Element.ALIGN_CENTER;
                            Loan_tabl4cel14c.PaddingBottom = 7;
                            Loan_tabl4cel14c.BorderWidth = 0;
                            table4.AddCell(Loan_tabl4cel14c);

                            PdfPCell Loan_tabl4cel14d = new PdfPCell(new PdfPCell(new Paragraph(Amortization_YTD_Amount, FontFactory.GetFont("arial", 8))));
                            Loan_tabl4cel14d.HorizontalAlignment = Element.ALIGN_RIGHT;
                            Loan_tabl4cel14d.PaddingBottom = 7;
                            Loan_tabl4cel14d.BorderWidth = 0;
                            table4.AddCell(Loan_tabl4cel14d);
                        }
                    }
                    //
                    // OTHER DEDUCTIONS
                    GDB = new GetDatabase();
                    GDB.ClientName = prms.CN;
                    SelfieRegistration2Controller.GetDB2(GDB);
                    Connection con3 = new Connection();
                    con3.myparameters.Clear();
                    con3.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID[index].ToString() });
                    con3.myparameters.Add(new myParameters { ParameterName = "@payoutDate", mytype = SqlDbType.VarChar, Value = prms.PayoutDate });
                    DataTable dt18 = con3.GetDataTable("sp_GetDeductions_Payslip");


                    GDB = new GetDatabase();
                    GDB.ClientName = prms.CN;
                    SelfieRegistration2Controller.GetDB2(GDB);
                    con = new Connection();
                    con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = prms.EmpID[index].ToString() });
                    con.myparameters.Add(new myParameters { ParameterName = "@year", mytype = SqlDbType.VarChar, Value = prms.ytd_year });
                    con.myparameters.Add(new myParameters { ParameterName = "@payoutDate", mytype = SqlDbType.VarChar, Value = prms.PayoutDate });
                    DataTable dt19 = con.GetDataTable("sp_GetDeductions_YTD_Payslip");

                    if (dt18.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt18.Rows.Count; i++)
                        {
                            string DeductionDesc = "", Deduction_Amount = "", Deduction_YTD_Amount = "";

                            DeductionDesc = dt18.Rows[i]["deductiondescription"].ToString();
                            Deduction_Amount = dt18.Rows[i]["deductionamount"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt18.Rows[i]["deductionamount"].ToString()));
                            Deduction_YTD_Amount = dt19.Rows[i]["deductionAmount"] == DBNull.Value ? "" : String.Format("{0:n2}", decimal.Parse(dt19.Rows[i]["deductionAmount"].ToString()));

                            PdfPCell OtherDeduction_tabl4cel14a = new PdfPCell(new PdfPCell(new Paragraph(DeductionDesc, FontFactory.GetFont("arial", 8))));
                            OtherDeduction_tabl4cel14a.PaddingBottom = 7;
                            OtherDeduction_tabl4cel14a.BorderWidth = 0;
                            table4.AddCell(OtherDeduction_tabl4cel14a);

                            PdfPCell OtherDeduction_tabl4cel14b = new PdfPCell(new PdfPCell(new Paragraph("", FontFactory.GetFont("arial", 8))));
                            OtherDeduction_tabl4cel14b.HorizontalAlignment = Element.ALIGN_RIGHT;
                            OtherDeduction_tabl4cel14b.PaddingBottom = 7;
                            OtherDeduction_tabl4cel14b.BorderWidth = 0;
                            table4.AddCell(OtherDeduction_tabl4cel14b);

                            PdfPCell OtherDeduction_tabl4cel14c = new PdfPCell(new PdfPCell(new Paragraph(Deduction_Amount, FontFactory.GetFont("arial", 8))));
                            OtherDeduction_tabl4cel14c.HorizontalAlignment = Element.ALIGN_CENTER;
                            OtherDeduction_tabl4cel14c.PaddingBottom = 7;
                            OtherDeduction_tabl4cel14c.BorderWidth = 0;
                            table4.AddCell(OtherDeduction_tabl4cel14c);

                            PdfPCell OtherDeduction_tabl4cel14d = new PdfPCell(new PdfPCell(new Paragraph(Deduction_YTD_Amount, FontFactory.GetFont("arial", 8))));
                            OtherDeduction_tabl4cel14d.HorizontalAlignment = Element.ALIGN_RIGHT;
                            OtherDeduction_tabl4cel14d.PaddingBottom = 7;
                            OtherDeduction_tabl4cel14d.BorderWidth = 0;
                            table4.AddCell(OtherDeduction_tabl4cel14d);
                        }
                    }
                    //
                    //
                    PdfPCell tabl4cell19 = new PdfPCell(new PdfPCell(new Paragraph("WithHolding Tax", FontFactory.GetFont("arial", 8))));
                    tabl4cell19.PaddingBottom = 7;
                    tabl4cell19.BorderWidth = 0;
                    tabl4cell19.BackgroundColor = BaseColor.LIGHT_GRAY;
                    table4.AddCell(tabl4cell19);

                    PdfPCell tabl4cell20 = new PdfPCell(new PdfPCell(new Paragraph("", FontFactory.GetFont("arial", 8))));
                    tabl4cell20.HorizontalAlignment = Element.ALIGN_RIGHT;
                    tabl4cell20.PaddingBottom = 7;
                    tabl4cell20.BorderWidth = 0;
                    tabl4cell20.BackgroundColor = BaseColor.LIGHT_GRAY;
                    table4.AddCell(tabl4cell20);

                    PdfPCell tabl4cell21 = new PdfPCell(new PdfPCell(new Paragraph(whtax, FontFactory.GetFont("arial", 8))));
                    tabl4cell21.HorizontalAlignment = Element.ALIGN_CENTER;
                    tabl4cell21.PaddingBottom = 7;
                    tabl4cell21.BorderWidth = 0;
                    tabl4cell21.BackgroundColor = BaseColor.LIGHT_GRAY;
                    table4.AddCell(tabl4cell21);

                    PdfPCell tabl4cell22 = new PdfPCell(new PdfPCell(new Paragraph(ytdWhTax, FontFactory.GetFont("arial", 8))));
                    tabl4cell22.HorizontalAlignment = Element.ALIGN_RIGHT;
                    tabl4cell22.PaddingBottom = 7;
                    tabl4cell22.BorderWidth = 0;
                    tabl4cell22.BackgroundColor = BaseColor.LIGHT_GRAY;
                    table4.AddCell(tabl4cell22);
                    //

                    //
                    PdfPCell tabl4cell23 = new PdfPCell(new PdfPCell(new Paragraph("Net Pay", FontFactory.GetFont("arial", 8))));
                    tabl4cell23.PaddingBottom = 7;
                    tabl4cell23.BorderWidth = 0;
                    table4.AddCell(tabl4cell23);

                    PdfPCell tabl4cell24 = new PdfPCell(new PdfPCell(new Paragraph("", FontFactory.GetFont("arial", 8))));
                    tabl4cell24.HorizontalAlignment = Element.ALIGN_RIGHT;
                    tabl4cell24.PaddingBottom = 7;
                    tabl4cell24.BorderWidth = 0;
                    table4.AddCell(tabl4cell24);

                    PdfPCell tabl4cell25 = new PdfPCell(new PdfPCell(new Paragraph(netpay, FontFactory.GetFont("arial", 8))));
                    tabl4cell25.HorizontalAlignment = Element.ALIGN_CENTER;
                    tabl4cell25.PaddingBottom = 7;
                    tabl4cell25.BorderWidth = 0;
                    table4.AddCell(tabl4cell25);

                    PdfPCell tabl4cell26 = new PdfPCell(new PdfPCell(new Paragraph(ytdNetPay, FontFactory.GetFont("arial", 8))));
                    tabl4cell26.HorizontalAlignment = Element.ALIGN_RIGHT;
                    tabl4cell26.PaddingBottom = 7;
                    tabl4cell26.BorderWidth = 0;
                    table4.AddCell(tabl4cell26);


                    //
                    PdfPCell tabl4cell27 = new PdfPCell(new PdfPCell(new Paragraph("", FontFactory.GetFont("arial", 8, Font.UNDERLINE)))) { Border = PdfPCell.NO_BORDER, HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT };
                    tabl4cell27.Colspan = 4;
                    tabl4cell27.PaddingTop = 10;
                    tabl4cell27.PaddingBottom = 7;
                    tabl4cell27.BorderWidth = 0;
                    table4.AddCell(tabl4cell27);
                    //
                    table2.AddCell(table4);


                    PdfPCell cell22 = new PdfPCell() { Border = PdfPCell.NO_BORDER };
                    cell22.Colspan = 2;


                    table1.AddCell(cell11);
                    table1.AddCell(cell12);
                    table2.AddCell(cell22);
                    document.Add(table0);
                    document.Add(table1);
                    document.Add(table2);
                    document.Close();
                    #endregion

                    #region Email Sending

                    GDB.ClientName = prms.CN;
                    SelfieRegistration2Controller.GetDB2(GDB);
                    con = new Connection();
                    con.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.VarChar, Value = prms.EmpID[index].ToString() });
                    DataTable DTEmail = con.GetDataTable("sp_getEmailsbyEmpID");
                    string recipientEmail = "";
                    foreach (DataRow item in DTEmail.Rows)
                    {
                        recipientEmail = item["email"].ToString();
                    }

                    if (prms.SendToEmail == "1" && recipientEmail != "")
                    {
                        //comment this section for no email
                        string emailaccount = "durusthr@illimitado.com";
                        string emailpassword = "@1230Qwerty";
                        string loopemail = recipientEmail;
                        //string loopemail = "jsangco@illimitado.com";
                        string prefix = HttpContext.Current.Server.MapPath("~/pdf/");
                        using (MailMessage mm = new MailMessage(emailaccount, loopemail))//  "rvitug@illimitado.com"))
                        {
                            ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
                            MailAddress aliasmail = new MailAddress("durusthr@illimitado.com", "durusthr@illimitado.com");
                            MailAddress aliasreplymail = new MailAddress("durusthr@illimitado.com");
                            MailAddress aliasmailBCC = new MailAddress("durusthr@illimitado.com");
                            mm.From = aliasmail;
                            mm.Bcc.Add(aliasmailBCC);
                            mm.Subject = "Payslip";
                            //mm.Body = @"Please find enclosed your payslip for the period Feb 11 to 25th. Password is your Lastname. Note: Password is case sensitive.";
                            //mm.Body = "Password is your Lastname.";
                            //mm.Body = "Note: Password is case sensitive.";
                            //string name = dt2.Rows[0][6].ToString() + "_" + dt2.Rows[0][5].ToString() + "_" + param.PayDay;
                            mm.Attachments.Add(new Attachment(HttpContext.Current.Server.MapPath("pdf") + "\\" + prms.CN.ToLower() + "\\" + filename));

                            mm.IsBodyHtml = false;
                            SmtpClient smtp = new SmtpClient();
                            smtp.Host = "smtp.office365.com";
                            smtp.EnableSsl = true;
                            NetworkCredential NetworkCred = new NetworkCredential(emailaccount, emailpassword);
                            smtp.UseDefaultCredentials = true;
                            smtp.Credentials = NetworkCred;
                            smtp.Port = 587;
                            smtp.Send(mm);
                        }
                        //comment this section for no email
                    }
                    else
                    {

                    }
                    #endregion

                    //string onlineDIR = "https://ws.durusthr.com/ILM_WS_Live/pdf/" + prms.CN.ToLower() + "/" + filename;
                    //return onlineDIR + filename;
                }

                return "Success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }

        [HttpPost]
        [Route("GetDynamicPayslipPayoutDates")]
        public List<PayOutDates> GetPayOutDates(PayOutDatesParam param)
        {
            List<PayOutDates> returnList = new List<PayOutDates>();
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = param.CN;
            SelfieRegistration2Controller.GetDB2(GDB);
            Connection con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@year", mytype = System.Data.SqlDbType.VarChar, Value = param.Year });
            con.myparameters.Add(new myParameters { ParameterName = "@month", mytype = System.Data.SqlDbType.VarChar, Value = param.Month });
            DataTable DT = con.GetDataTable("sp_GetPayoutDates");
            foreach (DataRow row in DT.Rows)
            {
                PayOutDates item = new PayOutDates()
                {
                    PayOutDate = row["PayOutDate"].ToString()
                };
                returnList.Add(item);
            }
            return returnList;
        }

        #region Non WebService Method
        static DataTable DynamicPayslip_GetPassword(string empid)
        {
            DataTable pp = null;

            try
            {
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@empid", mytype = System.Data.SqlDbType.VarChar, Value = empid });
                DataTable dt = con.GetDataTable("sp_DynamicPayslip_GetPassword");

                pp = dt;
            }
            catch (Exception e)
            {
                // Throw Exception
                // Most cause of error
                // Table tbl_DynamicPayslip_Password is not yet created
                // Stored Procedure sp_DynamicPayslip_GetPassword is not existing
                // Error in stored procedure
                throw;
            }

            return pp;
        }
        #endregion
    }
}
