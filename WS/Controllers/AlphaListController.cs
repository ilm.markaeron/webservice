﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using illimitadoWepAPI.Models;
using System.Xml.Serialization;
using System.IO;
using System.Data;
using System.Data.SqlClient;
using illimitadoWepAPI.MyClass;
using static illimitadoWepAPI.Models.AlphaListModel;
using static illimitadoWepAPI.Models.UserProfile;
using System.Net.Http.Headers;
using System.Web.Hosting;
using Excel = Microsoft.Office.Interop.Excel;
using Microsoft.Office.Interop.Excel;
using ExpertXls.ExcelLib;
using System.Threading.Tasks;
using System.Web;


namespace illimitadoWepAPI.Controllers
{
    public class AlphaListController : ApiController
    {
        [HttpPost]
        [Route("Generate1604CF71")]
        public string Generate1604CF71(Generate1604CF Data)
        {
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = Data.CN;
            SelfieRegistration2Controller.GetDB2(GDB);
    
            Connection con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@YEAR", mytype = SqlDbType.NVarChar, Value = Data.Year });
            System.Data.DataTable EmpData = con.GetDataTable("sp_GetEmployee_1604CF71_Details");

            try
            {
                string dataDir = HostingEnvironment.MapPath("~/Files/");
                FileStream stream = new FileStream(dataDir + "1604CF_71.xlsx", FileMode.Open);
                //ExcelWriter writer = new ExcelWriter(stream);


                string filename;
                filename = Data.Year + "-1604CF_71_" + DateTime.Now.ToString("hh-mm-tt") + ".xlsx";
                using (var fileStream = new FileStream(dataDir + filename, FileMode.Create, FileAccess.Write))
                {
                    stream.CopyTo(fileStream);
                }
                stream.Close();

                ExcelWorkbookFormat workbookFormat = ExcelWorkbookFormat.Xls_2003;
                string testDocFile = dataDir + "1604CF_71.xlsx";
                ExcelWorkbook workbook = new ExcelWorkbook(testDocFile);
                ExcelWorksheet firstWorksheet = workbook.Worksheets[0];

                int cellnum = 2;
                for (int i = 0; i < EmpData.Rows.Count; i++)
                {
                    firstWorksheet["A" + cellnum].Text = EmpData.Rows[i][0].ToString();
                    firstWorksheet["B" + cellnum].Text = EmpData.Rows[i][1].ToString();
                    firstWorksheet["C" + cellnum].Text = EmpData.Rows[i][2].ToString();
                    firstWorksheet["D" + cellnum].Text = EmpData.Rows[i][3].ToString();
                    firstWorksheet["E" + cellnum].Text = EmpData.Rows[i][4].ToString();
                    firstWorksheet["F" + cellnum].Text = EmpData.Rows[i][5].ToString();
                    firstWorksheet["G" + cellnum].Text = EmpData.Rows[i][6].ToString();
                    firstWorksheet["H" + cellnum].Text = EmpData.Rows[i][7].ToString();
                    firstWorksheet["I" + cellnum].Text = EmpData.Rows[i][8].ToString();
                    firstWorksheet["J" + cellnum].Text = EmpData.Rows[i][9].ToString();
                    firstWorksheet["K" + cellnum].Text = EmpData.Rows[i][10].ToString();
                    firstWorksheet["L" + cellnum].Text = EmpData.Rows[i][11].ToString();
                    firstWorksheet["M" + cellnum].Text = EmpData.Rows[i][12].ToString();
                    firstWorksheet["N" + cellnum].Text = EmpData.Rows[i][13].ToString();
                    firstWorksheet["O" + cellnum].Text = EmpData.Rows[i][14].ToString();
                    firstWorksheet["P" + cellnum].Text = EmpData.Rows[i][15].ToString();
                    firstWorksheet["Q" + cellnum].Text = EmpData.Rows[i][16].ToString();
                    firstWorksheet["R" + cellnum].Text = EmpData.Rows[i][17].ToString();
                    firstWorksheet["S" + cellnum].Text = EmpData.Rows[i][18].ToString();
                    firstWorksheet["T" + cellnum].Text = EmpData.Rows[i][19].ToString();
                    firstWorksheet["U" + cellnum].Text = ComputeTax(EmpData.Rows[i][19].ToString()).ToString();
                    firstWorksheet["V" + cellnum].Text = EmpData.Rows[i][20].ToString();
                    firstWorksheet["W" + cellnum].Text = EmpData.Rows[i][21].ToString();
                    firstWorksheet["X" + cellnum].Text = EmpData.Rows[i][22].ToString();
                    firstWorksheet["Y" + cellnum].Text = EmpData.Rows[i][23].ToString();
                    firstWorksheet["Z" + cellnum].Text = EmpData.Rows[i][24].ToString();
                    cellnum++;
                }

                System.Web.HttpResponse httpResponse = System.Web.HttpContext.Current.Response;

                httpResponse.Clear();
                httpResponse.ContentType = "Application/x-msexcel";
                httpResponse.AddHeader("Content-Disposition", String.Format("attachment; filename={0}", filename));
                string newDIR = HostingEnvironment.MapPath("~/sFTP/");
                workbook.Save(newDIR + filename);

                return "Success";
            }
            catch (Exception e)
            {
                return "error " + e;
            }
        }


        [HttpPost]
        [Route("Generate1604CF72")]
        public string Generate1604CF72(Generate1604CF Data)
        {
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = Data.CN;
            SelfieRegistration2Controller.GetDB2(GDB);

            Connection con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@YEAR", mytype = SqlDbType.NVarChar, Value = Data.Year });
            System.Data.DataTable EmpData = con.GetDataTable("sp_GetEmployee_1604CF71_Details");

            try
            {
                string dataDir = HostingEnvironment.MapPath("~/Files/");
                FileStream stream = new FileStream(dataDir + "1604CF_72.xlsx", FileMode.Open);
                //ExcelWriter writer = new ExcelWriter(stream);


                string filename;
                filename = Data.Year + "-1604CF_72_" + DateTime.Now.ToString("hh-mm-tt") + ".xlsx";
                using (var fileStream = new FileStream(dataDir + filename, FileMode.Create, FileAccess.Write))
                {
                    stream.CopyTo(fileStream);
                }
                stream.Close();

                ExcelWorkbookFormat workbookFormat = ExcelWorkbookFormat.Xls_2003;
                string testDocFile = dataDir + "1604CF_72.xlsx";
                ExcelWorkbook workbook = new ExcelWorkbook(testDocFile);
                ExcelWorksheet firstWorksheet = workbook.Worksheets[0];

                int cellnum = 2;
                for (int i = 0; i < EmpData.Rows.Count; i++)
                {
                    firstWorksheet["A" + cellnum].Text = EmpData.Rows[i][0].ToString();
                    firstWorksheet["B" + cellnum].Text = EmpData.Rows[i][1].ToString();
                    firstWorksheet["C" + cellnum].Text = EmpData.Rows[i][2].ToString();
                    firstWorksheet["D" + cellnum].Text = EmpData.Rows[i][3].ToString();
                    firstWorksheet["E" + cellnum].Text = EmpData.Rows[i][4].ToString();
                    firstWorksheet["F" + cellnum].Text = EmpData.Rows[i][9].ToString();
                    firstWorksheet["G" + cellnum].Text = EmpData.Rows[i][10].ToString();
                    firstWorksheet["H" + cellnum].Text = EmpData.Rows[i][11].ToString();
                    firstWorksheet["I" + cellnum].Text = EmpData.Rows[i][12].ToString();
                    firstWorksheet["J" + cellnum].Text = EmpData.Rows[i][13].ToString();
                    firstWorksheet["K" + cellnum].Text = EmpData.Rows[i][14].ToString();
                    firstWorksheet["L" + cellnum].Text = EmpData.Rows[i][16].ToString();
                    firstWorksheet["M" + cellnum].Text = EmpData.Rows[i][17].ToString();
                    firstWorksheet["N" + cellnum].Text = EmpData.Rows[i][8].ToString();
                    firstWorksheet["O" + cellnum].Text = EmpData.Rows[i][18].ToString();
                    firstWorksheet["P" + cellnum].Text = EmpData.Rows[i][19].ToString();
                    firstWorksheet["Q" + cellnum].Text = ComputeTax(EmpData.Rows[i][19].ToString()).ToString();
                    cellnum++;
                }

                System.Web.HttpResponse httpResponse = System.Web.HttpContext.Current.Response;

                httpResponse.Clear();
                httpResponse.ContentType = "Application/x-msexcel";
                httpResponse.AddHeader("Content-Disposition", String.Format("attachment; filename={0}", filename));
                string newDIR = HostingEnvironment.MapPath("~/sFTP/");
                workbook.Save(newDIR + filename);

                return "Success";
            }
            catch (Exception e)
            {
                return "error " + e;
            }
        }


        [HttpPost]
        [Route("Generate1604CF73")]
        public string Generate1604CF73(Generate1604CF Data)
        {
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = Data.CN;
            SelfieRegistration2Controller.GetDB2(GDB);

            Connection con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@YEAR", mytype = SqlDbType.NVarChar, Value = Data.Year });
            System.Data.DataTable EmpData = con.GetDataTable("sp_GetEmployee_1604CF71_Details");

            try
            {
                string dataDir = HostingEnvironment.MapPath("~/Files/");
                FileStream stream = new FileStream(dataDir + "1604CF_73.xlsx", FileMode.Open);
                //ExcelWriter writer = new ExcelWriter(stream);


                string filename;
                filename = Data.Year + "-1604CF_73_" + DateTime.Now.ToString("hh-mm-tt") + ".xlsx";
                using (var fileStream = new FileStream(dataDir + filename, FileMode.Create, FileAccess.Write))
                {
                    stream.CopyTo(fileStream);
                }
                stream.Close();

                ExcelWorkbookFormat workbookFormat = ExcelWorkbookFormat.Xls_2003;
                string testDocFile = dataDir + "1604CF_73.xlsx";
                ExcelWorkbook workbook = new ExcelWorkbook(testDocFile);
                ExcelWorksheet firstWorksheet = workbook.Worksheets[0];

                int cellnum = 2;
                for (int i = 0; i < EmpData.Rows.Count; i++)
                {
                    firstWorksheet["A" + cellnum].Text = EmpData.Rows[i][0].ToString();
                    firstWorksheet["B" + cellnum].Text = EmpData.Rows[i][1].ToString();
                    firstWorksheet["C" + cellnum].Text = EmpData.Rows[i][2].ToString();
                    firstWorksheet["D" + cellnum].Text = EmpData.Rows[i][3].ToString();
                    firstWorksheet["E" + cellnum].Text = EmpData.Rows[i][4].ToString();
                    firstWorksheet["F" + cellnum].Text = EmpData.Rows[i][5].ToString();
                    firstWorksheet["G" + cellnum].Text = EmpData.Rows[i][8].ToString();
                    firstWorksheet["H" + cellnum].Text = EmpData.Rows[i][9].ToString();
                    firstWorksheet["I" + cellnum].Text = EmpData.Rows[i][10].ToString();
                    firstWorksheet["J" + cellnum].Text = EmpData.Rows[i][11].ToString();
                    firstWorksheet["K" + cellnum].Text = EmpData.Rows[i][12].ToString();
                    firstWorksheet["L" + cellnum].Text = EmpData.Rows[i][13].ToString();
                    firstWorksheet["M" + cellnum].Text = EmpData.Rows[i][14].ToString();
                    firstWorksheet["N" + cellnum].Text = EmpData.Rows[i][15].ToString();
                    firstWorksheet["O" + cellnum].Text = EmpData.Rows[i][16].ToString();
                    firstWorksheet["P" + cellnum].Text = EmpData.Rows[i][17].ToString();
                    firstWorksheet["Q" + cellnum].Text = EmpData.Rows[i][18].ToString();
                    firstWorksheet["R" + cellnum].Text = EmpData.Rows[i][19].ToString();
                    firstWorksheet["S" + cellnum].Text = EmpData.Rows[i][20].ToString();
                    firstWorksheet["T" + cellnum].Text = EmpData.Rows[i][21].ToString();
                    firstWorksheet["U" + cellnum].Text = ComputeTax(EmpData.Rows[i][21].ToString()).ToString();
                    firstWorksheet["V" + cellnum].Text = EmpData.Rows[i][22].ToString();
                    firstWorksheet["W" + cellnum].Text = EmpData.Rows[i][23].ToString();
                    firstWorksheet["X" + cellnum].Text = EmpData.Rows[i][24].ToString();
                    cellnum++;
                }

                System.Web.HttpResponse httpResponse = System.Web.HttpContext.Current.Response;

                httpResponse.Clear();
                httpResponse.ContentType = "Application/x-msexcel";
                httpResponse.AddHeader("Content-Disposition", String.Format("attachment; filename={0}", filename));
                string newDIR = HostingEnvironment.MapPath("~/sFTP/");
                workbook.Save(newDIR + filename);

                return "Success";
            }
            catch (Exception e)
            {
                return "error " + e;
            }
        }


        private double ComputeTax(string strTotalIncome)
        {
            double totalincome = Convert.ToDouble(strTotalIncome);
            double taxdue = 0;
            if (totalincome <= 250000)
            {
                taxdue = 0;
            }
            else if (totalincome <= 400000 && totalincome > 250000)
            {
                taxdue = (totalincome - 250000) * .2;
            }
            else if (totalincome <= 800000 && totalincome > 400000)
            {
                taxdue = ((totalincome - 400000) * .25) + 30000;
            }
            else if (totalincome <= 2000000 && totalincome > 800000)
            {
                taxdue = ((totalincome - 800000) * .3) + 130000;
            }
            else if (totalincome <= 8000000 && totalincome > 2000000)
            {
                taxdue = ((totalincome - 2000000) * .32) + 490000;
            }
            else if (totalincome > 8000000)
            {
                taxdue = ((totalincome - 8000000) * .35) + 2410000;
            }
            return taxdue;
        }

    }
}
