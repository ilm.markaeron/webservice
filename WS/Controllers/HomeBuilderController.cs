﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using illimitadoWepAPI.Models;
using System.Xml.Serialization;
using System.IO;
using System.Data;
using System.Data.SqlClient;
using illimitadoWepAPI.HomeBuilderClass;
using static illimitadoWepAPI.Models.TimeKeeping;

namespace illimitadoWepAPI.Controllers
{
    public class HomeBuilderController : ApiController
    {
        [HttpPost]
        [Route("UserAcct")]
        public List<selectuseracct> newString(selectuseracct sp)
        {
            List<selectuseracct> Access = new List<selectuseracct>();
            HomeBuilderConnection con = new HomeBuilderConnection();
            DataTable dt = new DataTable();
            con.myparameters.Add(new myParameters { ParameterName = "@USERNAME", mytype = SqlDbType.NVarChar, Value = sp.username });
            con.myparameters.Add(new myParameters { ParameterName = "@PASSWORD", mytype = SqlDbType.NVarChar, Value = sp.password });
            dt = con.GetDataTable("sp_selectuseracct"); // Stored Proc
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                selectuseracct login = new selectuseracct();
                login.firstname = dt.Rows[i][1].ToString(); //Nakabase sa stored proc
                login.middlename = dt.Rows[i][2].ToString();
                login.lastname = dt.Rows[i][3].ToString();
                login.email = dt.Rows[i][4].ToString();
                login.company_name = dt.Rows[i][5].ToString();
                login.zipcode = dt.Rows[i][6].ToString();
                login.city = dt.Rows[i][7].ToString();
                login.street = dt.Rows[i][8].ToString();
                login.business_phone = dt.Rows[i][9].ToString();
                login.website = dt.Rows[i][10].ToString();
                login.accstatus = dt.Rows[i][11].ToString();
                Access.Add(login);
            }
            return Access;
        }

        [HttpPost]
        [Route("UserLogin")]
        public string Login(selectuseracct LogIn)
        {
            HomeBuilderConnection con = new HomeBuilderConnection();
            con.myparameters.Add(new myParameters { ParameterName = "@USERNAME", mytype = SqlDbType.NVarChar, Value = LogIn.username });
            con.myparameters.Add(new myParameters { ParameterName = "@PASSWORD", mytype = SqlDbType.NVarChar, Value = LogIn.password });
            //con.ExecuteNonQuery("sp_login"); // Stored Proc
            DataRow dr;
            dr = con.GetSingleRow("sp_login");

            return dr["UserExists"].ToString();

        }

    }
}

