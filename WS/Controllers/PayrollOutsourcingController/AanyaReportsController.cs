﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
//using System.Web.Mvc;

#region For Function Packages
// Database Connection Part 
using System.Configuration;
using System.Reflection;
using System.Web.Hosting;
using System.Net;
using System.IO;
using Newtonsoft.Json;
using ExpertXls.ExcelLib;

using System.Data;
using iTextSharp.text.pdf;
using iTextSharp.text;

using Renci.SshNet;
#endregion

#region For Webservices Function
using illimitadoWepAPI.Models;
using illimitadoWepAPI.Models.PayrollOutsourcingModel;
using illimitadoWepAPI.MyClass;
using System.Web.Http;
#endregion

namespace illimitadoWepAPI.Controllers
{
    public class AanyaReportsController : ApiController
    {
        #region Error Messages
        private static string errMSGDefault = "There's an error in the system";
        private static string errMSGnoData = "Warning: No data to display";
        private static string errMSGnoTenantCode = "Warning: Missing tenant code";
        #endregion

        // GET: AanyaReports
        //public ActionResult Index()
        //{
        //    return View();
        //}

        #region Get Database Connection

        private void GetConnection(string series_code)
        {
            //series_code = Crypto.url_decrypt(series_code);
            string path = HttpContext.Current.Server.MapPath("~/appsettings.json");

            //StreamReader r = new StreamReader(path);
            string jsonString = File.ReadAllText(path);
            var jsonObject = JObject.Parse(jsonString);
            var jsonAanyaConnectionString = jsonObject["AanyaConnectionString"].ToString();
            AanyaConnectionString connectionObject = JsonConvert.DeserializeObject<AanyaConnectionString>(jsonAanyaConnectionString);

            var settings = ConfigurationManager.ConnectionStrings["connectionstring"];
            var fi = typeof(ConfigurationElement).GetField(
                          "_bReadOnly",
                          BindingFlags.Instance | BindingFlags.NonPublic);
            fi.SetValue(settings, false);

            if (connectionObject.isTestMode == "true" && series_code == "0001")
            {
                settings.ConnectionString = $"Data Source={connectionObject.instance_name_test}; Database = {series_code + connectionObject.catalog_test};User Id={connectionObject.user_name_test};Password={connectionObject.user_hash_test};";
            }
            else
            {
                settings.ConnectionString = $"Data Source={connectionObject.instance_name}; Database = {series_code + connectionObject.catalog};User Id={connectionObject.user_name};Password={connectionObject.user_hash};";
            }
            
        }
        #endregion

        #region BIR

        [HttpPost]
        [Route("GetGov1601Cv2_aanya")]
        public string GetGov1601Cv2(ReportParameter parameter)
        {
            try
            {
                GetConnection(parameter.series_code);
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@tenantid", mytype = SqlDbType.NVarChar, Value = parameter.tenant_id });
                con.myparameters.Add(new myParameters { ParameterName = "@month", mytype = SqlDbType.NVarChar, Value = parameter.month });
                con.myparameters.Add(new myParameters { ParameterName = "@year", mytype = SqlDbType.NVarChar, Value = parameter.year });
                DataTable GovDueDetails = con.GetDataTable("sp_GetGov1601-C");
                DataTable CompanyInfo = GetCompanyInfo(parameter.tenant_id, parameter.series_code);
                DataTable signatory = GetCompanySignatory(parameter.tenant_id, parameter.series_code, "20");

                // If Gov Details is null
                if (GovDueDetails.Rows.Count < 1) return errMSGnoData;

                // If Company Info is null
                if (CompanyInfo.Rows.Count < 1) return errMSGnoData;

                string month = GovDueDetails.Rows[0][0].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][0].ToString();//       [1month]
                string year = GovDueDetails.Rows[0][1].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][1].ToString();//        [1year],	
                string two = GovDueDetails.Rows[0][2].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][2].ToString();//         [2],		
                string three = GovDueDetails.Rows[0][3].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][3].ToString();//       [3],		
                string four = GovDueDetails.Rows[0][4].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][4].ToString();//        [4],	
                string five = GovDueDetails.Rows[0][5].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][5].ToString();//        [5],	
                string six = GovDueDetails.Rows[0][6].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][6].ToString();//         [6],	
                string seven = GovDueDetails.Rows[0][7].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][7].ToString();//       [7],	
                string eigth = GovDueDetails.Rows[0][8].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][8].ToString();//       [8]
                string nine = GovDueDetails.Rows[0][9].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][9].ToString();//        [9]
                string ten = GovDueDetails.Rows[0][10].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][10].ToString();//       [10],
                string oneone = GovDueDetails.Rows[0][11].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][11].ToString();//    [11],
                string onetwo = GovDueDetails.Rows[0][12].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][12].ToString();//    [12],
                string onethree = GovDueDetails.Rows[0][13].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][13].ToString();//  [13],
                string onefour = GovDueDetails.Rows[0][14].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][14].ToString();//   [14],
                string onefive = GovDueDetails.Rows[0][15].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][15].ToString();//   [15]
                string onesixa = GovDueDetails.Rows[0][16].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][16].ToString();//   [16A],
                string onesivb = GovDueDetails.Rows[0][17].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][17].ToString();//   [16B],
                string onesixc = GovDueDetails.Rows[0][18].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][18].ToString();//   [16C]
                string oneseven = GovDueDetails.Rows[0][19].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][19].ToString();//  [17]
                string oneeigth = GovDueDetails.Rows[0][20].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][20].ToString();//   [18]
                string onenine = GovDueDetails.Rows[0][21].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][21].ToString();//   [19]
                string twozero = GovDueDetails.Rows[0][22].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][22].ToString();//   [20]
                string twoonea = GovDueDetails.Rows[0][23].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][23].ToString();//   [21A]
                string twooneb = GovDueDetails.Rows[0][24].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][24].ToString();//   [21B]
                string twotwo = GovDueDetails.Rows[0][25].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][25].ToString();//    [22],
                string twothree = GovDueDetails.Rows[0][26].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][26].ToString();//  [23]
                string twofoura = GovDueDetails.Rows[0][27].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][27].ToString();//  [24A],
                string twofourb = GovDueDetails.Rows[0][28].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][28].ToString();//  [24B],
                string twofourc = GovDueDetails.Rows[0][29].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][29].ToString();//  [24C],
                string twofourd = GovDueDetails.Rows[0][30].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][30].ToString();//  [24D],
                string twofive = GovDueDetails.Rows[0][31].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][31].ToString();//   [25]
                string twoseven = GovDueDetails.Rows[0][32].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][32].ToString();//  [27],
                string position = GovDueDetails.Rows[0][33].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][33].ToString();//  [POSITION],
                string twoeigth = GovDueDetails.Rows[0][34].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][34].ToString();//  [28],
                string twonine = GovDueDetails.Rows[0][35].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][35].ToString();//   [29],
                string threezera = GovDueDetails.Rows[0][36].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][36].ToString();// [30A]
                string threezerb = GovDueDetails.Rows[0][37].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][37].ToString();// [30B]
                string threezerc = GovDueDetails.Rows[0][38].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][38].ToString();// [30C]
                string threezerd = GovDueDetails.Rows[0][39].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][39].ToString();// [30D]
                string threeone = GovDueDetails.Rows[0][40].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][40].ToString();//  [31]
                string CompanyCode = GovDueDetails.Rows[0][41].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][41].ToString();
                string input = five;
                double partSize = 3;
                int k = 0;
                var output = input
                    .ToLookup(c => Math.Floor(k++ / partSize))
                    .Select(e => new String(e.ToArray()));
                string[] tinlist = output.ToArray();
                string tin1 = String.Join("   ", tinlist[0].Reverse());
                string tin2 = String.Join("   ", tinlist[1].Reverse());
                string tin3 = String.Join("   ", tinlist[2].Reverse());
                string tin4 = "000";
                string pathmoto = "1601C";
                PdfReader reader1 = new PdfReader(HttpContext.Current.Server.MapPath("~/Files/PayrollOutSourcing/PDFTemplate/") + pathmoto + ".pdf");
                string reader2 = HttpContext.Current.Server.MapPath("~/Files/PayrollOutSourcing/PDFTemplate/") + pathmoto;
                var pdfReader1 = new PdfReader(reader1);
                AcroFields af = pdfReader1.AcroFields;


                string formFile = reader2;
                // Output file path
                string newFile = HttpContext.Current.Server.MapPath("~/sFTP/PayrollOutsourcing/") + CompanyInfo.Rows[0][5].ToString() + "_1601C_" + parameter.year + parameter.month + ".pdf";
                // read the template file
                PdfReader reader = new PdfReader(formFile + ".pdf");
                // instantiate PDFStamper object
                // The Output file will be created from template file and edited by the PDFStamper
                PdfStamper stamper = new PdfStamper(reader, new FileStream(newFile, FileMode.Create));

                // Object to deal with the Output file's textfields
                AcroFields fields = stamper.AcroFields;
                string month2 = String.Join("   ", month.Reverse());
                string year2 = String.Join("   ", year.Reverse());
                fields.SetField("MONTH", String.Join("", month2.Reverse()));
                fields.SetField("YEAR", String.Join("", year2.Reverse()));
                fields.SetField("2", two);
                fields.SetField("3", three);
                fields.SetField("4", four);
                fields.SetField("TIN1", String.Join("", tin1.Reverse()));
                fields.SetField("TIN2", String.Join("", tin2.Reverse()));
                fields.SetField("TIN3", String.Join("", tin3.Reverse()));
                fields.SetField("TIN4", String.Join("   ", tin4.Reverse()));
                fields.SetField("6", six);
                fields.SetField("7", seven);
                fields.SetField("8", eigth);
                fields.SetField("9", nine);
                fields.SetField("10", ten);
                fields.SetField("11", oneone);
                fields.SetField("12", onetwo);
                fields.SetField("13", onethree);
                fields.SetField("14", onefour);
                fields.SetField("15", onefive);
                fields.SetField("16A", onesixa);
                fields.SetField("16B", onesivb);
                fields.SetField("16C", onesixc);
                fields.SetField("17", oneseven);
                fields.SetField("18", oneeigth);
                fields.SetField("19", onenine);
                fields.SetField("20", twozero);
                fields.SetField("21A", twoonea);
                fields.SetField("21B", twooneb);
                fields.SetField("22", twotwo);
                fields.SetField("23", twothree);
                fields.SetField("24A", twofoura);
                fields.SetField("24B", twofourb);
                fields.SetField("24C", twofourc);
                fields.SetField("24D", twofourd);
                fields.SetField("25", twofive);
                fields.SetField("27", signatory.Rows[0][1].ToString().ToUpper());
                fields.SetField("POSITION", signatory.Rows[0][2].ToString().ToUpper());
                fields.SetField("28", signatory.Rows[1][1].ToString().ToUpper());
                fields.SetField("untitled34", signatory.Rows[1][2].ToString().ToUpper());
                fields.SetField("29", twonine);
                fields.SetField("30A", threezera);
                fields.SetField("30B", threezerb);
                fields.SetField("30C", threezerc);
                fields.SetField("30D", threezerd);
                fields.SetField("31", threeone);
                Byte[] ID1Bit = null;
                Image ID1 = null;
                var ID1ContentByte = stamper.GetOverContent(1); // 3 represents page number
                AcroFields.FieldPosition fieldpos = fields.GetFieldPositions("signature1")[0];
                Rectangle rect = fieldpos.position;
                float xxxx = rect.Left;
                float yyyy = rect.Bottom;
                try
                {
                    string img1url = HttpContext.Current.Server.MapPath("~/Files/PayrollOutSourcing/Signature/") + signatory.Rows[0][3].ToString();
                    using (var webClient = new WebClient())
                    {
                        ID1Bit = webClient.DownloadData(img1url);
                    }
                    ID1 = Image.GetInstance(ID1Bit); // convert to byte array
                    ID1.SetAbsolutePosition(xxxx, yyyy); // set position inside field to place picture 
                    ID1.ScaleAbsolute(rect); // set absolute size of image as per size of field itself
                    ID1ContentByte.AddImage(ID1); // add image to field
                }
                catch (Exception)
                {

                }
                fieldpos = fields.GetFieldPositions("signature2")[0];
                rect = fieldpos.position;
                xxxx = rect.Left;
                yyyy = rect.Bottom;
                try
                {
                    string img1url = HttpContext.Current.Server.MapPath("~/Files/PayrollOutSourcing/Signature/") + signatory.Rows[1][3].ToString();
                    using (var webClient = new WebClient())
                    {
                        ID1Bit = webClient.DownloadData(img1url);
                    }
                    ID1 = Image.GetInstance(ID1Bit); // convert to byte array
                    ID1.SetAbsolutePosition(xxxx, yyyy); // set position inside field to place picture 
                    ID1.ScaleAbsolute(rect); // set absolute size of image as per size of field itself
                    ID1ContentByte.AddImage(ID1); // add image to field
                }
                catch (Exception)
                {

                }
                stamper.FormFlattening = true;
                stamper.Close();
                //HttpContext.Current.Response.ContentType = "Application/pdf";
                //HttpContext.Current.Response.AppendHeader("Content-Disposition", "attachment; filename= " + CompanyCode + "_1601C_" + DateTime.Now.ToString("YYYYMM") + ".pdf");
                //HttpContext.Current.Response.TransmitFile(HttpContext.Current.Server.MapPath("~/sFTP/PayrollOutsourcing/" + "1601C" + DateTime.Now.ToString("dd-H-mm") + ".pdf"));
                //HttpContext.Current.Response.End();
                //return "Successfully Running File";

                string ReturnPath = "https://ws.durusthr.com/ILM_WS_Live/sFTP/PayrollOutsourcing/" + CompanyInfo.Rows[0][5].ToString() + "_1601C_" + parameter.year + parameter.month + ".pdf";
                return ReturnPath;
            }
            catch (Exception e)
            {
                return "Error: " + e.ToString();
                throw;
            }
        }

        [HttpPost]
        [Route("Extract1601C_aanya")]
        public string Extract1601C(ReportParameter parameter)
        {
            try
            {
                FileStream stream = null;
                string dataDir = HostingEnvironment.MapPath("~/Files/PayrollOutSourcing/");

                stream = new FileStream(dataDir + "Template1601C.xls", FileMode.Open);
                string filename;
                string datebuild = DateTime.Now.ToString("MMddyyyy");
                string datebuild1 = DateTime.Now.ToString("yyyyMM");
                string timebuild = DateTime.Now.ToString("hhtt");

                GetConnection(parameter.series_code);
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@TenantID", mytype = SqlDbType.NVarChar, Value = parameter.tenant_id });
                con.myparameters.Add(new myParameters { ParameterName = "@year", mytype = SqlDbType.NVarChar, Value = parameter.year });
                con.myparameters.Add(new myParameters { ParameterName = "@month", mytype = SqlDbType.NVarChar, Value = parameter.month });
                DataTable DT = con.GetDataTable("sp_Export_1601C");

                if (DT.Rows.Count < 1) return errMSGnoData;

                string companyCode = "";
                for (int i = 0; i < DT.Rows.Count; i++)
                {
                    companyCode = DT.Rows[0]["cCode"].ToString();
                    break;
                }

                filename = companyCode + "_1601C_" + parameter.year + parameter.month + "_" + timebuild + ".xls";
                using (var fileStream = new FileStream(dataDir + filename, FileMode.Create, FileAccess.Write))
                {
                    stream.CopyTo(fileStream);
                }
                stream.Close();
                ExcelWorkbookFormat workbookFormat = ExcelWorkbookFormat.Xls_2003;
                string testDocFile = dataDir + "Template1601C.xls";
                ExcelWorkbook workbook = new ExcelWorkbook(testDocFile);
                ExcelWorksheet firstWorksheet = workbook.Worksheets[0];

                int x = 2;
                for (int i = 0; i < DT.Rows.Count; i++, x++)
                {
                    firstWorksheet["A" + x].Text = DT.Rows[i]["EmpID"].ToString();
                    firstWorksheet["B" + x].Text = DT.Rows[i]["TaxTable"].ToString();
                    firstWorksheet["C" + x].Text = DT.Rows[i]["PayCode"].ToString();
                    firstWorksheet["D" + x].Text = DT.Rows[i]["PeriodFrom"].ToString();
                    firstWorksheet["E" + x].Text = DT.Rows[i]["PerioTo"].ToString();
                    firstWorksheet["F" + x].Value = DT.Rows[i]["Basic"];
                    firstWorksheet["G" + x].Value = DT.Rows[i]["MISC"];
                    firstWorksheet["H" + x].Value = DT.Rows[i]["Overtime"];
                    firstWorksheet["I" + x].Value = DT.Rows[i]["Tax"];
                    firstWorksheet["J" + x].Value = DT.Rows[i]["GrossIncome"];
                    firstWorksheet["K" + x].Value = DT.Rows[i]["WitholdingTax"];
                    firstWorksheet["L" + x].Value = DT.Rows[i]["EmployeeSSS"];
                    firstWorksheet["M" + x].Value = DT.Rows[i]["EmployeeMCR"];
                    firstWorksheet["N" + x].Value = DT.Rows[i]["EmployeePagibig"];
                    firstWorksheet["O" + x].Value = DT.Rows[i]["OtherNTXIncome"];
                    firstWorksheet["P" + x].Value = DT.Rows[i]["EmployerSSS"];
                    firstWorksheet["Q" + x].Value = DT.Rows[i]["EmployerMCR"];
                    firstWorksheet["R" + x].Value = DT.Rows[i]["EmployerEC"];
                    firstWorksheet["S" + x].Value = DT.Rows[i]["EmployerPagibig"];
                    firstWorksheet["T" + x].Value = DT.Rows[i]["NTDeminis"];
                    firstWorksheet["U" + x].Value = DT.Rows[i]["NT13thMonth"];
                    firstWorksheet["V" + x].Value = DT.Rows[i]["NTSSS_PHIC_HDMF"];
                    firstWorksheet["W" + x].Value = DT.Rows[i]["NTOhters"];
                    firstWorksheet["X" + x].Value = DT.Rows[i]["MWEBasic"];
                    firstWorksheet["Y" + x].Value = DT.Rows[i]["MWEOT"];
                    firstWorksheet["Z" + x].Value = DT.Rows[i]["MWEOther"];
                    firstWorksheet["AA" + x].Value = DT.Rows[i]["Below20K"];
                    firstWorksheet["AB" + x].Value = DT.Rows[i]["ExcessHDMF"];
                    firstWorksheet["AC" + x].Value = DT.Rows[i]["taxRefund"];
                    firstWorksheet["AD" + x].Value = DT.Rows[i]["taxPayable"];
                }
                firstWorksheet.Name = companyCode + "1601C_" + parameter.year + parameter.month;
                HttpResponse httpResponse = System.Web.HttpContext.Current.Response;

                httpResponse.Clear();
                httpResponse.ContentType = "Application/x-msexcel";
                httpResponse.AddHeader("Content-Disposition", String.Format("attachment; filename={0}", filename));
                string newDIR = HttpContext.Current.Server.MapPath("~/sFTP/PayrollOutsourcing/");
                string onlineDIR = "https://ws.durusthr.com/ILM_WS_Live/sFTP/PayrollOutsourcing/";
                workbook.Save(newDIR + filename);
                stream.Close();
                return onlineDIR + filename;
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }

        [HttpPost]
        [Route("GetGov1601CNEWv2_aanya")]
        public string GetGov1601CNEWv2(ReportParameter parameter)
        {
            try
            {
                GetConnection(parameter.series_code);
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@tenantid", mytype = SqlDbType.NVarChar, Value = parameter.tenant_id });
                con.myparameters.Add(new myParameters { ParameterName = "@month", mytype = SqlDbType.NVarChar, Value = parameter.month });
                con.myparameters.Add(new myParameters { ParameterName = "@year", mytype = SqlDbType.NVarChar, Value = parameter.year });
                DataTable GovDueDetails = con.GetDataTable("sp_GetGov1601-C-2018");
                DataTable CompanyInfo = GetCompanyInfo(parameter.tenant_id, parameter.series_code);
                DataTable signatory = GetCompanySignatory(parameter.tenant_id, parameter.series_code, "21");

                if (GovDueDetails.Rows.Count < 1) return errMSGnoData;
                if (CompanyInfo.Rows.Count < 1) return errMSGnoData;

                string month = GovDueDetails.Rows[0][0].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][0].ToString();//       [1month]
                string year = GovDueDetails.Rows[0][1].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][1].ToString();//        [1year],	
                string two = GovDueDetails.Rows[0][2].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][2].ToString();//         [2],		
                string three = GovDueDetails.Rows[0][3].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][3].ToString();//       [3],		
                string four = GovDueDetails.Rows[0][4].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][4].ToString();//        [4],	
                string five = GovDueDetails.Rows[0][5].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][5].ToString();//        [5],	
                string six = GovDueDetails.Rows[0][6].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][6].ToString();//         [6],	
                string seven = GovDueDetails.Rows[0][7].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][7].ToString();//       [7],	
                string eigth = GovDueDetails.Rows[0][8].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][8].ToString();//       [8]
                string nine = GovDueDetails.Rows[0][9].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][9].ToString();//        [9]
                string ten = GovDueDetails.Rows[0][10].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][10].ToString();//       [10],
                string oneone = GovDueDetails.Rows[0][11].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][11].ToString();//    [11],
                string onetwo = GovDueDetails.Rows[0][12].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][12].ToString();//    [12],
                string onethree = GovDueDetails.Rows[0][13].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][13].ToString();//  [13],
                string onefour = GovDueDetails.Rows[0][14].ToString().Replace(",", " ") == "" ? "N/A" : GovDueDetails.Rows[0][14].ToString().Replace(",", " ");//   [14],
                string onefive = GovDueDetails.Rows[0][15].ToString().Replace(",", " ") == "" ? "N/A" : GovDueDetails.Rows[0][15].ToString().Replace(",", " ");//   [15]
                string onesixa = GovDueDetails.Rows[0][16].ToString().Replace(",", " ") == "" ? "N/A" : GovDueDetails.Rows[0][16].ToString().Replace(",", " ");//   [16],
                string onesivb = GovDueDetails.Rows[0][17].ToString().Replace(",", " ") == "" ? "N/A" : GovDueDetails.Rows[0][17].ToString().Replace(",", " ");//   [17],
                string onesixc = GovDueDetails.Rows[0][18].ToString().Replace(",", " ") == "" ? "N/A" : GovDueDetails.Rows[0][18].ToString().Replace(",", " ");//   [18]
                string oneseven = GovDueDetails.Rows[0][19].ToString().Replace(",", " ") == "" ? "N/A" : GovDueDetails.Rows[0][19].ToString().Replace(",", " ");//  [19]
                string oneeigth = GovDueDetails.Rows[0][20].ToString().Replace(",", " ") == "" ? "N/A" : GovDueDetails.Rows[0][20].ToString().Replace(",", " ");//   [20]
                string onenine = GovDueDetails.Rows[0][21].ToString().Replace(",", " ") == "" ? "N/A" : GovDueDetails.Rows[0][21].ToString().Replace(",", " ");//   [21]
                string twozero = GovDueDetails.Rows[0][22].ToString().Replace(",", " ") == "" ? "N/A" : GovDueDetails.Rows[0][22].ToString().Replace(",", " ");//   [22]
                string twoonea = GovDueDetails.Rows[0][23].ToString().Replace(",", " ") == "" ? "N/A" : GovDueDetails.Rows[0][23].ToString().Replace(",", " ");//   [23]
                string twooneb = GovDueDetails.Rows[0][24].ToString().Replace(",", " ") == "" ? "N/A" : GovDueDetails.Rows[0][24].ToString().Replace(",", " ");//   [24]
                string twotwo = GovDueDetails.Rows[0][25].ToString().Replace(",", " ") == "" ? "N/A" : GovDueDetails.Rows[0][25].ToString().Replace(",", " ");//    [25],
                string twothree = GovDueDetails.Rows[0][26].ToString().Replace(",", " ") == "" ? "N/A" : GovDueDetails.Rows[0][26].ToString().Replace(",", " ");//  [26]
                string twofoura = GovDueDetails.Rows[0][27].ToString().Replace(",", " ") == "" ? "N/A" : GovDueDetails.Rows[0][27].ToString().Replace(",", " ");//  [27],
                string twofourb = GovDueDetails.Rows[0][28].ToString().Replace(",", " ") == "" ? "N/A" : GovDueDetails.Rows[0][28].ToString().Replace(",", " ");//  [28],
                string twofourc = GovDueDetails.Rows[0][29].ToString().Replace(",", " ") == "" ? "N/A" : GovDueDetails.Rows[0][29].ToString().Replace(",", " ");//  [29],
                string twofourd = GovDueDetails.Rows[0][30].ToString().Replace(",", " ") == "" ? "N/A" : GovDueDetails.Rows[0][30].ToString().Replace(",", " ");//  [30],
                string twofive = GovDueDetails.Rows[0][31].ToString().Replace(",", " ") == "" ? "N/A" : GovDueDetails.Rows[0][31].ToString().Replace(",", " ");//   [31]
                string twoseven = GovDueDetails.Rows[0][32].ToString().Replace(",", " ") == "" ? "N/A" : GovDueDetails.Rows[0][32].ToString().Replace(",", " ");//  [32],
                string position = GovDueDetails.Rows[0][33].ToString().Replace(",", " ") == "" ? "N/A" : GovDueDetails.Rows[0][33].ToString().Replace(",", " ");//  [33],
                string twoeigth = GovDueDetails.Rows[0][34].ToString().Replace(",", " ") == "" ? "N/A" : GovDueDetails.Rows[0][34].ToString().Replace(",", " ");//  [34],
                string twonine = GovDueDetails.Rows[0][35].ToString().Replace(",", " ") == "" ? "N/A" : GovDueDetails.Rows[0][35].ToString().Replace(",", " ");//   [35],
                string threezera = GovDueDetails.Rows[0][36].ToString().Replace(",", " ") == "" ? "N/A" : GovDueDetails.Rows[0][36].ToString().Replace(",", " ");// [w36]
                //string threezerb = GovDueDetails.Rows[0][37].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][37].ToString();// [30B]
                //string threezerc = GovDueDetails.Rows[0][38].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][38].ToString();// [30C]
                //string threezerd = GovDueDetails.Rows[0][39].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][39].ToString();// [30D]
                //string threeone = GovDueDetails.Rows[0][40].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][40].ToString();//  [31]
                string CompanyCode = GovDueDetails.Rows[0][37].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][37].ToString();
                string input = six;
                double partSize = 3;
                int k = 0;
                var output = input
                    .ToLookup(c => Math.Floor(k++ / partSize))
                    .Select(e => new String(e.ToArray()));
                string[] tinlist = output.ToArray();
                string tin1 = String.Join("   ", tinlist[0].Reverse());
                string tin2 = String.Join("   ", tinlist[1].Reverse());
                string tin3 = String.Join("   ", tinlist[2].Reverse());
                string tin4 = "000";
                string onefour1 = String.Join("   ", onefour.Reverse());
                string onefive1 = String.Join("   ", onefive.Reverse());
                string onesixa1 = String.Join("   ", onesixa.Reverse());
                string onesivb1 = String.Join("   ", onesivb.Reverse());
                string onesixc1 = String.Join("   ", onesixc.Reverse());
                string oneseven1 = String.Join("   ", oneseven.Reverse());
                string oneeigth1 = String.Join("   ", oneeigth.Reverse());
                string onenine1 = String.Join("   ", onenine.Reverse());
                string twozero1 = String.Join("   ", twozero.Reverse());
                string twoonea1 = String.Join("   ", twoonea.Reverse());
                string twooneb1 = String.Join("   ", twooneb.Reverse());
                string twotwo1 = String.Join("   ", twotwo.Reverse());
                string twothree1 = String.Join("   ", twothree.Reverse());
                string twofoura1 = String.Join("   ", twofoura.Reverse());
                string twofourb1 = String.Join("   ", twofourb.Reverse());
                string twofourc1 = String.Join("   ", twofourc.Reverse());
                string twofourd1 = String.Join("   ", twofourd.Reverse());
                string twofive1 = String.Join("   ", twofive.Reverse());
                string twoseven1 = String.Join("   ", twoseven.Reverse());
                string position1 = String.Join("   ", position.Reverse());
                string twoeigth1 = String.Join("   ", twoeigth.Reverse());
                string twonine1 = String.Join("   ", twonine.Reverse());
                string threezera1 = String.Join("   ", threezera.Reverse());
                string pathmoto = "1601C-NEW";
                PdfReader reader1 = new PdfReader(HttpContext.Current.Server.MapPath("~/Files/PayrollOutSourcing/PDFTemplate/") + pathmoto + ".pdf");
                string reader2 = HttpContext.Current.Server.MapPath("~/Files/PayrollOutSourcing/PDFTemplate/") + pathmoto;
                var pdfReader1 = new PdfReader(reader1);
                AcroFields af = pdfReader1.AcroFields;


                string formFile = reader2;
                // Output file path
                string newFile = HttpContext.Current.Server.MapPath("~/sFTP/PayrollOutsourcing/") + CompanyInfo.Rows[0][5].ToString() + "_1601C_NEW_" + parameter.year + parameter.month + ".pdf";
                // read the template file
                PdfReader reader = new PdfReader(formFile + ".pdf");
                // instantiate PDFStamper object
                // The Output file will be created from template file and edited by the PDFStamper
                PdfStamper stamper = new PdfStamper(reader, new FileStream(newFile, FileMode.Create));

                // Object to deal with the Output file's textfields
                AcroFields fields = stamper.AcroFields;
                string month2 = String.Join("   ", month.Reverse());
                string year2 = String.Join("   ", year.Reverse());
                fields.SetField("MONTH", String.Join("", month2.Reverse()));
                fields.SetField("YEAR", String.Join("", year2.Reverse()));
                //fields.SetField("2", two);
                //fields.SetField("3", three);
                //fields.SetField("4", four);
                fields.SetField("TIN1", String.Join("", tin1.Reverse()));
                fields.SetField("TIN2", String.Join("", tin2.Reverse()));
                fields.SetField("TIN3", String.Join("", tin3.Reverse()));
                fields.SetField("TIN4", String.Join("   ", tin4.Reverse()));
                //fields.SetField("6", six);
                fields.SetField("7", seven);
                fields.SetField("8", eigth);
                fields.SetField("9", nine);
                fields.SetField("10", ten);
                fields.SetField("11", oneone);
                fields.SetField("12", onetwo);
                fields.SetField("13", onethree);
                fields.SetField("14", String.Join("", onefour1.Reverse()).Replace(".", "  "));
                fields.SetField("15", String.Join("", onefive1.Reverse()).Replace(".", "  "));
                fields.SetField("16", String.Join("", onesixa1.Reverse()).Replace(".", "  "));
                fields.SetField("17", String.Join("", onesivb1.Reverse()).Replace(".", "  "));
                fields.SetField("18", String.Join("", onesixc1.Reverse()).Replace(".", "  "));
                fields.SetField("19", String.Join("", oneseven1.Reverse()).Replace(".", "  "));
                fields.SetField("20", String.Join("", oneeigth1.Reverse()).Replace(".", "  "));
                fields.SetField("21", String.Join("", onenine1.Reverse()).Replace(".", "  "));
                fields.SetField("22", String.Join("", twozero1.Reverse()).Replace(".", "  "));
                fields.SetField("23", String.Join("", twoonea1.Reverse()).Replace(".", "  "));
                fields.SetField("24", String.Join("", twooneb1.Reverse()).Replace(".", "  "));
                fields.SetField("25", String.Join("", twotwo1.Reverse()).Replace(".", "  "));
                fields.SetField("26", String.Join("", twothree1.Reverse()).Replace(".", "  "));
                fields.SetField("27", String.Join("", twofoura1.Reverse()).Replace(".", "  "));
                fields.SetField("28", String.Join("", twofourb1.Reverse()).Replace(".", "  "));
                fields.SetField("29", String.Join("", twofourc1.Reverse()).Replace(".", "  "));
                fields.SetField("30", String.Join("", twofourd1.Reverse()).Replace(".", "  "));
                fields.SetField("31", String.Join("", twofive1.Reverse()).Replace(".", "  "));
                fields.SetField("32", String.Join("", twoseven1.Reverse()).Replace(".", "  "));
                fields.SetField("33", String.Join("", position1.Reverse()).Replace(".", "  "));
                fields.SetField("34", String.Join("", twoeigth1.Reverse()).Replace(".", "  "));
                fields.SetField("35", String.Join("", twonine1.Reverse()).Replace(".", "  "));
                fields.SetField("36", String.Join("", threezera1.Reverse()).Replace(".", "  "));
                fields.SetField("38D", String.Join("", threezera1.Reverse()).Replace(".", "  "));
                // fields.SetField("NAME1", signatory.Rows[0][1].ToString().ToUpper());
                fields.SetField("NAME2", signatory.Rows[0][1].ToString().ToUpper());
                //fields.SetField("30B", threezera);
                //fields.SetField("30C", threezerc);
                //fields.SetField("30D", threezerd);
                //fields.SetField("31", threeone);
                Byte[] ID1Bit = null;
                Image ID1 = null;
                var ID1ContentByte = stamper.GetOverContent(1); // 3 represents page number
                AcroFields.FieldPosition fieldpos = fields.GetFieldPositions("signature2")[0];
                Rectangle rect = fieldpos.position;
                float xxxx = rect.Left;
                float yyyy = rect.Bottom;
                try
                {
                    string img1url = HttpContext.Current.Server.MapPath("~/Files/PayrollOutSourcing/Signature/") + signatory.Rows[0][3].ToString();
                    using (var webClient = new WebClient())
                    {
                        ID1Bit = webClient.DownloadData(img1url);
                    }
                    ID1 = Image.GetInstance(ID1Bit); // convert to byte array
                    ID1.SetAbsolutePosition(xxxx, yyyy); // set position inside field to place picture 
                    ID1.ScaleAbsolute(rect); // set absolute size of image as per size of field itself
                    ID1ContentByte.AddImage(ID1); // add image to field
                }
                catch (Exception)
                {

                }
                stamper.FormFlattening = true;
                stamper.Close();

                string ReturnPath = "https://ws.durusthr.com/ILM_WS_Live/sFTP/PayrollOutsourcing/" + CompanyInfo.Rows[0][5].ToString() + "_1601C_NEW_" + parameter.year + parameter.month + ".pdf";
                return ReturnPath;
            }
            catch (Exception e)
            {
                return "Error: " + e.ToString();
                throw;
            }
        }

        [HttpPost]
        [Route("PO_NTX_Report_aanya")]
        public string Extract_NTX_Report(ReportParameter parameter)
        {
            FileStream stream = null;
            try
            {
                GetConnection(parameter.series_code);
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@TenantCode", mytype = SqlDbType.NVarChar, Value = parameter.tenant_code});
                con.myparameters.Add(new myParameters { ParameterName = "@Month", mytype = SqlDbType.NVarChar, Value = parameter.month });
                con.myparameters.Add(new myParameters { ParameterName = "@Year", mytype = SqlDbType.NVarChar, Value = parameter.year });
                DataTable NTXData = con.GetDataTable("sp_Extract_NTX_Report");

                if (NTXData.Rows.Count < 1) return errMSGnoData;

                string dataDir = HostingEnvironment.MapPath("~/Files/PayrollOutSourcing/");
                stream = new FileStream(dataDir + "NTXReport.xls", FileMode.Open);
                string filename;
                string datebuild = DateTime.Now.ToString("MMddyyyy");
                string timebuild = DateTime.Now.ToString("hhmmtt");

                filename = parameter.tenant_code + "_NTXReport_" + parameter.month + parameter.year + "_" + timebuild + ".xls";
                using (var fileStream = new FileStream(dataDir + filename, FileMode.Create, FileAccess.Write))
                //using (var fileStream = new FileStream(dataDir + filename, FileMode.Create, FileAccess.Write))
                {
                    stream.CopyTo(fileStream);
                }
                stream.Close();
                ExcelWorkbookFormat workbookFormat = ExcelWorkbookFormat.Xls_2003;
                string testDocFile = dataDir + "NTXReport.xls";
                ExcelWorkbook workbook = new ExcelWorkbook(testDocFile);
                ExcelWorksheet firstWorksheet = workbook.Worksheets[0];
                ExcelWorksheet NTX = workbook.Worksheets[0];
                int x = 2;
                for (int i = 0; i < NTXData.Rows.Count; i++, x++)
                {
                    NTX["A" + x].Text = NTXData.Rows[i]["Company Name"].ToString();
                    NTX["B" + x].Text = NTXData.Rows[i]["Income Code"].ToString();
                    NTX["C" + x].Text = NTXData.Rows[i]["Tag"].ToString();
                    NTX["D" + x].Text = NTXData.Rows[i]["NTX"].ToString();
                }
                HttpResponse httpResponse = System.Web.HttpContext.Current.Response;
                httpResponse.Clear();
                httpResponse.ContentType = "Application/x-msexcel";
                httpResponse.AddHeader("Content-Disposition", String.Format("attachment; filename={0}", filename));
                string newDIR = HttpContext.Current.Server.MapPath("~/sFTP/PayrollOutsourcing/");
                string onlineDIR = "http://ws.durusthr.com/ILM_WS_Live/sFTP/PayrollOutsourcing/";
                //filename = filename.Replace(',', '_');
                //filename = filename.Replace(" ", String.Empty);
                workbook.Save(newDIR + filename);
                stream.Close();
                //return "Success";
                if (NTXData.Rows.Count > 0)
                {
                    return onlineDIR + filename;
                }
                else
                {
                    return "No Rows";
                }
            }
            catch (Exception e)
            {
                stream.Close();
                return "Error: " + e.ToString();
            }
        }

        [HttpPost]
        [Route("PO_TaxPerMonth_Report_aanya")]
        public string Extract_TaxPerMonth_Report(ReportParameter parameter)
        {
            FileStream stream = null;
            try
            {
                GetConnection(parameter.series_code);
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@Year", mytype = SqlDbType.NVarChar, Value = parameter.year });
                con.myparameters.Add(new myParameters { ParameterName = "@TenantCode", mytype = SqlDbType.NVarChar, Value = parameter.tenant_code });
                DataTable WHTAXData = con.GetDataTable("sp_Extract_TaxPerMonth");

                if (WHTAXData.Rows.Count < 1) return errMSGnoData;

                string dataDir = HostingEnvironment.MapPath("~/Files/PayrollOutSourcing/");
                stream = new FileStream(dataDir + "TaxPerMonthReport.xls", FileMode.Open);
                string filename;
                string datebuild = DateTime.Now.ToString("MMddyyyy");
                string timebuild = DateTime.Now.ToString("hhmmtt");

                //filename = datebuild + timebuild + "_PEZAReport" + ".xls";
                filename = parameter.tenant_code + "_TaxPerMonth" + "_" + parameter.year + "_" + timebuild + ".xls";
                using (var fileStream = new FileStream(dataDir + filename, FileMode.Create, FileAccess.Write))
                //using (var fileStream = new FileStream(dataDir + filename, FileMode.Create, FileAccess.Write))
                {
                    stream.CopyTo(fileStream);
                }
                stream.Close();
                ExcelWorkbookFormat workbookFormat = ExcelWorkbookFormat.Xls_2003;
                string testDocFile = dataDir + "TaxPerMonthReport.xls";
                ExcelWorkbook workbook = new ExcelWorkbook(testDocFile);
                ExcelWorksheet firstWorksheet = workbook.Worksheets[0];
                ExcelWorksheet WHTAX = workbook.Worksheets[0];
                int x = 2;
                for (int i = 0; i < WHTAXData.Rows.Count; i++, x++)
                {
                    WHTAX["A" + x].Text = WHTAXData.Rows[i]["Company Name"].ToString();
                    WHTAX["B" + x].Text = WHTAXData.Rows[i]["Month"].ToString();
                    WHTAX["C" + x].Text = WHTAXData.Rows[i]["TAXTABLE"].ToString();
                    WHTAX["D" + x].Text = WHTAXData.Rows[i]["BIR OR#"].ToString();
                    WHTAX["E" + x].Text = WHTAXData.Rows[i]["BIR ORDATE"].ToString();
                    WHTAX["F" + x].Text = WHTAXData.Rows[i]["PAY CODE"].ToString();
                    WHTAX["G" + x].Text = WHTAXData.Rows[i]["TAX WITHHELD"].ToString();
                    WHTAX["H" + x].Text = WHTAXData.Rows[i]["YEAR"].ToString();
                }
                HttpResponse httpResponse = System.Web.HttpContext.Current.Response;
                httpResponse.Clear();
                httpResponse.ContentType = "Application/x-msexcel";
                httpResponse.AddHeader("Content-Disposition", String.Format("attachment; filename={0}", filename));
                string newDIR = HttpContext.Current.Server.MapPath("~/sFTP/PayrollOutsourcing/");
                string onlineDIR = "http://ws.durusthr.com/ILM_WS_Live/sFTP/PayrollOutsourcing/";
                //filename = filename.Replace(',', '_');
                //filename = filename.Replace(" ", String.Empty);
                workbook.Save(newDIR + filename);
                stream.Close();
                //return "Success";
                if (WHTAXData.Rows.Count > 0)
                {
                    return onlineDIR + filename;
                }
                else
                {
                    return "No Rows";
                }
            }
            catch (Exception e)
            {
                stream.Close();
                return "Error: " + e.ToString();
            }
        }

        [HttpPost]
        [Route("PO_PEZA_Report_aanya")]
        public string Extract_PEZA_Report(ReportParameter parameter)
        {
            FileStream stream = null;
            try
            {
                GetConnection(parameter.series_code);
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@Month", mytype = SqlDbType.NVarChar, Value = parameter.month });
                con.myparameters.Add(new myParameters { ParameterName = "@Year", mytype = SqlDbType.NVarChar, Value = parameter.year });
                con.myparameters.Add(new myParameters { ParameterName = "@TenantCode", mytype = SqlDbType.NVarChar, Value = parameter.tenant_code });
                DataTable PEZAData = con.GetDataTable("sp_Extract_PEZA_Report");

                if (PEZAData.Rows.Count < 1) return errMSGnoData;

                string dataDir = HostingEnvironment.MapPath("~/Files/PayrollOutSourcing/");
                stream = new FileStream(dataDir + "PEZAReport.xls", FileMode.Open);
                string filename;
                string datebuild = DateTime.Now.ToString("MMddyyyy");
                string timebuild = DateTime.Now.ToString("hhmmtt");



                filename = parameter.tenant_code + "_PEZAReport_" + parameter.year + "_" + parameter.month + ".xls";
                using (var fileStream = new FileStream(dataDir + filename, FileMode.Create, FileAccess.Write))
                //using (var fileStream = new FileStream(dataDir + filename, FileMode.Create, FileAccess.Write))
                {
                    stream.CopyTo(fileStream);
                }
                stream.Close();
                ExcelWorkbookFormat workbookFormat = ExcelWorkbookFormat.Xls_2003;
                string testDocFile = dataDir + "PEZAReport.xls";
                ExcelWorkbook workbook = new ExcelWorkbook(testDocFile);
                ExcelWorksheet firstWorksheet = workbook.Worksheets[0];
                ExcelWorksheet PEZA = workbook.Worksheets[0];
                int x = 2;
                for (int i = 0; i < PEZAData.Rows.Count; i++, x++)
                {
                    PEZA["A" + x].Text = PEZAData.Rows[i]["PERIOD START"].ToString();
                    PEZA["B" + x].Text = PEZAData.Rows[i]["PERIOD END"].ToString();
                    PEZA["C" + x].Value = PEZAData.Rows[i]["PEZA CLASSIFICATION"];
                    PEZA["D" + x].Value = PEZAData.Rows[i]["F-COUNT"];
                    PEZA["E" + x].Value = PEZAData.Rows[i]["M-COUNT"];
                    PEZA["F" + x].Value = PEZAData.Rows[i]["F-TOTAL"];
                    PEZA["G" + x].Value = PEZAData.Rows[i]["F-TAXABLE"];
                    PEZA["H" + x].Value = PEZAData.Rows[i]["F-NONTAXABLE"];
                    PEZA["I" + x].Value = PEZAData.Rows[i]["M-TOTAL"];
                    PEZA["J" + x].Value = PEZAData.Rows[i]["M-TAXABLE"];
                    PEZA["K" + x].Value = PEZAData.Rows[i]["M-NONTAXABLE"];
                }
                HttpResponse httpResponse = System.Web.HttpContext.Current.Response;
                httpResponse.Clear();
                httpResponse.ContentType = "Application/x-msexcel";
                httpResponse.AddHeader("Content-Disposition", String.Format("attachment; filename={0}", filename));
                string newDIR = HttpContext.Current.Server.MapPath("~/sFTP/PayrollOutsourcing/");
                string onlineDIR = "http://ws.durusthr.com/ILM_WS_Live/sFTP/PayrollOutsourcing/";
                //filename = filename.Replace(',', '_');
                //filename = filename.Replace(" ", String.Empty);
                workbook.Save(newDIR + filename);
                stream.Close();
                //return "Success";
                if (PEZAData.Rows.Count > 0)
                {
                    return onlineDIR + filename;
                }
                else
                {
                    return "No Rows";
                }
            }
            catch (Exception e)
            {
                stream.Close();
                return "Error: " + e.ToString();
            }
        }

        #endregion

        #region 2316
        [HttpPost]
        [Route("Generate2316_aanya")]
        public string MergePDF(ReportParameter parameter)
        {
            Update2316 Up = new Update2316();
            Up.EmpID = parameter.employee_code;
            Up.TenantID = parameter.tenant_id;
            Up.Year = parameter.year;
            Up.db = parameter.series_code;
            UPdate2316(Up);
            //Response.Redirect("~/pdf/BIR19021.pdf"); // view pdf
            string yr = DateTime.Now.ToString("yyyy");
            //         string path = HttpContext.Current.Server.MapPath("pdf");



            //DataTable DTSI = new DataTable();
            //DTSI.Columns.AddRange(new DataColumn[1] { new DataColumn("StartingID") });
            //for (int i = 0; i < SF.empID.Length; i++)
            //{
            //    DTSI.Rows.Add(SF.empID[i]);
            //}


            GetConnection(parameter.series_code);
            Connection con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@TenantID", mytype = SqlDbType.VarChar, Value = parameter.tenant_id });
            con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = parameter.employee_code });
            con.myparameters.Add(new myParameters { ParameterName = "@Year", mytype = SqlDbType.VarChar, Value = parameter.year });
            con.myparameters.Add(new myParameters { ParameterName = "@Type", mytype = SqlDbType.VarChar, Value = parameter.type_2316 });
            DataTable DT = con.GetDataTable("sp_ExportPDF");
            DataTable signatory = GetCompanySignatory(parameter.tenant_id, parameter.series_code, "37");

            string year = parameter.year, from, to, tin, zipcode, ERTIN, ERZIP, empName = "", EEDOB, EENumber, ERName, PERZIP;
            var pdfReader1 = new iTextSharp.text.pdf.PdfReader(HttpContext.Current.Server.MapPath("pdf") + "/PayrollOutSourcing/2316A.pdf");
            AcroFields af = pdfReader1.AcroFields;
            string datebuild = DateTime.Now.ToString("MMddyyyy");
            string CompanyCode = "";
            string empid = "";

            // Checks if there is 2316 data
            if (DT.Rows.Count < 1) return errMSGnoData;

            foreach (DataRow row in DT.Rows)
            {
                empid = row["EmpID"].ToString();
                CompanyCode = row["EmployerName"].ToString();
            }
            // string empids = uProfile.NTID;


            // Template file path
            string formFile = HttpContext.Current.Server.MapPath("pdf") + "/PayrollOutSourcing/2316A.pdf";


            // Output file path
            string newFile = HttpContext.Current.Server.MapPath("pdf") + "/PayrollOutSourcing/" + CompanyCode + "_2316_" + empid + "_" + year + ".pdf";


            // read the template file
            iTextSharp.text.pdf.PdfReader reader = new iTextSharp.text.pdf.PdfReader(formFile);
            //  reader.Close();

            // instantiate PDFStamper object
            // The Output file will be created from template file and edited by the PDFStamper
            PdfStamper stamper = new PdfStamper(reader, new FileStream(
                        newFile, FileMode.Create));

            // Object to deal with the Output file's textfields
            AcroFields fields = stamper.AcroFields;



            foreach (DataRow row in DT.Rows)
            {
                year = row["ForYear"].ToString();



                //  int len = year.Length;



                for (int x = 0; x < year.Length; x++)
                {
                    fields.SetField("year" + (x + 1), year[x].ToString());
                }



                from = row["FromPeriod"].ToString();
                for (int x = 0; x < from.Length; x++)
                {
                    fields.SetField("From" + (x + 1), from[x].ToString());
                }



                to = row["ToPeriod"].ToString();
                for (int x = 0; x < to.Length; x++)
                {
                    fields.SetField("To" + (x + 1), to[x].ToString());
                }



                tin = row["TIN"].ToString();
                for (int x = 0; x < tin.Length; x++)
                {
                    fields.SetField("tin" + (x + 1), tin[x].ToString());
                }

                EEDOB = row["DOB"].ToString();
                for (int x = 0; x < EEDOB.Length; x++)
                {
                    fields.SetField("bdate" + (x + 1), EEDOB[x].ToString());
                }

                EENumber = row["EENumber"].ToString();
                for (int x = 0; x < EENumber.Length; x++)
                {
                    fields.SetField("cNum" + (x + 1), EENumber[x].ToString());
                }

                empName = row["EmpName"].ToString();

                fields.SetField("employeeName", row["EmpName"].ToString());

                fields.SetField("registeredAddress", row["RegisteredAddress"].ToString());

                zipcode = row["ZipCode"].ToString();
                for (int x = 0; x < zipcode.Length; x++)
                {
                    fields.SetField("Zipcode6A" + (x + 1), zipcode[x].ToString());
                }


                fields.SetField("EmployersName", row["ERName"].ToString());
                fields.SetField("ERRegisteredAddress", row["ERAddress"].ToString());
                //fields.SetField("ERRegisteredAddress", row["RegisteredAddress"].ToString());


                if (row["EmployerType"].ToString() == "MainER")
                {

                    fields.SetField("untitled48", "Yes"); // checkbox options : "Yes":"No", true
                }
                else
                {
                    fields.SetField("untitled48", "No"); // checkbox options : "Yes":"No", true
                }
                if (row["EmployerType"].ToString() == "2ndER")
                {

                    fields.SetField("untitled49", "Yes"); // checkbox options : "Yes":"No", true
                }
                else
                {
                    fields.SetField("untitled49", "No"); // checkbox options : "Yes":"No", true
                }



                ERTIN = row["ERTIN"].ToString();
                for (int x = 0; x < ERTIN.Length; x++)
                {
                    fields.SetField("ETIN" + (x + 1), ERTIN[x].ToString());
                }


                //  int len = year.Length;




                fields.SetField("Gross", row["GrossIncome"].ToString());


                ERZIP = row["ERZIP"].ToString();
                for (int x = 0; x < ERZIP.Length; x++)
                {
                    fields.SetField("14A" + (x + 1), ERZIP[x].ToString());
                }



                fields.SetField("PEmployersName", row["PERName"].ToString());
                fields.SetField("PRegisteredAddress", row["PERAddress"].ToString());
                PERZIP = row["PERZIP"].ToString();
                for (int x = 0; x < PERZIP.Length; x++)
                {
                    fields.SetField("18A" + (x + 1), PERZIP[x].ToString());
                }



                fields.SetField("Gross", row["GrossIncome"].ToString());
                fields.SetField("Income", row["TotalNonTaxablePresent"].ToString());
                fields.SetField("Compensation", row["TaxableIncomePresent"].ToString());
                fields.SetField("Taxable", row["TaxableIncomePrevious"].ToString());
                fields.SetField("GrossTaxable", row["GrossTaxable"].ToString());
                fields.SetField("Tax_Due", row["TaxDue"].ToString());
                fields.SetField("PEmployer", row["TaxWithheldPresent"].ToString());
                fields.SetField("PrevEmployer", row["TaxWithheldPrevious"].ToString());
                fields.SetField("Witheld", row["TotalTaxWithheld"].ToString());
                fields.SetField("untitled2", row["BasicSalaryMWE"].ToString());
                fields.SetField("holidayPay", row["HolidayPayMWE"].ToString());



                fields.SetField("overtimePay", row["OvertimePayMWE"].ToString());
                fields.SetField("nightDiff", row["NightDiffPayMWE"].ToString());
                fields.SetField("HazardPayMWE", row["hazardPay"].ToString());
                fields.SetField("13thMonthPay", row["_13thMonthOtherBen"].ToString());
                fields.SetField("Deminisis", row["DeMinimis"].ToString());
                fields.SetField("unionDues", row["SSSGSISEmployee"].ToString());
                fields.SetField("compensation", row["SalariesOther"].ToString());
                fields.SetField("totalNonTaxable", row["TotalNonTaxableExempt"].ToString());
                fields.SetField("basicSalary", row["BasicSalary"].ToString());



                fields.SetField("representation", row["Representation"].ToString());
                fields.SetField("transportation", row["Transportation"].ToString());
                fields.SetField("COLA", row["COLA"].ToString());
                fields.SetField("FHA", row["FixedHousing"].ToString());
                fields.SetField("42A", "");
                fields.SetField("42A2", row["Others42a"].ToString());
                fields.SetField("42B", "");
                fields.SetField("42B2", row["Others42b"].ToString());
                fields.SetField("commission", row["Commission"].ToString());
                fields.SetField("profitSharing", row["ProfitSharing"].ToString());
                fields.SetField("directorFees", row["Fees"].ToString());
                fields.SetField("taxable13month", row["Taxable13thMonth"].ToString());
                fields.SetField("hazardPay2", row["HazardPay"].ToString());
                fields.SetField("overtimepay2", row["OvertimePay"].ToString());
                fields.SetField("49A", row["Others49"].ToString());
                fields.SetField("taxableIncome", row["TotalTaxableCompensation"].ToString());

                fields.SetField("AgentName", row["PresentEmployeer"].ToString());
                fields.SetField("PresentEmploterPrintedName", row["PresentEmployeer"].ToString());

                fields.SetField("EmployeePrintedName", row["EmpName"].ToString());
                fields.SetField("EmployeeSigPrintedName", row["EmpName"].ToString());

                #region For Signatories
                Byte[] ID1Bit = null;
                Image imgSign1 = null, imgSign2 = null;
                var ID1ContentByte = stamper.GetOverContent(1); // 3 represents page number
                var ID2ContentByte = stamper.GetOverContent(1);
                AcroFields.FieldPosition signature1 = fields.GetFieldPositions("signature1a")[0];
                AcroFields.FieldPosition signature2 = fields.GetFieldPositions("signature1b")[0];
                Rectangle rect1 = signature1.position, rect2 = signature2.position;
                float posX1 = rect1.Left, posY1 = rect1.Bottom;
                float posX2 = rect2.Left, posY2 = rect2.Bottom;
                try
                {
                    string imagePath = signatory.Rows[0][3].ToString() == null ? "" : signatory.Rows[0][3].ToString();
                    string img1url = HttpContext.Current.Server.MapPath("~/Files/PayrollOutSourcing/Signature/") + imagePath;
                    using (var webClient = new WebClient())
                    {
                        ID1Bit = webClient.DownloadData(img1url);
                    }
                    imgSign1 = Image.GetInstance(ID1Bit); // convert to byte array
                    imgSign2 = Image.GetInstance(ID1Bit); // convert to byte array
                    imgSign1.SetAbsolutePosition(posX1, posY1); // set position inside field to place picture 
                    imgSign2.SetAbsolutePosition(posX2, posY2); // set position inside field to place picture 
                    imgSign1.ScaleAbsolute(rect1); // set absolute size of image as per size of field itself
                    imgSign2.ScaleAbsolute(rect2); // set absolute size of image as per size of field itself
                    ID1ContentByte.AddImage(imgSign1); // add image to field
                    ID2ContentByte.AddImage(imgSign2); // add image to field
                }
                catch (Exception)
                {

                }
                #endregion
            }
            stamper.FormFlattening = true;
            stamper.Close();   //CompanyCode_2316_EMPID_YYYY
            string filename = CompanyCode + "_2316_" + empid + "_" + year + ".pdf";
            string onlineDIR = "http://ws.durusthr.com/ILM_WS_Live/pdf/PayrollOutSourcing/";
            if (DT.Rows.Count > 0)
            {
                return onlineDIR + filename;
            }
            else
            {
                return "No Rows";
            }
        }
        #endregion

        #region Custom

        [HttpPost]
        [Route("GetGovDuev2_aanya")]
        public string GetGovDuev2(ReportParameter parameter)
        {
            try
            {
                GetConnection(parameter.series_code);
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@tenantid", mytype = SqlDbType.NVarChar, Value = parameter.tenant_id });
                con.myparameters.Add(new myParameters { ParameterName = "@month", mytype = SqlDbType.NVarChar, Value = parameter.month });
                con.myparameters.Add(new myParameters { ParameterName = "@year", mytype = SqlDbType.NVarChar, Value = parameter.year });
                con.myparameters.Add(new myParameters { ParameterName = "@day", mytype = SqlDbType.NVarChar, Value = parameter.date });
                DataTable GovDueDetails = con.GetDataTable("sp_GetGovDueDetails");

                if (GovDueDetails.Rows.Count < 1) return errMSGnoData;

                DataTable CompanyInfo = GetCompanyInfo(parameter.tenant_id, parameter.series_code);
                DataTable signatory = GetCompanySignatory(parameter.tenant_id, parameter.series_code, "18");
                string name = GovDueDetails.Rows[0][0].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][0].ToString();
                string position = GovDueDetails.Rows[0][1].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][1].ToString();
                string company = GovDueDetails.Rows[0][2].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][2].ToString();
                string companyadd = GovDueDetails.Rows[0][3].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][3].ToString();
                string payoutmonthyear = GovDueDetails.Rows[0][4].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][4].ToString();
                string SemiMonthAmount = GovDueDetails.Rows[0][5].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][5].ToString();
                string SSSPremium = GovDueDetails.Rows[0][6].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][6].ToString();
                string PhilHealth = GovDueDetails.Rows[0][7].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][7].ToString();
                string HomeFund = GovDueDetails.Rows[0][8].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][8].ToString();
                string SSSLoans = GovDueDetails.Rows[0][9].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][9].ToString();
                string SSSOtherLoans = GovDueDetails.Rows[0][10].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][10].ToString();
                string HDMFLoans = GovDueDetails.Rows[0][11].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][11].ToString();
                string deadline = GovDueDetails.Rows[0][12].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][12].ToString();
                string ppname = GovDueDetails.Rows[0][13].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][13].ToString();
                string GrossTaxIncome = GovDueDetails.Rows[0][14].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][14].ToString();
                string NonTaxIncome = GovDueDetails.Rows[0][15].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][15].ToString();
                string NetTaxIncome = GovDueDetails.Rows[0][16].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][16].ToString();
                string CompanyCode = GovDueDetails.Rows[0][17].ToString() == "" ? "N/A" : GovDueDetails.Rows[0][17].ToString();
                string pathmoto;
                pathmoto = "GovDueBlank";
                PdfReader reader1 = new PdfReader(HttpContext.Current.Server.MapPath("~/Files/PayrollOutSourcing/PDFTemplate/") + pathmoto + ".pdf");
                string reader2 = HttpContext.Current.Server.MapPath("~/Files/PayrollOutSourcing/PDFTemplate/") + pathmoto;
                var pdfReader1 = new PdfReader(reader1);
                AcroFields af = pdfReader1.AcroFields;


                string formFile = reader2;
                // Output file path
                string newFile = HttpContext.Current.Server.MapPath("~/sFTP/PayrollOutsourcing/") + CompanyInfo.Rows[0][5].ToString() + "_GovDue_" + parameter.year + parameter.month + ".pdf";
                // read the template file
                PdfReader reader = new PdfReader(formFile + ".pdf");
                // instantiate PDFStamper object
                // The Output file will be created from template file and edited by the PDFStamper
                PdfStamper stamper = new PdfStamper(reader, new FileStream(newFile, FileMode.Create));

                // Object to deal with the Output file's textfields
                AcroFields fields = stamper.AcroFields;

                fields.SetField("date", DateTime.Now.ToString("dd MMMMM yyyy"));
                fields.SetField("nameadd", name +
                                      Environment.NewLine + position +
                                      Environment.NewLine + company +
                                      Environment.NewLine + companyadd);
                fields.SetField("greetings", "Dear Sir/Madam,");
                fields.SetField("firstpar", "Please prepare cheques immediately for the payment of the following government agencies covering the remittances of SSS, PHILHEALTH, HDMF and BIR premiums/withholding taxes for the the month of " + payoutmonthyear + " for " + company + " : " +
                                      Environment.NewLine +
                                      Environment.NewLine + "           " + CompanyCode + "" +
                                      Environment.NewLine + "           BIR WH Tax : SEMI-MONTHLY              PHP " + SemiMonthAmount + ""
                                      +
                                      Environment.NewLine + "               (Note: BIR Form 1601 Gross Taxable Income should be " + GrossTaxIncome + ")  " +
                                      Environment.NewLine + "               (Note: BIR Form 1601 Non-Taxable Income should be " + NonTaxIncome + ") " +
                                      Environment.NewLine + "               (Note: BIR Form 1601 NET Taxable Income should be " + NetTaxIncome + ")  ");

                fields.SetField("secondpar", "           Social Security System Premium :           PHP " + SSSPremium + "" +
                       Environment.NewLine + "           PhilHealth Insurance Corp.          :           PHP " + PhilHealth + "" +
                       Environment.NewLine + "           Home Dev't. Mutual Fund            :           PHP " + HomeFund + "" +
                       Environment.NewLine + "           SSS SL/EL/CL Loans                   :           PHP " + SSSLoans + "" +
                       Environment.NewLine + "           SSS Other Loans                         :           PHP " + SSSOtherLoans + "" +
                       Environment.NewLine + "           HDMF Loans                                :           PHP " + HDMFLoans + "");


                fields.SetField("thirdpar", "   Kindly double check the above payments against your payroll register and payroll journal entries and remit to the corresponding agencies through your bank on or before the " + deadline + ". ");


                fields.SetField("farewell", "Yours truly," + Environment.NewLine +
                                      Environment.NewLine + signatory.Rows[0][1].ToString().ToUpper() +
                                      Environment.NewLine + signatory.Rows[0][2].ToString().ToUpper());
                Byte[] ID1Bit = null;
                Image ID1 = null;
                var ID1ContentByte = stamper.GetOverContent(1); // 3 represents page number
                AcroFields.FieldPosition fieldpos = fields.GetFieldPositions("signature")[0];
                Rectangle rect = fieldpos.position;
                float xxxx = rect.Left;
                float yyyy = rect.Bottom;
                try
                {
                    string img1url = HttpContext.Current.Server.MapPath("~/Files/PayrollOutSourcing/Signature/") + signatory.Rows[0][3].ToString();
                    using (var webClient = new WebClient())
                    {
                        ID1Bit = webClient.DownloadData(img1url);
                    }
                    ID1 = Image.GetInstance(ID1Bit); // convert to byte array
                    ID1.SetAbsolutePosition(xxxx, yyyy); // set position inside field to place picture 
                    ID1.ScaleAbsolute(rect); // set absolute size of image as per size of field itself
                    ID1ContentByte.AddImage(ID1); // add image to field
                }
                catch (Exception)
                {

                }
                // "Flatten" the form so it wont be editable/usable anymore
                stamper.FormFlattening = true;
                // closing the stamper
                stamper.Close();

                string ReturnPath = "https://ws.durusthr.com/ILM_WS_Live/sFTP/PayrollOutsourcing/" + CompanyInfo.Rows[0][5].ToString() + "_GovDue_" + parameter.year + parameter.month + ".pdf";
                return ReturnPath;
            }
            catch (Exception e)
            {
                return "Error";
                throw;
            }
        }

        [HttpPost]
        [Route("ssphh2genv2_aanya")]
        public string SSPHH2Listv2(ReportParameter parameter)
        {
            try
            {
                GetConnection(parameter.series_code);

                if (TenantCodeChecker(parameter.tenant_code)) return errMSGnoTenantCode;

                Connection con = new Connection();
                DataTable DT = new DataTable();
                con.myparameters.Add(new myParameters { ParameterName = "@code", mytype = SqlDbType.NVarChar, Value = parameter.tenant_code });
                con.myparameters.Add(new myParameters { ParameterName = "@month", mytype = SqlDbType.NVarChar, Value = parameter.month });
                con.myparameters.Add(new myParameters { ParameterName = "@year", mytype = SqlDbType.NVarChar, Value = parameter.year });
                DT = con.GetDataTable("sp_GetSSSPH2");

                if (DT.Rows.Count < 1) return errMSGnoData;

                DataTable signatory = GetCompanySignatory(parameter.tenant_id, parameter.series_code, "25");
                string x = ConfigurationManager.ConnectionStrings["connectionstring"].ToString();
                List<SSPHH2List> ListReturned = new List<SSPHH2List>();

                //return ListReturned;
                string oldFile = HttpContext.Current.Server.MapPath("~/Files/PayrollOutSourcing/PDFTemplate/") + "SSPHH2" + ".pdf";
                //string newFile = HttpContext.Current.Server.MapPath("~/Files/PayrollOutSourcing/PDFTemplate/") + "SSPHH2 v2" + ".pdf";
                string newFile = HttpContext.Current.Server.MapPath("~/sFTP/PayrollOutsourcing/") + parameter.tenant_code.ToUpper() + "_SSPHH2v2_" + parameter.year + parameter.month + ".pdf";

                PdfReader pdfReader = new PdfReader(oldFile);
                PdfStamper pdfStamper = new PdfStamper(pdfReader, new FileStream(newFile, FileMode.Create));
                pdfStamper.Close();

                // open the reader
                PdfReader reader = new PdfReader(oldFile);
                Rectangle size = reader.GetPageSizeWithRotation(1);
                Document document = new Document(size);

                // open the writer
                //FileStream fs = new FileStream(newFile, FileMode.Create, FileAccess.Write);
                FileStream fs = new FileStream(newFile, FileMode.Create, FileAccess.Write);
                PdfWriter writer = PdfWriter.GetInstance(document, fs);
                document.Open();

                // the pdf content
                PdfContentByte cb = writer.DirectContent;

                // select the font properties
                BaseFont bf = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                cb.SetColorFill(BaseColor.DARK_GRAY);
                cb.SetFontAndSize(bf, 8);

                PdfImportedPage page = writer.GetImportedPage(reader, 1);
                cb.AddTemplate(page, 0, 0);

                int NOOFDATA = DT.Rows.Count;
                Double pages = NOOFDATA / 76;
                //Double finalnoofpages = 1;
                //Double baseno = 1.000;
                int txtheight = 640;
                int txtheight2 = 130;
                int baseno2 = 45;
                //int baseno = 00000;

                cb.BeginText();
                //text = DateTime.Today;
                // put the alignment and coordinates here
                cb.ShowTextAligned(0, DateTime.Now.ToString("MM-dd-yyyy"), 50, 750, 0);
                cb.EndText();

                cb.BeginText();
                //text = DT.Rows[i]["CompanyName"].ToString();
                // put the alignment and coordinates here
                cb.ShowTextAligned(0, DT.Rows[0]["CompanyName"].ToString(), 225, 740, 0);
                cb.EndText();

                cb.BeginText();
                //text = DT.Rows[i]["CompanyName"].ToString();
                // put the alignment and coordinates here
                cb.ShowTextAligned(0, DT.Rows[0]["companycode"].ToString(), 292, 722, 0);
                cb.EndText();

                cb.BeginText();
                //text = DT.Rows[i]["CompanyName"].ToString();
                // put the alignment and coordinates here
                cb.ShowTextAligned(0, DT.Rows[0]["startdate"].ToString(), 310, 710, 0);
                cb.EndText();

                cb.BeginText();
                //text = DT.Rows[i]["CompanyName"].ToString();
                // put the alignment and coordinates here
                cb.ShowTextAligned(0, DT.Rows[0]["enddate"].ToString(), 373, 710, 0);
                cb.EndText();

                int numb1 = DT.Rows.Count - 1;
                var couterr = numb1.ToString();
                int counter = 0;

                string text = "";

                for (int i = 0; i < DT.Rows.Count; i++)
                {
                    var counters = i.ToString();
                    SSPHH2List Get = new SSPHH2List();
                    Get.empid = DT.Rows[i]["Empid"].ToString();
                    Get.sss = DT.Rows[i]["SSSnum"].ToString();
                    Get.hdmf = DT.Rows[i]["HDMF"].ToString();
                    Get.empname = DT.Rows[i]["EmployeeName"].ToString();
                    Get.employeephealth = DT.Rows[i]["EmployeePhilHealth"].ToString();
                    Get.employerphealth = DT.Rows[i]["EmployerPhilHealth"].ToString();
                    Get.totalemployeephealth = DT.Rows[i]["TotalEmployeePhilHealth"].ToString();
                    Get.totalemployerphealth = DT.Rows[i]["TotalEmployerPhilHealth"].ToString();
                    Get.finaltotal = DT.Rows[i]["FinalTotal"].ToString();
                    Get.companycode = DT.Rows[i]["CompanyCode"].ToString();
                    Get.companyname = DT.Rows[i]["CompanyName"].ToString();
                    Get.startdate = DT.Rows[i]["StartDate"].ToString();
                    Get.enddate = DT.Rows[i]["EndDate"].ToString();
                    //Get.payoutdate = DT.Rows[i]["PayoutDate"].ToString();
                    ListReturned.Add(Get);



                    if (i < baseno2)
                    {
                        if (counters == couterr)
                        {
                            cb.BeginText();
                            //text = DT.Rows[i]["Empid"].ToString();
                            // put the alignment and coordinates here
                            cb.ShowTextAligned(0, "Totals:" + NOOFDATA, 75, txtheight2, 0);
                            cb.EndText();

                            cb.BeginText();
                            //text = DT.Rows[i]["EmployeePHealth"].ToString();
                            // put the alignment and coordinates here
                            cb.ShowTextAligned(0, DT.Rows[0]["TotalEmployeePhilHealth"].ToString(), 442, txtheight2, 0);
                            cb.EndText();


                            cb.BeginText();
                            //text = DT.Rows[i]["EmployerPHealth"].ToString();
                            // put the alignment and coordinates here
                            cb.ShowTextAligned(0, DT.Rows[0]["TotalEmployerPhilHealth"].ToString(), 495, txtheight2, 0);
                            cb.EndText();

                            cb.BeginText();
                            //text = DT.Rows[i]["EmployeePHealth"].ToString();
                            // put the alignment and coordinates here
                            cb.ShowTextAligned(0, "Total PH:", 442, txtheight2 - 10, 0);
                            cb.EndText();


                            cb.BeginText();
                            //text = DT.Rows[i]["EmployerPHealth"].ToString();
                            // put the alignment and coordinates here
                            cb.ShowTextAligned(0, DT.Rows[0]["FinalTotal"].ToString(), 495, txtheight2 - 10, 0);
                            cb.EndText();

                            cb.BeginText();
                            //text = DT.Rows[i]["Empid"].ToString();
                            // put the alignment and coordinates here
                            cb.ShowTextAligned(0, "Certified Correct :", 75, txtheight2 - 55, 0);
                            cb.EndText();

                            cb.BeginText();
                            //text = DT.Rows[i]["Empid"].ToString();
                            // put the alignment and coordinates here
                            cb.ShowTextAligned(0, "-----------------------------------------------------------------", 75, txtheight2 - 65, 0);
                            cb.EndText();

                            cb.BeginText();
                            //text = DT.Rows[i]["Empid"].ToString();
                            // put the alignment and coordinates here
                            cb.ShowTextAligned(0, signatory.Rows[0][1].ToString().ToUpper(), 75, txtheight2 - 75, 0);
                            cb.EndText();

                            cb.BeginText();
                            //text = DT.Rows[i]["Empid"].ToString();
                            // put the alignment and coordinates here
                            cb.ShowTextAligned(0, signatory.Rows[0][2].ToString().ToUpper(), 75, txtheight2 - 85, 0);
                            cb.EndText();

                            Image image = Image.GetInstance(HttpContext.Current.Server.MapPath("~/Files/PayrollOutSourcing/Signature/") + signatory.Rows[0][3].ToString());
                            image.SetAbsolutePosition(79, 45);
                            image.ScaleAbsolute(50f, 50f);
                            cb.AddImage(image, false);
                        }



                        cb.BeginText();
                        text = DT.Rows[i]["Empid"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(0, text, 75, txtheight, 0);
                        cb.EndText();

                        cb.BeginText();
                        text = DT.Rows[i]["SSSnum"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(0, text, 132, txtheight, 0);
                        cb.EndText();

                        cb.BeginText();
                        text = DT.Rows[i]["HDMF"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(0, text, 199, txtheight, 0);
                        cb.EndText();

                        cb.BeginText();
                        text = DT.Rows[i]["EmployeeName"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(0, text.Length > 17 ? text.Remove(17) : text, 275, txtheight, 0);
                        cb.EndText();

                        cb.BeginText();
                        text = DT.Rows[i]["EmployeePhilHealth"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(0, text, 442, txtheight, 0);
                        cb.EndText();


                        cb.BeginText();
                        text = DT.Rows[i]["EmployerPhilHealth"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(0, text, 495, txtheight, 0);
                        cb.EndText();

                        if (i % 3 == 0)
                        {
                            int decheight = txtheight - 10;
                            txtheight = decheight;
                        }
                        else
                        {
                            int decheight = txtheight - 12;
                            txtheight = decheight;
                        }


                    }
                    else if ((i % baseno2) == 0)
                    {
                        txtheight = 640;
                        document.NewPage();
                        cb.SetColorFill(BaseColor.DARK_GRAY);
                        cb.SetFontAndSize(bf, 8);
                        PdfImportedPage page2 = writer.GetImportedPage(reader, 1);
                        cb.AddTemplate(page2, 0, 0);
                        // put the alignment and coordinates here

                        if (counters == couterr)
                        {
                            cb.BeginText();
                            // put the alignment and coordinates here
                            cb.ShowTextAligned(0, "Totals:" + NOOFDATA, 75, txtheight2, 0);
                            cb.EndText();

                            cb.BeginText();
                            // put the alignment and coordinates here
                            cb.ShowTextAligned(0, DT.Rows[0]["TotalEmployeePhilHealth"].ToString(), 442, txtheight2, 0);
                            cb.EndText();


                            cb.BeginText();
                            // put the alignment and coordinates here
                            cb.ShowTextAligned(0, DT.Rows[0]["TotalEmployerPhilHealth"].ToString(), 495, txtheight2, 0);
                            cb.EndText();

                            cb.BeginText();
                            // put the alignment and coordinates here
                            cb.ShowTextAligned(0, "Total PH:", 442, txtheight2 - 10, 0);
                            cb.EndText();


                            cb.BeginText();
                            // put the alignment and coordinates here
                            cb.ShowTextAligned(0, DT.Rows[0]["FinalTotal"].ToString(), 495, txtheight2 - 10, 0);
                            cb.EndText();

                            cb.BeginText();
                            // put the alignment and coordinates here
                            cb.ShowTextAligned(0, "Certified Correct :", 75, txtheight2 - 55, 0);
                            cb.EndText();

                            cb.BeginText();
                            // put the alignment and coordinates here
                            cb.ShowTextAligned(0, "-----------------------------------------------------------------", 75, txtheight2 - 65, 0);
                            cb.EndText();

                            cb.BeginText();
                            // put the alignment and coordinates here
                            cb.ShowTextAligned(0, signatory.Rows[0][1].ToString().ToUpper(), 75, txtheight2 - 75, 0);
                            cb.EndText();

                            cb.BeginText();
                            // put the alignment and coordinates here
                            cb.ShowTextAligned(0, signatory.Rows[0][2].ToString().ToUpper(), 75, txtheight2 - 85, 0);
                            cb.EndText();
                            Image image = Image.GetInstance(HttpContext.Current.Server.MapPath("~/Files/PayrollOutSourcing/Signature/") + signatory.Rows[0][3].ToString());
                            image.SetAbsolutePosition(79, 45);
                            image.ScaleAbsolute(50f, 50f);
                            cb.AddImage(image, false);
                        }

                        cb.BeginText();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(0, DateTime.Now.ToString("MM-dd-yyyy"), 50, 750, 0);
                        cb.EndText();

                        cb.BeginText();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(0, DT.Rows[0]["CompanyName"].ToString(), 225, 740, 0);
                        cb.EndText();

                        cb.BeginText();
                        //text = DT.Rows[i]["CompanyName"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(0, DT.Rows[0]["companycode"].ToString(), 292, 722, 0);
                        cb.EndText();

                        cb.BeginText();
                        //text = DT.Rows[i]["CompanyName"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(0, DT.Rows[0]["startdate"].ToString(), 310, 710, 0);
                        cb.EndText();

                        cb.BeginText();
                        //text = DT.Rows[i]["CompanyName"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(0, DT.Rows[0]["enddate"].ToString(), 373, 710, 0);
                        cb.EndText();

                        cb.BeginText();
                        text = DT.Rows[i]["Empid"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(0, text, 75, txtheight, 0);
                        cb.EndText();

                        cb.BeginText();
                        text = DT.Rows[i]["SSSnum"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(0, text, 132, txtheight, 0);
                        cb.EndText();

                        cb.BeginText();
                        text = DT.Rows[i]["HDMF"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(0, text, 199, txtheight, 0);
                        cb.EndText();

                        cb.BeginText();
                        text = DT.Rows[i]["EmployeeName"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(0, text.Length > 17 ? text.Remove(17) : text, 275, txtheight, 0);
                        cb.EndText();

                        cb.BeginText();
                        text = DT.Rows[i]["EmployeePhilHealth"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(0, text, 442, txtheight, 0);
                        cb.EndText();


                        cb.BeginText();
                        text = DT.Rows[i]["EmployerPhilHealth"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(0, text, 495, txtheight, 0);
                        cb.EndText();


                        if (i % 3 == 0)
                        {
                            int decheight = txtheight - 10;
                            txtheight = decheight;
                        }
                        else
                        {
                            int decheight = txtheight - 12;
                            txtheight = decheight;
                        }
                    }
                    else
                    {
                        if (counters == couterr)
                        {
                            cb.BeginText();
                            //text = DT.Rows[i]["Empid"].ToString();
                            // put the alignment and coordinates here
                            cb.ShowTextAligned(0, "Totals:" + NOOFDATA, 75, txtheight2, 0);
                            cb.EndText();

                            cb.BeginText();
                            //text = DT.Rows[i]["EmployeePHealth"].ToString();
                            // put the alignment and coordinates here
                            cb.ShowTextAligned(0, DT.Rows[0]["TotalEmployeePhilHealth"].ToString(), 442, txtheight2, 0);
                            cb.EndText();


                            cb.BeginText();
                            //text = DT.Rows[i]["EmployerPHealth"].ToString();
                            // put the alignment and coordinates here
                            cb.ShowTextAligned(0, DT.Rows[0]["TotalEmployerPhilHealth"].ToString(), 495, txtheight2, 0);
                            cb.EndText();

                            cb.BeginText();
                            //text = DT.Rows[i]["EmployeePHealth"].ToString();
                            // put the alignment and coordinates here
                            cb.ShowTextAligned(0, "Total PH:", 442, txtheight2 - 10, 0);
                            cb.EndText();


                            cb.BeginText();
                            //text = DT.Rows[i]["EmployerPHealth"].ToString();
                            // put the alignment and coordinates here
                            cb.ShowTextAligned(0, DT.Rows[0]["FinalTotal"].ToString(), 495, txtheight2 - 10, 0);
                            cb.EndText();

                            cb.BeginText();
                            //text = DT.Rows[i]["Empid"].ToString();
                            // put the alignment and coordinates here
                            cb.ShowTextAligned(0, "Certified Correct :", 75, txtheight2 - 55, 0);
                            cb.EndText();

                            cb.BeginText();
                            //text = DT.Rows[i]["Empid"].ToString();
                            // put the alignment and coordinates here
                            cb.ShowTextAligned(0, "-----------------------------------------------------------------", 75, txtheight2 - 65, 0);
                            cb.EndText();

                            cb.BeginText();
                            //text = DT.Rows[i]["Empid"].ToString();
                            // put the alignment and coordinates here
                            cb.ShowTextAligned(0, signatory.Rows[0][1].ToString().ToUpper(), 75, txtheight2 - 75, 0);
                            cb.EndText();

                            cb.BeginText();
                            //text = DT.Rows[i]["Empid"].ToString();
                            // put the alignment and coordinates here
                            cb.ShowTextAligned(0, signatory.Rows[0][2].ToString().ToUpper(), 75, txtheight2 - 85, 0);
                            cb.EndText();
                            Image image = Image.GetInstance(HttpContext.Current.Server.MapPath("~/Files/PayrollOutSourcing/Signature/") + signatory.Rows[0][3].ToString());
                            image.SetAbsolutePosition(79, 45);
                            image.ScaleAbsolute(50f, 50f);
                            cb.AddImage(image, false);
                        }



                        cb.BeginText();
                        text = DT.Rows[i]["Empid"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(0, text, 75, txtheight, 0);
                        cb.EndText();

                        cb.BeginText();
                        text = DT.Rows[i]["SSSnum"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(0, text, 132, txtheight, 0);
                        cb.EndText();

                        cb.BeginText();
                        text = DT.Rows[i]["HDMF"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(0, text, 199, txtheight, 0);
                        cb.EndText();

                        cb.BeginText();
                        text = DT.Rows[i]["EmployeeName"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(0, text.Length > 17 ? text.Remove(17) : text, 275, txtheight, 0);
                        cb.EndText();

                        cb.BeginText();
                        text = DT.Rows[i]["EmployeePhilHealth"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(0, text, 442, txtheight, 0);
                        cb.EndText();


                        cb.BeginText();
                        text = DT.Rows[i]["EmployerPhilHealth"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(0, text, 495, txtheight, 0);
                        cb.EndText();


                        if (i % 3 == 0)
                        {
                            int decheight = txtheight - 10;
                            txtheight = decheight;
                        }
                        else
                        {
                            int decheight = txtheight - 12;
                            txtheight = decheight;
                        }
                        counter = counter + 1;
                    }
                }

                // close the streams and voilÃ¡ the file should be changed :)
                document.Close();
                fs.Close();
                writer.Close();
                reader.Close();

                string returnPath = "https://ws.durusthr.com/ILM_WS_Live/sFTP/PayrollOutsourcing/" + parameter.tenant_code.ToUpper() + "_SSPHH2v2_" + parameter.year + parameter.month + ".pdf";
                return returnPath;

            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }

        [HttpPost]
        [Route("ssphhdgen_aanya")]
        public string SSPHHDList(ReportParameter parameter)
        {
            try
            {
                GetConnection(parameter.series_code);

                if (TenantCodeChecker(parameter.tenant_code)) return errMSGnoTenantCode;

                Connection con = new Connection();
                DataTable DT = new DataTable();
                con.myparameters.Add(new myParameters { ParameterName = "@code", mytype = SqlDbType.NVarChar, Value = parameter.tenant_code });
                con.myparameters.Add(new myParameters { ParameterName = "@month", mytype = SqlDbType.NVarChar, Value = parameter.month });
                con.myparameters.Add(new myParameters { ParameterName = "@year", mytype = SqlDbType.NVarChar, Value = parameter.year });
                DT = con.GetDataTable("sp_GetSSSPHD");

                if (DT.Rows.Count < 1) return errMSGnoData;

                string x = ConfigurationManager.ConnectionStrings["connectionstring"].ToString();
                List<SSPHHDList> ListReturned = new List<SSPHHDList>();

                //return ListReturned;
                string oldFile = HttpContext.Current.Server.MapPath("~/Files/PayrollOutSourcing/PDFTemplate/") + "SSPHHD" + ".pdf";
                string newFile = HttpContext.Current.Server.MapPath("~/sFTP/PayrollOutSourcing/") + parameter.tenant_code.ToUpper() + "_SSPHHD_" + parameter.year + parameter.month + ".pdf";

                //create file for filestream to access and write values
                PdfReader pdfReader = new PdfReader(oldFile);
                PdfStamper pdfStamper = new PdfStamper(pdfReader, new FileStream(newFile, FileMode.Create));
                pdfStamper.Close();

                // open the reader
                PdfReader reader = new PdfReader(oldFile);
                Rectangle size = reader.GetPageSizeWithRotation(1);
                Document document = new Document(size);

                // open the writer
                FileStream fs = new FileStream(newFile, FileMode.Create, FileAccess.Write);
                PdfWriter writer = PdfWriter.GetInstance(document, fs);
                document.Open();

                // the pdf content
                PdfContentByte cb = writer.DirectContent;

                // select the font properties
                BaseFont bf = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                cb.SetColorFill(BaseColor.DARK_GRAY);
                cb.SetFontAndSize(bf, 8);

                PdfImportedPage page = writer.GetImportedPage(reader, 1);
                cb.AddTemplate(page, 0, 0);


                cb.BeginText();
                //text = DateTime.Today;
                // put the alignment and coordinates here
                cb.ShowTextAligned(0, DateTime.Now.ToString("MM-dd-yyyy"), 30, 565, 0);
                cb.EndText();

                cb.BeginText();
                //text = DT.Rows[i]["CompanyName"].ToString();
                // put the alignment and coordinates here
                cb.ShowTextAligned(Element.ALIGN_CENTER, DT.Rows[0]["CompanyName"].ToString(), 380, 565, 0);
                cb.EndText();

                cb.BeginText();
                //text = DT.Rows[i]["CompanyName"].ToString();
                // put the alignment and coordinates here
                cb.ShowTextAligned(0, DT.Rows[0]["CompanyCode"].ToString(), 377, 543, 0);
                cb.EndText();

                cb.BeginText();
                //text = DT.Rows[i]["CompanyName"].ToString();
                // put the alignment and coordinates here
                cb.ShowTextAligned(0, DT.Rows[0]["startdate"].ToString() + " " + "TO", 403, 532, 0);
                cb.EndText();

                cb.BeginText();
                //text = DT.Rows[i]["CompanyName"].ToString();
                // put the alignment and coordinates here
                cb.ShowTextAligned(0, DT.Rows[0]["enddate"].ToString(), 460, 532, 0);
                cb.EndText();

                int NOOFDATA = DT.Rows.Count;
                Double pages = NOOFDATA / 76;
                int txtheight = 460;
                int txtheight2 = 60;
                int baseno2 = 35;

                int numb1 = DT.Rows.Count - 1;
                var couterr = numb1.ToString();

                string text = "";

                for (int i = 0; i < NOOFDATA; i++)
                {
                    var counters = i.ToString();

                    SSPHHDList Get = new SSPHHDList();

                    Get.Empid = DT.Rows[i]["Empid"].ToString();
                    Get.SSSnum = DT.Rows[i]["SSSnum"].ToString();
                    Get.HDMF = DT.Rows[i]["HDMF"].ToString();
                    Get.EmployeeName = DT.Rows[i]["EmployeeName"].ToString();
                    Get.SSSPremEmployee = DT.Rows[i]["SSSPremEmployee"].ToString();
                    Get.SSSPremEcEmployer = DT.Rows[i]["SSSPremEcEmployer"].ToString();
                    Get.EmployerEC = DT.Rows[i]["EmployerEC"].ToString();
                    Get.EmployeePhilHealth = DT.Rows[i]["EmployeePhilHealth"].ToString();
                    Get.EmployerPhilHealth = DT.Rows[i]["EmployerPhilHealth"].ToString();
                    Get.PagIbigAmount = DT.Rows[i]["PagIbigAmount"].ToString();
                    Get.PagibigERAmount = DT.Rows[i]["PagibigERAmount"].ToString();
                    Get.TotalEmployeeSSS = DT.Rows[i]["TotalEmployeeSSS"].ToString();
                    Get.TotalEmployerSSS = DT.Rows[i]["TotalEmployerSSS"].ToString();
                    Get.TotalEmployerECSSS = DT.Rows[i]["TotalEmployerECSSS"].ToString();
                    Get.FinalTotalSSS = DT.Rows[i]["FinalTotalSSS"].ToString();
                    Get.TotalEmployeePhilHealth = DT.Rows[i]["TotalEmployeePhilHealth"].ToString();
                    Get.TotalEmployerPhilHealth = DT.Rows[i]["TotalEmployerPhilHealth"].ToString();
                    Get.FinalTotalPhilHealth = DT.Rows[i]["FinalTotalPhilHealth"].ToString();
                    Get.TotalPagIbigAmount = DT.Rows[i]["TotalPagIbigAmount"].ToString();
                    Get.TotalPagibigERAmount = DT.Rows[i]["TotalPagibigERAmount"].ToString();
                    Get.FinalTotalPagibig = DT.Rows[i]["FinalTotalPagibig"].ToString();
                    Get.StartDate = DT.Rows[i]["StartDate"].ToString();
                    Get.EndDate = DT.Rows[i]["EndDate"].ToString();
                    Get.CompanyCode = DT.Rows[i]["CompanyCode"].ToString();
                    Get.CompanyName = DT.Rows[i]["CompanyName"].ToString();

                    ListReturned.Add(Get);

                    if (i < baseno2)
                    {
                        //string text = "";
                        // put the alignment and coordinates here
                        if (counters == couterr)
                        {
                            cb.BeginText();
                            //text = DT.Rows[i]["Empid"].ToString();
                            // put the alignment and coordinates here
                            cb.ShowTextAligned(0, "Totals:", 30, txtheight2, 0);
                            cb.EndText();

                            cb.BeginText();
                            //text = DT.Rows[i]["Empid"].ToString();
                            // put the alignment and coordinates here
                            cb.ShowTextAligned(0, "" + DT.Rows.Count, 92, txtheight2, 0);
                            cb.EndText();


                            cb.BeginText();
                            //text = DT.Rows[i]["EmployeePHealth"].ToString();
                            // put the alignment and coordinates here
                            cb.ShowTextAligned(Element.ALIGN_RIGHT, DT.Rows[i]["TotalEmployeeSSS"].ToString(), 447, txtheight2, 0);
                            cb.EndText();

                            cb.BeginText();
                            //text = DT.Rows[i]["EmployeePHealth"].ToString();
                            // put the alignment and coordinates here
                            cb.ShowTextAligned(Element.ALIGN_RIGHT, DT.Rows[i]["TotalEmployerSSS"].ToString(), 507, txtheight2, 0);
                            cb.EndText();

                            cb.BeginText();
                            //text = DT.Rows[i]["EmployeePHealth"].ToString();
                            // put the alignment and coordinates here
                            cb.ShowTextAligned(Element.ALIGN_RIGHT, DT.Rows[i]["TotalEmployerECSSS"].ToString(), 557, txtheight2, 0);
                            cb.EndText();

                            cb.BeginText();
                            //text = DT.Rows[i]["EmployeePHealth"].ToString();
                            // put the alignment and coordinates here
                            cb.ShowTextAligned(Element.ALIGN_RIGHT, "Total SSS:", 507, txtheight2 - 10, 0);
                            cb.EndText();

                            cb.BeginText();
                            //text = DT.Rows[i]["EmployeePHealth"].ToString();
                            // put the alignment and coordinates here
                            cb.ShowTextAligned(Element.ALIGN_RIGHT, DT.Rows[i]["FinalTotalSSS"].ToString(), 557, txtheight2 - 10, 0);
                            cb.EndText();

                            cb.BeginText();
                            //text = DT.Rows[i]["EmployeePHealth"].ToString();
                            // put the alignment and coordinates here
                            cb.ShowTextAligned(Element.ALIGN_RIGHT, DT.Rows[i]["TotalEmployeePhilHealth"].ToString(), 608, txtheight2, 0);
                            cb.EndText();

                            cb.BeginText();
                            //text = DT.Rows[i]["EmployeePHealth"].ToString();
                            // put the alignment and coordinates here
                            cb.ShowTextAligned(Element.ALIGN_RIGHT, DT.Rows[i]["TotalEmployerPhilHealth"].ToString(), 660, txtheight2, 0);
                            cb.EndText();

                            cb.BeginText();
                            //text = DT.Rows[i]["EmployeePHealth"].ToString();
                            // put the alignment and coordinates here
                            cb.ShowTextAligned(Element.ALIGN_RIGHT, "Total PH:", 608, txtheight2 - 10, 0);
                            cb.EndText();

                            cb.BeginText();
                            //text = DT.Rows[i]["EmployeePHealth"].ToString();
                            // put the alignment and coordinates here
                            cb.ShowTextAligned(Element.ALIGN_RIGHT, DT.Rows[i]["FinalTotalPhilHealth"].ToString(), 660, txtheight2 - 10, 0);
                            cb.EndText();


                            cb.BeginText();
                            //text = DT.Rows[i]["EmployeePHealth"].ToString();
                            // put the alignment and coordinates here
                            cb.ShowTextAligned(Element.ALIGN_RIGHT, DT.Rows[i]["TotalPagIbigAmount"].ToString(), 712, txtheight2, 0);
                            cb.EndText();

                            cb.BeginText();
                            //text = DT.Rows[i]["EmployeePHealth"].ToString();
                            // put the alignment and coordinates here
                            cb.ShowTextAligned(Element.ALIGN_RIGHT, DT.Rows[i]["TotalPagibigERAmount"].ToString(), 761, txtheight2, 0);
                            cb.EndText();

                            cb.BeginText();
                            //text = DT.Rows[i]["EmployeePHealth"].ToString();
                            // put the alignment and coordinates here
                            cb.ShowTextAligned(Element.ALIGN_RIGHT, "Total HDMF:", 712, txtheight2 - 10, 0);
                            cb.EndText();

                            cb.BeginText();
                            //text = DT.Rows[i]["EmployeePHealth"].ToString();
                            // put the alignment and coordinates here
                            cb.ShowTextAligned(Element.ALIGN_RIGHT, DT.Rows[i]["FinalTotalPagibig"].ToString(), 761, txtheight2 - 10, 0);
                            cb.EndText();




                        }

                        cb.BeginText();
                        text = DT.Rows[i]["Empid"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(0, text, 30, txtheight, 0);
                        cb.EndText();

                        cb.BeginText();
                        text = DT.Rows[i]["SSSnum"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(0, text, 92, txtheight, 0);
                        cb.EndText();

                        cb.BeginText();
                        text = DT.Rows[i]["HDMF"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(0, text, 156, txtheight, 0);
                        cb.EndText();

                        cb.BeginText();
                        text = DT.Rows[i]["EmployeeName"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(0, text.Length > 17 ? text.Remove(17) : text, 233, txtheight, 0);
                        cb.EndText();

                        cb.BeginText();
                        text = DT.Rows[i]["SSSPremEmployee"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(Element.ALIGN_RIGHT, text, 447, txtheight, 0);
                        cb.EndText();

                        cb.BeginText();
                        text = DT.Rows[i]["SSSPremEcEmployer"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(Element.ALIGN_RIGHT, text, 507, txtheight, 0);
                        cb.EndText();

                        cb.BeginText();
                        text = DT.Rows[i]["EmployerEC"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(Element.ALIGN_RIGHT, text, 557, txtheight, 0);
                        cb.EndText();

                        cb.BeginText();
                        text = DT.Rows[i]["EmployeePhilHealth"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(Element.ALIGN_RIGHT, text, 608, txtheight, 0);
                        cb.EndText();

                        cb.BeginText();
                        text = DT.Rows[i]["EmployerPhilHealth"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(Element.ALIGN_RIGHT, text, 660, txtheight, 0);
                        cb.EndText();

                        cb.BeginText();
                        text = DT.Rows[i]["PagIbigAmount"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(Element.ALIGN_RIGHT, text, 712, txtheight, 0);
                        cb.EndText();

                        cb.BeginText();
                        text = DT.Rows[i]["PagibigERAmount"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(Element.ALIGN_RIGHT, text, 761, txtheight, 0);
                        cb.EndText();




                        if (i % 4 == 0)
                        {
                            int decheight = txtheight - 11;
                            txtheight = decheight;
                        }
                        else if (i % 5 == 0)
                        {
                            int decheight = txtheight - 10;
                            txtheight = decheight;
                        }
                        else if (i % 7 == 0)
                        {
                            int decheight = txtheight - 11;
                            txtheight = decheight;
                        }
                        else
                        {
                            int decheight = txtheight - 12;
                            txtheight = decheight;
                        }

                    }
                    else if ((i % baseno2) == 0)
                    {
                        txtheight = 460;
                        document.NewPage();
                        cb.SetColorFill(BaseColor.DARK_GRAY);
                        cb.SetFontAndSize(bf, 8);
                        //PdfImportedPage page = writer.GetImportedPage(reader, 2);
                        PdfImportedPage page2 = writer.GetImportedPage(reader, 1);
                        cb.AddTemplate(page2, 0, 0);
                        //BaseFont bf = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                        //string text = "";
                        // put the alignment and coordinates here

                        if (counters == couterr)
                        {
                            cb.BeginText();
                            //text = DT.Rows[i]["Empid"].ToString();
                            // put the alignment and coordinates here
                            cb.ShowTextAligned(0, "Totals:", 30, txtheight2, 0);
                            cb.EndText();

                            cb.BeginText();
                            //text = DT.Rows[i]["Empid"].ToString();
                            // put the alignment and coordinates here
                            cb.ShowTextAligned(0, "" + DT.Rows.Count, 92, txtheight2, 0);
                            cb.EndText();


                            cb.BeginText();
                            //text = DT.Rows[i]["EmployeePHealth"].ToString();
                            // put the alignment and coordinates here
                            cb.ShowTextAligned(Element.ALIGN_RIGHT, DT.Rows[i]["TotalEmployeeSSS"].ToString(), 447, txtheight2, 0);
                            cb.EndText();

                            cb.BeginText();
                            //text = DT.Rows[i]["EmployeePHealth"].ToString();
                            // put the alignment and coordinates here
                            cb.ShowTextAligned(Element.ALIGN_RIGHT, DT.Rows[i]["TotalEmployerSSS"].ToString(), 507, txtheight2, 0);
                            cb.EndText();

                            cb.BeginText();
                            //text = DT.Rows[i]["EmployeePHealth"].ToString();
                            // put the alignment and coordinates here
                            cb.ShowTextAligned(Element.ALIGN_RIGHT, DT.Rows[i]["TotalEmployerECSSS"].ToString(), 557, txtheight2, 0);
                            cb.EndText();

                            cb.BeginText();
                            //text = DT.Rows[i]["EmployeePHealth"].ToString();
                            // put the alignment and coordinates here
                            cb.ShowTextAligned(Element.ALIGN_RIGHT, "Total SSS:", 507, txtheight2 - 10, 0);
                            cb.EndText();

                            cb.BeginText();
                            //text = DT.Rows[i]["EmployeePHealth"].ToString();
                            // put the alignment and coordinates here
                            cb.ShowTextAligned(Element.ALIGN_RIGHT, DT.Rows[i]["FinalTotalSSS"].ToString(), 557, txtheight2 - 10, 0);
                            cb.EndText();

                            cb.BeginText();
                            //text = DT.Rows[i]["EmployeePHealth"].ToString();
                            // put the alignment and coordinates here
                            cb.ShowTextAligned(Element.ALIGN_RIGHT, DT.Rows[i]["TotalEmployeePhilHealth"].ToString(), 608, txtheight2, 0);
                            cb.EndText();

                            cb.BeginText();
                            //text = DT.Rows[i]["EmployeePHealth"].ToString();
                            // put the alignment and coordinates here
                            cb.ShowTextAligned(Element.ALIGN_RIGHT, DT.Rows[i]["TotalEmployerPhilHealth"].ToString(), 660, txtheight2, 0);
                            cb.EndText();

                            cb.BeginText();
                            //text = DT.Rows[i]["EmployeePHealth"].ToString();
                            // put the alignment and coordinates here
                            cb.ShowTextAligned(Element.ALIGN_RIGHT, "Total PH:", 608, txtheight2 - 10, 0);
                            cb.EndText();

                            cb.BeginText();
                            //text = DT.Rows[i]["EmployeePHealth"].ToString();
                            // put the alignment and coordinates here
                            cb.ShowTextAligned(Element.ALIGN_RIGHT, DT.Rows[i]["FinalTotalPhilHealth"].ToString(), 660, txtheight2 - 10, 0);
                            cb.EndText();


                            cb.BeginText();
                            //text = DT.Rows[i]["EmployeePHealth"].ToString();
                            // put the alignment and coordinates here
                            cb.ShowTextAligned(Element.ALIGN_RIGHT, DT.Rows[i]["TotalPagIbigAmount"].ToString(), 712, txtheight2, 0);
                            cb.EndText();

                            cb.BeginText();
                            //text = DT.Rows[i]["EmployeePHealth"].ToString();
                            // put the alignment and coordinates here
                            cb.ShowTextAligned(Element.ALIGN_RIGHT, DT.Rows[i]["TotalPagibigERAmount"].ToString(), 761, txtheight2, 0);
                            cb.EndText();

                            cb.BeginText();
                            //text = DT.Rows[i]["EmployeePHealth"].ToString();
                            // put the alignment and coordinates here
                            cb.ShowTextAligned(Element.ALIGN_RIGHT, "Total HDMF:", 712, txtheight2 - 10, 0);
                            cb.EndText();

                            cb.BeginText();
                            //text = DT.Rows[i]["EmployeePHealth"].ToString();
                            // put the alignment and coordinates here
                            cb.ShowTextAligned(Element.ALIGN_RIGHT, DT.Rows[i]["FinalTotalPagibig"].ToString(), 761, txtheight2 - 10, 0);
                            cb.EndText();





                        }

                        cb.BeginText();
                        //text = DateTime.Today;
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(0, DateTime.Now.ToString("MM-dd-yyyy"), 30, 565, 0);
                        cb.EndText();

                        cb.BeginText();
                        //text = DT.Rows[i]["CompanyName"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(Element.ALIGN_CENTER, DT.Rows[0]["CompanyName"].ToString(), 380, 565, 0);
                        cb.EndText();

                        cb.BeginText();
                        //text = DT.Rows[i]["CompanyName"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(0, DT.Rows[0]["CompanyCode"].ToString(), 377, 543, 0);
                        cb.EndText();

                        cb.BeginText();
                        //text = DT.Rows[i]["CompanyName"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(0, DT.Rows[0]["startdate"].ToString() + " " + "TO", 403, 532, 0);
                        cb.EndText();

                        cb.BeginText();
                        //text = DT.Rows[i]["CompanyName"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(0, DT.Rows[0]["enddate"].ToString(), 460, 532, 0);
                        cb.EndText();

                        cb.BeginText();
                        text = DT.Rows[i]["Empid"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(0, text, 30, txtheight, 0);
                        cb.EndText();

                        cb.BeginText();
                        text = DT.Rows[i]["SSSnum"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(0, text, 92, txtheight, 0);
                        cb.EndText();

                        cb.BeginText();
                        text = DT.Rows[i]["HDMF"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(0, text, 156, txtheight, 0);
                        cb.EndText();

                        cb.BeginText();
                        text = DT.Rows[i]["EmployeeName"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(0, text.Length > 17 ? text.Remove(17) : text, 233, txtheight, 0);
                        cb.EndText();

                        cb.BeginText();
                        text = DT.Rows[i]["SSSPremEmployee"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(Element.ALIGN_RIGHT, text, 447, txtheight, 0);
                        cb.EndText();

                        cb.BeginText();
                        text = DT.Rows[i]["SSSPremEcEmployer"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(Element.ALIGN_RIGHT, text, 507, txtheight, 0);
                        cb.EndText();

                        cb.BeginText();
                        text = DT.Rows[i]["EmployerEC"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(Element.ALIGN_RIGHT, text, 557, txtheight, 0);
                        cb.EndText();

                        cb.BeginText();
                        text = DT.Rows[i]["EmployeePhilHealth"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(Element.ALIGN_RIGHT, text, 608, txtheight, 0);
                        cb.EndText();

                        cb.BeginText();
                        text = DT.Rows[i]["EmployerPhilHealth"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(Element.ALIGN_RIGHT, text, 660, txtheight, 0);
                        cb.EndText();

                        cb.BeginText();
                        text = DT.Rows[i]["PagIbigAmount"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(Element.ALIGN_RIGHT, text, 712, txtheight, 0);
                        cb.EndText();

                        cb.BeginText();
                        text = DT.Rows[i]["PagibigERAmount"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(Element.ALIGN_RIGHT, text, 761, txtheight, 0);
                        cb.EndText();


                        if (i % 4 == 0)
                        {
                            int decheight = txtheight - 11;
                            txtheight = decheight;
                        }
                        else if (i % 5 == 0)
                        {
                            int decheight = txtheight - 10;
                            txtheight = decheight;
                        }
                        else if (i % 7 == 0)
                        {
                            int decheight = txtheight - 11;
                            txtheight = decheight;
                        }
                        else
                        {
                            int decheight = txtheight - 12;
                            txtheight = decheight;
                        }
                    }
                    else
                    {


                        //string text = "";
                        // put the alignment and coordinates here

                        if (counters == couterr)
                        {
                            cb.BeginText();
                            //text = DT.Rows[i]["Empid"].ToString();
                            // put the alignment and coordinates here
                            cb.ShowTextAligned(0, "Totals:", 30, txtheight2, 0);
                            cb.EndText();

                            cb.BeginText();
                            //text = DT.Rows[i]["Empid"].ToString();
                            // put the alignment and coordinates here
                            cb.ShowTextAligned(0, "" + DT.Rows.Count, 92, txtheight2, 0);
                            cb.EndText();


                            cb.BeginText();
                            //text = DT.Rows[i]["EmployeePHealth"].ToString();
                            // put the alignment and coordinates here
                            cb.ShowTextAligned(Element.ALIGN_RIGHT, DT.Rows[i]["TotalEmployeeSSS"].ToString(), 447, txtheight2, 0);
                            cb.EndText();

                            cb.BeginText();
                            //text = DT.Rows[i]["EmployeePHealth"].ToString();
                            // put the alignment and coordinates here
                            cb.ShowTextAligned(Element.ALIGN_RIGHT, DT.Rows[i]["TotalEmployerSSS"].ToString(), 507, txtheight2, 0);
                            cb.EndText();

                            cb.BeginText();
                            //text = DT.Rows[i]["EmployeePHealth"].ToString();
                            // put the alignment and coordinates here
                            cb.ShowTextAligned(Element.ALIGN_RIGHT, DT.Rows[i]["TotalEmployerECSSS"].ToString(), 557, txtheight2, 0);
                            cb.EndText();

                            cb.BeginText();
                            //text = DT.Rows[i]["EmployeePHealth"].ToString();
                            // put the alignment and coordinates here
                            cb.ShowTextAligned(Element.ALIGN_RIGHT, "Total SSS:", 507, txtheight2 - 10, 0);
                            cb.EndText();

                            cb.BeginText();
                            //text = DT.Rows[i]["EmployeePHealth"].ToString();
                            // put the alignment and coordinates here
                            cb.ShowTextAligned(Element.ALIGN_RIGHT, DT.Rows[i]["FinalTotalSSS"].ToString(), 557, txtheight2 - 10, 0);
                            cb.EndText();

                            cb.BeginText();
                            //text = DT.Rows[i]["EmployeePHealth"].ToString();
                            // put the alignment and coordinates here
                            cb.ShowTextAligned(Element.ALIGN_RIGHT, DT.Rows[i]["TotalEmployeePhilHealth"].ToString(), 608, txtheight2, 0);
                            cb.EndText();

                            cb.BeginText();
                            //text = DT.Rows[i]["EmployeePHealth"].ToString();
                            // put the alignment and coordinates here
                            cb.ShowTextAligned(Element.ALIGN_RIGHT, DT.Rows[i]["TotalEmployerPhilHealth"].ToString(), 660, txtheight2, 0);
                            cb.EndText();

                            cb.BeginText();
                            //text = DT.Rows[i]["EmployeePHealth"].ToString();
                            // put the alignment and coordinates here
                            cb.ShowTextAligned(Element.ALIGN_RIGHT, "Total PH:", 608, txtheight2 - 10, 0);
                            cb.EndText();

                            cb.BeginText();
                            //text = DT.Rows[i]["EmployeePHealth"].ToString();
                            // put the alignment and coordinates here
                            cb.ShowTextAligned(Element.ALIGN_RIGHT, DT.Rows[i]["FinalTotalPhilHealth"].ToString(), 660, txtheight2 - 10, 0);
                            cb.EndText();


                            cb.BeginText();
                            //text = DT.Rows[i]["EmployeePHealth"].ToString();
                            // put the alignment and coordinates here
                            cb.ShowTextAligned(Element.ALIGN_RIGHT, DT.Rows[i]["TotalPagIbigAmount"].ToString(), 712, txtheight2, 0);
                            cb.EndText();

                            cb.BeginText();
                            //text = DT.Rows[i]["EmployeePHealth"].ToString();
                            // put the alignment and coordinates here
                            cb.ShowTextAligned(Element.ALIGN_RIGHT, DT.Rows[i]["TotalPagibigERAmount"].ToString(), 761, txtheight2, 0);
                            cb.EndText();

                            cb.BeginText();
                            //text = DT.Rows[i]["EmployeePHealth"].ToString();
                            // put the alignment and coordinates here
                            cb.ShowTextAligned(Element.ALIGN_RIGHT, "Total HDMF:", 712, txtheight2 - 10, 0);
                            cb.EndText();

                            cb.BeginText();
                            //text = DT.Rows[i]["EmployeePHealth"].ToString();
                            // put the alignment and coordinates here
                            cb.ShowTextAligned(Element.ALIGN_RIGHT, DT.Rows[i]["FinalTotalPagibig"].ToString(), 761, txtheight2 - 10, 0);
                            cb.EndText();




                        }

                        cb.BeginText();
                        text = DT.Rows[i]["Empid"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(0, text, 30, txtheight, 0);
                        cb.EndText();

                        cb.BeginText();
                        text = DT.Rows[i]["SSSnum"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(0, text, 92, txtheight, 0);
                        cb.EndText();

                        cb.BeginText();
                        text = DT.Rows[i]["HDMF"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(0, text, 156, txtheight, 0);
                        cb.EndText();

                        cb.BeginText();
                        text = DT.Rows[i]["EmployeeName"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(0, text.Length > 17 ? text.Remove(17) : text, 233, txtheight, 0);
                        cb.EndText();

                        cb.BeginText();
                        text = DT.Rows[i]["SSSPremEmployee"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(Element.ALIGN_RIGHT, text, 447, txtheight, 0);
                        cb.EndText();

                        cb.BeginText();
                        text = DT.Rows[i]["SSSPremEcEmployer"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(Element.ALIGN_RIGHT, text, 507, txtheight, 0);
                        cb.EndText();

                        cb.BeginText();
                        text = DT.Rows[i]["EmployerEC"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(Element.ALIGN_RIGHT, text, 557, txtheight, 0);
                        cb.EndText();

                        cb.BeginText();
                        text = DT.Rows[i]["EmployeePhilHealth"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(Element.ALIGN_RIGHT, text, 608, txtheight, 0);
                        cb.EndText();

                        cb.BeginText();
                        text = DT.Rows[i]["EmployerPhilHealth"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(Element.ALIGN_RIGHT, text, 660, txtheight, 0);
                        cb.EndText();

                        cb.BeginText();
                        text = DT.Rows[i]["PagIbigAmount"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(Element.ALIGN_RIGHT, text, 712, txtheight, 0);
                        cb.EndText();

                        cb.BeginText();
                        text = DT.Rows[i]["PagibigERAmount"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(Element.ALIGN_RIGHT, text, 761, txtheight, 0);
                        cb.EndText();


                        if (i % 4 == 0)
                        {
                            int decheight = txtheight - 11;
                            txtheight = decheight;
                        }
                        else if (i % 5 == 0)
                        {
                            int decheight = txtheight - 10;
                            txtheight = decheight;
                        }
                        else if (i % 7 == 0)
                        {
                            int decheight = txtheight - 11;
                            txtheight = decheight;
                        }
                        else
                        {
                            int decheight = txtheight - 12;
                            txtheight = decheight;
                        }
                    }
                }
                // close the streams and voilá the file should be changed :)
                document.Close();
                fs.Close();
                writer.Close();
                reader.Close();


                string returnPath = "https://ws.durusthr.com/ILM_WS_Live/sFTP/PayrollOutsourcing/" + parameter.tenant_code.ToUpper() + "_SSPHHD_" + parameter.year + parameter.month + ".pdf";
                return returnPath;
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }

        [HttpPost]
        [Route("pwhtaxmgen_aanya")]
        public string PWHTAXList(ReportParameter parameter)
        {
            try
            {
                GetConnection(parameter.series_code);

                if (TenantCodeChecker(parameter.tenant_code)) return errMSGnoTenantCode;

                Connection con = new Connection();
                DataTable DT = new DataTable();
                con.myparameters.Add(new myParameters { ParameterName = "@code", mytype = SqlDbType.NVarChar, Value = parameter.tenant_code });
                con.myparameters.Add(new myParameters { ParameterName = "@month", mytype = SqlDbType.NVarChar, Value = parameter.month });
                con.myparameters.Add(new myParameters { ParameterName = "@year", mytype = SqlDbType.NVarChar, Value = parameter.year });
                DT = con.GetDataTable("sp_GetPWHTAXM");

                if (DT.Rows.Count < 1) return errMSGnoData;

                string x = ConfigurationManager.ConnectionStrings["connectionstring"].ToString();
                List<PWHTAXMList> ListReturned = new List<PWHTAXMList>();

                //return ListReturned;
                string oldFile = HttpContext.Current.Server.MapPath("~/Files/PayrollOutSourcing/PDFTemplate/") + "WHTAXM" + ".pdf";
                //string newFile = HttpContext.Current.Server.MapPath("~/Files/PayrollOutSourcing/PDFTemplate/") + "WHTAXM v2" + ".pdf";
                string newFile = HttpContext.Current.Server.MapPath("~/sFTP/PayrollOutsourcing/") + parameter.tenant_code.ToUpper() + "_WHTAXM_" + parameter.year + parameter.month + ".pdf";

                PdfReader pdfReader = new PdfReader(oldFile);
                PdfStamper pdfStamper = new PdfStamper(pdfReader, new FileStream(newFile, FileMode.Create));
                pdfStamper.Close();

                // open the reader
                PdfReader reader = new PdfReader(oldFile);
                Rectangle size = reader.GetPageSizeWithRotation(1);
                Document document = new Document(size);

                // open the writer
                FileStream fs = new FileStream(newFile, FileMode.Create, FileAccess.Write);
                PdfWriter writer = PdfWriter.GetInstance(document, fs);
                document.Open();

                // the pdf content
                PdfContentByte cb = writer.DirectContent;

                // select the font properties
                BaseFont bf = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                cb.SetColorFill(BaseColor.DARK_GRAY);
                cb.SetFontAndSize(bf, 8);

                PdfImportedPage page = writer.GetImportedPage(reader, 1);
                cb.AddTemplate(page, 0, 0);


                cb.BeginText();
                //text = DateTime.Today;
                // put the alignment and coordinates here
                cb.ShowTextAligned(0, DateTime.Now.ToString("MM-dd-yyyy"), 50, 750, 0);
                cb.EndText();

                cb.BeginText();
                //text = DT.Rows[i]["CompanyName"].ToString();
                // put the alignment and coordinates here
                cb.ShowTextAligned(0, DT.Rows[0]["CompanyName"].ToString(), 225, 748, 0);
                cb.EndText();

                cb.BeginText();
                //text = DT.Rows[i]["CompanyName"].ToString();
                // put the alignment and coordinates here
                cb.ShowTextAligned(0, DT.Rows[0]["startdate"].ToString() + " " + "TO", 345, 725, 0);
                cb.EndText();

                cb.BeginText();
                //text = DT.Rows[i]["CompanyName"].ToString();
                // put the alignment and coordinates here
                cb.ShowTextAligned(0, DT.Rows[0]["enddate"].ToString(), 403, 725, 0);
                cb.EndText();

                int NOOFDATA = DT.Rows.Count;
                Double pages = NOOFDATA / 76;
                //Double finalnoofpages = 1;
                //Double baseno = 1.000;
                int txtheight = 668;
                int txtheight2 = 100;
                int baseno2 = 43;
                //int baseno = 00000;

                int numb1 = DT.Rows.Count - 1;
                var couterr = numb1.ToString();

                string text = "";

                for (int i = 0; i < NOOFDATA; i++)
                {
                    var counters = i.ToString();
                    PWHTAXMList Get = new PWHTAXMList();
                    Get.empid = DT.Rows[i]["Empid"].ToString();
                    Get.empname = DT.Rows[i]["EmployeeName"].ToString();
                    Get.companycode = DT.Rows[i]["CompanyCode"].ToString();
                    Get.companyname = DT.Rows[i]["CompanyName"].ToString();
                    Get.taxstatus = DT.Rows[i]["TaxStatus"].ToString();
                    Get.tinnum = DT.Rows[i]["TINnum"].ToString();
                    Get.whtax = DT.Rows[i]["WHTax"].ToString();
                    Get.startdate = DT.Rows[i]["StartDate"].ToString();
                    Get.enddate = DT.Rows[i]["EndDate"].ToString();
                    Get.finaltotal = DT.Rows[i]["FinalTotal"].ToString();
                    //Get.payoutdate = DT.Rows[i]["PayoutDate"].ToString();
                    ListReturned.Add(Get);


                    if (i < baseno2)
                    {
                        //string text = "";
                        // put the alignment and coordinates here
                        if (counters == couterr)
                        {
                            cb.BeginText();
                            //text = DT.Rows[i]["Empid"].ToString();
                            // put the alignment and coordinates here
                            cb.ShowTextAligned(0, "Totals:", 75, txtheight2, 0);
                            cb.EndText();

                            cb.BeginText();
                            //text = DT.Rows[i]["Empid"].ToString();
                            // put the alignment and coordinates here
                            cb.ShowTextAligned(0, "Totals:", 132, txtheight2, 0);
                            cb.EndText();
                            cb.BeginText();
                            //text = DT.Rows[i]["Empid"].ToString();
                            // put the alignment and coordinates here
                            cb.ShowTextAligned(0, "Totals:", 132, txtheight2, 0);
                            cb.EndText();

                            cb.BeginText();
                            //text = DT.Rows[i]["EmployeePHealth"].ToString();
                            // put the alignment and coordinates here
                            cb.ShowTextAligned(0, "" + DT.Rows.Count, 360, txtheight2, 0);
                            cb.EndText();


                            cb.BeginText();
                            //text = DT.Rows[i]["EmployerPHealth"].ToString();
                            // put the alignment and coordinates here
                            cb.ShowTextAligned(0, DT.Rows[0]["FinalTotal"].ToString(), 458, txtheight2, 0);
                            cb.EndText();


                        }

                        cb.BeginText();
                        text = DT.Rows[i]["Empid"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(0, text, 75, txtheight, 0);
                        cb.EndText();

                        cb.BeginText();
                        text = DT.Rows[i]["EmployeeName"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(0, text.Length > 17 ? text.Remove(17) : text, 132, txtheight, 0);
                        cb.EndText();

                        cb.BeginText();
                        text = DT.Rows[i]["TaxStatus"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(0, text, 300, txtheight, 0);
                        cb.EndText();

                        cb.BeginText();
                        text = DT.Rows[i]["TINnum"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(0, text, 365, txtheight, 0);
                        cb.EndText();

                        cb.BeginText();
                        text = DT.Rows[i]["WHTax"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(0, text, 458, txtheight, 0);
                        cb.EndText();


                        if (i % 3 == 0)
                        {
                            int decheight = txtheight - 13;
                            txtheight = decheight;
                        }
                        else if (i % 5 == 0)
                        {
                            int decheight = txtheight - 14;
                            txtheight = decheight;
                        }
                        else
                        {
                            int decheight = txtheight - 13;
                            txtheight = decheight;
                        }

                    }
                    else if ((i % baseno2) == 0)
                    {
                        txtheight = 668;
                        document.NewPage();
                        cb.SetColorFill(BaseColor.DARK_GRAY);
                        cb.SetFontAndSize(bf, 8);
                        //PdfImportedPage page = writer.GetImportedPage(reader, 2);
                        PdfImportedPage page2 = writer.GetImportedPage(reader, 1);
                        cb.AddTemplate(page2, 0, 0);
                        //BaseFont bf = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                        //string text = "";
                        // put the alignment and coordinates here

                        if (counters == couterr)
                        {
                            cb.BeginText();
                            //text = DT.Rows[i]["Empid"].ToString();
                            // put the alignment and coordinates here
                            cb.ShowTextAligned(0, "Totals:", 75, txtheight2, 0);
                            cb.EndText();

                            cb.BeginText();
                            //text = DT.Rows[i]["Empid"].ToString();
                            // put the alignment and coordinates here
                            cb.ShowTextAligned(0, "Totals:", 132, txtheight2, 0);
                            cb.EndText();
                            cb.BeginText();
                            //text = DT.Rows[i]["Empid"].ToString();
                            // put the alignment and coordinates here
                            cb.ShowTextAligned(0, "Totals:", 132, txtheight2, 0);
                            cb.EndText();

                            cb.BeginText();
                            //text = DT.Rows[i]["EmployeePHealth"].ToString();
                            // put the alignment and coordinates here
                            cb.ShowTextAligned(0, "" + DT.Rows.Count, 360, txtheight2, 0);
                            cb.EndText();


                            cb.BeginText();
                            //text = DT.Rows[i]["EmployerPHealth"].ToString();
                            // put the alignment and coordinates here
                            cb.ShowTextAligned(0, DT.Rows[0]["FinalTotal"].ToString(), 458, txtheight2, 0);
                            cb.EndText();


                        }

                        cb.BeginText();
                        //text = DateTime.Today;
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(0, DateTime.Now.ToString("MM-dd-yyyy"), 50, 750, 0);
                        cb.EndText();

                        cb.BeginText();
                        //text = DT.Rows[i]["CompanyName"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(0, DT.Rows[0]["CompanyName"].ToString(), 225, 748, 0);
                        cb.EndText();

                        cb.BeginText();
                        //text = DT.Rows[i]["CompanyName"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(0, DT.Rows[0]["startdate"].ToString() + " " + "TO", 345, 725, 0);
                        cb.EndText();

                        cb.BeginText();
                        //text = DT.Rows[i]["CompanyName"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(0, DT.Rows[0]["enddate"].ToString(), 403, 725, 0);
                        cb.EndText();

                        cb.BeginText();
                        text = DT.Rows[i]["Empid"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(0, text, 75, txtheight, 0);
                        cb.EndText();

                        cb.BeginText();
                        text = DT.Rows[i]["EmployeeName"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(0, text.Length > 17 ? text.Remove(17) : text, 132, txtheight, 0);
                        cb.EndText();

                        cb.BeginText();
                        text = DT.Rows[i]["TaxStatus"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(0, text, 300, txtheight, 0);
                        cb.EndText();

                        cb.BeginText();
                        text = DT.Rows[i]["TINnum"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(0, text, 365, txtheight, 0);
                        cb.EndText();

                        cb.BeginText();
                        text = DT.Rows[i]["WHTax"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(0, text, 458, txtheight, 0);
                        cb.EndText();


                        if (i % 3 == 0)
                        {
                            int decheight = txtheight - 13;
                            txtheight = decheight;
                        }
                        else if (i % 5 == 0)
                        {
                            int decheight = txtheight - 14;
                            txtheight = decheight;
                        }
                        else
                        {
                            int decheight = txtheight - 13;
                            txtheight = decheight;
                        }
                    }
                    else
                    {


                        //string text = "";
                        // put the alignment and coordinates here

                        if (counters == couterr)
                        {
                            cb.BeginText();
                            //text = DT.Rows[i]["Empid"].ToString();
                            // put the alignment and coordinates here
                            cb.ShowTextAligned(0, "Totals:", 75, txtheight2, 0);
                            cb.EndText();

                            cb.BeginText();
                            //text = DT.Rows[i]["Empid"].ToString();
                            // put the alignment and coordinates here
                            cb.ShowTextAligned(0, "Totals:", 132, txtheight2, 0);
                            cb.EndText();
                            cb.BeginText();
                            //text = DT.Rows[i]["Empid"].ToString();
                            // put the alignment and coordinates here
                            cb.ShowTextAligned(0, "Totals:", 132, txtheight2, 0);
                            cb.EndText();

                            cb.BeginText();
                            //text = DT.Rows[i]["EmployeePHealth"].ToString();
                            // put the alignment and coordinates here
                            cb.ShowTextAligned(0, "" + DT.Rows.Count, 360, txtheight2, 0);
                            cb.EndText();


                            cb.BeginText();
                            //text = DT.Rows[i]["EmployerPHealth"].ToString();
                            // put the alignment and coordinates here
                            cb.ShowTextAligned(0, DT.Rows[0]["FinalTotal"].ToString(), 458, txtheight2, 0);
                            cb.EndText();


                        }

                        cb.BeginText();
                        text = DT.Rows[i]["Empid"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(0, text, 75, txtheight, 0);
                        cb.EndText();

                        cb.BeginText();
                        text = DT.Rows[i]["EmployeeName"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(0, text.Length > 17 ? text.Remove(17) : text, 132, txtheight, 0);
                        cb.EndText();

                        cb.BeginText();
                        text = DT.Rows[i]["TaxStatus"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(0, text, 300, txtheight, 0);
                        cb.EndText();

                        cb.BeginText();
                        text = DT.Rows[i]["TINnum"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(0, text, 365, txtheight, 0);
                        cb.EndText();

                        cb.BeginText();
                        text = DT.Rows[i]["WHTax"].ToString();
                        // put the alignment and coordinates here
                        cb.ShowTextAligned(0, text, 458, txtheight, 0);
                        cb.EndText();


                        if (i % 3 == 0)
                        {
                            int decheight = txtheight - 13;
                            txtheight = decheight;
                        }
                        else if (i % 5 == 0)
                        {
                            int decheight = txtheight - 14;
                            txtheight = decheight;
                        }
                        else
                        {
                            int decheight = txtheight - 13;
                            txtheight = decheight;
                        }
                    }
                }

                // close the streams and voilá the file should be changed :)
                document.Close();
                fs.Close();
                writer.Close();
                reader.Close();

                string returnPath = "https://ws.durusthr.com/ILM_WS_Live/sFTP/PayrollOutsourcing/" + parameter.tenant_code.ToUpper() + "_WHTAXM_" + parameter.year + parameter.month + ".pdf"; ;
                return returnPath;
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }

        [HttpPost]
        [Route("PO_LoansReport_aanya")]
        public string PO_Extract_Loans(ReportParameter parameter)
        {
            FileStream stream = null;
            try
            {
                GetConnection(parameter.series_code);

                if (TenantCodeChecker(parameter.tenant_code)) return errMSGnoTenantCode;

                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@TenantCode", mytype = SqlDbType.NVarChar, Value = parameter.tenant_code });
                con.myparameters.Add(new myParameters { ParameterName = "@Month", mytype = SqlDbType.NVarChar, Value = parameter.month });
                con.myparameters.Add(new myParameters { ParameterName = "@Year", mytype = SqlDbType.NVarChar, Value = parameter.year });
                DataTable LR = con.GetDataTable("sp_Extract_Loans");

                if (LR.Rows.Count < 1) return errMSGnoData;

                string dataDir = HostingEnvironment.MapPath("~/Files/PayrollOutSourcing/");
                stream = new FileStream(dataDir + "LoansReport.xls", FileMode.Open);
                string filename;
                string datebuild = DateTime.Now.ToString("MMddyyyy");
                string timebuild = DateTime.Now.ToString("hhmmtt");

                //filename = datebuild + timebuild + "_PEZAReport" + ".xls";
                filename = parameter.tenant_code + "_LoansReport" + "_" + parameter.year + parameter.month + "_" + datebuild + "_" + timebuild + ".xls";
                using (var fileStream = new FileStream(dataDir + filename, FileMode.Create, FileAccess.Write))
                //using (var fileStream = new FileStream(dataDir + filename, FileMode.Create, FileAccess.Write))
                {
                    stream.CopyTo(fileStream);
                }
                stream.Close();
                ExcelWorkbookFormat workbookFormat = ExcelWorkbookFormat.Xls_2003;
                string testDocFile = dataDir + "LoansReport.xls";
                ExcelWorkbook workbook = new ExcelWorkbook(testDocFile);
                ExcelWorksheet firstWorksheet = workbook.Worksheets[0];
                ExcelWorksheet LOANS = workbook.Worksheets[0];
                int x = 2;
                for (int i = 0; i < LR.Rows.Count; i++, x++)
                {
                    LOANS["A" + x].Text = LR.Rows[i]["PayrollNum"].ToString();
                    LOANS["B" + x].Text = LR.Rows[i]["PayDate"].ToString();
                    LOANS["C" + x].Text = LR.Rows[i]["PayCode"].ToString();
                    LOANS["D" + x].Text = LR.Rows[i]["EmployeeID"].ToString();
                    LOANS["E" + x].Text = LR.Rows[i]["LastName"].ToString();
                    LOANS["F" + x].Text = LR.Rows[i]["FirstName"].ToString();
                    LOANS["G" + x].Text = LR.Rows[i]["FileStatus"].ToString();
                    LOANS["H" + x].Text = LR.Rows[i]["LoanCode"].ToString();
                    LOANS["I" + x].Text = LR.Rows[i]["LoanDateStart"].ToString();
                    LOANS["J" + x].Value = LR.Rows[i]["LoanPrincipal"];
                    LOANS["K" + x].Value = LR.Rows[i]["LoanAmount"];
                    LOANS["L" + x].Value = LR.Rows[i]["LoanPayments"];
                    LOANS["M" + x].Value = LR.Rows[i]["LoanBalance"];
                    LOANS["N" + x].Value = LR.Rows[i]["LoanDeductionAMT"];
                    LOANS["O" + x].Text = LR.Rows[i]["LoanDeductionFreq"].ToString();
                    LOANS["P" + x].Text = LR.Rows[i]["LoanStatus"].ToString();
                    LOANS["Q" + x].Text = LR.Rows[i]["LoanDateGranted"].ToString();
                    LOANS["R" + x].Text = LR.Rows[i]["From"].ToString();
                    LOANS["S" + x].Text = LR.Rows[i]["To"].ToString();
                    LOANS["T" + x].Text = LR.Rows[i]["JECostCenter"].ToString();
                }
                HttpResponse httpResponse = System.Web.HttpContext.Current.Response;
                httpResponse.Clear();
                httpResponse.ContentType = "Application/x-msexcel";
                httpResponse.AddHeader("Content-Disposition", String.Format("attachment; filename={0}", filename));
                string newDIR = HttpContext.Current.Server.MapPath("~/sFTP/PayrollOutsourcing/");
                string onlineDIR = "http://ws.durusthr.com/ILM_WS_Live/sFTP/PayrollOutsourcing/";
                //filename = filename.Replace(',', '_');
                //filename = filename.Replace(" ", String.Empty);
                workbook.Save(newDIR + filename);
                stream.Close();
                //return "Success";
                if (LR.Rows.Count > 0)
                {
                    return onlineDIR + filename;
                }
                else
                {
                    return "No Rows";
                }
            }
            catch (Exception e)
            {
                stream.Close();
                return "Error: " + e.ToString();
            }
        }

        #endregion

        #region Methods
        private bool TenantCodeChecker(string tenant_code)
        {
            bool result = false;

            if (tenant_code == "" || tenant_code == null)
            {
                result = true;
            }

            return result;
        }

        private DataTable GetCompanyInfo(string Company, string db)
        {
            GetConnection(db);
            Connection con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@Company", mytype = SqlDbType.VarChar, Value = Company });
            DataTable dt = con.GetDataTable("sp_getCompanyInfo");
            return dt;
        }
        private DataTable GetCompanySignatory(string Company, string db, string ID)
        {
            GetConnection(db);
            Connection con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@TenantID", mytype = SqlDbType.VarChar, Value = Company });
            con.myparameters.Add(new myParameters { ParameterName = "@ReportID", mytype = SqlDbType.VarChar, Value = ID });
            DataTable dt = con.GetDataTable("sp_getSignatory");
            return dt;
        }

        public string UPdate2316(Update2316 param)
        {
            try
            {
                GetConnection(param.db);
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = param.EmpID });
                con.myparameters.Add(new myParameters { ParameterName = "@tenantID", mytype = SqlDbType.VarChar, Value = param.TenantID });
                string userID = con.ExecuteScalar("sp_GetUserID_Payslip");

                DataTable DT = new DataTable();
                DT.Columns.AddRange(new DataColumn[1]
                {
                    new DataColumn("EmpID")
                });
                DT.Rows.Add(userID);

                GetConnection(param.db);
                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@UserId", mytype = SqlDbType.Structured, Value = DT });
                con.myparameters.Add(new myParameters { ParameterName = "@ForYear", mytype = SqlDbType.VarChar, Value = param.Year });
                con.ExecuteNonQuery("sp_ComputeActiveEmployeeITR_TX");
                return "Success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }
        #endregion

        #region Test Region
        [HttpPost]
        [Route("TestAanyaConnection")]
        public string TestConnection()
        {
            string result = "success";

            try
            {
                GetConnection("0001");
            }
            catch (Exception e)
            {
                result = "failed";
                throw;
            }

            return result;
        }

        [HttpPost]
        [Route("TestDataEncryption")]
        public string TestDataEncryption(string data_text)
        {
            return Crypto.url_encrypt(data_text);
        }

        [HttpPost]
        [Route("TestDataDecryption")]
        public string TestDataDecryption(string data_text)
        {
            return Crypto.url_decrypt(data_text);
        }
        #endregion
    }
}