﻿using System;
using System.Web.Http;
using illimitadoWepAPI.Models.PayrollOutsourcingModel;
using System.Data;
using illimitadoWepAPI.MyClass;
using System.Configuration;
using System.Reflection;
using System.Collections.Generic;
using ExpertXls.ExcelLib;
using System.IO;
using System.Web.Hosting;
using static illimitadoWepAPI.Models.AlphaListModel;
using static illimitadoWepAPI.Models.UserProfile;
using System.Web;
using System.Net.Mail;
using System.Net;
using iTextSharp.text.pdf;
using iTextSharp.text;
using System.Globalization;
using System.Net.Http;
using System.Text.RegularExpressions;
using Excel = Microsoft.Office.Interop.Excel;
using System.Runtime.InteropServices;
using System.Diagnostics;
using illimitadoWepAPI.Models;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using illimitadoWepAPI.Models.ScheduleDemoModels;
using PdfSharp.Pdf.Security;
using PdfSharp.Pdf.IO;
using PdfSharp.Pdf;
using System.IO.Compression;
using System.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

#region Test Only 
using System.Data.SqlClient;
#endregion

namespace illimitadoWepAPI.Controllers.PayrollOutsourcingController
{
    public class PayrollOutsourcingController : ApiController
    {
        protected CrystalDecisions.Web.CrystalReportViewer CrystalReportViewer1;
        public void TempConnectionString()
        {
            var settings = ConfigurationManager.ConnectionStrings["connectionstring"];
            var fi = typeof(ConfigurationElement).GetField(
                          "_bReadOnly",
                          BindingFlags.Instance | BindingFlags.NonPublic);
            fi.SetValue(settings, false);
            //--Local--//
            //settings.ConnectionString = "Data Source=192.168.2.33,1444;Database=CRT_Dbase2019-03-29;User Id=DevUser;Password=securedmgt@1230;";
            //--AWS--//
            settings.ConnectionString = "Data Source=livedb-ilm.c3srcolvclfy.ap-southeast-1.rds.amazonaws.com,1412; Database = OP360DB;User Id=ILM_LiveDBUser;Password=ILMV3ry53cur3dMGTpa55w0rd2020;";

        }
        public void TempConnectionStringv2()
        {
            var settings = ConfigurationManager.ConnectionStrings["connectionstring"];
            var fi = typeof(ConfigurationElement).GetField(
                          "_bReadOnly",
                          BindingFlags.Instance | BindingFlags.NonPublic);
            fi.SetValue(settings, false);
            //--Local--//
            //settings.ConnectionString = "Data Source=192.168.2.33,1444;Database=CRT_Dbase2019-03-29;User Id=DevUser;Password=securedmgt@1230;";
            //--AWS--//

            settings.ConnectionString = "Data Source=livedb-ilm.c3srcolvclfy.ap-southeast-1.rds.amazonaws.com,1412; Database = DurustHRdb1;User Id=ILM_LiveDBUser;Password=ILMV3ry53cur3dMGTpa55w0rd2020;";


        }
        public void TempConnectionStringv4(string db)
        {
            var settings = ConfigurationManager.ConnectionStrings["connectionstring"];
            var fi = typeof(ConfigurationElement).GetField(
                          "_bReadOnly",
                          BindingFlags.Instance | BindingFlags.NonPublic);
            fi.SetValue(settings, false);
            //--Local--//
            //settings.ConnectionString = "Data Source=192.168.2.33,1444;Database=CRT_Dbase2019-03-29;User Id=DevUser;Password=securedmgt@1230;";
            //--AWS--//
            if (db == "crawford")
            {
                settings.ConnectionString = "Data Source=livedb-ilm.c3srcolvclfy.ap-southeast-1.rds.amazonaws.com,1412; Database = DurustHRdb1;User Id=ILM_LiveDBUser;Password=ILMV3ry53cur3dMGTpa55w0rd2020;";
            }
            else if (db == "legacy")
            {
                settings.ConnectionString = "Data Source=livedb-ilm.c3srcolvclfy.ap-southeast-1.rds.amazonaws.com,1412; Database = LegacyPayrollDB;User Id=ILM_LiveDBUser;Password=ILMV3ry53cur3dMGTpa55w0rd2020;";
            }
            else
            {
                string dbase = db + "_LegacyPayrollDB;";
                settings.ConnectionString = "Data Source=165.192.96.2,1433; Database = " + dbase + ";User Id=sa;Password=Twozerotwoone@cancer;";
            }
        }
        public void TempConnectionStringv3()
        {
            var settings = ConfigurationManager.ConnectionStrings["connectionstring"];
            var fi = typeof(ConfigurationElement).GetField(
                          "_bReadOnly",
                          BindingFlags.Instance | BindingFlags.NonPublic);
            fi.SetValue(settings, false);
            //--Local--//
            //settings.ConnectionString = "Data Source=192.168.2.33,1444;Database=CRT_Dbase2019-03-29;User Id=DevUser;Password=securedmgt@1230;";
            //--AWS--//
            settings.ConnectionString = "Data Source=livedb-ilm.c3srcolvclfy.ap-southeast-1.rds.amazonaws.com,1412; Database = DurustHRdb2;User Id=ILM_LiveDBUser;Password=ILMV3ry53cur3dMGTpa55w0rd2020;";

        }
        #region Alternate version
        [HttpPost]
        [Route("PO_ImportTimeAttendance_Alt")]
        public string PO_ImportTimeAttendance_Alt(TimeAttendanceAlt timeAttendanceAlt)
        {
            try
            {
                DataTable importList = new DataTable();
                importList.Columns.AddRange(new DataColumn[43] {
                    new DataColumn("Emp_ID"),
                    new DataColumn("First_Name"),
                    new DataColumn("Last_Name"),
                    new DataColumn("Reg_Days_Work"),
                    new DataColumn("Tardy_Hrs"),
                    new DataColumn("Undertime_Hrs"),
                    new DataColumn("Absent_Days"),
                    new DataColumn("LWOP_Days"),
                    new DataColumn("VL_Days"),
                    new DataColumn("SL_Days"),
                    new DataColumn("Other_Leaves_Days"),
                    new DataColumn("Night_Diff"),
                    new DataColumn("Regular_OT"),
                    new DataColumn("Regular_OT_NP"),
                    new DataColumn("Legal_Hol_OT"),
                    new DataColumn("Legal_Hol_OT_NP"),
                    new DataColumn("Legal_Hol_OT_EHrs"),
                    new DataColumn("Legal_Hol_OT_EHrs_NP"),
                    new DataColumn("Legal_Hol_RD"),
                    new DataColumn("Legal_Hol_RD_NP"),
                    new DataColumn("Legal_Hol_RD_EHrs"),
                    new DataColumn("Legal_Hol_RD_EHrs_NP"),
                    new DataColumn("Special_Hol_OT"),
                    new DataColumn("Special_Hol_OT_NP"),
                    new DataColumn("Special_Hol_OT_EHrs"),
                    new DataColumn("Special_Hol_OT_Ehrs_NP"),
                    new DataColumn("Special_Hol_RD"),
                    new DataColumn("Special_Hol_RD_NP"),
                    new DataColumn("Special_Hol_RD_EHrs"),
                    new DataColumn("Special_Hol_RD_EHrs_NP"),
                    new DataColumn("RestDay_OT"),
                    new DataColumn("RestDay_OT_NP"),
                    new DataColumn("RestDay_OT_EHrs"),
                    new DataColumn("RestDay_OT_EHrs_NP"),
                    new DataColumn("Double_Hol_OT"),
                    new DataColumn("Double_Hol_OT_NP"),
                    new DataColumn("Double_Hol_OT_EHrs"),
                    new DataColumn("Double_Hol_OT_EHrs_NP"),
                    new DataColumn("Double_Hol_RD"),
                    new DataColumn("Double_Hol_RD_NP"),
                    new DataColumn("Double_Hol_RD_EHrs"),
                    new DataColumn("Double_Hol_RD_EHrs_NP"),
                    new DataColumn("Remarks")});
                for (int i = 0; i < timeAttendanceAlt.Emp_ID.Length; i++)
                {
                    importList.Rows.Add(
                        timeAttendanceAlt.Emp_ID[i],
                        timeAttendanceAlt.First_Name[i],
                        timeAttendanceAlt.Last_Name[i],
                        timeAttendanceAlt.Reg_Days_Work[i],
                        timeAttendanceAlt.Tardy_Hrs[i],
                        timeAttendanceAlt.Undertime_Hrs[i],
                        timeAttendanceAlt.Absent_Days[i],
                        timeAttendanceAlt.LWOP_Days[i],
                        timeAttendanceAlt.VL_Days[i],
                        timeAttendanceAlt.SL_Days[i],
                        timeAttendanceAlt.Other_Leaves_Days[i],
                        timeAttendanceAlt.Night_Diff[i],
                        timeAttendanceAlt.Regular_OT[i],
                        timeAttendanceAlt.Regular_OT_NP[i],
                        timeAttendanceAlt.Legal_Hol_OT[i],
                        timeAttendanceAlt.Legal_Hol_OT_NP[i],
                        timeAttendanceAlt.Legal_Hol_OT_EHrs[i],
                        timeAttendanceAlt.Legal_Hol_OT_EHrs_NP[i],
                        timeAttendanceAlt.Legal_Hol_RD[i],
                        timeAttendanceAlt.Legal_Hol_RD_NP[i],
                        timeAttendanceAlt.Legal_Hol_RD_EHrs[i],
                        timeAttendanceAlt.Legal_Hol_RD_EHrs_NP[i],
                        timeAttendanceAlt.Special_Hol_OT[i],
                        timeAttendanceAlt.Special_Hol_OT_NP[i],
                        timeAttendanceAlt.Special_Hol_OT_EHrs[i],
                        timeAttendanceAlt.Special_Hol_OT_Ehrs_NP[i],
                        timeAttendanceAlt.Special_Hol_RD[i],
                        timeAttendanceAlt.Special_Hol_RD_NP[i],
                        timeAttendanceAlt.Special_Hol_RD_EHrs[i],
                        timeAttendanceAlt.Special_Hol_RD_EHrs_NP[i],
                        timeAttendanceAlt.RestDay_OT[i],
                        timeAttendanceAlt.RestDay_OT_NP[i],
                        timeAttendanceAlt.RestDay_OT_EHrs[i],
                        timeAttendanceAlt.RestDay_OT_EHrs_NP[i],
                        timeAttendanceAlt.Double_Hol_OT[i],
                        timeAttendanceAlt.Double_Hol_OT_NP[i],
                        timeAttendanceAlt.Double_Hol_OT_EHrs[i],
                        timeAttendanceAlt.Double_Hol_OT_EHrs_NP[i],
                        timeAttendanceAlt.Double_Hol_RD[i],
                        timeAttendanceAlt.Double_Hol_RD_NP[i],
                        timeAttendanceAlt.Double_Hol_RD_EHrs[i],
                        timeAttendanceAlt.Double_Hol_RD_EHrs_NP[i],
                        timeAttendanceAlt.Remarks[i]);
                }
                TempConnectionStringv4(timeAttendanceAlt.db);
                Connection con = new Connection();

                con.myparameters.Add(new myParameters { ParameterName = "@ClientCode", mytype = SqlDbType.VarChar, Value = timeAttendanceAlt.ClientCode });
                con.myparameters.Add(new myParameters { ParameterName = "@StartDate", mytype = SqlDbType.VarChar, Value = timeAttendanceAlt.StartDate });
                con.myparameters.Add(new myParameters { ParameterName = "@EndDate", mytype = SqlDbType.VarChar, Value = timeAttendanceAlt.EndDate });
                con.myparameters.Add(new myParameters { ParameterName = "@Data", mytype = SqlDbType.Structured, Value = importList });
                con.myparameters.Add(new myParameters { ParameterName = "@LoginID", mytype = SqlDbType.VarChar, Value = timeAttendanceAlt.LoginID });
                con.myparameters.Add(new myParameters { ParameterName = "@PayoutType", mytype = SqlDbType.VarChar, Value = timeAttendanceAlt.PayoutType });
                con.myparameters.Add(new myParameters { ParameterName = "@ifAdj", mytype = SqlDbType.VarChar, Value = timeAttendanceAlt.ifAdj });
                con.ExecuteNonQuery("sp_PayrollOutsourcing_Import_TimeAttendance");
                return "success";
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }
        [HttpPost]
        [Route("PO_ImportIncomeDeductions_Alt")]
        public string PO_ImportIncomeDeductions_Alt(IncomeDeductionAlt incomeDeductionAlt)
        {
            try
            {
                DataTable importList = new DataTable();
                importList.Columns.AddRange(new DataColumn[8] {
                new DataColumn("Emp_ID"),
                new DataColumn("Date"),
                new DataColumn("Code"),
                new DataColumn("Amount"),
                new DataColumn("Recur_Start_Date"),
                new DataColumn("Recur_End_Date"),
                new DataColumn("Frequency"),
                new DataColumn("Remarks")});

                for (int i = 0; i < incomeDeductionAlt.Emp_ID.Length; i++)
                {
                    importList.Rows.Add(incomeDeductionAlt.Emp_ID[i],
                        incomeDeductionAlt.Date[i],
                        incomeDeductionAlt.Code[i],
                        incomeDeductionAlt.Amount[i],
                        incomeDeductionAlt.Recur_Start_Date[i],
                        incomeDeductionAlt.Recur_End_Date[i],
                        incomeDeductionAlt.Frequency[i],
                        incomeDeductionAlt.Remarks[i]);
                }
                TempConnectionStringv4(incomeDeductionAlt.db);
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@ClientCode", mytype = SqlDbType.VarChar, Value = incomeDeductionAlt.ClientCode });
                con.myparameters.Add(new myParameters { ParameterName = "@StartDate", mytype = SqlDbType.VarChar, Value = incomeDeductionAlt.StartDate });
                con.myparameters.Add(new myParameters { ParameterName = "@EndDate", mytype = SqlDbType.VarChar, Value = incomeDeductionAlt.EndDate });
                con.myparameters.Add(new myParameters { ParameterName = "@IncomeOrDeduction", mytype = SqlDbType.VarChar, Value = incomeDeductionAlt.IncomeOrDeduction });
                con.myparameters.Add(new myParameters { ParameterName = "@Data", mytype = SqlDbType.Structured, Value = importList });
                con.myparameters.Add(new myParameters { ParameterName = "@LoginID", mytype = SqlDbType.VarChar, Value = incomeDeductionAlt.LoginID });
                con.myparameters.Add(new myParameters { ParameterName = "@PayoutType", mytype = SqlDbType.VarChar, Value = incomeDeductionAlt.PayoutType });
                con.myparameters.Add(new myParameters { ParameterName = "@filename", mytype = SqlDbType.VarChar, Value = incomeDeductionAlt.filename });
                con.ExecuteNonQuery("sp_PayrollOutsourcing_Import_IncomeDeduction");
                return "success";
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }
        [HttpPost]
        [Route("getTA")]
        public string GETTA(TimeAttendanceAlt timeAttendanceAlt)
        {
            try
            {

                TempConnectionStringv4(timeAttendanceAlt.db);
                Connection con = new Connection();

                con.myparameters.Add(new myParameters { ParameterName = "@tenantID", mytype = SqlDbType.VarChar, Value = timeAttendanceAlt.TenantID });
                con.myparameters.Add(new myParameters { ParameterName = "@StartDate", mytype = SqlDbType.VarChar, Value = timeAttendanceAlt.StartDate });
                con.myparameters.Add(new myParameters { ParameterName = "@EndDate", mytype = SqlDbType.VarChar, Value = timeAttendanceAlt.EndDate });
                con.myparameters.Add(new myParameters { ParameterName = "@LoginID", mytype = SqlDbType.VarChar, Value = timeAttendanceAlt.LoginID });
                con.myparameters.Add(new myParameters { ParameterName = "@PayoutType", mytype = SqlDbType.VarChar, Value = timeAttendanceAlt.PayoutType });
                con.ExecuteNonQuery("sp_getTA");
                return "success";
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }
        [HttpPost]
        [Route("getSummaryDaily")]
        public string getSummaryDaily(TimeAttendanceAlt timeAttendanceAlt)
        {
            try
            {

                FileStream stream = null;
                string dataDir = HostingEnvironment.MapPath("~/Files/PayrollOutSourcing/");

                stream = new FileStream(dataDir + "SummaryHours_Daily_Template.xls", FileMode.Open);
                string filename;
                string datebuild = DateTime.Now.ToString("MMddyyyy");
                string datebuild1 = DateTime.Now.ToString("yyyyMM");
                string timebuild = DateTime.Now.ToString("hhtt");

                TempConnectionStringv4(timeAttendanceAlt.db);
                Connection con = new Connection();

                con.myparameters.Add(new myParameters { ParameterName = "@tenantID", mytype = SqlDbType.VarChar, Value = timeAttendanceAlt.TenantID });
                con.myparameters.Add(new myParameters { ParameterName = "@StartDate", mytype = SqlDbType.VarChar, Value = timeAttendanceAlt.StartDate });
                con.myparameters.Add(new myParameters { ParameterName = "@EndDate", mytype = SqlDbType.VarChar, Value = timeAttendanceAlt.EndDate });

                DataTable DT = con.GetDataTable("Get_SummartDailyReports");


                filename = "SummaryHours_" + timeAttendanceAlt.StartDate.ToString("MMddyyyy") + "_" + timeAttendanceAlt.EndDate.ToString("MMddyyyy") + ".xls";
                using (var fileStream = new FileStream(dataDir + filename, FileMode.Create, FileAccess.Write))
                //using (var fileStream = new FileStream(dataDir + filename, FileMode.Create, FileAccess.Write))
                {
                    stream.CopyTo(fileStream);
                }
                stream.Close();
                ExcelWorkbookFormat workbookFormat = ExcelWorkbookFormat.Xls_2003;
                string testDocFile = dataDir + "SummaryHours_Daily_Template.xls";
                ExcelWorkbook workbook = new ExcelWorkbook(testDocFile);
                ExcelWorksheet firstWorksheet = workbook.Worksheets[0];



                int x = 2;
                for (int i = 0; i < DT.Rows.Count; i++, x++)
                {

                    firstWorksheet["A" + x].Text = DT.Rows[i]["Empid"].ToString();
                    firstWorksheet["B" + x].Text = DT.Rows[i]["EMPLOYEE NAME"].ToString();
                    firstWorksheet["C" + x].Text = DT.Rows[i]["LASTNAME"].ToString();
                    firstWorksheet["D" + x].Text = DT.Rows[i]["FIRSTNAME"].ToString();
                    firstWorksheet["E" + x].Text = DT.Rows[i]["MIDDLENAME"].ToString();
                    firstWorksheet["F" + x].Text = DT.Rows[i]["SCHEDULE DATE"].ToString();
                    firstWorksheet["G" + x].Text = DT.Rows[i]["SCHEDULED IN"].ToString();
                    firstWorksheet["H" + x].Text = DT.Rows[i]["SCHEDULED OUT"].ToString();
                    firstWorksheet["I" + x].Text = DT.Rows[i]["SCHEDULE TYPE"].ToString();
                    firstWorksheet["J" + x].Text = DT.Rows[i]["TIME IN"].ToString();
                    firstWorksheet["K" + x].Text = DT.Rows[i]["TIME OUT"].ToString();
                    firstWorksheet["L" + x].Text = DT.Rows[i]["TARDY HRS"].ToString();
                    firstWorksheet["M" + x].Text = DT.Rows[i]["UNDERTIME HRS"].ToString();
                    firstWorksheet["N" + x].Text = DT.Rows[i]["ABSENT"].ToString();
                    firstWorksheet["O" + x].Text = DT.Rows[i]["LWOP DAYS"].ToString();
                    firstWorksheet["P" + x].Text = DT.Rows[i]["VL DAYS"].ToString();
                    firstWorksheet["Q" + x].Text = DT.Rows[i]["SL DAYS"].ToString();
                    firstWorksheet["R" + x].Text = DT.Rows[i]["OTHER LV DAYS"].ToString();
                    firstWorksheet["S" + x].Text = DT.Rows[i]["NITE-DIFF 10%"].ToString();
                    firstWorksheet["T" + x].Text = DT.Rows[i]["REGULAR-OT"].ToString();
                    firstWorksheet["U" + x].Text = DT.Rows[i]["REG-OT (NP HRS 10%)"].ToString();
                    firstWorksheet["V" + x].Text = DT.Rows[i]["LEGAL HOL OT"].ToString();
                    firstWorksheet["W" + x].Text = DT.Rows[i]["LEGAL HOL OT (NP HRS 10%)"].ToString();
                    firstWorksheet["X" + x].Text = DT.Rows[i]["LEGAL HOL OT > 8HRS"].ToString();
                    firstWorksheet["Y" + x].Text = DT.Rows[i]["LEGAL HOL OT >8 (NP HRS 10%)"].ToString();
                    firstWorksheet["Z" + x].Text = DT.Rows[i]["LEGAL HOL RD"].ToString();
                    firstWorksheet["AA" + x].Text = DT.Rows[i]["LEGAL HOL RD (NP HRS 10%)"].ToString();
                    firstWorksheet["AB" + x].Text = DT.Rows[i]["LEGAL HOL RD >8HRS"].ToString();
                    firstWorksheet["AC" + x].Text = DT.Rows[i]["LEGAL HOL RD >8 (NP HRS 10%)"].ToString();
                    firstWorksheet["AD" + x].Text = DT.Rows[i]["SPECL HOL OT"].ToString();
                    firstWorksheet["AE" + x].Text = DT.Rows[i]["SPECL HOL OT (NP HRS 10%)"].ToString();
                    firstWorksheet["AF" + x].Text = DT.Rows[i]["SPECL HOLIDAY OT >8"].ToString();
                    firstWorksheet["AG" + x].Text = DT.Rows[i]["SPECL HOL OT >8 (NP HRS 10%)"].ToString();
                    firstWorksheet["AH" + x].Text = DT.Rows[i]["SPECIAL HOLIDAY RD"].ToString();
                    firstWorksheet["AI" + x].Text = DT.Rows[i]["SPECIAL HOL RD (NP HRS 10%)"].ToString();
                    firstWorksheet["AJ" + x].Text = DT.Rows[i]["SPECL HOLIDAY ON RD > 8HRS"].ToString();
                    firstWorksheet["AK" + x].Text = DT.Rows[i]["SPECL HOL RD > 8 (NP HRS 10%)"].ToString();
                    firstWorksheet["AL" + x].Text = DT.Rows[i]["REST DAY OT"].ToString();
                    firstWorksheet["AM" + x].Text = DT.Rows[i]["REST DAY OT (NP HRS 10%)"].ToString();
                    firstWorksheet["AN" + x].Text = DT.Rows[i]["REST DAY OT > 8HRS"].ToString();
                    firstWorksheet["AO" + x].Text = DT.Rows[i]["REST DAY OT > 8 (NP HRS 10%)"].ToString();
                    firstWorksheet["AP" + x].Text = DT.Rows[i]["DOUBLE HOLIDAY OT"].ToString();
                    firstWorksheet["AQ" + x].Text = DT.Rows[i]["DOUBLE HOLIDAY OT (NP HRS 10%)"].ToString();
                    firstWorksheet["AR" + x].Text = DT.Rows[i]["DOUBLE HOLIDAY OT > 8HRS"].ToString();
                    firstWorksheet["AS" + x].Text = DT.Rows[i]["DOUBLE HOLIDAY OT > 8 (NP HRS 10%)"].ToString();
                    firstWorksheet["AT" + x].Text = DT.Rows[i]["DOUBLE HOLIDAY RD"].ToString();
                    firstWorksheet["AU" + x].Text = DT.Rows[i]["DOUBLE HOL RD (NP HRS 10%)"].ToString();
                    firstWorksheet["AV" + x].Text = DT.Rows[i]["DOUBLE HOL ON RD > 8HRS"].ToString();
                    firstWorksheet["AW" + x].Text = DT.Rows[i]["DOUBLE HOL RD > 8 (NP HRS 10%)"].ToString();


                }
                //   firstWorksheet.Name = companyCode + "_1601C_" + datebuild1;
                HttpResponse httpResponse = System.Web.HttpContext.Current.Response;

                httpResponse.Clear();
                httpResponse.ContentType = "Application/x-msexcel";
                httpResponse.AddHeader("Content-Disposition", String.Format("attachment; filename={0}", filename));
                string newDIR = HttpContext.Current.Server.MapPath("~/sFTP/PayrollOutsourcing/");
                string onlineDIR = "https://ws.durusthr.com/ILM_WS_Live/sFTP/PayrollOutsourcing/";
                //filename = filename.Replace(',', '_');
                //filename = filename.Replace(" ", String.Empty);
                workbook.Save(newDIR + filename);
                stream.Close();
                return onlineDIR + filename;


            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }

        [HttpPost]
        [Route("getSummaryCutoff")]
        public string getSummaryCutoff(TimeAttendanceAlt timeAttendanceAlt)
        {
            try
            {

                FileStream stream = null;
                string dataDir = HostingEnvironment.MapPath("~/Files/PayrollOutSourcing/");

                stream = new FileStream(dataDir + "Template_SummaryCutoff.xls", FileMode.Open);
                string filename;
                string datebuild = DateTime.Now.ToString("MMddyyyy");
                string datebuild1 = DateTime.Now.ToString("yyyyMM");
                string timebuild = DateTime.Now.ToString("hhtt");

                TempConnectionStringv4(timeAttendanceAlt.db);
                Connection con = new Connection();

                con.myparameters.Add(new myParameters { ParameterName = "@tenantID", mytype = SqlDbType.VarChar, Value = timeAttendanceAlt.TenantID });
                con.myparameters.Add(new myParameters { ParameterName = "@StartDate", mytype = SqlDbType.VarChar, Value = timeAttendanceAlt.StartDate });
                con.myparameters.Add(new myParameters { ParameterName = "@EndDate", mytype = SqlDbType.VarChar, Value = timeAttendanceAlt.EndDate });

                DataTable DT = con.GetDataTable("sp_AdjSummaryHoursv1 ");


                filename = "SHD" + timeAttendanceAlt.StartDate.ToString("MMddyyyy") + "_" + timeAttendanceAlt.EndDate.ToString("MMddyyyy") + ".xls";
                using (var fileStream = new FileStream(dataDir + filename, FileMode.Create, FileAccess.Write))
                //using (var fileStream = new FileStream(dataDir + filename, FileMode.Create, FileAccess.Write))
                {
                    stream.CopyTo(fileStream);
                }
                stream.Close();
                ExcelWorkbookFormat workbookFormat = ExcelWorkbookFormat.Xls_2003;
                string testDocFile = dataDir + "Template_SummaryCutoff.xls";
                ExcelWorkbook workbook = new ExcelWorkbook(testDocFile);
                ExcelWorksheet firstWorksheet = workbook.Worksheets[0];



                int x = 2;
                for (int i = 0; i < DT.Rows.Count; i++, x++)
                {

                    firstWorksheet["A" + x].Text = DT.Rows[i]["Empid"].ToString();
                    firstWorksheet["B" + x].Text = DT.Rows[i]["LASTNAME"].ToString();
                    firstWorksheet["C" + x].Text = DT.Rows[i]["FIRSTNAME"].ToString();
                    firstWorksheet["D" + x].Text = DT.Rows[i]["REG DAYS WRK"].ToString();
                    firstWorksheet["E" + x].Text = DT.Rows[i]["TARDY HRS"].ToString();
                    firstWorksheet["F" + x].Text = DT.Rows[i]["UNDERTIME HRS"].ToString();
                    firstWorksheet["G" + x].Text = DT.Rows[i]["ABSENT DAYS"].ToString();
                    firstWorksheet["H" + x].Text = DT.Rows[i]["LWOP DAYS"].ToString();
                    firstWorksheet["I" + x].Text = DT.Rows[i]["VL DAYS"].ToString();
                    firstWorksheet["J" + x].Text = DT.Rows[i]["SL DAYS"].ToString();
                    firstWorksheet["K" + x].Text = DT.Rows[i]["OTHER LV DAYS"].ToString();
                    firstWorksheet["L" + x].Text = DT.Rows[i]["NITE-DIFF 10%"].ToString();
                    firstWorksheet["M" + x].Text = DT.Rows[i]["REGULAR-OT"].ToString();
                    firstWorksheet["N" + x].Text = DT.Rows[i]["REG-OT (NP HRS 10%)"].ToString();
                    firstWorksheet["O" + x].Text = DT.Rows[i]["LEGAL HOL OT"].ToString();
                    firstWorksheet["P" + x].Text = DT.Rows[i]["LEGAL HOL OT (NP HRS 10%)"].ToString();
                    firstWorksheet["Q" + x].Text = DT.Rows[i]["LEGAL HOL OT > 8HRS"].ToString();
                    firstWorksheet["R" + x].Text = DT.Rows[i]["LEGAL HOL OT >8 (NP HRS 10%)"].ToString();
                    firstWorksheet["S" + x].Text = DT.Rows[i]["LEGAL HOL RD"].ToString();
                    firstWorksheet["T" + x].Text = DT.Rows[i]["LEGAL HOL RD (NP HRS 10%)"].ToString();
                    firstWorksheet["U" + x].Text = DT.Rows[i]["LEGAL HOL RD >8HRS"].ToString();
                    firstWorksheet["V" + x].Text = DT.Rows[i]["LEGAL HOL RD >8 (NP HRS 10%)"].ToString();
                    firstWorksheet["W" + x].Text = DT.Rows[i]["SPECL HOL OT"].ToString();
                    firstWorksheet["X" + x].Text = DT.Rows[i]["SPECL HOL OT (NP HRS 10%)"].ToString();
                    firstWorksheet["Y" + x].Text = DT.Rows[i]["SPECL HOLIDAY OT >8"].ToString();
                    firstWorksheet["Z" + x].Text = DT.Rows[i]["SPECL HOL OT >8 (NP HRS 10%)"].ToString();
                    firstWorksheet["AA" + x].Text = DT.Rows[i]["SPECIAL HOLIDAY RD"].ToString();
                    firstWorksheet["AB" + x].Text = DT.Rows[i]["SPECIAL HOL RD (NP HRS 10%)"].ToString();
                    firstWorksheet["AC" + x].Text = DT.Rows[i]["SPECL HOLIDAY ON RD > 8HRS"].ToString();
                    firstWorksheet["AD" + x].Text = DT.Rows[i]["SPECL HOL RD > 8 (NP HRS 10%)"].ToString();
                    firstWorksheet["AE" + x].Text = DT.Rows[i]["REST DAY OT"].ToString();
                    firstWorksheet["AF" + x].Text = DT.Rows[i]["REST DAY OT (NP HRS 10%)"].ToString();
                    firstWorksheet["AG" + x].Text = DT.Rows[i]["REST DAY OT > 8HRS"].ToString();
                    firstWorksheet["AH" + x].Text = DT.Rows[i]["REST DAY OT > 8 (NP HRS 10%)"].ToString();
                    firstWorksheet["AI" + x].Text = DT.Rows[i]["DOUBLE HOLIDAY OT"].ToString();
                    firstWorksheet["AJ" + x].Text = DT.Rows[i]["DOUBLE HOLIDAY OT (NP HRS 10%)"].ToString();
                    firstWorksheet["AK" + x].Text = DT.Rows[i]["DOUBLE HOLIDAY OT > 8HRS"].ToString();
                    firstWorksheet["AL" + x].Text = DT.Rows[i]["DOUBLE HOLIDAY OT > 8 (NP HRS 10%)"].ToString();
                    firstWorksheet["AM" + x].Text = DT.Rows[i]["DOUBLE HOLIDAY RD"].ToString();
                    firstWorksheet["AN" + x].Text = DT.Rows[i]["DOUBLE HOL RD (NP HRS 10%)"].ToString();
                    firstWorksheet["AO" + x].Text = DT.Rows[i]["DOUBLE HOL ON RD > 8HRS"].ToString();
                    firstWorksheet["AP" + x].Text = DT.Rows[i]["DOUBLE HOL RD > 8 (NP HRS 10%)"].ToString();
                    firstWorksheet["AQ" + x].Text = DT.Rows[i]["REMARKS"].ToString();



                }
                //   firstWorksheet.Name = companyCode + "_1601C_" + datebuild1;
                HttpResponse httpResponse = System.Web.HttpContext.Current.Response;

                httpResponse.Clear();
                httpResponse.ContentType = "Application/x-msexcel";
                httpResponse.AddHeader("Content-Disposition", String.Format("attachment; filename={0}", filename));
                string newDIR = HttpContext.Current.Server.MapPath("~/sFTP/PayrollOutsourcing/");
                string onlineDIR = "https://ws.durusthr.com/ILM_WS_Live/sFTP/PayrollOutsourcing/";
                //filename = filename.Replace(',', '_');
                //filename = filename.Replace(" ", String.Empty);
                workbook.Save(newDIR + filename);
                stream.Close();
                return onlineDIR + filename;


            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }
        [HttpPost]
        [Route("PO_ImportLoanRegister_Alt")]
        public string PO_ImportLoanRegister_Alt(LoanRegisterAlt loanRegisterAlt)
        {
            try
            {
                DataTable importList = new DataTable();
                importList.Columns.AddRange(new DataColumn[13]
                {
                    new DataColumn("Emp_ID"),
                    new DataColumn("Loan_Code"),
                    new DataColumn("Principal_Amount"),
                    new DataColumn("Total_Payments"),
                    new DataColumn("Loan_Starting_Deductions_Date"),
                    new DataColumn("Total_Previous_Payment"),
                    new DataColumn("Monthly_Loan_Amort_Amount"),
                    new DataColumn("Loan_Amort_Frequency"),
                    new DataColumn("Loan_Maturity_Date"),
                    new DataColumn("Date_Loan_Granted"),
                    new DataColumn("Account_Number"),
                    new DataColumn("Remarks"),
                    new DataColumn("Loan_Amount"),
                });
                for (int i = 0; i < loanRegisterAlt.Emp_ID.Length; i++)
                {
                    importList.Rows.Add(loanRegisterAlt.Emp_ID[i],
                        loanRegisterAlt.Loan_Code[i],
                        loanRegisterAlt.Principal_Amount[i],
                        loanRegisterAlt.Total_Payments[i],
                        loanRegisterAlt.Loan_Starting_Deductions_Date[i],
                        loanRegisterAlt.Total_Previous_Payment[i],
                        loanRegisterAlt.Monthly_Loan_Amort_Amount[i],
                        loanRegisterAlt.Loan_Amort_Frequency[i],
                        loanRegisterAlt.Loan_Maturity_Date[i],
                        loanRegisterAlt.Date_Loan_Granted[i],
                        loanRegisterAlt.Account_Number[i],
                        loanRegisterAlt.Remarks[i],
                        loanRegisterAlt.Loan_Amount[i]);
                }
                TempConnectionStringv4(loanRegisterAlt.db);
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@ClientCode", mytype = SqlDbType.VarChar, Value = loanRegisterAlt.ClientCode });
                con.myparameters.Add(new myParameters { ParameterName = "@StartDate", mytype = SqlDbType.VarChar, Value = loanRegisterAlt.StartDate });
                con.myparameters.Add(new myParameters { ParameterName = "@EndDate", mytype = SqlDbType.VarChar, Value = loanRegisterAlt.EndDate });
                con.myparameters.Add(new myParameters { ParameterName = "@Data", mytype = SqlDbType.Structured, Value = importList });
                con.myparameters.Add(new myParameters { ParameterName = "@LoginID", mytype = SqlDbType.VarChar, Value = loanRegisterAlt.LoginID });
                con.myparameters.Add(new myParameters { ParameterName = "@PayoutType", mytype = SqlDbType.VarChar, Value = loanRegisterAlt.PayoutType });
                con.myparameters.Add(new myParameters { ParameterName = "@filename", mytype = SqlDbType.VarChar, Value = loanRegisterAlt.filename });
                con.ExecuteNonQuery("sp_PayrollOutsourcing_Import_LoanRegister");
                return "Success";
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }

        [HttpPost]
        [Route("WS_Delete_Uploaded_EDL")]
        public string Delete_Uploaded_EDL(LoanRegisterAlt loanRegisterAlt)
        {
            try
            {

                TempConnectionStringv4(loanRegisterAlt.db);
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@option", mytype = SqlDbType.VarChar, Value = loanRegisterAlt.option });
                con.myparameters.Add(new myParameters { ParameterName = "@filename", mytype = SqlDbType.VarChar, Value = loanRegisterAlt.filename });
                con.myparameters.Add(new myParameters { ParameterName = "@time", mytype = SqlDbType.VarChar, Value = loanRegisterAlt.time });
                con.myparameters.Add(new myParameters { ParameterName = "@tenantId", mytype = SqlDbType.VarChar, Value = loanRegisterAlt.tenantId });
                con.myparameters.Add(new myParameters { ParameterName = "@loginId", mytype = SqlDbType.VarChar, Value = loanRegisterAlt.LoginID });
                con.ExecuteNonQuery("sp_Delete_Uploaded_EDL");
                return "Success";
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }
        [HttpPost]
        [Route("PO_ImportSSSMaternity")]
        public string PO_ImportSSSMaternity(SSSMaternity sSSMaternity)
        {
            try
            {
                DataTable importList = new DataTable();
                importList.Columns.AddRange(new DataColumn[16]
                {
                    new DataColumn("Emp_ID"),
                    new DataColumn("Basic_Salary"),
                    new DataColumn("Laundry_Allowance"),
                    new DataColumn("Meal_Allowance"),
                    new DataColumn("Rice_Subsidy"),
                    new DataColumn("Communication"),
                    new DataColumn("Transpo_Allowance"),
                    new DataColumn("Total_Pay"),
                    new DataColumn("SSS_EE_Cont"),
                    new DataColumn("PHIC_EE_Cont"),
                    new DataColumn("HDMF_EE_Cont"),
                    new DataColumn("SSS_Maternity_Benefit"),
                    new DataColumn("Salary_Differential"),
                    new DataColumn("Taxable_Income"),
                    new DataColumn("WithHolding_Tax"),
                    new DataColumn("Net_Pay")
                });
                for (int i = 0; i < sSSMaternity.Emp_ID.Length; i++)
                {
                    importList.Rows.Add(sSSMaternity.Emp_ID[i],
                        sSSMaternity.Basic_Salary[i],
                        sSSMaternity.Laundry_Allowance[i],
                        sSSMaternity.Meal_Allowance[i],
                        sSSMaternity.Rice_Subsidy[i],
                        sSSMaternity.Communication[i],
                        sSSMaternity.Transpo_Allowance[i],
                        sSSMaternity.Total_Pay[i],
                        sSSMaternity.SSS_EE_Cont[i],
                        sSSMaternity.PHIC_EE_Cont[i],
                        sSSMaternity.HDMF_EE_Cont[i],
                        sSSMaternity.SSS_Maternity_Benefit[i],
                        sSSMaternity.Salary_Differential[i],
                        sSSMaternity.Taxable_Income[i],
                        sSSMaternity.WithHolding_Tax[i],
                        sSSMaternity.Net_Pay[i]);
                }
                TempConnectionStringv4(sSSMaternity.db);
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@ClientCode", mytype = SqlDbType.VarChar, Value = sSSMaternity.ClientCode });
                con.myparameters.Add(new myParameters { ParameterName = "@StartDate", mytype = SqlDbType.VarChar, Value = sSSMaternity.StartDate });
                con.myparameters.Add(new myParameters { ParameterName = "@EndDate", mytype = SqlDbType.VarChar, Value = sSSMaternity.EndDate });
                con.myparameters.Add(new myParameters { ParameterName = "@Data", mytype = SqlDbType.Structured, Value = importList });
                con.myparameters.Add(new myParameters { ParameterName = "@LoginID", mytype = SqlDbType.VarChar, Value = sSSMaternity.LoginID });
                con.ExecuteNonQuery("sp_PayrollOutsourcing_Import_SSSMaternity");
                return "Success";
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }
        [HttpPost]
        [Route("PO_ImportITR")]
        public string PO_ImportITR(ITR_Model iTR_Model)
        {
            try
            {
                DataTable importList = new DataTable();
                importList.Columns.AddRange(new DataColumn[16] {
                    new DataColumn("Emp_ID"),
                    new DataColumn("Employer"),
                    new DataColumn("Employer_Date"),
                    new DataColumn("TIN"),
                    new DataColumn("Employer_Address"),
                    new DataColumn("Withheld_To_Prev_Employer"),
                    new DataColumn("Prev_SSS_HDMF_PH"),
                    new DataColumn("Taxed_Salaries"),
                    new DataColumn("Taxed_Bonus"),
                    new DataColumn("Taxable_Income"),
                    new DataColumn("Gross_To_Employer"),
                    new DataColumn("Gross_Plus_SSS_HDMF_PH_To_Employer"),
                    new DataColumn("NT_Salaries"),
                    new DataColumn("NT_Bonus"),
                    new DataColumn("Deminimis"),
                    new DataColumn("Total_NT_Income")
                });
                for (int i = 0; i < iTR_Model.Emp_ID.Length; i++)
                {
                    importList.Rows.Add(iTR_Model.Emp_ID[i],
                                        iTR_Model.Employer[i],
                                        iTR_Model.Employer_Date[i],
                                        iTR_Model.TIN[i],
                                        iTR_Model.Employer_Address[i],
                                        iTR_Model.Withheld_To_Prev_Employer[i],
                                        iTR_Model.Prev_SSS_HDMF_PH[i],
                                        iTR_Model.Taxed_Salaries[i],
                                        iTR_Model.Taxed_Bonus[i],
                                        iTR_Model.Taxable_Income[i],
                                        iTR_Model.Gross_To_Employer[i],
                                        iTR_Model.Gross_Plus_SSS_HDMF_PH_To_Employer[i],
                                        iTR_Model.NT_Salaries[i],
                                        iTR_Model.NT_Bonus[i],
                                        iTR_Model.Deminimis[i],
                                        iTR_Model.Total_NT_Income[i]);
                }
                TempConnectionString();
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@ClientCode", mytype = SqlDbType.VarChar, Value = iTR_Model.ClientCode });
                con.myparameters.Add(new myParameters { ParameterName = "@Data", mytype = SqlDbType.Structured, Value = importList });
                con.myparameters.Add(new myParameters { ParameterName = "@LoginID", mytype = SqlDbType.VarChar, Value = iTR_Model.LoginID });
                con.ExecuteNonQuery("sp_PayrollOutsourcing_Import_ITR");
                return "Success";
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }
        [HttpPost]
        [Route("PO_ImportITRv2")]
        public string PO_ImportITRv2(ITR_Modelv2 param)
        {
            try
            {
                DataTable importList = new DataTable();
                importList.Columns.AddRange(new DataColumn[48] {
                    new DataColumn("EmpID"),
                    new DataColumn("EmpName"),
                    new DataColumn("ForYYYY"),
                    new DataColumn("ForMMFrom"),
                    new DataColumn("ForMMTo"),
                    new DataColumn("TIN"),
                    new DataColumn("EmployerName"),
                    new DataColumn("RegAddress"),
                    new DataColumn("ZipCode"),
                    new DataColumn("EmployerType"),
                    new DataColumn("PrevERTIN"),
                    new DataColumn("PrevERName"),
                    new DataColumn("PrevERAddress"),
                    new DataColumn("PrevERZip"),
                    new DataColumn("GrossIncomePresentEmp"),
                    new DataColumn("NTXIncomePresentEmp"),
                    new DataColumn("TXIncomePresentEmp"),
                    new DataColumn("TXIncomePrevEmp"),
                    new DataColumn("TXIncome"),
                    new DataColumn("TaxDue"),
                    new DataColumn("WithTaxPresentEmp"),
                    new DataColumn("WithTaxPrevEmp"),
                    new DataColumn("WithTaxAdj"),
                    new DataColumn("BasicSalaryMWE"),
                    new DataColumn("HolidayPayMWE"),
                    new DataColumn("OvertimePayMWE"),
                    new DataColumn("NSDPayMWE"),
                    new DataColumn("HazardPayMWE"),
                    new DataColumn("_13thPayotherBen"),
                    new DataColumn("DeMinimisBen"),
                    new DataColumn("GovContribandOther"),
                    new DataColumn("SalandOtherCompensation"),
                    new DataColumn("TotalNTXIncome"),
                    new DataColumn("BasicSalary"),
                    new DataColumn("Representation"),
                    new DataColumn("Transportation"),
                    new DataColumn("COLA"),
                    new DataColumn("FixedHousingAllow"),
                    new DataColumn("OthersLine42a"),
                    new DataColumn("OthersLine42b"),
                    new DataColumn("Commission"),
                    new DataColumn("ProfitSharing"),
                    new DataColumn("Fees"),
                    new DataColumn("TX13thBen"),
                    new DataColumn("HazardPay"),
                    new DataColumn("OTPay"),
                    new DataColumn("Others49"),
                    new DataColumn("TotalTXIncome")
                });
                for (int i = 0; i < param.EmpID.Length; i++)
                {
                    importList.Rows.Add(param.EmpID[i],
                                        param.EmpName[i],
                                        param.ForYYYY[i],
                                        param.ForMMFrom[i],
                                        param.ForMMTo[i],
                                        param.TIN[i],
                                        param.EmployerName[i],
                                        param.RegAddress[i],
                                        param.ZipCode[i],
                                        param.EmployerType[i],
                                        param.PrevERTIN[i],
                                        param.PrevERName[i],
                                        param.PrevERAddress[i],
                                        param.PrevERZip[i],
                                        param.GrossIncomePresentEmp[i],
                                        param.NTXIncomePresentEmp[i],
                                        param.TXIncomePresentEmp[i],
                                        param.TXIncomePrevEmp[i],
                                        param.TXIncome[i],
                                        param.TaxDue[i],
                                        param.WithTaxPresentEmp[i],
                                        param.WithTaxPrevEmp[i],
                                        param.WithTaxAdj[i],
                                        param.BasicSalaryMWE[i],
                                        param.HolidayPayMWE[i],
                                        param.OvertimePayMWE[i],
                                        param.NSDPayMWE[i],
                                        param.HazardPayMWE[i],
                                        param._13thPayotherBen[i],
                                        param.DeMinimisBen[i],
                                        param.GovContribandOther[i],
                                        param.SalandOtherCompensation[i],
                                        param.TotalNTXIncome[i],
                                        param.BasicSalary[i],
                                        param.Representation[i],
                                        param.Transportation[i],
                                        param.COLA[i],
                                        param.FixedHousingAllow[i],
                                        param.OthersLine42a[i],
                                        param.OthersLine42b[i],
                                        param.Commission[i],
                                        param.ProfitSharing[i],
                                        param.Fees[i],
                                        param.TX13thBen[i],
                                        param.HazardPay[i],
                                        param.OTPay[i],
                                        param.Others49[i],
                                        param.TotalTXIncome[i]);
                }
                TempConnectionStringv4(param.db);
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@ClientCode", mytype = SqlDbType.VarChar, Value = param.ClientCode });
                con.myparameters.Add(new myParameters { ParameterName = "@Data", mytype = SqlDbType.Structured, Value = importList });
                con.myparameters.Add(new myParameters { ParameterName = "@LoginID", mytype = SqlDbType.VarChar, Value = param.LoginID });
                con.myparameters.Add(new myParameters { ParameterName = "@ifLatest", mytype = SqlDbType.VarChar, Value = param.IfLatest });
                con.ExecuteNonQuery("sp_PayrollOutsourcing_Import_ITR");
                return "Success";
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }

        [HttpPost]
        [Route("PO_ImportNewJoiners")]
        public PO_NewJoiners PO_ImportNewJoiners(Main_Model param)
        {
            PO_NewJoiners ret = new PO_NewJoiners();
            List<NewJoinersReturn> list = new List<NewJoinersReturn>();
            try
            {
                DataTable importList = new DataTable();
                importList.Columns.AddRange(new DataColumn[46]
                {
                  new DataColumn("Emp_ID"),
                  new DataColumn("Last_Name"),
                  new DataColumn("First_Name"),
                  new DataColumn("Middle_Name"),
                  new DataColumn("Days_To_Credit"),
                  new DataColumn("IsRehired"),
                  new DataColumn("Gender"),
                  new DataColumn("Email"),
                  new DataColumn("Grade_Level"),
                  new DataColumn("Cost_Center"),
                  new DataColumn("Current_Position"),
                  new DataColumn("Current_Division"),
                  new DataColumn("Current_Department"),
                  new DataColumn("DIrect_Indirect_Labor"),
                  new DataColumn("Peza_Classification"),
                  new DataColumn("Birth_Date"),
                  new DataColumn("Date_Employed"),
                  new DataColumn("IsOT_Allowed"),
                  new DataColumn("Tax_Status"),
                  new DataColumn("Monthly_Basic_Salary"),
                  new DataColumn("PayoutScheme"),
                  new DataColumn("Additional_HDMF"),
                  new DataColumn("Payment_Type"),
                  new DataColumn("BPI_Account_Number"),
                  new DataColumn("Pagibig_ID"),
                  new DataColumn("SSS_ID"),
                  new DataColumn("TIN_ID"),
                  new DataColumn("Philhealth_ID"),
                  new DataColumn("Employment_Status"),
                  new DataColumn("Employment_Type"),
                  new DataColumn("Address_1"),
                  new DataColumn("Address_2"),
                  new DataColumn("Contact_Number"),
                  new DataColumn("Annual_RT"),
                  new DataColumn("Currency"),
                  new DataColumn("GL_Operating_Unit"),
                  new DataColumn("GL_Location"),
                  new DataColumn("GL_Business_Unit"),
                  new DataColumn("GL_DeptID"),
                  new DataColumn("GL_Project"),
                  new DataColumn("isSSS"),
                  new DataColumn("isPHIC"),
                  new DataColumn("isHDMF"),
                  new DataColumn("HasExited"),
                  new DataColumn("RateType"),
                  new DataColumn("MWE"),
                });
                for (int i = 0; i < param.Emp_ID.Length; i++)
                {
                    importList.Rows.Add(param.Emp_ID[i],
                  param.Last_Name[i],
                  param.First_Name[i],
                  param.Middle_Name[i],
                  param.Days_To_Credit[i],
                  param.IsRehired[i],
                  param.Gender[i],
                  param.Email[i],
                  param.Grade_Level[i],
                  param.Cost_Center[i],
                  param.Current_Position[i],
                  param.Current_Division[i],
                  param.Current_Department[i],
                  param.DIrect_Indirect_Labor[i],
                  param.Peza_Classification[i],
                  param.Birth_Date[i],
                  param.Date_Employed[i],
                  param.IsOT_Allowed[i],
                  param.Tax_Status[i],
                  param.Monthly_Basic_Salary[i],
                  param.PayoutScheme[i],
                  param.Additional_HDMF[i],
                  param.Payment_Type[i],
                  param.BPI_Account_Number[i],
                  param.Pagibig_ID[i],
                  param.SSS_ID[i],
                  param.TIN_ID[i],
                  param.Philhealth_ID[i],
                  param.Employment_Status[i],
                  param.Employment_Type[i],
                  param.Address_1[i],
                  param.Address_2[i],
                  param.Contact_Number[i],
                  param.Annual_RT[i],
                  param.Currency[i],
                  param.GL_Operating_Unit[i],
                  param.GL_Location[i],
                  param.GL_Business_Unit[i],
                  param.GL_DeptID[i],
                  param.GL_Project[i],
                  param.isPHIC[i],
                  param.isHDMF[i],
                  param.isSSS[i],
                  param.HasExited[i],
                  param.RateType[i],
                  param.MWE[i]);
                }
                TempConnectionStringv4(param.db);
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@ClientCode", mytype = SqlDbType.VarChar, Value = param.ClientCode });
                con.myparameters.Add(new myParameters { ParameterName = "@Data", mytype = SqlDbType.Structured, Value = importList });
                con.myparameters.Add(new myParameters { ParameterName = "@LoginID", mytype = SqlDbType.VarChar, Value = param.LoginID });
                con.myparameters.Add(new myParameters { ParameterName = "@StartDate", mytype = SqlDbType.VarChar, Value = param.StartDate });
                con.myparameters.Add(new myParameters { ParameterName = "@EndDate", mytype = SqlDbType.VarChar, Value = param.EndDate });
                con.myparameters.Add(new myParameters { ParameterName = "@PayoutType", mytype = SqlDbType.VarChar, Value = param.PayoutType });
                DataTable DT = con.GetDataTable("sp_PayrollOutsourcing_Import_NewJoiners");
                foreach (DataRow row in DT.Rows)
                {
                    NewJoinersReturn NJR = new NewJoinersReturn();
                    NJR.EmpID = row["EmpID"].ToString();
                    NJR.Email = row["Email"].ToString();
                    NJR.GPass = row["GeneratedPass"].ToString();
                    list.Add(NJR);
                    //Send to email
                    string emailaccount = "durusthr@illimitado.com";
                    string emailpassword = "@1230Qwerty";
                    using (MailMessage mm = new MailMessage(emailaccount, row["Email"].ToString()))
                    {
                        MailAddress aliasmail = new MailAddress("durusthr@illimitado.com", "durusthr@illimitado.com");
                        MailAddress aliasreplymail = new MailAddress("durusthr@illimitado.com");
                        MailAddress aliasmailBCC = new MailAddress("durusthr@illimitado.com");
                        mm.From = aliasmail;
                        mm.Bcc.Add(aliasmailBCC);
                        mm.Subject = "Notifications";
                        mm.Body = "Please use this temporary password to login: " + row["GeneratedPass"].ToString();
                        mm.IsBodyHtml = true;
                        SmtpClient smtp = new SmtpClient();
                        smtp.Host = "smtp.office365.com";
                        smtp.EnableSsl = true;
                        NetworkCredential NetworkCred = new NetworkCredential(emailaccount, emailpassword);
                        smtp.UseDefaultCredentials = true;
                        smtp.Credentials = NetworkCred;
                        smtp.Port = 587;
                        smtp.Send(mm);
                    }
                }
                ret.DU = list;
                ret.myreturn = "Success";
            }
            catch (Exception e)
            {
                ret.DU = list;
                ret.myreturn = e.ToString();
            }
            return ret;
        }
        [HttpPost]
        [Route("WS_GetCurrencyList")]
        public List<CurrencyList> CurrencyListGet(CurrencyModel param)
        {
            //GetDatabase GDB = new GetDatabase();
            //GDB.ClientName = login.companyname;
            List<CurrencyList> DU = new List<CurrencyList>();
            TempConnectionStringv4(param.db);
            Connection con = new Connection();




            con.myparameters.Add(new myParameters { ParameterName = "@TenantID", mytype = SqlDbType.NVarChar, Value = param.TenantID });
            DataTable DT = con.GetDataTable("sp_getCurrencyList");
            foreach (DataRow row in DT.Rows)
            {
                CurrencyList List = new CurrencyList()
                {
                    Currency = row["Currency"].ToString(),



                };
                DU.Add(List);
            }
            return DU;


        }

        //FOR ILM NJ API
        [HttpPost]
        [Route("newjoiners")]
        public string PO_ImportNewJoiners(NJ_API param)
        {

            try
            {
                DataTable importList = new DataTable();
                importList.Columns.AddRange(new DataColumn[46]
                {
                  new DataColumn("Emp_ID"),
                  new DataColumn("Last_Name"),
                  new DataColumn("First_Name"),
                  new DataColumn("Middle_Name"),
                  new DataColumn("Days_To_Credit"),
                  new DataColumn("IsRehired"),
                  new DataColumn("Gender"),
                  new DataColumn("Email"),
                  new DataColumn("Grade_Level"),
                  new DataColumn("Cost_Center"),
                  new DataColumn("Current_Position"),
                  new DataColumn("Current_Division"),
                  new DataColumn("Current_Department"),
                  new DataColumn("DIrect_Indirect_Labor"),
                  new DataColumn("Peza_Classification"),
                  new DataColumn("Birth_Date"),
                  new DataColumn("Date_Employed"),
                  new DataColumn("IsOT_Allowed"),
                  new DataColumn("Tax_Status"),
                  new DataColumn("Monthly_Basic_Salary"),
                  new DataColumn("PayoutScheme"),
                  new DataColumn("Additional_HDMF"),
                  new DataColumn("Payment_Type"),
                  new DataColumn("BPI_Account_Number"),
                  new DataColumn("Pagibig_ID"),
                  new DataColumn("SSS_ID"),
                  new DataColumn("TIN_ID"),
                  new DataColumn("Philhealth_ID"),
                  new DataColumn("Employment_Status"),
                  new DataColumn("Employment_Type"),
                  new DataColumn("Address_1"),
                  new DataColumn("Address_2"),
                  new DataColumn("Contact_Number"),
                  new DataColumn("Annual_RT"),
                  new DataColumn("Currency"),
                  new DataColumn("GL_Operating_Unit"),
                  new DataColumn("GL_Location"),
                  new DataColumn("GL_Business_Unit"),
                  new DataColumn("GL_DeptID"),
                  new DataColumn("GL_Project"),
                  new DataColumn("isSSS"),
                  new DataColumn("isPHIC"),
                  new DataColumn("isHDMF"),
                  new DataColumn("HasExited"),
                  new DataColumn("RateType"),
                  new DataColumn("MWE"),
                });
                for (int i = 0; i < param.Emp_ID.Length; i++)
                {
                    importList.Rows.Add(param.Emp_ID[i],
                  param.Last_Name[i],
                  param.First_Name[i],
                  param.Middle_Name[i],
                  param.Days_To_Credit[i],
                  param.IsRehired[i],
                  param.Gender[i],
                  param.Email[i],
                  param.Grade_Level[i],
                  param.Cost_Center[i],
                  param.Current_Position[i],
                  param.Current_Division[i],
                  param.Current_Department[i],
                  param.DIrect_Indirect_Labor[i],
                  param.Peza_Classification[i],
                  param.Birth_Date[i],
                  param.Date_Employed[i],
                  param.IsOT_Allowed[i],
                  param.Tax_Status[i],
                  param.Monthly_Basic_Salary[i],
                  param.PayoutScheme[i],
                  param.Additional_HDMF[i],
                  param.Payment_Type[i],
                  param.BPI_Account_Number[i],
                  param.Pagibig_ID[i],
                  param.SSS_ID[i],
                  param.TIN_ID[i],
                  param.Philhealth_ID[i],
                  param.Employment_Status[i],
                  param.Employment_Type[i],
                  param.Address_1[i],
                  param.Address_2[i],
                  param.Contact_Number[i],
                  param.Annual_RT[i],
                  param.Currency[i],
                  param.GL_Operating_Unit[i],
                  param.GL_Location[i],
                  param.GL_Business_Unit[i],
                  param.GL_DeptID[i],
                  param.GL_Project[i],
                  param.isPHIC[i],
                  param.isHDMF[i],
                  param.isSSS[i],
                  param.HasExited[i],
                  param.RateType[i],
                  param.MWE[i]);
                }
                TempConnectionStringv4(param.db);
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@Token", mytype = SqlDbType.VarChar, Value = param.Token });
                con.myparameters.Add(new myParameters { ParameterName = "@ClientCode", mytype = SqlDbType.VarChar, Value = param.ClientCode });
                con.myparameters.Add(new myParameters { ParameterName = "@LoginID", mytype = SqlDbType.VarChar, Value = param.LoginID });
                con.myparameters.Add(new myParameters { ParameterName = "@password", mytype = SqlDbType.VarChar, Value = param.Password });
                string toke = con.ExecuteScalar("sp_getToken");
                if (toke == "")
                {
                    return "Invalid Token";
                }
                else if (toke == "invalid credentials")
                {
                    return "Invalid Credentials";
                }
                else
                {
                    TempConnectionStringv4(param.db);
                    con = new Connection();
                    //con.myparameters.Add(new myParameters { ParameterName = "@Token", mytype = SqlDbType.VarChar, Value = param.Token });
                    con.myparameters.Add(new myParameters { ParameterName = "@ClientCode", mytype = SqlDbType.VarChar, Value = param.ClientCode });
                    con.myparameters.Add(new myParameters { ParameterName = "@Data", mytype = SqlDbType.Structured, Value = importList });
                    con.myparameters.Add(new myParameters { ParameterName = "@LoginID", mytype = SqlDbType.VarChar, Value = param.LoginID });
                    con.myparameters.Add(new myParameters { ParameterName = "@StartDate", mytype = SqlDbType.VarChar, Value = param.StartDate });
                    con.myparameters.Add(new myParameters { ParameterName = "@EndDate", mytype = SqlDbType.VarChar, Value = param.EndDate });
                    con.myparameters.Add(new myParameters { ParameterName = "@PayoutType", mytype = SqlDbType.VarChar, Value = param.PayoutType });
                    DataTable DT = con.GetDataTable("sp_newjoiners_api");



                    foreach (DataRow row in DT.Rows)
                    {
                        API_ILM_NJ_RETURN NJR = new API_ILM_NJ_RETURN();
                        NJR.EmpID = row["EmpID"].ToString();
                        NJR.Email = row["Email"].ToString();
                        NJR.GPass = row["GeneratedPass"].ToString();

                        //Send to email
                        string emailaccount = "durusthr@illimitado.com";
                        string emailpassword = "@1230Qwerty";
                        using (MailMessage mm = new MailMessage(emailaccount, row["Email"].ToString()))
                        {
                            MailAddress aliasmail = new MailAddress("durusthr@illimitado.com", "durusthr@illimitado.com");
                            MailAddress aliasreplymail = new MailAddress("durusthr@illimitado.com");
                            MailAddress aliasmailBCC = new MailAddress("durusthr@illimitado.com");
                            mm.From = aliasmail;
                            mm.Bcc.Add(aliasmailBCC);
                            mm.Subject = "Notifications";
                            mm.Body = "Please use this temporary password to login: " + row["GeneratedPass"].ToString();
                            mm.IsBodyHtml = true;
                            SmtpClient smtp = new SmtpClient();
                            smtp.Host = "smtp.office365.com";
                            smtp.EnableSsl = true;
                            NetworkCredential NetworkCred = new NetworkCredential(emailaccount, emailpassword);
                            smtp.UseDefaultCredentials = true;
                            smtp.Credentials = NetworkCred;
                            smtp.Port = 587;
                            smtp.Send(mm);
                        }
                    }
                    return toke;
                }
            }
            catch (Exception e)
            {
                return e.ToString();
            }

        }
        [HttpPost]
        [Route("empdatachange")]
        public string emp_data_change(datachange datachange)
        {
            try
            {
                DataTable importList = new DataTable();
                importList.Columns.AddRange(new DataColumn[5]{
                    new DataColumn("Emp_ID"),
                    new DataColumn("ColumnType"),
                    new DataColumn("NewValue"),
                    new DataColumn("OldValue"),
                    new DataColumn("Remarks")
                });
                for (int i = 0; i < datachange.Emp_ID.Length; i++)
                {
                    importList.Rows.Add(datachange.Emp_ID[i], datachange.ColumnType[i], datachange.NewValue[i], datachange.OldValue[i], datachange.Remarks[i]);
                }
                TempConnectionStringv4(datachange.db);
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@Token", mytype = SqlDbType.VarChar, Value = datachange.Token });
                con.myparameters.Add(new myParameters { ParameterName = "@ClientCode", mytype = SqlDbType.VarChar, Value = datachange.ClientCode });
                con.myparameters.Add(new myParameters { ParameterName = "@LoginID", mytype = SqlDbType.VarChar, Value = datachange.LoginID });
                con.myparameters.Add(new myParameters { ParameterName = "@password", mytype = SqlDbType.VarChar, Value = datachange.Password });

                string toke = con.ExecuteScalar("sp_getToken");
                if (toke == "")
                {
                    return "invalid token";
                }
                else if (toke == "invalid credentials")
                {
                    return "Invalid Credentials";
                }
                else
                {
                    TempConnectionStringv4(datachange.db);
                    con = new Connection();
                    //con.myparameters.Add(new myParameters { ParameterName = "@Token", mytype = SqlDbType.VarChar, Value = datachange.Token });
                    con.myparameters.Add(new myParameters { ParameterName = "@ClientCode", mytype = SqlDbType.VarChar, Value = datachange.ClientCode });
                    con.myparameters.Add(new myParameters { ParameterName = "@Data", mytype = SqlDbType.Structured, Value = importList });
                    con.myparameters.Add(new myParameters { ParameterName = "@LoginID", mytype = SqlDbType.VarChar, Value = datachange.LoginID });
                    con.ExecuteNonQuery("sp_datachange_api");
                    return "Success";
                }
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }

        //FOR ILM NJ API END

        //EDITED 1/4/2021
        [HttpPost]
        [Route("PO_ImportNewJoinersv2")]
        public PO_NewJoiners PO_ImportNewJoinersv2(Main_Model param)
        {
            PO_NewJoiners ret = new PO_NewJoiners();
            List<NewJoinersReturn> list = new List<NewJoinersReturn>();
            try
            {
                DataTable importList = new DataTable();
                importList.Columns.AddRange(new DataColumn[47]
                {
                  new DataColumn("Emp_ID"),
                  new DataColumn("Last_Name"),
                  new DataColumn("First_Name"),
                  new DataColumn("Middle_Name"),
                  new DataColumn("Days_To_Credit"),
                  new DataColumn("IsRehired"),
                  new DataColumn("Gender"),
                  new DataColumn("Email"),
                  new DataColumn("Grade_Level"),
                  new DataColumn("Cost_Center"),
                  new DataColumn("Current_Position"),
                  new DataColumn("Current_Division"),
                  new DataColumn("Current_Department"),
                  new DataColumn("DIrect_Indirect_Labor"),
                  new DataColumn("Peza_Classification"),
                  new DataColumn("Birth_Date"),
                  new DataColumn("Date_Employed"),
                  new DataColumn("IsOT_Allowed"),
                  new DataColumn("Tax_Status"),
                  new DataColumn("Monthly_Basic_Salary"),
                  new DataColumn("PayoutScheme"),
                  new DataColumn("Additional_HDMF"),
                  new DataColumn("Payment_Type"),
                  new DataColumn("BPI_Account_Number"),
                  new DataColumn("Pagibig_ID"),
                  new DataColumn("SSS_ID"),
                  new DataColumn("TIN_ID"),
                  new DataColumn("Philhealth_ID"),
                  new DataColumn("Employment_Status"),
                  new DataColumn("Employment_Type"),
                  new DataColumn("Address_1"),
                  new DataColumn("Address_2"),
                  new DataColumn("Contact_Number"),
                  new DataColumn("Annual_RT"),
                  new DataColumn("Currency"),
                  new DataColumn("GL_Operating_Unit"),
                  new DataColumn("GL_Location"),
                  new DataColumn("GL_Business_Unit"),
                  new DataColumn("GL_DeptID"),
                  new DataColumn("GL_Project"),
                  new DataColumn("isSSS"),
                  new DataColumn("isPHIC"),
                  new DataColumn("isHDMF"),
                  new DataColumn("HasExited"),
                  new DataColumn("RateType"),
                  new DataColumn("MWE"),
                  new DataColumn("SalaryType")
                });
                for (int i = 0; i < param.Emp_ID.Length; i++)
                {
                    importList.Rows.Add(param.Emp_ID[i],
                  param.Last_Name[i],
                  param.First_Name[i],
                  param.Middle_Name[i],
                  param.Days_To_Credit[i],
                  param.IsRehired[i],
                  param.Gender[i],
                  param.Email[i],
                  param.Grade_Level[i],
                  param.Cost_Center[i],
                  param.Current_Position[i],
                  param.Current_Division[i],
                  param.Current_Department[i],
                  param.DIrect_Indirect_Labor[i],
                  param.Peza_Classification[i],
                  param.Birth_Date[i],
                  param.Date_Employed[i],
                  param.IsOT_Allowed[i],
                  param.Tax_Status[i],
                  param.Monthly_Basic_Salary[i],
                  param.PayoutScheme[i],
                  param.Additional_HDMF[i],
                  param.Payment_Type[i],
                  param.BPI_Account_Number[i],
                  param.Pagibig_ID[i],
                  param.SSS_ID[i],
                  param.TIN_ID[i],
                  param.Philhealth_ID[i],
                  param.Employment_Status[i],
                  param.Employment_Type[i],
                  param.Address_1[i],
                  param.Address_2[i],
                  param.Contact_Number[i],
                  param.Annual_RT[i],
                  param.Currency[i],
                  param.GL_Operating_Unit[i],
                  param.GL_Location[i],
                  param.GL_Business_Unit[i],
                  param.GL_DeptID[i],
                  param.GL_Project[i],
                  param.isPHIC[i],
                  param.isHDMF[i],
                  param.isSSS[i],
                  param.HasExited[i],
                  param.RateType[i],
                  param.MWE[i],
                  param.SalaryType[i]);
                }
                TempConnectionStringv4(param.db);
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@ClientCode", mytype = SqlDbType.VarChar, Value = param.ClientCode });
                con.myparameters.Add(new myParameters { ParameterName = "@Data", mytype = SqlDbType.Structured, Value = importList });
                con.myparameters.Add(new myParameters { ParameterName = "@LoginID", mytype = SqlDbType.VarChar, Value = param.LoginID });
                con.myparameters.Add(new myParameters { ParameterName = "@StartDate", mytype = SqlDbType.VarChar, Value = param.StartDate });
                con.myparameters.Add(new myParameters { ParameterName = "@EndDate", mytype = SqlDbType.VarChar, Value = param.EndDate });
                con.myparameters.Add(new myParameters { ParameterName = "@PayoutType", mytype = SqlDbType.VarChar, Value = param.PayoutType });
                DataTable DT = con.GetDataTable("sp_PayrollOutsourcing_Import_NewJoinersv2");
                foreach (DataRow row in DT.Rows)
                {
                    NewJoinersReturn NJR = new NewJoinersReturn();
                    NJR.EmpID = row["EmpID"].ToString();
                    NJR.Email = row["Email"].ToString();
                    NJR.GPass = row["GeneratedPass"].ToString();
                    list.Add(NJR);
                    //Send to email
                    string emailaccount = "durusthr@illimitado.com";
                    string emailpassword = "@1230Qwerty";
                    using (MailMessage mm = new MailMessage(emailaccount, row["Email"].ToString()))
                    {
                        MailAddress aliasmail = new MailAddress("durusthr@illimitado.com", "durusthr@illimitado.com");
                        MailAddress aliasreplymail = new MailAddress("durusthr@illimitado.com");
                        MailAddress aliasmailBCC = new MailAddress("durusthr@illimitado.com");
                        mm.From = aliasmail;
                        mm.Bcc.Add(aliasmailBCC);
                        mm.Subject = "Notifications";
                        mm.Body = "Please use this temporary password to login: " + row["GeneratedPass"].ToString();
                        mm.IsBodyHtml = true;
                        SmtpClient smtp = new SmtpClient();
                        smtp.Host = "smtp.office365.com";
                        smtp.EnableSsl = true;
                        NetworkCredential NetworkCred = new NetworkCredential(emailaccount, emailpassword);
                        smtp.UseDefaultCredentials = true;
                        smtp.Credentials = NetworkCred;
                        smtp.Port = 587;
                        smtp.Send(mm);
                    }
                }
                ret.DU = list;
                ret.myreturn = "Success";
            }
            catch (Exception e)
            {
                ret.DU = list;
                ret.myreturn = e.ToString();
            }
            return ret;
        }
        [HttpPost]
        [Route("ImportMultiCostCenter")]
        public string PO_ImportMultiCostCenter(Multiple_CostCenters main_Model)
        {
            try
            {
                DataTable importList = new DataTable();
                importList.Columns.AddRange(new DataColumn[4]
                {
                  new DataColumn("EmpID"),
                  new DataColumn("CostCode"),
                  new DataColumn("AllocationPerc"),
                  new DataColumn("Action")
                });
                for (int i = 0; i < main_Model.EmpID.Length; i++)
                {
                    importList.Rows.Add(main_Model.EmpID[i],
                  main_Model.CostCode[i],
                  main_Model.AllocationPerc[i],
                  main_Model.Action[i]);
                }
                TempConnectionStringv4(main_Model.db);
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@ClientCode", mytype = SqlDbType.VarChar, Value = main_Model.ClientCode });
                con.myparameters.Add(new myParameters { ParameterName = "@Data", mytype = SqlDbType.Structured, Value = importList });
                con.myparameters.Add(new myParameters { ParameterName = "@LoginID", mytype = SqlDbType.VarChar, Value = main_Model.LoginID });
                con.ExecuteNonQuery("sp_ImportOther");
                return "Success";
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }
        [HttpPost]
        [Route("PO_ImportDataChanges")]
        public string PO_ImportDataChanges(DataChanges_Model dataChanges_Model)
        {
            try
            {
                DataTable importList = new DataTable();
                importList.Columns.AddRange(new DataColumn[5]{
                    new DataColumn("Emp_ID"),
                    new DataColumn("ColumnType"),
                    new DataColumn("NewValue"),
                    new DataColumn("OldValue"),
                    new DataColumn("Remarks")
                });
                for (int i = 0; i < dataChanges_Model.Emp_ID.Length; i++)
                {
                    importList.Rows.Add(dataChanges_Model.Emp_ID[i], dataChanges_Model.ColumnType[i], dataChanges_Model.NewValue[i], dataChanges_Model.OldValue[i], dataChanges_Model.Remarks[i]);
                }
                TempConnectionStringv4(dataChanges_Model.db);
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@ClientCode", mytype = SqlDbType.VarChar, Value = dataChanges_Model.ClientCode });
                con.myparameters.Add(new myParameters { ParameterName = "@Data", mytype = SqlDbType.Structured, Value = importList });
                con.myparameters.Add(new myParameters { ParameterName = "@LoginID", mytype = SqlDbType.VarChar, Value = dataChanges_Model.LoginID });
                con.ExecuteNonQuery("sp_PayrollOutsourcing_Import_DataChanges");
                return "Success";
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }
        [Route("PO_ImportLeavers")]
        public string PO_ImportLeavers(Leavers_Model leavers_Model)
        {
            try
            {
                DataTable importList = new DataTable();
                importList.Columns.AddRange(new DataColumn[2]
                {
                    new DataColumn("Emp_ID"),
                    new DataColumn("Last_Employment_Date")
                });
                for (int i = 0; i < leavers_Model.Emp_ID.Length; i++)
                {
                    importList.Rows.Add(leavers_Model.Emp_ID[i], leavers_Model.Last_Employment_Date[i]);
                }
                TempConnectionStringv4(leavers_Model.db);
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@ClientCode", mytype = SqlDbType.VarChar, Value = leavers_Model.ClientCode });
                con.myparameters.Add(new myParameters { ParameterName = "@Data", mytype = SqlDbType.Structured, Value = importList });
                con.myparameters.Add(new myParameters { ParameterName = "@LoginID", mytype = SqlDbType.VarChar, Value = leavers_Model.LoginID });
                con.ExecuteNonQuery("sp_PayrollOutsourcing_Exiting");
                return "Success";
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }

        }
        [HttpPost]
        [Route("PO_Generate_PayrollRegister")]
        public string PO_Generate_PayrollRegister()
        {
            try
            {
                TempConnectionString();
                Connection con = new Connection();
                con.ExecuteNonQuery("sp_PayrollOutsourcing_GeneratePayrollReg_FirstStep");
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
            try
            {
                TempConnectionString();
                Connection con = new Connection();
                //con.myparameters.Add(new myParameters { ParameterName = "@Company", mytype = SqlDbType.VarChar, Value = payrollRegister_Model.Company });
                //con.myparameters.Add(new myParameters { ParameterName = "@PayOutDate", mytype = SqlDbType.VarChar, Value = payrollRegister_Model.PayoutDate });
                //con.myparameters.Add(new myParameters { ParameterName = "@PayOutSchemeID", mytype = SqlDbType.VarChar, Value = payrollRegister_Model.PayoutSchemesID});
                //con.myparameters.Add(new myParameters { ParameterName = "@PaymentTermsID", mytype = SqlDbType.VarChar, Value = payrollRegister_Model.PaymentTermsID});
                //con.myparameters.Add(new myParameters { ParameterName = "@CutOffStartDate", mytype = SqlDbType.VarChar, Value = payrollRegister_Model.CutoffStartDate});
                //con.myparameters.Add(new myParameters { ParameterName = "@CutOffEndDate", mytype = SqlDbType.VarChar, Value = payrollRegister_Model.CutoffEndDate});
                //con.myparameters.Add(new myParameters { ParameterName = "@IsAnnualized", mytype = SqlDbType.VarChar, Value = payrollRegister_Model.IsAnnualized });
                //con.myparameters.Add(new myParameters { ParameterName = "@ProcessedBy", mytype = SqlDbType.VarChar, Value = payrollRegister_Model.ProcessedBy });
                con.ExecuteNonQuery("sp_Payroll_ComputePayroll");

            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
            try
            {
                TempConnectionString();
                Connection con = new Connection();
                DataTable EmpData = con.GetDataTable("sp_PayrollOutsourcing_GenerateTest");

                try
                {
                    string dataDir = HostingEnvironment.MapPath("~/Files/");
                    FileStream stream = new FileStream(dataDir + "GeneratePayreg_Test_PO.xls", FileMode.Open);
                    string filename;
                    string datebuild = DateTime.Now.ToString("MMddyyyy");
                    string timebuild = DateTime.Now.ToString("hh-mmtt");

                    filename = "PayrollRegister_" + datebuild + timebuild + ".xls";
                    using (var fileStream = new FileStream(dataDir + filename, FileMode.Create, FileAccess.Write))
                    {
                        stream.CopyTo(fileStream);
                    }
                    stream.Close();

                    ExcelWorkbookFormat workbookFormat = ExcelWorkbookFormat.Xls_2003;
                    string testDocFile = dataDir + "GeneratePayreg_Test_PO.xls";
                    ExcelWorkbook workbook = new ExcelWorkbook(testDocFile);
                    ExcelWorksheet excelWorksheet = workbook.Worksheets[0];
                    if (EmpData.Rows.Count > 0)
                    {

                    }
                    int cellNum = 2;
                    for (int i = 0; i < EmpData.Rows.Count; i++)
                    {
                        excelWorksheet["A" + cellNum].Text = EmpData.Rows[i][0].ToString();
                        excelWorksheet["B" + cellNum].Text = EmpData.Rows[i][1].ToString();
                        excelWorksheet["C" + cellNum].Text = EmpData.Rows[i][2].ToString();
                        excelWorksheet["D" + cellNum].Text = EmpData.Rows[i][3].ToString();
                        excelWorksheet["E" + cellNum].Text = EmpData.Rows[i][4].ToString();
                        excelWorksheet["F" + cellNum].Text = EmpData.Rows[i][5].ToString();
                        excelWorksheet["G" + cellNum].Text = EmpData.Rows[i][6].ToString();
                        excelWorksheet["H" + cellNum].Text = EmpData.Rows[i][7].ToString();
                        excelWorksheet["I" + cellNum].Text = EmpData.Rows[i][8].ToString();
                        excelWorksheet["J" + cellNum].Text = EmpData.Rows[i][9].ToString();
                        excelWorksheet["K" + cellNum].Text = EmpData.Rows[i][10].ToString();
                        excelWorksheet["L" + cellNum].Text = EmpData.Rows[i][11].ToString();
                        excelWorksheet["M" + cellNum].Text = EmpData.Rows[i][12].ToString();
                        excelWorksheet["N" + cellNum].Text = EmpData.Rows[i][13].ToString();
                        excelWorksheet["O" + cellNum].Text = EmpData.Rows[i][14].ToString();
                        excelWorksheet["P" + cellNum].Text = EmpData.Rows[i][15].ToString();
                        excelWorksheet["Q" + cellNum].Text = EmpData.Rows[i][16].ToString();
                        excelWorksheet["R" + cellNum].Text = EmpData.Rows[i][17].ToString();
                        excelWorksheet["S" + cellNum].Text = EmpData.Rows[i][18].ToString();
                        excelWorksheet["T" + cellNum].Text = EmpData.Rows[i][19].ToString();
                        excelWorksheet["U" + cellNum].Text = EmpData.Rows[i][20].ToString();
                        excelWorksheet["V" + cellNum].Text = EmpData.Rows[i][21].ToString();
                        excelWorksheet["W" + cellNum].Text = EmpData.Rows[i][22].ToString();
                        excelWorksheet["X" + cellNum].Text = EmpData.Rows[i][23].ToString();
                        excelWorksheet["Y" + cellNum].Text = EmpData.Rows[i][24].ToString();
                        excelWorksheet["Z" + cellNum].Text = EmpData.Rows[i][25].ToString();
                        excelWorksheet["AA" + cellNum].Text = EmpData.Rows[i][26].ToString();
                        excelWorksheet["AB" + cellNum].Text = EmpData.Rows[i][27].ToString();
                        excelWorksheet["AC" + cellNum].Text = EmpData.Rows[i][28].ToString();
                        excelWorksheet["AD" + cellNum].Text = EmpData.Rows[i][29].ToString();
                        excelWorksheet["AE" + cellNum].Text = EmpData.Rows[i][30].ToString();
                        excelWorksheet["AF" + cellNum].Text = EmpData.Rows[i][31].ToString();
                        excelWorksheet["AG" + cellNum].Text = EmpData.Rows[i][32].ToString();
                        excelWorksheet["AH" + cellNum].Text = EmpData.Rows[i][33].ToString();
                        excelWorksheet["AI" + cellNum].Text = EmpData.Rows[i][34].ToString();
                        excelWorksheet["AJ" + cellNum].Text = EmpData.Rows[i][35].ToString();
                        excelWorksheet["AK" + cellNum].Text = EmpData.Rows[i][36].ToString();
                        excelWorksheet["AL" + cellNum].Text = EmpData.Rows[i][37].ToString();
                        cellNum++;
                    }
                    HttpResponse httpResponse = HttpContext.Current.Response;
                    httpResponse.Clear();
                    httpResponse.ContentType = "Application/x-msexcel";
                    httpResponse.AddHeader("Content-Disposition", String.Format("attachment; filename={0}", filename));
                    string newDIR = HttpContext.Current.Server.MapPath("~/sFTP/PayrollOutsourcing/");
                    string onlineDIR = "http://ws.durusthr.com/ilm_ws_live/sFTP/PayrollOutsourcing/";
                    workbook.Save(newDIR + filename);
                    return onlineDIR + filename;
                }
                catch (Exception ex)
                {
                    return ex.ToString();
                }
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }
        #endregion
        #region original Version
        [HttpPost]
        [Route("PO_ImportTimeAttendance")]
        public string PO_ImportTimeAttendance(TimeAttendance timeAttendance)
        {
            try
            {
                DataTable importList = new DataTable();
                importList.Columns.AddRange(new DataColumn[44] {
                    new DataColumn("Emp_ID"),
                    new DataColumn("First_Name"),
                    new DataColumn("Last_Name"),
                    new DataColumn("Branch_Code"),
                    new DataColumn("Reg_Days_Work"),
                    new DataColumn("Tardy_Hrs"),
                    new DataColumn("Undertime_Hrs"),
                    new DataColumn("Absent_Days"),
                    new DataColumn("LWOP_Days"),
                    new DataColumn("VL_Days"),
                    new DataColumn("SL_Days"),
                    new DataColumn("Other_Leaves_Days"),
                    new DataColumn("Night_Diff"),
                    new DataColumn("Regular_OT"),
                    new DataColumn("Regular_OT_NP"),
                    new DataColumn("Legal_Hol_OT"),
                    new DataColumn("Legal_Hol_OT_NP"),
                    new DataColumn("Legal_Hol_OT_EHrs"),
                    new DataColumn("Legal_Hol_OT_EHrs_NP"),
                    new DataColumn("Legal_Hol_RD"),
                    new DataColumn("Legal_Hol_RD_NP"),
                    new DataColumn("Legal_Hol_RD_EHrs"),
                    new DataColumn("Legal_Hol_RD_EHrs_NP"),
                    new DataColumn("Special_Hol_OT"),
                    new DataColumn("Special_Hol_OT_NP"),
                    new DataColumn("Special_Hol_OT_EHrs"),
                    new DataColumn("Special_Hol_OT_Ehrs_NP"),
                    new DataColumn("Special_Hol_RD"),
                    new DataColumn("Special_Hol_RD_NP"),
                    new DataColumn("Special_Hol_RD_EHrs"),
                    new DataColumn("Special_Hol_RD_EHrs_NP"),
                    new DataColumn("RestDay_OT"),
                    new DataColumn("RestDay_OT_NP"),
                    new DataColumn("RestDay_OT_EHrs"),
                    new DataColumn("RestDay_OT_EHrs_NP"),
                    new DataColumn("Double_Hol_OT"),
                    new DataColumn("Double_Hol_OT_NP"),
                    new DataColumn("Double_Hol_OT_EHrs"),
                    new DataColumn("Double_Hol_OT_EHrs_NP"),
                    new DataColumn("Double_Hol_RD"),
                    new DataColumn("Double_Hol_RD_NP"),
                    new DataColumn("Double_Hol_RD_EHrs"),
                    new DataColumn("Double_Hol_RD_EHrs_NP"),
                    new DataColumn("Remarks")});
                for (int i = 0; i < timeAttendance.TimeAttendanceDatas.Count; i++)
                {
                    importList.Rows.Add(
                        timeAttendance.TimeAttendanceDatas[i].Emp_ID,
                        timeAttendance.TimeAttendanceDatas[i].First_Name,
                        timeAttendance.TimeAttendanceDatas[i].Last_Name,
                        timeAttendance.TimeAttendanceDatas[i].Branch_Code,
                        timeAttendance.TimeAttendanceDatas[i].Reg_Days_Work,
                        timeAttendance.TimeAttendanceDatas[i].Tardy_Hrs,
                        timeAttendance.TimeAttendanceDatas[i].Undertime_Hrs,
                        timeAttendance.TimeAttendanceDatas[i].Absent_Days,
                        timeAttendance.TimeAttendanceDatas[i].LWOP_Days,
                        timeAttendance.TimeAttendanceDatas[i].VL_Days,
                        timeAttendance.TimeAttendanceDatas[i].SL_Days,
                        timeAttendance.TimeAttendanceDatas[i].Other_Leaves_Days,
                        timeAttendance.TimeAttendanceDatas[i].Night_Diff,
                        timeAttendance.TimeAttendanceDatas[i].Regular_OT,
                        timeAttendance.TimeAttendanceDatas[i].Regular_OT_NP,
                        timeAttendance.TimeAttendanceDatas[i].Legal_Hol_OT,
                        timeAttendance.TimeAttendanceDatas[i].Legal_Hol_OT_NP,
                        timeAttendance.TimeAttendanceDatas[i].Legal_Hol_OT_EHrs,
                        timeAttendance.TimeAttendanceDatas[i].Legal_Hol_OT_EHrs_NP,
                        timeAttendance.TimeAttendanceDatas[i].Legal_Hol_RD,
                        timeAttendance.TimeAttendanceDatas[i].Legal_Hol_RD_NP,
                        timeAttendance.TimeAttendanceDatas[i].Legal_Hol_RD_EHrs,
                        timeAttendance.TimeAttendanceDatas[i].Legal_Hol_RD_EHrs_NP,
                        timeAttendance.TimeAttendanceDatas[i].Special_Hol_OT,
                        timeAttendance.TimeAttendanceDatas[i].Special_Hol_OT_NP,
                        timeAttendance.TimeAttendanceDatas[i].Special_Hol_OT_EHrs,
                        timeAttendance.TimeAttendanceDatas[i].Special_Hol_OT_Ehrs_NP,
                        timeAttendance.TimeAttendanceDatas[i].Special_Hol_RD,
                        timeAttendance.TimeAttendanceDatas[i].Special_Hol_RD_NP,
                        timeAttendance.TimeAttendanceDatas[i].Special_Hol_RD_EHrs,
                        timeAttendance.TimeAttendanceDatas[i].Special_Hol_RD_EHrs_NP,
                        timeAttendance.TimeAttendanceDatas[i].RestDay_OT,
                        timeAttendance.TimeAttendanceDatas[i].RestDay_OT_NP,
                        timeAttendance.TimeAttendanceDatas[i].RestDay_OT_EHrs,
                        timeAttendance.TimeAttendanceDatas[i].RestDay_OT_EHrs_NP,
                        timeAttendance.TimeAttendanceDatas[i].Double_Hol_OT,
                        timeAttendance.TimeAttendanceDatas[i].Double_Hol_OT_NP,
                        timeAttendance.TimeAttendanceDatas[i].Double_Hol_OT_EHrs,
                        timeAttendance.TimeAttendanceDatas[i].Double_Hol_OT_EHrs_NP,
                        timeAttendance.TimeAttendanceDatas[i].Double_Hol_RD,
                        timeAttendance.TimeAttendanceDatas[i].Double_Hol_RD_NP,
                        timeAttendance.TimeAttendanceDatas[i].Double_Hol_RD_EHrs,
                        timeAttendance.TimeAttendanceDatas[i].Double_Hol_RD_EHrs_NP,
                        timeAttendance.TimeAttendanceDatas[i].Remarks);
                }
                TempConnectionString();
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@StartDate", mytype = SqlDbType.VarChar, Value = timeAttendance.StartDate });
                con.myparameters.Add(new myParameters { ParameterName = "@EndDate", mytype = SqlDbType.VarChar, Value = timeAttendance.EndDate });
                con.myparameters.Add(new myParameters { ParameterName = "@ifAdj", mytype = SqlDbType.VarChar, Value = timeAttendance.ifAdj });
                con.myparameters.Add(new myParameters { ParameterName = "@Data", mytype = SqlDbType.Structured, Value = importList });
                con.ExecuteNonQuery("sp_PayrollOutsourcing_Import_TimeAttendance");
                return "success";
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }
        [HttpPost]
        [Route("PO_ImportIncomeDeductions")]
        public string PO_ImportIncomeDeductions(IncomeDeduction incomeDeduction)
        {
            try
            {
                DataTable importList = new DataTable();
                importList.Columns.AddRange(new DataColumn[8] {
                new DataColumn("Emp_ID"),
                new DataColumn("Date"),
                new DataColumn("Code"),
                new DataColumn("Amount"),
                new DataColumn("Recur_Start_Date"),
                new DataColumn("Recur_End_Date"),
                new DataColumn("Frequency"),
                new DataColumn("Remarks")});

                for (int i = 0; i < incomeDeduction.incomeDeductionDatas.Count; i++)
                {
                    importList.Rows.Add(incomeDeduction.incomeDeductionDatas[i].Emp_ID,
                        incomeDeduction.incomeDeductionDatas[i].Date,
                        incomeDeduction.incomeDeductionDatas[i].Code,
                        incomeDeduction.incomeDeductionDatas[i].Amount,
                        incomeDeduction.incomeDeductionDatas[i].Recur_Start_Date,
                        incomeDeduction.incomeDeductionDatas[i].Recur_End_Date,
                        incomeDeduction.incomeDeductionDatas[i].Frequency,
                        incomeDeduction.incomeDeductionDatas[i].Remarks);
                }
                TempConnectionString();
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@StartDate", mytype = SqlDbType.VarChar, Value = incomeDeduction.StartDate });
                con.myparameters.Add(new myParameters { ParameterName = "@EndDate", mytype = SqlDbType.VarChar, Value = incomeDeduction.EndDate });
                con.myparameters.Add(new myParameters { ParameterName = "@IncomeOrDeduction", mytype = SqlDbType.VarChar, Value = incomeDeduction.IncomeOrDeduction });
                con.myparameters.Add(new myParameters { ParameterName = "@Data", mytype = SqlDbType.Structured, Value = importList });
                con.myparameters.Add(new myParameters { ParameterName = "@PayoutType", mytype = SqlDbType.VarChar, Value = incomeDeduction.PayoutType });
                con.ExecuteNonQuery("sp_PayrollOutsourcing_Import_IncomeDeduction");
                return "success";
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }
        [HttpPost]
        [Route("PO_ImportLoanRegister")]
        public string PO_ImportLoanRegister(LoanRegister loanRegister)
        {
            try
            {
                DataTable importList = new DataTable();
                importList.Columns.AddRange(new DataColumn[13]
                {
                    new DataColumn("Emp_ID"),
                    new DataColumn("Loan_Code"),
                    new DataColumn("Principal_Amount"),
                    new DataColumn("Total_Payments"),
                    new DataColumn("Loan_Starting_Deductions_Date"),
                    new DataColumn("Total_Previous_Payment"),
                    new DataColumn("Monthly_Loan_Amort_Amount"),
                    new DataColumn("Loan_Amort_Frequency"),
                    new DataColumn("Loan_Maturity_Date"),
                    new DataColumn("Date_Loan_Granted"),
                    new DataColumn("Account_Number"),
                    new DataColumn("Remarks"),
                    new DataColumn("Loan_Amount"),
                });
                for (int i = 0; i < loanRegister.loanRegisterDatas.Count; i++)
                {
                    importList.Rows.Add(loanRegister.loanRegisterDatas[i].Emp_ID,
                        loanRegister.loanRegisterDatas[i].Loan_Code,
                        loanRegister.loanRegisterDatas[i].Principal_Amount,
                        loanRegister.loanRegisterDatas[i].Total_Payments,
                        loanRegister.loanRegisterDatas[i].Loan_Starting_Deductions_Date,
                        loanRegister.loanRegisterDatas[i].Total_Previous_Payment,
                        loanRegister.loanRegisterDatas[i].Monthly_Loan_Amort_Amount,
                        loanRegister.loanRegisterDatas[i].Loan_Amort_Frequency,
                        loanRegister.loanRegisterDatas[i].Loan_Maturity_Date,
                        loanRegister.loanRegisterDatas[i].Date_Loan_Granted,
                        loanRegister.loanRegisterDatas[i].Account_Number,
                        loanRegister.loanRegisterDatas[i].Remarks,
                        loanRegister.loanRegisterDatas[i].Loan_Amount);
                }
                TempConnectionString();
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@StartDate", mytype = SqlDbType.VarChar, Value = loanRegister.StartDate });
                con.myparameters.Add(new myParameters { ParameterName = "@EndDate", mytype = SqlDbType.VarChar, Value = loanRegister.EndDate });
                con.myparameters.Add(new myParameters { ParameterName = "@Data", mytype = SqlDbType.Structured, Value = importList });
                con.myparameters.Add(new myParameters { ParameterName = "@PayoutType", mytype = SqlDbType.VarChar, Value = loanRegister.PayoutType });
                con.ExecuteNonQuery("sp_PayrollOutsourcing_Import_LoanRegister");

                return "Success";
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }
        #endregion
        #region Temporary and To Be Converted to .Net Core
        [Route("PO_CalculatePayroll")]
        public string CalculatePayroll(CalculateParam param)
        {
            try
            {
                TempConnectionStringv4(param.db);
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@CutoffStartDate", mytype = SqlDbType.NVarChar, Value = param.CutoffStart });
                con.myparameters.Add(new myParameters { ParameterName = "@CutOffEndDate", mytype = SqlDbType.NVarChar, Value = param.CutoffEnd });
                con.myparameters.Add(new myParameters { ParameterName = "@EmployeeStatus", mytype = SqlDbType.VarChar, Value = param.EmpStatus });
                con.myparameters.Add(new myParameters { ParameterName = "@TenantID", mytype = SqlDbType.VarChar, Value = param.TenantID });
                con.ExecuteNonQuery("sp_ComputeLogHours");
                return "Success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }
        [Route("PO_CalculatePayrollv2")]
        public string CalculatePayrollv2(CalculateParam param)
        {
            try
            {
                DataTable currData = new DataTable();
                currData.Columns.AddRange(new DataColumn[2] { new DataColumn("currency"), new DataColumn("rate") });
                for (int i = 0; i < param.currency.Length; i++)
                {
                    currData.Rows.Add(param.currency[i], param.rate[i]);
                }
                TempConnectionStringv4(param.db);
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@CutoffStartDate", mytype = SqlDbType.NVarChar, Value = param.CutoffStart });
                con.myparameters.Add(new myParameters { ParameterName = "@CutOffEndDate", mytype = SqlDbType.NVarChar, Value = param.CutoffEnd });
                con.myparameters.Add(new myParameters { ParameterName = "@EmployeeStatus", mytype = SqlDbType.VarChar, Value = param.EmpStatus });
                con.myparameters.Add(new myParameters { ParameterName = "@TenantID", mytype = SqlDbType.VarChar, Value = param.TenantID });
                con.myparameters.Add(new myParameters { ParameterName = "@UseCurrency", mytype = SqlDbType.VarChar, Value = param.currStatus });
                con.myparameters.Add(new myParameters { ParameterName = "@CurrencyRate", mytype = SqlDbType.Structured, Value = currData });
                con.ExecuteNonQuery("sp_ComputeLogHoursv2");
                return "Success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }


        [HttpPost]
        [Route("PO_CalculatePayrollSpecial")]
        public string CalculatePayrollSpecial(CalculateSpecialParam param)
        {
            try
            {
                DataTable EmpIDs = new DataTable();
                EmpIDs.Columns.AddRange(new DataColumn[1]
                {
                    new DataColumn("EmpID")
                });
                for (int i = 0; i < param.EmpID.Length; i++)
                {
                    EmpIDs.Rows.Add(param.EmpID[i]);
                }
                TempConnectionStringv4(param.db);
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@CutoffStartDate", mytype = SqlDbType.NVarChar, Value = param.CutoffStart });
                con.myparameters.Add(new myParameters { ParameterName = "@CutOffEndDate", mytype = SqlDbType.NVarChar, Value = param.CutoffEnd });
                con.myparameters.Add(new myParameters { ParameterName = "@EmployeeStatus", mytype = SqlDbType.NVarChar, Value = param.EmpStatus });
                con.myparameters.Add(new myParameters { ParameterName = "@TenantID", mytype = SqlDbType.NVarChar, Value = param.TenantID });
                con.myparameters.Add(new myParameters { ParameterName = "@UserID", mytype = SqlDbType.Structured, Value = EmpIDs });
                con.ExecuteNonQuery("sp_ComputeLogHours_SelectedEmp");
                return "Success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }
        [Route("PO_ExtractPayReg")]
        public string ExtractReport(efile param)
        {
            FileStream stream = null;
            try
            {
                DataTable DTParam = new DataTable();
                //DTParam.Columns.AddRange(new DataColumn[1] { new DataColumn("startingids") });
                //for (int i = 0; i < Params.StartingIDS.Length; i++)
                //{
                //    DTParam.Rows.Add(Params.StartingIDS[i]);
                //}

                TempConnectionStringv4(param.db);
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@TenantID", mytype = SqlDbType.NVarChar, Value = param.TenantID });
                con.myparameters.Add(new myParameters { ParameterName = "@PayoutDate", mytype = SqlDbType.NVarChar, Value = param.PayoutDate });
                con.myparameters.Add(new myParameters { ParameterName = "@EmpStatus", mytype = SqlDbType.VarChar, Value = param.EmpStatus });
                DataTable PayRegData = con.GetDataTable("sp_Extract_PayReg");


                string dataDir = HostingEnvironment.MapPath("~/Files/PayrollOutSourcing/");

                stream = new FileStream(dataDir + "ExtractReport.xls", FileMode.Open);
                string filename;
                string datebuild = DateTime.Now.ToString("MMddyyyy");
                string timebuild = DateTime.Now.ToString("hh-mmtt");



                filename = datebuild + "_ExtractReport" + ".xls";
                using (var fileStream = new FileStream(dataDir + filename, FileMode.Create, FileAccess.Write))
                //using (var fileStream = new FileStream(dataDir + filename, FileMode.Create, FileAccess.Write))
                {
                    stream.CopyTo(fileStream);
                }
                stream.Close();
                ExcelWorkbookFormat workbookFormat = ExcelWorkbookFormat.Xls_2003;
                string testDocFile = dataDir + "ExtractReport.xls";
                ExcelWorkbook workbook = new ExcelWorkbook(testDocFile);
                ExcelWorksheet firstWorksheet = workbook.Worksheets[0];

                #region PayReg Sheet

                ExcelWorksheet PayReg = workbook.Worksheets[0];
                ExcelWorksheet sheet2 = workbook.Worksheets[1];
                ExcelWorksheet sheet3 = workbook.Worksheets[2];
                ExcelWorksheet sheet4 = workbook.Worksheets[3];
                ExcelWorksheet sheet5 = workbook.Worksheets[4];
                ExcelWorksheet sheet6 = workbook.Worksheets[5];
                ExcelWorksheet sheet7 = workbook.Worksheets[6];
                //Sheet7.WorksheetSecurity.ProtectWorksheet("sbc01");



                int x = 2;
                for (int i = 0; i < PayRegData.Rows.Count; i++, x++)
                {

                    PayReg["A" + x].Text = PayRegData.Rows[i]["PayrollNum"].ToString(); //payroll#
                    PayReg["B" + x].Text = PayRegData.Rows[i]["PayDate"].ToString(); //paydate
                    PayReg["C" + x].Text = PayRegData.Rows[i]["PayCode"].ToString(); //pay code
                    PayReg["D" + x].Text = PayRegData.Rows[i]["EmpID"].ToString(); //employee id
                    PayReg["E" + x].Text = PayRegData.Rows[i]["LastName"].ToString(); //last name
                    PayReg["F" + x].Text = PayRegData.Rows[i]["FirstName"].ToString(); //first name
                    PayReg["G" + x].Text = PayRegData.Rows[i]["FileStatus"].ToString(); //file status
                    PayReg["H" + x].Text = PayRegData.Rows[i]["TaxStatus"].ToString(); //tax status
                    PayReg["I" + x].Value = PayRegData.Rows[i]["DailyRate"]; //daily rate
                    PayReg["J" + x].Value = PayRegData.Rows[i]["SalaryRateType"]; //salary rate type
                    PayReg["K" + x].Value = PayRegData.Rows[i]["BasicSalary"].ToString(); //basic salary
                    PayReg["L" + x].Value = PayRegData.Rows[i]["MiscAmount"].ToString(); //misc amount
                    PayReg["M" + x].Value = PayRegData.Rows[i]["LeaveAmount"].ToString();
                    PayReg["N" + x].Value = PayRegData.Rows[i]["Overtime"]; //overtime
                    PayReg["O" + x].Value = PayRegData.Rows[i]["OtherTxIncome"].ToString(); //other tx income
                    PayReg["P" + x].Text = PayRegData.Rows[i]["Adjustments"].ToString(); //adjustments
                    PayReg["Q" + x].Value = PayRegData.Rows[i]["GrossIncome"]; //gross income
                    PayReg["R" + x].Value = PayRegData.Rows[i]["WithholdingTax"]; //withholding tax
                    PayReg["S" + x].Value = PayRegData.Rows[i]["NetSalaryAfterTax"]; //net salary after tax
                    PayReg["T" + x].Value = PayRegData.Rows[i]["EmployeeSSS"]; //employee sss
                    PayReg["U" + x].Value = PayRegData.Rows[i]["EmployeePhilHealth"]; //employee mcr(philhealth)
                    PayReg["V" + x].Value = PayRegData.Rows[i]["EmployeePagibig"]; //employee pagibig
                    PayReg["W" + x].Value = PayRegData.Rows[i]["OtherNtxIncome"]; //other ntx income
                    PayReg["X" + x].Value = PayRegData.Rows[i]["LoanPayments"]; //loan payments
                    PayReg["Y" + x].Value = PayRegData.Rows[i]["Deductions"]; //deductions
                    PayReg["Z" + x].Value = PayRegData.Rows[i]["NetSalary"]; //net salary
                    PayReg["AA" + x].Value = PayRegData.Rows[i]["EmployerSSS"]; //employer sss
                    PayReg["AB" + x].Value = PayRegData.Rows[i]["EmployerPhilhealth"]; //employer mcr(philhealth)
                    PayReg["AC" + x].Value = PayRegData.Rows[i]["EmployerEC"]; //employer ec
                    PayReg["AD" + x].Value = PayRegData.Rows[i]["EmployerPagibig"]; //employer pagibig
                    PayReg["AE" + x].Value = PayRegData.Rows[i]["PayrollCost"]; //payroll cost
                    PayReg["AF" + x].Value = PayRegData.Rows[i]["PreviousYTDGross"]; //payment type
                    PayReg["AG" + x].Value = PayRegData.Rows[i]["PreviousYTDWitholding"]; //bank acct#
                    PayReg["AH" + x].Value = PayRegData.Rows[i]["PreviousYTDSSS"]; //bank name
                    PayReg["AI" + x].Value = PayRegData.Rows[i]["PreviousYTDMCR"]; //date employed
                    PayReg["AJ" + x].Value = PayRegData.Rows[i]["PreviousYTDPagibig"]; //date terminated
                    PayReg["AK" + x].Value = PayRegData.Rows[i]["Previous13th"]; //cost center
                    PayReg["AL" + x].Text = PayRegData.Rows[i]["PaymentType"].ToString(); //currency
                    PayReg["AM" + x].Text = PayRegData.Rows[i]["BankAcc"].ToString(); //exchange rate
                    PayReg["AN" + x].Text = PayRegData.Rows[i]["TaxTable"].ToString(); //tax table
                    PayReg["AO" + x].Text = PayRegData.Rows[i]["BankName"].ToString(); //payment freq
                    PayReg["AP" + x].Text = PayRegData.Rows[i]["CommentField"].ToString();
                    PayReg["AQ" + x].Text = PayRegData.Rows[i]["ErrorField"].ToString();
                    PayReg["AR" + x].Text = PayRegData.Rows[i]["DateEmployed"].ToString();
                    PayReg["AS" + x].Text = PayRegData.Rows[i]["DateTerminated"].ToString();
                    PayReg["AT" + x].Text = PayRegData.Rows[i]["CostCenter"].ToString();
                    PayReg["AU" + x].Text = PayRegData.Rows[i]["Currency"].ToString();
                    PayReg["AV" + x].Text = PayRegData.Rows[i]["ExchangeRate"].ToString();
                    PayReg["AW" + x].Text = PayRegData.Rows[i]["TaxTable"].ToString();
                    PayReg["AX" + x].Text = PayRegData.Rows[i]["PaymentPreq"].ToString();
                    PayReg["AY" + x].Value = PayRegData.Rows[i]["MTDGross"];
                    PayReg["AZ" + x].Value = PayRegData.Rows[i]["MTDBasic"];
                    PayReg["BA" + x].Value = PayRegData.Rows[i]["MTDSSSEmployee"];
                    PayReg["BB" + x].Value = PayRegData.Rows[i]["MTDMCREmployee"];
                    PayReg["BC" + x].Value = PayRegData.Rows[i]["MTDPagIbigEmployee"];
                    PayReg["BD" + x].Value = PayRegData.Rows[i]["MTDSSSEmployer"];
                    PayReg["BE" + x].Value = PayRegData.Rows[i]["MTDMCREmployer"];
                    PayReg["BF" + x].Value = PayRegData.Rows[i]["MTDECEmployer"];
                    PayReg["BG" + x].Value = PayRegData.Rows[i]["MTDPabIbigEmployer"];
                    PayReg["BH" + x].Value = PayRegData.Rows[i]["MTDWHTAX"];
                    PayReg["BI" + x].Value = PayRegData.Rows[i]["MonthlyBasic"];
                    PayReg["BJ" + x].Value = PayRegData.Rows[i]["MonthlyAllow"];
                    PayReg["BK" + x].Value = PayRegData.Rows[i]["MTDNTX"];
                    PayReg["BL" + x].Text = PayRegData.Rows[i]["FROM"].ToString();
                    PayReg["BM" + x].Text = PayRegData.Rows[i]["TO"].ToString();




                }
                #endregion
                TempConnectionStringv4(param.db);
                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@TenantID", mytype = SqlDbType.NVarChar, Value = param.TenantID });
                con.myparameters.Add(new myParameters { ParameterName = "@PayoutDate", mytype = SqlDbType.NVarChar, Value = param.PayoutDate });
                con.myparameters.Add(new myParameters { ParameterName = "@EmpStatus", mytype = SqlDbType.VarChar, Value = param.EmpStatus });
                DataTable secondSheet = con.GetDataTable("sp_ExportSheet2");

                x = 2;
                for (int i = 0; i < secondSheet.Rows.Count; i++, x++)
                {
                    sheet2["A" + x].Text = secondSheet.Rows[i]["PayrollNum"].ToString();
                    sheet2["B" + x].Text = secondSheet.Rows[i]["PayOutDate"].ToString();
                    sheet2["C" + x].Text = secondSheet.Rows[i]["PayCode"].ToString();
                    sheet2["D" + x].Text = secondSheet.Rows[i]["EmployeeID"].ToString();
                    sheet2["E" + x].Text = secondSheet.Rows[i]["LastName"].ToString();
                    sheet2["F" + x].Text = secondSheet.Rows[i]["FirstName"].ToString();
                    sheet2["G" + x].Text = secondSheet.Rows[i]["FileStatus"].ToString();
                    sheet2["H" + x].Text = secondSheet.Rows[i]["TransactionType"].ToString();
                    sheet2["I" + x].Text = secondSheet.Rows[i]["Date"].ToString();
                    sheet2["J" + x].Text = secondSheet.Rows[i]["CostCenter"].ToString();
                    sheet2["K" + x].Text = secondSheet.Rows[i]["Days"].ToString();
                    sheet2["L" + x].Text = secondSheet.Rows[i]["Hours"].ToString();
                    sheet2["M" + x].Text = secondSheet.Rows[i]["Minutes"].ToString();
                    sheet2["N" + x].Value = secondSheet.Rows[i]["Amount"];
                    sheet2["O" + x].Text = secondSheet.Rows[i]["Multiplier"].ToString();
                    sheet2["P" + x].Text = secondSheet.Rows[i]["RecurStart"].ToString();
                    sheet2["Q" + x].Text = secondSheet.Rows[i]["RecurEnd"].ToString();
                    sheet2["R" + x].Text = secondSheet.Rows[i]["Freq"].ToString();
                    sheet2["S" + x].Text = secondSheet.Rows[i]["Remark"].ToString();
                    sheet2["T" + x].Text = secondSheet.Rows[i]["SystemDayTime"].ToString();
                    sheet2["U" + x].Text = secondSheet.Rows[i]["FROM"].ToString();
                    sheet2["V" + x].Text = secondSheet.Rows[i]["TO"].ToString();
                    sheet2["W" + x].Text = secondSheet.Rows[i]["JECOSTCENTER"].ToString();

                }

                TempConnectionStringv4(param.db);
                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@TenantID", mytype = SqlDbType.NVarChar, Value = param.TenantID });
                con.myparameters.Add(new myParameters { ParameterName = "@PayoutDate", mytype = SqlDbType.NVarChar, Value = param.PayoutDate });
                con.myparameters.Add(new myParameters { ParameterName = "@EmpStatus", mytype = SqlDbType.VarChar, Value = param.EmpStatus });
                DataTable thirdSheet = con.GetDataTable("sp_ExportSheet3");

                x = 2;
                for (int i = 0; i < thirdSheet.Rows.Count; i++, x++)
                {
                    sheet3["A" + x].Text = thirdSheet.Rows[i]["PayrollNum"].ToString();
                    sheet3["B" + x].Text = thirdSheet.Rows[i]["PayDate"].ToString();
                    sheet3["C" + x].Text = thirdSheet.Rows[i]["PayCode"].ToString();
                    sheet3["D" + x].Text = thirdSheet.Rows[i]["EmpID"].ToString();
                    sheet3["E" + x].Text = thirdSheet.Rows[i]["LastName"].ToString();
                    sheet3["F" + x].Text = thirdSheet.Rows[i]["FirstName"].ToString();
                    sheet3["G" + x].Text = thirdSheet.Rows[i]["FileStatus"].ToString();
                    sheet3["H" + x].Text = thirdSheet.Rows[i]["OTCode"].ToString();
                    sheet3["I" + x].Text = thirdSheet.Rows[i]["Date"].ToString();
                    sheet3["J" + x].Text = thirdSheet.Rows[i]["CostCenter"].ToString();
                    sheet3["K" + x].Value = thirdSheet.Rows[i]["OTRate"];
                    sheet3["L" + x].Value = thirdSheet.Rows[i]["OTHours"];
                    sheet3["M" + x].Value = thirdSheet.Rows[i]["OTMinutes"];
                    sheet3["N" + x].Value = thirdSheet.Rows[i]["OTAmount"];
                    sheet3["O" + x].Value = thirdSheet.Rows[i]["OTNPHours"];
                    sheet3["P" + x].Value = thirdSheet.Rows[i]["OTNPMinutes"];
                    sheet3["Q" + x].Value = thirdSheet.Rows[i]["OTNPAmount"];
                    sheet3["R" + x].Value = thirdSheet.Rows[i]["OTNP2Hours"];
                    sheet3["S" + x].Value = thirdSheet.Rows[i]["OTNP2Minutes"];
                    sheet3["T" + x].Value = thirdSheet.Rows[i]["OTNP2Amount"];
                    sheet3["U" + x].Value = thirdSheet.Rows[i]["OTandNPAmount"];
                    sheet3["V" + x].Text = thirdSheet.Rows[i]["RecurStart"].ToString();
                    sheet3["W" + x].Text = thirdSheet.Rows[i]["RecurEnd"].ToString();
                    sheet3["X" + x].Text = thirdSheet.Rows[i]["Freq"].ToString();
                    sheet3["Y" + x].Text = thirdSheet.Rows[i]["Remarks"].ToString();
                    sheet3["Z" + x].Text = thirdSheet.Rows[i]["SystemOfHours"].ToString();
                    sheet3["AA" + x].Text = thirdSheet.Rows[i]["SystemOTTime"].ToString();
                    sheet3["AB" + x].Text = thirdSheet.Rows[i]["SystemNPHours"].ToString();
                    sheet3["AC" + x].Text = thirdSheet.Rows[i]["SystemNPTime"].ToString();
                    sheet3["AD" + x].Text = thirdSheet.Rows[i]["From"].ToString();
                    sheet3["AE" + x].Text = thirdSheet.Rows[i]["To"].ToString();
                    sheet3["AF" + x].Text = thirdSheet.Rows[i]["JECostCenter"].ToString();




                }
                TempConnectionStringv4(param.db);
                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@TenantID", mytype = SqlDbType.NVarChar, Value = param.TenantID });
                con.myparameters.Add(new myParameters { ParameterName = "@PayoutDate", mytype = SqlDbType.NVarChar, Value = param.PayoutDate });
                con.myparameters.Add(new myParameters { ParameterName = "@EmpStatus", mytype = SqlDbType.VarChar, Value = param.EmpStatus });
                DataTable FSheet = con.GetDataTable("sp_ExportSheet4");

                x = 2;
                for (int i = 0; i < FSheet.Rows.Count; i++, x++)
                {
                    sheet4["A" + x].Text = FSheet.Rows[i]["PayrollNum"].ToString();
                    sheet4["B" + x].Text = FSheet.Rows[i]["PayDate"].ToString();
                    sheet4["C" + x].Text = FSheet.Rows[i]["PayCode"].ToString();
                    sheet4["D" + x].Text = FSheet.Rows[i]["EmployeeID"].ToString();
                    sheet4["E" + x].Text = FSheet.Rows[i]["LastName"].ToString();
                    sheet4["F" + x].Text = FSheet.Rows[i]["FirstName"].ToString();
                    sheet4["G" + x].Text = FSheet.Rows[i]["FileStatus"].ToString();
                    sheet4["H" + x].Text = FSheet.Rows[i]["IncomeCode"].ToString();
                    sheet4["I" + x].Text = FSheet.Rows[i]["Date"].ToString();
                    sheet4["J" + x].Value = FSheet.Rows[i]["Amount"];
                    sheet4["K" + x].Text = FSheet.Rows[i]["RecurStart"].ToString();
                    sheet4["L" + x].Text = FSheet.Rows[i]["RecurEnd"].ToString();
                    sheet4["M" + x].Text = FSheet.Rows[i]["Freq"].ToString();
                    sheet4["N" + x].Text = FSheet.Rows[i]["Remarks"].ToString();
                    sheet4["O" + x].Text = FSheet.Rows[i]["From"].ToString();
                    sheet4["P" + x].Text = FSheet.Rows[i]["To"].ToString();
                    sheet4["Q" + x].Text = FSheet.Rows[i]["JECostCenter"].ToString();

                }

                TempConnectionStringv4(param.db);
                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@TenantID", mytype = SqlDbType.NVarChar, Value = param.TenantID });
                con.myparameters.Add(new myParameters { ParameterName = "@PayoutDate", mytype = SqlDbType.NVarChar, Value = param.PayoutDate });
                con.myparameters.Add(new myParameters { ParameterName = "@EmpStatus", mytype = SqlDbType.VarChar, Value = param.EmpStatus });
                DataTable FifthSheet = con.GetDataTable("sp_ExportSheet5");

                x = 2;
                for (int i = 0; i < FifthSheet.Rows.Count; i++, x++)
                {
                    sheet5["A" + x].Text = FifthSheet.Rows[i]["PayrollNum"].ToString();
                    sheet5["B" + x].Text = FifthSheet.Rows[i]["PayDate"].ToString();
                    sheet5["C" + x].Text = FifthSheet.Rows[i]["PayCode"].ToString();
                    sheet5["D" + x].Text = FifthSheet.Rows[i]["EmployeeID"].ToString();
                    sheet5["E" + x].Text = FifthSheet.Rows[i]["LastName"].ToString();
                    sheet5["F" + x].Text = FifthSheet.Rows[i]["FirstName"].ToString();
                    sheet5["G" + x].Text = FifthSheet.Rows[i]["FileStatus"].ToString();
                    sheet5["H" + x].Text = FifthSheet.Rows[i]["IncomeCode"].ToString();
                    sheet5["I" + x].Text = FifthSheet.Rows[i]["Date"].ToString();
                    sheet5["J" + x].Value = FifthSheet.Rows[i]["Amount"];
                    sheet5["K" + x].Text = FifthSheet.Rows[i]["RecurStart"].ToString();
                    sheet5["L" + x].Text = FifthSheet.Rows[i]["RecurEnd"].ToString();
                    sheet5["M" + x].Text = FifthSheet.Rows[i]["Freq"].ToString();
                    sheet5["N" + x].Text = FifthSheet.Rows[i]["Remarks"].ToString();
                    sheet5["O" + x].Text = FifthSheet.Rows[i]["From"].ToString();
                    sheet5["P" + x].Text = FifthSheet.Rows[i]["To"].ToString();
                    sheet5["Q" + x].Text = FifthSheet.Rows[i]["JECostCenter"].ToString();



                }
                TempConnectionStringv4(param.db);
                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@TenantID", mytype = SqlDbType.NVarChar, Value = param.TenantID });
                con.myparameters.Add(new myParameters { ParameterName = "@PayoutDate", mytype = SqlDbType.NVarChar, Value = param.PayoutDate });
                con.myparameters.Add(new myParameters { ParameterName = "@EmpStatus", mytype = SqlDbType.VarChar, Value = param.EmpStatus });
                DataTable SixSheet = con.GetDataTable("sp_ExportSheet6");

                x = 2;
                for (int i = 0; i < SixSheet.Rows.Count; i++, x++)
                {
                    sheet6["A" + x].Text = SixSheet.Rows[i]["PayrollNum"].ToString();
                    sheet6["B" + x].Text = SixSheet.Rows[i]["PayDate"].ToString();
                    sheet6["C" + x].Text = SixSheet.Rows[i]["PayCode"].ToString();
                    sheet6["D" + x].Text = SixSheet.Rows[i]["EmployeeID"].ToString();
                    sheet6["E" + x].Text = SixSheet.Rows[i]["LastName"].ToString();
                    sheet6["F" + x].Text = SixSheet.Rows[i]["FirstName"].ToString();
                    sheet6["G" + x].Text = SixSheet.Rows[i]["FileStatus"].ToString();
                    sheet6["H" + x].Text = SixSheet.Rows[i]["LoanCode"].ToString();
                    sheet6["I" + x].Text = SixSheet.Rows[i]["LoanDateStart"].ToString();
                    sheet6["J" + x].Value = SixSheet.Rows[i]["LoanPrincipal"];
                    sheet6["K" + x].Value = SixSheet.Rows[i]["LoanAmount"];
                    sheet6["L" + x].Text = SixSheet.Rows[i]["LoanPayments"].ToString();
                    sheet6["M" + x].Value = SixSheet.Rows[i]["LoanBalance"];
                    sheet6["N" + x].Text = SixSheet.Rows[i]["LoanDeductionAMT"].ToString();
                    sheet6["O" + x].Text = SixSheet.Rows[i]["LoanDeductionFreq"].ToString();
                    sheet6["P" + x].Text = SixSheet.Rows[i]["LoanStatus"].ToString();
                    sheet6["Q" + x].Text = SixSheet.Rows[i]["LoanDateGranted"].ToString();
                    sheet6["R" + x].Text = SixSheet.Rows[i]["From"].ToString();
                    sheet6["S" + x].Text = SixSheet.Rows[i]["To"].ToString();
                    sheet6["T" + x].Text = SixSheet.Rows[i]["JECostCenter"].ToString();


                }

                TempConnectionStringv4(param.db);
                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@TenantID", mytype = SqlDbType.NVarChar, Value = param.TenantID });
                con.myparameters.Add(new myParameters { ParameterName = "@PayoutDate", mytype = SqlDbType.NVarChar, Value = param.PayoutDate });
                con.myparameters.Add(new myParameters { ParameterName = "@EmpStatus", mytype = SqlDbType.VarChar, Value = param.EmpStatus });
                DataTable SevenSheet = con.GetDataTable("sp_Exportsheet7");

                x = 2;
                for (int i = 0; i < SevenSheet.Rows.Count; i++, x++)
                {
                    sheet7["A" + x].Text = SevenSheet.Rows[i]["PayrollNum"].ToString();
                    sheet7["B" + x].Text = SevenSheet.Rows[i]["PayDate"].ToString();
                    sheet7["C" + x].Text = SevenSheet.Rows[i]["PayCode"].ToString();
                    sheet7["D" + x].Text = SevenSheet.Rows[i]["EmployeeID"].ToString();
                    sheet7["E" + x].Text = SevenSheet.Rows[i]["LastName"].ToString();
                    sheet7["F" + x].Text = SevenSheet.Rows[i]["FirstName"].ToString();
                    sheet7["G" + x].Text = SevenSheet.Rows[i]["FileStatus"].ToString();
                    sheet7["H" + x].Text = SevenSheet.Rows[i]["DeductionCode"].ToString();
                    sheet7["I" + x].Text = SevenSheet.Rows[i]["Date"].ToString();
                    sheet7["J" + x].Value = SevenSheet.Rows[i]["DeductionAmount"];
                    sheet7["K" + x].Text = SevenSheet.Rows[i]["DeductionFreq"].ToString();
                    sheet7["L" + x].Text = SevenSheet.Rows[i]["LastDeductionDate"].ToString();
                    sheet7["M" + x].Text = SevenSheet.Rows[i]["From"].ToString();
                    sheet7["N" + x].Text = SevenSheet.Rows[i]["To"].ToString();
                    sheet7["O" + x].Text = SevenSheet.Rows[i]["JECostCenter"].ToString();



                }
                HttpResponse httpResponse = System.Web.HttpContext.Current.Response;

                httpResponse.Clear();
                httpResponse.ContentType = "Application/x-msexcel";
                httpResponse.AddHeader("Content-Disposition", String.Format("attachment; filename={0}", filename));
                string newDIR = HttpContext.Current.Server.MapPath("~/sFTP/PayrollOutsourcing/");
                string onlineDIR = "http://ws.durusthr.com/ILM_WS_Live/sFTP/PayrollOutsourcing/";
                //filename = filename.Replace(',', '_');
                //filename = filename.Replace(" ", String.Empty);
                workbook.Save(newDIR + filename);
                stream.Close();
                //return "Success";

                if (PayRegData.Rows.Count > 0)
                {
                    return onlineDIR + filename;
                }
                else
                {

                    return "No Rows";
                }
            }
            catch (Exception e)
            {
                stream.Close();
                return "Error: " + e.ToString();
            }
        }

        [Route("PO_ExtractPayRegv2")]
        public string ExtractReportv2(efile param)
        {

            FileStream stream = null;
            try
            {
                DataTable DTParam = new DataTable();
                //DTParam.Columns.AddRange(new DataColumn[1] { new DataColumn("startingids") });
                //for (int i = 0; i < Params.StartingIDS.Length; i++)
                //{
                //    DTParam.Rows.Add(Params.StartingIDS[i]);
                //}

                TempConnectionStringv2();
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@TenantID", mytype = SqlDbType.NVarChar, Value = param.TenantID });
                con.myparameters.Add(new myParameters { ParameterName = "@CutoffStart", mytype = SqlDbType.NVarChar, Value = param.CutoffStart });
                con.myparameters.Add(new myParameters { ParameterName = "@CutoffEnd", mytype = SqlDbType.NVarChar, Value = param.CutoffEnd });
                con.myparameters.Add(new myParameters { ParameterName = "@EmpStatus", mytype = SqlDbType.VarChar, Value = param.EmpStatus });
                DataTable PayRegData = con.GetDataTable("sp_Extract_PayRegv1");


                string dataDir = HostingEnvironment.MapPath("~/Files/PayrollOutSourcing/");

                stream = new FileStream(dataDir + "ExtractReport.xls", FileMode.Open);
                string filename;
                // string datebuild = DateTime.Now.ToString("MMddyyyy");
                string cc = "";
                if (PayRegData.Rows.Count != 0)
                {
                    cc = PayRegData.Rows[0]["Ccode"].ToString();
                }

                string cStart = Regex.Replace(param.CutoffStart, @"[^0-9a-zA-Z]+", "");
                string cEnd = Regex.Replace(param.CutoffEnd, @"[^0-9a-zA-Z]+", "");


                string datebuild = DateTime.Now.ToString("yyyyMM");
                string timebuild = DateTime.Now.ToString("hhtt");
                filename = cc + "_PayReg_" + cStart + "_" + cEnd + "_" + timebuild + ".xls";
                using (var fileStream = new FileStream(dataDir + filename, FileMode.Create, FileAccess.Write))
                //using (var fileStream = new FileStream(dataDir + filename, FileMode.Create, FileAccess.Write))
                {
                    stream.CopyTo(fileStream);
                }
                stream.Close();
                ExcelWorkbookFormat workbookFormat = ExcelWorkbookFormat.Xls_2003;
                string testDocFile = dataDir + "ExtractReport.xls";
                ExcelWorkbook workbook = new ExcelWorkbook(testDocFile);
                ExcelWorksheet firstWorksheet = workbook.Worksheets[0];

                #region PayReg Sheet

                ExcelWorksheet PayReg = workbook.Worksheets[0];
                ExcelWorksheet sheet2 = workbook.Worksheets[1];
                ExcelWorksheet sheet3 = workbook.Worksheets[2];
                ExcelWorksheet sheet4 = workbook.Worksheets[3];
                ExcelWorksheet sheet5 = workbook.Worksheets[4];
                ExcelWorksheet sheet6 = workbook.Worksheets[5];
                ExcelWorksheet sheet7 = workbook.Worksheets[6];
                //Sheet7.WorksheetSecurity.ProtectWorksheet("sbc01");
                //AX TAXTABLE

                int x = 2;
                for (int i = 0; i < PayRegData.Rows.Count; i++, x++)
                {

                    PayReg["A" + x].Text = PayRegData.Rows[i]["PayrollNum"].ToString(); //payroll#
                    PayReg["B" + x].Text = PayRegData.Rows[i]["PayDate"].ToString(); //paydate
                    PayReg["C" + x].Text = PayRegData.Rows[i]["PayCode"].ToString(); //pay code
                    PayReg["D" + x].Text = PayRegData.Rows[i]["EmpID"].ToString(); //employee id
                    PayReg["E" + x].Text = PayRegData.Rows[i]["LastName"].ToString(); //last name
                    PayReg["F" + x].Text = PayRegData.Rows[i]["FirstName"].ToString(); //first name
                    PayReg["G" + x].Text = PayRegData.Rows[i]["FileStatus"].ToString(); //file status
                    PayReg["H" + x].Text = PayRegData.Rows[i]["TaxStatus"].ToString(); //tax status
                    PayReg["I" + x].Value = PayRegData.Rows[i]["DailyRate"]; //daily rate
                    PayReg["J" + x].Value = PayRegData.Rows[i]["SalaryRateType"]; //salary rate type
                    PayReg["K" + x].Value = PayRegData.Rows[i]["BasicSalary"]; //basic salary
                    PayReg["L" + x].Value = PayRegData.Rows[i]["MiscAmount"]; //misc amount
                    PayReg["M" + x].Value = PayRegData.Rows[i]["LeaveAmount"];
                    PayReg["N" + x].Value = PayRegData.Rows[i]["OvertimeReg"]; //overtime
                    PayReg["O" + x].Value = PayRegData.Rows[i]["OvertimeHol"];
                    PayReg["P" + x].Value = PayRegData.Rows[i]["OtherTxIncome"]; //other tx income
                    PayReg["Q" + x].Text = PayRegData.Rows[i]["Adjustments"].ToString(); //adjustments
                    PayReg["R" + x].Value = PayRegData.Rows[i]["GrossIncome"]; //gross income
                    PayReg["S" + x].Value = PayRegData.Rows[i]["WithholdingTax"]; //withholding tax
                    PayReg["T" + x].Value = PayRegData.Rows[i]["NetSalaryAfterTax"]; //net salary after tax
                    PayReg["U" + x].Value = PayRegData.Rows[i]["EmployeeSSS"]; //employee sss
                    PayReg["V" + x].Value = PayRegData.Rows[i]["EmployeePhilHealth"]; //employee mcr(philhealth)
                    PayReg["W" + x].Value = PayRegData.Rows[i]["EmployeePagibig"]; //employee pagibig
                    PayReg["X" + x].Value = PayRegData.Rows[i]["OtherNtxIncome"]; //other ntx income
                    PayReg["Y" + x].Value = PayRegData.Rows[i]["LoanPayments"]; //loan payments
                    PayReg["Z" + x].Value = PayRegData.Rows[i]["Deductions"]; //deductions
                    PayReg["AA" + x].Value = PayRegData.Rows[i]["NetSalary"]; //net salary
                    PayReg["AB" + x].Value = PayRegData.Rows[i]["EmployerSSS"]; //employer sss
                    PayReg["AC" + x].Value = PayRegData.Rows[i]["EmployerPhilhealth"]; //employer mcr(philhealth)
                    PayReg["AD" + x].Value = PayRegData.Rows[i]["EmployerEC"]; //employer ec
                    PayReg["AE" + x].Value = PayRegData.Rows[i]["EmployerPagibig"]; //employer pagibig
                    PayReg["AF" + x].Value = PayRegData.Rows[i]["PayrollCost"]; //payroll cost
                    PayReg["AG" + x].Value = PayRegData.Rows[i]["PreviousYTDGross"]; //payment type
                    PayReg["AH" + x].Value = PayRegData.Rows[i]["PreviousYTDWitholding"]; //bank acct#
                    PayReg["AI" + x].Value = PayRegData.Rows[i]["PreviousYTDSSS"]; //bank name
                    PayReg["AJ" + x].Value = PayRegData.Rows[i]["PreviousYTDMCR"]; //date employed
                    PayReg["AK" + x].Value = PayRegData.Rows[i]["PreviousYTDPagibig"]; //date terminated
                    PayReg["AL" + x].Value = PayRegData.Rows[i]["Previous13thNT"]; //cost center
                    PayReg["AM" + x].Value = PayRegData.Rows[i]["Previous13thTX"];
                    PayReg["AN" + x].Text = PayRegData.Rows[i]["PaymentType"].ToString(); //currency
                    PayReg["AO" + x].Text = PayRegData.Rows[i]["BankAcc"].ToString(); //exchange rate
                    PayReg["AP" + x].Text = PayRegData.Rows[i]["BankName"].ToString(); //payment freq
                    PayReg["AQ" + x].Text = PayRegData.Rows[i]["CommentField"].ToString();
                    PayReg["AR" + x].Text = PayRegData.Rows[i]["ErrorField"].ToString();
                    PayReg["AS" + x].Text = PayRegData.Rows[i]["DateEmployed"].ToString();
                    PayReg["AT" + x].Text = PayRegData.Rows[i]["DateTerminated"].ToString();
                    PayReg["AU" + x].Value = PayRegData.Rows[i]["CostCenter"];
                    PayReg["AV" + x].Text = PayRegData.Rows[i]["Currency"].ToString();
                    PayReg["AW" + x].Text = PayRegData.Rows[i]["ExchangeRate"].ToString();
                    PayReg["AX" + x].Text = PayRegData.Rows[i]["PaymentPreq"].ToString();
                    PayReg["AY" + x].Value = PayRegData.Rows[i]["MTDGross"];
                    PayReg["AZ" + x].Value = PayRegData.Rows[i]["MTDBasic"];
                    PayReg["BA" + x].Value = PayRegData.Rows[i]["MTDSSSEmployee"];
                    PayReg["BB" + x].Value = PayRegData.Rows[i]["MTDMCREmployee"];
                    PayReg["BC" + x].Value = PayRegData.Rows[i]["MTDPagIbigEmployee"];
                    PayReg["BD" + x].Value = PayRegData.Rows[i]["MTDSSSEmployer"];
                    PayReg["BE" + x].Value = PayRegData.Rows[i]["MTDMCREmployer"];
                    PayReg["BF" + x].Value = PayRegData.Rows[i]["MTDECEmployer"];
                    PayReg["BG" + x].Value = PayRegData.Rows[i]["MTDPabIbigEmployer"];
                    PayReg["BH" + x].Value = PayRegData.Rows[i]["MTDWHTAX"];
                    PayReg["BI" + x].Value = PayRegData.Rows[i]["MonthlyBasic"];
                    PayReg["BJ" + x].Value = PayRegData.Rows[i]["MonthlyAllow"];
                    PayReg["BK" + x].Value = PayRegData.Rows[i]["MTDNTX"];
                    PayReg["BL" + x].Text = PayRegData.Rows[i]["FROM"].ToString();
                    PayReg["BM" + x].Text = PayRegData.Rows[i]["TO"].ToString();




                }
                #endregion
                TempConnectionStringv2();
                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@TenantID", mytype = SqlDbType.NVarChar, Value = param.TenantID });
                con.myparameters.Add(new myParameters { ParameterName = "@CutoffStart", mytype = SqlDbType.NVarChar, Value = param.CutoffStart });
                con.myparameters.Add(new myParameters { ParameterName = "@CutoffEnd", mytype = SqlDbType.NVarChar, Value = param.CutoffEnd });
                con.myparameters.Add(new myParameters { ParameterName = "@EmpStatus", mytype = SqlDbType.VarChar, Value = param.EmpStatus });
                DataTable secondSheet = con.GetDataTable("sp_ExportSheet2v1");

                x = 2;
                for (int i = 0; i < secondSheet.Rows.Count; i++, x++)
                {
                    sheet2["A" + x].Text = secondSheet.Rows[i]["PayrollNum"].ToString();
                    sheet2["B" + x].Text = secondSheet.Rows[i]["PayOutDate"].ToString();
                    sheet2["C" + x].Text = secondSheet.Rows[i]["PayCode"].ToString();
                    sheet2["D" + x].Text = secondSheet.Rows[i]["EmployeeID"].ToString();
                    sheet2["E" + x].Text = secondSheet.Rows[i]["LastName"].ToString();
                    sheet2["F" + x].Text = secondSheet.Rows[i]["FirstName"].ToString();
                    sheet2["G" + x].Text = secondSheet.Rows[i]["FileStatus"].ToString();
                    sheet2["H" + x].Text = secondSheet.Rows[i]["TransactionType"].ToString();
                    sheet2["I" + x].Text = secondSheet.Rows[i]["Date"].ToString();
                    sheet2["J" + x].Value = secondSheet.Rows[i]["CostCenter"];
                    sheet2["K" + x].Value = secondSheet.Rows[i]["Days"];
                    sheet2["L" + x].Value = secondSheet.Rows[i]["Hours"];
                    sheet2["M" + x].Value = secondSheet.Rows[i]["Amount"];
                    sheet2["N" + x].Text = secondSheet.Rows[i]["Remark"].ToString();
                    sheet2["O" + x].Text = secondSheet.Rows[i]["FROM"].ToString();
                    sheet2["P" + x].Text = secondSheet.Rows[i]["TO"].ToString();

                }

                TempConnectionStringv2();
                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@TenantID", mytype = SqlDbType.NVarChar, Value = param.TenantID });
                con.myparameters.Add(new myParameters { ParameterName = "@CutoffStart", mytype = SqlDbType.NVarChar, Value = param.CutoffStart });
                con.myparameters.Add(new myParameters { ParameterName = "@CutoffEnd", mytype = SqlDbType.NVarChar, Value = param.CutoffEnd });
                con.myparameters.Add(new myParameters { ParameterName = "@EmpStatus", mytype = SqlDbType.VarChar, Value = param.EmpStatus });
                DataTable thirdSheet = con.GetDataTable("sp_ExportSheet3v1");

                x = 2;
                for (int i = 0; i < thirdSheet.Rows.Count; i++, x++)
                {
                    sheet3["A" + x].Text = thirdSheet.Rows[i]["PayrollNum"].ToString();
                    sheet3["B" + x].Text = thirdSheet.Rows[i]["PayDate"].ToString();
                    sheet3["C" + x].Text = thirdSheet.Rows[i]["PayCode"].ToString();
                    sheet3["D" + x].Text = thirdSheet.Rows[i]["EmpID"].ToString();
                    sheet3["E" + x].Text = thirdSheet.Rows[i]["LastName"].ToString();
                    sheet3["F" + x].Text = thirdSheet.Rows[i]["FirstName"].ToString();
                    sheet3["G" + x].Text = thirdSheet.Rows[i]["FileStatus"].ToString();
                    sheet3["H" + x].Text = thirdSheet.Rows[i]["OTCode"].ToString();
                    sheet3["I" + x].Text = thirdSheet.Rows[i]["Date"].ToString();
                    sheet3["J" + x].Value = thirdSheet.Rows[i]["CostCenter"];
                    sheet3["K" + x].Value = thirdSheet.Rows[i]["OTRate"];
                    sheet3["L" + x].Value = thirdSheet.Rows[i]["OTHours"];
                    sheet3["M" + x].Value = thirdSheet.Rows[i]["OTMinutes"];
                    sheet3["N" + x].Value = thirdSheet.Rows[i]["OTAmount"];
                    sheet3["O" + x].Text = thirdSheet.Rows[i]["Remark"].ToString();
                    sheet3["P" + x].Text = thirdSheet.Rows[i]["From"].ToString();
                    sheet3["Q" + x].Text = thirdSheet.Rows[i]["To"].ToString();





                }
                TempConnectionStringv2();
                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@TenantID", mytype = SqlDbType.NVarChar, Value = param.TenantID });
                con.myparameters.Add(new myParameters { ParameterName = "@CutoffStart", mytype = SqlDbType.NVarChar, Value = param.CutoffStart });
                con.myparameters.Add(new myParameters { ParameterName = "@CutoffEnd", mytype = SqlDbType.NVarChar, Value = param.CutoffEnd });
                con.myparameters.Add(new myParameters { ParameterName = "@EmpStatus", mytype = SqlDbType.VarChar, Value = param.EmpStatus });
                DataTable FSheet = con.GetDataTable("sp_ExportSheet4v1");

                x = 2;
                for (int i = 0; i < FSheet.Rows.Count; i++, x++)
                {
                    sheet4["A" + x].Text = FSheet.Rows[i]["PayrollNum"].ToString();
                    sheet4["B" + x].Text = FSheet.Rows[i]["PayDate"].ToString();
                    sheet4["C" + x].Text = FSheet.Rows[i]["PayCode"].ToString();
                    sheet4["D" + x].Text = FSheet.Rows[i]["EmployeeID"].ToString();
                    sheet4["E" + x].Text = FSheet.Rows[i]["LastName"].ToString();
                    sheet4["F" + x].Text = FSheet.Rows[i]["FirstName"].ToString();
                    sheet4["G" + x].Text = FSheet.Rows[i]["FileStatus"].ToString();
                    sheet4["H" + x].Text = FSheet.Rows[i]["IncomeCode"].ToString();
                    sheet4["I" + x].Text = FSheet.Rows[i]["Date"].ToString();
                    sheet4["J" + x].Value = FSheet.Rows[i]["Amount"];
                    sheet4["K" + x].Text = FSheet.Rows[i]["RecurStart"].ToString();
                    sheet4["L" + x].Text = FSheet.Rows[i]["RecurEnd"].ToString();
                    sheet4["M" + x].Text = FSheet.Rows[i]["Freq"].ToString();
                    sheet4["N" + x].Text = FSheet.Rows[i]["Remarks"].ToString();
                    sheet4["O" + x].Text = FSheet.Rows[i]["From"].ToString();
                    sheet4["P" + x].Text = FSheet.Rows[i]["To"].ToString();
                    sheet4["Q" + x].Text = FSheet.Rows[i]["JECostCenter"].ToString();

                }

                TempConnectionStringv2();
                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@TenantID", mytype = SqlDbType.NVarChar, Value = param.TenantID });
                con.myparameters.Add(new myParameters { ParameterName = "@CutoffStart", mytype = SqlDbType.NVarChar, Value = param.CutoffStart });
                con.myparameters.Add(new myParameters { ParameterName = "@CutoffEnd", mytype = SqlDbType.NVarChar, Value = param.CutoffEnd });
                con.myparameters.Add(new myParameters { ParameterName = "@EmpStatus", mytype = SqlDbType.VarChar, Value = param.EmpStatus });
                DataTable FifthSheet = con.GetDataTable("sp_ExportSheet5v1");

                x = 2;
                for (int i = 0; i < FifthSheet.Rows.Count; i++, x++)
                {
                    sheet5["A" + x].Text = FifthSheet.Rows[i]["PayrollNum"].ToString();
                    sheet5["B" + x].Text = FifthSheet.Rows[i]["PayDate"].ToString();
                    sheet5["C" + x].Text = FifthSheet.Rows[i]["PayCode"].ToString();
                    sheet5["D" + x].Text = FifthSheet.Rows[i]["EmployeeID"].ToString();
                    sheet5["E" + x].Text = FifthSheet.Rows[i]["LastName"].ToString();
                    sheet5["F" + x].Text = FifthSheet.Rows[i]["FirstName"].ToString();
                    sheet5["G" + x].Text = FifthSheet.Rows[i]["FileStatus"].ToString();
                    sheet5["H" + x].Text = FifthSheet.Rows[i]["IncomeCode"].ToString();
                    sheet5["I" + x].Text = FifthSheet.Rows[i]["Date"].ToString();
                    sheet5["J" + x].Value = FifthSheet.Rows[i]["Amount"];
                    sheet5["K" + x].Text = FifthSheet.Rows[i]["RecurStart"].ToString();
                    sheet5["L" + x].Text = FifthSheet.Rows[i]["RecurEnd"].ToString();
                    sheet5["M" + x].Text = FifthSheet.Rows[i]["Freq"].ToString();
                    sheet5["N" + x].Text = FifthSheet.Rows[i]["Remarks"].ToString();
                    sheet5["O" + x].Text = FifthSheet.Rows[i]["From"].ToString();
                    sheet5["P" + x].Text = FifthSheet.Rows[i]["To"].ToString();
                    sheet5["Q" + x].Text = FifthSheet.Rows[i]["JECostCenter"].ToString();



                }
                TempConnectionStringv2();
                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@TenantID", mytype = SqlDbType.NVarChar, Value = param.TenantID });
                con.myparameters.Add(new myParameters { ParameterName = "@CutoffStart", mytype = SqlDbType.NVarChar, Value = param.CutoffStart });
                con.myparameters.Add(new myParameters { ParameterName = "@CutoffEnd", mytype = SqlDbType.NVarChar, Value = param.CutoffEnd });
                con.myparameters.Add(new myParameters { ParameterName = "@EmpStatus", mytype = SqlDbType.VarChar, Value = param.EmpStatus });
                DataTable SixSheet = con.GetDataTable("sp_ExportSheet6v1");

                x = 2;
                for (int i = 0; i < SixSheet.Rows.Count; i++, x++)
                {
                    sheet6["A" + x].Text = SixSheet.Rows[i]["PayrollNum"].ToString();
                    sheet6["B" + x].Text = SixSheet.Rows[i]["PayDate"].ToString();
                    sheet6["C" + x].Text = SixSheet.Rows[i]["PayCode"].ToString();
                    sheet6["D" + x].Text = SixSheet.Rows[i]["EmployeeID"].ToString();
                    sheet6["E" + x].Text = SixSheet.Rows[i]["LastName"].ToString();
                    sheet6["F" + x].Text = SixSheet.Rows[i]["FirstName"].ToString();
                    sheet6["G" + x].Text = SixSheet.Rows[i]["FileStatus"].ToString();
                    sheet6["H" + x].Text = SixSheet.Rows[i]["LoanCode"].ToString();
                    sheet6["I" + x].Text = SixSheet.Rows[i]["LoanDateStart"].ToString();
                    sheet6["J" + x].Value = SixSheet.Rows[i]["LoanPrincipal"];
                    sheet6["K" + x].Value = SixSheet.Rows[i]["LoanAmount"];
                    sheet6["L" + x].Text = SixSheet.Rows[i]["LoanPayments"].ToString();
                    sheet6["M" + x].Value = SixSheet.Rows[i]["LoanBalance"];
                    sheet6["N" + x].Text = SixSheet.Rows[i]["LoanDeductionAMT"].ToString();
                    sheet6["O" + x].Text = SixSheet.Rows[i]["LoanDeductionFreq"].ToString();
                    sheet6["P" + x].Text = SixSheet.Rows[i]["LoanStatus"].ToString();
                    sheet6["Q" + x].Text = SixSheet.Rows[i]["LoanDateGranted"].ToString();
                    sheet6["R" + x].Text = SixSheet.Rows[i]["From"].ToString();
                    sheet6["S" + x].Text = SixSheet.Rows[i]["To"].ToString();
                    sheet6["T" + x].Text = SixSheet.Rows[i]["JECostCenter"].ToString();


                }

                TempConnectionStringv2();
                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@TenantID", mytype = SqlDbType.NVarChar, Value = param.TenantID });
                con.myparameters.Add(new myParameters { ParameterName = "@CutoffStart", mytype = SqlDbType.NVarChar, Value = param.CutoffStart });
                con.myparameters.Add(new myParameters { ParameterName = "@CutoffEnd", mytype = SqlDbType.NVarChar, Value = param.CutoffEnd });
                con.myparameters.Add(new myParameters { ParameterName = "@EmpStatus", mytype = SqlDbType.VarChar, Value = param.EmpStatus });
                DataTable SevenSheet = con.GetDataTable("sp_Exportsheet7v1");

                x = 2;
                for (int i = 0; i < SevenSheet.Rows.Count; i++, x++)
                {
                    sheet7["A" + x].Text = SevenSheet.Rows[i]["PayrollNum"].ToString();
                    sheet7["B" + x].Text = SevenSheet.Rows[i]["PayDate"].ToString();
                    sheet7["C" + x].Text = SevenSheet.Rows[i]["PayCode"].ToString();
                    sheet7["D" + x].Text = SevenSheet.Rows[i]["EmployeeID"].ToString();
                    sheet7["E" + x].Text = SevenSheet.Rows[i]["LastName"].ToString();
                    sheet7["F" + x].Text = SevenSheet.Rows[i]["FirstName"].ToString();
                    sheet7["G" + x].Text = SevenSheet.Rows[i]["FileStatus"].ToString();
                    sheet7["H" + x].Text = SevenSheet.Rows[i]["DeductionCode"].ToString();
                    sheet7["I" + x].Text = SevenSheet.Rows[i]["Date"].ToString();
                    sheet7["J" + x].Value = SevenSheet.Rows[i]["DeductionAmount"];
                    sheet7["K" + x].Text = SevenSheet.Rows[i]["DeductionFreq"].ToString();
                    sheet7["L" + x].Text = SevenSheet.Rows[i]["LastDeductionDate"].ToString();
                    sheet7["M" + x].Text = SevenSheet.Rows[i]["From"].ToString();
                    sheet7["N" + x].Text = SevenSheet.Rows[i]["To"].ToString();
                    sheet7["O" + x].Text = SevenSheet.Rows[i]["JECostCenter"].ToString();



                }
                HttpResponse httpResponse = System.Web.HttpContext.Current.Response;

                httpResponse.Clear();
                httpResponse.ContentType = "Application/x-msexcel";
                httpResponse.AddHeader("Content-Disposition", String.Format("attachment; filename={0}", filename));
                string newDIR = HttpContext.Current.Server.MapPath("~/sFTP/PayrollOutsourcing/");
                string onlineDIR = "http://ws.durusthr.com/ILM_WS_Live/sFTP/PayrollOutsourcing/";
                //filename = filename.Replace(',', '_');
                //filename = filename.Replace(" ", String.Empty);
                workbook.Save(newDIR + filename);
                stream.Close();
                //return "Success";

                if (PayRegData.Rows.Count > 0)
                {
                    return onlineDIR + filename;
                }
                else
                {

                    return "No Rows";
                }
            }
            catch (Exception e)
            {
                stream.Close();
                return "Error: " + e.ToString();
            }
        }
        #endregion
        #region Payslip Related
        [HttpPost]
        [Route("PO_AccessPayslip")]
        public string AccessPayslip(AccessPayslip param)
        {
            try
            {
                TempConnectionStringv2();
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@TenantID", mytype = SqlDbType.VarChar, Value = param.TenantID });
                con.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.VarChar, Value = param.EmpID });
                con.myparameters.Add(new myParameters { ParameterName = "@PayslipPass", mytype = SqlDbType.VarChar, Value = param.PayslipPass });
                return con.ExecuteScalar("sp_AccessPayslip");
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }
        [HttpPost]
        [Route("PO_GeneratePayslip_List")]
        public List<PayslipList> PayslipList(PayslipParamsBulk param)
        {
            List<PayslipList> ReturnList = new List<PayslipList>();
            try
            {
                DataTable List = new DataTable();
                List.Columns.AddRange(new DataColumn[1]
                {
                    new DataColumn("EmpID")
                });
                for (int i = 0; i < param.empIDs.Length; i++)
                {
                    List.Rows.Add(param.empIDs[i]);
                }
                TempConnectionStringv4(param.PayslipType);
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@tenantID", mytype = SqlDbType.VarChar, Value = param.tenantID });
                con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.Structured, Value = List });
                DataTable dt = con.GetDataTable("sp_GetClientUserIds_Payslip");
                foreach (DataRow item in dt.Rows)
                {
                    PayslipList P = new PayslipList()
                    {
                        URL = GeneratePayslip2(item["empID"].ToString(), "{" + item["Id"].ToString() + "}", param.payoutDate, param.tenantID, item["LastName"].ToString(), param.PayslipType.ToLower())
                    };
                    string payslipPath = GeneratePayslip(item["empID"].ToString(), "{" + item["Id"].ToString() + "}", param.payoutDate, param.tenantID, item["LastName"].ToString(), param.PayslipType.ToLower());
                    //Add Password
                    if (param.hasPassword)
                    {
                        PdfSharp.Pdf.PdfDocument document = PdfSharp.Pdf.IO.PdfReader.Open(payslipPath, "some text");
                        PdfSecuritySettings securitySettings = document.SecuritySettings;
                        securitySettings.UserPassword = item["TINnum"].ToString().Replace("-", string.Empty);
                        securitySettings.OwnerPassword = "Illimitado";
                        document.Save(payslipPath);
                        //PdfStamper stamper = new PdfStamper(reader, new FileStream(payslipPath, FileMode.Create));
                        //stamper.SetEncryption(true, item["TINnum"].ToString(), "illimitado", PdfWriter.ALLOW_SCREENREADERS);
                    }
                    string LetterReceiverName = item["Firstname"].ToString() + " " + item["LastName"].ToString();
                    string[] emailDate = param.payoutDate.Split(' ');
                    //Send to email
                    if (param.sendEmail)
                    {
                        string attachmentPath = payslipPath;
                        string emailaccount = "durusthr@illimitado.com";
                        string emailpassword = "@1230Qwerty";
                        using (MailMessage mm = new MailMessage(emailaccount, item["Email"].ToString()))
                        {
                            MailAddress aliasmail = new MailAddress("durusthr@illimitado.com", "durusthr@illimitado.com");
                            MailAddress aliasreplymail = new MailAddress("durusthr@illimitado.com");
                            MailAddress aliasmailBCC = new MailAddress("durusthr@illimitado.com");
                            mm.From = aliasmail;
                            mm.Bcc.Add(aliasmailBCC);
                            if (param.PayslipType == "crawford")
                            {
                                mm.Subject = "Crawford Philippines Payslip of " + LetterReceiverName + " for " + emailDate[0];
                            }
                            else if (param.PayslipType == "legacy")
                            {
                                mm.Subject = "Payslip of " + LetterReceiverName + " for " + emailDate[0];
                            }
                            mm.Body = "";
                            mm.Attachments.Add(new Attachment(attachmentPath));
                            mm.IsBodyHtml = false;
                            SmtpClient smtp = new SmtpClient();
                            smtp.Host = "smtp.office365.com";
                            smtp.EnableSsl = true;
                            NetworkCredential NetworkCred = new NetworkCredential(emailaccount, emailpassword);
                            smtp.UseDefaultCredentials = true;
                            smtp.Credentials = NetworkCred;
                            smtp.Port = 587;
                            smtp.Send(mm);
                        }
                    }
                    ReturnList.Add(P);
                }
                return ReturnList;
            }
            catch (Exception e)
            {
                return ReturnList;
            }
        }
        [HttpPost]
        [Route("PO_GeneratePayslip_Array")]
        public string GeneratePayslip_Array(PayslipParamsBulk param)
        {
            try
            {
                DataTable empIds = new DataTable();
                empIds.Columns.AddRange(new DataColumn[1]{
                    new DataColumn("empId")
                });
                for (int i = 0; i < param.empIDs.Length; i++)
                {
                    empIds.Rows.Add(param.empIDs[i]);
                }
                TempConnectionStringv4(param.db);
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@tenantID", mytype = SqlDbType.VarChar, Value = param.tenantID });
                con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.Structured, Value = empIds });
                DataTable dt = con.GetDataTable("sp_GetClientUserIds_Payslip");
                foreach (DataRow item in dt.Rows)
                {
                    string payslipPath = GeneratePayslip(item["empID"].ToString(), "{" + item["Id"].ToString() + "}", param.payoutDate, param.tenantID, item["LastName"].ToString(), param.PayslipType.ToLower());
                    //Add Password
                    if (param.hasPassword)
                    {
                        PdfSharp.Pdf.PdfDocument document = PdfSharp.Pdf.IO.PdfReader.Open(payslipPath, "some text");
                        PdfSecuritySettings securitySettings = document.SecuritySettings;
                        securitySettings.UserPassword = item["TINnum"].ToString().Replace("-", string.Empty);
                        securitySettings.OwnerPassword = "Crawford";
                        document.Save(payslipPath);
                        //PdfStamper stamper = new PdfStamper(reader, new FileStream(payslipPath, FileMode.Create));
                        //stamper.SetEncryption(true, item["TINnum"].ToString(), "illimitado", PdfWriter.ALLOW_SCREENREADERS);
                    }
                    string LetterReceiverName = item["Firstname"].ToString() + " " + item["LastName"].ToString();
                    string[] emailDate = param.payoutDate.Split(' ');
                    //Send to email
                    if (param.sendEmail)
                    {
                        string attachmentPath = payslipPath;
                        string emailaccount = "durusthr@illimitado.com";
                        string emailpassword = "@1230Qwerty";
                        using (MailMessage mm = new MailMessage(emailaccount, item["Email"].ToString()))
                        {
                            MailAddress aliasmail = new MailAddress("durusthr@illimitado.com", "durusthr@illimitado.com");
                            MailAddress aliasreplymail = new MailAddress("durusthr@illimitado.com");
                            MailAddress aliasmailBCC = new MailAddress("durusthr@illimitado.com");
                            mm.From = aliasmail;
                            mm.Bcc.Add(aliasmailBCC);
                            if (param.PayslipType.ToLower() == "crawford")
                            {
                                mm.Subject = "Crawford Philippines Payslip of " + LetterReceiverName + " for " + emailDate[0];
                            }
                            else if (param.PayslipType.ToLower() == "legacy")
                            {
                                mm.Subject = "Payslip of " + LetterReceiverName + " for " + emailDate[0];
                            }
                            mm.Body = "";
                            mm.Attachments.Add(new Attachment(attachmentPath));
                            mm.IsBodyHtml = false;
                            SmtpClient smtp = new SmtpClient();
                            smtp.Host = "smtp.office365.com";
                            smtp.EnableSsl = true;
                            NetworkCredential NetworkCred = new NetworkCredential(emailaccount, emailpassword);
                            smtp.UseDefaultCredentials = true;
                            smtp.Credentials = NetworkCred;
                            smtp.Port = 587;
                            smtp.Send(mm);
                        }
                    }
                }
                return "Success";
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }
        [HttpPost]
        [Route("PO_GeneratePayslip_Bulk")]
        public string GeneratePayslip_Bulk(PayslipParamsBulk param)
        {
            
            try
            {
                TempConnectionStringv2();
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@tenantID", mytype = SqlDbType.VarChar, Value = param.tenantID });
                DataTable DT = con.GetDataTable("sp_CustomClientUserIDsv2");
                foreach (DataRow row in DT.Rows)
                {
                    string attachmentPath = GeneratePayslip(row["empid"].ToString(), "{" + row["Id"].ToString() + "}", param.payoutDate, param.tenantID, "Payslip", param.PayslipType.ToLower());
                    string emailaccount = "durusthr@illimitado.com";
                    string emailpassword = "@1230Qwerty";
                    using (MailMessage mm = new MailMessage(emailaccount, row["Email"].ToString()))
                    {
                        MailAddress aliasmail = new MailAddress("durusthr@illimitado.com", "durusthr@illimitado.com");
                        MailAddress aliasreplymail = new MailAddress("durusthr@illimitado.com");
                        mm.From = aliasmail;
                        mm.Subject = "Notifications";
                        mm.Body = "";
                        mm.Attachments.Add(new Attachment(attachmentPath));
                        mm.IsBodyHtml = false;
                        SmtpClient smtp = new SmtpClient();
                        smtp.Host = "smtp.office365.com";
                        smtp.EnableSsl = true;
                        NetworkCredential NetworkCred = new NetworkCredential(emailaccount, emailpassword);
                        smtp.UseDefaultCredentials = true;
                        smtp.Credentials = NetworkCred;
                        smtp.Port = 587;
                        smtp.Send(mm);
                    }
                }
                //for (int i = 0; i < param.empIDs.Length; i++)
                //{
                //    //Gets UserID (GUID) using EmpID
                //    TempConnectionStringv2();
                //    con = new Connection();
                //    con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = param.empIDs[i] });
                //    con.myparameters.Add(new myParameters { ParameterName = "@tenantID", mytype = SqlDbType.VarChar, Value = param.tenantID });
                //    string userID = con.ExecuteScalar("sp_GetUserID_Payslip");
                //    //Get the email using UserID
                //    TempConnectionStringv2();
                //    Connection con2 = new Connection();
                //    con2.myparameters.Add(new myParameters { ParameterName = "@userID", mytype = SqlDbType.VarChar, Value = userID });
                //    string loopemail = con2.ExecuteScalar("sp_GetEmail_Payslip");
                //    string attachmentPath = GeneratePayslip(param.empIDs[i],"{" + userID + "}", param.payoutDate);
                //    string emailaccount = "durusthr@illimitado.com";
                //    string emailpassword = "@1230Qwerty";
                //    using (MailMessage mm = new MailMessage(emailaccount, loopemail))
                //    {
                //        MailAddress aliasmail = new MailAddress("durusthr@illimitado.com", "durusthr@illimitado.com");
                //        MailAddress aliasreplymail = new MailAddress("durusthr@illimitado.com");
                //        mm.From = aliasmail;
                //        mm.Subject = "Notifications";
                //        mm.Body = "";
                //        mm.Attachments.Add(new Attachment(attachmentPath));
                //        mm.IsBodyHtml = false;
                //        SmtpClient smtp = new SmtpClient();
                //        smtp.Host = "smtp.office365.com";
                //        smtp.EnableSsl = true;
                //        NetworkCredential NetworkCred = new NetworkCredential(emailaccount, emailpassword);
                //        smtp.UseDefaultCredentials = true;
                //        smtp.Credentials = NetworkCred;
                //        smtp.Port = 587;
                //        smtp.Send(mm);
                //    }
                //}
                return "Success";
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }
        [Route("PO_GeneratePayslip_Test")]
        public string GeneratePayslip_Test(PayslipParams param)
        {
            try
            {
                //Gets UserID (GUID) using EmpID
                TempConnectionStringv2();
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = param.empID });
                con.myparameters.Add(new myParameters { ParameterName = "@tenantID", mytype = SqlDbType.VarChar, Value = param.tenantID });
                string userID = con.ExecuteScalar("sp_GetUserID_Payslip");
                //Get the email using UserID
                TempConnectionStringv2();
                Connection con2 = new Connection();
                con2.myparameters.Add(new myParameters { ParameterName = "@userID", mytype = SqlDbType.VarChar, Value = userID });
                string loopemail = con2.ExecuteScalar("sp_GetEmail_Payslip");
                string attachmentPath = GeneratePayslip(param.empID, "{" + userID + "}", param.payoutDate, param.tenantID, "Test", param.PayslipType.ToLower());
                string emailaccount = "durusthr@illimitado.com";
                string emailpassword = "@1230Qwerty";
                using (MailMessage mm = new MailMessage(emailaccount, loopemail))
                {
                    MailAddress aliasmail = new MailAddress("durusthr@illimitado.com", "durusthr@illimitado.com");
                    MailAddress aliasreplymail = new MailAddress("durusthr@illimitado.com");
                    mm.From = aliasmail;
                    mm.Subject = "Notifications";
                    mm.Body = "";
                    mm.Attachments.Add(new Attachment(attachmentPath));
                    mm.IsBodyHtml = false;
                    SmtpClient smtp = new SmtpClient();
                    smtp.Host = "smtp.office365.com";
                    smtp.EnableSsl = true;
                    NetworkCredential NetworkCred = new NetworkCredential(emailaccount, emailpassword);
                    smtp.UseDefaultCredentials = true;
                    smtp.Credentials = NetworkCred;
                    smtp.Port = 587;
                    smtp.Send(mm);
                }
                return "Success";
            }
            catch (Exception ex)
            {
                return "FAILED : " + ex.ToString();
            }
        }
        [HttpPost]
        [Route("PO_GenerateAll_Payslip")]
        public string PO_GenerateAll_Payslip(PayslipParamsBulk param)
        {
            var dateAndTime = DateTime.Parse(param.payoutDate);
            //Generate Payslip
            string payslipPath = "";
             payslipPath = GeneratePayslip_All(param.payoutDate, param.tenantID, "legacy");

            //payslipPath = GeneratePayslipNew(string empID, string userID, string payoutDate, string tenantID, string lastname, string payslipType)

            //GetPDF
            //string finalFilePath = HttpContext.Current.Server.MapPath("~/GeneratedPayslip/GLOBALPAY/GLOBALPAY_2021-01-25.pdf");
            //Open PDF
            PdfSharp.Pdf.PdfDocument inputDoc = PdfSharp.Pdf.IO.PdfReader.Open(payslipPath, PdfDocumentOpenMode.Import);

            Directory.CreateDirectory(HttpContext.Current.Server.MapPath("~/GeneratedPayslip/GenerateAllTest/" + param.tenantID + "/"));
            string outputPath = HttpContext.Current.Server.MapPath("~/GeneratedPayslip/GenerateAllTest/" + param.tenantID + "/");
            //Gets Employee Info
            List<EmployeeInfoForGenerateAll> employeeInfoForGenerateAllList = new List<EmployeeInfoForGenerateAll>();

            TempConnectionStringv4("legacy");
            Connection con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@payoutDate", mytype = SqlDbType.VarChar, Value = param.payoutDate });
            con.myparameters.Add(new myParameters { ParameterName = "@tenantId", mytype = SqlDbType.VarChar, Value = param.tenantID });
            DataTable dt = con.GetDataTable("sp_getEmployeeInfo_ForGenerateAll");
            foreach (DataRow row in dt.Rows)
            {
                EmployeeInfoForGenerateAll employeeInfoForGenerate = new EmployeeInfoForGenerateAll()
                {
                    EmpID = row["EmpID"].ToString(),
                    FirstName = row["FirstName"].ToString(),
                    LastName = row["LastName"].ToString(),
                    TinNum = row["TINnum"].ToString()
                };
                employeeInfoForGenerateAllList.Add(employeeInfoForGenerate);
            }
            int asd = dt.Rows.Count;
            //SplitPDF
            string name = Path.GetFileNameWithoutExtension(payslipPath);
            for (int i = 0; i < inputDoc.PageCount; i++)
            {
                PdfSharp.Pdf.PdfDocument outputDoc = new PdfSharp.Pdf.PdfDocument();
                outputDoc.Version = inputDoc.Version;

                outputDoc.Info.Title = String.Format("Payslip for {0} - {1}", employeeInfoForGenerateAllList[i].EmpID, param.payoutDate);
                outputDoc.Info.Creator = inputDoc.Info.Creator;

                outputDoc.AddPage(inputDoc.Pages[i]);
                //add password
                PdfSecuritySettings securitySettings = outputDoc.SecuritySettings;
                //Get TIN for Password
                securitySettings.UserPassword = employeeInfoForGenerateAllList[i].TinNum.ToString().Replace("-", string.Empty);
                securitySettings.OwnerPassword = "Illimitado";




                //string asssd = Path.Combine(outputPath, String.Format("Payslip for {0} - {1}.pdf", employeeInfoForGenerateAllList[i].EmpID, dateAndTime.Date.ToString("yyyy-MM-dd")));
                outputDoc.Save(Path.Combine(outputPath, String.Format("Payslip for {0} - {1}.pdf", employeeInfoForGenerateAllList[i].EmpID, dateAndTime.Date.ToString("yyyy-MM-dd"))));
            }
            ////Archive Folder // May problema pa 
            string zipPath = HttpContext.Current.Server.MapPath("~/GeneratedPayslip/GenerateAllTest/" + param.tenantID + "/" + "Payslip-" + dateAndTime.Date.ToString("yyyy-MM-dd") + ".zip");

            if (File.Exists(zipPath))
            {
                File.Delete(zipPath);
            }
            else
            {
                ZipFile.CreateFromDirectory(outputPath, zipPath);
            }

            string returnPath = "http://ws.durusthr.com/ILM_WS_Live/GeneratedPayslip/GenerateAllTest/" + param.tenantID + "/" + "Payslip-" + dateAndTime.Date.ToString("yyyy-MM-dd") + ".zip";
            return returnPath;
        }

        public class EmployeeInfoForGenerateAll
        {
            public string EmpID { get; set; }
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public string TinNum { get; set; }
        }
        public string GeneratePayslip_All(string payoutDate, string tenantID, string payslipType)
        {
            try
            {
                ReportDocument reportDocument = new ReportDocument();
                ConnectionInfo conRpt = new ConnectionInfo();
                string path = "";
                path = HttpContext.Current.Server.MapPath("~/Payslip_Legacy - Test.rpt");
                conRpt.DatabaseName = "LegacyPayrollDB";

                reportDocument.Load(path);
                //reportDocument.SetParameterValue("@empID", userID);
                //reportDocument.SetParameterValue("@tenantID", "{7FF8ECEF-092B-4FAF-B37B-C730688A3032}");
                reportDocument.SetParameterValue("@payoutDate", payoutDate);
                conRpt.ServerName = "livedb-ilm.c3srcolvclfy.ap-southeast-1.rds.amazonaws.com,1412";
                conRpt.UserID = "ILM_LiveDBUser";
                conRpt.Password = "ILMV3ry53cur3dMGTpa55w0rd2020";
                reportDocument.SetDatabaseLogon("ILM_LiveDBUser", "ILMV3ry53cur3dMGTpa55w0rd2020");
                Tables rptTables = reportDocument.Database.Tables;
                for (int i = 0; i < rptTables.Count; i++)
                {
                    CrystalDecisions.CrystalReports.Engine.Table rptTable = rptTables[i];
                    TableLogOnInfo tblInfo = rptTable.LogOnInfo;
                    tblInfo.ConnectionInfo = conRpt;
                    // next table
                }
                // if the report contains sub reports, you will need to set the connection info there too
                if (reportDocument.Subreports.Count > 0)
                {
                    for (int i = 0; i < reportDocument.Subreports.Count; i++)
                    {
                        using (ReportDocument rptSub = reportDocument.OpenSubreport(reportDocument.Subreports[i].Name))
                        {
                            Tables rptTables2 = rptSub.Database.Tables;
                            for (int ix = 0; ix < rptTables2.Count; ix++)
                            {
                                CrystalDecisions.CrystalReports.Engine.Table rptTable = rptTables2[ix];
                                TableLogOnInfo tblInfo = rptTable.LogOnInfo;
                                tblInfo.ConnectionInfo = conRpt;
                                // next table
                            }
                            rptSub.Close();
                        }
                    }
                }
                TempConnectionStringv4(payslipType);
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@tenantID", mytype = SqlDbType.VarChar, Value = tenantID });
                string tenantName = con.ExecuteScalar("sp_GetTenantName_Payslip");
                //Directory.CreateDirectory(tenantName);
                Directory.CreateDirectory(HttpContext.Current.Server.MapPath("~/GeneratedPayslip/" + tenantName));
                //string fileNameIden = userID.Trim(new char[] { '{', '}' });
                string[] filename2 = payoutDate.Split(' ');
                // string finalFilePath = HttpContext.Current.Server.MapPath("~/GeneratedPayslip/" + empID + "_" + filename2[0].Trim('-') + ".pdf");
                string finalFilePath = HttpContext.Current.Server.MapPath("~/GeneratedPayslip/" + tenantName + "/" + tenantName + "_" + filename2[0].Trim('-') + ".pdf");
                reportDocument.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, finalFilePath);
                reportDocument.Dispose();
                // return finalFilePath;
                // return empID + "_" + filename2[0].Trim('-') + ".pdf";
                return finalFilePath;
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }



        public string GeneratePayslip2(string empID, string userID, string payoutDate, string tenantID, string lastName, string payslipType)
        {
            try
            {
                ReportDocument reportDocument = new ReportDocument();
                ConnectionInfo conRpt = new ConnectionInfo();
                string path = "";
                if (payslipType == "crawford")
                {
                    path = HttpContext.Current.Server.MapPath("~/Payslip.rpt");
                    conRpt.DatabaseName = "DurustHRdb1";
                }
                else if (payslipType == "legacy")
                {
                    path = HttpContext.Current.Server.MapPath("~/Payslip_Legacy.rpt");
                    conRpt.DatabaseName = "LegacyPayrollDB";
                }
                reportDocument.Load(path);
                reportDocument.SetParameterValue("@empID", userID);
                //reportDocument.SetParameterValue("@tenantID", "{7FF8ECEF-092B-4FAF-B37B-C730688A3032}");
                reportDocument.SetParameterValue("@payoutDate", payoutDate);
                conRpt.ServerName = "livedb-ilm.c3srcolvclfy.ap-southeast-1.rds.amazonaws.com,1412";
                conRpt.UserID = "ILM_LiveDBUser";
                conRpt.Password = "ILMV3ry53cur3dMGTpa55w0rd2020";
                reportDocument.SetDatabaseLogon("ILM_LiveDBUser", "ILMV3ry53cur3dMGTpa55w0rd2020");
                Tables rptTables = reportDocument.Database.Tables;
                for (int i = 0; i < rptTables.Count; i++)
                {
                    CrystalDecisions.CrystalReports.Engine.Table rptTable = rptTables[i];
                    TableLogOnInfo tblInfo = rptTable.LogOnInfo;
                    tblInfo.ConnectionInfo = conRpt;
                    // next table
                }
                // if the report contains sub reports, you will need to set the connection info there too
                if (reportDocument.Subreports.Count > 0)
                {
                    for (int i = 0; i < reportDocument.Subreports.Count; i++)
                        using (ReportDocument rptSub = reportDocument.OpenSubreport(reportDocument.Subreports[i].Name))
                        {
                            Tables rptTables2 = rptSub.Database.Tables;
                            for (int ix = 0; ix < rptTables2.Count; ix++)
                            {
                                CrystalDecisions.CrystalReports.Engine.Table rptTable = rptTables2[ix];
                                TableLogOnInfo tblInfo = rptTable.LogOnInfo;
                                tblInfo.ConnectionInfo = conRpt;
                                // next table
                            }
                            rptSub.Close();
                        }
                }

                TempConnectionStringv4(payslipType);
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@tenantID", mytype = SqlDbType.VarChar, Value = tenantID });
                string tenantName = con.ExecuteScalar("sp_GetTenantName_Payslip");
                //Directory.CreateDirectory(tenantName);
                Directory.CreateDirectory(HttpContext.Current.Server.MapPath("~/GeneratedPayslip/" + tenantName));
                string fileNameIden = userID.Trim(new char[] { '{', '}' });
                string[] filename2 = payoutDate.Split(' ');
                // string finalFilePath = HttpContext.Current.Server.MapPath("~/GeneratedPayslip/" + empID + "_" + filename2[0].Trim('-') + ".pdf");
                string finalFilePath = HttpContext.Current.Server.MapPath("~/GeneratedPayslip/" + tenantName + "/" + empID + "_" + lastName + "_" + filename2[0].Trim('-') + ".pdf");
                reportDocument.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, finalFilePath);
                reportDocument.Dispose();
                // return finalFilePath;
                // return empID + "_" + filename2[0].Trim('-') + ".pdf";
                return empID + "_" + lastName + "_" + filename2[0].Trim('-') + ".pdf";
            }

            catch (Exception ex)
            {
                return ex.ToString();
            }
            
        }

        public string GeneratePayslipNew(string empID, string userID, string payoutDate, string tenantID, string lastname, string payslipType)
        {
            try
            {
             
                List<payslipResponse> resp = new List<payslipResponse>();
                List<payslipDetailsResponse> respDetails = new List<payslipDetailsResponse>();
                List<payslipDetailsResponse> overtime = new List<payslipDetailsResponse>();
                List<payslipDetailsResponse> taxable = new List<payslipDetailsResponse>();
                List<payslipDetailsResponse> nonTaxable = new List<payslipDetailsResponse>();
                List<payslipDetailsResponse> deductions = new List<payslipDetailsResponse>();
                List<payslipDetailsResponse> loan = new List<payslipDetailsResponse>();
                List<payslipDetailsResponse> attendance = new List<payslipDetailsResponse>();
                TempConnectionStringv4(payslipType);
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = userID });
                con.myparameters.Add(new myParameters { ParameterName = "@payoutdate", mytype = SqlDbType.VarChar, Value = payoutDate });
                DataTable dt = con.GetDataTable("getPayslip");
                foreach (DataRow row in dt.Rows)
                {
                    payslipResponse P = new payslipResponse()
                    {
                        ClientName = row["ClientName"].ToString(),
                        Address = row["Address"].ToString(),
                        Address2 = row["Address2"].ToString(),
                        LogoPath = row["LogoPath"].ToString(),
                        EmpId = row["EmpId"].ToString(),
                        EmpName = row["EmpName"].ToString(),
                        Position = row["Position"].ToString(),
                        Department = row["Department"].ToString(),
                        SSSNum = row["SSSNum"].ToString(),
                        TINnum = row["TINnum"].ToString(),
                        PagibigNum = row["PagibigNum"].ToString(),
                        PhilhealthNum = row["PhilhealthNum"].ToString(),
                        PayPeriod = row["PayPeriod"].ToString(),

                        PayOutDate = row["PayOutDate"].ToString(),
                        BasicPay = row["BasicPay"].ToString(),
                        NetPay = row["NetPay"].ToString(),
                        YTDBasicPay = row["YTDBasicPay"].ToString(),
                        YTDEarningsTaxableAmount = row["YTDEarningsTaxableAmount"].ToString(),
                        EarningsTaxableAmount = row["EarningsTaxableAmount"].ToString(),
                        YTDEarningsNonTaxableAmount = row["YTDEarningsNonTaxableAmount"].ToString(),
                        EarningsNonTaxableAmount = row["EarningsNonTaxableAmount"].ToString(),
                        YTDAttendanceTotal = row["YTDAttendanceTotal"].ToString(),
                        AttendanceTotal = row["AttendanceTotal"].ToString(),
                        YTDGovDeduction = row["YTDGovDeduction"].ToString(),
                        GovDeduction = row["GovDeduction"].ToString(),
                        YTDWHtax = row["YTDWHtax"].ToString(),
                        WHTax = row["WHTax"].ToString(),
                        YTDDeductionsTotal = row["YTDDeductionsTotal"].ToString(),
                        DeductionsTotal = row["DeductionsTotal"].ToString(),
                        YTDNetPay = row["YTDNetPay"].ToString(),
                        absentCode = row["absentCode"].ToString(),
                        absentDays = row["absentDays"].ToString(),
                        absentAmount = row["absentAmount"].ToString(),
                        regCode = row["regCode"].ToString(),
                        regDays = row["regDays"].ToString(),
                        regAmount = row["regAmount"].ToString(),
                        undCode = row["undCode"].ToString(),
                        undDays = row["undDays"].ToString(),
                        undAmount = row["undAmount"].ToString(),
                        tardCode = row["tardCode"].ToString(),
                        tardDays = row["tardDays"].ToString(),
                        tardAmount = row["tardAmount"].ToString(),
                        lwoCode = row["lwoCode"].ToString(),
                        lwoDays = row["lwoDays"].ToString(),
                        lwoAmount = row["lwoAmount"].ToString(),
                        SSSAmount = row["SSSAmount"].ToString(),
                        PagIbigAmount = row["PagIbigAmount"].ToString(),
                        PhilHealthAmount = row["PhilHealthAmount"].ToString(),
                        MonthlyRate = row["MonthlyRate"].ToString()


                    };
                    resp.Add(P);
                }
                TempConnectionStringv4(payslipType);
                Connection con1 = new Connection();
                con1.myparameters.Add(new myParameters { ParameterName = "@payoutdate", mytype = SqlDbType.VarChar, Value = payoutDate });
                con1.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = userID });
                con1.myparameters.Add(new myParameters { ParameterName = "@payOutType", mytype = SqlDbType.VarChar, Value = payslipType });
                con1.myparameters.Add(new myParameters { ParameterName = "@tenantId", mytype = SqlDbType.VarChar, Value = tenantID });
                DataTable dt1 = con1.GetDataTable("getPayslipDetails");


                System.Data.DataSet ds = new System.Data.DataSet();
                DataTable dt2 = new DataTable();

                if (dt1.Rows.Count > 0)
                {
                    foreach (DataRow row in dt1.Rows)
                    {
                        payslipDetailsResponse P = new payslipDetailsResponse()
                        {
                            EmpId = row["EmpId"].ToString(),
                            type = row["type"].ToString(),
                            days = row["days"].ToString(),
                            amount = row["amount"].ToString(),
                            code = row["code"].ToString()
                        };
                        respDetails.Add(P);
                    }
                }

                //dt1 = ToDataTable(deductions);
                dt.TableName = "Payslip";
                ds.Tables.Add(dt);



                //public Dataset ds = new DataSet();
                attendance = (from x in respDetails
                              where x.type.Equals("attendance")
                              select new payslipDetailsResponse
                              {

                                  EmpId = x.EmpId,
                                  type = x.type,
                                  days = x.days,
                                  amount = x.amount,
                                  code = x.code
                              }).ToList();
                dt2 = ToDataTable(attendance);
                dt2.TableName = "Attendance";
                ds.Tables.Add(dt2);

                //public Dataset ds = new DataSet();
                deductions = (from x in respDetails
                              where x.type.Equals("Deductions")
                              select new payslipDetailsResponse
                              {

                                  EmpId = x.EmpId,
                                  type = x.type,
                                  days = x.days,
                                  amount = x.amount,
                                  code = x.code
                              }).ToList();
                dt2 = ToDataTable(deductions);
                dt2.TableName = "Deduction";
                ds.Tables.Add(dt2);

                taxable = (from x in respDetails
                           where x.type.Equals("Taxable")
                           select new payslipDetailsResponse
                           {

                               EmpId = x.EmpId,
                               type = x.type,
                               days = x.days,
                               amount = x.amount,
                               code = x.code
                           }).ToList();
                dt2 = ToDataTable(taxable);
                dt2.TableName = "TEarnings";
                ds.Tables.Add(dt2);

                nonTaxable = (from x in respDetails
                              where x.type.Equals("NonTaxable")
                              select new payslipDetailsResponse
                              {

                                  EmpId = x.EmpId,
                                  type = x.type,
                                  days = x.days,
                                  amount = x.amount,
                                  code = x.code
                              }).ToList();
                dt2 = ToDataTable(nonTaxable);
                dt2.TableName = "NTEarnings";
                ds.Tables.Add(dt2);

                overtime = (from x in respDetails
                            where x.type.Equals("Overtime")
                            select new payslipDetailsResponse
                            {

                                EmpId = x.EmpId,
                                type = x.type,
                                days = x.days,
                                amount = x.amount,
                                code = x.code
                            }).ToList();
                dt2 = ToDataTable(overtime);
                dt2.TableName = "Overtime";
                ds.Tables.Add(dt2);

                loan = (from x in respDetails
                        where x.type.Equals("Loans")
                        select new payslipDetailsResponse
                        {

                            EmpId = x.EmpId,
                            type = x.type,
                            days = x.days,
                            amount = x.amount,
                            code = x.code
                        }).ToList();
                dt2 = ToDataTable(nonTaxable);
                dt2.TableName = "Loans";
                ds.Tables.Add(dt2);

                ReportDocument myReportDocument;
                myReportDocument = new ReportDocument();

                myReportDocument.Load(HttpContext.Current.Server.MapPath("~/Reports/rptPayslip.rpt"));
                //myReportDocument.Load(HttpContext.Current.Server.MapPath("~/Reports/dynamic_payslip.rpt"));
                myReportDocument.SetDataSource(ds);
                
                //myReportDocument.Load(path);

                //CrystalReportViewer1.ReportSource = myReportDocument;

                //CrystalReportViewer1.DataBind();

                //TempConnectionStringv4("legacy");
                Connection con3 = new Connection();
                con3.myparameters.Add(new myParameters { ParameterName = "@tenantID", mytype = SqlDbType.VarChar, Value = tenantID });
                string tenantName = con3.ExecuteScalar("sp_GetTenantName_Payslip");
                //Directory.CreateDirectory(tenantName);
                Directory.CreateDirectory(HttpContext.Current.Server.MapPath("~/GeneratedPayslip/" + tenantName));
                string fileNameIden = userID.Trim(new char[] { '{', '}' });
                string[] filename2 = payoutDate.Split(' ');
                string lastName = "";
                // string finalFilePath = HttpContext.Current.Server.MapPath("~/GeneratedPayslip/" + empID + "_" + filename2[0].Trim('-') + ".pdf");
                string finalFilePath = HttpContext.Current.Server.MapPath("~/GeneratedPayslip/" + tenantName + "/" + empID + "_" + lastName + "_" + filename2[0].Trim('-') + ".pdf");
                myReportDocument.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, finalFilePath);
                myReportDocument.Dispose();


                //var a = CrystalReportViewer1.ParameterFieldInfo;
                //CrystalReportViewer1.ParameterFieldInfo = paramfs;
                // CrystalReportViewer.HasToggleGroupTreeButton = false;
                //CrystalReportViewer.ToolPanelView = CrystalDecisions.Web.ToolPanelViewType.None;
                //CrystalReportViewer.HasToggleParameterPanelButton = false;

                //var path = "D:\\HR-ILM\\ILMServer\\ILMServer\\CrystalReportManagement\\ExportPayslip\\Payslip" + item.last_name + ".pdf";


                return finalFilePath;

            }
            catch (Exception e)
            {
                var a = e.Message;
                return e.ToString();
            }


       
        }

        public DataTable ToDataTable<T>(List<T> items)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);
            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);



            foreach (PropertyInfo prop in Props)
            {
                dataTable.Columns.Add(prop.Name);
            }



            foreach (T item in items)
            {
                var values = new object[Props.Length];
                for (int i = 0; i < Props.Length; i++)
                {
                    values[i] = Props[i].GetValue(item, null);
                }
                dataTable.Rows.Add(values);
            }
            return dataTable;
        }
        public string GeneratePayslip(string empID, string userID, string payoutDate, string tenantID, string lastname, string payslipType)
        {
            try
            {
                ReportDocument reportDocument = new ReportDocument();
                ConnectionInfo conRpt = new ConnectionInfo();
                string path = "";
                if (payslipType == "crawford")
                {
                    path = HttpContext.Current.Server.MapPath("~/Payslip.rpt");
                    conRpt.DatabaseName = "DurustHRdb1";
                }
                else if (payslipType == "legacy")
                {
                    path = HttpContext.Current.Server.MapPath("~/Payslip_Legacy.rpt");
                    conRpt.DatabaseName = "LegacyPayrollDB";

                }
                reportDocument.Load(path);
                reportDocument.SetParameterValue("@empID", userID);
                //reportDocument.SetParameterValue("@tenantID", "{7FF8ECEF-092B-4FAF-B37B-C730688A3032}");
                reportDocument.SetParameterValue("@payoutDate", payoutDate);
                conRpt.ServerName = "livedb-ilm.c3srcolvclfy.ap-southeast-1.rds.amazonaws.com,1412";
                conRpt.UserID = "ILM_LiveDBUser";
                conRpt.Password = "ILMV3ry53cur3dMGTpa55w0rd2020";
                reportDocument.SetDatabaseLogon("ILM_LiveDBUser", "ILMV3ry53cur3dMGTpa55w0rd2020");
                Tables rptTables = reportDocument.Database.Tables;
                for (int i = 0; i < rptTables.Count; i++)
                {
                    CrystalDecisions.CrystalReports.Engine.Table rptTable = rptTables[i];
                    TableLogOnInfo tblInfo = rptTable.LogOnInfo;
                    tblInfo.ConnectionInfo = conRpt;
                    // next table
                }
                // if the report contains sub reports, you will need to set the connection info there too
                if (reportDocument.Subreports.Count > 0)
                {
                    for (int i = 0; i < reportDocument.Subreports.Count; i++)
                    {
                        using (ReportDocument rptSub = reportDocument.OpenSubreport(reportDocument.Subreports[i].Name))
                        {
                            Tables rptTables2 = rptSub.Database.Tables;
                            for (int ix = 0; ix < rptTables2.Count; ix++)
                            {
                                CrystalDecisions.CrystalReports.Engine.Table rptTable = rptTables2[ix];
                                TableLogOnInfo tblInfo = rptTable.LogOnInfo;
                                tblInfo.ConnectionInfo = conRpt;
                                // next table
                            }
                            rptSub.Close();
                        }
                    }
                }
                TempConnectionStringv2();
                TempConnectionStringv4(payslipType);
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@tenantID", mytype = SqlDbType.VarChar, Value = tenantID });
                string tenantName = con.ExecuteScalar("sp_GetTenantName_Payslip");
                //Directory.CreateDirectory(tenantName);
                Directory.CreateDirectory(HttpContext.Current.Server.MapPath("~/GeneratedPayslip/" + tenantName));
                string fileNameIden = userID.Trim(new char[] { '{', '}' });
                string[] filename2 = payoutDate.Split(' ');
                string finalFilePath = HttpContext.Current.Server.MapPath("~/GeneratedPayslip/" + tenantName + "/" + empID + "_" + lastname + "_" + filename2[0].Trim('-') + ".pdf");
                reportDocument.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, finalFilePath);
                reportDocument.Dispose();
                return finalFilePath;
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }
        #endregion
        [HttpPost]
        [Route("GetTenantName")]
        public string GetTenantName(report param)
        {
            TempConnectionStringv4(param.db);
            Connection con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@tenantID", mytype = System.Data.SqlDbType.VarChar, Value = param.tenantID });
            string tenantName = con.ExecuteScalar("sp_GetTenantName_Payslip");
            return tenantName;
        }
        [HttpPost]
        [Route("GetTenantID")]
        public string GetTenantID(TenantIDParams param)
        {
            TempConnectionStringv4(param.db);
            Connection con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@clientName", mytype = System.Data.SqlDbType.VarChar, Value = param.ClientName });
            con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = System.Data.SqlDbType.VarChar, Value = param.EmpID });
            string tenantID = con.ExecuteScalar("sp_GetTenantIdByClientName");
            return tenantID;
        }
        [HttpPost]
        [Route("ReportList")]
        public List<Report_List> EmpInfo_Filter(report param)
        {
            List<Report_List> List = new List<Report_List>();



            TempConnectionStringv4(param.db);
            Connection con = new Connection();

            con.myparameters.Add(new myParameters { ParameterName = "@TenantID", mytype = SqlDbType.VarChar, Value = param.tenantID });

            DataTable DT = con.GetDataTable("sp_getActiveReports");
            foreach (DataRow row in DT.Rows)
            {
                Report_List P = new Report_List()
                {
                    ReportID = row["ReportID"].ToString(),
                    ReportName = row["ReportName"].ToString(),
                    type = row["fileType"].ToString(),

                };
                List.Add(P);
            }
            return List;
        }
        [HttpPost]
        [Route("importTax")]
        public string importTax(taxImport param)
        {
            try
            {
                DataTable importList = new DataTable();
                importList.Columns.AddRange(new DataColumn[4]
                {
                        new DataColumn("Emp_ID"),
                        new DataColumn("GovDeductionType"),
                        new DataColumn("PayOutDate"),
                        new DataColumn("Amount"),

                });
                for (int i = 0; i < param.Emp_ID.Length; i++)
                {
                    importList.Rows.Add(param.Emp_ID[i],
                      param.GovDeductionType[i],
                      param.PayoutDate[i],
                      param.Amount[i]);
                    //RENEDIT
                }
                TempConnectionStringv4(param.db);
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@LoginID", mytype = SqlDbType.VarChar, Value = param.LoginID });
                con.myparameters.Add(new myParameters { ParameterName = "@ClientCode", mytype = SqlDbType.VarChar, Value = param.ClientCode });
                con.myparameters.Add(new myParameters { ParameterName = "@Data", mytype = SqlDbType.Structured, Value = importList });
                con.myparameters.Add(new myParameters { ParameterName = "@PayoutType", mytype = SqlDbType.VarChar, Value = param.PayoutType });
                con.ExecuteNonQuery("sp_PayrollOutsourcing_Import_GovDeduction");
                return "Success";
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }

        [HttpPost]
        [Route("Extract_EmployeeLoans")]
        public string EmployeeLoans(efile param)
        {

            FileStream stream = null;
            try
            {
                DataTable DTParam = new DataTable();
                DTParam.Columns.AddRange(new DataColumn[1] { new DataColumn("empid") });
                for (int i = 0; i < param.empid.Length; i++)
                {
                    DTParam.Rows.Add(param.empid[i]);
                }


                string dataDir = HostingEnvironment.MapPath("~/Files/PayrollOutSourcing/");

                stream = new FileStream(dataDir + "EmployeeLoan.xls", FileMode.Open);
                string filename;
                //string datebuild = DateTime.Now.ToString("MMddyyyy");
                string cc = "";



                string datebuild = DateTime.Now.ToString("yyyyMM");
                string timebuild = DateTime.Now.ToString("hhtt");

                filename = "Employee_Loan" + "_" + timebuild + ".xls";
                using (var fileStream = new FileStream(dataDir + filename, FileMode.Create, FileAccess.Write))
                //using (var fileStream = new FileStream(dataDir + filename, FileMode.Create, FileAccess.Write))
                {
                    stream.CopyTo(fileStream);
                }
                stream.Close();
                ExcelWorkbookFormat workbookFormat = ExcelWorkbookFormat.Xls_2003;
                string testDocFile = dataDir + "EmployeeLoan.xls";
                ExcelWorkbook workbook = new ExcelWorkbook(testDocFile);
                //  ExcelWorksheet firstWorksheet = workbook.Worksheets[0];




                ExcelWorksheet sheet6 = workbook.Worksheets[0];

                //Sheet7.WorksheetSecurity.ProtectWorksheet("sbc01");
                //AX TAXTABLE

                int x = 2;

                TempConnectionStringv4(param.db);
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@TenantID", mytype = SqlDbType.NVarChar, Value = param.TenantID });
                con.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.Structured, Value = DTParam });
                DataTable SixSheet = con.GetDataTable("getLoansPerEmployee");

                x = 2;
                for (int i = 0; i < SixSheet.Rows.Count; i++, x++)
                {
                    sheet6["A" + x].Text = SixSheet.Rows[i]["PayrollNum"].ToString();
                    sheet6["B" + x].Text = SixSheet.Rows[i]["PayDate"].ToString();
                    sheet6["C" + x].Text = SixSheet.Rows[i]["PayCode"].ToString();
                    sheet6["D" + x].Text = SixSheet.Rows[i]["EmployeeID"].ToString();
                    sheet6["E" + x].Text = SixSheet.Rows[i]["LastName"].ToString();
                    sheet6["F" + x].Text = SixSheet.Rows[i]["FirstName"].ToString();
                    sheet6["G" + x].Text = SixSheet.Rows[i]["FileStatus"].ToString();
                    sheet6["H" + x].Text = SixSheet.Rows[i]["LoanCode"].ToString();
                    sheet6["I" + x].Text = SixSheet.Rows[i]["LoanDateStart"].ToString();
                    sheet6["J" + x].Value = SixSheet.Rows[i]["LoanPrincipal"];
                    sheet6["K" + x].Value = SixSheet.Rows[i]["LoanAmount"];
                    sheet6["L" + x].Value = SixSheet.Rows[i]["LoanPayments"];
                    sheet6["M" + x].Value = SixSheet.Rows[i]["LoanBalance"];
                    sheet6["N" + x].Value = SixSheet.Rows[i]["LoanDeductionAMT"];
                    sheet6["O" + x].Text = SixSheet.Rows[i]["LoanDeductionFreq"].ToString();
                    sheet6["P" + x].Text = SixSheet.Rows[i]["LoanStatus"].ToString();
                    sheet6["Q" + x].Text = SixSheet.Rows[i]["LoanDateGranted"].ToString();
                    sheet6["R" + x].Text = SixSheet.Rows[i]["From"].ToString();
                    sheet6["S" + x].Text = SixSheet.Rows[i]["To"].ToString();
                    sheet6["T" + x].Text = SixSheet.Rows[i]["JECostCenter"].ToString();


                }


                HttpResponse httpResponse = System.Web.HttpContext.Current.Response;

                httpResponse.Clear();
                httpResponse.ContentType = "Application/x-msexcel";
                httpResponse.AddHeader("Content-Disposition", String.Format("attachment; filename={0}", filename));
                string newDIR = HttpContext.Current.Server.MapPath("~/sFTP/PayrollOutsourcing/");
                string onlineDIR = "https://ws.durusthr.com/ILM_WS_Live/sFTP/PayrollOutsourcing/";
                //filename = filename.Replace(',', '_');
                //filename = filename.Replace(" ", String.Empty);
                workbook.Save(newDIR + filename);
                stream.Close();
                //return "Success";

                if (SixSheet.Rows.Count > 0)
                {
                    return onlineDIR + filename;
                }
                else
                {

                    return "No Rows";
                }
            }
            catch (Exception e)
            {
                stream.Close();
                return "Error: " + e.ToString();
            }
        }
        [HttpPost]
        [Route("import13thMonth")]
        public string import13thMonth(taxImport param)
        {
            try
            {
                DataTable importList = new DataTable();
                importList.Columns.AddRange(new DataColumn[3]
                {
                        new DataColumn("Emp_ID"),
                        new DataColumn("PayOutDate"),
                        new DataColumn("Amount"),

                });
                for (int i = 0; i < param.Emp_ID.Length; i++)
                {
                    importList.Rows.Add(param.Emp_ID[i],
                      param.PayoutDate[i],
                      param.Amount[i]);
                }
                TempConnectionStringv4(param.db);
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@LoginID", mytype = SqlDbType.VarChar, Value = param.LoginID });
                con.myparameters.Add(new myParameters { ParameterName = "@ClientCode", mytype = SqlDbType.VarChar, Value = param.ClientCode });
                con.myparameters.Add(new myParameters { ParameterName = "@Data", mytype = SqlDbType.Structured, Value = importList });

                con.ExecuteNonQuery("sp_PayrollOutsourcing_Import_13th");
                return "Success";
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }


        [HttpPost]
        [Route("PO_GenerateandSendPayslip")]
        public List<PayslipList> GenerateandSendPayslip(PayslipParamsBulk param)
        {
            List<PayslipList> ReturnList = new List<PayslipList>();
            try
            {
                DataTable List = new DataTable();
                List.Columns.AddRange(new DataColumn[1]
                {
                    new DataColumn("EmpID")
                });
                DataTable paySlip = new DataTable();
                paySlip.Columns.AddRange(new DataColumn[5]
                {
                    new DataColumn("EmpID"),
                    new DataColumn("payOutDate"),
                    new DataColumn("link"),
                    new DataColumn("tenantID"),
                    new DataColumn("email")
                });
                for (int i = 0; i < param.empIDs.Length; i++)
                {
                    List.Rows.Add(param.empIDs[i]);
                }
                TempConnectionStringv4(param.PayslipType);
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@tenantID", mytype = SqlDbType.VarChar, Value = param.tenantID });
                con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.Structured, Value = List });
                DataTable dt = con.GetDataTable("sp_GetClientUserIds_Payslip");


                foreach (DataRow item in dt.Rows)
                {
                    string strURL = GeneratePayslipNew(item["empID"].ToString(), item["Id"].ToString() , param.payoutDate, param.tenantID, item["LastName"].ToString(), param.PayslipType.ToLower());


                    PayslipList P = new PayslipList()
                    {
                        URL = strURL
                    };
                    string payslipPath = GeneratePayslipNew(item["empID"].ToString(),  item["Id"].ToString() , param.payoutDate, param.tenantID, item["LastName"].ToString(), param.PayslipType.ToLower());
                    //Add Password
                    paySlip.Rows.Add(item["Id"].ToString(), param.payoutDate, payslipPath, param.tenantID, item["Email"].ToString());
                    if (param.hasPassword)
                    {
                        PdfSharp.Pdf.PdfDocument document = PdfSharp.Pdf.IO.PdfReader.Open(payslipPath, "some text");
                        PdfSecuritySettings securitySettings = document.SecuritySettings;
                        securitySettings.UserPassword = item["TINnum"].ToString().Replace("-", string.Empty);
                        securitySettings.OwnerPassword = "Illimitado";
                        document.Save(payslipPath);
                        //PdfStamper stamper = new PdfStamper(reader, new FileStream(payslipPath, FileMode.Create));
                        //stamper.SetEncryption(true, item["TINnum"].ToString(), "illimitado", PdfWriter.ALLOW_SCREENREADERS);
                    }
                    string LetterReceiverName = item["Firstname"].ToString() + " " + item["LastName"].ToString();
                    string[] emailDate = param.payoutDate.Split(' ');


                    //Send to email
                    //if (param.sendEmail)
                    //{
                    //    string attachmentPath = payslipPath;
                    //    string emailaccount = "durusthr@illimitado.com";
                    //    string emailpassword = "@1230Qwerty";
                    //    using (MailMessage mm = new MailMessage(emailaccount, item["Email"].ToString()))
                    //    {
                    //        MailAddress aliasmail = new MailAddress("durusthr@illimitado.com", "durusthr@illimitado.com");
                    //        MailAddress aliasreplymail = new MailAddress("durusthr@illimitado.com");
                    //        MailAddress aliasmailBCC = new MailAddress("durusthr@illimitado.com");
                    //        mm.From = aliasmail;
                    //        mm.Bcc.Add(aliasmailBCC);
                    //        if (param.PayslipType == "crawford")
                    //        {
                    //            mm.Subject = "Crawford Philippines Payslip of " + LetterReceiverName + " for " + emailDate[0];
                    //        }
                    //        else if (param.PayslipType == "legacy")
                    //        {
                    //            mm.Subject = "Payslip of " + LetterReceiverName + " for " + emailDate[0];
                    //        }
                    //        mm.Body = "";
                    //        mm.Attachments.Add(new Attachment(attachmentPath));
                    //        mm.IsBodyHtml = false;
                    //        SmtpClient smtp = new SmtpClient();
                    //        smtp.Host = "smtp.office365.com";
                    //        smtp.EnableSsl = true;
                    //        NetworkCredential NetworkCred = new NetworkCredential(emailaccount, emailpassword);
                    //        smtp.UseDefaultCredentials = true;
                    //        smtp.Credentials = NetworkCred;
                    //        smtp.Port = 587;
                    //        smtp.Send(mm);
                    //    }
                    //}
                    ReturnList.Add(P);
                }


                TempConnectionStringv4(param.PayslipType);
                Connection con2 = new Connection();
                con2.myparameters.Add(new myParameters { ParameterName = "@EmpDetails", mytype = SqlDbType.Structured, Value = paySlip });
                con2.ExecuteNonQuery("sp_insertSendEmail");
                return ReturnList;
            }
            catch (Exception e)
            {
                return ReturnList;
            }
        }
        [HttpPost]
        [Route("PO_GenerateandSendPayslip_crawford")]
        public List<PayslipList> GenerateandSendPayslip_crawford(PayslipParamsBulk param)
        {
            List<PayslipList> ReturnList = new List<PayslipList>();
            try
            {
                DataTable List = new DataTable();
                List.Columns.AddRange(new DataColumn[1]
                {
                    new DataColumn("EmpID")
                });
                DataTable paySlip = new DataTable();
                paySlip.Columns.AddRange(new DataColumn[5]
                {
                    new DataColumn("EmpID"),
                    new DataColumn("payOutDate"),
                    new DataColumn("link"),
                    new DataColumn("tenantID"),
                    new DataColumn("email")
                });
                for (int i = 0; i < param.empIDs.Length; i++)
                {
                    List.Rows.Add(param.empIDs[i]);
                }
                TempConnectionStringv4(param.PayslipType);
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@tenantID", mytype = SqlDbType.VarChar, Value = param.tenantID });
                con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.Structured, Value = List });
                DataTable dt = con.GetDataTable("sp_GetClientUserIds_Payslip");


                foreach (DataRow item in dt.Rows)
                {
                    string strURL = GeneratePayslip2(item["empID"].ToString(), "{" + item["Id"].ToString() + "}", param.payoutDate, param.tenantID, item["LastName"].ToString(), param.PayslipType.ToLower());


                    PayslipList P = new PayslipList()
                    {
                        URL = strURL
                    };
                    string payslipPath = GeneratePayslip(item["empID"].ToString(), "{" + item["Id"].ToString() + "}", param.payoutDate, param.tenantID, item["LastName"].ToString(), param.PayslipType.ToLower());
                    //Add Password
                    paySlip.Rows.Add(item["Id"].ToString(), param.payoutDate, payslipPath, param.tenantID, item["Email"].ToString());
                    if (param.hasPassword)
                    {
                        PdfSharp.Pdf.PdfDocument document = PdfSharp.Pdf.IO.PdfReader.Open(payslipPath, "some text");
                        PdfSecuritySettings securitySettings = document.SecuritySettings;
                        securitySettings.UserPassword = item["TINnum"].ToString().Replace("-", string.Empty);
                        securitySettings.OwnerPassword = "Illimitado";
                        document.Save(payslipPath);
                        //PdfStamper stamper = new PdfStamper(reader, new FileStream(payslipPath, FileMode.Create));
                        //stamper.SetEncryption(true, item["TINnum"].ToString(), "illimitado", PdfWriter.ALLOW_SCREENREADERS);
                    }
                    string LetterReceiverName = item["Firstname"].ToString() + " " + item["LastName"].ToString();
                    string[] emailDate = param.payoutDate.Split(' ');


                    //Send to email
                    //if (param.sendEmail)
                    //{
                    //    string attachmentPath = payslipPath;
                    //    string emailaccount = "durusthr@illimitado.com";
                    //    string emailpassword = "@1230Qwerty";
                    //    using (MailMessage mm = new MailMessage(emailaccount, item["Email"].ToString()))
                    //    {
                    //        MailAddress aliasmail = new MailAddress("durusthr@illimitado.com", "durusthr@illimitado.com");
                    //        MailAddress aliasreplymail = new MailAddress("durusthr@illimitado.com");
                    //        MailAddress aliasmailBCC = new MailAddress("durusthr@illimitado.com");
                    //        mm.From = aliasmail;
                    //        mm.Bcc.Add(aliasmailBCC);
                    //        if (param.PayslipType == "crawford")
                    //        {
                    //            mm.Subject = "Crawford Philippines Payslip of " + LetterReceiverName + " for " + emailDate[0];
                    //        }
                    //        else if (param.PayslipType == "legacy")
                    //        {
                    //            mm.Subject = "Payslip of " + LetterReceiverName + " for " + emailDate[0];
                    //        }
                    //        mm.Body = "";
                    //        mm.Attachments.Add(new Attachment(attachmentPath));
                    //        mm.IsBodyHtml = false;
                    //        SmtpClient smtp = new SmtpClient();
                    //        smtp.Host = "smtp.office365.com";
                    //        smtp.EnableSsl = true;
                    //        NetworkCredential NetworkCred = new NetworkCredential(emailaccount, emailpassword);
                    //        smtp.UseDefaultCredentials = true;
                    //        smtp.Credentials = NetworkCred;
                    //        smtp.Port = 587;
                    //        smtp.Send(mm);
                    //    }
                    //}
                    ReturnList.Add(P);
                }


                TempConnectionStringv4(param.PayslipType);
                Connection con2 = new Connection();
                con2.myparameters.Add(new myParameters { ParameterName = "@EmpDetails", mytype = SqlDbType.Structured, Value = paySlip });
                con2.ExecuteNonQuery("sp_insertSendEmail");
                return ReturnList;
            }
            catch (Exception e)
            {
                return ReturnList;
            }
        }



        [HttpPost]
        [Route("WS_sendPayslip")]
        public string sendPayslip(PayslipParamsBulk param)
        {
            List<PayslipList> ReturnList = new List<PayslipList>();


            #region Get WS_PayslipSendCredentials JSON
            string path = HttpContext.Current.Server.MapPath("~/appsettings.json");
            string jsonString = File.ReadAllText(path);
            var jsonObject = JObject.Parse(jsonString);
            var jsonWS_PayslipSendCredentials = jsonObject["WS_PayslipSendCredentials"].ToString();
            WS_PayslipSendCredentials sendPayslipCredentials = JsonConvert.DeserializeObject<WS_PayslipSendCredentials>(jsonWS_PayslipSendCredentials);
            #endregion

            try
            {
                DataTable List = new DataTable();
                List.Columns.AddRange(new DataColumn[1]
                {
                    new DataColumn("EmpID")
                });
                for (int i = 0; i < param.empIDs.Length; i++)
                {
                    List.Rows.Add(param.empIDs[i]);
                }
                TempConnectionStringv4(param.PayslipType);
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@tenantID", mytype = SqlDbType.VarChar, Value = param.tenantID });
                con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.Structured, Value = List });
                con.myparameters.Add(new myParameters { ParameterName = "@payoutdate", mytype = SqlDbType.VarChar, Value = param.payoutDate });
                DataTable dt = con.GetDataTable("sp_GetAllSendPayslip");

                foreach (DataRow item in dt.Rows)
                {
                    string attachmentPath, emailaccount, emailpassword;
                    if (param.PayslipType == "legacy")
                    {
                        attachmentPath = item["PDFLink"].ToString();
                        //emailaccount = "durusthr@illimitado.com";
                        //emailpassword = "@1230Qwerty";                        
                        emailaccount = sendPayslipCredentials.legacy_email;
                        emailpassword = sendPayslipCredentials.legacy_password;
                    }
                    else
                    {
                        attachmentPath = item["PDFLink"].ToString();
                        //emailaccount = "payslip@illimitado.com";
                        //emailpassword = "Cal75266";
                        emailaccount = sendPayslipCredentials.crawford_email;
                        emailpassword = sendPayslipCredentials.crawford_password;
                    }


                    using (MailMessage mm = new MailMessage(emailaccount, item["Email"].ToString()))
                    {
                        ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
                        MailAddress aliasmail = new MailAddress(emailaccount, emailaccount);
                        MailAddress aliasreplymail = new MailAddress(emailaccount);
                        //MailAddress aliasmailBCC = new MailAddress(emailaccount);
                        mm.From = aliasmail;
                        //mm.Bcc.Add(aliasmailBCC);
                        if (param.PayslipType == "crawford")
                        {
                            mm.Subject = "Crawford Philippines Payslip of " + item["LetterReceiverName"].ToString() + " for " + item["emailDate"].ToString();
                        }
                        else if (param.PayslipType == "legacy")
                        {
                            mm.Subject = "Payslip of " + item["LetterReceiverName"].ToString() + " for " + item["emailDate"].ToString();
                        }
                        mm.Body = "";
                        mm.Attachments.Add(new Attachment(attachmentPath));
                        mm.IsBodyHtml = false;
                        SmtpClient smtp = new SmtpClient();
                        smtp.Host = "smtp.office365.com";
                        smtp.EnableSsl = true;
                        NetworkCredential NetworkCred = new NetworkCredential(emailaccount, emailpassword);
                        smtp.UseDefaultCredentials = true;
                        smtp.Credentials = NetworkCred;
                        smtp.Port = 587;
                        smtp.Send(mm);
                    }
                }


                //Send to email
                //if (param.sendEmail)
                //{

                //}

                return "Success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }
        [HttpPost]
        [Route("delete13thMonth")]
        public string delete13thMonth(taxDelete param)
        {
            try
            {
                TempConnectionStringv4(param.db);
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@ClientCode", mytype = SqlDbType.VarChar, Value = param.ClientCode });
                con.myparameters.Add(new myParameters { ParameterName = "@PayoutDate", mytype = SqlDbType.VarChar, Value = param.PayOutDate });

                con.ExecuteNonQuery("sp_Delete13thMonth");
                return "Success";
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }

        [HttpPost]
        [Route("deleteGovDeduction")]
        public string deleteGovDeduction(taxDelete param)
        {
            try
            {
                TempConnectionStringv4(param.db);
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@ClientCode", mytype = SqlDbType.VarChar, Value = param.ClientCode });
                con.myparameters.Add(new myParameters { ParameterName = "@PayoutDate", mytype = SqlDbType.VarChar, Value = param.PayOutDate });
                con.myparameters.Add(new myParameters { ParameterName = "@PayoutType", mytype = SqlDbType.VarChar, Value = param.PayoutType });
                con.ExecuteNonQuery("sp_DeleteGovDed");
                return "Success";
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }
        [HttpPost]
        [Route("deleteTax")]
        public string deleteTax(taxDelete param)
        {
            try
            {
                TempConnectionStringv4(param.db);
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@ClientCode", mytype = SqlDbType.VarChar, Value = param.ClientCode });
                con.myparameters.Add(new myParameters { ParameterName = "@PayoutDate", mytype = SqlDbType.VarChar, Value = param.PayOutDate });


                con.ExecuteNonQuery("sp_DeleteTax");
                return "Success";
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }
        [HttpPost]
        [Route("PO_AccountCreation")]
        public ReturnAC AC(PO_AccountCreation param)
        {
            ReturnAC ACStatus = new ReturnAC();
            try
            {
                TempConnectionStringv4(param.db);
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@userid", mytype = SqlDbType.VarChar, Value = param.userid });
                con.myparameters.Add(new myParameters { ParameterName = "@password", mytype = SqlDbType.VarChar, Value = param.password });
                con.myparameters.Add(new myParameters { ParameterName = "@tenantid", mytype = SqlDbType.VarChar, Value = param.tenantid });
                DataTable DT = con.GetDataTable("sp_CreateAccount");
                foreach (DataRow row in DT.Rows)
                {
                    ACStatus.Status = row["Status"].ToString();
                }
            }
            catch (Exception e)
            {
                ACStatus.Status = e.ToString();
            }
            return ACStatus;
        }
        [HttpPost]
        [Route("DeteleteDetails")]
        public string DeleteDetails(delete_Details param)
        {
            try
            {
                TempConnectionStringv4(param.db);
                Connection con = new Connection();

                con.myparameters.Add(new myParameters { ParameterName = "@cutoffstart", mytype = SqlDbType.VarChar, Value = param.cutOffStart });
                con.myparameters.Add(new myParameters { ParameterName = "@cutoffend", mytype = SqlDbType.VarChar, Value = param.cutOffEnd });
                con.myparameters.Add(new myParameters { ParameterName = "@tenantid", mytype = SqlDbType.VarChar, Value = param.tenantID });
                con.myparameters.Add(new myParameters { ParameterName = "@payouttype", mytype = SqlDbType.VarChar, Value = param.PayOutType });
                con.myparameters.Add(new myParameters { ParameterName = "@option", mytype = SqlDbType.VarChar, Value = param.option });
                con.myparameters.Add(new myParameters { ParameterName = "@ifAdj", mytype = SqlDbType.VarChar, Value = param.ifAdj });
                return con.ExecuteScalar("sp_deleteTK").ToString();

            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }



        [Route("EmployeeInfo_Filter")]
        public List<EmployeeInfo_Filter_List> EmpInfo_Filter(empinfo param)
        {
            List<EmployeeInfo_Filter_List> List = new List<EmployeeInfo_Filter_List>();

            DataTable DTParam = new DataTable();
            DTParam.Columns.AddRange(new DataColumn[1] { new DataColumn("empid") });
            for (int i = 0; i < param.EmpID.Length; i++)
            {
                DTParam.Rows.Add(param.EmpID[i]);
            }

            TempConnectionStringv4(param.db);
            Connection con = new Connection();

            con.myparameters.Add(new myParameters { ParameterName = "@TenantCode", mytype = SqlDbType.VarChar, Value = param.TenantCode });
            con.myparameters.Add(new myParameters { ParameterName = "@EmpId", mytype = SqlDbType.Structured, Value = DTParam });
            DataTable DT = con.GetDataTable("sp_EmployeeInfo_Filter");
            foreach (DataRow row in DT.Rows)
            {
                EmployeeInfo_Filter_List P = new EmployeeInfo_Filter_List()
                {
                    EmpID = row["EmpID"].ToString(),
                    LastName = row["LastName"].ToString(),
                    FirstName = row["FirstName"].ToString(),
                    MiddleName = row["MiddleName"].ToString(),
                    Rehired = row["Rehired"].ToString(),
                    Gender = row["Gender"].ToString(),
                    Email = row["Email"].ToString(),
                    GradeLevel = row["GradeLevel"].ToString(),
                    CostCenter = row["CostCenter"].ToString(),
                    JobTitle = row["JobTitle"].ToString(),
                    CurrentDivision = row["CurrentDivision"].ToString(),
                    CurrentDepartment = row["CurrentDepartment"].ToString(),
                    LaborType = row["LaborType"].ToString(),
                    BirthDate = row["BirthDate"].ToString(),
                    DateEmployed = row["DateEmployed"].ToString(),
                    AllowedOT = row["AllowedOT"].ToString(),
                    TaxStatus = row["TaxStatus"].ToString(),
                    MonthlyRate = row["MonthlyRate"].ToString(),
                    AdditionalHDMF = row["AdditionalHDMF"].ToString(),
                    PaymentType = row["PaymentType"].ToString(),
                    AccountNumber = row["AccountNumber"].ToString(),
                    PagibigNum = row["PagibigNum"].ToString(),
                    SSSNum = row["SSSNum"].ToString(),
                    TINnum = row["TINnum"].ToString(),
                    PhilhealthNum = row["PhilhealthNum"].ToString(),
                    EmploymentStatus = row["EmploymentStatus"].ToString(),
                    PayrollStatus = row["PayrollStatus"].ToString(),
                    EmployeeType = row["EmployeeType"].ToString(),
                    PermanentBlock = row["PermanentBlock"].ToString(),
                    PermamnetAddress = row["PermamnetAddress"].ToString(),
                    HomePhone = row["HomePhone"].ToString(),
                    AnnualRt = row["AnnualRt"].ToString(),
                    AWD = row["AWD"].ToString(),
                    Currency = row["Currency"].ToString(),
                    Dateterminated = row["Dateterminated"].ToString(),
                    RateType = row["RateType"].ToString(),
                    MWE = row["MWE"].ToString(),
                    isSSS = row["isSSS"].ToString(),
                    isPhic = row["isPhic"].ToString(),
                    isHDMF = row["isHDMF"].ToString(),
                    isWHTAX = row["isWHTAX"].ToString(),
                    SalaryType = row["SalaryType"].ToString()
                };
                List.Add(P);
            }
            return List;
        }

        [HttpPost]
        [Route("EmployeeInfo_Update")]
        public string EmpInfo_Update(EmployeeInfo_Update param)
        {
            try
            {
                TempConnectionStringv4(param.db);
                Connection con = new Connection();

                con.myparameters.Add(new myParameters { ParameterName = "@TenantCode", mytype = SqlDbType.VarChar, Value = param.TenantCode });
                con.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.VarChar, Value = param.EmpID });
                con.myparameters.Add(new myParameters { ParameterName = "@NewEmpID", mytype = SqlDbType.VarChar, Value = param.NewEmpID });
                con.myparameters.Add(new myParameters { ParameterName = "@LastName", mytype = SqlDbType.VarChar, Value = param.LastName });
                con.myparameters.Add(new myParameters { ParameterName = "@FirstName", mytype = SqlDbType.VarChar, Value = param.FirstName });
                con.myparameters.Add(new myParameters { ParameterName = "@MiddleName", mytype = SqlDbType.VarChar, Value = param.MiddleName });
                con.myparameters.Add(new myParameters { ParameterName = "@Rehired", mytype = SqlDbType.VarChar, Value = param.Rehired });
                con.myparameters.Add(new myParameters { ParameterName = "@Gender", mytype = SqlDbType.VarChar, Value = param.Gender });
                con.myparameters.Add(new myParameters { ParameterName = "@Email", mytype = SqlDbType.VarChar, Value = param.Email });
                con.myparameters.Add(new myParameters { ParameterName = "@GradeLevel", mytype = SqlDbType.VarChar, Value = param.GradeLevel });
                con.myparameters.Add(new myParameters { ParameterName = "@CostCenter", mytype = SqlDbType.VarChar, Value = param.CostCenter });
                con.myparameters.Add(new myParameters { ParameterName = "@JobTitle", mytype = SqlDbType.VarChar, Value = param.JobTitle });
                con.myparameters.Add(new myParameters { ParameterName = "@CurrentDivision", mytype = SqlDbType.VarChar, Value = param.CurrentDivision });
                con.myparameters.Add(new myParameters { ParameterName = "@CurrentDepartment", mytype = SqlDbType.VarChar, Value = param.CurrentDepartment });
                con.myparameters.Add(new myParameters { ParameterName = "@LaborType", mytype = SqlDbType.VarChar, Value = param.LaborType });
                con.myparameters.Add(new myParameters { ParameterName = "@BirthDate", mytype = SqlDbType.VarChar, Value = param.BirthDate });
                con.myparameters.Add(new myParameters { ParameterName = "@DateEmployed", mytype = SqlDbType.VarChar, Value = param.DateEmployed });
                con.myparameters.Add(new myParameters { ParameterName = "@AllowedOT", mytype = SqlDbType.VarChar, Value = param.AllowedOT });
                con.myparameters.Add(new myParameters { ParameterName = "@TaxStatus", mytype = SqlDbType.VarChar, Value = param.TaxStatus });
                con.myparameters.Add(new myParameters { ParameterName = "@MonthlyRate", mytype = SqlDbType.VarChar, Value = param.MonthlyRate });
                con.myparameters.Add(new myParameters { ParameterName = "@AdditionalHDMF", mytype = SqlDbType.VarChar, Value = param.AdditionalHDMF });
                con.myparameters.Add(new myParameters { ParameterName = "@PaymentType", mytype = SqlDbType.VarChar, Value = param.PaymentType });
                con.myparameters.Add(new myParameters { ParameterName = "@AccountNumber", mytype = SqlDbType.VarChar, Value = param.AccountNumber });
                con.myparameters.Add(new myParameters { ParameterName = "@PagibigNum", mytype = SqlDbType.VarChar, Value = param.PagibigNum });
                con.myparameters.Add(new myParameters { ParameterName = "@SSSNum", mytype = SqlDbType.VarChar, Value = param.SSSNum });
                con.myparameters.Add(new myParameters { ParameterName = "@TINnum", mytype = SqlDbType.VarChar, Value = param.TINnum });
                con.myparameters.Add(new myParameters { ParameterName = "@PhilhealthNum", mytype = SqlDbType.VarChar, Value = param.PhilhealthNum });
                con.myparameters.Add(new myParameters { ParameterName = "@EmploymentStatus", mytype = SqlDbType.VarChar, Value = param.EmploymentStatus });
                con.myparameters.Add(new myParameters { ParameterName = "@PayrollStatus", mytype = SqlDbType.VarChar, Value = param.PayrollStatus });
                con.myparameters.Add(new myParameters { ParameterName = "@EmployeeType", mytype = SqlDbType.VarChar, Value = param.EmployeeType });
                con.myparameters.Add(new myParameters { ParameterName = "@PermanentBlock", mytype = SqlDbType.VarChar, Value = param.PermanentBlock });
                con.myparameters.Add(new myParameters { ParameterName = "@PermamnetAddress", mytype = SqlDbType.VarChar, Value = param.PermamnetAddress });
                con.myparameters.Add(new myParameters { ParameterName = "@HomePhone", mytype = SqlDbType.VarChar, Value = param.HomePhone });
                con.myparameters.Add(new myParameters { ParameterName = "@AnnualRt", mytype = SqlDbType.VarChar, Value = param.AnnualRt });
                con.myparameters.Add(new myParameters { ParameterName = "@AWD", mytype = SqlDbType.VarChar, Value = param.AWD });
                con.myparameters.Add(new myParameters { ParameterName = "@Currency", mytype = SqlDbType.VarChar, Value = param.Currency });
                con.myparameters.Add(new myParameters { ParameterName = "@Dateterminated", mytype = SqlDbType.VarChar, Value = param.Dateterminated });
                con.myparameters.Add(new myParameters { ParameterName = "@RateType", mytype = SqlDbType.VarChar, Value = param.RateType });
                con.myparameters.Add(new myParameters { ParameterName = "@MWE", mytype = SqlDbType.VarChar, Value = param.MWE });

                con.myparameters.Add(new myParameters { ParameterName = "@isSSS", mytype = SqlDbType.VarChar, Value = param.isSSS });
                con.myparameters.Add(new myParameters { ParameterName = "@isPhic", mytype = SqlDbType.VarChar, Value = param.isPhic });
                con.myparameters.Add(new myParameters { ParameterName = "@isHDMF", mytype = SqlDbType.VarChar, Value = param.isHDMF });
                con.myparameters.Add(new myParameters { ParameterName = "@isWHTAX", mytype = SqlDbType.VarChar, Value = param.isWHTAX });
                con.myparameters.Add(new myParameters { ParameterName = "@SalaryType", mytype = SqlDbType.VarChar, Value = param.SalaryType });
                con.ExecuteNonQuery("sp_EmployeeInfo_Update");
                return "Success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }

        [HttpPost]
        [Route("Benefits_DeleteED")]
        public string Benefits_DeleteED(benefit param)
        {
            try
            {
                TempConnectionStringv4(param.db);
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@tenantID", mytype = SqlDbType.VarChar, Value = param.TenantID });
                con.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.VarChar, Value = param.ID });
                con.myparameters.Add(new myParameters { ParameterName = "@EDType", mytype = SqlDbType.VarChar, Value = param.EDType });
                con.myparameters.Add(new myParameters { ParameterName = "@deleteEDID", mytype = SqlDbType.VarChar, Value = param.EDID });
                con.myparameters.Add(new myParameters { ParameterName = "@Date", mytype = SqlDbType.VarChar, Value = param.Date });
                con.myparameters.Add(new myParameters { ParameterName = "@Code", mytype = SqlDbType.VarChar, Value = param.Code });
                con.myparameters.Add(new myParameters { ParameterName = "@Amount", mytype = SqlDbType.VarChar, Value = param.Amount });
                con.myparameters.Add(new myParameters { ParameterName = "@RecurStart", mytype = SqlDbType.VarChar, Value = param.RecurStart });
                con.myparameters.Add(new myParameters { ParameterName = "@RecurEnd", mytype = SqlDbType.VarChar, Value = param.RecurEnd });
                con.myparameters.Add(new myParameters { ParameterName = "@Frequency", mytype = SqlDbType.VarChar, Value = param.Frequency });
                con.myparameters.Add(new myParameters { ParameterName = "@Remarks", mytype = SqlDbType.VarChar, Value = param.Remarks });
                con.myparameters.Add(new myParameters { ParameterName = "@ClosedOn", mytype = SqlDbType.VarChar, Value = param.ClosedOn });
                con.ExecuteNonQuery("sp_Benefits_DelelteED");
                return "Success";
            }   
            catch (Exception e)
            {
                return e.ToString();
            }
        }

        [HttpPost]
        [Route("Benefits_DeleteED_Details")]
        public string Benefits_DeleteED_Details(benefit param)
        {
            try
            {
                TempConnectionStringv4(param.db);
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@tenantID", mytype = SqlDbType.VarChar, Value = param.TenantID });
                con.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.VarChar, Value = param.ID });
                con.myparameters.Add(new myParameters { ParameterName = "@EDType", mytype = SqlDbType.VarChar, Value = param.EDType });

                con.myparameters.Add(new myParameters { ParameterName = "@Code", mytype = SqlDbType.VarChar, Value = param.Code });
                con.myparameters.Add(new myParameters { ParameterName = "@Amount", mytype = SqlDbType.VarChar, Value = param.Amount });
                con.myparameters.Add(new myParameters { ParameterName = "@payoutdate", mytype = SqlDbType.VarChar, Value = param.payoutdate });
                con.myparameters.Add(new myParameters { ParameterName = "@PaymentTerms", mytype = SqlDbType.VarChar, Value = param.PaymentTerms });
                con.ExecuteNonQuery("sp_Benefits_DelelteED_Details");
                return "Success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }


        //ADDED FOR CLIENT SETTINGS
        [HttpPost]
        [Route("CS_getInformation")]
        public List<getClientInfoList> getInformation(csinfo param)
        {
            List<getClientInfoList> List = new List<getClientInfoList>();
            TempConnectionStringv4(param.db);
            Connection con = new Connection();

            con.myparameters.Add(new myParameters { ParameterName = "@tenantId", mytype = SqlDbType.VarChar, Value = param.TenantId });
            DataTable DT = con.GetDataTable("sp_CS_getInformation");
            foreach (DataRow row in DT.Rows)
            {
                getClientInfoList P = new getClientInfoList()
                {
                    TenantCode = row["TenantCode"].ToString(),
                    CompanyName = row["CompanyName"].ToString(),
                    CompanyAdd1 = row["CompanyAdd1"].ToString(),
                    CompanyAdd2 = row["CompanyAdd2"].ToString(),
                    SSSID = row["SSSID"].ToString(),
                    PHICID = row["PHICID"].ToString(),
                    HDMFID = row["HDMFID"].ToString(),
                    TIN = row["TIN"].ToString(),
                    isSSS = row["isSSS"].ToString(),
                    isPHIC = row["isPHIC"].ToString(),
                    isHDMF = row["isHDMF"].ToString(),
                    isTIN = row["isTIN"].ToString(),
                    Start1stCutoff = row["Start1stCutoff"].ToString(),
                    End1stCutoff = row["End1stCutoff"].ToString(),
                    Payday1st = row["Payday1st"].ToString(),
                    Start2ndCutoff = row["Start2ndCutoff"].ToString(),
                    End2ndCutoff = row["End2ndCutoff"].ToString(),
                    Payday2nd = row["Payday2nd"].ToString(),

                    BankName = row["BankName"].ToString(),
                    BankAccountNumber = row["BankAccountNumber"].ToString(),
                    AnnualWorkingDays = row["AnnualWorkingDays"].ToString(),
                    isPayOutDate = row["isPayOutDate"].ToString(),

                };
                List.Add(P);
            }
            return List;
        }

        [HttpPost]
        [Route("CS_deletePCLoans")]
        public string DeleteLoanCodes(PCLoans param)
        {
            try
            {
                TempConnectionStringv4(param.db);
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@tenantId", mytype = SqlDbType.VarChar, Value = param.TenantID });
                con.myparameters.Add(new myParameters { ParameterName = "@LoanCode", mytype = SqlDbType.VarChar, Value = param.LoanCode });
                con.myparameters.Add(new myParameters { ParameterName = "@LoanName", mytype = SqlDbType.VarChar, Value = param.LoanName });
                con.myparameters.Add(new myParameters { ParameterName = "@LoanType", mytype = SqlDbType.VarChar, Value = param.LoanType });
                con.ExecuteNonQuery("sp_CS_deletePaycodeLoans");
                return "Success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }

        [HttpPost]
        [Route("CS_getPaycodeLoans")]
        public List<getPaycodeLoansList> getPCLoans(cspcloans param)
        {
            List<getPaycodeLoansList> List = new List<getPaycodeLoansList>();


            TempConnectionStringv4(param.db);
            Connection con = new Connection();

            con.myparameters.Add(new myParameters { ParameterName = "@tenantId", mytype = SqlDbType.VarChar, Value = param.TenantId });
            DataTable DT = con.GetDataTable("sp_CS_getPaycodeLoans");
            foreach (DataRow row in DT.Rows)
            {
                getPaycodeLoansList P = new getPaycodeLoansList()
                {
                    LoanNameID = row["LoanNameID"].ToString(),
                    LoanCode = row["LoanCode"].ToString(),
                    LoanName = row["LoanName"].ToString(),
                    LoanType = row["LoanType"].ToString(),
                };
                List.Add(P);
            }
            return List;
        }


        [HttpPost]
        [Route("CS_updateInformation")]
        public string UpdateInformation(UpdateInfo param)
        {
            try
            {
                TempConnectionStringv4(param.db);
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@tenantId", mytype = SqlDbType.VarChar, Value = param.TenantID });
                con.myparameters.Add(new myParameters { ParameterName = "@TenantCode", mytype = SqlDbType.VarChar, Value = param.TenantCode });
                con.myparameters.Add(new myParameters { ParameterName = "@TenantName", mytype = SqlDbType.VarChar, Value = param.TenantName });
                con.myparameters.Add(new myParameters { ParameterName = "@Address1", mytype = SqlDbType.VarChar, Value = param.Address1 });
                con.myparameters.Add(new myParameters { ParameterName = "@Address2", mytype = SqlDbType.VarChar, Value = param.Address2 });
                con.myparameters.Add(new myParameters { ParameterName = "@SSSID", mytype = SqlDbType.VarChar, Value = param.SSSID });
                con.myparameters.Add(new myParameters { ParameterName = "@PHICID", mytype = SqlDbType.VarChar, Value = param.PHICID });
                con.myparameters.Add(new myParameters { ParameterName = "@HDMFID", mytype = SqlDbType.VarChar, Value = param.HDMFID });
                con.myparameters.Add(new myParameters { ParameterName = "@TINID", mytype = SqlDbType.VarChar, Value = param.TINID });
                con.myparameters.Add(new myParameters { ParameterName = "@isSSS", mytype = SqlDbType.VarChar, Value = param.isSSS });
                con.myparameters.Add(new myParameters { ParameterName = "@isPHIC", mytype = SqlDbType.VarChar, Value = param.isPHIC });
                con.myparameters.Add(new myParameters { ParameterName = "@isHDMF", mytype = SqlDbType.VarChar, Value = param.isHDMF });
                con.myparameters.Add(new myParameters { ParameterName = "@isTIN", mytype = SqlDbType.VarChar, Value = param.isTIN });
                con.myparameters.Add(new myParameters { ParameterName = "@Start1stCutoff", mytype = SqlDbType.VarChar, Value = param.Start1stCutoff });
                con.myparameters.Add(new myParameters { ParameterName = "@End1stCutoff", mytype = SqlDbType.VarChar, Value = param.End1stCutoff });
                con.myparameters.Add(new myParameters { ParameterName = "@Payday1st", mytype = SqlDbType.VarChar, Value = param.Payday1st });
                con.myparameters.Add(new myParameters { ParameterName = "@Start2ndCutoff", mytype = SqlDbType.VarChar, Value = param.Start2ndCutoff });
                con.myparameters.Add(new myParameters { ParameterName = "@End2ndCutoff", mytype = SqlDbType.VarChar, Value = param.End2ndCutoff });
                con.myparameters.Add(new myParameters { ParameterName = "@Payday2nd", mytype = SqlDbType.VarChar, Value = param.Payday2nd });

                con.myparameters.Add(new myParameters { ParameterName = "@BankName", mytype = SqlDbType.VarChar, Value = param.BankName });
                con.myparameters.Add(new myParameters { ParameterName = "@BankAccountNumber", mytype = SqlDbType.VarChar, Value = param.BankAccountNumber });
                con.myparameters.Add(new myParameters { ParameterName = "@AnnualWorkingDays", mytype = SqlDbType.VarChar, Value = param.AnnualWorkingDays });
                con.myparameters.Add(new myParameters { ParameterName = "@isPayOutDate", mytype = SqlDbType.VarChar, Value = param.isPayOutDate });


                con.ExecuteNonQuery("sp_CS_updateInformation");
                return "Success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }

        [HttpPost]
        [Route("CS_updatePaycodeLoans")]
        public string UpdatePaycodeLoans(UpdatePCLoans param)
        {
            try
            {
                TempConnectionStringv4(param.db);
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@tenantId", mytype = SqlDbType.VarChar, Value = param.TenantID });
                con.myparameters.Add(new myParameters { ParameterName = "@LoanID", mytype = SqlDbType.VarChar, Value = param.LoanID });
                con.myparameters.Add(new myParameters { ParameterName = "@LoanCode", mytype = SqlDbType.VarChar, Value = param.LoanCode });
                con.myparameters.Add(new myParameters { ParameterName = "@LoanName", mytype = SqlDbType.VarChar, Value = param.LoanName });
                con.myparameters.Add(new myParameters { ParameterName = "@LoanType", mytype = SqlDbType.VarChar, Value = param.LoanType });
                con.myparameters.Add(new myParameters { ParameterName = "@IsDelete", mytype = SqlDbType.VarChar, Value = param.IsDelete });
                con.myparameters.Add(new myParameters { ParameterName = "@logUser", mytype = SqlDbType.VarChar, Value = param.LogUser });

                con.ExecuteNonQuery("sp_CS_updatePaycodeLoans");
                return "Success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }


        [HttpPost]
        [Route("CS_getEarningsCode")]
        public List<getPaycodeEarnings> getPCEarnings(cspcloans param)
        {
            List<getPaycodeEarnings> List = new List<getPaycodeEarnings>();


            TempConnectionStringv4(param.db);
            Connection con = new Connection();

            con.myparameters.Add(new myParameters { ParameterName = "@tenantId", mytype = SqlDbType.VarChar, Value = param.TenantId });
            DataTable DT = con.GetDataTable("sp_CS_getEarningsCode");
            foreach (DataRow row in DT.Rows)
            {
                getPaycodeEarnings P = new getPaycodeEarnings()
                {
                    EarningsTypeID = row["EarningsTypeID"].ToString(),
                    EarningsCode = row["EarningsCode"].ToString(),
                    EarningsDescription = row["EarningsDescription"].ToString(),
                    EarningsCategory = row["EarningsCategory"].ToString(),
                    IsTaxable = row["IsTaxable"].ToString(),
                    IsAbsenceAffected = row["IsAbsenceAffected"].ToString(),
                };
                List.Add(P);
            }
            return List;
        }


        [HttpPost]
        [Route("CS_getDeductionCode")]
        public List<getPaycodeDeductions> getPCDeductions(cspcloans param)
        {
            List<getPaycodeDeductions> List = new List<getPaycodeDeductions>();


            TempConnectionStringv4(param.db);
            Connection con = new Connection();

            con.myparameters.Add(new myParameters { ParameterName = "@tenantId", mytype = SqlDbType.VarChar, Value = param.TenantId });
            DataTable DT = con.GetDataTable("sp_CS_getDeductionCode");
            foreach (DataRow row in DT.Rows)
            {
                getPaycodeDeductions P = new getPaycodeDeductions()
                {
                    DeductionsTypeID = row["DeductionsTypeID"].ToString(),
                    DeductionsCode = row["DeductionsCode"].ToString(),
                    DeductionsDescription = row["DeductionsDescription"].ToString(),
                    DeductionsCategory = row["DeductionsCategory"].ToString(),
                    IsTaxable = row["IsTaxable"].ToString(),
                    IsBonus = row["IsBonus"].ToString(),
                };
                List.Add(P);
            }
            return List;
        }

        [HttpPost]
        [Route("CS_updatedeleteEarningsCode")]
        public string UpDeleteEarningsCode(UpdeleteEarningsCode param)
        {
            try
            {
                TempConnectionStringv4(param.db);
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@tenantId", mytype = SqlDbType.VarChar, Value = param.TenantID });
                con.myparameters.Add(new myParameters { ParameterName = "@logUser", mytype = SqlDbType.VarChar, Value = param.LogUser });
                con.myparameters.Add(new myParameters { ParameterName = "@EarningsTypeID", mytype = SqlDbType.VarChar, Value = param.EarningsTypeID });
                con.myparameters.Add(new myParameters { ParameterName = "@EarningsCode", mytype = SqlDbType.VarChar, Value = param.EarningsCode });
                con.myparameters.Add(new myParameters { ParameterName = "@EarningsDesc", mytype = SqlDbType.VarChar, Value = param.EarningsDesc });
                con.myparameters.Add(new myParameters { ParameterName = "@EarningsCategory", mytype = SqlDbType.VarChar, Value = param.EarningsCategory });
                con.myparameters.Add(new myParameters { ParameterName = "@IsAbsenceAffected", mytype = SqlDbType.VarChar, Value = param.IsAbsenceAffected });
                con.myparameters.Add(new myParameters { ParameterName = "@isTaxable", mytype = SqlDbType.VarChar, Value = param.isTaxable });
                con.myparameters.Add(new myParameters { ParameterName = "@isDelete", mytype = SqlDbType.VarChar, Value = param.isDelete });
                con.ExecuteNonQuery("sp_CS_updatedeleteEarningsCode");
                return "Success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }

        [HttpPost]
        [Route("CS_updatedeleteDeductionsCode")]
        public string UpDeleteDeductionsCode(UpdeleteDeductionsCode param)
        {
            try
            {
                TempConnectionStringv4(param.db);
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@tenantId", mytype = SqlDbType.VarChar, Value = param.TenantID });
                con.myparameters.Add(new myParameters { ParameterName = "@logUser", mytype = SqlDbType.VarChar, Value = param.LogUser });
                con.myparameters.Add(new myParameters { ParameterName = "@DeductionsTypeID", mytype = SqlDbType.VarChar, Value = param.DeductionsTypeID });
                con.myparameters.Add(new myParameters { ParameterName = "@DeductionsCode", mytype = SqlDbType.VarChar, Value = param.DeductionsCode });
                con.myparameters.Add(new myParameters { ParameterName = "@DeductionsDesc", mytype = SqlDbType.VarChar, Value = param.DeductionsDesc });
                con.myparameters.Add(new myParameters { ParameterName = "@DeductionsCategory", mytype = SqlDbType.VarChar, Value = param.DeductionsCategory });
                con.myparameters.Add(new myParameters { ParameterName = "@IsBonus", mytype = SqlDbType.VarChar, Value = param.IsBonus });
                con.myparameters.Add(new myParameters { ParameterName = "isTaxable", mytype = SqlDbType.VarChar, Value = param.isTaxable });
                con.myparameters.Add(new myParameters { ParameterName = "isDelete", mytype = SqlDbType.VarChar, Value = param.isDelete });
                con.ExecuteNonQuery("sp_CS_updatedeleteDeductionsCode");
                return "Success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }


        [HttpPost]
        [Route("CS_addDeductionsCode")]
        public string addDeductionsCode(UpdeleteDeductionsCode param)
        {
            try
            {
                TempConnectionStringv4(param.db);
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@tenantId", mytype = SqlDbType.VarChar, Value = param.TenantID });
                con.myparameters.Add(new myParameters { ParameterName = "@logUser", mytype = SqlDbType.VarChar, Value = param.LogUser });
                con.myparameters.Add(new myParameters { ParameterName = "@DeductionsTypeID", mytype = SqlDbType.VarChar, Value = param.DeductionsTypeID });
                con.myparameters.Add(new myParameters { ParameterName = "@DeductionsCode", mytype = SqlDbType.VarChar, Value = param.DeductionsCode });
                con.myparameters.Add(new myParameters { ParameterName = "@DeductionsDesc", mytype = SqlDbType.VarChar, Value = param.DeductionsDesc });
                con.myparameters.Add(new myParameters { ParameterName = "@DeductionsCategory", mytype = SqlDbType.VarChar, Value = param.DeductionsCategory });
                con.myparameters.Add(new myParameters { ParameterName = "@IsBonus", mytype = SqlDbType.VarChar, Value = param.IsBonus });
                con.myparameters.Add(new myParameters { ParameterName = "isTaxable", mytype = SqlDbType.VarChar, Value = param.isTaxable });
                con.myparameters.Add(new myParameters { ParameterName = "isDelete", mytype = SqlDbType.VarChar, Value = param.isDelete });
                con.ExecuteNonQuery("sp_CS_addDeductionsCode");
                return "Success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }


        [HttpPost]
        [Route("CS_addEarningsCode")]
        public string addEarningsCode(UpdeleteEarningsCode param)
        {
            try
            {
                TempConnectionStringv4(param.db);
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@tenantId", mytype = SqlDbType.VarChar, Value = param.TenantID });
                con.myparameters.Add(new myParameters { ParameterName = "@logUser", mytype = SqlDbType.VarChar, Value = param.LogUser });
                con.myparameters.Add(new myParameters { ParameterName = "@EarningsTypeID", mytype = SqlDbType.VarChar, Value = param.EarningsTypeID });
                con.myparameters.Add(new myParameters { ParameterName = "@EarningsCode", mytype = SqlDbType.VarChar, Value = param.EarningsCode });
                con.myparameters.Add(new myParameters { ParameterName = "@EarningsDesc", mytype = SqlDbType.VarChar, Value = param.EarningsDesc });
                con.myparameters.Add(new myParameters { ParameterName = "@EarningsCategory", mytype = SqlDbType.VarChar, Value = param.EarningsCategory });
                con.myparameters.Add(new myParameters { ParameterName = "@IsAbsenceAffected", mytype = SqlDbType.VarChar, Value = param.IsAbsenceAffected });
                con.myparameters.Add(new myParameters { ParameterName = "@isTaxable", mytype = SqlDbType.VarChar, Value = param.isTaxable });
                con.myparameters.Add(new myParameters { ParameterName = "@isDelete", mytype = SqlDbType.VarChar, Value = param.isDelete });
                con.ExecuteNonQuery("sp_CS_addEarningsCode");
                return "Success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }

        [HttpPost]
        [Route("CS_addPaycodeLoans")]
        public string addLoansCode(addPCLoans param)
        {
            try
            {
                TempConnectionStringv4(param.db);
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@tenantId", mytype = SqlDbType.VarChar, Value = param.TenantID });
                con.myparameters.Add(new myParameters { ParameterName = "@logUser", mytype = SqlDbType.VarChar, Value = param.LogUser });
                con.myparameters.Add(new myParameters { ParameterName = "@LoanCode", mytype = SqlDbType.VarChar, Value = param.LoanCode });
                con.myparameters.Add(new myParameters { ParameterName = "@LoanName", mytype = SqlDbType.VarChar, Value = param.LoanName });
                con.myparameters.Add(new myParameters { ParameterName = "@LoanType", mytype = SqlDbType.VarChar, Value = param.LoanType });

                con.ExecuteNonQuery("sp_CS_addPaycodeLoans");
                return "Success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }

        [HttpPost]
        [Route("CS_updateCombinedReports")]
        public string updateCombinedReports(CombinedSettings param)
        {
            try
            {
                List<clientSettingsCombinedList> List = new List<clientSettingsCombinedList>();

                DataTable DTParam = new DataTable();
                DTParam.Columns.AddRange(new DataColumn[1] { new DataColumn("combinedTenantID") });
                for (int i = 0; i < param.combinedTenantID.Length; i++)
                {
                    DTParam.Rows.Add(param.combinedTenantID[i]);
                }

                TempConnectionStringv4(param.db);
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@tenantId", mytype = SqlDbType.VarChar, Value = param.TenantID });
                con.myparameters.Add(new myParameters { ParameterName = "@Data", mytype = SqlDbType.Structured, Value = DTParam });


                con.ExecuteNonQuery("sp_CS_updateCombinedReports");
                return "Success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }

        [HttpPost]
        [Route("CS_getCombinedReports")]
        public List<getCombinedReports> getCombinedREports(combinedReports param)
        {
            List<getCombinedReports> List = new List<getCombinedReports>();


            TempConnectionStringv4(param.db);
            Connection con = new Connection();

            con.myparameters.Add(new myParameters { ParameterName = "@tenantId", mytype = SqlDbType.VarChar, Value = param.TenantId });
            DataTable DT = con.GetDataTable("sp_CS_getCombinedReports");
            foreach (DataRow row in DT.Rows)
            {
                getCombinedReports P = new getCombinedReports()
                {
                    includeCompany = row["includeCompany"].ToString()
                };
                List.Add(P);
            }
            return List;
        }

        [HttpPost]
        [Route("CS_updateIntegrateWith")]
        public string updateIntergration(CombinedSettings param)
        {
            try
            {
                List<clientSettingsCombinedList> List = new List<clientSettingsCombinedList>();

                DataTable DTParam = new DataTable();
                DTParam.Columns.AddRange(new DataColumn[1] { new DataColumn("combinedTenantID") });
                for (int i = 0; i < param.combinedTenantID.Length; i++)
                {
                    DTParam.Rows.Add(param.combinedTenantID[i]);
                }

                TempConnectionStringv4(param.db);
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@tenantId", mytype = SqlDbType.VarChar, Value = param.TenantID });
                con.myparameters.Add(new myParameters { ParameterName = "@Data", mytype = SqlDbType.Structured, Value = DTParam });


                con.ExecuteNonQuery("sp_CS_updateIntegrateWith");
                return "Success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }
        //ADDED FOR CLIENT SETTINGS END

        [HttpPost]
        [Route("PO_EmployeeDetails")]
        public EmployeeDetails EmployeeDetails(EmployeeDetailsParam param)
        {
            EmployeeDetails EmpDetails = new EmployeeDetails();
            try
            {
                TempConnectionStringv2();
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@TenantID", mytype = SqlDbType.VarChar, Value = param.TenantID });
                con.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.VarChar, Value = param.EmpID });
                DataTable DT = con.GetDataTable("sp_EmployeeDetails");
                foreach (DataRow row in DT.Rows)
                {
                    EmpDetails.FirstName = row["FirstName"].ToString();
                    EmpDetails.MiddleName = row["MiddleName"].ToString();
                    EmpDetails.LastName = row["LastName"].ToString();
                    EmpDetails.myreturn = "Success";
                }
            }
            catch (Exception e)
            {
                EmpDetails.myreturn = e.ToString();
            }
            return EmpDetails;
        }
        [HttpPost]
        [Route("PO_Login")]
        public ReturnLogin RetLogin(PO_Login param)
        {
            ReturnLogin RL = new ReturnLogin();
            try
            {
                TempConnectionStringv4(param.db);
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@TenantCode", mytype = SqlDbType.VarChar, Value = param.TenantCode });
                con.myparameters.Add(new myParameters { ParameterName = "@Username", mytype = SqlDbType.VarChar, Value = param.UserName });
                con.myparameters.Add(new myParameters { ParameterName = "@Password", mytype = SqlDbType.VarChar, Value = param.Password });
                DataTable DT = con.GetDataTable("sp_Login");
                foreach (DataRow row in DT.Rows)
                {
                    RL.Status = row["Status"].ToString();
                    RL.Token = row["Token"].ToString();
                    RL.TenantID = row["TenantID"].ToString();
                }
            }
            catch (Exception e)
            {
                RL.Status = e.ToString();
            }
            return RL;
        }
        [HttpPost]
        [Route("PO_ChangePassword")]
        public ReturnChangePass PO_ChangePass(PO_ChangePass param)
        {
            ReturnChangePass RL = new ReturnChangePass();
            try
            {
                TempConnectionStringv2();
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@TenantCode", mytype = SqlDbType.VarChar, Value = param.TenantCode });
                con.myparameters.Add(new myParameters { ParameterName = "@Username", mytype = SqlDbType.VarChar, Value = param.UserName });
                con.myparameters.Add(new myParameters { ParameterName = "@Password", mytype = SqlDbType.VarChar, Value = param.Password });
                con.myparameters.Add(new myParameters { ParameterName = "@NewPassword", mytype = SqlDbType.VarChar, Value = param.NewPassword });
                DataTable DT = con.GetDataTable("sp_ChangePassword");
                foreach (DataRow row in DT.Rows)
                {
                    RL.Status = row["ChangePassStatus"].ToString();
                }
            }
            catch (Exception e)
            {
                RL.Status = e.ToString();
            }
            return RL;
        }
        [HttpPost]
        [Route("PO_GetEmployees")]
        public List<Employees> GetEmployees(EmployeesParam param)
        {
            List<Employees> List = new List<Employees>();
            TempConnectionStringv4(param.db);
            Connection con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@tenantID", mytype = SqlDbType.VarChar, Value = param.TenantID });
            con.myparameters.Add(new myParameters { ParameterName = "@option", mytype = SqlDbType.VarChar, Value = param.Option });
            DataTable DT = con.GetDataTable("sp_GetEmployee");
            foreach (DataRow row in DT.Rows)
            {
                Employees E = new Employees()
                {
                    EmpID = row["EmpID"].ToString(),
                    EmpName = row["EmpName"].ToString()
                };
                List.Add(E);
            }
            return List;
        }


        [HttpPost]
        [Route("ws_getFilenames")]
        public List<FileList> getFilenames(EmployeesParam param)
        {
            List<FileList> List = new List<FileList>();
            TempConnectionStringv4(param.db);
            Connection con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@tenantId", mytype = SqlDbType.VarChar, Value = param.TenantID });
            con.myparameters.Add(new myParameters { ParameterName = "@option", mytype = SqlDbType.VarChar, Value = param.Option });
            DataTable DT = con.GetDataTable("sp_getFilenames");
            foreach (DataRow row in DT.Rows)
            {
                FileList E = new FileList()
                {
                    id = row["id"].ToString(),
                    date = row["date"].ToString(),
                    file = row["file"].ToString()
                };
                List.Add(E);
            }
            return List;
        }

        [HttpPost]
        [Route("PO_ExtractPayRegv3")]
        public string ExtractReportv3(efile param)
        {
            FileStream stream = null;
            try
            {
                DataTable DTParam = new DataTable();
                //DTParam.Columns.AddRange(new DataColumn[1] { new DataColumn("startingids") });
                //for (int i = 0; i < Params.StartingIDS.Length; i++)
                //{
                //    DTParam.Rows.Add(Params.StartingIDS[i]);
                //}

                TempConnectionStringv3();
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@TenantID", mytype = SqlDbType.NVarChar, Value = param.TenantID });
                con.myparameters.Add(new myParameters { ParameterName = "@CutoffStart", mytype = SqlDbType.NVarChar, Value = param.CutoffStart });
                con.myparameters.Add(new myParameters { ParameterName = "@CutoffEnd", mytype = SqlDbType.NVarChar, Value = param.CutoffEnd });
                con.myparameters.Add(new myParameters { ParameterName = "@EmpStatus", mytype = SqlDbType.VarChar, Value = param.EmpStatus });
                DataTable PayRegData = con.GetDataTable("sp_Extract_PayRegv1");


                string dataDir = HostingEnvironment.MapPath("~/Files/PayrollOutSourcing/");

                stream = new FileStream(dataDir + "ExtractReport.xls", FileMode.Open);
                string filename;
                string datebuild = DateTime.Now.ToString("MMddyyyy");
                string timebuild = DateTime.Now.ToString("hhmmtt");



                filename = datebuild + timebuild + "_ExtractReport" + ".xls";
                using (var fileStream = new FileStream(dataDir + filename, FileMode.Create, FileAccess.Write))
                //using (var fileStream = new FileStream(dataDir + filename, FileMode.Create, FileAccess.Write))
                {
                    stream.CopyTo(fileStream);
                }
                stream.Close();
                ExcelWorkbookFormat workbookFormat = ExcelWorkbookFormat.Xls_2003;
                string testDocFile = dataDir + "ExtractReport.xls";
                ExcelWorkbook workbook = new ExcelWorkbook(testDocFile);
                ExcelWorksheet firstWorksheet = workbook.Worksheets[0];

                #region PayReg Sheet

                ExcelWorksheet PayReg = workbook.Worksheets[0];
                ExcelWorksheet sheet2 = workbook.Worksheets[1];
                ExcelWorksheet sheet3 = workbook.Worksheets[2];
                ExcelWorksheet sheet4 = workbook.Worksheets[3];
                ExcelWorksheet sheet5 = workbook.Worksheets[4];
                ExcelWorksheet sheet6 = workbook.Worksheets[5];
                ExcelWorksheet sheet7 = workbook.Worksheets[6];
                //Sheet7.WorksheetSecurity.ProtectWorksheet("sbc01");
                //AX TAXTABLE

                int x = 2;
                for (int i = 0; i < PayRegData.Rows.Count; i++, x++)
                {

                    PayReg["A" + x].Text = PayRegData.Rows[i]["PayrollNum"].ToString(); //payroll#
                    PayReg["B" + x].Text = PayRegData.Rows[i]["PayDate"].ToString(); //paydate
                    PayReg["C" + x].Text = PayRegData.Rows[i]["PayCode"].ToString(); //pay code
                    PayReg["D" + x].Text = PayRegData.Rows[i]["EmpID"].ToString(); //employee id
                    PayReg["E" + x].Text = PayRegData.Rows[i]["LastName"].ToString(); //last name
                    PayReg["F" + x].Text = PayRegData.Rows[i]["FirstName"].ToString(); //first name
                    PayReg["G" + x].Text = PayRegData.Rows[i]["FileStatus"].ToString(); //file status
                    PayReg["H" + x].Text = PayRegData.Rows[i]["TaxStatus"].ToString(); //tax status
                    PayReg["I" + x].Value = PayRegData.Rows[i]["DailyRate"]; //daily rate
                    PayReg["J" + x].Value = PayRegData.Rows[i]["SalaryRateType"]; //salary rate type
                    PayReg["K" + x].Value = PayRegData.Rows[i]["BasicSalary"].ToString(); //basic salary
                    PayReg["L" + x].Value = PayRegData.Rows[i]["MiscAmount"].ToString(); //misc amount
                    PayReg["M" + x].Value = PayRegData.Rows[i]["LeaveAmount"].ToString();
                    PayReg["N" + x].Value = PayRegData.Rows[i]["OvertimeReg"]; //overtime
                    PayReg["O" + x].Value = PayRegData.Rows[i]["OvertimeHol"];
                    PayReg["P" + x].Value = PayRegData.Rows[i]["OtherTxIncome"]; //other tx income
                    PayReg["Q" + x].Text = PayRegData.Rows[i]["Adjustments"].ToString(); //adjustments
                    PayReg["R" + x].Value = PayRegData.Rows[i]["GrossIncome"]; //gross income
                    PayReg["S" + x].Value = PayRegData.Rows[i]["WithholdingTax"]; //withholding tax
                    PayReg["T" + x].Value = PayRegData.Rows[i]["NetSalaryAfterTax"]; //net salary after tax
                    PayReg["U" + x].Value = PayRegData.Rows[i]["EmployeeSSS"]; //employee sss
                    PayReg["V" + x].Value = PayRegData.Rows[i]["EmployeePhilHealth"]; //employee mcr(philhealth)
                    PayReg["W" + x].Value = PayRegData.Rows[i]["EmployeePagibig"]; //employee pagibig
                    PayReg["X" + x].Value = PayRegData.Rows[i]["OtherNtxIncome"]; //other ntx income
                    PayReg["Y" + x].Value = PayRegData.Rows[i]["LoanPayments"]; //loan payments
                    PayReg["Z" + x].Value = PayRegData.Rows[i]["Deductions"]; //deductions
                    PayReg["AA" + x].Value = PayRegData.Rows[i]["NetSalary"]; //net salary
                    PayReg["AB" + x].Value = PayRegData.Rows[i]["EmployerSSS"]; //employer sss
                    PayReg["AC" + x].Value = PayRegData.Rows[i]["EmployerPhilhealth"]; //employer mcr(philhealth)
                    PayReg["AD" + x].Value = PayRegData.Rows[i]["EmployerEC"]; //employer ec
                    PayReg["AE" + x].Value = PayRegData.Rows[i]["EmployerPagibig"]; //employer pagibig
                    PayReg["AF" + x].Value = PayRegData.Rows[i]["PayrollCost"]; //payroll cost
                    PayReg["AG" + x].Value = PayRegData.Rows[i]["PreviousYTDGross"]; //payment type
                    PayReg["AH" + x].Value = PayRegData.Rows[i]["PreviousYTDWitholding"]; //bank acct#
                    PayReg["AI" + x].Value = PayRegData.Rows[i]["PreviousYTDSSS"]; //bank name
                    PayReg["AJ" + x].Value = PayRegData.Rows[i]["PreviousYTDMCR"]; //date employed
                    PayReg["AK" + x].Value = PayRegData.Rows[i]["PreviousYTDPagibig"]; //date terminated
                    PayReg["AL" + x].Value = PayRegData.Rows[i]["Previous13thNT"]; //cost center
                    PayReg["AM" + x].Value = PayRegData.Rows[i]["Previous13thTX"];
                    PayReg["AN" + x].Text = PayRegData.Rows[i]["PaymentType"].ToString(); //currency
                    PayReg["AO" + x].Text = PayRegData.Rows[i]["BankAcc"].ToString(); //exchange rate
                    PayReg["AP" + x].Text = PayRegData.Rows[i]["BankName"].ToString(); //payment freq
                    PayReg["AQ" + x].Text = PayRegData.Rows[i]["CommentField"].ToString();
                    PayReg["AR" + x].Text = PayRegData.Rows[i]["ErrorField"].ToString();
                    PayReg["AS" + x].Text = PayRegData.Rows[i]["DateEmployed"].ToString();
                    PayReg["AT" + x].Text = PayRegData.Rows[i]["DateTerminated"].ToString();
                    PayReg["AU" + x].Value = PayRegData.Rows[i]["CostCenter"];
                    PayReg["AV" + x].Text = PayRegData.Rows[i]["Currency"].ToString();
                    PayReg["AW" + x].Text = PayRegData.Rows[i]["ExchangeRate"].ToString();
                    PayReg["AX" + x].Text = PayRegData.Rows[i]["PaymentPreq"].ToString();
                    PayReg["AY" + x].Value = PayRegData.Rows[i]["MTDGross"];
                    PayReg["AZ" + x].Value = PayRegData.Rows[i]["MTDBasic"];
                    PayReg["BA" + x].Value = PayRegData.Rows[i]["MTDSSSEmployee"];
                    PayReg["BB" + x].Value = PayRegData.Rows[i]["MTDMCREmployee"];
                    PayReg["BC" + x].Value = PayRegData.Rows[i]["MTDPagIbigEmployee"];
                    PayReg["BD" + x].Value = PayRegData.Rows[i]["MTDSSSEmployer"];
                    PayReg["BE" + x].Value = PayRegData.Rows[i]["MTDMCREmployer"];
                    PayReg["BF" + x].Value = PayRegData.Rows[i]["MTDECEmployer"];
                    PayReg["BG" + x].Value = PayRegData.Rows[i]["MTDPabIbigEmployer"];
                    PayReg["BH" + x].Value = PayRegData.Rows[i]["MTDWHTAX"];
                    PayReg["BI" + x].Value = PayRegData.Rows[i]["MonthlyBasic"];
                    PayReg["BJ" + x].Value = PayRegData.Rows[i]["MonthlyAllow"];
                    PayReg["BK" + x].Value = PayRegData.Rows[i]["MTDNTX"];
                    PayReg["BL" + x].Text = PayRegData.Rows[i]["FROM"].ToString();
                    PayReg["BM" + x].Text = PayRegData.Rows[i]["TO"].ToString();




                }
                #endregion
                TempConnectionStringv3();
                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@TenantID", mytype = SqlDbType.NVarChar, Value = param.TenantID });
                con.myparameters.Add(new myParameters { ParameterName = "@CutoffStart", mytype = SqlDbType.NVarChar, Value = param.CutoffStart });
                con.myparameters.Add(new myParameters { ParameterName = "@CutoffEnd", mytype = SqlDbType.NVarChar, Value = param.CutoffEnd });
                con.myparameters.Add(new myParameters { ParameterName = "@EmpStatus", mytype = SqlDbType.VarChar, Value = param.EmpStatus });
                DataTable secondSheet = con.GetDataTable("sp_ExportSheet2v1");

                x = 2;
                for (int i = 0; i < secondSheet.Rows.Count; i++, x++)
                {
                    sheet2["A" + x].Text = secondSheet.Rows[i]["PayrollNum"].ToString();
                    sheet2["B" + x].Text = secondSheet.Rows[i]["PayOutDate"].ToString();
                    sheet2["C" + x].Text = secondSheet.Rows[i]["PayCode"].ToString();
                    sheet2["D" + x].Text = secondSheet.Rows[i]["EmployeeID"].ToString();
                    sheet2["E" + x].Text = secondSheet.Rows[i]["LastName"].ToString();
                    sheet2["F" + x].Text = secondSheet.Rows[i]["FirstName"].ToString();
                    sheet2["G" + x].Text = secondSheet.Rows[i]["FileStatus"].ToString();
                    sheet2["H" + x].Text = secondSheet.Rows[i]["TransactionType"].ToString();
                    sheet2["I" + x].Text = secondSheet.Rows[i]["Date"].ToString();
                    sheet2["J" + x].Value = secondSheet.Rows[i]["CostCenter"];
                    sheet2["K" + x].Value = secondSheet.Rows[i]["Days"];
                    sheet2["L" + x].Value = secondSheet.Rows[i]["Hours"];
                    sheet2["M" + x].Value = secondSheet.Rows[i]["Amount"];
                    sheet2["N" + x].Text = secondSheet.Rows[i]["Remark"].ToString();
                    sheet2["O" + x].Text = secondSheet.Rows[i]["FROM"].ToString();
                    sheet2["P" + x].Text = secondSheet.Rows[i]["TO"].ToString();

                }

                TempConnectionStringv3();
                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@TenantID", mytype = SqlDbType.NVarChar, Value = param.TenantID });
                con.myparameters.Add(new myParameters { ParameterName = "@CutoffStart", mytype = SqlDbType.NVarChar, Value = param.CutoffStart });
                con.myparameters.Add(new myParameters { ParameterName = "@CutoffEnd", mytype = SqlDbType.NVarChar, Value = param.CutoffEnd });
                con.myparameters.Add(new myParameters { ParameterName = "@EmpStatus", mytype = SqlDbType.VarChar, Value = param.EmpStatus });
                DataTable thirdSheet = con.GetDataTable("sp_ExportSheet3v1");

                x = 2;
                for (int i = 0; i < thirdSheet.Rows.Count; i++, x++)
                {
                    sheet3["A" + x].Text = thirdSheet.Rows[i]["PayrollNum"].ToString();
                    sheet3["B" + x].Text = thirdSheet.Rows[i]["PayDate"].ToString();
                    sheet3["C" + x].Text = thirdSheet.Rows[i]["PayCode"].ToString();
                    sheet3["D" + x].Text = thirdSheet.Rows[i]["EmpID"].ToString();
                    sheet3["E" + x].Text = thirdSheet.Rows[i]["LastName"].ToString();
                    sheet3["F" + x].Text = thirdSheet.Rows[i]["FirstName"].ToString();
                    sheet3["G" + x].Text = thirdSheet.Rows[i]["FileStatus"].ToString();
                    sheet3["H" + x].Text = thirdSheet.Rows[i]["OTCode"].ToString();
                    sheet3["I" + x].Text = thirdSheet.Rows[i]["Date"].ToString();
                    sheet3["J" + x].Value = thirdSheet.Rows[i]["CostCenter"];
                    sheet3["K" + x].Value = thirdSheet.Rows[i]["OTRate"];
                    sheet3["L" + x].Value = thirdSheet.Rows[i]["OTHours"];
                    sheet3["M" + x].Value = thirdSheet.Rows[i]["OTMinutes"];
                    sheet3["N" + x].Value = thirdSheet.Rows[i]["OTAmount"];
                    sheet3["O" + x].Text = thirdSheet.Rows[i]["Remark"].ToString();
                    sheet3["P" + x].Text = thirdSheet.Rows[i]["From"].ToString();
                    sheet3["Q" + x].Text = thirdSheet.Rows[i]["To"].ToString();





                }
                TempConnectionStringv3();
                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@TenantID", mytype = SqlDbType.NVarChar, Value = param.TenantID });
                con.myparameters.Add(new myParameters { ParameterName = "@CutoffStart", mytype = SqlDbType.NVarChar, Value = param.CutoffStart });
                con.myparameters.Add(new myParameters { ParameterName = "@CutoffEnd", mytype = SqlDbType.NVarChar, Value = param.CutoffEnd });
                con.myparameters.Add(new myParameters { ParameterName = "@EmpStatus", mytype = SqlDbType.VarChar, Value = param.EmpStatus });
                DataTable FSheet = con.GetDataTable("sp_ExportSheet4v1");

                x = 2;
                for (int i = 0; i < FSheet.Rows.Count; i++, x++)
                {
                    sheet4["A" + x].Text = FSheet.Rows[i]["PayrollNum"].ToString();
                    sheet4["B" + x].Text = FSheet.Rows[i]["PayDate"].ToString();
                    sheet4["C" + x].Text = FSheet.Rows[i]["PayCode"].ToString();
                    sheet4["D" + x].Text = FSheet.Rows[i]["EmployeeID"].ToString();
                    sheet4["E" + x].Text = FSheet.Rows[i]["LastName"].ToString();
                    sheet4["F" + x].Text = FSheet.Rows[i]["FirstName"].ToString();
                    sheet4["G" + x].Text = FSheet.Rows[i]["FileStatus"].ToString();
                    sheet4["H" + x].Text = FSheet.Rows[i]["IncomeCode"].ToString();
                    sheet4["I" + x].Text = FSheet.Rows[i]["Date"].ToString();
                    sheet4["J" + x].Value = FSheet.Rows[i]["Amount"];
                    sheet4["K" + x].Text = FSheet.Rows[i]["RecurStart"].ToString();
                    sheet4["L" + x].Text = FSheet.Rows[i]["RecurEnd"].ToString();
                    sheet4["M" + x].Text = FSheet.Rows[i]["Freq"].ToString();
                    sheet4["N" + x].Text = FSheet.Rows[i]["Remarks"].ToString();
                    sheet4["O" + x].Text = FSheet.Rows[i]["From"].ToString();
                    sheet4["P" + x].Text = FSheet.Rows[i]["To"].ToString();
                    sheet4["Q" + x].Text = FSheet.Rows[i]["JECostCenter"].ToString();

                }

                TempConnectionStringv3();
                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@TenantID", mytype = SqlDbType.NVarChar, Value = param.TenantID });
                con.myparameters.Add(new myParameters { ParameterName = "@CutoffStart", mytype = SqlDbType.NVarChar, Value = param.CutoffStart });
                con.myparameters.Add(new myParameters { ParameterName = "@CutoffEnd", mytype = SqlDbType.NVarChar, Value = param.CutoffEnd });
                con.myparameters.Add(new myParameters { ParameterName = "@EmpStatus", mytype = SqlDbType.VarChar, Value = param.EmpStatus });
                DataTable FifthSheet = con.GetDataTable("sp_ExportSheet5v1");

                x = 2;
                for (int i = 0; i < FifthSheet.Rows.Count; i++, x++)
                {
                    sheet5["A" + x].Text = FifthSheet.Rows[i]["PayrollNum"].ToString();
                    sheet5["B" + x].Text = FifthSheet.Rows[i]["PayDate"].ToString();
                    sheet5["C" + x].Text = FifthSheet.Rows[i]["PayCode"].ToString();
                    sheet5["D" + x].Text = FifthSheet.Rows[i]["EmployeeID"].ToString();
                    sheet5["E" + x].Text = FifthSheet.Rows[i]["LastName"].ToString();
                    sheet5["F" + x].Text = FifthSheet.Rows[i]["FirstName"].ToString();
                    sheet5["G" + x].Text = FifthSheet.Rows[i]["FileStatus"].ToString();
                    sheet5["H" + x].Text = FifthSheet.Rows[i]["IncomeCode"].ToString();
                    sheet5["I" + x].Text = FifthSheet.Rows[i]["Date"].ToString();
                    sheet5["J" + x].Value = FifthSheet.Rows[i]["Amount"];
                    sheet5["K" + x].Text = FifthSheet.Rows[i]["RecurStart"].ToString();
                    sheet5["L" + x].Text = FifthSheet.Rows[i]["RecurEnd"].ToString();
                    sheet5["M" + x].Text = FifthSheet.Rows[i]["Freq"].ToString();
                    sheet5["N" + x].Text = FifthSheet.Rows[i]["Remarks"].ToString();
                    sheet5["O" + x].Text = FifthSheet.Rows[i]["From"].ToString();
                    sheet5["P" + x].Text = FifthSheet.Rows[i]["To"].ToString();
                    sheet5["Q" + x].Text = FifthSheet.Rows[i]["JECostCenter"].ToString();



                }
                TempConnectionStringv3();
                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@TenantID", mytype = SqlDbType.NVarChar, Value = param.TenantID });
                con.myparameters.Add(new myParameters { ParameterName = "@CutoffStart", mytype = SqlDbType.NVarChar, Value = param.CutoffStart });
                con.myparameters.Add(new myParameters { ParameterName = "@CutoffEnd", mytype = SqlDbType.NVarChar, Value = param.CutoffEnd });
                con.myparameters.Add(new myParameters { ParameterName = "@EmpStatus", mytype = SqlDbType.VarChar, Value = param.EmpStatus });
                DataTable SixSheet = con.GetDataTable("sp_ExportSheet6v1");

                x = 2;
                for (int i = 0; i < SixSheet.Rows.Count; i++, x++)
                {
                    sheet6["A" + x].Text = SixSheet.Rows[i]["PayrollNum"].ToString();
                    sheet6["B" + x].Text = SixSheet.Rows[i]["PayDate"].ToString();
                    sheet6["C" + x].Text = SixSheet.Rows[i]["PayCode"].ToString();
                    sheet6["D" + x].Text = SixSheet.Rows[i]["EmployeeID"].ToString();
                    sheet6["E" + x].Text = SixSheet.Rows[i]["LastName"].ToString();
                    sheet6["F" + x].Text = SixSheet.Rows[i]["FirstName"].ToString();
                    sheet6["G" + x].Text = SixSheet.Rows[i]["FileStatus"].ToString();
                    sheet6["H" + x].Text = SixSheet.Rows[i]["LoanCode"].ToString();
                    sheet6["I" + x].Text = SixSheet.Rows[i]["LoanDateStart"].ToString();
                    sheet6["J" + x].Value = SixSheet.Rows[i]["LoanPrincipal"];
                    sheet6["K" + x].Value = SixSheet.Rows[i]["LoanAmount"];
                    sheet6["L" + x].Text = SixSheet.Rows[i]["LoanPayments"].ToString();
                    sheet6["M" + x].Value = SixSheet.Rows[i]["LoanBalance"];
                    sheet6["N" + x].Text = SixSheet.Rows[i]["LoanDeductionAMT"].ToString();
                    sheet6["O" + x].Text = SixSheet.Rows[i]["LoanDeductionFreq"].ToString();
                    sheet6["P" + x].Text = SixSheet.Rows[i]["LoanStatus"].ToString();
                    sheet6["Q" + x].Text = SixSheet.Rows[i]["LoanDateGranted"].ToString();
                    sheet6["R" + x].Text = SixSheet.Rows[i]["From"].ToString();
                    sheet6["S" + x].Text = SixSheet.Rows[i]["To"].ToString();
                    sheet6["T" + x].Text = SixSheet.Rows[i]["JECostCenter"].ToString();


                }

                TempConnectionStringv3();
                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@TenantID", mytype = SqlDbType.NVarChar, Value = param.TenantID });
                con.myparameters.Add(new myParameters { ParameterName = "@CutoffStart", mytype = SqlDbType.NVarChar, Value = param.CutoffStart });
                con.myparameters.Add(new myParameters { ParameterName = "@CutoffEnd", mytype = SqlDbType.NVarChar, Value = param.CutoffEnd });
                con.myparameters.Add(new myParameters { ParameterName = "@EmpStatus", mytype = SqlDbType.VarChar, Value = param.EmpStatus });
                DataTable SevenSheet = con.GetDataTable("sp_Exportsheet7v1");

                x = 2;
                for (int i = 0; i < SevenSheet.Rows.Count; i++, x++)
                {
                    sheet7["A" + x].Text = SevenSheet.Rows[i]["PayrollNum"].ToString();
                    sheet7["B" + x].Text = SevenSheet.Rows[i]["PayDate"].ToString();
                    sheet7["C" + x].Text = SevenSheet.Rows[i]["PayCode"].ToString();
                    sheet7["D" + x].Text = SevenSheet.Rows[i]["EmployeeID"].ToString();
                    sheet7["E" + x].Text = SevenSheet.Rows[i]["LastName"].ToString();
                    sheet7["F" + x].Text = SevenSheet.Rows[i]["FirstName"].ToString();
                    sheet7["G" + x].Text = SevenSheet.Rows[i]["FileStatus"].ToString();
                    sheet7["H" + x].Text = SevenSheet.Rows[i]["DeductionCode"].ToString();
                    sheet7["I" + x].Text = SevenSheet.Rows[i]["Date"].ToString();
                    sheet7["J" + x].Value = SevenSheet.Rows[i]["DeductionAmount"];
                    sheet7["K" + x].Text = SevenSheet.Rows[i]["DeductionFreq"].ToString();
                    sheet7["L" + x].Text = SevenSheet.Rows[i]["LastDeductionDate"].ToString();
                    sheet7["M" + x].Text = SevenSheet.Rows[i]["From"].ToString();
                    sheet7["N" + x].Text = SevenSheet.Rows[i]["To"].ToString();
                    sheet7["O" + x].Text = SevenSheet.Rows[i]["JECostCenter"].ToString();



                }
                HttpResponse httpResponse = System.Web.HttpContext.Current.Response;

                httpResponse.Clear();
                httpResponse.ContentType = "Application/x-msexcel";
                httpResponse.AddHeader("Content-Disposition", String.Format("attachment; filename={0}", filename));
                string newDIR = HttpContext.Current.Server.MapPath("~/sFTP/PayrollOutsourcing/");
                string onlineDIR = "http://ws.durusthr.com/ILM_WS_Live/sFTP/PayrollOutsourcing/";
                //filename = filename.Replace(',', '_');
                //filename = filename.Replace(" ", String.Empty);
                workbook.Save(newDIR + filename);
                stream.Close();
                //return "Success";

                if (PayRegData.Rows.Count > 0)
                {
                    return onlineDIR + filename;
                }
                else
                {

                    return "No Rows";
                }
            }
            catch (Exception e)
            {
                stream.Close();
                return "Error: " + e.ToString();
            }
        }

        //public List<RateTypesList> getRateTypes(PayoutDatesParam param)
        //{
        //    List<RateTypesList> List = new List<RateTypesList>();
        //}
            [HttpPost]
        [Route("WS_GetProfileLoanName")]
        public List<ReturnLoanCode> GetProfileLoanName(PO_ChangePass param)
        {
           List<ReturnLoanCode> RL = new List<ReturnLoanCode>();
            try
            {
                TempConnectionStringv4(param.db);
                Connection con = new Connection();

                con.myparameters.Add(new myParameters { ParameterName = "@code", mytype = SqlDbType.VarChar, Value = param.tenantcode });
                con.myparameters.Add(new myParameters { ParameterName = "@empid", mytype = SqlDbType.VarChar, Value = param.empid });
                DataTable DT = con.GetDataTable("sp_getLoanProfile");
                foreach (DataRow row in DT.Rows)
                {
                    ReturnLoanCode r = new ReturnLoanCode()
                    {
                    loanCode = row["LoanNameCode"].ToString(),
                    loanName = row["LoanName"].ToString()
                     };
                    RL.Add(r);
                }
              
            }
            catch (Exception e)
            {
                
            }
            return RL;
        }

        [HttpPost]
        [Route("WS_GetPayOutdate")]
        public List<ReturnPayoutdate> getPayOutdate(PO_ChangePass param)
        {
            List<ReturnPayoutdate> RL = new List<ReturnPayoutdate>();
            try
            {
                TempConnectionStringv4(param.db);
                Connection con = new Connection();

                con.myparameters.Add(new myParameters { ParameterName = "@code", mytype = SqlDbType.VarChar, Value = param.tenantcode });
                con.myparameters.Add(new myParameters { ParameterName = "@empid", mytype = SqlDbType.VarChar, Value = param.empid });
                con.myparameters.Add(new myParameters { ParameterName = "@loanCode", mytype = SqlDbType.VarChar, Value = param.LoanCode });
                DataTable DT = con.GetDataTable("sp_getLoanProfilePayoutDate");
                foreach (DataRow row in DT.Rows)
                {
                    ReturnPayoutdate r = new ReturnPayoutdate() {
                        payoutdate = row["PayOutDate"].ToString()
                };
                 RL.Add(r);
                }
            }
            catch (Exception e)
            {
                //RL.payoutdate = e.ToString();
            }
            return RL;
        }
        [HttpPost]
        [Route("WS_getProfileLoans")]
        public List <ReturnProfileLoans> ProfileLoans(PO_ChangePass param)
        {
            List<ReturnProfileLoans> RL = new List<ReturnProfileLoans>();
            try
            {
                TempConnectionStringv4(param.db);
                Connection con = new Connection();

                con.myparameters.Add(new myParameters { ParameterName = "@code", mytype = SqlDbType.VarChar, Value = param.tenantcode });
                con.myparameters.Add(new myParameters { ParameterName = "@empid", mytype = SqlDbType.VarChar, Value = param.empid });
                con.myparameters.Add(new myParameters { ParameterName = "@loanCode", mytype = SqlDbType.VarChar, Value = param.LoanCode });
                con.myparameters.Add(new myParameters { ParameterName = "@payOutdate", mytype = SqlDbType.VarChar, Value = param.payoutdate });
                DataTable DT = con.GetDataTable("sp_Get_ProfileLoans");
                foreach (DataRow row in DT.Rows)
                {
                    ReturnProfileLoans r = new ReturnProfileLoans()
                    {
                        payoutdate = row["PayOutDate"].ToString(),
                        loanName = row["LoanName"].ToString(),
                        loanDate = row["LoanDate"].ToString(),
                        loanAmount = row["LoanAmount"].ToString(),
                        amortizationAmount = row["AmortizationAmount"].ToString(),
                        loanBalance = row["LoanBalance"].ToString()
                    };
                    RL.Add(r);

                }
            }
            catch (Exception e)
            {
              
            }
            return RL;
        }

        [HttpPost]
        [Route("PO_ExtractPayRegv4")]
        public string ExtractReportv4(efile param)
        {

            FileStream stream = null;
            try
            {
                DataTable DTParam = new DataTable();
                DTParam.Columns.AddRange(new DataColumn[1] { new DataColumn("empid") });
                for (int i = 0; i < param.empid.Length; i++)
                {
                    DTParam.Rows.Add(param.empid[i]);
                }

                TempConnectionStringv4(param.db);
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@TenantID", mytype = SqlDbType.NVarChar, Value = param.TenantID });
                con.myparameters.Add(new myParameters { ParameterName = "@CutoffStart", mytype = SqlDbType.NVarChar, Value = param.CutoffStart });
                con.myparameters.Add(new myParameters { ParameterName = "@CutoffEnd", mytype = SqlDbType.NVarChar, Value = param.CutoffEnd });
                con.myparameters.Add(new myParameters { ParameterName = "@EmpStatus", mytype = SqlDbType.VarChar, Value = param.EmpStatus });
                con.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.Structured, Value = DTParam });
                DataTable PayRegData = con.GetDataTable("sp_Extract_PayRegv4");


                string dataDir = HostingEnvironment.MapPath("~/Files/PayrollOutSourcing/");

                stream = new FileStream(dataDir + "ExtractReport.xls", FileMode.Open);
                string filename;
                // string datebuild = DateTime.Now.ToString("MMddyyyy");
                string cc = "";
                if (PayRegData.Rows.Count != 0)
                {
                    cc = PayRegData.Rows[0]["Ccode"].ToString();
                }

                string cStart = Regex.Replace(param.CutoffStart, @"[^0-9a-zA-Z]+", "");
                string cEnd = Regex.Replace(param.CutoffEnd, @"[^0-9a-zA-Z]+", "");


                string datebuild = DateTime.Now.ToString("yyyyMM");
                string timebuild = DateTime.Now.ToString("hhtt");
                filename = cc + "_PayReg_" + cStart + "_" + cEnd + "_" + timebuild + ".xls";
                using (var fileStream = new FileStream(dataDir + filename, FileMode.Create, FileAccess.Write))
                //using (var fileStream = new FileStream(dataDir + filename, FileMode.Create, FileAccess.Write))
                {
                    stream.CopyTo(fileStream);
                }
                stream.Close();
                ExcelWorkbookFormat workbookFormat = ExcelWorkbookFormat.Xls_2003;
                string testDocFile = dataDir + "ExtractReport.xls";
                ExcelWorkbook workbook = new ExcelWorkbook(testDocFile);
                ExcelWorksheet firstWorksheet = workbook.Worksheets[0];

                #region PayReg Sheet

                ExcelWorksheet PayReg = workbook.Worksheets[0];
                ExcelWorksheet sheet2 = workbook.Worksheets[1];
                ExcelWorksheet sheet3 = workbook.Worksheets[2];
                ExcelWorksheet sheet4 = workbook.Worksheets[3];
                ExcelWorksheet sheet5 = workbook.Worksheets[4];
                ExcelWorksheet sheet6 = workbook.Worksheets[5];
                ExcelWorksheet sheet7 = workbook.Worksheets[6];
                //Sheet7.WorksheetSecurity.ProtectWorksheet("sbc01");
                //AX TAXTABLE

                int x = 2;
                for (int i = 0; i < PayRegData.Rows.Count; i++, x++)
                {

                    PayReg["A" + x].Text = PayRegData.Rows[i]["PayrollNum"].ToString(); //payroll#
                    PayReg["B" + x].Text = PayRegData.Rows[i]["PayDate"].ToString(); //paydate
                    PayReg["C" + x].Text = PayRegData.Rows[i]["PayCode"].ToString(); //pay code
                    PayReg["D" + x].Text = PayRegData.Rows[i]["EmpID"].ToString(); //employee id
                    PayReg["E" + x].Text = PayRegData.Rows[i]["LastName"].ToString(); //last name
                    PayReg["F" + x].Text = PayRegData.Rows[i]["FirstName"].ToString(); //first name
                    PayReg["G" + x].Text = PayRegData.Rows[i]["FileStatus"].ToString(); //file status
                    PayReg["H" + x].Text = PayRegData.Rows[i]["TaxStatus"].ToString(); //tax status
                    PayReg["I" + x].Value = PayRegData.Rows[i]["DailyRate"]; //daily rate
                    PayReg["J" + x].Value = PayRegData.Rows[i]["SalaryRateType"]; //salary rate type
                    PayReg["K" + x].Value = PayRegData.Rows[i]["BasicSalary"]; //basic salary
                    PayReg["L" + x].Value = PayRegData.Rows[i]["MiscAmount"]; //misc amount
                    PayReg["M" + x].Value = PayRegData.Rows[i]["LeaveAmount"];
                    PayReg["N" + x].Value = PayRegData.Rows[i]["OvertimeReg"]; //overtime
                    PayReg["O" + x].Value = PayRegData.Rows[i]["OvertimeHol"];
                    PayReg["P" + x].Value = PayRegData.Rows[i]["OtherTxIncome"]; //other tx income
                    PayReg["Q" + x].Text = PayRegData.Rows[i]["Adjustments"].ToString(); //adjustments
                    PayReg["R" + x].Value = PayRegData.Rows[i]["GrossIncome"]; //gross income
                    PayReg["S" + x].Value = PayRegData.Rows[i]["WithholdingTax"]; //withholding tax
                    PayReg["T" + x].Value = PayRegData.Rows[i]["NetSalaryAfterTax"]; //net salary after tax
                    PayReg["U" + x].Value = PayRegData.Rows[i]["EmployeeSSS"]; //employee sss
                    PayReg["V" + x].Value = PayRegData.Rows[i]["EmployeePhilHealth"]; //employee mcr(philhealth)
                    PayReg["W" + x].Value = PayRegData.Rows[i]["EmployeePagibig"]; //employee pagibig
                    PayReg["X" + x].Value = PayRegData.Rows[i]["OtherNtxIncome"]; //other ntx income
                    PayReg["Y" + x].Value = PayRegData.Rows[i]["LoanPayments"]; //loan payments
                    PayReg["Z" + x].Value = PayRegData.Rows[i]["Deductions"]; //deductions
                    PayReg["AA" + x].Value = PayRegData.Rows[i]["NetSalary"]; //net salary
                    PayReg["AB" + x].Value = PayRegData.Rows[i]["EmployerSSS"]; //employer sss
                    PayReg["AC" + x].Value = PayRegData.Rows[i]["EmployerPhilhealth"]; //employer mcr(philhealth)
                    PayReg["AD" + x].Value = PayRegData.Rows[i]["EmployerEC"]; //employer ec
                    PayReg["AE" + x].Value = PayRegData.Rows[i]["EmployerPagibig"]; //employer pagibig
                    PayReg["AF" + x].Value = PayRegData.Rows[i]["PayrollCost"]; //payroll cost
                    PayReg["AG" + x].Value = PayRegData.Rows[i]["PreviousYTDGross"]; //payment type
                    PayReg["AH" + x].Value = PayRegData.Rows[i]["PreviousYTDWitholding"]; //bank acct#
                    PayReg["AI" + x].Value = PayRegData.Rows[i]["PreviousYTDSSS"]; //bank name
                    PayReg["AJ" + x].Value = PayRegData.Rows[i]["PreviousYTDMCR"]; //date employed
                    PayReg["AK" + x].Value = PayRegData.Rows[i]["PreviousYTDPagibig"]; //date terminated
                    PayReg["AL" + x].Value = PayRegData.Rows[i]["Previous13thNT"]; //cost center
                    PayReg["AM" + x].Value = PayRegData.Rows[i]["Previous13thTX"];
                    PayReg["AN" + x].Text = PayRegData.Rows[i]["PaymentType"].ToString(); //currency
                    PayReg["AO" + x].Text = PayRegData.Rows[i]["BankAcc"].ToString(); //exchange rate
                    PayReg["AP" + x].Text = PayRegData.Rows[i]["BankName"].ToString(); //payment freq
                    PayReg["AQ" + x].Text = PayRegData.Rows[i]["CommentField"].ToString();
                    PayReg["AR" + x].Text = PayRegData.Rows[i]["ErrorField"].ToString();
                    PayReg["AS" + x].Text = PayRegData.Rows[i]["DateEmployed"].ToString();
                    PayReg["AT" + x].Text = PayRegData.Rows[i]["DateTerminated"].ToString();
                    PayReg["AU" + x].Value = PayRegData.Rows[i]["CostCenter"];
                    PayReg["AV" + x].Text = PayRegData.Rows[i]["Currency"].ToString();
                    PayReg["AW" + x].Text = PayRegData.Rows[i]["ExchangeRate"].ToString();
                    PayReg["AX" + x].Text = PayRegData.Rows[i]["PaymentPreq"].ToString();
                    PayReg["AY" + x].Value = PayRegData.Rows[i]["MTDGross"];
                    PayReg["AZ" + x].Value = PayRegData.Rows[i]["MTDBasic"];
                    PayReg["BA" + x].Value = PayRegData.Rows[i]["MTDSSSEmployee"];
                    PayReg["BB" + x].Value = PayRegData.Rows[i]["MTDMCREmployee"];
                    PayReg["BC" + x].Value = PayRegData.Rows[i]["MTDPagIbigEmployee"];
                    PayReg["BD" + x].Value = PayRegData.Rows[i]["MTDSSSEmployer"];
                    PayReg["BE" + x].Value = PayRegData.Rows[i]["MTDMCREmployer"];
                    PayReg["BF" + x].Value = PayRegData.Rows[i]["MTDECEmployer"];
                    PayReg["BG" + x].Value = PayRegData.Rows[i]["MTDPabIbigEmployer"];
                    PayReg["BH" + x].Value = PayRegData.Rows[i]["MTDWHTAX"];
                    PayReg["BI" + x].Value = PayRegData.Rows[i]["MonthlyBasic"];
                    PayReg["BJ" + x].Value = PayRegData.Rows[i]["MonthlyAllow"];
                    PayReg["BK" + x].Value = PayRegData.Rows[i]["MTDNTX"];
                    PayReg["BL" + x].Text = PayRegData.Rows[i]["FROM"].ToString();
                    PayReg["BM" + x].Text = PayRegData.Rows[i]["TO"].ToString();




                }
                #endregion
                TempConnectionStringv4(param.db);
                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@TenantID", mytype = SqlDbType.NVarChar, Value = param.TenantID });
                con.myparameters.Add(new myParameters { ParameterName = "@CutoffStart", mytype = SqlDbType.NVarChar, Value = param.CutoffStart });
                con.myparameters.Add(new myParameters { ParameterName = "@CutoffEnd", mytype = SqlDbType.NVarChar, Value = param.CutoffEnd });
                con.myparameters.Add(new myParameters { ParameterName = "@EmpStatus", mytype = SqlDbType.VarChar, Value = param.EmpStatus });
                con.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.Structured, Value = DTParam });
                DataTable secondSheet = con.GetDataTable("sp_ExportSheet2v4");

                x = 2;
                for (int i = 0; i < secondSheet.Rows.Count; i++, x++)
                {
                    sheet2["A" + x].Text = secondSheet.Rows[i]["PayrollNum"].ToString();
                    sheet2["B" + x].Text = secondSheet.Rows[i]["PayOutDate"].ToString();
                    sheet2["C" + x].Text = secondSheet.Rows[i]["PayCode"].ToString();
                    sheet2["D" + x].Text = secondSheet.Rows[i]["EmployeeID"].ToString();
                    sheet2["E" + x].Text = secondSheet.Rows[i]["LastName"].ToString();
                    sheet2["F" + x].Text = secondSheet.Rows[i]["FirstName"].ToString();
                    sheet2["G" + x].Text = secondSheet.Rows[i]["FileStatus"].ToString();
                    sheet2["H" + x].Text = secondSheet.Rows[i]["TransactionType"].ToString();
                    sheet2["I" + x].Text = secondSheet.Rows[i]["Date"].ToString();
                    sheet2["J" + x].Value = secondSheet.Rows[i]["CostCenter"];
                    sheet2["K" + x].Value = secondSheet.Rows[i]["Days"];
                    sheet2["L" + x].Value = secondSheet.Rows[i]["Hours"];
                    sheet2["M" + x].Value = secondSheet.Rows[i]["Amount"];
                    sheet2["N" + x].Text = secondSheet.Rows[i]["Remark"].ToString();
                    sheet2["O" + x].Text = secondSheet.Rows[i]["FROM"].ToString();
                    sheet2["P" + x].Text = secondSheet.Rows[i]["TO"].ToString();

                }

                TempConnectionStringv4(param.db);
                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@TenantID", mytype = SqlDbType.NVarChar, Value = param.TenantID });
                con.myparameters.Add(new myParameters { ParameterName = "@CutoffStart", mytype = SqlDbType.NVarChar, Value = param.CutoffStart });
                con.myparameters.Add(new myParameters { ParameterName = "@CutoffEnd", mytype = SqlDbType.NVarChar, Value = param.CutoffEnd });
                con.myparameters.Add(new myParameters { ParameterName = "@EmpStatus", mytype = SqlDbType.VarChar, Value = param.EmpStatus });
                con.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.Structured, Value = DTParam });
                DataTable thirdSheet = con.GetDataTable("sp_ExportSheet3v4");

                x = 2;
                for (int i = 0; i < thirdSheet.Rows.Count; i++, x++)
                {
                    sheet3["A" + x].Text = thirdSheet.Rows[i]["PayrollNum"].ToString();
                    sheet3["B" + x].Text = thirdSheet.Rows[i]["PayDate"].ToString();
                    sheet3["C" + x].Text = thirdSheet.Rows[i]["PayCode"].ToString();
                    sheet3["D" + x].Text = thirdSheet.Rows[i]["EmpID"].ToString();
                    sheet3["E" + x].Text = thirdSheet.Rows[i]["LastName"].ToString();
                    sheet3["F" + x].Text = thirdSheet.Rows[i]["FirstName"].ToString();
                    sheet3["G" + x].Text = thirdSheet.Rows[i]["FileStatus"].ToString();
                    sheet3["H" + x].Text = thirdSheet.Rows[i]["OTCode"].ToString();
                    sheet3["I" + x].Text = thirdSheet.Rows[i]["Date"].ToString();
                    sheet3["J" + x].Value = thirdSheet.Rows[i]["CostCenter"];
                    sheet3["K" + x].Value = thirdSheet.Rows[i]["OTRate"];
                    sheet3["L" + x].Value = thirdSheet.Rows[i]["OTHours"];
                    sheet3["M" + x].Value = thirdSheet.Rows[i]["OTMinutes"];
                    sheet3["N" + x].Value = thirdSheet.Rows[i]["OTAmount"];
                    sheet3["O" + x].Text = thirdSheet.Rows[i]["Remark"].ToString();
                    sheet3["P" + x].Text = thirdSheet.Rows[i]["From"].ToString();
                    sheet3["Q" + x].Text = thirdSheet.Rows[i]["To"].ToString();





                }
                TempConnectionStringv4(param.db);
                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@TenantID", mytype = SqlDbType.NVarChar, Value = param.TenantID });
                con.myparameters.Add(new myParameters { ParameterName = "@CutoffStart", mytype = SqlDbType.NVarChar, Value = param.CutoffStart });
                con.myparameters.Add(new myParameters { ParameterName = "@CutoffEnd", mytype = SqlDbType.NVarChar, Value = param.CutoffEnd });
                con.myparameters.Add(new myParameters { ParameterName = "@EmpStatus", mytype = SqlDbType.VarChar, Value = param.EmpStatus });
                con.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.Structured, Value = DTParam });
                DataTable FSheet = con.GetDataTable("sp_ExportSheet4v4");

                x = 2;
                for (int i = 0; i < FSheet.Rows.Count; i++, x++)
                {
                    sheet4["A" + x].Text = FSheet.Rows[i]["PayrollNum"].ToString();
                    sheet4["B" + x].Text = FSheet.Rows[i]["PayDate"].ToString();
                    sheet4["C" + x].Text = FSheet.Rows[i]["PayCode"].ToString();
                    sheet4["D" + x].Text = FSheet.Rows[i]["EmployeeID"].ToString();
                    sheet4["E" + x].Text = FSheet.Rows[i]["LastName"].ToString();
                    sheet4["F" + x].Text = FSheet.Rows[i]["FirstName"].ToString();
                    sheet4["G" + x].Text = FSheet.Rows[i]["FileStatus"].ToString();
                    sheet4["H" + x].Text = FSheet.Rows[i]["IncomeCode"].ToString();
                    sheet4["I" + x].Text = FSheet.Rows[i]["Date"].ToString();
                    sheet4["J" + x].Value = FSheet.Rows[i]["Amount"];
                    sheet4["K" + x].Text = FSheet.Rows[i]["RecurStart"].ToString();
                    sheet4["L" + x].Text = FSheet.Rows[i]["RecurEnd"].ToString();
                    sheet4["M" + x].Text = FSheet.Rows[i]["Freq"].ToString();
                    sheet4["N" + x].Text = FSheet.Rows[i]["Remarks"].ToString();
                    sheet4["O" + x].Text = FSheet.Rows[i]["From"].ToString();
                    sheet4["P" + x].Text = FSheet.Rows[i]["To"].ToString();
                    sheet4["Q" + x].Text = FSheet.Rows[i]["JECostCenter"].ToString();

                }

                TempConnectionStringv4(param.db);
                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@TenantID", mytype = SqlDbType.NVarChar, Value = param.TenantID });
                con.myparameters.Add(new myParameters { ParameterName = "@CutoffStart", mytype = SqlDbType.NVarChar, Value = param.CutoffStart });
                con.myparameters.Add(new myParameters { ParameterName = "@CutoffEnd", mytype = SqlDbType.NVarChar, Value = param.CutoffEnd });
                con.myparameters.Add(new myParameters { ParameterName = "@EmpStatus", mytype = SqlDbType.VarChar, Value = param.EmpStatus });
                con.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.Structured, Value = DTParam });
                DataTable FifthSheet = con.GetDataTable("sp_ExportSheet5v4");

                x = 2;
                for (int i = 0; i < FifthSheet.Rows.Count; i++, x++)
                {
                    sheet5["A" + x].Text = FifthSheet.Rows[i]["PayrollNum"].ToString();
                    sheet5["B" + x].Text = FifthSheet.Rows[i]["PayDate"].ToString();
                    sheet5["C" + x].Text = FifthSheet.Rows[i]["PayCode"].ToString();
                    sheet5["D" + x].Text = FifthSheet.Rows[i]["EmployeeID"].ToString();
                    sheet5["E" + x].Text = FifthSheet.Rows[i]["LastName"].ToString();
                    sheet5["F" + x].Text = FifthSheet.Rows[i]["FirstName"].ToString();
                    sheet5["G" + x].Text = FifthSheet.Rows[i]["FileStatus"].ToString();
                    sheet5["H" + x].Text = FifthSheet.Rows[i]["IncomeCode"].ToString();
                    sheet5["I" + x].Text = FifthSheet.Rows[i]["Date"].ToString();
                    sheet5["J" + x].Value = FifthSheet.Rows[i]["Amount"];
                    sheet5["K" + x].Text = FifthSheet.Rows[i]["RecurStart"].ToString();
                    sheet5["L" + x].Text = FifthSheet.Rows[i]["RecurEnd"].ToString();
                    sheet5["M" + x].Text = FifthSheet.Rows[i]["Freq"].ToString();
                    sheet5["N" + x].Text = FifthSheet.Rows[i]["Remarks"].ToString();
                    sheet5["O" + x].Text = FifthSheet.Rows[i]["From"].ToString();
                    sheet5["P" + x].Text = FifthSheet.Rows[i]["To"].ToString();
                    sheet5["Q" + x].Text = FifthSheet.Rows[i]["JECostCenter"].ToString();



                }
                TempConnectionStringv4(param.db);
                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@TenantID", mytype = SqlDbType.NVarChar, Value = param.TenantID });
                con.myparameters.Add(new myParameters { ParameterName = "@CutoffStart", mytype = SqlDbType.NVarChar, Value = param.CutoffStart });
                con.myparameters.Add(new myParameters { ParameterName = "@CutoffEnd", mytype = SqlDbType.NVarChar, Value = param.CutoffEnd });
                con.myparameters.Add(new myParameters { ParameterName = "@EmpStatus", mytype = SqlDbType.VarChar, Value = param.EmpStatus });
                con.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.Structured, Value = DTParam });
                DataTable SixSheet = con.GetDataTable("sp_ExportSheet6v4");

                x = 2;
                for (int i = 0; i < SixSheet.Rows.Count; i++, x++)
                {
                    sheet6["A" + x].Text = SixSheet.Rows[i]["PayrollNum"].ToString();
                    sheet6["B" + x].Text = SixSheet.Rows[i]["PayDate"].ToString();
                    sheet6["C" + x].Text = SixSheet.Rows[i]["PayCode"].ToString();
                    sheet6["D" + x].Text = SixSheet.Rows[i]["EmployeeID"].ToString();
                    sheet6["E" + x].Text = SixSheet.Rows[i]["LastName"].ToString();
                    sheet6["F" + x].Text = SixSheet.Rows[i]["FirstName"].ToString();
                    sheet6["G" + x].Text = SixSheet.Rows[i]["FileStatus"].ToString();
                    sheet6["H" + x].Text = SixSheet.Rows[i]["LoanCode"].ToString();
                    sheet6["I" + x].Text = SixSheet.Rows[i]["LoanDateStart"].ToString();
                    sheet6["J" + x].Value = SixSheet.Rows[i]["LoanPrincipal"];
                    sheet6["K" + x].Value = SixSheet.Rows[i]["LoanAmount"];
                    sheet6["L" + x].Value = SixSheet.Rows[i]["LoanPayments"];
                    sheet6["M" + x].Value = SixSheet.Rows[i]["LoanBalance"];
                    sheet6["N" + x].Value = SixSheet.Rows[i]["LoanDeductionAMT"];
                    sheet6["O" + x].Text = SixSheet.Rows[i]["LoanDeductionFreq"].ToString();
                    sheet6["P" + x].Text = SixSheet.Rows[i]["LoanStatus"].ToString();
                    sheet6["Q" + x].Text = SixSheet.Rows[i]["LoanDateGranted"].ToString();
                    sheet6["R" + x].Text = SixSheet.Rows[i]["From"].ToString();
                    sheet6["S" + x].Text = SixSheet.Rows[i]["To"].ToString();
                    sheet6["T" + x].Text = SixSheet.Rows[i]["JECostCenter"].ToString();


                }

                TempConnectionStringv4(param.db);
                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@TenantID", mytype = SqlDbType.NVarChar, Value = param.TenantID });
                con.myparameters.Add(new myParameters { ParameterName = "@CutoffStart", mytype = SqlDbType.NVarChar, Value = param.CutoffStart });
                con.myparameters.Add(new myParameters { ParameterName = "@CutoffEnd", mytype = SqlDbType.NVarChar, Value = param.CutoffEnd });
                con.myparameters.Add(new myParameters { ParameterName = "@EmpStatus", mytype = SqlDbType.VarChar, Value = param.EmpStatus });
                con.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.Structured, Value = DTParam });
                DataTable SevenSheet = con.GetDataTable("sp_Exportsheet7v4");

                x = 2;
                for (int i = 0; i < SevenSheet.Rows.Count; i++, x++)
                {
                    sheet7["A" + x].Text = SevenSheet.Rows[i]["PayrollNum"].ToString();
                    sheet7["B" + x].Text = SevenSheet.Rows[i]["PayDate"].ToString();
                    sheet7["C" + x].Text = SevenSheet.Rows[i]["PayCode"].ToString();
                    sheet7["D" + x].Text = SevenSheet.Rows[i]["EmployeeID"].ToString();
                    sheet7["E" + x].Text = SevenSheet.Rows[i]["LastName"].ToString();
                    sheet7["F" + x].Text = SevenSheet.Rows[i]["FirstName"].ToString();
                    sheet7["G" + x].Text = SevenSheet.Rows[i]["FileStatus"].ToString();
                    sheet7["H" + x].Text = SevenSheet.Rows[i]["DeductionCode"].ToString();
                    sheet7["I" + x].Text = SevenSheet.Rows[i]["Date"].ToString();
                    sheet7["J" + x].Value = SevenSheet.Rows[i]["DeductionAmount"];
                    sheet7["K" + x].Text = SevenSheet.Rows[i]["DeductionFreq"].ToString();
                    sheet7["L" + x].Text = SevenSheet.Rows[i]["LastDeductionDate"].ToString();
                    sheet7["M" + x].Text = SevenSheet.Rows[i]["From"].ToString();
                    sheet7["N" + x].Text = SevenSheet.Rows[i]["To"].ToString();
                    sheet7["O" + x].Text = SevenSheet.Rows[i]["JECostCenter"].ToString();



                }
                HttpResponse httpResponse = System.Web.HttpContext.Current.Response;

                httpResponse.Clear();
                httpResponse.ContentType = "Application/x-msexcel";
                httpResponse.AddHeader("Content-Disposition", String.Format("attachment; filename={0}", filename));
                string newDIR = HttpContext.Current.Server.MapPath("~/sFTP/PayrollOutsourcing/");
                string onlineDIR = "http://ws.durusthr.com/ILM_WS_Live/sFTP/PayrollOutsourcing/";
                //filename = filename.Replace(',', '_');
                //filename = filename.Replace(" ", String.Empty);
                workbook.Save(newDIR + filename);
                stream.Close();
                //return "Success";

                if (PayRegData.Rows.Count > 0)
                {
                    return onlineDIR + filename;
                }
                else
                {

                    return "No Rows";
                }
            }
            catch (Exception e)
            {
                stream.Close();
                return "Error: " + e.ToString();
            }
        }
        [HttpPost]
        [Route("PO_ExtractPayRegv5")]
        public string ExtractReportv5(efile param)
        {

            FileStream stream = null;
            try
            {
                DataTable DTParam = new DataTable();
                DTParam.Columns.AddRange(new DataColumn[1] { new DataColumn("empid") });
                for (int i = 0; i < param.empid.Length; i++)
                {
                    DTParam.Rows.Add(param.empid[i]);
                }

                TempConnectionStringv4(param.db);
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@TenantID", mytype = SqlDbType.NVarChar, Value = param.TenantID });
                con.myparameters.Add(new myParameters { ParameterName = "@CutoffStart", mytype = SqlDbType.NVarChar, Value = param.CutoffStart });
                con.myparameters.Add(new myParameters { ParameterName = "@CutoffEnd", mytype = SqlDbType.NVarChar, Value = param.CutoffEnd });
                con.myparameters.Add(new myParameters { ParameterName = "@EmpStatus", mytype = SqlDbType.VarChar, Value = param.EmpStatus });
                con.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.Structured, Value = DTParam });
                DataTable PayRegData = con.GetDataTable("sp_Extract_PayRegv5");


                string dataDir = HostingEnvironment.MapPath("~/Files/PayrollOutSourcing/");

                stream = new FileStream(dataDir + "ExtractReport.xls", FileMode.Open);
                string filename;
                //string datebuild = DateTime.Now.ToString("MMddyyyy");
                string cc = "";
                if (PayRegData.Rows.Count != 0)
                {
                    cc = PayRegData.Rows[0]["Ccode"].ToString();
                }

                string cStart = Regex.Replace(param.CutoffStart, @"[^0-9a-zA-Z]+", "");
                string cEnd = Regex.Replace(param.CutoffEnd, @"[^0-9a-zA-Z]+", "");


                string datebuild = DateTime.Now.ToString("yyyyMM");
                string timebuild = DateTime.Now.ToString("hhtt");
                if (param.EmpStatus == "SSS MAT")
                {
                    filename = cc + "_SSSMAT_" + cEnd + ".xls";
                }
                else if (param.EmpStatus == "13th Month")
                {
                    filename = cc + "_13thMonth_" + cEnd + "_0.xls";
                }
                else if (param.EmpStatus == "Final")
                {
                    filename = cc + "_FinalPay_" + cEnd + "_0.xls";
                }

                else
                {
                    filename = cc + "_PayReg_" + cStart + "_" + cEnd + "_" + timebuild + ".xls";
                }
                using (var fileStream = new FileStream(dataDir + filename, FileMode.Create, FileAccess.Write))
                //using (var fileStream = new FileStream(dataDir + filename, FileMode.Create, FileAccess.Write))
                {
                    stream.CopyTo(fileStream);
                }
                stream.Close();
                ExcelWorkbookFormat workbookFormat = ExcelWorkbookFormat.Xls_2003;
                string testDocFile = dataDir + "ExtractReport.xls";
                ExcelWorkbook workbook = new ExcelWorkbook(testDocFile);
                ExcelWorksheet firstWorksheet = workbook.Worksheets[0];

                #region PayReg Sheet

                ExcelWorksheet PayReg = workbook.Worksheets[0];
                ExcelWorksheet sheet2 = workbook.Worksheets[1];
                ExcelWorksheet sheet3 = workbook.Worksheets[2];
                ExcelWorksheet sheet4 = workbook.Worksheets[3];
                ExcelWorksheet sheet5 = workbook.Worksheets[4];
                ExcelWorksheet sheet6 = workbook.Worksheets[5];
                ExcelWorksheet sheet7 = workbook.Worksheets[6];
                //Sheet7.WorksheetSecurity.ProtectWorksheet("sbc01");
                //AX TAXTABLE

                int x = 2;
                for (int i = 0; i < PayRegData.Rows.Count; i++, x++)
                {

                    PayReg["A" + x].Text = PayRegData.Rows[i]["PayrollNum"].ToString(); //payroll#
                    PayReg["B" + x].Text = PayRegData.Rows[i]["PayDate"].ToString(); //paydate
                    PayReg["C" + x].Text = PayRegData.Rows[i]["PayCode"].ToString(); //pay code
                    PayReg["D" + x].Text = PayRegData.Rows[i]["EmpID"].ToString(); //employee id
                    PayReg["E" + x].Text = PayRegData.Rows[i]["LastName"].ToString(); //last name
                    PayReg["F" + x].Text = PayRegData.Rows[i]["FirstName"].ToString(); //first name
                    PayReg["G" + x].Text = PayRegData.Rows[i]["FileStatus"].ToString(); //file status
                    PayReg["H" + x].Text = PayRegData.Rows[i]["TaxStatus"].ToString(); //tax status
                    PayReg["I" + x].Value = PayRegData.Rows[i]["DailyRate"]; //daily rate
                    PayReg["J" + x].Value = PayRegData.Rows[i]["SalaryRateType"]; //salary rate type
                    PayReg["K" + x].Value = PayRegData.Rows[i]["BasicSalary"]; //basic salary
                    PayReg["L" + x].Value = PayRegData.Rows[i]["MiscAmount"]; //misc amount
                    PayReg["M" + x].Value = PayRegData.Rows[i]["LeaveAmount"];
                    PayReg["N" + x].Value = PayRegData.Rows[i]["OvertimeReg"]; //overtime
                    PayReg["O" + x].Value = PayRegData.Rows[i]["OvertimeHol"];
                    PayReg["P" + x].Value = PayRegData.Rows[i]["OtherTxIncome"]; //other tx income
                    PayReg["Q" + x].Text = PayRegData.Rows[i]["Adjustments"].ToString(); //adjustments
                    PayReg["R" + x].Value = PayRegData.Rows[i]["GrossIncome"]; //gross income
                    PayReg["S" + x].Value = PayRegData.Rows[i]["WithholdingTax"]; //withholding tax
                    PayReg["T" + x].Value = PayRegData.Rows[i]["NetSalaryAfterTax"]; //net salary after tax
                    PayReg["U" + x].Value = PayRegData.Rows[i]["EmployeeSSS"]; //employee sss
                    PayReg["V" + x].Value = PayRegData.Rows[i]["EmployeePhilHealth"]; //employee mcr(philhealth)
                    PayReg["W" + x].Value = PayRegData.Rows[i]["EmployeePagibig"]; //employee pagibig
                    PayReg["X" + x].Value = PayRegData.Rows[i]["OtherNtxIncome"]; //other ntx income
                    PayReg["Y" + x].Value = PayRegData.Rows[i]["LoanPayments"]; //loan payments
                    PayReg["Z" + x].Value = PayRegData.Rows[i]["Deductions"]; //deductions
                    PayReg["AA" + x].Value = PayRegData.Rows[i]["NetSalary"]; //net salary
                    PayReg["AB" + x].Value = PayRegData.Rows[i]["EmployerSSS"]; //employer sss
                    PayReg["AC" + x].Value = PayRegData.Rows[i]["EmployerPhilhealth"]; //employer mcr(philhealth)
                    PayReg["AD" + x].Value = PayRegData.Rows[i]["EmployerEC"]; //employer ec
                    PayReg["AE" + x].Value = PayRegData.Rows[i]["EmployerPagibig"]; //employer pagibig
                    PayReg["AF" + x].Value = PayRegData.Rows[i]["PayrollCost"]; //payroll cost
                    PayReg["AG" + x].Value = PayRegData.Rows[i]["PreviousYTDGross"]; //payment type
                    PayReg["AH" + x].Value = PayRegData.Rows[i]["PreviousYTDWitholding"]; //bank acct#
                    PayReg["AI" + x].Value = PayRegData.Rows[i]["PreviousYTDSSS"]; //bank name
                    PayReg["AJ" + x].Value = PayRegData.Rows[i]["PreviousYTDMCR"]; //date employed
                    PayReg["AK" + x].Value = PayRegData.Rows[i]["PreviousYTDPagibig"]; //date terminated
                    PayReg["AL" + x].Value = PayRegData.Rows[i]["Previous13thNT"]; //cost center
                    PayReg["AM" + x].Value = PayRegData.Rows[i]["Previous13thTX"];
                    PayReg["AN" + x].Text = PayRegData.Rows[i]["PaymentType"].ToString(); //currency
                    PayReg["AO" + x].Text = PayRegData.Rows[i]["BankAcc"].ToString(); //exchange rate
                    PayReg["AP" + x].Text = PayRegData.Rows[i]["BankName"].ToString(); //payment freq
                    PayReg["AQ" + x].Text = PayRegData.Rows[i]["CommentField"].ToString();
                    PayReg["AR" + x].Text = PayRegData.Rows[i]["ErrorField"].ToString();
                    PayReg["AS" + x].Text = PayRegData.Rows[i]["DateEmployed"].ToString();
                    PayReg["AT" + x].Text = PayRegData.Rows[i]["DateTerminated"].ToString();
                    PayReg["AU" + x].Value = PayRegData.Rows[i]["CostCenter"];
                    PayReg["AV" + x].Text = PayRegData.Rows[i]["Currency"].ToString();
                    PayReg["AW" + x].Text = PayRegData.Rows[i]["ExchangeRate"].ToString();
                    PayReg["AX" + x].Text = PayRegData.Rows[i]["PaymentPreq"].ToString();
                    PayReg["AY" + x].Value = PayRegData.Rows[i]["MTDGross"];
                    PayReg["AZ" + x].Value = PayRegData.Rows[i]["MTDBasic"];
                    PayReg["BA" + x].Value = PayRegData.Rows[i]["MTDSSSEmployee"];
                    PayReg["BB" + x].Value = PayRegData.Rows[i]["MTDMCREmployee"];
                    PayReg["BC" + x].Value = PayRegData.Rows[i]["MTDPagIbigEmployee"];
                    PayReg["BD" + x].Value = PayRegData.Rows[i]["MTDSSSEmployer"];
                    PayReg["BE" + x].Value = PayRegData.Rows[i]["MTDMCREmployer"];
                    PayReg["BF" + x].Value = PayRegData.Rows[i]["MTDECEmployer"];
                    PayReg["BG" + x].Value = PayRegData.Rows[i]["MTDPabIbigEmployer"];
                    PayReg["BH" + x].Value = PayRegData.Rows[i]["MTDWHTAX"];
                    PayReg["BI" + x].Value = PayRegData.Rows[i]["MonthlyBasic"];
                    PayReg["BJ" + x].Value = PayRegData.Rows[i]["MonthlyAllow"];
                    PayReg["BK" + x].Value = PayRegData.Rows[i]["MTDNTX"];
                    PayReg["BL" + x].Text = PayRegData.Rows[i]["FROM"].ToString();
                    PayReg["BM" + x].Text = PayRegData.Rows[i]["TO"].ToString();




                }
                #endregion
                TempConnectionStringv4(param.db);
                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@TenantID", mytype = SqlDbType.NVarChar, Value = param.TenantID });
                con.myparameters.Add(new myParameters { ParameterName = "@CutoffStart", mytype = SqlDbType.NVarChar, Value = param.CutoffStart });
                con.myparameters.Add(new myParameters { ParameterName = "@CutoffEnd", mytype = SqlDbType.NVarChar, Value = param.CutoffEnd });
                con.myparameters.Add(new myParameters { ParameterName = "@EmpStatus", mytype = SqlDbType.VarChar, Value = param.EmpStatus });
                con.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.Structured, Value = DTParam });
                DataTable secondSheet = con.GetDataTable("sp_ExportSheet2v5");

                x = 2;
                for (int i = 0; i < secondSheet.Rows.Count; i++, x++)
                {
                    sheet2["A" + x].Text = secondSheet.Rows[i]["PayrollNum"].ToString();
                    sheet2["B" + x].Text = secondSheet.Rows[i]["PayOutDate"].ToString();
                    sheet2["C" + x].Text = secondSheet.Rows[i]["PayCode"].ToString();
                    sheet2["D" + x].Text = secondSheet.Rows[i]["EmployeeID"].ToString();
                    sheet2["E" + x].Text = secondSheet.Rows[i]["LastName"].ToString();
                    sheet2["F" + x].Text = secondSheet.Rows[i]["FirstName"].ToString();
                    sheet2["G" + x].Text = secondSheet.Rows[i]["FileStatus"].ToString();
                    sheet2["H" + x].Text = secondSheet.Rows[i]["TransactionType"].ToString();
                    sheet2["I" + x].Text = secondSheet.Rows[i]["Date"].ToString();
                    sheet2["J" + x].Value = secondSheet.Rows[i]["CostCenter"];
                    sheet2["K" + x].Value = secondSheet.Rows[i]["Days"];
                    sheet2["L" + x].Value = secondSheet.Rows[i]["Hours"];
                    sheet2["M" + x].Value = secondSheet.Rows[i]["Amount"];
                    sheet2["N" + x].Text = secondSheet.Rows[i]["Remark"].ToString();
                    sheet2["O" + x].Text = secondSheet.Rows[i]["FROM"].ToString();
                    sheet2["P" + x].Text = secondSheet.Rows[i]["TO"].ToString();

                }

                TempConnectionStringv4(param.db);
                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@TenantID", mytype = SqlDbType.NVarChar, Value = param.TenantID });
                con.myparameters.Add(new myParameters { ParameterName = "@CutoffStart", mytype = SqlDbType.NVarChar, Value = param.CutoffStart });
                con.myparameters.Add(new myParameters { ParameterName = "@CutoffEnd", mytype = SqlDbType.NVarChar, Value = param.CutoffEnd });
                con.myparameters.Add(new myParameters { ParameterName = "@EmpStatus", mytype = SqlDbType.VarChar, Value = param.EmpStatus });
                con.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.Structured, Value = DTParam });
                DataTable thirdSheet = con.GetDataTable("sp_ExportSheet3v5");

                x = 2;
                for (int i = 0; i < thirdSheet.Rows.Count; i++, x++)
                {
                    sheet3["A" + x].Text = thirdSheet.Rows[i]["PayrollNum"].ToString();
                    sheet3["B" + x].Text = thirdSheet.Rows[i]["PayDate"].ToString();
                    sheet3["C" + x].Text = thirdSheet.Rows[i]["PayCode"].ToString();
                    sheet3["D" + x].Text = thirdSheet.Rows[i]["EmpID"].ToString();
                    sheet3["E" + x].Text = thirdSheet.Rows[i]["LastName"].ToString();
                    sheet3["F" + x].Text = thirdSheet.Rows[i]["FirstName"].ToString();
                    sheet3["G" + x].Text = thirdSheet.Rows[i]["FileStatus"].ToString();
                    sheet3["H" + x].Text = thirdSheet.Rows[i]["OTCode"].ToString();
                    sheet3["I" + x].Text = thirdSheet.Rows[i]["Date"].ToString();
                    sheet3["J" + x].Value = thirdSheet.Rows[i]["CostCenter"];
                    sheet3["K" + x].Value = thirdSheet.Rows[i]["OTRate"];
                    sheet3["L" + x].Value = thirdSheet.Rows[i]["OTHours"];
                    sheet3["M" + x].Value = thirdSheet.Rows[i]["OTMinutes"];
                    sheet3["N" + x].Value = thirdSheet.Rows[i]["OTAmount"];
                    sheet3["O" + x].Text = thirdSheet.Rows[i]["Remark"].ToString();
                    sheet3["P" + x].Text = thirdSheet.Rows[i]["From"].ToString();
                    sheet3["Q" + x].Text = thirdSheet.Rows[i]["To"].ToString();





                }
                TempConnectionStringv4(param.db);
                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@TenantID", mytype = SqlDbType.NVarChar, Value = param.TenantID });
                con.myparameters.Add(new myParameters { ParameterName = "@CutoffStart", mytype = SqlDbType.NVarChar, Value = param.CutoffStart });
                con.myparameters.Add(new myParameters { ParameterName = "@CutoffEnd", mytype = SqlDbType.NVarChar, Value = param.CutoffEnd });
                con.myparameters.Add(new myParameters { ParameterName = "@EmpStatus", mytype = SqlDbType.VarChar, Value = param.EmpStatus });
                con.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.Structured, Value = DTParam });
                DataTable FSheet = con.GetDataTable("sp_ExportSheet4v5");

                x = 2;
                for (int i = 0; i < FSheet.Rows.Count; i++, x++)
                {
                    sheet4["A" + x].Text = FSheet.Rows[i]["PayrollNum"].ToString();
                    sheet4["B" + x].Text = FSheet.Rows[i]["PayDate"].ToString();
                    sheet4["C" + x].Text = FSheet.Rows[i]["PayCode"].ToString();
                    sheet4["D" + x].Text = FSheet.Rows[i]["EmployeeID"].ToString();
                    sheet4["E" + x].Text = FSheet.Rows[i]["LastName"].ToString();
                    sheet4["F" + x].Text = FSheet.Rows[i]["FirstName"].ToString();
                    sheet4["G" + x].Text = FSheet.Rows[i]["FileStatus"].ToString();
                    sheet4["H" + x].Text = FSheet.Rows[i]["IncomeCode"].ToString();
                    sheet4["I" + x].Text = FSheet.Rows[i]["Date"].ToString();
                    sheet4["J" + x].Value = FSheet.Rows[i]["Amount"];
                    sheet4["K" + x].Text = FSheet.Rows[i]["RecurStart"].ToString();
                    sheet4["L" + x].Text = FSheet.Rows[i]["RecurEnd"].ToString();
                    sheet4["M" + x].Text = FSheet.Rows[i]["Freq"].ToString();
                    sheet4["N" + x].Text = FSheet.Rows[i]["Remarks"].ToString();
                    sheet4["O" + x].Text = FSheet.Rows[i]["From"].ToString();
                    sheet4["P" + x].Text = FSheet.Rows[i]["To"].ToString();
                    sheet4["Q" + x].Text = FSheet.Rows[i]["JECostCenter"].ToString();

                }

                TempConnectionStringv4(param.db);
                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@TenantID", mytype = SqlDbType.NVarChar, Value = param.TenantID });
                con.myparameters.Add(new myParameters { ParameterName = "@CutoffStart", mytype = SqlDbType.NVarChar, Value = param.CutoffStart });
                con.myparameters.Add(new myParameters { ParameterName = "@CutoffEnd", mytype = SqlDbType.NVarChar, Value = param.CutoffEnd });
                con.myparameters.Add(new myParameters { ParameterName = "@EmpStatus", mytype = SqlDbType.VarChar, Value = param.EmpStatus });
                con.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.Structured, Value = DTParam });
                DataTable FifthSheet = con.GetDataTable("sp_ExportSheet5v5");

                x = 2;
                for (int i = 0; i < FifthSheet.Rows.Count; i++, x++)
                {
                    sheet5["A" + x].Text = FifthSheet.Rows[i]["PayrollNum"].ToString();
                    sheet5["B" + x].Text = FifthSheet.Rows[i]["PayDate"].ToString();
                    sheet5["C" + x].Text = FifthSheet.Rows[i]["PayCode"].ToString();
                    sheet5["D" + x].Text = FifthSheet.Rows[i]["EmployeeID"].ToString();
                    sheet5["E" + x].Text = FifthSheet.Rows[i]["LastName"].ToString();
                    sheet5["F" + x].Text = FifthSheet.Rows[i]["FirstName"].ToString();
                    sheet5["G" + x].Text = FifthSheet.Rows[i]["FileStatus"].ToString();
                    sheet5["H" + x].Text = FifthSheet.Rows[i]["IncomeCode"].ToString();
                    sheet5["I" + x].Text = FifthSheet.Rows[i]["Date"].ToString();
                    sheet5["J" + x].Value = FifthSheet.Rows[i]["Amount"];
                    sheet5["K" + x].Text = FifthSheet.Rows[i]["RecurStart"].ToString();
                    sheet5["L" + x].Text = FifthSheet.Rows[i]["RecurEnd"].ToString();
                    sheet5["M" + x].Text = FifthSheet.Rows[i]["Freq"].ToString();
                    sheet5["N" + x].Text = FifthSheet.Rows[i]["Remarks"].ToString();
                    sheet5["O" + x].Text = FifthSheet.Rows[i]["From"].ToString();
                    sheet5["P" + x].Text = FifthSheet.Rows[i]["To"].ToString();
                    sheet5["Q" + x].Text = FifthSheet.Rows[i]["JECostCenter"].ToString();



                }
                TempConnectionStringv4(param.db);
                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@TenantID", mytype = SqlDbType.NVarChar, Value = param.TenantID });
                con.myparameters.Add(new myParameters { ParameterName = "@CutoffStart", mytype = SqlDbType.NVarChar, Value = param.CutoffStart });
                con.myparameters.Add(new myParameters { ParameterName = "@CutoffEnd", mytype = SqlDbType.NVarChar, Value = param.CutoffEnd });
                con.myparameters.Add(new myParameters { ParameterName = "@EmpStatus", mytype = SqlDbType.VarChar, Value = param.EmpStatus });
                con.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.Structured, Value = DTParam });
                DataTable SixSheet = con.GetDataTable("sp_ExportSheet6v5");

                x = 2;
                for (int i = 0; i < SixSheet.Rows.Count; i++, x++)
                {
                    sheet6["A" + x].Text = SixSheet.Rows[i]["PayrollNum"].ToString();
                    sheet6["B" + x].Text = SixSheet.Rows[i]["PayDate"].ToString();
                    sheet6["C" + x].Text = SixSheet.Rows[i]["PayCode"].ToString();
                    sheet6["D" + x].Text = SixSheet.Rows[i]["EmployeeID"].ToString();
                    sheet6["E" + x].Text = SixSheet.Rows[i]["LastName"].ToString();
                    sheet6["F" + x].Text = SixSheet.Rows[i]["FirstName"].ToString();
                    sheet6["G" + x].Text = SixSheet.Rows[i]["FileStatus"].ToString();
                    sheet6["H" + x].Text = SixSheet.Rows[i]["LoanCode"].ToString();
                    sheet6["I" + x].Text = SixSheet.Rows[i]["LoanDateStart"].ToString();
                    sheet6["J" + x].Value = SixSheet.Rows[i]["LoanPrincipal"];
                    sheet6["K" + x].Value = SixSheet.Rows[i]["LoanAmount"];
                    sheet6["L" + x].Value = SixSheet.Rows[i]["LoanPayments"];
                    sheet6["M" + x].Value = SixSheet.Rows[i]["LoanBalance"];
                    sheet6["N" + x].Value = SixSheet.Rows[i]["LoanDeductionAMT"];
                    sheet6["O" + x].Text = SixSheet.Rows[i]["LoanDeductionFreq"].ToString();
                    sheet6["P" + x].Text = SixSheet.Rows[i]["LoanStatus"].ToString();
                    sheet6["Q" + x].Text = SixSheet.Rows[i]["LoanDateGranted"].ToString();
                    sheet6["R" + x].Text = SixSheet.Rows[i]["From"].ToString();
                    sheet6["S" + x].Text = SixSheet.Rows[i]["To"].ToString();
                    sheet6["T" + x].Text = SixSheet.Rows[i]["JECostCenter"].ToString();


                }

                TempConnectionStringv4(param.db);
                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@TenantID", mytype = SqlDbType.NVarChar, Value = param.TenantID });
                con.myparameters.Add(new myParameters { ParameterName = "@CutoffStart", mytype = SqlDbType.NVarChar, Value = param.CutoffStart });
                con.myparameters.Add(new myParameters { ParameterName = "@CutoffEnd", mytype = SqlDbType.NVarChar, Value = param.CutoffEnd });
                con.myparameters.Add(new myParameters { ParameterName = "@EmpStatus", mytype = SqlDbType.VarChar, Value = param.EmpStatus });
                con.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.Structured, Value = DTParam });
                DataTable SevenSheet = con.GetDataTable("sp_Exportsheet7v5");

                x = 2;
                for (int i = 0; i < SevenSheet.Rows.Count; i++, x++)
                {
                    sheet7["A" + x].Text = SevenSheet.Rows[i]["PayrollNum"].ToString();
                    sheet7["B" + x].Text = SevenSheet.Rows[i]["PayDate"].ToString();
                    sheet7["C" + x].Text = SevenSheet.Rows[i]["PayCode"].ToString();
                    sheet7["D" + x].Text = SevenSheet.Rows[i]["EmployeeID"].ToString();
                    sheet7["E" + x].Text = SevenSheet.Rows[i]["LastName"].ToString();
                    sheet7["F" + x].Text = SevenSheet.Rows[i]["FirstName"].ToString();
                    sheet7["G" + x].Text = SevenSheet.Rows[i]["FileStatus"].ToString();
                    sheet7["H" + x].Text = SevenSheet.Rows[i]["DeductionCode"].ToString();
                    sheet7["I" + x].Text = SevenSheet.Rows[i]["Date"].ToString();
                    sheet7["J" + x].Value = SevenSheet.Rows[i]["DeductionAmount"];
                    sheet7["K" + x].Text = SevenSheet.Rows[i]["DeductionFreq"].ToString();
                    sheet7["L" + x].Text = SevenSheet.Rows[i]["LastDeductionDate"].ToString();
                    sheet7["M" + x].Text = SevenSheet.Rows[i]["From"].ToString();
                    sheet7["N" + x].Text = SevenSheet.Rows[i]["To"].ToString();
                    sheet7["O" + x].Text = SevenSheet.Rows[i]["JECostCenter"].ToString();



                }
                HttpResponse httpResponse = System.Web.HttpContext.Current.Response;

                httpResponse.Clear();
                httpResponse.ContentType = "Application/x-msexcel";
                httpResponse.AddHeader("Content-Disposition", String.Format("attachment; filename={0}", filename));
                string newDIR = HttpContext.Current.Server.MapPath("~/sFTP/PayrollOutsourcing/");
                string onlineDIR = "https://ws.durusthr.com/ILM_WS_Live/sFTP/PayrollOutsourcing/";
                //filename = filename.Replace(',', '_');
                //filename = filename.Replace(" ", String.Empty);
                workbook.Save(newDIR + filename);
                stream.Close();
                //return "Success";

                if (PayRegData.Rows.Count > 0)
                {
                    return onlineDIR + filename;
                }
                else
                {

                    return "No Rows";
                }
            }
            catch (Exception e)
            {
                stream.Close();
                return "Error: " + e.ToString();
            }
        }

        [HttpPost]
        [Route("PO_ExtractPayReg4Currency")]
        public string ExtractPayReg4Currency(efile param)
        {

            FileStream stream = null;
            try
            {
                DataTable DTParam = new DataTable();
                DTParam.Columns.AddRange(new DataColumn[1] { new DataColumn("empid") });
                for (int i = 0; i < param.empid.Length; i++)
                {
                    DTParam.Rows.Add(param.empid[i]);
                }
                DataTable dataCurr = new DataTable();
                dataCurr.Columns.AddRange(new DataColumn[2] { new DataColumn("currency"), new DataColumn("rate") });
                for (int i = 0; i < param.currency.Length; i++)
                {
                    dataCurr.Rows.Add(param.currency[i], param.rate[i]);

                }

                TempConnectionStringv4(param.db);
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@TenantID", mytype = SqlDbType.NVarChar, Value = param.TenantID });
                con.myparameters.Add(new myParameters { ParameterName = "@CutoffStart", mytype = SqlDbType.NVarChar, Value = param.CutoffStart });
                con.myparameters.Add(new myParameters { ParameterName = "@CutoffEnd", mytype = SqlDbType.NVarChar, Value = param.CutoffEnd });
                con.myparameters.Add(new myParameters { ParameterName = "@EmpStatus", mytype = SqlDbType.VarChar, Value = param.EmpStatus });
                con.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.Structured, Value = DTParam });
                con.myparameters.Add(new myParameters { ParameterName = "@CurrencyRate", mytype = SqlDbType.Structured, Value = dataCurr });

                DataTable PayRegData = con.GetDataTable("sp_Extract_PayReg4Currency");


                string dataDir = HostingEnvironment.MapPath("~/Files/PayrollOutSourcing/");

                stream = new FileStream(dataDir + "ExtractReport.xls", FileMode.Open);
                string filename;
                //string datebuild = DateTime.Now.ToString("MMddyyyy");
                string cc = "";
                if (PayRegData.Rows.Count != 0)
                {
                    cc = PayRegData.Rows[0]["Ccode"].ToString();
                }

                string cStart = Regex.Replace(param.CutoffStart, @"[^0-9a-zA-Z]+", "");
                string cEnd = Regex.Replace(param.CutoffEnd, @"[^0-9a-zA-Z]+", "");


                string datebuild = DateTime.Now.ToString("yyyyMM");
                string timebuild = DateTime.Now.ToString("hhtt");
                if (param.EmpStatus == "SSS MAT")
                {
                    filename = cc + "_SSSMAT_" + cEnd + ".xls";
                }
                else if (param.EmpStatus == "13th Month")
                {
                    filename = cc + "_13thMonth_" + cEnd + "_0.xls";
                }
                else if (param.EmpStatus == "Final")
                {
                    filename = cc + "_FinalPay_" + cEnd + "_0.xls";
                }

                else
                {
                    filename = cc + "_PayReg_" + cStart + "_" + cEnd + "_" + timebuild + ".xls";
                }
                using (var fileStream = new FileStream(dataDir + filename, FileMode.Create, FileAccess.Write))
                //using (var fileStream = new FileStream(dataDir + filename, FileMode.Create, FileAccess.Write))
                {
                    stream.CopyTo(fileStream);
                }
                stream.Close();
                ExcelWorkbookFormat workbookFormat = ExcelWorkbookFormat.Xls_2003;
                string testDocFile = dataDir + "ExtractReport.xls";
                ExcelWorkbook workbook = new ExcelWorkbook(testDocFile);
                ExcelWorksheet firstWorksheet = workbook.Worksheets[0];

                #region PayReg Sheet

                ExcelWorksheet PayReg = workbook.Worksheets[0];
                ExcelWorksheet sheet2 = workbook.Worksheets[1];
                ExcelWorksheet sheet3 = workbook.Worksheets[2];
                ExcelWorksheet sheet4 = workbook.Worksheets[3];
                ExcelWorksheet sheet5 = workbook.Worksheets[4];
                ExcelWorksheet sheet6 = workbook.Worksheets[5];
                ExcelWorksheet sheet7 = workbook.Worksheets[6];
                //Sheet7.WorksheetSecurity.ProtectWorksheet("sbc01");
                //AX TAXTABLE

                int x = 2;
                for (int i = 0; i < PayRegData.Rows.Count; i++, x++)
                {

                    PayReg["A" + x].Text = PayRegData.Rows[i]["PayrollNum"].ToString(); //payroll#
                    PayReg["B" + x].Text = PayRegData.Rows[i]["PayDate"].ToString(); //paydate
                    PayReg["C" + x].Text = PayRegData.Rows[i]["PayCode"].ToString(); //pay code
                    PayReg["D" + x].Text = PayRegData.Rows[i]["EmpID"].ToString(); //employee id
                    PayReg["E" + x].Text = PayRegData.Rows[i]["LastName"].ToString(); //last name
                    PayReg["F" + x].Text = PayRegData.Rows[i]["FirstName"].ToString(); //first name
                    PayReg["G" + x].Text = PayRegData.Rows[i]["FileStatus"].ToString(); //file status
                    PayReg["H" + x].Text = PayRegData.Rows[i]["TaxStatus"].ToString(); //tax status
                    PayReg["I" + x].Value = PayRegData.Rows[i]["DailyRate"]; //daily rate
                    PayReg["J" + x].Value = PayRegData.Rows[i]["SalaryRateType"]; //salary rate type
                    PayReg["K" + x].Value = PayRegData.Rows[i]["BasicSalary"]; //basic salary
                    PayReg["L" + x].Value = PayRegData.Rows[i]["MiscAmount"]; //misc amount
                    PayReg["M" + x].Value = PayRegData.Rows[i]["LeaveAmount"];
                    PayReg["N" + x].Value = PayRegData.Rows[i]["OvertimeReg"]; //overtime
                    PayReg["O" + x].Value = PayRegData.Rows[i]["OvertimeHol"];
                    PayReg["P" + x].Value = PayRegData.Rows[i]["OtherTxIncome"]; //other tx income
                    PayReg["Q" + x].Text = PayRegData.Rows[i]["Adjustments"].ToString(); //adjustments
                    PayReg["R" + x].Value = PayRegData.Rows[i]["GrossIncome"]; //gross income
                    PayReg["S" + x].Value = PayRegData.Rows[i]["WithholdingTax"]; //withholding tax
                    PayReg["T" + x].Value = PayRegData.Rows[i]["NetSalaryAfterTax"]; //net salary after tax
                    PayReg["U" + x].Value = PayRegData.Rows[i]["EmployeeSSS"]; //employee sss
                    PayReg["V" + x].Value = PayRegData.Rows[i]["EmployeePhilHealth"]; //employee mcr(philhealth)
                    PayReg["W" + x].Value = PayRegData.Rows[i]["EmployeePagibig"]; //employee pagibig
                    PayReg["X" + x].Value = PayRegData.Rows[i]["OtherNtxIncome"]; //other ntx income
                    PayReg["Y" + x].Value = PayRegData.Rows[i]["LoanPayments"]; //loan payments
                    PayReg["Z" + x].Value = PayRegData.Rows[i]["Deductions"]; //deductions
                    PayReg["AA" + x].Value = PayRegData.Rows[i]["NetSalary"]; //net salary
                    PayReg["AB" + x].Value = PayRegData.Rows[i]["EmployerSSS"]; //employer sss
                    PayReg["AC" + x].Value = PayRegData.Rows[i]["EmployerPhilhealth"]; //employer mcr(philhealth)
                    PayReg["AD" + x].Value = PayRegData.Rows[i]["EmployerEC"]; //employer ec
                    PayReg["AE" + x].Value = PayRegData.Rows[i]["EmployerPagibig"]; //employer pagibig
                    PayReg["AF" + x].Value = PayRegData.Rows[i]["PayrollCost"]; //payroll cost
                    PayReg["AG" + x].Value = PayRegData.Rows[i]["PreviousYTDGross"]; //payment type
                    PayReg["AH" + x].Value = PayRegData.Rows[i]["PreviousYTDWitholding"]; //bank acct#
                    PayReg["AI" + x].Value = PayRegData.Rows[i]["PreviousYTDSSS"]; //bank name
                    PayReg["AJ" + x].Value = PayRegData.Rows[i]["PreviousYTDMCR"]; //date employed
                    PayReg["AK" + x].Value = PayRegData.Rows[i]["PreviousYTDPagibig"]; //date terminated
                    PayReg["AL" + x].Value = PayRegData.Rows[i]["Previous13thNT"]; //cost center
                    PayReg["AM" + x].Value = PayRegData.Rows[i]["Previous13thTX"];
                    PayReg["AN" + x].Text = PayRegData.Rows[i]["PaymentType"].ToString(); //currency
                    PayReg["AO" + x].Text = PayRegData.Rows[i]["BankAcc"].ToString(); //exchange rate
                    PayReg["AP" + x].Text = PayRegData.Rows[i]["BankName"].ToString(); //payment freq
                    PayReg["AQ" + x].Text = PayRegData.Rows[i]["CommentField"].ToString();
                    PayReg["AR" + x].Text = PayRegData.Rows[i]["ErrorField"].ToString();
                    PayReg["AS" + x].Text = PayRegData.Rows[i]["DateEmployed"].ToString();
                    PayReg["AT" + x].Text = PayRegData.Rows[i]["DateTerminated"].ToString();
                    PayReg["AU" + x].Value = PayRegData.Rows[i]["CostCenter"];
                    PayReg["AV" + x].Text = PayRegData.Rows[i]["Currency"].ToString();
                    PayReg["AW" + x].Text = PayRegData.Rows[i]["ExchangeRate"].ToString();
                    PayReg["AX" + x].Text = PayRegData.Rows[i]["PaymentPreq"].ToString();
                    PayReg["AY" + x].Value = PayRegData.Rows[i]["MTDGross"];
                    PayReg["AZ" + x].Value = PayRegData.Rows[i]["MTDBasic"];
                    PayReg["BA" + x].Value = PayRegData.Rows[i]["MTDSSSEmployee"];
                    PayReg["BB" + x].Value = PayRegData.Rows[i]["MTDMCREmployee"];
                    PayReg["BC" + x].Value = PayRegData.Rows[i]["MTDPagIbigEmployee"];
                    PayReg["BD" + x].Value = PayRegData.Rows[i]["MTDSSSEmployer"];
                    PayReg["BE" + x].Value = PayRegData.Rows[i]["MTDMCREmployer"];
                    PayReg["BF" + x].Value = PayRegData.Rows[i]["MTDECEmployer"];
                    PayReg["BG" + x].Value = PayRegData.Rows[i]["MTDPabIbigEmployer"];
                    PayReg["BH" + x].Value = PayRegData.Rows[i]["MTDWHTAX"];
                    PayReg["BI" + x].Value = PayRegData.Rows[i]["MonthlyBasic"];
                    PayReg["BJ" + x].Value = PayRegData.Rows[i]["MonthlyAllow"];
                    PayReg["BK" + x].Value = PayRegData.Rows[i]["MTDNTX"];
                    PayReg["BL" + x].Text = PayRegData.Rows[i]["FROM"].ToString();
                    PayReg["BM" + x].Text = PayRegData.Rows[i]["TO"].ToString();




                }
                #endregion
                TempConnectionStringv4(param.db);
                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@TenantID", mytype = SqlDbType.NVarChar, Value = param.TenantID });
                con.myparameters.Add(new myParameters { ParameterName = "@CutoffStart", mytype = SqlDbType.NVarChar, Value = param.CutoffStart });
                con.myparameters.Add(new myParameters { ParameterName = "@CutoffEnd", mytype = SqlDbType.NVarChar, Value = param.CutoffEnd });
                con.myparameters.Add(new myParameters { ParameterName = "@EmpStatus", mytype = SqlDbType.VarChar, Value = param.EmpStatus });
                con.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.Structured, Value = DTParam });
                DataTable secondSheet = con.GetDataTable("sp_ExportSheet2v5");

                x = 2;
                for (int i = 0; i < secondSheet.Rows.Count; i++, x++)
                {
                    sheet2["A" + x].Text = secondSheet.Rows[i]["PayrollNum"].ToString();
                    sheet2["B" + x].Text = secondSheet.Rows[i]["PayOutDate"].ToString();
                    sheet2["C" + x].Text = secondSheet.Rows[i]["PayCode"].ToString();
                    sheet2["D" + x].Text = secondSheet.Rows[i]["EmployeeID"].ToString();
                    sheet2["E" + x].Text = secondSheet.Rows[i]["LastName"].ToString();
                    sheet2["F" + x].Text = secondSheet.Rows[i]["FirstName"].ToString();
                    sheet2["G" + x].Text = secondSheet.Rows[i]["FileStatus"].ToString();
                    sheet2["H" + x].Text = secondSheet.Rows[i]["TransactionType"].ToString();
                    sheet2["I" + x].Text = secondSheet.Rows[i]["Date"].ToString();
                    sheet2["J" + x].Value = secondSheet.Rows[i]["CostCenter"];
                    sheet2["K" + x].Value = secondSheet.Rows[i]["Days"];
                    sheet2["L" + x].Value = secondSheet.Rows[i]["Hours"];
                    sheet2["M" + x].Value = secondSheet.Rows[i]["Amount"];
                    sheet2["N" + x].Text = secondSheet.Rows[i]["Remark"].ToString();
                    sheet2["O" + x].Text = secondSheet.Rows[i]["FROM"].ToString();
                    sheet2["P" + x].Text = secondSheet.Rows[i]["TO"].ToString();

                }

                TempConnectionStringv4(param.db);
                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@TenantID", mytype = SqlDbType.NVarChar, Value = param.TenantID });
                con.myparameters.Add(new myParameters { ParameterName = "@CutoffStart", mytype = SqlDbType.NVarChar, Value = param.CutoffStart });
                con.myparameters.Add(new myParameters { ParameterName = "@CutoffEnd", mytype = SqlDbType.NVarChar, Value = param.CutoffEnd });
                con.myparameters.Add(new myParameters { ParameterName = "@EmpStatus", mytype = SqlDbType.VarChar, Value = param.EmpStatus });
                con.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.Structured, Value = DTParam });
                DataTable thirdSheet = con.GetDataTable("sp_ExportSheet3v5");

                x = 2;
                for (int i = 0; i < thirdSheet.Rows.Count; i++, x++)
                {
                    sheet3["A" + x].Text = thirdSheet.Rows[i]["PayrollNum"].ToString();
                    sheet3["B" + x].Text = thirdSheet.Rows[i]["PayDate"].ToString();
                    sheet3["C" + x].Text = thirdSheet.Rows[i]["PayCode"].ToString();
                    sheet3["D" + x].Text = thirdSheet.Rows[i]["EmpID"].ToString();
                    sheet3["E" + x].Text = thirdSheet.Rows[i]["LastName"].ToString();
                    sheet3["F" + x].Text = thirdSheet.Rows[i]["FirstName"].ToString();
                    sheet3["G" + x].Text = thirdSheet.Rows[i]["FileStatus"].ToString();
                    sheet3["H" + x].Text = thirdSheet.Rows[i]["OTCode"].ToString();
                    sheet3["I" + x].Text = thirdSheet.Rows[i]["Date"].ToString();
                    sheet3["J" + x].Value = thirdSheet.Rows[i]["CostCenter"];
                    sheet3["K" + x].Value = thirdSheet.Rows[i]["OTRate"];
                    sheet3["L" + x].Value = thirdSheet.Rows[i]["OTHours"];
                    sheet3["M" + x].Value = thirdSheet.Rows[i]["OTMinutes"];
                    sheet3["N" + x].Value = thirdSheet.Rows[i]["OTAmount"];
                    sheet3["O" + x].Text = thirdSheet.Rows[i]["Remark"].ToString();
                    sheet3["P" + x].Text = thirdSheet.Rows[i]["From"].ToString();
                    sheet3["Q" + x].Text = thirdSheet.Rows[i]["To"].ToString();





                }
                TempConnectionStringv4(param.db);
                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@TenantID", mytype = SqlDbType.NVarChar, Value = param.TenantID });
                con.myparameters.Add(new myParameters { ParameterName = "@CutoffStart", mytype = SqlDbType.NVarChar, Value = param.CutoffStart });
                con.myparameters.Add(new myParameters { ParameterName = "@CutoffEnd", mytype = SqlDbType.NVarChar, Value = param.CutoffEnd });
                con.myparameters.Add(new myParameters { ParameterName = "@EmpStatus", mytype = SqlDbType.VarChar, Value = param.EmpStatus });
                con.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.Structured, Value = DTParam });
                DataTable FSheet = con.GetDataTable("sp_ExportSheet4v5");

                x = 2;
                for (int i = 0; i < FSheet.Rows.Count; i++, x++)
                {
                    sheet4["A" + x].Text = FSheet.Rows[i]["PayrollNum"].ToString();
                    sheet4["B" + x].Text = FSheet.Rows[i]["PayDate"].ToString();
                    sheet4["C" + x].Text = FSheet.Rows[i]["PayCode"].ToString();
                    sheet4["D" + x].Text = FSheet.Rows[i]["EmployeeID"].ToString();
                    sheet4["E" + x].Text = FSheet.Rows[i]["LastName"].ToString();
                    sheet4["F" + x].Text = FSheet.Rows[i]["FirstName"].ToString();
                    sheet4["G" + x].Text = FSheet.Rows[i]["FileStatus"].ToString();
                    sheet4["H" + x].Text = FSheet.Rows[i]["IncomeCode"].ToString();
                    sheet4["I" + x].Text = FSheet.Rows[i]["Date"].ToString();
                    sheet4["J" + x].Value = FSheet.Rows[i]["Amount"];
                    sheet4["K" + x].Text = FSheet.Rows[i]["RecurStart"].ToString();
                    sheet4["L" + x].Text = FSheet.Rows[i]["RecurEnd"].ToString();
                    sheet4["M" + x].Text = FSheet.Rows[i]["Freq"].ToString();
                    sheet4["N" + x].Text = FSheet.Rows[i]["Remarks"].ToString();
                    sheet4["O" + x].Text = FSheet.Rows[i]["From"].ToString();
                    sheet4["P" + x].Text = FSheet.Rows[i]["To"].ToString();
                    sheet4["Q" + x].Text = FSheet.Rows[i]["JECostCenter"].ToString();

                }

                TempConnectionStringv4(param.db);
                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@TenantID", mytype = SqlDbType.NVarChar, Value = param.TenantID });
                con.myparameters.Add(new myParameters { ParameterName = "@CutoffStart", mytype = SqlDbType.NVarChar, Value = param.CutoffStart });
                con.myparameters.Add(new myParameters { ParameterName = "@CutoffEnd", mytype = SqlDbType.NVarChar, Value = param.CutoffEnd });
                con.myparameters.Add(new myParameters { ParameterName = "@EmpStatus", mytype = SqlDbType.VarChar, Value = param.EmpStatus });
                con.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.Structured, Value = DTParam });
                DataTable FifthSheet = con.GetDataTable("sp_ExportSheet5v5");

                x = 2;
                for (int i = 0; i < FifthSheet.Rows.Count; i++, x++)
                {
                    sheet5["A" + x].Text = FifthSheet.Rows[i]["PayrollNum"].ToString();
                    sheet5["B" + x].Text = FifthSheet.Rows[i]["PayDate"].ToString();
                    sheet5["C" + x].Text = FifthSheet.Rows[i]["PayCode"].ToString();
                    sheet5["D" + x].Text = FifthSheet.Rows[i]["EmployeeID"].ToString();
                    sheet5["E" + x].Text = FifthSheet.Rows[i]["LastName"].ToString();
                    sheet5["F" + x].Text = FifthSheet.Rows[i]["FirstName"].ToString();
                    sheet5["G" + x].Text = FifthSheet.Rows[i]["FileStatus"].ToString();
                    sheet5["H" + x].Text = FifthSheet.Rows[i]["IncomeCode"].ToString();
                    sheet5["I" + x].Text = FifthSheet.Rows[i]["Date"].ToString();
                    sheet5["J" + x].Value = FifthSheet.Rows[i]["Amount"];
                    sheet5["K" + x].Text = FifthSheet.Rows[i]["RecurStart"].ToString();
                    sheet5["L" + x].Text = FifthSheet.Rows[i]["RecurEnd"].ToString();
                    sheet5["M" + x].Text = FifthSheet.Rows[i]["Freq"].ToString();
                    sheet5["N" + x].Text = FifthSheet.Rows[i]["Remarks"].ToString();
                    sheet5["O" + x].Text = FifthSheet.Rows[i]["From"].ToString();
                    sheet5["P" + x].Text = FifthSheet.Rows[i]["To"].ToString();
                    sheet5["Q" + x].Text = FifthSheet.Rows[i]["JECostCenter"].ToString();



                }
                TempConnectionStringv4(param.db);
                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@TenantID", mytype = SqlDbType.NVarChar, Value = param.TenantID });
                con.myparameters.Add(new myParameters { ParameterName = "@CutoffStart", mytype = SqlDbType.NVarChar, Value = param.CutoffStart });
                con.myparameters.Add(new myParameters { ParameterName = "@CutoffEnd", mytype = SqlDbType.NVarChar, Value = param.CutoffEnd });
                con.myparameters.Add(new myParameters { ParameterName = "@EmpStatus", mytype = SqlDbType.VarChar, Value = param.EmpStatus });
                con.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.Structured, Value = DTParam });
                DataTable SixSheet = con.GetDataTable("sp_ExportSheet6v5");

                x = 2;
                for (int i = 0; i < SixSheet.Rows.Count; i++, x++)
                {
                    sheet6["A" + x].Text = SixSheet.Rows[i]["PayrollNum"].ToString();
                    sheet6["B" + x].Text = SixSheet.Rows[i]["PayDate"].ToString();
                    sheet6["C" + x].Text = SixSheet.Rows[i]["PayCode"].ToString();
                    sheet6["D" + x].Text = SixSheet.Rows[i]["EmployeeID"].ToString();
                    sheet6["E" + x].Text = SixSheet.Rows[i]["LastName"].ToString();
                    sheet6["F" + x].Text = SixSheet.Rows[i]["FirstName"].ToString();
                    sheet6["G" + x].Text = SixSheet.Rows[i]["FileStatus"].ToString();
                    sheet6["H" + x].Text = SixSheet.Rows[i]["LoanCode"].ToString();
                    sheet6["I" + x].Text = SixSheet.Rows[i]["LoanDateStart"].ToString();
                    sheet6["J" + x].Value = SixSheet.Rows[i]["LoanPrincipal"];
                    sheet6["K" + x].Value = SixSheet.Rows[i]["LoanAmount"];
                    sheet6["L" + x].Value = SixSheet.Rows[i]["LoanPayments"];
                    sheet6["M" + x].Value = SixSheet.Rows[i]["LoanBalance"];
                    sheet6["N" + x].Value = SixSheet.Rows[i]["LoanDeductionAMT"];
                    sheet6["O" + x].Text = SixSheet.Rows[i]["LoanDeductionFreq"].ToString();
                    sheet6["P" + x].Text = SixSheet.Rows[i]["LoanStatus"].ToString();
                    sheet6["Q" + x].Text = SixSheet.Rows[i]["LoanDateGranted"].ToString();
                    sheet6["R" + x].Text = SixSheet.Rows[i]["From"].ToString();
                    sheet6["S" + x].Text = SixSheet.Rows[i]["To"].ToString();
                    sheet6["T" + x].Text = SixSheet.Rows[i]["JECostCenter"].ToString();


                }

                TempConnectionStringv4(param.db);
                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@TenantID", mytype = SqlDbType.NVarChar, Value = param.TenantID });
                con.myparameters.Add(new myParameters { ParameterName = "@CutoffStart", mytype = SqlDbType.NVarChar, Value = param.CutoffStart });
                con.myparameters.Add(new myParameters { ParameterName = "@CutoffEnd", mytype = SqlDbType.NVarChar, Value = param.CutoffEnd });
                con.myparameters.Add(new myParameters { ParameterName = "@EmpStatus", mytype = SqlDbType.VarChar, Value = param.EmpStatus });
                con.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.Structured, Value = DTParam });
                DataTable SevenSheet = con.GetDataTable("sp_Exportsheet7v5");

                x = 2;
                for (int i = 0; i < SevenSheet.Rows.Count; i++, x++)
                {
                    sheet7["A" + x].Text = SevenSheet.Rows[i]["PayrollNum"].ToString();
                    sheet7["B" + x].Text = SevenSheet.Rows[i]["PayDate"].ToString();
                    sheet7["C" + x].Text = SevenSheet.Rows[i]["PayCode"].ToString();
                    sheet7["D" + x].Text = SevenSheet.Rows[i]["EmployeeID"].ToString();
                    sheet7["E" + x].Text = SevenSheet.Rows[i]["LastName"].ToString();
                    sheet7["F" + x].Text = SevenSheet.Rows[i]["FirstName"].ToString();
                    sheet7["G" + x].Text = SevenSheet.Rows[i]["FileStatus"].ToString();
                    sheet7["H" + x].Text = SevenSheet.Rows[i]["DeductionCode"].ToString();
                    sheet7["I" + x].Text = SevenSheet.Rows[i]["Date"].ToString();
                    sheet7["J" + x].Value = SevenSheet.Rows[i]["DeductionAmount"];
                    sheet7["K" + x].Text = SevenSheet.Rows[i]["DeductionFreq"].ToString();
                    sheet7["L" + x].Text = SevenSheet.Rows[i]["LastDeductionDate"].ToString();
                    sheet7["M" + x].Text = SevenSheet.Rows[i]["From"].ToString();
                    sheet7["N" + x].Text = SevenSheet.Rows[i]["To"].ToString();
                    sheet7["O" + x].Text = SevenSheet.Rows[i]["JECostCenter"].ToString();



                }
                HttpResponse httpResponse = System.Web.HttpContext.Current.Response;

                httpResponse.Clear();
                httpResponse.ContentType = "Application/x-msexcel";
                httpResponse.AddHeader("Content-Disposition", String.Format("attachment; filename={0}", filename));
                string newDIR = HttpContext.Current.Server.MapPath("~/sFTP/PayrollOutsourcing/");
                string onlineDIR = "https://ws.durusthr.com/ILM_WS_Live/sFTP/PayrollOutsourcing/";
                //filename = filename.Replace(',', '_');
                //filename = filename.Replace(" ", String.Empty);
                workbook.Save(newDIR + filename);
                stream.Close();
                //return "Success";

                if (PayRegData.Rows.Count > 0)
                {
                    return onlineDIR + filename;
                }
                else
                {

                    return "No Rows";
                }
            }
            catch (Exception e)
            {
                stream.Close();
                return "Error: " + e.ToString();
            }
        }
        [HttpPost]
        [Route("PO_GetLogUser")]
        public string GetEmployees(LogUserParam param)
        {
            try
            {
                TempConnectionStringv4(param.db);
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.VarChar, Value = param.EmpID });
                con.myparameters.Add(new myParameters { ParameterName = "@tenantID", mytype = SqlDbType.VarChar, Value = param.TenantID });
                return con.ExecuteScalar("sp_getLogName");
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }//REN
        [HttpPost]
        [Route("PO_getIncomeCode")]
        public List<InCode> GetIncomeCode(IncomeCodeParam param)
        {
            List<InCode> List = new List<InCode>();
            TempConnectionStringv2();
            Connection con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@ClientCode", mytype = SqlDbType.VarChar, Value = param.ClientCode });
            DataTable DT = con.GetDataTable("sp_getIncomeCode");
            foreach (DataRow row in DT.Rows)
            {
                InCode E = new InCode()
                {
                    IncomeCode = row["IncomeCode"].ToString(),
                };
                List.Add(E);
            }
            return List;
        }
        [HttpPost]
        [Route("PO_getCode")]
        public List<IncomeCode> GetCode(IncomeCodeParam param)
        {
            List<IncomeCode> List = new List<IncomeCode>();
            TempConnectionStringv4(param.db);
            Connection con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@ClientCode", mytype = SqlDbType.VarChar, Value = param.ClientCode });
            con.myparameters.Add(new myParameters { ParameterName = "@codetype", mytype = SqlDbType.VarChar, Value = param.CodeType });
            DataTable DT = con.GetDataTable("sp_getCode");
            foreach (DataRow row in DT.Rows)
            {
                IncomeCode E = new IncomeCode()
                {
                    Code = row[0].ToString(),
                };
                List.Add(E);
            }
            return List;
        }
        [HttpPost]
        [Route("PO_getDeductionCode")]
        public List<DedCode> GetDeductionCode(IncomeCodeParam param)
        {
            List<DedCode> List = new List<DedCode>();
            TempConnectionStringv2();
            Connection con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@ClientCode", mytype = SqlDbType.VarChar, Value = param.ClientCode });
            DataTable DT = con.GetDataTable("sp_getDeductionCode");
            foreach (DataRow row in DT.Rows)
            {
                DedCode E = new DedCode()
                {
                    DeductionCode = row["DeductionCode"].ToString(),
                };
                List.Add(E);
            }
            return List;
        }

        [Route("Generate2316v2")]
        public string MergePDFv2(pdf SF)
        {
            Update2316 Up = new Update2316();
            Up.EmpID = SF.empID;
            Up.TenantID = SF.TenantID;
            Up.Year = SF.Year;
            Up.db = SF.db;
            UPdate2316(Up);
            //Response.Redirect("~/pdf/BIR19021.pdf"); // view pdf
            string yr = DateTime.Now.ToString("yyyy");
            //         string path = HttpContext.Current.Server.MapPath("pdf");



            //DataTable DTSI = new DataTable();
            //DTSI.Columns.AddRange(new DataColumn[1] { new DataColumn("StartingID") });
            //for (int i = 0; i < SF.empID.Length; i++)
            //{
            //    DTSI.Rows.Add(SF.empID[i]);
            //}


            TempConnectionStringv4(SF.db);
            Connection con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@TenantID", mytype = SqlDbType.VarChar, Value = SF.TenantID });
            con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = SF.empID });
            con.myparameters.Add(new myParameters { ParameterName = "@Year", mytype = SqlDbType.VarChar, Value = SF.Year });
            con.myparameters.Add(new myParameters { ParameterName = "@Type", mytype = SqlDbType.VarChar, Value = SF.type });
            DataTable DT = con.GetDataTable("sp_ExportPDF2316v2");
            string year = SF.Year, from, to, tin, zipcode, ERTIN, ERZIP, empName = "", EEDOB, EENumber, ERName, PERZIP;
            var pdfReader1 = new iTextSharp.text.pdf.PdfReader(HttpContext.Current.Server.MapPath("pdf") + "/PayrollOutSourcing/2316A.pdf");
            AcroFields af = pdfReader1.AcroFields;
            string datebuild = DateTime.Now.ToString("MMddyyyy");
            string CompanyCode = "";
            string empid = "";
            string lastName = "";
            string tinFileName = "";


            foreach (DataRow row in DT.Rows)
            {
                empid = row["EmpID"].ToString();
                CompanyCode = row["EmployerName"].ToString();
                lastName = row["LastName"].ToString();
                tinFileName = row["TIN"].ToString().Replace("-", ""); ;
            }
            // string empids = uProfile.NTID;


            // Template file path
            string formFile = HttpContext.Current.Server.MapPath("pdf") + "/PayrollOutSourcing/2316A.pdf";


            // Output file path
            string newFile = HttpContext.Current.Server.MapPath("pdf") + "/PayrollOutSourcing/" + lastName + "_" + tinFileName + "0000" + "_" + "12312020.pdf";


            // read the template file
            iTextSharp.text.pdf.PdfReader reader = new iTextSharp.text.pdf.PdfReader(formFile);
            //  reader.Close();

            // instantiate PDFStamper object
            // The Output file will be created from template file and edited by the PDFStamper
            PdfStamper stamper = new PdfStamper(reader, new FileStream(
                        newFile, FileMode.Create));

            // Object to deal with the Output file's textfields
            AcroFields fields = stamper.AcroFields;



            foreach (DataRow row in DT.Rows)
            {
                year = row["ForYear"].ToString();



                //  int len = year.Length;



                for (int x = 0; x < year.Length; x++)
                {
                    fields.SetField("year" + (x + 1), year[x].ToString());
                }



                from = row["FromPeriod"].ToString();
                for (int x = 0; x < from.Length; x++)
                {
                    fields.SetField("From" + (x + 1), from[x].ToString());
                }



                to = row["ToPeriod"].ToString();
                for (int x = 0; x < to.Length; x++)
                {
                    fields.SetField("To" + (x + 1), to[x].ToString());
                }



                tin = row["TIN"].ToString();
                for (int x = 0; x < tin.Length; x++)
                {
                    fields.SetField("tin" + (x + 1), tin[x].ToString());
                }

                EEDOB = row["DOB"].ToString();
                for (int x = 0; x < EEDOB.Length; x++)
                {
                    fields.SetField("bdate" + (x + 1), EEDOB[x].ToString());
                }

                EENumber = row["EENumber"].ToString();
                for (int x = 0; x < EENumber.Length; x++)
                {
                    fields.SetField("cNum" + (x + 1), EENumber[x].ToString());
                }

                empName = row["EmpName"].ToString();

                fields.SetField("employeeName", row["EmpName"].ToString());

                fields.SetField("registeredAddress", row["RegisteredAddress"].ToString());

                zipcode = row["ZipCode"].ToString();
                for (int x = 0; x < zipcode.Length; x++)
                {
                    fields.SetField("Zipcode6A" + (x + 1), zipcode[x].ToString());
                }


                fields.SetField("EmployersName", row["ERName"].ToString());
                fields.SetField("ERRegisteredAddress", row["ERAddress"].ToString());
                //fields.SetField("ERRegisteredAddress", row["RegisteredAddress"].ToString());


                if (row["EmployerType"].ToString() == "MainER")
                {

                    fields.SetField("untitled48", "Yes"); // checkbox options : "Yes":"No", true
                }
                else
                {
                    fields.SetField("untitled48", "No"); // checkbox options : "Yes":"No", true
                }
                if (row["EmployerType"].ToString() == "2ndER")
                {

                    fields.SetField("untitled49", "Yes"); // checkbox options : "Yes":"No", true
                }
                else
                {
                    fields.SetField("untitled49", "No"); // checkbox options : "Yes":"No", true
                }



                ERTIN = row["ERTIN"].ToString();
                for (int x = 0; x < ERTIN.Length; x++)
                {
                    fields.SetField("ETIN" + (x + 1), ERTIN[x].ToString());
                }


                //  int len = year.Length;




                fields.SetField("Gross", row["GrossIncome"].ToString());


                ERZIP = row["ERZIP"].ToString();
                for (int x = 0; x < ERZIP.Length; x++)
                {
                    fields.SetField("14A" + (x + 1), ERZIP[x].ToString());
                }



                fields.SetField("PEmployersName", row["PERName"].ToString());
                fields.SetField("PRegisteredAddress", row["PERAddress"].ToString());
                PERZIP = row["PERZIP"].ToString();
                for (int x = 0; x < PERZIP.Length; x++)
                {
                    fields.SetField("18A" + (x + 1), PERZIP[x].ToString());
                }



                fields.SetField("Gross", row["GrossIncome"].ToString());
                fields.SetField("Income", row["TotalNonTaxablePresent"].ToString());
                fields.SetField("Compensation", row["TaxableIncomePresent"].ToString());
                fields.SetField("Taxable", row["TaxableIncomePrevious"].ToString());
                fields.SetField("GrossTaxable", row["GrossTaxable"].ToString());
                fields.SetField("Tax_Due", row["TaxDue"].ToString());
                fields.SetField("PEmployer", row["TaxWithheldPresent"].ToString());
                fields.SetField("PrevEmployer", row["TaxWithheldPrevious"].ToString());
                fields.SetField("Witheld", row["TotalTaxWithheld"].ToString());
                fields.SetField("untitled2", row["BasicSalaryMWE"].ToString());
                fields.SetField("holidayPay", row["HolidayPayMWE"].ToString());



                fields.SetField("overtimePay", row["OvertimePayMWE"].ToString());
                fields.SetField("nightDiff", row["NightDiffPayMWE"].ToString());
                fields.SetField("HazardPayMWE", row["hazardPay"].ToString());
                fields.SetField("13thMonthPay", row["_13thMonthOtherBen"].ToString());
                fields.SetField("Deminisis", row["DeMinimis"].ToString());
                fields.SetField("unionDues", row["SSSGSISEmployee"].ToString());
                fields.SetField("compensation", row["SalariesOther"].ToString());
                fields.SetField("totalNonTaxable", row["TotalNonTaxableExempt"].ToString());
                fields.SetField("basicSalary", row["BasicSalary"].ToString());



                fields.SetField("representation", row["Representation"].ToString());
                fields.SetField("transportation", row["Transportation"].ToString());
                fields.SetField("COLA", row["COLA"].ToString());
                fields.SetField("FHA", row["FixedHousing"].ToString());
                fields.SetField("42A", row["Others42a"].ToString());
                fields.SetField("42B", row["Others42b"].ToString());
                fields.SetField("commission", row["Commission"].ToString());
                fields.SetField("profitSharing", row["ProfitSharing"].ToString());
                fields.SetField("directorFees", row["Fees"].ToString());
                fields.SetField("taxable13month", row["Taxable13thMonth"].ToString());
                fields.SetField("hazardPay2", row["HazardPay"].ToString());
                fields.SetField("overtimepay2", row["OvertimePay"].ToString());
                fields.SetField("49A", row["Others49"].ToString());
                fields.SetField("taxableIncome", row["TotalTaxableCompensation"].ToString());

                fields.SetField("AgentName", row["PresentEmployeer"].ToString());
                fields.SetField("PresentEmploterPrintedName", row["PresentEmployeer"].ToString());

                fields.SetField("EmployeePrintedName", row["EmpName"].ToString());
                fields.SetField("EmployeeSigPrintedName", row["EmpName"].ToString());
            }
            stamper.FormFlattening = true;
            stamper.Close();   //CompanyCode_2316_EMPID_YYYY
            string filename = lastName + "_" + tinFileName + "0000" + "_" + "12312020.pdf";
            string onlineDIR = "http://ws.durusthr.com/ILM_WS_Live/pdf/PayrollOutSourcing/";
            if (DT.Rows.Count > 0)
            {
                return onlineDIR + filename;
            }
            else
            {
                return "No Rows";
            }
        }

        public DataTable GetCompanySignatory(string Company, string db, string ID)
        {
            TempConnectionStringv4(db);
            Connection con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@TenantID", mytype = SqlDbType.VarChar, Value = Company });
            con.myparameters.Add(new myParameters { ParameterName = "@ReportID", mytype = SqlDbType.VarChar, Value = ID });
            DataTable dt = con.GetDataTable("sp_getSignatory");
            return dt;
        }

        [Route("Generate2316")]
        public string MergePDF(pdf SF)
        {
            Update2316 Up = new Update2316();
            Up.EmpID = SF.empID;
            Up.TenantID = SF.TenantID;
            Up.Year = SF.Year;
            Up.db = SF.db;
            UPdate2316(Up);
            //Response.Redirect("~/pdf/BIR19021.pdf"); // view pdf
            string yr = DateTime.Now.ToString("yyyy");
            //         string path = HttpContext.Current.Server.MapPath("pdf");



            //DataTable DTSI = new DataTable();
            //DTSI.Columns.AddRange(new DataColumn[1] { new DataColumn("StartingID") });
            //for (int i = 0; i < SF.empID.Length; i++)
            //{
            //    DTSI.Rows.Add(SF.empID[i]);
            //}


            TempConnectionStringv4(SF.db);
            Connection con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@TenantID", mytype = SqlDbType.VarChar, Value = SF.TenantID });
            con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = SF.empID });
            con.myparameters.Add(new myParameters { ParameterName = "@Year", mytype = SqlDbType.VarChar, Value = SF.Year });
            con.myparameters.Add(new myParameters { ParameterName = "@Type", mytype = SqlDbType.VarChar, Value = SF.type });
            DataTable DT = con.GetDataTable("sp_ExportPDF");
            DataTable signatory = GetCompanySignatory(SF.TenantID, SF.db, "37");

            string year = SF.Year, from, to, tin, zipcode, ERTIN, ERZIP, empName = "", EEDOB, EENumber, ERName, PERZIP;
            var pdfReader1 = new iTextSharp.text.pdf.PdfReader(HttpContext.Current.Server.MapPath("pdf") + "/PayrollOutSourcing/2316A.pdf");
            AcroFields af = pdfReader1.AcroFields;
            string datebuild = DateTime.Now.ToString("MMddyyyy");
            string CompanyCode = "";
            string empid = "";
            string _lastname = "";
            string _tin = "";
            string _enddate = "";



            foreach (DataRow row in DT.Rows)
            {
                empid = row["EmpID"].ToString();
                CompanyCode = row["EmployerName"].ToString();
                _lastname = row["LastName"].ToString();
                _tin = row["TIN"].ToString();
                _enddate = row["EndDate"].ToString();
            }
            // string empids = uProfile.NTID;


            // Template file path
            string formFile = HttpContext.Current.Server.MapPath("pdf") + "/PayrollOutSourcing/2316A.pdf";


            // Output file path LASTNAME_TIN00000_ENDDATE
            string newPath = _lastname + "_" + _tin + "0000_" + _enddate + ".pdf"; // added 01-25-2022
            //old-path CompanyCode + "_2316_" + empid + "_" + year + ".pdf
            string newFile = HttpContext.Current.Server.MapPath("pdf") + "/PayrollOutSourcing/" + newPath;


            // read the template file
            iTextSharp.text.pdf.PdfReader reader = new iTextSharp.text.pdf.PdfReader(formFile);
            //  reader.Close();

            // instantiate PDFStamper object
            // The Output file will be created from template file and edited by the PDFStamper
            PdfStamper stamper = new PdfStamper(reader, new FileStream(
                        newFile, FileMode.Create));

            // Object to deal with the Output file's textfields
            AcroFields fields = stamper.AcroFields;



            foreach (DataRow row in DT.Rows)
            {
                year = row["ForYear"].ToString();



                //  int len = year.Length;



                for (int x = 0; x < year.Length; x++)
                {
                    fields.SetField("year" + (x + 1), year[x].ToString());
                }



                from = row["FromPeriod"].ToString();
                for (int x = 0; x < from.Length; x++)
                {
                    fields.SetField("From" + (x + 1), from[x].ToString());
                }



                to = row["ToPeriod"].ToString();
                for (int x = 0; x < to.Length; x++)
                {
                    fields.SetField("To" + (x + 1), to[x].ToString());
                }



                tin = row["TIN"].ToString();
                for (int x = 0; x < tin.Length; x++)
                {
                    fields.SetField("tin" + (x + 1), tin[x].ToString());
                }

                EEDOB = row["DOB"].ToString();
                for (int x = 0; x < EEDOB.Length; x++)
                {
                    fields.SetField("bdate" + (x + 1), EEDOB[x].ToString());
                }

                EENumber = row["EENumber"].ToString();
                for (int x = 0; x < EENumber.Length; x++)
                {
                    fields.SetField("cNum" + (x + 1), EENumber[x].ToString());
                }

                empName = row["EmpName"].ToString();

                fields.SetField("employeeName", row["EmpName"].ToString());

                fields.SetField("registeredAddress", row["RegisteredAddress"].ToString());

                zipcode = row["ZipCode"].ToString();
                for (int x = 0; x < zipcode.Length; x++)
                {
                    fields.SetField("Zipcode6A" + (x + 1), zipcode[x].ToString());
                }


                fields.SetField("EmployersName", row["ERName"].ToString());
                fields.SetField("ERRegisteredAddress", row["ERAddress"].ToString());
                //fields.SetField("ERRegisteredAddress", row["RegisteredAddress"].ToString());


                if (row["EmployerType"].ToString() == "MainER")
                {

                    fields.SetField("untitled48", "Yes"); // checkbox options : "Yes":"No", true
                }
                else
                {
                    fields.SetField("untitled48", "No"); // checkbox options : "Yes":"No", true
                }
                if (row["EmployerType"].ToString() == "2ndER")
                {

                    fields.SetField("untitled49", "Yes"); // checkbox options : "Yes":"No", true
                }
                else
                {
                    fields.SetField("untitled49", "No"); // checkbox options : "Yes":"No", true
                }



                ERTIN = row["ERTIN"].ToString();
                for (int x = 0; x < ERTIN.Length; x++)
                {
                    fields.SetField("ETIN" + (x + 1), ERTIN[x].ToString());
                }


                //  int len = year.Length;




                fields.SetField("Gross", row["GrossIncome"].ToString());


                ERZIP = row["ERZIP"].ToString();
                for (int x = 0; x < ERZIP.Length; x++)
                {
                    fields.SetField("14A" + (x + 1), ERZIP[x].ToString());
                }



                fields.SetField("PEmployersName", row["PERName"].ToString());
                fields.SetField("PRegisteredAddress", row["PERAddress"].ToString());
                PERZIP = row["PERZIP"].ToString();
                for (int x = 0; x < PERZIP.Length; x++)
                {
                    fields.SetField("18A" + (x + 1), PERZIP[x].ToString());
                }



                fields.SetField("Gross", row["GrossIncome"].ToString());
                fields.SetField("Income", row["TotalNonTaxablePresent"].ToString());
                fields.SetField("Compensation", row["TaxableIncomePresent"].ToString());
                fields.SetField("Taxable", row["TaxableIncomePrevious"].ToString());
                fields.SetField("GrossTaxable", row["GrossTaxable"].ToString());
                fields.SetField("Tax_Due", row["TaxDue"].ToString());
                fields.SetField("PEmployer", row["TaxWithheldPresent"].ToString());
                fields.SetField("PrevEmployer", row["TaxWithheldPrevious"].ToString());
                fields.SetField("Witheld", row["TotalTaxWithheld"].ToString());
                fields.SetField("untitled2", row["BasicSalaryMWE"].ToString());
                fields.SetField("holidayPay", row["HolidayPayMWE"].ToString());



                fields.SetField("overtimePay", row["OvertimePayMWE"].ToString());
                fields.SetField("nightDiff", row["NightDiffPayMWE"].ToString());
                fields.SetField("HazardPayMWE", row["hazardPay"].ToString());
                fields.SetField("13thMonthPay", row["_13thMonthOtherBen"].ToString());
                fields.SetField("Deminisis", row["DeMinimis"].ToString());
                fields.SetField("unionDues", row["SSSGSISEmployee"].ToString());
                fields.SetField("compensation", row["SalariesOther"].ToString());
                fields.SetField("totalNonTaxable", row["TotalNonTaxableExempt"].ToString());
                fields.SetField("basicSalary", row["BasicSalary"].ToString());



                fields.SetField("representation", row["Representation"].ToString());
                fields.SetField("transportation", row["Transportation"].ToString());
                fields.SetField("COLA", row["COLA"].ToString());
                fields.SetField("FHA", row["FixedHousing"].ToString());
                fields.SetField("42A", "");
                fields.SetField("42A2", row["Others42a"].ToString());
                fields.SetField("42B", "");
                fields.SetField("42B2", row["Others42b"].ToString());
                fields.SetField("commission", row["Commission"].ToString());
                fields.SetField("profitSharing", row["ProfitSharing"].ToString());
                fields.SetField("directorFees", row["Fees"].ToString());
                fields.SetField("taxable13month", row["Taxable13thMonth"].ToString());
                fields.SetField("hazardPay2", row["HazardPay"].ToString());
                fields.SetField("overtimepay2", row["OvertimePay"].ToString());
                fields.SetField("49A", row["Others49"].ToString());
                fields.SetField("taxableIncome", row["TotalTaxableCompensation"].ToString());

                fields.SetField("AgentName", row["PresentEmployeer"].ToString());
                fields.SetField("PresentEmploterPrintedName", row["PresentEmployeer"].ToString());

                fields.SetField("EmployeePrintedName", row["EmpName"].ToString());
                fields.SetField("EmployeeSigPrintedName", row["EmpName"].ToString());

                #region For Signatories
                Byte[] ID1Bit = null;
                Image imgSign1 = null, imgSign2 = null;
                var ID1ContentByte = stamper.GetOverContent(1); // 3 represents page number
                var ID2ContentByte = stamper.GetOverContent(1);
                AcroFields.FieldPosition signature1 = fields.GetFieldPositions("signature1a")[0];
                AcroFields.FieldPosition signature2 = fields.GetFieldPositions("signature1b")[0];
                Rectangle rect1 = signature1.position, rect2 = signature2.position;
                float posX1 = rect1.Left, posY1 = rect1.Bottom;
                float posX2 = rect2.Left, posY2 = rect2.Bottom;
                try
                {
                    string imagePath = signatory.Rows[0][3].ToString() == null ? "" : signatory.Rows[0][3].ToString();
                    string img1url = HttpContext.Current.Server.MapPath("~/Files/PayrollOutSourcing/Signature/") + imagePath;
                    using (var webClient = new WebClient())
                    {
                        ID1Bit = webClient.DownloadData(img1url);
                    }
                    imgSign1 = Image.GetInstance(ID1Bit); // convert to byte array
                    imgSign2 = Image.GetInstance(ID1Bit); // convert to byte array
                    imgSign1.SetAbsolutePosition(posX1, posY1); // set position inside field to place picture 
                    imgSign2.SetAbsolutePosition(posX2, posY2); // set position inside field to place picture 
                    imgSign1.ScaleAbsolute(rect1); // set absolute size of image as per size of field itself
                    imgSign2.ScaleAbsolute(rect2); // set absolute size of image as per size of field itself
                    ID1ContentByte.AddImage(imgSign1); // add image to field
                    ID2ContentByte.AddImage(imgSign2); // add image to field
                }
                catch (Exception)
                {

                }
                #endregion
            }
            stamper.FormFlattening = true;
            stamper.Close();   //CompanyCode_2316_EMPID_YYYY
            string filename = newPath;
            string onlineDIR = "http://ws.durusthr.com/ILM_WS_Live/pdf/PayrollOutSourcing/";
            if (DT.Rows.Count > 0)
            {
                return onlineDIR + filename;
            }
            else
            {
                return "No Rows";
            }
        }
        public string UPdate2316(Update2316 param)
        {
            try
            {
                TempConnectionStringv4(param.db);
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = param.EmpID });
                con.myparameters.Add(new myParameters { ParameterName = "@tenantID", mytype = SqlDbType.VarChar, Value = param.TenantID });
                string userID = con.ExecuteScalar("sp_GetUserID_Payslip");

                DataTable DT = new DataTable();
                DT.Columns.AddRange(new DataColumn[1]
                {
                    new DataColumn("EmpID")
                });
                DT.Rows.Add(userID);

                TempConnectionStringv4(param.db);
                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@UserId", mytype = SqlDbType.Structured, Value = DT });
                con.myparameters.Add(new myParameters { ParameterName = "@ForYear", mytype = SqlDbType.VarChar, Value = param.Year });
                con.ExecuteNonQuery("sp_ComputeActiveEmployeeITR_TX");
                return "Success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }


        [HttpPost]
        [Route("GetPayoutDates")]
        public List<PayoutDates> PayoutDates(PayoutDatesParam param)
        {
            List<PayoutDates> List = new List<PayoutDates>();
            TempConnectionStringv4(param.db);
            Connection con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@tenantid", mytype = SqlDbType.VarChar, Value = param.TenantID });
            con.myparameters.Add(new myParameters { ParameterName = "@year", mytype = SqlDbType.VarChar, Value = param.Year });
            con.myparameters.Add(new myParameters { ParameterName = "@paysliptype", mytype = SqlDbType.VarChar, Value = param.PayslipType });
            DataTable DT = con.GetDataTable("sp_getpayoutdates");
            foreach (DataRow row in DT.Rows)
            {
                PayoutDates P = new PayoutDates()
                {
                    PayoutDate = row["PayOutDate"].ToString()
                };
                List.Add(P);
            }
            return List;
        }
        [HttpPost]
        [Route("GetPayoutDatesv2")]
        public List<PayoutDates> PayoutDatesv2(PayoutDatesParam param)
        {
            List<PayoutDates> List = new List<PayoutDates>();
            TempConnectionStringv4(param.db);
            Connection con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@tenantid", mytype = SqlDbType.VarChar, Value = param.TenantID });
            con.myparameters.Add(new myParameters { ParameterName = "@year", mytype = SqlDbType.VarChar, Value = param.Year });
            con.myparameters.Add(new myParameters { ParameterName = "@paysliptype", mytype = SqlDbType.VarChar, Value = param.PayslipType });
            con.myparameters.Add(new myParameters { ParameterName = "@payOutType", mytype = SqlDbType.VarChar, Value = param.PayOutType });
            DataTable DT = con.GetDataTable("sp_getpayoutdatesv2");
            foreach (DataRow row in DT.Rows)
            {
                PayoutDates P = new PayoutDates()
                {
                    PayoutDate = row["PayOutDate"].ToString()
                };
                List.Add(P);
            }
            return List;
        }

        [HttpPost]
        [Route("GetPayoutDates_PerEmp")]
        public List<PayoutDates> GetPayoutDates_PerEmp(PayoutDatesParamV2 param)
        {
            List<PayoutDates> List = new List<PayoutDates>();
            TempConnectionStringv4(param.db);
            Connection con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@tenantid", mytype = SqlDbType.VarChar, Value = param.TenantID });
            con.myparameters.Add(new myParameters { ParameterName = "@year", mytype = SqlDbType.VarChar, Value = param.Year });
            con.myparameters.Add(new myParameters { ParameterName = "@paysliptype", mytype = SqlDbType.VarChar, Value = param.PayslipType });
            con.myparameters.Add(new myParameters { ParameterName = "@empID ", mytype = SqlDbType.VarChar, Value = param.EmpID });
            DataTable DT = con.GetDataTable("sp_getPayOutDatePerEmployee");
            foreach (DataRow row in DT.Rows)
            {
                PayoutDates P = new PayoutDates()
                {
                    PayoutDate = row["PayOutDate"].ToString()
                };
                List.Add(P);
            }
            return List;
        }

        [HttpPost]
        [Route("ws_getRateTypes")]
        public List<RateTypesList> getRateTypes(PayoutDatesParam param)
        {
            List<RateTypesList> List = new List<RateTypesList>();
            TempConnectionStringv4(param.db);
            Connection con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@tenantid", mytype = SqlDbType.VarChar, Value = param.TenantID });

            DataTable DT = con.GetDataTable("sp_getRateTypes");
            foreach (DataRow row in DT.Rows)
            {
                RateTypesList P = new RateTypesList()
                {
                    id = row["id"].ToString(),
                    rateName = row["rateName"].ToString(),
                };
                List.Add(P);
            }
            return List;
        }

        [HttpPost]
        [Route("ws_addRateTypes")]
        public string addRateTypes(PayoutDatesParam param)
        {
            try
            {
                List<RateTypesList> List = new List<RateTypesList>();
                TempConnectionStringv4(param.db);
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@tenantid", mytype = SqlDbType.VarChar, Value = param.TenantID });
                con.myparameters.Add(new myParameters { ParameterName = "@rateName", mytype = SqlDbType.VarChar, Value = param.rateName });
                con.myparameters.Add(new myParameters { ParameterName = "@loginid", mytype = SqlDbType.VarChar, Value = param.loginID });
                con.ExecuteNonQuery("sp_addRateTypes");
                return "Success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }

        }

        [HttpPost]
        [Route("ws_getRates")]
        public List<listGetRates> listGetRates(PayoutDatesParam param)
        {
            List<listGetRates> List = new List<listGetRates>();
            TempConnectionStringv4(param.db);
            Connection con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@tenantid", mytype = SqlDbType.VarChar, Value = param.TenantID });
            con.myparameters.Add(new myParameters { ParameterName = "@rateTypeID", mytype = SqlDbType.VarChar, Value = param.rateTypeID });
            DataTable DT = con.GetDataTable("sp_getRates");
            foreach (DataRow row in DT.Rows)
            {
                listGetRates P = new listGetRates()
                {
                    OvertimeID = row["OvertimeID"].ToString(),
                    OTDesc = row["OTDesc"].ToString(),
                    OTRate = row["OTRate"].ToString()
                };
                List.Add(P);
            }
            return List;
        }
        [HttpPost]
        [Route("ws_updateRates")]
        public string updateRates(PayoutDatesParam param)
        {
            try
            {
                DataTable dt = new DataTable();
                dt.Columns.AddRange(new DataColumn[2] { new DataColumn("NewValue"), new DataColumn("rate") });
                for (int i = 0; i < param.NewValue.Length; i++)
                {
                    dt.Rows.Add(param.NewValue[i], param.OvertimeID[i]);

                }
                TempConnectionStringv4(param.db);
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@tenantID", mytype = SqlDbType.VarChar, Value = param.TenantID });
                con.myparameters.Add(new myParameters { ParameterName = "@rateTypeID", mytype = SqlDbType.VarChar, Value = param.rateTypeID });
                con.myparameters.Add(new myParameters { ParameterName = "@Data", mytype = SqlDbType.Structured, Value = dt });
                con.ExecuteNonQuery("sp_updateRates");
                return "Success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }

        }

        [HttpPost]
        [Route("ws_deleteRateType")]
        public string deleteRateTypes(PayoutDatesParam param)
        {
            try
            {
                List<RateTypesList> List = new List<RateTypesList>();
                TempConnectionStringv4(param.db);
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@tenantid", mytype = SqlDbType.VarChar, Value = param.TenantID });
                con.myparameters.Add(new myParameters { ParameterName = "@rateid", mytype = SqlDbType.VarChar, Value = param.rateTypeID });
                con.ExecuteNonQuery("sp_deleteRateTypes");
                return "Success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }

        }

        [HttpPost]
        [Route("ws_updateRateName")]
        public string updateRateName(PayoutDatesParam param)
        {
            try
            {
                List<RateTypesList> List = new List<RateTypesList>();
                TempConnectionStringv4(param.db);
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@tenantid", mytype = SqlDbType.VarChar, Value = param.TenantID });
                con.myparameters.Add(new myParameters { ParameterName = "@ratename", mytype = SqlDbType.VarChar, Value = param.rateName });
                con.myparameters.Add(new myParameters { ParameterName = "@rateid", mytype = SqlDbType.VarChar, Value = param.rateTypeID });
                con.ExecuteNonQuery("sp_updateRateName");
                return "Success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }

        }

        [HttpPost]
        [Route("PO_ComputeDTCAbsences")]
        public string ComputeDTCAbsences(DTCAbsencessParams param)
        {
            try
            {
                TempConnectionStringv4(param.db);
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@LoginID", mytype = SqlDbType.VarChar, Value = param.LoginID });
                con.myparameters.Add(new myParameters { ParameterName = "@ClientCode", mytype = SqlDbType.VarChar, Value = param.ClientCode });
                con.myparameters.Add(new myParameters { ParameterName = "@StartDate", mytype = SqlDbType.VarChar, Value = param.StartDate });
                con.myparameters.Add(new myParameters { ParameterName = "@EndDate", mytype = SqlDbType.VarChar, Value = param.EndDate });
                con.myparameters.Add(new myParameters { ParameterName = "@PayoutType", mytype = SqlDbType.VarChar, Value = param.PayoutType });
                con.ExecuteNonQuery("SP_ComputeDTCAbsences");
                return "Success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }
        [HttpPost]
        [Route("Compute13thMonth")]
        public string Compute13thMonth(compute13th param)
        {
            try
            {
                DataTable DTParam = new DataTable();
                DTParam.Columns.AddRange(new DataColumn[1] { new DataColumn("UserId") });
                for (int i = 0; i < param.UserId.Length; i++)
                {
                    DTParam.Rows.Add(param.UserId[i]);
                }
                TempConnectionStringv4(param.db);
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@TenantID", mytype = SqlDbType.VarChar, Value = param.TenantID });
                con.myparameters.Add(new myParameters { ParameterName = "@AsOfDate", mytype = SqlDbType.VarChar, Value = param.AsOfDate });
                con.myparameters.Add(new myParameters { ParameterName = "@PayoutDate", mytype = SqlDbType.VarChar, Value = param.PayoutDate });
                con.myparameters.Add(new myParameters { ParameterName = "@HiredDate", mytype = SqlDbType.VarChar, Value = param.HiredDate });
                con.myparameters.Add(new myParameters { ParameterName = "@UserId", mytype = SqlDbType.Structured, Value = DTParam });
                DataTable DT = con.GetDataTable("sp_Compute13month");

                return "Success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }

        }
        [HttpPost]
        [Route("Benefits_Filter_ED")]
        public List<Benefits_Filter_ED> Benefits_Filter_ED(benefit param)
        {
            List<Benefits_Filter_ED> List = new List<Benefits_Filter_ED>();

            DataTable DTParam = new DataTable();
            DTParam.Columns.AddRange(new DataColumn[1] { new DataColumn("empid") });
            for (int i = 0; i < param.EmpID.Length; i++)
            {
                DTParam.Rows.Add(param.EmpID[i]);
            }
            TempConnectionStringv4(param.db);
            Connection con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@tenantID", mytype = SqlDbType.VarChar, Value = param.TenantID });
            con.myparameters.Add(new myParameters { ParameterName = "@EDStatus", mytype = SqlDbType.VarChar, Value = param.EDStatus });
            con.myparameters.Add(new myParameters { ParameterName = "@edlOption", mytype = SqlDbType.VarChar, Value = param.edlOption });
            con.myparameters.Add(new myParameters { ParameterName = "@uploadDate", mytype = SqlDbType.VarChar, Value = param.uploadDate });
            con.myparameters.Add(new myParameters { ParameterName = "@EmpId", mytype = SqlDbType.Structured, Value = DTParam });
            DataTable DT = con.GetDataTable("sp_Benefits_Filter_ED");
            foreach (DataRow row in DT.Rows)
            {
                Benefits_Filter_ED P = new Benefits_Filter_ED()
                {
                    EmpID = row["EmpID"].ToString(),
                    EmpName = row["EmpName"].ToString(),
                    CreatedON = row["CreatedON"].ToString(),
                    EDCode = row["EDCode"].ToString(),
                    Amount = row["Amount"].ToString(),
                    StartDate = row["StartDate"].ToString(),
                    EndDate = row["EndDate"].ToString(),
                    Frequency = row["Frequency"].ToString(),
                    Remarks = row["Remarks"].ToString(),
                    ClosedOn = row["ClosedOn"].ToString(),
                    EDID = row["EDID"].ToString()
                };
                List.Add(P);
            }
            return List;
        }

        //Allocation 


        [HttpPost]
        [Route("ws_CS_getAllocation")]
        public List<Allocation> CS_getAllocation(benefit param)
        {
            List<Allocation> List = new List<Allocation>();

            DataTable DTParam = new DataTable();
            DTParam.Columns.AddRange(new DataColumn[1] { new DataColumn("empid") });
            for (int i = 0; i < param.EmpID.Length; i++)
            {
                DTParam.Rows.Add(param.EmpID[i]);
            }
            TempConnectionStringv4(param.db);
            Connection con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@tenantID", mytype = SqlDbType.VarChar, Value = param.TenantID });
            con.myparameters.Add(new myParameters { ParameterName = "@EmpId", mytype = SqlDbType.Structured, Value = DTParam });
            DataTable DT = con.GetDataTable("sp_CS_getAllocation");
            foreach (DataRow row in DT.Rows)
            {
                Allocation P = new Allocation()
                {
                    EmpID = row["EmpID"].ToString(),
                    EmpName = row["EmpName"].ToString(),
                    CostCode = row["CostCode"].ToString(),
                    AllocationPercentage = row["AllocationPercentage"].ToString()
                };
                List.Add(P);
            }
            return List;
        }


        [HttpPost]
        [Route("ws_CS_updateAllocation")]
        public string Benefits_UpdateLoans(cs_allocation param)
        {
            try
            {
                TempConnectionStringv4(param.db);
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "tenantID", mytype = SqlDbType.VarChar, Value = param.tenantID });
                con.myparameters.Add(new myParameters { ParameterName = "option", mytype = SqlDbType.VarChar, Value = param.option });
                con.myparameters.Add(new myParameters { ParameterName = "EmpId", mytype = SqlDbType.VarChar, Value = param.EmpId });
                con.myparameters.Add(new myParameters { ParameterName = "CostCode", mytype = SqlDbType.VarChar, Value = param.CostCode });
                con.myparameters.Add(new myParameters { ParameterName = "Allocation", mytype = SqlDbType.VarChar, Value = param.Allocation });
                con.myparameters.Add(new myParameters { ParameterName = "NewCostCode", mytype = SqlDbType.VarChar, Value = param.NewCostCode });
                con.myparameters.Add(new myParameters { ParameterName = "NewAllocation", mytype = SqlDbType.VarChar, Value = param.NewAllocation });
                con.ExecuteNonQuery("sp_CS_updateAllocation");
                return "Success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }



        [HttpPost]
        [Route("Benefits_Filter_ED_Details")]
        public List<Benefits_Filter_ED_Details> Benefits_Filter_ED_Details(benefit param)
        {
            List<Benefits_Filter_ED_Details> List = new List<Benefits_Filter_ED_Details>();

            DataTable DTParam = new DataTable();
            DTParam.Columns.AddRange(new DataColumn[1] { new DataColumn("empid") });
            for (int i = 0; i < param.EmpID.Length; i++)
            {
                DTParam.Rows.Add(param.EmpID[i]);
            }
            TempConnectionStringv4(param.db);
            Connection con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@tenantID", mytype = SqlDbType.VarChar, Value = param.TenantID });
            con.myparameters.Add(new myParameters { ParameterName = "@edlOption", mytype = SqlDbType.VarChar, Value = param.edlOption });
            con.myparameters.Add(new myParameters { ParameterName = "@payoutdate", mytype = SqlDbType.VarChar, Value = param.payoutdate });
            con.myparameters.Add(new myParameters { ParameterName = "@EmpId", mytype = SqlDbType.Structured, Value = DTParam });
            DataTable DT = con.GetDataTable("sp_Benefits_Filter_ED_Details");
            foreach (DataRow row in DT.Rows)
            {
                Benefits_Filter_ED_Details P = new Benefits_Filter_ED_Details()
                {
                    EmpID = row["EmpID"].ToString(),
                    EmpName = row["EmpName"].ToString(),
                    Code = row["Code"].ToString(),
                    Amount = row["Amount"].ToString(),
                    PaymentTerms = row["PaymentTerms"].ToString(),
                    PayoutDate = row["PayoutDate"].ToString()
                };
                List.Add(P);
            }
            return List;
        }

        [HttpPost]
        [Route("Benefits_FilterLoans")]
        public List<Benefits_FilterLoans> Benefits_FilterLoans(benefit param)
        {
            List<Benefits_FilterLoans> List = new List<Benefits_FilterLoans>();
            DataTable DTParam = new DataTable();
            DTParam.Columns.AddRange(new DataColumn[1] { new DataColumn("empid") });
            for (int i = 0; i < param.EmpID.Length; i++)
            {
                DTParam.Rows.Add(param.EmpID[i]);
            }
            TempConnectionStringv4(param.db);
            Connection con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@tenantID", mytype = SqlDbType.VarChar, Value = param.TenantID });
            con.myparameters.Add(new myParameters { ParameterName = "@deductionsStatus", mytype = SqlDbType.VarChar, Value = param.deductionsStatus });
            con.myparameters.Add(new myParameters { ParameterName = "@uploadDate", mytype = SqlDbType.VarChar, Value = param.uploadDate });
            con.myparameters.Add(new myParameters { ParameterName = "@EmpId", mytype = SqlDbType.Structured, Value = DTParam });
            DataTable DT = con.GetDataTable("sp_Benefits_FilterLoans");
            foreach (DataRow row in DT.Rows)
            {
                Benefits_FilterLoans P = new Benefits_FilterLoans()
                {
                    EmpID = row["EmpID"].ToString(),
                    EmpName = row["EmpName"].ToString(),
                    CreatedON = row["CreatedON"].ToString(),
                    LoanID = row["LoanID"].ToString(),
                    LoanNameCode = row["LoanNameCode"].ToString(),
                    PrincipalAmount = row["PrincipalAmount"].ToString(),
                    TotalPayments = row["TotalPayments"].ToString(),
                    WithInterest = row["WithInterest"].ToString(),
                    StartDate = row["StartDate"].ToString(),
                    AmortizationAmount = row["AmortizationAmount"].ToString(),
                    Frequency = row["Frequency"].ToString(),
                    EndDate = row["EndDate"].ToString(),
                    LoanDate = row["LoanDate"].ToString(),
                    PromissoryNoteNum = row["PromissoryNoteNum"].ToString(),
                    Remarks = row["Remarks"].ToString(),
                    LoanAmount = row["LoanAmount"].ToString(),
                    ClosedOn = row["ClosedOn"].ToString()
                };
                List.Add(P);
            }
            return List;
        }

        [HttpPost]
        [Route("WS_update_DeductionEarning")]
        public String UpdateDeductionEarnings(modelUpdateEarningDeduction param)
        {
            try
            {
                DataTable DTParam = new DataTable();
                DTParam.Columns.AddRange(new DataColumn[15] {
                new DataColumn("EmpID"),
                new DataColumn("EmpName"),
                new DataColumn("CreatedON"),
             new DataColumn("PayrollType"),
              new DataColumn("Frequency"),
             new DataColumn("NewFrequency"),
             new DataColumn("EDType"),
             new DataColumn("Amount"),
             new DataColumn("NewAmount"),
             new DataColumn("StartDate"),
             new DataColumn("NewStartDate"),
             new DataColumn("EndDate"),
             new DataColumn("NewEndDate"),
             new DataColumn("ClosedOnDate"),
             new DataColumn("NewClosedOnDate")

            });
                for (int i = 0; i < param.EmpID.Length; i++)
                {
                    DTParam.Rows.Add(param.EmpID[i],
                        param.EmpName[i],
                        param.CreatedON[i],
                        param.PayrollType[i],
                        param.Frequency[i],
                        param.NewFrequency[i],
                        param.EDType[i],
                        param.Amount[i],
                        param.NewAmount[i],
                        param.StartDate[i],
                        param.NewStartDate[i],
                        param.EndDate[i],
                        param.NewEndDate[i],
                        param.ClosedOnDate[i],
                        param.NewClosedOnDate[i]
                        );
                }
                TempConnectionStringv4(param.db);
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@tenantID", mytype = SqlDbType.VarChar, Value = param.tenantID });
                con.myparameters.Add(new myParameters { ParameterName = "@option", mytype = SqlDbType.VarChar, Value = param.option });
                con.myparameters.Add(new myParameters { ParameterName = "@loginId", mytype = SqlDbType.VarChar, Value = param.loginId });
                con.myparameters.Add(new myParameters { ParameterName = "@Data", mytype = SqlDbType.Structured, Value = DTParam });
                con.ExecuteScalar("sp_UpdateBulk_Benefits_ED");

                return "Success";
            }catch(Exception e)
            {
                return e.ToString();
            }
        }

        [HttpPost]
        [Route("WS_UpdateLoans")]
        public String updateLoans(updateLoans param)
        {
            try
            {
                DataTable DTParam = new DataTable();
                DTParam.Columns.AddRange(new DataColumn[19] {
                new DataColumn("LoanID"),
                new DataColumn("EmpID"),
                new DataColumn("EmpName"),
             new DataColumn("CreatedON"),
              new DataColumn("LoanType"),
             new DataColumn("AmortizationAmount"),
             new DataColumn("NewAmortizationAmount"),
             new DataColumn("Frequency"),
             new DataColumn("NewFrequency"),
             new DataColumn("WithIntAmount"),
             new DataColumn("NewWithIntAmount"),
             new DataColumn("TotalPrevPayments"),
             new DataColumn("NewTotalPrevPayments"),
             new DataColumn("StartDate"),
              new DataColumn("NewStartDate"),
               new DataColumn("EndDate"),
                new DataColumn("NewEndDate"),
                 new DataColumn("ClosedOnDate"),
                  new DataColumn("NewClosedOnDate")

            });
                for (int i = 0; i < param.EmpID.Length; i++)
                {
                    DTParam.Rows.Add(param.LoanID[i],
                        param.EmpID[i],
                        param.EmpName[i],
                        param.CreatedON[i],
                        param.LoanType[i],
                        param.AmortizationAmount[i],
                        param.NewAmortizationAmount[i],
                        param.Frequency[i],
                        param.NewFrequency[i],
                        param.WithIntAmount[i],
                        param.NewWithIntAmount[i],
                        param.TotalPrevPayments[i],
                        param.NewTotalPrevPayments[i],
                        param.StartDate[i],
                        param.NewStartDate[i],
                        param.EndDate[i],
                        param.NewEndDate[i],
                        param.ClosedOnDate[i],
                        param.NewClosedOnDate[i]
                        );
                }
                TempConnectionStringv4(param.db);
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@tenantID", mytype = SqlDbType.VarChar, Value = param.tenantID });
           
                con.myparameters.Add(new myParameters { ParameterName = "@loginId", mytype = SqlDbType.VarChar, Value = param.loginId });
                con.myparameters.Add(new myParameters { ParameterName = "@Data", mytype = SqlDbType.Structured, Value = DTParam });
                con.ExecuteScalar("sp_UpdateBulk_Benefits_Loans");

                return "Success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }

        [HttpPost]
        [Route("Benefits_FilterLoans_Details")]
        public List<Benefits_FilterLoans_Details> Benefits_FilterLoans_Details(benefit param)
        {
            List<Benefits_FilterLoans_Details> List = new List<Benefits_FilterLoans_Details>();
            DataTable DTParam = new DataTable();
            DTParam.Columns.AddRange(new DataColumn[1] { new DataColumn("empid") });
            for (int i = 0; i < param.EmpID.Length; i++)
            {
                DTParam.Rows.Add(param.EmpID[i]);
            }
            TempConnectionStringv4(param.db);
            Connection con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@tenantID", mytype = SqlDbType.VarChar, Value = param.TenantID });
            con.myparameters.Add(new myParameters { ParameterName = "@payoutdate", mytype = SqlDbType.VarChar, Value = param.payoutdate });
            con.myparameters.Add(new myParameters { ParameterName = "@EmpId", mytype = SqlDbType.Structured, Value = DTParam });
            DataTable DT = con.GetDataTable("sp_Benefits_FilterLoans_Details");
            foreach (DataRow row in DT.Rows)
            {
                Benefits_FilterLoans_Details P = new Benefits_FilterLoans_Details()
                {
                    EmpId = row["EmpId"].ToString(),
                    EmpName = row["EmpName"].ToString(),
                    AmortizationAmount = row["AmortizationAmount"].ToString(),
                    LoanNameCode = row["LoanNameCode"].ToString(),
                    DateDeducted = row["DateDeducted"].ToString(),
                    PayOutDate = row["PayOutDate"].ToString(),
                    RemainingBalance = row["RemainingBalance"].ToString(),
                    Frequency = row["Frequency"].ToString(),
                    LoanID = row["LoanID"].ToString()
                };
                List.Add(P);
            }
            return List;
        }

        [HttpPost]
        [Route("Benefits_UpdateLoans")]
        public string Benefits_UpdateLoans(benefitloans param)
        {
            try
            {
                TempConnectionStringv4(param.db);
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "tenantID ", mytype = SqlDbType.VarChar, Value = param.TenantID });
                con.myparameters.Add(new myParameters { ParameterName = "EmpID ", mytype = SqlDbType.VarChar, Value = param.EmpID });
                con.myparameters.Add(new myParameters { ParameterName = "LoanCode ", mytype = SqlDbType.VarChar, Value = param.LoanCode });
                con.myparameters.Add(new myParameters { ParameterName = "LoanPrincipalAmount ", mytype = SqlDbType.VarChar, Value = param.LoanPrincipalAmount });
                con.myparameters.Add(new myParameters { ParameterName = "TotalPayments ", mytype = SqlDbType.VarChar, Value = param.TotalPayments });
                con.myparameters.Add(new myParameters { ParameterName = "TotalBalance ", mytype = SqlDbType.VarChar, Value = param.TotalBalance });
                con.myparameters.Add(new myParameters { ParameterName = "LoanStartDeductionPaydate ", mytype = SqlDbType.VarChar, Value = param.LoanStartDeductionPaydate });
                con.myparameters.Add(new myParameters { ParameterName = "MonthlyLoanAmortAmount ", mytype = SqlDbType.VarChar, Value = param.MonthlyLoanAmortAmount });
                con.myparameters.Add(new myParameters { ParameterName = "LoanAmortFrequency ", mytype = SqlDbType.VarChar, Value = param.LoanAmortFrequency });
                con.myparameters.Add(new myParameters { ParameterName = "LoanMaturityDate ", mytype = SqlDbType.VarChar, Value = param.LoanMaturityDate });
                con.myparameters.Add(new myParameters { ParameterName = "DateLoanGranted ", mytype = SqlDbType.VarChar, Value = param.DateLoanGranted });
                con.myparameters.Add(new myParameters { ParameterName = "PromissoryNote ", mytype = SqlDbType.VarChar, Value = param.PromissoryNote });
                con.myparameters.Add(new myParameters { ParameterName = "Remarks ", mytype = SqlDbType.VarChar, Value = param.Remarks });
                con.myparameters.Add(new myParameters { ParameterName = "LoanAmount ", mytype = SqlDbType.VarChar, Value = param.LoanAmount });
                con.myparameters.Add(new myParameters { ParameterName = "ClosedOnDate ", mytype = SqlDbType.VarChar, Value = param.ClosedOnDate });
                con.myparameters.Add(new myParameters { ParameterName = "updateLoanCode ", mytype = SqlDbType.VarChar, Value = param.updateLoanCode });
                con.myparameters.Add(new myParameters { ParameterName = "updateLoanPrincipalAmount ", mytype = SqlDbType.VarChar, Value = param.updateLoanPrincipalAmount });
                con.myparameters.Add(new myParameters { ParameterName = "updateTotalPayments ", mytype = SqlDbType.VarChar, Value = param.updateTotalPayments });
                con.myparameters.Add(new myParameters { ParameterName = "updateTotalBalance ", mytype = SqlDbType.VarChar, Value = param.updateTotalBalance });
                con.myparameters.Add(new myParameters { ParameterName = "updateLoanStartDeductionPaydate ", mytype = SqlDbType.VarChar, Value = param.updateLoanStartDeductionPaydate });
                con.myparameters.Add(new myParameters { ParameterName = "updateMonthlyLoanAmortAmount ", mytype = SqlDbType.VarChar, Value = param.updateMonthlyLoanAmortAmount });
                con.myparameters.Add(new myParameters { ParameterName = "updateLoanAmortFrequency ", mytype = SqlDbType.VarChar, Value = param.updateLoanAmortFrequency });
                con.myparameters.Add(new myParameters { ParameterName = "updateLoanMaturityDate ", mytype = SqlDbType.VarChar, Value = param.updateLoanMaturityDate });
                con.myparameters.Add(new myParameters { ParameterName = "updateDateLoanGranted ", mytype = SqlDbType.VarChar, Value = param.updateDateLoanGranted });
                con.myparameters.Add(new myParameters { ParameterName = "updatePromissoryNote ", mytype = SqlDbType.VarChar, Value = param.updatePromissoryNote });
                con.myparameters.Add(new myParameters { ParameterName = "updateRemarks ", mytype = SqlDbType.VarChar, Value = param.updateRemarks });
                con.myparameters.Add(new myParameters { ParameterName = "updateLoanAmount ", mytype = SqlDbType.VarChar, Value = param.updateLoanAmount });
                con.myparameters.Add(new myParameters { ParameterName = "updateClosedOnDate", mytype = SqlDbType.VarChar, Value = param.updateClosedOnDate });
                con.myparameters.Add(new myParameters { ParameterName = "LoanID", mytype = SqlDbType.VarChar, Value = param.LoanID });
                con.ExecuteNonQuery("sp_Benefits_UpdateLoans");
                return "Success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }

        [HttpPost]
        [Route("Benefits_UpdateLoans_Details")]
        public string Benefits_UpdateLoans_Details(benefitloans param)
        {
            try
            {
                TempConnectionStringv4(param.db);
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@tenantID", mytype = SqlDbType.VarChar, Value = param.TenantID });
                con.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.VarChar, Value = param.EmpID });
                con.myparameters.Add(new myParameters { ParameterName = "@LoanID", mytype = SqlDbType.VarChar, Value = param.LoanID });

                con.myparameters.Add(new myParameters { ParameterName = "@updateLoanCode", mytype = SqlDbType.VarChar, Value = param.updateLoanCode });
                con.myparameters.Add(new myParameters { ParameterName = "@updatePayoutDate", mytype = SqlDbType.VarChar, Value = param.updatePayoutDate });
                con.myparameters.Add(new myParameters { ParameterName = "@updateRemainingBalance", mytype = SqlDbType.VarChar, Value = param.updateRemainingBalance });
                con.myparameters.Add(new myParameters { ParameterName = "@updateDateDeducted", mytype = SqlDbType.VarChar, Value = param.updateDateDeducted });
                con.myparameters.Add(new myParameters { ParameterName = "@updateMonthlyLoanAmortAmount", mytype = SqlDbType.VarChar, Value = param.updateMonthlyLoanAmortAmount });
                con.myparameters.Add(new myParameters { ParameterName = "@updateLoanAmortFrequency", mytype = SqlDbType.VarChar, Value = param.updateLoanAmortFrequency });

                con.ExecuteNonQuery("sp_Benefits_UpdateLoans_Details");
                return "Success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }

        [HttpPost]
        [Route("Benefits_DeleteLoans")]
        public string Benefits_DeleteLoans(benefitloans param)
        {
            try
            {
                TempConnectionStringv4(param.db);
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "tenantID ", mytype = SqlDbType.VarChar, Value = param.TenantID });
                con.myparameters.Add(new myParameters { ParameterName = "EmpID ", mytype = SqlDbType.VarChar, Value = param.EmpID });
                con.myparameters.Add(new myParameters { ParameterName = "LoanCode ", mytype = SqlDbType.VarChar, Value = param.LoanCode });
                con.myparameters.Add(new myParameters { ParameterName = "LoanPrincipalAmount ", mytype = SqlDbType.VarChar, Value = param.LoanPrincipalAmount });
                con.myparameters.Add(new myParameters { ParameterName = "TotalPayments ", mytype = SqlDbType.VarChar, Value = param.TotalPayments });
                con.myparameters.Add(new myParameters { ParameterName = "TotalBalance ", mytype = SqlDbType.VarChar, Value = param.TotalBalance });
                con.myparameters.Add(new myParameters { ParameterName = "LoanStartDeductionPaydate ", mytype = SqlDbType.VarChar, Value = param.LoanStartDeductionPaydate });
                con.myparameters.Add(new myParameters { ParameterName = "MonthlyLoanAmortAmount ", mytype = SqlDbType.VarChar, Value = param.MonthlyLoanAmortAmount });
                con.myparameters.Add(new myParameters { ParameterName = "LoanAmortFrequency ", mytype = SqlDbType.VarChar, Value = param.LoanAmortFrequency });
                con.myparameters.Add(new myParameters { ParameterName = "LoanMaturityDate ", mytype = SqlDbType.VarChar, Value = param.LoanMaturityDate });
                con.myparameters.Add(new myParameters { ParameterName = "DateLoanGranted ", mytype = SqlDbType.VarChar, Value = param.DateLoanGranted });
                con.myparameters.Add(new myParameters { ParameterName = "PromissoryNote ", mytype = SqlDbType.VarChar, Value = param.PromissoryNote });
                con.myparameters.Add(new myParameters { ParameterName = "Remarks ", mytype = SqlDbType.VarChar, Value = param.Remarks });
                con.myparameters.Add(new myParameters { ParameterName = "LoanAmount ", mytype = SqlDbType.VarChar, Value = param.LoanAmount });
                con.myparameters.Add(new myParameters { ParameterName = "ClosedOnDate ", mytype = SqlDbType.VarChar, Value = param.ClosedOnDate });
                con.myparameters.Add(new myParameters { ParameterName = "@LoanID", mytype = SqlDbType.VarChar, Value = param.LoanID });
                con.ExecuteNonQuery("sp_Benefits_DeleteLoans");
                return "Success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }

        [HttpPost]
        [Route("Benefits_DeleteLoans_Details")]
        public string Benefits_DeleteLoans_Details(benefitloans param)
        {
            try
            {
                TempConnectionStringv4(param.db);
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@tenantID ", mytype = SqlDbType.VarChar, Value = param.TenantID });
                con.myparameters.Add(new myParameters { ParameterName = "@EmpID ", mytype = SqlDbType.VarChar, Value = param.EmpID });
                con.myparameters.Add(new myParameters { ParameterName = "@LoanID ", mytype = SqlDbType.VarChar, Value = param.LoanID });
                con.ExecuteNonQuery("sp_Benefits_DeleteLoans_Details");
                return "Success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }


        [HttpPost]
        [Route("Reset_Payroll")]
        public string Reset_Payroll(resetpayroll param)
        {
            try
            {
                TempConnectionStringv4(param.db);
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "TenantID ", mytype = SqlDbType.VarChar, Value = param.TenantID });
                con.myparameters.Add(new myParameters { ParameterName = "CutOffStart ", mytype = SqlDbType.VarChar, Value = param.CutOffStart });
                con.myparameters.Add(new myParameters { ParameterName = "CutOffEnd ", mytype = SqlDbType.VarChar, Value = param.CutOffEnd });
                con.myparameters.Add(new myParameters { ParameterName = "PayoutType ", mytype = SqlDbType.VarChar, Value = param.PayoutType });
                con.ExecuteNonQuery("sp_payroll_reset");
                return "Success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }

        [HttpPost]
        [Route("Benefits_UpdateED")]
        public string Benefits_UpdateED(benefit param)
        {
            try
            {
                TempConnectionStringv4(param.db);
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@tenantID", mytype = SqlDbType.VarChar, Value = param.TenantID });
                con.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.VarChar, Value = param.ID });
                con.myparameters.Add(new myParameters { ParameterName = "@EDType", mytype = SqlDbType.VarChar, Value = param.EDType });
                con.myparameters.Add(new myParameters { ParameterName = "@Date", mytype = SqlDbType.VarChar, Value = param.Date });
                con.myparameters.Add(new myParameters { ParameterName = "@Code", mytype = SqlDbType.VarChar, Value = param.Code });
                con.myparameters.Add(new myParameters { ParameterName = "@updateEDID", mytype = SqlDbType.VarChar, Value = param.EDID });
                con.myparameters.Add(new myParameters { ParameterName = "@Amount", mytype = SqlDbType.VarChar, Value = param.Amount });
                con.myparameters.Add(new myParameters { ParameterName = "@RecurStart", mytype = SqlDbType.VarChar, Value = param.RecurStart });
                con.myparameters.Add(new myParameters { ParameterName = "@RecurEnd", mytype = SqlDbType.VarChar, Value = param.RecurEnd });
                con.myparameters.Add(new myParameters { ParameterName = "@Frequency", mytype = SqlDbType.VarChar, Value = param.Frequency });
                con.myparameters.Add(new myParameters { ParameterName = "@Remarks", mytype = SqlDbType.VarChar, Value = param.Remarks });
                con.myparameters.Add(new myParameters { ParameterName = "@updateDate", mytype = SqlDbType.VarChar, Value = param.updateDate });
                con.myparameters.Add(new myParameters { ParameterName = "@updateCode", mytype = SqlDbType.VarChar, Value = param.updateCode });
                con.myparameters.Add(new myParameters { ParameterName = "@updateAmount", mytype = SqlDbType.VarChar, Value = param.updateAmount });
                con.myparameters.Add(new myParameters { ParameterName = "@updateRecurStart", mytype = SqlDbType.VarChar, Value = param.updateRecurStart });
                con.myparameters.Add(new myParameters { ParameterName = "@updateRecurEnd", mytype = SqlDbType.VarChar, Value = param.updateRecurEnd });
                con.myparameters.Add(new myParameters { ParameterName = "@updateFrequency", mytype = SqlDbType.VarChar, Value = param.updateFrequency });
                con.myparameters.Add(new myParameters { ParameterName = "@updateRemarks", mytype = SqlDbType.VarChar, Value = param.updateRemarks });
                con.myparameters.Add(new myParameters { ParameterName = "@updateClosedOn", mytype = SqlDbType.VarChar, Value = param.updateClosedOn });
                con.ExecuteNonQuery("sp_Benefits_UpdateED");
                return "Success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }

        [HttpPost]
        [Route("Benefits_UpdateED_Details")]
        public string Benefits_UpdateED_Details(benefit param)
        {
            try
            {
                TempConnectionStringv4(param.db);
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@tenantID", mytype = SqlDbType.VarChar, Value = param.TenantID });
                con.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.VarChar, Value = param.ID });
                con.myparameters.Add(new myParameters { ParameterName = "@EDType", mytype = SqlDbType.VarChar, Value = param.EDType });
                con.myparameters.Add(new myParameters { ParameterName = "@Code", mytype = SqlDbType.VarChar, Value = param.Code });
                con.myparameters.Add(new myParameters { ParameterName = "@Amount", mytype = SqlDbType.VarChar, Value = param.Amount });

                con.myparameters.Add(new myParameters { ParameterName = "@PaymentTerms", mytype = SqlDbType.VarChar, Value = param.PaymentTerms });
                con.myparameters.Add(new myParameters { ParameterName = "@PayoutDate", mytype = SqlDbType.VarChar, Value = param.payoutdate });

                con.myparameters.Add(new myParameters { ParameterName = "@updateCode", mytype = SqlDbType.VarChar, Value = param.updateCode });
                con.myparameters.Add(new myParameters { ParameterName = "@updateAmount", mytype = SqlDbType.VarChar, Value = param.updateAmount });
                con.myparameters.Add(new myParameters { ParameterName = "@updatePaymentTerms", mytype = SqlDbType.VarChar, Value = param.updatePaymentTerms });
                con.myparameters.Add(new myParameters { ParameterName = "@updatePayoutDate", mytype = SqlDbType.VarChar, Value = param.updatePayoutDate });
                con.ExecuteNonQuery("sp_Benefits_UpdateED_Details");
                return "Success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }


        [HttpPost]
        [Route("Extract_JournalVoucherv2")]
        public string JournalVoucherv2(efile param)
        {

            FileStream stream = null;
            try
            {

                TempConnectionStringv4(param.db);
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@TenantID", mytype = SqlDbType.NVarChar, Value = param.TenantID });
                con.myparameters.Add(new myParameters { ParameterName = "@CutoffStart", mytype = SqlDbType.NVarChar, Value = param.CutoffStart });
                con.myparameters.Add(new myParameters { ParameterName = "@CutoffEnd", mytype = SqlDbType.NVarChar, Value = param.CutoffEnd });
                con.myparameters.Add(new myParameters { ParameterName = "@Status", mytype = SqlDbType.NVarChar, Value = param.EmpStatus });
                DataTable data1 = con.GetDataTable("sp_jv_woAllocationv2");


                string dataDir = HostingEnvironment.MapPath("~/Files/PayrollOutSourcing/");

                stream = new FileStream(dataDir + "Template_JV.xls", FileMode.Open);
                string filename;
                // string datebuild = DateTime.Now.ToString("MMddyyyy");
                string cc = param.cCode;


                string cStart = Regex.Replace(param.CutoffStart, @"[^0-9a-zA-Z]+", "");
                string cEnd = Regex.Replace(param.CutoffEnd, @"[^0-9a-zA-Z]+", "");


                string datebuild = DateTime.Now.ToString("yyyyMM");
                string timebuild = DateTime.Now.ToString("hhtt");
                filename = cc + "_JV_" + cStart + "_" + cEnd + "_" + timebuild + ".xls";
                using (var fileStream = new FileStream(dataDir + filename, FileMode.Create, FileAccess.Write))
                //using (var fileStream = new FileStream(dataDir + filename, FileMode.Create, FileAccess.Write))
                {
                    stream.CopyTo(fileStream);
                }
                stream.Close();
                ExcelWorkbookFormat workbookFormat = ExcelWorkbookFormat.Xls_2003;
                string testDocFile = dataDir + "Template_JV.xls";
                ExcelWorkbook workbook = new ExcelWorkbook(testDocFile);
                ExcelWorksheet firstWorksheet = workbook.Worksheets[0];

                #region PayReg Sheet

                ExcelWorksheet WOAllocation = workbook.Worksheets[0];
                ExcelWorksheet Allocation = workbook.Worksheets[1];
                ExcelWorksheet sc1 = workbook.Worksheets[2];
                ExcelWorksheet sc2 = workbook.Worksheets[3];
                //Sheet7.WorksheetSecurity.ProtectWorksheet("sbc01");
                //AX TAXTABLE

                int x = 3;
                for (int i = 0; i < data1.Rows.Count; i++, x++)
                {

                    WOAllocation["A" + x].Text = data1.Rows[i]["PayrollNum"].ToString(); //payroll#
                    WOAllocation["B" + x].Text = data1.Rows[i]["PayDate"].ToString(); //paydate
                    WOAllocation["C" + x].Text = data1.Rows[i]["PayCode"].ToString(); //pay code
                    WOAllocation["D" + x].Text = data1.Rows[i]["userID"].ToString(); //employee id
                    WOAllocation["E" + x].Text = data1.Rows[i]["LastName"].ToString(); //last name
                    WOAllocation["F" + x].Text = data1.Rows[i]["FirstName"].ToString(); //first name
                    WOAllocation["G" + x].Text = data1.Rows[i]["EmployeeType"].ToString(); //file status
                    WOAllocation["H" + x].Text = data1.Rows[i]["TaxStatus"].ToString(); //tax status
                    WOAllocation["I" + x].Value = data1.Rows[i]["BranchCode"]; //daily rate
                    WOAllocation["J" + x].Value = data1.Rows[i]["Allocation"]; //salary rate type
                    WOAllocation["K" + x].Value = data1.Rows[i]["DailyRate"]; //basic salary
                    WOAllocation["L" + x].Value = data1.Rows[i]["MonthlyRate"]; //misc amount
                    WOAllocation["M" + x].Value = data1.Rows[i]["PayrollCost"];
                    WOAllocation["N" + x].Value = data1.Rows[i]["BasicPay"]; //overtime
                    WOAllocation["O" + x].Value = data1.Rows[i]["AbsentAmount"];
                    WOAllocation["P" + x].Value = data1.Rows[i]["TardyAmount"]; //other tx income
                    WOAllocation["Q" + x].Value = data1.Rows[i]["UndertimeAmount"]; //adjustments
                    WOAllocation["R" + x].Value = data1.Rows[i]["LWOPAmount"]; //gross income
                    WOAllocation["S" + x].Value = data1.Rows[i]["WorkHoursAmount"]; //withholding tax
                    WOAllocation["T" + x].Value = data1.Rows[i]["NDOvertimeAmount"]; //net salary after tax
                    WOAllocation["U" + x].Value = data1.Rows[i]["OvertimeAmount"]; //employee sss
                    WOAllocation["V" + x].Value = data1.Rows[i]["PerfBonusNonAdmin"];
                    WOAllocation["W" + x].Value = data1.Rows[i]["Pay13TMonth"];
                    WOAllocation["X" + x].Value = data1.Rows[i]["PerfBonusAdmin"];
                    WOAllocation["Y" + x].Value = data1.Rows[i]["Maternity"];
                    WOAllocation["Z" + x].Value = data1.Rows[i]["NetPayAdjustment"];
                    WOAllocation["AA" + x].Value = data1.Rows[i]["Reimbursement"];
                    WOAllocation["AB" + x].Value = data1.Rows[i]["SSSSLADJ_NTX"];
                    WOAllocation["AC" + x].Value = data1.Rows[i]["BasicADJ"];
                    WOAllocation["AD" + x].Value = data1.Rows[i]["Maternity_SALDIF"];

                    WOAllocation["AE" + x].Value = data1.Rows[i]["Pay13TMonthNONAdmin"];
                    WOAllocation["AF" + x].Value = data1.Rows[i]["STIP"];
                    WOAllocation["AG" + x].Value = data1.Rows[i]["Pay13thADJNONAdmin"];
                    WOAllocation["AH" + x].Value = data1.Rows[i]["BEREAVEMENT"];
                    WOAllocation["AI" + x].Value = data1.Rows[i]["Pay13thADJAdmin"];
                    WOAllocation["AJ" + x].Value = data1.Rows[i]["LAUNDRYALLOW"];
                    WOAllocation["AK" + x].Value = data1.Rows[i]["LAUNDRYALLOWADJ"];
                    WOAllocation["AL" + x].Value = data1.Rows[i]["MEALALLOW"];
                    WOAllocation["AM" + x].Value = data1.Rows[i]["MEALALLOWADJ"];
                    WOAllocation["AN" + x].Value = data1.Rows[i]["RICESUBSIDY"];
                    WOAllocation["AO" + x].Value = data1.Rows[i]["RiceADJ"]; //employee mcr(philhealth)
                    WOAllocation["AP" + x].Value = data1.Rows[i]["TranspoTX"]; //employee pagibig
                    WOAllocation["AQ" + x].Value = data1.Rows[i]["  TRANSPOALLOW_NTX"];

                    WOAllocation["AR" + x].Value = data1.Rows[i]["CommunicationAllowance"]; //other ntx income
                    WOAllocation["AS" + x].Value = data1.Rows[i]["OnCallAllowance"]; //loan payments
                    WOAllocation["AT" + x].Value = data1.Rows[i]["OtherAllowance"]; //deductions
                    WOAllocation["AU" + x].Value = data1.Rows[i]["OtherIncomeTax"]; //net salary
                    WOAllocation["AV" + x].Value = data1.Rows[i]["USRNRETENTION"]; //employer sss
                    WOAllocation["AW" + x].Value = data1.Rows[i]["Referral"]; //employer mcr(philhealth)
                    WOAllocation["AX" + x].Value = data1.Rows[i]["HMO"]; //employer ec
                    WOAllocation["AY" + x].Value = data1.Rows[i]["HMOCard"]; //employer pagibig
                    WOAllocation["AZ" + x].Value = data1.Rows[i]["CompanyID"]; //payroll cost
                    WOAllocation["BA" + x].Value = data1.Rows[i]["CompanyBadge"]; //payment type
                    WOAllocation["BB" + x].Value = data1.Rows[i]["BLDGBadge"]; //bank acct#
                    WOAllocation["BC" + x].Value = data1.Rows[i]["RetirementFund"]; //bank name
                    WOAllocation["BD" + x].Value = data1.Rows[i]["SSSSLAmount"]; //date employed
                    WOAllocation["BE" + x].Value = data1.Rows[i]["SSSCLAmount"]; //date terminated
                    WOAllocation["BF" + x].Value = data1.Rows[i]["SSSSLADJAmount"]; //cost center
                    WOAllocation["BG" + x].Value = data1.Rows[i]["SSSCLADJAmount"];
                    WOAllocation["BH" + x].Value = data1.Rows[i]["PAGMPLAmount"]; //currency
                    WOAllocation["BI" + x].Value = data1.Rows[i]["PAGMPLADJAmount"]; //exchange rate
                    WOAllocation["BJ" + x].Value = data1.Rows[i]["CODADV"]; //payment freq
                    WOAllocation["BK" + x].Value = data1.Rows[i]["FLUVACCINE"];
                    WOAllocation["BL" + x].Value = data1.Rows[i]["NEGNETPAY"];
                    WOAllocation["BM" + x].Value = data1.Rows[i]["SSSSICKNESS"];
                    WOAllocation["BN" + x].Value = data1.Rows[i]["SSSMATERNITY"];
                    WOAllocation["BO" + x].Value = data1.Rows[i]["ITSUPPLIES"];
                    WOAllocation["BP" + x].Value = data1.Rows[i]["PHONECHARGE"];
                    WOAllocation["BQ" + x].Value = data1.Rows[i]["OtherDED"];
                    WOAllocation["BR" + x].Value = data1.Rows[i]["GOVTSTATUTORYDEDUCTION"];
                    WOAllocation["BS" + x].Value = data1.Rows[i]["SSSSALARYDIFFERENTIAL"];
                    WOAllocation["BT" + x].Value = data1.Rows[i]["LOANADJ"];
                    WOAllocation["BU" + x].Value = data1.Rows[i]["Tax Refund"];
                    WOAllocation["BV" + x].Value = data1.Rows[i]["Tax Payable"];
                    WOAllocation["BW" + x].Value = data1.Rows[i]["PAGCAL"];
                    WOAllocation["BX" + x].Value = data1.Rows[i]["SSSSLERP"];
                    WOAllocation["BY" + x].Value = data1.Rows[i]["COSL"];
                    WOAllocation["BZ" + x].Value = data1.Rows[i]["PhilHealthERAmount"];
                    WOAllocation["CA" + x].Value = data1.Rows[i]["PagibigERAmount"];
                    WOAllocation["CB" + x].Value = data1.Rows[i]["SSS"];
                    WOAllocation["CC" + x].Value = data1.Rows[i]["PhilHealth"];
                    WOAllocation["CD" + x].Value = data1.Rows[i]["TotalSSS"];
                    WOAllocation["CE" + x].Value = data1.Rows[i]["Pagibig"];
                    WOAllocation["CF" + x].Value = data1.Rows[i]["WHTax"];
                    WOAllocation["CG" + x].Value = data1.Rows[i]["NetPay"];





                }
                #endregion
                TempConnectionStringv4(param.db);
                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@TenantID", mytype = SqlDbType.NVarChar, Value = param.TenantID });
                con.myparameters.Add(new myParameters { ParameterName = "@CutoffStart", mytype = SqlDbType.NVarChar, Value = param.CutoffStart });
                con.myparameters.Add(new myParameters { ParameterName = "@CutoffEnd", mytype = SqlDbType.NVarChar, Value = param.CutoffEnd });
                con.myparameters.Add(new myParameters { ParameterName = "@Status", mytype = SqlDbType.NVarChar, Value = param.EmpStatus });
                DataTable data2 = con.GetDataTable("sp_jv_Allocationv2");
                x = 3;
                for (int i = 0; i < data2.Rows.Count; i++, x++)
                {

                    Allocation["A" + x].Text = data2.Rows[i]["PayrollNum"].ToString(); //payroll#
                    Allocation["B" + x].Text = data2.Rows[i]["PayDate"].ToString(); //paydate
                    Allocation["C" + x].Text = data2.Rows[i]["PayCode"].ToString(); //pay code
                    Allocation["D" + x].Text = data2.Rows[i]["userID"].ToString(); //employee id
                    Allocation["E" + x].Text = data2.Rows[i]["LastName"].ToString(); //last name
                    Allocation["F" + x].Text = data2.Rows[i]["FirstName"].ToString(); //first name
                    Allocation["G" + x].Text = data2.Rows[i]["EmployeeType"].ToString(); //file status
                    Allocation["H" + x].Text = data2.Rows[i]["TaxStatus"].ToString(); //tax status
                    Allocation["I" + x].Value = data2.Rows[i]["BranchCode"]; //daily rate
                    Allocation["J" + x].Value = data2.Rows[i]["Allocation"]; //salary rate type
                    Allocation["K" + x].Value = data2.Rows[i]["DailyRate"]; //basic salary
                    Allocation["L" + x].Value = data2.Rows[i]["MonthlyRate"]; //misc amount
                    Allocation["M" + x].Value = data2.Rows[i]["PayrollCost"];
                    Allocation["N" + x].Value = data2.Rows[i]["BasicPay"]; //overtime
                    Allocation["O" + x].Value = data2.Rows[i]["AbsentAmount"];
                    Allocation["P" + x].Value = data2.Rows[i]["TardyAmount"]; //other tx income
                    Allocation["Q" + x].Value = data2.Rows[i]["UndertimeAmount"]; //adjustments
                    Allocation["R" + x].Value = data2.Rows[i]["LWOPAmount"]; //gross income
                    Allocation["S" + x].Value = data2.Rows[i]["WorkHoursAmount"]; //withholding tax
                    Allocation["T" + x].Value = data2.Rows[i]["NDOvertimeAmount"]; //net salary after tax
                    Allocation["U" + x].Value = data2.Rows[i]["OvertimeAmount"]; //employee sss
                    Allocation["V" + x].Value = data2.Rows[i]["PerfBonusNonAdmin"];
                    Allocation["W" + x].Value = data2.Rows[i]["Pay13TMonth"];
                    Allocation["X" + x].Value = data2.Rows[i]["PerfBonusAdmin"];
                    Allocation["Y" + x].Value = data2.Rows[i]["Maternity"];
                    Allocation["Z" + x].Value = data2.Rows[i]["NetPayAdjustment"];
                    Allocation["AA" + x].Value = data2.Rows[i]["Reimbursement"];
                    Allocation["AB" + x].Value = data2.Rows[i]["SSSSLADJ_NTX"];
                    Allocation["AC" + x].Value = data2.Rows[i]["BasicADJ"];
                    Allocation["AD" + x].Value = data2.Rows[i]["Maternity_SALDIF"];

                    Allocation["AE" + x].Value = data2.Rows[i]["Pay13TMonthNONAdmin"];
                    Allocation["AF" + x].Value = data2.Rows[i]["STIP"];
                    Allocation["AG" + x].Value = data2.Rows[i]["Pay13thADJNONAdmin"];
                    Allocation["AH" + x].Value = data2.Rows[i]["BEREAVEMENT"];
                    Allocation["AI" + x].Value = data2.Rows[i]["Pay13thADJAdmin"];
                    Allocation["AJ" + x].Value = data2.Rows[i]["LAUNDRYALLOW"];
                    Allocation["AK" + x].Value = data2.Rows[i]["LAUNDRYALLOWADJ"];
                    Allocation["AL" + x].Value = data2.Rows[i]["MEALALLOW"];
                    Allocation["AM" + x].Value = data2.Rows[i]["MEALALLOWADJ"];
                    Allocation["AN" + x].Value = data2.Rows[i]["RICESUBSIDY"];
                    Allocation["AO" + x].Value = data2.Rows[i]["RiceADJ"]; //employee mcr(philhealth)
                    Allocation["AP" + x].Value = data2.Rows[i]["TranspoTX"]; //employee pagibig
                    Allocation["AQ" + x].Value = data2.Rows[i]["  TRANSPOALLOW_NTX"];

                    Allocation["AR" + x].Value = data2.Rows[i]["CommunicationAllowance"]; //other ntx income
                    Allocation["AS" + x].Value = data2.Rows[i]["OnCallAllowance"]; //loan payments
                    Allocation["AT" + x].Value = data2.Rows[i]["OtherAllowance"]; //deductions
                    Allocation["AU" + x].Value = data2.Rows[i]["OtherIncomeTax"]; //net salary
                    Allocation["AV" + x].Value = data2.Rows[i]["USRNRETENTION"]; //employer sss
                    Allocation["AW" + x].Value = data2.Rows[i]["Referral"]; //employer mcr(philhealth)
                    Allocation["AX" + x].Value = data2.Rows[i]["HMO"]; //employer ec
                    Allocation["AY" + x].Value = data2.Rows[i]["HMOCard"]; //employer pagibig
                    Allocation["AZ" + x].Value = data2.Rows[i]["CompanyID"]; //payroll cost
                    Allocation["BA" + x].Value = data2.Rows[i]["CompanyBadge"]; //payment type
                    Allocation["BB" + x].Value = data2.Rows[i]["BLDGBadge"]; //bank acct#
                    Allocation["BC" + x].Value = data2.Rows[i]["RetirementFund"]; //bank name
                    Allocation["BD" + x].Value = data2.Rows[i]["SSSSLAmount"]; //date employed
                    Allocation["BE" + x].Value = data2.Rows[i]["SSSCLAmount"]; //date terminated
                    Allocation["BF" + x].Value = data2.Rows[i]["SSSSLADJAmount"]; //cost center
                    Allocation["BG" + x].Value = data2.Rows[i]["SSSCLADJAmount"];
                    Allocation["BH" + x].Value = data2.Rows[i]["PAGMPLAmount"]; //currency
                    Allocation["BI" + x].Value = data2.Rows[i]["PAGMPLADJAmount"]; //exchange rate
                    Allocation["BJ" + x].Value = data2.Rows[i]["CODADV"]; //payment freq
                    Allocation["BK" + x].Value = data2.Rows[i]["FLUVACCINE"];
                    Allocation["BL" + x].Value = data2.Rows[i]["NEGNETPAY"];
                    Allocation["BM" + x].Value = data2.Rows[i]["SSSSICKNESS"];
                    Allocation["BN" + x].Value = data2.Rows[i]["SSSMATERNITY"];
                    Allocation["BO" + x].Value = data2.Rows[i]["ITSUPPLIES"];
                    Allocation["BP" + x].Value = data2.Rows[i]["PHONECHARGE"];
                    Allocation["BQ" + x].Value = data2.Rows[i]["OtherDED"];
                    Allocation["BR" + x].Value = data2.Rows[i]["GOVTSTATUTORYDEDUCTION"];
                    Allocation["BS" + x].Value = data2.Rows[i]["SSSSALARYDIFFERENTIAL"];
                    Allocation["BT" + x].Value = data2.Rows[i]["LOANADJ"];
                    Allocation["BU" + x].Value = data2.Rows[i]["Tax Refund"];
                    Allocation["BV" + x].Value = data2.Rows[i]["Tax Payable"];
                    Allocation["BW" + x].Value = data2.Rows[i]["PAGCAL"];
                    Allocation["BX" + x].Value = data2.Rows[i]["SSSSLERP"];
                    Allocation["BY" + x].Value = data2.Rows[i]["COSL"];
                    Allocation["BZ" + x].Value = data2.Rows[i]["PhilHealthERAmount"];
                    Allocation["CA" + x].Value = data2.Rows[i]["PagibigERAmount"];
                    Allocation["CB" + x].Value = data2.Rows[i]["SSS"];
                    Allocation["CC" + x].Value = data2.Rows[i]["PhilHealth"];
                    Allocation["CD" + x].Value = data2.Rows[i]["TotalSSS"];
                    Allocation["CE" + x].Value = data2.Rows[i]["Pagibig"];
                    Allocation["CF" + x].Value = data2.Rows[i]["WHTax"];
                    Allocation["CG" + x].Value = data2.Rows[i]["NetPay"];
                }



                //return "Success";
                TempConnectionStringv4(param.db);
                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@TenantID", mytype = SqlDbType.NVarChar, Value = param.TenantID });
                con.myparameters.Add(new myParameters { ParameterName = "@CutoffStart", mytype = SqlDbType.NVarChar, Value = param.CutoffStart });
                con.myparameters.Add(new myParameters { ParameterName = "@CutoffEnd", mytype = SqlDbType.NVarChar, Value = param.CutoffEnd });
                con.myparameters.Add(new myParameters { ParameterName = "@Status", mytype = SqlDbType.NVarChar, Value = param.EmpStatus });
                DataTable c1 = con.GetDataTable("sp_jv_c1v2");
                x = 2;
                for (int i = 0; i < c1.Rows.Count; i++, x++)
                {
                    sc1["A" + x].Text = c1.Rows[i]["userID"].ToString();
                    sc1["B" + x].Text = c1.Rows[i]["LastName"].ToString();
                    sc1["C" + x].Text = c1.Rows[i]["FirstName"].ToString();
                    sc1["D" + x].Text = c1.Rows[i]["Allocation"].ToString();
                    sc1["E" + x].Text = c1.Rows[i]["AccountNumber"].ToString();
                    sc1["F" + x].Text = c1.Rows[i]["AccountCode"].ToString();
                    sc1["G" + x].Value = c1.Rows[i]["Amount"];
                    sc1["H" + x].Value = c1.Rows[i]["Debit"];
                    sc1["I" + x].Value = c1.Rows[i]["credit"];
                    sc1["J" + x].Text = c1.Rows[i]["BranchCode"].ToString();
                }
                TempConnectionStringv4(param.db);
                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@TenantID", mytype = SqlDbType.NVarChar, Value = param.TenantID });
                con.myparameters.Add(new myParameters { ParameterName = "@CutoffStart", mytype = SqlDbType.NVarChar, Value = param.CutoffStart });
                con.myparameters.Add(new myParameters { ParameterName = "@CutoffEnd", mytype = SqlDbType.NVarChar, Value = param.CutoffEnd });
                con.myparameters.Add(new myParameters { ParameterName = "@Status", mytype = SqlDbType.NVarChar, Value = param.EmpStatus });
                DataTable c2 = con.GetDataTable("sp_jv_c2v2");
                x = 2;
                for (int i = 0; i < c2.Rows.Count; i++, x++)
                {
                    sc2["A" + x].Text = c2.Rows[i]["userID"].ToString();
                    sc2["B" + x].Text = c2.Rows[i]["LastName"].ToString();
                    sc2["C" + x].Text = c2.Rows[i]["FirstName"].ToString();
                    sc2["D" + x].Text = c2.Rows[i]["Allocation"].ToString();
                    sc2["E" + x].Text = c2.Rows[i]["AccountNumber"].ToString();
                    sc2["F" + x].Text = c2.Rows[i]["AccountCode"].ToString();
                    sc2["G" + x].Value = c2.Rows[i]["Amount"];
                    sc2["H" + x].Value = c2.Rows[i]["Debit"];
                    sc2["I" + x].Value = c2.Rows[i]["credit"];
                    sc2["J" + x].Text = c2.Rows[i]["BranchCode"].ToString();
                }
                HttpResponse httpResponse = System.Web.HttpContext.Current.Response;

                httpResponse.Clear();
                httpResponse.ContentType = "Application/x-msexcel";
                httpResponse.AddHeader("Content-Disposition", String.Format("attachment; filename={0}", filename));
                string newDIR = HttpContext.Current.Server.MapPath("~/sFTP/PayrollOutsourcing/");
                string onlineDIR = "http://ws.durusthr.com/ILM_WS_Live/sFTP/PayrollOutsourcing/";
                //filename = filename.Replace(',', '_');
                //filename = filename.Replace(" ", String.Empty);
                workbook.Save(newDIR + filename);
                stream.Close();
                if (data1.Rows.Count > 0)
                {
                    return onlineDIR + filename;
                }
                else
                {

                    return "No Rows";
                }
            }
            catch (Exception e)
            {
                stream.Close();
                return "Error: " + e.ToString();
            }
        }

        [HttpPost]
        [Route("Extract_JournalVoucherv3")]
        public string JournalVoucherv3(efile param)
        {

            FileStream stream = null;
            try
            {

                TempConnectionStringv4(param.db);
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@TenantID", mytype = SqlDbType.NVarChar, Value = param.TenantID });
                con.myparameters.Add(new myParameters { ParameterName = "@CutoffStart", mytype = SqlDbType.NVarChar, Value = param.CutoffStart });
                con.myparameters.Add(new myParameters { ParameterName = "@CutoffEnd", mytype = SqlDbType.NVarChar, Value = param.CutoffEnd });
                con.myparameters.Add(new myParameters { ParameterName = "@Status", mytype = SqlDbType.NVarChar, Value = param.EmpStatus });
                DataTable data1 = con.GetDataTable("sp_jv_woAllocationv2");


                string dataDir = HostingEnvironment.MapPath("~/Files/PayrollOutSourcing/");

                stream = new FileStream(dataDir + "Template_JV.xls", FileMode.Open);
                string filename;
                // string datebuild = DateTime.Now.ToString("MMddyyyy");
                string cc = param.cCode;


                string cStart = Regex.Replace(param.CutoffStart, @"[^0-9a-zA-Z]+", "");
                string cEnd = Regex.Replace(param.CutoffEnd, @"[^0-9a-zA-Z]+", "");


                string datebuild = DateTime.Now.ToString("yyyyMM");
                string timebuild = DateTime.Now.ToString("hhtt");
                filename = cc + "_JV_" + cStart + "_" + cEnd + "_" + timebuild + ".xls";
                using (var fileStream = new FileStream(dataDir + filename, FileMode.Create, FileAccess.Write))
                //using (var fileStream = new FileStream(dataDir + filename, FileMode.Create, FileAccess.Write))
                {
                    stream.CopyTo(fileStream);
                }
                stream.Close();
                ExcelWorkbookFormat workbookFormat = ExcelWorkbookFormat.Xls_2003;
                string testDocFile = dataDir + "Template_JV.xls";
                ExcelWorkbook workbook = new ExcelWorkbook(testDocFile);
                ExcelWorksheet firstWorksheet = workbook.Worksheets[0];

                #region PayReg Sheet

                ExcelWorksheet WOAllocation = workbook.Worksheets[0];
                ExcelWorksheet Allocation = workbook.Worksheets[1];
                ExcelWorksheet sc1 = workbook.Worksheets[2];
                ExcelWorksheet sc2 = workbook.Worksheets[3];
                ExcelWorksheet jv = workbook.Worksheets[4];
                ExcelWorksheet jvSummary = workbook.Worksheets[5];
                
                //Sheet7.WorksheetSecurity.ProtectWorksheet("sbc01");
                //AX TAXTABLE

                int x = 3;
                for (int i = 0; i < data1.Rows.Count; i++, x++)
                {

                    WOAllocation["A" + x].Text = data1.Rows[i]["PayrollNum"].ToString(); //payroll#
                    WOAllocation["B" + x].Text = data1.Rows[i]["PayDate"].ToString(); //paydate
                    WOAllocation["C" + x].Text = data1.Rows[i]["PayCode"].ToString(); //pay code
                    WOAllocation["D" + x].Text = data1.Rows[i]["userID"].ToString(); //employee id
                    WOAllocation["E" + x].Text = data1.Rows[i]["LastName"].ToString(); //last name
                    WOAllocation["F" + x].Text = data1.Rows[i]["FirstName"].ToString(); //first name
                    WOAllocation["G" + x].Text = data1.Rows[i]["EmployeeType"].ToString(); //file status
                    WOAllocation["H" + x].Text = data1.Rows[i]["TaxStatus"].ToString(); //tax status
                    WOAllocation["I" + x].Value = data1.Rows[i]["BranchCode"]; //daily rate
                    WOAllocation["J" + x].Value = data1.Rows[i]["Allocation"]; //salary rate type
                    WOAllocation["K" + x].Value = data1.Rows[i]["DailyRate"]; //basic salary
                    WOAllocation["L" + x].Value = data1.Rows[i]["MonthlyRate"]; //misc amount
                    WOAllocation["M" + x].Value = data1.Rows[i]["PayrollCost"];
                    WOAllocation["N" + x].Value = data1.Rows[i]["BasicPay"]; //overtime
                    WOAllocation["O" + x].Value = data1.Rows[i]["AbsentAmount"];
                    WOAllocation["P" + x].Value = data1.Rows[i]["TardyAmount"]; //other tx income
                    WOAllocation["Q" + x].Value = data1.Rows[i]["UndertimeAmount"]; //adjustments
                    WOAllocation["R" + x].Value = data1.Rows[i]["LWOPAmount"]; //gross income
                    WOAllocation["S" + x].Value = data1.Rows[i]["WorkHoursAmount"]; //withholding tax
                    WOAllocation["T" + x].Value = data1.Rows[i]["NDOvertimeAmount"]; //net salary after tax
                    WOAllocation["U" + x].Value = data1.Rows[i]["OvertimeAmount"]; //employee sss
                    WOAllocation["V" + x].Value = data1.Rows[i]["PerfBonusNonAdmin"];
                    WOAllocation["W" + x].Value = data1.Rows[i]["Pay13TMonth"];
                    WOAllocation["X" + x].Value = data1.Rows[i]["PerfBonusAdmin"];
                    WOAllocation["Y" + x].Value = data1.Rows[i]["Maternity"];
                    WOAllocation["Z" + x].Value = data1.Rows[i]["NetPayAdjustment"];
                    WOAllocation["AA" + x].Value = data1.Rows[i]["Reimbursement"];
                    WOAllocation["AB" + x].Value = data1.Rows[i]["SSSSLADJ_NTX"];
                    WOAllocation["AC" + x].Value = data1.Rows[i]["BasicADJ"];
                    WOAllocation["AD" + x].Value = data1.Rows[i]["Maternity_SALDIF"];

                    WOAllocation["AE" + x].Value = data1.Rows[i]["Pay13TMonthNONAdmin"];
                    WOAllocation["AF" + x].Value = data1.Rows[i]["STIP"];
                    WOAllocation["AG" + x].Value = data1.Rows[i]["Pay13thADJNONAdmin"];
                    WOAllocation["AH" + x].Value = data1.Rows[i]["BEREAVEMENT"];
                    WOAllocation["AI" + x].Value = data1.Rows[i]["Pay13thADJAdmin"];
                    WOAllocation["AJ" + x].Value = data1.Rows[i]["DEMINIMIS"];
                    WOAllocation["AK" + x].Value = data1.Rows[i]["LAUNDRYALLOW"];
                    WOAllocation["AL" + x].Value = data1.Rows[i]["LAUNDRYALLOWADJ"];
                    WOAllocation["AM" + x].Value = data1.Rows[i]["MEALALLOW"];
                    WOAllocation["AN" + x].Value = data1.Rows[i]["MEALALLOWADJ"];
                    WOAllocation["AO" + x].Value = data1.Rows[i]["RICESUBSIDY"];
                    WOAllocation["AP" + x].Value = data1.Rows[i]["RiceADJ"]; //employee mcr(philhealth)
                    WOAllocation["AQ" + x].Value = data1.Rows[i]["TranspoTX"]; //employee pagibig
                    WOAllocation["AR" + x].Value = data1.Rows[i]["  TRANSPOALLOW_NTX"];

                    WOAllocation["AS" + x].Value = data1.Rows[i]["CommunicationAllowance"]; //other ntx income
                    WOAllocation["AT" + x].Value = data1.Rows[i]["OnCallAllowance"]; //loan payments
                    WOAllocation["AU" + x].Value = data1.Rows[i]["OtherAllowance"]; //deductions
                    WOAllocation["AV" + x].Value = data1.Rows[i]["OtherIncomeTax"]; //net salary
                    WOAllocation["AW" + x].Value = data1.Rows[i]["USRNRETENTION"]; //employer sss
                    WOAllocation["AX" + x].Value = data1.Rows[i]["Referral"]; //employer mcr(philhealth)
                    WOAllocation["AY" + x].Value = data1.Rows[i]["HMO"]; //employer ec
                    WOAllocation["AZ" + x].Value = data1.Rows[i]["HMOCard"]; //employer pagibig
                    WOAllocation["BA" + x].Value = data1.Rows[i]["CompanyID"]; //payroll cost
                    WOAllocation["BB" + x].Value = data1.Rows[i]["CompanyBadge"]; //payment type
                    WOAllocation["BC" + x].Value = data1.Rows[i]["BLDGBadge"]; //bank acct#
                    WOAllocation["BD" + x].Value = data1.Rows[i]["RetirementFund"]; //bank name
                    WOAllocation["BE" + x].Value = data1.Rows[i]["SSSSLAmount"]; //date employed
                    WOAllocation["BF" + x].Value = data1.Rows[i]["SSSCLAmount"]; //date terminated
                    WOAllocation["BG" + x].Value = data1.Rows[i]["SSSSLADJAmount"]; //cost center
                    WOAllocation["BH" + x].Value = data1.Rows[i]["SSSCLADJAmount"];
                    WOAllocation["BI" + x].Value = data1.Rows[i]["PAGMPLAmount"]; //currency
                    WOAllocation["BJ" + x].Value = data1.Rows[i]["PAGMPLADJAmount"]; //exchange rate
                    WOAllocation["BK" + x].Value = data1.Rows[i]["CODADV"]; //payment freq
                    WOAllocation["BL" + x].Value = data1.Rows[i]["FLUVACCINE"];
                    WOAllocation["BM" + x].Value = data1.Rows[i]["NEGNETPAY"];
                    WOAllocation["BN" + x].Value = data1.Rows[i]["SSSSICKNESS"];
                    WOAllocation["BO" + x].Value = data1.Rows[i]["SSSMATERNITY"];
                    WOAllocation["BP" + x].Value = data1.Rows[i]["ITSUPPLIES"];
                    WOAllocation["BQ" + x].Value = data1.Rows[i]["PHONECHARGE"];
                    WOAllocation["BR" + x].Value = data1.Rows[i]["OtherDED"];
                    WOAllocation["BS" + x].Value = data1.Rows[i]["GOVTSTATUTORYDEDUCTION"];
                    WOAllocation["BT" + x].Value = data1.Rows[i]["SSSSALARYDIFFERENTIAL"];
                    WOAllocation["BU" + x].Value = data1.Rows[i]["LOANADJ"];
                    WOAllocation["BV" + x].Value = data1.Rows[i]["Tax Refund"];
                    WOAllocation["BW" + x].Value = data1.Rows[i]["Tax Payable"];
                    WOAllocation["BX" + x].Value = data1.Rows[i]["PAGCAL"];
                    WOAllocation["BY" + x].Value = data1.Rows[i]["SSSSLERP"];
                    WOAllocation["BZ" + x].Value = data1.Rows[i]["COSL"];
                    WOAllocation["CA" + x].Value = data1.Rows[i]["PhilHealthERAmount"];
                    WOAllocation["CB" + x].Value = data1.Rows[i]["PagibigERAmount"];
                    WOAllocation["CC" + x].Value = data1.Rows[i]["SSS"];
                    WOAllocation["CD" + x].Value = data1.Rows[i]["PhilHealth"];
                    WOAllocation["CE" + x].Value = data1.Rows[i]["TotalSSS"];
                    WOAllocation["CF" + x].Value = data1.Rows[i]["Pagibig"];
                    WOAllocation["CG" + x].Value = data1.Rows[i]["WHTax"];
                    WOAllocation["CH" + x].Value = data1.Rows[i]["NetPay"];








                }
                #endregion
                TempConnectionStringv4(param.db);
                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@TenantID", mytype = SqlDbType.NVarChar, Value = param.TenantID });
                con.myparameters.Add(new myParameters { ParameterName = "@CutoffStart", mytype = SqlDbType.NVarChar, Value = param.CutoffStart });
                con.myparameters.Add(new myParameters { ParameterName = "@CutoffEnd", mytype = SqlDbType.NVarChar, Value = param.CutoffEnd });
                con.myparameters.Add(new myParameters { ParameterName = "@Status", mytype = SqlDbType.NVarChar, Value = param.EmpStatus });
                DataTable data2 = con.GetDataTable("sp_jv_Allocationv2");
                x = 3;
                for (int i = 0; i < data2.Rows.Count; i++, x++)
                {

                    Allocation["A" + x].Text = data2.Rows[i]["PayrollNum"].ToString(); //payroll#
                    Allocation["B" + x].Text = data2.Rows[i]["PayDate"].ToString(); //paydate
                    Allocation["C" + x].Text = data2.Rows[i]["PayCode"].ToString(); //pay code
                    Allocation["D" + x].Text = data2.Rows[i]["userID"].ToString(); //employee id
                    Allocation["E" + x].Text = data2.Rows[i]["LastName"].ToString(); //last name
                    Allocation["F" + x].Text = data2.Rows[i]["FirstName"].ToString(); //first name
                    Allocation["G" + x].Text = data2.Rows[i]["EmployeeType"].ToString(); //file status
                    Allocation["H" + x].Text = data2.Rows[i]["TaxStatus"].ToString(); //tax status
                    Allocation["I" + x].Value = data2.Rows[i]["BranchCode"]; //daily rate
                    Allocation["J" + x].Value = data2.Rows[i]["Allocation"]; //salary rate type
                    Allocation["K" + x].Value = data2.Rows[i]["DailyRate"]; //basic salary
                    Allocation["L" + x].Value = data2.Rows[i]["MonthlyRate"]; //misc amount
                    Allocation["M" + x].Value = data2.Rows[i]["PayrollCost"];
                    Allocation["N" + x].Value = data2.Rows[i]["BasicPay"]; //overtime
                    Allocation["O" + x].Value = data2.Rows[i]["AbsentAmount"];
                    Allocation["P" + x].Value = data2.Rows[i]["TardyAmount"]; //other tx income
                    Allocation["Q" + x].Value = data2.Rows[i]["UndertimeAmount"]; //adjustments
                    Allocation["R" + x].Value = data2.Rows[i]["LWOPAmount"]; //gross income
                    Allocation["S" + x].Value = data2.Rows[i]["WorkHoursAmount"]; //withholding tax
                    Allocation["T" + x].Value = data2.Rows[i]["NDOvertimeAmount"]; //net salary after tax
                    Allocation["U" + x].Value = data2.Rows[i]["OvertimeAmount"]; //employee sss
                    Allocation["V" + x].Value = data2.Rows[i]["PerfBonusNonAdmin"];
                    Allocation["W" + x].Value = data2.Rows[i]["Pay13TMonth"];
                    Allocation["X" + x].Value = data2.Rows[i]["PerfBonusAdmin"];
                    Allocation["Y" + x].Value = data2.Rows[i]["Maternity"];
                    Allocation["Z" + x].Value = data2.Rows[i]["NetPayAdjustment"];
                    Allocation["AA" + x].Value = data2.Rows[i]["Reimbursement"];
                    Allocation["AB" + x].Value = data2.Rows[i]["SSSSLADJ_NTX"];
                    Allocation["AC" + x].Value = data2.Rows[i]["BasicADJ"];
                    Allocation["AD" + x].Value = data2.Rows[i]["Maternity_SALDIF"];

                    Allocation["AE" + x].Value = data2.Rows[i]["Pay13TMonthNONAdmin"];
                    Allocation["AF" + x].Value = data2.Rows[i]["STIP"];
                    Allocation["AG" + x].Value = data2.Rows[i]["Pay13thADJNONAdmin"];
                    Allocation["AH" + x].Value = data2.Rows[i]["BEREAVEMENT"];
                    Allocation["AI" + x].Value = data2.Rows[i]["Pay13thADJAdmin"];
                    Allocation["AJ" + x].Value = data2.Rows[i]["DEMINIMIS"];
                    Allocation["AK" + x].Value = data2.Rows[i]["LAUNDRYALLOW"];
                    Allocation["AL" + x].Value = data2.Rows[i]["LAUNDRYALLOWADJ"];
                    Allocation["AN" + x].Value = data2.Rows[i]["MEALALLOW"];
                    Allocation["AN" + x].Value = data2.Rows[i]["MEALALLOWADJ"];
                    Allocation["AO" + x].Value = data2.Rows[i]["RICESUBSIDY"];
                    Allocation["AP" + x].Value = data2.Rows[i]["RiceADJ"]; //employee mcr(philhealth)
                    Allocation["AQ" + x].Value = data2.Rows[i]["TranspoTX"]; //employee pagibig
                    Allocation["AR" + x].Value = data2.Rows[i]["  TRANSPOALLOW_NTX"];

                    Allocation["AS" + x].Value = data2.Rows[i]["CommunicationAllowance"]; //other ntx income
                    Allocation["AT" + x].Value = data2.Rows[i]["OnCallAllowance"]; //loan payments
                    Allocation["AU" + x].Value = data2.Rows[i]["OtherAllowance"]; //deductions
                    Allocation["AV" + x].Value = data2.Rows[i]["OtherIncomeTax"]; //net salary
                    Allocation["AW" + x].Value = data2.Rows[i]["USRNRETENTION"]; //employer sss
                    Allocation["AX" + x].Value = data2.Rows[i]["Referral"]; //employer mcr(philhealth)
                    Allocation["AY" + x].Value = data2.Rows[i]["HMO"]; //employer ec
                    Allocation["AZ" + x].Value = data2.Rows[i]["HMOCard"]; //employer pagibig
                    Allocation["BA" + x].Value = data2.Rows[i]["CompanyID"]; //payroll cost
                    Allocation["BB" + x].Value = data2.Rows[i]["CompanyBadge"]; //payment type
                    Allocation["BC" + x].Value = data2.Rows[i]["BLDGBadge"]; //bank acct#
                    Allocation["BD" + x].Value = data2.Rows[i]["RetirementFund"]; //bank name
                    Allocation["BE" + x].Value = data2.Rows[i]["SSSSLAmount"]; //date employed
                    Allocation["BF" + x].Value = data2.Rows[i]["SSSCLAmount"]; //date terminated
                    Allocation["BG" + x].Value = data2.Rows[i]["SSSSLADJAmount"]; //cost center
                    Allocation["BH" + x].Value = data2.Rows[i]["SSSCLADJAmount"];
                    Allocation["BI" + x].Value = data2.Rows[i]["PAGMPLAmount"]; //currency
                    Allocation["BJ" + x].Value = data2.Rows[i]["PAGMPLADJAmount"]; //exchange rate
                    Allocation["BK" + x].Value = data2.Rows[i]["CODADV"]; //payment freq
                    Allocation["BL" + x].Value = data2.Rows[i]["FLUVACCINE"];
                    Allocation["BM" + x].Value = data2.Rows[i]["NEGNETPAY"];
                    Allocation["BN" + x].Value = data2.Rows[i]["SSSSICKNESS"];
                    Allocation["BO" + x].Value = data2.Rows[i]["SSSMATERNITY"];
                    Allocation["BP" + x].Value = data2.Rows[i]["ITSUPPLIES"];
                    Allocation["BQ" + x].Value = data2.Rows[i]["PHONECHARGE"];
                    Allocation["BR" + x].Value = data2.Rows[i]["OtherDED"];
                    Allocation["BS" + x].Value = data2.Rows[i]["GOVTSTATUTORYDEDUCTION"];
                    Allocation["BT" + x].Value = data2.Rows[i]["SSSSALARYDIFFERENTIAL"];
                    Allocation["BU" + x].Value = data2.Rows[i]["LOANADJ"];
                    Allocation["BV" + x].Value = data2.Rows[i]["Tax Refund"];
                    Allocation["BW" + x].Value = data2.Rows[i]["Tax Payable"];
                    Allocation["BX" + x].Value = data2.Rows[i]["PAGCAL"];
                    Allocation["BY" + x].Value = data2.Rows[i]["SSSSLERP"];
                    Allocation["BZ" + x].Value = data2.Rows[i]["COSL"];
                    Allocation["CA" + x].Value = data2.Rows[i]["PhilHealthERAmount"];
                    Allocation["CB" + x].Value = data2.Rows[i]["PagibigERAmount"];
                    Allocation["CC" + x].Value = data2.Rows[i]["SSS"];
                    Allocation["CD" + x].Value = data2.Rows[i]["PhilHealth"];
                    Allocation["CE" + x].Value = data2.Rows[i]["TotalSSS"];
                    Allocation["CF" + x].Value = data2.Rows[i]["Pagibig"];
                    Allocation["CG" + x].Value = data2.Rows[i]["WHTax"];
                    Allocation["CH" + x].Value = data2.Rows[i]["NetPay"];
                }



                //return "Success";
                TempConnectionStringv4(param.db);
                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@TenantID", mytype = SqlDbType.NVarChar, Value = param.TenantID });
                con.myparameters.Add(new myParameters { ParameterName = "@CutoffStart", mytype = SqlDbType.NVarChar, Value = param.CutoffStart });
                con.myparameters.Add(new myParameters { ParameterName = "@CutoffEnd", mytype = SqlDbType.NVarChar, Value = param.CutoffEnd });
                con.myparameters.Add(new myParameters { ParameterName = "@Status", mytype = SqlDbType.NVarChar, Value = param.EmpStatus });
                DataTable c1 = con.GetDataTable("sp_jv_c1v3");
                x = 2;
                for (int i = 0; i < c1.Rows.Count; i++, x++)
                {
                    sc1["A" + x].Text = c1.Rows[i]["userID"].ToString();
                    sc1["B" + x].Text = c1.Rows[i]["LastName"].ToString();
                    sc1["C" + x].Text = c1.Rows[i]["FirstName"].ToString();
                    sc1["D" + x].Text = c1.Rows[i]["Allocation"].ToString();
                    sc1["E" + x].Text = c1.Rows[i]["AccountNumber"].ToString();
                    sc1["F" + x].Text = c1.Rows[i]["AccountCode"].ToString();
                    sc1["G" + x].Value = c1.Rows[i]["Amount"];
                    sc1["H" + x].Value = c1.Rows[i]["Debit"];
                    sc1["I" + x].Value = c1.Rows[i]["credit"];
                    sc1["J" + x].Text = c1.Rows[i]["BranchCode"].ToString();
                }
                TempConnectionStringv4(param.db);
                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@TenantID", mytype = SqlDbType.NVarChar, Value = param.TenantID });
                con.myparameters.Add(new myParameters { ParameterName = "@CutoffStart", mytype = SqlDbType.NVarChar, Value = param.CutoffStart });
                con.myparameters.Add(new myParameters { ParameterName = "@CutoffEnd", mytype = SqlDbType.NVarChar, Value = param.CutoffEnd });
                con.myparameters.Add(new myParameters { ParameterName = "@Status", mytype = SqlDbType.NVarChar, Value = param.EmpStatus });
                DataTable c2 = con.GetDataTable("sp_jv_c2v3");
                x = 2;
                for (int i = 0; i < c2.Rows.Count; i++, x++)
                {
                    sc2["A" + x].Text = c2.Rows[i]["userID"].ToString();
                    sc2["B" + x].Text = c2.Rows[i]["LastName"].ToString();
                    sc2["C" + x].Text = c2.Rows[i]["FirstName"].ToString();
                    sc2["D" + x].Text = c2.Rows[i]["Allocation"].ToString();
                    sc2["E" + x].Text = c2.Rows[i]["AccountNumber"].ToString();
                    sc2["F" + x].Text = c2.Rows[i]["AccountCode"].ToString();
                    sc2["G" + x].Value = c2.Rows[i]["Amount"];
                    sc2["H" + x].Value = c2.Rows[i]["Debit"];
                    sc2["I" + x].Value = c2.Rows[i]["credit"];
                    sc2["J" + x].Text = c2.Rows[i]["BranchCode"].ToString();
                }
                TempConnectionStringv4(param.db);
                con = new Connection();
                //con.myparameters.Add(new myParameters { ParameterName = "@TenantID", mytype = SqlDbType.NVarChar, Value = param.TenantID });
                //con.myparameters.Add(new myParameters { ParameterName = "@CutoffStart", mytype = SqlDbType.NVarChar, Value = param.CutoffStart });
                //con.myparameters.Add(new myParameters { ParameterName = "@CutoffEnd", mytype = SqlDbType.NVarChar, Value = param.CutoffEnd });
                //con.myparameters.Add(new myParameters { ParameterName = "@Status", mytype = SqlDbType.NVarChar, Value = param.EmpStatus });
                DataTable sum = con.GetDataTable("sp_Get_JVSummary");
                x = 2;
                for (int i = 0; i < sum.Rows.Count; i++, x++)
                {
                    jvSummary["A" + x].Value = sum.Rows[i]["AccountNumber"];
                    jvSummary["B" + x].Value = sum.Rows[i]["AccountCode"];
                    jvSummary["C" + x].Value = sum.Rows[i]["Amount"];
                    jvSummary["D" + x].Value = sum.Rows[i]["Debit"];
                    jvSummary["E" + x].Value = sum.Rows[i]["credit"];


                }

                TempConnectionStringv4(param.db);
                con = new Connection();
                //con.myparameters.Add(new myParameters { ParameterName = "@TenantID", mytype = SqlDbType.NVarChar, Value = param.TenantID });
                //con.myparameters.Add(new myParameters { ParameterName = "@CutoffStart", mytype = SqlDbType.NVarChar, Value = param.CutoffStart });
                //con.myparameters.Add(new myParameters { ParameterName = "@CutoffEnd", mytype = SqlDbType.NVarChar, Value = param.CutoffEnd });
                //con.myparameters.Add(new myParameters { ParameterName = "@Status", mytype = SqlDbType.NVarChar, Value = param.EmpStatus });
                DataTable DTjv = con.GetDataTable("get_spJV");
                x = 2;
                for (int i = 0; i < DTjv.Rows.Count; i++, x++)
                {
                    jv["A" + x].Value = DTjv.Rows[i]["AccountNumber"];
                    jv["B" + x].Value = DTjv.Rows[i]["AccountCode"];
                    jv["C" + x].Value = DTjv.Rows[i]["Amount"];
                    jv["D" + x].Value = DTjv.Rows[i]["Debit"];
                    jv["E" + x].Value = DTjv.Rows[i]["credit"];
                    jv["F" + x].Value = DTjv.Rows[i]["BranchCode"];

                }
                HttpResponse httpResponse = System.Web.HttpContext.Current.Response;

                httpResponse.Clear();
                httpResponse.ContentType = "Application/x-msexcel";
                httpResponse.AddHeader("Content-Disposition", String.Format("attachment; filename={0}", filename));
                string newDIR = HttpContext.Current.Server.MapPath("~/sFTP/PayrollOutsourcing/");
                string onlineDIR = "http://ws.durusthr.com/ILM_WS_Live/sFTP/PayrollOutsourcing/";
                //filename = filename.Replace(',', '_');
                //filename = filename.Replace(" ", String.Empty);
                workbook.Save(newDIR + filename);
                stream.Close();
                if (data1.Rows.Count > 0 || data2.Rows.Count > 0)
                {
                    return onlineDIR + filename;
                }
                else
                {

                    return "No Rows";
                }
            }
            catch (Exception e)
            {
                stream.Close();
                return "Error: " + e.ToString();
            }
        }
        [HttpPost]
        [Route("ws_GetBulk_Benefits_ED")]
        public string GetBulk_Benefits_ED(edlUpdate param)
        {

            FileStream stream = null;
            try
            {
                DataTable DTParam = new DataTable();
                string datebuild = DateTime.Now.ToString("yyyyMM");
                string timebuild = DateTime.Now.ToString("hhtt");
                DTParam.Columns.AddRange(new DataColumn[1] { new DataColumn("empid") });
                for (int i = 0; i < param.EmpID.Length; i++)
                {
                    DTParam.Rows.Add(param.EmpID[i]);
                }

                string filename;
                string dataDir = HostingEnvironment.MapPath("~/Files/PayrollOutSourcing/InputFiles/");
                if (param.option == "Earnings")
                {
                    stream = new FileStream(dataDir + "Earnings_Edit.xls", FileMode.Open);
                    filename = "Employee_Earnings_" + datebuild + "_" + timebuild + ".xls";
                }
                else if (param.option == "Deductions")
                {

                    stream = new FileStream(dataDir + "Deductions_Edit.xls", FileMode.Open);
                    filename = "Employee_Deductions_" + datebuild + "_" + timebuild + ".xls";
                }
                else
                {
                    stream = new FileStream(dataDir + "Loan_Edit.xls", FileMode.Open);
                    filename = "Employee_Loan_" + datebuild + "_" + timebuild + ".xls";
                }


                //string datebuild = DateTime.Now.ToString("MMddyyyy");
                string cc = "";






                using (var fileStream = new FileStream(dataDir + filename, FileMode.Create, FileAccess.Write))
                //using (var fileStream = new FileStream(dataDir + filename, FileMode.Create, FileAccess.Write))
                {
                    stream.CopyTo(fileStream);
                }
                stream.Close();
                ExcelWorkbookFormat workbookFormat = ExcelWorkbookFormat.Xls_2003;
                string testDocFile = dataDir + filename;
                ExcelWorkbook workbook = new ExcelWorkbook(testDocFile);
                //  ExcelWorksheet firstWorksheet = workbook.Worksheets[0];




                ExcelWorksheet sheet6 = workbook.Worksheets[0];

                //Sheet7.WorksheetSecurity.ProtectWorksheet("sbc01");
                //AX TAXTABLE
                DataTable SixSheet;
                int x = 2;
                if (param.option != "Loans")
                {
                    TempConnectionStringv4(param.db);
                    Connection con = new Connection();
                    con.myparameters.Add(new myParameters { ParameterName = "@tenantID", mytype = SqlDbType.NVarChar, Value = param.tenantID });
                    con.myparameters.Add(new myParameters { ParameterName = "@option", mytype = SqlDbType.NVarChar, Value = param.option });
                    con.myparameters.Add(new myParameters { ParameterName = "@loginId", mytype = SqlDbType.NVarChar, Value = param.loginId });
                    con.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.Structured, Value = DTParam });
                    SixSheet = con.GetDataTable("sp_update_Earnings_Deduction");

                    x = 2;
                    for (int i = 0; i < SixSheet.Rows.Count; i++, x++)
                    {
                        sheet6["A" + x].Text = SixSheet.Rows[i]["EmpId"].ToString();
                        sheet6["B" + x].Text = SixSheet.Rows[i]["EarningsID"].ToString();
                        sheet6["C" + x].Text = SixSheet.Rows[i]["Fname"].ToString();
                        sheet6["D" + x].Text = SixSheet.Rows[i]["CreatedOn"].ToString();
                        sheet6["E" + x].Text = SixSheet.Rows[i]["PayrollType"].ToString();
                        sheet6["F" + x].Text = SixSheet.Rows[i]["Frequency"].ToString();
                        sheet6["H" + x].Text = SixSheet.Rows[i]["code"].ToString();
                        sheet6["I" + x].Text = SixSheet.Rows[i]["amount"].ToString();
                        sheet6["K" + x].Text = SixSheet.Rows[i]["recurstart"].ToString();
                        sheet6["M" + x].Text = SixSheet.Rows[i]["recurend"].ToString();
                        sheet6["O" + x].Text = SixSheet.Rows[i]["ClosedOn"].ToString();

                    }
                }
                else
                {
                    TempConnectionStringv4(param.db);
                    Connection con = new Connection();
                    con.myparameters.Add(new myParameters { ParameterName = "@tenantID", mytype = SqlDbType.NVarChar, Value = param.tenantID });
                    con.myparameters.Add(new myParameters { ParameterName = "@loginId", mytype = SqlDbType.NVarChar, Value = param.loginId });
                    con.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.Structured, Value = DTParam });
                    SixSheet = con.GetDataTable("sp_GetBulk_Benefits_Loans");

                    x = 2;
                    for (int i = 0; i < SixSheet.Rows.Count; i++, x++)
                    {
                        sheet6["A" + x].Text = SixSheet.Rows[i]["LoanID"].ToString();
                        sheet6["B" + x].Text = SixSheet.Rows[i]["EmpId"].ToString();
                        sheet6["C" + x].Text = SixSheet.Rows[i]["EmpName"].ToString();
                        sheet6["D" + x].Text = SixSheet.Rows[i]["CreatedOn"].ToString();
                        sheet6["E" + x].Text = SixSheet.Rows[i]["LoanNameCode"].ToString();
                        sheet6["F" + x].Text = SixSheet.Rows[i]["AmortizationAmount"].ToString();
                        sheet6["H" + x].Text = SixSheet.Rows[i]["Frequency"].ToString();
                        sheet6["J" + x].Text = SixSheet.Rows[i]["WithInterest"].ToString();
                        sheet6["L" + x].Text = SixSheet.Rows[i]["TotalPreviousPayments"].ToString();
                        sheet6["N" + x].Text = SixSheet.Rows[i]["StartDate"].ToString();
                        sheet6["P" + x].Text = SixSheet.Rows[i]["EndDate"].ToString();
                        sheet6["R" + x].Text = SixSheet.Rows[i]["ClosedOn"].ToString();



                    }
                }
                HttpResponse httpResponse = System.Web.HttpContext.Current.Response;

                httpResponse.Clear();
                httpResponse.ContentType = "Application/x-msexcel";
                httpResponse.AddHeader("Content-Disposition", String.Format("attachment; filename={0}", filename));
                string newDIR = HttpContext.Current.Server.MapPath("~/sFTP/PayrollOutsourcing/");
                string onlineDIR = "https://ws.durusthr.com/ILM_WS_Live/sFTP/PayrollOutsourcing/";
                //filename = filename.Replace(',', '_');
                //filename = filename.Replace(" ", String.Empty);
                workbook.Save(newDIR + filename);
                stream.Close();
                //return "Success";

                if (SixSheet.Rows.Count > 0)
                {
                    return onlineDIR + filename;
                }
                else
                {

                    return "No Rows";
                }
            }
            catch (Exception e)
            {
                stream.Close();
                return "Error: " + e.ToString();
            }
        }

        [HttpPost]
        [Route("Extract_JournalVoucherv4")]
        public string JournalVoucherv4(efile param)
        {

            FileStream stream = null;
            try
            {

                TempConnectionStringv4(param.db);
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@TenantID", mytype = SqlDbType.NVarChar, Value = param.TenantID });
                con.myparameters.Add(new myParameters { ParameterName = "@CutoffStart", mytype = SqlDbType.NVarChar, Value = param.CutoffStart });
                con.myparameters.Add(new myParameters { ParameterName = "@CutoffEnd", mytype = SqlDbType.NVarChar, Value = param.CutoffEnd });
                con.myparameters.Add(new myParameters { ParameterName = "@Status", mytype = SqlDbType.NVarChar, Value = param.EmpStatus });
                DataTable data1 = con.GetDataTable("sp_jv_woAllocationv3");


                string dataDir = HostingEnvironment.MapPath("~/Files/PayrollOutSourcing/");

                stream = new FileStream(dataDir + "Template_JV.xls", FileMode.Open);
                string filename;
                // string datebuild = DateTime.Now.ToString("MMddyyyy");
                string cc = param.cCode;


                string cStart = Regex.Replace(param.CutoffStart, @"[^0-9a-zA-Z]+", "");
                string cEnd = Regex.Replace(param.CutoffEnd, @"[^0-9a-zA-Z]+", "");


                string datebuild = DateTime.Now.ToString("yyyyMM");
                string timebuild = DateTime.Now.ToString("hhtt");
                filename = cc + "_JV_" + cStart + "_" + cEnd + "_" + timebuild + ".xls";
                using (var fileStream = new FileStream(dataDir + filename, FileMode.Create, FileAccess.Write))
                //using (var fileStream = new FileStream(dataDir + filename, FileMode.Create, FileAccess.Write))
                {
                    stream.CopyTo(fileStream);
                }
                stream.Close();
                ExcelWorkbookFormat workbookFormat = ExcelWorkbookFormat.Xls_2003;
                string testDocFile = dataDir + "Template_JV.xls";
                ExcelWorkbook workbook = new ExcelWorkbook(testDocFile);
                ExcelWorksheet firstWorksheet = workbook.Worksheets[0];

                #region PayReg Sheet

                ExcelWorksheet WOAllocation = workbook.Worksheets[0];
                ExcelWorksheet Allocation = workbook.Worksheets[1];
                ExcelWorksheet sc1 = workbook.Worksheets[2];
                ExcelWorksheet sc2 = workbook.Worksheets[3];
                ExcelWorksheet jv = workbook.Worksheets[4];
                ExcelWorksheet jvSummary = workbook.Worksheets[5];
                //Sheet7.WorksheetSecurity.ProtectWorksheet("sbc01");
                //AX TAXTABLE

                int x = 3;
                for (int i = 0; i < data1.Rows.Count; i++, x++)
                {

                    WOAllocation["A" + x].Text = data1.Rows[i]["PayrollNum"].ToString(); //payroll#
                    WOAllocation["B" + x].Text = data1.Rows[i]["PayDate"].ToString(); //paydate
                    WOAllocation["C" + x].Text = data1.Rows[i]["PayCode"].ToString(); //pay code
                    WOAllocation["D" + x].Text = data1.Rows[i]["userID"].ToString(); //employee id
                    WOAllocation["E" + x].Text = data1.Rows[i]["LastName"].ToString(); //last name
                    WOAllocation["F" + x].Text = data1.Rows[i]["FirstName"].ToString(); //first name
                    WOAllocation["G" + x].Text = data1.Rows[i]["EmployeeType"].ToString(); //file status
                    WOAllocation["H" + x].Text = data1.Rows[i]["TaxStatus"].ToString(); //tax status
                    WOAllocation["I" + x].Value = data1.Rows[i]["BranchCode"]; //daily rate
                    WOAllocation["J" + x].Value = data1.Rows[i]["Allocation"]; //salary rate type
                    WOAllocation["K" + x].Value = data1.Rows[i]["DailyRate"]; //basic salary
                    WOAllocation["L" + x].Value = data1.Rows[i]["MonthlyRate"]; //misc amount
                    WOAllocation["M" + x].Value = data1.Rows[i]["PayrollCost"];
                    WOAllocation["N" + x].Value = data1.Rows[i]["BasicPay"]; //overtime
                    WOAllocation["O" + x].Value = data1.Rows[i]["AbsentAmount"];
                    WOAllocation["P" + x].Value = data1.Rows[i]["TardyAmount"]; //other tx income
                    WOAllocation["Q" + x].Value = data1.Rows[i]["UndertimeAmount"]; //adjustments
                    WOAllocation["R" + x].Value = data1.Rows[i]["LWOPAmount"]; //gross income
                    WOAllocation["S" + x].Value = data1.Rows[i]["WorkHoursAmount"]; //withholding tax
                    WOAllocation["T" + x].Value = data1.Rows[i]["NDOvertimeAmount"]; //net salary after tax
                    WOAllocation["U" + x].Value = data1.Rows[i]["OvertimeAmount"]; //employee sss
                    WOAllocation["V" + x].Value = data1.Rows[i]["PerfBonusNonAdmin"];
                    WOAllocation["W" + x].Value = data1.Rows[i]["Pay13TMonth"];
                    WOAllocation["X" + x].Value = data1.Rows[i]["PerfBonusAdmin"];
                    WOAllocation["Y" + x].Value = data1.Rows[i]["Maternity"];
                    WOAllocation["Z" + x].Value = data1.Rows[i]["NetPayAdjustment"];
                    WOAllocation["AA" + x].Value = data1.Rows[i]["Reimbursement"];
                    WOAllocation["AB" + x].Value = data1.Rows[i]["SSSSLADJ_NTX"];
                    WOAllocation["AC" + x].Value = data1.Rows[i]["BasicADJ"];
                    WOAllocation["AD" + x].Value = data1.Rows[i]["Maternity_SALDIF"];

                    WOAllocation["AE" + x].Value = data1.Rows[i]["Pay13TMonthNONAdmin"];
                    WOAllocation["AF" + x].Value = data1.Rows[i]["STIP"];
                    WOAllocation["AG" + x].Value = data1.Rows[i]["Pay13thADJNONAdmin"];
                    WOAllocation["AH" + x].Value = data1.Rows[i]["BEREAVEMENT"];
                    WOAllocation["AI" + x].Value = data1.Rows[i]["Pay13thADJAdmin"];
                    WOAllocation["AJ" + x].Value = data1.Rows[i]["LAUNDRYALLOW"];
                    WOAllocation["AK" + x].Value = data1.Rows[i]["LAUNDRYALLOWADJ"];
                    WOAllocation["AL" + x].Value = data1.Rows[i]["MEALALLOW"];
                    WOAllocation["AM" + x].Value = data1.Rows[i]["MEALALLOWADJ"];
                    WOAllocation["AN" + x].Value = data1.Rows[i]["RICESUBSIDY"];
                    WOAllocation["AO" + x].Value = data1.Rows[i]["RiceADJ"]; //employee mcr(philhealth)
                    WOAllocation["AP" + x].Value = data1.Rows[i]["TranspoTX"]; //employee pagibig
                    WOAllocation["AQ" + x].Value = data1.Rows[i]["  TRANSPOALLOW_NTX"];

                    WOAllocation["AR" + x].Value = data1.Rows[i]["CommunicationAllowance"]; //other ntx income
                    WOAllocation["AS" + x].Value = data1.Rows[i]["OnCallAllowance"]; //loan payments
                    WOAllocation["AT" + x].Value = data1.Rows[i]["OtherAllowance"]; //deductions
                    WOAllocation["AU" + x].Value = data1.Rows[i]["OtherIncomeTax"]; //net salary
                    WOAllocation["AV" + x].Value = data1.Rows[i]["USRNRETENTION"]; //employer sss
                    WOAllocation["AW" + x].Value = data1.Rows[i]["Referral"]; //employer mcr(philhealth)
                    WOAllocation["AX" + x].Value = data1.Rows[i]["HMO"]; //employer ec
                    WOAllocation["AY" + x].Value = data1.Rows[i]["HMOCard"]; //employer pagibig
                    WOAllocation["AZ" + x].Value = data1.Rows[i]["CompanyID"]; //payroll cost
                    WOAllocation["BA" + x].Value = data1.Rows[i]["CompanyBadge"]; //payment type
                    WOAllocation["BB" + x].Value = data1.Rows[i]["BLDGBadge"]; //bank acct#
                    WOAllocation["BC" + x].Value = data1.Rows[i]["RetirementFund"]; //bank name
                    WOAllocation["BD" + x].Value = data1.Rows[i]["SSSSLAmount"]; //date employed
                    WOAllocation["BE" + x].Value = data1.Rows[i]["SSSCLAmount"]; //date terminated
                    WOAllocation["BF" + x].Value = data1.Rows[i]["SSSSLADJAmount"]; //cost center
                    WOAllocation["BG" + x].Value = data1.Rows[i]["SSSCLADJAmount"];
                    WOAllocation["BH" + x].Value = data1.Rows[i]["PAGMPLAmount"]; //currency
                    WOAllocation["BI" + x].Value = data1.Rows[i]["PAGMPLADJAmount"]; //exchange rate
                    WOAllocation["BJ" + x].Value = data1.Rows[i]["CODADV"]; //payment freq
                    WOAllocation["BK" + x].Value = data1.Rows[i]["FLUVACCINE"];
                    WOAllocation["BL" + x].Value = data1.Rows[i]["NEGNETPAY"];
                    WOAllocation["BM" + x].Value = data1.Rows[i]["SSSSICKNESS"];
                    WOAllocation["BN" + x].Value = data1.Rows[i]["SSSMATERNITY"];
                    WOAllocation["BO" + x].Value = data1.Rows[i]["ITSUPPLIES"];
                    WOAllocation["BP" + x].Value = data1.Rows[i]["PHONECHARGE"];
                    WOAllocation["BQ" + x].Value = data1.Rows[i]["OtherDED"];
                    WOAllocation["BR" + x].Value = data1.Rows[i]["GOVTSTATUTORYDEDUCTION"];
                    WOAllocation["BS" + x].Value = data1.Rows[i]["SSSSALARYDIFFERENTIAL"];
                    WOAllocation["BT" + x].Value = data1.Rows[i]["LOANADJ"];
                    WOAllocation["BU" + x].Value = data1.Rows[i]["Tax Refund"];
                    WOAllocation["BV" + x].Value = data1.Rows[i]["Tax Payable"];
                    WOAllocation["BW" + x].Value = data1.Rows[i]["PAGCAL"];
                    WOAllocation["BX" + x].Value = data1.Rows[i]["SSSSLERP"];
                    WOAllocation["BY" + x].Value = data1.Rows[i]["COSL"];
                    WOAllocation["BZ" + x].Value = data1.Rows[i]["PhilHealthERAmount"];
                    WOAllocation["CA" + x].Value = data1.Rows[i]["PagibigERAmount"];
                    WOAllocation["CB" + x].Value = data1.Rows[i]["SSS"];
                    WOAllocation["CC" + x].Value = data1.Rows[i]["PhilHealth"];
                    WOAllocation["CD" + x].Value = data1.Rows[i]["TotalSSS"];
                    WOAllocation["CE" + x].Value = data1.Rows[i]["Pagibig"];
                    WOAllocation["CF" + x].Value = data1.Rows[i]["WHTax"];
                    WOAllocation["CG" + x].Value = data1.Rows[i]["NetPay"];





                }
                #endregion
                TempConnectionStringv4(param.db);
                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@TenantID", mytype = SqlDbType.NVarChar, Value = param.TenantID });
                con.myparameters.Add(new myParameters { ParameterName = "@CutoffStart", mytype = SqlDbType.NVarChar, Value = param.CutoffStart });
                con.myparameters.Add(new myParameters { ParameterName = "@CutoffEnd", mytype = SqlDbType.NVarChar, Value = param.CutoffEnd });
                con.myparameters.Add(new myParameters { ParameterName = "@Status", mytype = SqlDbType.NVarChar, Value = param.EmpStatus });
                DataTable data2 = con.GetDataTable("sp_jv_Allocationv3");
                x = 3;
                for (int i = 0; i < data2.Rows.Count; i++, x++)
                {

                    Allocation["A" + x].Text = data2.Rows[i]["PayrollNum"].ToString(); //payroll#
                    Allocation["B" + x].Text = data2.Rows[i]["PayDate"].ToString(); //paydate
                    Allocation["C" + x].Text = data2.Rows[i]["PayCode"].ToString(); //pay code
                    Allocation["D" + x].Text = data2.Rows[i]["userID"].ToString(); //employee id
                    Allocation["E" + x].Text = data2.Rows[i]["LastName"].ToString(); //last name
                    Allocation["F" + x].Text = data2.Rows[i]["FirstName"].ToString(); //first name
                    Allocation["G" + x].Text = data2.Rows[i]["EmployeeType"].ToString(); //file status
                    Allocation["H" + x].Text = data2.Rows[i]["TaxStatus"].ToString(); //tax status
                    Allocation["I" + x].Value = data2.Rows[i]["BranchCode"]; //daily rate
                    Allocation["J" + x].Value = data2.Rows[i]["Allocation"]; //salary rate type
                    Allocation["K" + x].Value = data2.Rows[i]["DailyRate"]; //basic salary
                    Allocation["L" + x].Value = data2.Rows[i]["MonthlyRate"]; //misc amount
                    Allocation["M" + x].Value = data2.Rows[i]["PayrollCost"];
                    Allocation["N" + x].Value = data2.Rows[i]["BasicPay"]; //overtime
                    Allocation["O" + x].Value = data2.Rows[i]["AbsentAmount"];
                    Allocation["P" + x].Value = data2.Rows[i]["TardyAmount"]; //other tx income
                    Allocation["Q" + x].Value = data2.Rows[i]["UndertimeAmount"]; //adjustments
                    Allocation["R" + x].Value = data2.Rows[i]["LWOPAmount"]; //gross income
                    Allocation["S" + x].Value = data2.Rows[i]["WorkHoursAmount"]; //withholding tax
                    Allocation["T" + x].Value = data2.Rows[i]["NDOvertimeAmount"]; //net salary after tax
                    Allocation["U" + x].Value = data2.Rows[i]["OvertimeAmount"]; //employee sss
                    Allocation["V" + x].Value = data2.Rows[i]["PerfBonusNonAdmin"];
                    Allocation["W" + x].Value = data2.Rows[i]["Pay13TMonth"];
                    Allocation["X" + x].Value = data2.Rows[i]["PerfBonusAdmin"];
                    Allocation["Y" + x].Value = data2.Rows[i]["Maternity"];
                    Allocation["Z" + x].Value = data2.Rows[i]["NetPayAdjustment"];
                    Allocation["AA" + x].Value = data2.Rows[i]["Reimbursement"];
                    Allocation["AB" + x].Value = data2.Rows[i]["SSSSLADJ_NTX"];
                    Allocation["AC" + x].Value = data2.Rows[i]["BasicADJ"];
                    Allocation["AD" + x].Value = data2.Rows[i]["Maternity_SALDIF"];

                    Allocation["AE" + x].Value = data2.Rows[i]["Pay13TMonthNONAdmin"];
                    Allocation["AF" + x].Value = data2.Rows[i]["STIP"];
                    Allocation["AG" + x].Value = data2.Rows[i]["Pay13thADJNONAdmin"];
                    Allocation["AH" + x].Value = data2.Rows[i]["BEREAVEMENT"];
                    Allocation["AI" + x].Value = data2.Rows[i]["Pay13thADJAdmin"];
                    Allocation["AJ" + x].Value = data2.Rows[i]["LAUNDRYALLOW"];
                    Allocation["AK" + x].Value = data2.Rows[i]["LAUNDRYALLOWADJ"];
                    Allocation["AL" + x].Value = data2.Rows[i]["MEALALLOW"];
                    Allocation["AM" + x].Value = data2.Rows[i]["MEALALLOWADJ"];
                    Allocation["AN" + x].Value = data2.Rows[i]["RICESUBSIDY"];
                    Allocation["AO" + x].Value = data2.Rows[i]["RiceADJ"]; //employee mcr(philhealth)
                    Allocation["AP" + x].Value = data2.Rows[i]["TranspoTX"]; //employee pagibig
                    Allocation["AQ" + x].Value = data2.Rows[i]["  TRANSPOALLOW_NTX"];

                    Allocation["AR" + x].Value = data2.Rows[i]["CommunicationAllowance"]; //other ntx income
                    Allocation["AS" + x].Value = data2.Rows[i]["OnCallAllowance"]; //loan payments
                    Allocation["AT" + x].Value = data2.Rows[i]["OtherAllowance"]; //deductions
                    Allocation["AU" + x].Value = data2.Rows[i]["OtherIncomeTax"]; //net salary
                    Allocation["AV" + x].Value = data2.Rows[i]["USRNRETENTION"]; //employer sss
                    Allocation["AW" + x].Value = data2.Rows[i]["Referral"]; //employer mcr(philhealth)
                    Allocation["AX" + x].Value = data2.Rows[i]["HMO"]; //employer ec
                    Allocation["AY" + x].Value = data2.Rows[i]["HMOCard"]; //employer pagibig
                    Allocation["AZ" + x].Value = data2.Rows[i]["CompanyID"]; //payroll cost
                    Allocation["BA" + x].Value = data2.Rows[i]["CompanyBadge"]; //payment type
                    Allocation["BB" + x].Value = data2.Rows[i]["BLDGBadge"]; //bank acct#
                    Allocation["BC" + x].Value = data2.Rows[i]["RetirementFund"]; //bank name
                    Allocation["BD" + x].Value = data2.Rows[i]["SSSSLAmount"]; //date employed
                    Allocation["BE" + x].Value = data2.Rows[i]["SSSCLAmount"]; //date terminated
                    Allocation["BF" + x].Value = data2.Rows[i]["SSSSLADJAmount"]; //cost center
                    Allocation["BG" + x].Value = data2.Rows[i]["SSSCLADJAmount"];
                    Allocation["BH" + x].Value = data2.Rows[i]["PAGMPLAmount"]; //currency
                    Allocation["BI" + x].Value = data2.Rows[i]["PAGMPLADJAmount"]; //exchange rate
                    Allocation["BJ" + x].Value = data2.Rows[i]["CODADV"]; //payment freq
                    Allocation["BK" + x].Value = data2.Rows[i]["FLUVACCINE"];
                    Allocation["BL" + x].Value = data2.Rows[i]["NEGNETPAY"];
                    Allocation["BM" + x].Value = data2.Rows[i]["SSSSICKNESS"];
                    Allocation["BN" + x].Value = data2.Rows[i]["SSSMATERNITY"];
                    Allocation["BO" + x].Value = data2.Rows[i]["ITSUPPLIES"];
                    Allocation["BP" + x].Value = data2.Rows[i]["PHONECHARGE"];
                    Allocation["BQ" + x].Value = data2.Rows[i]["OtherDED"];
                    Allocation["BR" + x].Value = data2.Rows[i]["GOVTSTATUTORYDEDUCTION"];
                    Allocation["BS" + x].Value = data2.Rows[i]["SSSSALARYDIFFERENTIAL"];
                    Allocation["BT" + x].Value = data2.Rows[i]["LOANADJ"];
                    Allocation["BU" + x].Value = data2.Rows[i]["Tax Refund"];
                    Allocation["BV" + x].Value = data2.Rows[i]["Tax Payable"];
                    Allocation["BW" + x].Value = data2.Rows[i]["PAGCAL"];
                    Allocation["BX" + x].Value = data2.Rows[i]["SSSSLERP"];
                    Allocation["BY" + x].Value = data2.Rows[i]["COSL"];
                    Allocation["BZ" + x].Value = data2.Rows[i]["PhilHealthERAmount"];
                    Allocation["CA" + x].Value = data2.Rows[i]["PagibigERAmount"];
                    Allocation["CB" + x].Value = data2.Rows[i]["SSS"];
                    Allocation["CC" + x].Value = data2.Rows[i]["PhilHealth"];
                    Allocation["CD" + x].Value = data2.Rows[i]["TotalSSS"];
                    Allocation["CE" + x].Value = data2.Rows[i]["Pagibig"];
                    Allocation["CF" + x].Value = data2.Rows[i]["WHTax"];
                    Allocation["CG" + x].Value = data2.Rows[i]["NetPay"];
                }



                //return "Success";
                TempConnectionStringv4(param.db);
                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@TenantID", mytype = SqlDbType.NVarChar, Value = param.TenantID });
                con.myparameters.Add(new myParameters { ParameterName = "@CutoffStart", mytype = SqlDbType.NVarChar, Value = param.CutoffStart });
                con.myparameters.Add(new myParameters { ParameterName = "@CutoffEnd", mytype = SqlDbType.NVarChar, Value = param.CutoffEnd });
                con.myparameters.Add(new myParameters { ParameterName = "@Status", mytype = SqlDbType.NVarChar, Value = param.EmpStatus });
                DataTable c1 = con.GetDataTable("sp_jv_c1v4");
                x = 2;
                for (int i = 0; i < c1.Rows.Count; i++, x++)
                {
                    sc1["A" + x].Text = c1.Rows[i]["userID"].ToString();
                    sc1["B" + x].Text = c1.Rows[i]["LastName"].ToString();
                    sc1["C" + x].Text = c1.Rows[i]["FirstName"].ToString();
                    sc1["D" + x].Text = c1.Rows[i]["Allocation"].ToString();
                    sc1["E" + x].Text = c1.Rows[i]["AccountNumber"].ToString();
                    sc1["F" + x].Text = c1.Rows[i]["AccountCode"].ToString();
                    sc1["G" + x].Value = c1.Rows[i]["Amount"];
                    sc1["H" + x].Value = c1.Rows[i]["Debit"];
                    sc1["I" + x].Value = c1.Rows[i]["credit"];
                    sc1["J" + x].Text = c1.Rows[i]["BranchCode"].ToString();
                }
                TempConnectionStringv4(param.db);
                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@TenantID", mytype = SqlDbType.NVarChar, Value = param.TenantID });
                con.myparameters.Add(new myParameters { ParameterName = "@CutoffStart", mytype = SqlDbType.NVarChar, Value = param.CutoffStart });
                con.myparameters.Add(new myParameters { ParameterName = "@CutoffEnd", mytype = SqlDbType.NVarChar, Value = param.CutoffEnd });
                con.myparameters.Add(new myParameters { ParameterName = "@Status", mytype = SqlDbType.NVarChar, Value = param.EmpStatus });
                DataTable c2 = con.GetDataTable("sp_jv_c2v4");
                x = 2;
                for (int i = 0; i < c2.Rows.Count; i++, x++)
                {
                    sc2["A" + x].Text = c2.Rows[i]["userID"].ToString();
                    sc2["B" + x].Text = c2.Rows[i]["LastName"].ToString();
                    sc2["C" + x].Text = c2.Rows[i]["FirstName"].ToString();
                    sc2["D" + x].Text = c2.Rows[i]["Allocation"].ToString();
                    sc2["E" + x].Text = c2.Rows[i]["AccountNumber"].ToString();
                    sc2["F" + x].Text = c2.Rows[i]["AccountCode"].ToString();
                    sc2["G" + x].Value = c2.Rows[i]["Amount"];
                    sc2["H" + x].Value = c2.Rows[i]["Debit"];
                    sc2["I" + x].Value = c2.Rows[i]["credit"];
                    sc2["J" + x].Text = c2.Rows[i]["BranchCode"].ToString();
                }

                TempConnectionStringv4(param.db);
                con = new Connection();
                //con.myparameters.Add(new myParameters { ParameterName = "@TenantID", mytype = SqlDbType.NVarChar, Value = param.TenantID });
                //con.myparameters.Add(new myParameters { ParameterName = "@CutoffStart", mytype = SqlDbType.NVarChar, Value = param.CutoffStart });
                //con.myparameters.Add(new myParameters { ParameterName = "@CutoffEnd", mytype = SqlDbType.NVarChar, Value = param.CutoffEnd });
                //con.myparameters.Add(new myParameters { ParameterName = "@Status", mytype = SqlDbType.NVarChar, Value = param.EmpStatus });
                DataTable sum = con.GetDataTable("sp_Get_JVSummary");
                x = 2;
                for (int i = 0; i < sum.Rows.Count; i++, x++)
                {
                    jvSummary["A" + x].Value = sum.Rows[i]["AccountNumber"];
                    jvSummary["B" + x].Value = sum.Rows[i]["AccountCode"];
                    jvSummary["C" + x].Value = sum.Rows[i]["Amount"];
                    jvSummary["D" + x].Value = sum.Rows[i]["Debit"];
                    jvSummary["E" + x].Value = sum.Rows[i]["credit"];


                }

                TempConnectionStringv4(param.db);
                con = new Connection();
                //con.myparameters.Add(new myParameters { ParameterName = "@TenantID", mytype = SqlDbType.NVarChar, Value = param.TenantID });
                //con.myparameters.Add(new myParameters { ParameterName = "@CutoffStart", mytype = SqlDbType.NVarChar, Value = param.CutoffStart });
                //con.myparameters.Add(new myParameters { ParameterName = "@CutoffEnd", mytype = SqlDbType.NVarChar, Value = param.CutoffEnd });
                //con.myparameters.Add(new myParameters { ParameterName = "@Status", mytype = SqlDbType.NVarChar, Value = param.EmpStatus });
                DataTable DTjv = con.GetDataTable("get_spJV");
                x = 2;
                for (int i = 0; i < DTjv.Rows.Count; i++, x++)
                {
                    jv["A" + x].Value = DTjv.Rows[i]["AccountNumber"];
                    jv["B" + x].Value = DTjv.Rows[i]["AccountCode"];
                    jv["C" + x].Value = DTjv.Rows[i]["Amount"];
                    jv["D" + x].Value = DTjv.Rows[i]["Debit"];
                    jv["E" + x].Value = DTjv.Rows[i]["credit"];
                    jv["F" + x].Value = DTjv.Rows[i]["BranchCode"];

                }
                HttpResponse httpResponse = System.Web.HttpContext.Current.Response;

                httpResponse.Clear();
                httpResponse.ContentType = "Application/x-msexcel";
                httpResponse.AddHeader("Content-Disposition", String.Format("attachment; filename={0}", filename));
                string newDIR = HttpContext.Current.Server.MapPath("~/sFTP/PayrollOutsourcing/");
                string onlineDIR = "http://ws.durusthr.com/ILM_WS_Live/sFTP/PayrollOutsourcing/";
                //filename = filename.Replace(',', '_');
                //filename = filename.Replace(" ", String.Empty);

                workbook.Save(newDIR + filename);
                stream.Close();
                if (data1.Rows.Count > 0)
                {
                    return onlineDIR + filename;
                }
                else
                {

                    return "No Rows";
                }
            }
            catch (Exception e)
            {
                stream.Close();
                return "Error: " + e.ToString();
            }
        }

        [HttpPost]
        [Route("Retirement_Benefitv2")]
        public string Retirementv2(POLoansReport param)
        {
            FileStream stream = null;
            try
            {
           
                string dataDir = HostingEnvironment.MapPath("~/Files/PayrollOutSourcing/");
                string outDir = HostingEnvironment.MapPath("~/sFTP/PayrollOutSourcing/");



                stream = new FileStream(dataDir + "RetireBenefitTemplate.xls", FileMode.Open);
                string filename;
                string datebuild = DateTime.Now.ToString("MMddyyyy");
                string datebuild1 = DateTime.Now.ToString("yyyyMM");
                string timebuild = DateTime.Now.ToString("hhtt");



                DataTable DTParam = new DataTable();
                DTParam.Columns.AddRange(new DataColumn[1] { new DataColumn("empid") });
                for (int i = 0; i < param.empID.Length; i++)
                {
                    DTParam.Rows.Add(param.empID[i]);
                }
                TempConnectionStringv4(param.db);
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@year", mytype = SqlDbType.NVarChar, Value = param.Year });
                con.myparameters.Add(new myParameters { ParameterName = "@Userid", mytype = SqlDbType.Structured, Value = DTParam });
                DataTable LR = con.GetDataTable("sp_ComputeEmployeeYTD");

                filename = "RetireBenefit" + "_" + datebuild + "_" + timebuild + ".xls";
                using (var fileStream = new FileStream(outDir + filename, FileMode.Create, FileAccess.Write))
                //using (var fileStream = new FileStream(dataDir + filename, FileMode.Create, FileAccess.Write))
                {
                    stream.CopyTo(fileStream);
                }
                stream.Close();
                ExcelWorkbookFormat workbookFormat = ExcelWorkbookFormat.Xls_2003;
                string testDocFile = dataDir + "RetireBenefitTemplate.xls";
                ExcelWorkbook workbook = new ExcelWorkbook(testDocFile);
                ExcelWorksheet Retirement = workbook.Worksheets[0];





                int x = 2;
                for (int i = 0; i < LR.Rows.Count; i++, x++)
                {
                    Retirement["A" + x].Text = LR.Rows[i]["Transaction"].ToString();
                    Retirement["B" + x].Text = LR.Rows[i]["Entity"].ToString();
                    Retirement["C" + x].Text = LR.Rows[i]["EID"].ToString();
                    Retirement["D" + x].Text = LR.Rows[i]["Employee Name"].ToString();
                    Retirement["E" + x].Text = LR.Rows[i]["Tax Status"].ToString();
                    Retirement["F" + x].Value = LR.Rows[i]["No Of Dependents"].ToString();
                    Retirement["G" + x].Value = LR.Rows[i]["Latest Basic Salary"];
                    Retirement["H" + x].Value = LR.Rows[i]["Daily rate"];
                    Retirement["I" + x].Value = LR.Rows[i]["Total Taxes withheld for the year"];
                    Retirement["J" + x].Value = LR.Rows[i]["TIN"];
                    Retirement["K" + x].Value = LR.Rows[i]["SSS"];
                    Retirement["L" + x].Value = LR.Rows[i]["PHIC"];
                    Retirement["M" + x].Value = LR.Rows[i]["HDMF"];
                    Retirement["N" + x].Value = LR.Rows[i]["Gross Taxable Compensation Income for the year"];
                    Retirement["O" + x].Value = LR.Rows[i]["De Minimis"];
                    Retirement["P" + x].Value = LR.Rows[i]["Other"];
                    Retirement["Q" + x].Value = LR.Rows[i]["Non Taxable/Exempt Compensation Income"];
                    Retirement["R" + x].Value = LR.Rows[i]["Gross Compensation (sum of A.1 and A.2)"];

                }
          
                HttpResponse httpResponse = System.Web.HttpContext.Current.Response;



                httpResponse.Clear();
                httpResponse.ContentType = "Application/x-msexcel";
                httpResponse.AddHeader("Content-Disposition", String.Format("attachment; filename={0}", filename));
                string newDIR = HttpContext.Current.Server.MapPath("~/sFTP/PayrollOutsourcing/");
                string onlineDIR = "https://ws.durusthr.com/ILM_WS_Live/sFTP/PayrollOutsourcing/";
                //filename = filename.Replace(',', '_');
                //filename = filename.Replace(" ", String.Empty);
                workbook.Save(newDIR + filename);
                stream.Close();
                return onlineDIR + filename;
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }

        [HttpPost]
        [Route("Retirement_Benefit")]
        public string Retirement(POLoansReport param)
        {
            FileStream stream = null;
            try
            {
                DataTable DTParam = new DataTable();
                DTParam.Columns.AddRange(new DataColumn[1] { new DataColumn("empid") });
                for (int i = 0; i < param.empID.Length; i++)
                {
                    DTParam.Rows.Add(param.empID[i]);
                }
                TempConnectionStringv4(param.db);
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@year", mytype = SqlDbType.NVarChar, Value = param.Year });
                con.myparameters.Add(new myParameters { ParameterName = "@Userid", mytype = SqlDbType.Structured, Value = DTParam });
                DataTable LR = con.GetDataTable("sp_ComputeEmployeeYTD");
                string dataDir = HostingEnvironment.MapPath("~/Files/PayrollOutSourcing/");
                stream = new FileStream(dataDir + "RetireBenefitTemplate.xls", FileMode.Open);
                string filename;
                string datebuild = DateTime.Now.ToString("MMddyyyy");
                string timebuild = DateTime.Now.ToString("hhmmtt");

                //filename = datebuild + timebuild + "_PEZAReport" + ".xls";
                filename = "RetireBenefit" + "_" + datebuild + "_" + timebuild + ".xls";
                using (var fileStream = new FileStream(dataDir + filename, FileMode.Create, FileAccess.Write))
                //using (var fileStream = new FileStream(dataDir + filename, FileMode.Create, FileAccess.Write))
                {
                    stream.CopyTo(fileStream);
                }
                stream.Close();
                ExcelWorkbookFormat workbookFormat = ExcelWorkbookFormat.Xls_2003;
                string testDocFile = dataDir + "RetireBenefitTemplate.xls";
                ExcelWorkbook workbook = new ExcelWorkbook(testDocFile);
                ExcelWorksheet firstWorksheet = workbook.Worksheets[0];
                ExcelWorksheet Retirement = workbook.Worksheets[0];
                int x = 2;
                for (int i = 0; i < LR.Rows.Count; i++, x++)
                {
                    Retirement["A" + x].Text = LR.Rows[i]["Transaction"].ToString();
                    Retirement["B" + x].Text = LR.Rows[i]["Entity"].ToString();
                    Retirement["C" + x].Text = LR.Rows[i]["EID"].ToString();
                    Retirement["D" + x].Text = LR.Rows[i]["Employee Name"].ToString();
                    Retirement["E" + x].Text = LR.Rows[i]["Tax Status"].ToString();
                    Retirement["F" + x].Value = LR.Rows[i]["No Of Dependents"].ToString();
                    Retirement["G" + x].Value = LR.Rows[i]["Latest Basic Salary"];
                    Retirement["H" + x].Value = LR.Rows[i]["Daily rate"];
                    Retirement["I" + x].Value = LR.Rows[i]["Total Taxes withheld for the year"];
                    Retirement["J" + x].Value = LR.Rows[i]["TIN"];
                    Retirement["K" + x].Value = LR.Rows[i]["SSS"];
                    Retirement["L" + x].Value = LR.Rows[i]["PHIC"];
                    Retirement["M" + x].Value = LR.Rows[i]["HDMF"];
                    Retirement["N" + x].Value = LR.Rows[i]["Gross Taxable Compensation Income for the year"];
                    Retirement["O" + x].Value = LR.Rows[i]["De Minimis"];
                    Retirement["P" + x].Value = LR.Rows[i]["Other"];
                    Retirement["Q" + x].Value = LR.Rows[i]["Non Taxable/Exempt Compensation Income"];
                    Retirement["R" + x].Value = LR.Rows[i]["Gross Compensation (sum of A.1 and A.2)"];

                }
                HttpResponse httpResponse = System.Web.HttpContext.Current.Response;
                httpResponse.Clear();
                httpResponse.ContentType = "Application/x-msexcel";
                httpResponse.AddHeader("Content-Disposition", String.Format("attachment; filename={0}", filename));
                string newDIR = HttpContext.Current.Server.MapPath("~/sFTP/PayrollOutsourcing/");
                string onlineDIR = "https://ws.durusthr.com/ILM_WS_Live/sFTP/PayrollOutsourcing/";
                //filename = filename.Replace(',', '_');
                //filename = filename.Replace(" ", String.Empty);
                workbook.Save(newDIR + filename);
                stream.Close();
                //return "Success";
                if (LR.Rows.Count > 0)
                {
                    return onlineDIR + filename;
                }
                else
                {
                    return "No Rows";
                }
            }
            catch (Exception e)
            {
                stream.Close();
                return "Error: " + e.ToString();
            }
        }

        #region Reports

        [HttpPost]
        [Route("PO_Annualization_Report")]
        public string Extract_Annualization_Report(AnnualizationReport param)
        {
            FileStream stream = null;
            try
            {
                TempConnectionStringv4(param.db);
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@Year", mytype = SqlDbType.NVarChar, Value = param.Year });
                con.myparameters.Add(new myParameters { ParameterName = "@Tenant", mytype = SqlDbType.NVarChar, Value = param.TenantCode });
                DataTable EmpData = con.GetDataTable("sp_Annualization");
                string dataDir = HostingEnvironment.MapPath("~/Files/PayrollOutSourcing/");
                stream = new FileStream(dataDir + "AnnualizationReport.xls", FileMode.Open);
                string filename;
                string datebuild = DateTime.Now.ToString("MMddyyyy");
                string timebuild = DateTime.Now.ToString("hhmmtt");

                //filename = datebuild + timebuild + "_PEZAReport" + ".xls";
                filename = param.TenantCode + "_Annualization" + "_" + datebuild + "_" + timebuild + ".xls";
                using (var fileStream = new FileStream(dataDir + filename, FileMode.Create, FileAccess.Write))
                //using (var fileStream = new FileStream(dataDir + filename, FileMode.Create, FileAccess.Write))
                {
                    stream.CopyTo(fileStream);
                }
                stream.Close();
                ExcelWorkbookFormat workbookFormat = ExcelWorkbookFormat.Xls_2003;
                string testDocFile = dataDir + "AnnualizationReport.xls";
                ExcelWorkbook workbook = new ExcelWorkbook(testDocFile);
                ExcelWorksheet Annualization = workbook.Worksheets[0];
                int x = 2;
                for (int i = 0; i < EmpData.Rows.Count; i++, x++)
                {
                    Annualization["A" + x].Text = EmpData.Rows[i]["UserInformationid"].ToString();
                    Annualization["B" + x].Text = EmpData.Rows[i]["Code"].ToString();
                    Annualization["C" + x].Text = EmpData.Rows[i]["EmpId"].ToString();
                    Annualization["D" + x].Text = EmpData.Rows[i]["LastName"].ToString();
                    Annualization["E" + x].Text = EmpData.Rows[i]["FirstName"].ToString();
                    //Annualization["F" + x].Text = EmpData.Rows[i]["MiddleName"].ToString();
                    Annualization["F" + x].Text = EmpData.Rows[i]["DateEmployed"].ToString();
                    Annualization["G" + x].Value = EmpData.Rows[i]["OtherNTXBonusesPaidPrev"].ToString();
                    Annualization["H" + x].Value = EmpData.Rows[i]["MonthPaid"].ToString();
                    Annualization["I" + x].Value = EmpData.Rows[i]["MonthOthers"].ToString();
                    Annualization["J" + x].Value = EmpData.Rows[i]["DeMinimisPrev"].ToString();
                    Annualization["K" + x].Value = EmpData.Rows[i]["DeMinimisPaid"].ToString();
                    Annualization["L" + x].Value = EmpData.Rows[i]["Contribution"].ToString();
                    Annualization["M" + x].Value = EmpData.Rows[i]["NonTaxable13thMonthProjected"].ToString();
                    Annualization["N" + x].Value = EmpData.Rows[i]["Taxable13thMonthPaid"].ToString();
                    Annualization["O" + x].Value = EmpData.Rows[i]["Taxable13thMonthProjected"].ToString();
                    Annualization["P" + x].Value = EmpData.Rows[i]["GrossTaxablePrev"].ToString();
                    Annualization["Q" + x].Value = EmpData.Rows[i]["GrossPay"].ToString();
                    Annualization["R" + x].Value = EmpData.Rows[i]["CurrentMonthly"].ToString();
                    Annualization["S" + x].Value = EmpData.Rows[i]["CurrentEarningsAmountTX"].ToString();
                    Annualization["T" + x].Value = EmpData.Rows[i]["FutureMonthBasic"].ToString();
                    Annualization["U" + x].Value = EmpData.Rows[i]["FMEarningsamountTX"].ToString();
                    Annualization["V" + x].Value = EmpData.Rows[i]["Net Taxable"].ToString();
                    Annualization["W" + x].Value = EmpData.Rows[i]["Estimated TaxDue"].ToString();
                    Annualization["X" + x].Value = EmpData.Rows[i]["TaxWitheldPrev"].ToString();
                    Annualization["Y" + x].Value = EmpData.Rows[i]["TotalTaxWithheld"].ToString();
                    Annualization["Z" + x].Value = EmpData.Rows[i]["TaxRefund"].ToString();
                    Annualization["AA" + x].Value = EmpData.Rows[i]["TaxPayable"].ToString();
                }
                HttpResponse httpResponse = System.Web.HttpContext.Current.Response;
                httpResponse.Clear();
                httpResponse.ContentType = "Application/x-msexcel";
                httpResponse.AddHeader("Content-Disposition", String.Format("attachment; filename={0}", filename));
                string newDIR = HttpContext.Current.Server.MapPath("~/sFTP/PayrollOutsourcing/");
                string onlineDIR = "http://ws.durusthr.com/ILM_WS_Live/sFTP/PayrollOutsourcing/";
                //filename = filename.Replace(',', '_');
                //filename = filename.Replace(" ", String.Empty);
                workbook.Save(newDIR + filename);
                stream.Close();
                //return "Success";
                if (EmpData.Rows.Count > 0)
                {
                    return onlineDIR + filename;
                }
                else
                {
                    return "No Rows";
                }
            }
            catch (Exception e)
            {
                stream.Close();
                return "Error: " + e.ToString();
            }
        }

        [HttpPost]
        [Route("PO_LoansReport")]
        public string PO_Extract_Loans(POLoansReport param)
        {
            FileStream stream = null;
            try
            {
                TempConnectionStringv4(param.db);
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@TenantCode", mytype = SqlDbType.NVarChar, Value = param.TenantCode });
                con.myparameters.Add(new myParameters { ParameterName = "@Month", mytype = SqlDbType.NVarChar, Value = param.Month });
                con.myparameters.Add(new myParameters { ParameterName = "@Year", mytype = SqlDbType.NVarChar, Value = param.Year });
                DataTable LR = con.GetDataTable("sp_Extract_Loans");
                string dataDir = HostingEnvironment.MapPath("~/Files/PayrollOutSourcing/");
                stream = new FileStream(dataDir + "LoansReport.xls", FileMode.Open);
                string filename;
                string datebuild = DateTime.Now.ToString("MMddyyyy");
                string timebuild = DateTime.Now.ToString("hhmmtt");

                //filename = datebuild + timebuild + "_PEZAReport" + ".xls";
                filename = param.TenantCode + "_LoansReport" + "_" + param.Year + param.Month + "_" + datebuild + "_" + timebuild + ".xls";
                using (var fileStream = new FileStream(dataDir + filename, FileMode.Create, FileAccess.Write))
                //using (var fileStream = new FileStream(dataDir + filename, FileMode.Create, FileAccess.Write))
                {
                    stream.CopyTo(fileStream);
                }
                stream.Close();
                ExcelWorkbookFormat workbookFormat = ExcelWorkbookFormat.Xls_2003;
                string testDocFile = dataDir + "LoansReport.xls";
                ExcelWorkbook workbook = new ExcelWorkbook(testDocFile);
                ExcelWorksheet firstWorksheet = workbook.Worksheets[0];
                ExcelWorksheet LOANS = workbook.Worksheets[0];
                int x = 2;
                for (int i = 0; i < LR.Rows.Count; i++, x++)
                {
                    LOANS["A" + x].Text = LR.Rows[i]["PayrollNum"].ToString();
                    LOANS["B" + x].Text = LR.Rows[i]["PayDate"].ToString();
                    LOANS["C" + x].Text = LR.Rows[i]["PayCode"].ToString();
                    LOANS["D" + x].Text = LR.Rows[i]["EmployeeID"].ToString();
                    LOANS["E" + x].Text = LR.Rows[i]["LastName"].ToString();
                    LOANS["F" + x].Text = LR.Rows[i]["FirstName"].ToString();
                    LOANS["G" + x].Text = LR.Rows[i]["FileStatus"].ToString();
                    LOANS["H" + x].Text = LR.Rows[i]["LoanCode"].ToString();
                    LOANS["I" + x].Text = LR.Rows[i]["LoanDateStart"].ToString();
                    LOANS["J" + x].Value = LR.Rows[i]["LoanPrincipal"];
                    LOANS["K" + x].Value = LR.Rows[i]["LoanAmount"];
                    LOANS["L" + x].Value = LR.Rows[i]["LoanPayments"];
                    LOANS["M" + x].Value = LR.Rows[i]["LoanBalance"];
                    LOANS["N" + x].Value = LR.Rows[i]["LoanDeductionAMT"];
                    LOANS["O" + x].Text = LR.Rows[i]["LoanDeductionFreq"].ToString();
                    LOANS["P" + x].Text = LR.Rows[i]["LoanStatus"].ToString();
                    LOANS["Q" + x].Text = LR.Rows[i]["LoanDateGranted"].ToString();
                    LOANS["R" + x].Text = LR.Rows[i]["From"].ToString();
                    LOANS["S" + x].Text = LR.Rows[i]["To"].ToString();
                    LOANS["T" + x].Text = LR.Rows[i]["JECostCenter"].ToString();
                }
                HttpResponse httpResponse = System.Web.HttpContext.Current.Response;
                httpResponse.Clear();
                httpResponse.ContentType = "Application/x-msexcel";
                httpResponse.AddHeader("Content-Disposition", String.Format("attachment; filename={0}", filename));
                string newDIR = HttpContext.Current.Server.MapPath("~/sFTP/PayrollOutsourcing/");
                string onlineDIR = "http://ws.durusthr.com/ILM_WS_Live/sFTP/PayrollOutsourcing/";
                //filename = filename.Replace(',', '_');
                //filename = filename.Replace(" ", String.Empty);
                workbook.Save(newDIR + filename);
                stream.Close();
                //return "Success";
                if (LR.Rows.Count > 0)
                {
                    return onlineDIR + filename;
                }
                else
                {
                    return "No Rows";
                }
            }
            catch (Exception e)
            {
                stream.Close();
                return "Error: " + e.ToString();
            }
        }

        [HttpPost]
        [Route("ResetFinalPay")]
        public string ResetFinalPay(resetFinalPay param)
        {
            try
            {
                DataTable DTParam = new DataTable();
                DTParam.Columns.AddRange(new DataColumn[1] { new DataColumn("UserId") });
                for (int i = 0; i < param.UserId.Length; i++)
                {
                    DTParam.Rows.Add(param.UserId[i]);
                }
                TempConnectionStringv4(param.db);
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@TenantID", mytype = SqlDbType.VarChar, Value = param.TenantID });
                con.myparameters.Add(new myParameters { ParameterName = "@PayoutType", mytype = SqlDbType.VarChar, Value = param.PayoutType });
                con.myparameters.Add(new myParameters { ParameterName = "@CutoffStartDate", mytype = SqlDbType.VarChar, Value = param.CutoffStart });
                con.myparameters.Add(new myParameters { ParameterName = "@CutoffEndDate", mytype = SqlDbType.VarChar, Value = param.CutoffEnd });
                con.myparameters.Add(new myParameters { ParameterName = "@UserId", mytype = SqlDbType.Structured, Value = DTParam });
                DataTable DT = con.GetDataTable("sp_ComputeFinalReset");

                return "Success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }


        [HttpPost]
        [Route("ResetPayrollSpecial")]
        public string ResetPayrollSpecial(CalculateSpecialParam param)
        {
            try
            {
                DataTable EmpIDs = new DataTable();
                EmpIDs.Columns.AddRange(new DataColumn[1]
                {
                    new DataColumn("EmpID")
                });
                for (int i = 0; i < param.EmpID.Length; i++)
                {
                    EmpIDs.Rows.Add(param.EmpID[i]);
                }
                TempConnectionStringv4(param.db);
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@CutoffStart", mytype = SqlDbType.NVarChar, Value = param.CutoffStart });
                con.myparameters.Add(new myParameters { ParameterName = "@CutoffEnd", mytype = SqlDbType.NVarChar, Value = param.CutoffEnd });
                con.myparameters.Add(new myParameters { ParameterName = "@PayoutType", mytype = SqlDbType.NVarChar, Value = param.EmpStatus });
                con.myparameters.Add(new myParameters { ParameterName = "@TenantID", mytype = SqlDbType.NVarChar, Value = param.TenantID });
                con.myparameters.Add(new myParameters { ParameterName = "@userid", mytype = SqlDbType.Structured, Value = EmpIDs });
                con.ExecuteNonQuery("sp_payroll_reset_special");
                return "Success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }

        [HttpPost]
        [Route("ResetSSSMaternity")]
        public string ResetSSSMaternity(generatesss param)
        {
            try
            {
                DataTable EmpList = new DataTable();
                EmpList.Columns.AddRange(new DataColumn[1] { new DataColumn("empid") });
                for (int i = 0; i < param.empid.Length; i++)
                {
                    EmpList.Rows.Add(param.empid[i]);
                }
                TempConnectionStringv4(param.db);
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@TenantID", mytype = SqlDbType.VarChar, Value = param.tenantid });
                con.myparameters.Add(new myParameters { ParameterName = "@UserID", mytype = SqlDbType.Structured, Value = EmpList });
                con.myparameters.Add(new myParameters { ParameterName = "@CutoffStart", mytype = SqlDbType.VarChar, Value = param.cutoffstart });
                con.myparameters.Add(new myParameters { ParameterName = "@CutoffEnd", mytype = SqlDbType.VarChar, Value = param.cutoffend });
                con.myparameters.Add(new myParameters { ParameterName = "@Payouttype", mytype = SqlDbType.VarChar, Value = param.payouttype });

                con.ExecuteNonQuery("sp_ComputeSSSMaternityReset ");
                return "Success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }

        [HttpPost]
        [Route("Reset13thMonth")]
        public string Reset13thMonth(reset13th param)
        {
            try
            {
                DataTable DTParam = new DataTable();
                DTParam.Columns.AddRange(new DataColumn[1] { new DataColumn("UserId") });
                for (int i = 0; i < param.UserId.Length; i++)
                {
                    DTParam.Rows.Add(param.UserId[i]);
                }
                TempConnectionStringv4(param.db);
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@TenantID", mytype = SqlDbType.VarChar, Value = param.TenantID });
                con.myparameters.Add(new myParameters { ParameterName = "@PayoutType", mytype = SqlDbType.VarChar, Value = param.PayoutType });
                con.myparameters.Add(new myParameters { ParameterName = "@PayoutDate", mytype = SqlDbType.VarChar, Value = param.PayoutDate });
                con.myparameters.Add(new myParameters { ParameterName = "@UserId", mytype = SqlDbType.Structured, Value = DTParam });
                DataTable DT = con.GetDataTable("sp_Compute13MReset");

                return "Success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }

        }

        [HttpPost]
        [Route("PO_CalculateFinalPay")]
        public string CalculateFinalPay(CalculateFinalpayParam param)
        {
            try
            {
                DataTable EmpIDs = new DataTable();
                EmpIDs.Columns.AddRange(new DataColumn[1]
                {
                    new DataColumn("EmpID")
                });
                for (int i = 0; i < param.EmpID.Length; i++)
                {
                    EmpIDs.Rows.Add(param.EmpID[i]);
                }
                TempConnectionStringv4(param.db);
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@LoginID", mytype = SqlDbType.NVarChar, Value = param.LoginID });
                con.myparameters.Add(new myParameters { ParameterName = "@TenantID", mytype = SqlDbType.VarChar, Value = param.TenantID });
                con.myparameters.Add(new myParameters { ParameterName = "@CutoffStartDate", mytype = SqlDbType.NVarChar, Value = param.CutoffStartDate });
                con.myparameters.Add(new myParameters { ParameterName = "@CutoffEndDate", mytype = SqlDbType.NVarChar, Value = param.CutOffEndDate });
                con.myparameters.Add(new myParameters { ParameterName = "@Year", mytype = SqlDbType.NVarChar, Value = param.Year });
                con.myparameters.Add(new myParameters { ParameterName = "@UserID", mytype = SqlDbType.Structured, Value = EmpIDs });
                con.ExecuteNonQuery("sp_ComputeFinalLogHours");
                return "Success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }

        [HttpPost]
        [Route("GenerateSSSMaternity")]
        public string GenerateSSSMaternity(generatesss param)
        {
            try
            {
                DataTable EmpList = new DataTable();
                EmpList.Columns.AddRange(new DataColumn[1] { new DataColumn("empid") });
                for (int i = 0; i < param.empid.Length; i++)
                {
                    EmpList.Rows.Add(param.empid[i]);
                }
                TempConnectionStringv4(param.db);
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@TenantID", mytype = SqlDbType.VarChar, Value = param.tenantid });
                con.myparameters.Add(new myParameters { ParameterName = "@UserID", mytype = SqlDbType.Structured, Value = EmpList });
                con.myparameters.Add(new myParameters { ParameterName = "@CutoffStartDate", mytype = SqlDbType.VarChar, Value = param.cutoffstart });
                con.myparameters.Add(new myParameters { ParameterName = "@CutOffEndDate", mytype = SqlDbType.VarChar, Value = param.cutoffend });
                con.myparameters.Add(new myParameters { ParameterName = "@Days", mytype = SqlDbType.VarChar, Value = param.days });
                con.myparameters.Add(new myParameters { ParameterName = "@Payouttype", mytype = SqlDbType.VarChar, Value = param.payouttype });

                con.ExecuteNonQuery("sp_ComputeSSSMaternity");
                return "Success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }
        [HttpPost]
        [Route("PO_HDMFLoans")]
        public string Extract_HDMFLoans(HDMFLoans param)
        {
            FileStream stream = null;
            try
            {
                TempConnectionStringv2();
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@TenantCode", mytype = SqlDbType.NVarChar, Value = param.TenantCode });
                con.myparameters.Add(new myParameters { ParameterName = "@Month", mytype = SqlDbType.NVarChar, Value = param.Month });
                con.myparameters.Add(new myParameters { ParameterName = "@Year", mytype = SqlDbType.NVarChar, Value = param.Year });
                DataTable HDMF = con.GetDataTable("sp_Extract_HDMFLoans");
                string dataDir = HostingEnvironment.MapPath("~/Files/PayrollOutSourcing/");
                stream = new FileStream(dataDir + "HDMFLoans.xls", FileMode.Open);
                string filename;
                string datebuild = DateTime.Now.ToString("MMddyyyy");
                string timebuild = DateTime.Now.ToString("hhmmtt");

                //filename = datebuild + timebuild + "_PEZAReport" + ".xls";
                filename = param.TenantCode + "_HDMFLOANS" + "_" + param.Year + param.Month + "_" + datebuild + "_" + timebuild + ".xls";
                using (var fileStream = new FileStream(dataDir + filename, FileMode.Create, FileAccess.Write))
                //using (var fileStream = new FileStream(dataDir + filename, FileMode.Create, FileAccess.Write))
                {
                    stream.CopyTo(fileStream);
                }
                stream.Close();
                ExcelWorkbookFormat workbookFormat = ExcelWorkbookFormat.Xls_2003;
                string testDocFile = dataDir + "HDMFLoans.xls";
                ExcelWorkbook workbook = new ExcelWorkbook(testDocFile);
                ExcelWorksheet firstWorksheet = workbook.Worksheets[0];
                ExcelWorksheet HDMFLOANS = workbook.Worksheets[0];
                int x = 2;
                for (int i = 0; i < HDMF.Rows.Count; i++, x++)
                {
                    HDMFLOANS["A" + x].Text = HDMF.Rows[i]["EYERID"].ToString();
                    HDMFLOANS["B" + x].Text = HDMF.Rows[i]["BDATE"].ToString();
                    HDMFLOANS["C" + x].Text = HDMF.Rows[i]["TIN"].ToString();
                    HDMFLOANS["D" + x].Text = HDMF.Rows[i]["HDMFID"].ToString();
                    HDMFLOANS["E" + x].Text = HDMF.Rows[i]["LNAME"].ToString();
                    HDMFLOANS["F" + x].Text = HDMF.Rows[i]["FNAME"].ToString();
                    HDMFLOANS["G" + x].Text = HDMF.Rows[i]["MID"].ToString();
                    HDMFLOANS["H" + x].Text = HDMF.Rows[i]["PERCOV"].ToString();
                    HDMFLOANS["I" + x].Text = HDMF.Rows[i]["ORNO"].ToString();
                    HDMFLOANS["J" + x].Text = HDMF.Rows[i]["ORDATE"].ToString();
                    HDMFLOANS["K" + x].Text = HDMF.Rows[i]["AMOUNT"].ToString();
                }
                HttpResponse httpResponse = System.Web.HttpContext.Current.Response;
                httpResponse.Clear();
                httpResponse.ContentType = "Application/x-msexcel";
                httpResponse.AddHeader("Content-Disposition", String.Format("attachment; filename={0}", filename));
                string newDIR = HttpContext.Current.Server.MapPath("~/sFTP/PayrollOutsourcing/");
                string onlineDIR = "http://ws.durusthr.com/ILM_WS_Live/sFTP/PayrollOutsourcing/";
                //filename = filename.Replace(',', '_');
                //filename = filename.Replace(" ", String.Empty);
                workbook.Save(newDIR + filename);
                stream.Close();
                //return "Success";
                if (HDMF.Rows.Count > 0)
                {
                    return onlineDIR + filename;
                }
                else
                {
                    return "No Rows";
                }
            }
            catch (Exception e)
            {
                stream.Close();
                return "Error: " + e.ToString();
            }
        }

        [HttpPost]
        [Route("Extract_JournalVoucher")]
        public string JournalVoucher(efile param)
        {

            FileStream stream = null;
            try
            {

                TempConnectionStringv2();
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@TenantID", mytype = SqlDbType.NVarChar, Value = param.TenantID });
                con.myparameters.Add(new myParameters { ParameterName = "@CutoffStart", mytype = SqlDbType.NVarChar, Value = param.CutoffStart });
                con.myparameters.Add(new myParameters { ParameterName = "@CutoffEnd", mytype = SqlDbType.NVarChar, Value = param.CutoffEnd });

                DataTable data1 = con.GetDataTable("sp_jv_woAllocation");


                string dataDir = HostingEnvironment.MapPath("~/Files/PayrollOutSourcing/");

                stream = new FileStream(dataDir + "Template_JV.xls", FileMode.Open);
                string filename;
                // string datebuild = DateTime.Now.ToString("MMddyyyy");
                string cc = param.cCode;


                string cStart = Regex.Replace(param.CutoffStart, @"[^0-9a-zA-Z]+", "");
                string cEnd = Regex.Replace(param.CutoffEnd, @"[^0-9a-zA-Z]+", "");


                string datebuild = DateTime.Now.ToString("yyyyMM");
                string timebuild = DateTime.Now.ToString("hhtt");
                filename = cc + "_JV_" + cStart + "_" + cEnd + "_" + timebuild + ".xls";
                using (var fileStream = new FileStream(dataDir + filename, FileMode.Create, FileAccess.Write))
                //using (var fileStream = new FileStream(dataDir + filename, FileMode.Create, FileAccess.Write))
                {
                    stream.CopyTo(fileStream);
                }
                stream.Close();
                ExcelWorkbookFormat workbookFormat = ExcelWorkbookFormat.Xls_2003;
                string testDocFile = dataDir + "Template_JV.xls";
                ExcelWorkbook workbook = new ExcelWorkbook(testDocFile);
                ExcelWorksheet firstWorksheet = workbook.Worksheets[0];

                #region PayReg Sheet

                ExcelWorksheet WOAllocation = workbook.Worksheets[0];
                ExcelWorksheet Allocation = workbook.Worksheets[1];
                ExcelWorksheet sc1 = workbook.Worksheets[2];
                ExcelWorksheet sc2 = workbook.Worksheets[3];
                //Sheet7.WorksheetSecurity.ProtectWorksheet("sbc01");
                //AX TAXTABLE

                int x = 3;
                for (int i = 0; i < data1.Rows.Count; i++, x++)
                {

                    WOAllocation["A" + x].Text = data1.Rows[i]["PayrollNum"].ToString(); //payroll#
                    WOAllocation["B" + x].Text = data1.Rows[i]["PayDate"].ToString(); //paydate
                    WOAllocation["C" + x].Text = data1.Rows[i]["PayCode"].ToString(); //pay code
                    WOAllocation["D" + x].Text = data1.Rows[i]["userID"].ToString(); //employee id
                    WOAllocation["E" + x].Text = data1.Rows[i]["LastName"].ToString(); //last name
                    WOAllocation["F" + x].Text = data1.Rows[i]["FirstName"].ToString(); //first name
                    WOAllocation["G" + x].Text = data1.Rows[i]["EmployeeType"].ToString(); //file status
                    WOAllocation["H" + x].Text = data1.Rows[i]["TaxStatus"].ToString(); //tax status
                    WOAllocation["I" + x].Value = data1.Rows[i]["BranchCode"]; //daily rate
                    WOAllocation["J" + x].Value = data1.Rows[i]["Allocation"]; //salary rate type
                    WOAllocation["K" + x].Value = data1.Rows[i]["DailyRate"]; //basic salary
                    WOAllocation["L" + x].Value = data1.Rows[i]["MonthlyRate"]; //misc amount
                    WOAllocation["M" + x].Value = data1.Rows[i]["PayrollCost"];
                    WOAllocation["N" + x].Value = data1.Rows[i]["BasicPay"]; //overtime
                    WOAllocation["O" + x].Value = data1.Rows[i]["AbsentAmount"];
                    WOAllocation["P" + x].Value = data1.Rows[i]["TardyAmount"]; //other tx income
                    WOAllocation["Q" + x].Value = data1.Rows[i]["UndertimeAmount"]; //adjustments
                    WOAllocation["R" + x].Value = data1.Rows[i]["LWOPAmount"]; //gross income
                    WOAllocation["S" + x].Value = data1.Rows[i]["WorkHoursAmount"]; //withholding tax
                    WOAllocation["T" + x].Value = data1.Rows[i]["NDOvertimeAmount"]; //net salary after tax
                    WOAllocation["U" + x].Value = data1.Rows[i]["OvertimeAmount"]; //employee sss
                    WOAllocation["V" + x].Value = data1.Rows[i]["PerfBonusNonAdmin"];
                    WOAllocation["W" + x].Value = data1.Rows[i]["Pay13TMonth"];
                    WOAllocation["X" + x].Value = data1.Rows[i]["PerfBonusAdmin"];
                    WOAllocation["Y" + x].Value = data1.Rows[i]["Maternity"];
                    WOAllocation["Z" + x].Value = data1.Rows[i]["NetPayAdjustment"];
                    WOAllocation["AA" + x].Value = data1.Rows[i]["Reimbursement"];
                    WOAllocation["AB" + x].Value = data1.Rows[i]["SSSSLADJ_NTX"];
                    WOAllocation["AC" + x].Value = data1.Rows[i]["BasicADJ"];
                    WOAllocation["AD" + x].Value = data1.Rows[i]["Maternity_SALDIF"];

                    WOAllocation["AE" + x].Value = data1.Rows[i]["Pay13TMonthNONAdmin"];
                    WOAllocation["AF" + x].Value = data1.Rows[i]["STIP"];
                    WOAllocation["AG" + x].Value = data1.Rows[i]["Pay13thADJNONAdmin"];
                    WOAllocation["AH" + x].Value = data1.Rows[i]["BEREAVEMENT"];
                    WOAllocation["AI" + x].Value = data1.Rows[i]["Pay13thADJAdmin"];
                    WOAllocation["AJ" + x].Value = data1.Rows[i]["LAUNDRYALLOW"];
                    WOAllocation["AK" + x].Value = data1.Rows[i]["LAUNDRYALLOWADJ"];
                    WOAllocation["AL" + x].Value = data1.Rows[i]["MEALALLOW"];
                    WOAllocation["AM" + x].Value = data1.Rows[i]["MEALALLOWADJ"];
                    WOAllocation["AN" + x].Value = data1.Rows[i]["RICESUBSIDY"];
                    WOAllocation["AO" + x].Value = data1.Rows[i]["RiceADJ"]; //employee mcr(philhealth)
                    WOAllocation["AP" + x].Value = data1.Rows[i]["TranspoTX"]; //employee pagibig
                    WOAllocation["AQ" + x].Value = data1.Rows[i]["  TRANSPOALLOW_NTX"];

                    WOAllocation["AR" + x].Value = data1.Rows[i]["CommunicationAllowance"]; //other ntx income
                    WOAllocation["AS" + x].Value = data1.Rows[i]["OnCallAllowance"]; //loan payments
                    WOAllocation["AT" + x].Value = data1.Rows[i]["OtherAllowance"]; //deductions
                    WOAllocation["AU" + x].Value = data1.Rows[i]["OtherIncomeTax"]; //net salary
                    WOAllocation["AV" + x].Value = data1.Rows[i]["USRNRETENTION"]; //employer sss
                    WOAllocation["AW" + x].Value = data1.Rows[i]["Referral"]; //employer mcr(philhealth)
                    WOAllocation["AX" + x].Value = data1.Rows[i]["HMO"]; //employer ec
                    WOAllocation["AY" + x].Value = data1.Rows[i]["HMOCard"]; //employer pagibig
                    WOAllocation["AZ" + x].Value = data1.Rows[i]["CompanyID"]; //payroll cost
                    WOAllocation["BA" + x].Value = data1.Rows[i]["CompanyBadge"]; //payment type
                    WOAllocation["BB" + x].Value = data1.Rows[i]["BLDGBadge"]; //bank acct#
                    WOAllocation["BC" + x].Value = data1.Rows[i]["RetirementFund"]; //bank name
                    WOAllocation["BD" + x].Value = data1.Rows[i]["SSSSLAmount"]; //date employed
                    WOAllocation["BE" + x].Value = data1.Rows[i]["SSSCLAmount"]; //date terminated
                    WOAllocation["BF" + x].Value = data1.Rows[i]["SSSSLADJAmount"]; //cost center
                    WOAllocation["BG" + x].Value = data1.Rows[i]["SSSCLADJAmount"];
                    WOAllocation["BH" + x].Value = data1.Rows[i]["PAGMPLAmount"]; //currency
                    WOAllocation["BI" + x].Value = data1.Rows[i]["PAGMPLADJAmount"]; //exchange rate
                    WOAllocation["BJ" + x].Value = data1.Rows[i]["CODADV"]; //payment freq
                    WOAllocation["BK" + x].Value = data1.Rows[i]["FLUVACCINE"];
                    WOAllocation["BL" + x].Value = data1.Rows[i]["NEGNETPAY"];
                    WOAllocation["BM" + x].Value = data1.Rows[i]["SSSSICKNESS"];
                    WOAllocation["BN" + x].Value = data1.Rows[i]["SSSMATERNITY"];
                    WOAllocation["BO" + x].Value = data1.Rows[i]["ITSUPPLIES"];
                    WOAllocation["BP" + x].Value = data1.Rows[i]["PHONECHARGE"];
                    WOAllocation["BQ" + x].Value = data1.Rows[i]["OtherDED"];
                    WOAllocation["BR" + x].Value = data1.Rows[i]["GOVTSTATUTORYDEDUCTION"];
                    WOAllocation["BS" + x].Value = data1.Rows[i]["SSSSALARYDIFFERENTIAL"];
                    WOAllocation["BT" + x].Value = data1.Rows[i]["LOANADJ"];
                    WOAllocation["BU" + x].Value = data1.Rows[i]["Tax Refund"];
                    WOAllocation["BV" + x].Value = data1.Rows[i]["Tax Payable"];
                    WOAllocation["BW" + x].Value = data1.Rows[i]["PAGCAL"];
                    WOAllocation["BX" + x].Value = data1.Rows[i]["SSSSLERP"];
                    WOAllocation["BY" + x].Value = data1.Rows[i]["COSL"];
                    WOAllocation["BZ" + x].Value = data1.Rows[i]["PhilHealthERAmount"];
                    WOAllocation["CA" + x].Value = data1.Rows[i]["PagibigERAmount"];
                    WOAllocation["CB" + x].Value = data1.Rows[i]["SSS"];
                    WOAllocation["CC" + x].Value = data1.Rows[i]["PhilHealth"];
                    WOAllocation["CD" + x].Value = data1.Rows[i]["TotalSSS"];
                    WOAllocation["CE" + x].Value = data1.Rows[i]["Pagibig"];
                    WOAllocation["CF" + x].Value = data1.Rows[i]["WHTax"];
                    WOAllocation["CG" + x].Value = data1.Rows[i]["NetPay"];





                }
                #endregion
                TempConnectionStringv2();
                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@TenantID", mytype = SqlDbType.NVarChar, Value = param.TenantID });
                con.myparameters.Add(new myParameters { ParameterName = "@CutoffStart", mytype = SqlDbType.NVarChar, Value = param.CutoffStart });
                con.myparameters.Add(new myParameters { ParameterName = "@CutoffEnd", mytype = SqlDbType.NVarChar, Value = param.CutoffEnd });

                DataTable data2 = con.GetDataTable("sp_jv_Allocation");
                x = 3;
                for (int i = 0; i < data2.Rows.Count; i++, x++)
                {

                    Allocation["A" + x].Text = data2.Rows[i]["PayrollNum"].ToString(); //payroll#
                    Allocation["B" + x].Text = data2.Rows[i]["PayDate"].ToString(); //paydate
                    Allocation["C" + x].Text = data2.Rows[i]["PayCode"].ToString(); //pay code
                    Allocation["D" + x].Text = data2.Rows[i]["userID"].ToString(); //employee id
                    Allocation["E" + x].Text = data2.Rows[i]["LastName"].ToString(); //last name
                    Allocation["F" + x].Text = data2.Rows[i]["FirstName"].ToString(); //first name
                    Allocation["G" + x].Text = data2.Rows[i]["EmployeeType"].ToString(); //file status
                    Allocation["H" + x].Text = data2.Rows[i]["TaxStatus"].ToString(); //tax status
                    Allocation["I" + x].Value = data2.Rows[i]["BranchCode"]; //daily rate
                    Allocation["J" + x].Value = data2.Rows[i]["Allocation"]; //salary rate type
                    Allocation["K" + x].Value = data2.Rows[i]["DailyRate"]; //basic salary
                    Allocation["L" + x].Value = data2.Rows[i]["MonthlyRate"]; //misc amount
                    Allocation["M" + x].Value = data2.Rows[i]["PayrollCost"];
                    Allocation["N" + x].Value = data2.Rows[i]["BasicPay"]; //overtime
                    Allocation["O" + x].Value = data2.Rows[i]["AbsentAmount"];
                    Allocation["P" + x].Value = data2.Rows[i]["TardyAmount"]; //other tx income
                    Allocation["Q" + x].Value = data2.Rows[i]["UndertimeAmount"]; //adjustments
                    Allocation["R" + x].Value = data2.Rows[i]["LWOPAmount"]; //gross income
                    Allocation["S" + x].Value = data2.Rows[i]["WorkHoursAmount"]; //withholding tax
                    Allocation["T" + x].Value = data2.Rows[i]["NDOvertimeAmount"]; //net salary after tax
                    Allocation["U" + x].Value = data2.Rows[i]["OvertimeAmount"]; //employee sss
                    Allocation["V" + x].Value = data2.Rows[i]["RiceADJ"]; //employee mcr(philhealth)
                    Allocation["W" + x].Value = data2.Rows[i]["TranspoTX"]; //employee pagibig
                    Allocation["X" + x].Value = data2.Rows[i]["CommunicationAllowance"]; //other ntx income
                    Allocation["Y" + x].Value = data2.Rows[i]["OnCallAllowance"]; //loan payments
                    Allocation["Z" + x].Value = data2.Rows[i]["OtherAllowance"]; //deductions
                    Allocation["AA" + x].Value = data2.Rows[i]["OtherIncomeTax"]; //net salary
                    Allocation["AB" + x].Value = data2.Rows[i]["USRNRETENTION"]; //employer sss
                    Allocation["AC" + x].Value = data2.Rows[i]["Referral"]; //employer mcr(philhealth)
                    Allocation["AD" + x].Value = data2.Rows[i]["HMO"]; //employer ec
                    Allocation["AE" + x].Value = data2.Rows[i]["HMOCard"]; //employer pagibig
                    Allocation["AF" + x].Value = data2.Rows[i]["CompanyID"]; //payroll cost
                    Allocation["AG" + x].Value = data2.Rows[i]["CompanyBadge"]; //payment type
                    Allocation["AH" + x].Value = data2.Rows[i]["BLDGBadge"]; //bank acct#
                    Allocation["AI" + x].Value = data2.Rows[i]["RetirementFund"]; //bank name
                    Allocation["AJ" + x].Value = data2.Rows[i]["SSSSLAmount"]; //date employed
                    Allocation["AK" + x].Value = data2.Rows[i]["SSSCLAmount"]; //date terminated
                    Allocation["AL" + x].Value = data2.Rows[i]["SSSSLADJAmount"]; //cost center
                    Allocation["AM" + x].Value = data2.Rows[i]["SSSCLADJAmount"];
                    Allocation["AN" + x].Value = data2.Rows[i]["PAGMPLAmount"]; //currency
                    Allocation["AO" + x].Value = data2.Rows[i]["PAGMPLADJAmount"]; //exchange rate
                    Allocation["AP" + x].Value = data2.Rows[i]["CODADV"]; //payment freq
                    Allocation["AQ" + x].Value = data2.Rows[i]["FLUVACCINE"];
                    Allocation["AR" + x].Value = data2.Rows[i]["NEGNETPAY"];
                    Allocation["AS" + x].Value = data2.Rows[i]["SSSSICKNESS"];
                    Allocation["AT" + x].Value = data2.Rows[i]["SSSMATERNITY"];
                    Allocation["AU" + x].Value = data2.Rows[i]["ITSUPPLIES"];
                    Allocation["AV" + x].Value = data2.Rows[i]["PHONECHARGE"];
                    Allocation["AW" + x].Value = data2.Rows[i]["OtherDED"];
                    Allocation["AX" + x].Value = data2.Rows[i]["GOVTSTATUTORYDEDUCTION"];
                    Allocation["AY" + x].Value = data2.Rows[i]["SSSSALARYDIFFERENTIAL"];
                    Allocation["BT" + x].Value = data1.Rows[i]["LOANADJ"];
                    Allocation["BU" + x].Value = data1.Rows[i]["Tax Refund"];
                    Allocation["BV" + x].Value = data1.Rows[i]["Tax Payable"];
                    Allocation["BW" + x].Value = data1.Rows[i]["PAGCAL"];
                    Allocation["BX" + x].Value = data1.Rows[i]["SSSSLERP"];
                    Allocation["BY" + x].Value = data1.Rows[i]["COSL"];
                    Allocation["BZ" + x].Value = data1.Rows[i]["PhilHealthERAmount"];
                    Allocation["CA" + x].Value = data1.Rows[i]["PagibigERAmount"];
                    Allocation["CB" + x].Value = data1.Rows[i]["SSS"];
                    Allocation["CC" + x].Value = data1.Rows[i]["PhilHealth"];
                    Allocation["CD" + x].Value = data1.Rows[i]["TotalSSS"];
                    Allocation["CE" + x].Value = data1.Rows[i]["Pagibig"];
                    Allocation["CF" + x].Value = data1.Rows[i]["WHTax"];
                    Allocation["CG" + x].Value = data1.Rows[i]["NetPay"];
                }



                //return "Success";
                TempConnectionStringv2();
                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@TenantID", mytype = SqlDbType.NVarChar, Value = param.TenantID });
                con.myparameters.Add(new myParameters { ParameterName = "@CutoffStart", mytype = SqlDbType.NVarChar, Value = param.CutoffStart });
                con.myparameters.Add(new myParameters { ParameterName = "@CutoffEnd", mytype = SqlDbType.NVarChar, Value = param.CutoffEnd });

                DataTable c1 = con.GetDataTable("sp_jv_c1");
                x = 2;
                for (int i = 0; i < c1.Rows.Count; i++, x++)
                {
                    sc1["A" + x].Text = c1.Rows[i]["userID"].ToString();
                    sc1["B" + x].Text = c1.Rows[i]["LastName"].ToString();
                    sc1["C" + x].Text = c1.Rows[i]["FirstName"].ToString();
                    sc1["D" + x].Text = c1.Rows[i]["Allocation"].ToString();
                    sc1["E" + x].Text = c1.Rows[i]["AccountNumber"].ToString();
                    sc1["F" + x].Text = c1.Rows[i]["AccountCode"].ToString();
                    sc1["G" + x].Value = c1.Rows[i]["Amount"];
                    sc1["H" + x].Value = c1.Rows[i]["Debit"];
                    sc1["I" + x].Value = c1.Rows[i]["credit"];
                    sc1["J" + x].Text = c1.Rows[i]["BranchCode"].ToString();
                }
                TempConnectionStringv2();
                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@TenantID", mytype = SqlDbType.NVarChar, Value = param.TenantID });
                con.myparameters.Add(new myParameters { ParameterName = "@CutoffStart", mytype = SqlDbType.NVarChar, Value = param.CutoffStart });
                con.myparameters.Add(new myParameters { ParameterName = "@CutoffEnd", mytype = SqlDbType.NVarChar, Value = param.CutoffEnd });

                DataTable c2 = con.GetDataTable("sp_jv_c2");
                x = 2;
                for (int i = 0; i < c2.Rows.Count; i++, x++)
                {
                    sc2["A" + x].Text = c2.Rows[i]["userID"].ToString();
                    sc2["B" + x].Text = c2.Rows[i]["LastName"].ToString();
                    sc2["C" + x].Text = c2.Rows[i]["FirstName"].ToString();
                    sc2["D" + x].Text = c2.Rows[i]["Allocation"].ToString();
                    sc2["E" + x].Text = c2.Rows[i]["AccountNumber"].ToString();
                    sc2["F" + x].Text = c2.Rows[i]["AccountCode"].ToString();
                    sc2["G" + x].Value = c2.Rows[i]["Amount"];
                    sc2["H" + x].Value = c2.Rows[i]["Debit"];
                    sc2["I" + x].Value = c2.Rows[i]["credit"];
                    sc2["J" + x].Text = c2.Rows[i]["BranchCode"].ToString();
                }
                HttpResponse httpResponse = System.Web.HttpContext.Current.Response;

                httpResponse.Clear();
                httpResponse.ContentType = "Application/x-msexcel";
                httpResponse.AddHeader("Content-Disposition", String.Format("attachment; filename={0}", filename));
                string newDIR = HttpContext.Current.Server.MapPath("~/sFTP/PayrollOutsourcing/");
                string onlineDIR = "http://ws.durusthr.com/ILM_WS_Live/sFTP/PayrollOutsourcing/";
                //filename = filename.Replace(',', '_');
                //filename = filename.Replace(" ", String.Empty);
                workbook.Save(newDIR + filename);
                stream.Close();
                if (data1.Rows.Count > 0)
                {
                    return onlineDIR + filename;
                }
                else
                {

                    return "No Rows";
                }
            }
            catch (Exception e)
            {
                stream.Close();
                return "Error: " + e.ToString();
            }
        }


        [HttpPost]
        [Route("PO_SummaryReport")]
        public string GenerateSummaryReport(SumReportParams param)
        {
            FileStream stream = null;
            try
            {
                DataTable DTParam = new DataTable();
                //DTParam.Columns.AddRange(new DataColumn[1] { new DataColumn("startingids") });
                //for (int i = 0; i < Params.StartingIDS.Length; i++)
                //{
                //    DTParam.Rows.Add(Params.StartingIDS[i]);
                //}
                TempConnectionStringv2();
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@tenantID", mytype = SqlDbType.NVarChar, Value = param.tenantID });
                con.myparameters.Add(new myParameters { ParameterName = "@startDate", mytype = SqlDbType.NVarChar, Value = param.startDate });
                con.myparameters.Add(new myParameters { ParameterName = "@endDate", mytype = SqlDbType.NVarChar, Value = param.endDate });
                DataTable PayRegData = con.GetDataTable("sp_EOMReport");

                string dataDir = HostingEnvironment.MapPath("~/Files/PayrollOutSourcing/");
                stream = new FileStream(dataDir + "SummaryReport.xls", FileMode.Open);
                string filename;
                string datebuild = DateTime.Now.ToString("MMddyyyy");
                string timebuild = DateTime.Now.ToString("hhmmtt");

                //filename = datebuild + timebuild + "_SummaryReport" + ".xls";
                string[] filenameStartdate = param.startDate.Split(' ');
                string[] filenameEnddate = param.endDate.Split(' ');
                filename = PayRegData.Rows[0][0].ToString() + "_SummaryReport_" + filenameStartdate[0] + "_" + filenameEnddate[0] + ".xls";
                using (var fileStream = new FileStream(dataDir + filename, FileMode.Create, FileAccess.Write))
                //using (var fileStream = new FileStream(dataDir + filename, FileMode.Create, FileAccess.Write))
                {
                    stream.CopyTo(fileStream);
                }
                stream.Close();
                ExcelWorkbookFormat workbookFormat = ExcelWorkbookFormat.Xls_2003;
                string testDocFile = dataDir + "SummaryReport.xls";
                ExcelWorkbook workbook = new ExcelWorkbook(testDocFile);
                ExcelWorksheet firstWorksheet = workbook.Worksheets[0];
                #region PayReg Sheet
                ExcelWorksheet PayReg = workbook.Worksheets[0];
                //Sheet7.WorksheetSecurity.ProtectWorksheet("sbc01");
                //AX TAXTABLE
                int x = 2;
                for (int i = 0; i < PayRegData.Rows.Count; i++, x++)
                {
                    PayReg["A" + x].Text = PayRegData.Rows[i]["CompanyCode"].ToString();
                    PayReg["B" + x].Text = PayRegData.Rows[i]["EmpID"].ToString();
                    PayReg["C" + x].Text = PayRegData.Rows[i]["LastName"].ToString();
                    PayReg["D" + x].Text = PayRegData.Rows[i]["FirstName"].ToString();
                    PayReg["E" + x].Text = PayRegData.Rows[i]["CostCenter"].ToString();
                    PayReg["F" + x].Text = PayRegData.Rows[i]["BranchCode"].ToString();
                    PayReg["G" + x].Text = (Convert.ToDecimal(PayRegData.Rows[i]["AllocationPercentage"].ToString()) * 100).ToString("0.00");
                    PayReg["H" + x].Text = PayRegData.Rows[i]["BasicSalary"].ToString();
                    PayReg["I" + x].Text = ConvertIfNull(PayRegData.Rows[i]["RegularDays"].ToString());
                    PayReg["J" + x].Text = ConvertIfNull(PayRegData.Rows[i]["Undertime"].ToString());
                    PayReg["K" + x].Text = ConvertIfNull(PayRegData.Rows[i]["LWOP"].ToString());
                    PayReg["L" + x].Text = ConvertIfNull(PayRegData.Rows[i]["Absent"].ToString());
                    PayReg["M" + x].Text = ConvertIfNull(PayRegData.Rows[i]["NightDiff"].ToString());
                    PayReg["N" + x].Text = ConvertIfNull(PayRegData.Rows[i]["Overtime"].ToString());
                    PayReg["O" + x].Text = ConvertIfNull(PayRegData.Rows[i]["LAUNDRY ALLOW"].ToString());
                    PayReg["P" + x].Text = ConvertIfNull(PayRegData.Rows[i]["LAUNDRY ALLOW ADJ"].ToString());
                    PayReg["Q" + x].Text = ConvertIfNull(PayRegData.Rows[i]["MEAL ALLOW"].ToString());
                    PayReg["R" + x].Text = ConvertIfNull(PayRegData.Rows[i]["MEAL ALLOW ADJ"].ToString());
                    PayReg["S" + x].Text = ConvertIfNull(PayRegData.Rows[i]["RICE SUBSIDY"].ToString());
                    PayReg["T" + x].Text = ConvertIfNull(PayRegData.Rows[i]["RICE SUBSIDY ADJ"].ToString());
                    PayReg["U" + x].Text = ConvertIfNull(PayRegData.Rows[i]["TRANSPO ALLOW-TX"].ToString());
                    PayReg["V" + x].Text = ConvertIfNull(PayRegData.Rows[i]["TRANSPO ALLOW-NTX"].ToString());
                    PayReg["W" + x].Text = ConvertIfNull(PayRegData.Rows[i]["COMMUNICATION ALLOW"].ToString());
                    PayReg["X" + x].Text = ConvertIfNull(PayRegData.Rows[i]["REFERRAL"].ToString());
                    PayReg["Y" + x].Text = ConvertIfNull(PayRegData.Rows[i]["ON-CALL SUPPORT ALLOW"].ToString());
                    PayReg["Z" + x].Text = ConvertIfNull(PayRegData.Rows[i]["OTHER ALLOW"].ToString());
                    PayReg["AA" + x].Text = ConvertIfNull(PayRegData.Rows[i]["BEREAVEMENT ASSISTANCE"].ToString());
                    PayReg["AB" + x].Text = ConvertIfNull(PayRegData.Rows[i]["13TH MO PAY"].ToString());
                    PayReg["AC" + x].Text = ConvertIfNull(PayRegData.Rows[i]["PERF BONUS"].ToString());
                    PayReg["AD" + x].Text = ConvertIfNull(PayRegData.Rows[i]["STIP"].ToString());
                    PayReg["AE" + x].Text = ConvertIfNull(PayRegData.Rows[i]["BASIC ADJ"].ToString());
                    PayReg["AF" + x].Text = ConvertIfNull(PayRegData.Rows[i]["13TH MO PAY ADJ"].ToString());
                    PayReg["AG" + x].Text = ConvertIfNull(PayRegData.Rows[i]["OTHER INCOME-TX"].ToString());
                    PayReg["AH" + x].Text = ConvertIfNull(PayRegData.Rows[i]["MATERNITY BENEFIT"].ToString());
                    PayReg["AI" + x].Text = ConvertIfNull(PayRegData.Rows[i]["MATERNITY SALDIF-NT"].ToString());
                    PayReg["AJ" + x].Text = ConvertIfNull(PayRegData.Rows[i]["USRN RETENTION"].ToString());
                    PayReg["AK" + x].Text = ConvertIfNull(PayRegData.Rows[i]["REIMBURSEMENT"].ToString());
                    PayReg["AL" + x].Text = ConvertIfNull(PayRegData.Rows[i]["NET PAY ADJUSTMENT"].ToString());
                    PayReg["AM" + x].Text = ConvertIfNull(PayRegData.Rows[i]["SSS SL ADJ - NTX"].ToString());
                    PayReg["AN" + x].Text = ConvertIfNull(PayRegData.Rows[i]["Tax Refund"].ToString());
                    PayReg["AO" + x].Text = ConvertIfNull(PayRegData.Rows[i]["Tax Payable"].ToString());
                    PayReg["AP" + x].Text = ConvertIfNull(PayRegData.Rows[i]["Employee SSS"].ToString());
                    PayReg["AQ" + x].Text = ConvertIfNull(PayRegData.Rows[i]["Employee MCR"].ToString());
                    PayReg["AR" + x].Text = ConvertIfNull(PayRegData.Rows[i]["Employee PAgibig"].ToString());
                    PayReg["AS" + x].Text = ConvertIfNull(PayRegData.Rows[i]["Employer SSS"].ToString());
                    PayReg["AT" + x].Text = ConvertIfNull(PayRegData.Rows[i]["Employer MCR"].ToString());
                    PayReg["AU" + x].Text = ConvertIfNull(PayRegData.Rows[i]["Employer Pagibig"].ToString());
                    PayReg["AV" + x].Text = ConvertIfNull(PayRegData.Rows[i]["Payroll Cost"].ToString());
                }
                #endregion
                HttpResponse httpResponse = System.Web.HttpContext.Current.Response;
                httpResponse.Clear();
                httpResponse.ContentType = "Application/x-msexcel";
                httpResponse.AddHeader("Content-Disposition", String.Format("attachment; filename={0}", filename));
                string newDIR = HttpContext.Current.Server.MapPath("~/sFTP/PayrollOutsourcing/");
                string onlineDIR = "http://ws.durusthr.com/ILM_WS_Live/sFTP/PayrollOutsourcing/";
                //filename = filename.Replace(',', '_');
                //filename = filename.Replace(" ", String.Empty);
                workbook.Save(newDIR + filename);
                stream.Close();
                //return "Success";
                if (PayRegData.Rows.Count > 0)
                {
                    return onlineDIR + filename;
                }
                else
                {
                    return "No Rows";
                }
            }
            catch (Exception e)
            {
                stream.Close();
                return "Error: " + e.ToString();
            }
        }
        private string ConvertIfNull(string textboxText)
        {
            return string.Format(Convert.ToDecimal(string.IsNullOrEmpty(textboxText) ? "0.00" : textboxText).ToString(), "0.00");
        }
        [HttpPost]
        [Route("PO_Employee_Information_Report")]
        public string Extract_Employee_Information_Report(EmpInfoReport param)
        {
            FileStream stream = null;
            try
            {
                TempConnectionStringv4(param.db);
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@TenantCode", mytype = SqlDbType.NVarChar, Value = param.TenantCode });
                con.myparameters.Add(new myParameters { ParameterName = "@Status", mytype = SqlDbType.NVarChar, Value = param.Status });
                DataTable EmpData = con.GetDataTable("sp_Extract_EmployeeInfo_Report");
                string dataDir = HostingEnvironment.MapPath("~/Files/PayrollOutSourcing/");
                stream = new FileStream(dataDir + "EmployeeInfoReport.xls", FileMode.Open);
                string filename;
                string datebuild = DateTime.Now.ToString("MMddyyyy");
                string timebuild = DateTime.Now.ToString("hhmmtt");

                //filename = datebuild + timebuild + "_PEZAReport" + ".xls";
                filename = param.TenantCode + "_Employee_Information" + "_" + datebuild + "_" + timebuild + ".xls";
                using (var fileStream = new FileStream(dataDir + filename, FileMode.Create, FileAccess.Write))
                //using (var fileStream = new FileStream(dataDir + filename, FileMode.Create, FileAccess.Write))
                {
                    stream.CopyTo(fileStream);
                }
                stream.Close();
                ExcelWorkbookFormat workbookFormat = ExcelWorkbookFormat.Xls_2003;
                string testDocFile = dataDir + "EmployeeInfoReport.xls";
                ExcelWorkbook workbook = new ExcelWorkbook(testDocFile);
                ExcelWorksheet firstWorksheet = workbook.Worksheets[0];
                ExcelWorksheet Employee_Information = workbook.Worksheets[0];
                int x = 2;
                for (int i = 0; i < EmpData.Rows.Count; i++, x++)
                {
                    Employee_Information["A" + x].Text = EmpData.Rows[i]["EmpId"].ToString();
                    Employee_Information["B" + x].Text = EmpData.Rows[i]["LastName"].ToString();
                    Employee_Information["C" + x].Text = EmpData.Rows[i]["FirstName"].ToString();
                    Employee_Information["D" + x].Text = EmpData.Rows[i]["MiddleName"].ToString();
                    Employee_Information["E" + x].Text = EmpData.Rows[i]["DaystoCredit"].ToString();
                    Employee_Information["F" + x].Text = EmpData.Rows[i]["Rehired"].ToString();
                    Employee_Information["G" + x].Text = EmpData.Rows[i]["Gender"].ToString();
                    Employee_Information["H" + x].Text = EmpData.Rows[i]["Email"].ToString();
                    Employee_Information["I" + x].Text = EmpData.Rows[i]["GRADE LEVEL/BAND"].ToString();
                    Employee_Information["J" + x].Text = EmpData.Rows[i]["CostCenter"].ToString();
                    Employee_Information["K" + x].Text = EmpData.Rows[i]["JobTitle"].ToString();
                    Employee_Information["L" + x].Text = EmpData.Rows[i]["CURRENT DIVISION"].ToString();
                    Employee_Information["M" + x].Text = EmpData.Rows[i]["CURRENT DEPARTMENT"].ToString();
                    Employee_Information["N" + x].Text = EmpData.Rows[i]["DIRECT/INDIRECT LABOR"].ToString();
                    Employee_Information["O" + x].Text = EmpData.Rows[i]["DATE OF BIRTH"].ToString();
                    Employee_Information["P" + x].Text = EmpData.Rows[i]["DATE EMPLOYED"].ToString();
                    Employee_Information["Q" + x].Text = EmpData.Rows[i]["AllowedOT"].ToString();
                    Employee_Information["R" + x].Text = EmpData.Rows[i]["TaxStatus"].ToString();
                    Employee_Information["S" + x].Text = EmpData.Rows[i]["MonthlyRate"].ToString();
                    Employee_Information["T" + x].Text = EmpData.Rows[i]["PAYOUT SCHEME"].ToString();
                    Employee_Information["U" + x].Text = EmpData.Rows[i]["AdditionalHDMF"].ToString();
                    Employee_Information["V" + x].Text = EmpData.Rows[i]["PaymentType"].ToString();
                    Employee_Information["W" + x].Text = EmpData.Rows[i]["AccountNumber"].ToString();
                    Employee_Information["X" + x].Text = EmpData.Rows[i]["PagibigNum"].ToString();
                    Employee_Information["Y" + x].Text = EmpData.Rows[i]["SSSNum"].ToString();
                    Employee_Information["Z" + x].Text = EmpData.Rows[i]["TINnum"].ToString();
                    Employee_Information["AA" + x].Text = EmpData.Rows[i]["PhilhealthNum"].ToString();
                    Employee_Information["AB" + x].Text = EmpData.Rows[i]["EMPLOYMENT STATUS"].ToString();
                    Employee_Information["AC" + x].Text = EmpData.Rows[i]["EmployeeType"].ToString();
                    Employee_Information["AD" + x].Text = EmpData.Rows[i]["ADDRESS LINE 1"].ToString();
                    Employee_Information["AE" + x].Text = EmpData.Rows[i]["ADDRESS LINE 2"].ToString();
                    Employee_Information["AF" + x].Text = EmpData.Rows[i]["HomePhone"].ToString();
                    Employee_Information["AG" + x].Text = EmpData.Rows[i]["AnnualRt"].ToString();
                    Employee_Information["AH" + x].Text = EmpData.Rows[i]["Currency"].ToString();
                    Employee_Information["AI" + x].Text = EmpData.Rows[i]["GL OPERATING UNIT"].ToString();
                    Employee_Information["AJ" + x].Text = EmpData.Rows[i]["GL LOCATION"].ToString();
                    Employee_Information["AK" + x].Text = EmpData.Rows[i]["GL BUSINESS UNIT"].ToString();
                    Employee_Information["AL" + x].Text = EmpData.Rows[i]["GL DEPTID"].ToString();
                    Employee_Information["AM" + x].Text = EmpData.Rows[i]["GL PROJECT"].ToString();
                }
                HttpResponse httpResponse = System.Web.HttpContext.Current.Response;
                httpResponse.Clear();
                httpResponse.ContentType = "Application/x-msexcel";
                httpResponse.AddHeader("Content-Disposition", String.Format("attachment; filename={0}", filename));
                string newDIR = HttpContext.Current.Server.MapPath("~/sFTP/PayrollOutsourcing/");
                string onlineDIR = "http://ws.durusthr.com/ILM_WS_Live/sFTP/PayrollOutsourcing/";
                //filename = filename.Replace(',', '_');
                //filename = filename.Replace(" ", String.Empty);
                workbook.Save(newDIR + filename);
                stream.Close();
                //return "Success";
                if (EmpData.Rows.Count > 0)
                {
                    return onlineDIR + filename;
                }
                else
                {
                    return "No Rows";
                }
            }
            catch (Exception e)
            {
                stream.Close();
                return "Error: " + e.ToString();
            }
        }


        [HttpPost]
        [Route("PO_PHIC_EPRS_Report")]
        public string Extract_PHIC_EPRS_Report(EPRSReport param)
        {
            FileStream stream = null;
            try
            {
                TempConnectionStringv4(param.db);
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@Month", mytype = SqlDbType.NVarChar, Value = param.Month });
                con.myparameters.Add(new myParameters { ParameterName = "@Year", mytype = SqlDbType.NVarChar, Value = param.Year });
                con.myparameters.Add(new myParameters { ParameterName = "@TenantCode", mytype = SqlDbType.NVarChar, Value = param.TenantCode });
                DataTable EPRSData = con.GetDataTable("sp_Extract_PHIC_EPRS");
                string dataDir = HostingEnvironment.MapPath("~/Files/PayrollOutSourcing/");
                stream = new FileStream(dataDir + "EPRSReport.xls", FileMode.Open);
                string filename;
                string datebuild = DateTime.Now.ToString("MMddyyyy");
                string timebuild = DateTime.Now.ToString("hhmmtt");

                //filename = datebuild + timebuild + "_PEZAReport" + ".xls";
                filename = param.TenantCode + "_PHIC_EPRS" + "_" + param.Year + param.Month + ".xls";
                using (var fileStream = new FileStream(dataDir + filename, FileMode.Create, FileAccess.Write))
                //using (var fileStream = new FileStream(dataDir + filename, FileMode.Create, FileAccess.Write))
                {
                    stream.CopyTo(fileStream);
                }
                stream.Close();
                ExcelWorkbookFormat workbookFormat = ExcelWorkbookFormat.Xls_2003;
                string testDocFile = dataDir + "EPRSReport.xls";
                ExcelWorkbook workbook = new ExcelWorkbook(testDocFile);
                ExcelWorksheet firstWorksheet = workbook.Worksheets[0];
                ExcelWorksheet EPRS = workbook.Worksheets[0];
                int x = 2;
                for (int i = 0; i < EPRSData.Rows.Count; i++, x++)
                {
                    EPRS["A" + x].Text = EPRSData.Rows[i]["PhilhealthNum"].ToString();
                    EPRS["B" + x].Text = EPRSData.Rows[i]["MonthlyRate"].ToString();
                    EPRS["C" + x].Text = EPRSData.Rows[i]["EE_Stat"].ToString();
                    EPRS["D" + x].Text = EPRSData.Rows[i]["EFF_Date"].ToString();
                    EPRS["E" + x].Text = EPRSData.Rows[i]["BirthDate"].ToString();

                }
                HttpResponse httpResponse = System.Web.HttpContext.Current.Response;
                httpResponse.Clear();
                httpResponse.ContentType = "Application/x-msexcel";
                httpResponse.AddHeader("Content-Disposition", String.Format("attachment; filename={0}", filename));
                string newDIR = HttpContext.Current.Server.MapPath("~/sFTP/PayrollOutsourcing/");
                string onlineDIR = "https://ws.durusthr.com/ILM_WS_Live/sFTP/PayrollOutsourcing/";
                //filename = filename.Replace(',', '_');
                //filename = filename.Replace(" ", String.Empty);
                workbook.Save(newDIR + filename);
                stream.Close();
                //return "Success";
                if (EPRSData.Rows.Count > 0)
                {
                    return onlineDIR + filename;
                }
                else
                {
                    return "No Rows";
                }
            }
            catch (Exception e)
            {
                stream.Close();
                return "Error: " + e.ToString();
            }
        }

        [HttpPost]
        [Route("Extract_PH_ER2")]
        public string Extract_PH_ER2(efile param)
        {
            FileStream stream = null;
            try
            {
                string dataDir = HostingEnvironment.MapPath("~/Files/PayrollOutSourcing/");

                stream = new FileStream(dataDir + "Template_PH_ER2.xls", FileMode.Open);
                string filename;
                string datebuild = DateTime.Now.ToString("MMddyyyy");
                string datebuild1 = DateTime.Now.ToString("yyyyMM");
                string timebuild = DateTime.Now.ToString("hhtt");
                string date1 = "";
                string date2 = "";
                string fdate1 = "";
                string fdate2 = "";

                TempConnectionStringv4(param.db);
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@TenantID", mytype = SqlDbType.NVarChar, Value = param.TenantID });
                con.myparameters.Add(new myParameters { ParameterName = "@Year", mytype = SqlDbType.NVarChar, Value = param.year });
                con.myparameters.Add(new myParameters { ParameterName = "@Month", mytype = SqlDbType.NVarChar, Value = param.month });
                //   con.myparameters.Add(new myParameters { ParameterName = "@payoutdate", mytype = SqlDbType.NVarChar, Value = param.PayoutDate });
                //   con.myparameters.Add(new myParameters { ParameterName = "@EmpStatus", mytype = SqlDbType.VarChar, Value = param.EmpStatus });
                DataTable DT = con.GetDataTable("sp_Export_PH_ER2");
                TempConnectionStringv4(param.db);
                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@TenantID", mytype = SqlDbType.NVarChar, Value = param.TenantID });
                DataTable DT1 = con.GetDataTable("sp_getTenantCode");
                string companyCode = DT1.Rows[0]["code"].ToString();
                for (int i = 0; i < DT.Rows.Count; i++)
                {
                    // companyCode = DT.Rows[0]["cCode"].ToString();
                    date1 = DT.Rows[0]["firstday"].ToString();
                    date2 = DT.Rows[0]["lastday"].ToString();
                    //fdate1 = DT.Rows[0]["fndate1"].ToString();
                    // fdate2 = DT.Rows[0]["fndate2"].ToString();
                    break;
                }
                filename = companyCode + "_PH_ER2_" + param.year + param.month + ".xls";
                using (var fileStream = new FileStream(dataDir + filename, FileMode.Create, FileAccess.Write))
                //using (var fileStream = new FileStream(dataDir + filename, FileMode.Create, FileAccess.Write))
                {
                    stream.CopyTo(fileStream);
                }
                stream.Close();
                ExcelWorkbookFormat workbookFormat = ExcelWorkbookFormat.Xls_2003;
                string testDocFile = dataDir + "Template_PH_ER2.xls";
                ExcelWorkbook workbook = new ExcelWorkbook(testDocFile);
                ExcelWorksheet firstWorksheet = workbook.Worksheets[0];



                int x = 2;
                for (int i = 0; i < DT.Rows.Count; i++, x++)
                {

                    firstWorksheet["A" + x].Text = DT.Rows[i]["EmployeeID"].ToString();
                    firstWorksheet["B" + x].Text = DT.Rows[i]["PayrollID"].ToString();
                    firstWorksheet["C" + x].Text = DT.Rows[i]["LastName"].ToString();
                    firstWorksheet["D" + x].Text = DT.Rows[i]["FirstName"].ToString();
                    firstWorksheet["E" + x].Text = DT.Rows[i]["MiddleName"].ToString();
                    firstWorksheet["F" + x].Text = DT.Rows[i]["Position"].ToString();
                    firstWorksheet["G" + x].Text = DT.Rows[i]["DateEmployed"].ToString();
                    firstWorksheet["H" + x].Text = DT.Rows[i]["SalaryRate"].ToString();
                    firstWorksheet["I" + x].Text = DT.Rows[i]["Salary"].ToString();
                    firstWorksheet["J" + x].Text = DT.Rows[i]["Paycode"].ToString();
                    firstWorksheet["K" + x].Text = DT.Rows[i]["SSS"].ToString();
                    firstWorksheet["L" + x].Text = DT.Rows[i]["PhilHealth"].ToString();
                    firstWorksheet["M" + x].Text = DT.Rows[i]["FileStatus"].ToString();
                    firstWorksheet["N" + x].Text = DT.Rows[i]["Birthdate"].ToString();
                    firstWorksheet["O" + x].Text = DT.Rows[i]["CompanyName"].ToString();
                    firstWorksheet["P" + x].Text = DT.Rows[i]["AnnualWorking"].ToString();
                    firstWorksheet["Q" + x].Text = DT.Rows[i]["SalaryCredit"].ToString();



                }
                //    firstWorksheet.Name = companyCode  +"_"+fdate1+ "_"+fdate2 + datebuild1;
                HttpResponse httpResponse = System.Web.HttpContext.Current.Response;

                httpResponse.Clear();
                httpResponse.ContentType = "Application/x-msexcel";
                httpResponse.AddHeader("Content-Disposition", String.Format("attachment; filename={0}", filename));
                string newDIR = HttpContext.Current.Server.MapPath("~/sFTP/PayrollOutsourcing/");
                string onlineDIR = "https://ws.durusthr.com/ILM_WS_Live/sFTP/PayrollOutsourcing/";
                //filename = filename.Replace(',', '_');
                //filename = filename.Replace(" ", String.Empty);
                workbook.Save(newDIR + filename);
                stream.Close();
                return onlineDIR + filename;
            }
            catch (Exception e)
            {
                return e.ToString();
            }

        }

        [Route("Export_HDMFPYMNT")]
        public string ExtractExport_HDMFPYMNT(efile param)
        {
            try
            {
                FileStream stream = null;
                string dataDir = HostingEnvironment.MapPath("~/Files/PayrollOutSourcing/");

                stream = new FileStream(dataDir + "Template_HDMFPYMNT.xls", FileMode.Open);
                string filename;
                string datebuild = DateTime.Now.ToString("MMddyyyy");
                string datebuild1 = DateTime.Now.ToString("yyyyMM");
                string timebuild = DateTime.Now.ToString("hhtt");
                string date1 = "";
                string date2 = "";
                string fdate1 = "";
                string fdate2 = "";

                TempConnectionStringv4(param.db);
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@TenantID", mytype = SqlDbType.NVarChar, Value = param.TenantID });
                con.myparameters.Add(new myParameters { ParameterName = "@Year", mytype = SqlDbType.NVarChar, Value = param.year });
                con.myparameters.Add(new myParameters { ParameterName = "@Month", mytype = SqlDbType.NVarChar, Value = param.month });
                //   con.myparameters.Add(new myParameters { ParameterName = "@payoutdate", mytype = SqlDbType.NVarChar, Value = param.PayoutDate });
                //   con.myparameters.Add(new myParameters { ParameterName = "@EmpStatus", mytype = SqlDbType.VarChar, Value = param.EmpStatus });
                DataTable DT = con.GetDataTable("sp_Export_HDMFPYMNT");
                string companyCode = "";
                for (int i = 0; i < DT.Rows.Count; i++)
                {
                    companyCode = DT.Rows[0]["cCode"].ToString();
                    date1 = DT.Rows[0]["firstday"].ToString();
                    date2 = DT.Rows[0]["lastday"].ToString();
                    fdate1 = DT.Rows[0]["fndate1"].ToString();
                    fdate2 = DT.Rows[0]["fndate2"].ToString();
                    break;
                }
                filename = companyCode + "_HDMFPYMNT_" + param.year + param.month + ".xls";
                using (var fileStream = new FileStream(dataDir + filename, FileMode.Create, FileAccess.Write))
                //using (var fileStream = new FileStream(dataDir + filename, FileMode.Create, FileAccess.Write))
                {
                    stream.CopyTo(fileStream);
                }
                stream.Close();
                ExcelWorkbookFormat workbookFormat = ExcelWorkbookFormat.Xls_2003;
                string testDocFile = dataDir + "Template_HDMFPYMNT.xls";
                ExcelWorkbook workbook = new ExcelWorkbook(testDocFile);
                ExcelWorksheet firstWorksheet = workbook.Worksheets[0];



                int x = 2;
                for (int i = 0; i < DT.Rows.Count; i++, x++)
                {

                    firstWorksheet["A" + x].Text = DT.Rows[i]["LastName"].ToString();
                    firstWorksheet["B" + x].Text = DT.Rows[i]["FirstName"].ToString();
                    firstWorksheet["C" + x].Text = DT.Rows[i]["MiddleName"].ToString();
                    firstWorksheet["D" + x].Text = DT.Rows[i]["EE"].ToString();
                    firstWorksheet["E" + x].Text = DT.Rows[i]["ER"].ToString();
                    firstWorksheet["F" + x].Text = DT.Rows[i]["EmployeeNumber"].ToString();
                    firstWorksheet["G" + x].Text = DT.Rows[i]["TIN"].ToString();
                    firstWorksheet["H" + x].Text = DT.Rows[i]["Birthdate"].ToString();
                    firstWorksheet["I" + x].Text = DT.Rows[i]["Company"].ToString();



                }
                firstWorksheet.Name = companyCode + "_" + fdate1 + "_" + fdate2 + datebuild1;
                HttpResponse httpResponse = System.Web.HttpContext.Current.Response;

                httpResponse.Clear();
                httpResponse.ContentType = "Application/x-msexcel";
                httpResponse.AddHeader("Content-Disposition", String.Format("attachment; filename={0}", filename));
                string newDIR = HttpContext.Current.Server.MapPath("~/sFTP/PayrollOutsourcing/");
                string onlineDIR = "https://ws.durusthr.com/ILM_WS_Live/sFTP/PayrollOutsourcing/";
                //filename = filename.Replace(',', '_');
                //filename = filename.Replace(" ", String.Empty);
                workbook.Save(newDIR + filename);
                stream.Close();
                return onlineDIR + filename;
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }

        [HttpPost]
        [Route("PO_NTX_Report")]
        public string Extract_NTX_Report(NTXReport param)
        {
            FileStream stream = null;
            try
            {
                TempConnectionStringv4(param.db);
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@TenantCode", mytype = SqlDbType.NVarChar, Value = param.TenantCode });
                con.myparameters.Add(new myParameters { ParameterName = "@Month", mytype = SqlDbType.NVarChar, Value = param.Month });
                con.myparameters.Add(new myParameters { ParameterName = "@Year", mytype = SqlDbType.NVarChar, Value = param.Year });
                DataTable NTXData = con.GetDataTable("sp_Extract_NTX_Report");
                string dataDir = HostingEnvironment.MapPath("~/Files/PayrollOutSourcing/");
                stream = new FileStream(dataDir + "NTXReport.xls", FileMode.Open);
                string filename;
                string datebuild = DateTime.Now.ToString("MMddyyyy");
                string timebuild = DateTime.Now.ToString("hhmmtt");

                filename = param.TenantCode + "_NTXReport_" + param.Month + param.Year + "_" + timebuild + ".xls";
                using (var fileStream = new FileStream(dataDir + filename, FileMode.Create, FileAccess.Write))
                //using (var fileStream = new FileStream(dataDir + filename, FileMode.Create, FileAccess.Write))
                {
                    stream.CopyTo(fileStream);
                }
                stream.Close();
                ExcelWorkbookFormat workbookFormat = ExcelWorkbookFormat.Xls_2003;
                string testDocFile = dataDir + "NTXReport.xls";
                ExcelWorkbook workbook = new ExcelWorkbook(testDocFile);
                ExcelWorksheet firstWorksheet = workbook.Worksheets[0];
                ExcelWorksheet NTX = workbook.Worksheets[0];
                int x = 2;
                for (int i = 0; i < NTXData.Rows.Count; i++, x++)
                {
                    NTX["A" + x].Text = NTXData.Rows[i]["Company Name"].ToString();
                    NTX["B" + x].Text = NTXData.Rows[i]["Income Code"].ToString();
                    NTX["C" + x].Text = NTXData.Rows[i]["Tag"].ToString();
                    NTX["D" + x].Text = NTXData.Rows[i]["NTX"].ToString();
                }
                HttpResponse httpResponse = System.Web.HttpContext.Current.Response;
                httpResponse.Clear();
                httpResponse.ContentType = "Application/x-msexcel";
                httpResponse.AddHeader("Content-Disposition", String.Format("attachment; filename={0}", filename));
                string newDIR = HttpContext.Current.Server.MapPath("~/sFTP/PayrollOutsourcing/");
                string onlineDIR = "http://ws.durusthr.com/ILM_WS_Live/sFTP/PayrollOutsourcing/";
                //filename = filename.Replace(',', '_');
                //filename = filename.Replace(" ", String.Empty);
                workbook.Save(newDIR + filename);
                stream.Close();
                //return "Success";
                if (NTXData.Rows.Count > 0)
                {
                    return onlineDIR + filename;
                }
                else
                {
                    return "No Rows";
                }
            }
            catch (Exception e)
            {
                stream.Close();
                return "Error: " + e.ToString();
            }
        }
        [HttpPost]
        [Route("PO_PEZA_Report")]
        public string Extract_PEZA_Report(PEZAReport param)
        {
            FileStream stream = null;
            try
            {
                TempConnectionStringv4(param.db);
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@Month", mytype = SqlDbType.NVarChar, Value = param.Month });
                con.myparameters.Add(new myParameters { ParameterName = "@Year", mytype = SqlDbType.NVarChar, Value = param.Year });
                con.myparameters.Add(new myParameters { ParameterName = "@TenantCode", mytype = SqlDbType.NVarChar, Value = param.TenantCode });
                DataTable PEZAData = con.GetDataTable("sp_Extract_PEZA_Report");
                string dataDir = HostingEnvironment.MapPath("~/Files/PayrollOutSourcing/");
                stream = new FileStream(dataDir + "PEZAReport.xls", FileMode.Open);
                string filename;
                string datebuild = DateTime.Now.ToString("MMddyyyy");
                string timebuild = DateTime.Now.ToString("hhmmtt");



                filename = param.TenantCode + "_PEZAReport_" + param.Year + "_" + param.Month + ".xls";
                using (var fileStream = new FileStream(dataDir + filename, FileMode.Create, FileAccess.Write))
                //using (var fileStream = new FileStream(dataDir + filename, FileMode.Create, FileAccess.Write))
                {
                    stream.CopyTo(fileStream);
                }
                stream.Close();
                ExcelWorkbookFormat workbookFormat = ExcelWorkbookFormat.Xls_2003;
                string testDocFile = dataDir + "PEZAReport.xls";
                ExcelWorkbook workbook = new ExcelWorkbook(testDocFile);
                ExcelWorksheet firstWorksheet = workbook.Worksheets[0];
                ExcelWorksheet PEZA = workbook.Worksheets[0];
                int x = 2;
                for (int i = 0; i < PEZAData.Rows.Count; i++, x++)
                {
                    PEZA["A" + x].Text = PEZAData.Rows[i]["PERIOD START"].ToString();
                    PEZA["B" + x].Text = PEZAData.Rows[i]["PERIOD END"].ToString();
                    PEZA["C" + x].Value = PEZAData.Rows[i]["PEZA CLASSIFICATION"];
                    PEZA["D" + x].Value = PEZAData.Rows[i]["F-COUNT"];
                    PEZA["E" + x].Value = PEZAData.Rows[i]["M-COUNT"];
                    PEZA["F" + x].Value = PEZAData.Rows[i]["F-TOTAL"];
                    PEZA["G" + x].Value = PEZAData.Rows[i]["F-TAXABLE"];
                    PEZA["H" + x].Value = PEZAData.Rows[i]["F-NONTAXABLE"];
                    PEZA["I" + x].Value = PEZAData.Rows[i]["M-TOTAL"];
                    PEZA["J" + x].Value = PEZAData.Rows[i]["M-TAXABLE"];
                    PEZA["K" + x].Value = PEZAData.Rows[i]["M-NONTAXABLE"];
                }
                HttpResponse httpResponse = System.Web.HttpContext.Current.Response;
                httpResponse.Clear();
                httpResponse.ContentType = "Application/x-msexcel";
                httpResponse.AddHeader("Content-Disposition", String.Format("attachment; filename={0}", filename));
                string newDIR = HttpContext.Current.Server.MapPath("~/sFTP/PayrollOutsourcing/");
                string onlineDIR = "http://ws.durusthr.com/ILM_WS_Live/sFTP/PayrollOutsourcing/";
                //filename = filename.Replace(',', '_');
                //filename = filename.Replace(" ", String.Empty);
                workbook.Save(newDIR + filename);
                stream.Close();
                //return "Success";
                if (PEZAData.Rows.Count > 0)
                {
                    return onlineDIR + filename;
                }
                else
                {
                    return "No Rows";
                }
            }
            catch (Exception e)
            {
                stream.Close();
                return "Error: " + e.ToString();
            }
        }
        [HttpPost]
        [Route("Extract1601C")]
        public string Extract1601C(efile param)
        {
            try
            {
                FileStream stream = null;
                string dataDir = HostingEnvironment.MapPath("~/Files/PayrollOutSourcing/");



                stream = new FileStream(dataDir + "Template1601C.xls", FileMode.Open);
                string filename;
                string datebuild = DateTime.Now.ToString("MMddyyyy");
                string datebuild1 = DateTime.Now.ToString("yyyyMM");
                string timebuild = DateTime.Now.ToString("hhtt");



                TempConnectionStringv4(param.db);
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@TenantID", mytype = SqlDbType.NVarChar, Value = param.TenantID });
                con.myparameters.Add(new myParameters { ParameterName = "@year", mytype = SqlDbType.NVarChar, Value = param.year });
                con.myparameters.Add(new myParameters { ParameterName = "@month", mytype = SqlDbType.NVarChar, Value = param.month });
                // con.myparameters.Add(new myParameters { ParameterName = "@payoutdate", mytype = SqlDbType.NVarChar, Value = param.PayoutDate });
                //   con.myparameters.Add(new myParameters { ParameterName = "@EmpStatus", mytype = SqlDbType.VarChar, Value = param.EmpStatus });
                DataTable DT = con.GetDataTable("sp_Export_1601C");
                string companyCode = "";
                for (int i = 0; i < DT.Rows.Count; i++)
                {
                    companyCode = DT.Rows[0]["cCode"].ToString();
                    break;
                }
                filename = companyCode + "_1601C_" + param.year + param.month + "_" + timebuild + ".xls";
                using (var fileStream = new FileStream(dataDir + filename, FileMode.Create, FileAccess.Write))
                //using (var fileStream = new FileStream(dataDir + filename, FileMode.Create, FileAccess.Write))
                {
                    stream.CopyTo(fileStream);
                }
                stream.Close();
                ExcelWorkbookFormat workbookFormat = ExcelWorkbookFormat.Xls_2003;
                string testDocFile = dataDir + "Template1601C.xls";
                ExcelWorkbook workbook = new ExcelWorkbook(testDocFile);
                ExcelWorksheet firstWorksheet = workbook.Worksheets[0];





                int x = 2;
                for (int i = 0; i < DT.Rows.Count; i++, x++)
                {



                    firstWorksheet["A" + x].Text = DT.Rows[i]["EmpID"].ToString();
                    firstWorksheet["B" + x].Text = DT.Rows[i]["TaxTable"].ToString();
                    firstWorksheet["C" + x].Text = DT.Rows[i]["PayCode"].ToString();
                    firstWorksheet["D" + x].Text = DT.Rows[i]["PeriodFrom"].ToString();
                    firstWorksheet["E" + x].Text = DT.Rows[i]["PerioTo"].ToString();
                    firstWorksheet["F" + x].Value = DT.Rows[i]["Basic"];
                    firstWorksheet["G" + x].Value = DT.Rows[i]["MISC"];
                    firstWorksheet["H" + x].Value = DT.Rows[i]["Overtime"];
                    firstWorksheet["I" + x].Value = DT.Rows[i]["Tax"];
                    firstWorksheet["J" + x].Value = DT.Rows[i]["GrossIncome"];
                    firstWorksheet["K" + x].Value = DT.Rows[i]["WitholdingTax"];
                    firstWorksheet["L" + x].Value = DT.Rows[i]["EmployeeSSS"];
                    firstWorksheet["M" + x].Value = DT.Rows[i]["EmployeeMCR"];
                    firstWorksheet["N" + x].Value = DT.Rows[i]["EmployeePagibig"];
                    firstWorksheet["O" + x].Value = DT.Rows[i]["OtherNTXIncome"];
                    firstWorksheet["P" + x].Value = DT.Rows[i]["EmployerSSS"];
                    firstWorksheet["Q" + x].Value = DT.Rows[i]["EmployerMCR"];
                    firstWorksheet["R" + x].Value = DT.Rows[i]["EmployerEC"];
                    firstWorksheet["S" + x].Value = DT.Rows[i]["EmployerPagibig"];
                    firstWorksheet["T" + x].Value = DT.Rows[i]["NTDeminis"];
                    firstWorksheet["U" + x].Value = DT.Rows[i]["NT13thMonth"];
                    firstWorksheet["V" + x].Value = DT.Rows[i]["NTSSS_PHIC_HDMF"];
                    firstWorksheet["W" + x].Value = DT.Rows[i]["NTOhters"];
                    firstWorksheet["X" + x].Value = DT.Rows[i]["MWEBasic"];
                    firstWorksheet["Y" + x].Value = DT.Rows[i]["MWEOT"];
                    firstWorksheet["Z" + x].Value = DT.Rows[i]["MWEOther"];
                    firstWorksheet["AA" + x].Value = DT.Rows[i]["Below20K"];
                    firstWorksheet["AB" + x].Value = DT.Rows[i]["ExcessHDMF"];
                    firstWorksheet["AC" + x].Value = DT.Rows[i]["taxRefund"];
                    firstWorksheet["AD" + x].Value = DT.Rows[i]["taxPayable"];
                }
                firstWorksheet.Name = companyCode + "1601C_" + param.year + param.month;
                HttpResponse httpResponse = System.Web.HttpContext.Current.Response;



                httpResponse.Clear();
                httpResponse.ContentType = "Application/x-msexcel";
                httpResponse.AddHeader("Content-Disposition", String.Format("attachment; filename={0}", filename));
                string newDIR = HttpContext.Current.Server.MapPath("~/sFTP/PayrollOutsourcing/");
                string onlineDIR = "https://ws.durusthr.com/ILM_WS_Live/sFTP/PayrollOutsourcing/";
                //filename = filename.Replace(',', '_');
                //filename = filename.Replace(" ", String.Empty);
                workbook.Save(newDIR + filename);
                stream.Close();
                return onlineDIR + filename;
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }
        [HttpPost]
        [Route("WS_PublishedSettingv2")]
        public string PublishedSet(settingPublish param)
        {
            try
            {
                DataTable DTParam = new DataTable();
                DTParam.Columns.AddRange(new DataColumn[1] { new DataColumn("UserId") });
                for (int i = 0; i < param.empID.Length; i++)
                {
                    DTParam.Rows.Add(param.empID[i]);
                }
                TempConnectionStringv4(param.db);
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@tenantID", mytype = SqlDbType.VarChar, Value = param.tenantID });
                con.myparameters.Add(new myParameters { ParameterName = "@import", mytype = SqlDbType.Structured, Value = DTParam });
                con.myparameters.Add(new myParameters { ParameterName = "@payOutDate", mytype = SqlDbType.VarChar, Value = param.payOutDate });
                con.myparameters.Add(new myParameters { ParameterName = "@setting", mytype = SqlDbType.VarChar, Value = param.setting });
                con.myparameters.Add(new myParameters { ParameterName = "@publishedBy", mytype = SqlDbType.VarChar, Value = param.publishedBy });
                con.ExecuteNonQuery("sp_PublishedSetting");
                return "Success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }

        [HttpPost]
        [Route("ExtractSSS")]
        public string ExtractSSS(efile param)
        {
            try
            {
                FileStream stream = null;
                string dataDir = HostingEnvironment.MapPath("~/Files/PayrollOutSourcing/");

                stream = new FileStream(dataDir + "ExportSSS.xls", FileMode.Open);
                string filename;
                string datebuild = DateTime.Now.ToString("MMddyyyy");
                string datebuild1 = DateTime.Now.ToString("yyyyMM");
                string timebuild = DateTime.Now.ToString("hhtt");

                TempConnectionStringv4(param.db);
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@TenantID", mytype = SqlDbType.NVarChar, Value = param.TenantID });
                con.myparameters.Add(new myParameters { ParameterName = "@year", mytype = SqlDbType.NVarChar, Value = param.year });
                con.myparameters.Add(new myParameters { ParameterName = "@month", mytype = SqlDbType.NVarChar, Value = param.month });
                //   con.myparameters.Add(new myParameters { ParameterName = "@payoutdate", mytype = SqlDbType.NVarChar, Value = param.PayoutDate });
                //   con.myparameters.Add(new myParameters { ParameterName = "@EmpStatus", mytype = SqlDbType.VarChar, Value = param.EmpStatus });
                DataTable DT = con.GetDataTable("ExportSSS");
                string companyCode = "";
                for (int i = 0; i < DT.Rows.Count; i++)
                {
                    companyCode = DT.Rows[0]["cCode"].ToString();
                    break;
                }
                filename = companyCode + "SSPHHD" + param.year + param.month + ".xls";
                using (var fileStream = new FileStream(dataDir + filename, FileMode.Create, FileAccess.Write))
                //using (var fileStream = new FileStream(dataDir + filename, FileMode.Create, FileAccess.Write))
                {
                    stream.CopyTo(fileStream);
                }
                stream.Close();
                ExcelWorkbookFormat workbookFormat = ExcelWorkbookFormat.Xls_2003;
                string testDocFile = dataDir + "ExportSSS.xls";
                ExcelWorkbook workbook = new ExcelWorkbook(testDocFile);
                ExcelWorksheet firstWorksheet = workbook.Worksheets[0];



                int x = 2;
                for (int i = 0; i < DT.Rows.Count; i++, x++)
                {

                    firstWorksheet["A" + x].Text = DT.Rows[i]["FileStatus"].ToString();
                    firstWorksheet["B" + x].Text = DT.Rows[i]["EmpID"].ToString();
                    firstWorksheet["C" + x].Text = DT.Rows[i]["LastName"].ToString();
                    firstWorksheet["D" + x].Text = DT.Rows[i]["FirstName"].ToString();
                    firstWorksheet["E" + x].Text = DT.Rows[i]["Paycode"].ToString();
                    firstWorksheet["F" + x].Text = DT.Rows[i]["EmployeeSSS"].ToString();
                    firstWorksheet["G" + x].Text = DT.Rows[i]["EmployeeMCR"].ToString();
                    firstWorksheet["H" + x].Text = DT.Rows[i]["EmployeePagibig"].ToString();
                    firstWorksheet["I" + x].Text = DT.Rows[i]["EmployerSSS"].ToString();
                    firstWorksheet["J" + x].Text = DT.Rows[i]["EmployerMCR"].ToString();
                    firstWorksheet["K" + x].Text = DT.Rows[i]["EmployerEC"].ToString();
                    firstWorksheet["L" + x].Text = DT.Rows[i]["EmployerPagibig"].ToString();
                    firstWorksheet["M" + x].Text = DT.Rows[i]["PeriodTo"].ToString();
                    firstWorksheet["N" + x].Text = DT.Rows[i]["PeriodFrom"].ToString();
                    firstWorksheet["O" + x].Text = DT.Rows[i]["costCenter"].ToString();

                }
                firstWorksheet.Name = companyCode + "_1601C_" + datebuild1;
                HttpResponse httpResponse = System.Web.HttpContext.Current.Response;

                httpResponse.Clear();
                httpResponse.ContentType = "Application/x-msexcel";
                httpResponse.AddHeader("Content-Disposition", String.Format("attachment; filename={0}", filename));
                string newDIR = HttpContext.Current.Server.MapPath("~/sFTP/PayrollOutsourcing/");
                string onlineDIR = "https://ws.durusthr.com/ILM_WS_Live/sFTP/PayrollOutsourcing/";
                //filename = filename.Replace(',', '_');
                //filename = filename.Replace(" ", String.Empty);
                workbook.Save(newDIR + filename);
                stream.Close();
                return onlineDIR + filename;
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }
        [HttpPost]
        [Route("ExtractTest")]
        public string ExtractTest(efile param)
        {
            try
            {
                TempConnectionStringv2();
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@TenantID", mytype = SqlDbType.NVarChar, Value = param.TenantID });
                con.myparameters.Add(new myParameters { ParameterName = "@CutOffStart", mytype = SqlDbType.NVarChar, Value = param.CutoffStart });
                con.myparameters.Add(new myParameters { ParameterName = "@CutOffEnd", mytype = SqlDbType.NVarChar, Value = param.CutoffEnd });
                con.myparameters.Add(new myParameters { ParameterName = "@date1", mytype = SqlDbType.NVarChar, Value = param.date });
                //   con.myparameters.Add(new myParameters { ParameterName = "@payoutdate", mytype = SqlDbType.NVarChar, Value = param.PayoutDate });
                //   con.myparameters.Add(new myParameters { ParameterName = "@EmpStatus", mytype = SqlDbType.VarChar, Value = param.EmpStatus });
                DataTable DT = con.GetDataTable("sp_ExportWHouse");
                string newDIR = HttpContext.Current.Server.MapPath("~/sFTP/PayrollOutsourcing/");
                FileStream fs1 = new FileStream(newDIR + "WHOUSE.txt", FileMode.Create, FileAccess.Write);
                StreamWriter writer = new StreamWriter(fs1);
                for (int x = 0; x < DT.Rows.Count; x++)
                {
                    if (x == DT.Rows.Count - 1)
                    {
                        writer.Write(DT.Rows[x]["Data"].ToString());
                    }
                    else
                    {
                        writer.Write(DT.Rows[x]["Data"].ToString() + Environment.NewLine);
                    }
                }
                writer.Close();
                string onlineDIR = "http://ws.durusthr.com/ILM_WS_Live/sFTP/PayrollOutsourcing/";
                return onlineDIR + "WHOUSE.txt";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }
        [Route("ExtractWHOUSE")]
        public string ExtractWHOUSE(efile param)
        {
            try
            {
                TempConnectionStringv2();
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@TenantID", mytype = SqlDbType.NVarChar, Value = param.TenantID });
                con.myparameters.Add(new myParameters { ParameterName = "@CutOffStart", mytype = SqlDbType.NVarChar, Value = param.CutoffStart });
                con.myparameters.Add(new myParameters { ParameterName = "@CutOffEnd", mytype = SqlDbType.NVarChar, Value = param.CutoffEnd });
                con.myparameters.Add(new myParameters { ParameterName = "@date1", mytype = SqlDbType.NVarChar, Value = param.date });
                //   con.myparameters.Add(new myParameters { ParameterName = "@payoutdate", mytype = SqlDbType.NVarChar, Value = param.PayoutDate });
                //   con.myparameters.Add(new myParameters { ParameterName = "@EmpStatus", mytype = SqlDbType.VarChar, Value = param.EmpStatus });
                DataTable DT = con.GetDataTable("sp_ExportWHouse");
                //string newDIR = HttpContext.Current.Server.MapPath("~/sFTP/PayrollOutsourcing/");
                //FileStream fs1 = new FileStream(newDIR + "WHOUSE.txt", FileMode.OpenOrCreate, FileAccess.Write);
                //StreamWriter writer = new StreamWriter(fs1);
                string text = "";

                for (int x = 0; x < DT.Rows.Count; x++)
                {
                    if (x == DT.Rows.Count - 1)
                    {
                        text = text + DT.Rows[x]["Data"].ToString();
                    }
                    else
                    {
                        text = text + DT.Rows[x]["Data"].ToString() + "\n";
                    }
                }
                // writer.Close();
                //   string onlineDIR = "http://ws.durusthr.com/ILM_WS_Live/sFTP/PayrollOutsourcing/";



                return text;
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }
        [HttpPost]
        [Route("ExtractWhousev3")]
        public string ExtractWhousev3(efile param)
        {
            try
            {
                DataTable DTParam = new DataTable();
                DTParam.Columns.AddRange(new DataColumn[1] { new DataColumn("empid") });
                for (int i = 0; i < param.empid.Length; i++)
                {
                    DTParam.Rows.Add(param.empid[i]);
                }



                TempConnectionStringv2();
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@TenantID", mytype = SqlDbType.NVarChar, Value = param.TenantID });
                con.myparameters.Add(new myParameters { ParameterName = "@CutOffStart", mytype = SqlDbType.NVarChar, Value = param.CutoffStart });
                con.myparameters.Add(new myParameters { ParameterName = "@CutOffEnd", mytype = SqlDbType.NVarChar, Value = param.CutoffEnd });
                con.myparameters.Add(new myParameters { ParameterName = "@date1", mytype = SqlDbType.NVarChar, Value = param.date });
                con.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.Structured, Value = DTParam });
                //   con.myparameters.Add(new myParameters { ParameterName = "@payoutdate", mytype = SqlDbType.NVarChar, Value = param.PayoutDate });
                //   con.myparameters.Add(new myParameters { ParameterName = "@EmpStatus", mytype = SqlDbType.VarChar, Value = param.EmpStatus });
                DataTable DT = con.GetDataTable("sp_ExportWHousev3");
                string newDIR = HttpContext.Current.Server.MapPath("~/sFTP/PayrollOutsourcing/");
                FileStream fs1 = new FileStream(newDIR + "WHOUSE.txt", FileMode.Create, FileAccess.Write);
                StreamWriter writer = new StreamWriter(fs1);
                for (int x = 0; x < DT.Rows.Count; x++)
                {
                    if (x == DT.Rows.Count - 1)
                    {
                        writer.Write(DT.Rows[x]["Data"].ToString());
                    }
                    else
                    {
                        writer.Write(DT.Rows[x]["Data"].ToString() + Environment.NewLine);
                    }
                }
                writer.Close();
                string onlineDIR = "https://ws.durusthr.com/ILM_WS_Live/sFTP/PayrollOutsourcing/";
                return onlineDIR + "WHOUSE.txt";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }
        [HttpPost]
        [Route("ExtractWhousev4")]
        public string ExtractWhousev4(efile param)
        {
            try
            {
                DataTable DTParam = new DataTable();
                DTParam.Columns.AddRange(new DataColumn[1] { new DataColumn("empid") });
                for (int i = 0; i < param.empid.Length; i++)
                {
                    DTParam.Rows.Add(param.empid[i]);
                }





                TempConnectionStringv4(param.db);
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@TenantID", mytype = SqlDbType.NVarChar, Value = param.TenantID });
                con.myparameters.Add(new myParameters { ParameterName = "@CutOffStart", mytype = SqlDbType.NVarChar, Value = param.CutoffStart });
                con.myparameters.Add(new myParameters { ParameterName = "@CutOffEnd", mytype = SqlDbType.NVarChar, Value = param.CutoffEnd });
                con.myparameters.Add(new myParameters { ParameterName = "@date1", mytype = SqlDbType.NVarChar, Value = param.date });
                con.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.Structured, Value = DTParam });
                con.myparameters.Add(new myParameters { ParameterName = "@Status", mytype = SqlDbType.NVarChar, Value = param.EmpStatus });
                //   con.myparameters.Add(new myParameters { ParameterName = "@payoutdate", mytype = SqlDbType.NVarChar, Value = param.PayoutDate });
                //   con.myparameters.Add(new myParameters { ParameterName = "@EmpStatus", mytype = SqlDbType.VarChar, Value = param.EmpStatus });
                DataTable DT = con.GetDataTable("sp_ExportWHousev4");
                string newDIR = HttpContext.Current.Server.MapPath("~/sFTP/PayrollOutsourcing/");
                FileStream fs1 = new FileStream(newDIR + "WHOUSE.txt", FileMode.Create, FileAccess.Write);
                StreamWriter writer = new StreamWriter(fs1);
                for (int x = 0; x < DT.Rows.Count; x++)
                {
                    if (x == DT.Rows.Count - 1)
                    {
                        writer.Write(DT.Rows[x]["Data"].ToString());
                    }
                    else
                    {
                        writer.Write(DT.Rows[x]["Data"].ToString() + Environment.NewLine);
                    }
                }
                writer.Close();
                string onlineDIR = "https://ws.durusthr.com/ILM_WS_Live/sFTP/PayrollOutsourcing/";
                return onlineDIR + "WHOUSE.txt";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }
        [HttpPost]
        [Route("ExtractWhousev5")]
        public string ExtractWhousev5(efile param)
        {
            try
            {
                DataTable DTParam = new DataTable();
                DTParam.Columns.AddRange(new DataColumn[1] { new DataColumn("empid") });
                for (int i = 0; i < param.empid.Length; i++)
                {
                    DTParam.Rows.Add(param.empid[i]);
                }

                TempConnectionStringv4(param.db);
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@TenantID", mytype = SqlDbType.NVarChar, Value = param.TenantID });
                con.myparameters.Add(new myParameters { ParameterName = "@CutOffStart", mytype = SqlDbType.NVarChar, Value = param.CutoffStart });
                con.myparameters.Add(new myParameters { ParameterName = "@CutOffEnd", mytype = SqlDbType.NVarChar, Value = param.CutoffEnd });
                con.myparameters.Add(new myParameters { ParameterName = "@date1", mytype = SqlDbType.NVarChar, Value = param.date });
                con.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.Structured, Value = DTParam });
                con.myparameters.Add(new myParameters { ParameterName = "@Status", mytype = SqlDbType.NVarChar, Value = param.EmpStatus });

                //   con.myparameters.Add(new myParameters { ParameterName = "@payoutdate", mytype = SqlDbType.NVarChar, Value = param.PayoutDate });
                //   con.myparameters.Add(new myParameters { ParameterName = "@EmpStatus", mytype = SqlDbType.VarChar, Value = param.EmpStatus });
                DataTable DT = con.GetDataTable("sp_GenerateWhouse");
                DateTime oDate = Convert.ToDateTime(param.date);
                string dateNow = oDate.ToString("yyyyMMdd");
                string tenantCode = param.cCode;
                string filename = tenantCode + "_" + dateNow + ".txt";
                string newDIR = HttpContext.Current.Server.MapPath("~/sFTP/PayrollOutsourcing/");
                FileStream fs1 = new FileStream(newDIR + filename, FileMode.Create, FileAccess.Write);
                StreamWriter writer = new StreamWriter(fs1);
                for (int x = 0; x < DT.Rows.Count; x++)
                {
                    if (x == DT.Rows.Count - 1)
                    {
                        writer.Write(DT.Rows[x]["Data"].ToString());
                    }
                    else
                    {
                        writer.Write(DT.Rows[x]["Data"].ToString() + Environment.NewLine);
                    }
                }
                writer.Close();
                string onlineDIR = "https://ws.durusthr.com/ILM_WS_Live/sFTP/PayrollOutsourcing/";
                return onlineDIR + filename;
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }
        [HttpPost]
        [Route("ExtractUB_BankFile")]
        public string ExtractUB_BankFile(efile param)
        {
            try
            {
                DataTable DTParam = new DataTable();
                DTParam.Columns.AddRange(new DataColumn[1] { new DataColumn("empid") });
                for (int i = 0; i < param.empid.Length; i++)
                {
                    DTParam.Rows.Add(param.empid[i]);
                }
                TempConnectionStringv4(param.db);
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@TenantID", mytype = SqlDbType.NVarChar, Value = param.TenantID });
                con.myparameters.Add(new myParameters { ParameterName = "@CutOffStart", mytype = SqlDbType.NVarChar, Value = param.CutoffStart });
                con.myparameters.Add(new myParameters { ParameterName = "@CutOffEnd", mytype = SqlDbType.NVarChar, Value = param.CutoffEnd });
                con.myparameters.Add(new myParameters { ParameterName = "@date1", mytype = SqlDbType.NVarChar, Value = param.date });
                con.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.Structured, Value = DTParam });
                con.myparameters.Add(new myParameters { ParameterName = "@Status", mytype = SqlDbType.NVarChar, Value = param.EmpStatus });
                //   con.myparameters.Add(new myParameters { ParameterName = "@payoutdate", mytype = SqlDbType.NVarChar, Value = param.PayoutDate });
                //   con.myparameters.Add(new myParameters { ParameterName = "@EmpStatus", mytype = SqlDbType.VarChar, Value = param.EmpStatus });
                DataTable DT = con.GetDataTable("sp_UB_BankCreditFile");
                string newDIR = HttpContext.Current.Server.MapPath("~/sFTP/PayrollOutsourcing/");
                FileStream fs1 = new FileStream(newDIR + "UBBankFile.txt", FileMode.Create, FileAccess.Write);
                StreamWriter writer = new StreamWriter(fs1);
                for (int x = 0; x < DT.Rows.Count; x++)
                {
                    if (x == DT.Rows.Count - 1)
                    {
                        writer.Write(DT.Rows[x]["Data"].ToString());
                    }
                    else
                    {
                        writer.Write(DT.Rows[x]["Data"].ToString() + Environment.NewLine);
                    }
                }
                writer.Close();
                string onlineDIR = "https://ws.durusthr.com/ILM_WS_Live/sFTP/PayrollOutsourcing/";
                return onlineDIR + "UBBankFile.txt";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }
        [HttpPost]
        [Route("PO_TaxPerMonth_Report")]
        public string Extract_TaxPerMonth_Report(TaxPerMonthReport param)
        {
            FileStream stream = null;
            try
            {
                TempConnectionStringv4(param.db);
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@Year", mytype = SqlDbType.NVarChar, Value = param.Year });
                con.myparameters.Add(new myParameters { ParameterName = "@TenantCode", mytype = SqlDbType.NVarChar, Value = param.TenantCode });
                DataTable WHTAXData = con.GetDataTable("sp_Extract_TaxPerMonth");
                string dataDir = HostingEnvironment.MapPath("~/Files/PayrollOutSourcing/");
                stream = new FileStream(dataDir + "TaxPerMonthReport.xls", FileMode.Open);
                string filename;
                string datebuild = DateTime.Now.ToString("MMddyyyy");
                string timebuild = DateTime.Now.ToString("hhmmtt");

                //filename = datebuild + timebuild + "_PEZAReport" + ".xls";
                filename = param.TenantCode + "_TaxPerMonth" + "_" + param.Year + "_" + timebuild + ".xls";
                using (var fileStream = new FileStream(dataDir + filename, FileMode.Create, FileAccess.Write))
                //using (var fileStream = new FileStream(dataDir + filename, FileMode.Create, FileAccess.Write))
                {
                    stream.CopyTo(fileStream);
                }
                stream.Close();
                ExcelWorkbookFormat workbookFormat = ExcelWorkbookFormat.Xls_2003;
                string testDocFile = dataDir + "TaxPerMonthReport.xls";
                ExcelWorkbook workbook = new ExcelWorkbook(testDocFile);
                ExcelWorksheet firstWorksheet = workbook.Worksheets[0];
                ExcelWorksheet WHTAX = workbook.Worksheets[0];
                int x = 2;
                for (int i = 0; i < WHTAXData.Rows.Count; i++, x++)
                {
                    WHTAX["A" + x].Text = WHTAXData.Rows[i]["Company Name"].ToString();
                    WHTAX["B" + x].Text = WHTAXData.Rows[i]["Month"].ToString();
                    WHTAX["C" + x].Text = WHTAXData.Rows[i]["TAXTABLE"].ToString();
                    WHTAX["D" + x].Text = WHTAXData.Rows[i]["BIR OR#"].ToString();
                    WHTAX["E" + x].Text = WHTAXData.Rows[i]["BIR ORDATE"].ToString();
                    WHTAX["F" + x].Text = WHTAXData.Rows[i]["PAY CODE"].ToString();
                    WHTAX["G" + x].Text = WHTAXData.Rows[i]["TAX WITHHELD"].ToString();
                    WHTAX["H" + x].Text = WHTAXData.Rows[i]["YEAR"].ToString();
                }
                HttpResponse httpResponse = System.Web.HttpContext.Current.Response;
                httpResponse.Clear();
                httpResponse.ContentType = "Application/x-msexcel";
                httpResponse.AddHeader("Content-Disposition", String.Format("attachment; filename={0}", filename));
                string newDIR = HttpContext.Current.Server.MapPath("~/sFTP/PayrollOutsourcing/");
                string onlineDIR = "http://ws.durusthr.com/ILM_WS_Live/sFTP/PayrollOutsourcing/";
                //filename = filename.Replace(',', '_');
                //filename = filename.Replace(" ", String.Empty);
                workbook.Save(newDIR + filename);
                stream.Close();
                //return "Success";
                if (WHTAXData.Rows.Count > 0)
                {
                    return onlineDIR + filename;
                }
                else
                {
                    return "No Rows";
                }
            }
            catch (Exception e)
            {
                stream.Close();
                return "Error: " + e.ToString();
            }
        }
        [HttpPost]
        [Route("Extract_SSSECS")]
        public string Extract_SSSECS(efile param)
        {
            try
            {
                string filename = param.cCode + "_SSSECS_PREMIUM_BROADS.txt";
                TempConnectionStringv2();
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@TenantID", mytype = SqlDbType.NVarChar, Value = param.TenantID });


                //   con.myparameters.Add(new myParameters { ParameterName = "@payoutdate", mytype = SqlDbType.NVarChar, Value = param.PayoutDate });
                //   con.myparameters.Add(new myParameters { ParameterName = "@EmpStatus", mytype = SqlDbType.VarChar, Value = param.EmpStatus });
                DataTable DT = con.GetDataTable("sp_SSSECS");
                string newDIR = HttpContext.Current.Server.MapPath("~/sFTP/PayrollOutsourcing/");
                FileStream fs1 = new FileStream(newDIR + filename, FileMode.Create, FileAccess.Write);
                StreamWriter writer = new StreamWriter(fs1);
                for (int x = 0; x < DT.Rows.Count; x++)
                {
                    if (x == DT.Rows.Count - 1)
                    {
                        writer.Write(DT.Rows[x]["Data"].ToString());
                    }
                    else
                    {
                        writer.Write(DT.Rows[x]["Data"].ToString() + Environment.NewLine);
                    }
                }
                writer.Close();
                string onlineDIR = "http://ws.durusthr.com/ILM_WS_Live/sFTP/PayrollOutsourcing/";
                return onlineDIR + filename;
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }
        [HttpPost]
        [Route("Extract_SSSECSv2")]
        public string Extract_SSSECSv2(efile param)
        {
            try
            {
                string filename = param.cCode + "_SSSECS_PREMIUM_" + param.year + param.month + ".txt";
                TempConnectionStringv4(param.db);
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@TenantID", mytype = SqlDbType.NVarChar, Value = param.TenantID });
                con.myparameters.Add(new myParameters { ParameterName = "@year", mytype = SqlDbType.NVarChar, Value = param.year });
                con.myparameters.Add(new myParameters { ParameterName = "@month", mytype = SqlDbType.NVarChar, Value = param.month });

                //   con.myparameters.Add(new myParameters { ParameterName = "@payoutdate", mytype = SqlDbType.NVarChar, Value = param.PayoutDate });
                //   con.myparameters.Add(new myParameters { ParameterName = "@EmpStatus", mytype = SqlDbType.VarChar, Value = param.EmpStatus });
                DataTable DT = con.GetDataTable("sp_SSSECSv2");
                string newDIR = HttpContext.Current.Server.MapPath("~/sFTP/PayrollOutsourcing/");
                FileStream fs1 = new FileStream(newDIR + filename, FileMode.Create, FileAccess.Write);
                StreamWriter writer = new StreamWriter(fs1);
                for (int x = 0; x < DT.Rows.Count; x++)
                {
                    if (x == DT.Rows.Count - 1)
                    {
                        writer.Write(DT.Rows[x]["Data"].ToString());
                    }
                    else
                    {
                        writer.Write(DT.Rows[x]["Data"].ToString() + Environment.NewLine);
                    }
                }
                writer.Close();
                string onlineDIR = "https://ws.durusthr.com/ILM_WS_Live/sFTP/PayrollOutsourcing/";
                return onlineDIR + filename;
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }

        [HttpPost]
        [Route("ExtractYTD")]
        public string ExtractYTD(efile param)
        {

            FileStream stream = null;
            try
            {
                DataTable DTParam = new DataTable();
                DTParam.Columns.AddRange(new DataColumn[1] { new DataColumn("empid") });
                for (int i = 0; i < param.empid.Length; i++)
                {
                    DTParam.Rows.Add(param.empid[i]);
                }

                TempConnectionStringv4(param.db);
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@TenantID", mytype = SqlDbType.NVarChar, Value = param.TenantID });
                con.myparameters.Add(new myParameters { ParameterName = "@Year", mytype = SqlDbType.NVarChar, Value = param.year });
                con.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.Structured, Value = DTParam });
                DataTable ytdPayreg = con.GetDataTable("sp_YTD_Payreg");


                string dataDir = HostingEnvironment.MapPath("~/Files/PayrollOutSourcing/");

                stream = new FileStream(dataDir + "ExtractYTD.xls", FileMode.Open);
                string filename;
                // string datebuild = DateTime.Now.ToString("MMddyyyy");
                string cc = "";
                if (ytdPayreg.Rows.Count != 0)
                {
                    cc = ytdPayreg.Rows[0]["Ccode"].ToString();
                }




                string datebuild = DateTime.Now.ToString("yyyyMM");
                string timebuild = DateTime.Now.ToString("hhtt");
                filename = cc + "YTD" + param.year + ".xls";
                using (var fileStream = new FileStream(dataDir + filename, FileMode.Create, FileAccess.Write))
                //using (var fileStream = new FileStream(dataDir + filename, FileMode.Create, FileAccess.Write))
                {
                    stream.CopyTo(fileStream);
                }
                stream.Close();
                ExcelWorkbookFormat workbookFormat = ExcelWorkbookFormat.Xls_2003;
                string testDocFile = dataDir + "ExtractYTD.xls";
                ExcelWorkbook workbook = new ExcelWorkbook(testDocFile);
                ExcelWorksheet firstWorksheet = workbook.Worksheets[0];

                #region PayReg Sheet

                ExcelWorksheet PayReg = workbook.Worksheets[0];
                ExcelWorksheet sheet4 = workbook.Worksheets[1];
                ExcelWorksheet sheet5 = workbook.Worksheets[2];
                ExcelWorksheet sheet2 = workbook.Worksheets[3];
                ExcelWorksheet sheet6 = workbook.Worksheets[4];
                ExcelWorksheet OTsheet = workbook.Worksheets[5];
                //Sheet7.WorksheetSecurity.ProtectWorksheet("sbc01");
                //AX TAXTABLE

                int x = 2;
                for (int i = 0; i < ytdPayreg.Rows.Count; i++, x++)
                {

                    PayReg["A" + x].Text = ytdPayreg.Rows[i]["PayrollNum"].ToString(); //payroll#
                    PayReg["B" + x].Text = ytdPayreg.Rows[i]["PayDate"].ToString(); //paydate
                    PayReg["C" + x].Text = ytdPayreg.Rows[i]["PayCode"].ToString(); //pay code
                    PayReg["D" + x].Text = ytdPayreg.Rows[i]["EmpID"].ToString(); //employee id
                    PayReg["E" + x].Text = ytdPayreg.Rows[i]["LastName"].ToString(); //last name
                    PayReg["F" + x].Text = ytdPayreg.Rows[i]["FirstName"].ToString(); //first name
                    PayReg["G" + x].Text = ytdPayreg.Rows[i]["FileStatus"].ToString(); //file status
                    PayReg["H" + x].Text = ytdPayreg.Rows[i]["TaxStatus"].ToString(); //tax status
                    PayReg["I" + x].Value = ytdPayreg.Rows[i]["DailyRate"]; //daily rate
                    PayReg["J" + x].Value = ytdPayreg.Rows[i]["SalaryRateType"]; //salary rate type
                    PayReg["K" + x].Value = ytdPayreg.Rows[i]["BasicSalary"]; //basic salary
                    PayReg["L" + x].Value = ytdPayreg.Rows[i]["MiscAmount"]; //misc amount
                    PayReg["M" + x].Value = ytdPayreg.Rows[i]["LeaveAmount"];
                    PayReg["N" + x].Value = ytdPayreg.Rows[i]["OvertimeReg"]; //overtime
                    PayReg["O" + x].Value = ytdPayreg.Rows[i]["OvertimeHol"];
                    PayReg["P" + x].Value = ytdPayreg.Rows[i]["OtherTxIncome"]; //other tx income
                    PayReg["Q" + x].Text = ytdPayreg.Rows[i]["Adjustments"].ToString(); //adjustments
                    PayReg["R" + x].Value = ytdPayreg.Rows[i]["GrossIncome"]; //gross income
                    PayReg["S" + x].Value = ytdPayreg.Rows[i]["WithholdingTax"]; //withholding tax
                    PayReg["T" + x].Value = ytdPayreg.Rows[i]["NetSalaryAfterTax"]; //net salary after tax
                    PayReg["U" + x].Value = ytdPayreg.Rows[i]["EmployeeSSS"]; //employee sss
                    PayReg["V" + x].Value = ytdPayreg.Rows[i]["EmployeePhilHealth"]; //employee mcr(philhealth)
                    PayReg["W" + x].Value = ytdPayreg.Rows[i]["EmployeePagibig"]; //employee pagibig
                    PayReg["X" + x].Value = ytdPayreg.Rows[i]["OtherNtxIncome"]; //other ntx income
                    PayReg["Y" + x].Value = ytdPayreg.Rows[i]["LoanPayments"]; //loan payments
                    PayReg["Z" + x].Value = ytdPayreg.Rows[i]["Deductions"]; //deductions
                    PayReg["AA" + x].Value = ytdPayreg.Rows[i]["NetSalary"]; //net salary
                    PayReg["AB" + x].Value = ytdPayreg.Rows[i]["EmployerSSS"]; //employer sss
                    PayReg["AC" + x].Value = ytdPayreg.Rows[i]["EmployerPhilhealth"]; //employer mcr(philhealth)
                    PayReg["AD" + x].Value = ytdPayreg.Rows[i]["EmployerEC"]; //employer ec
                    PayReg["AE" + x].Value = ytdPayreg.Rows[i]["EmployerPagibig"]; //employer pagibig
                    PayReg["AF" + x].Value = ytdPayreg.Rows[i]["PayrollCost"]; //payroll cost
                    PayReg["AG" + x].Value = ytdPayreg.Rows[i]["PreviousYTDGross"]; //payment type
                    PayReg["AH" + x].Value = ytdPayreg.Rows[i]["PreviousYTDWitholding"]; //bank acct#
                    PayReg["AI" + x].Value = ytdPayreg.Rows[i]["PreviousYTDSSS"]; //bank name
                    PayReg["AJ" + x].Value = ytdPayreg.Rows[i]["PreviousYTDMCR"]; //date employed
                    PayReg["AK" + x].Value = ytdPayreg.Rows[i]["PreviousYTDPagibig"]; //date terminated
                    PayReg["AL" + x].Value = ytdPayreg.Rows[i]["Previous13thNT"]; //cost center
                    PayReg["AM" + x].Value = ytdPayreg.Rows[i]["Previous13thTX"];
                    PayReg["AN" + x].Text = ytdPayreg.Rows[i]["PaymentType"].ToString(); //currency
                    PayReg["AO" + x].Text = ytdPayreg.Rows[i]["BankAcc"].ToString(); //exchange rate
                    PayReg["AP" + x].Text = ytdPayreg.Rows[i]["BankName"].ToString(); //payment freq
                    PayReg["AQ" + x].Text = ytdPayreg.Rows[i]["CommentField"].ToString();
                    PayReg["AR" + x].Text = ytdPayreg.Rows[i]["ErrorField"].ToString();
                    PayReg["AS" + x].Text = ytdPayreg.Rows[i]["DateEmployed"].ToString();
                    PayReg["AT" + x].Text = ytdPayreg.Rows[i]["DateTerminated"].ToString();
                    PayReg["AU" + x].Value = ytdPayreg.Rows[i]["CostCenter"];
                    PayReg["AV" + x].Text = ytdPayreg.Rows[i]["Currency"].ToString();
                    PayReg["AW" + x].Text = ytdPayreg.Rows[i]["ExchangeRate"].ToString();
                    PayReg["AX" + x].Text = ytdPayreg.Rows[i]["PaymentPreq"].ToString();
                    PayReg["AY" + x].Value = ytdPayreg.Rows[i]["MTDGross"];
                    PayReg["AZ" + x].Value = ytdPayreg.Rows[i]["MTDBasic"];
                    PayReg["BA" + x].Value = ytdPayreg.Rows[i]["MTDSSSEmployee"];
                    PayReg["BB" + x].Value = ytdPayreg.Rows[i]["MTDMCREmployee"];
                    PayReg["BC" + x].Value = ytdPayreg.Rows[i]["MTDPagIbigEmployee"];
                    PayReg["BD" + x].Value = ytdPayreg.Rows[i]["MTDSSSEmployer"];
                    PayReg["BE" + x].Value = ytdPayreg.Rows[i]["MTDMCREmployer"];
                    PayReg["BF" + x].Value = ytdPayreg.Rows[i]["MTDECEmployer"];
                    PayReg["BG" + x].Value = ytdPayreg.Rows[i]["MTDPabIbigEmployer"];
                    PayReg["BH" + x].Value = ytdPayreg.Rows[i]["MTDWHTAX"];
                    PayReg["BI" + x].Value = ytdPayreg.Rows[i]["MonthlyBasic"];
                    PayReg["BJ" + x].Value = ytdPayreg.Rows[i]["MonthlyAllow"];
                    PayReg["BK" + x].Value = ytdPayreg.Rows[i]["MTDNTX"];
                    PayReg["BL" + x].Text = ytdPayreg.Rows[i]["FROM"].ToString();
                    PayReg["BM" + x].Text = ytdPayreg.Rows[i]["TO"].ToString();




                }
                #endregion


                TempConnectionStringv4(param.db);
                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@TenantID", mytype = SqlDbType.NVarChar, Value = param.TenantID });
                con.myparameters.Add(new myParameters { ParameterName = "@Year", mytype = SqlDbType.NVarChar, Value = param.year });
                con.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.Structured, Value = DTParam });
                DataTable FSheet = con.GetDataTable("sp_YTD_Taxable");

                x = 2;
                for (int i = 0; i < FSheet.Rows.Count; i++, x++)
                {
                    sheet4["A" + x].Text = FSheet.Rows[i]["PayrollNum"].ToString();
                    sheet4["B" + x].Text = FSheet.Rows[i]["PayDate"].ToString();
                    sheet4["C" + x].Text = FSheet.Rows[i]["PayCode"].ToString();
                    sheet4["D" + x].Text = FSheet.Rows[i]["EmployeeID"].ToString();
                    sheet4["E" + x].Text = FSheet.Rows[i]["LastName"].ToString();
                    sheet4["F" + x].Text = FSheet.Rows[i]["FirstName"].ToString();
                    sheet4["G" + x].Text = FSheet.Rows[i]["FileStatus"].ToString();
                    sheet4["H" + x].Text = FSheet.Rows[i]["IncomeCode"].ToString();
                    sheet4["I" + x].Text = FSheet.Rows[i]["Date"].ToString();
                    sheet4["J" + x].Value = FSheet.Rows[i]["Amount"];
                    sheet4["K" + x].Text = FSheet.Rows[i]["RecurStart"].ToString();
                    sheet4["L" + x].Text = FSheet.Rows[i]["RecurEnd"].ToString();
                    sheet4["M" + x].Text = FSheet.Rows[i]["Freq"].ToString();
                    sheet4["N" + x].Text = FSheet.Rows[i]["Remarks"].ToString();
                    sheet4["O" + x].Text = FSheet.Rows[i]["From"].ToString();
                    sheet4["P" + x].Text = FSheet.Rows[i]["To"].ToString();
                    sheet4["Q" + x].Text = FSheet.Rows[i]["JECostCenter"].ToString();

                }

                TempConnectionStringv4(param.db);
                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@TenantID", mytype = SqlDbType.NVarChar, Value = param.TenantID });
                con.myparameters.Add(new myParameters { ParameterName = "@Year", mytype = SqlDbType.NVarChar, Value = param.year });
                con.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.Structured, Value = DTParam });
                DataTable FifthSheet = con.GetDataTable("sp_YTD_NonTaxable");

                x = 2;
                for (int i = 0; i < FifthSheet.Rows.Count; i++, x++)
                {
                    sheet5["A" + x].Text = FifthSheet.Rows[i]["PayrollNum"].ToString();
                    sheet5["B" + x].Text = FifthSheet.Rows[i]["PayDate"].ToString();
                    sheet5["C" + x].Text = FifthSheet.Rows[i]["PayCode"].ToString();
                    sheet5["D" + x].Text = FifthSheet.Rows[i]["EmployeeID"].ToString();
                    sheet5["E" + x].Text = FifthSheet.Rows[i]["LastName"].ToString();
                    sheet5["F" + x].Text = FifthSheet.Rows[i]["FirstName"].ToString();
                    sheet5["G" + x].Text = FifthSheet.Rows[i]["FileStatus"].ToString();
                    sheet5["H" + x].Text = FifthSheet.Rows[i]["IncomeCode"].ToString();
                    sheet5["I" + x].Text = FifthSheet.Rows[i]["Date"].ToString();
                    sheet5["J" + x].Value = FifthSheet.Rows[i]["Amount"];
                    sheet5["K" + x].Text = FifthSheet.Rows[i]["RecurStart"].ToString();
                    sheet5["L" + x].Text = FifthSheet.Rows[i]["RecurEnd"].ToString();
                    sheet5["M" + x].Text = FifthSheet.Rows[i]["Freq"].ToString();
                    sheet5["N" + x].Text = FifthSheet.Rows[i]["Remarks"].ToString();
                    sheet5["O" + x].Text = FifthSheet.Rows[i]["From"].ToString();
                    sheet5["P" + x].Text = FifthSheet.Rows[i]["To"].ToString();
                    sheet5["Q" + x].Text = FifthSheet.Rows[i]["JECostCenter"].ToString();



                }
                TempConnectionStringv4(param.db);
                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@TenantID", mytype = SqlDbType.NVarChar, Value = param.TenantID });
                con.myparameters.Add(new myParameters { ParameterName = "@Year", mytype = SqlDbType.NVarChar, Value = param.year });
                con.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.Structured, Value = DTParam });
                DataTable secondSheet = con.GetDataTable("sp_YTD_MISC");

                x = 2;
                for (int i = 0; i < secondSheet.Rows.Count; i++, x++)
                {
                    sheet2["A" + x].Text = secondSheet.Rows[i]["PayrollNum"].ToString();
                    sheet2["B" + x].Text = secondSheet.Rows[i]["PayOutDate"].ToString();
                    sheet2["C" + x].Text = secondSheet.Rows[i]["PayCode"].ToString();
                    sheet2["D" + x].Text = secondSheet.Rows[i]["EmployeeID"].ToString();
                    sheet2["E" + x].Text = secondSheet.Rows[i]["LastName"].ToString();
                    sheet2["F" + x].Text = secondSheet.Rows[i]["FirstName"].ToString();
                    sheet2["G" + x].Text = secondSheet.Rows[i]["FileStatus"].ToString();
                    sheet2["H" + x].Text = secondSheet.Rows[i]["TransactionType"].ToString();
                    sheet2["I" + x].Text = secondSheet.Rows[i]["Date"].ToString();
                    sheet2["J" + x].Value = secondSheet.Rows[i]["CostCenter"];
                    sheet2["K" + x].Value = secondSheet.Rows[i]["Days"];
                    sheet2["L" + x].Value = secondSheet.Rows[i]["Hours"];
                    sheet2["M" + x].Value = secondSheet.Rows[i]["Amount"];
                    sheet2["N" + x].Text = secondSheet.Rows[i]["Remark"].ToString();
                    sheet2["O" + x].Text = secondSheet.Rows[i]["FROM"].ToString();
                    sheet2["P" + x].Text = secondSheet.Rows[i]["TO"].ToString();

                }

                TempConnectionStringv4(param.db);
                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@TenantID", mytype = SqlDbType.NVarChar, Value = param.TenantID });
                con.myparameters.Add(new myParameters { ParameterName = "@Year", mytype = SqlDbType.NVarChar, Value = param.year });
                con.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.Structured, Value = DTParam });
                DataTable SixSheet = con.GetDataTable("sp_YTD_loan");

                x = 2;
                for (int i = 0; i < SixSheet.Rows.Count; i++, x++)
                {
                    sheet6["A" + x].Text = SixSheet.Rows[i]["PayrollNum"].ToString();
                    sheet6["B" + x].Text = SixSheet.Rows[i]["PayDate"].ToString();
                    sheet6["C" + x].Text = SixSheet.Rows[i]["PayCode"].ToString();
                    sheet6["D" + x].Text = SixSheet.Rows[i]["EmployeeID"].ToString();
                    sheet6["E" + x].Text = SixSheet.Rows[i]["LastName"].ToString();
                    sheet6["F" + x].Text = SixSheet.Rows[i]["FirstName"].ToString();
                    sheet6["G" + x].Text = SixSheet.Rows[i]["FileStatus"].ToString();
                    sheet6["H" + x].Text = SixSheet.Rows[i]["LoanCode"].ToString();
                    sheet6["I" + x].Text = SixSheet.Rows[i]["LoanDateStart"].ToString();
                    sheet6["J" + x].Value = SixSheet.Rows[i]["LoanPrincipal"];
                    sheet6["K" + x].Value = SixSheet.Rows[i]["LoanAmount"];
                    sheet6["L" + x].Value = SixSheet.Rows[i]["LoanPayments"];
                    sheet6["M" + x].Value = SixSheet.Rows[i]["LoanBalance"];
                    sheet6["N" + x].Value = SixSheet.Rows[i]["LoanDeductionAMT"];
                    sheet6["O" + x].Text = SixSheet.Rows[i]["LoanDeductionFreq"].ToString();
                    sheet6["P" + x].Text = SixSheet.Rows[i]["LoanStatus"].ToString();
                    sheet6["Q" + x].Text = SixSheet.Rows[i]["LoanDateGranted"].ToString();
                    sheet6["R" + x].Text = SixSheet.Rows[i]["From"].ToString();
                    sheet6["S" + x].Text = SixSheet.Rows[i]["To"].ToString();
                    sheet6["T" + x].Text = SixSheet.Rows[i]["JECostCenter"].ToString();


                }

                TempConnectionStringv4(param.db);
                con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@TenantID", mytype = SqlDbType.NVarChar, Value = param.TenantID });
                con.myparameters.Add(new myParameters { ParameterName = "@Year", mytype = SqlDbType.NVarChar, Value = param.year });
                con.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.Structured, Value = DTParam });
                DataTable OTDT = con.GetDataTable("sp_YTD_Overtime");

                x = 2;
                for (int i = 0; i < OTDT.Rows.Count; i++, x++)
                {
                    OTsheet["A" + x].Text = OTDT.Rows[i]["PayrollNum"].ToString();
                    OTsheet["B" + x].Text = OTDT.Rows[i]["PayDate"].ToString();
                    OTsheet["C" + x].Text = OTDT.Rows[i]["PayCode"].ToString();
                    OTsheet["D" + x].Text = OTDT.Rows[i]["EmpID"].ToString();
                    OTsheet["E" + x].Text = OTDT.Rows[i]["LastName"].ToString();
                    OTsheet["F" + x].Text = OTDT.Rows[i]["FirstName"].ToString();
                    OTsheet["G" + x].Text = OTDT.Rows[i]["FileStatus"].ToString();
                    OTsheet["H" + x].Text = OTDT.Rows[i]["OTCode"].ToString();
                    OTsheet["I" + x].Text = OTDT.Rows[i]["Date"].ToString();
                    OTsheet["J" + x].Value = OTDT.Rows[i]["CostCenter"];
                    OTsheet["K" + x].Value = OTDT.Rows[i]["OTRate"];
                    OTsheet["L" + x].Value = OTDT.Rows[i]["OTHours"];
                    OTsheet["M" + x].Value = OTDT.Rows[i]["OTMinutes"];
                    OTsheet["N" + x].Value = OTDT.Rows[i]["OTAmount"];
                    OTsheet["O" + x].Text = OTDT.Rows[i]["Remark"].ToString();
                    OTsheet["P" + x].Text = OTDT.Rows[i]["From"].ToString();
                    OTsheet["Q" + x].Text = OTDT.Rows[i]["To"].ToString();

                }

                HttpResponse httpResponse = System.Web.HttpContext.Current.Response;

                httpResponse.Clear();
                httpResponse.ContentType = "Application/x-msexcel";
                httpResponse.AddHeader("Content-Disposition", String.Format("attachment; filename={0}", filename));
                string newDIR = HttpContext.Current.Server.MapPath("~/sFTP/PayrollOutsourcing/");
                string onlineDIR = "http://ws.durusthr.com/ILM_WS_Live/sFTP/PayrollOutsourcing/";
                //filename = filename.Replace(',', '_');
                //filename = filename.Replace(" ", String.Empty);
                workbook.Save(newDIR + filename);
                stream.Close();
                //return "Success";

                if (ytdPayreg.Rows.Count > 0)
                {
                    return onlineDIR + filename;
                }
                else
                {

                    return "No Rows";
                }
            }
            catch (Exception e)
            {
                stream.Close();
                return "Error: " + e.ToString();
            }
        }
        #endregion

        #region HDMF Report
        [HttpPost]
        [Route("GetHDMFReport")]
        public string GetHDMFReport(HDMFReportModel model)
        {
            string response = "";

            // Connect to DB
            TempConnectionStringv4(model.db);
            Connection con = new Connection();

            try
            {
                // Get HDMF List
                con.myparameters.Add(new myParameters { ParameterName = "@tenantid", mytype = SqlDbType.NVarChar, Value = model.tenant_id });
                con.myparameters.Add(new myParameters { ParameterName = "@year", mytype = SqlDbType.NVarChar, Value = model.year });
                con.myparameters.Add(new myParameters { ParameterName = "@month", mytype = SqlDbType.NVarChar, Value = model.month });
                DataTable dt_hdmf_list = con.GetDataTable("sp_get_hdmf_list");

                if (dt_hdmf_list.Rows.Count < 1) return "There are no reports that is set";

                // Path
                string webRootPath = HostingEnvironment.MapPath("~/Files/PayrollOutSourcing/Reports");
                string customDir = "HDMF Reports/";
                string reportDir = Path.Combine(webRootPath, customDir);
                string zipFileName = "HDMFReports.zip";
                string liveLinkPath = "https://ws.durusthr.com/ILM_WS_Live/";
                string reportRawDir = "Files/PayrollOutSourcing/Reports/";
                string reportLiveDir = Path.Combine(liveLinkPath, reportRawDir);

                if (!Directory.Exists(reportDir))
                    Directory.CreateDirectory(reportDir);

                if (File.Exists(reportDir + zipFileName))
                    File.Delete(reportDir + zipFileName);

                // Zip File
                using (var archive = ZipFile.Open(reportDir + zipFileName, ZipArchiveMode.Create))
                {
                    foreach (DataRow _row in dt_hdmf_list.Rows)
                    {
                        string hdmf_code = _row["hdmf_code"].ToString();

                        #region Get HDMF Report
                        con.myparameters.Clear();
                        con.myparameters.Add(new myParameters { ParameterName = "@tenantid", mytype = SqlDbType.NVarChar, Value = model.tenant_id });
                        con.myparameters.Add(new myParameters { ParameterName = "@year", mytype = SqlDbType.NVarChar, Value = model.year });
                        con.myparameters.Add(new myParameters { ParameterName = "@month", mytype = SqlDbType.NVarChar, Value = model.month });
                        con.myparameters.Add(new myParameters { ParameterName = "@type", mytype = SqlDbType.NVarChar, Value = hdmf_code });
                        DataTable dt_result = con.GetDataTable("sp_generate_hdmf_handler");
                        #endregion

                        if (dt_result.Rows.Count > 0)
                        {
                            // Create Directory and HDMF Reports Text File
                            var fileName = _row["hdmf_filename"].ToString().Trim() + ".txt";

                            var fullPath = Path.Combine(webRootPath, customDir) + $@"{fileName}";

                            if (File.Exists(fullPath))
                                File.Delete(fullPath);

                            StreamWriter sw = new StreamWriter(fullPath, false);

                            foreach (DataRow result in dt_result.Rows)
                            {
                                sw.WriteLine(result[0]);
                            }

                            sw.Close();

                            archive.CreateEntryFromFile(reportDir + fileName, Path.GetFileName(reportDir + fileName));
                        }
                    }
                }

                // End and return
                response = reportLiveDir + customDir + zipFileName;
            }
            catch (Exception e)
            {
                Console.WriteLine("Error: " + e.Message);
                response = "Internal Server Error";
            }
            
            return response;
        }
        #endregion
    }
}
