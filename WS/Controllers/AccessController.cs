﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using illimitadoWepAPI.Models;
using System.Xml.Serialization;
using System.IO;
using System.Data;
using System.Data.SqlClient;
using illimitadoWepAPI.MyClass;
using OfficeOpenXml;
using System.Web.Hosting;
using ExpertXls.ExcelLib;
using System.Configuration;
using System.Reflection;
using System.Web;

namespace illimitadoWepAPI.Controllers
{
    public class AccessController : ApiController
    {
        public void tempconnectionstring()
        {
            var settings = ConfigurationManager.ConnectionStrings["connectionstring"];
            var fi = typeof(ConfigurationElement).GetField(
                          "_bReadOnly",
                          BindingFlags.Instance | BindingFlags.NonPublic);
            fi.SetValue(settings, false);
            settings.ConnectionString = "Data Source=livedb-ilm.c3srcolvclfy.ap-southeast-1.rds.amazonaws.com,1412; Database = ILM_Main;User Id=ILM_LiveDBUser;Password=ILMV3ry53cur3dMGTpa55w0rd2020;";

        }
        [HttpPost]
        [Route("GetAllAccess")]
        public ListAccess GetAllAccess(AccessParam AccessParam)
        {
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = AccessParam.CN;
            SelfieRegistration2Controller.GetDB2(GDB);
            ListAccess ListAccessResult = new ListAccess();
            try
            {
                

                Connection Connection = new Connection();
                List<Access> AccessList = new List<Access>();
                DataTable AccessDT = new DataTable();
                Connection.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = AccessParam.EmpID });
                Connection.myparameters.Add(new myParameters { ParameterName = "@FUNCTION", mytype = SqlDbType.NVarChar, Value = AccessParam.Function });
                AccessDT = Connection.GetDataTable("sp_SearchFunctionList");
                //CultureInfo provider = CultureInfo.InvariantCulture;
                if (AccessDT.Rows.Count > 0)
                {
                    foreach (DataRow row in AccessDT.Rows)
                    {
                        Access Access = new Access
                        {
                            id = row[4].ToString(),
                            Parameter = row[0].ToString(),
                            EditName = row[3].ToString(),
                            Link = row[1].ToString(),
                            Icon = row[2].ToString()
                        };
                        AccessList.Add(Access);
                    }
                    ListAccessResult.Access = AccessList;
                    ListAccessResult.myreturn = "Success";
                }
                else
                {
                    ListAccessResult.myreturn = "No Data Available";
                }
            }
            catch (Exception ex)
            {
                ListAccessResult.myreturn = ex.ToString();
            }
           

            return ListAccessResult;
        }
        [HttpPost]
        [Route("MR_Export_Excel")]
        public string MR_Export_Excel(MR_Export param)
        {
            try
            {
                GetDatabase GDB = new GetDatabase();
                GDB.ClientName = param.CN;
                SelfieRegistration2Controller.GetDB2(GDB);

                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@qry", mytype = SqlDbType.VarChar, Value = param.qry });
                DataTable dt = con.GetDataTable("sp_MR_Export_Excel");
                //BindingSource bsource = new BindingSource();
                //bsource.DataSource = dt;
                //dataGridView1.DataSource = bsource;
                DataSet ds = new DataSet("New_DataSet");
                ds.Locale = System.Threading.Thread.CurrentThread.CurrentCulture;
                ds.Tables.Add(dt);

                foreach (DataTable dataTable in ds.Tables)
                    foreach (DataRow dataRow in dataTable.Rows)
                        foreach (DataColumn dataColumn in dataTable.Columns)
                            if (dataRow.IsNull(dataColumn))
                            {
                                if (dataColumn.DataType.IsValueType) dataRow[dataColumn] = Activator.CreateInstance(dataColumn.DataType);
                                else if (dataColumn.DataType == typeof(bool)) dataRow[dataColumn] = false;
                                else if (dataColumn.DataType == typeof(Guid)) dataRow[dataColumn] = Guid.Empty;
                                else if (dataColumn.DataType == typeof(string)) dataRow[dataColumn] = string.Empty;
                                else if (dataColumn.DataType == typeof(DateTime)) dataRow[dataColumn] = string.Empty;
                                else if (dataColumn.DataType == typeof(int) || dataColumn.DataType == typeof(byte) || dataColumn.DataType == typeof(short) || dataColumn.DataType == typeof(long) || dataColumn.DataType == typeof(float) || dataColumn.DataType == typeof(double)) dataRow[dataColumn] = 0;
                                else dataRow[dataColumn] = null;
                            }

                using (ExcelPackage pck = new ExcelPackage())
                {
                    foreach (DataTable dataTable in ds.Tables)
                    {
                        OfficeOpenXml.ExcelWorksheet workSheet = pck.Workbook.Worksheets.Add(dataTable.TableName);
                        using (var cellitem = workSheet.Cells["A:Z"])
                        {
                            cellitem.Style.Numberformat.Format = "@";
                        }
                        workSheet.Cells["A1"].LoadFromDataTable(dataTable, true);
                        workSheet.Cells["A:Z"].AutoFitColumns();
                    }
                    using (var memoryStream = new MemoryStream())
                    {

                        //HttpContext.Current.Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                        // HttpContext.Current.Response.AddHeader("content-disposition", "attachment; filename=tableExport" + DateTime.Now.ToString("MM-dd-yyyy-hhmmss") + ".xlsx");
                        pck.SaveAs(memoryStream);
                        var bytearr = memoryStream.ToArray();
                        string path = HostingEnvironment.MapPath("~/sFTP/tableExport.xlsx");
                        File.WriteAllBytes(path, bytearr);
                        //memoryStream.WriteTo(HttpContext.Current.Response.OutputStream);
                        //HttpContext.Current.Response.Flush();
                        //HttpContext.Current.Response.End();
                    }
                }

                return "http://ws.durusthr.com/ILM_WS_Live/sFTP/tableExport.xlsx";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }
        [HttpPost]
        [Route("CR_Attendance")]
        public string CR_AttendanceReport_Excel(CR_Report param)
        {
            try
            {

                string xb = ExcelColumnFromNumber(5);
                //killExcel();
                string dataDir = System.Web.Hosting.HostingEnvironment.MapPath("~/sFTP/");
                //FileStream stream = new FileStream(dataDir + "CRTVlookup.xls", FileMode.Open);
                string filename;
                string nfilename;
                filename = "BlankDocument.xls";
                FileStream stream = new FileStream(dataDir + filename, FileMode.Open);
                string datebuild = DateTime.Now.ToString("MMddyyyy");
                string timebuild = DateTime.Now.ToString("hh-mmtt");
                nfilename = "Attendance_Report" + datebuild + ".xls";

                using (var fileStream = new FileStream(dataDir + nfilename, FileMode.Create, FileAccess.Write))
                {
                    stream.CopyTo(fileStream);
                }
                stream.Close();



                ExcelWorkbookFormat workbookFormat = ExcelWorkbookFormat.Xls_2003;
                //string testDocFile = dataDir + "CrtSampleFileExport.xls";
                string testDocFile = dataDir + nfilename;
                ExpertXls.ExcelLib.ExcelWorkbook workbook = new ExpertXls.ExcelLib.ExcelWorkbook(testDocFile);
                //ExcelWorksheet firstWorksheet = workbook.Worksheets[0];
                ExpertXls.ExcelLib.ExcelWorksheet firstWorksheet = workbook.Worksheets[0];

                int row = 1;
                //      SeriesID, BusinessSegment, BusinessUnit, ImmediateSupervisor, EmpLevel, EmpCategory, EmpName, EmpID, CutOff_From, CutOff_To, 
                //      Date, Day, Schedule, SchedIN, Break1dsply, Lunchdsply, Break2dsply, Break3dsply, SchedOUT, SchedDate, LunchEnd, Break1Minutes, 
                //      Break2Minutes, Break3Minutes,ClientName,ClientID,BreakType,
                //Break1End,Break2End ,Break3End ,ShiftType,Break1dsplyEnd,Break2dsplyEnd,Break3dsplyEnd,ShiftNum ,ScheduleType,ShiftType

                GetDatabase GDB = new GetDatabase();
                GDB.ClientName = param.CN;
                SelfieRegistration2Controller.GetDB2(GDB);

                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@datefrom", mytype = SqlDbType.Date, Value = param.datefrom });
                con.myparameters.Add(new myParameters { ParameterName = "@dateto", mytype = SqlDbType.Date, Value = param.dateto });

                //DataTable timedt = con.GetDataTable("sp_AttendanceReportGetTimeIn");
                DataTable timedt = con.GetDataTable("sp_AttendanceReportNew");

                string curdate = "", prevdate = "", curdatetime = "", prevdatetime = "1900-01-01";

                firstWorksheet["A" + row.ToString()].Text = "Employee ID";
                firstWorksheet["B" + row.ToString()].Text = "Employee Name";
                firstWorksheet["C" + row.ToString()].Text = "Immediate Supervisor";
                firstWorksheet["D" + row.ToString()].Text = "Department Name";
                firstWorksheet["E" + row.ToString()].Text = "Company";
                firstWorksheet["F" + row.ToString()].Text = "Schedule Date";
                firstWorksheet["G" + row.ToString()].Text = "Day";
                firstWorksheet["H" + row.ToString()].Text = "Scheduled Start";
                firstWorksheet["I" + row.ToString()].Text = "Scheduled Stop";
                firstWorksheet["J" + row.ToString()].Text = "Start Time";
                firstWorksheet["K" + row.ToString()].Text = "End Time";
                firstWorksheet["L" + row.ToString()].Text = "Hours";
                firstWorksheet["M" + row.ToString()].Text = "Tardiness";
                firstWorksheet["N" + row.ToString()].Text = "Undertime";
                firstWorksheet["O" + row.ToString()].Text = "Status";
                firstWorksheet["P" + row.ToString()].Text = "Comment";
                row = 2;
                if (timedt.Rows.Count > 0)
                {
                    int colNumber = 17;
                    string colLetter = ExcelColumnFromNumber(colNumber);
                    foreach (DataRow dr in timedt.Rows)
                    {
                        curdate = dr["SchedDate"].ToString() != "" ? dr["SchedDate"].ToString() : "1900-01-01";
                        curdatetime = dr["FieldTime"].ToString() != "" ? dr["FieldTime"].ToString() : "1900-01-01";
                        // ----- new start
                        firstWorksheet["A" + row.ToString()].Text = dr["Employee ID"].ToString();//Employee ID";
                        firstWorksheet["B" + row.ToString()].Text = dr["Employee Name"].ToString();//Employee Name";
                        firstWorksheet["C" + row.ToString()].Text = dr["Immediate Supervisor"].ToString();//Immediate Supervisor";
                        firstWorksheet["D" + row.ToString()].Text = dr["Department Name"].ToString();//Company";
                        firstWorksheet["E" + row.ToString()].Text = dr["Company"].ToString();//Company";
                        firstWorksheet["F" + row.ToString()].Text = dr["Schedule Date"].ToString();//Schedule Date";
                        firstWorksheet["G" + row.ToString()].Text = dr["Day"].ToString();//Day";
                        firstWorksheet["H" + row.ToString()].Text = dr["Scheduled Start"].ToString();//Scheduled Start";
                        firstWorksheet["I" + row.ToString()].Text = dr["Scheduled Stop"].ToString();//Scheduled Stop";
                        firstWorksheet["J" + row.ToString()].Text = dr["Start Time"].ToString();//Start Time";
                        firstWorksheet["K" + row.ToString()].Text = dr["End Time"].ToString();//End Time";
                        firstWorksheet["L" + row.ToString()].Text = dr["Hours"].ToString();//Hours";
                        firstWorksheet["M" + row.ToString()].Text = dr["Tardiness"].ToString();//Tardiness";
                        firstWorksheet["N" + row.ToString()].Text = dr["Undertime"].ToString();//Undertime";
                        firstWorksheet["O" + row.ToString()].Text = dr["Status"].ToString();//Status";
                        firstWorksheet["P" + row.ToString()].Text = dr["Comment"].ToString();//"Comment";
                                                                                             // ----- new end


                        TimeSpan ts = Convert.ToDateTime(prevdatetime).Subtract(Convert.ToDateTime(curdatetime));
                        double a = System.Math.Abs(ts.TotalMinutes);
                        if (curdate != prevdate)
                        {
                            row++;
                            colNumber = 17;
                        }
                        else
                        {
                            if ((curdate == prevdate || (prevdate == "")) && (a > 1 || prevdatetime == "1900-01-01"))
                            {
                            }
                        }
                        colLetter = ExcelColumnFromNumber(colNumber);
                        if (((curdate == prevdate || prevdate == "" || colNumber == 1) && (a > 1 || prevdatetime == "1900-01-01")) || (colNumber % 2 != 0))
                        {
                            firstWorksheet[colLetter + (row - 1).ToString()].Text = dr["FieldTime"].ToString();
                            if (colNumber % 2 != 0)
                            {
                                firstWorksheet[colLetter + "1"].Text = "Time IN";
                            }
                            else
                            {
                                firstWorksheet[colLetter + "1"].Text = "Time OUT";
                            }
                            prevdate = curdate;
                            prevdatetime = curdatetime;
                            colNumber++;
                        }

                        //if (curdate != "")
                        //{
                        //    if ((curdate == prevdate || prevdate == "") && (a > 1 || prevdatetime == "1900-01-01"))
                        //    {

                        //    }
                        //}

                    }
                }
                string newDIR = HostingEnvironment.MapPath("~/sFTP/");
                workbook.Save(newDIR + nfilename);
                byte[] Content = File.ReadAllBytes(newDIR + nfilename); //missing ;
                string path = HostingEnvironment.MapPath("~/sFTP/Attendance_Report.xls");
                File.WriteAllBytes(path, Content);

                return "http://ws.durusthr.com/ILM_WS_Live/sFTP/Attendance_Report.xls";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }
        public static string ExcelColumnFromNumber(int column)
        {
            string columnString = "";
            decimal columnNumber = column;
            while (columnNumber > 0)
            {
                decimal currentLetterNumber = (columnNumber - 1) % 26;
                char currentLetter = (char)(currentLetterNumber + 65);
                columnString = currentLetter + columnString;
                columnNumber = (columnNumber - (currentLetterNumber + 1)) / 26;
            }
            return columnString;
        }
        [HttpPost]
        [Route("ToDo_ScheduleUploadSearch")]
        public SchedClass SchedClass(SchedParam param)
        {
            DataTable EmpList = new DataTable();
            EmpList.Columns.AddRange(new DataColumn[1] {
                new DataColumn("EmpID"),
            });
            DataTable ScheduleTypeList = new DataTable();
            ScheduleTypeList.Columns.AddRange(new DataColumn[1] {
                new DataColumn("ScheduleType"),
            });

            for (int i = 0; i < param.EmpID.Length; i++)
            {
                EmpList.Rows.Add(param.EmpID[i]);
            }

            for (int i = 0; i < param.ScheduleType.Length; i++)
            {
                ScheduleTypeList.Rows.Add(param.ScheduleType[i]);
            }

            SchedClass Ret = new SchedClass();
            List<SchedList> RetList = new List<SchedList>();
            GetDatabase GDB = new GetDatabase();
            GDB.ClientName = param.CN;
            SelfieRegistration2Controller.GetDB2(GDB);
            try
            {
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.Structured, Value = EmpList });
                con.myparameters.Add(new myParameters { ParameterName = "@SchedType", mytype = SqlDbType.Structured, Value = ScheduleTypeList });
                con.myparameters.Add(new myParameters { ParameterName = "@SCHEDDATEFROM", mytype = SqlDbType.NVarChar, Value = param.startdate });
                con.myparameters.Add(new myParameters { ParameterName = "@SCHEDDATETO", mytype = SqlDbType.NVarChar, Value = param.enddate });
                DataTable DT = con.GetDataTable("sp_SearchSchedEmpName");
                foreach (DataRow row in DT.Rows)
                {
                    SchedList items = new SchedList()
                    {
                        SeriesID = row["SeriesID"].ToString(),
                        BusinessSegment = row["BusinessSegment"].ToString(),
                        BusinessUnit = row["BusinessUnit"].ToString(),
                        ImmediateSupervisor = row["ImmediateSupervisor"].ToString(),
                        EmpLevel = row["EmpLevel"].ToString(),
                        EmpCategory = row["EmpCategory"].ToString(),
                        EmpName = row["EmpName"].ToString(),
                        EmpID = row["EmpID"].ToString(),
                        CutOff_From = row["CutOff_From"].ToString(),
                        CutOff_To = row["CutOff_To"].ToString(),
                        Date = row["Date"].ToString(),
                        Day = row["Day"].ToString(),
                        Schedule = row["Schedule"].ToString(),
                        SchedIN = row["SchedIN"].ToString(),
                        Break1dsply = row["Break1dsply"].ToString(),
                        Lunchdsply = row["Lunchdsply"].ToString(),
                        Break2dsply = row["Break2dsply"].ToString(),
                        Break3dsply = row["Break3dsply"].ToString(),
                        SchedOUT = row["SchedOUT"].ToString(),
                        SchedDate = row["SchedDate"].ToString(),
                        LunchEnd = row["LunchEnd"].ToString(),
                        Break1Minutes = row["Break1Minutes"].ToString(),
                        Break2Minutes = row["Break2Minutes"].ToString(),
                        Break3Minutes = row["Break3Minutes"].ToString(),
                        ClientName = row["ClientName"].ToString(),
                        ClientID = row["ClientID"].ToString(),
                        BreakType = row["BreakType"].ToString(),
                        Break1End = row["Break1End"].ToString(),
                        Break2End = row["Break2End"].ToString(),
                        Break3End = row["Break3End"].ToString(),
                        ShiftType = row["ShiftType"].ToString(),
                        Break1dsplyEnd = row["Break1dsplyEnd"].ToString(),
                        Break2dsplyEnd = row["Break2dsplyEnd"].ToString(),
                        Berak3dsplyEnd = row["Break3dsplyEnd"].ToString(),
                        ShiftNum = row["ShiftNum"].ToString(),
                        ScheduleType = row["ScheduleType"].ToString(),
                    };
                    RetList.Add(items);
                    Ret.ReturnList = RetList;
                    Ret.myreturn = "Success";
                }
            }
            catch (Exception e)
            {
                Ret.ReturnList = RetList;
                Ret.myreturn = e.ToString();
            }
            return Ret;
        }
        [HttpPost]
        [Route("BIOTAMS_Add")]
        public string AddBiometricDevice(BioParams param)
        {
            try
            {
                tempconnectionstring();
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@ClientName", mytype = SqlDbType.NVarChar, Value = param.CN });
                con.myparameters.Add(new myParameters { ParameterName = "@DeviceName", mytype = SqlDbType.NVarChar, Value = param.DeviceName });
                con.myparameters.Add(new myParameters { ParameterName = "@SerialID", mytype = SqlDbType.NVarChar, Value = param.SerialID });
                con.myparameters.Add(new myParameters { ParameterName = "@KeyCode", mytype = SqlDbType.NVarChar, Value = param.KeyCode });
                con.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.NVarChar, Value = param.EmpID });
                con.ExecuteNonQuery("spV_AddClientBioDevice");
                return "Success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }
        [HttpPost]
        [Route("BIOTAMS_Edit")]
        public string EditBiometricDevice(BioParams param)
        {
            try
            {
                tempconnectionstring();
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@RowID", mytype = SqlDbType.NVarChar, Value = param.RowID });
                con.myparameters.Add(new myParameters { ParameterName = "@DeviceName", mytype = SqlDbType.NVarChar, Value = param.DeviceName });
                con.myparameters.Add(new myParameters { ParameterName = "@SerialID", mytype = SqlDbType.NVarChar, Value = param.SerialID });
                con.myparameters.Add(new myParameters { ParameterName = "@KeyCode", mytype = SqlDbType.NVarChar, Value = param.KeyCode });
                con.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.NVarChar, Value = param.EmpID });
                con.ExecuteNonQuery("spV_UpdateClientBioDevice");
                return "Success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }
        [HttpPost]
        [Route("BIOTAMS_Delete")]
        public string DeleteBiometricDevice(BioParams param)
        {
            try
            {
                tempconnectionstring();
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@RowID", mytype = SqlDbType.NVarChar, Value = param.RowID });
                con.ExecuteNonQuery("spV_DeleteClientBioDevice");
                return "Success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }

        [HttpPost]
        [Route("BIOTAMS_GetInfo")]
        public List<BiometricsInfo> BiometricsInfo (BioParams param)
        {
            List<BiometricsInfo> List = new List<BiometricsInfo>();
            tempconnectionstring();
            Connection con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@CN", mytype = SqlDbType.NVarChar, Value = param.CN });
            DataTable DT = con.GetDataTable("spV_GetAllClientBioDevice");
            foreach (DataRow row in DT.Rows)
            {
                BiometricsInfo BI = new BiometricsInfo()
                {
                    RowID = row["RowID"].ToString(),
                    ClientID = row["ClientID"].ToString(),
                    DeviceName = row["DeviceName"].ToString(),
                    SerialID = row["SerialID"].ToString(),
                    KeyCode = row["KeyCode"].ToString(),
                    ModifiedBy = row["ModifiedBy"].ToString(),
                    ModifiedDate = row["ModifiedDate"].ToString()
                };
                List.Add(BI);
            }
            return List;
        }

        [HttpPost]
        [Route("AppGyverPic")]
        public string TakePicture(AppGyverParams param)
        {
            try
            {
                byte[] pic = Convert.FromBase64String(param.base64);
                var path = HttpContext.Current.Server.MapPath("~ProfilePhotos/");
                var folder = Path.Combine(path, "AppGyver");
                Directory.CreateDirectory(folder);
                MemoryStream stream = new MemoryStream();
                using (stream)
                {
                    File.WriteAllBytes(folder + "/" + "NewPic" + ".jpeg", pic);
                }
                string returnpath = folder + "/" + "NewPic" + ".jpeg";
                return returnpath;
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }

        [HttpPost]
        [Route("BioTimeInOut")]
        public string BioTAMSLogs(BioTAMSParams param)
        {
            try
            {
                GetDatabase GDB = new GetDatabase();
                GDB.ClientName = param.CompanyName;
                SelfieRegistration2Controller.GetDB2(GDB);
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@BioEmpID", mytype = SqlDbType.VarChar, Value = param.BioEmpID });
                con.myparameters.Add(new myParameters { ParameterName = "@RecordDate", mytype = SqlDbType.VarChar, Value = param.RecordDate });
                con.myparameters.Add(new myParameters { ParameterName = "@LogType", mytype = SqlDbType.VarChar, Value = param.LogType });
                con.myparameters.Add(new myParameters { ParameterName = "@SerialID", mytype = SqlDbType.VarChar, Value = param.SerialID });
                con.ExecuteNonQuery("sp_BioTAMS_LogsInsert");
                return "Success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }
    }
}
