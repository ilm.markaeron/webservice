﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using illimitadoWepAPI.Models;
using System.Xml.Serialization;
using System.IO;
using System.Data;
using System.Data.SqlClient;
using illimitadoWepAPI.VueClass;
using Microsoft.ProjectOxford.Face;
using System.Web;
using System.Threading.Tasks;

namespace illimitadoWepAPI.Controllers
{
    public class VueLoginController : ApiController
    {

        [HttpPost]
        [Route("VueLogin")]
        public string VueLogin(VueLogin VueLogin)
        {
            VueConnection Connection = new VueConnection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@empId", mytype = SqlDbType.NVarChar, Value = VueLogin.EmpID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@password", mytype = SqlDbType.NVarChar, Value = VueLogin.Password });
            DataRow dr;

            dr = Connection.GetSingleRow("sp_HRMS_VUE_Login");


            return dr["access"].ToString();

        }

        [HttpPost]
        [Route("VueEmployeeProfile")]
        public VueUserProfile VueEmployeeProfile(VueLogin VueLogin)
        {
            VueConnection Connection = new VueConnection();


            Connection.myparameters.Add(new myParameters { ParameterName = "@empId", mytype = SqlDbType.NVarChar, Value = VueLogin.EmpID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@password", mytype = SqlDbType.NVarChar, Value = VueLogin.Password });
            DataRow dr;

            dr = Connection.GetSingleRow("sp_HRMS_VUE_EmployeeProfile");

            VueUserProfile userprofile = new VueUserProfile
            {
                EmpID = dr["username"].ToString(),
                Password = dr["userpassword"].ToString(),
                FirstName = dr["First_Name"].ToString(),
                LastName = dr["Last_Name"].ToString(),
                MiddleName = dr["Middle_Name"].ToString(),
                FaceID = dr["faceID"].ToString(),
                Access = dr["access"].ToString(),
                JoinDate = dr["DateJoined"].ToString(),
                Marrieddate = dr["MarriedDate"].ToString(),
                BirthDay = dr["DOB"].ToString(),
                CivilStatus = dr["MaritalStatus"].ToString(),
                Coordinates = dr["Coordinates"].ToString(),
                Alias = dr["Alias"].ToString(),
                ProfilePic = dr["ProfilePic"].ToString(),
                SignatureID = dr["SignatureID"].ToString(),
                DateNow = dr["DateNow"].ToString() == "" ? null : Convert.ToDateTime(dr["DateNow"]).ToString("yyyy-MM-dd"),
                DateTimeNow = dr["DateTimeNow"].ToString() == "" ? null : Convert.ToDateTime(dr["DateTimeNow"]).ToString("MMM dd, yyyy HH:mm:ss"),
                ReasonID = dr["ReasonID"].ToString(),
                ReasonDesc = dr["ReasonDesc"].ToString(),
                Category = dr["Category"].ToString(),
                StartTime = dr["StartTime"].ToString() == "" ? null : Convert.ToDateTime(dr["StartTime"]).ToString("MMM dd, yyyy HH:mm:ss")
            };
            return userprofile;

        }

        [HttpPost]
        [Route("VueTimeIn")]
        public string VueTimeIn(VueLogin VueLogin)
        {
            VueConnection con = new VueConnection();
            con.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.NVarChar, Value = VueLogin.EmpID });
            //con.myparameters.Add(new myParameters { ParameterName = "@Coordinates", mytype = SqlDbType.NVarChar, Value = DailyParameters.Coordinates });
            //con.myparameters.Add(new myParameters { ParameterName = "@NearestEstablishment", mytype = SqlDbType.NVarChar, Value = DailyParameters.NearestEstablishment });
            con.ExecuteNonQuery("sp_HRMS_VUE_TimeIn");

            return "Success";
        }

        [HttpPost]
        [Route("VueBreak")]
        public string VueBreak(VueForBreaks VueForBreaks)
        {
            VueConnection con = new VueConnection();
            con.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.NVarChar, Value = VueForBreaks.EmpID });
            con.myparameters.Add(new myParameters { ParameterName = "@ReasonID", mytype = SqlDbType.NVarChar, Value = VueForBreaks.ReasonID });
            con.ExecuteNonQuery("sp_HRMS_VUE_Break");

            return "Success";
        }

        [HttpPost]
        [Route("VueTimeOut")]
        public string VueTimeOut(VueLogin VueLogin)
        {
            VueConnection con = new VueConnection();
            con.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.NVarChar, Value = VueLogin.EmpID });
            con.myparameters.Add(new myParameters { ParameterName = "@Coordinates", mytype = SqlDbType.NVarChar, Value = "" });
            con.myparameters.Add(new myParameters { ParameterName = "@NearestEstablishment", mytype = SqlDbType.NVarChar, Value = "" });
            //con.myparameters.Add(new myParameters { ParameterName = "@Coordinates", mytype = SqlDbType.NVarChar, Value = DailyParameters.Coordinates });
            //con.myparameters.Add(new myParameters { ParameterName = "@NearestEstablishment", mytype = SqlDbType.NVarChar, Value = DailyParameters.NearestEstablishment });
            con.ExecuteNonQuery("sp_HRMS_VUE_TimeOut");

            return "Success";
        }



        [HttpPost]
        [Route("BreakList")]
        public List<BreakList> BreakList(ReasonIDs ReasonIDs)
        {
            VueConnection con = new VueConnection();
            DataTable DT = new DataTable();
            DT = con.GetDataTable("sp_HRMS_VUE_ReasonIDs");

            List<BreakList> ListReturned = new List<BreakList>();

            for (int i = 0; i < DT.Rows.Count; i++)
            {
                BreakList BreakListItems = new BreakList();
                BreakListItems.ReasonID = DT.Rows[i]["ReasonID"].ToString();
                BreakListItems.ReasonDesc = DT.Rows[i]["ReasonDesc"].ToString();
                ListReturned.Add(BreakListItems);
            }
            return ListReturned;
        }

        [HttpPost]
        [Route("VueCurrentDayActivity")]
        public VueDailyResult VueCurrentDayActivity(VueLogin VueLogin)
        {
            VueDailyResult GetDailyResult = new VueDailyResult();
            //try
            //{
            VueConnection Connection = new VueConnection();
            List<VueDaily> DailyList = new List<VueDaily>();
            DataTable dtDAILY = new DataTable();
            //BasicFunction BasicFunction = new BasicFunction();
            Connection.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.NVarChar, Value = VueLogin.EmpID });
            dtDAILY = Connection.GetDataTable("sp_HRMS_VUE_CurrentDayActivity");
            //CultureInfo provider = CultureInfo.InvariantCulture;
            if (dtDAILY.Rows.Count > 0)
            {
                foreach (DataRow row in dtDAILY.Rows)
                {
                    VueDaily Daily = new VueDaily
                    {
                        SchedDate = Convert.ToDateTime(row["SchedDate"]).ToString("yyyy-MM-dd"),
                        SchedIN = row["Start_Time"].ToString() == "" ? null : Convert.ToDateTime(row["Start_Time"]).ToString("hh:mm tt"),
                        SchedOUT = row["End_Time"].ToString() == "" ? null : Convert.ToDateTime(row["End_Time"]).ToString("hh:mm tt"),
                        ReasonDesc = row["ReasonDesc"].ToString()
   
                    };
                    DailyList.Add(Daily);
                }
                GetDailyResult.Daily = DailyList;
                GetDailyResult.myreturn = "Success";
            }
            else
            {
                GetDailyResult.myreturn = "No Data Available";
            }
            //}
            //catch
            //{
            //    GetDailyResult.myreturn = "Error Fetching Data";

            //}
            return GetDailyResult;
        }

        [HttpPost]
        [Route("VueLoggedTeammates")]
        public VueLoggedTeamResult VueLoggedTeammates(VueLogin VueLogin)
        {
            VueLoggedTeamResult GetDailyResult = new VueLoggedTeamResult();
            //try
            //{
            VueConnection Connection = new VueConnection();
            List<VueLoggedTeam> DailyList = new List<VueLoggedTeam>();
            DataTable dtDAILY = new DataTable();
            //BasicFunction BasicFunction = new BasicFunction();
            Connection.myparameters.Add(new myParameters { ParameterName = "@empid", mytype = SqlDbType.NVarChar, Value = VueLogin.EmpID });
            dtDAILY = Connection.GetDataTable("sp_HRMS_VUE_SearchSavedLoggedTeam");
            //CultureInfo provider = CultureInfo.InvariantCulture;
            if (dtDAILY.Rows.Count > 0)
            {
                foreach (DataRow row in dtDAILY.Rows)
                {
                    VueLoggedTeam Daily = new VueLoggedTeam
                    {
                        EmpID = row["EmpID"].ToString(),
                        EmpName = row["EmpName"].ToString(),
                        BaseID = row["BaseID"].ToString() == "" ? null : row["BaseID"].ToString(),
                        Reason = row["Reason"].ToString()


                    };
                    DailyList.Add(Daily);
                }
                GetDailyResult.VueLoggedTeam = DailyList;
                GetDailyResult.myreturn = "Success";
            }
            else
            {
                GetDailyResult.myreturn = "No Data Available";
            }
            //}
            //catch
            //{
            //    GetDailyResult.myreturn = "Error Fetching Data";

            //}
            return GetDailyResult;
        }

        [HttpPost]
        [Route("GetDateTimeNow")]
        public string GetDateTimeNow()
        {
            VueConnection Connection = new VueConnection();

            DataRow dr;

            dr = Connection.GetSingleRow("sp_HRMS_VUE_Get_ServerTime");


            return Convert.ToDateTime(dr["DateTimeNow"]).ToString("MMM dd, yyyy HH:mm:ss");

        }

        [HttpPost]
        [Route("Favorites")]
        public List<Favorites> Favorites(VueLogin VueLogin)
        {
            Favorites Favorites = new Favorites();
            VueConnection con = new VueConnection();
            con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = VueLogin.EmpID });
            DataTable DT = new DataTable();
            DT = con.GetDataTable("sp_Favourites");
            List<Favorites> ListReturned = new List<Favorites>();
            for (int i = 0; i < DT.Rows.Count; i++)
            {
                Favorites FavoritesListItems = new Favorites();
                FavoritesListItems.fav1i = DT.Rows[0][0].ToString();
                FavoritesListItems.spanfav1 = DT.Rows[0][1].ToString();
                FavoritesListItems.linkfav1 = DT.Rows[0][2].ToString();
                FavoritesListItems.favid1 = DT.Rows[0][3].ToString();

                FavoritesListItems.fav2i = DT.Rows[1][0].ToString();
                FavoritesListItems.spanfav2 = DT.Rows[1][1].ToString();
                FavoritesListItems.linkfav2 = DT.Rows[1][2].ToString();
                FavoritesListItems.favid2 = DT.Rows[1][3].ToString();

                FavoritesListItems.fav3i = DT.Rows[2][0].ToString();
                FavoritesListItems.spanfav3 = DT.Rows[2][1].ToString();
                FavoritesListItems.linkfav3 = DT.Rows[2][2].ToString();
                FavoritesListItems.favid3 = DT.Rows[2][3].ToString();

                FavoritesListItems.fav4i = DT.Rows[3][0].ToString();
                FavoritesListItems.spanfav4 = DT.Rows[3][1].ToString();
                FavoritesListItems.linkfav4 = DT.Rows[3][2].ToString();
                FavoritesListItems.favid4 = DT.Rows[3][3].ToString();

                FavoritesListItems.fav5i = DT.Rows[4][0].ToString();
                FavoritesListItems.spanfav5 = DT.Rows[4][1].ToString();
                FavoritesListItems.linkfav5 = DT.Rows[4][2].ToString();
                FavoritesListItems.favid5 = DT.Rows[4][3].ToString();

                FavoritesListItems.fav6i = DT.Rows[5][0].ToString();
                FavoritesListItems.spanfav6 = DT.Rows[5][1].ToString();
                FavoritesListItems.linkfav6 = DT.Rows[5][2].ToString();
                FavoritesListItems.favid6 = DT.Rows[5][3].ToString();

                FavoritesListItems.fav7i = DT.Rows[6][0].ToString();
                FavoritesListItems.spanfav7 = DT.Rows[6][1].ToString();
                FavoritesListItems.linkfav7 = DT.Rows[6][2].ToString();
                FavoritesListItems.favid7 = DT.Rows[6][3].ToString();

                FavoritesListItems.fav8i = DT.Rows[7][0].ToString();
                FavoritesListItems.spanfav8 = DT.Rows[7][1].ToString();
                FavoritesListItems.linkfav8 = DT.Rows[7][2].ToString();
                FavoritesListItems.favid8 = DT.Rows[7][3].ToString();
                ListReturned.Add(FavoritesListItems);
            }
            return ListReturned;
        }


        [HttpPost]
        [Route("PayPeriod")]
        public List<PayoutScheme> PayPeriod(PayoutScheme PayoutScheme)
        {
            VueConnection con = new VueConnection();
            con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = PayoutScheme.EmpID });
            DataTable DT = new DataTable();
            DT = con.GetDataTable("sp_HRMS_VUE_PayoutSchemes");
            List<PayoutScheme> ListReturned = new List<PayoutScheme>();
            for (int i = 0; i < DT.Rows.Count; i++)
            {
                PayoutScheme PayoutSchemeListItems = new PayoutScheme();
                PayoutSchemeListItems.EmpID = PayoutScheme.EmpID;
                PayoutSchemeListItems.PayOutID = DT.Rows[i]["id"].ToString();
                PayoutSchemeListItems.PayOutName = DT.Rows[i]["PayOutScheme"].ToString();
                PayoutSchemeListItems.PayFrom = DT.Rows[i]["PayOutFrom"].ToString() + " - " + DT.Rows[i]["PayOutTo"].ToString();
                PayoutSchemeListItems.PayOn = DT.Rows[i]["PayOn"].ToString();
                PayoutSchemeListItems.PayOutDate = DT.Rows[i]["PayOutDate"].ToString();
                ListReturned.Add(PayoutSchemeListItems);
            }
            return ListReturned;
        }

        [HttpPost]
        [Route("DashboardTimeRecord")]
        public List<TimeRecord> CurrentTimeRecord(TimeRecord TimeRecord)
        {
            VueConnection con = new VueConnection();
            con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = TimeRecord.EmpID });
            con.myparameters.Add(new myParameters { ParameterName = "@PAYPERIODFROM", mytype = SqlDbType.NVarChar, Value = ""});
            con.myparameters.Add(new myParameters { ParameterName = "@PAYPERIODTO", mytype = SqlDbType.NVarChar, Value = "" });
            DataTable DT = new DataTable();
            DT = con.GetDataTable("sp_HRMS_VUE_TimeRecord");
            List<TimeRecord> ListReturned = new List<TimeRecord>();
            for (int i = 0; i < DT.Rows.Count; i++)
            {
                TimeRecord TimeRecordListItems = new TimeRecord();
                TimeRecordListItems.RunningPaidHours = DT.Rows[i]["PaidHours"].ToString();
                TimeRecordListItems.LeavesTaken = DT.Rows[i]["LeavesTaken"].ToString();
                TimeRecordListItems.Mot = DT.Rows[i]["Mot"].ToString();
                TimeRecordListItems.Payfromto = DT.Rows[i]["Payfromto"].ToString();
                ListReturned.Add(TimeRecordListItems);
            }
            return ListReturned;
        }

        [HttpPost]
        [Route("SearchDashboardTimeRecord")]
        public List<TimeRecord> SearchDashboardTimeRecords(TimeRecord TimeRecord)
        {
            VueConnection con = new VueConnection();
            con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = TimeRecord.EmpID });
            con.myparameters.Add(new myParameters { ParameterName = "@PAYPERIODMONTH", mytype = SqlDbType.NVarChar, Value = TimeRecord.PayPeriodFrom });
            con.myparameters.Add(new myParameters { ParameterName = "@PAYPERIODDAY", mytype = SqlDbType.NVarChar, Value = TimeRecord.PayPeriodTo });
            DataTable DT = new DataTable();
            DT = con.GetDataTable("sp_HRMS_VUE_SearchTimeRecord");
            List<TimeRecord> ListReturned = new List<TimeRecord>();
            for (int i = 0; i < DT.Rows.Count; i++)
            {
                TimeRecord TimeRecordListItems = new TimeRecord();
                TimeRecordListItems.RunningPaidHours = DT.Rows[i]["PaidHours"].ToString();
                ListReturned.Add(TimeRecordListItems);
            }
            return ListReturned;
        }

        [HttpPost]
        [Route("VueNewsUpdates")]
        public List<ImageNewsUpdates> NewsUpdates(VueLogin VueLogin)
        {

            VueConnection con = new VueConnection();
            con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = VueLogin.EmpID });
            DataTable DT = new DataTable();
            DT = con.GetDataTable("sp_HRMS_VUE_NewsUpdates");
            List<ImageNewsUpdates> ListReturned = new List<ImageNewsUpdates>();
            for (int i = 0; i < DT.Rows.Count; i++)
            {

                ImageNewsUpdates ImageNewsUpdatesListItems = new ImageNewsUpdates();
                ImageNewsUpdatesListItems.img = "data:image/png;base64," + DT.Rows[i]["Image"].ToString();
                ImageNewsUpdatesListItems.title = DT.Rows[i]["Title"].ToString();
                ImageNewsUpdatesListItems.news = DT.Rows[i]["Message"].ToString();
                ImageNewsUpdatesListItems.sender = DT.Rows[i]["SenderName"].ToString();
                ListReturned.Add(ImageNewsUpdatesListItems);
            }
            return ListReturned;
        }

        [HttpPost]
        [Route("UploadPicture")]
        public async Task<HttpResponseMessage> UploadProfile(SavePicture SavePic)
        {
            //HttpRequest httpRequest = HttpContext.Current.Request;

            //BinaryReader binaryReader = new BinaryReader(httpRequest.Files[0].InputStream);

            //string NTID = httpRequest.Form["NTID"];
            //string Name = httpRequest.Form["Name"];
            //byte[] Photo = binaryReader.ReadBytes(httpRequest.Files[0].ContentLength);
            SavePic.b64img = SavePic.b64img.Replace("data:image/png;base64,", "");
            SavePic.b64img = SavePic.b64img.Replace("data:image/jpeg;base64,", "");
            string NTID = SavePic.NTID;
            byte[] Photo = Convert.FromBase64String(SavePic.b64img);
            //string Set = httpRequest.Form["Set"];

            HttpResponseMessage msg;

            //var path = HttpContext.Current.Server.MapPath("~/Profile/");
            var path = HttpContext.Current.Server.MapPath("~/ProfilePhotos/");
            //var path = "http://uat-vm.southeastasia.cloudapp.azure.com:81/hrms_test/ProfilePhotos";
            var folder = Path.Combine(path, NTID);
            Directory.CreateDirectory(folder);

            MemoryStream stream = new MemoryStream();

            try
            {
                using (stream)
                {
                    //File.WriteAllBytes(folder + "/" + NTID + "_pic" + Set + ".png", Photo);
                    File.WriteAllBytes(folder + "/" + NTID + "_pic" + SavePic.num + ".png", Photo);
                }

                msg = Request.CreateResponse(HttpStatusCode.Accepted);
            }
            catch
            {
                msg = Request.CreateResponse(HttpStatusCode.NotAcceptable);
            }

            return msg;
        }








        [HttpPost]
        [Route("DetectPicture")]
        public async Task<HttpResponseMessage> DetectFace(SavePicture SavePic)
        {
            //HttpRequest httpRequest = HttpContext.Current.Request;
            //BinaryReader binaryReader = new BinaryReader(httpRequest.Files[0].InputStream);
            //byte[] Photo = binaryReader.ReadBytes(httpRequest.Files[0].ContentLength);
            SavePic.b64img = SavePic.b64img.Replace("data:image/png;base64,", "");
            SavePic.b64img = SavePic.b64img.Replace("data:image/jpeg;base64,", "");
            byte[] Photo = Convert.FromBase64String(SavePic.b64img);

            //MemoryStream inputStream = new MemoryStream(Photo);
            Stream inputStream = new MemoryStream(Photo);

            try
            {
                //FaceServiceClient faceServiceClient = new FaceServiceClient("f2058d893bfe4cbda5c2b67cb82d1145", "https://southeastasia.api.cognitive.microsoft.com/face/v1.0");
                //FaceServiceClient faceServiceClient = new FaceServiceClient("1b0c7f9bae724281be613407ca9952bd", "https://westcentralus.api.cognitive.microsoft.com/face/v1.0");
                FaceServiceClient faceServiceClient = new FaceServiceClient("52bfa9947cac4e9db7dbae1382e91a11", "https://westcentralus.api.cognitive.microsoft.com/face/v1.0");
                string personGroupId = "ilmdevs";
                var faces = await faceServiceClient.DetectAsync(inputStream);
                var faceIds = faces.Select(face => face.FaceId).ToArray();

                try
                {
                    var results = await faceServiceClient.IdentifyAsync(personGroupId, faceIds);

                    if (results.Count() > 1)
                    {
                        return Request.CreateResponse(HttpStatusCode.BadRequest, "Multiple faces detected!");
                        //return "Multiple faces detected!";
                        //return Request.CreateResponse(HttpStatusCode.NotAcceptable);
                    }
                    else
                    {
                        var candidateId = results.FirstOrDefault().Candidates.FirstOrDefault().PersonId;
                        var person = await faceServiceClient.GetPersonAsync(personGroupId, candidateId);
                        return Request.CreateResponse(HttpStatusCode.Accepted, person.PersonId);
                        //return person.Name;
                        //return Request.CreateResponse(HttpStatusCode.Accepted);
                    }
                }
                catch
                {
                    return Request.CreateResponse(HttpStatusCode.NotAcceptable, "Error");
                    //return "Error";
                    //return Request.CreateResponse(HttpStatusCode.NotAcceptable);
                }
            }
            catch
            {
                return Request.CreateResponse(HttpStatusCode.NotAcceptable, "Error");
                //return "Error";
                //return Request.CreateResponse(HttpStatusCode.NotAcceptable);
            }
        }





        [HttpPost]
        [Route("RegisterPicture")]
        public async Task<string> RegisterFace(SelfieRegistrations selfieReg)
        {
            string name = selfieReg.Name;
            string ntid = selfieReg.NTID;

            try
            {
                //FaceServiceClient faceServiceClient = new FaceServiceClient("f2058d893bfe4cbda5c2b67cb82d1145", "https://southeastasia.api.cognitive.microsoft.com/face/v1.0");
                //FaceServiceClient faceServiceClient = new FaceServiceClient("1b0c7f9bae724281be613407ca9952bd", "https://westcentralus.api.cognitive.microsoft.com/face/v1.0");
                //FaceServiceClient faceServiceClient = new FaceServiceClient("f5fb19bbc51641d6b4216b2a02fdee7b", "https://southeastasia.api.cognitive.microsoft.com/face/v1.0");
                FaceServiceClient faceServiceClient = new FaceServiceClient("52bfa9947cac4e9db7dbae1382e91a11", "https://westcentralus.api.cognitive.microsoft.com/face/v1.0");
                var person = await faceServiceClient.CreatePersonAsync("ilmdevs", ntid);

                Guid persondID = person.PersonId;

                //var path = HttpContext.Current.Server.MapPath("~/Profile/");
                var path = HttpContext.Current.Server.MapPath("~/ProfilePhotos/");
                var folder = Path.Combine(path, ntid);

                Stream stream1 = File.OpenRead(folder + "/" + ntid + "_pic1.png");
                Stream stream2 = File.OpenRead(folder + "/" + ntid + "_pic2.png");

                await faceServiceClient.AddPersonFaceAsync("ilmdevs", persondID, stream1);
                await faceServiceClient.AddPersonFaceAsync("ilmdevs", persondID, stream2);

                await faceServiceClient.TrainPersonGroupAsync("ilmdevs");

                string guidStr = persondID.ToString("D");
                VueConnection con = new VueConnection();
                con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = ntid });
                con.myparameters.Add(new myParameters { ParameterName = "@FACEID", mytype = SqlDbType.NVarChar, Value = guidStr });
                con.ExecuteNonQuery("sp_HRMS_VUE_UpdateFaceID");
                return guidStr;
            }
            catch (Exception e)
            {
                return e.Message.ToString();
            }
        }

        [HttpPost]
        [Route("VueFaceLogin")]
        public List<VueFaceLogin> FaceLoginVue(SelfieRegistrations SelfieRegistrations)
        {

            VueConnection con = new VueConnection();
            con.myparameters.Add(new myParameters { ParameterName = "@FACEID", mytype = SqlDbType.NVarChar, Value = SelfieRegistrations.FaceID });
            DataTable DT = new DataTable();
            DT = con.GetDataTable("sp_HRMS_VUE_FaceLogin");
            List<VueFaceLogin> ListReturned = new List<VueFaceLogin>();
            for (int i = 0; i < DT.Rows.Count; i++)
            {

                VueFaceLogin VueFaceLogin = new VueFaceLogin();
                VueFaceLogin.Access = DT.Rows[i]["access"].ToString();
                VueFaceLogin.EmpID = DT.Rows[i]["empId"].ToString();
                VueFaceLogin.Password = DT.Rows[i]["password"].ToString();
                ListReturned.Add(VueFaceLogin);
            }
            return ListReturned;
        }

        [HttpPost]
        [Route("VueSchedule")]

        public List<VueSchedule> ScheduleVue(ScheduleParam ScheduleParam)
        {

            VueConnection con = new VueConnection();
            con.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.NVarChar, Value = ScheduleParam.EmpID });
            con.myparameters.Add(new myParameters { ParameterName = "@StartDate", mytype = SqlDbType.NVarChar, Value = Convert.ToDateTime(ScheduleParam.StartDate).ToString("yyyy-MM-dd") });
            con.myparameters.Add(new myParameters { ParameterName = "@EndDate", mytype = SqlDbType.NVarChar, Value = Convert.ToDateTime(ScheduleParam.EndDate).ToString("yyyy-MM-dd") });
            DataTable DT = new DataTable();
            DT = con.GetDataTable("sp_Vue_GetSchedule");
            List<VueSchedule> ListReturned = new List<VueSchedule>();
            for (int i = 0; i < DT.Rows.Count; i++)
            {

                VueSchedule VueFaceLogin = new VueSchedule();
                VueFaceLogin.Day = DT.Rows[i]["Day"].ToString();
                VueFaceLogin.SchedIN = DT.Rows[i]["SchedIN"].ToString() == "" ? null : Convert.ToDateTime(DT.Rows[i]["SchedIN"]).ToString("hh:mm tt");
                VueFaceLogin.SchedOUT = DT.Rows[i]["SchedOUT"].ToString() == "" ? null : Convert.ToDateTime(DT.Rows[i]["SchedOUT"]).ToString("hh:mm tt");
                VueFaceLogin.SchedDate = Convert.ToDateTime(DT.Rows[i]["SchedDate"]).ToString("yyyy-MM-dd");
                VueFaceLogin.HourWork = DT.Rows[i]["HoursWorked"].ToString();
                ListReturned.Add(VueFaceLogin);
            }
            return ListReturned;
        }

        [HttpPost]
        [Route("VueScheduledin")]

        public List<VueSchedule> ScheduleVuedin(ScheduleParam ScheduleParam)
        {

            VueConnection con = new VueConnection();
            con.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.NVarChar, Value = ScheduleParam.EmpID });
            con.myparameters.Add(new myParameters { ParameterName = "@PAYPERIODMONTH", mytype = SqlDbType.NVarChar, Value = ScheduleParam.Month});
            con.myparameters.Add(new myParameters { ParameterName = "@PAYPERIODDAY", mytype = SqlDbType.NVarChar, Value = ScheduleParam.Day });
            con.myparameters.Add(new myParameters { ParameterName = "@YEAR", mytype = SqlDbType.NVarChar, Value = ScheduleParam.Year });
            DataTable DT = new DataTable();
            DT = con.GetDataTable("[sp_Vue_GetScheduledin]");
            List<VueSchedule> ListReturned = new List<VueSchedule>();
            for (int i = 0; i < DT.Rows.Count; i++)
            {

                VueSchedule VueFaceLogin = new VueSchedule();
                VueFaceLogin.Day = DT.Rows[i]["Day"].ToString();
                VueFaceLogin.SchedIN = DT.Rows[i]["SchedIN"].ToString() == "" ? null : Convert.ToDateTime(DT.Rows[i]["SchedIN"]).ToString("hh:mm tt");
                VueFaceLogin.SchedOUT = DT.Rows[i]["SchedOUT"].ToString() == "" ? null : Convert.ToDateTime(DT.Rows[i]["SchedOUT"]).ToString("hh:mm tt");
                VueFaceLogin.SchedDate = Convert.ToDateTime(DT.Rows[i]["SchedDate"]).ToString("yyyy-MM-dd");
                VueFaceLogin.HourWork = DT.Rows[i]["HoursWorked"].ToString();
                ListReturned.Add(VueFaceLogin);
            }
            return ListReturned;
        }

        [HttpPost]
        [Route("VueActivity")]

        public List<VueAcitivity> ActivityVue(ScheduleParam ScheduleParam)
        {

            VueConnection con = new VueConnection();
            con.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.NVarChar, Value = ScheduleParam.EmpID });
            con.myparameters.Add(new myParameters { ParameterName = "@SchedDate", mytype = SqlDbType.NVarChar, Value = ScheduleParam.SchedDate });
            DataTable DT = new DataTable();
            DT = con.GetDataTable("sp_Vue_DisplayActivitybySchedate");
            List<VueAcitivity> ListReturned = new List<VueAcitivity>();
            for (int i = 0; i < DT.Rows.Count; i++)
            {

                VueAcitivity VueAcitivty = new VueAcitivity();
                VueAcitivty.SchedDate = Convert.ToDateTime(DT.Rows[i]["SchedDate"]).ToString("MMM-dd-yyyy");
                //SchedDay = Convert.ToDateTime(row["SchedDate"]).DayOfWeek.ToString().Substring(0, 3),
                VueAcitivty.SchedIN = DT.Rows[i]["Start_Time"].ToString() == "" ? null : Convert.ToDateTime(DT.Rows[i]["Start_Time"]).ToString("hh:mm tt");
                VueAcitivty.SchedOUT = DT.Rows[i]["End_Time"].ToString() == "" ? null : Convert.ToDateTime(DT.Rows[i]["End_Time"]).ToString("hh:mm tt");
                VueAcitivty.TotalTimeMin = DT.Rows[i]["TotalTimeMin"].ToString();
                VueAcitivty.SchedType = DT.Rows[i]["ReasonDesc"].ToString();
                ListReturned.Add(VueAcitivty);
            }
            return ListReturned;
        }


    }
}
