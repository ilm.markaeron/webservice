﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using illimitadoWepAPI.Models.ScheduleModels;
using illimitadoWepAPI.ScheduleClass;
using System.Data;

namespace illimitadoWepAPI.Controllers.ScheduleController
{
    public class UserInformationController : ApiController
    {
        [HttpPost]
        [Route("SchedUserSignUp")]
        public string InsertUserSignUp(UserInformation InsertUserinfo)
        {
            try
            {                
                Connection Con = new Connection();
                Con.myparameters.Add(new myParameters { ParameterName = "@USEREMAILID", mytype = SqlDbType.NVarChar, Value = InsertUserinfo.UserEmailID });
                Con.myparameters.Add(new myParameters { ParameterName = "@FNAME", mytype = SqlDbType.NVarChar, Value = InsertUserinfo.First_Name });
                Con.myparameters.Add(new myParameters { ParameterName = "@MNAME", mytype = SqlDbType.NVarChar, Value = InsertUserinfo.Middle_Name });
                Con.myparameters.Add(new myParameters { ParameterName = "@LNAME", mytype = SqlDbType.NVarChar, Value = InsertUserinfo.Last_Name });
                Con.myparameters.Add(new myParameters { ParameterName = "@JOBTITLE", mytype = SqlDbType.NVarChar, Value = InsertUserinfo.Job_Title });
                Con.myparameters.Add(new myParameters { ParameterName = "@COMPANY", mytype = SqlDbType.NVarChar, Value = InsertUserinfo.Company });
                Con.myparameters.Add(new myParameters { ParameterName = "@BRANCH", mytype = SqlDbType.NVarChar, Value = InsertUserinfo.Branch });
                Con.myparameters.Add(new myParameters { ParameterName = "@PHONE", mytype = SqlDbType.NVarChar, Value = InsertUserinfo.Phone_Number });
                Con.myparameters.Add(new myParameters { ParameterName = "@MOBILE", mytype = SqlDbType.NVarChar, Value = InsertUserinfo.Mobile_Number });
                Con.ExecuteScalar("sp_InsertUserAccount");

                return "success";
            }
            catch (Exception e)
            {
                return "failed" + e;
            }
        }
        [HttpPost]
        [Route("GetListPartner")]
        public List<getPartnerList> GetListPartner(getPartnerList getPartner)
        {
            Connection con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@PClientID", mytype = SqlDbType.NVarChar, Value = getPartner.PClientID });


            DataTable DT = new DataTable();
            DT = con.GetDataTable("sp_GetListPartner");

            List<getPartnerList> ListReturned = new List<getPartnerList>();
            for (int i = 0; i < DT.Rows.Count; i++)
            {
                getPartnerList Get = new getPartnerList();
                //account table             
                Get.PClientID = DT.Rows[i]["PClient_ID"].ToString();
                Get.SubClient_ID = DT.Rows[i]["SubClient_ID"].ToString();
                Get.SubClientCode = DT.Rows[i]["SubClientCode"].ToString();
                Get.SubClientName = DT.Rows[i]["SubClientName"].ToString();
                Get.DateCreated = DT.Rows[i]["DateCreated"].ToString();

                //user information table
                Get.DateModified = DT.Rows[i]["DateModified"].ToString();
                Get.Floor = DT.Rows[i]["Floor"].ToString();
                Get.Building = DT.Rows[i]["Building"].ToString();
                Get.Street = DT.Rows[i]["Street"].ToString();
                Get.Barangay = DT.Rows[i]["Barangay"].ToString();
                Get.Municipality = DT.Rows[i]["Municipality"].ToString();
                Get.Province = DT.Rows[i]["Province"].ToString();
                Get.Region = DT.Rows[i]["Region"].ToString();
                Get.Country = DT.Rows[i]["Country"].ToString();
                Get.ZipCode = DT.Rows[i]["ZipCode"].ToString();
                Get.CompanyNationality = DT.Rows[i]["CompanyNationality"].ToString();
                Get.NumberOfEmployees = DT.Rows[i]["NumberOfEmployees"].ToString();
                Get.CompanyTIN = DT.Rows[i]["CompanyTIN"].ToString();
                Get.IsActive = DT.Rows[i]["IsActive"].ToString();

                ListReturned.Add(Get);
            }
            return ListReturned;
        }
        [HttpPost]
        [Route("CheckPartnershipAlias")]
        public string CheckPartnershipAlias(CheckPartnershipAlias partner)
        {
            try
            {
                Connection Connection = new Connection();

                Connection.myparameters.Add(new myParameters { ParameterName = "@PClientID", mytype = SqlDbType.NVarChar, Value = partner.PClientID });
                Connection.myparameters.Add(new myParameters { ParameterName = "@PartnerAlias", mytype = SqlDbType.NVarChar, Value = partner.PartnerAlias });
                return Connection.ExecuteScalar("sp_CheckPartnershipAlias").ToString();
            }
            catch (Exception e)
            {

                return e.ToString();
            }

        }
        [HttpPost]
        [Route("UserInformation")]
        public List<UserInformation> ScheduleVue(UserInformation GetUserinfo)
        {
            Connection con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@USEREMAILID", mytype = SqlDbType.NVarChar, Value = GetUserinfo.UserEmailID });
            con.myparameters.Add(new myParameters { ParameterName = "@USERPASS", mytype = SqlDbType.NVarChar, Value = GetUserinfo.UserPass });
            DataTable DT = new DataTable();
            DT = con.GetDataTable("sp_GetUserAccount");

            List<UserInformation> ListReturned = new List<UserInformation>();
            for (int i = 0; i < DT.Rows.Count; i++)
            {
                UserInformation Get = new UserInformation();
                //account table             
                Get.UserEmailID = DT.Rows[i]["UserEmailID"].ToString();
                Get.UserPass = DT.Rows[i]["UserPass"].ToString();
                Get.IsActive = DT.Rows[i]["IsActive"].ToString();
                Get.ForgotPass = DT.Rows[i]["ForgotPass"].ToString();
                Get.RandomCode = DT.Rows[i]["RandomCode"].ToString();

                //user information table
                Get.First_Name = DT.Rows[i]["First_Name"].ToString();
                Get.Middle_Name = DT.Rows[i]["Middle_Name"].ToString();
                Get.Last_Name = DT.Rows[i]["Last_Name"].ToString();
                Get.Job_Title = DT.Rows[i]["Job_Title"].ToString();
                Get.Company = DT.Rows[i]["Company"].ToString();
                Get.Branch = DT.Rows[i]["Branch"].ToString();
                Get.Phone_Number = DT.Rows[i]["Phone_Number"].ToString();
                Get.Mobile_Number = DT.Rows[i]["Mobile_Number"].ToString();
                ListReturned.Add(Get);
            }
            return ListReturned;
        }

        [HttpPost]
        [Route("ScheduleLogin")]
        public bool Login(UserInformation login)
        {
            Connection con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@UserEmail", mytype = SqlDbType.NVarChar, Value = login.UserEmailID });
            con.myparameters.Add(new myParameters { ParameterName = "@Password", mytype = SqlDbType.NVarChar, Value = login.UserPass });
            int result = Convert.ToInt32(con.ExecuteScalar("sp_LoginAccount"));
            if (result > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        [HttpPost]
        [Route("SchedulerCreatePartner")]
        public string InsertNewPartner(SubClientParams Params)
        {
            try
            {
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@pclientid", mytype = SqlDbType.VarChar, Value = Params.PClient_ID });
                con.myparameters.Add(new myParameters { ParameterName = "@subclientcode", mytype = SqlDbType.VarChar, Value = Params.SubClientCode });
                con.myparameters.Add(new myParameters { ParameterName = "@subclientname", mytype = SqlDbType.VarChar, Value = Params.SubClientName });
                con.myparameters.Add(new myParameters { ParameterName = "@dateofincorporation", mytype = SqlDbType.VarChar, Value = Params.DateOfIncorporation });
                con.myparameters.Add(new myParameters { ParameterName = "@floor", mytype = SqlDbType.VarChar, Value = Params.Floor });
                con.myparameters.Add(new myParameters { ParameterName = "@building", mytype = SqlDbType.VarChar, Value = Params.Building });
                con.myparameters.Add(new myParameters { ParameterName = "@street", mytype = SqlDbType.VarChar, Value = Params.Street });
                con.myparameters.Add(new myParameters { ParameterName = "@barangay", mytype = SqlDbType.VarChar, Value = Params.Barangay });
                con.myparameters.Add(new myParameters { ParameterName = "@municipality", mytype = SqlDbType.VarChar, Value = Params.Municipality });
                con.myparameters.Add(new myParameters { ParameterName = "@province", mytype = SqlDbType.VarChar, Value = Params.Province });
                con.myparameters.Add(new myParameters { ParameterName = "@region", mytype = SqlDbType.VarChar, Value = Params.Region });
                con.myparameters.Add(new myParameters { ParameterName = "@country", mytype = SqlDbType.VarChar, Value = Params.Country });
                con.myparameters.Add(new myParameters { ParameterName = "@zipcode", mytype = SqlDbType.VarChar, Value = Params.ZipCode });
                con.myparameters.Add(new myParameters { ParameterName = "@companynationality", mytype = SqlDbType.VarChar, Value = Params.CompanyNationality });
                con.myparameters.Add(new myParameters { ParameterName = "@noofemployees", mytype = SqlDbType.VarChar, Value = Params.NoOfEmployees });
                con.myparameters.Add(new myParameters { ParameterName = "@companytin", mytype = SqlDbType.VarChar, Value = Params.CompanyTIN });
                con.myparameters.Add(new myParameters { ParameterName = "@username", mytype = SqlDbType.VarChar, Value = Params.Username });
                con.myparameters.Add(new myParameters { ParameterName = "@firstname", mytype = SqlDbType.VarChar, Value = Params.FirstName });
                con.myparameters.Add(new myParameters { ParameterName = "@middlename", mytype = SqlDbType.VarChar, Value = Params.MiddleName });
                con.myparameters.Add(new myParameters { ParameterName = "@lastname", mytype = SqlDbType.VarChar, Value = Params.LastName });
                con.myparameters.Add(new myParameters { ParameterName = "@reptitle", mytype = SqlDbType.VarChar, Value = Params.RepTitle });
                con.myparameters.Add(new myParameters { ParameterName = "@repemail", mytype = SqlDbType.VarChar, Value = Params.RepEmail });
                con.myparameters.Add(new myParameters { ParameterName = "@repcontact", mytype = SqlDbType.VarChar, Value = Params.RepContact });
                con.ExecuteNonQuery("sp_Create_Client");
                return "Success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }
    }
}
