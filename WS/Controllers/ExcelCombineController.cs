﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Hosting;
using System.Web.Http;
using System.Data;
using System.Configuration;
using System.Reflection;
using Microsoft.Office.Interop.Excel;

namespace illimitadoWepAPI.Controllers
{
    public class ExcelCombineController : ApiController
    {
        [HttpPost]
        [Route("combine")]
        public string CombineCSV()
        {
            try
            {
                //exPath path
                System.Data.DataTable dt = new System.Data.DataTable();
                //dt.Columns.AddRange(new DataColumn[1] { new DataColumn("EMPID") });
                //dt.Rows.Add(@"C:\Users\DEV06\Documents\fasdf.xlsx");
                //dt.Rows.Add(@"C:\Users\DEV06\Documents\zxczxc.xlsx");
                //string path1 = path.paths[0].Replace("/","\\");
                //string path2 = path.paths[1].Replace("/", "\\");
                MergeExcel.DoMerge(new string[]
                {
                @"C:\Users\MSSQLSERVER\attendance.csv",
                @"C:\Users\MSSQLSERVER\EPRS.csv",
                @"C:\Users\MSSQLSERVER\leavereport.csv",
                @"C:\Users\MSSQLSERVER\LoanReport.csv",
                @"C:\Users\MSSQLSERVER\PayrollRegister.csv",
                @"C:\Users\MSSQLSERVER\StatutoryRemittances.csv"
                },
                    @"C:\Users\MSSQLSERVER\result.xlsx", "E", 2, "EPRS", dt);
                return "success";
            }
            catch (Exception e)
            {
                return e.ToString();

            }
            //MergeExcel.DoMerge(new string[]
            //{
            //    @"C:\Users\Dev15\Documents\files\Dev15\test emails\StatutoryRemittances11.csv"
            //},
            //    @"C:\Users\Dev15\Desktop\result.xlsx", "E", 2, "Statutory");

        }


    }
    public class MergeExcel
    {
        Microsoft.Office.Interop.Excel.Application app = new Microsoft.Office.Interop.Excel.ApplicationClass();
        Microsoft.Office.Interop.Excel.Workbook bookDest = null;
        Microsoft.Office.Interop.Excel.Worksheet sheetDest = null;
        Microsoft.Office.Interop.Excel.Workbook bookSource = null;
        Microsoft.Office.Interop.Excel.Worksheet sheetSource = null;
        string[] _sourceFiles = null;
        string _destFile = string.Empty;
        string _columnEnd = string.Empty;
        int _headerRowCount = 0;
        int _currentRowCount = 1;


        public MergeExcel(string[] sourceFiles, string destFile, string columnEnd, int headerRowCount, string sheetname, System.Data.DataTable dt)
        {
            bookDest = (Microsoft.Office.Interop.Excel.WorkbookClass)app.Workbooks.Add(Missing.Value);

            sheetDest = bookDest.Worksheets.Add() as Microsoft.Office.Interop.Excel.Worksheet;
            sheetDest.Name = sheetname + "1";
            _sourceFiles = sourceFiles;
            _destFile = destFile;
            _columnEnd = columnEnd;
            _headerRowCount = headerRowCount;

            bookSource = app.Workbooks._Open(_sourceFiles[0], Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value);
            int x = bookSource.Worksheets.Count;
            sheetSource = bookSource.Worksheets[1] as Microsoft.Office.Interop.Excel.Worksheet;

            int sheetRowCount = sheetSource.UsedRange.Rows.Count;
            Microsoft.Office.Interop.Excel.Range range = sheetSource.get_Range(string.Format("A{0}", _currentRowCount), _columnEnd + sheetRowCount.ToString());
            range.Copy(sheetDest.get_Range(string.Format("A{0}", _currentRowCount), Missing.Value));
            _currentRowCount += range.Rows.Count;



            bookSource.Close(false, Missing.Value, Missing.Value);

            //
            _currentRowCount = 1;
            sheetDest = bookDest.Worksheets.Add() as Microsoft.Office.Interop.Excel.Worksheet;
            sheetDest.Name = sheetname + "2";
            sheetDest.Activate();
            _sourceFiles = sourceFiles;
            _destFile = destFile;
            _columnEnd = columnEnd;
            _headerRowCount = headerRowCount;

            bookSource = app.Workbooks._Open(_sourceFiles[1], Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value);
            x = bookSource.Worksheets.Count;
            sheetSource = bookSource.Worksheets[1] as Microsoft.Office.Interop.Excel.Worksheet;

            sheetRowCount = sheetSource.UsedRange.Rows.Count;
            range = sheetSource.get_Range(string.Format("A{0}", _currentRowCount), _columnEnd + sheetRowCount.ToString());
            range.Copy(sheetDest.get_Range(string.Format("A{0}", _currentRowCount), Missing.Value));
            _currentRowCount += range.Rows.Count;
            bookSource.Close(false, Missing.Value, Missing.Value);
            //

            _currentRowCount = 1;
            sheetDest = bookDest.Worksheets.Add() as Microsoft.Office.Interop.Excel.Worksheet;
            sheetDest.Name = sheetname + "3";
            sheetDest.Activate();
            _sourceFiles = sourceFiles;
            _destFile = destFile;
            _columnEnd = columnEnd;
            _headerRowCount = headerRowCount;

            bookSource = app.Workbooks._Open(_sourceFiles[2], Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value);
            x = bookSource.Worksheets.Count;
            sheetSource = bookSource.Worksheets[1] as Microsoft.Office.Interop.Excel.Worksheet;

            sheetRowCount = sheetSource.UsedRange.Rows.Count;
            range = sheetSource.get_Range(string.Format("A{0}", _currentRowCount), _columnEnd + sheetRowCount.ToString());
            range.Copy(sheetDest.get_Range(string.Format("A{0}", _currentRowCount), Missing.Value));
            _currentRowCount += range.Rows.Count;
            bookSource.Close(false, Missing.Value, Missing.Value);
            //

            _currentRowCount = 1;
            sheetDest = bookDest.Worksheets.Add() as Microsoft.Office.Interop.Excel.Worksheet;
            sheetDest.Name = sheetname + "4";
            sheetDest.Activate();
            _sourceFiles = sourceFiles;
            _destFile = destFile;
            _columnEnd = columnEnd;
            _headerRowCount = headerRowCount;

            bookSource = app.Workbooks._Open(_sourceFiles[3], Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value);
            x = bookSource.Worksheets.Count;
            sheetSource = bookSource.Worksheets[1] as Microsoft.Office.Interop.Excel.Worksheet;

            sheetRowCount = sheetSource.UsedRange.Rows.Count;
            range = sheetSource.get_Range(string.Format("A{0}", _currentRowCount), _columnEnd + sheetRowCount.ToString());
            range.Copy(sheetDest.get_Range(string.Format("A{0}", _currentRowCount), Missing.Value));
            _currentRowCount += range.Rows.Count;
            bookSource.Close(false, Missing.Value, Missing.Value);
            //

            _currentRowCount = 1;
            sheetDest = bookDest.Worksheets.Add() as Microsoft.Office.Interop.Excel.Worksheet;
            sheetDest.Name = sheetname + "5";
            sheetDest.Activate();
            _sourceFiles = sourceFiles;
            _destFile = destFile;
            _columnEnd = columnEnd;
            _headerRowCount = headerRowCount;

            bookSource = app.Workbooks._Open(_sourceFiles[4], Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value);
            x = bookSource.Worksheets.Count;
            sheetSource = bookSource.Worksheets[1] as Microsoft.Office.Interop.Excel.Worksheet;

            sheetRowCount = sheetSource.UsedRange.Rows.Count;
            range = sheetSource.get_Range(string.Format("A{0}", _currentRowCount), _columnEnd + sheetRowCount.ToString());
            range.Copy(sheetDest.get_Range(string.Format("A{0}", _currentRowCount), Missing.Value));
            _currentRowCount += range.Rows.Count;
            bookSource.Close(false, Missing.Value, Missing.Value);
            //    for (int i = 0; i <= sourceFiles.Length; i++)
            //{

            //}
            //
            //for (int i = 0; i <= dt.Rows.Count; i++)
            //{
            //    sheetDest = bookDest.Worksheets.Add() as Microsoft.Office.Interop.Excel.Worksheet;
            //    string v = i.ToString();
            //    sheetDest.Name = sheetname+v;
            //    sheetDest.Activate();
            //    _destFile = destFile;
            //    _columnEnd = columnEnd;
            //    _headerRowCount = headerRowCount;

            //    bookSource = app.Workbooks._Open(string.Join(", ", dt.Rows[i].ItemArray), Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value);
            //    x = bookSource.Worksheets.Count;
            //    sheetSource = bookSource.Worksheets[1] as Microsoft.Office.Interop.Excel.Worksheet;

            //    sheetRowCount = sheetSource.UsedRange.Rows.Count;
            //    range = sheetSource.get_Range(string.Format("A{0}", _headerRowCount), _columnEnd + sheetRowCount.ToString());
            //    range.Copy(sheetDest.get_Range(string.Format("A{0}", _currentRowCount), Missing.Value));
            //    _currentRowCount += range.Rows.Count;

            //    bookSource.Close(false, Missing.Value, Missing.Value);
            //}
            //
            bookDest.Saved = true;
            bookDest.SaveCopyAs(_destFile);

            app.Quit();
        }
        public static void DoMerge(string[] sourceFiles, string destFile, string columnEnd, int headerRowCount, string sheetname, System.Data.DataTable dt)
        {
            new MergeExcel(sourceFiles, destFile, "ZZ", 2, "Statutory", dt);
        }
    }
}
