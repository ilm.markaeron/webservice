﻿using System;
using System.Web.Http;
using System.Data;
using illimitadoWepAPI.MyClass;
using System.Configuration;
using System.Reflection;
using System.Collections.Generic;
using ExpertXls.ExcelLib;
using System.IO;
using System.Web.Hosting;
using illimitadoWepAPI.Models.WorkforceModel;
using static illimitadoWepAPI.Models.AlphaListModel;
using static illimitadoWepAPI.Models.UserProfile;
using System.Web;
using System.Net.Mail;
using System.Net;
using iTextSharp.text.pdf;
using iTextSharp.text;
using System.Globalization;
using System.Net.Http;
using System.Text.RegularExpressions;
using Excel = Microsoft.Office.Interop.Excel;
using System.Runtime.InteropServices;
using System.Diagnostics;
using static illimitadoWepAPI.Models.WorkforceModel.WorkForceModel;

namespace illimitadoWepAPI.Controllers.WorkforceController
{
    public class WorkForceController : ApiController
    {
        public static string GetDB2(GetDatabase GetDatabase)
        {
            // com.connectionstring = ConfigurationManager.ConnectionStrings["connectionstring"].ConnectionString.ToString();
            var settings = ConfigurationManager.ConnectionStrings["connectionstring"];
            var fi = typeof(ConfigurationElement).GetField(
                          "_bReadOnly",
                          BindingFlags.Instance | BindingFlags.NonPublic);
            fi.SetValue(settings, false);
            settings.ConnectionString = "Data Source=livedb-ilm.c3srcolvclfy.ap-southeast-1.rds.amazonaws.com,1412;Database=ILM_Main;User Id=ILM_LiveDBUser;Password=ILMV3ry53cur3dMGTpa55w0rd2020;";
            if (GetDatabase.ClientName == "ilmvivere"
                || GetDatabase.ClientName == "ilmvisiontv"
                || GetDatabase.ClientName == "ilmayhpi"
                || GetDatabase.ClientName == "ilmbataan2020"
                || GetDatabase.ClientName == "ilmbataan2020-samal")
            {
                Connection MyConnection = new Connection();
                MyConnection.myparameters.Add(new myParameters { ParameterName = "@ClientName", mytype = SqlDbType.NVarChar, Value = GetDatabase.ClientName.Substring(3, GetDatabase.ClientName.Length - 3) });
                DataRow Mydr;
                Mydr = MyConnection.GetSingleRow("sp_CheckClientDB");
                GetDatabase Mydab = new GetDatabase
                {
                    ClientName = Mydr["DB_Settings"].ToString()
                };
                settings.ConnectionString = "Data Source=ilmlive002.c3srcolvclfy.ap-southeast-1.rds.amazonaws.com,1433;Database=" + Mydab.ClientName + ";User Id=ILM_LiveDBUser;Password=ILMpa55w0rd2021;";
            }
            else if (GetDatabase.ClientName == "vivere")
            {
                settings.ConnectionString = "Data Source=ilmlive002.c3srcolvclfy.ap-southeast-1.rds.amazonaws.com,1433;Database=VivereDB;User Id=ILM_LiveDBUser;Password=ILMpa55w0rd2021;";
            }
            else
            {
                #region Original
                Connection Connection = new Connection();
                Connection.myparameters.Add(new myParameters { ParameterName = "@ClientName", mytype = SqlDbType.NVarChar, Value = GetDatabase.ClientName });
                DataRow dr;
                dr = Connection.GetSingleRow("sp_CheckClientDB");

                GetDatabase dab = new GetDatabase
                {
                    ClientName = dr["DB_Settings"].ToString()
                };

                if (dab.ClientName == "ILM_Live")
                {
                    settings.ConnectionString = "Data Source=livedb-ilm.c3srcolvclfy.ap-southeast-1.rds.amazonaws.com,1412;Database=ILM_Live;User Id=ILM_LiveDBUser;Password=ILMV3ry53cur3dMGTpa55w0rd2020;";
                }
                else
                {
                    settings.ConnectionString = "Data Source=livedb-ilm.c3srcolvclfy.ap-southeast-1.rds.amazonaws.com,1412;Database=" + dab.ClientName + ";User Id=ILM_LiveDBUser;Password=ILMV3ry53cur3dMGTpa55w0rd2020;";
                }
                #endregion
            }
            return settings.ToString();
        }
        public void vivereSERVERconnectionstring()
        {
            var settings = ConfigurationManager.ConnectionStrings["connectionstring"];
            var fi = typeof(ConfigurationElement).GetField(
                          "_bReadOnly",
                          BindingFlags.Instance | BindingFlags.NonPublic);
            fi.SetValue(settings, false);
            settings.ConnectionString = "Data Source=ilmlive002.c3srcolvclfy.ap-southeast-1.rds.amazonaws.com,1433; Database = ILM_Main;User Id=ILM_LiveDBUser;Password=ILMpa55w0rd2021;";
        }

        public void tempconnectionstring()
        {
            var settings = ConfigurationManager.ConnectionStrings["connectionstring"];
            var fi = typeof(ConfigurationElement).GetField(
                          "_bReadOnly",
                          BindingFlags.Instance | BindingFlags.NonPublic);
            fi.SetValue(settings, false);
            //--Local--//
            //settings.ConnectionString = "Data Source=192.168.2.33,1444;Database=CRT_Dbase2019-03-29;User Id=DevUser;Password=securedmgt@1230;";
            //--AWS--//
            settings.ConnectionString = "Data Source=livedb-ilm.c3srcolvclfy.ap-southeast-1.rds.amazonaws.com,1412; Database = ILM_Main;User Id=ILM_LiveDBUser;Password=ILMV3ry53cur3dMGTpa55w0rd2020;";

        }
        public void CTtempconnectionstring()
        {
            var settings = ConfigurationManager.ConnectionStrings["connectionstring"];
            var fi = typeof(ConfigurationElement).GetField(
                          "_bReadOnly",
                          BindingFlags.Instance | BindingFlags.NonPublic);
            fi.SetValue(settings, false);
            //--Local--//
            //settings.ConnectionString = "Data Source=192.168.2.33,1444;Database=CRT_Dbase2019-03-29;User Id=DevUser;Password=securedmgt@1230;";
            //--AWS--//
            settings.ConnectionString = "Data Source=livedb-ilm.c3srcolvclfy.ap-southeast-1.rds.amazonaws.com,1412; Database = CTrackDB;User Id=ILM_LiveDBUser;Password=ILMV3ry53cur3dMGTpa55w0rd2020;";

        }

        [HttpPost]
        [Route("WF_AutoLogin")]
        public string WFLoginContainer(autoLogin login)
        {
            //GetDatabase GDB = new GetDatabase();
            //GDB.ClientName = login.companyname;
            tempconnectionstring();

            try
            {
                string valid = "";
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@company", mytype = SqlDbType.NVarChar, Value = login.companyname });
                con.myparameters.Add(new myParameters { ParameterName = "@username", mytype = SqlDbType.NVarChar, Value = login.id });
                valid = con.ExecuteScalar("sp_WM_Insert_tblAutoLogin").ToString();
                return valid;
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }
        [HttpPost]
        [Route("CT_AutoLogin")]
        public string CTLoginContainer(autoLogin login)
        {
            //GetDatabase GDB = new GetDatabase();
            //GDB.ClientName = login.companyname;
            CTtempconnectionstring();

            try
            {
                string valid = "";
                Connection con = new Connection();
              //  con.myparameters.Add(new myParameters { ParameterName = "@company", mytype = SqlDbType.NVarChar, Value = login.companyname });
                con.myparameters.Add(new myParameters { ParameterName = "@username", mytype = SqlDbType.NVarChar, Value = login.id });
                valid = con.ExecuteScalar("sp_autologin").ToString();
                return valid;
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }
        [HttpPost]
        [Route("WF_GetAutoLogin")]
        public List<AutoLoginList> WFGetLoginContainer(autoLogin login)
        {
            //GetDatabase GDB = new GetDatabase();
            //GDB.ClientName = login.companyname;
            List<AutoLoginList> DU = new List<AutoLoginList>();
            tempconnectionstring();


            string valid = "";
            Connection con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@GUID", mytype = SqlDbType.NVarChar, Value = login.UID });
            DataTable DT = con.GetDataTable("sp_WM_CheckAutoLogin");
            foreach (DataRow row in DT.Rows)
            {
                AutoLoginList Users = new AutoLoginList()
                {
                    company = row["company"].ToString(),
                    id = row["id"].ToString(),
                    status = row["status"].ToString(),


                };
                DU.Add(Users);
            }
            return DU;


        }
        [HttpPost]
        [Route("CT_GetAutoLogin")]
        public List<AutoLoginList> CTGetLoginContainer(autoLogin login)
        {
            //GetDatabase GDB = new GetDatabase();
            //GDB.ClientName = login.companyname;
            List<AutoLoginList> DU = new List<AutoLoginList>();
            CTtempconnectionstring();


            string valid = "";
            Connection con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@GUID", mytype = SqlDbType.NVarChar, Value = login.UID });
            DataTable DT = con.GetDataTable("sp_CT_CheckAutoLogin");
            foreach (DataRow row in DT.Rows)
            {
                AutoLoginList Users = new AutoLoginList()
                {
                   // company = row["company"].ToString(),
                    id = row["id"].ToString(),
                    status = row["status"].ToString(),


                };
                DU.Add(Users);
            }
            return DU;


        }

        //added by ren for PerfApp
        [HttpPost]
        [Route("PerfApp_AutoLogin")]
        public string PerfLoginContainer(autoLogin login)
        {
            //GetDatabase GDB = new GetDatabase();
            //GDB.ClientName = login.companyname;
            tempconnectionstring();

            try
            {
                string valid = "";
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@company", mytype = SqlDbType.NVarChar, Value = login.companyname });
                con.myparameters.Add(new myParameters { ParameterName = "@username", mytype = SqlDbType.NVarChar, Value = login.id });
                valid = con.ExecuteScalar("sp_PerfApp_InsertAutoLogin").ToString();
                return valid;
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }

        //added by ren for PerfApp copied from Mark
        [HttpPost]
        [Route("PerfApp_GetAutoLogin")]
        public List<AutoLoginList> PerfGetLoginContainer(autoLogin login)
        {
            //GetDatabase GDB = new GetDatabase();
            //GDB.ClientName = login.companyname;
            List<AutoLoginList> DU = new List<AutoLoginList>();
            tempconnectionstring();


            string valid = "";
            Connection con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@GUID", mytype = SqlDbType.NVarChar, Value = login.UID });
            DataTable DT = con.GetDataTable("sp_PerfApp_CheckAutoLogin");
            foreach (DataRow row in DT.Rows)
            {
                AutoLoginList Users = new AutoLoginList()
                {
                    company = row["company"].ToString(),
                    id = row["id"].ToString(),
                    status = row["status"].ToString(),


                };
                DU.Add(Users);
            }
            return DU;


        }

        [HttpPost]
        [Route("WF_GetLiveMonitor")]
        public List<GetLiveMonitorList> WFLiveMonitor(autoLogin data)
        {



            DataTable filterUser1 = new DataTable();
            DataTable filterUser = new DataTable();
            DataTable Name = new DataTable();




            Name.Columns.AddRange(new DataColumn[1] { new DataColumn("EmpID") });
            for (int i = 0; i < data.EmpID.Length; i++)
            {
                Name.Rows.Add(data.EmpID[i]);



            }



            List<GetLiveMonitorList> DU = new List<GetLiveMonitorList>();
            tempconnectionstring();
            Connection con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@company", mytype = SqlDbType.VarChar, Value = data.companyname });
            con.myparameters.Add(new myParameters { ParameterName = "@EmpName", mytype = SqlDbType.Structured, Value = Name });
            DataTable DT = con.GetDataTable("sp_WM_LiveMonitor");
            foreach (DataRow row in DT.Rows)
            {
                GetLiveMonitorList Users = new GetLiveMonitorList()
                {
                    ReasonDesc = row["ReasonDesc"].ToString(),
                    ReasonID = row["ReasonID"].ToString(),
                    Count = row["Count"].ToString(),



                };
                DU.Add(Users);
            }
            return DU;
        }
        [HttpPost]
        [Route("WF_Manual_Login")]
        public string ManualLogin(manualLogin data)
        {
            try
            {
                string valid;
                tempconnectionstring();
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@company", mytype = SqlDbType.VarChar, Value = data.companyname });
                con.myparameters.Add(new myParameters { ParameterName = "@username", mytype = SqlDbType.VarChar, Value = data.username });
                con.myparameters.Add(new myParameters { ParameterName = "@password", mytype = SqlDbType.VarChar, Value = data.password });
                valid = con.ExecuteScalar("sp_WM_Manual_LogIn").ToString();
                return valid;
            }
            catch (Exception e)
            {
                return "false";
            }

        }

        [HttpPost]
        [Route("WF_ViewLogs")]
        public List<GetViewLogsList> WFViewLogs(viewLogs param)
        {

            List<GetViewLogsList> DU = new List<GetViewLogsList>();
            tempconnectionstring();
            Connection con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@company", mytype = SqlDbType.VarChar, Value = param.company });
            con.myparameters.Add(new myParameters { ParameterName = "@curDate", mytype = SqlDbType.VarChar, Value = param.curDate });
            con.myparameters.Add(new myParameters { ParameterName = "@maxDate", mytype = SqlDbType.VarChar, Value = param.maxDate });
            con.myparameters.Add(new myParameters { ParameterName = "@empID", mytype = SqlDbType.VarChar, Value = param.empID });

            DataTable DT = con.GetDataTable("sp_WM_ViewLogs");
            foreach (DataRow row in DT.Rows)
            {
                GetViewLogsList Users = new GetViewLogsList()
                {
                    Date = row["Date"].ToString(),
                    Day = row["Day"].ToString(),
                    ScheduleType = row["ScheduleType"].ToString(),
                    Shift = row["Shift"].ToString(),
                    Schedule = row["Schedule"].ToString(),
                    StartTime = row["StartTime"].ToString(),
                    EndTime = row["EndTime"].ToString(),
                    ReasonID = row["ReasonID"].ToString(),
                    Activity = row["Activity"].ToString(),
                    TotalWorkedHours = row["TotalWorkedHours"].ToString(),
                    ApproverStatus = row["ApproverStatus"].ToString(),
                    UserStatus = row["UserStatus"].ToString(),

                };
                DU.Add(Users);
            }
            return DU;
        }

        //03/12/2019
        [HttpPost]
        [Route("WM_EmpNameList")]
        public List<FilterName> WFNameList(SearchFilterParams param)
        {

            List<FilterName> DU = new List<FilterName>();
            if (param.company.ToLower() == "vivere")
            {
                vivereSERVERconnectionstring();
            }
            else
            {
                tempconnectionstring();
            }            
            Connection con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@company", mytype = SqlDbType.VarChar, Value = param.company });
            
            DataTable DT = con.GetDataTable("sp_WM_EmpNameList");
            foreach (DataRow row in DT.Rows)
            {
                FilterName NameFilter = new FilterName()
                {
                    EmpID = row["EmpID"].ToString(),
                    EmployeeName = row["EmployeeName"].ToString(),
                    MngrID = row["MngrID"].ToString(),
                    EmpLevel = row["EmpLevel"].ToString(),
                    BusinessSegment = row["BusinessSegment"].ToString(),
                    BusinessUnit = row["BusinessUnit"].ToString(),
                };
                DU.Add(NameFilter);
            }
            return DU;
        }

        [HttpPost]
        [Route("WM_EmpLevelList")]
        public List<FilterLevel> WFEmpLevelList(SearchFilterParams param)
        {

            List<FilterLevel> DU = new List<FilterLevel>();
            tempconnectionstring();
            Connection con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@company", mytype = SqlDbType.VarChar, Value = param.company });

            DataTable DT = con.GetDataTable("sp_WM_EmpLevelList");
            foreach (DataRow row in DT.Rows)
            {
                FilterLevel LevelFilter = new FilterLevel()
                {
                    EmpLevel = row["EmpLevel"].ToString(),
                    BusinessUnit = row["BusinessUnit"].ToString()
                };
                DU.Add(LevelFilter);
            }
            return DU;
        }
        [HttpPost]
        [Route("WM_ISList")]
        public List<ISLevel> WFISList(SearchFilterParams param)
        {

            List<ISLevel> DU = new List<ISLevel>();
            tempconnectionstring();
            Connection con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@company", mytype = SqlDbType.VarChar, Value = param.company });

            DataTable DT = con.GetDataTable("sp_WM_ISList");
            foreach (DataRow row in DT.Rows)
            {
                ISLevel ISFilter = new ISLevel()
                {
                    ImmediateSupervisor = row["ImmediateSupervisor"].ToString(),
                    EmpName = row["EmpName"].ToString(),
                    EmpLevel = row["EmpLevel"].ToString(),
                    BusinessUnit = row["BusinessUnit"].ToString(),
                    BusinessSegment = row["BusinessSegment"].ToString()
                };
                DU.Add(ISFilter);
            }
            return DU;
        }

        [HttpPost]
        [Route("WM_BusinessUnitList")]
        public List<FilterBusinessUnit> WFBUL(SearchFilterParams param)
        {

            List<FilterBusinessUnit> DU = new List<FilterBusinessUnit>();
            tempconnectionstring();
            Connection con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@company", mytype = SqlDbType.VarChar, Value = param.company });

            DataTable DT = con.GetDataTable("sp_WM_BusinessUnitList");
            foreach (DataRow row in DT.Rows)
            {
                FilterBusinessUnit BUnit = new FilterBusinessUnit()
                {
                    BusinessUnit = row["BusinessUnit"].ToString(),
                    BusinessSegment = row["BusinessSegment"].ToString(),
         
                };
                DU.Add(BUnit);
            }
            return DU;
        }

        [HttpPost]
        [Route("WM_BusinessSegmentList")]
        public List<FilterBusinessSegment> WFBSegment(SearchFilterParams param)
        {

            List<FilterBusinessSegment> DU = new List<FilterBusinessSegment>();
            tempconnectionstring();
            Connection con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@company", mytype = SqlDbType.VarChar, Value = param.company });

            DataTable DT = con.GetDataTable("sp_WM_BusinessSegmentList");
            foreach (DataRow row in DT.Rows)
            {
                FilterBusinessSegment BSegment = new FilterBusinessSegment()
                {
                    BusinessSegment = row["BusinessSegment"].ToString(),
               

                };
                DU.Add(BSegment);
            }
            return DU;
        }
       

        [HttpPost]
        [Route("WM_SearchPanel")]
        public List<SearchPanelReturns> WFSearchPanel(SearchPanelParams param)
        {
            DataTable Name = new DataTable();
            DataTable EmpLevel = new DataTable();
            DataTable ISLIst = new DataTable();
            DataTable BusinessUnit = new DataTable();
            DataTable BusinessSegment = new DataTable();

            Name.Columns.AddRange(new DataColumn[2] { new DataColumn("EmpID"), new DataColumn("EmpName") });
            EmpLevel.Columns.AddRange(new DataColumn[1] { new DataColumn("EmpLevel") });
            ISLIst.Columns.AddRange(new DataColumn[1] { new DataColumn("ISLIst") });
            BusinessUnit.Columns.AddRange(new DataColumn[1] { new DataColumn("BusinessUnit") });
            BusinessSegment.Columns.AddRange(new DataColumn[1] { new DataColumn("BusinessSegment") });

            for (int i = 0; i < param.EmpID.Length; i++)
            {
                Name.Rows.Add(param.EmpID[i], param.EmpName[i]);

            }
            for (int i = 0; i < param.EmpLevel.Length; i++)
            {
                EmpLevel.Rows.Add(param.EmpLevel[i]);
            }
            for (int i = 0; i < param.ISLIst.Length; i++)
            {
                ISLIst.Rows.Add(param.ISLIst[i]);
            }
            for (int i = 0; i < param.BusinessUnit.Length; i++)
            {
                BusinessUnit.Rows.Add(param.BusinessUnit[i]);
            }
            for (int i = 0; i < param.BusinessSegment.Length; i++)
            {
                BusinessSegment.Rows.Add(param.BusinessSegment[i]);
            }
            List<SearchPanelReturns> SPR = new List<SearchPanelReturns>();
            tempconnectionstring();
            Connection con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@company", mytype = SqlDbType.VarChar, Value = param.company });
            con.myparameters.Add(new myParameters { ParameterName = "@EmpName", mytype = SqlDbType.Structured, Value = Name });
            con.myparameters.Add(new myParameters { ParameterName = "@EmpLevel", mytype = SqlDbType.Structured, Value = EmpLevel });
            con.myparameters.Add(new myParameters { ParameterName = "@IS", mytype = SqlDbType.Structured, Value = ISLIst });
            con.myparameters.Add(new myParameters { ParameterName = "@BUnit", mytype = SqlDbType.Structured, Value = BusinessUnit });
            con.myparameters.Add(new myParameters { ParameterName = "@BSegment", mytype = SqlDbType.Structured, Value = BusinessSegment });
            DataTable DT = con.GetDataTable("sp_WM_SearchPanel");
            foreach (DataRow row in DT.Rows)
            {
                SearchPanelReturns Users = new SearchPanelReturns()
                {
                    SeriesID = row["SeriesID"].ToString(),
                    EmpID = row["EmpID"].ToString(),
                    Scheddate = row["Scheddate"].ToString(),
                    Schedule = row["Schedule"].ToString(),
                    SeriesIDb = row["SeriesIDb"].ToString(),
                    EmpIDb = row["EmpIDb"].ToString(),
                    Start_Time = row["Start_Time"].ToString(),
                    End_Time = row["End_Time"].ToString(),
                    Activity_Status = row["Activity_Status"].ToString(),
                    ReasonID = row["ReasonID"].ToString()
                };
                SPR.Add(Users);
            }
            return SPR;
        }

        //03/12/2019

        //04/12/2019
        [HttpPost]
        [Route("WM_SearchPanelV2")]
        public List<SearchPanelReturnsV2> WFSearchPanel(SearchPanelParamsV2 param)
        {
            DataTable Name = new DataTable();


            Name.Columns.AddRange(new DataColumn[2] { new DataColumn("EmpID"), new DataColumn("EmpName") });

            for (int i = 0; i < param.EmpID.Length; i++)
            {
                Name.Rows.Add(param.EmpID[i], param.EmpName[i]);

            }

            List<SearchPanelReturnsV2> SPR = new List<SearchPanelReturnsV2>();
            tempconnectionstring();
            Connection con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@company", mytype = SqlDbType.VarChar, Value = param.company });
            con.myparameters.Add(new myParameters { ParameterName = "@EmpName", mytype = SqlDbType.Structured, Value = Name });
            DataTable DT = con.GetDataTable("sp_WM_SearchPanelV2");
            foreach (DataRow row in DT.Rows)
            {
                SearchPanelReturnsV2 Users = new SearchPanelReturnsV2()
                {

                    EmpID = row["EmpID"].ToString(),
                    EmpName = row["EmpName"].ToString(),
                    ImmediateSupervisor = row["ImmediateSupervisor"].ToString(),
                    ScheduleType = row["ScheduleType"].ToString(),
                    SchedIN = row["SchedIN"].ToString(),
                    SchedOUT = row["SchedOUT"].ToString(),
                    Status = row["Status"].ToString(),
                    BusinessSegment = row["BusinessSegment"].ToString(),
                    BusinessUnit = row["BusinessUnit"].ToString(),
                    EmpLevel = row["EmpLevel"].ToString(),
                    Activity_Status = row["Activity_Status"].ToString(),
                    ReasonID = row["ReasonID"].ToString()
                };
                SPR.Add(Users);
            }
            return SPR;
        }

        [HttpPost]
        [Route("WM_SearchPanelV3")]
        public List<SearchPanelReturnsV2> WFSearchPanelv3(SearchPanelParamsV2 param)
        {
            DataTable Name = new DataTable();


            Name.Columns.AddRange(new DataColumn[2] { new DataColumn("EmpID"), new DataColumn("EmpName") });

            for (int i = 0; i < param.EmpID.Length; i++)
            {
                Name.Rows.Add(param.EmpID[i], param.EmpName[i]);

            }

            List<SearchPanelReturnsV2> SPR = new List<SearchPanelReturnsV2>();
            tempconnectionstring();
            Connection con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@company", mytype = SqlDbType.VarChar, Value = param.company });
            con.myparameters.Add(new myParameters { ParameterName = "@EmpName", mytype = SqlDbType.Structured, Value = Name });
            DataTable DT = con.GetDataTable("sp_WM_SearchPanelV3");
            foreach (DataRow row in DT.Rows)
            {
                SearchPanelReturnsV2 Users = new SearchPanelReturnsV2()
                {

                    EmpID = row["EmpID"].ToString(),
                    EmpName = row["EmpName"].ToString(),
                    ImmediateSupervisor = row["ImmediateSupervisor"].ToString(),
                    ScheduleType = row["ScheduleType"].ToString(),
                    SchedIN = row["SchedIN"].ToString(),
                    SchedOUT = row["SchedOUT"].ToString(),
                    Status = row["Status"].ToString(),
                    Status2 = row["Status2"].ToString(),
                    Status1 = row["Status1"].ToString(),
                    BusinessSegment = row["BusinessSegment"].ToString(),
                    BusinessUnit = row["BusinessUnit"].ToString(),
                    EmpLevel = row["EmpLevel"].ToString(),
                    Activity_Status = row["Activity_Status"].ToString(),
                    ReasonID = row["ReasonID"].ToString(),
                    EmpStatus = row["EmpStatus"].ToString(),
                    StartTime = row ["StartTime"].ToString()



                };
                SPR.Add(Users);
            }
            return SPR;
        }
        [HttpPost]
        [Route("WM_UpdateEmpStatus")]
        public string WM_UpdateEmpStatus(EmployeeStatus param)
        {
            try
            {
                GetDatabase GDB = new GetDatabase();
                GDB.ClientName = param.CN;
                GetDB2(GDB);

                DataTable DT = new DataTable();
                DT.Columns.AddRange(new DataColumn[1] { new DataColumn("EmpID") });
                for (int i = 0; i < param.EmpID.Length; i++)
                {
                    DT.Rows.Add(param.EmpID[i]);
                }

                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.Structured, Value = DT });
                con.myparameters.Add(new myParameters { ParameterName = "@EmpStatus", mytype = SqlDbType.VarChar, Value = param.EmpStatus });
                con.ExecuteNonQuery("sp_WM_UpdateEmpStatus");
                return "Success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }
        [HttpPost]
        [Route("WM_getLabelName")]
        public string getLabel(manualLogin data)
        {
            try
            {
                string valid;
                tempconnectionstring();
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@company", mytype = SqlDbType.VarChar, Value = data.companyname });

                valid = con.ExecuteScalar("sp_WM_getLabelName").ToString();
                return valid;
            }
            catch (Exception e)
            {
                return "false";
            }

        }

        [HttpPost]
        [Route("get_WMLabel")]
        public string wmlabel(RetrieveLabel prms)
        {
            try
            {
                string valid;
                tempconnectionstring();
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@company", mytype = SqlDbType.VarChar, Value = prms.CN });

                valid = con.ExecuteScalar("sp_WM_RetrieveLabel").ToString();
                return valid;
            }
            catch (Exception e)
            {
                return "false";
            }

        }
    }

}

