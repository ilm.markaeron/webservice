﻿using illimitadoWepAPI.Models;
using illimitadoWepAPI.MyClass;
using Microsoft.ProjectOxford.Face;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Hosting;
using System.Web.Http;
using Kairos.Net;


namespace illimitadoWepAPI.Controllers
{
    public class FaceRecognitionController : ApiController
    {

        [HttpPost]
        [Route("RegisterFaceRecognition")]
        public async Task<string> RegisterFaceRecognition(FaceRecognition selfieReg)
        {
            string empid = selfieReg.EMPID;
            string image = selfieReg.IMAGE;

            KairosClient client = new KairosClient();

            client.ApplicationID = "56302ff1";
            client.ApplicationKey = "727663717176360ed1825e958f5c3d00";

            try
            {
                 // Get the image and face information
                var enrollResponse = client.Enroll(image, empid, "Illimitado", "FULL");

                // Get the user enrollment transaction info
                var userImage = enrollResponse.Images[0].Transaction;

                // Enroll the user
                return userImage.subject_id;
            }
            catch
            {
                return "null";
            }
        }

        [HttpPost]
        [Route("RecognizeFaceRecognition")]
        public async Task<string> RecognizeFaceRecognition(FaceRecognition selfieReg)
        {
            string empid = selfieReg.EMPID;
            string image = selfieReg.IMAGE;

            KairosClient client = new KairosClient();

            client.ApplicationID = "56302ff1";
            client.ApplicationKey = "727663717176360ed1825e958f5c3d00";

            try
            {
                var enrollResponse = client.Recognize(image, "rekognize");

                // Get the user enrollment transaction info
                var userImage = enrollResponse.Images[0].Transaction;

                // Enroll the user
                return userImage.subject_id;
            }
            catch
            {
                return "null";
            }
        }
    }
}
