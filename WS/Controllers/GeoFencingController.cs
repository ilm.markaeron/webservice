﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using illimitadoWepAPI.Models;
using illimitadoWepAPI.MyClass;
using System.Data;

namespace illimitadoWepAPI.Controllers
{
    public class GeoFencingController : ApiController
    {
        [HttpPost]
        [Route("LoadBranchDetails")]
        public List<BranchDetails> LoadBranchDetails(ClientDetails CD)
        {
            List<BranchDetails> GetBranchList = new List<BranchDetails>();

            try
            {
                GetDatabase GDB = new GetDatabase();
                GDB.ClientName = CD.ClientName;
                SelfieRegistration2Controller.GetDB2(GDB);

                Connection con = new Connection();
                DataTable dt = con.GetDataTable("sp_GetBranchName");
                foreach (DataRow item in dt.Rows)
                {
                    BranchDetails Get = new BranchDetails()
                    {
                        BranchID = item["ID"].ToString(),
                        BranchName = item["OfficeName"].ToString()
                    };
                    GetBranchList.Add(Get);
                }
                    
                
            }
            catch (Exception e)
            {
                GetBranchList = null;
            }

            return GetBranchList;
        }

        [HttpPost]
        [Route("LoadPerimeterDetails")]
        public List<PerimeterDetails> LoadPerimeterDetails(ClientDetails CD)
        {
            List<PerimeterDetails> GetPerimeterList = new List<PerimeterDetails>();

            try
            {
                GetDatabase GDB = new GetDatabase();
                GDB.ClientName = CD.ClientName;
                SelfieRegistration2Controller.GetDB2(GDB);

                Connection con = new Connection();
                DataTable dt = con.GetDataTable("spV_ShowPerimeterDetails");
                foreach (DataRow item in dt.Rows)
                {
                    PerimeterDetails PD = new PerimeterDetails()
                    {
                        RowID = item["RowID"].ToString(),
                        BranchName = item["OfficeName"].ToString(),
                        Latitude = item["Latitude"].ToString(),
                        Longitude = item["Longitude"].ToString(),
                        Range = item["Range"].ToString(),
                        ModifiedBy = item["ModifiedBy"].ToString(),
                        ModifiedDate = item["ModifiedDate"].ToString()

                    };
                    GetPerimeterList.Add(PD);
                }

                return GetPerimeterList;
            }
            catch (Exception e)
            {
                GetPerimeterList = null;
            }

            return GetPerimeterList;
        }
        [HttpPost]
        [Route("SavePerimeterDetails")]
        public string SavePerimeterDetails(PerimeterDetails PD)
        {
            string result;
            try
            {
                GetDatabase GDB = new GetDatabase();
                GDB.ClientName = PD.ClientName;
                SelfieRegistration2Controller.GetDB2(GDB);

                Connection con = new Connection();
                con.myparameters.Clear();
                con.myparameters.Add(new myParameters { ParameterName = "@RowID", mytype = SqlDbType.Int, Value = PD.RowID });
                con.myparameters.Add(new myParameters { ParameterName = "@BranchID", mytype = SqlDbType.VarChar, Value = PD.BranchName });
                con.myparameters.Add(new myParameters { ParameterName = "@Latitude", mytype = SqlDbType.VarChar, Value = PD.Latitude });
                con.myparameters.Add(new myParameters { ParameterName = "@Longitude", mytype = SqlDbType.VarChar, Value = PD.Longitude });
                con.myparameters.Add(new myParameters { ParameterName = "@Range", mytype = SqlDbType.VarChar, Value = PD.Range });
                con.myparameters.Add(new myParameters { ParameterName = "@Empid", mytype = SqlDbType.VarChar, Value = PD.EmpID });
                result = con.ExecuteScalar("spV_InsertPerimeterDetails");

            }
            catch (Exception ex)
            {
                result = ex.Message;
            }
            return result;
        }

        [HttpPost]
        [Route("DeletePerimeterDetails")]
        public string DeletePerimeterDetails(PerimeterDetails PD)
        {
            string result;
            try
            {
                GetDatabase GDB = new GetDatabase();
                GDB.ClientName = PD.ClientName;
                SelfieRegistration2Controller.GetDB2(GDB);

                Connection con = new Connection();
                con.myparameters.Clear();
                con.myparameters.Add(new myParameters { ParameterName = "@RowID", mytype = SqlDbType.Int, Value = PD.RowID });
                result = con.ExecuteScalar("sp_VDeletePerimeterDetails");
            }
            catch (Exception ex)
            {
                result = ex.Message;
            }
            return result;
        }
        [HttpPost]
        [Route("GetPerimeterDetails")]
        public List<PerimeterDetails> GetPerimeterDetails(PerimeterDetails PD)
        {
            List<PerimeterDetails> GetPerimeterList = new List<PerimeterDetails>();

            try
            {
                GetDatabase GDB = new GetDatabase();
                GDB.ClientName = PD.ClientName;
                SelfieRegistration2Controller.GetDB2(GDB);

                Connection con = new Connection();
                con.myparameters.Clear();
                con.myparameters.Add(new myParameters { ParameterName = "@RowID", mytype = SqlDbType.Int, Value = PD.RowID });
                DataTable dt = con.GetDataTable("spV_GetPerimeterDetails");
                foreach (DataRow item in dt.Rows)
                {
                    PerimeterDetails PD2 = new PerimeterDetails()
                    {
                        RowID = item["RowID"].ToString(),
                        BranchName = item["OfficeName"].ToString(),
                        Latitude = item["Latitude"].ToString(),
                        Longitude = item["Longitude"].ToString(),
                        Range = item["Range"].ToString()

                    };
                    GetPerimeterList.Add(PD2);
                }
            }
            catch (Exception e)
            {
                GetPerimeterList = null;
            }

            return GetPerimeterList;
        }
    }
}
