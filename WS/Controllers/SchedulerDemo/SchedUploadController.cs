﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using illimitadoWepAPI.SchedulerPortalClass;
using illimitadoWepAPI.Models.ScheduleDemoModels;
using illimitadoWepAPI.Controllers.SchedulerDemo;
using System.Data;

namespace illimitadoWepAPI.Controllers.SchedulerDemo
{
    public class SchedUploadController : ApiController
    {
        //[HttpPost]
        //[Route("getbsegment")]
        //public List<businessSegments> getBusinessSegment()
        //{
        //    Connection con = new Connection();
        //    DataTable dt = new DataTable();
        //    dt = con.GetDataTable("sp_GetBusinessSegment");
        //    List<businessSegments> final = new List<businessSegments>();
        //    for (int i = 0; i < dt.Rows.Count; i++)
        //    {
        //        businessSegments temp = new businessSegments();
        //        temp.businessSegment = dt.Rows[i]["SubSubject"].ToString();

        //        final.Add(temp);
        //    }
        //    return final;
        //}

        //[HttpPost]
        //[Route("getbUnit")]
        //public List<businessUnits> businessUnits()
        //{
        //    Connection con = new Connection();
        //    DataTable dt = new DataTable();
        //    dt = con.GetDataTable("sp_GetBusinessUnit");
        //    List<businessUnits> final = new List<businessUnits>();
        //    for (int i = 0; i < dt.Rows.Count; i++)
        //    {
        //        businessUnits temp = new businessUnits();
        //        temp.businessUnit = dt.Rows[i]["SubSubject"].ToString();

        //        final.Add(temp);
        //    }
        //    return final;
        //}

        //[HttpPost]
        //[Route("getEmployee")]
        //public List<employees> getEmployee()
        //{
        //    Connection con = new Connection();
        //    DataTable dt = new DataTable();
        //    dt = con.GetDataTable("sp_GetAllEmpFullName");
        //    List<employees> final = new List<employees>();
        //    for (int i = 0; i < dt.Rows.Count; i++)
        //    {
        //        employees temp = new employees();
        //        temp.empName = dt.Rows[i]["FullName"].ToString();
        //        temp.empUID = dt.Rows[i]["UserID"].ToString();

        //        final.Add(temp);
        //    }
        //    return final;
        //}

        static DataTable dtsched = new DataTable();
        //[HttpPost]
        //[Route("getSelected")]
        //public void getSelected(List<emp> user)
        //{
        //    dtsched.Clear();
        //    string all = "";
        //    if (dtsched.Columns.Count < 1)
        //    {
        //        dtsched.Columns.Add("Empid", typeof(string));
        //    }
        //    for (int oo = 0; oo < user.Count; oo++)
        //    {
        //        DataRow colof = dtsched.NewRow();
        //        colof["Empid"] = user[oo].empUID.ToString();
        //        all += user[oo].empUID.ToString() + "-";
        //        dtsched.Rows.Add(colof);
        //    }
        //}

        //public void getList(srch search)
        //{
        //    Connection con = new Connection();
        //    con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = search.UserID });
        //    con.myparameters.Add(new myParameters { ParameterName = "@SCHEDDATEFROM", mytype = SqlDbType.NVarChar, Value = Convert.ToDateTime(search.SchedFrom).ToString("yyyy-MM-dd") });
        //    con.myparameters.Add(new myParameters { ParameterName = "@SCHEDDATETO", mytype = SqlDbType.NVarChar, Value = Convert.ToDateTime(search.SchedTo).ToString("yyyy-MM-dd") });
        //    con.ExecuteNonQuery("sp_SearchSchedEmpName");
        //}
        [HttpPost]
        [Route("SD_SchedulerPortalListSimulator")]
        // Para sa display ng dropdowns, mga parameter values: 
        // 0 = lahat ng SubSbujects; 
        // 1 = lahat ng subsubject ng Title, 
        // 2 = lahat ng subsubject ng Role, 
        // 3 = lahat ng subsubject ng Area, 
        // 4 = lahat ng subsubject ng Branch, 
        // 5 = lahat ng subsubject ng Department, 
        // 6 = lahat ng subsubject ng SubDepartment
        public subsubjects ListSimulator(subjectlist sl)
        {
            try
            {
                Connection con = new Connection();
                subsubjects subsubjects = new subsubjects();
                List<subsubjectlist> list = new List<subsubjectlist>();
                con.myparameters.Add(new myParameters { ParameterName = "@SUBJECTID", mytype = SqlDbType.NVarChar, Value = sl._SID });
                con.myparameters.Add(new myParameters { ParameterName = "@PCLient_ID", mytype = SqlDbType.NVarChar, Value = sl.ClientID });
                DataTable dt = con.GetDataTable("sp_SD_SubSubjectSimulator");

                foreach (DataRow row in dt.Rows)
                {
                    subsubjectlist param = new subsubjectlist()
                    {
                        _RowID = row["RowID"].ToString(),
                        _Subject = row["SubSubject"].ToString()
                    };
                    list.Add(param);
                }
                subsubjects.subsubjectlist = list;
                subsubjects._myreturn = "Success";
                return subsubjects;
            }
            catch (Exception e)
            {
                subsubjects subsubjects = new subsubjects();
                List<subsubjectlist> list = new List<subsubjectlist>();
                subsubjects.subsubjectlist = list;
                subsubjects._myreturn = e.ToString();
                return subsubjects;
            }
        }
        //[HttpPost]
        //[Route("getSched")]
        //public List<schedule> getSched(srch search)
        //{
        //    Connection con = new Connection();
        //    DataTable dt = new DataTable();
        //    DataTable TermData = new DataTable();
        //    TermData.Columns.AddRange(new DataColumn[1] { new DataColumn("EmpID") });
        //    for (int i = 0; i < search.UID.Length; i++)
        //    {
        //        TermData.Rows.Add(search.UID[i]);
        //    }
        //    con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.Structured, Value = TermData });
        //    con.myparameters.Add(new myParameters { ParameterName = "@SCHEDDATEFROM", mytype = SqlDbType.NVarChar, Value = search.SchedFrom });
        //    con.myparameters.Add(new myParameters { ParameterName = "@SCHEDDATETO", mytype = SqlDbType.NVarChar, Value = search.SchedTo });
        //    dt = con.GetDataTable("sp_SearchSchedEmpName");
        //    List<schedule> final = new List<schedule>();
        //    for (int i = 0; i < dt.Rows.Count; i++)
        //    {
        //        schedule temp = new schedule();
        //        temp.SeriesID = dt.Rows[i]["SeriesID"].ToString();
        //        temp.Department = dt.Rows[i]["Department"].ToString();
        //        temp.SubDepartment = dt.Rows[i]["SubDepartment"].ToString();
        //        temp.EmpName = dt.Rows[i]["EmpName"].ToString();
        //        temp.UserID = dt.Rows[i]["UserID"].ToString();
        //        temp.CutOff_From = dt.Rows[i]["CutOff_From"].ToString();
        //        temp.CutOff_To = dt.Rows[i]["CutOff_To"].ToString();
        //        temp.Date = dt.Rows[i]["Date"].ToString();
        //        temp.Day = dt.Rows[i]["Day"].ToString();
        //        temp.Schedule = dt.Rows[i]["Schedule"].ToString();
        //        temp.SchedIN = dt.Rows[i]["SchedIN"].ToString();
        //        temp.Break1dsply = dt.Rows[i]["Break1dsply"].ToString();
        //        temp.Lunch = dt.Rows[i]["Lunch"].ToString();
        //        temp.Break2dsply = dt.Rows[i]["Break2dsply"].ToString();
        //        temp.Break3dsply = dt.Rows[i]["Break3dsply"].ToString();
        //        temp.SchedOUT = dt.Rows[i]["SchedOUT"].ToString();
        //        temp.SchedDate = dt.Rows[i]["SchedDate"].ToString();
        //        temp.LunchEnd = dt.Rows[i]["LunchEnd"].ToString();
        //        temp.Break1Minutes = dt.Rows[i]["Break1Minutes"].ToString();
        //        temp.Break2Minutes = dt.Rows[i]["Break2Minutes"].ToString();
        //        temp.Break3Minutes = dt.Rows[i]["Break3Minutes"].ToString();
        //        final.Add(temp);
        //    }
        //    return final;
        //}
        //[HttpPost]
        //[Route("AddRetrieveSched")]
        //public string AddRetrieveSched(srch search)
        //{
        //    try
        //    {
        //        Connection con = new Connection();
        //        DataTable TermData = new DataTable();
        //        TermData.Columns.AddRange(new DataColumn[1] { new DataColumn("EmpID") });
        //        for (int i = 0; i < search.UID.Length; i++)
        //        {
        //            TermData.Rows.Add(search.UID[i]);
        //        }
        //        con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.Structured, Value = TermData });
        //        con.myparameters.Add(new myParameters { ParameterName = "@SCHEDDATEFROM", mytype = SqlDbType.NVarChar, Value = search.SchedFrom });
        //        con.myparameters.Add(new myParameters { ParameterName = "@SCHEDDATETO", mytype = SqlDbType.NVarChar, Value = search.SchedTo });
        //        DataTable DT = new DataTable();
        //        con.ExecuteNonQuery("sp_AddEmployeeSchedbyName");
        //        return "s";
        //    }
        //    catch (Exception e)
        //    {
        //        return "error" + e;
        //    }
        //}
        [HttpPost]
        [Route("AddRetrieveSched")]
        public string AddRetrieveSched(srch search)
        {
            try
            {
                Connection con = new Connection();
                DataTable empIds = new DataTable();
                empIds.Columns.AddRange(new DataColumn[1] { new DataColumn("EmpID") });
                for (int i = 0; i < search.UID.Length; i++)
                {
                    empIds.Rows.Add(search.UID[i]);
                }
                DataTable DeptIds = new DataTable();
                DeptIds.Columns.AddRange(new DataColumn[1] { new DataColumn("DeptIds") });
                for (int i = 0; i < search.Department.Length; i++)
                {
                    DeptIds.Rows.Add(search.Department[i]);
                }
                DataTable subDeptIds = new DataTable();
                subDeptIds.Columns.AddRange(new DataColumn[1] { new DataColumn("subDeptIds") });
                for (int i = 0; i < search.SubDepartment.Length; i++)
                {
                    subDeptIds.Rows.Add(search.SubDepartment[i]);
                }
                con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.Structured, Value = empIds });
                con.myparameters.Add(new myParameters { ParameterName = "@DeptIds", mytype = SqlDbType.Structured, Value = DeptIds });
                con.myparameters.Add(new myParameters { ParameterName = "@subDeptIds", mytype = SqlDbType.Structured, Value = subDeptIds });
                con.myparameters.Add(new myParameters { ParameterName = "@SCHEDDATEFROM", mytype = SqlDbType.NVarChar, Value = search.SchedFrom });
                con.myparameters.Add(new myParameters { ParameterName = "@SCHEDDATETO", mytype = SqlDbType.NVarChar, Value = search.SchedTo });
                con.myparameters.Add(new myParameters { ParameterName = "@PCLient_ID", mytype = SqlDbType.NVarChar, Value = search.ClientID });
                con.ExecuteNonQuery("sp_AddEmployeeSchedbyName");
                return "True";
            }
            catch (Exception e)
            {
                return "error" + e;
            }
        }
        [HttpPost]
        [Route("getSched")]
        public List<schedule> getSched(srch search)
        {
            Connection con = new Connection();
            DataTable dt = new DataTable();
            DataTable empIds = new DataTable();
            empIds.Columns.AddRange(new DataColumn[1] { new DataColumn("EmpID") });
            for (int i = 0; i < search.UID.Length; i++)
            {
                empIds.Rows.Add(search.UID[i]);
            }
            DataTable DeptIds = new DataTable();
            DeptIds.Columns.AddRange(new DataColumn[1] { new DataColumn("DeptIds") });
            for (int i = 0; i < search.Department.Length; i++)
            {
                DeptIds.Rows.Add(search.Department[i]);
            }
            DataTable subDeptIds = new DataTable();
            subDeptIds.Columns.AddRange(new DataColumn[1] { new DataColumn("subDeptIds") });
            for (int i = 0; i < search.SubDepartment.Length; i++)
            {
                subDeptIds.Rows.Add(search.SubDepartment[i]);
            }
            con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.Structured, Value = empIds });
            con.myparameters.Add(new myParameters { ParameterName = "@DeptIds", mytype = SqlDbType.Structured, Value = DeptIds });
            con.myparameters.Add(new myParameters { ParameterName = "@subDeptIds", mytype = SqlDbType.Structured, Value = subDeptIds });
            con.myparameters.Add(new myParameters { ParameterName = "@SCHEDDATEFROM", mytype = SqlDbType.NVarChar, Value = search.SchedFrom });
            con.myparameters.Add(new myParameters { ParameterName = "@SCHEDDATETO", mytype = SqlDbType.NVarChar, Value = search.SchedTo });
            con.myparameters.Add(new myParameters { ParameterName = "@PCLient_ID", mytype = SqlDbType.NVarChar, Value = search.ClientID });
            dt = con.GetDataTable("sp_SearchSchedEmpName");
            List<schedule> final = new List<schedule>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                schedule temp = new schedule();
                temp.SeriesID = dt.Rows[i]["SeriesID"].ToString();
                temp.Department = dt.Rows[i]["Department"].ToString();
                temp.SubDepartment = dt.Rows[i]["SubDepartment"].ToString();
                temp.EmpName = dt.Rows[i]["EmpName"].ToString();
                temp.UserID = dt.Rows[i]["UserID"].ToString();
                temp.CutOff_From = dt.Rows[i]["CutOff_From"].ToString();
                temp.CutOff_To = dt.Rows[i]["CutOff_To"].ToString();
                temp.Date = dt.Rows[i]["Date"].ToString();
                temp.Day = dt.Rows[i]["Day"].ToString();
                temp.Schedule = dt.Rows[i]["Schedule"].ToString();
                temp.SchedIN = dt.Rows[i]["SchedIN"].ToString();
                temp.Break1dsply = dt.Rows[i]["Break1dsply"].ToString();
                temp.Lunch = dt.Rows[i]["Lunch"].ToString();
                temp.Break2dsply = dt.Rows[i]["Break2dsply"].ToString();
                temp.Break3dsply = dt.Rows[i]["Break3dsply"].ToString();
                temp.SchedOUT = dt.Rows[i]["SchedOUT"].ToString();
                temp.SchedDate = dt.Rows[i]["SchedDate"].ToString();
                temp.LunchEnd = dt.Rows[i]["LunchEnd"].ToString();
                temp.Break1Minutes = dt.Rows[i]["Break1Minutes"].ToString();
                temp.Break2Minutes = dt.Rows[i]["Break2Minutes"].ToString();
                temp.Break3Minutes = dt.Rows[i]["Break3Minutes"].ToString();
                temp.MonthName = dt.Rows[i]["MonthName"].ToString();
                temp.BreakType = dt.Rows[i]["BreakType"].ToString();
                final.Add(temp);
            }
            return final;
        }
        [HttpPost]
        [Route("SD_Nationality")]
        public List<SDGetNationality> SD_Nationality(srch search)
        {
            //GetDatabase GDB = new GetDatabase();
            //GDB.ClientName = login.CN;
            //tempconnectionstring();
            Connection con = new Connection();
            DataTable DT = new DataTable();
            DT = con.GetDataTable("sp_SD_Nationality");
            List<SDGetNationality> ListReturned = new List<SDGetNationality>();
            if (DT.Rows.Count > 0)
            {
                foreach (DataRow row in DT.Rows)
                {
                    SDGetNationality Displaynationality = new SDGetNationality
                    {
                        Nationality = row["Nationality"].ToString()
                    };
                    ListReturned.Add(Displaynationality);
                }
            }

            return ListReturned;
        }
    }
}