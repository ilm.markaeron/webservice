﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using illimitadoWepAPI.SchedulerPortalClass;
using illimitadoWepAPI.Models.ScheduleDemoModels;
using illimitadoWepAPI.Controllers.SchedulerDemo;
using System.Data;

namespace illimitadoWepAPI.Controllers.SchedulerDemo
{
    public class MasterControlController : ApiController
    {
        [HttpPost]
        [Route("getMasterControl")]
        public List<MasterControlItems> GetMasterControl()
        {
            Connection con = new Connection();
            DataTable dt = new DataTable();
            dt = con.GetDataTable("sp_SD_getMasterControl");
            List<MasterControlItems> final = new List<MasterControlItems>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                MasterControlItems temp = new MasterControlItems();
                temp.id = dt.Rows[i]["ID"].ToString();
                temp.EmployeeCount1 = dt.Rows[i]["EmployeeCount1"].ToString();
                temp.EmployeeCount2 = dt.Rows[i]["EmployeeCount2"].ToString();
                final.Add(temp);
            }
            return final;
        }
        MasterControlItems mc = new MasterControlItems();
        [HttpPost]
        [Route("updateMasterControlItem")]
        public string updateMasterControl(MCItem para)
        {
            try
            {
                DataTable TermData = new DataTable();
                TermData.Columns.AddRange(new DataColumn[2] { new DataColumn("ControlID"), new DataColumn("EmpLevel") });
                for (int i = 0; i < para.itemarr.Length; i++)
                {
                    TermData.Rows.Add(para.controlID, para.itemarr[i]);
                }
                Connection con = new Connection();
                con.myparameters.Clear();
                con.myparameters.Add(new myParameters { ParameterName = "@CID", mytype = SqlDbType.VarChar, Value = para.controlID });
                con.myparameters.Add(new myParameters { ParameterName = "@ItemList", mytype = SqlDbType.Structured, Value = TermData });
                con.myparameters.Add(new myParameters { ParameterName = "@target", mytype = SqlDbType.VarChar, Value = para.target });
                con.ExecuteNonQuery("sp_SD_UpdateMasterControl");
                return "success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }
        [HttpPost]
        [Route("getSavedDetails")]
        public List<GetDetails> getsavedindustry(ctrlID cid)
        {
            ctrlID param = new ctrlID();
            param = cid;
            if (param.column == "industry")
            {
                try
                {
                    DataTable dt = new DataTable();
                    List<GetDetails> lstind = new List<GetDetails>();
                    Connection con = new Connection();
                    con.myparameters.Clear();
                    con.myparameters.Add(new myParameters { ParameterName = "@controlID", mytype = SqlDbType.VarChar, Value = cid.ctid });
                    dt = con.GetDataTable("sp_SD_MCGetIndustry");
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        GetDetails temp = new GetDetails();
                        temp.ID = dt.Rows[i][0].ToString();
                        temp.resDetail = dt.Rows[i][1].ToString();
                        lstind.Add(temp);
                    }

                    return lstind;
                }
                catch
                {
                    return null;
                }
            }
            else if (param.column == "region")
            {
                try
                {
                    DataTable dt = new DataTable();
                    List<GetDetails> lstind = new List<GetDetails>();
                    Connection con = new Connection();
                    con.myparameters.Clear();
                    con.myparameters.Add(new myParameters { ParameterName = "@controlID", mytype = SqlDbType.VarChar, Value = cid.ctid });
                    dt = con.GetDataTable("sp_SD_MCGetRegion");
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        GetDetails temp = new GetDetails();
                        temp.ID = dt.Rows[i][0].ToString();
                        temp.resDetail = dt.Rows[i][1].ToString();
                        lstind.Add(temp);
                    }

                    return lstind;
                }
                catch
                {
                    return null;
                }
            }
            else if (param.column == "province")
            {
                try
                {
                    DataTable dt = new DataTable();
                    List<GetDetails> lstind = new List<GetDetails>();
                    Connection con = new Connection();
                    con.myparameters.Clear();
                    con.myparameters.Add(new myParameters { ParameterName = "@controlID", mytype = SqlDbType.VarChar, Value = cid.ctid });
                    dt = con.GetDataTable("sp_SD_MCGetProvince");
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        GetDetails temp = new GetDetails();
                        temp.ID = dt.Rows[i][0].ToString();
                        temp.resDetail = dt.Rows[i][1].ToString();
                        lstind.Add(temp);
                    }

                    return lstind;
                }
                catch
                {
                    return null;
                }
            }
            else if (param.column == "city")
            {
                try
                {
                    DataTable dt = new DataTable();
                    List<GetDetails> lstind = new List<GetDetails>();
                    Connection con = new Connection();
                    con.myparameters.Clear();
                    con.myparameters.Add(new myParameters { ParameterName = "@controlID", mytype = SqlDbType.VarChar, Value = cid.ctid });
                    dt = con.GetDataTable("sp_SD_MCGetCity");
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        GetDetails temp = new GetDetails();
                        temp.ID = dt.Rows[i][0].ToString();
                        temp.resDetail = dt.Rows[i][1].ToString();
                        lstind.Add(temp);
                    }

                    return lstind;
                }
                catch
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
        }
        [HttpPost]
        [Route("GetAllProvinces")]
        public List<MultiSelectItems> AllProvinces()
        {
            List<MultiSelectItems> ListReturned = new List<MultiSelectItems>();
            DataTable DT = new DataTable();
            try
            {
                illimitadoWepAPI.LoanMakerClass.Connection con = new LoanMakerClass.Connection();
               
                DT = con.GetDataTable("sp_Vue_GetAllProvince");
                for (int i = 0; i < DT.Rows.Count; i++)
                {
                    MultiSelectItems MS = new MultiSelectItems();
                    MS.ItemName = DT.Rows[i]["ItemName"].ToString();
                    MS.Message = "Success";
                    ListReturned.Add(MS);
                }
                return ListReturned;
            }
            catch (Exception e)
            {
                MultiSelectItems MS = new MultiSelectItems();
                MS.Message = e.Message;
                ListReturned.Add(MS);
                return ListReturned;
            }
        }
        [HttpPost]
        [Route("GetAllCity")]
        public List<MultiSelectItems> AllCities()
        {
            List<MultiSelectItems> ListReturned = new List<MultiSelectItems>();
            DataTable DT = new DataTable();
            try
            {
                illimitadoWepAPI.LoanMakerClass.Connection con = new LoanMakerClass.Connection();

                DT = con.GetDataTable("sp_Vue_GetAllCity");
                for (int i = 0; i < DT.Rows.Count; i++)
                {
                    MultiSelectItems MS = new MultiSelectItems();
                    MS.ItemName = DT.Rows[i]["ItemName"].ToString();
                    MS.Message = "Success";
                    ListReturned.Add(MS);
                }
                return ListReturned;
            }
            catch (Exception e)
            {
                MultiSelectItems MS = new MultiSelectItems();
                MS.Message = e.Message;
                ListReturned.Add(MS);
                return ListReturned;
            }
        }
    }
}
