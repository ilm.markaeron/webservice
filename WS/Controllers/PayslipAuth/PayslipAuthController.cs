﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Http;
using illimitadoWepAPI.Models;
using illimitadoWepAPI.Services.PayslipAuth;
using Newtonsoft.Json;

namespace illimitadoWepAPI.Controllers
{
    public class PayslipAuthController : ApiController
    {
        #region Variables and Constants
        private SPayslipAuth _SPayslipAuth = new SPayslipAuth();

        #endregion

        #region Key Test
        [HttpPost]
        [Route("api/ILM/CheckToken")]
        [Authorize(Roles = "Administrator, SuperAdministrator, User")]
        public IHttpActionResult TokenChecker(PayslipModelPA model)
        {
            var identity = (ClaimsIdentity)User.Identity;
            var roles = identity.Claims
                        .Where(c => c.Type == ClaimTypes.Role)
                        .Select(c => c.Value);

            return Ok($"Welcome, {identity.Name}");
        }
        #endregion

        #region
        [HttpPost]
        [Route("api/ILM/GetCrawfordPayslip")]
        [Authorize(Roles = "Administrator, CrawfordUser")]
        public List<PayslipURL> GetCrawfordPayslip(PayslipModelPA model)
        {
            var output = _SPayslipAuth.GetCrawfordPayslip(model);
            if (output.Count < 1)
                return null;

            //var result = JsonConvert.SerializeObject(output);

            return output;
        }
        #endregion
    }
}