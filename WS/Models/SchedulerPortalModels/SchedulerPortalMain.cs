﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace illimitadoWepAPI.Models.SchedulerPortalModels
{
    public class test_accinfolist
    {
        public string _sid { get; set; }
        public string _sfn { get; set; }
        public string _smi { get; set; }
        public string _sln { get; set; }
    }
    public class test_accinfo
    {
        public List<test_accinfolist> test_accinfolist;
        public string _myret { get; set; }
    }
    public class test_annlist
    {
        public string _annid { get; set; }
        public string _anntitle { get; set; }
        public string _annmess { get; set; }
        public string _anndatetime { get; set; }
        public string _anncreatedby { get; set; }
    }
    public class test_ann
    {
        public List<test_annlist> test_annlist;
        public string _myret { get; set; }
    }
    public class test_schedlist
    {
        public string _id { get; set; }
        public string _sdate { get; set; }
        public string _day { get; set; }
        public string _stimein { get; set; }
        public string _stimeout { get; set; }
    }
    public class test_sched
    {
        public List<test_schedlist> test_schedlist;
        public string _myret { get; set; }
    }
    public class test_getsched
    {
        public string _sid { get; set; }
        public string _startdate { get; set; }
        public string _enddate { get; set; }
        public string _format { get; set; }
    }
    public class test_tito
    {
        public string _sid { get; set; }
        public string _status { get; set; }
    }
    public class schedulerLogin
    {
        public string _username { get; set; }
        public string _password { get; set; }
    }
    public class employeeInfoList
    {
        public string _firstName { get; set; }
        public string _middleName { get; set; }
        public string _lastName { get; set; }
        public string _title { get; set; }
        public string _role { get; set; }
        public string _area { get; set; }
        public string _branch { get; set; }
        public string _department { get; set; }
        public string _subdepartment { get; set; }
        public string _userType { get; set; }
        public string _email { get; set; }
        public string _phoneNumber { get; set; }
        public string _status { get; set; }
        public string _userid { get; set; }
        public string _name { get; set; }
        public string _loginid { get; set; }
        public bool _autoapprove { get; set; }

    }
    public class employeeInfo
    {
        public List<employeeInfoList> employeeInfoList;
        public string _myreturn { get; set; }
        public string _UID { get; set; }
    }
    public class changePassword
    {
        public string _UID { get; set; }
        public string _oldPassword { get; set; }
        public string _newPassword { get; set; }
    }
    public class subjectlist
    {
        public string _SID { get; set; }
        public string _Subject { get; set; }
        public bool _Status { get; set; }
        public string _DefaultName { get; set; }
        public string _ModifiedBy { get; set; }
        public string _ModifiedDate { get; set; }
        public bool _Edit { get; set; }
    }
    public class subjects
    {
        public List<subjectlist> subjectlist;
        public string _myreturn { get; set; }
    }
    //public class subsubjectlist
    //{
    //    public string _RowID { get; set; }
    //    public string _SID { get; set; }
    //    public string _Subject { get; set; }
    //    public bool _Status { get; set; }
    //    public string _ModifiedBy { get; set; }
    //    public string _ModifiedDate { get; set; }
    //    public bool _Edit { get; set; }
    //}
    public class subsubjects
    {
        public List<subsubjectlist> subsubjectlist;
        public string _myreturn { get; set; }
    }
    public class gridsearch
    {
        public string _status { get; set; }
        public string _branch { get; set; }
        public string _department { get; set; }
        public string _subdepartment { get; set; }
        public string _requestor { get; set; }
        public string _datefrom { get; set; }
        public string _dateto { get; set; }
    }
    public class gridrequest
    {
        public string _requestid { get; set; }
        public string _requestor { get; set; }
        public string _company { get; set; }
        public string _branch { get; set; }
        public string _department { get; set; }
        public string _status { get; set; }
        public string _subdepartment { get; set; }
        public string _datefrom { get; set; }
        public string _dateto { get; set; } 
    }
    public class gridrequestlist
    {
        public List<gridrequest> gridrequest;
        public string _myreturn { get; set; }
    }
    public class contactinfo
    {
        public string _contactperson { get; set; }
        public string _title { get; set; }
        public string _phonenumber { get; set; }
        public string _email { get; set; }
    }
    public class requestinfo
    {
        public string _requestid { get; set; }
        public string _daterequested { get; set; }
        public string _requestedtime { get; set; }
        public string _requestorname { get; set; }
        public string _requestorarea { get; set; }
        public string _requestorbranch { get; set; }
        public string _department { get; set; }
        public string _subdepartment { get; set; }
        public string _companyname { get; set; }
        public string _industry { get; set; }
        public string _noofemployees { get; set; }
        public string _hris { get; set; }
        public string _unitbldg { get; set; }
        public string _city { get; set; }
        public string _status { get; set; }
        public string _reason { get; set; }
        public string _modifiedby { get; set; }
        public List<contactinfo> contactinfo;
    }
    public class requestinfolist
    {
        public List<requestinfo> requestinfo;
        public string _myreturn { get; set; }
    }
    public class subjectlabel
    {
        public string _title { get; set; }
        public string _role { get; set; }
        public string _area { get; set; }
        public string _branch { get; set; }
        public string _department { get; set; }
        public string _subdepartment { get; set; }
    }
    public class subjectlabellist
    {
        public List<subjectlabel> subjectlabel;
        public string _myreturn { get; set; }
    }
    public class requestor
    {
        public string _userid { get; set; }
        public string _requestorname { get; set; }
        public string _usertype { get; set; }
    }
    public class requestorlist
    {
        public List<requestor> requestor;
        public string _myreturn { get; set; }
    }
    public class createuser
    {
        public string _userid { get; set; }
        public string _user { get; set; }
        public string _pass { get; set; }
        public string _fname { get; set; }
        public string _mname { get; set; }
        public string _lname { get; set; }
        public string _title { get; set; }
        public string _role { get; set; }
        public string _area { get; set; }
        public string _branch { get; set; }
        public string _department { get; set; }
        public string _subdepartment { get; set; }
        public string _company { get; set; }
        public string _usertype { get; set; }
        public string _email { get; set; }
        public string _phone { get; set; }
        public string _updatetype { get; set; }
        public bool _autoapprove { get; set; }
    }
    public class userType
    {
        public string _userID { get; set; }
        public string _userType { get; set; }
    }
    public class AuditTrail
    {
        public string _requestid { get; set; }
        public string _requestorname { get; set; }
        public string _companyname { get; set; }
        public string _daterequested { get; set; }
        public string _timerequested { get; set; }
        public string _appointmentreason { get; set; }
        public string _status { get; set; }
        public string _modifiedby { get; set; }
        public string _modifiedon { get; set; }
    }
    public class AuditTrailList
    {
        public List<AuditTrail> AuditTrail;
        public string _myreturn { get; set; }
    }
    public class AuditParameters
    {
        public string _datefrom { get; set; }
        public string _dateto { get; set; }
    }
    public class DropdownHierarchy
    {
        public string _source { get; set; }
        public string _target { get; set; }
        public string _paramater { get; set; }
        public String[] _items { get; set; }
    }
    public class DropdownHierarchyItems
    {
        public string _itemid { get; set; }
        public string _itemname { get; set; }
    }
    public class DropdownHierarchyList
    {
        public List<DropdownHierarchyItems> DDITEMS;
        public string _myreturn { get; set; }
    }
    public class UserDataMappingItems
    {
        public String[] _fname { get; set; }
        public String[] _mname { get; set; }
        public String[] _lname { get; set; }
        public String[] _title { get; set; }
        public String[] _role { get; set; }
        public String[] _area { get; set; }
        public String[] _branch { get; set; }
        public String[] _department { get; set; }
        public String[] _subdepartment { get; set; }
        public String[] _usertype { get; set; }
        public String[] _email { get; set; }
        public String[] _phonenumber { get; set; }
    }
    public class UserMap
    {
        public string _rowid { get; set; }
        public string _fname { get; set; }
        public string _mname { get; set; }
        public string _lname { get; set; }
        public string _title { get; set; }
        public string _role { get; set; }
        public string _area { get; set; }
        public string _branch { get; set; }
        public string _department { get; set; }
        public string _subdepartment { get; set; }
        public string _usertype { get; set; }
        public string _email { get; set; }
        public string _phonenumber { get; set; }
    }
    public class UserList
    {
        public string[] _ret { get; set; }
        public List<UserMap> UserMapList;
        public string _myreturn { get; set; }
    }

    //----------revised

    public class schedulerLoginv2
    {
        public string _username { get; set; }
        public string _password { get; set; }
        public string CN { get; set; }
    }

    public class subjectlistv2
    {
        public string _SID { get; set; }
        public string _Subject { get; set; }
        public bool _Status { get; set; }
        public string _DefaultName { get; set; }
        public string _ModifiedBy { get; set; }
        public string _ModifiedDate { get; set; }
        public bool _Edit { get; set; }
        public string CN { get; set; }
        public string SCN { get; set; }
    }
    public class subjectsV2
    {
        public List<subjectlistv2> subjectlist;
        public string _myreturn { get; set; }

        public string CN { get; set; }
        public string SCN { get; set; }


    }

    public class subsubjectlist
    {
        public string _RowID { get; set; }
        public string _SID { get; set; }
        public string _Subject { get; set; }
        public bool _Status { get; set; }
        public string _ModifiedBy { get; set; }
        public string _ModifiedDate { get; set; }
        public bool _Edit { get; set; }

        public string CN { get; set; }
        public string SCN { get; set; }
    }
    public class subsubjectsv2
    {
        public List<subsubjectlist> subsubjectlist;
        public string _myreturn { get; set; }

        public string CN { get; set; }
    }

    public class requestorv2
    {
        public string _userid { get; set; }
        public string _requestorname { get; set; }
        public string _usertype { get; set; }
        public string CN { get; set; }
        public string SCN { get; set; }
    }
    public class requestorlistv2
    {
        public List<requestorv2> requestor;
        public string _myreturn { get; set; }
    }


    public class employeeInfoListv2
    {
        public string _firstName { get; set; }
        public string _middleName { get; set; }
        public string _lastName { get; set; }
        public string _title { get; set; }
        public string _role { get; set; }
        public string _area { get; set; }
        public string _branch { get; set; }
        public string _department { get; set; }
        public string _subdepartment { get; set; }
        public string _userType { get; set; }
        public string _email { get; set; }
        public string _phoneNumber { get; set; }
        public string _status { get; set; }
        public string _userid { get; set; }
        public string _name { get; set; }
        public string _loginid { get; set; }
        public bool _autoapprove { get; set; }
        public string CN { get; set; }

        public string SCN { get; set; }


    }

    public class userType3
    {
        public string _userID { get; set; }
        public string _userType { get; set; }
        public string CN { get; set; }
        public string SCN { get; set; }
    }
    public class employeeInfov2
    {
        public List<employeeInfoListv2> employeeInfoList;
        public string _myreturn { get; set; }
        public string _UID { get; set; }
        public string CN { get; set; }
    }

    public class employeeInfov3
    {
        public List<employeeInfoListv2> employeeInfoList;
        public string _myreturn { get; set; }
        public string _UID { get; set; }
        public string CN { get; set; }
        public string SCN { get; set; }
    }

    public class createuserv2
    {
        public string _userid { get; set; }
        public string _user { get; set; }
        public string _pass { get; set; }
        public string _fname { get; set; }
        public string _mname { get; set; }
        public string _lname { get; set; }
        public string _title { get; set; }
        public string _role { get; set; }
        public string _area { get; set; }
        public string _branch { get; set; }
        public string _department { get; set; }
        public string _subdepartment { get; set; }
        public string _company { get; set; }
        public String[] _usertype { get; set; }
        public string _email { get; set; }
        public string _phone { get; set; }
        public string _updatetype { get; set; }
        public bool _autoapprove { get; set; }
        public string CN { get; set; }
    }


    public class subjectlabelv2
    {
        public string _title { get; set; }
        public string _role { get; set; }
        public string _area { get; set; }
        public string _branch { get; set; }
        public string _department { get; set; }
        public string _subdepartment { get; set; }
        public string CN { get; set; }
        public string SCN { get; set; }
    }
    public class subjectlabellistv2
    {
        public List<subjectlabelv2> subjectlabel;
        public string _myreturn { get; set; }
    }

    public class changePasswordv2
    {
        public string _UID { get; set; }
        public string _oldPassword { get; set; }
        public string _newPassword { get; set; }
        public string CN { get; set; }

        public string SCN { get; set; }
    }


    public class DropdownHierarchyv2
    {
        public string _source { get; set; }
        public string _target { get; set; }
        public String[] _items { get; set; }
        public string CN { get; set; }
        public string SCN { get; set; }
    }
    public class DropdownHierarchyItemsv2
    {
        public string _itemid { get; set; }
        public string _itemname { get; set; }
    }
    public class DropdownHierarchyListv2
    {
        public List<DropdownHierarchyItemsv2> DDITEMS;
        public string _myreturn { get; set; }
    }


    public class UserDataMappingItemsv2
    {
        public String[] _fname { get; set; }
        public String[] _mname { get; set; }
        public String[] _lname { get; set; }
        public String[] _title { get; set; }
        public String[] _role { get; set; }
        public String[] _area { get; set; }
        public String[] _branch { get; set; }
        public String[] _department { get; set; }
        public String[] _subdepartment { get; set; }
        public String[] _usertype { get; set; }
        public String[] _email { get; set; }
        public String[] _phonenumber { get; set; }
        public String CN { get; set; }
    }
    public class UserMapv2
    {
        public string _rowid { get; set; }
        public string _fname { get; set; }
        public string _mname { get; set; }
        public string _lname { get; set; }
        public string _title { get; set; }
        public string _role { get; set; }
        public string _area { get; set; }
        public string _branch { get; set; }
        public string _department { get; set; }
        public string _subdepartment { get; set; }
        public string _usertype { get; set; }
        public string _email { get; set; }
        public string _phonenumber { get; set; }
        public string CN { get; set; }
    }
    public class UserListv2
    {
        public string[] _ret { get; set; }
        public List<UserMapv2> UserMapList;
        public string _myreturn { get; set; }
    }

    public class gridsearchv2
    {
        public string _status { get; set; }
        public string _branch { get; set; }
        public string _department { get; set; }
        public string _subdepartment { get; set; }
        public string _requestor { get; set; }
        public string _datefrom { get; set; }
        public string _dateto { get; set; }
        public string CN { get; set; }
        public string SCN { get; set; }
    }
    public class gridrequestv2
    {
        public string _requestid { get; set; }
        public string _requestor { get; set; }
        public string _company { get; set; }
        public string _branch { get; set; }
        public string _department { get; set; }
        public string _status { get; set; }
        public string _subdepartment { get; set; }
        public string _datefrom { get; set; }
        public string _dateto { get; set; }
    }
    public class gridrequestlistv2
    {
        public List<gridrequestv2> gridrequest;
        public string _myreturn { get; set; }
    }

    public class AuditTrailv2
    {
        public string _requestid { get; set; }
        public string _requestorname { get; set; }
        public string _companyname { get; set; }
        public string _daterequested { get; set; }
        public string _timerequested { get; set; }
        public string _appointmentreason { get; set; }
        public string _status { get; set; }
        public string _modifiedby { get; set; }
        public string _modifiedon { get; set; }
    }
    public class AuditTrailListv2
    {
        public List<AuditTrailv2> AuditTrail;
        public string _myreturn { get; set; }
    }
    public class AuditParametersv2
    {
        public string _datefrom { get; set; }
        public string _dateto { get; set; }
        public string CN { get; set; }
    }
    public class GetMaxUserID
    {
        public string _maxuid { get; set; }
        public string _myreturn { get; set; }
    }
    public class getNotiff
    {
        public string getNotif { get; set; }
    }

    public class ScheduleToDoNotiff
    {
        public List<getNotiff> getNotifCount;
        public string myreturn;
    }

    public class notif
    {
        public string ClientID { get; set; }
        public string SubClientID { get; set; }
    }

    public class PulongWebParams
    {
        public string UserID { get; set; }
        public string Userpass { get; set; }
        public string Client_Code { get; set; }
    }

    public class PulongWebReturnDetails
    {
        public string EmpID { get; set; }
        public string PClientID { get; set; }
        public string SubClientID { get; set; }
        public string userName { get; set; }
    }

    public class PulongWebList
    {
        public List<PulongWebReturnDetails> PulongWebReturnDetails;
        public string _myreturn { get; set; }
    }

    public class SchedulerParams
    {
        public string USEREMAIL { get; set; }
        public string PASSWORD { get; set; }
        public string CLIENTCODE { get; set; }
    }

    public class SchedulerReturnDetails
    {
        public string PClientID { get; set; }
        public string SubClientID { get; set; }
        public string UserType { get; set; }
        public string AutoApprove { get; set; }
        public string EmpName { get; set; }
        public string UserID { get; set; }
    }

    public class SchedulerList
    {
        public List<SchedulerReturnDetails> SchedulerReturnDetails;
        public string _myreturn { get; set; }
    }

    #region Pulong Mobile
    public class DemoSchedule
    {
        public string _ID { get; set; }
        public string _StartTime { get; set; }
        public string _EndTime { get; set; }
        public string _Status { get; set; }
    }
    public class GetDemoSchedule
    {
        public List<DemoSchedule> DemoSchedule;
        public string _myreturn { get; set; }
    }
    public class DemoScheduleParams
    {
        public string _scheddate { get; set; }
        public string _userid { get; set; }
    }
    public class UserSchedule
    {
        public string _SeriesID { get; set; }
        public string _CompanyID { get; set; }
        public string _StartDate { get; set; }
        public string _EndDate { get; set; }
        public string _StartTime { get; set; }
        public string _EndTime { get; set; }
        public string _Status { get; set; }
        public string _UserID { get; set; }
    }
    public class GetUserSchedule
    {
        public List<UserSchedule> UserSchedule;
        public string _myreturn { get; set; }
    }

    public class Company
    {
        public string _CompanyName { get; set; }
    }
    public class GetCompanyNames
    {
        public List<Company> Companies;
        public string _myreturn { get; set; }
    }
    public class GetCompanyNamesParams
    {
        public string CN { get; set; }
    }
    public class GetCompanyNamesv2
    {
        public List<Companyv2> CompanyNames;
        public string _myreturn { get; set; }
    }
    public class Companyv2
    {
        public string companyName;
    }

    public class LoginParams
    {
        public string PClientCode { get; set; }
        public string username { get; set; }
        public string password { get; set; }
    }

    public class LoginDetails
    {
        public string PClientID { get; set; }
        public string SubClientID { get; set; }
        public string UserType { get; set; }
        public string AutoApprove { get; set; }
        public string EmpName { get; set; }
        public string USERID { get; set; }
    }

    public class LoginList
    {
        public List<LoginDetails> LoginDetails;
        public string _myreturn { get; set; }
    }
    #endregion

    public class createuserv3
    {
        public string _userid { get; set; }
        public string _user { get; set; }
        public string _pass { get; set; }
        public string _fname { get; set; }
        public string _mname { get; set; }
        public string _lname { get; set; }
        public string _title { get; set; }
        public string _role { get; set; }
        public string _area { get; set; }
        public string _branch { get; set; }
        public string _department { get; set; }
        public string _subdepartment { get; set; }
        public string _company { get; set; }
        public String[] _usertype { get; set; }
        public string _email { get; set; }
        public string _phone { get; set; }
        public string _updatetype { get; set; }
        public bool _autoapprove { get; set; }
        public string CN { get; set; }
        public string SCN { get; set; }
    }

    public class updateuserv3
    {
        public String[] _userid { get; set; }
        public string _user { get; set; }
        public string _pass { get; set; }
        public string _fname { get; set; }
        public string _mname { get; set; }
        public string _lname { get; set; }
        public string _title { get; set; }
        public string _role { get; set; }
        public string _area { get; set; }
        public string _branch { get; set; }
        public string _department { get; set; }
        public string _subdepartment { get; set; }
        public string _company { get; set; }
        public String[] _usertype { get; set; }
        public string _email { get; set; }
        public string _phone { get; set; }
        public string _updatetype { get; set; }
        public bool _autoapprove { get; set; }
        public string CN { get; set; }
        public string SCN { get; set; }
    }

    public class companyupdateparams
    {
        public string PClient_ID { get; set; }
        public string SubClientCode { get; set; }
    }

    public class companylist
    {
        public string SubClient_ID { get; set; }
        public string SubClientName { get; set; }
        public string SubClientCode { get; set; }
        public string DateOfIncorporation { get; set; }
        public string Floor { get; set; }
        public string Building { get; set; }
        public string Street { get; set; }
        public string Barangay { get; set; }
        public string Municipality { get; set; }
        public string Province { get; set; }
        public string Region { get; set; }
        public string Country { get; set; }
        public string ZipCode { get; set; }
        public string CompanyNationality { get; set; }
        public string NumberOfEmployees { get; set; }
        public string CompanyTin { get; set; }
        public string RepUserID { get; set; }
        public string RepUserName { get; set; }
        public string RepFirstName { get; set; }
        public string RepMiddleName { get; set; }
        public string RepLastName { get; set; }
        public string RepTitle { get; set; }
        public string RepEmail { get; set; }
        public string RepPhoneNumber { get; set; }
        public string message { get; set; }
    }

    public class updatecompanyparams
    {
        public string PClient_ID { get; set; }
        public string SubClient_ID { get; set; }
        public string SubClientName { get; set; }
        public string SubClientCode { get; set; }
        public string DateOfIncorporation { get; set; }
        public string Floor { get; set; }
        public string Building { get; set; }
        public string Street { get; set; }
        public string Barangay { get; set; }
        public string Municipality { get; set; }
        public string Province { get; set; }
        public string Region { get; set; }
        public string Country { get; set; }
        public string ZipCode { get; set; }
        public string CompanyNationality { get; set; }
        public string NumberOfEmployees { get; set; }
        public string CompanyTin { get; set; }
        public string RepUserName { get; set; }
        public string RepFirstName { get; set; }
        public string RepMiddleName { get; set; }
        public string RepLastName { get; set; }
        public string RepTitle { get; set; }
        public string RepEmail { get; set; }
        public string RepPhoneNumber { get; set; }
        public string prevUserID { get; set; }
    }

    public class checkrepusername
    {
        public string PClient_ID { get; set; }
        public string RepUserName { get; set; }
    }

    public class schedulerCredEmail
    {
        public string repEmail { get; set; }
        public string PClient_ID { get; set; }
        public string SubClientCode { get; set; }
    }
    public class setSubclientInactive
    {
        public string PClient_ID { get; set; }
        public string SubClient_ID { get; set; }
        public string message { get; set; }
    }
    #region Brandon Migrate Api for Pulong
    public class Client
    {
        public String ClientID { get; set; }
        public String[] Department { get; set; }
        public String[] SubDepartment { get; set; }
    }
    public class businessSegments
    {
        public string businessSegment { get; set; }
        public string rowID { get; set; }
    }
   
    public class businessUnits
    {
        public string businessUnit { get; set; }
        public string rowID { get; set; }
    }
    public class employees
    {
        public string empName { get; set; }
        public string empUID { get; set; }
    }
    public class createUserInfo
    {
        public string empname { get; set; }
        public string deptname { get; set; }
        public string subdeptname { get; set; }
        public string email { get; set; }
        public string phonenum { get; set; }
        public String[] usertype { get; set; }
        public string empstat { get; set; }
        public string empFName { get; set; }
        public string empMName { get; set; }
        public string empLName { get; set; }
        public string editEmpID { get; set; }
        public string ClientID { get; set; }

    }


    public class CheckEmpID
    {
        public string PClientID { get; set; }
        public string EmpID { get; set; }
    }
    public class CheckEmpIDpul
    {
        public string PClientID { get; set; }
        public string SubClientID { get; set; }
        public string EmpID { get; set; }
    }

    public class createUserInfoV2
    {
        public string empname { get; set; }
        public string deptname { get; set; }
        public string subdeptname { get; set; }
        public string email { get; set; }
        public string phonenum { get; set; }
        public String[] usertype { get; set; }
        public string empstat { get; set; }
        public string empFName { get; set; }
        public string empMName { get; set; }
        public string empLName { get; set; }
        public string EmpID { get; set; }
        public string ClientID { get; set; }
        public string multiEdit { get; set; }

    }
    public class updateStatusParam
    {
        public String[] uID { get; set; }
        public string status { get; set; }
        public string ClientID { get; set; }
    }
    public class subjectlistUpdate
    {
        public string _SID { get; set; }
        public string _Subject { get; set; }
        public bool _Status { get; set; }
        public string _DefaultName { get; set; }
        public string _ModifiedBy { get; set; }
        public string _ModifiedDate { get; set; }
        public bool _Edit { get; set; }
        public string ClientID { get; set; }
    }
    public class subsubjectlistlabel
    {
        public string _RowID { get; set; }
        public string _SID { get; set; }
        public string _Subject { get; set; }
        public bool _Status { get; set; }
        public string _ModifiedBy { get; set; }
        public string _ModifiedDate { get; set; }
        public bool _Edit { get; set; }
        public string ClientID { get; set; }
    }
    public class subsubjectslistforlabel
    {
        public List<subsubjectlistlabel> subsubjectlist;
        public string _myreturn { get; set; }
    }
    public class MultiSelectItems
    {
        public string ItemType { get; set; }
        public string ItemID { get; set; }
        public string ItemName { get; set; }
        public string Message { get; set; }
    }
    public class Location
    {
        public string Loc { get; set; }
    }
    public class LocationList
    {
        public List<Location> Locations { get; set; }
    }
    public class regItems
    {
        public String[] regsel { get; set; }
    }
    public class province
    {
        public String[] provinceID { get; set; }
    }
    public class MCItem
    {
        public String[] itemarr { get; set; }
        public String target { get; set; }
        public String controlID { get; set; }
        public String ClientID { get; set; }
    }
    public class MasterControlItems
    {
        public String id { get; set; }
        public String EmployeeCount1 { get; set; }
        public String EmployeeCount2 { get; set; }
        public bool isDisabled { get; set; }
    }
    public class GetDetails
    {
        public String resDetail { get; set; }
        public String ID { get; set; }
    }
    public class ctrlID
    {
        public string ctid { get; set; }
        public string column { get; set; }
    }
    public class DeleteMC
    {
        public string ctlID { get; set; }
    }
    public class employeeID
    {
        public string uID { get; set; }
        public string ClientID { get; set; }
    }
    public class upass
    {
        public string userpass { get; set; }
        public string message { get; set; }
    }
    public class updatePass
    {
        public string uID { get; set; }
        public string newpass { get; set; }
        public string ClientID { get; set; }
    }
    public class userinfo
    {
        public String[] EID { get; set; }
        public String[] Department { get; set; }
        public String[] SubDepartment { get; set; }
        public string ClientID { get; set; }
    }

    public class userinfoV2
    {
        public String[] EID { get; set; }
        public String[] Department { get; set; }
        public String[] SubDepartment { get; set; }
        public string Status { get; set; }
        public string ClientID { get; set; }
    }
    public class info
    {
        public string empUserID { get; set; }
        public string empName { get; set; }
        public string deptname { get; set; }
        public string subdeptname { get; set; }
        public string empEmail { get; set; }
        public string empPnum { get; set; }
        public string empUtype { get; set; }
        public string empStat { get; set; }
        public string FName { get; set; }
        public string MName { get; set; }
        public string LName { get; set; }
        public string userEmailID { get; set; }
        public string DeptID { get; set; }
        public string SubDeptID { get; set; }
        public string Company { get; set; }

    }
    public class updateField
    {
        public string SCHEDULE { get; set; }
        public string START { get; set; }
        public string FIRSTBREAK { get; set; }
        public string LUNCH { get; set; }
        public string SECONDBREAK { get; set; }
        public string ENDBREAK { get; set; }
        public string ENDOFSHIFT { get; set; }
        public String[] ID { get; set; }
        public string FIRSTBREAKDSP { get; set; }
        public string SECONDBREAKDSP { get; set; }
        public string ENDBREAKDSP { get; set; }
        public string LUNCHBREAKEND { get; set; }
        public string RESTDAY1 { get; set; }
        public string RESTDAY2 { get; set; }
        public string RESTDAY3 { get; set; }
        public string FBREAKTIMEMINUTES { get; set; }
        public string SBREAKTIMEMINUTES { get; set; }
        public string EBREAKTIMEMINUTES { get; set; }
        public string CLIENTID { get; set; }
        public string CLIENTNAME { get; set; }
        public string PClientID { get; set; }
        public string BreakType { get; set; }
    }

    public class tokenlist
    {
        public string tokenID { get; set; }
        public string message { get; set; }
    }

    public class tokenparams
    {
        public string Token { get; set; }
        public string UserID { get; set; }
        public string PClientID { get; set; }
        public string TokenAction { get; set; }
    }
    #endregion

    #region Brandon Migrate Api Model for Pulong Mobile

    public class MDCBOOKDEMO
    {
        public string companyID { get; set; }
        public string userID { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public string SchedDate { get; set; }
        public string TokenID { get; set; }
    }

    public class Appointment
    {
        public string REQUESTTO { get; set; }
        public string CompanyID { get; set; }
        public string UID { get; set; }
        public string BGCOLOR { get; set; }
        public string APPOINTMENTFOR { get; set; }
        public string APPONTMENTREASON { get; set; }
        public string COMPNAME { get; set; }
        public string INDUSTRY { get; set; }
        public string NUMOFEMPLOYEES { get; set; }
        public string CONTACTNAME { get; set; }
        public string CONTACTNUMBER { get; set; }
        public string EMAIL { get; set; }
        public string ZIPCODE { get; set; }
        public string UNITBLDG { get; set; }
        public string STREET { get; set; }
        public string MUNICIPALITY { get; set; }
        public string CITY { get; set; }
        public string REGION { get; set; }
        public string PROVINCE { get; set; }
        public string STATUS { get; set; }
        public string FK_AvailabilityID { get; set; }
        public string USERID { get; set; }
        public string HRIS { get; set; }
        public string TITLE { get; set; }
        public string CREATEDBY { get; set; }
        public string CREATEDDATE { get; set; }
        public string SERIESID { get; set; }
        public string STARTTIME { get; set; }
        public string ENDTIME { get; set; }
        public string STARTDATE { get; set; }
        public string ENDDATE { get; set; }
        public string REQUESTDATE { get; set; }

        public string TABLEFIELD { get; set; }
        public string ClientName { get; set; }

        public string VILLAGE { get; set; }


        public string BARANGAY { get; set; }

        public string COUNTNOTIFY { get; set; }
        public string DURATION { get; set; }
        public string DISTANCE { get; set; }

        public string ORIGIN { get; set; }

        public string LOCATION { get; set; }

        public string ALLVAILID { get; set; }

        public string AVAILSTARTTIME { get; set; }

        public string AVAILENDTIME { get; set; }

        public string AVAILSTATUS { get; set; }
        public string GAppiCompanyID { get; set; }

        public string EMPID { get; set; }
        public string EMPNAME { get; set; }
        public string EMPSTARTDATE { get; set; }
        public string EMPSTARTTIME { get; set; }
        public string EMPENDDATE { get; set; }
        public string EMPENDTIME { get; set; }
        public string EMPZIPCODE { get; set; }
        public string EMPUNITBLDG { get; set; }
        public string EMPSTREET { get; set; }
        public string EMPMUNICIPALITY { get; set; }
        public string EMPCITY { get; set; }
        public string EMPREGION { get; set; }
        public string EMPPROVINCE { get; set; }
        public string APPOINTEMPLOYEE { get; set; }
        public string PClient_ID { get; set; }
        public string SubClientID { get; set; }

    }
    public class ZipCode
    {
        public string ZIPCODE { get; set; }
        public string REGION { get; set; }
        public string PROVINCE { get; set; }
        public string CITY { get; set; }

        public string IndustryID { get; set; }
        public string IndustryInfo { get; set; }

        public string ID { get; set; }

    }
    public class BrgyList
    {
        public string BRGYID { get; set; }
        public string BRGYNAME { get; set; }
        public string ZIPCODE { get; set; }
    }
    public class MDCBOOKDEMOSEND
    {
        public List<MDCBOOKDEMO> BOOKDATA;
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public string CompanyID { get; set; }
        public string PClientID { get; set; }//added

    }
    public class Token
    {
        public string CN { get; set; }
        public string EMPID { get; set; }
        public string TOKENID { get; set; }
    }
    public class GetDatabase
    {
        public String ClientName { get; set; }
        public string ClientLink { get; set; }
    }
    #endregion

}