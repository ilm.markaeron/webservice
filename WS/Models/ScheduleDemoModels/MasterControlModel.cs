﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using illimitadoWepAPI.SchedulerPortalClass;
using illimitadoWepAPI.Models.ScheduleDemoModels;
using illimitadoWepAPI.Controllers.SchedulerDemo;

namespace illimitadoWepAPI.Models.ScheduleDemoModels
{
    public class MasterControlModel
    {
    }
    public class MasterControlItems
    {
        public String id { get; set; }
        public String EmployeeCount1 { get; set; }
        public String EmployeeCount2 { get; set; }
    }

    public class MCItem
    {
        public String[] itemarr { get; set; }
        public String target { get; set; }
        public String controlID { get; set; }
    }
    public class GetDetails
    {
        public String resDetail { get; set; }
        public String ID { get; set; }
    }
    public class ctrlID
    {
        public string ctid { get; set; }
        public string column { get; set; }
    }
    public class MultiSelectItems
    {
        public string ItemType { get; set; }
        public string ItemID { get; set; }
        public string ItemName { get; set; }
        public string Message { get; set; }
    }
    public class Location
    {
        public string Loc { get; set; }
    }
}