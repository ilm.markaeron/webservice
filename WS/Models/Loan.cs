﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace illimitadoWepAPI.Models
{
    public class Loan
    {
        public string LoanAmount { get; set; }
        public string LoanTerm { get; set; }

    }



    public class GetLoanResult
    {
        public List<Loan> Loan;
        public string myreturn;
    }

    public class payslip
    {
        public string paysliplink { get; set; }
    }
    public class GetpayslipResult
    {
        public List<payslip> payslip;
        public string myreturn;
    }

    public class graph
    {
        public string graphlocation { get; set; }
        public int graphtotal { get; set; }
        public string graphcolor { get; set; }
    }

    public class GetgraphResult
    {
        public List<graph> graph;
        public string myreturn;
    }

    public class availablebalance
    {
        public string balance { get; set; }
    }
    public class banking
    {
        public string userid { get; set; }
        public string password { get; set; }
        public string useridTransferring { get; set; }
        public string AmountBeingTransfer { get; set; }
        public string useridRecipient { get; set; }
    }
    public class GetbankingResult
    {
        public List<banking> banking;
        public string myreturn;
    }

    public class LoanReview
    {
        public string ID { get; set; }
        public string EmpID { get; set; }
        public string Loan { get; set; }
        public string ApplicationDate { get; set; }
        public string CheckReceiveDate { get; set; }
        public string CheckReleaseDate { get; set; }
        public string LoanTermValue { get; set; }
        public string LoanTermType { get; set; }
        public string LoanAmount { get; set; }
        public string PaymentAmount { get; set; }
        public string PaymentTerms { get; set; }
        public string LoanBalance { get; set; }
        public string Authorization { get; set; }
        public string Status { get; set; }

    }



    public class GetLoanReviewResult
    {
        public List<LoanReview> LoanReview;
        public string myreturn;
    }

    public class SelCustomForms
    {
        public string EmpID { get; set; }
        public string Forms { get; set; }
        public string Status { get; set; }

    }

    public class CustomForms
    {
        public string ID { get; set; }
        public string Forms { get; set; }
        public string CodeName { get; set; }

    }



    public class GetCustomFormsResult
    {
        public List<CustomForms> CustomForms;
        public string myreturn;
    }

    public class SelCustomFormsv2
    {
        public string EmpID { get; set; }
        public string Forms { get; set; }
        public string Status { get; set; }
        public string CN { get; set; }
    }
    public class PayslipDDLItems
    {
        public string Year { get; set; }
        public string PayPeriod { get; set; }
        public string PayoutDate { get; set; }
    }

    public class PayslipDDL
    {
        public List<PayslipDDLItems> PayslipDDLItems;
        public string myreturn { get; set; }
    }
    public class sendPay
    {
        public string CN { get; set; }
        public string empid { get; set; }
    }
    public class sendPayv2
    {
        public string CN { get; set; }
        public string PayDay { get; set; }
        public string PayYear { get; set; }
        public string SendToEmail { get; set; }
    }

    #region Newcore Standard Payslip
    public class NewcorePayslipParameters
    {
        public string branch { get; set; }
        public string will_send_to_email { get; set; }
        public string payout_date { get; set; }
        public List<pay_period> pay_periods { get; set; }
    }
    public class pay_period
    {
        public string period { get; set; }
    }
    #endregion

    public class company_email
    {
        public string email     { get; set; }
        public string password  { get; set; }
    }
}