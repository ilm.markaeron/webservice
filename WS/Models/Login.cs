﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace illimitadoWepAPI.Models
{
    public class EmpSal
    {
        public string CN { get; set; }
        public string EmpID { get; set; }
        public string Salary { get; set; }
    }
    public class EmpLogLeaveParam
    {
        public string CN { get; set; }
        public string EmpID { get; set; }
        public string SchedDate { get; set; }
    }
    public class EmpLogLeave
    {
        public string TimeIN { get; set; }
        public string TimeOUT { get; set; }
        public string ApprovedOT { get; set; }
        public string ApprovedLeave { get; set; }
    }
    public class EmployeeLogsLeave
    {
        public List<EmpLogLeave> EmpLogLeave;
        public string myreturn { get; set; }
    }
    public class EmpPayslipParam
    {
        public string CN { get; set; }
        public string EmpID { get; set; }
        public string PayPeriod { get; set; }
    }
    public class EmployeePayslipDetails
    {
        public string EmpID { get; set; }
        public string PayPeriod { get; set; }
        public string PayDay { get; set; }
        public string HourlyRate { get; set; }
        public string HDMF { get; set; }
        public string PhilHealth { get; set; }
        public string SSS { get; set; }
        public string Tax { get; set; }
        public string Employer_HDMF { get; set; }
        public string Employer_PhilHealth { get; set; }
        public string Employer_SSS { get; set; }
        public string Employer_SSS_EC { get; set; }
        public string NetPay { get; set; }
        public string NightDiff { get; set; }
        public string BasicPay { get; set; }
        public string DailyRate { get; set; }
        public string GrossPay { get; set; }
        public string Total_Reg_Hrs { get; set; }
        public string Total_Reg_Amt { get; set; }
    }
    public class EmpPayslip
    {
        public List<EmployeePayslipDetails> Payslip;
        public string myreturn { get; set; }
    }
    public class jsnclss
    {
        public JObject obj { get; set; }
    }
    public class Login
    {
        public String LoginID { get; set; }
        public String Password { get; set; }
        public String FaceID { get; set; }
        public String Coordinates { get; set; }
    }
    public class genpayslipparameters
    {
        public string CN { get; set; }
        public string payday { get; set; }
        public string payyear { get; set; }
    }

    public class BreakParam
    {
        public String CN { get; set; }
    }

    public class AdminSetting
    {
        public string EmpID { get; set; }
        public string GeoLocStatus { get; set; }
        public string ElevationStatus { get; set; }
        public string DefaultLoc { get; set; }
    }

    public class GetAdminSettingsResult
    {
        public List<AdminSetting> AdminSetting;
        public string myreturn;
    }

    public class Greetings
    {
        public String Images { get; set; }
        public String Message { get; set; }
        public String Title { get; set; }
        public String Sender { get; set; }
        public String Type { get; set; }

    }
    public class GreetingModel
    {
        public String ID { get; set; }
        public String Image { get; set; }
        public String Message { get; set; }
        public string DateCreated { get; set; }
        public String Title { get; set; }
        public String Sender { get; set; }
        public string status { get; set; }
        public String Type { get; set; }
        public String NotifLike { get; set; }
        public String NotifType { get; set; }
        public String isShow { get; set; }
        public String isSeen { get; set; }

    }
    public class GetGreetingsResult
    {
        public List<Greetings> Greetings;
    }
    public class LoginVue
    {
        public String LoginID { get; set; }
        public String Password { get; set; }

        public String Picture { get; set; }

    }

    public class GetLoginVueResult
    {
        public List<LoginVue> LoginVue;
    }

    public class GetDatabase
    {
        public String ClientName { get; set; }
        public string ClientLink { get; set; }
    }

    public class ChangePass
    {
        public String EmpID { get; set; }
        public String NewPassword { get; set; }

    }

    public class Emails
    {
        public String Email { get; set; }
        public String EmpID { get; set; }

        public String Password { get; set; }
        public String Code { get; set; }
        public String CN { get; set; }

    }

    public class Ads
    {
        public String Image { get; set; }
        public String LoanType { get; set; }
        public String LoanID { get; set; }
    }

    public class GetAdsResult
    {
        public List<Ads> Ads;
        public string myreturn { get; set; }
    }
    public class AdsDetails
    {
        public String PaymentDate { get; set; }
        public String LoanAmount { get; set; }
        public String MaxAmount { get; set; }
    }

    public class GetAdsDetails
    {
        public List<AdsDetails> AdsDetails;
        public string myreturn { get; set; }
    }

    public class Adsparam
    {
        public String EmpID { get; set; }
        public String LoanID { get; set; }
    }


    // News And Update Start
    public class NewsAndUpdateModel
    {
        public string CN { get; set; }
        public string NewsID { get; set; }
        public string EmpID { get; set; }
        public string NewLikeCount { get; set; }
        public string NotifType { get; set; }
    }
    public class GetNewsAndUpdate
    {
        public string Image { get; set; }
        public string Title { get; set; }
        public string Message { get; set; }
        public string SenderName { get; set; }
        public string SenderID { get; set; }
        public string SenderImage { get; set; }
        public string IsSeen { get; set; }
        public string NewAndUpdateID { get; set; }
        public string Date { get; set; }
        public string Likes { get; set; }
        public string NotifLike { get; set; }
        public string NotifType { get; set; }
        public string IsShow { get; set; }


    }
    public class getMobileAccess
    {
        public String isAccess { get; set; }

    }

    public class employee
    {

        public String CN { get; set; }
        public String empid { get; set; }
    }
    public class NewsAndUpdateList
    {
        public List<GetNewsAndUpdate> GetNewsAndUpdate;
        public string myreturn;
    }
    // News And Update End


  



    // SURVEY Start
    public class GetSurvey
    {
        public string SurveyID { get; set; }
        public string SurveyTitle { get; set; }
        public string QuestionNum { get; set; }
        public string Question { get; set; }
        public string QuestionType { get; set; }
        public string ScaleMinGuide { get; set; }
        public string ScaleMaxGuide { get; set; }
        public string ScaleMinVal { get; set; }
        public string ScaleMaxVal { get; set; }

        public List<GetSurveyList> Choices { get; set; }
    }

    public class GetSurveyList
    {
        public string Choice { get; set; }
        public string Survey { get; set; }
    }



    public class SList
    {
        public string SurveyID { get; set; }
        public string SurveyTitle { get; set; }
        public string SurveyDescription { get; set; }
        public List<SurveyList> SurveyList { get; set; }


    }
    public class SurveyList
    {
        //public string Choice { get; set; }
        public string QuestionNum { get; set; }
        public string Question { get; set; }
        public string QuestionType { get; set; }
        public List<SChoiceList> Choices { get; set; }
        public string MinScale { get; set; }
        public string MaxScale { get; set; }
    }
    public class SurveyListALL
    {
        public List<SurveyList> SurveyList;
    }

    public class SChoiceList
    {
        public String Choicedata;
    }



    public class surveyL
    {
        public List<SList> Survey;
        public string myreturn;
    }
    // SURVEY End


    //---------------------------------------------------------------Revised Change DB Start------------------------------------------------------------------------
    public class Loginv2
    {
        public String LoginID { get; set; }
        public String Password { get; set; }
        public String FaceID { get; set; }
        public String Coordinates { get; set; }
        public String CN { get; set; }
    }

    public class BundyParams
    {
        public string CN { get; set; }
        public string base64 { get; set; }
        public string location { get; set; }
        public string EmpID { get; set; }
        public DateTime DTInserted { get; set; } 
    }
    public struct Name
    {
        public string firstName;
        public string middleName;
        public string lastName;

        public Name(string _firstName, string _middleName, string _lastName)
        {
            firstName = _firstName;
            middleName = _middleName;
            lastName = _lastName;
        }
    }
    public class BundyReturnParams
    {
        public string EmpID { get; set; }
        public string firstName { get; set; }
        public string middleName { get; set; }
        public string lastName { get; set; }
        public string myReturn { get; set; }
    }

    public class AdminSettingv2
    {
        public string EmpID { get; set; }
        public string GeoLocStatus { get; set; }
        public string ElevationStatus { get; set; }
        public string DefaultLoc { get; set; }
        public string CN { get; set; }
    }

    public class GetAdminSettingsResultv2
    {
        public List<AdminSettingv2> AdminSetting;
        public string myreturn;
    }

    public class Adsparamv2
    {
        public String EmpID { get; set; }
        public String LoanID { get; set; }
        public String CN { get; set; }
    }

    public class employeev2
    {
        public String empid { get; set; }
        public String CN { get; set; }
    }

    public class ChangePassv2
    {
        public String EmpID { get; set; }
        public String NewPassword { get; set; }
        public string CN { get; set; }
    }

    public class GetBrgyv2
    {
        public string CN { get; set; }
        public string ZipCode { get; set; }
    }
    public class BrgyListv2
    {
        public string BrgyID { get; set; }
        public string BrgyName { get; set; }
    }
    public class Brgyv2
    {
        public List<BrgyListv2> BrgyList;
        public string myreturn { get; set; }
    }

    public class GetNewsAndUpdatev2
    {
        public string Image { get; set; }
        public string Title { get; set; }
        public string Message { get; set; }
        public string SenderName { get; set; }
        public string SenderID { get; set; }
        public string IsSeen { get; set; }
        public string NewAndUpdateID { get; set; }
        public string CN { get; set; }
    }
    //---------------------------------------------------------------Revised Change DB End------------------------------------------------------------------------

    //Forgot Password Start
    public class ForgotPassword
    {
        public string EMAIL { get; set; }
        public string RANDOMCODE { get; set; }
        public string VALID { get; set; }
        public string PASSWORD { get; set; }
        public string GENERATECODE { get; set; }
        public string COMPANYNAME { get; set; }
        public string CN { get; set; }

    }
    public class CheckDefault
    {
        public String LoginID { get; set; }
        public String Password { get; set; }
        public String isForget { get; set; }
        public String NTID { get; set; }
        public String CN { get; set; }
        public String DefaultPassword { get; set; }
        public String CustomPassword { get; set; }
    }
    //Forgot Password End
    public class DHRSendAttachParam
    {
        public string CN { get; set; }
        public string EmpID { get; set; }
        public string LoanAmount { get; set; }
    }
    public class FaceToken
    {
        public String CN { get; set; }
        public String LoginID { get; set; }
        public String TokenID { get; set; }
        public DateTime DTInserted { get; set; }
    }
    public class LuxParam
    {
        public string CN { get; set; }
        public string LoginID { get; set; }
    }
    public class qrparams
    {
        public string CN { get; set; }
        public string EmpID { get; set; }
        public string Time { get; set; }
    }
    public class qrparamsv2
    {
        public string CN { get; set; }
        public String[] listEmpid { get; set; }
    }
    public class qrret
    {
        public string URL { get; set; }
        public string fullname { get; set; }
        public string activity { get; set; }
    }

    public class qrvalidate
    {
        public string EmpID { get; set; }
        public string CN { get; set; }
    }

    public class faceAppInsert
    {
        public string CN { get; set; }
        public string EmpID { get; set; }
        public string BaseID { get; set; }
        public string value { get; set; }
    }

    public class saveQRImage
    {
        public string CN { get; set; }
        public string EmpID { get; set; }
        public string base64 { get; set; }
    }

    public class displayOrgChart
    {
        public string CN { get; set; }
        public string RootID { get; set; }
    }

    public class getRoot
    {
        public string EmpID { get; set; }
        public string EmpName { get; set; }
        public string EmpLevel { get; set; }
    }

    public class getRootList
    {
        public List<getRoot> getRoot;
        public string myreturn { get; set; }
    }

    public class generatePDF
    {
        public string CN { get; set; }
        public string EmpID { get; set; }
        public string PayoutDate { get; set; }
        public string ytd_year { get; set; }
        public string[] id { get; set; }
    }
    public class generatePDFv2
    {
        public string CN { get; set; }
        public string[] EmpID { get; set; }
        public string PayoutDate { get; set; }
        public string ytd_year { get; set; }
        public string SendToEmail { get; set; }
    }
    public class OfflineActivity
    {
        public string CN { get; set; }
        public string[] EmpID { get; set; }
        public string[] RecordDT { get; set; }
        public string[] Reason { get; set; }
    }
    public class PayOutDates
    {
        public string PayOutDate { get; set; }
    }
    public class PayOutDatesParam
    {
        public string CN { get; set; }
        public string Year { get; set; }
        public string Month { get; set; }
    }
}