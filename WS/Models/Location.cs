﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace illimitadoWepAPI.Models
{
    //public class Location
    //{
    //    public string Loc { get; set; }
    //}

    //public class ListAllLocationResult
    //{
    //    public List<Location> Locations;
    //    public string myreturn;
    //}

    public class LocationNotif
    {
        public string ID { get; set; }
        public string EmpID { get; set; }
        public string IS_EmpID { get; set; }
        public string DateTime { get; set; }
        public string NotifTitle { get; set; }
        public string NotifDescription { get; set; }

    }

    public class ListAllLocationNotifResult
    {
        public List<LocationNotif> LocationNotif;
        public string myreturn;
    }

    public class AvailableBenefits
    {
        public string BenefitType { get; set; }
        public string BenefitID { get; set; }
        public string BenefitName { get; set; }
        public string BenefitsAmount { get; set; }
        public string MOP { get; set; }

        public class ListAllAvailableBenefits
        {
            public List<AvailableBenefits> Benefits;
            public string myreturn;
        }
    }

    public class ReimbursableReceiptBenefits
    {
        public string BenefitName { get; set; }
        public string BenefitID { get; set; }

        public class ListReimbursableReceiptBenefits
        {
            public List<ReimbursableReceiptBenefits> Reimbursable;
            public string myreturn;
        }
    }


    public class ZipMunRegCountryParams
    {
        public string ID { get; set; }
        public string Zipcode { get; set; }
        public string Country { get; set; }
        public string Region { get; set; }
        public string Province { get; set; }
        public string Municipal { get; set; }

    }
    public class ZipMunRegCountrys
    {
        public List<ZipMunRegCountryParams> ZipMunRegCountryParams;
        public string myreturn;
    }
    public class BrgyList
    {
        public string BRGYID { get; set; }
        public string BRGYNAME { get; set; }
        public string ZIPCODE { get; set; }
    }

    public class LocationNotifv2
    {
        public string ID { get; set; }
        public string EmpID { get; set; }
        public string IS_EmpID { get; set; }
        public string DateTime { get; set; }
        public string NotifTitle { get; set; }
        public string NotifDescription { get; set; }
        public string CN { get; set; }
    }

    public class SafetyCheck
    {
        public string CN { get; set; }
        public string EmpID { get; set; }
    }

    public class GetSafetyCheck
    {
        public string scDID { get; set; }
        public string scName { get; set; }
        public string scQMID { get; set; }
        public string QuestionTitle { get; set; }
        public string scQDID { get; set; }
        public string QuestionDescription { get; set; }
        public string sDate { get; set; }
        public string eDate { get; set; }
        public string sTime { get; set; }
        public string eTime { get; set; }
    }

    public class ListSafetyCheck
    {
        public List<GetSafetyCheck> getSafetyCheck;
        public string myreturn;
    }

    public class insertSafetyCheckAnswer
    {
        public string CN { get; set; }
        public string EmpID { get; set; }
        public string scQMID { get; set; }
        public string scQDID { get; set; }
    }


}