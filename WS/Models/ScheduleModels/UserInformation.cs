﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace illimitadoWepAPI.Models.ScheduleModels
{
    public class UserInformation
    {
        public string UserEmailID { get; set; }
        public string UserPass { get; set; }
        public string IsActive { get; set; }
        public string ForgotPass { get; set; }
        public string RandomCode { get; set; }

        //user information table
        public string First_Name { get; set; }
        public string Middle_Name { get; set; }
        public string Last_Name { get; set; }
        public string Job_Title { get; set; }
        public string Company { get; set; }
        public string Branch { get; set; }
        public string Phone_Number { get; set; }
        public string Mobile_Number { get; set; }
    }
    public class getPartnerList
    {
        public string PClientID { get; set; }
        public string SubClient_ID { get; set; }
        public string SubClientCode { get; set; }

        public string SubClientName { get; set; }
        public string DateCreated { get; set; }
        public string DateModified { get; set; }
        public string Floor { get; set; }
        public string Building { get; set; }
        public string Street { get; set; }
        public string Barangay { get; set; }
        public string Municipality { get; set; }

        public string Province { get; set; }
        public string Region { get; set; }
        public string Country { get; set; }

        public string ZipCode { get; set; }
        public string CompanyNationality { get; set; }
        public string NumberOfEmployees { get; set; }
        public string CompanyTIN { get; set; }
        public string IsActive { get; set; }


    }
    public class CheckPartnershipAlias
    {
        public string PClientID { get; set; }
        public string PartnerAlias { get; set; }


    }
    public class SubClientParams
    {
        public string PClient_ID { get; set; }
        public string SubClientCode { get; set; }
        public string SubClientName { get; set; }
        public string DateOfIncorporation { get; set; }
        public string Floor { get; set; }
        public string Building { get; set; }
        public string Street { get; set; }
        public string Barangay { get; set; }
        public string Municipality { get; set; }
        public string Province { get; set; }
        public string Region { get; set; }
        public string Country { get; set; }
        public string ZipCode { get; set; }
        public string CompanyNationality { get; set; }
        public string NoOfEmployees { get; set; }
        public string CompanyTIN { get; set; }
        public string Username { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string RepTitle { get; set; }
        public string RepEmail { get; set; }
        public string RepContact { get; set; }
    }
}