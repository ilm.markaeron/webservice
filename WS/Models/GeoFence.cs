﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace illimitadoWepAPI.Models
{
    public class ClientDetails
    {
        public string ClientName { get; set; }
    }
    public class BranchDetails
    {
        public string BranchID { get; set; }

        public string BranchName { get; set; }
    }
    public class PerimeterDetails
    {
        public string ClientName { get; set; }

        public string RowID { get; set; }

        public string BranchName { get; set; }

        public string Latitude { get; set; }

        public string Longitude { get; set; }

        public string Range { get; set; }

        public string EmpID { get; set; }

        public string ModifiedBy { get; set; }

        public string ModifiedDate { get; set; }
    }

}