﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace illimitadoWepAPI.Models
{
    public class HomeBuilder
    {
    }
    public class selectuseracct
    {
        public int id { get; set; }
        public string firstname { get; set; }
        public string middlename { get; set; }
        public string lastname { get; set; }
        public string email { get; set; }
        public string company_name { get; set; }
        public string zipcode { get; set; }
        public string city { get; set; }
        public string street { get; set; }
        public string business_phone { get; set; }
        public string website { get; set; }
        public string username { get; set; }
        public string password { get; set; }
        public string accstatus { get; set; }
    }
}