﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace illimitadoWepAPI.Models
{
    public class PayslipAuthModel
    {
        public string key { get; set; }
    }
    public class PAUserLogin
    {
        public string username { get; set; }
        public string password { get; set; }
    }

    public class LoginPA
    {
        public string username  { get; set; }
        public string password  { get; set; }
        public string key       { get; set; }
    }

    public class UserPA
    {
        public string role { get; set; }
        public string name { get; set; }
        public string parameter1 { get; set; }
        public string parameter2 { get; set; }
    }

    public class PayslipModelPA
    {
        //public List<employee_number> EmployeeNumber { get; set; }
        public string Code { get; set; }
        public string PayoutDate { get; set; }
    }

    public class employee_number
    {
        public string _employee_number { get; set; }
    }

    public class ConnectionStringModel
    {
        public string DB_Master     { get; set; }
        public string instance_name { get; set; }
        public string user_name     { get; set; }
        public string user_hash     { get; set; }
        public string catalog       { get; set; }
    }

    public class PayslipURL
    {
        public string url { get; set; }
    }
}