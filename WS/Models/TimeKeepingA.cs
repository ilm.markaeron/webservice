﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace illimitadoWepAPI.Models
{
    public class TimeKeeping
    {
        public class Daily
        {
            public string SchedDate { get; set; }
            public string SchedDay { get; set; }
            public string SchedIN { get; set; }
            public string SchedOUT { get; set; }
            public string TotalTimeMin { get; set; }
            public string SchedType { get; set; }
            public string DateFormat { get; set; }
            public string EmpID { get; set; }
            public string ForApproval { get; set; }
            public string SeriesID { get; set; }
            public string Name { get; set; }
            public string Reason { get; set; }
            public string ReasonID { get; set; }

        }
        public class GetDailyResult
        {
            public List<Daily> Daily;
            public string myreturn;
        }

        public class DailyParameters
        {
            public string NTID { get; set; }
            public string Start_Date { get; set; }
            public string End_Date { get; set; }
            public string Coordinates { get; set; }
            public string NearestEstablishment { get; set; }
            public string CN { get; set; }
            public string SeriesID { get; set; }
        }

        // Nhoraisa Insert Survey
        public class CarInformation
        {
            public string CN { get; set; }
            public string _EmpID { get; set; }
            public string _Own { get; set; }
            public string _isRemindedDue { get; set; }
            public string _Brand { get; set; }
            public string _Model { get; set; }
            public string _Variant { get; set; }
            public string _DatePurchased { get; set; }
            public string _YearMake { get; set; }
            public string _PlateNum { get; set; }
            public string _isInsured { get; set; }
            public string _PolicyExpDate { get; set; }
            public string _PremiumAmount { get; set; }
            public string _isRemindedExp { get; set; }
            public string _isCarLoan { get; set; }
            public string _DueDate { get; set; }
            public string _AmortizationAmount { get; set; }
            public string _LoanEndMonth { get; set; }
            public string _LoanEndYear { get; set; }
            public string _Url { get; set; }
        }

        public class HomeInformation
        {
            public string CN { get; set; }
            public string _EmpID { get; set; }
            public string _HomeStatus { get; set; }
            public string _HouseType { get; set; }
            public string _PayOften { get; set; }
            public string _NextPayDue { get; set; }
            public string _PayDueDate { get; set; }
            public string _RentAmount { get; set; }
            public string _isRemindedHoa { get; set; }
            public string _PayOftenHoa { get; set; }
            public string _NextPayDueHoa { get; set; }
            public string _PayDueDateHoa { get; set; }
            public string _HoaAmount { get; set; }
            public string _LoanBank { get; set; }
            public string _LoanTerm { get; set; }
            public string _LoanTermMonth { get; set; }
            public string _LoanStartMonth { get; set; }
            public string _LoanStartYear { get; set; }
            public string _PayOftenLoan { get; set; }
            public string _NextPayDueLoan { get; set; }
            public string _PayDueDateLoan { get; set; }
            public string _PayMonthlyAmount { get; set; }
            public string _Url { get; set; }
        }

        public class SchoolFeeInformation
        {
            public string CN { get; set; }
            public string _EmpID { get; set; }
            public string _isPayingTuition { get; set; }
            public string _SchoolName { get; set; }
            public string _DownPaymentAmount { get; set; }
            public string _1stQAmount { get; set; }
            public string _1stQDate { get; set; }
            public string _2ndQAmount { get; set; }
            public string _2ndQDate { get; set; }
            public string _3rdQAmount { get; set; }
            public string _3rdQDate { get; set; }
            public string _4thQAmount { get; set; }
            public string _4thQDate { get; set; }
        }

        public class LoanInformation
        {
            public string CN { get; set; }
            public string _EmpID { get; set; }
            public string _isOtherLoan { get; set; }
            public string _BankLoan { get; set; }
            public string _LoanTermNum { get; set; }
            public string _LoanTerm { get; set; }
            public string _LoanStartMonth { get; set; }
            public string _LoanStartYears { get; set; }
            public string _PayOften { get; set; }
            public string _NextPayDue { get; set; }
            public string _PayDueDate { get; set; }
            public string _PayMonthlyAmount { get; set; }
            public string _Url { get; set; }
        }

        public class UtlitiesInformation
        {
            public string CN { get; set; }
            public string _EmpID { get; set; }
            public string _ServiceType { get; set; }
            public string _ProviderName { get; set; }
            public string _AccountNumber { get; set; }
            public string _MonthlyFee { get; set; }
            public string _DueDate { get; set; }
        }

        public class ForBreaks
        {
            public string CN { get; set;}
            public string NTID { get; set; }
            public string ReasonID{ get; set; }
            public string Coordinates { get; set; }
            public string NearestEstablishment { get; set; }
           // public string StartOrEnd { get; set; }
        }

        public class UpadateFaceID
        {
            public string NTID { get; set; }
            public string FaceID { get; set; }
  
        }


        public class ListAllActivityByRange
        {
            public string EmpID { get; set; }
            public string SeriesID { get; set; }
            public string StartTIme { get; set; }
            public string StartEnd { get; set; }
            public string ReasonID { get; set; }
            public string ReasonDesc { get; set; }
            public string IsPaid { get; set; }
            public string SchedDate { get; set; }
            public string HourWork { get; set; }
            public string ActivityStatus { get; set; }
            public string Day { get; set; }
            public string SchedDateStatus { get; set; }
        }
        public class ListAllActivityResult
        {
            public List<ListAllActivityByRange> ListAllActivity;
            public string myreturn;
        }

        public class Hoursworked
        {
            public string EmpID { get; set; }
            //public string SeriesID { get; set; }
            //public string StartTIme { get; set; }
            //public string StartEnd { get; set; }
            //public string ReasonID { get; set; }
            //public string ReasonDesc { get; set; }
            //public string isPaid { get; set; }
            public string SchedDate { get; set; }
            public string HourWork { get; set; }
            public string Schedule { get; set; }
            public string Day { get; set; }
            public string TotalTime { get; set; }
        }
        public class ListAllHoursworked
        {
            public List<Hoursworked> ListAllActivity;
            public string myreturn;
        }

        //public class GetLogs
        //{
        //    public string SeriesID { get; set; }
        //    public string EmpID { get; set; }
        //    public string EmpName { get; set; }
        //    public string StartTime { get; set; }
        //    public string EndTime { get; set; }
        //    public string Reason { get; set; }
        //    public string SchedDate { get; set; }
        //    public string TotalTime { get; set; }
        //    public string Status { get; set; }
        //    public string Approver { get; set; }

        //}

        //public class GetLogsID
        //{
        //    public string EmpID { get; set; }
        //}

        //public class Logs
        //{
        //    public List<GetLogs> LogsListData;
        //    public string _myreturn { get; set; }
        //}



        //public class GetLeaves
        //{
        //    public string ID { get; set; }
        //    public string EmpID { get; set; }
        //    public string EmpName { get; set; }
        //    public string ManagerID { get; set; }
        //    public string ManagerName { get; set; }
        //    public string LeaveDate { get; set; }
        //    public string LeaveType { get; set; }
        //    public string DateApplied { get; set; }
        //    public string Halfday { get; set; }
        //    public string LeaveID { get; set; }
        //    public string Gender { get; set; }
        //    public string Expired { get; set; }
        //    public string Approver { get; set; }

        //}

        //public class GetLeavesID
        //{
        //    public string EmpID { get; set; }
        //}

        //public class Leaves
        //{
        //    public List<GetLeaves> LeavesListData;
        //    public string _myreturn { get; set; }
        //}

        //public class UpdateisSeen
        //{
        //    public string Type { get; set; }
        //    public string SeriesID { get; set; }
        //}

        //public class ApproveDenyLeaves
        //{
        //    public string SeriesID { get; set; }
        //    public string EmpID { get; set; }
        //    public string Status { get; set; }

        //}

        //public class ApproveDenyLogs
        //{
        //    public string SeriesID { get; set; }
        //    public string EmpID { get; set; }
        //    public string Status { get; set; }

        //}


        public class ListAllReasonBreakTypes
        {
            public List<ListAllBreakReasonTypes> ListAllResonType;
            public string myreturn;
        }

        public class AvailableLastSeries
        {
            public string LastSeries { get; set; }

        }


        public class ListAllBreakReasonTypes
        {
            public string ReasonID { get; set; }
            public string ReasonDesc { get; set; }

        }


        public class ListAllReasonType
        {
            public string ReasonID { get; set; }
            public string ReasonDesc { get; set; }
        }
        public class ListAllReasonTypeResult
        {
            public List<ListAllReasonType> ListAllReasonTypes;
            public string myreturn;
        }

        public class ListAllSchedulebyRange
        {
            public string EmpID { get; set; }
            public string Start_Time { get; set; }
            public string End_Time { get; set; }
            public string SchedDate { get; set; }

        }
        public class ListAllSchedulebyRangeResult
        {
            public List<ListAllSchedulebyRange> ListAllSchedule;
            public string myreturn;
        }

        public class ListAllSwapRequest
        {
            public string EMPID { get; set; }
            public string SwapType { get; set; }
            public string Schedule { get; set; }
            public string SwapID { get; set; }
        }

        public class ListAllSwapRequesteResult
        {
            public List<ListAllSwapRequest> ListAllSwapRequests;
            public string myreturn;
        }

        public class LastActivity
        {
            public string LastReasonID { get; set; }
            public string LastType { get; set; }
        }
        public class ListLastActivityResult
        {
            public List<LastActivity> ListLastActivity;
            public string myreturn;
        }

        //---------------------------------------------------------------Revised Change DB Start------------------------------------------------------------------------

        public class DailyParametersv2
        {
            public string NTID { get; set; }
            public string Start_Date { get; set; }
            public string End_Date { get; set; }
            public string Coordinates { get; set; }
            public string NearestEstablishment { get; set; }
            public string CN { get; set; }
        }

        public class ForBreaksv2
        {
            public string NTID { get; set; }
            public string ReasonID { get; set; }
            public string CN { get; set; }
        }

        public class ListAllActivityByRangev2
        {
            public string EmpID { get; set; }
            public string SeriesID { get; set; }
            public string StartTIme { get; set; }
            public string StartEnd { get; set; }
            public string ReasonID { get; set; }
            public string ReasonDesc { get; set; }
            public string IsPaid { get; set; }
            public string SchedDate { get; set; }
            public string HourWork { get; set; }
            public string ActivityStatus { get; set; }
            public string Day { get; set; }
            public string SchedDateStatus { get; set; }
            public string CN { get; set; }
            public string EMPCOMMENT { get; set; }
            public string APPROVERCOMMENT { get; set; }
            public string FILEPATH { get; set; }
            public string Coordinates { get; set; }
            public string NearestEstablishment { get; set; }
        }

        public class Dailyv2
        {
            public string SchedDate { get; set; }
            public string SchedDay { get; set; }
            public string SchedIN { get; set; }
            public string SchedOUT { get; set; }
            public string TotalTimeMin { get; set; }
            public string SchedType { get; set; }
            public string DateFormat { get; set; }
            public string EmpID { get; set; }
            public string ForApproval { get; set; }
            public string SeriesID { get; set; }
            public string Name { get; set; }
            public string Reason { get; set; }
            public string CN { get; set; }
        }

        public class UpadateFaceIDv2
        {
            public string NTID { get; set; }
            public string FaceID { get; set; }
            public string CN { get; set; }
        }

        public class DailyParametersv3
        {
            public string NTID { get; set; }
            public string Start_Date { get; set; }
            public string End_Date { get; set; }
            public string Coordinates { get; set; }
            public string NearestEstablishment { get; set; }
            public string IpLocation { get; set; }
            public string Status { get; set; }
            public string RequestEarly { get; set; }
            public string RequestLate { get; set; }
            public string CN { get; set; }
        }
        #region 2010-10-10
        public class GetSegmentSetv1
        {
            public List<DHRSegmentSetv1> SegmentSettingsList;
            public string myreturn;
        }


        public class DHRSegmentSetv1
        {
            public string CN { get; set; }
            public string ROWID { get; set; }

            public string SEGMENTID { get; set; }
            public string SEGMENTPLATFORM { get; set; }
            public string ISFACIALRECOG { get; set; }
            public string ISGEOFENCING { get; set; }
            public string ISGEOLOCATION { get; set; }

            public string REASONDESC { get; set; }
            public string CATEGORY { get; set; }
            public string REASONDESCRIPTION { get; set; }
            public string ALLOWDELETE { get; set; }
            

        }
        #endregion
        #region 2018-10-15
        public class DHRSEGMENTV1
        {
            public string CN { get; set; }
            public string SEGMENTID { get; set; }
            public string SEGMENT { get; set; }
            public string SEGMENTTYPE { get; set; }
            public string SEGMENTDESC { get; set; }
            public string ISACTIVE { get; set; }
            public string ISMULTIPLE { get; set; }
            public string LOGINID { get; set; }

        }
        public class DHRSEGMENTARRAYV1
        {
            public string CN { get; set; }
            public bool FACIALISACTIVE { get; set; }
            public bool GEOISACTIVE { get; set; }
            public bool GEOLOCATIONISACTIVE { get; set; }
            public string ROWID { get; set; }

        }
        #endregion

        public class SwapModel
        {
            public string CN { get; set; }
            public string EmpID { get; set; }
            public string RequestDate { get; set; }
            public string OldDate { get; set; }
            public string Type { get; set; }
            public string SelectedType { get; set; }

            public string EmpLevel { get; set; }
            public string EmpCat { get; set; }

            public string SwapID { get; set; }
            public string SystemLevel { get; set; }
            public string SystemCategory { get; set; }
            public string CanSwapWithLevel { get; set; }
            public string CanSwapWithCategory { get; set; }
            public string CanSwapFromBranch { get; set; }
            public string CanSwapFromSegment { get; set; }
            public string CanSwapFromBusinessUnit { get; set; }
            public string ID { get; set; }
        }
        public class SwapSched
        {
            public string EmpID { get; set; }
            public string Sched { get; set; }
        }
        public class SwapSchedList
        {
            public List<SwapSched> swapDateList { get; set; }
        }

        public class WeekSched
        {
            public string SchedDate { get; set; }
            public string Schedule { get; set; }
            public string SeriesID { get; set; }
            public string SchedTitle { get; set; }
        }
        public class ListWeekSchedResult
        {
            public List<WeekSched> ListWeekSched;
            public string myreturn;
        }
        public class SwapSelectedSched
        {
            public string SchedDate { get; set; }
            public string EmpID { get; set; }
            public string Shift { get; set; }
            public string Lunch { get; set; }
            public string Break1 { get; set; }
            public string Break2 { get; set; }
            public string LeaveDate { get; set; }
        }
        public class ListSwapSelectedSched
        {
            public List<SwapSelectedSched> ListSelectedSched;
            public string myReturn;
        }
        public class SwapModelv2
        {
            public string CN { get; set; }
            public string EmpID { get; set; }
            public string SelectedDate { get; set; }
            public string SelectedSched { get; set; }
            public string SwapID { get; set; }
            public string SwapStatus { get; set; }
        }
        public class SwapSelectedSchedv2
        {
            public string EmpID { get; set; }
            public string Schedule { get; set; }
            public string NewSchedule { get; set; }
        }
        public class ListSwapSelectedSchedv2
        {
            public List<SwapSelectedSchedv2> listSwapSelectedSchedv2 { get; set; }
            public string myReturn { get; set; }
        }

        public class NotifParam
        {
            public string EMPID { get; set; }
            public string CN { get; set; }
        }

        public class ListAllLeaveNotification
        {
            public string EmpID { get; set; }
            public string SeriesID { get; set; }
            public string EmpName { get; set; }
            public string LeaveDate { get; set; }
            public string LeaveStatus { get; set; }
            public string DateApproved { get; set; }
            public string LeaveType { get; set; }
            public string NotifLike { get; set; }
            public string NotifType { get; set; }
            public string isShow { get; set; }
            public string isSeen { get; set; }
        }
        public class ListAllLeaveNotificationResult
        {
            public List<ListAllLeaveNotification> ListAllLeave;
            public string myreturn;
        }

        public class ListAllTimeNotification
        {
            public string EmpID { get; set; }
            public string SeriesID { get; set; }
            public string EmpName { get; set; }
            public string SchedDate { get; set; }
            public string StartTIme { get; set; }
            public string StartEnd { get; set; }
            public string ReasonName { get; set; }
            public string ActivityStatus { get; set; }
            public string NotifLike { get; set; }
            public string NotifType { get; set; }
            public string isShow { get; set; }
            public string isSeen { get; set; }
        }
        public class ListAllTimeNotificationResult
        {
            public List<ListAllTimeNotification> ListAllTime;
            public string myreturn;
        }

        public class ListAllCountNotification
        {
            public string Time { get; set; }
            public string Leave { get; set; }
            public string News { get; set; }
            public string Greet { get; set; }
            public string All { get; set; }

        }
        public class ListAllCountNotificationResult
        {
            public List<ListAllCountNotification> ListAllCount;
            public string myreturn;
        }

        public class checkMC
        {
            public string EMPID { get; set; }
            public string CN { get; set; }
            public string PARAM { get; set; }
        }
        //---------------------------------------------------------------Revised Change DB End------------------------------------------------------------------------

    }
}