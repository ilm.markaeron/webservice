﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace illimitadoWepAPI.Models
{
    public class ReportModel
    {

    }
    #region Sample Models
    public class SampleInfo
    {
        public string id { get; set; }
        public string name { get; set; }
        public int age { get; set; }
    }

    public class DataToInputInJarFile
    {
        public string inputTemplateFile { get; set; }
        public string outputFile { get; set; }
        public int fontSize { get; set; }
        public string text1 { get; set; }
        public string text2 { get; set; }
        public string text3 { get; set; }
        public string text4 { get; set; }
        public string text5 { get; set; }
        public string text1Coordinates { get; set; }
        public string text2Coordinates { get; set; }
        public string text3Coordinates { get; set; }
        public string text4Coordinates { get; set; }
        public string text5Coordinates { get; set; }
    }

    public class DatabaseID
    {
        public string empID { get; set; }
    }
    #endregion

    #region SSLoan Class Model
    public class SSLoanParameters
    {
        public string db { get; set; }
        public string CompanyCode { get; set; }
        public string tenantID { get; set; }
        public string month { get; set; }
        public string year { get; set; }
        public string reportID { get; set; }
    }

    public class SSLoanDataTable
    {
        public string companyName { get; set; }
        public string companyAdress { get; set; }
        public string sssNumber { get; set; }
        public string billingMonth { get; set; }
        public string totalAmountPaid { get; set; }
        public string totalRecords { get; set; }
    }
    #endregion

    #region GovTax Class Model
    public class GovTaxParamaters
    {
        public string db { get; set; }
        public string CompanyCode { get; set; }
        public string tenantID { get; set; }
        public string year { get; set; }
    }
    public class GovTaxDataTable
    {
        public string companyName { get; set; }
        public string TaxTable { get; set; }
        public float TaxWithheld { get; set; }
    }
    #endregion

    #region GovR5 Class Model
    public class GovR5Paramaters
    {
        public string db { get; set; }
        public string CompanyCode { get; set; }
        public string tenantID { get; set; }
        public string month { get; set; }
        public string year { get; set; }
        public string reportID { get; set; }
    }
    public class GovR5DataTable
    {
        public string sssNumber { get; set; }
        public string tinNumber { get; set; }
        public string employerName { get; set; }
        public string address { get; set; }
        public float sssAmount_ER { get; set; }
        public float sssAmount_EC { get; set; }
    }
    #endregion

    #region PHQTR Class Model
    public class PHQTRParamaters
    {
        public string db { get; set; }
        public string CompanyCode { get; set; }
        public string tenantID { get; set; }
        public string month { get; set; }
        public string year { get; set; }
        public string reportID { get; set; }
    }
    public class PHQTRDataTable
    {
        public string philhealthNumber { get; set; }
        public string lastName { get; set; }
        public string firstName { get; set; }
        public string middleName { get; set; }
        public float PS { get; set; }
        public float ES { get; set; }
    }
    #endregion
    public class HDMPayList
    {

        public string db { get; set; }
        public string CN { get; set; }
        public string code { get; set; }
        public string mmonth { get; set; }
        public string yyear { get; set; }
        public string firstname { get; set; }
        public string middlename { get; set; }
        public string lastname { get; set; }
        public string birthdate { get; set; }
        public string tinnum { get; set; }
        public string ee { get; set; }
        public string er { get; set; }
        public string companycode { get; set; }
        public string companyname { get; set; }
        public string totalee { get; set; }
        public string totaler { get; set; }
        public string finaltotal { get; set; }

    }
    public class SSPHH2List
    {
        public string db { get; set; }
        public string CN { get; set; }
        public string code { get; set; }
        public string mmonth { get; set; }
        public string yyear { get; set; }
        public string empid { get; set; }
        public string sss { get; set; }
        public string hdmf { get; set; }
        public string empname { get; set; }
        public string employeephealth { get; set; }
        public string employerphealth { get; set; }
        public string totalemployeephealth { get; set; }
        public string totalemployerphealth { get; set; }
        public string finaltotal { get; set; }
        public string payoutdate { get; set; }
        public string companycode { get; set; }
        public string companyname { get; set; }
        public string startdate { get; set; }
        public string enddate { get; set; }
        public string tenantID { get; set; }
        public string reportID { get; set; }
    }

    public class PWHTAXMList
    {
        public string db { get; set; }
        public string CN { get; set; }
        public string code { get; set; }
        public string mmonth { get; set; }
        public string yyear { get; set; }
        public string empid { get; set; }
        public string companycode { get; set; }
        public string companyname { get; set; }
        public string empname { get; set; }
        public string whtax { get; set; }
        public string taxstatus { get; set; }
        public string tinnum { get; set; }
        public string startdate { get; set; }
        public string enddate { get; set; }
        public string finaltotal { get; set; }
    }
    public class SSPHHDList
    {
        public string db { get; set; }
        public string code { get; set; }
        public string mmonth { get; set; }
        public string yyear { get; set; }
        public string Empid { get; set; }
        public string SSSnum { get; set; }
        public string HDMF { get; set; }
        public string EmployeeName { get; set; }
        public string SSSPremEmployee { get; set; }
        public string SSSPremEcEmployer { get; set; }
        public string EmployerEC { get; set; }
        public string EmployeePhilHealth { get; set; }
        public string EmployerPhilHealth { get; set; }
        public string PagIbigAmount { get; set; }
        public string PagibigERAmount { get; set; }
        public string TotalEmployeeSSS { get; set; }
        public string TotalEmployerSSS { get; set; }
        public string TotalEmployerECSSS { get; set; }
        public string FinalTotalSSS { get; set; }
        public string TotalEmployeePhilHealth { get; set; }
        public string TotalEmployerPhilHealth { get; set; }
        public string FinalTotalPhilHealth { get; set; }
        public string TotalPagIbigAmount { get; set; }
        public string TotalPagibigERAmount { get; set; }
        public string FinalTotalPagibig { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string CompanyCode { get; set; }
        public string CompanyName { get; set; }
    }

    public class PAGQTRList
    {
        public string tenantID { get; set; }
        public string reportID { get; set; }
        public string db { get; set; }
        public string code { get; set; }
        public string mmonth { get; set; }
        public string yyear { get; set; }
        public string Empid { get; set; }
        public string HDMF { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string PagIbigAmount { get; set; }
        public string PagibigERAmount { get; set; }
        public string TotalEEER { get; set; }
        public string PeriodCover { get; set; }
        public string TotalPagIbigAmount { get; set; }
        public string TotalPagibigERAmount { get; set; }
        public string FinalTotalPagibig { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string CompanyCode { get; set; }
        public string CompanyAddress { get; set; }
        public string CompanyName { get; set; }
        public string ERNumber { get; set; }

    }
    public class PHER2List
    {
        public string PHNum { get; set; }
        public string Name { get; set; }
        public string Position { get; set; }
        public string Salary { get; set; }
        public string DateEmployed { get; set; }
    }
    public class ParamPAGLM
    {
        public string db { get; set; }
        public string TenantID { get; set; }
        public string Month { get; set; }
        public string Year { get; set; }
        public string reportID { get; set; }
    }
    public class ParamPHER2
    {
        public string db { get; set; }
        public string TenantID { get; set; }
        public string Month { get; set; }
        public string Year { get; set; }
        public string reportID { get; set; }
    }
    public class ParamGOVPH
    {
        public string Company { get; set; }
        public string Month { get; set; }
        public string Year { get; set; }
    }
    public class ParamSSSFIL
    {
        public string db { get; set; }
        public string TenantID { get; set; }
        public string Month { get; set; }
        public string Year { get; set; }
        public string reportID { get; set; }
    }
    public class ParamQTRS
    {
        public string db { get; set; }
        public string TenantID { get; set; }
        public string Month { get; set; }
        public string Year { get; set; }
        public string reportID { get; set; }
    }
    public class ParamSSSR1A
    {
        public string db { get; set; }
        public string TenantID { get; set; }
        public string Month { get; set; }
        public string Year { get; set; }
        public string reportID { get; set; }
    }
    public class GovDueParam
    {
        public string db { get; set; }
        public string tenantid { get; set; }
        public string month { get; set; }
        public string year { get; set; }
        public string seldate { get; set; }
        public string reportID { get; set; }


    }
    public class ParamSSSQTR
    {
        public string db { get; set; }
        public string TenantID { get; set; }
        public string Month { get; set; }
        public string Year { get; set; }
        public string reportID { get; set; }
    }
}
