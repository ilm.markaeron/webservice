﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace illimitadoWepAPI.Models
{
    public class AanyaReportsModel
    {

    }

    #region Report Parameters
    public class ReportParameter
    {
        public string series_code { get; set; }
        public string report_id { get; set; }
        public string tenant_id { get; set; }
        public string tenant_code { get; set; }
        public string employee_id { get; set; }
        public List<employee_id> employee_id_multi { get; set; }
        public string employee_code { get; set; }
        public List<employee_code> employee_code_multi { get; set; }
        public string year { get; set; }
        public string month { get; set; }
        public string date { get; set; }
        public string date_from { get; set; }
        public string date_to { get; set; }
        public string status { get; set; }
        public string type_2316 { get; set; }
    }
    public class employee_id
    {
        public string id { get; set; }
    }
    public class employee_code
    {
        public string code { get; set; }
    }
    #endregion

    public class AanyaConnectionString
    {
        public string isTestMode { get; set; }

        public string instance_name { get; set; }
        public string catalog { get; set; }
        public string user_name { get; set; }
        public string user_hash { get; set; }
        public string _DB_Master { get; set; }

        public string instance_name_test { get; set; }
        public string catalog_test { get; set; }
        public string user_name_test { get; set; }
        public string user_hash_test { get; set; }
    }
}