﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace illimitadoWepAPI.Models
{
    public class LeaveMngt
    {
        public string LeaveDate { get; set; }
        public string LeaveStatus { get; set; }
        public string LeaveType { get; set; }
        public string isHalfDay { get; set; }
        public string isPaid { get; set; }
        public string NTID { get; set; }
        public string Coordinates { get; set; }
        public string LeaveID { get; set; }
        public string EmployeeName { get; set; }
        public string DateApplied { get; set; }
        public string ApprovedCancelDate { get; set; }
        public string ShiftType { get; set; }
        public string EmpReason { get; set; }
        public string EmpComment { get; set; }
        public string ApproverComment { get; set; }
        public string FilePath { get; set; }
        public string LeaveID2 { get; set; }

    }


    public class leaveRequestor
    {
        public string CN { get; set; }
        public String[] LeaveID { get; set; }
    }
    //view slot
    public class FilterLeaveSlot
    {
        public string dayleavetype { get; set; }
        public string leavereason { get; set; }
        public string date { get; set; }
        public string Slot { get; set; }
    }
    public class CheckLeaveSlot
    {
        public List<LeaveSlotReturn> Leave;
        public string myreturn;
    }
    public class checkLeaveParam
    {
        public string EMPID { get; set; }
        public string leaveid { get; set; }
        public string leavereason { get; set; }
        public string startdate { get; set; }
        public string enddate { get; set; }
        public string CN { get; set; }
    }
    public class LeaveSlotReturn
    {
        public string returnslot { get; set; }
    }
    //
    public class SlotDeduction
    {
        public string EMPID { get; set; }
        public string leaveid { get; set; }
        public string leavereason { get; set; }
        public string LeaveDate { get; set; }
        public string CN { get; set; }
        public string type { get; set; }
    }
    public class HoopLeaveSlotDeductionlist
    {
    }
    public class HoopLeaveSlotDeduction
    {
        public List<HoopLeaveSlotDeductionlist> Leave;
        public string myreturn;
    }

    public class LeaveSlotParams
    {
        public String[] Leave { get; set; }
        public String EMPID { get; set; }
        public String startdate { get; set; }
        public String enddate { get; set; }
        public string CN { get; set; }
    }
    //
    public class SlotAvailable
    {
        public string EMPID { get; set; }
        public string Leave { get; set; }
        public string startdate { get; set; }
        public string enddate { get; set; }
        public string CN { get; set; }
    }
    public class ViewLeaveSlotV2list
    {
        public string dayleavetype { get; set; }
        public string leavereason { get; set; }
        public string date { get; set; }
        public string Slot { get; set; }
        public string leaveids { get; set; }
        public string leavestatus { get; set; }
    }

    public class ViewLeaveSlotV2
    {
        public List<ViewLeaveSlotV2list> Leave;
        public string myreturn;
    }
    public class FlightParam
    {
        public string CN { get; set; }
        public string EMPID { get; set; }
    }
    public class FlightBalance
    {
        public List<GetFlightBalance> DHRFlightBalance;
        public string myreturn;
    }
    public class GetFlightBalance
    {
        public string FLIGHTBALANCE { get; set; }

    }
    public class SettingsParameter
    {
        public string CN { get; set; }
        public string SERIESID { get; set; }
    }

    public class GetLeaveResult
    {
        public List<LeaveMngt> Leave;
        public string myreturn;
    }
    public class GetLeaveTypeSetting
    {
        public List<GetLeaveTypeSettinglist> Leave;
        public string myreturn;
    }

    public class LTypeSetting
    {
        public String leaveID { get; set; }
        public string CN { get; set; }
    }
    public class GetLeaveTypeSettings
    {
        public string MOBILE { get; set; }
        public string WEB { get; set; }
        public string ALLOWUSERTRACK { get; set; }

    }

    public class LeaveTypeSettings
    {
        public List<GetLeaveTypeSettings> CompanyBenefitsLeaves;
        public string myreturn;
    }

    public class GetLeaveTypeSettinglist
    {
        public string ID { get; set; }
        public string EmpLevelId { get; set; }
        public string EmpCategory { get; set; }
        public string LeaveID { get; set; }
        public string CreditWhen { get; set; }
        public string NumberLeaveToCredit { get; set; }
        public string ExemptionFrom { get; set; }
        public string ExemptionTo { get; set; }
        public string CashConvertFrom { get; set; }
        public string CashConvertTo { get; set; }
        public string CashConvertPaidOn { get; set; }
        public string MaxCreditsConvert { get; set; }
        public string CarryOverFrom { get; set; }
        public string CarryOverTo { get; set; }
        public string CarryOverMax { get; set; }
        public string DateModified { get; set; }
        public string isCashConvert { get; set; }
        public string isCarryOver { get; set; }
        public string StartAccrue { get; set; }
        public string StartAccrueEvery { get; set; }
        public string StartAccrue_IfDayOption { get; set; }
        public string StartAccrueFrom { get; set; }
        public string StartAccrueNum { get; set; }
        public string WillAccrueInc { get; set; }
        public string StartInc { get; set; }
        public string StartIncEvery { get; set; }
        public string StartInc_IfDayOption { get; set; }
        public string StartIncFrom { get; set; }
        public string DateSet { get; set; }
        public string ExitLeaveConversion { get; set; }
        public string ExitMaxCredit { get; set; }
        public string MaxLeaveAfterLeaveDate { get; set; }
        public string LeavesWithTax { get; set; }
        public string isOffsetLimit { get; set; }
        public string minOffsetOTHrs { get; set; }
        public string maxOffsetOTHrs { get; set; }
        public string MaxLeaveBeforeLeaveDate { get; set; }
    }

    public class FillingParameters
    {
        public string CN { get; set; }
        public string EMPID { get; set; }
        public string LEAVETYPEID { get; set; }
    }

    public class FillingSettings
    {
        public List<GetFillingSettings> GetFillingSettings;
        public string myreturn;
    }
    public class GetFillingSettings
    {
        public string LATEDAY { get; set; }
        public string DATETODAY { get; set; }
        public string LATEFILLING { get; set; }
        public string EARLYFILLING { get; set; }
        public string EARLYDAY { get; set; }
        public string CANCELFILLING { get; set; }
        public string CANCELDAY { get; set; }
    }
    public class AvailableLeave
    {
        //public string CTO { get; set; }
        //public string LastLeave { get; set; }
        public string Ltype { get; set; }
        public string SerialID { get; set; }

        public string LeaveBal { get; set; }
        //public string LastLeave { get; set; }
    }


    public class ListAllSched
    {
        public string EmpID { get; set; }
        public string SchedIn { get; set; }
        public string SchedOut { get; set; }
        public string SchedDate { get; set; }
        //public string LastLeave { get; set; }
    }






    public class AvailableLastLeave
    {
        public string LastLeave { get; set; }

    }

    public class GetAvailableLeavesCTRACK
    {
        public List<AvailableLeaveCTRACK> CTRACTAvailableLeaves;
        public string myreturn;
    }
    public class CTRACKLeaveParameters
    {
        public string CN { get; set; }
        public string EMPID { get; set; }
        public string SELECTEDDATE { get; set; }
        public string LEAVEID { get; set; }
    }

    public class AvailableLeaveCTRACK
    {
        public string LeaveDays { get; set; }
        public string LeaveHours { get; set; }
    }

    public class GetAvailableLeaves
    {
        public List<AvailableLeave> AvailableLeaves;
        public string AvailableLastLeaves;
        public string myreturn;
    }

    public class LeaveParameters
    {
        public string NTID { get; set; }
        public string Start_Date { get; set; }
        public string End_Date { get; set; }

    }
    public class GetAvailableLeavesGen
    {
        public string id { get; set; }
        public string LType { get; set; }
        public string serialID { get; set; }
        public string leaveBalance { get; set; }
        public string leaveDate { get; set; }
        public string leaveHourse { get; set; }
    }
    public class GetAvailableLeavesGenList
    {
        public List<GetAvailableLeavesGen> availableLeavesGen;
        public string AvailableLastLeaves;
        public string myReturn;
    }


    //Start Leaves and Logs - Nhoraisa

    public class GetShowLeaves
    {
        public string ID { get; set; }
        public string EmpID { get; set; }
        public string EmpName { get; set; }
        public string ManagerID { get; set; }
        public string ManagerName { get; set; }
        public string LeaveDate { get; set; }
        public string LeaveType { get; set; }
        public string DateApplied { get; set; }
        public string Halfday { get; set; }
        public string LeaveID { get; set; }
        public string Gender { get; set; }
        public string Expired { get; set; }
        public string Approver { get; set; }
        public string ShiftType { get; set; }
        public string EmpComment { get; set; }
        public string EmpReason { get; set; }
        public string ApproverComment { get; set; }
        public string FilePath { get; set; }
        public string LeaveHours { get; set; }
        public string LeaveMinutes { get; set; }


    }

    public class ShowLeavesID
    {
        public string EmpID { get; set; }
    }

    public class ShowLeaves
    {
        public List<GetShowLeaves> LeavesListData;
        public string _myreturn { get; set; }
    }

    public class GetShowLeavesv2
    {
        public string ID { get; set; }
        public string EmpID { get; set; }
        public string EmpName { get; set; }
        public string ManagerID { get; set; }
        public string ManagerName { get; set; }
        public string LeaveDate { get; set; }
        public string LeaveType { get; set; }
        public string DateApplied { get; set; }
        public string Halfday { get; set; }
        public string LeaveID { get; set; }
        public string Gender { get; set; }
        public string Expired { get; set; }
        public string Approver { get; set; }
        public string ShiftType { get; set; }
        public string EmpComment { get; set; }
        public string EmpReason { get; set; }
        public string ApproverComment { get; set; }
        public string FilePath { get; set; }
        public string LeaveHours { get; set; }
        public string LeaveMinutes { get; set; }
        public string DepartureDate { get; set; }
        public string ReturnDate { get; set; }


    }

    public class ShowLeavesIDV2
    {
        public string EmpID { get; set; }
        public string CN { get; set; }
    }

    public class ShowLeavesv2
    {
        public List<GetShowLeavesv2> LeavesListDatav2;
        public string _myreturn { get; set; }
    }
    public class ShowGetLogs
    {
        public string SeriesID { get; set; }
        public string EmpID { get; set; }
        public string EmpName { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public string Reason { get; set; }
        public string SchedDate { get; set; }
        public string TotalTime { get; set; }
        public string Status { get; set; }
        public string Approver { get; set; }
        public string FilePath { get; set; }
        public string EmpComment { get; set; }
        public string ShiftNum { get; set; }
        public string Location { get; set; }

    }

    public class ShowLogsID
    {
        public string EmpID { get; set; }
    }

    public class ShowLogs
    {
        public List<ShowGetLogs> LogsListData;
        public string _myreturn { get; set; }
    }

    public class GetLogs
    {
        public string SeriesID { get; set; }
        public string EmpID { get; set; }
        public string EmpName { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public string Reason { get; set; }
        public string SchedDate { get; set; }
        public string TotalTime { get; set; }
        public string Status { get; set; }
        public string Approver { get; set; }

    }

    public class GetLogsID
    {
        public string EmpID { get; set; }
    }

    public class Logs
    {
        public List<GetLogs> LogsListData;
        public string _myreturn { get; set; }
    }




    public class GetLeaves
    {
        public string ID { get; set; }
        public string EmpID { get; set; }
        public string EmpName { get; set; }
        public string ManagerID { get; set; }
        public string ManagerName { get; set; }
        public string LeaveDate { get; set; }
        public string LeaveType { get; set; }
        public string DateApplied { get; set; }
        public string Halfday { get; set; }
        //public string LeaveID { get; set; }
        //public string Gender { get; set; }
        //public string Expired { get; set; }
        public string Approver { get; set; }

    }

    public class GetLeavesID
    {
        public string EmpID { get; set; }
    }

    public class Leaves
    {
        public List<GetLeaves> LeavesListData;
        public string _myreturn { get; set; }
    }



    public class UpdateisSeen
    {
        public string Type { get; set; }
        public string SeriesID { get; set; }
    }

    public class ApproveDenyLeaves
    {
        public string SeriesID { get; set; }
        public string EmpID { get; set; }
        public string Status { get; set; }

    }

    public class ApproveDenyLogs
    {
        public string SeriesID { get; set; }
        public string EmpID { get; set; }
        public string Status { get; set; }

    }



    //End Leaves and Logs - Nhoraisa

    public class DataTableAnswerInsert
    {
        public string _EmpID { get; set; }
        public string _SurveyID { get; set; }
        public string _Qno { get; set; }

        public string _Qtype { get; set; }
        public string _Qtion { get; set; }
        public string list { get; set; }
    }



    public class GetSchedule
    {
        public string NTID { get; set; }
        public string Start_Date { get; set; }
        public string End_Date { get; set; }

    }

    public class ScheList
    {

        public string NTID { get; set; }
        public string SchedIN { get; set; }
        public string SchedOut { get; set; }
        public string SchedDate { get; set; }



    }



    public class ScheListResult
    {
        public List<ScheList> SchedList;
        public string myreturn;
    }





    public class GetLeaveNotif
    {
        public List<LeaveNotification> LeaveNotif;
        public string myreturn;
    }
    public class LeaveNotification
    {
        public string ID { get; set; }
        public string ImEmpID { get; set; }
        public string Subordinate { get; set; }
        public string LeaveDate { get; set; }
        public string LeaveType { get; set; }
        public string IsHalfDay { get; set; }

    }
    //---------------------------------------------------------------Revised Change DB Start------------------------------------------------------------------------
    public class ShowLogsIDv2
    {
        public string EmpID { get; set; }
        public string CN { get; set; }
    }

    public class ShowLeavesIDv2
    {
        public string EmpID { get; set; }
        public string CN { get; set; }
    }

    public class LeaveParametersv2
    {
        public string NTID { get; set; }
        public string Start_Date { get; set; }
        public string End_Date { get; set; }
        public string CN { get; set; }
    }
    public class FlightParameters
    {
        public string EMPID { get; set; }
        public string CN { get; set; }
    }
    public class File_new_leave_param
    {
        public string EMPID { get; set; }
        public string LEAVEDATE { get; set; }
        public string LEAVETYPE { get; set; }
        public string LEAVEID { get; set; }
        public string ISHALFDAY { get; set; }
        public string LOCATION { get; set; }
        public string CN { get; set; }
        public string SHIFTTYPE { get; set; }
        public string EMPREASON { get; set; }
        public string EMPCOMMENT { get; set; }
        public string FILEPATH { get; set; }
        public string LEAVESTARTDATE { get; set; }
        public string LEAVEENDDATE { get; set; }
        public string MULTIPLE { get; set; }
        public string leave_id_slot { get; set; }
        //public string LastLeave { get; set; }
    }
    public class FlightRequest
    {
        public string DepartureDate { get; set; }
        public string ReturnDate { get; set; }
        public string CN { get; set; }
        public string EMPID { get; set; }
    }
    public class LeaveMngtv2
    {
        public string LeaveDate { get; set; }
        public string LeaveStatus { get; set; }
        public string LeaveType { get; set; }
        public string isHalfDay { get; set; }
        public string isPaid { get; set; }
        public string NTID { get; set; }
        public string Coordinates { get; set; }
        public string LeaveID { get; set; }
        public string EmployeeName { get; set; }
        public string DateApplied { get; set; }
        public string ApprovedCancelDate { get; set; }
        public string CN { get; set; }
        public string ShiftType { get; set; }
        public string EmpReason { get; set; }
        public string EmpComment { get; set; }
        public string FilePath { get; set; }
        public string LeaveStartDate { get; set; }
        public string LeaveEndDate { get; set; }
        public string Multiple { get; set; }
        public string HOURS { get; set; }
        public string MINUTES { get; set; }
        public string ID { get; set; }
    }

    public class LeaveMngtv3
    {
        public string LeaveDate { get; set; }
        public string LeaveStatus { get; set; }
        public string DateApplied { get; set; }
        public string LeaveType { get; set; }
        public string isHalfDay { get; set; }
        public string isPaid { get; set; }
        public string NTID { get; set; }
        public string Coordinates { get; set; }
        public string LeaveID { get; set; }
        public string EmployeeName { get; set; }
        public string ApprovedCancelDate { get; set; }
        public string CN { get; set; }
        public string ShiftType { get; set; }
        public string EmpReason { get; set; }
        public string EmpComment { get; set; }
        public string FilePath { get; set; }
        public string Rules { get; set; }
        public string Submitted { get; set; }
        public string Gender { get; set; }
        public string LastDateMod { get; set; }
        public string LeaveHours { get; set; }
        public string LeaveMinutes { get; set; }
        public string LeaveRequestStartDate { get; set; }
        public string LeaveRequestEndDate { get; set; }
        public string multiple { get; set; }



    }

    public class GetLeavesIDv2
    {
        public string EmpID { get; set; }
        public string CN { get; set; }
    }

    public class UpdateisSeenv2
    {
        public string Type { get; set; }
        public string SeriesID { get; set; }
        public string CN { get; set; }
    }

    public class GetLogsIDv2
    {
        public string EmpID { get; set; }
        public string CN { get; set; }
    }

    public class DataTableAnswerInsertv2
    {
        public string _EmpID { get; set; }
        public string _SurveyID { get; set; }
        public string _Qno { get; set; }
        public string _Qtype { get; set; }
        public string _Qtion { get; set; }
        public string list { get; set; }
        public string CN { get; set; }
    }

    public class ApproveDenyLogsv2
    {
        public string SeriesID { get; set; }
        public string EmpID { get; set; }
        public string Status { get; set; }
        public string CN { get; set; }
        public string ApproverComment { get; set; }
    }

    public class ApproveDenyLeavesv2
    {
        public string SeriesID { get; set; }
        public string EmpID { get; set; }
        public string Status { get; set; }
        public string CN { get; set; }
        public string ApproverComment { get; set; }
    }
    public class GetLeaveResultv2
    {
        public List<LeaveMngt> NonCancellable;
        public string myreturn;
    }
    public class leaveDHRv1
    {
        public string LeaveDate { get; set; }
        public string LeaveStatus { get; set; }
        public string LeaveType { get; set; }
        public string isHalfDay { get; set; }
        public string isPaid { get; set; }
        public string NTID { get; set; }
        public string Coordinates { get; set; }
        public string LeaveID { get; set; }
        public string EmployeeName { get; set; }
        public string DateApplied { get; set; }
        public string ApprovedCancelDate { get; set; }
        public string CN { get; set; }
        public string ID { get; set; }
    }
    //---------------------------------------------------------------Revised Change DB End------------------------------------------------------------------------

    public class LeaveMngtv4
    {
        public string LeaveDate { get; set; }
        public string LeaveStatus { get; set; }
        public string DateApplied { get; set; }
        public string LeaveType { get; set; }
        public string isHalfDay { get; set; }
        public string isPaid { get; set; }
        public string NTID { get; set; }
        public string Coordinates { get; set; }
        public string LeaveID { get; set; }
        public string EmployeeName { get; set; }
        public string ApprovedCancelDate { get; set; }
        public string CN { get; set; }
        public string ShiftType { get; set; }
        public string EmpReason { get; set; }
        public string EmpComment { get; set; }
        public string FilePath { get; set; }
        public string Rules { get; set; }
        public string Submitted { get; set; }
        public string Gender { get; set; }
        public string LastDateMod { get; set; }
        public string LeaveHours { get; set; }
        public string LeaveMinutes { get; set; }
        public string LeaveRequestStartDate { get; set; }
        public string LeaveRequestEndDate { get; set; }
        public string multiple { get; set; }



    }
    public class emprequestleave
    {
        public string empid { get; set; }
        public string timefiled { get; set; }
        public string LeaveID { get; set; }
        public string LeaveDate { get; set; }
        public string IsHalfDay { get; set; }
        public string IsPaid { get; set; }
        public string LeaveType { get; set; }
        public string LeaveRequestStartDate { get; set; }
        public string LeaveRequestEndDate { get; set; }
        public string LeaveHours { get; set; }
        public string LeaveMinutes { get; set; }
        public string LeaveComment { get; set; }
        public string CN { get; set; }
        public string multiple { get; set; }
    }
    public class LeaveConversion
    {
        public string EmpID { get; set; }
        public string CN { get; set; }
        public string LeaveID { get; set; }
        public string ConversionAmount { get; set; }
        public string ID { get; set; }
        public string Status { get; set; }
    }
    public class GetLeaveTypeConversion
    {
        public List<LeaveTypeAvailable> AvailableLeaveType;
        public string myreturn;
    }
    public class LeaveTypeAvailable
    {
        public string LeaveID { get; set; }
        public string LeaveType { get; set; }
    }
    public class GetConversionSet
    {
        public List<GetAllConversionSet> AllConversionSet;
        public string myreturn;
    }
    public class GetAllConversionSet
    {
        public string LeaveID { get; set; }
        public string MaxConversion { get; set; }
        public string MaxRemainBalance { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string isTaxable { get; set; }
        public string PaidDate { get; set; }
        public string LType { get; set; }
        public string LeaveBalance { get; set; }
        public string ConvertedAmount { get; set; }
    }
    public class GetAllLeaveConversion
    {
        public List<GetAllLeaveConversionList> LeaveConversion;
        public string myreturn;
    }
    public class GetAllLeaveConversionList
    {
        public string LeaveID { get; set; }
        public string MaxConversion { get; set; }
        public string MaxRemainBalance { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string isTaxable { get; set; }
        public string PaidDate { get; set; }
    }
    public class GetTodoAllLeaveConversion
    {
        public List<GetToDoAllLeaveConversionList> ToDoLeaveConversion;
        public string myreturn;
    }
    public class GetToDoAllLeaveConversionList
    {
        public string ConversionID { get; set; }
        public string EmpID { get; set; }
        public string EmpName { get; set; }
        public string LeaveType { get; set; }
        public string LType { get; set; }
        public string ConversionAmount { get; set; }
        public string Approver { get; set; }
        public string MngrName { get; set; }
        public string Status { get; set; }
        public string DateApplied { get; set; }
        public string DateApprove { get; set; }
        public string Result { get; set; }
    }
    public class forApprover2
    {
        public string CN { get; set; }
        public String[] requestorID { get; set; }
        public String[] leaveID { get; set; }
        public String[] SerialID { get; set; }
    }
    public class reimReq
    {
        public string CN { get; set; }
        public string ID { get; set; }
    }
    public class reimApprover2
    {
        public string CN { get; set; }
        public string ID { get; set; }
    }

    public class reimApprover
    {
        public string CN { get; set; }
        public string empID { get; set; }
        public string benType { get; set; }
    }
    public class addSegmentApprover2
    {
        public string CN { get; set; }
        public string empID { get; set; }
        public String[] SeriesID { get; set; }
    }
    public class emailnotif
    {
        public string[] toRecipients { get; set; }
        public string[] ccRecipients { get; set; }
        public string[] bccRecipients { get; set; }
        public string subjectline { get; set; }
        public string htmlMessage { get; set; }
    }
    public class approveDenyReim
    {
        public string ID { get; set; }
        public string Status { get; set; }
        public string UnclearCopy { get; set; }
        public string IncorrentType { get; set; }
        public string WrongDate { get; set; }
        public string BenType { get; set; }
        public string VendorName { get; set; }
        public string Date { get; set; }
        public string Time { get; set; }
        public string TIN { get; set; }
        public string Amount { get; set; }
        public string Description { get; set; }
        public string EmpID { get; set; }
        public string ModifiedDate { get; set; }
    }
}