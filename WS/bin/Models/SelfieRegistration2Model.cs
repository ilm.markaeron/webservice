﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace illimitadoWepAPI.Models
{
    public class DailyParameters2
    {
        public string NTID { get; set; }
        public string Start_Date { get; set; }
        public string End_Date { get; set; }
        public string Coordinates { get; set; }
        public string NearestEstablishment { get; set; }
        public string companyname { get; set; }
    }
    public class ListAllActivityByRange2
    {
        public string EmpID { get; set; }
        public string SeriesID { get; set; }
        public string StartTIme { get; set; }
        public string StartEnd { get; set; }
        public string ReasonID { get; set; }
        public string ReasonDesc { get; set; }
        public string IsPaid { get; set; }
        public string SchedDate { get; set; }
        public string HourWork { get; set; }
        public string ActivityStatus { get; set; }
        public string Day { get; set; }
        public string SchedDateStatus { get; set; }
        public string AddedBy { get; set; }
        public string SchedIn { get; set; }
        public string SchedOut { get; set; }
        public string EmpComment { get; set; }
        public string ApproverComment { get; set; }
        public string FilePath { get; set; }

    }
    public class ListAllActivityResult2
    {
        public List<ListAllActivityByRange2> ListAllActivity;
        public string myreturn;
    }
    public class signamo
    {
        public string EmpID { get; set; }
        public string sign1 { get; set; }
        public string sign2 { get; set; }
        public string sign3 { get; set; }
        public string CN { get; set; }
    }
    public class profilepicmo
    {
        public string b64img { get; set; }
        public string NTID { get; set; }
        public string num { get; set; }
        public string CN { get; set; }
        public string personID { get; set; }
        public string thres { get; set; }

    }
    public class ExportedMonitoredList
    {
        public string Emp_ID { get; set; }
        public string Employee_Name { get; set; }
        public string Date_Added { get; set; }
        public string Active_Date { get; set; }
        public string Inactive_Date { get; set; }
    }
    public class ExportMonitoredClass
    {
        public List<ExportedMonitoredList> ExportList { get; set; }
        public string MyReturn { get; set; }
    }
    public class MonitoringList
    {
        public string _date { get; set; }
        public string _client { get; set; }
        public string _count { get; set; }
        public string _bundyclockcount { get; set; }
        public string _mobilecount { get; set; }
        public string _mobileioscount { get; set; }
        public string _webcount { get; set; }
        public string _otherscount { get; set; }
    }
    public class MonitoringClass
    {
        public List<MonitoringList> MonitoringList { get; set; }
        public string _myreturn { get; set; }
    }
    public class MonitoringClients
    {
        public string ClientID { get; set; }
        public string ClientName { get; set; }
    }
    public class MonitoringClientsList
    {
        public List<MonitoringClients> ClientsList { get; set; }
        public string MyReturn { get; set; }
    }
    public class MonitoringParams
    {
        public string _datefrom { get; set; }
        public string _dateto { get; set; }
    }
    public class EmpCountList
    {
        public string _date { get; set; }
        public string _activeEmpCount { get; set; }
        public string _addition { get; set; }
        public string _deletion { get; set; }
        public string _net { get; set; }
    }

    public class EmpCountClass
    {
        public List<EmpCountList> EmpCountList { get; set; }
        public string _myreturn { get; set; }
    }

    public class EmpCountParams
    {
        public string _datefrom { get; set; }
        public string _dateto { get; set; }
        public string _clientname { get; set; }
    }


    public class Base64cmp
    {
        public string CN { get; set; }
        public string EmpID { get; set; }
    }

    public class ConfidenceLevel
    {
        public string CN { get; set; }
        public string EmpID { get; set; }
        public string ConfidLevel { get; set; }
    }

    #region Marvic
    public class DailyParameters3
    {
        public string NTID { get; set; }
        public string Start_Date { get; set; }
        public string End_Date { get; set; }
        public string Coordinates { get; set; }
        public string NearestEstablishment { get; set; }
        public string companyname { get; set; }
    }
    #endregion
}