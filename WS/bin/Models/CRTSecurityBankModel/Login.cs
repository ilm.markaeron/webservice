﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace illimitadoWepAPI.Models.CRTSecurityBankModel
{
    public class GetDatabase
    {
        public string ClientName { get; set; }
        public string ClientLink { get; set; }
    }
    public class CRTComRepv2
    {
        public string item { get; set; }
        public string reason { get; set; }
    }
    public class autoLogin
    {
        public string id { get; set; }
        public string company { get; set; }
        public string username { get; set; }
        public string password { get; set; }
    }

    public class CRTLogin
    {
        public string CN { get; set; }
        public string UserID { get; set; }
        public string Password { get; set; }
    }
    public class InsertClientData
    {
        public string CN { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string CompanyAlias { get; set; }
        public string DateofIncorporation { get; set; }
        public string AddressFloor { get; set; }
        public string AddressBuilding { get; set; }
        public string AddressStreet { get; set; }
        public string AddressBrgy { get; set; }
        public string AddressMunicipality { get; set; }
        public string AddressProvince { get; set; }
        public string AddressRegion { get; set; }
        public string AddressCountry { get; set; }
        public string AddressZipCode { get; set; }
        public string NationalityCountry { get; set; }
        public string CompanyEQCustomerNumber { get; set; }
        public string TotalNumberOfEmployees { get; set; }
        public string AccountType { get; set; }
        public string ContactType { get; set; }
        public string ContactNumber { get; set; }
        public string EmailType { get; set; }
        public string EmailAddress { get; set; }
        public string Website { get; set; }
        public string SSS { get; set; }
        public string PhilHealth { get; set; }
        public string Tin { get; set; }
        public string TinRDO { get; set; }
        public string PagIbig { get; set; }
        public string PagibigBranch { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string RepresentativeTitle { get; set; }
        public string RepresentativeContactNumber { get; set; }
        public string isDeleted { get; set; }
        public string DateCreated { get; set; }
        public string CreatedById { get; set; }

        public string Code { get; set; }
        public string PDF_Status { get; set; }
    }
    public class ClientDetails
    {
        public string ClientId { get; set; }
        public string CN { get; set; }
        public string UserName { get; set; }
        public string CompanyName { get; set; }
        public string CompanyAlias { get; set; }
        public string DateofIncorporation { get; set; }
        public string AddressFloor { get; set; }
        public string AddressBuilding { get; set; }
        public string AddressStreet { get; set; }
        public string AddressBrgy { get; set; }
        public string AddressMunicipality { get; set; }
        public string AddressProvince { get; set; }
        public string AddressRegion { get; set; }
        public string AddressCountry { get; set; }
        public string AddressZipCode { get; set; }
        public string NationalityCountry { get; set; }
        public string CompanyEQCustomerNumber { get; set; }
        public string TotalNumberOfEmployees { get; set; }
        public string AccountType { get; set; }
        public string ContactType { get; set; }
        public string ContactNumber { get; set; }
        public string EmailType { get; set; }
        public string EmailAddress { get; set; }
        public string Website { get; set; }
        public string SSS { get; set; }
        public string PhilHealth { get; set; }
        public string Tin { get; set; }
        public string TinRDO { get; set; }
        public string PagIbig { get; set; }
        public string PagibigBranch { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string RepresentativeTitle { get; set; }
        public string RepresentativeContactNumber { get; set; }
        public string isDeleted { get; set; }
        public string CreatedById { get; set; }
        public string SiteAllocation { get; set; }
        public string Branch { get; set; }
    }
    public class GetClientDetails
    {
        public List<ClientDetails> Clientdetails;
        public string myreturn;
    }
    public class UpdateClientData
    {
        public string ClientId { get; set; }
        public string CN { get; set; }
        public string UserName { get; set; }
        public string CompanyAlias { get; set; }
        public string DateofIncorporation { get; set; }
        public string AddressFloor { get; set; }
        public string AddressBuilding { get; set; }
        public string AddressStreet { get; set; }
        public string AddressBrgy { get; set; }
        public string AddressMunicipality { get; set; }
        public string AddressProvince { get; set; }
        public string AddressRegion { get; set; }
        public string AddressCountry { get; set; }
        public string AddressZipCode { get; set; }
        public string NationalityCountry { get; set; }
        public string CompanyEQCustomerNumber { get; set; }
        public string TotalNumberOfEmployees { get; set; }
        public string AccountType { get; set; }
        public string ContactType { get; set; }
        public string ContactNumber { get; set; }
        public string EmailType { get; set; }
        public string EmailAddress { get; set; }
        public string Website { get; set; }
        public string SSS { get; set; }
        public string PhilHealth { get; set; }
        public string Tin { get; set; }
        public string TinRDO { get; set; }
        public string PagIbig { get; set; }
        public string PagibigBranch { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string RepresentativeTitle { get; set; }
        public string RepresentativeContactNumber { get; set; }
        public string isDeleted { get; set; }
        public string ModifiedById { get; set; }
        public string PDF_Status { get; set; }
        public string Salad_Info { get; set; }
        public string CClientID { get; set; }
        public string lastAlias { get; set; }
        public string SaladSigID { get; set; }
    }
    public class Forgotpasswordtable
    {
        public string ClientID { get; set; }
        public string DateCreated { get; set; }
        public string Code { get; set; }
    }
    public class ForgotpasswordtableList
    {
        public List<ForgotpasswordtableList> Code;
        public string myreturn;
    }
    public class DisplayNewUser
    {
        public string CN { get; set; }
        public string Branch { get; set; }
        public string DateAdded { get; set; }
        public string NumberOfUser { get; set; }
        public string CompanyName { get; set; }
        public string CreatedBy { get; set; }
        public string StartingID { get; set; }
        public string AccountType { get; set; }
        public string DateHired { get; set; }
        public string isSent { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Suffix { get; set; }
        public string StartingIDCharacter { get; set; }
        public string CRTDateSent { get; set; }
        public string DataEntryStatus { get; set; }
        public string CRTStatus { get; set; }

    }
    public class GetDisplayNewUser
    {
        public List<DisplayNewUser> Displaynewuser;
        public string myreturn;
    }
    public class GetRDO
    {
        public string code { get; set; }
        public string branch { get; set; }
    }
    public class GetRDOList
    {
        public List<GetRDO> Displayrdo;
        public string myreturn;
    }
    public class GetPagibig
    {
        public string region { get; set; }
        public string code { get; set; }
        public string branch { get; set; }
    }
    public class GetPagibigList
    {
        public List<GetPagibig> Displaypagibig;
        public string myreturn;
    }
    public class GetNationality
    {
        public string Nationality { get; set; }
    }
    public class GetNationalityList
    {
        public List<GetNationality> Displaynationality;
        public string myreturn;
    }
    public class UpdateNewUser
    {
        public string CN { get; set; }
        public string NumberOfUser { get; set; }
        public string StartingID { get; set; }
        public string AccountType { get; set; }
        public string DateHired { get; set; }
        public string isSent { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string StartingIDCharacter { get; set; }
    }
    public class GetZipcodeArea
    {
        public string Zipcode { get; set; }
        public string Country { get; set; }
        public string Region { get; set; }
        public string Province { get; set; }
        public string Municipal { get; set; }

    }
    public class GetZipcodeAreaList
    {
        public List<GetZipcodeArea> Displayzipcodearea;
        public string myreturn;
    }
    public class PayrollCrtDetails
    {
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string Suffix { get; set; }
        public string CardName { get; set; }
        public string Birthday { get; set; }
        public string PlaceOfBirth { get; set; }
        public string Nationality { get; set; }
        public string NationalityCountry { get; set; }
        public string Gender { get; set; }
        public string MobileNumber { get; set; }
        public string LandlineNumber { get; set; }
        public string HouseNumberPresent { get; set; }
        public string StreetVillageSubdivisionBrgyPresent { get; set; }
        public string Country { get; set; }
        public string Region { get; set; }
        public string Province { get; set; }
        public string Municipality { get; set; }
        public string ZipCode { get; set; }
        public string HouseNumberPermanent { get; set; }
        public string StreetVillageSubdivisionBrgyPermanent { get; set; }
        public string CountryPermanent { get; set; }
        public string RegionPermanent { get; set; }
        public string ProvincePermanent { get; set; }
        public string MunicipalityPermanent { get; set; }
        public string ZipCodePermanent { get; set; }
        public string Email { get; set; }
        public string EmployerName { get; set; }
        public string EmployeeTIN { get; set; }
        public string CivilStatus { get; set; }
        public string Last_Name { get; set; }
        public string First_Name { get; set; }
        public string Middle_Name { get; set; }
        public string SuffixMother { get; set; }
        public string NumberOfPads { get; set; }
        public string OtherDetails { get; set; }
    }
    public class GetPayrollCrtDetails
    {
        public List<PayrollCrtDetails> Displaypayrollcrt;
        public string myreturn;
    }
    public class AlphaListModel
    {
    }

    public class GeneratePayrollCRT
    {
        public string EmpID { get; set; }
        public string Year { get; set; }
        public string CN { get; set; }
    }
    public class CRTCheckEmail
    {
        public string CompanyAlias { get; set; }
        public string EmployeeID { get; set; }
        public string Email { get; set; }
    }
    public class SHowclientFullNameAndEmail
    {
        public string FullName { get; set; }
        public string Email { get; set; }
    }
    public class GetSHowclientFullNameAndEmail
    {
        public List<SHowclientFullNameAndEmail> Displaypayrollcrt;
        public string myreturn;
    }
    public class ClientSettingDaily
    {
        public string UserName { get; set; }
        public string CN { get; set; }
        public string Setting { get; set; }
        public string ID { get; set; }
        public string Value { get; set; }
        public string CountDays { get; set; }
        public string CountWeeks { get; set; }
        public string Monday { get; set; }
        public string Tuesday { get; set; }
        public string Wednesday { get; set; }
        public string Thursday { get; set; }
        public string Friday { get; set; }
        public string Saturday { get; set; }
        public string Sunday { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public string recipient { get; set; }
        public string category { get; set; }
        public string setting { get; set; }

    }
    public class GetClientSettingDaily
    {
        public List<ClientSettingDaily> clientcettingdaily;
        public string myreturn;
    }
    public class ClientSettingWeekly
    {
        public string UserName { get; set; }
        public string CN { get; set; }
        public string Setting { get; set; }
        public string ID { get; set; }
        public string Value { get; set; }
        public string CountWeeks { get; set; }
        public string Monday { get; set; }
        public string Tuesday { get; set; }
        public string Wednesday { get; set; }
        public string Thursday { get; set; }
        public string Friday { get; set; }
        public string Saturday { get; set; }
        public string Sunday { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public string recipient { get; set; }

    }
    public class ClientSettingMain
    {
        public string id { get; set; }
        public string category { get; set; }
        public string dateOccured { get; set; }
        public string query { get; set; }
        public string recipient { get; set; }

    }
    public class ClientSettingMonthly
    {
        public string ID { get; set; }
        public string Value { get; set; }
        public string isActivated { get; set; }
        public string CountDays { get; set; }
        public string OrdinalDay { get; set; }
        public string Days { get; set; }
        public string CountMonths { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
    }
    public class ClientSettingYearly
    {

        public string ID { get; set; }
        public string value { get; set; }
        public string isActivated { get; set; }
        public string CountYears { get; set; }
        public string Month { get; set; }
        public string Day { get; set; }
        public string OrdinalDay { get; set; }
        public string Days { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
    }
    public class ClientSettingMonthlyEditable
    {
        public string isActivated1st { get; set; }
        public string CountDays { get; set; }
        public string OrdinalDays { get; set; }
        public string Days { get; set; }
        public string CountMonths { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public string recipient { get; set; }
    }

    public class ClientSettingYearlyEditable
    {
        public string isActivated1st { get; set; }
        public string CountYears { get; set; }
        public string OrdinalDay { get; set; }
        public string Days { get; set; }
        public string Day { get; set; }
        public string Month { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public string recipient { get; set; }
    }
    public class GetMainCRTTable
    {
        public string id { get; set; }
        public string category { get; set; }
        public string dateOccured { get; set; }
        public string query { get; set; }
        public string recipient { get; set; }
        public string link { get; set; }
    }
    public class GetMainCRTTableList
    {
        public List<GetMainCRTTable> Displaytablelist;
        public string myreturn;
    }
    public class MobileRegisterPersonalInfoTable
    {
        public string SiteAllocation { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string Suffix { get; set; }
        public string Prefix { get; set; }
        public string DateOfBirth { get; set; }
        public string PlaceOfBirth { get; set; }
        public string MobileNumber { get; set; }
        public string CivilStatus { get; set; }
        public string Nationality { get; set; }
        public string Gender { get; set; }
        public string DateCreated { get; set; }
    }

    public class MobileRegisterMothersInfoTable
    {
        public string Mothers_FirstName { get; set; }
        public string Mothers_MiddleName { get; set; }
        public string Mothers_LastName { get; set; }
        public string Mothers_Suffix { get; set; }
        public string DateCreated { get; set; }
    }
    //public class CRTMobileLogin
    //{
    //    public string StartingID { get; set; }
    //    public string FirstName { get; set; }
    //    public string MiddleName { get; set; }
    //    public string LastName { get; set; }
    //    public string DateHired { get; set; }
    //}

    public class CRTMobileLoginCompany
    {
        public string CompanyName { get; set; }
        public string StartingID { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Birthday { get; set; }
    }
    public class CRTMobileLoginCompanyV2
    {
        public string CompanyName { get; set; }
        public string StartingID { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Birthday { get; set; }
    }
    public class CRTMobileCompanyVerification
    {
        public string CompanyAlias { get; set; }
    }

    //public class GetCRTMobileLogin
    //{
    //    public List<CRTMobileLogin> Displayloginname;
    //    public string myreturMobileRegisterPersonalInfoTablen;
    //}

    public class MobileInsertEmployee
    {
        public string EmployeeID { get; set; }
        public string StartingID { get; set; }
        //public string SiteAllocation { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string Suffix { get; set; }
        public string Prefix { get; set; }
        public string Birthday { get; set; }
        public string PlaceOfBirth { get; set; }
        public string MobileNumber { get; set; }
        public string LandlineNumber { get; set; }
        public string Email { get; set; }
        public string CivilStatus { get; set; }
        public string Nationality { get; set; }
        public string NationalityCountry { get; set; }
        public string Gender { get; set; }
        public string Position { get; set; }
        public string DateHired { get; set; }
        public string EmploymentStatus { get; set; }
        public string EndOfContract { get; set; }
        public string ID1 { get; set; }
        public string ID1Image { get; set; }
        public string ID2 { get; set; }
        public string ID2Image { get; set; }
        public string PropertyType { get; set; }
        public string Phase { get; set; }
        public string ZipCode { get; set; }
        public string Block { get; set; }
        public string Lot { get; set; }
        public string Street { get; set; }
        public string Village { get; set; }
        public string Subdivision { get; set; }
        public string Barangay { get; set; }
        public string Municipality { get; set; }
        public string Province { get; set; }
        public string Region { get; set; }
        public string Country { get; set; }
        public string BldgNo { get; set; }
        public string NameOfPlace { get; set; }
        public string Wing { get; set; }
        public string UnitNo { get; set; }
        public string BldgName { get; set; }
        public string FloorNo { get; set; }
        public string RoomAptNo { get; set; }
        public string PropertyTypeP { get; set; }
        public string PhasePermanent { get; set; }
        public string ZipCodePermanent { get; set; }
        public string BlockPermanent { get; set; }
        public string LotPermanent { get; set; }
        public string StreetPermanent { get; set; }
        public string VillagePermanent { get; set; }
        public string SubdivisionPermanent { get; set; }
        public string BarangayPermanent { get; set; }
        public string MunicipalityPermanent { get; set; }
        public string ProvincePermanent { get; set; }
        public string RegionPermanent { get; set; }
        public string CountryPermanent { get; set; }
        public string BldgNoPermanent { get; set; }
        public string NameOfPlacePermanent { get; set; }
        public string WingPermanent { get; set; }
        public string UnitNoPermanent { get; set; }
        public string BldgNamePermanent { get; set; }
        public string FloorNoPermanent { get; set; }
        public string RoomAptNoPermanent { get; set; }
        public string First_Name { get; set; }
        public string Middle_Name { get; set; }
        public string Last_Name { get; set; }
        public string SuffixMother { get; set; }
        public string EmployeeTIN { get; set; }
        public string Ref1_FirstName { get; set; }
        public string Ref1_MiddleName { get; set; }
        public string Ref1_LastName { get; set; }
        public string Ref1_MobileNumber { get; set; }
        public string Ref2_FirstName { get; set; }
        public string Ref2_MiddleName { get; set; }
        public string Ref2_LastName { get; set; }
        public string Ref2_MobileNumber { get; set; }
        public string CompanyName { get; set; }
        public string Signature1 { get; set; }
        public string Signature2 { get; set; }
        public string Signature3 { get; set; }
    }
    public class GenerateompanyCheck
    {
        public string CompanyName { get; set; }
    }
    public class CRTGenerateCount
    {
        public string CompanyName { get; set; }
    }

    public class MobileZipCodeSearch
    {
        public string ZipCode { get; set; }
        public string Country { get; set; }
        public string Region { get; set; }
        public string Province { get; set; }
        public string Municipal { get; set; }
        public string ProvCode { get; set; }
    }

    public class GetMobileZipCode
    {
        public List<MobileZipCodeSearch> Displayzipcode;
        public string myreturn;
    }

    #region Added By Akram
    public class SendNewClientEmail
    {
        public string UserName { get; set; }
    }
    public class ForgotPassword
    {
        public string Email { get; set; }
    }
    public class SaladParams
    {
        public string CN { get; set; }
        public string StartingID { get; set; }
    }

    public class SaladForms
    {
        public string CN { get; set; }
        public String[] StartingIDs { get; set; }
    }

    #endregion
    public class MobileBranchSearch
    {
        public string StartingID { get; set; }

    }

    public class MobileGetAllData
    {
        public string CompanyName { get; set; }
        public string StartingID { get; set; }
        public string Branch { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string Suffix { get; set; }
        public string Prefix { get; set; }
        public string Birthday { get; set; }
        public string PlaceOfBirth { get; set; }
        public string MobileNumber { get; set; }
        public string LandlineNumber { get; set; }
        public string Email { get; set; }
        public string CivilStatus { get; set; }
        public string Nationality { get; set; }
        public string NationalityCountry { get; set; }
        public string Gender { get; set; }
        public string Position { get; set; }
        public string DateHired2 { get; set; }
        public string EmploymentStatus { get; set; }
        public string EndOfContract { get; set; }
        public string ID1 { get; set; }
        public string ID1Image { get; set; }
        public string ID2 { get; set; }
        public string ID2Image { get; set; }
        public string PropertyType { get; set; }
        public string Phase { get; set; }
        public string ZipCode { get; set; }
        public string Block { get; set; }
        public string Lot { get; set; }
        public string Street { get; set; }
        public string Village { get; set; }
        public string Subdivision { get; set; }
        public string Barangay { get; set; }
        public string Municipality { get; set; }
        public string Province { get; set; }
        public string Region { get; set; }
        public string Country { get; set; }
        public string BldgNo { get; set; }
        public string NameOfPlace { get; set; }
        public string Wing { get; set; }
        public string UnitNo { get; set; }
        public string BldgName { get; set; }
        public string FloorNo { get; set; }
        public string RoomAptNo { get; set; }
        public string PropertyTypePermanent { get; set; }
        public string PhasePermanent { get; set; }
        public string ZipCodePermanent { get; set; }
        public string BlockPermanent { get; set; }
        public string LotPermanent { get; set; }
        public string StreetPermanent { get; set; }
        public string VillagePermanent { get; set; }
        public string SubdivisionPermanent { get; set; }
        public string BarangayPermanent { get; set; }
        public string MunicipalityPermanent { get; set; }
        public string ProvincePermanent { get; set; }
        public string RegionPermanent { get; set; }
        public string CountryPermanent { get; set; }
        public string BldgNoPermanent { get; set; }
        public string NameOfPlacePermanent { get; set; }
        public string WingPermanent { get; set; }
        public string UnitNoPermanent { get; set; }
        public string BldgNamePermanent { get; set; }
        public string FloorNoPermanent { get; set; }
        public string RoomAptNoPermanent { get; set; }
        public string First_Name { get; set; }
        public string Middle_Name { get; set; }
        public string Last_Name { get; set; }
        public string SuffixMother { get; set; }
        public string EmployeeTIN { get; set; }
        public string Ref1_FirstName { get; set; }
        public string Ref1_MiddleName { get; set; }
        public string Ref1_LastName { get; set; }
        public string Ref1_MobileNumber { get; set; }
        public string Ref2_FirstName { get; set; }
        public string Ref2_MiddleName { get; set; }
        public string Ref2_LastName { get; set; }
        public string Ref2_MobileNumber { get; set; }
        public string Signature1 { get; set; }
        public string Signature2 { get; set; }
        public string Signature3 { get; set; }
        public string Rank { get; set; }
    }

    public class GetMobileAllData
    {
        public List<MobileGetAllData> Displayalldata;
        public string myreturn;
    }

    public class GetMunicipality
    {
        public string Municipal { get; set; }
    }
    public class GetMunicipalityList
    {
        public List<GetMunicipality> Displaymunicipality;
        public string myreturn;
    }
    public class GetCompanyName
    {
        public string CompanyName { get; set; }
    }
    public class GetCompanyNameList
    {
        public List<GetCompanyName> DisplayCompany;
        public string myreturn;
    }
    public class GetMobileReadOnly
    {
        public List<MobileGetAllData> Displayalldata;
        public string myreturn;
    }
    //brandon add admin
    public class CRTAddAdminUsers
    {
        public string ID { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Title { get; set; }
        public string Role { get; set; }
        public string Area { get; set; }
        public string Branch { get; set; }
        public string Department { get; set; }
        public string Sub_Department { get; set; }
        public string User_Type { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string lastID { get; set; }
    }
    public class CRTUpdateAdminUsersv2
    {
        public String[] ID { get; set; }
        public string UserID { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Title { get; set; }
        public string Role { get; set; }
        public string Area { get; set; }
        public string Branch { get; set; }
        public string Department { get; set; }
        public string SubDepartment { get; set; }
        public string UserType { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string UpdateType { get; set; }
        public string CClientID { get; set; }
    }
    public class CRTAddAdminUsersDisplay
    {
        public string ID { get; set; }
        public string UserID { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Title { get; set; }
        public string Role { get; set; }
        public string Area { get; set; }
        public string Branch { get; set; }
        public string Department { get; set; }
        public string Sub_Department { get; set; }
        public string UserType { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
    }
    public class GetCRTAddAdminUsers
    {
        public List<CRTAddAdminUsersDisplay> Displaynewuser;
        public string myreturn;
    }
    //brandon search salad client
    public class CRTSaladCompanySearchDisplay
    {
        public string Generated { get; set; }
        public string GeneratedPDF { get; set; }
        public string CompanyAlias { get; set; }
        public string CompanyName { get; set; }
        public string Branch { get; set; }
        public string StartingID { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string Suffix { get; set; }
        public string CardName { get; set; }
        public string Birthday { get; set; }
        public string PlaceofBirth { get; set; }
        public string CRTStatus { get; set; }
        public string CRTDateSent { get; set; }
    }
    public class GetEmployeeID
    {
        public string CompanyAlias { get; set; }
        public string EmployeeID { get; set; }
       
    }
    public class GetCRTSaladCompanySearchDisplay
    {
        public List<CRTSaladCompanySearchDisplay> Displaycompanysalad;
        public string myreturn;
    }
    //brandon search salad client
    //udtcrt crt
    public class SaladItemsCRT
    {
        public String _cn { get; set; }
        public String CclientID { get; set; }
        public String[] _startingid { get; set; }
    }
    //udtcrt crt end
    //Scheduler start

    public class subjectlistv2
    {
        public string _SID { get; set; }
        public string _Subject { get; set; }
        public bool _Status { get; set; }
        public string _DefaultName { get; set; }
        public string _ModifiedBy { get; set; }
        public string _ModifiedDate { get; set; }
        public bool _Edit { get; set; }
        public string CN { get; set; }
    }
    public class subsubjectlist
    {
        public string _RowID { get; set; }
        public string _SID { get; set; }
        public string _Subject { get; set; }
        public bool _Status { get; set; }
        public string _ModifiedBy { get; set; }
        public string _ModifiedDate { get; set; }
        public bool _Edit { get; set; }

        public string CN { get; set; }
    }
    public class subsubjectsv2
    {
        public List<subsubjectlist> subsubjectlist;
        public string _myreturn { get; set; }

        public string CN { get; set; }
    }
    public class subjectsV2
    {
        public List<subjectlistv2> subjectlist;
        public string _myreturn { get; set; }
        public string CN { get; set; }
    }
    //scheduler end
    #region Revised
    public class CRTAddAdminUsersv2
    {
        public string ID { get; set; }
        public string UserID { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Title { get; set; }
        public string Role { get; set; }
        public string Area { get; set; }
        public string Branch { get; set; }
        public string Department { get; set; }
        public string SubDepartment { get; set; }
        public string UserType { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string LastID { get; set; }
        public string CClientID { get; set; }
    }
    public class CRTPublicParam
    {
        public string CClientID { get; set; }
        public string from { get; set; }
        public string to { get; set; }
        public string CompanyName { get; set; }
        public string crtStatus { get; set; }
    }
    public class AdminLoginInfo
    {
        public string UserID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string UserType { get; set; }
        public string CClientID { get; set; }
        public string _myreturn { get; set; }
    }
    public class InsertClientDatav2
    {
        public string CN { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string CompanyAlias { get; set; }
        public string DateofIncorporation { get; set; }
        public string AddressFloor { get; set; }
        public string AddressBuilding { get; set; }
        public string AddressStreet { get; set; }
        public string AddressBrgy { get; set; }
        public string AddressMunicipality { get; set; }
        public string AddressProvince { get; set; }
        public string AddressRegion { get; set; }
        public string AddressCountry { get; set; }
        public string AddressZipCode { get; set; }
        public string NationalityCountry { get; set; }
        public string CompanyEQCustomerNumber { get; set; }
        public string TotalNumberOfEmployees { get; set; }
        public string AccountType { get; set; }
        public string ContactType { get; set; }
        public string ContactNumber { get; set; }
        public string EmailType { get; set; }
        public string EmailAddress { get; set; }
        public string Website { get; set; }
        public string SSS { get; set; }
        public string PhilHealth { get; set; }
        public string Tin { get; set; }
        public string TinRDO { get; set; }
        public string PagIbig { get; set; }
        public string PagibigBranch { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string RepresentativeTitle { get; set; }
        public string RepresentativeContactNumber { get; set; }
        public string isDeleted { get; set; }
        public string DateCreated { get; set; }
        public string CreatedById { get; set; }
        public string Code { get; set; }
        public string PDF_Status { get; set; }
        public string Salad_Info { get; set; }
        public string CClientID { get; set; }
        public string SaladSigID { get; set; }
    }
    public class SendNewClientEmailv2
    {
        public string UserName { get; set; }
        public string CClientID { get; set; }
        public string CompanyAlias { get; set; }
    }
    public class subjectlabelv2
    {
        public string _title { get; set; }
        public string _role { get; set; }
        public string _area { get; set; }
        public string _branch { get; set; }
        public string _department { get; set; }
        public string _subdepartment { get; set; }
        public string CClientID { get; set; }
    }
    public class subjectlabellistv2
    {
        public List<subjectlabelv2> subjectlabel;
        public string _myreturn { get; set; }
    }
    public class MobileInsertEmployeev2
    {
        public string EmployeeID { get; set; }
        public string StartingID { get; set; }
        //public string SiteAllocation { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string Suffix { get; set; }
        public string Prefix { get; set; }
        public string Birthday { get; set; }
        public string PlaceOfBirth { get; set; }
        public string MobileNumber { get; set; }
        public string LandlineNumber { get; set; }
        public string Email { get; set; }
        public string CivilStatus { get; set; }
        public string Nationality { get; set; }
        public string NationalityCountry { get; set; }
        public string Gender { get; set; }
        public string Position { get; set; }
        public string DateHired { get; set; }
        public string EmploymentStatus { get; set; }
        public string EndOfContract { get; set; }
        public string ID1 { get; set; }
        public string ID1Image { get; set; }
        public string ID2 { get; set; }
        public string ID2Image { get; set; }
        public string PropertyType { get; set; }
        public string Phase { get; set; }
        public string ZipCode { get; set; }
        public string Block { get; set; }
        public string Lot { get; set; }
        public string Street { get; set; }
        public string Village { get; set; }
        public string Subdivision { get; set; }
        public string Barangay { get; set; }
        public string Municipality { get; set; }
        public string Province { get; set; }
        public string Region { get; set; }
        public string Country { get; set; }
        public string BldgNo { get; set; }
        public string NameOfPlace { get; set; }
        public string Wing { get; set; }
        public string UnitNo { get; set; }
        public string BldgName { get; set; }
        public string FloorNo { get; set; }
        public string RoomAptNo { get; set; }
        public string PropertyTypeP { get; set; }
        public string PhasePermanent { get; set; }
        public string ZipCodePermanent { get; set; }
        public string BlockPermanent { get; set; }
        public string LotPermanent { get; set; }
        public string StreetPermanent { get; set; }
        public string VillagePermanent { get; set; }
        public string SubdivisionPermanent { get; set; }
        public string BarangayPermanent { get; set; }
        public string MunicipalityPermanent { get; set; }
        public string ProvincePermanent { get; set; }
        public string RegionPermanent { get; set; }
        public string CountryPermanent { get; set; }
        public string BldgNoPermanent { get; set; }
        public string NameOfPlacePermanent { get; set; }
        public string WingPermanent { get; set; }
        public string UnitNoPermanent { get; set; }
        public string BldgNamePermanent { get; set; }
        public string FloorNoPermanent { get; set; }
        public string RoomAptNoPermanent { get; set; }
        public string First_Name { get; set; }
        public string Middle_Name { get; set; }
        public string Last_Name { get; set; }
        public string SuffixMother { get; set; }
        public string EmployeeTIN { get; set; }
        public string Ref1_FirstName { get; set; }
        public string Ref1_MiddleName { get; set; }
        public string Ref1_LastName { get; set; }
        public string Ref1_MobileNumber { get; set; }
        public string Ref2_FirstName { get; set; }
        public string Ref2_MiddleName { get; set; }
        public string Ref2_LastName { get; set; }
        public string Ref2_MobileNumber { get; set; }
        public string CompanyName { get; set; }
        public string Signature1 { get; set; }
        public string Signature2 { get; set; }
        public string Signature3 { get; set; }
        public string Site { get; set; }
        public string Rank { get; set; }
        public string NumberofPads { get; set; }
    }
    #endregion

    public class GetPDFStatus
    {
        public string CompanyName { get; set; }
    }
    public class Regions
    {
        public string RegionID { get; set; }
        public string RegionName { get; set; }
    }
    public class Regionsv2
    {
        public string RegionID { get; set; }
        public string RegionName { get; set; }
        public string CompanyAlias { get; set; }
    }
    public class Provinces
    {
        public string ProvinceID { get; set; }
        public string ProvinceName { get; set; }
    }
    public class Provincesv2
    {
        public string ProvinceID { get; set; }
        public string ProvinceName { get; set; }
        public string CompanyAlias { get; set; }
    }
    public class CityMunicipalities
    {
        public string CityMunID { get; set; }
        public string CityMunName { get; set; }
    }

    public class GetAccountType
    {
        public string CompanyName { get; set; }
        public string StartingID { get; set; }
    }

    public class GetLandlineNumber
    {
        public string CompanyAlias { get; set; }
    }
    public class DisplayNewUserv2
    {
        public string CN { get; set; }
        public string Branch { get; set; }
        public string DateAdded { get; set; }
        public string NumberOfUser { get; set; }
        public string CompanyName { get; set; }
        public string CreatedBy { get; set; }
        public string StartingID { get; set; }
        public string AccountType { get; set; }
        public string DateHired { get; set; }
        public string isSent { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Suffix { get; set; }
        public string StartingIDCharacter { get; set; }
        public string CRTDateSent { get; set; }
        public string DataEntryStatus { get; set; }
        public string CRTStatus { get; set; }
        public string EmployeeTIN { get; set; }
    }
    public class DuplicateUsers
    {
        public string RowID { get; set; }
        public string EmployeeID { get; set; }
        public string Conflict { get; set; }
    }
    public class UserBulkUpload
    {
        public String[] employeeID { get; set; }
        public String[] fName { get; set; }
        public String[] mName { get; set; }
        public String[] lName { get; set; }
        public String[] accountType { get; set; }
        public String[] tin { get; set; }
        public String[] birthDate { get; set; }
        public String[] Suffix { get; set; }
        public String[] site { get; set; }
        public String[] rowNo { get; set; }
        public string CN { get; set; }
        public string Timezone { get; set; }
    }
    public class GetDisplayNewUserv2
    {
        public List<DisplayNewUserv2> Displaynewuser;
        public string myreturn;
    }
    public class GetClientDetailsv2
    {
        public List<ClientDetailsv2> Clientdetails;
        public string myreturn;
    }
    public class ClientDetailsv2
    {
        public string ClientId { get; set; }
        public string CN { get; set; }
        public string UserName { get; set; }
        public string CompanyName { get; set; }
        public string CompanyAlias { get; set; }
        public string DateofIncorporation { get; set; }
        public string AddressFloor { get; set; }
        public string AddressBuilding { get; set; }
        public string AddressStreet { get; set; }
        public string AddressBrgy { get; set; }
        public string AddressMunicipality { get; set; }
        public string AddressProvince { get; set; }
        public string AddressRegion { get; set; }
        public string AddressCountry { get; set; }
        public string AddressZipCode { get; set; }
        public string NationalityCountry { get; set; }
        public string CompanyEQCustomerNumber { get; set; }
        public string TotalNumberOfEmployees { get; set; }
        public string AccountType { get; set; }
        public string ContactType { get; set; }
        public string ContactNumber { get; set; }
        public string EmailType { get; set; }
        public string EmailAddress { get; set; }
        public string Website { get; set; }
        public string SSS { get; set; }
        public string PhilHealth { get; set; }
        public string Tin { get; set; }
        public string TinRDO { get; set; }
        public string PagIbig { get; set; }
        public string PagibigBranch { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string RepresentativeTitle { get; set; }
        public string RepresentativeContactNumber { get; set; }
        public string isDeleted { get; set; }
        public string CreatedById { get; set; }
        public bool SaladInfo { get; set; }
        public bool SaladPDF { get; set; }
        public bool SaladSigID { get; set; }
        public string NameOfTeam { get; set; }
        public string posRank { get; set; }
    }
    public class CRTMobileCompanyDetails
    {
        public string CompanyType { get; set; }
        public string CompanyName { get; set; }
        public string ContactInfo { get; set; }
    }
    public class CRTTINValidation
    {
        public string TIN { get; set; }
    }
    public class CRTComRep
    {
        public string item { get; set; }
        public string reason { get; set; }
    }
    public class InsertClientDatav3
    {
        public string CN { get; set; }
        public string Password { get; set; }
        public string CompanyAlias { get; set; }
        public string DateofIncorporation { get; set; }
        public string AddressFloor { get; set; }
        public string AddressBuilding { get; set; }
        public string AddressStreet { get; set; }
        public string AddressBrgy { get; set; }
        public string AddressMunicipality { get; set; }
        public string AddressProvince { get; set; }
        public string AddressRegion { get; set; }
        public string AddressCountry { get; set; }
        public string AddressZipCode { get; set; }
        public string NationalityCountry { get; set; }
        public string CompanyEQCustomerNumber { get; set; }
        public string TotalNumberOfEmployees { get; set; }
        public string AccountType { get; set; }
        public string ContactType { get; set; }
        public string ContactNumber { get; set; }
        public string EmailType { get; set; }
        public string Website { get; set; }
        public string SSS { get; set; }
        public string PhilHealth { get; set; }
        public string Tin { get; set; }
        public string TinRDO { get; set; }
        public string PagIbig { get; set; }
        public string PagibigBranch { get; set; }
        public string isDeleted { get; set; }
        public string DateCreated { get; set; }
        public string CreatedById { get; set; }
        public string Code { get; set; }
        public string PDF_Status { get; set; }
        public string Salad_Info { get; set; }
        public string CClientID { get; set; }
        public string SaladSigID { get; set; }
        public String[] UserName { get; set; }
        public String[] EmailAddress { get; set; }
        public String[] FirstName { get; set; }
        public String[] MiddleName { get; set; }
        public String[] LastName { get; set; }
        public String[] RepresentativeTitle { get; set; }
        public String[] RepresentativeContactNumber { get; set; }
        public string NameOfTeam { get; set; }
        public bool PosRank { get; set; }
    }
    public class CompanyRepInfo
    {
        public string ComRepID { get; set; }
        public string Username { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string RepTitle { get; set; }
        public string RepEmail { get; set; }
        public string RepContact { get; set; }
    }
    public class UpdateClientDatav2
    {
        public string ClientId { get; set; }
        public string CN { get; set; }

        public string CompanyAlias { get; set; }
        public string DateofIncorporation { get; set; }
        public string AddressFloor { get; set; }
        public string AddressBuilding { get; set; }
        public string AddressStreet { get; set; }
        public string AddressBrgy { get; set; }
        public string AddressMunicipality { get; set; }
        public string AddressProvince { get; set; }
        public string AddressRegion { get; set; }
        public string AddressCountry { get; set; }
        public string AddressZipCode { get; set; }
        public string NationalityCountry { get; set; }
        public string CompanyEQCustomerNumber { get; set; }
        public string TotalNumberOfEmployees { get; set; }
        public string AccountType { get; set; }
        public string ContactType { get; set; }
        public string ContactNumber { get; set; }
        public string EmailType { get; set; }

        public string Website { get; set; }
        public string SSS { get; set; }
        public string PhilHealth { get; set; }
        public string Tin { get; set; }
        public string TinRDO { get; set; }
        public string PagIbig { get; set; }
        public string PagibigBranch { get; set; }
        public string isDeleted { get; set; }
        public string ModifiedById { get; set; }
        public string PDF_Status { get; set; }
        public string Salad_Info { get; set; }
        public string CClientID { get; set; }
        public string lastAlias { get; set; }
        public string SaladSigID { get; set; }
        public String[] RepID { get; set; }
        public String[] UserName { get; set; }
        public String[] FirstName { get; set; }
        public String[] MiddleName { get; set; }
        public String[] LastName { get; set; }
        public String[] RepresentativeTitle { get; set; }
        public String[] EmailAddress { get; set; }
        public String[] RepresentativeContactNumber { get; set; }
    }
    public class CRTGenerationParams
    {
        public String[] StartingIDS { get; set; }
        public string CompanyAlias { get; set; }
        public string Branch { get; set; }
    }
    public class CRTTINValidationv2
    {
        public string TIN { get; set; }
        public string CN { get; set; }
    }
    public class UpdateClientDatav3
    {
        public string ClientId { get; set; }
        public string CN { get; set; }

        public string CompanyAlias { get; set; }
        public string DateofIncorporation { get; set; }
        public string AddressFloor { get; set; }
        public string AddressBuilding { get; set; }
        public string AddressStreet { get; set; }
        public string AddressBrgy { get; set; }
        public string AddressMunicipality { get; set; }
        public string AddressProvince { get; set; }
        public string AddressRegion { get; set; }
        public string AddressCountry { get; set; }
        public string AddressZipCode { get; set; }
        public string NationalityCountry { get; set; }
        public string CompanyEQCustomerNumber { get; set; }
        public string TotalNumberOfEmployees { get; set; }
        public string AccountType { get; set; }
        public string ContactType { get; set; }
        public string ContactNumber { get; set; }
        public string EmailType { get; set; }

        public string Website { get; set; }
        public string SSS { get; set; }
        public string PhilHealth { get; set; }
        public string Tin { get; set; }
        public string TinRDO { get; set; }
        public string PagIbig { get; set; }
        public string PagibigBranch { get; set; }
        public string isDeleted { get; set; }
        public string ModifiedById { get; set; }
        public string PDF_Status { get; set; }
        public string Salad_Info { get; set; }
        public string CClientID { get; set; }
        public string lastAlias { get; set; }
        public string SaladSigID { get; set; }

        public string NameOfTeam { get; set; }
        public bool PosRank { get; set; }

        public String[] RepID { get; set; }
        public String[] UserName { get; set; }
        public String[] FirstName { get; set; }
        public String[] MiddleName { get; set; }
        public String[] LastName { get; set; }
        public String[] RepresentativeTitle { get; set; }
        public String[] EmailAddress { get; set; }
        public String[] RepresentativeContactNumber { get; set; }
    }
    //testing Dashboard Brandon
    public class DashboardCount
    {
        public string CN { get; set; }
        public string TotalUpload { get; set; }
        public string CompleteDataEntry { get; set; }
        public string IncompleteDataEntry { get; set; }
        public string CRTGenerated { get; set; }
        public string RoundedComplete { get; set; }
        public string RoundedIncomplete { get; set; }
    }

    public class GetDashboardCount
    {
        public List<DashboardCount> DisplayTotalCount;
        //public string myreturn;
    }
    public class DashboardClientDetails
    {
        public string CompanyName { get; set; }
        public string Branch { get; set; }
        public string Complete { get; set; }
        public string Incomplete { get; set; }
    }
    public class GetDashboardClientDetails
    {
        public List<DashboardClientDetails> Clientdetails;
        public string myreturn;
    }
    public class CRTDashboardGeneration
    {
        //public String[] StartingIDS { get; set; }
        public string CN { get; set; }
        public string Site { get; set; }
        public string Result { get; set; }
    }
    public class CRTCheckMobNum
    {
        public string CompanyAlias { get; set; }
        public string ContactNumber { get; set; }
        public string EmployeeID { get; set; }
    }
    public class CRTUpdateTnC
    {
        public string CompanyAlias { get; set; }
        public string EmployeeID { get; set; }
        public string TNC { get; set; }
    }
    public class CRTSearchUser
    {
        public string site { get; set; }
        public string id { get; set; }
        public string company { get; set; }
    }

    public class GetCRTSearchUser
    {
        public List<UserResultList> result;
        public string myreturn;
    }

    public class UserResultList
    {
        public string id { get; set; }
        public string fn { get; set; }
        public string mn { get; set; }
        public string ln { get; set; }
        public string suffix { get; set; }
        public string dob { get; set; }
        public string site { get; set; }
        public string acctype { get; set; }
        public string empTIN { get; set; }
    }

    //SP_Returns
    public class SP_Returns
    {
        public string id { get; set; }
        public string fn { get; set; }
        public string mn { get; set; }
        public string ln { get; set; }
        public string suffix { get; set; }
        public string dob { get; set; }
        public string site { get; set; }
        public string acctype { get; set; }
        public string empTIN { get; set; }
    }

    public class SP_CRT_getSite_Returns
    {
        public string site { get; set; }
    }

    public class SP_CRT_getEmp_Returns
    {
        public string id { get; set; }
        public string empname { get; set; }
    }

    public class SP_CRT_getCompanyName_Returns
    {
        public string CompanyName { get; set; }

    }
    public class ClassGetCRTUser
    {
        //FOR THE RESULT
        public List<SP_Returns> DisplaySearchResult;
        public string myreturn;
    }

    public class ClassGetSite
    {
        //FOR THE RESULT
        public List<SP_CRT_getSite_Returns> returnSite;
        public string myreturn;
    }

    public class ClassGetEmp
    {
        //FOR THE RESULT
        public List<SP_CRT_getEmp_Returns> returnEmp;
        public string myreturn;
    }

    public class ClassGetCompanyName
    {
        //FOR THE RESULT
        public List<SP_CRT_getCompanyName_Returns> returnCN;
        public string myreturn;
    }

    //Extract Exclusion
    public class CRTExclusionExtract
    {

        public string company { get; set; }

    }
    public class SP_Param_getClient
    {
        public string clientID { get; set; }
    }
    public class SP_Param_getSite
    {
        public string company { get; set; }
    }
    public class SP_Param
    {
        public string site { get; set; }
        public string id { get; set; }
        public string company { get; set; }
    }
    //END BY 18/08/2019



    //NEW SP  9/19/2019
    public class ClassGetExclusion
    {
        //FOR THE RESULT
        public List<GetExclusionReturns> returnEmployeeID;
        public string myreturn;
    }


    public class param_GetExclusion
    {
        public string CompanyName { get; set; }
    }


    public class GetExclusionReturns
    {
        public string EmployeeID { get; set; }

    }
    // END 9/19/2019

    //Update Exclusion
    public class UpdateExc
    {
        public string RowID { get; set; }
        public string EmployeeID { get; set; }
        public string ExclusionType { get; set; }
    }

    public class DeleteExc
    {
        public string ExclusionType { get; set; }
    }
    //Update Exclusion
    public class ExclusionParams
    {
        public String[] EmployeeID { get; set; }
        public String[] ExclusionType { get; set; }
        public string company { get; set; }

    }

    //SEARCH FILTER
    public class SearchFilterParams
    {
        public String[] empID { get; set; }
        public String[] site { get; set; }
        public string company { get; set; }

    }
    //SEARCH FILTER
    public class FilteredUsers
    {
        public string StartingID { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Suffix { get; set; }
        public string DateHired { get; set; }
        public string Branch { get; set; }
        public string AccountType { get; set; }
        public string EmployeeTIN { get; set; }
    }
    //Filter exclusion
    public class ExclusionFilterParams
    {
        public String[] EmployeeID { get; set; }
        public String[] ExclusionType { get; set; }
        public string company { get; set; }

    }
    public class FilterExclusionReturn
    {
        public string EmployeeID { get; set; }
        public string CompanyAlias { get; set; }
        public string OpenedAccount { get; set; }
        public string DuplicateMobileNum { get; set; }
        public string DuplicateTIN { get; set; }
    }
    public class getList_AutoLogin
    {
        public string id { get; set; }
        public string company { get; set; }
        public string username { get; set; }
        public string password { get; set; }
      
    }

    public class EmpDeleteExclusionParams
    {
        public String[] ExclusionType { get; set; }
        public string company { get; set; }
        public string empID { get; set; }
    }

    //extract ID Exclusion
    public class CRTExclusionExtractID
    {
        public String[] EmpID { get; set; }
        public string company { get; set; }

    }
    
}

//CRT_EmailValidationForImport
//CRTDisplayNewUser
//CRTAddUserClient


