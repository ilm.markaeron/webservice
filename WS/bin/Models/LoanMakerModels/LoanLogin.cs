﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace illimitadoWepAPI.Models.LoanMakerModels
{
    public class LoanLogin
    {
        public string UserName { get; set; }
        public string Password { get; set; }
    }

    public class LoginName
    {
        public string _username { get; set; }
    }
    public class LoanLoginContainer
    {
        public string EmpID { get; set; }
        public string Password { get; set; }
        public string isForget { get; set; }
        public string NTID { get; set; }
        public string DefaultPassword { get; set; }
    }
    public class Location
    {
        public string Loc { get; set; }
    }
    public class ListAllLocationResult
    {
        public List<Location> Locations;
        public string myreturn;
    }
    public class LoanGraph
    {
        public string _LoanType { get; set; }
    }
    public class MultiSelectItems
    {
        public string ItemType { get; set; }
        public string ItemID { get; set; }
        public string ItemName { get; set; }
        public string Message { get; set; }
    }
}