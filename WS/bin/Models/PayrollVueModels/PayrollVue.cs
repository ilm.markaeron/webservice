﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace illimitadoWepAPI.Models.PayrollVueModels
{
    public class PayrollVueClients
    {
        public string clientname { get; set; }
        public string CN { get; set; }
    }
    public class PayrollLogin
    {
        public string username { get; set; }

        public string password { get; set; }
    }
    public class GeneratePayroll
    {

        public string clientname { get; set; }

        public string payslipformat { get; set; }
    }
    public class getClientList
    {
        public string ClientName { get; set; }
        public string DB_Settings { get; set; }
    }
    public class getValidationList
    {
        public string CN { get; set; }
        public string option { get; set; }
    }
    public class DuplicateUsers
    {
        public string RowID { get; set; }
        public string EmployeeID { get; set; }
        public string Conflict { get; set; }
    }
    public class Validation_List
    {
        public string validation { get; set; }
    }

    public class deleteUpload
    {
        public string CN { get; set; }
        public string PayOutDate { get; set; }
    }
    public class insertLeave
    {
        public string CN { get; set; }
        public string[] EmpID { get; set; }
        public string[] LeaveDate { get; set; }
        public string[] isHalfDay { get; set; }
        public string[] LeaveType { get; set; }
        public string[] isPaid { get; set; }
        public string[] ApprovedBy { get; set; }
        public string[] DateApproved { get; set; }
        public string[] DateApplied { get; set; }
        public string[] LeaveStatus { get; set; }
        public string[] EmpReason { get; set; }
        public string[] EmpComment { get; set; }
        public string[] ApproverComment { get; set; }
        public string[] LeaveHours { get; set; }
        public string[] LeaveMinutes { get; set; }
    }
    public class UserBulkUpload
    {
        public String[] employeeID { get; set; }
        public String[] fName { get; set; }
        public String[] mName { get; set; }
        public String[] lName { get; set; }
        public String[] accountType { get; set; }
        public String[] tin { get; set; }
        public String[] birthDate { get; set; }
        public String[] Suffix { get; set; }
        public String[] site { get; set; }
        public String[] rowNo { get; set; }
        public string CN { get; set; }
        public string Timezone { get; set; }
    }
    public class DeductionsUploadCategory
    {
        public string CN { get; set; }
        public String[] rowNo { get; set; }
        public String[] DeductionsCategoryID { get; set; }
        public String[] DeductionsCatCode { get; set; }
        public String[] DeductionsCategory { get; set; }
        public String[] CreatedOn { get; set; }
        public String[] CreatedBy { get; set; }
        public String[] ModifiedOn { get; set; }
        public String[] ModifiedBy { get; set; }
    }
    public class DeductionsUploadType
    {
        public string CN { get; set; }
        public String[] rowNo { get; set; }
        public String[] DeductionTypeID { get; set; }
        public String[] DeductionCode { get; set; }
        public String[] DeductionDescription { get; set; }
        public String[] DeductionCategoryID { get; set; }
        public String[] IsTaxable { get; set; }
        public String[] IsBonus { get; set; }
        public String[] CreatedOn { get; set; }
        public String[] CreatedBy { get; set; }
        public String[] ModifiedOn { get; set; }
        public String[] ModifiedBy { get; set; }
        public String[] Active { get; set; }
    }
    public class DeductionEmployee
    {
        public String[] RowNum { get; set; }
        public string CN { get; set; }
        public String[] EmpID { get; set; }
        public String[] DeductionTypeID { get; set; }
        public String[] OneTime { get; set; }
        public String[] DeductionAmount { get; set; }
        public String[] StartDate { get; set; }
        public String[] EndDate { get; set; }
        public String[] NoEndDate { get; set; }
        public String[] PaymentTermsID { get; set; }
        public String[] IsPause { get; set; }
        public String[] IsResume { get; set; }
        public String[] IsStop { get; set; }
        public String[] IsDone { get; set; }
        public String[] UpdateAmount { get; set; }
        public String[] UpdateAmountOn { get; set; }
        public String[] UpdateAmountBy { get; set; }
        public String[] PausedOn { get; set; }
        public String[] PausedBy { get; set; }
        public String[] ResumedOn { get; set; }
        public String[] ResumedBy { get; set; }
        public String[] StoppedOn { get; set; }
        public String[] StoppedBy { get; set; }
        public String[] CreatedOn { get; set; }
        public String[] CreatedBy { get; set; }
        public String[] ModifiedOn { get; set; }
        public String[] ModifiedBy { get; set; }
    }
    public class DeductionEmployeeDetails
    {
        public string CN { get; set; }
        public String[] EmployeeDeductionDetailsID { get; set; }
        public String[] EmployeeDeductionID { get; set; }
        public String[] DeductionAmount { get; set; }
        public String[] PayOutDate { get; set; }
        public String[] ProcessedOn { get; set; }
        public String[] ProcessedBy { get; set; }
        public String[] ClosedOn { get; set; }
        public String[] ClosedBy { get; set; }

    }
    public class EarningsCategoryList
    {
        public string CN { get; set; }
        public String[] EarningsCatCode { get; set; }
        public String[] EarningsCategory { get; set; }
        public String[] CreatedOn { get; set; }
        public String[] CreatedBy { get; set; }
        public String[] ModifiedOn { get; set; }
        public String[] ModifiedBy { get; set; }
        public String[] ClosedOn { get; set; }

    }
    public class EarningsTypeList
    {
        public string CN { get; set; }
        public String[] EarningsCode { get; set; }
        public String[] EarningsDescription { get; set; }
        public String[] EarningsCategoryID { get; set; }
        public String[] IsTaxable { get; set; }
        public String[] IsBonus { get; set; }
        public String[] AddTo { get; set; }
        public String[] CreatedOn { get; set; }
        public String[] CreatedBy { get; set; }
        public String[] ModifiedOn { get; set; }
        public String[] ModifiedBy { get; set; }
    }

    public class EmployeeEarningsList
    {
        public string CN { get; set; }
        public String[] EmpID { get; set; }
        public String[] EarningsTypeID { get; set; }
        public String[] OneTime { get; set; }
        public String[] EarningsAmount { get; set; }
        public String[] StartDate { get; set; }
        public String[] EndDate { get; set; }
        public String[] NoEndDate { get; set; }
        public String[] PaymentTermsID { get; set; }
        public String[] IsPause { get; set; }
        public String[] IsResume { get; set; }
        public String[] IsStop { get; set; }
        public String[] IsDone { get; set; }
        public String[] PausedOn { get; set; }
        public String[] PausedBy { get; set; }
        public String[] ResumedOn { get; set; }
        public String[] ResumedBy { get; set; }
        public String[] StoppedOn { get; set; }
        public String[] StoppedBy { get; set; }
        public String[] UpdateAmount { get; set; }
        public String[] UpdateAmountOn { get; set; }
        public String[] UpdateAmountBy { get; set; }
        public String[] CreatedOn { get; set; }
        public String[] CreatedBy { get; set; }
        public String[] ModifiedOn { get; set; }
        public String[] ModifiedBy { get; set; }

        public String[] EmpName { get; set; }
        public String[] EarningsName { get; set; }
        public String[] FinalDeduction { get; set; }
    }
    public class EmployeeEarningsDetailsList
    {
        public string CN { get; set; }
        public String[] EmployeeEarningsDetailsID { get; set; }
        public String[] EmployeeEarningsID { get; set; }
        public String[] EarningsAmount { get; set; }
        public String[] PayOutDate { get; set; }
        public String[] ProcessedOn { get; set; }
        public String[] ProcessedBy { get; set; }
        public String[] ClosedOn { get; set; }
        public String[] ClosedBy { get; set; }
    }

    public class LoanList
    {
        public string CN { get; set; }
        public String[] Loan_Amount { get; set; }
        public String[] Loan_Term { get; set; }

    }

    public class LoanNameList
    {

        public string CN { get; set; }
        public String[] LoanNameCode { get; set; }
        public String[] LoanName { get; set; }
        public String[] LoanTypeID { get; set; }
        public String[] CreatedOn { get; set; }
        public String[] CreatedBy { get; set; }
        public String[] ModifiedOn { get; set; }
        public String[] ModifiedBy { get; set; }
        public String[] Active { get; set; }

    }

    public class LoanTypeList
    {
        public string CN { get; set; }
        public String[] LoanTypeID { get; set; }
        public String[] LoanTypeCode { get; set; }
        public String[] LoanTypeName { get; set; }
        public String[] CreatedOn { get; set; }
        public String[] CreatedBy { get; set; }
        public String[] ModifiedOn { get; set; }
        public String[] ModifiedBy { get; set; }
    }
    public class EmployeeLoanList
    {
        public string CN { get; set; }
        public String[] EmpID { get; set; }
        public String[] LoanTypeID { get; set; }
        public String[] LoanNameID { get; set; }
        public String[] PaymentTermsID { get; set; }
        public String[] LoanDate { get; set; }
        public String[] PrincipalAmount { get; set; }
        public String[] WithInterest { get; set; }
        public String[] BeginningBalance { get; set; }
        public String[] AmortizationAmount { get; set; }
        public String[] StatusID { get; set; }
        public String[] StartDate { get; set; }
        public String[] EndDate { get; set; }
        public String[] IsPause { get; set; }
        public String[] IsResume { get; set; }
        public String[] IsStop { get; set; }
        public String[] IsDone { get; set; }
        public String[] PausedOn { get; set; }
        public String[] PausedBy { get; set; }
        public String[] ResumedOn { get; set; }
        public String[] ResumedBy { get; set; }
        public String[] StoppedOn { get; set; }
        public String[] StoppedBy { get; set; }
        public String[] CreatedOn { get; set; }
        public String[] CreatedBy { get; set; }
        public String[] ModifiedOn { get; set; }
        public String[] ModifiedBy { get; set; }
        public String[] ClosedOn { get; set; }
        public String[] ClosedBy { get; set; }
    }

    public class EmployeeLoanDetailsList
    {
        public string CN { get; set; }
        public String[] EmployeeLoanDetailsID { get; set; }
        public String[] EmployeeLoanID { get; set; }
        public String[] AmortizationAmount { get; set; }
        public String[] DateDeducted { get; set; }
        public String[] PaymentTermsID { get; set; }
        public String[] PayOutDate { get; set; }
        public String[] ProcessedOn { get; set; }
        public String[] ProcessedBy { get; set; }

    }

    public class EmployeeLogHoursList
    {
        public string CN { get; set; }
        public String[] EmployeeLogHoursID { get; set; }
        public String[] EmpID { get; set; }
        public String[] Basicpay { get; set; }
        public String[] PayOutSchemeID { get; set; }
        public String[] HoursWorked { get; set; }
        public String[] Tardiness { get; set; }
        public String[] Absences { get; set; }
        public String[] Undertime { get; set; }
        public String[] LWOP { get; set; }
        public String[] HourlyRate { get; set; }
        public String[] EfficiencyRate { get; set; }
        public String[] PaymentTermsID { get; set; }
        public String[] PayOutDate { get; set; }
        public String[] IsDone { get; set; }
        public String[] IsAdded { get; set; }
        public String[] AddedOn { get; set; }
        public String[] AddedBy { get; set; }
        public String[] ProcessedOn { get; set; }
        public String[] ProcessedBy { get; set; }
        public String[] ClosedOn { get; set; }
        public String[] ClosedBy { get; set; }
        public String[] CutOffStartDate { get; set; }
        public String[] CutOffEndDate { get; set; }
    }
    //public class EmployeeLogHoursDetailsList
    //{
    //    public String[] RowNum { get; set; }
    //    public string CN { get; set; }
    //    public String[] EmployeeLogHoursID { get; set; }
    //    public String[] HoursworkedAmount { get; set; }
    //    public String[] TardinessAmount { get; set; }
    //    public String[] AbsencesAmount { get; set; }
    //    public String[] UndertimeAmount { get; set; }
    //    public String[] LWOPAmount { get; set; }
    //    public String[] PaymentTermsID { get; set; }
    //    public String[] PayOutDate { get; set; }
    //    public String[] ProcessedOn { get; set; }
    //    public String[] ProcessedBy { get; set; }

    //}

    public class EmployeeHourDetailsList
    {
        public string CN { get; set; }
        public String[] EmployeeLogHoursDetailsID { get; set; }
        public String[] EmployeeLogHoursID { get; set; }
        public String[] HoursworkedAmount { get; set; }
        public String[] TardinessAmount { get; set; }
        public String[] AbsencesAmount { get; set; }
        public String[] UndertimeAmount { get; set; }
        public String[] LWOPAmount { get; set; }
        public String[] PaymentTermsID { get; set; }
        public String[] PayOutDate { get; set; }
        public String[] ProcessedOn { get; set; }
        public String[] ProcessedBy { get; set; }

    }

    public class OvertimeList
    {
        public string CN { get; set; }
        public String[] OvertimeCode { get; set; }
        public String[] OvertimeDescription { get; set; }
        public String[] Rate { get; set; }
        public String[] CreatedOn { get; set; }
        public String[] CreatedBy { get; set; }
        public String[] ModifiedOn { get; set; }
        public String[] ModifiedBy { get; set; }

    }

    public class EmployeeOvertimeList
    {
        public string CN { get; set; }
        public String[] EmployeeOvertimeID { get; set; }
        public String[] EmpID { get; set; }
        public String[] OvertimeID { get; set; }
        public String[] OvertimeHours { get; set; }
        public String[] CompanyRate { get; set; }
        public String[] HourlyRate { get; set; }
        public String[] PaymentTermsID { get; set; }
        public String[] PayOutDate { get; set; }
        public String[] IsDone { get; set; }
        public String[] IsAdded { get; set; }
        public String[] AddedOn { get; set; }
        public String[] AddedBy { get; set; }
        public String[] ProcessedOn { get; set; }
        public String[] ProcessedBy { get; set; }
        public String[] ClosedOn { get; set; }
        public String[] ClosedBy { get; set; }

      

    }
    public class EmployeeOvertimeDetailsList
    {
        public string CN { get; set; }
        public String[] EmployeeOvertimeDetailsID { get; set; }
        public String[] EmployeeOvertimeID { get; set; }
        public String[] OvertimeAmount { get; set; }
        public String[] PaymentTermsID { get; set; }
        public String[] PayOutDate { get; set; }
        public String[] ProcessedOn { get; set; }
        public String[] ProcessedBy { get; set; }
    }

    public class EmployeePagibigDetails
    {
        public string CN { get; set; }
        public String[] EmployeePagIbigID { get; set; }
        public String[] EmpID { get; set; }
        public String[] GrossPay { get; set; }
        public String[] PagIbigAmount { get; set; }
        public String[] PagIbigEmployerAmount { get; set; }
        public String[] PaymentTermsID { get; set; }
        public String[] PayOutDate { get; set; }
        public String[] ProcessedOn { get; set; }
        public String[] ProcessedBy { get; set; }

    }

    public class EmployeePHILHEALTHDetails
    {
        public string CN { get; set; }
        public String[] EmployeePhilHealthID { get; set; }
        public String[] EmpID { get; set; }
        public String[] GrossPay { get; set; }
        public String[] PhilHealthAmount { get; set; }
        public String[] PhilHealthEmployerAmount { get; set; }
        public String[] PaymentTermsID { get; set; }
        public String[] PayOutDate { get; set; }
        public String[] ProcessedOn { get; set; }
        public String[] ProcessedBy { get; set; }


    }
    public class EmployeeSSSDetails
    {
        public string CN { get; set; }
        public String[] EmployeeSSSID { get; set; }
        public String[] EmpID { get; set; }
        public String[] GrossPay { get; set; }
        public String[] SSSAmount { get; set; }
        public String[] SSSEmployerAmount { get; set; }
        public String[] ECAmount { get; set; }
        public String[] PaymentTermsID { get; set; }
        public String[] PayOutDate { get; set; }
        public String[] ProcessedOn { get; set; }
        public String[] ProcessedBy { get; set; }

    }

    public class EmployeeNetpayList
    {
        public string CN { get; set; }
        public String[] EmployeeNetPayID { get; set; }
        public String[] EmpID { get; set; }
        public String[] PayoutSchemeID { get; set; }
        public String[] PaymentTermsID { get; set; }
        public String[] PayOutDate { get; set; }
        public String[] BasicPay { get; set; }

        public String[] HoursWorkedAmount { get; set; }
        public String[] TardinessAmount { get; set; }
        public String[] AbsencesAmount { get; set; }
        public String[] UndertimeAmount { get; set; }
        public String[] LWOP { get; set; }

        public String[] EfficiencyRate { get; set; }
        public String[] GrossPay { get; set; }
        public String[] TaxableIncome { get; set; }
        public String[] OvertimeGrossPay { get; set; }
        public String[] SSSAmount { get; set; }

        public String[] PhilHealthAmount { get; set; }
        public String[] PagIbigAmount { get; set; }
        public String[] EarningsTotal { get; set; }
        public String[] EarningsGrossPay { get; set; }
        public String[] EarningsTaxableAmount { get; set; }

        public String[] EarningsNonTaxableAmount { get; set; }
        public String[] EarningsTaxableBonus { get; set; }
        public String[] EarningsNonTaxableBonus { get; set; }
        public String[] DeductionsTotal { get; set; }
        public String[] DeductionsGrossPay { get; set; }

        public String[] DeductionsTaxableAmount { get; set; }
        public String[] DeductionsNonTaxableAmount { get; set; }
        public String[] DeductionsTaxableBonus { get; set; }
        public String[] DeductionsNonTaxableBonus { get; set; }
        public String[] TotalLoanAmount { get; set; }

        public String[] WHTax { get; set; }
        public String[] NetPay { get; set; }

    }

    public class InsertAT
    {
        public string emp { get; set; }
        public string client { get; set; }
        public string ipaddress { get; set; }
        public string status { get; set; }
    }

    public class getPayrollAT
    {
        public List<PayrollAuditTrail> result;
        public string myreturn;
    }
    public class PayrollAuditTrail
    {
        public string empid { get; set; }
        public string empname { get; set; }
        public string client { get; set; }
        public string status { get; set; }
        public string datestamp { get; set; }
        public string ipaddress { get; set; }
    }

    public class EmployeeEarningsListV2
    {
        public string CN { get; set; }
        public String[] EmpID { get; set; }
        public String[] EarningsTypeID { get; set; }
        public String[] OneTime { get; set; }
        public String[] EarningsAmount { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public String[] NoEndDate { get; set; }
        public String[] PaymentTermsID { get; set; }
        public String[] IsPause { get; set; }
        public String[] IsResume { get; set; }
        public String[] IsStop { get; set; }
        public String[] IsDone { get; set; }
        public String[] PausedOn { get; set; }
        public String[] PausedBy { get; set; }
        public String[] ResumedOn { get; set; }
        public String[] ResumedBy { get; set; }
        public String[] StoppedOn { get; set; }
        public String[] StoppedBy { get; set; }
        public String[] UpdateAmount { get; set; }
        public String[] UpdateAmountOn { get; set; }
        public String[] UpdateAmountBy { get; set; }
        public String[] CreatedOn { get; set; }
        public String[] CreatedBy { get; set; }
        public String[] ModifiedOn { get; set; }
        public String[] ModifiedBy { get; set; }

        //public String[] EmpName { get; set; }
        //public String[] EarningsName { get; set; }
        //public String[] FinalDeduction { get; set; }
    }

    public class EmployeeLoanListV2
    {
        public string CN { get; set; }
        public String[] EmpID { get; set; }
        public String[] LoanTypeID { get; set; }
        public String[] LoanNameID { get; set; }
        public String[] PaymentTermsID { get; set; }
        public String[] LoanDate { get; set; }
        public String[] PrincipalAmount { get; set; }
        public String[] WithInterest { get; set; }
        public String[] BeginningBalance { get; set; }
        public String[] AmortizationAmount { get; set; }
        public String[] StatusID { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public String[] IsPause { get; set; }
        public String[] IsResume { get; set; }
        public String[] IsStop { get; set; }
        public String[] IsDone { get; set; }
        public String[] PausedOn { get; set; }
        public String[] PausedBy { get; set; }
        public String[] ResumedOn { get; set; }
        public String[] ResumedBy { get; set; }
        public String[] StoppedOn { get; set; }
        public String[] StoppedBy { get; set; }
        public String[] CreatedOn { get; set; }
        public String[] CreatedBy { get; set; }
        public String[] ModifiedOn { get; set; }
        public String[] ModifiedBy { get; set; }
        public String[] ClosedOn { get; set; }
        public String[] ClosedBy { get; set; }
    }

    public class DeductionEmployeeV2
    {
        public String[] RowNum { get; set; }
        public string CN { get; set; }
        public String[] EmpID { get; set; }
        public String[] DeductionTypeID { get; set; }
        public String[] OneTime { get; set; }
        public String[] DeductionAmount { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public String[] NoEndDate { get; set; }
        public String[] PaymentTermsID { get; set; }
        public String[] IsPause { get; set; }
        public String[] IsResume { get; set; }
        public String[] IsStop { get; set; }
        public String[] IsDone { get; set; }
        public String[] UpdateAmount { get; set; }
        public String[] UpdateAmountOn { get; set; }
        public String[] UpdateAmountBy { get; set; }
        public String[] PausedOn { get; set; }
        public String[] PausedBy { get; set; }
        public String[] ResumedOn { get; set; }
        public String[] ResumedBy { get; set; }
        public String[] StoppedOn { get; set; }
        public String[] StoppedBy { get; set; }
        public String[] CreatedOn { get; set; }
        public String[] CreatedBy { get; set; }
        public String[] ModifiedOn { get; set; }
        public String[] ModifiedBy { get; set; }
    }

    public class EmployeeAdjustmentsListV2
    {
        public string CN { get; set; }
        public String[] EmpID { get; set; }

        //public String[] DeductionTypeID { get; set; }
        //public String[] DeductionAmount { get; set; }

        public String[] AdjustedAmount { get; set; }

        public String[] AdjustmentTypeID { get; set; }
        public String[] OneTime { get; set; }
        //public String[] EarningsAmount { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public String[] NoEndDate { get; set; }
        public String[] PaymentTermsID { get; set; }
        public String[] IsPause { get; set; }
        public String[] IsResume { get; set; }
        public String[] IsStop { get; set; }
        public String[] IsDone { get; set; }
        public String[] PausedOn { get; set; }
        public String[] PausedBy { get; set; }
        public String[] ResumedOn { get; set; }
        public String[] ResumedBy { get; set; }
        public String[] StoppedOn { get; set; }
        public String[] StoppedBy { get; set; }
        public String[] UpdateAmount { get; set; }
        public String[] UpdateAmountOn { get; set; }
        public String[] UpdateAmountBy { get; set; }
        public String[] CreatedOn { get; set; }
        public String[] CreatedBy { get; set; }
        public String[] ModifiedOn { get; set; }
        public String[] ModifiedBy { get; set; }

        public String[] EmpName { get; set; }
        public String[] EarningsName { get; set; }
        public String[] FinalDeduction { get; set; }


        
        
    }
    public class PayrolRegister
    {
        /// <netpay>

        public string CN { get; set; }
        public String[] NPEmployeeNetPayID { get; set; }//
        public String[] NPEmpID { get; set; }//
        public String[] NPPayoutSchemeID { get; set; }
        public String[] NPPaymentTermsID { get; set; }
        public String[] NPPayOutDate { get; set; }
        public String[] NPBasicPay { get; set; }

        public String[] NPHoursWorkedAmount { get; set; }
        public String[] NPTardinessAmount { get; set; }//
        public String[] NPAbsencesAmount { get; set; }
        public String[] NPUndertimeAmount { get; set; }//
        public String[] NPLWOP { get; set; }//

        public String[] NPEfficiencyRate { get; set; }
        public String[] NPGrossPay { get; set; }//
        public String[] NPTaxableIncome { get; set; }//
        public String[] NPOvertimeGrossPay { get; set; }
        public String[] NPSSSAmount { get; set; }

        public String[] NPPhilHealthAmount { get; set; }
        public String[] NPPagIbigAmount { get; set; }
        public String[] NPEarningsTotal { get; set; }
        public String[] NPEarningsGrossPay { get; set; }
        public String[] NPEarningsTaxableAmount { get; set; }

        public String[] NPEarningsNonTaxableAmount { get; set; }
        public String[] NPEarningsTaxableBonus { get; set; }
        public String[] NPEarningsNonTaxableBonus { get; set; }
        public String[] NPDeductionsTotal { get; set; }
        public String[] NPDeductionsGrossPay { get; set; }

        public String[] NPDeductionsTaxableAmount { get; set; }
        public String[] NPDeductionsNonTaxableAmount { get; set; }
        public String[] NPDeductionsTaxableBonus { get; set; }
        public String[] NPDeductionsNonTaxableBonus { get; set; }
        public String[] NPTotalLoanAmount { get; set; }

        public String[] NPWHTax { get; set; }
        public String[] NPNetPay { get; set; }//

        /// </netpay>
        ///<EmployeeLoghours>
        public String[] ELEmployeeLogHoursID { get; set; }//
        public String[] ELEmpID { get; set; }//
        public String[] ELBasicpay { get; set; }//
        public String[] ELPayOutSchemeID { get; set; }
        public String[] ELHoursWorked { get; set; }
        public String[] ELTardiness { get; set; }//
        public String[] ELAbsences { get; set; }//
        public String[] ELUndertime { get; set; }//
        public String[] ELLWOP { get; set; }//
        public String[] ELHourlyRate { get; set; }//
        public String[] ELEfficiencyRate { get; set; }
        public String[] ELPaymentTermsID { get; set; }
        public String[] ELPayOutDate { get; set; }
        public String[] ELIsDone { get; set; }
        public String[] ELIsAdded { get; set; }
        public String[] ELAddedOn { get; set; }
        public String[] ELAddedBy { get; set; }
        public String[] ELProcessedOn { get; set; }
        public String[] ELProcessedBy { get; set; }
        public String[] ELClosedOn { get; set; }
        public String[] ELClosedBy { get; set; }
        public String[] ELCutOffStartDate { get; set; }
        public String[] ELCutOffEndDate { get; set; }
        ///</EmployeeLoghours>

        ///<EmployeeLoghoursDetails>
        public String[] ELDEmployeeLogHoursDetailsID { get; set; }//
        public String[] ELDEmployeeLogHoursID { get; set; }//
        public String[] ELDHoursworkedAmount { get; set; }
        public String[] ELDTardinessAmount { get; set; }//
        public String[] ELDAbsencesAmount { get; set; }//
        public String[] ELDUndertimeAmount { get; set; }//
        public String[] ELDLWOPAmount { get; set; }//
        public String[] ELDPaymentTermsID { get; set; }
        public String[] ELDPayOutDate { get; set; }
        public String[] ELDProcessedOn { get; set; }
        public String[] ELDProcessedBy { get; set; }

        ///</EmployeeLoghoursDetails>

        ///<EmployeePagibigdetails>

        public String[] EPIEmployeePagIbigID { get; set; }//
        public String[] EPIEmpID { get; set; }//
        public String[] EPIGrossPay { get; set; }//
        public String[] EPIPagIbigAmount { get; set; }//
        public String[] EPIPagIbigEmployerAmount { get; set; }//
        public String[] EPIPaymentTermsID { get; set; }
        public String[] EPIPayOutDate { get; set; }
        public String[] EPIProcessedOn { get; set; }
        public String[] EPIProcessedBy { get; set; }

        ///</EmployeePagibigdetails>

        ///<PhilhealthDetails>
        public String[] EPHEmployeePhilHealthID { get; set; }//
        public String[] EPHEmpID { get; set; }//
        public String[] EPHGrossPay { get; set; }//
        public String[] EPHPhilHealthAmount { get; set; }//
        public String[] EPHPhilHealthEmployerAmount { get; set; }//
        public String[] EPHPaymentTermsID { get; set; }
        public String[] EPHPayOutDate { get; set; }
        public String[] EPHProcessedOn { get; set; }
        public String[] EPHProcessedBy { get; set; }
        ///</PhilhealthDetails>

        ///<SSSdetails>
        public String[] SSSEmployeeSSSID { get; set; }//
        public String[] SSSEmpID { get; set; }//
        public String[] SSSGrossPay { get; set; }//
        public String[] SSSSSSAmount { get; set; }//
        public String[] SSSSSSEmployerAmount { get; set; }//
        public String[] SSSECAmount { get; set; }//
        public String[] SSSPaymentTermsID { get; set; }
        public String[] SSSPayOutDate { get; set; }
        public String[] SSSProcessedOn { get; set; }
        public String[] SSSProcessedBy { get; set; }

        ///</SSSdetails>


        /// <EmployeeOvertime>
        public String[] EOEmployeeOvertimeID { get; set; }
        public String[] EOEmpID { get; set; }
        public String[] EOOvertimeID { get; set; }
        public String[] EOOvertimeHours { get; set; }
        public String[] EOCompanyRate { get; set; }
        public String[] EOHourlyRate { get; set; }
        public String[] EOPaymentTermsID { get; set; }
        public String[] EOPayOutDate { get; set; }
        public String[] EOIsDone { get; set; }
        public String[] EOIsAdded { get; set; }
        public String[] EOAddedOn { get; set; }
        public String[] EOAddedBy { get; set; }
        public String[] EOProcessedOn { get; set; }
        public String[] EOProcessedBy { get; set; }
        public String[] EOClosedOn { get; set; }
        public String[] EOClosedBy { get; set; }
        public String[] EOEmployeeOvertimeDetailsID { get; set; }
        public String[] EOOvertimeAmount { get; set; }
        public string payoutdate { get; set; }
        public string cutoffstart { get; set; }
        public string cutoffend { get; set; }
  //      public string EOOvertimeAmount { get; set; }
        /// </EmployeeOvertime>
    }




}