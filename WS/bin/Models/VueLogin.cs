﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace illimitadoWepAPI.Models
{
    public class VueLogin
    {
        public string EmpID { get; set; }
        public string Password { get; set; }
    }
    public class VueAccess
    {
        public string Access { get; set; }
    }

    public class VueUserProfile
    {
        public string EmpID { get; set; }
        public string Password { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public string FaceID { get; set; }
        public string Access { get; set; }
        public string JoinDate { get; set; }
        public string Marrieddate { get; set; }
        public string BirthDay { get; set; }
        public string CivilStatus { get; set; }
        public string Coordinates { get; set; }
        public string Alias { get; set; }
        public string ProfilePic { get; set; }
        public string SignatureID { get; set; }
        public string DateNow { get; set; }
        public string DateTimeNow { get; set; }
        public string ReasonDesc { get; set; }
        public string ReasonID { get; set; }
        public string Category { get; set; }
        public string StartTime { get; set; }
    }

    public class BreakList
    {
        public String ReasonID { get; set; }
        public String ReasonDesc { get; set; }
    }
    public class ReasonIDs
    {
        public List<BreakList> BreakList;
    }

    public class VueForBreaks
    {
        public string EmpID { get; set; }
        public string ReasonID { get; set; }
        // public string StartOrEnd { get; set; }
    }

    public class VueDaily
    {
        public string SchedDate { get; set; }
        public string ReasonDesc { get; set; }
        public string SchedIN { get; set; }
        public string SchedOUT { get; set; }

    }
    public class VueDailyResult
    {
        public List<VueDaily> Daily;
        public string myreturn;
    }

    public class VueLoggedTeam
    {
        public string EmpID { get; set; }
        public string EmpName { get; set; }
        public string BaseID { get; set; }
        public string Reason { get; set; }

    }
    public class VueLoggedTeamResult
    {
        public List<VueLoggedTeam> VueLoggedTeam;
        public string myreturn;
    }

    public class Favorites
    {

        public string fav1i;
        public string spanfav1;
        public string linkfav1;
        public string favid1;

        public string fav2i;
        public string spanfav2;
        public string linkfav2;
        public string favid2;

        public string fav3i;
        public string spanfav3;
        public string linkfav3;
        public string favid3;

        public string fav4i;
        public string spanfav4;
        public string linkfav4;
        public string favid4;

        public string fav5i;
        public string spanfav5;
        public string linkfav5;
        public string favid5;

        public string fav6i;
        public string spanfav6;
        public string linkfav6;
        public string favid6;

        public string fav7i;
        public string spanfav7;
        public string linkfav7;
        public string favid7;

        public string fav8i;
        public string spanfav8;
        public string linkfav8;
        public string favid8;
    }

    public class TimeRecord
    {
        public string EmpID { get; set; }
        public string PayPeriodFrom { get; set; }
        public string PayPeriodTo { get; set; }
        public string RunningPaidHours { get; set; }
        public string LeavesTaken { get; set; }
        public string Mot { get; set; }
        public string Payfromto { get; set; }
    }

    public class CurrentTimeRecord
    {
        public List<TimeRecord> TimeRecord;
    }
    public class PayoutScheme
    {
        public string EmpID { get; set; }
        public string PayOutID { get; set; }
        public string PayOutName { get; set; }
        public string PayFrom { get; set; }
        public string PayTo { get; set; }
        public string PayOn { get; set; }
        public string PayOutDate { get; set; }
    }

    public class PayPeriod
    {
        public List<PayoutScheme> PayOutScheme;
    }
    public class ImageNewsUpdates
    {
        public string img;
        public string title;
        public string news;
        public string sender;
    }

    public class SelfieRegistrations
    {
        public string NTID { get; set; }
        public string Name { get; set; }
        public string FaceID { get; set; }
    }
    public class SavePicture
    {
        public string b64img { get; set; }
        public string NTID { get; set; }
        public string num { get; set; }
    }

    public class VueFaceLogin
    {
        public string Access { get; set; }
        public string EmpID { get; set; }
        public string Password { get; set; }
    }

    public class ScheduleParam
    {
        public string EmpID { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string SchedDate { get; set; }
        public string Month { get; set; }
        public string Day { get; set; }
        public string Year { get; set; }
    }


    public class VueSchedule
    {
        public string Day { get; set; }
        public string SchedIN { get; set; }
        public string SchedOUT { get; set; }
        public string SchedDate { get; set; }
        public string HourWork { get; set; }
    }

    public class VueAcitivity
    {
        public string SchedDate { get; set; }
        public string SchedIN { get; set; }
        public string SchedOUT { get; set; }
        public string SchedType { get; set; }
        public string TotalTimeMin { get; set; }
    }



}