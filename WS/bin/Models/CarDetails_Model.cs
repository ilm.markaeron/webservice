﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace illimitadoWepAPI.Models
{
    public class CarDetails_Model
    {
    }

    public class getCarDetails
    {
        public string ID { get; set; }
        public string brandname { get; set; }
        public string modelname { get; set; }
        public string variantname { get; set; }
        public string type { get; set; }

    }
    public class carlistfinal
    {
        public List<getCarDetails> CarResult;
        public string myreturn;
    }

    public class BankList
    {
        public string BankID { get; set; }
        public string BankName { get; set; }
    }
    public class banklistfinal
    {
        public List<BankList> BankResult;
        public string myreturn;
    }
    #region 2018-09-10
    public class BrandList
    {
        public List<GetBrand> Brand;
        public string myreturn;
    }
    public class GetBrand
    {
        public string Brand { get; set; }
    }



    public class ModelList
    {
        public List<GetModel> Model;
        public string myreturn;
    }
    public class GetModel
    {
        public string Model { get; set; }
    }


    public class VariantList
    {
        public List<GetVariant> Variant;
        public string myreturn;
    }
    public class GetVariant
    {
        public string Variant { get; set; }
    }


    public class ListResult
    {
        public string CN { get; set; }
        public string TYPE { get; set; }
        public string BRAND { get; set; }
        public string MODEL { get; set; }
    }


    #endregion
}