﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace illimitadoWepAPI.Models.ADBCalculatorModel
{
    public class PlanList
    {
        public string _planid { get; set; }
        public string _contractyears { get; set; }
        public string _planname { get; set; }
        public string _pricingcolumns { get; set; }
        public string _plansequenceid { get; set; }
    }
    public class PlanListClass
    {
        public List<PlanList> _planlist;
        public string _myreturn { get; set; }
    }

    public class PlanParam
    {
        public string _usercount { get; set; }
        public string _pricingcolumn { get; set; }

    }
    public class PlanResult
    {
        public string _result { get; set; }
        
    }
    public class PlanResultClass
    {
        public List<PlanResult> _planresult;
        public string _myreturn { get; set; }
    }

    public class PricingList
    {
        public string _pricingid { get; set; }
        public string _usercount { get; set; }
        public string _oneautomatic { get; set; }
        public string _onecruisecontrol { get; set; }
        public string _oneselfdrive { get; set; }
        public string _twocruisecontrol { get; set; }
        public string _twoselfdrive { get; set; }
        public string _threeselfdrive { get; set; }
        public string _fiveselfdrive { get; set; }
    }
    public class PricingListClass
    {
        public List<PricingList> _pricinglist;
        public string _myreturn { get; set; }
    }

    public class deletePricing
    {
        public string ID { get; set; }
    }

    public class deletePlan
    {
        public string ID { get; set; }
    }
}