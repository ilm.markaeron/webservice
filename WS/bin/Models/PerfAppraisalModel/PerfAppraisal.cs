﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace illimitadoWepAPI.Models
{

    public class GetComGoalsGroup
    {
        public string ID { get; set; }
        public string CN { get; set; }
    }
    public class isAuthEscalate
    {
        public string company { get; set; }
        public string position { get; set; }
        public string level { get; set; }
    }
    public class GetisAuthEscalate
    {
        public List<isAuthEscalateLists> result;
        public string myreturn;
    }
    public class isAuthEscalateLists
    {
        public string result { get; set; }
    }
    public class isReviewer
    {
        public string cID { get; set; }
        public string position { get; set; }
        public string level { get; set; }
    }
    public class GetisReviewer
    {
        public List<isReviewerLists> result;
        public string myreturn;
    }
    public class isReviewerLists
    {
        public string result { get; set; }
    }

    public class InsertSupervisor
    {
        public string cID { get; set; }
        public string sup { get; set; }
        public string supLvl { get; set; }
        public string supesc { get; set; }
    }
    public class GetInsertSupervisor
    {
        public List<InsertSupervisorLists> result;
        public string myreturn;
    }
    public class InsertSupervisorLists
    {
    }
    public class DeleteSupervisor
    {
        public string cID { get; set; }
    }
    public class GetDeleteSupervisor
    {
        public List<DeleteSupervisorLists> result;
        public string myreturn;
    }
    public class DeleteSupervisorLists
    {
    }
    public class SelectSupervisor
    {
        public string cID { get; set; }
    }
    public class GetSelectSupervisor
    {
        public List<SelectSupervisorLists> result;
        public string myreturn;
    }
    public class SelectSupervisorLists
    {
        public string company { get; set; }
        public string supervisor { get; set; }
        public string supervisorLevel { get; set; }
        public string supEscalate { get; set; }
    }
    public class CountPerfView
    {
        public string cID { get; set; }
        public string empID { get; set; }
    }
    public class CountNotificationV2
    {
        public string cID { get; set; }
        public string empID { get; set; }
    }
    public class CountNotificationV2Lists
    {
        public string totalNotif { get; set; }
        public string rev1Total { get; set; }
        public string rev2Total { get; set; }
        public string rev1PlanTotal { get; set; }
        public string rev2PlanTotal { get; set; }
        public string rev1PerfReviewer { get; set; }
        public string rev2PerfReviewer { get; set; }
        public string escPlan1 { get; set; }
        public string escPlan2 { get; set; }
        public string escAck1 { get; set; }
        public string escAck2 { get; set; }
    }
    public class GetCountPerfView
    {
        public List<CountPerfViewLists> result;
        public string myreturn;
    }
    public class GetCountNotificationV2
    {
        public List<CountNotificationV2Lists> result;
        public string myreturn;
    }
    public class CountPerfViewLists
    {
        public string notifCount { get; set; }
    }
    public class GetRevs
    {
        public string cID { get; set; }
    }
    public class deleteRevs
    {
        public string cID { get; set; }
    }
    public class deleteRevsEscalate
    {
        public string cID { get; set; }
    }
    public class GetRevsEscalate
    {
        public string cID { get; set; }
    }
    public class insertRevs
    {
        public string cID { get; set; }
        public string RTitle { get; set; }
        public string RLevel { get; set; }
    }
    public class insertRevsEscalate
    {
        public string cID { get; set; }
        public string RTitle { get; set; }
        public string RLevel { get; set; }
    }
    public class insertRevsLists
    {
    }
    public class insertRevsEscalateLists
    {
    }
    public class UpdateSettings1
    {
        public string cID { get; set; }
        public string rev1_days { get; set; }
        public string rev2_days { get; set; }
        public string message { get; set; }
        public string message_days { get; set; }
        public string rev1_mode { get; set; }
        public string rev2_mode { get; set; }
        public string message_mode { get; set; }
    }
    public class GetRevsLists
    {
        public string reviewerTitle { get; set; }
        public string reviewerLevel { get; set; }
    }
    public class GetRevsEscalateLists
    {
        public string reviewerTitle { get; set; }
        public string reviewerLevel { get; set; }
    }
    public class deleteRevsLists
    {
    }
    public class deleteRevsEscalateLists
    {
    }
    public class UpdateSettings1Lists
    {
    }
    public class GetinsertRevsEscalate
    {
        public List<insertRevsEscalateLists> result;
        public string myreturn;
    }
    public class GetUpdateSettings1
    {
        public List<UpdateSettings1Lists> result;
        public string myreturn;
    }
    public class GetGetRevsEscalate
    {
        public List<GetRevsEscalateLists> result;
        public string myreturn;
    }
    public class GetdeleteRevsEscalate
    {
        public List<deleteRevsEscalateLists> result;
        public string myreturn;
    }
    public class GetComGoals
    {
        public List<GetComGoalsLists> result;
        public string myreturn;
    }
    public class GetinsertRevs
    {
        public List<insertRevsLists> result;
        public string myreturn;
    }
    public class GetdeleteRevs
    {
        public List<deleteRevsLists> result;
        public string myreturn;
    }
    public class GetGetRevs
    {
        public List<GetRevsLists> result;
        public string myreturn;
    }
    public class GetComGoalsLists
    {
        public string id { get; set; }
        public string group_id { get; set; }
        public string goal_name { get; set; }
        public string goal_desc { get; set; }
        public string result { get; set; }
        public string weight { get; set; }
        public string emp_comment { get; set; }
        public string r1_str_com { get; set; }
        public string r1_dnp_com { get; set; }
        public string r1_date_created { get; set; }
        public string r2_str_com { get; set; }
        public string r2_dnp_com { get; set; }
        public string r2_date_created { get; set; }
    }

    public class ValidateCompany
    {
        public string company { get; set; }
    }

    public class GetValidateCompany
    {
        public List<ValidateCompanyLists> result;
        public string myreturn;
    }

    public class ValidateCompanyLists
    {
        public string ClientID { get; set; }
        public string DB_Settings { get; set; }
    }

    public class CheckCompanyAccountExist
    {
        public string company { get; set; }
    }

    public class GetCheckCompanyAccountExist
    {
        public List<CheckCompanyAccountExistLists> result;
        public string myreturn;
    }

    public class CheckCompanyAccountExistLists
    {
        public string result { get; set; }
    }

    public class GetAckDataResult
    {
        public List<AckDataResult> result;
        public string myreturn;
    }

    public class GetAckPlan
    {
        public List<AckPlanDataResultList> result;
        public string myreturn;
    }

    public class GetAckData
    {
        public string cID { get; set; }
        public string group_id { get; set; }
    }
    public class GetAckPlanData
    {
        public string cID { get; set; }
        public string group_id { get; set; }
    }

    public class GetValidUsers
    {
        public List<GetValidUserLists> result;
        public string myreturn;
    }

    public class GetValidUsersId
    {
        public List<GetValidUserListsId> result;
        public string myreturn;
    }

    public class GetUpperHierarchy
    {
        public List<GetUpperHierarchyLists> result;
        public string myreturn;
    }
    public class GetSelfReport
    {
        public List<SelfReportLists> result;
        public string myreturn;
    }
    public class GetGetGoalPlan
    {
        public List<GetGoalPlanLists> result;
        public string myreturn;
    }


    public class GetSetGoalGroup
    {
        public List<SetGoalGroupLists> result;
        public string myreturn;
    }

    public class SelfReportLists
    {
        public string name { get; set; }
        public string total { get; set; }
        public string pending { get; set; }
        public string done { get; set; }
    }
    public class SetGoalGroupLists
    {
    }

    public class GetValidUserLists
    {
        //public string EmpID { get; set; }
        //public string Emp_FName { get; set; }
        //public string Emp_MName { get; set; }
        //public string Emp_LName { get; set; }
        //public string EmpLevel { get; set; }
        //public string ImSup_ID { get; set; }
        //public string id { get; set; }

        public string ClientID { get; set; }
        public string EmpID { get; set; }
        public string First_Name { get; set; }
        public string Middle_Name { get; set; }
        public string Last_Name { get; set; }
        public string EmpLevel { get; set; }
        public string MngrID { get; set; }
        public string MngrName { get; set; }
        //public string username { get; set; }
        //public string password { get; set; }
    }
    public class GetValidUserLists2
    {

        public string ClientID { get; set; }
        public string EmpID { get; set; }
        public string First_Name { get; set; }
        public string Middle_Name { get; set; }
        public string Last_Name { get; set; }
        public string EmpLevel { get; set; }
        public string MngrID { get; set; }
        public string MngrName { get; set; }
      
    }

    public class GetValidUserListsId
    {
        public string Emp_ID { get; set; }
        public string First_Name { get; set; }
        public string Middle_Name { get; set; }
        public string Last_Name { get; set; }
        public string EmpLevel { get; set; }
        public string MngrID { get; set; }
        public string MngrName { get; set; }
    }

    public class GetUpperHierarchyLists
    {
        public string Emp_ID { get; set; }
        public string ImSup_ID { get; set; }
    }

    public class GetGoalPlanLists
    {
        public string id { get; set; }
        public string group_id { get; set; }
        public string goal_name { get; set; }
        public string weight { get; set; }
        public string description { get; set; }
        public string com_missed { get; set; }
        public string com_met { get; set; }
        public string com_exceeded { get; set; }
        public string com_dept { get; set; }
        public string goal_type { get; set; }
        public string rev1_comment { get; set; }
        public string rev2_comment { get; set; }
    }

    public class GetDeleteGoal
    {
        public List<GetDeleteGoalLists> result;
        public string myreturn;
    }
    public class GetDeleteGoalLists
    {
    }

    public class GetGetGroup
    {
        public List<GetGroupLists> result;
        public string myreturn;
    }
    public class GetGetReviewer
    {
        public List<GetReviewerLists> result;
        public string myreturn;
    }
    public class GetGetReviewerEscalate
    {
        public List<GetReviewerEscalateLists> result;
        public string myreturn;
    }
    public class GetGroupLists
    {
        public string id { get; set; }
        //public string notif { get; set; }
        public string doer_id { get; set; }
        //public string result_fin { get; set; }
        public string date_created { get; set; }
        //public string approved { get; set; }
        //public string emp_edit { get; set; }

        public string start_pending { get; set; }
        public string mid_pending { get; set; }
        public string end_pending { get; set; }

    }

    public class GetReviewerLists
    {
        public string id { get; set; }
        public string company { get; set; }
        public string reviewerTitle { get; set; }
        public string reviewerLevel { get; set; }

    }
    public class GetReviewerEscalateLists
    {
        public string id { get; set; }
        public string company { get; set; }
        public string reviewerTitle { get; set; }
        public string reviewerLevel { get; set; }

    }

    public class GetGetComUserHeir
    {
        public List<GetComUserHeirLists> result;
        public string myreturn;
    }
    public class GetComUserHeirLists
    {
        public string EmpID { get; set; }

    }
    public class GetGetComUserHeirNotif
    {
        public List<GetComUserHeirNotifLists> result;
        public string myreturn;
    }
    public class GetComUserHeirNotifLists
    {
        public string Emp_ID { get; set; }
        public string Emp_FName { get; set; }
        public string Emp_MName { get; set; }
        public string Emp_LName { get; set; }
        public string EmpLevel { get; set; }
        public string ImSup_ID { get; set; }
        public string ImSup_Name { get; set; }
        
        

    }
    public class GetSelectAll
    {
        public List<GetSelectAllLists> result;
        public string myreturn;
    }
    public class GetSelectAllLists
    {
        public string Emp_ID { get; set; }
        public string Emp_FName { get; set; }
        public string Emp_MName { get; set; }
        public string Emp_LName { get; set; }
        public string ImSup_ID { get; set; }
        public string ImSup_Name { get; set; }
        public string EmpLevel { get; set; }
        

    }

    public class GetResult
    {
        public string company { get; set; }
    }

    public class GetGetResult
    {
        public List<GetResultLists> result;
        public string myreturn;
    }
    public class GetFilterReports
    {
        public List<FilterReportsLists> result;
        public string myreturn;
    }

    public class GetDeleteAckPlanData
    {
        public List<DeleteAckPlanDataLists> result;
        public string myreturn;
    }
    public class GetDeleteAckData
    {
        public List<DeleteAckDataLists> result;
        public string myreturn;
    }
    public class GetResultLists
    {
        public string id { get; set; }
        public string name { get; set; }
        public string weight { get; set; }

    }
    public class DeleteAckDataLists
    {

    }

    public class UnlockReviewer1Lists
    {

    }

    public class GetUnlockReviewer1
    {
        public List<UnlockReviewer1Lists> result;
        public string myreturn;
    }
    public class DeleteAckPlanData
    {
        public string id { get; set; }

    }
    public class DeleteAckData
    {
        public string id { get; set; }
        public string ack_lvl { get; set; }
        public string modified_by { get; set; }
    }

    public class UnlockReviewer1
    {
        public string group_id { get; set; }
        public string ack_lvl { get; set; }
        public string modified_by { get; set; }

    }
    public class FilterReportsLists
    {
        public string EmpID { get; set; }
        public string Name { get; set; }
        public string MngrName { get; set; }
        public string reviewerLevel { get; set; }

    }
    public class DeleteAckPlanDataLists
    {
    }

    public class GetGetDates
    {
        public List<GetDatesLists> result;
        public string myreturn;
    }
    public class GetDatesLists
    {
        public string id { get; set; }
        public string name { get; set; }
        public string date_start { get; set; }
        public string date_end { get; set; }

    }

    public class GetSetGroup
    {
        public List<SetGroupLists> result;

        public string myreturn;
    }
    public class SetGroupLists
    {
        public string id { get; set; }
    }

    public class GetisAuth
    {
        public List<isAuthLists> result;
        public string myreturn;
    }
    public class isAuthLists
    {
        public string result { get; set; }
    }


    public class GetSetDate
    {
        public List<SetDateLists> result;
        public string myreturn;
    }
    public class SetDateLists
    {
        public string id { get; set; }
        public string name { get; set; }
        public string date_id { get; set; }
        public string date_start { get; set; }
        public string date_end { get; set; }

    }
    public class GetGetSettings
    {
        public List<GetSettingsLists> result;
        public string myreturn;
    }
    public class GetInitDataCompany
    {
        public List<InitDataCompanyLists> result;
        public string myreturn;
    }
    public class GetSelectEmpDetails
    {
        public List<SelectEmpDetailsLists> result;
        public string myreturn;
    }
    public class GetSettingsLists
    {
        public string company { get; set; }
        public string rev1_days { get; set; }
        public string rev2_days { get; set; }
        public string message { get; set; }
        public string message_days { get; set; }
        public string rev1_mode { get; set; }
        public string rev2_mode { get; set; }
        public string message_mode { get; set; }

    }
    public class SelectEmpDetailsLists
    {
        public string clientID { get; set; }
        public string EmpID { get; set; }
        public string First_Name { get; set; }
        public string Middle_Name { get; set; }
        public string Last_Name { get; set; }
        public string EmpLevel { get; set; }
        public string MngrID { get; set; }
        public string MngrName { get; set; }
        public string username { get; set; }
        public string password { get; set; }
        public string InActiveDate { get; set; }

    }
    public class EscalateReviewerLists
    {
        public string id { get; set; }
        public string esc { get; set; }
        public string getmessage { get; set; }
    }
    public class InitDataCompany
    {
        public string cID { get; set; }
    }
    public class SelectEmpDetails
    {
        public string empID { get; set; }
    }
    public class InitDataCompanyLists
    {
    }
    public class EscalateReviewer
    {
        public string cID { get; set; }
        public string emp { get; set; }

    }
    public class GetAllRev1Plan
    {
        public string cID { get; set; }

    }
    public class GetAllRev1
    {
        public string cID { get; set; }

    }
    public class GetAllNullRev1Plan
    {
        public string cID { get; set; }

    }
    public class GetAllRev2
    {
        public string cID { get; set; }

    }
    public class GetAllRev1PlanLists
    {
        public string creator_id { get; set; }
        public string escalated { get; set; }

    }
    public class GetAllNullRev1PlanLists
    {
        public string creator_id { get; set; }
        public string escalated { get; set; }

    }
    public class GetAllRev1Lists
    {
        public string ack_lvl { get; set; }
        public string creator_id { get; set; }
        public string escalated { get; set; }

    }
    public class GetAllRev2Lists
    {
        public string ack_lvl { get; set; }
        public string creator_id { get; set; }
        public string escalated { get; set; }

    }
    public class CountNotificationPerfAppraisal
    {
        public string cID { get; set; }
        public string empID { get; set; }

    }
    public class CountNotificationPerfAppraisalLists
    {
        public string notifCount { get; set; }

    }
    public class CountNotifRev2
    {
        public string cID { get; set; }
        public string empID { get; set; }

    }
    public class CountNotifRev1
    {
        public string cID { get; set; }
        public string empID { get; set; }

    }
    public class CountNotifRev2Lists
    {
        public string notifCountRev2 { get; set; }

    }
    public class CountNotifRev1Lists
    {
        public string notifCountRev1 { get; set; }

    }

    public class ClassCountNotif
    {
        //FOR THE RESULT
        public List<CountNotifReturns> returnCountNotif;
        public string myreturn;
    }
    public class ClassRev1Count
    {
        //FOR THE RESULT
        public List<CountRev1NotifReturns> returnRev1;
        public string myreturn;
    }

    public class ClassRev2Count
    {
        //FOR THE RESULT
        public List<CountRev2NotifReturns> returnRev2;
        public string myreturn;
    }

    public class CountParams
    {
        public string @cID { get; set; }
        public string @empID { get; set; }
        public string @datetoday { get; set; }
    }


    public class CountNotifReturns
    {
        public string totalNotif { get; set; }
        public string reviewer1 { get; set; }
        public string reviewer2 { get; set; }

    }
    public class CountRev1NotifReturns
    {
        public string Reviewer1 { get; set; }

    }
    public class CountRev2NotifReturns
    {
        public string Reviewer2 { get; set; }

    }
    public class GetGetAllNullRev1Plan
    {
        public List<GetAllNullRev1PlanLists> result;
        public string myreturn;
    }
    public class GetCountNotifRev1
    {
        public List<CountNotifRev1Lists> result;
        public string myreturn;
    }
    public class GetCountNotifRev2
    {
        public List<CountNotifRev2Lists> result;
        public string myreturn;
    }
    public class GetCountNotificationPerfAppraisal
    {
        public List<CountNotificationPerfAppraisalLists> result;
        public string myreturn;
    }
    public class GetGetAllRev2
    {
        public List<GetAllRev2Lists> result;
        public string myreturn;
    }
    public class GetGetAllRev1
    {
        public List<GetAllRev1Lists> result;
        public string myreturn;
    }
    public class GetGetAllRev1Plan
    {
        public List<GetAllRev1PlanLists> result;
        public string myreturn;
    }
    public class GetEscalateReviewer
    {
        public List<EscalateReviewerLists> result;
        public string myreturn;
    }
    public class GetGetPositions
    {
        public List<GetPositionsLists> result;
        public string myreturn;
    }
    public class GetPositionsLists
    {
        public string positions { get; set; }
    }
    public class GetGetRating
    {
        public List<GetRatingLists> result;
        public string myreturn;
    }
    public class GetRatingLists
    {
        public string id { get; set; }
        public string name { get; set; }
        public string company { get; set; }

    }
    public class sp_GetLowerNotifLists
    {
        public string Emp_ID { get; set; }

    }
    public class sp_GetLowerNotif
    {
        public string Emp_ID { get; set; }

    }
    public class Getsp_GetLowerNotif
    {
        public List<sp_GetLowerNotifLists> result;
        public string myreturn;
    }

    public class GetGetScoreCard
    {
        public List<GetScoreCardLists> result;
        public string myreturn;
    }
    public class GetScoreCardLists
    {
        public string id { get; set; }
        public string score_eval { get; set; }
        public string weight { get; set; }
        public string rating_id { get; set; }
        public string result_id { get; set; }
        public string company { get; set; }
        public string alt_id { get; set; }

    }
    public class GetGetGoals
    {
        public List<GetGoalsLists> result;
        public string myreturn;
    }
    public class GetGoalsLists //BRB
    {
      public string id { get; set; }
        public string group_id { get; set; }
        public string goal_desc { get; set; }
        public string weight { get; set; }
        public string com_missed { get; set; }
        public string com_met { get; set; }
        public string com_exceeded { get; set; }
        public string com_dept { get; set; }
        public string start_rev1_com { get; set; }
        public string start_rev2_com { get; set; }
        public string mid_results { get; set; }
        public string mid_remaining_plan { get; set; }
        public string mid_rev1_com { get; set; }
        public string mid_rev2_com { get; set; }
        public string end_results { get; set; }
        public string end_init_achievement { get; set; }
        public string end_rev1_com { get; set; }
        public string end_rev2_com { get; set; }
        public string end_final_achievement { get; set; }
        public string goal_type { get; set; }

    }

    public class GetUpdateGoals
    {
        public List<UpdateGoalsLists> result;
        public string myreturn;
    }
    public class UpdateGoalsLists
    {
    
    }

    public class GetGetDate
    {
        public List<GetDateLists> result;
        public string myreturn;
    }
    public class GetDateLists
    {
        public string id { get; set; }
        public string name { get; set; }
        public string date_id { get; set; }
        public string date_start { get; set; }
        public string date_end { get; set; }

    }
    public class UpdateGoalPlanLists
    {

    }
    public class SetExemptionLists
    {

    }
    public class GetGetRev1
    {
        public List<GetRev1Lists> result;
        public string myreturn;
    }
    public class GetUpdateGoalPlan
    {
        public List<UpdateGoalPlanLists> result;
        public string myreturn;
    }
    public class GetRev1Lists
    {
        public string id { get; set; }
    }
    public class AddCommentPlan
    {
        public string goal_id { get; set; }
        public string comment { get; set; }
        public string com_lvl { get; set; }
        public string date_created { get; set; }
    }
    public class AddCommentPlanLists
    {
    }
    public class GetGetRev2
    {
        public List<GetRev2Lists> result;
        public string myreturn;
    }
    public class GetRev2Lists
    {
        public string id { get; set; }
    }
    public class FilterReports
    {
        public string self { get; set; }
        public string lvl1 { get; set; }
        public string lvl2 { get; set; }
    }
    public class GetGetPending
    {
        public List<GetPendingLists> result;
        public string myreturn;
    }
    public class GetPendingLists
    {
        public string count { get; set; }
    }
    public class GetGetAllPendingRev1
    {
        public List<GetAllPendingRev1Lists> result;
        public string myreturn;
    }
    public class GetUpdateCommentPlan
    {
        public List<UpdateCommentPlanLists> result;
        public string myreturn;
    }
    public class GetInitNewCompanyAccounts
    {
        public List<InitNewCompanyAccountsLists> result;
        public string myreturn;
    }
    public class GetAddCommentPlan
    {
        public List<AddCommentPlanLists> result;
        public string myreturn;
    }
    public class GetDeleteCommentPlan
    {
        public List<DeleteCommentPlanLists> result;
        public string myreturn;
    }
    public class GetDeleteComment
    {
        public List<DeleteCommentLists> result;
        public string myreturn;
    }
    public class DeleteCommentPlan
    {
        public string id { get; set; }
        public string level { get; set; }
    }
    public class DeleteCommentLists
    {
    }
    public class DeleteComment
    {
        public string id { get; set; }
        public string level { get; set; }
    }
    public class GetAllPendingRev1Lists
    {
        public string id { get; set; }
        public string group_id { get; set; }
        public string ack_lvl { get; set; }
        public string status { get; set; }
        public string creator_id { get; set; }
        public string doer_id { get; set; }
        public string final_id { get; set; }
        public string doer_status { get; set; }
        public string date_req { get; set; }
        public string date_ack { get; set; }
        public string escalated { get; set; }
    }
   public class UpdateCommentPlanLists
    {
      
    }

    public class InitNewCompanyAccountsLists
    {

    }

    public class DeleteCommentPlanLists
    {
    }
    public class GetGetAllPendingRev2
    {
        public List<GetAllPendingRev2Lists> result;
        public string myreturn;
    }
    public class GetAllPendingRev2Lists
    {
        public string id { get; set; }
        public string group_id { get; set; }
        public string ack_lvl { get; set; }
        public string status { get; set; }
        public string creator_id { get; set; }
        public string doer_id { get; set; }
        public string final_id { get; set; }
        public string doer_status { get; set; }
        public string date_req { get; set; }
        public string date_ack { get; set; }
        public string escalated { get; set; }
    }
    public class GetGetNotif
    {
        public List<GetNotifLists> result;
        public string myreturn;
    }
    public class GetNotifLists
    {
        public string count { get; set; }
    }
    public class GetSetGoals
    {
        public List<SetGoalsLists> result;
        public string myreturn;
    }
    public class SetGoalsLists
    {
     
    }

    public class GetGetReport
    {
        public List<GetReportLists> result;
        public string myreturn;
    }
    public class GetGetReports
    {
        public List<GetReportsLists> result;
        public string myreturn;
    }
    public class GetReportLists
    {
        public string total { get; set; }
        public string pending { get; set; }
        public string done { get; set; }
        public string Name { get; set; }

    }
    public class GetReportsLists
    {
        public string EmpID { get; set; }
        public string Name { get; set; }
        public string ISName { get; set; }
        public string total_midyear { get; set; }
        public string pending_midyear { get; set; }
        public string done_midyear { get; set; }
        public string total_endyear { get; set; }
        public string pending_endyear { get; set; }
        public string done_endyear { get; set; }
        public string done { get; set; }
        public string level { get; set; }

    }

    public class GetExemptionLists
    {

        public string empid { get; set; }
        public string empname { get; set; }
        public string goalsettings_from { get; set; }
        public string goalsettings_to { get; set; }
        public string midyear_from { get; set; }
        public string midyear_to { get; set; }
        public string yearend_from { get; set; }
        public string yearend_to { get; set; }
        public string company { get; set; }
        public string modified_by { get; set; }

    }

    public class SelectViewDateLists
    {

        public string view_date { get; set; }

    }


    public class GetExemptionByIdLists
    {
       
        public string goalsettings_from { get; set; }
        public string goalsettings_to { get; set; }
        public string midyear_from { get; set; }
        public string midyear_to { get; set; }
        public string yearend_from { get; set; }
        public string yearend_to { get; set; }
        public string modified_by { get; set; }

    }

    public class FilterExemptionLists
    {

        public string empid { get; set; }
        public string empname { get; set; }
        public string goalsettings_from { get; set; }
        public string goalsettings_to { get; set; }
        public string midyear_from { get; set; }
        public string midyear_to { get; set; }
        public string yearend_from { get; set; }
        public string yearend_to { get; set; }
        public string company { get; set; }
        public string modified_by { get; set; }

    }

    public class GetGetReportRev1
    {
        public List<GetReportRev1Lists> result;
        public string myreturn;
    }
    public class GetReportRev1Lists
    {
        public string total { get; set; }
        public string pending { get; set; }
        public string done { get; set; }
    }


    public class GetGetReportRev2
    {
        public List<GetReportRev2Lists> result;
        public string myreturn;
    }
    public class GetReportRev2Lists
    {
        public string total { get; set; }
        public string pending { get; set; }
        public string done { get; set; }
    }

    public class GetGetMaxGroupId
    {
        public List<GetMaxGroupIdLists> result;
        public string myreturn;
    }
    public class GetMaxGroupIdLists
    {
        public string max { get; set; }
       

    }
    public class GetComUserHeirNotif
    {
        public string ID { get; set; }
        public string company { get; set; }
    }

    public class GetComUserHeir
    {
        public string empId { get; set; }
        public string company { get; set; }
    }
    public class GetReport
    {
        public string cID { get; set; }
    }

    public class GetReports
    {
        public string cID { get; set; }
    }
    public class GetExemption
    {
        public string cID { get; set; }
    }
    public class GetReportRev1
    {
        public string cID { get; set; }
    }
    public class GetReportRev2
    {
        public string cID { get; set; }
    }

    public class GetGetReportPlan
    {
        public List<GetReportPlanLists> result;
        public string myreturn;
    }
    public class GetGetReportsPlan
    {
        public List<GetReportsPlanLists> result;
        public string myreturn;
    }

    public class GetGetExemption
    {
        public List<GetExemptionLists> result;
        public string myreturn;
    }

    public class GetSelectViewDate
    {
        public List<SelectViewDateLists> result;
        public string myreturn;
    }
    public class GetGetExemptionById
    {
        public List<GetExemptionByIdLists> result;
        public string myreturn;
    }
    public class GetCountRev1Plan
    {
        public List<CountRev1PlanLists> result;
        public string myreturn;
    }
    public class GetCountRev2Plan
    {
        public List<CountRev2PlanLists> result;
        public string myreturn;
    }
    public class GetGetReportRev1Plan
    {
        public List<GetReportRev1PlanLists> result;
        public string myreturn;
    }
    public class GetGetReportRev2Plan
    {
        public List<GetReportRev2PlanLists> result;
        public string myreturn;
    }
    public class GetReportPlanLists
    {
        public string name { get; set; }
        public string total { get; set; }
        public string pending { get; set; }
        public string done { get; set; }
    }
    public class GetCountRev1Reviewer
    {
        public List<CountRev1ReviewerLists> result;
        public string myreturn;
    }
    public class GetCountRev2Reviewer
    {
        public List<CountRev2ReviewerLists> result;
        public string myreturn;
    }
    public class CountRev2PlanLists
    {
        public string notifcount { get; set; }
    }
    public class CountRev2ReviewerLists
    {
        public string notifcount { get; set; }
    }
    public class CountRev2Reviewer
    {
        public string cID { get; set; }
        public string empID { get; set; }
    }
    public class CountRev1ReviewerLists
    {
        public string notifcount { get; set; }
    }
    public class CountRev1Reviewer
    {
        public string cID { get; set; }
        public string empID { get; set; }
    }
    public class CountRev1PlanLists
    {
        public string notifcount { get; set; }
    }
    public class GetReportRev2PlanLists
    {
        public string name { get; set; }
        public string total { get; set; }
        public string pending { get; set; }
        public string done { get; set; }
    }
    public class GetReportRev1PlanLists
    {
        public string name { get; set; }
        public string total { get; set; }
        public string pending { get; set; }
        public string done { get; set; }
    }
    public class GetReportPlan
    {
        public string cID { get; set; }
    }
    public class GetReportRev2Plan
    {
        public string cID { get; set; }
    }
    public class GetReportRev1Plan
    {
        public string cID { get; set; }
    }
    public class GetReportsPlanLists
    {
        public string EmpID { get; set; }
        public string name { get; set; }
        public string ISName { get; set; }
        public string total_goalsettings { get; set; }
        public string pending_goalsettings { get; set; }
        public string done_goalsettings { get; set; }
        public string total_midyear { get; set; }
        public string pending_midyear { get; set; }
        public string done_midyear { get; set; }
        public string total_endyear { get; set; }
        public string pending_endyear { get; set; }
        public string done_endyear { get; set; }
        public string done { get; set; }
        public string level { get; set; }
    }
    public class GetReportsPlan
    {
        public string cID { get; set; }
    }
    public class GetDates
    {
        public string id { get; set; }
        public string CN { get; set; }
    }
    public class isAuth
    {
        public string company { get; set; }
        public string position { get; set; }
        public string level { get; set; }
    }
    public class SetDate
    {
        public string current_date { get; set; }
        public string company { get; set; }
    }
    public class GetSettings
    {
        public string cID { get; set; }
    }
    public class GetPositions
    {
        public string CN { get; set; }
    }
    public class GetDate
    {
        public string company { get; set; }
    }
    public class GetRev1
    {
        public string CN { get; set; }
    }
    public class GetRev2
    {
        public string CN { get; set; }
    }
    public class GetNotif
    {
        public string id { get; set; }
        public string CN { get; set; }
    }
    public class GetPending
    {
        public string id { get; set; }
        public string CN { get; set; }
    }
    public class GetAllPendingRev1
    {
        public string date { get; set; }
        public string CN { get; set; }
    }
    public class UpdateCommentPlan
    {
        public string goal_id { get; set; }
        public string comment { get; set; }
        public string com_lvl { get; set; }
    }
    public class InitNewCompanyAccounts
    {
        public string company { get; set; }
        public string companyId { get; set; }
    }
    public class GetAllPendingRev2
    {
        public string date { get; set; }
        public string CN { get; set; }
    }
    public class GetGoals
    {
        public string ID { get; set; }
    }
    public class GetGroup
    {
        public string ID { get; set; }
        public string date_start { get; set; }
        public string date_end { get; set; }
    }
    public class GetReviewer
    {
        public string company { get; set; }
        public string reviewerLevel { get; set; }
    }
    public class GetReviewerEscalate
    {
        public string company { get; set; }
        public string reviewerLevel { get; set; }
    }
    public class GetMaxGroupId
    {
        public string company { get; set; }
    }
    public class GetRating
    {
        public string company { get; set; }
    }
    
    public class GetScoreCard
    {
        public string company { get; set; }
    }
    public class SelectAll
    {
        public string CN { get; set; }
    }
    public class SetAck
    {
        public string group_id { get; set; }
        public string ack_lvl { get; set; }
        public string status { get; set; }
        public string creator_id { get; set; }
        public string date_req { get; set; }
        public string cID { get; set; }
    }
    public class SetAckPlanRev1
    {
        public string group_id { get; set; }
        public string status { get; set; }
        public string reviewer1 { get; set; }
        public string date_rev1 { get; set; }
    }
    public class SetAckPlanRev2
    {
        public string group_id { get; set; }
        public string status { get; set; }
        public string reviewer2 { get; set; }
        public string date_rev2 { get; set; }
    }

    public class SetAckPlan
    {
        public string group_id { get; set; }
        public string status { get; set; }
        public string creator_id { get; set; }
        public string date_req { get; set; }
        public string cID { get; set; }
    }

    public class DeleteExemption
    {
        public string cID { get; set; }
    }
    public class CountRev1Plan
    {
        public string cID { get; set; }
        public string empID { get; set; }
    }
    public class CountRev2Plan
    {
        public string cID { get; set; }
        public string empID { get; set; }
    }
    public class SetComment
    {
        public string CN { get; set; }
        public string goal_id { get; set; }
        public string str { get; set; }
        public string dnp { get; set; }
        public string com_lvl { get; set; }
        public string date { get; set; }
    }
    public class SetGoals
    {
        public string group_id { get; set; }
        public string goal_desc { get; set; }
        public string weight { get; set; }
        public string com_missed { get; set; }
        public string com_met { get; set; }
        public string com_exceeded { get; set; }
        public string com_dept { get; set; }
        public string start_rev1_com { get; set; }
        public string start_rev2_com { get; set; }
        public string mid_results { get; set; }
        public string mid_remaining_plan { get; set; }
        public string mid_rev1_com { get; set; }
        public string mid_rev2_com { get; set; }
        public string end_results { get; set; }
        public string end_init_achievement { get; set; }
        public string end_rev1_com { get; set; }
        public string end_rev2_com { get; set; }
        public string end_final_achievement { get; set; }
        public string goal_type { get; set; }
    }
    public class SetGroup
    {
        //public string notif { get; set; }
        public string doer_id { get; set; }
        //public string result_fin { get; set; }
        public string date_created { get; set; }
        //public string emp_edit { get; set; }
        //public string rev1_edit { get; set; }
    }
    public class UpdateAck
    {
        public string group_id { get; set; }
        public string ack_lvl { get; set; }
        public string status { get; set; }
        public string creator_id { get; set; }
        public string reviewer1 { get; set; }
        public string reviewer2 { get; set; }
        public string date_req { get; set; }
        public string date_rev1 { get; set; }
        public string date_rev2 { get; set; }
        public string date_done { get; set; }
        public string escalated { get; set; }
        public string modified_by { get; set; }
    }
    public class UpdateGroupApprove
    {
        public string id { get; set; }
    }

    public class UpdateAckPlan
    {
        public string id { get; set; }
        public string date_done { get; set; }
    }
    public class UpdateRev1
    {
        public string id { get; set; }
        public string ack_lvl { get; set; }
        public string reviewer1 { get; set; }
        public string date_rev1 { get; set; }
        public string status { get; set; }
        public string modified_by { get; set; }
    }

    public class UpdatePlanRev1
    {
        public string id { get; set; }
        public string ack_lvl { get; set; }
        public string reviewer1 { get; set; }
        public string date_rev1 { get; set; }
        public string status { get; set; }
    }
    public class UpdateRev2
    {
        public string id { get; set; }
        public string ack_lvl { get; set; }
        public string reviewer2 { get; set; }
        public string date_rev2 { get; set; }
        public string status { get; set; }
        public string modified_by { get; set; }
    }
    public class UnlockFunc
    {
        public String groupID { get; set; }
        public String ackLvl { get; set; }
    }

    public class UnlockEmp
    {
        public String group_id { get; set; }
        public String ack_lvl { get; set; }
    }

    public class UpdatePlanRev2
    {
        public string id { get; set; }
        public string ack_lvl { get; set; }
        public string reviewer2 { get; set; }
        public string date_rev2 { get; set; }
        public string status { get; set; }
    }
    public class SelfReport
    {
        public string cID { get; set; }
        public string empID { get; set; }
    }
    public class UpdateComment
    {
        public string goal_id { get; set; }
        public string com_level { get; set; }
        public string str { get; set; }
        public string dnp { get; set; }
    }

    public class UpdateSettings
    {
        public string cID { get; set; }
        public string rev1_days { get; set; }
        public string rev2_days { get; set; }
        public string message { get; set; }
        public string message_days { get; set; }
        public string rev1_mode { get; set; }
        public string rev2_mode { get; set; }
        public string message_mode { get; set; }
    }
    public class UpdateDate
    {
        public string CN { get; set; }
        public string id { get; set; }
        public string date_start { get; set; }
        public string date_end { get; set; }
    }
    public class UpdateGoals
    {
        public string id { get; set; }
        public string goal_desc { get; set; }
        public string weight { get; set; }
        public string com_missed { get; set; }
        public string com_met { get; set; }
        public string com_exceeded { get; set; }
        public string com_dept { get; set; }
        public string start_rev1_com { get; set; }
        public string start_rev2_com { get; set; }
        public string mid_results { get; set; }
        public string mid_remaining_plan { get; set; }
        public string mid_rev1_com { get; set; }
        public string mid_rev2_com { get; set; }
        public string end_results { get; set; }
        public string end_init_achievement { get; set; }
        public string end_rev1_com { get; set; }
        public string end_rev2_com { get; set; }
        public string end_final_achievement { get; set; }
    }
    public class UpdateGoalPlan
    {
        public string id { get; set; }
        public string goal_name { get; set; }
        public string weight { get; set; }
        public string description { get; set; }
        public string com_missed { get; set; }
        public string com_met { get; set; }
        public string com_exceeded { get; set; }
        public string com_dept { get; set; }
        public string goal_type { get; set; }
    }
    public class SetExemption
    {
        public string empid { get; set; }
        public string empname { get; set; }
        public string goalsettings_from { get; set; }
        public string goalsettings_to { get; set; }
        public string midyear_from { get; set; }
        public string midyear_to { get; set; }
        public string yearend_from { get; set; }
        public string yearend_to { get; set; }
        public string company { get; set; }
        public string modified_by { get; set; }
    }

    public class InsertViewData // //@modifiedby varchar(20), @company varchar(20), @viewdate varchar(20)
    {
        public string company { get; set; }
        public string viewdate { get; set; }
        public string modifiedby { get; set; }
    }

    public class UpdateRating
    {
        public string CN { get; set; }
        public string id { get; set; }
        public string name { get; set; }
    }
    public class UpdateResult
    {
        public string CN { get; set; }
        public string id { get; set; }
        public string name { get; set; }
    }
    public class UpdateScore
    {
        public string CN { get; set; }
        public string id { get; set; }

        public string score_eval { get; set; }
        public string weight { get; set; }

    }

    public class ValidateUser
    {
        public string company { get; set; }
        public string empId { get; set; }
        public string password { get; set; }

    }

    public class ValidateUserId
    {
        public string CN { get; set; }
        public string ID { get; set; }

    }

    public class UpperHierarchy
    {
        public string Emp_ID { get; set; }
        public string cID { get; set; }

    }
    public class GetGoalPlan
    {
        public string ID { get; set; }

    }

    public class SetGoalGroup
    {
        public string group_id { get; set; }
        public string goal_name { get; set; }
        public string weight { get; set; }
        public string description { get; set; }
        public string com_missed { get; set; }
        public string com_met { get; set; }
        public string com_exceeded { get; set; }
        public string com_dept { get; set; }
        public string goal_type { get; set; }

    }
    public class DeleteGoal
    {
        public string group_id { get; set; }

    }

    public class AckDataResult
    {
        public string id { get; set; }
        public string group_id { get; set; }
        public string ack_lvl { get; set; }
        public string status { get; set; }
        public string creator_id { get; set; }
        public string reviewer1 { get; set; }
        public string reviewer2 { get; set; }
        public string date_req { get; set; }
        public string date_rev1 { get; set; }
        public string date_rev2 { get; set; }
        public string date_done { get; set; }
        public string escalated { get; set; }
        public string modified_by { get; set; }
    }

    public class AckPlanDataResultList
    {
        public string id { get; set; }
        public string group_id { get; set; }
        public string ack_lvl { get; set; }
        public string status { get; set; }
        public string creator_id { get; set; }
        public string reviewer1 { get; set; }
        public string reviewer2 { get; set; }
        public string date_req { get; set; }
        public string date_rev1 { get; set; }
        public string date_rev2 { get; set; }
        public string date_done { get; set; }
        public string escalated { get; set; }
    }

    public class GetAuditTrail
    {
        public List<GetAuditTrailList> result;
        public string myreturn;
    }
    public class GetAuditTrailPerfApp
    {
        public string cID { get; set; }
        public string empID { get; set; }
        public string auditType { get; set; }
    }
    public class GetAuditTrailList
    {
        public string id { get; set; }
        public string name { get; set; }
        public string session { get; set; }
        public string date_req { get; set; }
        public string ack1 { get; set; }
        public string date_ack1 { get; set; }
        public string ack2 { get; set; }
        public string date_ack2 { get; set; }
        public string date_of_change { get; set; }
        public string what_change { get; set; }
        public string change_from { get; set; }
        public string change_to { get; set; }

    }

    //1/28/2020 REN
    public class getFilterExemption
    {
        public List<FilterExemptionLists> result;
        public string myreturn;
    }
    public class ExemptionParams
    {
        public string company { get; set; }
        public string empid { get; set; }

    }
    public class GetExemptionById
    {
        public string empid { get; set; }
        public string company { get; set; }
    }

    public class SelectViewDate
    {
        public string company { get; set; }
    }

    public class getUserExemption
    {
        public List<UserExemptionList> result;
        public string myreturn;
    }
    public class UserExemptionList
    {
        public string EmpID { get; set; }
        public string EmpName { get; set; }
    }
    public class UserExemptionParams
    {
        public string company { get; set; }
    }

    //01/29/2020


    //1/29/2020
    public class SearchAuditList
    {
        public string id { get; set; }
        public string name { get; set; }
        public string session { get; set; }
        public string date_req { get; set; }
        public string ack1 { get; set; }
        public string date_ack1 { get; set; }
        public string ack2 { get; set; }
        public string date_ack2 { get; set; }
        public string date_of_change { get; set; }
        public string what_change { get; set; }
        public string change_from { get; set; }
        public string change_to { get; set; }

    }

    public class AuditParams
    {
        public string cID { get; set; }
        public string empID { get; set; }
        public string user { get; set; }
        public string auditType { get; set; }
        public string dateFrom { get; set; }
        public string dateTo { get; set; }
        public String[] empid { get; set; }
        public String[] session { get; set; }
    }

    //2/7/2020
    public class ExempRev1
    {
        public string ERev1 { get; set; }

    }

    public class ExempRev2
    {
        public string ERev2 { get; set; }

    }

    public class ClassExemRev1Count
    {
        //FOR THE RESULT
        public List<ExempRev1> returnERev1;
        public string myreturn;
    }
    public class ClassExemRev2Count
    {
        //FOR THE RESULT
        public List<ExempRev2> returnERev2;
        public string myreturn;
    }
    public class insertPerfView
    {
        public string groupid { get; set; }
        public string empID { get; set; }
        public string session { get; set; }
        public string what_change { get; set; }
        public string change_from { get; set; }
        public string change_to { get; set; }
        public string cID { get; set; }
    }


}

