﻿using Org.BouncyCastle.Bcpg;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace illimitadoWepAPI.Models.PayrollOutsourcingModel
{
    public class TimeAttendanceAlt
    {
        public string db { get; set; }
        public string ClientCode { get; set; }
        public DateTime StartDate { get; set; }
        public string PayoutType { get; set; }
        public DateTime EndDate { get; set; }
        public string[] Emp_ID { get; set; }
        public string[] First_Name { get; set; }
        public string[] Last_Name { get; set; }
        public string[] Reg_Days_Work { get; set; }
        public string[] Tardy_Hrs { get; set; }
        public string[] Undertime_Hrs { get; set; }
        public string[] Absent_Days { get; set; }
        public string[] LWOP_Days { get; set; }
        public string[] VL_Days { get; set; }
        public string[] SL_Days { get; set; }
        public string[] Other_Leaves_Days { get; set; }
        public string[] Night_Diff { get; set; }
        public string[] Regular_OT { get; set; }
        public string[] Regular_OT_NP { get; set; }
        public string[] Legal_Hol_OT { get; set; }
        public string[] Legal_Hol_OT_NP { get; set; }
        public string[] Legal_Hol_OT_EHrs { get; set; }
        public string[] Legal_Hol_OT_EHrs_NP { get; set; }
        public string[] Legal_Hol_RD { get; set; }
        public string[] Legal_Hol_RD_NP { get; set; }
        public string[] Legal_Hol_RD_EHrs { get; set; }
        public string[] Legal_Hol_RD_EHrs_NP { get; set; }
        public string[] Special_Hol_OT { get; set; }
        public string[] Special_Hol_OT_NP { get; set; }
        public string[] Special_Hol_OT_EHrs { get; set; }
        public string[] Special_Hol_OT_Ehrs_NP { get; set; }
        public string[] Special_Hol_RD { get; set; }
        public string[] Special_Hol_RD_NP { get; set; }
        public string[] Special_Hol_RD_EHrs { get; set; }
        public string[] Special_Hol_RD_EHrs_NP { get; set; }
        public string[] RestDay_OT { get; set; }
        public string[] RestDay_OT_NP { get; set; }
        public string[] RestDay_OT_EHrs { get; set; }
        public string[] RestDay_OT_EHrs_NP { get; set; }
        public string[] Double_Hol_OT { get; set; }
        public string[] Double_Hol_OT_NP { get; set; }
        public string[] Double_Hol_OT_EHrs { get; set; }
        public string[] Double_Hol_OT_EHrs_NP { get; set; }
        public string[] Double_Hol_RD { get; set; }
        public string[] Double_Hol_RD_NP { get; set; }
        public string[] Double_Hol_RD_EHrs { get; set; }
        public string[] Double_Hol_RD_EHrs_NP { get; set; }
        public string[] Remarks { get; set; }
        public string LoginID { get; set; }
        public string TenantID { get; set; }
    }
    public class IncomeDeductionAlt
    {
        public string db { get; set; }
        public string ClientCode { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string IncomeOrDeduction { get; set; }
        public string[] Emp_ID { get; set; }
        public string[] Date { get; set; }
        public string[] Code { get; set; }
        public string[] Amount { get; set; }
        public string[] Recur_Start_Date { get; set; }
        public string[] Recur_End_Date { get; set; }
        public string[] Frequency { get; set; }
        public string[] Remarks { get; set; }
        public string LoginID { get; set; }
        public string PayoutType { get; set; }
        public string filename { get; set; }
    }

    public class settingPublish
    {
        public string payOutDate { get; set; }
        public string[] empID { get; set; }
        public string db { get; set; }
        public string setting { get; set; }
        public string tenantID { get; set; }
        public string publishedBy { get; set; }
    }
    public class taxImport
    {
        public string db { get; set; }
        public string ClientCode { get; set; }
        public string[] Emp_ID { get; set; }
        public string[] PayoutDate { get; set; }
        public string[] Amount { get; set; }
        public string LoginID { get; set; }
    }
    public class taxDelete
    {
        public string db { get; set; }
        public string ClientCode { get; set; }

        public string PayOutDate { get; set; }
    }
    public class LoanRegisterAlt
    {
        public string db { get; set; }
        public string ClientCode { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string PayoutType { get; set; }
        public string option { get; set; }
        public string time { get; set; }
        public string tenantId { get; set; }
        public string[] Emp_ID { get; set; }
        public string[] Loan_Code { get; set; }
        public string[] Principal_Amount { get; set; }
        public string[] Total_Payments { get; set; }
        public string[] Loan_Starting_Deductions_Date { get; set; }
        public string[] Total_Previous_Payment { get; set; }
        public string[] Monthly_Loan_Amort_Amount { get; set; }
        public string[] Loan_Amort_Frequency { get; set; }
        public string[] Loan_Maturity_Date { get; set; }
        public string[] Date_Loan_Granted { get; set; }
        public string[] Account_Number { get; set; }
        public string[] Remarks { get; set; }
        public string[] Loan_Amount { get; set; }
        public string LoginID { get; set; }
        public string filename { get; set; }
    }
    public class SSSMaternity
    {
        public string db { get; set; }
        public string ClientCode { get; set; }
        public string[] Emp_ID { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string[] Basic_Salary { get; set; }
        public string[] Laundry_Allowance { get; set; }
        public string[] Meal_Allowance { get; set; }
        public string[] Rice_Subsidy { get; set; }
        public string[] Communication { get; set; }
        public string[] Transpo_Allowance { get; set; }
        public string[] Total_Pay { get; set; }
        public string[] SSS_EE_Cont { get; set; }
        public string[] PHIC_EE_Cont { get; set; }
        public string[] HDMF_EE_Cont { get; set; }
        public string[] SSS_Maternity_Benefit { get; set; }
        public string[] Salary_Differential { get; set; }
        public string[] Taxable_Income { get; set; }
        public string[] WithHolding_Tax { get; set; }
        public string[] Net_Pay { get; set; }
        public string LoginID { get; set; }
    }
    public class ITR_Model
    {
        public string ClientCode { get; set; }
        public string[] Emp_ID { get; set; }
        public string[] Employer { get; set; }
        public string[] Employer_Date { get; set; }
        public string[] TIN { get; set; }
        public string[] Employer_Address { get; set; }
        public string[] Withheld_To_Prev_Employer { get; set; }
        public string[] Prev_SSS_HDMF_PH { get; set; }
        public string[] Taxed_Salaries { get; set; }
        public string[] Taxed_Bonus { get; set; }
        public string[] Taxable_Income { get; set; }
        public string[] Gross_To_Employer { get; set; }
        public string[] Gross_Plus_SSS_HDMF_PH_To_Employer { get; set; }
        public string[] NT_Salaries { get; set; }
        public string[] NT_Bonus { get; set; }
        public string[] Deminimis { get; set; }
        public string[] Total_NT_Income { get; set; }
        public string LoginID { get; set; }
    }
    public class ITR_Modelv2
    {
        public string db { get; set; }
        public string LoginID { get; set; }
        public string ClientCode { get; set; }
        public string IfLatest { get; set; }
        public string[] EmpID { get; set; }
        public string[] EmpName { get; set; }
        public string[] ForYYYY { get; set; }
        public string[] ForMMFrom { get; set; }
        public string[] ForMMTo { get; set; }
        public string[] TIN { get; set; }
        public string[] EmployerName { get; set; }
        public string[] RegAddress { get; set; }
        public string[] ZipCode { get; set; }
        public string[] EmployerType { get; set; }
        public string[] PrevERTIN { get; set; }
        public string[] PrevERName { get; set; }
        public string[] PrevERAddress { get; set; }
        public string[] PrevERZip { get; set; }
        public string[] GrossIncomePresentEmp { get; set; }
        public string[] NTXIncomePresentEmp { get; set; }
        public string[] TXIncomePresentEmp { get; set; }
        public string[] TXIncomePrevEmp { get; set; }
        public string[] TXIncome { get; set; }
        public string[] TaxDue { get; set; }
        public string[] WithTaxPresentEmp { get; set; }
        public string[] WithTaxPrevEmp { get; set; }
        public string[] WithTaxAdj { get; set; }
        public string[] BasicSalaryMWE { get; set; }
        public string[] HolidayPayMWE { get; set; }
        public string[] OvertimePayMWE { get; set; }
        public string[] NSDPayMWE { get; set; }
        public string[] HazardPayMWE { get; set; }
        public string[] _13thPayotherBen { get; set; }
        public string[] DeMinimisBen { get; set; }
        public string[] GovContribandOther { get; set; }
        public string[] SalandOtherCompensation { get; set; }
        public string[] TotalNTXIncome { get; set; }
        public string[] BasicSalary { get; set; }
        public string[] Representation { get; set; }
        public string[] Transportation { get; set; }
        public string[] COLA { get; set; }
        public string[] FixedHousingAllow { get; set; }
        public string[] OthersLine42a { get; set; }
        public string[] OthersLine42b { get; set; }
        public string[] Commission { get; set; }
        public string[] ProfitSharing { get; set; }
        public string[] Fees { get; set; }
        public string[] TX13thBen { get; set; }
        public string[] HazardPay { get; set; }
        public string[] OTPay { get; set; }
        public string[] Others49 { get; set; }
        public string[] TotalTXIncome { get; set; }
    }
    public class Main_Model
    {
        public string db { get; set; }
        public string ClientCode { get; set; }
        public string[] Emp_ID { get; set; }
        public string[] Last_Name { get; set; }
        public string[] First_Name { get; set; }
        public string[] Middle_Name { get; set; }
        public string[] Days_To_Credit { get; set; }
        public string[] IsRehired { get; set; }

        public string[] Gender { get; set; }
        public string[] Email { get; set; }
        public string[] Grade_Level { get; set; }
        public string[] Cost_Center { get; set; }
        public string[] Current_Position { get; set; }
        public string[] Current_Division { get; set; }
        public string[] Current_Department { get; set; }
        public string[] DIrect_Indirect_Labor { get; set; }
        public string[] Peza_Classification { get; set; }
        public string[] Birth_Date { get; set; }
        public string[] Date_Employed { get; set; }
        public string[] IsOT_Allowed { get; set; }
        public string[] Tax_Status { get; set; }
        public string[] Monthly_Basic_Salary { get; set; }
        public string[] PayoutScheme { get; set; }
        public string[] Additional_HDMF { get; set; }
        public string[] Payment_Type { get; set; }
        public string[] BPI_Account_Number { get; set; }
        public string[] Pagibig_ID { get; set; }
        public string[] SSS_ID { get; set; }
        public string[] TIN_ID { get; set; }
        public string[] Philhealth_ID { get; set; }
        public string[] Employment_Status { get; set; }
        public string[] Employment_Type { get; set; }
        public string[] Address_1 { get; set; }
        public string[] Address_2 { get; set; }
        public string[] Contact_Number { get; set; }
        public string[] Annual_RT { get; set; }
        public string[] Currency { get; set; }
        public string[] GL_Operating_Unit { get; set; }
        public string[] GL_Location { get; set; }
        public string[] GL_Business_Unit { get; set; }
        public string[] GL_DeptID { get; set; }
        public string[] GL_Project { get; set; }

        public string[] isSSS { get; set; }
        public string[] isPHIC { get; set; }
        public string[] isHDMF { get; set; }
        public string[] HasExited { get; set; }
        public string[] RateType { get; set; }
        public string[] MWE { get; set; }
        public string[] SalaryType { get; set; }

        public string LoginID { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string PayoutType { get; set; }
    }
    public class NJ_API
    {
        public string db { get; set; }
        public string ClientCode { get; set; }
        public string Password { get; set; }
        public string Token { get; set; }
        public string[] Emp_ID { get; set; }
        public string[] Last_Name { get; set; }
        public string[] First_Name { get; set; }
        public string[] Middle_Name { get; set; }
        public string[] Days_To_Credit { get; set; }
        public string[] IsRehired { get; set; }

        public string[] Gender { get; set; }
        public string[] Email { get; set; }
        public string[] Grade_Level { get; set; }
        public string[] Cost_Center { get; set; }
        public string[] Current_Position { get; set; }
        public string[] Current_Division { get; set; }
        public string[] Current_Department { get; set; }
        public string[] DIrect_Indirect_Labor { get; set; }
        public string[] Peza_Classification { get; set; }
        public string[] Birth_Date { get; set; }
        public string[] Date_Employed { get; set; }
        public string[] IsOT_Allowed { get; set; }
        public string[] Tax_Status { get; set; }
        public string[] Monthly_Basic_Salary { get; set; }
        public string[] PayoutScheme { get; set; }
        public string[] Additional_HDMF { get; set; }
        public string[] Payment_Type { get; set; }
        public string[] BPI_Account_Number { get; set; }
        public string[] Pagibig_ID { get; set; }
        public string[] SSS_ID { get; set; }
        public string[] TIN_ID { get; set; }
        public string[] Philhealth_ID { get; set; }
        public string[] Employment_Status { get; set; }
        public string[] Employment_Type { get; set; }
        public string[] Address_1 { get; set; }
        public string[] Address_2 { get; set; }
        public string[] Contact_Number { get; set; }
        public string[] Annual_RT { get; set; }
        public string[] Currency { get; set; }
        public string[] GL_Operating_Unit { get; set; }
        public string[] GL_Location { get; set; }
        public string[] GL_Business_Unit { get; set; }
        public string[] GL_DeptID { get; set; }
        public string[] GL_Project { get; set; }

        public string[] isSSS { get; set; }
        public string[] isPHIC { get; set; }
        public string[] isHDMF { get; set; }
        public string[] HasExited { get; set; }
        public string[] RateType { get; set; }
        public string[] MWE { get; set; }
        public string LoginID { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string PayoutType { get; set; }
    }
    public class DataChanges_Model
    {

        public string db { get; set; }
        public string ClientCode { get; set; }
        public string[] Emp_ID { get; set; }
        public string[] ColumnType { get; set; }
        public string[] NewValue { get; set; }
        public string[] OldValue { get; set; }
        public string[] Remarks { get; set; }
        public string LoginID { get; set; }
    }
    public class CurrencyModel
    {
        public string db { get; set; }
        public string TenantID { get; set; }
    }
    public class CurrencyList
    {
        public string Currency { get; set; }

    }
    public class Leavers_Model
    {
        public string db { get; set; }
        public string id { get; set; }
        public string ClientCode { get; set; }
        public string[] Emp_ID { get; set; }
        public string[] Last_Employment_Date { get; set; }
        public string LoginID { get; set; }
    }
    public class PayrollRegister_Model
    {
        public string Company { get; set; }
        public string PayoutDate { get; set; }
        public string PayoutSchemesID { get; set; }
        public string PaymentTermsID { get; set; }
        public string CutoffStartDate { get; set; }
        public string CutoffEndDate { get; set; }
        public string IsAnnualized { get; set; }
        public string ProcessedBy { get; set; }
    }
    public class CalculateParam
    {
        public string db { get; set; }
        public string CutoffStart { get; set; }
        public string CutoffEnd { get; set; }
        public string EmpStatus { get; set; }
        public string TenantID { get; set; }
        public string currStatus { get; set; }
        public string[] currency { get; set; }
        public string[] rate { get; set; }
    }
    public class CalculateSpecialParam
    {
        public string db { get; set; }
        public string CutoffStart { get; set; }
        public string CutoffEnd { get; set; }
        public string EmpStatus { get; set; }
        public string TenantID { get; set; }
        public string[] EmpID { get; set; }
    }
    public class efile
    {
        public string db { get; set; }
        public string TenantID { get; set; }
        public string PayoutDate { get; set; }
        public string EmpStatus { get; set; }
        public string CutoffStart { get; set; }
        public string CutoffEnd { get; set; }
        public string year { get; set; }
        public string month { get; set; }
        public string date { get; set; }
        public string[] empid { get; set; }
        public string[] currency { get; set; }
        public string[] rate { get; set; }
        public string cCode { get; set; }
    }
    public class PO_NewJoiners
    {
        public List<NewJoinersReturn> DU;
        public string myreturn { get; set; }
    }
    public class datachange
    {
        public string Token { get; set; }
        public string db { get; set; }
        public string ClientCode { get; set; }
        public string Password { get; set; }
        public string[] Emp_ID { get; set; }
        public string[] ColumnType { get; set; }
        public string[] NewValue { get; set; }
        public string[] OldValue { get; set; }
        public string[] Remarks { get; set; }
        public string LoginID { get; set; }
    }
    public class API_ILM_NJ
    {
        public List<API_ILM_NJ_RETURN> DU;
        public string myreturn { get; set; }
    }

    public class API_ILM_NJ_RETURN
    {
        public string EmpID { get; set; }
        public string Email { get; set; }
        public string GPass { get; set; }
    }
    public class Employees
    {
        public string EmpID { get; set; }
        public string EmpName { get; set; }
    }

    public class FileList
    {
        public string id { get; set; }
        public string date { get; set; }
        public string file { get; set; }
    }

    public class EmployeesParam
    {
        public string db { get; set; }
        public string ClientCode { get; set; }
        public string TenantID { get; set; }
        public string Option { get; set; }
    }
    public class DuplicateUsers
    {
        public string RowID { get; set; }
        public string EmployeeID { get; set; }
        public string Conflict { get; set; }
    }
    public class NewJoinersReturn
    {
        public string EmpID { get; set; }
        public string Email { get; set; }
        public string GPass { get; set; }
    }
    public class LogUserParam
    {
        public string db { get; set; }
        public string EmpID { get; set; }
        public string TenantID { get; set; }
    }
    public class LogUser
    {
        public string LogName { get; set; }
    }
    public class IncomeCodeParam
    {
        public string db { get; set; }
        public string ClientCode { get; set; }
        public string CodeType { get; set; }
    }
    public class DeductionCodeParam
    {
        public string ClientCode { get; set; }
    }
    public class InCode
    {
        public string IncomeCode { get; set; }
    }
    public class IncomeCode
    {
        public string Code { get; set; }
    }
    public class DedCode
    {
        public string DeductionCode { get; set; }
    }
    public class pdf
    {
        public string TenantID { get; set; }
        public string empID { get; set; }
        public string type { get; set; }
        public string Year { get; set; }
        public string db { get; set; }
    }
    public class Update2316
    {
        public string TenantID { get; set; }
        public string EmpID { get; set; }
        public string Year { get; set; }
        public string db { get; set; }
    }
    public class NTXReport
    {
        public string db { get; set; }
        public string TenantCode { get; set; }
        public string Month { get; set; }
        public string Year { get; set; }
    }
    public class PEZAReport
    {
        public string db { get; set; }
        public string Month { get; set; }
        public string Year { get; set; }
        public string TenantCode { get; set; }
    }
    public class TaxPerMonthReport
    {
        public string db { get; set; }
        public string Year { get; set; }
        public string TenantCode { get; set; }
    }
    public class PayslipParams
    {
        public string empID { get; set; }
        public string payoutDate { get; set; }
        public string tenantID { get; set; }
        public string PayslipType { get; set; }
    }
    public class PayslipParamsBulk
    {
        public string db { get; set; }
        public string[] empIDs { get; set; }
        public string tenantID { get; set; }
        public string payoutDate { get; set; }
        public bool sendEmail { get; set; }
        public bool hasPassword { get; set; }
        public string PayslipType { get; set; }
    }
    public class PayoutDatesParam
    {
        public string db { get; set; }
        public string TenantID { get; set; }
        public string Year { get; set; }
        public string PayslipType { get; set; }
        public string PayOutType { get; set; }
        public string loginID { get; set; }
        public string rateName { get; set; }
        public string rateTypeID { get; set; }
        public string[] NewValue { get; set; }
        public string[] OvertimeID { get; set; }

    }
    public class PayoutDatesParamV2
    {
        public string db { get; set; }
        public string TenantID { get; set; }
        public string Year { get; set; }
        public string PayslipType { get; set; }
        public string EmpID { get; set; }
    }
    public class RateTypesList
    {
        public string id { get; set; }
        public string rateName { get; set; }

    }
    public class listGetRates
    {
        public string OvertimeID { get; set; }
        public string OTDesc { get; set; }
        public string OTRate { get; set; }

    }
    public class PayoutDates
    {
        public string PayoutDate { get; set; }
    }
    public class EPRSReport
    {
        public string db { get; set; }
        public string Month { get; set; }
        public string Year { get; set; }
        public string TenantCode { get; set; }
    }
    public class EmpInfoReport
    {
        public string db { get; set; }
        public string TenantCode { get; set; }
        public string Status { get; set; }
    }
    public class PayslipList
    {
        public string URL { get; set; }
    }
    public class PO_Login
    {
        public string db { get; set; }
        public string TenantCode { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
    }
    public class ReturnLogin
    {
        public string Status { get; set; }
        public string Token { get; set; }
        public string TenantID { get; set; }
    }

    public class PO_ChangePass
    {
        public string TenantCode { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string NewPassword { get; set; }
        public string empid { get; set; }
        public string tenantcode { get; set; }
        public string db { get; set; }
        public string LoanCode { get; set; }
        public string payoutdate { get; set; }
    }
    public class ReturnLoanCode
    {
        public string loanName { get; set; }
        public string loanCode { get; set; }
    }
    public class ReturnPayoutdate
    {
        public string payoutdate { get; set; }

    }
    public class ReturnProfileLoans
    {
        public string payoutdate { get; set; }
        public string loanName { get; set; }
        public string loanDate { get; set; }
        public string loanAmount { get; set; }
        public string amortizationAmount { get; set; }
        public string loanBalance { get; set; }


    }
    public class ReturnChangePass
    {
        public string Status { get; set; }
    }
    public class Multiple_CostCenters
    {
        public string db { get; set; }
        public string ClientCode { get; set; }
        public string LoginID { get; set; }
        public string[] EmpID { get; set; }
        public string[] CostCode { get; set; }
        public string[] AllocationPerc { get; set; }
        public string[] Action { get; set; }
    }
    public class EmployeeDetails
    {
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string myreturn { get; set; }
    }
    public class EmployeeDetailsParam
    {
        public string TenantID { get; set; }
        public string EmpID { get; set; }
    }
    public class AccessPayslip
    {
        public string TenantID { get; set; }
        public string EmpID { get; set; }
        public string PayslipPass { get; set; }
    }
    public class SumReportParams
    {
        public string tenantID { get; set; }
        public string startDate { get; set; }
        public string endDate { get; set; }
    }
    public class DTCAbsencessParams
    {
        public string db { get; set; }
        public string LoginID { get; set; }
        public string ClientCode { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string PayoutType { get; set; }
    }
    public class Benefits_Filter_ED
    {
        public string EmpID { get; set; }
        public string EmpName { get; set; }
        public string CreatedON { get; set; }
        public string EDCode { get; set; }
        public string Amount { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string Frequency { get; set; }
        public string Remarks { get; set; }
        public string ClosedOn { get; set; }
    }
    public class Benefits_Filter_ED_Details
    {
        public string EmpID { get; set; }
        public string EmpName { get; set; }
        public string Code { get; set; }
        public string Amount { get; set; }
        public string PaymentTerms { get; set; }
        public string PayoutDate { get; set; }
    }

    public class Benefits_FilterLoans
    {
        public string EmpID { get; set; }
        public string EmpName { get; set; }
        public string CreatedON { get; set; }
        public string LoanID { get; set; }
        public string LoanNameCode { get; set; }
        public string PrincipalAmount { get; set; }
        public string TotalPayments { get; set; }
        public string WithInterest { get; set; }
        public string StartDate { get; set; }
        public string AmortizationAmount { get; set; }
        public string Frequency { get; set; }
        public string EndDate { get; set; }
        public string LoanDate { get; set; }
        public string PromissoryNoteNum { get; set; }
        public string Remarks { get; set; }
        public string LoanAmount { get; set; }
        public string ClosedOn { get; set; }
    }

    public class Benefits_FilterLoans_Details
    {
        public string EmpId { get; set; }
        public string EmpName { get; set; }
        public string AmortizationAmount { get; set; }
        public string LoanNameCode { get; set; }
        public string DateDeducted { get; set; }
        public string PayOutDate { get; set; }
        public string RemainingBalance { get; set; }
        public string Frequency { get; set; }
        public string LoanID { get; set; }
    }
    public class benefit
    {
        public string db { get; set; }
        public string id { get; set; }
        public string TenantID { get; set; }
        public string EDStatus { get; set; }
        public string edlOption { get; set; }
        public string uploadDate { get; set; }
        public string deductionsStatus { get; set; }
        public string[] EmpID { get; set; }
        public string ID { get; set; }
        public string EDType { get; set; }
        public string Date { get; set; }
        public string Code { get; set; }
        public string Amount { get; set; }
        public string RecurStart { get; set; }
        public string RecurEnd { get; set; }
        public string Frequency { get; set; }
        public string Remarks { get; set; }
        public string updateDate { get; set; }
        public string updateCode { get; set; }
        public string updateAmount { get; set; }
        public string updateRecurStart { get; set; }
        public string updateRecurEnd { get; set; }
        public string updateFrequency { get; set; }
        public string updateRemarks { get; set; }
        public string ClosedOn { get; set; }
        public string updateClosedOn { get; set; }
        public string payoutdate { get; set; }
        public string PaymentTerms { get; set; }
        public string updatePaymentTerms { get; set; }
        public string updatePayoutDate { get; set; }
    }
    public class HDMFLoans
    {
        public string TenantCode { get; set; }
        public string Month { get; set; }
        public string Year { get; set; }
    }
    public class resetFinalPay
    {
        public string db { get; set; }
        public string PayoutType { get; set; }
        public string TenantID { get; set; }
        public string CutoffStart { get; set; }
        public string CutoffEnd { get; set; }
        public string[] UserId { get; set; }
    }
    public class PO_AccountCreation
    {
        public string db { get; set; }
        public string userid { get; set; }
        public string password { get; set; }
        public string tenantid { get; set; }
    }
    public class AnnualizationReport
    {
        public string db { get; set; }
        public string TenantCode { get; set; }
        public string Year { get; set; }
    }
    public class Report_List
    {
        public string ReportID { get; set; }
        public string ReportName { get; set; }
        public string type { get; set; }

    }


    public class report
    {
        public string db { get; set; }
        public string tenantID { get; set; }

    }
    public class TenantIDParams
    {
        public string db { get; set; }
        public string ClientName { get; set; }
        public string EmpID { get; set; }

    }

    public class ReturnAC
    {
        public string Status { get; set; }
    }
    public class resetpayroll
    {
        public string db { get; set; }
        public string TenantID { get; set; }
        public string CutOffStart { get; set; }
        public string CutOffEnd { get; set; }
        public string PayoutType { get; set; }
    }
    public class benefitloans
    {
        public string db { get; set; }
        public string TenantID { get; set; }
        public string EmpID { get; set; }
        public string LoanCode { get; set; }
        public string LoanPrincipalAmount { get; set; }
        public string TotalPayments { get; set; }
        public string TotalBalance { get; set; }
        public string LoanStartDeductionPaydate { get; set; }
        public string MonthlyLoanAmortAmount { get; set; }
        public string LoanAmortFrequency { get; set; }
        public string LoanMaturityDate { get; set; }
        public string DateLoanGranted { get; set; }
        public string PromissoryNote { get; set; }
        public string Remarks { get; set; }
        public string LoanAmount { get; set; }
        public string ClosedOnDate { get; set; }
        public string updateLoanCode { get; set; }
        public string updateLoanPrincipalAmount { get; set; }
        public string updateTotalPayments { get; set; }
        public string updateTotalBalance { get; set; }
        public string updateLoanStartDeductionPaydate { get; set; }
        public string updateMonthlyLoanAmortAmount { get; set; }
        public string updateLoanAmortFrequency { get; set; }
        public string updateLoanMaturityDate { get; set; }
        public string updateDateLoanGranted { get; set; }
        public string updatePromissoryNote { get; set; }
        public string updateRemarks { get; set; }
        public string updateLoanAmount { get; set; }
        public string updateClosedOnDate { get; set; }
        public string updatePayoutDate { get; set; }
        public string updateRemainingBalance { get; set; }
        public string updateDateDeducted { get; set; }
        public string LoanID { get; set; }
    }
    public class EmployeeInfo_Update
    {
        public string db { get; set; }
        public string TenantCode { get; set; }
        public string EmpID { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string Rehired { get; set; }
        public string Gender { get; set; }
        public string Email { get; set; }
        public string GradeLevel { get; set; }
        public string CostCenter { get; set; }
        public string JobTitle { get; set; }
        public string CurrentDivision { get; set; }
        public string CurrentDepartment { get; set; }
        public string LaborType { get; set; }
        public string BirthDate { get; set; }
        public string DateEmployed { get; set; }
        public string AllowedOT { get; set; }
        public string TaxStatus { get; set; }
        public string MonthlyRate { get; set; }
        public string AdditionalHDMF { get; set; }
        public string PaymentType { get; set; }
        public string AccountNumber { get; set; }
        public string PagibigNum { get; set; }
        public string SSSNum { get; set; }
        public string TINnum { get; set; }
        public string PhilhealthNum { get; set; }
        public string EmploymentStatus { get; set; }
        public string PayrollStatus { get; set; }
        public string EmployeeType { get; set; }
        public string PermanentBlock { get; set; }
        public string PermamnetAddress { get; set; }
        public string HomePhone { get; set; }
        public string AnnualRt { get; set; }
        public string Currency { get; set; }
        public string Dateterminated { get; set; }
        public string RateType { get; set; }
        public string MWE { get; set; }
    }

    public class EmployeeInfo_Filter_List
    {
        public string EmpID { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string Rehired { get; set; }
        public string Gender { get; set; }
        public string Email { get; set; }
        public string GradeLevel { get; set; }
        public string CostCenter { get; set; }
        public string JobTitle { get; set; }
        public string CurrentDivision { get; set; }
        public string CurrentDepartment { get; set; }
        public string LaborType { get; set; }
        public string BirthDate { get; set; }
        public string DateEmployed { get; set; }
        public string AllowedOT { get; set; }
        public string TaxStatus { get; set; }
        public string MonthlyRate { get; set; }
        public string AdditionalHDMF { get; set; }
        public string PaymentType { get; set; }
        public string AccountNumber { get; set; }
        public string PagibigNum { get; set; }
        public string SSSNum { get; set; }
        public string TINnum { get; set; }
        public string PhilhealthNum { get; set; }
        public string EmploymentStatus { get; set; }
        public string PayrollStatus { get; set; }
        public string EmployeeType { get; set; }
        public string PermanentBlock { get; set; }
        public string PermamnetAddress { get; set; }
        public string HomePhone { get; set; }
        public string AnnualRt { get; set; }
        public string Currency { get; set; }
        public string Dateterminated { get; set; }
        public string RateType { get; set; }
        public string MWE { get; set; }
    }
    public class empinfo
    {
        public string db { get; set; }
        public string id { get; set; }
        public string TenantCode { get; set; }
        public string[] EmpID { get; set; }
    }
    public class delete_Details
    {
        public string db { get; set; }
        public string cutOffStart { get; set; }
        public string cutOffEnd { get; set; }
        public string tenantID { get; set; }
        public string PayOutType { get; set; }
        public string option { get; set; }

    }
    public class generatesss
    {
        public string cutoffstart { get; set; }
        public string cutoffend { get; set; }
        public string payouttype { get; set; }
        public string[] empid { get; set; }
        public string days { get; set; }
        public string tenantid { get; set; }
        public string db { get; set; }
    }
    public class reset13th
    {
        public string db { get; set; }
        public string PayoutDate { get; set; }
        public string TenantID { get; set; }
        public string PayoutType { get; set; }
        public string[] UserId { get; set; }
    }
    public class POLoansReport
    {
        public string db { get; set; }
        public string TenantCode { get; set; }
        public string Month { get; set; }
        public string Year { get; set; }
    }
    public class compute13th
    {
        public string db { get; set; }
        public string AsOfDate { get; set; }
        public string TenantID { get; set; }
        public string PayoutDate { get; set; }
        public string HiredDate { get; set; }
        public string[] UserId { get; set; }
    }
    public class CalculateFinalpayParam
    {
        public string db { get; set; }
        public string LoginID { get; set; }
        public string CutoffStartDate { get; set; }
        public string CutOffEndDate { get; set; }
        public string Year { get; set; }
        public string TenantID { get; set; }
        public string[] EmpID { get; set; }
    }

    //ADDED FOR CLIENT SETTINGS
    public class csinfo
    {
        public string TenantId { get; set; }
        public string db { get; set; }

    }

    public class getClientInfoList
    {
        public string TenantCode { get; set; }
        public string CompanyName { get; set; }
        public string CompanyAdd1 { get; set; }
        public string CompanyAdd2 { get; set; }
        public string SSSID { get; set; }
        public string PHICID { get; set; }
        public string HDMFID { get; set; }
        public string TIN { get; set; }
        public string isSSS { get; set; }
        public string isPHIC { get; set; }
        public string isHDMF { get; set; }
        public string isTIN { get; set; }
        public string Start1stCutoff { get; set; }
        public string End1stCutoff { get; set; }
        public string Payday1st { get; set; }
        public string Start2ndCutoff { get; set; }
        public string End2ndCutoff { get; set; }
        public string Payday2nd { get; set; }

        public string BankName { get; set; }
        public string BankAccountNumber { get; set; }
        public string AnnualWorkingDays { get; set; }
        public string isPayOutDate { get; set; }
    }
    public class getPaycodeLoansList
    {
        public string LoanNameID { get; set; }
        public string LoanCode { get; set; }
        public string LoanName { get; set; }
        public string LoanType { get; set; }
    }

    public class PCLoans
    {
        public string db { get; set; }
        public string TenantID { get; set; }
        public string LoanCode { get; set; }
        public string LoanName { get; set; }
        public string LoanType { get; set; }
    }
    public class cspcloans
    {
        public string TenantId { get; set; }
        public string db { get; set; }

    }
    public class combinedReports
    {
        public string TenantId { get; set; }
        public string db { get; set; }

    }

    public class UpdateInfo
    {
        public string db { get; set; }
        public string TenantID { get; set; }
        public string TenantCode { get; set; }
        public string TenantName { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string SSSID { get; set; }
        public string PHICID { get; set; }
        public string HDMFID { get; set; }
        public string TINID { get; set; }
        public string isSSS { get; set; }
        public string isPHIC { get; set; }
        public string isHDMF { get; set; }
        public string isTIN { get; set; }
        public string Start1stCutoff { get; set; }
        public string End1stCutoff { get; set; }
        public string Payday1st { get; set; }
        public string Start2ndCutoff { get; set; }
        public string End2ndCutoff { get; set; }
        public string Payday2nd { get; set; }


        public string BankName { get; set; }
        public string BankAccountNumber { get; set; }
        public string AnnualWorkingDays { get; set; }
        public string isPayOutDate { get; set; }
    }

    public class UpdatePCLoans
    {
        public string db { get; set; }
        public string TenantID { get; set; }
        public string LoanID { get; set; }
        public string LoanCode { get; set; }
        public string LoanName { get; set; }
        public string LoanType { get; set; }
        public string IsDelete { get; set; }
        public string LogUser { get; set; }
    }

    public class addPCLoans
    {
        public string db { get; set; }
        public string TenantID { get; set; }
        public string LoanCode { get; set; }
        public string LoanName { get; set; }
        public string LoanType { get; set; }
        public string LogUser { get; set; }
    }
    public class UpdeleteEarningsCode
    {
        public string db { get; set; }
        public string TenantID { get; set; }
        public string LogUser { get; set; }
        public string EarningsTypeID { get; set; }
        public string EarningsCode { get; set; }
        public string EarningsDesc { get; set; }
        public string EarningsCategory { get; set; }
        public string IsAbsenceAffected { get; set; }
        public string isTaxable { get; set; }
        public string isDelete { get; set; }
    }

    public class UpdeleteDeductionsCode
    {
        public string db { get; set; }
        public string TenantID { get; set; }
        public string LogUser { get; set; }
        public string DeductionsTypeID { get; set; }
        public string DeductionsCode { get; set; }
        public string DeductionsDesc { get; set; }
        public string DeductionsCategory { get; set; }
        public string IsBonus { get; set; }
        public string isTaxable { get; set; }
        public string isDelete { get; set; }
    }
    public class getPaycodeEarnings
    {
        public string EarningsTypeID { get; set; }
        public string EarningsCode { get; set; }
        public string EarningsDescription { get; set; }
        public string EarningsCategory { get; set; }
        public string IsTaxable { get; set; }
        public string IsAbsenceAffected { get; set; }
    }

    public class getPaycodeDeductions
    {

        public string DeductionsTypeID { get; set; }
        public string DeductionsCode { get; set; }
        public string DeductionsDescription { get; set; }
        public string DeductionsCategory { get; set; }
        public string IsTaxable { get; set; }
        public string IsBonus { get; set; }
    }
    public class clientSettingsCombinedList
    {
        public string combinedTenantID { get; set; }
    }
    public class CombinedSettings
    {
        public string db { get; set; }
        public string TenantID { get; set; }
        public string[] combinedTenantID { get; set; }
    }
    public class getCombinedReports
    {

        public string includeCompany { get; set; }

    }
    //ADDED FOR CLIENT SETTINGS END
    #region Unused
    public class TimeAttendance
    {
        public string StartDate { get; set; }
        public string EndDate { get; set; }

        public List<TimeAttendanceData> TimeAttendanceDatas = new List<TimeAttendanceData>();
    }
    public class TimeAttendanceData
    {
        public string Emp_ID { get; set; }
        public string First_Name { get; set; }
        public string Last_Name { get; set; }
        public string Reg_Days_Work { get; set; }
        public string Tardy_Hrs { get; set; }
        public string Undertime_Hrs { get; set; }
        public string Absent_Days { get; set; }
        public string LWOP_Days { get; set; }
        public string VL_Days { get; set; }
        public string SL_Days { get; set; }
        public string Other_Leaves_Days { get; set; }
        public string Night_Diff { get; set; }
        public string Regular_OT { get; set; }
        public string Regular_OT_NP { get; set; }
        public string Legal_Hol_OT { get; set; }
        public string Legal_Hol_OT_NP { get; set; }
        public string Legal_Hol_OT_EHrs { get; set; }
        public string Legal_Hol_OT_EHrs_NP { get; set; }
        public string Legal_Hol_RD { get; set; }
        public string Legal_Hol_RD_NP { get; set; }
        public string Legal_Hol_RD_EHrs { get; set; }
        public string Legal_Hol_RD_EHrs_NP { get; set; }
        public string Special_Hol_OT { get; set; }
        public string Special_Hol_OT_NP { get; set; }
        public string Special_Hol_OT_EHrs { get; set; }
        public string Special_Hol_OT_Ehrs_NP { get; set; }
        public string Special_Hol_RD { get; set; }
        public string Special_Hol_RD_NP { get; set; }
        public string Special_Hol_RD_EHrs { get; set; }
        public string Special_Hol_RD_EHrs_NP { get; set; }
        public string RestDay_OT { get; set; }
        public string RestDay_OT_NP { get; set; }
        public string RestDay_OT_EHrs { get; set; }
        public string RestDay_OT_EHrs_NP { get; set; }
        public string Double_Hol_OT { get; set; }
        public string Double_Hol_OT_NP { get; set; }
        public string Double_Hol_OT_EHrs { get; set; }
        public string Double_Hol_OT_EHrs_NP { get; set; }
        public string Double_Hol_RD { get; set; }
        public string Double_Hol_RD_NP { get; set; }
        public string Double_Hol_RD_EHrs { get; set; }
        public string Double_Hol_RD_EHrs_NP { get; set; }
        public string Remarks { get; set; }
    }
    public class IncomeDeduction
    {
        public string StartDate { get; set; }
        public string PayoutType { get; set; }
        public string EndDate { get; set; }
        public string IncomeOrDeduction { get; set; }
        public List<IncomeDeductionData> incomeDeductionDatas = new List<IncomeDeductionData>();
    }
    public class IncomeDeductionData
    {
        public string Emp_ID { get; set; }
        public string Date { get; set; }
        public string Code { get; set; }
        public string Amount { get; set; }
        public string Recur_Start_Date { get; set; }
        public string Recur_End_Date { get; set; }
        public string Frequency { get; set; }
        public string Remarks { get; set; }
    }
    public class LoanRegister
    {
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string PayoutType { get; set; }
        public List<LoanRegisterData> loanRegisterDatas { get; set; }
    }
    public class LoanRegisterData
    {
        public string Emp_ID { get; set; }
        public string Loan_Code { get; set; }
        public string Principal_Amount { get; set; }
        public string Total_Payments { get; set; }
        public string Loan_Starting_Deductions_Date { get; set; }
        public string Total_Previous_Payment { get; set; }
        public string Monthly_Loan_Amort_Amount { get; set; }
        public string Loan_Amort_Frequency { get; set; }
        public string Loan_Maturity_Date { get; set; }
        public string Date_Loan_Granted { get; set; }
        public string Account_Number { get; set; }
        public string Remarks { get; set; }
        public string Loan_Amount { get; set; }
    }
    #endregion
}