﻿using illimitadoWepAPI.MyClass;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;

namespace illimitadoWepAPI.App_Start
{
    public class PayslipMethod
    {

        public SqlConnection SysCon;
        public string ConString;
        public PayslipMethod()
        {
            ConString = ConfigurationManager.ConnectionStrings["connectionstring"].ConnectionString.ToString();
            SysCon = new SqlConnection(ConString);
        }

        public void RunPayslipScript(String DateFrom, String DateTo, String PayDate)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("sp_ComputePayroll", SysCon);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@DateFrom", DateFrom);
                cmd.Parameters.AddWithValue("@DateTo", DateTo);
                cmd.Parameters.AddWithValue("@PayON", PayDate);
                cmd.CommandTimeout = 86400;
                SysCon.Open();
                cmd.ExecuteNonQuery();
                SysCon.Close();
            }
            finally
            {
                SysCon.Close();
            }
        }
        public DataTable DisputesPayrollAdjustments(string empid, string datefrom, string dateto)
        {
            SysCon.Open();
            SqlCommand cmd = new SqlCommand("sp_GetPaySlipRecordsPaySlipDisputeAdj");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@EMPID", empid);
            cmd.Parameters.AddWithValue("@PAYFROM", datefrom);
            cmd.Parameters.AddWithValue("@PAYTO", dateto);
            cmd.Connection = SysCon;
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable disputes = new DataTable();
            disputes.Load(reader);
            SysCon.Close();
            return disputes;
        }
        public double DeminimisAmount(string empid, string datefrom, string dateto)
        {
            double deminimis;
            SysCon.Open();
            SqlCommand cmd = new SqlCommand("sp_Search_EmpCatDeminimis");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection = SysCon;
            cmd.Parameters.AddWithValue("@EMPID", empid);
            cmd.Parameters.AddWithValue("@PAYPERIODSTART", datefrom);
            cmd.Parameters.AddWithValue("@PAYPERIODEND", dateto);
            if (cmd.ExecuteScalar() != DBNull.Value)
            {
                deminimis = Convert.ToDouble(cmd.ExecuteScalar());
                SysCon.Close();
                return deminimis;
            }
            else
            {
                SysCon.Close();
                return deminimis = 0.00;
            }
        }
        public double FringeAmount(string empid, string datefrom, string dateto)
        {
            double fringe;
            SysCon.Open();
            SqlCommand cmd = new SqlCommand("sp_Search_EmpCatFringe");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection = SysCon;
            cmd.Parameters.AddWithValue("@EMPID", empid);
            fringe = Convert.ToDouble(cmd.ExecuteScalar());
            SysCon.Close();
            return fringe;
        }
        public double OtherBenefitsAmount(string empid, string datefrom, string dateto)
        {
            double otherbenefits;
            SysCon.Open();
            SqlCommand cmd = new SqlCommand("sp_Search_EmpCatOtherBenefits");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection = SysCon;
            cmd.Parameters.AddWithValue("@EMPID", empid);
            otherbenefits = Convert.ToDouble(cmd.ExecuteScalar());
            SysCon.Close();
            return otherbenefits;
        }
        public double SSSLoanDeduction(string empid, string datefrom, string dateto)
        {
            double sssloandeduction = 0.00;
            SysCon.Open();
            SqlCommand cmd = new SqlCommand("sp_GetPaySlipRecordsSSSLoans");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection = SysCon;
            cmd.Parameters.AddWithValue("@EMPID", empid);
            cmd.Parameters.AddWithValue("@DATEFROM", datefrom);
            cmd.Parameters.AddWithValue("@DATETO", dateto);
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable deduction = new DataTable();
            deduction.Load(reader);
            SysCon.Close();
            foreach (DataRow item in deduction.Rows)
            {
                if (item[0] != DBNull.Value)
                {
                    sssloandeduction = sssloandeduction + Convert.ToDouble(item[0].ToString());
                }
                else
                {

                }
            }
            sssloandeduction = Math.Round(sssloandeduction, 2);
            return sssloandeduction;
        }
        public double BonusPayoutMonth(string empid, string datefrom, string dateto)
        {
            double amount = 0.00;
            SysCon.Open();
            SqlCommand cmd = new SqlCommand("sp_Search_BonusPayoutMonth");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection = SysCon;
            cmd.Parameters.AddWithValue("@EMPID", empid);
            cmd.Parameters.AddWithValue("@DATEFROM", datefrom);
            cmd.Parameters.AddWithValue("@DATETO", dateto);
            if (cmd.ExecuteScalar() != DBNull.Value)
            {
                amount = Convert.ToDouble(cmd.ExecuteScalar());
            }
            SysCon.Close();
            return amount;
        }
        public double PayrollAdjustments(string empid, string payperiod, string adjustmenttype)
        {
            double adjustment = 0.00;
            SysCon.Open();
            SqlCommand cmd = new SqlCommand("sp_Get_Adjustments");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection = SysCon;
            cmd.Parameters.AddWithValue("@EMPID", empid);
            cmd.Parameters.AddWithValue("@PAYPERIOD", payperiod);
            //cmd.Parameters.AddWithValue("@ADJUSTMENTTYPE", adjustmenttype);
            if (cmd.ExecuteScalar() != DBNull.Value)
            {
                adjustment = Convert.ToDouble(cmd.ExecuteScalar());
            }
            SysCon.Close();
            return adjustment;
        }


        public DataTable getPayslipCompBenEmpCatBonusPayout(string emplevel, string empcat, string empid)
        {
            SysCon.Open();
            SqlCommand cmd = new SqlCommand("sp_Search_13thMonth_EmpCat");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@EMPLVL", emplevel);
            cmd.Parameters.AddWithValue("@EMPCAT", empcat);
            cmd.Parameters.AddWithValue("@EMPID", empid);
            cmd.Connection = SysCon;
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable bonuspayout = new DataTable();
            bonuspayout.Load(reader);
            SysCon.Close();
            return bonuspayout;
        }

        public string getGrossSalary(string empid, string datefrom, string dateto)
        {
            SysCon.Open();
            SqlCommand cmd = new SqlCommand("sp_GetGrossSalary", SysCon);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@EMPID", empid);
            cmd.Parameters.AddWithValue("@DATEFROM", datefrom);
            cmd.Parameters.AddWithValue("@DATETO", dateto);
            string result = cmd.ExecuteScalar().ToString();
            cmd.Dispose();
            SysCon.Close();
            return result;
        }

        public DataTable GetEmployeePayforDocs(string empid)
        {
            SysCon.Open();
            SqlCommand cmd = new SqlCommand("sp_SearchHRMSEmployeePay");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@EMPID", empid);
            cmd.Connection = SysCon;
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable sched = new DataTable();
            sched.Load(reader);
            SysCon.Close();
            return sched;
        }

        public string getTenureMonths(string empid)
        {
            SysCon.Open();
            SqlCommand cmd = new SqlCommand("sp_GetTenureMonths", SysCon);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@EMPID", empid);
            string result = cmd.ExecuteScalar().ToString();
            cmd.Dispose();
            SysCon.Close();
            return result;
        }

        public string getAbsences(string empid, string datefrom, string dateto)
        {
            SysCon.Open();
            SqlCommand cmd = new SqlCommand("sp_GetAbsences", SysCon);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@EMPID", empid);
            cmd.Parameters.AddWithValue("@DATEFROM", datefrom);
            cmd.Parameters.AddWithValue("@DATETO", dateto);
            string result = cmd.ExecuteScalar().ToString();
            cmd.Dispose();
            SysCon.Close();
            return result;
        }

        public string getTardiness(string empid, string datefrom, string dateto)
        {
            SysCon.Open();
            SqlCommand cmd = new SqlCommand("sp_GetTardiness", SysCon);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@EMPID", empid);
            cmd.Parameters.AddWithValue("@DATEFROM", datefrom);
            cmd.Parameters.AddWithValue("@DATETO", dateto);
            string result = cmd.ExecuteScalar().ToString();
            cmd.Dispose();
            SysCon.Close();
            return result;
        }

        public DataTable GetEmpLevelandCategory(string empid)
        {
            SysCon.Open();
            SqlCommand cmd = new SqlCommand("sp_SearchMasterControl_LeaveManagement_GetEmpLevelandCategory");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@EMPID", empid);
            cmd.Connection = SysCon;
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable EmpLevelandCat = new DataTable();
            EmpLevelandCat.Load(reader);
            SysCon.Close();
            return EmpLevelandCat;
        }

        public DataTable LatestPayslipForm()
        {
            SysCon.Open();
            SqlCommand cmd = new SqlCommand("sp_GetLatestPayslipForm");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection = SysCon;
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable payslipform = new DataTable();
            payslipform.Load(reader);
            SysCon.Close();
            return payslipform;
        }

        public DataTable PaySlipRecordsPayPeriod(string emplevel, string empcategory)
        {
            SysCon.Open();
            SqlCommand cmd = new SqlCommand("sp_GetPaySlipRecordsPayPeriod");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@EMPLVL", emplevel);
            cmd.Parameters.AddWithValue("@EMPCAT", empcategory);
            cmd.Connection = SysCon;
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable payslippayperiod = new DataTable();
            payslippayperiod.Load(reader);
            SysCon.Close();
            return payslippayperiod;
        }

        public DataTable PaySlipWorkedHours(string empid, string datefrom, string dateto)
        {
            SysCon.Open();
            SqlCommand cmd = new SqlCommand("sp_GetPaySlipWorkHours");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@EMPID", empid);
            cmd.Parameters.AddWithValue("@DATEFROM", datefrom);
            cmd.Parameters.AddWithValue("@DATETO", dateto);
            cmd.Connection = SysCon;
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable payslipworkedhours = new DataTable();
            payslipworkedhours.Load(reader);
            SysCon.Close();
            return payslipworkedhours;
        }

        public DataTable PaySlipTotalGovDeductions(string empid)
        {
            SysCon.Open();
            SqlCommand cmd = new SqlCommand("sp_GetPaySlipRecordsPaySlipDetailsAll");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@EMPID", empid);
            cmd.Connection = SysCon;
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable deduct = new DataTable();
            deduct.Load(reader);
            SysCon.Close();
            return deduct;
        }

        public DataTable PaySlipRecordsPaySlipDetails(string empid, string payperiod)
        {
            SysCon.Open();
            SqlCommand cmd = new SqlCommand("sp_GetPaySlipRecordsPaySlipDetails");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@EMPID", empid);
            cmd.Parameters.AddWithValue("@PAYPERIOD", payperiod);
            cmd.Connection = SysCon;
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable payslipdetails = new DataTable();
            payslipdetails.Load(reader);
            SysCon.Close();
            return payslipdetails;
        }

        public DataTable getPayslipRecordsAllowanceAmount(string empid)
        {
            SysCon.Open();
            SqlCommand cmd = new SqlCommand("sp_GetPaySlipRecordsAllowanceAmount");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@EMPID", empid);
            cmd.Connection = SysCon;
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable allowances = new DataTable();
            allowances.Load(reader);
            SysCon.Close();
            return allowances;
        }

        public DataTable getPayslipRecordsBonuses(string empid, string cutoff)
        {
            SysCon.Open();
            SqlCommand cmd = new SqlCommand("sp_GetPaySlipRecordsBonuses");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@EMPID", empid);
            cmd.Parameters.AddWithValue("@CUTOFF", cutoff);
            cmd.Connection = SysCon;
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable bonuses = new DataTable();
            bonuses.Load(reader);
            SysCon.Close();
            return bonuses;
        }

        public DataTable getPayslipCompBenEmpCatBonuses(string emplevel, string empcat, string payoutfrom, string payoutto, string bonusdayfrom, string bonusdayto)
        {
            SysCon.Open();
            SqlCommand cmd = new SqlCommand("sp_Search_CompBenEmpCatBonus");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@EMPLVL", emplevel);
            cmd.Parameters.AddWithValue("@EMPCAT", empcat);
            cmd.Parameters.AddWithValue("@PAYOUTFROM", payoutfrom);
            cmd.Parameters.AddWithValue("@PAYOUTTO", payoutto);
            cmd.Parameters.AddWithValue("@BONUSDAYFROM", bonusdayfrom);
            cmd.Parameters.AddWithValue("@BONUSDAYTO", bonusdayto);
            cmd.Connection = SysCon;
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable bonuses = new DataTable();
            bonuses.Load(reader);
            SysCon.Close();
            return bonuses;
        }

        public DataTable PaySlipRecordsPaySlipDetailsYearToDate(string empid, string payfrom, string payto)
        {
            SysCon.Open();
            SqlCommand cmd = new SqlCommand("sp_GetPaySlipRecordsPaySlipDetailsYearToDate");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@EMPID", empid);
            cmd.Parameters.AddWithValue("@PAYFROM", payfrom);
            cmd.Parameters.AddWithValue("@PAYTO", payto);
            cmd.Connection = SysCon;
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable payslipdetails = new DataTable();
            payslipdetails.Load(reader);
            SysCon.Close();
            return payslipdetails;
        }

        public DataTable PaySlipRecordsLeaveHoursAmountYearToDate(string empid, string payfrom, string payto)
        {
            SysCon.Open();
            SqlCommand cmd = new SqlCommand("sp_GetPaySlipRecordsLeavehoursAmountYearToDate");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@EMPID", empid);
            cmd.Parameters.AddWithValue("@PAYFROM", payfrom);
            cmd.Parameters.AddWithValue("@PAYTO", payto);
            cmd.Connection = SysCon;
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable payslipdetails = new DataTable();
            payslipdetails.Load(reader);
            SysCon.Close();
            return payslipdetails;
        }

        public DataTable PaySlipRecordsLeaveHoursAmount(string empid, string payfrom, string payto)
        {
            SysCon.Open();
            SqlCommand cmd = new SqlCommand("sp_GetPaySlipRecordsLeaveHoursAmount");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@EMPID", empid);
            cmd.Parameters.AddWithValue("@PAYPERIODFROM", payfrom);
            cmd.Parameters.AddWithValue("@PAYPERIODTO", payto);
            cmd.Connection = SysCon;
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable payslipdetails = new DataTable();
            payslipdetails.Load(reader);
            SysCon.Close();
            return payslipdetails;
        }

        public DataTable PaySlipRecordsEmpMaster(string empid)
        {
            SysCon.Open();
            SqlCommand cmd = new SqlCommand("sp_GetPaySlipRecordsEmpMaster");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@EMPID", empid);
            cmd.Connection = SysCon;
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable empmaster = new DataTable();
            empmaster.Load(reader);
            SysCon.Close();
            return empmaster;
        }

        public DataTable PaySlipRecordsConfidentialInfo(string empid)
        {
            SysCon.Open();
            SqlCommand cmd = new SqlCommand("sp_GetPaySlipRecordsConfidentialInfo");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@EMPID", empid);
            cmd.Connection = SysCon;
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable confidentialinfo = new DataTable();
            confidentialinfo.Load(reader);
            SysCon.Close();
            return confidentialinfo;
        }

        public DataTable PaySlipRecordsEmployeePay(string empid)
        {
            SysCon.Open();
            SqlCommand cmd = new SqlCommand("sp_GetPaySlipRecordsEmployeePay");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@EMPID", empid);
            cmd.Connection = SysCon;
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable employeepay = new DataTable();
            employeepay.Load(reader);
            SysCon.Close();
            return employeepay;
        }

        public string searchempname(string empid)
        {

            string temp = "";
            SysCon.Open();
            SqlCommand cmd = new SqlCommand("sp_Search_EmpName");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@EMPID", empid);
            cmd.Connection = SysCon;
            SqlDataReader reader = cmd.ExecuteReader();
            reader.Read();
            if (reader.HasRows)
            {
                temp = reader["empname"].ToString();
            }
            SysCon.Close();
            return temp;

        }

        public DataTable getdataforCOE(string empid)
        {
            SysCon.Open();
            SqlCommand cmd = new SqlCommand("sp_get_dataforCOE");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@EMPID", empid);
            cmd.Connection = SysCon;
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable coe = new DataTable();
            coe.Load(reader);
            SysCon.Close();
            return coe;
        }
        public string BitSign, BitSignPres, ApproverName, ApproverJobDesc, ReqName, ReqDesc;
        public void GetSignatureSignPicture(string approverEMPID, string Types)
        {
            SysCon.Open();
            SqlCommand cmd = new SqlCommand("sp_SearchBased64", SysCon);//declare sql query
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@EMPID", approverEMPID);
            cmd.Parameters.AddWithValue("@TYPES", Types);
            SqlDataReader myReader = cmd.ExecuteReader();//declare sqldatareader
            myReader.Read();
            if (myReader.HasRows)
            {
                BitSign = myReader.GetValue(0).ToString();
            }
            myReader.Close();
            cmd.Dispose();//dispose sqlquery
            SysCon.Close();//close sqlconnection
        }

        public void GetSignatureSignPicturePres(string approverEMPID)
        {
            SysCon.Open();
            SqlCommand cmd = new SqlCommand("sp_SearchBased64Pres", SysCon);//declare sql query
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@EMPID", approverEMPID);
            SqlDataReader myReader = cmd.ExecuteReader();//declare sqldatareader
            myReader.Read();
            if (myReader.HasRows)
            {
                BitSignPres = myReader.GetValue(0).ToString();
            }
            myReader.Close();
            cmd.Dispose();//dispose sqlquery
            SysCon.Close();//close sqlconnection
        }

        public string sig1, sig2, sig3;
        public void GetSignatureALL(string signature)
        {
            SysCon.Open();
            SqlCommand cmd = new SqlCommand("sp_search_DocumentSignature", SysCon);//declare sql query
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@SIGNATURE", signature);
            SqlDataReader myReader = cmd.ExecuteReader();//declare sqldatareader
            myReader.Read();
            if (myReader.HasRows)
            {
                if (signature == "Signature 1")
                {
                    sig1 = myReader.GetValue(0).ToString();
                }
                else if (signature == "Signature 2")
                {
                    sig2 = myReader.GetValue(0).ToString();
                }
                else if (signature == "Signature 3")
                {
                    sig3 = myReader.GetValue(0).ToString();
                }

            }
            myReader.Close();
            cmd.Dispose();//dispose sqlquery
            SysCon.Close();//close sqlconnection
        }

        public DataTable BonusPayoutMonths(string empid, string month)
        {
            Connection con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = empid });
            con.myparameters.Add(new myParameters { ParameterName = "@MONTH", mytype = SqlDbType.NVarChar, Value = month });
            return con.GetDataTable("sp_GetBonusPayouts");
        }

        public double grosspay(string empid, string payperiod)
        {

            double temp = 0;
            SysCon.Open();
            SqlCommand cmd = new SqlCommand("sp_GetGrossPay");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@EMPID", empid);
            cmd.Parameters.AddWithValue("@PAYPERIOD", payperiod);
            cmd.Connection = SysCon;
            SqlDataReader reader = cmd.ExecuteReader();
            reader.Read();
            if (reader.HasRows)
            {
                temp = Convert.ToDouble(reader["GrossPay"]);
            }
            SysCon.Close();
            return temp;

        }


        public DataTable GetSalarySchemes(string elvlsal, string ecatsal)
        {
            SysCon.Open();
            SqlCommand cmd = new SqlCommand("sp_SearchSalaryPayoutSchemes", SysCon);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@EMPLVL", elvlsal);
            cmd.Parameters.AddWithValue("@EMPCAT", ecatsal);
            SqlDataAdapter aar13 = new SqlDataAdapter(cmd);
            DataTable ds = new DataTable();
            aar13.Fill(ds);
            SysCon.Close();
            return ds;
        }

        public DataTable PayslipDetailsYearToDate(string empid, string year, string payperiod)
        {
            Connection con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = empid });
            con.myparameters.Add(new myParameters { ParameterName = "@YEAR", mytype = SqlDbType.NVarChar, Value = year });
            con.myparameters.Add(new myParameters { ParameterName = "@PAYPERIOD", mytype = SqlDbType.NVarChar, Value = payperiod });
            return con.GetDataTable("sp_GetPaySlipRecordsPaySlipDetailsYearToDate");
        }




    }
}