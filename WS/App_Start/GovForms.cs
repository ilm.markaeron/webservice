﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;

namespace illimitadoWepAPI.App_Start
{
    public class GovForms
    {
        public SqlConnection SysCon;
        public string ConString;
        public GovForms()
        {
            ConString = ConfigurationManager.ConnectionStrings["connectionstring"].ConnectionString.ToString();
            SysCon = new SqlConnection(ConString);
        }
        #region getPagibigMPL
        public DataTable getPagibigMPL(string empid)
        {
            SysCon.Open();
            SqlCommand cmd = new SqlCommand("sp_PagibigMPL", SysCon);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@empid", empid);
            SqlDataAdapter ar = new SqlDataAdapter(cmd);
            DataTable ds = new DataTable();
            ar.Fill(ds);
            SysCon.Close();
            return ds;
        }
        #endregion
        public string housenuma, maritala,
                    streetnamea,
                    barangaya,
                    towna,
                    citya,
                    provincea,
                    religiona,
                    zipcodea,
                    bloodtypea,
                    gendera,
                    maritalstata,
                    doba,
                    citizenshipa,
                    nameofa,
                    mobileareacodea,
                    mobilenumbera,
                    homeeracodea,
                    homenumbera,
                    personalemaila,
                    emrgnamea,
                    emrgnumbera,
                    emrgrelationshipa,
                    optionaemaila,
                    contacta,
                    pob,
                    religion,
                    mothername,
                    fathername,
                    payrollinfo,
                    bankname,
                    branchname,
                    accountnumber,
                    height,
                    weight;
        public string
            zemployeeNumber,
            zempfirstname,
            zempmiddlename,
            zemplastname,
            zntLog,
            zalias,
            zdepartment,
            zdepname,
            zbussName,
            zbussHead,
            zworks,
            zimiSuper,
            zdepfrom,
            zdepto,
            zlocfrom,
            zlocto,
            zcosfrom,
            zcosto,
            zemail,
            zextension,
            zjoiningDate,
            zproductionStart,
            ztenure,
            ztenureinmonth,
            zbatch,
            zclasstxt,
            zskill,
            zjob,
            zpromotion,
            zstatusSig,
            zdepcoderesult,
            zmngridnameresult,
            zemplevel,
            zcategory,
            zbusgment,
            wemail,
            DateJoined, ProductionStartDate, Wallet, Tenure, TenureMonths, Batch, Class, Skill, JobDesc, Promotion, NameOfOrganization,
            extension,
            zwallet;
        public string FNAMEEMRG, MNAMEEMRG, LNAMEEMRG;
        public string Form, Pattern, ReqStart, ReqEnd, CoveredStart, CoveredEnd, NotifMsg, President, PresBck, HR, HRBck, Savedloc, SharedEmail, EmailTo, EmailCc, EmailBcc, EmailSubject, EmailMessege, SignMessage, Default1, Default2;
        public int Function, DailyDay, DailyWeek, RecurWeek, Monday, Tuesday, Wednesday, Thursday, Friday, Saturday, Sunday, DayMonth, Month, Quarter, Day, Months, EndDay, AutoSign, Permission, PrintSign, FormRequested, SignedDays, SaveLoc, SharEmail, Email, Number;

        public void getEmployeePersonalInformation(string empId)
        {
            SysCon.Open();
            SqlDataReader myReader = null;
            SqlCommand cmd = new SqlCommand("sp_SearchHRMSEmployeePersonalInformation", SysCon);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection = SysCon;
            cmd.Parameters.AddWithValue("@EMPID", empId);
            myReader = cmd.ExecuteReader();
            while (myReader.Read())
            {
                housenuma = (myReader["HouseNumber"].ToString());
                streetnamea = (myReader["StreetName"].ToString());
                barangaya = (myReader["Barangay"].ToString());
                towna = (myReader["Town"].ToString());
                citya = (myReader["City"].ToString());
                religiona = (myReader["Region"].ToString());
                zipcodea = (myReader["ZipCode"].ToString());
                bloodtypea = (myReader["BloodType"].ToString());
                gendera = (myReader["Gender"].ToString());
                maritalstata = (myReader["MaritalStatus"].ToString());
                if (string.IsNullOrWhiteSpace(myReader["DOB"].ToString()) == true)
                {
                    doba = "";
                }
                else
                {
                    doba = Convert.ToDateTime(myReader["DOB"]).ToString("yyyy-MM-dd");
                }

                citizenshipa = (myReader["Citizenship"].ToString());
                nameofa = (myReader["NameOfOrganization"].ToString());
                mobileareacodea = (myReader["MobileAreaCode"].ToString());
                mobilenumbera = (myReader["MobileNumber"].ToString());
                homeeracodea = (myReader["HomeAreaCode"].ToString());
                homenumbera = (myReader["HomeNumber"].ToString());
                personalemaila = (myReader["Email"].ToString());
                wemail = (myReader["WorkEmail"].ToString());
                extension = (myReader["Extension"].ToString());
                emrgnamea = (myReader["EmrgName"].ToString());
                emrgnumbera = (myReader["EmrgNumber"].ToString());
                emrgrelationshipa = (myReader["EmrgRelationship"].ToString());
                optionaemaila = (myReader["OptionalEmail"].ToString());
                contacta = (myReader["ContactNo"].ToString());
                provincea = (myReader["Province"].ToString());
                FNAMEEMRG = (myReader["EmrgFname"].ToString());
                MNAMEEMRG = (myReader["EmrgMname"].ToString());
                LNAMEEMRG = (myReader["EmrgLname"].ToString());
                DateJoined = (myReader["DateJoined"].ToString());
                ProductionStartDate = (myReader["ProductionStartDate"].ToString());
                Wallet = (myReader["Wallet"].ToString());
                Tenure = (myReader["Tenure"].ToString());
                TenureMonths = (myReader["TenureMonths"].ToString());
                Batch = (myReader["Batch"].ToString());
                Class = (myReader["Class"].ToString());
                Skill = (myReader["Skill"].ToString());
                JobDesc = (myReader["JobDesc"].ToString());
                Promotion = (myReader["Promotion"].ToString());
                NameOfOrganization = (myReader["NameOfOrganization"].ToString());
                maritala = (myReader["MaritalStatus"].ToString());
                pob = (myReader["POB"].ToString());
                religion = (myReader["religion"].ToString());
                mothername = (myReader["MotherName"].ToString());
                fathername = (myReader["FatherName"].ToString());
                payrollinfo = (myReader["Payrollinfo"].ToString());
                bankname = (myReader["BankName"].ToString());
                branchname = (myReader["BranchName"].ToString());
                accountnumber = (myReader["AccountNumber"].ToString());
                height = (myReader["Height"].ToString());
                weight = (myReader["Weight"].ToString());
            }
            myReader.Close();
            cmd.Dispose();
            SysCon.Close();

        }
        public DataTable GetPersonalInfo(string empid)
        {
            SysCon.Open();
            SqlCommand cmd = new SqlCommand("sp_getPersonalContactInfo");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection = SysCon;
            cmd.Parameters.AddWithValue("@EMPID", empid);
            SqlDataAdapter sapersonal = new SqlDataAdapter(cmd);
            DataTable dtpersonal = new DataTable();
            sapersonal.Fill(dtpersonal);
            SysCon.Close();
            return dtpersonal;
        }
        public DataTable GetEmailInfo(string empid)
        {
            SysCon.Open();
            SqlCommand cmd = new SqlCommand("sp_getPersonalEmailInfo");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection = SysCon;
            cmd.Parameters.AddWithValue("@EMPID", empid);
            SqlDataAdapter saemail = new SqlDataAdapter(cmd);
            DataTable dtemail = new DataTable();
            saemail.Fill(dtemail);
            SysCon.Close();
            return dtemail;
        }
        public DataTable getCompanyProfile(string empid)
        {
            SysCon.Open();
            SqlCommand cmd = new SqlCommand("sp_ShowCompanyDetails", SysCon);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@EMPID", empid);
            SqlDataAdapter ar = new SqlDataAdapter(cmd);
            DataTable ds = new DataTable();
            ar.Fill(ds);
            SysCon.Close();
            return ds;
        }

        public void GetRequestFormSettings()
        {
            SysCon.Close();
            SysCon.Open();
            SqlCommand cmd = new SqlCommand("sp_SearchHRMSDocumentsRequestForm", SysCon);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataReader reader = cmd.ExecuteReader();
            reader.Read();
            if (reader.HasRows)
            {
                Form = reader["Form"].ToString();
                Function = Convert.ToInt32(reader["Functions"].ToString());
                Pattern = reader["Pattern"].ToString();
                DailyDay = Convert.ToInt32(reader["EveryNday"].ToString());
                DailyWeek = Convert.ToInt32(reader["EveryWeekday"].ToString());
                RecurWeek = Convert.ToInt32(reader["RecurWeek"].ToString());
                Monday = Convert.ToInt32(reader["RecurMon"].ToString());
                Tuesday = Convert.ToInt32(reader["RecurTue"].ToString());
                Wednesday = Convert.ToInt32(reader["RecurWed"].ToString());
                Thursday = Convert.ToInt32(reader["RecurThu"].ToString());
                Friday = Convert.ToInt32(reader["RecurFri"].ToString());
                Saturday = Convert.ToInt32(reader["RecurSat"].ToString());
                Sunday = Convert.ToInt32(reader["RecurSun"].ToString());
                DayMonth = Convert.ToInt32(reader["DayNMonth"].ToString());
                Month = Convert.ToInt32(reader["NMonth"].ToString());
                Quarter = Convert.ToInt32(reader["NQuarter"].ToString());
                Day = Convert.ToInt32(reader["NDay"].ToString());
                Months = Convert.ToInt32(reader["EveryNMonth"].ToString());
                ReqStart = reader["RequestStart"].ToString();
                ReqEnd = reader["RequestEnd"].ToString();
                EndDay = Convert.ToInt32(reader["EndDay"].ToString());
                CoveredStart = reader["CoveredStart"].ToString();
                CoveredEnd = reader["CoveredEnd"].ToString();
                NotifMsg = reader["NotificationMessege"].ToString();
                President = reader["President"].ToString();
                PresBck = reader["PresBackup"].ToString();
                HR = reader["HR"].ToString();
                HRBck = reader["HRBackup"].ToString();
                AutoSign = Convert.ToInt32(reader["AutoSign"].ToString());
                Permission = Convert.ToInt32(reader["SignPermission"].ToString());
                PrintSign = Convert.ToInt32(reader["PrintSign"].ToString());
                if (string.IsNullOrWhiteSpace(reader["Default1"].ToString()) == false)
                {
                    Default1 = reader["Default1"].ToString();
                }
                else
                {
                    Default1 = "False";
                }
                if (string.IsNullOrWhiteSpace(reader["Default2"].ToString()) == false)
                {
                    Default2 = reader["Default2"].ToString();
                }
                else
                {
                    Default2 = "False";
                }
                FormRequested = Convert.ToInt32(reader["FormIsRequested"].ToString());
                SignedDays = Convert.ToInt32(reader["SignedForNDays"].ToString());
                SaveLoc = Convert.ToInt32(reader["SaveLocation"].ToString());
                Savedloc = reader["Location"].ToString();
                SharEmail = Convert.ToInt32(reader["ShareEmail"].ToString());
                SharedEmail = reader["Email"].ToString();
                Email = Convert.ToInt32(reader["SendToEmail"].ToString());
                Number = Convert.ToInt32(reader["SendToNumber"].ToString());
                EmailTo = reader["EmailTo"].ToString();
                EmailCc = reader["EmailCc"].ToString();
                EmailBcc = reader["EmailBcc"].ToString();
                EmailSubject = reader["EmailSubject"].ToString();
                EmailMessege = reader["EmailMessege"].ToString();
                SignMessage = reader["SignMessage"].ToString();
                reader.Close();
                cmd.Dispose();

            }
            SysCon.Close();
        }
        public string signatory1, signatory2, position1, position2, active1, active2, empid1, empid2;
        public void SignatoryNew(string empid, string formname)
        {
            SysCon.Close();
            SysCon.Open();
            SqlCommand cmd = new SqlCommand("sp_GetSignatoryNew", SysCon);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@EmpID", empid);
            cmd.Parameters.AddWithValue("@FormName", formname);
            SqlDataReader reader = cmd.ExecuteReader();
            reader.Read();
            if (reader.HasRows)
            {
                signatory1 = reader["EmployeeName"].ToString();
                position1 = reader["JobDesc"].ToString();
                empid1 = reader["EmpID"].ToString();
            }
            reader.Close();
            cmd.Dispose();
            SysCon.Close();

            SysCon.Open();
            SqlCommand scmd = new SqlCommand("sp_GetSignatory2", SysCon);
            scmd.CommandType = CommandType.StoredProcedure;
            scmd.Parameters.AddWithValue("@EMPID", empid);
            SqlDataReader sreader = scmd.ExecuteReader();
            sreader.Read();
            if (sreader.HasRows)
            {
                empid2 = sreader["EmpID"].ToString();
                active2 = sreader["SIgnator2Status"].ToString();
                signatory2 = sreader["EmpName"].ToString();
                position2 = sreader["JobDesc"].ToString();
            }
            sreader.Close();
            scmd.Dispose();
            SysCon.Close();

        }
        public string ApproverEMPID, ApproverName, ApproverJobDesc, BitSign;
        public void GetAppEMP(int FORMIDS, string empid)
        {
            SysCon.Open();
            SqlCommand myCommand = new SqlCommand("sp_GetApproverEmpid", SysCon);//declare sql query
            myCommand.CommandType = CommandType.StoredProcedure;
            myCommand.Parameters.AddWithValue("@FORMID", FORMIDS);
            myCommand.Parameters.AddWithValue("@EMPID", empid);
            SqlDataReader myReader = myCommand.ExecuteReader();//declare sqldatareader
            myReader.Read();
            if (myReader.HasRows)
            {
                ApproverEMPID = myReader.GetValue(0).ToString();
            }
            myReader.Close();
            myCommand.Dispose();//dispose sqlquery
            SysCon.Close();//close sqlconnection
        }

        public void GetApproverNameJobDes(string approverEMPID)
        {
            SysCon.Open();
            SqlCommand cmd = new SqlCommand("sp_SearchEmpaneJobSinatory", SysCon);//declare sql query
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@EMPIDAPPROVER", approverEMPID);
            SqlDataReader myReader = cmd.ExecuteReader();//declare sqldatareader
            myReader.Read();
            if (myReader.HasRows)
            {
                ApproverName = myReader.GetValue(0).ToString();
                ApproverJobDesc = myReader.GetValue(1).ToString();
            }
            myReader.Close();
            cmd.Dispose();//dispose sqlquery
            SysCon.Close();//close sqlconnection
        }

        public void GetSignatureSignPicture(string approverEMPID, string Types)
        {
            SysCon.Open();
            SqlCommand cmd = new SqlCommand("sp_SearchBased64", SysCon);//declare sql query
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@EMPID", approverEMPID);
            cmd.Parameters.AddWithValue("@TYPES", Types);
            SqlDataReader myReader = cmd.ExecuteReader();//declare sqldatareader
            myReader.Read();
            if (myReader.HasRows)
            {
                BitSign = myReader.GetValue(0).ToString();
            }
            myReader.Close();
            cmd.Dispose();//dispose sqlquery
            SysCon.Close();//close sqlconnection
        }

        public string hourlys,
        dailys,
        monthlys,
        complexity_premiums,
        foods,
        travels,
        clothings,
        night_differentials,
        regular_overtimes,
        regular_holidays,
        regular_holiday_ots,
        regular_holiday_nds,
        special_holidays,
        special_holiday_ots,
        special_holiday_nds,
        bank_accounts,
        tax_statuss,
        employee_salarys;
        public void getEmployeePay(string EmpID)
        {
            SysCon.Open();
            SqlDataReader readers = null;
            SqlCommand cmd = new SqlCommand("sp_SearchHRMSEmployeePay", SysCon);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection = SysCon;
            cmd.Parameters.AddWithValue("@EMPID", EmpID);
            readers = cmd.ExecuteReader();
            while (readers.Read())
            {
                hourlys = (readers["hourly"].ToString());
                dailys = (readers["daily"].ToString());
                monthlys = (readers["monthly"].ToString());
                complexity_premiums = (readers["complexity_premium"].ToString());
                foods = (readers["food"].ToString());
                travels = (readers["travel"].ToString());
                clothings = (readers["clothing"].ToString());
                night_differentials = (readers["night_differential"].ToString());
                regular_overtimes = (readers["regular_overtime"].ToString());
                regular_holidays = (readers["regular_holiday"].ToString());
                regular_holiday_ots = (readers["regular_holiday_ot"].ToString());
                regular_holiday_nds = (readers["regular_holiday_nd"].ToString());
                special_holidays = (readers["special_holiday"].ToString());
                special_holiday_ots = (readers["special_holiday_ot"].ToString());
                special_holiday_nds = (readers["special_holiday_nd"].ToString());
                bank_accounts = (readers["bank_account"].ToString());
                tax_statuss = (readers["tax_status"].ToString());

            }


            readers.Close();
            cmd.Dispose();
            SysCon.Close();
        }

        #region Pag-Ibig Loyalty Card Application
        public DataTable GetLoyaltyCard(string empid)
        {
            SysCon.Open();
            SqlCommand cmd = new SqlCommand("sp_LoyaltyCardApp", SysCon);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@EMPID", empid);
            SqlDataAdapter ar = new SqlDataAdapter(cmd);
            DataTable ds = new DataTable();
            ar.Fill(ds);
            SysCon.Close();
            return ds;
        }
        #endregion

        #region SSS Salary Loan

        public DataTable GetSSSSalaryLoan(string empid)
        {
            SysCon.Open();
            SqlCommand cmd = new SqlCommand("sp_SSS_Salary_Loan", SysCon);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@EMPID", empid);
            SqlDataAdapter ee = new SqlDataAdapter(cmd);
            DataTable ds = new DataTable();
            ee.Fill(ds);
            SysCon.Close();
            return ds;
        }

        #endregion
        #region Maternity Benefits Application
        public DataTable GetMatBen(string empid)
        {
            SysCon.Open();
            SqlCommand cmd = new SqlCommand("sp_MatBenApp", SysCon);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@EMPID", empid);
            SqlDataAdapter ar = new SqlDataAdapter(cmd);
            DataTable ds = new DataTable();
            ar.Fill(ds);
            SysCon.Close();
            return ds;
        }
        #endregion

        public DataTable GetCompanyContactPhone(string CODEBRANCH)
        {
            SysCon.Open();
            SqlCommand cmd = new SqlCommand("sp_GetCompanyContactPhone", SysCon);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@CODEBRANCH", CODEBRANCH);
            SqlDataAdapter SalaInfo = new SqlDataAdapter(cmd);
            DataTable SalaData = new DataTable();
            SalaInfo.Fill(SalaData);
            SysCon.Close();
            return SalaData;
        }

        public DataTable GetCompanyEmail(string branchid)
        {
            SysCon.Open();
            SqlCommand cmd = new SqlCommand("sp_GetCompanyContactEmail", SysCon);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@CODEBRANCH", branchid);
            SqlDataAdapter SalaInfo = new SqlDataAdapter(cmd);
            DataTable SalaData = new DataTable();
            SalaInfo.Fill(SalaData);
            SysCon.Close();
            return SalaData;
        }

        public DataTable GetEmployeePayforDocs(string empid)
        {
            SysCon.Open();
            SqlCommand cmd = new SqlCommand("sp_SearchHRMSEmployeePay");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@EMPID", empid);
            cmd.Connection = SysCon;
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable sched = new DataTable();
            sched.Load(reader);
            SysCon.Close();
            return sched;
        }

        public string getJobDesc(string empid)
        {
            string jobdesc = "";
            SysCon.Open();
            SqlCommand cmd = new SqlCommand("sp_GetEmployeeMaster", SysCon);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@empid", empid);
            cmd.Connection = SysCon;
            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                jobdesc = (reader["JobDesc"].ToString());
            }
            SysCon.Close();
            return jobdesc;
        }
        #region SSSForms_Personal_Record_E1
        public DataTable GetSSSFORMSE1(string empid)
        {
            SysCon.Open();
            SqlCommand cmd = new SqlCommand("sp_SssE1", SysCon);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@EMPID", empid);
            SqlDataAdapter Data = new SqlDataAdapter(cmd);
            DataTable ssse1data = new DataTable();
            Data.Fill(ssse1data);
            SysCon.Close();
            return ssse1data;

        }
        #endregion

        public DataTable getDependent(string empid)
        {
            SysCon.Open();
            SqlCommand cmd = new SqlCommand("SearchDependentsv2", SysCon);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@empid", empid);
            cmd.Connection = SysCon;
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable empinfo = new DataTable();
            empinfo.Load(reader);
            SysCon.Close();
            return empinfo;
        }

        #region Calamity Loan
        public DataTable GetCalamityLoan(string empid)
        {
            SysCon.Open();
            SqlCommand cmd = new SqlCommand("sp_CalamityLoan", SysCon);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@EMPID", empid);
            SqlDataAdapter ar = new SqlDataAdapter(cmd);
            DataTable ds = new DataTable();
            ar.Fill(ds);
            SysCon.Close();
            return ds;
        }
        #endregion

        #region ML1FORM
        public DataTable GetML1FORM(string empid)
        {
            SysCon.Open();
            SqlCommand cmd = new SqlCommand("sp_ML1FORM", SysCon);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@EMPID", empid);
            SqlDataAdapter mld = new SqlDataAdapter(cmd);
            DataTable ds = new DataTable();
            mld.Fill(ds);
            SysCon.Close();
            return ds;
        }
        #endregion

        public void Signatory(string empid)
        {
            SysCon.Close();
            SysCon.Open();
            SqlCommand cmd = new SqlCommand("sp_GetSignatory1", SysCon);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@EMPID", empid);
            SqlDataReader reader = cmd.ExecuteReader();
            reader.Read();
            if (reader.HasRows)
            {
                empid1 = reader["EmpID"].ToString();
                active1 = reader["SIgnator1Status"].ToString();
                signatory1 = reader["EmpName"].ToString();
                position1 = reader["JobDesc"].ToString();
            }
            reader.Close();
            cmd.Dispose();
            SysCon.Close();

            SysCon.Open();
            SqlCommand scmd = new SqlCommand("sp_GetSignatory2", SysCon);
            scmd.CommandType = CommandType.StoredProcedure;
            scmd.Parameters.AddWithValue("@EMPID", empid);
            SqlDataReader sreader = scmd.ExecuteReader();
            sreader.Read();
            if (sreader.HasRows)
            {
                empid2 = sreader["EmpID"].ToString();
                active2 = sreader["SIgnator2Status"].ToString();
                signatory2 = sreader["EmpName"].ToString();
                position2 = sreader["JobDesc"].ToString();
            }
            sreader.Close();
            scmd.Dispose();
            SysCon.Close();

        }

        #region RF1

        public DataTable GetRF1(string empid)
        {
            SysCon.Open();
            SqlCommand cmd = new SqlCommand("sp_RF1", SysCon);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@EMPID", empid);
            SqlDataAdapter ar = new SqlDataAdapter(cmd);
            DataTable ds = new DataTable();
            ar.Fill(ds);
            SysCon.Close();
            return ds;
        }

        #endregion
        #region AlphaList SP's
        public DataTable getNamesandGovNumbers()
        {
            SysCon.Open();
            SqlCommand cmd = new SqlCommand("sp_getAllNamesandGovNumbers", SysCon);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection = SysCon;
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable empinfo = new DataTable();
            empinfo.Load(reader);
            SysCon.Close();
            return empinfo;
        }
        #endregion
        public DataTable getEmployeeIDbyFullName(string fullname)
        {
            SysCon.Open();
            SqlCommand cmd = new SqlCommand("sp_searchEmpIDbyFullName", SysCon);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@fullname", fullname);
            cmd.Connection = SysCon;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable temp = new DataTable();
            da.Fill(temp);
            SysCon.Close();
            return temp;
        }

        public string getEmpPay(string empid)
        {
            string jobdesc = "";
            SysCon.Open();
            SqlCommand cmd = new SqlCommand("sp_getEmpPay", SysCon);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@empid", empid);
            cmd.Connection = SysCon;
            SqlDataReader reader = cmd.ExecuteReader();
            try
            {
                while (reader.Read())
                {
                    jobdesc = (reader["employee_salary"].ToString());
                }
            }
            catch
            {
                jobdesc = "0";
            }
            SysCon.Close();
            return jobdesc;
        }

        public string getJoinDate(string empid)
        {
            string jobdesc = "";
            SysCon.Open();
            SqlCommand cmd = new SqlCommand("sp_GetEmployeeMaster", SysCon);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@empid", empid);
            cmd.Connection = SysCon;
            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                jobdesc = (reader["DateJoined"].ToString());
            }
            SysCon.Close();
            jobdesc = Convert.ToDateTime(jobdesc).ToString("MMddyyyy");
            return jobdesc;
        }

        public DataTable getGovContrib(string empid, string yr, string month)
        {
            SysCon.Open();
            SqlCommand cmd = new SqlCommand("sp_GetPaySlipGovContribution", SysCon);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@empid", empid);
            cmd.Parameters.AddWithValue("@year", yr);
            cmd.Parameters.AddWithValue("@month", month);
            cmd.Connection = SysCon;
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable empinfo = new DataTable();
            empinfo.Load(reader);
            SysCon.Close();
            return empinfo;
        }
        public DataTable PaySlipRecordsEmployeePay(string empid)
        {
            SysCon.Open();
            SqlCommand cmd = new SqlCommand("sp_GetPaySlipRecordsEmployeePay");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@EMPID", empid);
            cmd.Connection = SysCon;
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable employeepay = new DataTable();
            employeepay.Load(reader);
            SysCon.Close();
            return employeepay;
        }
        #region SSS Contribution Payment

        public DataTable GetSSSempchangereq(string empid)
        {
            SysCon.Open();
            SqlCommand cmd = new SqlCommand("sp_SSS_ChangeEmp_Request", SysCon);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@EMPID", empid);
            SqlDataAdapter ER = new SqlDataAdapter(cmd);
            DataTable CF = new DataTable();
            ER.Fill(CF);
            SysCon.Close();
            return CF;
        }

        #endregion

        #region FPF400FORM
        public DataTable GetFPF400FORM(string empid)
        {
            SysCon.Open();
            SqlCommand cmd = new SqlCommand("sp_GetFPF400", SysCon);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@EMPID", empid);
            SqlDataAdapter stlrf = new SqlDataAdapter(cmd);
            DataTable ds = new DataTable();
            stlrf.Fill(ds);
            SysCon.Close();
            return ds;
        }
        #endregion

        #region getEmployeeMaster
        public DataTable getEmployeeMaster(string empid)
        {
            SysCon.Open();
            SqlCommand cmd = new SqlCommand("sp_GetEmployeeMaster");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@EMPID", empid);
            cmd.Connection = SysCon;
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable ds = new DataTable();
            ds.Load(reader);
            SysCon.Close();
            zcategory = ds.Rows[0][6].ToString();
            zemplevel = ds.Rows[0][7].ToString();
            return ds;
        }
        #endregion

        public double DeminimisAmount(string empid, string datefrom, string dateto)
        {
            double deminimis;
            SysCon.Open();
            SqlCommand cmd = new SqlCommand("sp_Search_EmpCatDeminimis");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection = SysCon;
            cmd.Parameters.AddWithValue("@EMPID", empid);
            cmd.Parameters.AddWithValue("@PAYPERIODSTART", datefrom);
            cmd.Parameters.AddWithValue("@PAYPERIODEND", dateto);
            if (cmd.ExecuteScalar() != DBNull.Value)
            {
                deminimis = Convert.ToDouble(cmd.ExecuteScalar());
                SysCon.Close();
                return deminimis;
            }
            else
            {
                SysCon.Close();
                return deminimis = 0.00;
            }
        }

        public DataTable getPaySlipDetailsAllbyMonth(string empid, string month, string year)
        {
            SysCon.Open();
            SqlCommand cmd = new SqlCommand("sp_GetPaySlipRecordsPaySlipDetailsMonthly", SysCon);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@empid", empid);
            cmd.Parameters.AddWithValue("@month", month);
            cmd.Parameters.AddWithValue("@year", year);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            SysCon.Close();
            return dt;
        }

        public string ARName, ARDescription, ARSignature, ReqIncMessage;
        public void GetAuthorizedRepresentative(string userid)
        {
            SysCon.Close();
            SysCon.Open();
            SqlCommand cmd = new SqlCommand("sp_GetAuthorizedRepresentative", SysCon);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@EMPID", userid);
            SqlDataReader reader = cmd.ExecuteReader();
            reader.Read();
            if (reader.HasRows)
            {
                ARName = reader["EmpName"].ToString();
                ARDescription = reader["JobDesc"].ToString();
                ARSignature = reader["BaseID"].ToString();
                ReqIncMessage = reader["msgRequestorIncomplete"].ToString();
            }
            reader.Close();
            cmd.Dispose();
            SysCon.Close();
        }

        public DataTable getBIR1601C(string empid)
        {
            SysCon.Open();
            SqlCommand cmd = new SqlCommand("sp_getBIR1601C", SysCon);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@empid", empid);
            SqlDataAdapter ar = new SqlDataAdapter(cmd);
            DataTable ds = new DataTable();
            ar.Fill(ds);
            SysCon.Close();
            return ds;
        }

        public DataTable PaySlipTotalGovDeductions(string empid)
        {
            SysCon.Open();
            SqlCommand cmd = new SqlCommand("sp_GetPaySlipRecordsPaySlipDetailsAll");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@EMPID", empid);
            cmd.Connection = SysCon;
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable deduct = new DataTable();
            deduct.Load(reader);
            SysCon.Close();
            return deduct;
        }

        #region Dependentsearch
        public DataTable SearchDependentv2(string empid)
        {
            SysCon.Open();
            SqlCommand cmd = new SqlCommand("SearchDependentsv2 ");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@empid", empid);
            cmd.Connection = SysCon;
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable empmaster = new DataTable();
            empmaster.Load(reader);
            SysCon.Close();
            return empmaster;
        }
        #endregion

        public DataTable PaySlipTotalHours(string empid)
        {
            SysCon.Open();
            SqlCommand cmd = new SqlCommand("sp_GetPayslipTotalHours");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@empid", empid);
            cmd.Connection = SysCon;
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable payhourstotal = new DataTable();
            payhourstotal.Load(reader);
            SysCon.Close();
            return payhourstotal;
        }
        public DataTable GetBIRFORM1902(string empid)
        {
            SysCon.Open();
            SqlCommand cmd = new SqlCommand("sp_BIRFORM1902", SysCon);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@EMPID", empid);
            SqlDataAdapter ar = new SqlDataAdapter(cmd);
            DataTable ds = new DataTable();
            ar.Fill(ds);
            SysCon.Close();
            return ds;
        }

        public DataTable GetTaxStatus(string empid)
        {
            SysCon.Open();
            SqlCommand cmd = new SqlCommand("sp_GettaxStatus", SysCon);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@EMPID", empid);
            cmd.Connection = SysCon;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable temp = new DataTable();
            da.Fill(temp);
            SysCon.Close();
            return temp;
        }

        public DataTable GetBIRFORM1905(string empid)
        {
            SysCon.Open();
            SqlCommand cmd = new SqlCommand("sp_BIRFORM1905", SysCon);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@EMPID", empid);
            SqlDataAdapter ar = new SqlDataAdapter(cmd);
            DataTable ds = new DataTable();
            ar.Fill(ds);
            SysCon.Close();
            return ds;
        }

        public DataTable getPayrollHoursAmount(string empid)
        {
            SysCon.Open();
            SqlCommand cmd = new SqlCommand("sp_get_HRMS_Payroll_Hours_Amount");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@EMPID", empid);
            cmd.Connection = SysCon;
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable deduct = new DataTable();
            deduct.Load(reader);
            SysCon.Close();
            return deduct;
        }

    }
}